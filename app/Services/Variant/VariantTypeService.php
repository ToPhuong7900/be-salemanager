<?php

namespace App\Services\Variant;

use App\Libs\Common\Common;
use App\Models\Brand;
use App\Models\VariantType;
use App\Models\VariantTypeAttributes;
use App\Services\BaseService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class VariantTypeService extends BaseService
{
    private VariantType $variantType;
    private VariantTypeAttributes $variantTypeAttributes;

    public function setModelFields(array $inputs = []): VariantType
    {
        $this->variantType = new VariantType();
        Common::assignField($this->variantType, 'variant_type_name', $inputs);

        return $this->variantType;
    }

    public function setModelVariantTypeAttrFields(array $inputs = []): VariantTypeAttributes
    {
        $inputs['variant_type_id'] = $this->variantType->getKey();
        $this->variantTypeAttributes = new VariantTypeAttributes();
        Common::assignField($this->variantTypeAttributes, 'variant_type_id', $inputs);
        Common::assignField($this->variantTypeAttributes, 'variant_type_attributes_name', $inputs);
        Common::assignField($this->variantTypeAttributes, 'variant_type_attributes_active', $inputs);

        return $this->variantTypeAttributes;
    }
    public function setModel(): string
    {
        return VariantType::class;
    }

    public function getVariantTypeValidator()
    {
        $attributes = $this->variantType->getAttributes();
        $rules = [
            'variant_type_name' => [
                'required',
                'max:15',
                'unique:variant_type,variant_type_name,' . $this->variantType->getKey() . ",variant_type_id"
            ]
        ];

        return Validator::make($attributes, $rules);
    }

    private function getVariantTypeAttributeValidator()
    {
        $variantTypeAttributes = $this->variantTypeAttributes->getAttributes();
        $rules = [
            'variant_type_attributes_name' => [
                'required',
                'max:15',
                'unique:variant_type_attributes,variant_type_attributes_name,' . $this->variantTypeAttributes->getKey() . ",variant_type_attributes_id"
            ],
            'variant_type_id' => 'required'
        ];

        return Validator::make($variantTypeAttributes, $rules);
    }

    private function storeVariantType($input)
    {
        $this->setModelFields($input);

        $validator = $this->getVariantTypeValidator();

        if ($validator->passes())
        {
            DB::beginTransaction();
            $this->variantType->save();

            DB::commit();

            return [
                'data' => $this->variantType,
                'success' => true
            ];
        }
        DB::rollBack();
        return [
            'success' => false,
            'error' => $validator->errors()
        ];
    }

    public function storeVariantTypeAttributes($input)
    {
        $validator = $this->storeVariantType($input);
        if ($validator['success'])
        {

            $this->setModelVariantTypeAttrFields($input);

            $validator = $this->getVariantTypeValidator();
            if ($validator->passes()) {

                $this->variantTypeAttributes->save();

                DB::commit();
                return [
                    'data' => $this->variantTypeAttributes,
                    'success' => true
                ];
            } else {
                DB::rollBack();
                return [
                    'success' => false,
                    'error' => $validator->errors()
                ];
            }
        }
        return [
            'success' => false,
            'error' => $validator['error']
        ];
    }
}
