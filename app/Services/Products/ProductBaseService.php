<?php

namespace App\Services\Products;

use App\Libs\Permissions\ProductPermissionContract;
use App\Models\Product;
use App\Services\BaseService;

class ProductBaseService extends BaseService
{
    /**
     * @var ProductPermissionContract|\Illuminate\Contracts\Foundation\Application|\Illuminate\Foundation\Application|mixed|null
     */
    private mixed $permissionService;

    public function __construct(?ProductPermissionContract $permissionService = null)
    {
        parent::__construct();
        if (is_null($permissionService)) {
            $permissionService = resolve(ProductPermissionContract::class);
            $permissionService->setUser(\Auth::user());
        }
        $this->permissionService = $permissionService;
    }

    public function setModel(): string
    {
        return Product::class;
    }
}
