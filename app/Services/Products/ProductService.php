<?php

namespace App\Services\Products;

use App\Libs\Common\Common;
use App\Libs\Common\CommonConstant;
use App\Models\Product;
use App\Models\Purchase;
use App\Models\PurchaseItem;
use App\Models\Variant;
use App\Services\BaseService;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class ProductService extends BaseService
{
    private Product $product;
    public function setModelFields(array $inputs = []): Product
    {
        $this->product = new Product();
        Common::assignField($this->product, 'product_code', $inputs);
        Common::assignField($this->product, 'product_sku', $inputs);
        Common::assignField($this->product, 'product_name', $inputs);
        Common::assignField($this->product, 'product_desc', $inputs);
        Common::assignField($this->product, 'category_id', $inputs);
        Common::assignField($this->product, 'sub_category_id', $inputs);
        Common::assignField($this->product, 'brand_id', $inputs);
        Common::assignField($this->product, 'warehouse_id', $inputs);
        Common::assignField($this->product, 'store_id', $inputs);
        Common::assignField($this->product, 'product_active', $inputs);
        Common::assignField($this->product, 'base_unit_id', $inputs);
        Common::assignField($this->product, 'selling_type_id', $inputs);
        Common::assignField($this->product, 'product_price', $inputs);
        Common::assignField($this->product, 'tax_type', $inputs);
        Common::assignField($this->product, 'order_tax', $inputs);

        return $this->product;
    }

    private function calculateGrandTotal($purchaseStock)
    {
        $purchaseStock['net_unit_cost'] = $this->product->product_price;
        if ($this->product->tax_type == Purchase::EXCLUSIVE) {
            $purchaseStock['tax_amount'] = (($purchaseStock['net_unit_cost'] * $this->product->order_tax) / 100) * $purchaseStock['quantity'];
            $perItemTaxAmount = $purchaseStock['tax_amount'] / $purchaseStock['quantity'];
        } else {
            $purchaseStock['tax_amount'] = ($purchaseStock['net_unit_cost'] * $this->product->order_tax) / (100 + $this->product->order_tax) * $purchaseStock['quantity'];
            $perItemTaxAmount = $purchaseStock['tax_amount'] / $purchaseStock['quantity'];
            $purchaseStock['net_unit_cost'] -= $perItemTaxAmount;
        }


        $purchaseStock['grand_total'] = ($purchaseStock['net_unit_cost'] + $perItemTaxAmount) * $purchaseStock['quantity'];

        return $purchaseStock;
    }

    public function storeProduct($input)
    {
        $this->setModelFields($input);
        $validator = $this->getProductValidator();
        if ($validator->passes()) {
            DB::beginTransaction();
            $this->product->save();

            //create purchase
            $purchaseStock = [
                'warehouse_id' => $input['warehouse_id'],
                'quantity' => $input['quantity'],
                'status' => $input['status'],
                'purchase_date' => $input['purchase_date'],
            ];

            if ($purchaseStock)
            {

                if (empty($purchaseStock['warehouse_id'])) {
                    throw new UnprocessableEntityHttpException('Please Select the warehouse.');
                } elseif ($purchaseStock['quantity'] <= 0) {
                    throw new UnprocessableEntityHttpException('Please Enter Attlist One stock Quantity.');
                } elseif (empty($purchaseStock['status'])) {
                    throw new UnprocessableEntityHttpException('Please Select the status.');
                }

                $purchaseStock['tax_rate'] = 0;
                $purchaseStock['tax_amount'] = 0;
                $purchaseStock['discount_value'] = 0;
                $purchaseStock['shipping'] = 0;
                $purchaseStock['payment_type'] = 0;

                $purchaseStock['date'] = $purchaseStock['date'] ?? date('Y/m/d');

                $purchaseInputArray = Arr::only($purchaseStock, [
                    'warehouse_id', 'purchase_date', 'status', 'discount_value', 'tax_rate', 'tax_amount', 'shipping', 'payment_type',
                ]);

                /** @var Purchase $purchase */
                $purchase = Purchase::create($purchaseInputArray);

                $value = $this->calculateGrandTotal($purchaseStock);

                $purchaseItemArr = [
                    'purchase_id' => $purchase->purchase_id,
                    'product_id' => $this->product->product_id,
                    'product_price' => $this->product->product_price,
                    'net_unit_cost' => $value['net_unit_cost'],
                    'tax_type' => $this->product->tax_type,
                    'tax_value' => $this->product->order_tax,
                    'tax_amount' => $value['tax_amount'],
                    'discount_type' => $input['discount_type'],
                    'discount_value' => 0,
                    'discount_amount' => 0,
                    'quantity' => $value['quantity'],
                    'grand_total' => $value['grand_total'],
                ];

                $purchaseItem = new PurchaseItem($purchaseItemArr);
                $purchase->purchaseItems()->save($purchaseItem);

                $purchase->update([
                    'grand_total' => $value['grand_total'],
                ]);
            }


            if (Arr::get($input, 'productType') == CommonConstant::VARIATION_PRODUCT) {
                $variant = new Variant();
                $variant->product_id = $this->product->getKey();
                $variant->variant_type_id = Arr::get($input, 'variant_type_id');
                $variant->save();
            }
            DB::commit();
            return [
                'data' => $this->product,
                'success' => true
            ];
        } else {
            DB::rollBack();
            return [
                'success' => false,
                'error' => $validator->errors()
            ];
        }
    }

    public function getProductValidator()
    {
        $attributes = $this->product->getAttributes();
        $rules = [
            'product_code' => [
                'required',
                'max:15',
                'unique:product,product_code,' . $this->product->getKey() . ",product_id"
            ],
            'brand_name' => ['max:15'],
            'brand_desc' => ['max:512'],
            'category_id' => 'required',
            'store_id' => 'required',
            'warehouse_id' => 'required'
        ];

        return Validator::make($attributes, $rules);
    }

    public function updateProduct($input, $id)
    {
        $this->getRecord($id);
        $validator = $this->getProductValidator();
        if ($validator->passes())
        {
            DB::beginTransaction();

            $this->product->update($input);
            $this->product->save();

            if (
                Arr::exists($input, 'quantity') ||
                Arr::exists($input, 'product_price') ||
                Arr::exists($input, 'tax_type')
            )
            {

                $purchaseItems = $this->product->purchasesItem()->get();

                foreach ($purchaseItems as $item)
                {
                    $purchaseItemArr['quantity'] = $input['quantity'] ?? $item->quantity;
                    $purchaseItemArr['tax_amount'] = $item->tax_amount;
                    $purchaseItemArr['product_price'] = $input['product_price'] ?? $item->product_price;
                    $purchaseItemArr['tax_type'] = $input['tax_type'] ?? $item->tax_type;

                    $valueUpdate = $this->calculateGrandTotal($purchaseItemArr);
                    $item->update($valueUpdate);
                    $item->save();
                }
            }
            DB::commit();
            return [
                'data' => $this->product,
                'success' => true
            ];
        } else {
            DB::rollBack();
            return [
                'success' => false,
                'error' => $validator->errors()
            ];
        }
    }

    public function delete($product)
    {
        $result = Common::canDelete($product, 'product_id', $product->getKey());
        if ($result)
        {
            $purchaseItems = $product->purchasesItem()->get();
            if ($purchaseItems->exists())
            {
                foreach ($purchaseItems as $item)
                {
                    $purchase = $item->purchase();
                    $purchase->delete();
                    $item->delete();
                }
            }
            $product->delete();
        }
        return $result;
    }

    public function getRecord($id)
    {
        $this->product = Product::find($id);
        return $this->product;
    }

    public function setModel(): string
    {
        return Product::class;
    }
}
