<?php

namespace Tfcloud\Services\Report\Grouping;

use Tfcloud\Jobs\ProcessGroupingReport;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\ReportConstant;
use Tfcloud\Lib\Constant\ReportGroupingConstant;
use Tfcloud\Models\AuditAction;
use Tfcloud\Models\Report\ReportFieldGroupModel;
use Tfcloud\Models\Report\ReportFieldGroupType;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\ContactService;
use Tfcloud\Services\Interfaces\Report\Export\ExportGenReportService;
use Tfcloud\Services\Report\Grouping\GroupingModel\DefaultGroupingService;
use Tfcloud\Models\InterfaceRunLog;
use Tfcloud\Services\AuditService;
use Tfcloud\Services\UserService;

class ReportGroupingService extends BaseService
{
    private $groupingModelList = [
        ReportConstant::SYSTEM_REPORT_HLP01 => [
            ReportFieldGroupModel::SITE_OWNER => GroupingModel\HLP01\HLP01SiteOwnerGroupingService::class
        ],
        ReportConstant::SYSTEM_REPORT_DLO04 => [
            ReportFieldGroupModel::DLO_OPERATIVE => GroupingModel\DLO04\DLO04JobOperativeGroupingService::class
        ]
    ];


    /**
     * Check available grouping
     */
    public function isGrouping($reportSchedule)
    {
        return $reportSchedule->report_field_group_id != null;
    }

    /**
     * Main function to run Process Grouping
     */
    public function processGroupingToDispatch(
        ExportGenReportService $exportGenReportService,
        $reportSchedule,
        $query,
        $args,
        $options
    ) {
        try {
            //ExportGenReportService
            $reportFieldGroup = $reportSchedule->reportFieldGroup;
            $reportFieldGroupModel = $reportFieldGroup->reportFieldGroupModel;
            $reportCode = $reportFieldGroup->system_report_code;
            $groupingModelId = $reportFieldGroup->report_field_group_model_id;
            $groupFieldName = $reportFieldGroup->group_field_name;
            $emailFieldName = $reportFieldGroup->email_field_name;

            $groupingService = $this->getServiceInstance($reportCode, $groupingModelId);
            $listToGenFiles = $groupingService->getGrouping($query, $groupFieldName, $emailFieldName);
            $report = $args['report'];

            $options['grouping'] = true;
            $options['reportScheduleId'] = $reportSchedule->report_schedule_id;
            $options['groupingModelId'] = $groupingModelId;
            $options['groupFieldName'] = $groupFieldName;
            $options['emailFieldName'] = $emailFieldName;
            $options['reportCode'] = $reportCode;
            //on/off writing Audit
            $options['disableAudit'] = config('cloud.grouping_report_email_summary', true);
            //remove setting returnsForGrouping
            $options['returnsForGrouping'] = false;
            //total of number Sent Email
            $sentEmailTotal = 0;

            foreach ($listToGenFiles as $index => $field) {
                if ($field->emailField) {
                    $options[ReportGroupingConstant::GROUPING_INDEX] = $index;
                    $options[ReportGroupingConstant::GROUPING_FIELD] = $field->groupingField;
                    $options[ReportGroupingConstant::GROUPING_EMAIL] = $field->emailField;
                    ProcessGroupingReport::dispatchNow($exportGenReportService, $args, $options);
                    $sentEmailTotal++;
                } else {
                    $this->auditReportGrouping($reportFieldGroupModel, $field->groupingField, $report);
                }
            }

            $this->auditSentEmailTotal($sentEmailTotal, count($listToGenFiles), $report, $options['disableAudit']);
        } catch (\Exception $exception) {
            $exportGenReportService
                ->interfaceLog(InterfaceRunLog::INTERFACE_RUN_STATUS_ERROR, $exception->getMessage());
        } catch (\Throwable $throwable) {
            $exportGenReportService
                ->interfaceLog(InterfaceRunLog::INTERFACE_RUN_STATUS_ERROR, $throwable->getMessage());
        }
    }

    /**
     * Making grouping query of every dispatch in processGroupingToDispatch
     * @param $query: QueryBuilder from gender report
     * @param $options: Options of dispatch of processGroupingToDispatch
     * @return QueryBuilder
     */
    public function processGroupingQuery($query, $options)
    {
        $emailFieldName = $options['emailFieldName'];
        $groupingIndex = $options[ReportGroupingConstant::GROUPING_INDEX];
        $emailField = $options[ReportGroupingConstant::GROUPING_EMAIL];
        $groupingField = $options[ReportGroupingConstant::GROUPING_FIELD];
        $reportCode = $options['reportCode'];
        $groupingModelId = $options['groupingModelId'];
        $groupFieldName = $options['groupFieldName'];

        $groupingService = $this->getServiceInstance($reportCode, $groupingModelId);
        $groupingQuery = $groupingService->groupingFilter($query, $groupFieldName, $groupingField);
        return $groupingQuery;
    }

    private function getServiceInstance($reportCode, $groupingModelId)
    {
        if (isset($this->groupingModelList[$reportCode])) {
            if (isset($this->groupingModelList[$reportCode][$groupingModelId])) {
                $instance = $this->groupingModelList[$reportCode][$groupingModelId];
                return new $instance();
            }
        }

        return new DefaultGroupingService();
    }

    //Audit
    private function auditReportGrouping($reportFieldGroupModel, $groupingId, $report)
    {
        $auditTextInner = null;
        switch ($reportFieldGroupModel->report_field_group_type_id) {
            case ReportFieldGroupType::CONTACT:
                $auditTextInner = $this->getContactAuditText($groupingId);
                break;

            case ReportFieldGroupType::USER:
                $auditTextInner = $auditTextInner = $this->getUserAuditText($groupingId);
                break;

            case ReportFieldGroupType::SITE:
                switch ($reportFieldGroupModel->report_field_group_model_code) {
                    case ReportGroupingConstant::MODEL_SITE_CONTACT:
                        $auditTextInner = $auditTextInner = $this->getContactAuditText($groupingId);
                        break;
                    case ReportGroupingConstant::MODEL_SITE_OWNER:
                        $auditTextInner = $this->getUserAuditText($groupingId);
                        break;
                }
                break;
        }

        $auditService = new AuditService();
        if ($auditTextInner) {
            $auditService->auditText = "Email cannot be sent to [{$auditTextInner}] because of missing Email address.";
        } else {
            $auditService->auditText = "User or Contact can't be found out by id [$groupingId].";
        }

        $auditService->addAuditEntry($report, AuditAction::ACT_REPORT);
    }

    private function auditSentEmailTotal($sentEmailTotal, $listToGenFilesTotal, $report, $disableAudit)
    {
        if ($listToGenFilesTotal && $disableAudit) {
            $auditService = new AuditService();
            $auditService->auditText =
                "{$sentEmailTotal} out of {$listToGenFilesTotal} Grouped Email(s) have been queued for sending.";
            $auditService->addAuditEntry($report, AuditAction::ACT_REPORT);
        }
    }

    private function getContactAuditText($contactId)
    {
        $contactService = new ContactService();
        $contact = $contactService->get($contactId);
        $auditTextInner = Common::concatFields([$contact->contact_name, $contact->organisation]);
        return $auditTextInner;
    }

    private function getUserAuditText($userId)
    {
        $userService = new UserService();
        $user = $userService->get($userId);
        $auditTextInner = $user->display_name;
        return $auditTextInner;
    }
}
