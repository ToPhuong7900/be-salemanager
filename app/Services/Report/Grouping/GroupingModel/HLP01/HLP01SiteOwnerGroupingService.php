<?php

namespace Tfcloud\Services\Report\Grouping\GroupingModel\HLP01;

use Tfcloud\Services\Report\Grouping\GroupingModel\BaseGroupingService;

class HLP01SiteOwnerGroupingService extends BaseGroupingService
{
    protected function groupingFilterQuery($query, $filterField, $filterValue)
    {
        return $query->where($filterField, $filterValue);
    }

    protected function getGroupingList($query, $groupingField, $emailField)
    {
        return $this->selectGroupingList($query, $groupingField, 'site_owner.email_to')
            ->leftJoin('user AS site_owner', $groupingField, 'site_owner.id')
            ->whereNotNull($groupingField)
            ->where($groupingField, '>', 0)
            ->groupBy($groupingField)
            ->get();
    }
}
