<?php

namespace Tfcloud\Services\Report\Grouping\GroupingModel\DLO04;

use Tfcloud\Services\Report\Grouping\GroupingModel\BaseGroupingService;

class DLO04JobOperativeGroupingService extends BaseGroupingService
{
    protected function groupingFilterQuery($query, $filterField, $filterValue)
    {
        return $query->whereRaw('FIND_IN_SET(' . $filterValue . ', operative_ids)');
    }

    protected function getGroupingList($query, $groupingField, $emailField)
    {
        return $this->selectGroupingList($query, 'operative_user.id', 'operative_user.email_to')
            ->leftJoin('user AS operative_user', function ($leftJoin) {
                $leftJoin->on(\DB::raw('FIND_IN_SET(operative_user.id, operative_ids)'), '>', \DB::raw('0'));
            })
            ->where('operative_ids', '<>', '')
            ->groupBy('operative_user.id')
            ->get();
    }
}
