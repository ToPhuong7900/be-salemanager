<?php

namespace Tfcloud\Services\Report\Grouping\GroupingModel;

class DefaultGroupingService extends BaseGroupingService
{
    protected function groupingFilterQuery($query, $filterField, $filterValue)
    {
        return $query->where($filterField, $filterValue);
    }

    protected function getGroupingList($query, $groupingField, $emailField)
    {
        if ($groupingField == $emailField) {
            //1. Group Field is Email
            //2. Condition works with String field only
            $queryGroup = $query->where($groupingField, '<>', '');
        } else {
            $queryGroup = $query->whereNotNull($groupingField)->where($groupingField, '>', 0);
        }
        return $queryGroup->groupBy($groupingField)->get();
    }
}
