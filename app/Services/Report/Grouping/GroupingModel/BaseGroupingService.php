<?php

namespace Tfcloud\Services\Report\Grouping\GroupingModel;

use Tfcloud\Lib\Constant\ReportGroupingConstant;

abstract class BaseGroupingService
{
    protected $clearGroupingListOrder = true;

    //please go to defined functions
    abstract protected function groupingFilterQuery($query, $filterField, $filterValue);
    abstract protected function getGroupingList($query, $groupingField, $emailField);

    protected function beforeGroupingFilterQuery($query)
    {
        return $query;
    }

    protected function beforeGetGroupingList($query, $groupingField, $emailField)
    {
        //clear all orders
        if ($this->clearGroupingListOrder) {
            $query->getQuery()->orders = null;
        }

        //set new order
        return $query->orderByRaw(ReportGroupingConstant::GROUPING_EMAIL . " <> ''");
    }

    protected function selectGroupingList($query, $groupingField, $emailField)
    {
        return $query->select(
            "{$groupingField} AS " . ReportGroupingConstant::GROUPING_FIELD,
            "{$emailField} AS " . ReportGroupingConstant::GROUPING_EMAIL
        );
    }

    /**
     * Output
     * @param $query
     * @param $filterField
     * @param $filterValue
     * @return mixed
     */
    public function groupingFilter($query, $filterField, $filterValue)
    {
        $groupingQuery = $this->beforeGroupingFilterQuery($query);
        return $this->groupingFilterQuery($groupingQuery, $filterField, $filterValue);
    }

    /**
     * Ouput
     * @param $query
     * @param $groupingField
     * @param $emailField
     * @return mixed
     */
    public function getGrouping($query, $groupingField, $emailField)
    {
        $groupingQuery = $this->selectGroupingList($query, $groupingField, $emailField);
        $groupingQuery = $this->beforeGetGroupingList($groupingQuery, $groupingField, $emailField);
        return $this->getGroupingList($groupingQuery, $groupingField, $emailField);
    }
}
