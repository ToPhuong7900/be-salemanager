<?php

namespace Tfcloud\Services\Report;

use Input;
use Tfcloud\Lib\Exceptions\InvalidRecordException;
use Tfcloud\Lib\Exceptions\PermissionException;
use Tfcloud\Lib\Filters\UserReportFilterFilter;
use Tfcloud\Models\AuditAction;
use Tfcloud\Models\CrudAccessLevel;
use Tfcloud\Models\Module;
use Tfcloud\Models\ReportDateOption;
use Tfcloud\Models\ReportField;
use Tfcloud\Models\ReportFieldType;
use Tfcloud\Models\ReportOperatorType;
use Tfcloud\Models\UserReport;
use Tfcloud\Models\UserReportFilter;
use Tfcloud\Services\AuditService;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\PermissionService;

class UserReportFilterAppliedService extends BaseService
{
    /*
     * Tfcloud\Models\UserReport
     */
    protected $userReport = null;
    protected $userReportField = null;

    /*
     * Tfcloud\Models\SystemReport
     */
    protected $systemReport = null;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    public function getAll($id)
    {
        if (
            !$this->permissionService->hasModuleAccess(
                Module::MODULE_REPORTS,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
            )
        ) {
            $msg = "Not met minimum readonly access for report module in class" .
                " (" . __CLASS__ . ") method (" . __METHOD__ . "). " .
                " See line (" . __LINE__ . ")";
            throw new PermissionException($msg);
        }

        $query = $this->all($id);
        $this->filterAll($query);

        return $query;
    }

    public static function filterAll(&$query)
    {
        $filter = new UserReportFilterFilter(Input::all());
        $query->orderBy($filter->sort, $filter->sortOrder);
        if ($filter->sort != "code") {
            $query->orderBy('report_field_code', 'ASC');
        }
    }

    public function getReportFields($userReport)
    {
        return ReportField::orderby('report_field.output_order')
            ->leftjoin(
                'report_field_type',
                'report_field.report_field_type_id',
                '=',
                'report_field_type.report_field_type_id'
            )
            ->where('report_field.system_report_id', $userReport->system_report_id)
            ->get()
        ;
    }

    public function getReportDateFields($userReport)
    {
        return ReportField::orderby('report_field.output_order')
            ->leftjoin(
                'report_field_type',
                'report_field.report_field_type_id',
                '=',
                'report_field_type.report_field_type_id'
            )
            ->where('report_field.system_report_id', $userReport->system_report_id)
            ->where('report_field_type.report_field_type_id', ReportFieldType::REP_FIELD_TYPE_DATE)
            ->get();
    }

    public function getReportOperators($filter)
    {
        $reportOperatorType = ReportOperatorType::select();
        if ($filter->report_field_type_id == ReportFieldType::REP_FIELD_TYPE_DATE) {
            $reportOperatorType->where('avail_for_date', 'Y');
        } elseif ($filter->report_field_type_id == ReportFieldType::REP_FIELD_TYPE_TEXT) {
            $reportOperatorType->where('avail_for_text', 'Y');
        } elseif ($filter->report_field_type_id == ReportFieldType::REP_FIELD_TYPE_NUMERIC) {
            $reportOperatorType->where('avail_for_numeric', 'Y');
        } elseif ($filter->report_field_type_id == ReportFieldType::REP_FIELD_TYPE_BOOLEAN_NUMERIC) {
            $reportOperatorType->where('avail_for_01', 'Y');
        } elseif ($filter->report_field_type_id == ReportFieldType::REP_FIELD_TYPE_BOOLEAN_CHAR) {
            $reportOperatorType->where('avail_for_YN', 'Y');
        } elseif ($filter->report_field_type_id == ReportFieldType::REP_FIELD_TYPE_YN_UNKNOWN) {
            $reportOperatorType->where('avail_for_YN_UNKNOWN', 'Y');
        }

        $reportDateOption = [];
        if ($filter->report_field_type_id == ReportFieldType::REP_FIELD_TYPE_DATE) {
            $reportDateOption = ReportDateOption::select()->orderBy("display_order")
                ->get(['report_date_option_id', 'report_date_option_code', 'report_date_option_desc']);
        }

        return [
            'reportOperatorType' => $reportOperatorType
                ->get(['report_operator_type_id', 'report_operator_type_code', 'report_operator_type_desc']),
            'reportDateOption' => $reportDateOption,
        ];
    }

    public function getUserReport($id, $audit = true)
    {
        if (
            !$this->permissionService->hasModuleAccess(
                Module::MODULE_REPORTS,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
            )
        ) {
            $msg = "Not met minimum readonly access for report module in class" .
                " (" . __CLASS__ . ") method (" . __METHOD__ . "). " .
                " See line (" . __LINE__ . ")";
            throw new PermissionException($msg);
        }

        $this->userReport = UserReport::userSiteGroup()
            ->where('user_report_id', $id)
            ->first();

        if (is_null($this->userReport)) {
            throw new InvalidRecordException("No User Report found");
        }

        if ($audit) {
            $auditService = new AuditService();
            $auditService->addAuditEntry($this->userReport, AuditAction::ACT_VIEW);
        }

        return $this->userReport;
    }

    private function all($userRepId)
    {
        $query = UserReportFilter::select([
                "user_report_filter.user_report_filter_id",
                "report_field.report_field_code",
                "report_field_type.report_field_type_id",
                "report_field_type.report_field_type_code",
                "user_report_filter.user_report_id",
                "user_report_filter.filter_value",
                "report_operator_type.report_operator_type_code",
                "report_date_option.report_date_option_id",
                "report_date_option.report_date_option_code",
                "user_report_filter.report_operator_type_id",
                "user_report_filter.report_field_id",
                "user_report_filter.filter_value_as_field_id",
            ])
            ->where('user_report_filter.user_report_id', $userRepId)
            ->leftjoin('report_field', 'user_report_filter.report_field_id', "=", 'report_field.report_field_id')
            ->leftjoin(
                'report_field_type',
                'report_field_type.report_field_type_id',
                "=",
                'report_field.report_field_type_id'
            )
            ->leftjoin(
                'report_operator_type',
                'user_report_filter.report_operator_type_id',
                "=",
                'report_operator_type.report_operator_type_id'
            )
            ->leftjoin(
                'report_date_option',
                'user_report_filter.report_date_option_id',
                "=",
                'report_date_option.report_date_option_id'
            )
        ;

        return $query;
    }

    private function setModelFields($inputs)
    {
    }

    private function getUserReportValidator($isEdit = false)
    {
    }
}
