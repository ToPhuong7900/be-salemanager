<?php

namespace Tfcloud\Services\Report;

use Exception;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Tfcloud\Lib\AuditFields;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Constant\ReportConstant;
use Tfcloud\Lib\Exceptions\InvalidRecordException;
use Tfcloud\Lib\Exceptions\PermissionException;
use Tfcloud\Lib\Filters\Report\ReportQueueFilter;
use Tfcloud\Models\AuditAction;
use Tfcloud\Models\CrudAccessLevel;
use Tfcloud\Models\IdControl;
use Tfcloud\Models\Module;
use Tfcloud\Models\Report;
use Tfcloud\Models\Report\ReportQueue;
use Tfcloud\Models\Report\ReportQueueControl;
use Tfcloud\Models\Report\ReportQueueStatus;
use Tfcloud\Models\SiteGroup;
use Tfcloud\Models\SystemReport;
use Tfcloud\Models\UserReport;
use Tfcloud\Services\AuditService;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\WkHtmlToPdf\AsbestosReportForCBService;
use Tfcloud\Services\Report\WkHtmlToPdf\AsbestosReportForHertsService;
use Tfcloud\Services\Report\WkHtmlToPdf\AsbestosReportForNELService;
use Tfcloud\Services\Report\WkHtmlToPdf\CNDNELincsSurveyBuildingService;
use Tfcloud\Services\Report\WkHtmlToPdf\CPC04CostPlusSalesInvoiceStatementService;
use Tfcloud\Services\Report\WkHtmlToPdf\ProjectHighLightService;
use Tfcloud\Services\Report\WkHtmlToPdf\AsbestosReportForBradfordService;
use Tfcloud\Services\Report\WkHtmlToPdf\ProjectMonitoringReportService;
use Tfcloud\Services\Report\WkHtmlToPdf\CND09ConditionSurveyElementInformation;
use Tfcloud\Services\Report\WkHtmlToPdf\SiteAsbestosReportGwyneddService;
use Tfcloud\Services\Report\WkHtmlToPdf\Sefton\SiteAsbestosActionReportService;
use Tfcloud\Services\Report\WkHtmlToPdf\Sefton\SiteAsbestosRegisterReportService;

class ReportQueueService extends BaseService
{
    /*
     * Tfcloud\Models\Report\ReportQueue
     */
    protected $reportQueue;

    /*
     * Tfcloud\Services\ReportService
     */
    protected $reportService;

    /*
     * Tfcloud\Services\PermissionService
     */
    protected $permissionService;

    protected $log;

    public function __construct(
        PermissionService $permissionService,
        ReportService $reportService
    ) {
        $this->permissionService = $permissionService;
        $this->reportService = $reportService;
        $this->log = new Logger(CommonConstant::LOGGER_REPORT_BACKGROUND);
        $file = Common::initialiseReportBackgroundLogFile();
        $this->log->pushHandler(new StreamHandler($file, Logger::INFO));
    }

    public function get($id, $audit = true)
    {
        $this->permissionService
            ->hasModuleAccess(Module::MODULE_REPORTS, CrudAccessLevel::CRUD_ACCESS_LEVEL_READ, true);

        $query = ReportQueue::userSiteGroup()->where('report_queue_id', $id);

        if (!$this->permissionService->isAdministratorOrSuperuser()) {
            $query->where('user_id', \Auth::user()->getKey());
        }

        $this->reportQueue = $query->first();

        if (
            !$this->checkPermissionForModule($this->reportQueue, CrudAccessLevel::CRUD_ACCESS_LEVEL_READ)
            && $this->reportQueue->user_id != \Auth::user()->getKey()
        ) {
            $msg = "Not met minimum readonly access for report module";
            throw new PermissionException($msg);
        }

        if (is_null($this->reportQueue)) {
            throw new InvalidRecordException("No report found.");
        }

        if ($audit) {
            $auditService = new AuditService();
            $auditService->addAuditEntry($this->reportQueue, AuditAction::ACT_VIEW);
        }

        return $this->reportQueue;
    }

    public function getAll($inputs)
    {
        $this->permissionService
            ->hasModuleAccess(Module::MODULE_REPORTS, CrudAccessLevel::CRUD_ACCESS_LEVEL_READ, true);

        if ($this->permissionService->isAdministratorOrSuperuser()) {
            // Admin users, superuser or report module supervisors can see all scheduled reports
        } else {
            // Can only see their own scheduled reports.
            $inputs['owner'] = \Auth::user()->getKey();
        }

        return $this->filterAll($this->all(), $inputs);
    }

    public static function filterAll($query, $inputs)
    {
        $filter = new ReportQueueFilter($inputs);

        if (!is_null($filter->code)) {
            $query->where('report_queue_code', 'like', "%{$filter->code}%");
        }

        if (!is_null($filter->report)) {
            $query->where(function ($subQuery) use ($filter) {
                $subQuery->where('system_report.system_report_code', 'like', '%' . $filter->report . '%');
                $subQuery->orWhere('user_report.user_report_code', 'like', '%' . $filter->report . '%');
            });
        }

        if (!is_null($filter->reference)) {
            $query->where('user_reference', 'like', "%{$filter->reference}%");
        }

        if (!is_null($filter->owner)) {
            $query->where('report_queue.user_id', $filter->owner);
        }

        if (!is_null($filter->reportQueueStatus)) {
            $query->where('report_queue.report_queue_status_id', $filter->reportQueueStatus);
        }

        $query->orderBy($filter->sort, $filter->sortOrder);

        if ($filter->sort == 'report_code') {
            $query->orderBy('report_desc', $filter->sortOrder);
        }

        return $query;
    }

    public function newReportQueue()
    {
        $this->reportQueue = new ReportQueue();
        $this->initDefaultData();
        return $this->reportQueue;
    }

    public function addReportQueue(&$id, $inputs, array $options = [])
    {
        $this->newReportQueue();
        $this->formatInputData($inputs);
        $this->setModelFields($inputs);

        if (! $this->checkPermissionForModule($this->reportQueue, CrudAccessLevel::CRUD_ACCESS_LEVEL_CREATE)) {
            $msg = "Not met minimum create new access for report module";
            throw new PermissionException($msg);
        }

        $validator = $this->getReportQueueValidator($inputs);

        \DB::beginTransaction();
        $sErrMsg = "";
        $newCode = "";
        $bSequense = array_get($options, 'firstRow', true);
        if (
            IdControl::validateAndGetNextCode(
                Module::MODULE_REPORTS,
                'report_queue',
                $this->reportQueue->report_queue_code,
                $newCode,
                $sErrMsg,
                $bSequense
            )
        ) {
            //set next code here
            $this->reportQueue->report_queue_code = $newCode;
            $validator->success = true;
        } else {
            $validator->errors()->add('report_queue_code', $sErrMsg);
            $validator->success = false;
        }

        $validator->success = $validator->success && $validator->passes();
        if ($validator->success) {
            $this->reportQueue->save();

            $audit = new AuditService();
            $audit->addAuditEntry($this->reportQueue, AuditAction::ACT_INSERT);

            $id = $this->reportQueue->getKey();
            \DB::commit();
            try {
                $env = \App::environment();
                $command = \Config::get('cloud.commands.artisan');
                $command .= ' report:generate ';
                $command .= ' --env=' . $env;
                $command .= ' "' . $id . '" ';
                $process = popen($command, 'r');
                if (!is_resource($process)) {
                    $this->reportQueue->report_queue_status_id = ReportQueueStatus::FAIL;
                    $this->reportQueue->save();
                    $this->log->error("Failed to execute generation for report_queue_id=$id!", ['ERROR']);
                }
            } catch (Exception $ex) {
                $this->log->error($ex->getMessage(), ['ERROR']);
            }
        } else {
            \DB::rollback();
        }

        return $validator;
    }

    public function updateReportQueue($id, $inputs)
    {
        $this->permissionService
            ->hasModuleAccess(Module::MODULE_REPORTS, CrudAccessLevel::CRUD_ACCESS_LEVEL_UPDATE, true);

        $this->reportQueue = $this->get($id, false);

        if (!$this->checkPermissionForModule($this->reportQueue, CrudAccessLevel::CRUD_ACCESS_LEVEL_UPDATE)) {
            $msg = "Not met minimum update access for report module in class" .
                " (" . __CLASS__ . ") method (" . __METHOD__ . "). " .
                " See line (" . __LINE__ . ")";
            throw new PermissionException($msg);
        }

        $this->setModelFields($inputs);
        $this->formatInputData($inputs);
        $validator = $this->getReportQueueValidator(true);

        if ($validator->passes()) {
            \DB::transaction(function () {
                $auditFields = $this->setupAuditFields();
                $audit = new AuditService();
                $audit->auditFields($auditFields);
                $audit->addAuditEntry($this->reportQueue, AuditAction::ACT_UPDATE);
                $this->reportQueue->save();
            });

            $validator->success = true;
        }

        return $validator;
    }

    public function cancelReportQueue($id)
    {
        $this->reportQueue = $this->get($id);

        $messages = $this->checkCancelReportQueue($this->reportQueue);

        if (count($messages) === 0) {
            // 1. Update status to cancelling
            $this->reportQueue->report_queue_status_id = ReportQueueStatus::REQCAN;
            $this->update();

            // 2. Perform cancel
            //$messages = $this->killReport();
            $this->killReportControlProcess($id);

            // 3. Update status to cancel
            $this->reportQueue->report_queue_status_id = ReportQueueStatus::CAN;
            $this->reportQueue->details = 'Cancel requested by ' . $this->reportQueue->user->username;
            $this->update();
        }

        return $messages;
    }

    public function checkCancelReportQueue($reportQueue)
    {
        $messages = new MessageBag();

        if (
            !in_array(
                $reportQueue->report_queue_status_id,
                [ReportQueueStatus::PEN, ReportQueueStatus::RUN]
            )
        ) {
            $messages->add('Report Queue', 'This report is not running or pending.');
        }
        return $messages;
    }

    public function generateReportInQueue($reportQueueId)
    {
        $messages = new MessageBag();

        $this->reportQueue = $this->get($reportQueueId);

        if (count($messages) === 0) {
            // 1. First, update status to Running
            $this->reportQueue->report_queue_status_id = ReportQueueStatus::RUN;
            $this->update();

            // 2. Generate report
            if ($this->reportQueue->system_report_id) {
                $repId = $this->reportQueue->system_report_id;
                $repType = Report::SYSTEM_REPORT;
            } else {
                $repId = $this->reportQueue->system_report_id;
                $repType = Report::USER_REPORT;
            }
            $filterJson = $this->reportQueue->filter_query;
            $filterArr = json_decode($filterJson, true);
            $reportDir = \Config::get('cloud.legacy_paths.uploads')
                . DIRECTORY_SEPARATOR . \Auth::User()->site_group_id
                . DIRECTORY_SEPARATOR . 'reports'
                . DIRECTORY_SEPARATOR . $this->reportQueue->getKey();

            $inputs = [
                'repId' => $repId,
                'repType' => $repType,
                'reportTech' => ReportConstant::GENERAL_TECH,
                'repDownloadPath' => $reportDir,
                'repFormat' => 2
            ];

            $reportGenerateService = new ReportGenerateService($this->permissionService, $this->reportService);
            $returnData = $reportGenerateService->generateReport(array_merge($inputs, $filterArr));

            if (
                ($returnData === false)
                || (!is_array($returnData))
                || (array_get($returnData, 'result') !== true)
            ) {
                // 3a. Fail
                $messages->add('Report Queue', 'Fail.');
                $this->reportQueue->report_queue_status_id = ReportQueueStatus::FAIL;
                $this->update();
            } else {
                // 3b. Success, update status to Completed
                $this->reportQueue->report_queue_status_id = ReportQueueStatus::COMP;
                $this->update();
            }
        }

        return $messages;
    }

    public static function existRunningReport()
    {
        return ReportQueue::userSiteGroup()
            ->where('user_id', \Auth::user()->getKey())
            ->where('report_queue_status_id', '<=', ReportQueueStatus::RUN)
            ->exists()
        ;
    }

    public function lookups()
    {
        return $this->reportService->filterLookups();
    }

    public function filterLookups()
    {
        $reportQueueStatus = ReportQueueStatus::orderBy('report_queue_status_code')->whereIn('report_queue_status_id', [
            ReportQueueStatus::PEN,
            ReportQueueStatus::RUN,
            ReportQueueStatus::COMP,
            ReportQueueStatus::FAIL,
        ])->get();

        return [
            'reportQueueStatus' => $reportQueueStatus
        ];
    }

    public function checkPermissionForModule($reportQueue, $crudLevel)
    {
        if (is_null($reportQueue)) {
            return true;
        }
        $bAccess = true;
        if ($reportQueue->system_report_id) {
            if (empty($reportQueue->user_report_id)) {
                $bAccess = $this->permissionService->hasModuleAccess(
                    $reportQueue->systemReport->module_id,
                    $crudLevel
                );
            }
        } elseif ($reportQueue->user_report_id) {
            if (is_null($reportQueue->userReport)) {
                $bAccess = false;
            } else {
                $bAccess = $this->permissionService->hasModuleAccess(
                    $reportQueue->userReport->systemReport->module_id,
                    $crudLevel
                );
            }
        }

        return $bAccess;
    }

    public function getDownloadFilePath($id)
    {
        $this->reportQueue = $this->get($id);

        if (
            ($this->reportQueue->user_id != \Auth::User()->getKey() && !$this->permissionService->isSuperuser())
            || $this->reportQueue->report_queue_status_id != ReportQueueStatus::COMP
        ) {
            return null;
        }

        $reportDir = Common::getUploadSectionStoragePathway(
            \Auth::User()->site_group_id,
            ReportConstant::REPORT_UPLOAD_FOLDER,
            $this->reportQueue->getKey()
        );

        $repFileName = $this->reportQueue->report_file;

        return $reportDir . $repFileName;
    }

    protected function killReportControlProcess($reportQueueId)
    {
        $reportQueueControl = ReportQueueControl::where('report_queue_id', $reportQueueId)->first();
        if ($reportQueueControl) {
            $pid = $reportQueueControl->system_process_id;
            $isRunning = $this->checkPIDInTaskList($pid);
            if ($isRunning) {
                $killExec = exec("taskkill /F /PID $reportQueueControl->system_process_id");

                if (strpos($killExec, "SUCCESS") === false) {
                    //Log for investigating
                    $this->log->error(
                        "Fail to kill PID= $pid for report_queue_id = $reportQueueId. ErrMsg: $killExec",
                        ['ERROR']
                    );
                }
            }
            $reportQueueControl->delete();
        }
    }

    protected function checkPIDInTaskList($pid)
    {
        $tasks = shell_exec("tasklist /fi \"PID eq $pid\"");
        if (strpos($tasks, "$pid") !== false) {
            return true;
        }
        return false;
    }

    private function initDefaultData()
    {
        $this->reportQueue->site_group_id = SiteGroup::get()->site_group_id;
        $this->reportQueue->user_id       = \Auth::User()->getKey();
        $this->reportQueue->requested     = date('Y-m-d H:i:s');
        $this->reportQueue->email_report  = CommonConstant::DATABASE_VALUE_NO;
        $this->reportQueue->report_queue_status_id = ReportQueueStatus::PEN;
    }

    public function all()
    {
        return ReportQueue::userSiteGroup()
            ->join('user', 'user.id', '=', 'report_queue.user_id')
            ->leftJoin(
                'system_report',
                'system_report.system_report_id',
                '=',
                'report_queue.system_report_id'
            )
            ->leftJoin(
                'user_report',
                'user_report.user_report_id',
                '=',
                'report_queue.user_report_id'
            )
            ->leftJoin(
                'report_queue_status',
                'report_queue_status.report_queue_status_id',
                '=',
                'report_queue.report_queue_status_id'
            )
            ->select([
                'report_queue.*',
                'report_queue_status.report_queue_status_code',
                'report_queue_status.report_queue_status_desc',
                'user.username',
                \DB::raw(
                    "IFNULL(system_report.system_report_code,"
                    . " IFNULL(user_report.user_report_code, '')) as report_code"
                ),
                \DB::raw(
                    "IFNULL(system_report.system_report_desc,"
                    . " IFNULL(user_report.user_report_desc, '')) as report_desc"
                ),
                'system_report.system_report_code',
                'system_report.system_report_desc',
                'user_report.user_report_code',
                'user_report.user_report_desc'
            ]);
    }

    private function update()
    {
        \DB::transaction(function () {
            $auditFields = $this->setupAuditFields();
            $audit = new AuditService();
            $audit->auditFields($auditFields);
            $audit->addAuditEntry($this->reportQueue, AuditAction::ACT_UPDATE);
            $this->reportQueue->save();
        });
    }

    private function parseFiterQuery(&$inputs)
    {
        $filterQuery = $inputs;
        unset($filterQuery['_token']);
        unset($filterQuery['repId']);
        unset($filterQuery['repType']);
        unset($filterQuery['repTitle']);
        unset($filterQuery['startRow']);
        unset($filterQuery['maxRows']);
        unset($filterQuery['user_reference']);
        unset($filterQuery['report_queue_status_id']);
        unset($filterQuery['output_report_format_id']);
        $inputs['filter_query'] = json_encode($filterQuery);
    }

    private function formatInputData(&$inputs)
    {
        $inputs['max_row'] = array_get($inputs, 'maxRows', 100000);
        $inputs['start_row'] = array_get($inputs, 'startRow', 1);
        $inputs['report_queue_status_id'] = ReportQueueStatus::PEN;
        $selectorInputs = ['site_id', 'building_id', 'room_id','plant_id', 'project', 'cpContract'];
        if (array_key_exists('flt', $inputs)) {
            foreach ($inputs['flt'] as $key => $val) {
                if (in_array($val['f'], $selectorInputs)) {
                    $inputs[$val['f']] =  $val['v'];
                }
                $inputs['cpContract'] = $val['v'];
            }
        }
    }

    private function setModelFields($inputs)
    {
        $this->parseFiterQuery($inputs);
        Common::assignField($this->reportQueue, 'system_report_id', $inputs);
        Common::assignField($this->reportQueue, 'user_report_id', $inputs);
        Common::assignField($this->reportQueue, 'filter_query', $inputs);
        Common::assignField($this->reportQueue, 'output_report_format_id', $inputs);
        Common::assignField($this->reportQueue, 'user_reference', $inputs);
        Common::assignField($this->reportQueue, 'report_queue_status_id', $inputs);
        Common::assignField($this->reportQueue, 'email_report', $inputs);
        Common::assignField($this->reportQueue, 'start_row', $inputs);
        Common::assignField($this->reportQueue, 'max_row', $inputs);
    }

    private function getReportQueueValidator($inputs = [])
    {
        $rules = [
            'max_row'           => ['integer', 'max:2147483647', 'min:0'],
            'start_row'         => ['integer', 'max:2147483647', 'min:0'],
            'system_report_id'  => ['required_without:user_report_id'],
            'user_reference'    => ['max:45'],
        ];

        $messages = [
            'system_report_id.required_without'     => 'Either a system report or user report must be selected.',
        ];

        $ruleMessages = $this->customRuleAndMessages($inputs);

        $validator = Validator::make(
            $inputs,
            array_merge($rules, $ruleMessages['rules']),
            array_merge($messages, $ruleMessages['messages'])
        );
        $validator->success = false;

        return $validator;
    }

    private function customRuleAndMessages($inputs)
    {
        $reportCode = array_get($inputs, 'report_code');
        $validateRuleMessages = ['rules' => [], 'messages' => []];
        if ($reportCode) {
            switch ($reportCode) {
                case ReportConstant::SYSTEM_REPORT_ASB_CLI_HRT01:
                    $asbestosReportForHertsService = new AsbestosReportForHertsService($this->permissionService);
                    $validateRuleMessages['rules'] = $asbestosReportForHertsService->validateRules($inputs);
                    $validateRuleMessages['messages'] = $asbestosReportForHertsService->validateMessages($inputs);
                    break;
                case ReportConstant::SYSTEM_REPORT_ASB_CLI_GWN01:
                    $asbestosReportForHertsService = new SiteAsbestosReportGwyneddService($this->permissionService);
                    $validateRuleMessages['rules'] = $asbestosReportForHertsService->validateRules($inputs);
                    $validateRuleMessages['messages'] = $asbestosReportForHertsService->validateMessages($inputs);
                    break;
                case ReportConstant::SYSTEM_REPORT_ASB_CLI_SEF01:
                    $asbestosRegisterReportForSeftonService =
                        new SiteAsbestosRegisterReportService($this->permissionService);
                    $validateRuleMessages['rules'] = $asbestosRegisterReportForSeftonService->validateRules($inputs);
                    $validateRuleMessages['messages'] =
                        $asbestosRegisterReportForSeftonService->validateMessages($inputs);
                    break;
                case ReportConstant::SYSTEM_REPORT_ASB_CLI_SEF02:
                    $asbestosActionReportForSeftonService =
                        new SiteAsbestosActionReportService($this->permissionService);
                    $validateRuleMessages['rules'] = $asbestosActionReportForSeftonService->validateRules($inputs);
                    $validateRuleMessages['messages'] =
                        $asbestosActionReportForSeftonService->validateMessages($inputs);
                    break;
                case ReportConstant::SYSTEM_REPORT_CND_CLI_SUN01:
                    $reportService = new WkHtmlToPdf\Sunderland\CNDCLISUN01ReportService($this->permissionService);
                    $validateRuleMessages['rules'] = $reportService->validateRules($inputs);
                    $validateRuleMessages['messages'] = $reportService->validateMessages($inputs);
                    break;
                case ReportConstant::SYSTEM_REPORT_QST_CLI_SUN02:
                    $reportService = new WkHtmlToPdf\Sunderland\QSTCLISUN02ReportService($this->permissionService);
                    $validateRuleMessages['rules'] = $reportService->validateRules($inputs);
                    $validateRuleMessages['messages'] = $reportService->validateMessages($inputs);
                    break;
                case ReportConstant::SYSTEM_REPORT_ASB_CLI_NEL01:
                    $asbestosReportForNELService = new AsbestosReportForNELService($this->permissionService);
                    $validateRuleMessages['rules'] = $asbestosReportForNELService->validateRules($inputs);
                    $validateRuleMessages['messages'] = $asbestosReportForNELService->validateMessages($inputs);
                    break;
                case ReportConstant::SYSTEM_REPORT_CPC04:
                case ReportConstant::SYSTEM_REPORT_CPC05:
                    $costPlusSISService = new CPC04CostPlusSalesInvoiceStatementService($this->permissionService);
                    $validateRuleMessages['rules'] = $costPlusSISService->validateRules();
                    $validateRuleMessages['messages'] = $costPlusSISService->validateMessages($inputs);
                    break;
                case ReportConstant::SYSTEM_REPORT_PRJ13:
                    $projectHighlightService = new ProjectHighLightService($this->permissionService);
                    $validateRuleMessages['rules'] = $projectHighlightService->validateRules($inputs);
                    $validateRuleMessages['messages'] = $projectHighlightService->validateMessages($inputs);
                    break;
                case ReportConstant::SYSTEM_REPORT_ASB_CLI_BRA01:
                    $asbestosReportForBradfordService = new AsbestosReportForBradfordService($this->permissionService);
                    $validateRuleMessages['rules'] = $asbestosReportForBradfordService->validateRules($inputs);
                    $validateRuleMessages['messages'] = $asbestosReportForBradfordService->validateMessages($inputs);
                    break;
                case ReportConstant::SYSTEM_REPORT_CND09:
                    $condElementService = new CND09ConditionSurveyElementInformation($this->permissionService);
                    $validateRuleMessages['rules'] = $condElementService->validateRules($inputs);
                    $validateRuleMessages['messages'] = $condElementService->validateMessages($inputs);
                    break;
                case ReportConstant::SYSTEM_REPORT_PRJ11:
                    $projectMonitoringReportService = new ProjectMonitoringReportService($this->permissionService);
                    $validateRuleMessages['rules'] = $projectMonitoringReportService->validateRules($inputs);
                    $validateRuleMessages['messages'] = $projectMonitoringReportService->validateMessages($inputs);
                    break;
                case ReportConstant::SYSTEM_REPORT_CND_CLI_NEL01:
                    $CNDNELincsSurveyBuildingService = new CNDNELincsSurveyBuildingService($this->permissionService);
                    $validateRuleMessages['rules'] = $CNDNELincsSurveyBuildingService->validateRules($inputs);
                    $validateRuleMessages['messages'] = $CNDNELincsSurveyBuildingService->validateMessages($inputs);
                    break;
                case ReportConstant::SYSTEM_REPORT_ASB_CLI_CB01:
                    $asbestosReportForCBService = new AsbestosReportForCBService($this->permissionService);
                    $validateRuleMessages['rules'] = $asbestosReportForCBService->validateRules($inputs);
                    $validateRuleMessages['messages'] = $asbestosReportForCBService->validateMessages($inputs);
                    break;
            }
        }
        return $validateRuleMessages;
    }

    private function setupAuditFields()
    {
        $systemReportText = '';
        $systemReportId = $this->reportQueue->getOriginal('system_report_id');
        if ($systemReportId !== $this->reportQueue->system_report_id) {
            $systemReport = SystemReport::find($systemReportId);
            if ($systemReport) {
                $systemReportText = Common::concatFields(
                    [
                        $systemReport->system_report_code,
                        $systemReport->system_report_desc
                    ]
                );
            }
        }

        $userReportText = '';
        $userReportId = $this->reportQueue->getOriginal('user_report_id');
        if ($userReportId !== $this->reportQueue->user_report_id) {
            $userReport = UserReport::find($userReportId);
            if ($userReport) {
                $userReportText = Common::concatFields(
                    [
                        $userReport->user_report_code,
                        $userReport->user_report_desc
                    ]
                );
            }
        }

        $reportQueueStatusText = '';
        $reportQueueStatusId = $this->reportQueue->getOriginal('report_queue_status_id');
        if ($reportQueueStatusId !== $this->reportQueue->report_queue_status_id) {
            $reportQueueStatus = ReportQueueStatus::find($reportQueueStatusId);
            if ($reportQueueStatus) {
                $reportQueueStatusText = Common::concatFields([
                        $reportQueueStatus->report_queue_status_code,
                        $reportQueueStatus->report_queue_status_desc
                ]);
            }
        }

        $audiFields = new AuditFields($this->reportQueue);

        return $audiFields
            ->addField('System Report', 'system_report_id', ['originalText' => $systemReportText])
            ->addField('User Report', 'user_report_id', ['originalText' => $userReportText])
            ->addField('Report Queue Status', 'report_queue_status_id', ['originalText' => $reportQueueStatusText])
            ->getFields();
    }
}
