<?php

namespace Tfcloud\Services\Report;

use Tfcloud\Lib\AuditFields;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Exceptions\InvalidRecordException;
use Tfcloud\Lib\Exceptions\PermissionException;
use Tfcloud\Lib\FileUploads;
use Tfcloud\Lib\ServiceSupport;
use Tfcloud\Models\AuditAction;
use Tfcloud\Models\CrudAccessLevel;
use Tfcloud\Models\Module;
use Tfcloud\Models\ReportField;
use Tfcloud\Models\ReportFieldType;
use Tfcloud\Models\UserReportPdfDesign;
use Tfcloud\Models\UserReportType;
use Tfcloud\Services\AuditService;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\PermissionService;
// use Tfcloud\Lib\Reporting\wPDF2;
use Tfcloud\Lib\Reporting\wpdf2\WPDF2;

class UserReportPdfDesignService extends BaseService
{
    /*
     * Tfcloud\Models\UserReportPdfDesign
     */
    protected $userReportPdfDesign;

    /*
     * Tfcloud\Services\Report\UserReportService
     */
    protected $userReportService;

    /*
     * Tfcloud\Models\SystemReport
     */
    protected $systemReport = null;

    public function __construct(PermissionService $permissionService, UserReportService $userReportService)
    {
        $this->permissionService = $permissionService;
        $this->userReportService = $userReportService;
    }

    public function newUserReportPdfDesign()
    {
        $this->userReportPdfDesign = new UserReportPdfDesign();

        return $this->userReportPdfDesign;
    }

    public function getAll($inputs = [])
    {
        if (
            !$this->permissionService->hasModuleAccess(
                Module::MODULE_REPORTS,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
            )
        ) {
            $msg = "Not met minimum readonly access for report module in class" .
                " (" . __CLASS__ . ") method (" . __METHOD__ . "). " .
                " See line (" . __LINE__ . ")";
            throw new PermissionException($msg);
        }

        return $this->filterAll($this->all(), $inputs);
    }

    public function filterAll($query, $inputs)
    {
        if (isset($inputs['user_report_id']) && $userReportId = $inputs['user_report_id']) {
            $query->where('user_report_pdf_design.user_report_id', $userReportId);
        }

        return $query;
    }

    public function get($id, $audit = true)
    {
        if (
            !$this->permissionService->hasModuleAccess(
                Module::MODULE_REPORTS,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
            )
        ) {
            $msg = "Not met minimum readonly access for report module in class" .
                " (" . __CLASS__ . ") method (" . __METHOD__ . "). " .
                " See line (" . __LINE__ . ")";
            throw new PermissionException($msg);
        }

        $this->userReportPdfDesign = UserReportPdfDesign::find($id);

        if (
            is_null($this->userReportPdfDesign)
            || $this->userReportPdfDesign->userReport->site_group_id != \Auth::user()->site_group_id
        ) {
            throw new InvalidRecordException("No User Report Pdf Design found");
        }

        if ($audit) {
            $auditService = new AuditService();
            $auditService->addAuditEntry($this->userReportPdfDesign, AuditAction::ACT_VIEW);
        }

        return $this->userReportPdfDesign;
    }

    public function addUserReportPdfDesign(&$id, $inputs)
    {
        if (
            !$this->permissionService->hasModuleAccess(
                Module::MODULE_REPORTS,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_UPDATE
            )
        ) {
            $msg = "Not met minimum update new access for report module in class" .
                " (" . __CLASS__ . ") method (" . __METHOD__ . "). " .
                " See line (" . __LINE__ . ")";
            throw new PermissionException($msg);
        }

        $userReportId = isset($inputs['wpdfUserReportId']) ? $inputs['wpdfUserReportId'] : null;
        $userReport = $this->userReportService->get($userReportId, false);

        if ($userReport->site_group_id != \Auth::user()->site_group_id) {
            $msg = "Not met site group access for user report module in class" .
                " (" . __CLASS__ . ") method (" . __METHOD__ . "). " .
                " See line (" . __LINE__ . ")";
            throw new PermissionException($msg);
        }

        $this->newUserReportPdfDesign();
        $this->formatInputData($inputs);
        $this->setModelFields($inputs);

        $validator = $this->getValidator();

        if ($validator->passes()) {
            \DB::transaction(function () {
                $this->userReportPdfDesign->save();

                $audit = new AuditService();
                $audit->addAuditEntry($this->userReportPdfDesign, AuditAction::ACT_INSERT);
            });

            $id = $this->userReportPdfDesign->getKey();
            $userReport->wpdf_default_design = $id;
            $userReport->save();

            $validator->success = true;
        }

        return $validator;
    }

    public function updateUserReportPdfDesign($id, $inputs)
    {
        if (
            !$this->permissionService->hasModuleAccess(
                Module::MODULE_REPORTS,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_UPDATE
            )
        ) {
            $msg = "Not met minimum update new access for report module in class" .
                " (" . __CLASS__ . ") method (" . __METHOD__ . "). " .
                " See line (" . __LINE__ . ")";
            throw new PermissionException($msg);
        }

        $this->userReportPdfDesign = $this->get($id, false);
        $this->formatInputData($inputs);
        $this->setModelFields($inputs);

        $validator = $this->getValidator();

        if ($validator->passes()) {
            \DB::transaction(function () {
                if ($this->updateRequired()) {
                    $auditFields = $this->setupAuditFields();

                    $this->userReportPdfDesign->save();

                    $audit = new AuditService();
                    $audit->auditFields($auditFields);
                    $audit->addAuditEntry($this->userReportPdfDesign, AuditAction::ACT_UPDATE);
                }
            });

            $validator->success = true;
        }

        return $validator;
    }

    public function deleteUserReportPdfDesign($id)
    {
        if (
            !$this->permissionService->hasModuleAccess(
                Module::MODULE_REPORTS,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_UPDATE
            )
        ) {
            $msg = "Not met minimum update new access for report module in class" .
                " (" . __CLASS__ . ") method (" . __METHOD__ . "). " .
                " See line (" . __LINE__ . ")";
            throw new PermissionException($msg);
        }

        $this->userReportPdfDesign = $this->get($id, false);

        \DB::transaction(function () {
            $this->userReportPdfDesign->delete();

            $audit = new AuditService();
            $audit->addAuditEntry($this->userReportPdfDesign, AuditAction::ACT_DELETE);
        });
    }

    public function getWpdfTagList($systemReportId, $includeSumTag = false)
    {
        $groupTagList = ReportField::where('system_report_id', $systemReportId)
                ->orderBy('report_field_code', 'ASC')
                ->pluck('report_field_code')->toArray();

        $sumTagList = [];
        if ($includeSumTag) {
            $sumTagList = ReportField::select(\DB::raw("CONCAT('SUM ', report_field_code) AS sum_report_field_code"))
                ->where('system_report_id', $systemReportId)
                ->where('report_field_type_id', ReportFieldType::REP_FIELD_TYPE_NUMERIC)
                ->orderBy('report_field_code', 'ASC')
                ->pluck('sum_report_field_code')->toArray();
        }

        $tagList = implode(",", array_merge_recursive($groupTagList, $sumTagList));

        return $tagList;
    }

    public function getImagesList($userReportId)
    {
        $images = [];

        $fileUpload = new FileUploads();
        $reudOutputImagesFilePath = $fileUpload->getStoragePathway('wpdf', $userReportId);

        if (file_exists($reudOutputImagesFilePath) && $handle = opendir($reudOutputImagesFilePath)) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != "..") {
                    if (
                        (stripos($entry, ".jpg"))
                        || (stripos($entry, ".png"))
                        || (stripos($entry, ".gif"))
                        || (stripos($entry, ".bmp"))
                    ) {
                        $fullFilePath = $reudOutputImagesFilePath . DIRECTORY_SEPARATOR . $entry;

                        // '{ "name": "' . $entry . '", "src": "' . $fullFilePath . '" }';

                        $images[] = [
                            'name' => $entry,
                            'src' => url("api/report/user-reports/$userReportId/design-pdf/photo/$entry"),
                            'filePath' => $fullFilePath
                        ];
                    }
                }
            }

            closedir($handle);
        }

        return $images;
    }

    public function formatPdfDesignList($pdfDesignList)
    {
        $res = [];

        foreach ($pdfDesignList as $pdfDesign) {
            $res[] = [
                'id' => $pdfDesign->pdf_design_id,
                'name' => Common::truncateString($pdfDesign->pdf_design_name, 20)
            ];
        }

        return $res;
    }

    public function getTestDesignRecords($userReportId)
    {
        if (
            !$this->permissionService->hasModuleAccess(
                Module::MODULE_REPORTS,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_UPDATE
            ) || \Auth::user()->wpdf_report_allowed != CommonConstant::DATABASE_VALUE_YES
        ) {
            $msg = "Not met minimum update access for report module in class" .
                " (" . __CLASS__ . ") method (" . __METHOD__ . "). " .
                " See line (" . __LINE__ . ")";
            throw new PermissionException($msg);
        }

        $userReport = $this->userReportService->get($userReportId, false);

        if (
            !$this->permissionService->hasModuleAccess(
                $userReport->systemReport->module_id,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_UPDATE
            ) || $userReport->user_report_type_id != UserReportType::USER_REPORT_TYPE_PDF
        ) {
            $msg = "Not met minimum update access for report module in class" .
                " (" . __CLASS__ . ") method (" . __METHOD__ . "). " .
                " See line (" . __LINE__ . ")";
            throw new PermissionException($msg);
        }

        $viewName = $userReport->systemReport->view_name;

        $rawQuery = "SHOW COLUMNS FROM $viewName"
                . " WHERE `Type` LIKE 'varchar%'"
                . " AND `Null` LIKE 'YES'"
                . " AND `Field` LIKE '%Status%'";

        $results = \DB::select(\DB::raw($rawQuery));

        $groupByColumn = "";
        if ($results && count($results)) {
            $groupByColumn = $results[0]->Field;
        }

        $viewModel = $this->viewFormat($viewName);
        $viewNameModel = "\\Tfcloud\\Models\\Views\\$viewModel";
        $repViewModel = new $viewNameModel();

        $query = $repViewModel::userSiteGroup();
        if ($groupByColumn) {
            $query->orderBy($groupByColumn, 'ASC');
        }

        $records = $query->take(40)->get()->toArray();

        $totals = [];
        if ($groupByColumn) {
            // For the column they have selected to group by, we need to sum up any things that are numbers
            $summedNumericFields = "";
            $sep = ",";
            $numeric = ReportFieldType::REP_FIELD_TYPE_NUMERIC;

            $fields = ReportField::select(['report_field_code'])
                ->where('system_report_id', $userReport->systemReport->system_report_id)
                ->where('report_field_type_id', \DB::raw($numeric))
                ->get()->toArray();

            foreach ($fields as $field) {
                $reportFieldCode = $field['report_field_code'];
                $summedNumericFields .= "$sep SUM(`$reportFieldCode`) AS 'SUM $reportFieldCode'";
            }

            // SUM(`Net Total`) AS 'Net Total' etc
            $rawQuery = "SELECT `$groupByColumn` $summedNumericFields"
                    . " FROM $viewName"
                    . " GROUP BY `$groupByColumn`"
                    . " LIMIT 0, 50";

            $results = \DB::select(\DB::raw($rawQuery));

            foreach ($results as $record) {
                $record = (array) $record;
                $totals[$groupByColumn][$record[$groupByColumn]] = $record;
            }
        }

        return [
            'records' => $records,
            'groupByColumn' => $groupByColumn,
            'totals' => $totals
        ];
    }
    // wpdf v2
    public function previewPdfDesign($inputs)
    {
        $designData = json_decode($inputs['json']);
        $reportId = $inputs['user_report_id'];

        // get sql report
        $userReport = $this->userReportService->get($reportId, false);
        $systemReportCode = $userReport->systemReport ? $userReport->systemReport->system_report_code : "";
        $viewName = $userReport->systemReport->view_name;

        // get query
        $viewModel = $this->viewFormat($viewName);
        $viewNameModel = "\\Tfcloud\\Models\\Views\\$viewModel";
        $repViewModel = new $viewNameModel();

        $query = $repViewModel::userSiteGroup();
        $limit = 500;
        $query->limit($limit);
        $columnList = explode(',', $this->getWpdfTagList($userReport->system_report_id, false));
        $pdf = wPDF2::init($reportId, $systemReportCode, $designData, $query, $columnList);

        $pdf->createReport();
        return $pdf->serveForBrowser();
    }

    public function getJsonData($reportId)
    {
        $userReportPdfDesign = UserReportPdfDesign::where('user_report_id', $reportId)
                ->first();
        return $userReportPdfDesign ? $userReportPdfDesign->pdf_design_content : '';
    }
    public function savePdfDesignV2($inputs)
    {
        if (
            !$this->permissionService->hasModuleAccess(
                Module::MODULE_REPORTS,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_UPDATE
            )
        ) {
            throw new PermissionException("Not met minimum update new access for report module");
        }
        $success = false;
        \DB::transaction(function () use ($inputs, &$success) {
            $this->userReportPdfDesign = UserReportPdfDesign::where('user_report_id', $inputs['user_report_id'])
                ->first();
            if (!$this->userReportPdfDesign) {
                $this->userReportPdfDesign = new UserReportPdfDesign();
                $this->userReportPdfDesign->user_report_id = $inputs['user_report_id'];
            }
            $this->userReportPdfDesign->pdf_design_content = $inputs['json'];
            $this->userReportPdfDesign->pdf_design_name = $inputs['name'];

            $this->userReportPdfDesign->save();
            $success = true;
        });

        return $success;
    }
    // end wpdf v2
    private function all()
    {
        $query = UserReportPdfDesign
            ::join('user_report', 'user_report.user_report_id', '=', 'user_report_pdf_design.user_report_id')
            ->where('user_report.site_group_id', \Auth::user()->site_group_id)
            ->select(['pdf_design_id', 'pdf_design_name']);

        return $query;
    }

    private function formatInputData(&$inputs)
    {
        $inputs['user_report_id'] = array_get($inputs, 'wpdfUserReportId');
        $inputs['pdf_design_name'] = array_get($inputs, 'wpdfSaveName');
        $inputs['pdf_design_content'] = utf8_decode(array_get($inputs, 'wpdfJSONData'));
    }

    private function setModelFields($inputs)
    {
        Common::assignField($this->userReportPdfDesign, 'user_report_id', $inputs);
        Common::assignField($this->userReportPdfDesign, 'pdf_design_name', $inputs);
        Common::assignField($this->userReportPdfDesign, 'pdf_design_content', $inputs);
    }

    private function getValidator()
    {
        $attributes = $this->userReportPdfDesign->getAttributes();

        $rules = [
            'user_report_id' => ['required'],
            'pdf_design_name' => ['required', 'max:256', 'unique:user_report_pdf_design,pdf_design_name,'
                . $this->userReportPdfDesign->pdf_design_id .
                ',pdf_design_id,user_report_id,' . $this->userReportPdfDesign->user_report_id
            ],
        ];

        $messages = [];

        $validator = \Validator::make($attributes, $rules, $messages);
        $validator->success = false;

        return $validator;
    }

    private function setupAuditFields()
    {
        $auditFields = new AuditFields($this->userReportPdfDesign);

        $fields = $auditFields
            ->addField('Design Name', 'pdf_design_name')
            //->addField('Design Content', 'pdf_design_content') // to much value
            ->getFields();

        return $fields;
    }

    private function updateRequired()
    {
        return ServiceSupport::updateRequired([$this->userReportPdfDesign]);
    }

    private function viewFormat($viewName)
    {
        $viewNames = explode('_', $viewName);
        $viewNameModel = '';
        if (count($viewNames) > 0) {
            foreach ($viewNames as $value) {
                $viewNameModel .= ucfirst(strtolower($value));
            }
        }

        return $viewNameModel;
    }
}
