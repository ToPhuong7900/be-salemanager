<?php

namespace App\Services\Report;

use App\Services\BaseService;

class CsvExcelReportService extends BaseService
{
    private $permissionService;
    private $reportService;

    public function __construct(PermissionService $permissionService, ReportService $reportService)
    {
        $this->permissionService = $permissionService;
        $this->reportService = $reportService;
    }

    public function reOutputCsvData($inputs)
    {
        // Default values
        $iEndRow = 0;
        $iTotal = 0;
        $iRetRows = 0;

        $iStartRow = isset($inputs['startRow']) ? (int) $inputs['startRow'] : 1;

        if ($iStartRow < 1) {
            $iStartRow = 1;
        }

        $repId = array_get($inputs, 'repId', null);
        $repType = array_get($inputs, 'repType', '');

        $report = $this->reportService->get($repType, $repId);
        if (!$report instanceof Report) {
            return false;
        }

        $filterData = $inputs;
        foreach ($filterData as $key => $value) {
            if ((empty($value) && !is_numeric($value)) || $value == 'null') {
                unset($filterData[$key]);
            }
        }

        $repTitle = $report->getRepTitle();
        $repCode = $report->getRepCode();

        // Create a reports directory for the site group
        if ($repDownloadPath = array_get($inputs, 'repDownloadPath')) {
            $reportDir = $repDownloadPath;
            if (!is_dir($reportDir)) {
                return [];
            }
        } else {
            // Create a reports directory for the site group
            $reportDir = storage_path()
                . DIRECTORY_SEPARATOR . 'reports'
                . DIRECTORY_SEPARATOR . \Auth::User()->site_group_id;
            if (!is_dir($reportDir)) {
                mkdir($reportDir, 0755);
            }
            // Create a reports directory for the userid
            $reportDir .= DIRECTORY_SEPARATOR . \Auth::User()->id;
            if (!is_dir($reportDir)) {
                mkdir($reportDir, 0755);
            }
        }

        $repCsvFile = $reportDir . DIRECTORY_SEPARATOR . $repCode . '.csv';
        $repXlsFile = $reportDir . DIRECTORY_SEPARATOR . $repCode . '.xlsx';
        $repFormat = ReportConstant::REPORT_OUTPUT_CSV_FORMAT;
        $repFileName = $repCode . '.csv';
        $repFilePath = $repCsvFile;

        $fileExtension = array_get($inputs, 'fileExtension');
        if (
            $fileExtension == ReportConstant::REPORT_OUTPUT_XLSX_FORMAT ||
            (empty($fileExtension) && array_get($inputs, 'outputOption') == 2)
        ) {
            /**
             * There are 2 cases that $fileExtension is null:
             * 1: export report from a list (not a new filter list) => expect CSV file
             * 2: export report from running report page and select XLSX format => expect XLSX file
             * Thus, include $inputs['outputOption'] == 2 to make sure it comes from running report page
             */
            $repFormat = ReportConstant::REPORT_OUTPUT_XLSX_FORMAT;
            $repFileName = $repCode . '.xlsx';
            $repFilePath = $repXlsFile;
        }

        $filterText = "";
        $sOrderText = "";
        $bPrintHeader = true;   //Set to false to suppress the defailt header in preference of a custom header.

        // Generate report
        $fileWriter = (new SpoutFactory($repFilePath))->getSpoutWriter($report)->openFile();
        // Now prepare to output the data
        switch ($repCode) {
            // Suit Core Facts
            case ReportConstant::SYSTEM_REPORT_INS06:
            case ReportConstant::SYSTEM_REPORT_PL05:
                $inspectionCalendarService = new INS06InspectionCalendarService($this->permissionService, $report);
                $results = $inspectionCalendarService->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $numberColumns = [];
                    /**
                     * Get column key of number cols
                     * Except cols: Inspection type, frequency
                     */
                    if (!empty($results[0])) {
                        $numberColumns = range(2, count($results[0]) - 1);
                    }
                    $fileWriter->addNumberColKeys($numberColumns);
                }
                break;
            case ReportConstant::SYSTEM_REPORT_INS08:
                $inspectionForecastService = new INS08InspectionForecastService($this->permissionService, $report);
                $results = $inspectionForecastService->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]);
                }
                break;
            case ReportConstant::SYSTEM_REPORT_INS11:
                $inspectionGapService = new INS11InspectionGapService($this->permissionService, $report);
                $results = $inspectionGapService->getReportData($filterData, $filterText, $sOrderText);
                break;

            case ReportConstant::SYSTEM_REPORT_INS_CLI_RBWM01:
                $plantinspectionComplicanceServ =
                    new INSCLIRBWM01PlantInspectionComplianceService($this->permissionService, $report);
                $results = $plantinspectionComplicanceServ->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addDateColKeys([9]);
                }
                break;

            case ReportConstant::SYSTEM_REPORT_HLP_CLI_RCBC01:
                $rcbc01RedcarHelpcallReport =
                    new HLPRCBC01HelpcallMiniProjectService($this->permissionService, $report);
                $results = $rcbc01RedcarHelpcallReport->getReportData($filterData, $filterText, $sOrderText);
                break;

            case ReportConstant::SYSTEM_REPORT_INT_CLI_CEC01:
                $cec01StatusChangeHistoryInstructionReport =
                    new INTCEC01StatusChangeHistoryService($this->permissionService);
                $results = $cec01StatusChangeHistoryInstructionReport
                        ->getReportData($filterData, $filterText, $sOrderText);
                break;


            case ReportConstant::SYSTEM_REPORT_FAR23:
                $far23 = new CsvExcel\FixedAsset\Far23ReportService($this->permissionService, $filterData, $report);
                $results = $far23->getReportData($filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([5, 6]);
                }
                break;

            case ReportConstant::SYSTEM_REPORT_FAR24:
                $far24 = new CsvExcel\FixedAsset\Far24ReportService($this->permissionService, $filterData, $report);
                $results = $far24->getReportData($filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([5, 6]);
                }
                break;

            case ReportConstant::SYSTEM_REPORT_FAR25:
                $far25 = new CsvExcel\FixedAsset\Far25ReportService($this->permissionService, $filterData, $report);
                $results = $far25->getReportData($filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([5, 6]);
                }
                break;

            case ReportConstant::SYSTEM_REPORT_FAR26:
                $far26 = new CsvExcel\FixedAsset\Far26ReportService($this->permissionService, $filterData, $report);
                $results = $far26->getReportData($filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([5, 6]);
                }
                break;

            case ReportConstant::SYSTEM_REPORT_FAR27:
                $far27 = new CsvExcel\FixedAsset\Far27ReportService($this->permissionService, $filterData, $report);
                $results = $far27->getReportData($filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([5, 6]);
                }
                break;

            case ReportConstant::SYSTEM_REPORT_FAR28:
                $far28 = new CsvExcel\FixedAsset\Far28ReportService($this->permissionService, $filterData, $report);
                $results = $far28->getReportData($filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([5, 6]);
                }
                break;

            case ReportConstant::SYSTEM_REPORT_FAR29:
                $far29 = new CsvExcel\FixedAsset\Far29ReportService($this->permissionService, $filterData, $report);
                $results = $far29->getReportData($filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([5, 6]);
                }
                break;

            case ReportConstant::SYSTEM_REPORT_FAR30:
                $far30 = new CsvExcel\FixedAsset\Far30ReportService($this->permissionService, $filterData, $report);
                $results = $far30->getReportData($filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([5, 6]);
                }
                break;

            case ReportConstant::SYSTEM_REPORT_FAR33:
                $far33 = new CsvExcel\FixedAsset\Far33ReportService($this->permissionService, $filterData, $report);
                $results = $far33->getReportData($filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([5, 6]);
                }
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_RBY01:
                $farCliRby01 = new FarCliRBY01ReportService($this->permissionService);
                $results = $farCliRby01->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([18, 19]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_RBY02:
                $farCliRby02 = new FarCliRBY02ReportService($this->permissionService);
                $results = $farCliRby02->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([18, 19]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_RBY03:
                $farCliRby03 = new FarCliRBY03ReportService($this->permissionService);
                $results = $farCliRby03->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([18, 19]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_RBY04:
                $farCliRby04 = new FarCliRBY04ReportService($this->permissionService);
                $results = $farCliRby04->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([18, 19]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_RBY05:
                $farCliRby05 = new FarCliRBY05ReportService($this->permissionService);
                $results = $farCliRby05->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([18, 19]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_RBY06:
                $farCliRby06 = new FarCliRBY06ReportService($this->permissionService);
                $results = $farCliRby06->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([18, 19]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_RBY07:
                $farCliRby07 = new FarCliRBY07ReportService($this->permissionService);
                $results = $farCliRby07->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([18, 19]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_RBY08:
                $farCliRby08 = new FarCliRBY08ReportService($this->permissionService);
                $results = $farCliRby08->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([18, 19]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_RBY09:
                $farCliRby09 = new FarCliRBY09ReportService($this->permissionService);
                $results = $farCliRby09->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([18, 19]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_RBY10:
                $farCliRby10 = new FarCliRBY10ReportService($this->permissionService);
                $results = $farCliRby10->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([18, 19]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_MK01:
                $farCliMk01 = new FarCliMK01ReportService($this->permissionService);
                $results = $farCliMk01->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([5]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_MK02:
                $farCliMk02 = new FarCliMK02ReportService($this->permissionService);
                $results = $farCliMk02->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([5]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_MK03:
                $farCliMk03 = new FarCliMK03ReportService($this->permissionService);
                $results = $farCliMk03->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([5]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_MK04:
                $farCliMk04 = new FarCliMK04ReportService($this->permissionService);
                $results = $farCliMk04->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([5]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_MK05:
                $farCliMk05 = new FarCliMK05ReportService($this->permissionService);
                $results = $farCliMk05->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([5]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_MK06:
                $farCliMk06 = new FarCliMK06ReportService($this->permissionService);
                $results = $farCliMk06->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([5]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_MK07:
                $farCliMk07 = new FarCliMK07ReportService($this->permissionService);
                $results = $farCliMk07->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([5]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_MK08:
                $farCliMk08 = new FarCliMK08ReportService($this->permissionService);
                $results = $farCliMk08->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([5]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_MK09:
                $farCliMk09 = new FarCliMK09ReportService($this->permissionService);
                $results = $farCliMk09->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([5]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_MK10:
                $farCliMk10 = new FarCliMK10ReportService($this->permissionService);
                $results = $farCliMk10->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([5]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_CFF02:
                $farCliCff02 = new FarCliCFF02ReportService($this->permissionService);
                $results = $farCliCff02->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([9]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_CFF03:
                $farCliCff03 = new FarCliCFF03ReportService($this->permissionService);
                $results = $farCliCff03->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([9]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_CFF04:
                $farCliCff04 = new FarCliCFF04ReportService($this->permissionService);
                $results = $farCliCff04->getReportData($filterData, $filterText, $sOrderText);
                $bPrintHeader = false;
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([9]);
                }
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_CFF05:
                $farCliCff05 = new FarCliCFF05ReportService($this->permissionService);
                $results = $farCliCff05->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([9]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_CFF06:
                $farCliCff06 = new FarCliCFF06ReportService($this->permissionService);
                $results = $farCliCff06->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([9]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_CFF07:
                $farCliCff07 = new FarCliCFF07ReportService($this->permissionService);
                $results = $farCliCff07->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([9]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_CFF08:
                $farCliCff07 = new FarCliCFF08ReportService($this->permissionService);
                $results = $farCliCff07->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([9]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_CFF09:
                $farCliCff07 = new FarCliCFF09ReportService($this->permissionService);
                $results = $farCliCff07->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([9]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_SAND01:
                $farCliSand01 = new FarCliSand01ReportService($this->permissionService);
                $results = $farCliSand01->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([8, 9]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_GCC01:
                $farCliGcc01 = new FarCliGCC01ReportService($this->permissionService);
                $results = $farCliGcc01->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([7]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_GCC02:
                $farCliGcc02 = new FarCliGCC02ReportService($this->permissionService);
                $results = $farCliGcc02->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([7]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_GCC03:
                $farCliGcc03 = new FarCliGCC03ReportService($this->permissionService);
                $results = $farCliGcc03->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([7]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_GCC04:
                $farCliGcc04 = new FarCliGCC04ReportService($this->permissionService);
                $results = $farCliGcc04->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([7]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_GCC05:
                $farCliGcc05 = new FarCliGCC05ReportService($this->permissionService);
                $results = $farCliGcc05->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([7]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_GCC06:
                $farCliGcc06 = new FarCliGCC06ReportService($this->permissionService);
                $results = $farCliGcc06->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([7]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_NEWC01:
                $farCliNewc01 = new FarCliNEWC01ReportService($this->permissionService);
                $results = $farCliNewc01->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([3]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_NEWC03:
                $farCliNewc03 = new FarCliNEWC03ReportService($this->permissionService);
                $results = $farCliNewc03->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([3]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_NEWC04:
                $farCliNewc04 = new FarCliNEWC04ReportService($this->permissionService);
                $results = $farCliNewc04->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([3]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_NEWC05:
                $farCliNewc05 = new FarCliNEWC05ReportService($this->permissionService);
                $results = $farCliNewc05->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([3]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_NEWC06:
                $farCliNewc06 = new FarCliNEWC06ReportService($this->permissionService);
                $results = $farCliNewc06->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([3]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_NEWC07:
                $farCliNewc07 = new FarCliNEWC07ReportService($this->permissionService);
                $results = $farCliNewc07->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([3]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_NEWC08:
                $farCliNewc08 = new FarCliNEWC08ReportService($this->permissionService);
                $results = $farCliNewc08->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([3]);
                }
                $bPrintHeader = false;
                break;

            case ReportConstant::SYSTEM_REPORT_FAR_CLI_NMNI02:
                $farCliNMNI02 = new FarCliNMNI02ReportService($this->permissionService);
                $results = $farCliNMNI02->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([2, 3]);
                }
                $bPrintHeader = false;
                break;
            case ReportConstant::SYSTEM_REPORT_FAR_CLI_NMNI03:
                $farCliNMNI03 = new FarCliNMNI03ReportService($this->permissionService);
                $results = $farCliNMNI03->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([2, 3]);
                }
                $bPrintHeader = false;
                break;
            case ReportConstant::SYSTEM_REPORT_FAR_CLI_NMNI04:
                $farCliNMNI04 = new FarCliNMNI04ReportService($this->permissionService);
                $results = $farCliNMNI04->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([2, 3]);
                }
                $bPrintHeader = false;
                break;
            case ReportConstant::SYSTEM_REPORT_FAR_CLI_NMNI05:
                $farCliNMNI05 = new FarCliNMNI05ReportService($this->permissionService);
                $results = $farCliNMNI05->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([2, 3]);
                }
                $bPrintHeader = false;
                break;


            case ReportConstant::SYSTEM_REPORT_PR_CLI_STFC01:
                $prCliSTFC01 = new PrCliSTFC01ReportService($this->permissionService, $report);
                $results = $prCliSTFC01->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([6, 7, 8, 9]);
                }
                break;

            case ReportConstant::SYSTEM_REPORT_PR_CLI_STFC02:
                $prCliSTFC02 = new PrCliSTFC02ReportService($this->permissionService, $report);
                $results = $prCliSTFC02->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([4, 5, 6, 7]);
                }
                break;

            case ReportConstant::SYSTEM_REPORT_CND24:
                $cnd24Service = new CND24ReportService($this->permissionService, $report);
                $results = $cnd24Service->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $numberColumns = (int)(count($results[0]) - 1);
                    $fileWriter->addNumberColKeys([23, $numberColumns]);
                }
                break;

            case ReportConstant::SYSTEM_REPORT_CND25:
                $cnd25Service = new CND25ReportService($this->permissionService, $report);
                $results = $cnd25Service->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([25, 26, 27]);
                }
                break;
            case ReportConstant::SYSTEM_REPORT_DOC01:
                $doc01WithUserDefinedService = new DOC01WithUserDefinedService($this->permissionService, $report);
                $results = $doc01WithUserDefinedService->getReportData($filterData, $filterText, $sOrderText);
                break;

            case ReportConstant::SYSTEM_REPORT_DLO16:
                $dlo16Service = new DLO16ReportService($this->permissionService, $report);
                $results = $dlo16Service->getReportData($filterData, $filterText, $sOrderText);
                if ($fileWriter instanceof IXlsxSpoutWriter) {
                    $fileWriter->addNumberColKeys([2,3,4,5,6,7,8,9,10,11]);
                }
                break;

            case ReportConstant::SYSTEM_REPORT_ES48:
                $es48Service = new ES48ReportService($this->permissionService, $report);
                $results = $es48Service->getReportData($filterData, $filterText, $sOrderText);
                break;

            case ReportConstant::SYSTEM_REPORT_UT10:
                $ut10 = new UT10UtilityTotalUsageReportService($this->permissionService, $report);
                $results = $ut10->getReportData($filterData, $filterText, $sOrderText);
                break;
            default:
                \Log::error("Unknown Report");
                break;
        }

        $bRowFound = false;
        $bOutputFields = true;
        $aOutputFields = [];

        foreach ($results as $row) {
            $bSkip = false;
            if ($bSkip) {
                continue;
            }

            if (!$bRowFound) {
                // Output the headers by using the keys of this first row of data returned.
                $bFirstCol = true;
                $data = null;
                foreach ($row as $key => $value) {
                    $this->reGetHeader($key, $bFirstCol, $data);
                }

                if (empty($data)) {
                    $header = 'None Output fields';
                    $fileWriter->addRowFromString($header);
                    $bOutputFields = false;
                } else {
                    if ($bPrintHeader) {
                        $fileWriter->addHeaderRow($data);
                    }
                    $aOutputFields = array_keys($data);
                }

                $bRowFound = true;                  // No need to do the headers again
            }

            // Go through each key/value pair in the array
            $bFirstCol = true;
            $data = "";
            $rowArr = [];
            foreach ($row as $key => $value) {
                if ($bOutputFields && !in_array($key, $aOutputFields)) {
                    continue;
                }

                // Put values in double quotes.
                $escapeValue = str_replace('"', '""', $value);
                $avoidExcelExpression = "";
                if ($escapeValue) {
                    $firstChar = $escapeValue[0];
                        // RB 26/11/2019 Adding a space before the hyphen causes Excel to treat a negative number
                        // as a string. Appears to be ok to let it the value through when it is a number.
                    if (($firstChar == '-' && !is_numeric($value)) || ($firstChar == '=')) {
                        $avoidExcelExpression = ' ';
                    }
                }
                $rowArr[$key] = $avoidExcelExpression . $escapeValue;
            }

            $fileWriter->addRowFromArray($rowArr);

            ++$iRetRows;
        }



        // Now write out the title of the report and the details of the filtering applied.
        $fileWriter->addRowFromString(null);
        $fileWriter->addRowFromString("Report: $repTitle");

        if ($filterText) {
            $fileWriter->addRowFromString("Filtering details:");
            $fileWriter->addRowFromString($filterText);
        }

        if ($sOrderText != "") {
            $fileWriter->addRowFromString("Ordering details:");
            $fileWriter->addRowFromString($sOrderText);
        }

        if (!$bRowFound) {
            $fileWriter->addRowFromString(null);
            $fileWriter->addRowFromString("No data found.");
        } else {
            $fileWriter->addRowFromString("Rows returned:");
            $iEndRow = $iStartRow + $iRetRows - 1;
            $returnInformation = "Returned $iRetRows rows (from $iStartRow to $iEndRow)";

            if ($report->getRepUserReportUseQuery() == CommonConstant::DATABASE_VALUE_NO) {
                $returnInformation .= " of $iEndRow rows.";
            }
            $fileWriter->addRowFromString($returnInformation);
        }
        $fileWriter->closeFile();

        $iTotal = $iEndRow;

        // Add to Recent Items
        ReportGenerateService::setRecentReport($repType, $repId);

        $returnData = [
            'returnedRows'  => $iRetRows,
            'rowNumbers'    => $iEndRow > 0 ? "{$iStartRow} to {$iEndRow}" : 0,
            'totalRows'     => $iTotal,
            'repFormat'     => $repFormat,
            'repCode'       => $repCode,
            'repTitle'      => $repTitle,
            'repFile'       => url("report/reports/{$repCode}/download"),
            'repFileName'   => $repFileName,
            'repFilePath'   => $repFilePath,
            'result'        => true
        ];

        // add entry to audit log
        $auditService = new AuditService();
        if ($filterText) {
            $auditService->auditText = "Filtering details: {$filterText}";
        }
        $auditService->addAuditEntry($report, AuditAction::ACT_REPORT);

        return $returnData;
    }

    private function reGetHeader($key, &$bFirstCol, &$data)
    {
        $headerVal = $key;

        if ($headerVal != "") {
            // As long as the header column hasn't been altered to be blank - a
            // user defined field not in use, output it.
            if ($bFirstCol) {
                $bFirstCol = false;
            }

            $data[$key] = $headerVal;
        }
    }
}
