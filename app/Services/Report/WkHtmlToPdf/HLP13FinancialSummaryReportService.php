<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use Carbon\Carbon;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Common;
use Tfcloud\Models\DloJobType;
use Tfcloud\Models\InstructionParentType;
use Tfcloud\Models\Report;
use Tfcloud\Services\Dlo\DloJobFinanceSummaryService;
use Tfcloud\Services\Dlo\JobService;
use Tfcloud\Services\Instruction\InstructionListService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\UserDefinedService;

class HLP13FinancialSummaryReportService extends WkHtmlToPdfReportBaseService
{
    public $filterText = '';
    private $filterQuery = null;

    private $userDefinedService = null;
    private $dloJobFinanceSummaryService = null;
    private $instructionListService = null;
    private $dloJobService = null;

    public function __construct(PermissionService $permissionService, Report $report = null, $inputs = [])
    {
        parent::__construct($permissionService);
        $this->filterQuery = (new ClassMapReportLoader($report))->getFilterQuery($inputs);

        $this->userDefinedService = new UserDefinedService();
        $this->dloJobFinanceSummaryService = new DloJobFinanceSummaryService($this->permissionService);
        $this->instructionListService = new InstructionListService($this->permissionService);
        $this->dloJobService  = new JobService($this->permissionService, $this->userDefinedService);
    }

    public function printReport($repId, $options = [])
    {
        $repOutPutPdfFile = $this->generateReportOutPutPath(array_get($options, 'helpcall_code'));
        return $this->runReport($repOutPutPdfFile, $repId, $options);
    }

    public function runReport($repOutPutPdfFile, $repId, $options = [])
    {
        $helpcalls = $this->filterQuery->getAll();
        $this->filterText = $this->filterQuery->getFilterDetailText();

        $arrPdf = [];
        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);

        $headerLogo = $this->getHeaderLogo();
        $showSalesInvoice = Common::valueYorNtoBoolean(\Auth::user()->siteGroup->dlo_sales_invoicing);

        if ($helpcalls->count()) {
            foreach ($helpcalls as $helpcall) {
                $reportPath = $generatedPath . '/' . $helpcall->helpcall_id . '.pdf';

                $instructions
                    = $this->instructionListService->getInstructionsByParentType(
                        InstructionParentType::HELPCALL,
                        $helpcall->helpcall_id,
                        \Input::all(),
                        true,
                        ['check-permission' => false]
                    )
                    ->get();

                $dloJobs = $this->dloJobService->getDloJobsByParentType(
                    DloJobType::REACTIVE,
                    $helpcall->helpcall_id,
                    [],
                    ['check-permission' => false]
                )->get();

                if (!count($instructions) && !count($dloJobs)) {
                    continue;
                }

                $dloJobFinanceSummary = $this->dloJobFinanceSummaryService->getTotalJobsFinancialSummary($dloJobs);

                $jobsTotal = $dloJobFinanceSummary['totals']['cost_to_date'];
                $instructionsTotal = 0;
                foreach ($instructions as $instruction) {
                    $instructionsTotal += $instruction->actual;
                }
                $total = $jobsTotal + $instructionsTotal;

                $header = \View::make(
                    'reports.wkhtmltopdf.helpcall.hlp13.header',
                    [
                        'helpcall'  => $helpcall,
                        'logo' => $headerLogo
                    ]
                )->render();

                $headerPath = $generatedPath . "/header_{$helpcall->helpcall_id}.html";
                file_put_contents($headerPath, $header, LOCK_EX);


                $content = \View::make(
                    'reports.wkhtmltopdf.helpcall.hlp13.content',
                    [
                        'helpcall'             => $helpcall,
                        'instructions'         => $instructions,
                        'total'                => $total,
                        'jobsTotal'            => $jobsTotal,
                        'instructionsTotal'    => $instructionsTotal,
                        'dloJobFinanceSummary' => $dloJobFinanceSummary,
                        'showSalesInvoice'     => $showSalesInvoice,
                    ]
                )->render();
                $contentPath = $generatedPath . "/content_{$helpcall->helpcall_id}.html";
                file_put_contents($contentPath, $content, LOCK_EX);

                $outputOptions = [
                    'header-html' => $headerPath,
                    'orientation' => 'portrait',
                    'header-spacing' => 4,
                    'footer-spacing' => 4,
                ];

                $this->generateSingleHtmlToPdf($contentPath, $reportPath, $outputOptions);

                array_push($arrPdf, $reportPath);
            }
        }

        $cssPaging = [
            'fontSize' => 8,
            'x' => 400,
            'y' => 8
        ];
        $this->mergerPdf($arrPdf, $repOutPutPdfFile, ['cssPaging' => $cssPaging]);

        if (array_get($options, 'print-report')) {
            return $repOutPutPdfFile;
        }
        return true;
    }

    protected function setPagingCustom(&$page, $key, $totalPage, $x, $y, $customY2 = null)
    {
        $user = \Auth::user()->display_name;
        $now = Carbon::now()->format('d/m/Y H:i:s');
        $left = 10;
        $center = 260;
        $page->drawText("HLP13: Financial Summary", $left, $y, 'UTF-8');
        $page->drawText("Page " . ($key + 1) . " of $totalPage", $center, $y, 'UTF-8');
        $page->drawText("Created by $user at $now", $x, $y, 'UTF-8');
    }
}
