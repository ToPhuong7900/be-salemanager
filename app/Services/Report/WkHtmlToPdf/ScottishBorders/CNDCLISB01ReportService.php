<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf\ScottishBorders;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Tfcloud\Controllers\Admin\General\SiteGroupsController;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\BoxFilterQuery\IReportFilterQuery;
use Tfcloud\Lib\BoxFilter\Constants\OperatorTypeConstant;
use Tfcloud\Lib\Common;
use Tfcloud\Models\Condsurvey;
use Tfcloud\Models\CondLocationScore;
use Tfcloud\Models\CondLocationElementScore;
use Tfcloud\Models\CondsurveyStatus;
use Tfcloud\Models\Idwork;
use Tfcloud\Models\Report;
use Tfcloud\Models\SiteGroup;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Admin\General\SiteGroupsService;
use Tfcloud\Services\Report\WkHtmlToPdf\WkHtmlToPdfReportBaseService;

class CNDCLISB01ReportService extends WkHtmlToPdfReportBaseService
{
    private $sgController;
    private $sgService;
    public $filterText = '';
    private $reportBoxFilterLoader = null;

    public function __construct(PermissionService $permissionService, Report $report = null)
    {
        parent::__construct($permissionService);
        $this->sgService = new SiteGroupsService($permissionService);
        $this->sgController = new SiteGroupsController($permissionService, $this->sgService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function runReport($repOutPutPdfFile, $inputs, $repId)
    {
        $siteGroupId = Auth::User()->site_group_id;
        $photoPath = $this->sgController->getPhotoPath($siteGroupId); //get logo for outputting at top of report
        $singleCondSurvey = "";

        //get the site group name and desc. for the report header
        $siteGroup = SiteGroup::select()->where('site_group_id', '=', $siteGroupId)->first();
        $siteGroupHeader = $siteGroup->code . " - " . $siteGroup->description;

        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $conditionSurveys = $reportFilterQuery->filterAll($this->baseQuery())->get();
        } else {
            $condQuery = $this->getCondSurveyData($inputs);
            $conditionSurveys = [];
            $conditionSurveys = $condQuery->get();
        }

        $foundCondData = "";

        if (array_key_exists('singleOnly', $inputs)) {
            $singleCondSurvey = 'true';
        } else {
            $singleCondSurvey = 'false';
        }

        $condLocationScores = [];
        $conLocationElementScores = [];
        $idWorks = [];

        if (count($conditionSurveys) > 0) {
            $foundCondData = 'true';

            $condLocationScores = $this->getAllCondLocationScores($conditionSurveys);
            $conLocationElementScores = $this->getAllCondLocationElementScores($condLocationScores);
            $idWorks = $this->getAllIdWork($conditionSurveys);
        } else {
            //filter provided may be wrong; prevent service hanging by forcing error to occur
            $foundCondData = 'false';
        }
        // Create wkhtmltopdf temp folder to store generated files
        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);

        $header = View::make(
            'reports.wkhtmltopdf.condition.cndCliSB01.header',
            [
                'photoPath'        => $photoPath,
                'singleCondSurvey' => $singleCondSurvey,
                'siteGroupHeader'  => $siteGroupHeader
            ]
        )->render();

        $headerPath = $generatedPath . "/header.html";
        file_put_contents($headerPath, $header, LOCK_EX);

        $footer = View::make('reports.wkhtmltopdf.condition.cndCliSB01.footer')->render();
        $footerPath = $generatedPath . "/footer.html";
        file_put_contents($footerPath, $footer, LOCK_EX);

        $content = View::make(
            'reports.wkhtmltopdf.condition.cndCliSB01.content',
            [
                'foundCondData'        => $foundCondData,
                'conditionSurveys'     => $conditionSurveys,
                'photoPath'            => $photoPath,
                'condLocationScores'   => $condLocationScores,
                'condLocElementScores' => $conLocationElementScores,
                'idWorks'              => $idWorks,
            ]
        )->render();

        $contentPath = $generatedPath . "/content.html";

        file_put_contents($contentPath, $content, LOCK_EX);

        $whereCodes = [];               // The codes which have been used in the filter
        $whereTexts = [];               // Plain text about filtering applied
        $orCodes = [];                  // csv string of OR codes
        $bFilterDetail = false;

        if ($reportFilterQuery instanceof IReportFilterQuery) {
            $this->filterText = $reportFilterQuery->getFilterDetailText();
        } else {
            $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
            $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $this->filterText);
        }

        $options = [
            'header-html'    => $headerPath,
            'footer-html'    => $footerPath,
            'header-spacing' => 4,
            'footer-spacing' => 4,
            'orientation'    => 'landscape',
        ];

        if (array_key_exists('singleOnly', $inputs)) {
            $repOutPutPdfFile .= '.pdf';
            $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $options);
            return $repOutPutPdfFile;
        } else {
            $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $options);
            return true;
        }
    }

    public function validateFilter($inputs)
    {
        $this->formatInputData($inputs);
        Validator::extend('required_if_without', function () use ($inputs) {
            if (
                empty($inputs['surveyor_id']) &&
                empty($inputs['site_id']) &&
                empty($inputs['building_id']) &&
                empty($inputs['condsurvey_status_id']) &&
                empty($inputs['condsurvey_code']) &&
                empty($inputs['condsurvey_desc'])
            ) {
                return false;
            }
            return true;
        });

        return Validator::make($inputs, $this->validateRules(), $this->validateMessages());
    }

    private function validateRules()
    {
        return [
            'site_id' => ['required_if_without', 'required'],
        ];
    }

    private function validateMessages()
    {
        return [
            'site_id.required_if_without' => 'At least one filter is required to run this report.',
            'site_id.required'            => 'A location filter must be supplied.'
        ];
    }

    private function formatInputData(&$inputs)
    {
        foreach ($inputs as $key => $value) {
            if ($value === "") {
                unset($inputs[$key]);
            }
        }
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);

            $siteValue = $reportFilterQuery->getValueFilterField('site_id');
            $buildingValue = $reportFilterQuery->getValueFilterField('building_id');
            $surveyorValue = $reportFilterQuery->getValueFilterField('surveyor');
            $codeValue = $reportFilterQuery->getValueFilterField('condsurvey_code');
            $descValue = $reportFilterQuery->getValueFilterField('condsurvey_desc');

            $statusFilterField = $reportFilterQuery->getFilterField('condsurvey_status_id');

            $inputs['site_id'] = empty($siteValue) ? request('site_id') : $siteValue[0];
            $inputs['building_id'] = empty($buildingValue) ? request('building_id') : $buildingValue[0];
            $inputs['surveyor_id'] = empty($surveyorValue) ? request('surveyor_id') : $surveyorValue[0];
            $inputs['condsurvey_code'] = empty($codeValue) ? request('condsurvey_code') : $codeValue[0];
            $inputs['condsurvey_desc'] = empty($descValue) ? request('condsurvey_desc') : $descValue[0];

            $inputs['condsurvey_status_id'] = $this->getValueRequired($statusFilterField);
        }
        return $inputs;
    }

    private function getValueRequired($filterField)
    {
        if (!empty($filterField)) {
            foreach ($filterField->getFilterOperators() as $filterOperator) {
                $values = $filterOperator->getValues();
                switch ($filterOperator->getOperator()) {
                    case OperatorTypeConstant::NOT_BLANK:
                    case OperatorTypeConstant::BLANK:
                        return 1;
                        break;
                    default:
                        return empty($values) ? null : $values[0];
                }
            }
        }
        return null;
    }


    private function getCondSurveyData($inputs)
    {
        $query = $this->baseQuery();

        //apply filters
        $code = array_get($inputs, 'code', null);
        if ($code) {
            $query->where('condsurvey.condsurvey_code', '=', $inputs['code']);
        }

        if (array_key_exists('singleOnly', $inputs)) {
            $query->where('condsurvey.condsurvey_id', '=', $inputs['id']);
        } else {
            //check other inputs passed in by filter
            if ((isset($inputs['description'])) && ($inputs['description'] != "") && ($inputs['description'] != " ")) {
                $query->where('condsurvey.condsurvey_desc', '=', $inputs['description']);
            }

            if ((isset($inputs['site_id'])) && $inputs['site_id'] != "" && ($inputs['site_id'] != " ")) {
                $query->where('condsurvey.site_id', '=', $inputs['site_id']);
                //check all levels
                if (($inputs['building_id'] != "") && ($inputs['building_id'] != " ")) {
                    $query->where('condsurvey.condsurvey_building_id', '=', $inputs['building_id']);

                    if (($inputs['room_id'] != "") && ($inputs['room_id'] != " ")) {
                        $query->where('condsurvey.room_id', '=', $inputs['room_id']);
                    }
                }

                if (($inputs['building_id'] != "") && ($inputs['building_id'] != " ")) {
                    $query->where('condsurvey.condsurvey_building_block_id', '=', $inputs['block_id']);
                }
            } //end site id check

            if (($inputs['surveyor'] != "") && ($inputs['surveyor'] != " ")) {
                $query->where("condsurvey.surveyor", $inputs['surveyor']);
            }

            $status = array();
            foreach ((new CondsurveyStatus())->getStatusList() as $key => $value) {
                if (isset($inputs[$value])) {
                    $status[] = $key;
                }
            }
            if (count($status)) {
                $query->whereIn("condsurvey.condsurvey_status_id", $status);
            }
        }

        return $query;
    }

    private function baseQuery()
    {
        return Condsurvey::UserSiteGroup()->select()
        ->leftjoin(
            'site',
            'site.site_id',
            '=',
            'condsurvey.site_id'
        )
        ->leftjoin(
            'building',
            'building.building_id',
            '=',
            'condsurvey.condsurvey_building_id'
        )
        ->leftjoin(
            'address',
            'address.address_id',
            '=',
            'site.address_id'
        )
        ->leftjoin(
            'address_street',
            'address_street.address_street_id',
            '=',
            'address.address_street_id'
        )
        ->leftjoin(
            'gen_gis',
            'gen_gis.gen_gis_id',
            '=',
            'site.gen_gis_id'
        )
        ->leftjoin(
            'score',
            'score.score_id',
            '=',
            'condsurvey.score_id'
        )
        ->leftjoin(
            'condsurvey_status',
            'condsurvey_status.condsurvey_status_id',
            '=',
            'condsurvey.condsurvey_status_id'
        )
        ->leftjoin(
            'contact',
            'contact.contact_id',
            '=',
            'condsurvey.surveyor'
        )
        ->leftJoin(
            'gen_userdef_group',
            'gen_userdef_group.gen_userdef_group_id',
            '=',
            'site.gen_userdef_group_id'
        )
        ->orderBy('building.building_code');
    }

    private function getAllCondLocationScores($conditionSurveys)
    {
        $condSurveysKeys = array_pluck($conditionSurveys->toArray(), 'condsurvey_id');

        $condLocScore = CondLocationScore
            ::leftjoin(
                'building',
                'building.building_id',
                '=',
                'cond_location_score.location_record_id'
            )
            ->leftJoin(
                'score',
                'score.score_id',
                '=',
                'cond_location_score.score_id'
            )
            ->where('cond_location_type_id', '=', 2) //only building scores.
            ->whereIn('condsurvey_id', $condSurveysKeys);

        $condLocScore->orderBy('building.building_code');

        return $condLocScore->get();
    }

    private function getAllCondLocationElementScores($condLocationScores)
    {
        $condLocationScoreKeys = array_pluck($condLocationScores->toArray(), 'cond_location_score_id');

        $condLocElementScore = CondLocationElementScore::
            leftjoin(
                'cond_location_score',
                'cond_location_score.cond_location_score_id',
                '=',
                'cond_location_element_score.cond_location_score_id'
            )
            ->leftjoin(
                'idwork_element',
                'idwork_element.idwork_element_id',
                '=',
                'cond_location_element_score.idwork_element_id'
            )
            ->leftjoin(
                'score',
                'score.score_id',
                '=',
                'cond_location_element_score.score_id'
            )
            ->where('cond_location_score.cond_location_type_id', '=', 2) //only building scores.
            ->where('idwork_element.site_group_id', '=', Auth::User()->site_group_id)
            ->whereIn('cond_location_score.cond_location_score_id', $condLocationScoreKeys);

        return $condLocElementScore->get();
    }

    private function getAllIdWork($conditionSurveys)
    {
        $condSurveysKeys = array_pluck($conditionSurveys->toArray(), 'condsurvey_id');

        $idwork = Idwork::userSiteGroup()
            ->join(
                'building',
                'building.building_id',
                '=',
                'idwork.building_id'
            )
            ->leftjoin(
                'idwork_element',
                'idwork_element.idwork_element_id',
                '=',
                'idwork.element_id'
            )
            ->leftjoin(
                'idwork_item',
                'idwork_item.idwork_item_id',
                '=',
                'idwork.item_id'
            )
            ->leftjoin(
                'idwork_condition',
                'idwork_condition.idwork_condition_id',
                '=',
                'idwork.idwork_condition_id'
            )
            ->leftjoin(
                'idwork_priority',
                'idwork_priority.idwork_priority_id',
                '=',
                'idwork.idwork_priority_id'
            )
            ->leftJoin(
                'room',
                'room.room_id',
                '=',
                'idwork.room_id'
            )
            ->whereIn('condsurvey_id', $condSurveysKeys)

            ->orderBy('idwork_element.idwork_element_desc')

            ->select(['idwork.building_id',
                'idwork.idwork_code',
                'idwork_element.idwork_element_desc',
                'idwork_item.idworkitem_desc',
                'idwork_condition.idwork_condition_code',
                'idwork_priority.idwork_priority_code',
                'idwork.total_cost',
                'idwork.defect',
                'idwork.remedy',
                'room.room_number',
                'room.room_desc',
                'idwork.condsurvey_id',
                'idwork.location'
                ]);

        return $idwork->get();
    }

    private function generateFilterData($filterData, &$whereCodes, &$whereTexts, &$orCodes)
    {
        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Condition Survey Code = '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Condition Survey Description = '" . $val . "'");
        }

        if ($val = Common::iset($filterData['superseded'])) {
            array_push($whereTexts, "Superseded = '" . $val . "'");
        }

        if ($siteId = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $siteId, "Site Code"]);

            if ($buildingId = Common::iset($filterData['building_id'])) {
                array_push(
                    $whereCodes,
                    ['building', 'building_id', 'building_code', $buildingId, "Building Code"]
                );

                if ($val = Common::iset($filterData['block_id'])) {
                    array_push($whereCodes, [
                        'building_block',
                        'building_block_id',
                        'building_block_code',
                        $val,
                        "Building Block Code"
                    ]);
                }

                if ($val = Common::iset($filterData['room_id'])) {
                    array_push($whereCodes, ['room', 'room_id', 'room_number', $val, "Room Number"]);
                }
            }
        }
    }
}
