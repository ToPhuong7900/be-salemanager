<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Tfcloud\Controllers\Admin\General\SiteGroupsController;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Filters\Plant\PlantFilter;
use Tfcloud\Lib\Math;
use Tfcloud\Models\DloJob;
use Tfcloud\Models\DloJobLabour;
use Tfcloud\Models\DloParentType;
use Tfcloud\Models\GenTable;
use Tfcloud\Models\Helpcall;
use Tfcloud\Models\Plant;
use Tfcloud\Models\Report;
use Tfcloud\Services\Admin\General\SiteGroupsService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Property\PlantService;
use Tfcloud\Models\Inspection\Inspection;

class PL02AssetHistoryReportService extends WkHtmlToPdfReportBaseService
{
    private $plantService;
    private $sgController;
    private $sgService;
    public $filterText = '';
    private $reportBoxFilterLoader = '';

    public function __construct(PermissionService $permissionService, Report $report = null)
    {
        parent::__construct($permissionService);
        $this->plantService = new PlantService($permissionService);
        $this->sgService = new SiteGroupsService($permissionService);
        $this->sgController = new SiteGroupsController($permissionService, $this->sgService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function runReportSinglePlant($id)
    {
        //call report from button not report view
        $inputs['code'] = $id;
        $inputs['singleOnly'] = true;
        $repId = 254;

        return $this->runReport(
            $this->getTempFile(),
            $inputs,
            $repId
        );
    }

    public function runReport($repOutPutPdfFile, $inputs, $repId)
    {
        $siteGroupId = Auth::User()->site_group_id;
        $photoPath = $this->sgController->getPhotoPath($siteGroupId);

        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);

            $plants = $reportFilterQuery->filterAll($this->getQuery())
                ->orderBy('plant.plant_id')->get();

            $this->filterText = $reportFilterQuery->getFilterDetailText();
        } else {
            $plantQuery = $this->getQuery();
            $filteredPlantQuery = $this->setFilters($plantQuery, $inputs);
            $plants = $filteredPlantQuery->get();

            $whereCodes = [];               // The codes which have been used in the filter
            $whereTexts = [];               // Plain text about filtering applied
            $orCodes = [];                  // csv string of OR codes
            $bFilterDetail = false;

            $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
            $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $this->filterText);
        }
        //$plants = $this->plantService->getAll($inputs)->get();
        $totalNumberOfJobs = 0; //hold total count to go at end of report

        $getDate = date('Y-m-d');

        $earliestInspDateFound = str_replace('-', '', $getDate);
        $earliestHlpDateFound = str_replace('-', '', $getDate);

        $latestHlpDateFound = str_replace('-', '', $getDate);
        $latestInspDateFound = str_replace('-', '', $getDate);

        if (array_key_exists('singleOnly', $inputs)) {
            $showSingleHeader = 'true';
        } else {
            $showSingleHeader = 'false';
        }
        $plantCount = 0;
        $plantName = "";
        $plantNumber = "";
        $plantActive = "";

        foreach ($plants as $plant) {
            if (($plantCount === 0) && ($showSingleHeader === 'true')) {
                $plantNumber = $plant->plant_code;
                $plantName = $plant->plant_desc;
                $plantActive = $plant->active;
            }
            $plantCount = Math::addInt([$plantCount, 1]);
            $index = 0;
            $query = Helpcall::select()
                ->where(
                    'helpcall.plant_id',
                    '=',
                    $plant->plant_id
                )
                ->leftjoin(
                    'user',
                    'user.id',
                    '=',
                    'helpcall.logged_by'
                )
                ->leftjoin(
                    'category',
                    'category.category_id',
                    '=',
                    'helpcall.category_id'
                )
                ->leftjoin(
                    'helpcall_status',
                    'helpcall_status.helpcall_status_id',
                    '=',
                    'helpcall.helpcall_status_id'
                )
                ->orderBy('helpcall.created_at', 'ASC');
            $plant['helpcallData'] = $query->get();

            if (count($plant['helpcallData']) == 0) {
                //this plant has no helpcalls, set a flag to show there is no data
                $plant['hasHelpcalls'] = 'No helpcalls';
            } else {
                //this plant has helpcalls, proceed as normal
                $plant['hasHelpcalls'] = 'true';
            }
            foreach ($plant['helpcallData'] as $helpcall) {
                //record the dates to check the period the report has run from

                //write out all report dates to a more readable DD/MM/YYYY format
                $helpcall['loggedUser'] = strtoupper($helpcall->display_name);

                if ($helpcall->created_at) {
                    $createdTime = $helpcall->created_at;
                    $yyyMMDDOnly = substr($createdTime, 0, 10);

                    $createdYear = substr($yyyMMDDOnly, 0, 4);
                    $createdMonth = substr($yyyMMDDOnly, 5, 2);
                    $createdDay = substr($yyyMMDDOnly, 8, 2);
                    $concatDate = $createdDay . "/" . $createdMonth . "/" . $createdYear;

                    $helpcall['createdTime'] = $concatDate;

                    //check if the helpcall is the earliest returned
                    $yYYYMMDD = $createdYear . $createdMonth . $createdDay;

                    //overwrite latest date with next in line
                    $latestHlpDateFound = $yYYYMMDD;

                    if ($index == 0) {
                        //assign the first helpcall returned to the earliest date
                        if ($earliestHlpDateFound > $yYYYMMDD) {
                            $earliestHlpDateFound = $yYYYMMDD;
                        }
                        $index++;
                    }
                }

                if ($helpcall->due_date) {
                    $dueValue = $helpcall->due_date;
                    $yyyMMDDOnly = substr($dueValue, 0, 10);

                    $createdYear = substr($yyyMMDDOnly, 0, 4);
                    $createdMonth = substr($yyyMMDDOnly, 5, 2);
                    $createdDay = substr($yyyMMDDOnly, 8, 2);
                    $concatDate = $createdDay . "/" . $createdMonth . "/" . $createdYear;

                    $helpcall['dueTime'] = $concatDate;
                }

                if ($helpcall->completed_date) {
                    $completedValue = $helpcall->completed_date;
                    $yyyMMDDOnly = substr($completedValue, 0, 10);

                    $createdYear = substr($yyyMMDDOnly, 0, 4);
                    $createdMonth = substr($yyyMMDDOnly, 5, 2);
                    $createdDay = substr($yyyMMDDOnly, 8, 2);
                    $concatDate = $createdDay . "/" . $createdMonth . "/" . $createdYear;

                    $helpcall['completedDate'] = $concatDate;
                }
                $totalNumberOfJobs = Math::addInt([$totalNumberOfJobs, 1]); //increment the count of jobs

                //find any DLO jobs associated with the helpcall
                $dloQuery = DloJob::select()
                    ->where(
                        'dlo_job.parent_id',
                        '=',
                        $helpcall->helpcall_id
                    )
                    ->leftjoin(
                        'dlo_job_type',
                        'dlo_job_type.dlo_job_type_id',
                        '=',
                        'dlo_job.dlo_job_type_id'
                    )
                    ->leftjoin(
                        'user',
                        'user.id',
                        '=',
                        'dlo_job.created_user_id'
                    )
                    ->where(
                        'dlo_job.dlo_parent_type_id',
                        '=',
                        DloParentType::HELP_DESK
                    );

                $helpcall['dloJobData'] = $dloQuery->get();
                if (count($helpcall['dloJobData']) == 0) {
                    //this plant has no helpcalls, set a flag to show there is no data
                    $helpcall['hasDLO'] = 'No DLO Jobs';
                } else {
                    //this plant has helpcalls, proceed as normal
                    $helpcall['hasDLO'] = 'true';
                }

                foreach ($helpcall['dloJobData'] as $dloJob) {
                    //fix date formatting for dlo information
                    if ($dloJob->created_on) {
                        $createdTime = $dloJob->created_on;
                        $yyyMMDDOnly = substr($createdTime, 0, 10);

                        $createdYear = substr($yyyMMDDOnly, 0, 4);
                        $createdMonth = substr($yyyMMDDOnly, 5, 2);
                        $createdDay = substr($yyyMMDDOnly, 8, 2);
                        $concatDate = $createdDay . "/" . $createdMonth . "/" . $createdYear;

                        $dloJob['createdDate'] = $concatDate;

                        $createdTimeStamp = substr($createdTime, 10, 6);
                        $dloJob['createdTime'] = $createdTimeStamp;
                    }

                    //get any labour lines
                    $dloLabourQuery = DloJobLabour::select()
                    ->where(
                        'dlo_job_labour.dlo_job_id',
                        '=',
                        $dloJob->dlo_job_id
                    )
                    ->leftjoin(
                        'dlo_job_operative',
                        'dlo_job_operative.dlo_job_operative_id',
                        '=',
                        'dlo_job_labour.dlo_job_operative_id'
                    )
                    ->leftjoin(
                        'user',
                        'user.id',
                        '=',
                        'dlo_job_operative.user_id'
                    );

                    $dloJob['dloJobLabour'] = $dloLabourQuery->get();
                    if (count($dloJob['dloJobLabour']) == 0) {
                        //this plant has no labour, set a flag to show there is no data
                        $dloJob['hasDLOJobLabour'] = 'No DLO Jobs';
                    } else {
                        //this plant has dlo job labour, proceed as normal
                        $dloJob['hasDLOJobLabour'] = 'true';
                    }

                    foreach ($dloJob['dloJobLabour'] as $dloJobLabour) {
                        //calculate the total number of hours
                        $jobHours = 0;
                        $totalHours = 0;
                        $totalCost = 0;

                        // Use the Model attribute.
                        $jobHours = $dloJobLabour->total_hours;

                        //add hours for that labour to total for job
                        $totalHours = Math::addCurrency([$totalHours, $jobHours]);
                        //add cost for that labour to total for job
                        $totalCost = Math::addCurrency([$totalCost, $dloJobLabour->labour_total]);

                        $dloJobLabour['dloJobLabourHours'] = $totalHours;
                        $dloJobLabour['dloJobLabourCost'] = $totalCost;

                        //format the starting date
                        $start = DateTime::createFromFormat('Y-m-d H:i:s', $dloJobLabour->start_date);
                        if ($start) {
                            $dloJobLabour['createdDate'] = $start->format('d/m/Y');
                        }
                    } //end foreach dlo job labour
                } //end foreach dlo job
            } //end foreach helpcall

            //now get all inspections associated with the plant
            $inspectionQuery = Inspection::select()
                ->where(
                    'inspection.plant_id',
                    '=',
                    $plant->plant_id
                )
                ->leftjoin(
                    'user',
                    'user.id',
                    '=',
                    'inspection.owner_user_id'
                )
                ->leftjoin(
                    'inspection_type',
                    'inspection_type.inspection_type_id',
                    '=',
                    'inspection.inspection_type_id'
                )
                ->leftjoin(
                    'inspection_status',
                    'inspection_status.inspection_status_id',
                    '=',
                    'inspection.inspection_status_id'
                )
                ->orderBy('inspection.created_date', 'ASC');

            $plant['inspectionData'] = $inspectionQuery->get();

            if (count($plant['inspectionData']) == 0) {
                //this plant has no inspections, set a flag to show there is no data
                $plant['hasInspections'] = 'No inspections';
            } else {
                //this plant has inspections, proceed as normal
                $plant['hasInspections'] = 'true';
            }

            foreach ($plant['inspectionData'] as $inspection) {
                //check if a signature should be output
                if ($inspection->signature) {
                    //get client upload path
                    $uploadFolder = Config::get('cloud.legacy_paths.uploads');
                    $signaturePath = "";
                    $signaturePath = $uploadFolder .
                            "/" . $siteGroupId . "/inspection/" . $inspection->inspection_id .
                            "/images/" . $inspection->signature;

                    //check image exists
                    if (!file_exists($signaturePath)) {
                        //show no picture available file
                        $useDefaultPhoto = true;
                        $signaturePath = $this->fallBackImagePath($useDefaultPhoto);
                        $inspection['signaturePath'] = $signaturePath;
                    } else {
                        $inspection['signaturePath'] = $signaturePath;
                    }

                    $inspection['hasSignature'] = 'true';
                } else {
                    $inspection['hasSignature'] = 'false';
                    //try and repress errors
                    $useDefaultPhoto = true;
                    $inspection['signaturePath'] = $this->fallBackImagePath($useDefaultPhoto);
                }

                //format the dates to be output
                if ($inspection->created_date) {
                    $createdTime = $inspection->created_date;

                    $createdYear = substr($createdTime, 0, 4);
                    $createdMonth = substr($createdTime, 5, 2);
                    $createdDay = substr($createdTime, 8, 2);
                    $concatDate = $createdDay . "/" . $createdMonth . "/" . $createdYear;

                    $inspection['createdDate'] = $concatDate;

                    //check if the inspection is the earliest returned
                    $yYYYMMDD = $createdYear . $createdMonth . $createdDay ;

                    //overwrite latest date with next in line
                    $latestInspDateFound = $yYYYMMDD;

                    if ($index == 0) {
                        //assign the first inspection returned to the earliest date
                        if ($earliestInspDateFound > $yYYYMMDD) {
                            $earliestInspDateFound = $yYYYMMDD;
                        }
                        $index++;
                    }
                }

                if ($inspection->inspection_due_date) {
                    $createdTime = $inspection->inspection_due_date;

                    $createdYear = substr($createdTime, 0, 4);
                    $createdMonth = substr($createdTime, 5, 2);
                    $createdDay = substr($createdTime, 8, 2);
                    $concatDate = $createdDay . "/" . $createdMonth . "/" . $createdYear;

                    $inspection['dueDate'] = $concatDate;
                }

                if ($inspection->completed_date) {
                    $createdTime = $inspection->completed_date;

                    $createdYear = substr($createdTime, 0, 4);
                    $createdMonth = substr($createdTime, 5, 2);
                    $createdDay = substr($createdTime, 8, 2);
                    $concatDate = $createdDay . "/" . $createdMonth . "/" . $createdYear;

                    $inspection['completedDate'] = $concatDate;
                }

                $totalNumberOfJobs = Math::addInt([$totalNumberOfJobs, 1]); //increment the count of jobs

                //find any DLO jobs associated with the inspection
                $dloQuery = DloJob::select()
                    ->where(
                        'dlo_job.parent_id',
                        '=',
                        $inspection->inspection_id
                    )
                    ->leftjoin(
                        'dlo_job_type',
                        'dlo_job_type.dlo_job_type_id',
                        '=',
                        'dlo_job.dlo_job_type_id'
                    )
                    ->leftjoin(
                        'user',
                        'user.id',
                        '=',
                        'dlo_job.created_user_id'
                    )
                    ->where(
                        'dlo_job.dlo_parent_type_id',
                        '=',
                        DloParentType::INSPECTION
                    );

                $inspection['dloJobData'] = $dloQuery->get();

                if (count($inspection['dloJobData']) == 0) {
                    //this plant has no helpcalls, set a flag to show there is no data
                    $inspection['hasDLO'] = 'No DLO Jobs';
                } else {
                    //this plant has helpcalls, proceed as normal
                    $inspection['hasDLO'] = 'true';
                }

                foreach ($inspection['dloJobData'] as $inspectionDLOJob) {
                    if ($inspectionDLOJob->created_on) {
                        $createdTime = $inspectionDLOJob->created_on;
                        $yyyMMDDOnly = substr($createdTime, 0, 10);

                        $createdYear = substr($yyyMMDDOnly, 0, 4);
                        $createdMonth = substr($yyyMMDDOnly, 5, 2);
                        $createdDay = substr($yyyMMDDOnly, 8, 2);
                        $concatDate = $createdDay . "/" . $createdMonth . "/" . $createdYear;

                        $inspectionDLOJob['createdDate'] = $concatDate;

                        $createdTimeStamp = substr($createdTime, 10, 6);
                        $inspectionDLOJob['createdTime'] = $createdTimeStamp;
                    }

                    //get any labour lines
                    $dloLabourQuery = DloJobLabour::select()
                    ->where(
                        'dlo_job_labour.dlo_job_id',
                        '=',
                        $inspectionDLOJob->dlo_job_id
                    )
                    ->leftjoin(
                        'dlo_job_operative',
                        'dlo_job_operative.dlo_job_operative_id',
                        '=',
                        'dlo_job_labour.dlo_job_operative_id'
                    )
                    ->leftjoin(
                        'user',
                        'user.id',
                        '=',
                        'dlo_job_operative.user_id'
                    );

                    $inspectionDLOJob['dloJobLabour'] = $dloLabourQuery->get();
                    if (count($inspectionDLOJob['dloJobLabour']) == 0) {
                        //this plant has no labour, set a flag to show there is no data
                        $inspectionDLOJob['hasDLOJobLabour'] = 'No DLO Jobs';
                    } else {
                        //this plant has dlo job labour, proceed as normal
                        $inspectionDLOJob['hasDLOJobLabour'] = 'true';
                    }

                    foreach ($inspectionDLOJob['dloJobLabour'] as $dloJobLabour) {
                        //calculate the total number of hours
                        $jobHours = 0;
                        $totalHours = 0;
                        $totalCost = 0;

                        // Use the Model attribute.
                        $jobHours = $dloJobLabour->total_hours;

                        //add hours for that labour to total for job
                        $totalHours = Math::addCurrency([$totalHours, $jobHours]);
                        //add cost for that labour to total for job
                        $totalCost = Math::addCurrency([$totalCost, $dloJobLabour->labour_total]);

                        $dloJobLabour['dloJobLabourHours'] = $totalHours;
                        $dloJobLabour['dloJobLabourCost'] = $totalCost;

                        //format the starting date
                        $start = DateTime::createFromFormat('Y-m-d H:i:s', $dloJobLabour->start_date);
                        if ($start) {
                            $dloJobLabour['createdDate'] = $start->format('d/m/Y');
                        }
                    } //end foreach dlo job labour
                }
            } //end foreach inspection
        } //end foreach plant

        $earliestDate = "";
        if ($earliestHlpDateFound > $earliestInspDateFound) {
            $earliestDate = $earliestInspDateFound;
        } else {
            $earliestDate = $earliestHlpDateFound;
        }

        $latestDate = "";
        if ($latestHlpDateFound > $latestInspDateFound) {
            $latestDate = $latestHlpDateFound;
        } else {
            $latestDate = $latestInspDateFound;
        }

        $earliestYear = substr($earliestDate, 0, 4);
        $earliestMonth = substr($earliestDate, 4, 2);
        $earliestDay = substr($earliestDate, 6, 2);
        $fromDateRange = $earliestDay . "/" . $earliestMonth . "/" . $earliestYear;

        $latestYear = substr($latestDate, 0, 4);
        $latestMonth = substr($latestDate, 4, 2);
        $latestDay = substr($latestDate, 6, 2);
        $toDateRange = $latestDay . "/" . $latestMonth . "/" . $latestYear;

        // Create wkhtmltopdf temp folder to store generated files
        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);

        $header = View::make(
            'reports.wkhtmltopdf.plant.pl02.header',
            [
                'plants'       => $plants,
                'earliestDate' => $fromDateRange,
                'latestDate'   => $toDateRange,
                'photoPath'    => $photoPath,
                'assetNumber'  => $plantNumber,
                'active'       => $plantActive,
                'assetName'    => $plantName,
                'assetHeader'  => $showSingleHeader
            ]
        )->render();

        $headerPath = $generatedPath . "/header.html";
        file_put_contents($headerPath, $header, LOCK_EX);

        $footer = View::make('reports.wkhtmltopdf.plant.pl02.footer')->render();
        $footerPath = $generatedPath . "/footer.html";
        file_put_contents($footerPath, $footer, LOCK_EX);

        $content = View::make(
            'reports.wkhtmltopdf.plant.pl02.content',
            [
                'plants' => $plants,
                'totalJobNo' => $totalNumberOfJobs,
                'assetHeader' => $showSingleHeader
            ]
        )->render();

        $contentPath = $generatedPath . "/content.html";

        file_put_contents($contentPath, $content, LOCK_EX);

        $whereCodes = [];               // The codes which have been used in the filter
        $whereTexts = [];               // Plain text about filtering applied
        $orCodes = [];                  // csv string of OR codes
        $bFilterDetail = false;

        $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
        $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $this->filterText);

        $options = [
            'header-html' => $headerPath,
            'footer-html' => $footerPath,
            'header-spacing' => 4,
            'footer-spacing' => 4,
            'orientation' => 'portrait',
        ];

        if (array_key_exists('singleOnly', $inputs)) {
            $repOutPutPdfFile .= '.pdf';
            $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $options);
            return $repOutPutPdfFile;
        } else {
            $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $options);
            return true;
        }
    }

    private function getTempFile()
    {
        return tempnam(Common::getTempFolder(), 'PL02SinglePlant');
    }

    private function setFilters($query, $inputs)
    {
        $filter = new PlantFilter($inputs);
        $table = 'plant';

        if (array_key_exists('singleOnly', $inputs)) {
            //running from single plant disqualifies other filters
            if ($inputs['code'] != "") {
                $query->where(
                    'plant.plant_id',
                    '=',
                    $inputs['code']
                );
            }
        } else {
            if ($inputs['code'] != "") {
                $query->where(
                    'plant.plant_code',
                    '=',
                    $inputs['code']
                );
            }

            if ($inputs['plant_group_id'] != "") {
                $query->where(
                    'plant.plant_group_id',
                    '=',
                    $inputs['plant_group_id']
                );

                if ($inputs['plant_subgroup_id'] != "") {
                    $query->where(
                        'plant.plant_subgroup_id',
                        '=',
                        $inputs['plant_subgroup_id']
                    );

                    if ($inputs['plant_type_id'] != "") {
                        $query->where(
                            'plant.plant_type_id',
                            '=',
                            $inputs['plant_type_id']
                        );
                    }
                }
            } //end group id if

            if ($inputs['description'] != "") {
                $query->where(
                    'plant.plant_desc',
                    '=',
                    $inputs['description']
                );
            }

            if ($inputs['site_id'] != "") {
                $query->where(
                    'plant.site_id',
                    '=',
                    $inputs['site_id']
                );

                if ($inputs['building_id'] != "") {
                    $query->where(
                        'plant.building_id',
                        '=',
                        $inputs['building_id']
                    );

                    if ($inputs['room_id'] != "") {
                        $query->where(
                            'plant.room_id',
                            '=',
                            $inputs['room_id']
                        );
                    } //end room if
                } //end building if
            } //end site if

            if ($inputs['plant_criticality_id'] != "") {
                $query->where(
                    'plant.plant_criticality_id',
                    '=',
                    $inputs['plant_criticality_id']
                );
            }

            if ($inputs['plant_condition_id'] != "") {
                $query->where(
                    'plant.plant_condition_id',
                    '=',
                    $inputs['plant_condition_id']
                );
            }

            if ($inputs['status'] != "all") {
                $query->where(
                    'plant.active',
                    '=',
                    $inputs['status']
                );
            }

            // plantKind will not be defined if Grounds Maintenance module is switched off
            $plantKind = $inputs['plantKind'] ?? Plant::KIND_ALL;
            if ($plantKind != Plant::KIND_ALL) {
                $gmPlant = null;
                if ($plantKind == Plant::KIND_GENERAL) {
                    $gmPlant = CommonConstant::DATABASE_VALUE_NO;
                } elseif ($plantKind == Plant::KIND_GROUNDS_MAINTENANCE) {
                    $gmPlant = CommonConstant::DATABASE_VALUE_YES;
                }
                if ($gmPlant) {
                    $query->where(
                        'plant.gm_plant',
                        '=',
                        $gmPlant
                    );
                }
            }

            if ($inputs['contact'] != "") {
                $query->where(
                    'plant.maint_contact_id',
                    '=',
                    $inputs['contact']
                );
            }

            if ($inputs['owner'] != "") {
                $query->where(
                    'plant.owner_user_id',
                    '=',
                    $inputs['owner']
                );
            }
            //\Log::info("inputs.are"); \Log::info($inputs); \Log::info("end");
            // Replacement Year
            $currentYear = date_format(new \DateTime(), 'Y');
            if (
                !is_null($inputs['replacementYear']) &&
                ($inputs['replacementYear'] != "") &&
                is_numeric($inputs['replacementYear']) &&
                $inputs['replacementYear'] < $currentYear + 10 &&
                $inputs['replacementYear'] >= $currentYear
            ) {
                $query->where("plant.remaining_life", '=', $inputs['replacementYear'] - $currentYear);
            }

            // Age RAG
            if ($inputs['ageRagFrom'] !== '' && is_numeric($inputs['ageRagFrom'])) {
                $query->where(
                    DB::raw("round(plant.remaining_life / plant.expected_life * 100)"),
                    '>=',
                    $inputs['ageRagFrom']
                );
            }

            if ($inputs['ageRagTo'] !== '' && is_numeric($inputs['ageRagTo'])) {
                $query->where(
                    DB::raw("round(plant.remaining_life / plant.expected_life * 100)"),
                    '<=',
                    $inputs['ageRagTo']
                );
            }
        }
        $this->filterByUserDefines(GenTable::PLANT, $query, $filter, $table);

        return $query;
    }

    private function generateFilterData($filterData, &$whereCodes, &$whereTexts, &$orCodes)
    {
        if ($val = array_get($filterData, 'plant_group_id')) {
            array_push($whereCodes, ['plant_group', 'plant_group_id', 'plant_group_code', $val, "Plant Group"]);

            if ($val = array_get($filterData, 'plant_subgroup_id')) {
                array_push(
                    $whereCodes,
                    ['plant_subgroup', 'plant_subgroup_id', 'plant_subgroup_code', $val, "Plant Sub Group"]
                );

                if ($val = array_get($filterData, 'plant_type_id')) {
                    array_push(
                        $whereCodes,
                        ['plant_type', 'plant_type_id', 'plant_type_code', $val, "Plant Type"]
                    );
                }
            }
        }

        if ($val = array_get($filterData, 'code')) {
            array_push($whereTexts, "Code contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'description')) {
            array_push($whereTexts, "Description contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'replacementYear')) {
            array_push($whereTexts, "Replacement Year = $val");
        }

        $ageRagFrom = array_get($filterData, 'ageRagFrom');
        $ageRagTo = array_get($filterData, 'ageRagTo');
        if ($ageRagFrom && $ageRagTo) {
            array_push($whereTexts, "Age RAG (%) from $ageRagFrom to $ageRagTo");
        } else {
            if ($ageRagFrom) {
                array_push($whereTexts, "Age RAG (%) From $ageRagFrom");
            } elseif ($ageRagTo) {
                array_push($whereTexts, "Age RAG (%) To $ageRagTo");
            }
        }

        if ($siteId = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $siteId, "Site Code"]);

            if ($buildingId = Common::iset($filterData['building_id'])) {
                array_push(
                    $whereCodes,
                    ['building', 'building_id', 'building_code', $buildingId, "Building Code"]
                );

                if ($val = Common::iset($filterData['block_id'])) {
                    array_push($whereCodes, [
                        'building_block',
                        'building_block_id',
                        'building_block_code',
                        $val,
                        "Building Block Code"
                    ]);
                }

                if ($val = Common::iset($filterData['room_id'])) {
                    array_push($whereCodes, ['room', 'room_id', 'room_number', $val, "Room Number"]);
                }
            }
        }

        if ($val = array_get($filterData, 'plant_criticality_id')) {
            array_push(
                $whereCodes,
                ['plant_criticality', 'plant_criticality_id', 'plant_criticality_code', $val, "Operational Criticality"]
            );
        }

        if ($val = array_get($filterData, 'plant_condition_id')) {
            array_push(
                $whereCodes,
                ['plant_condition', 'plant_condition_id', 'plant_condition_code', $val, "Condition"]
            );
        }

        if ($val = array_get($filterData, 'status')) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, "Status = 'Active'");
                    break;
                case 'all':
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                default:
                    array_push($whereTexts, "Status = 'Inactive'");
                    break;
            }
        }

        if ($val = array_get($filterData, 'plantKind')) {
            switch ($val) {
                case Plant::KIND_GENERAL:
                    array_push($whereTexts, "Plant Kind = 'General'");
                    break;
                case Plant::KIND_GROUNDS_MAINTENANCE:
                    array_push($whereTexts, "Plant Kind = 'Grounds Maintenance'");
                    break;
                default:
                    break;
            }
        }

        if ($val = array_get($filterData, 'contact')) {
            array_push(
                $whereCodes,
                ['contact', 'contact_id', 'contact_name', $val, "Maint. Contact"]
            );
        }

        if ($val = array_get($filterData, 'owner')) {
            array_push(
                $whereCodes,
                ['user', 'id', 'display_name', $val, "Owner"]
            );
        }

        if ($val = array_get($filterData, 'prop_external_id')) {
            array_push(
                $whereCodes,
                ['prop_external', 'prop_external_id', 'prop_external_code', $val, "External Area Code"]
            );
        }

        $this->reAddUserDefinesQuery(GenTable::PLANT, $filterData, $whereCodes, $whereTexts);
    }

    private function getQuery()
    {
        return Plant::select();
    }
}
