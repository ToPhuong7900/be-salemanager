<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use DateTime;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Constant\SurveyConstant;
use Tfcloud\Models\Address;
use Tfcloud\Models\AsbOverallRisk;
use Tfcloud\Models\AsbRiskAssessment;
use Tfcloud\Models\AsbRiskStatus;
use Tfcloud\Models\AsbRoom;
use Tfcloud\Models\AsbSurvey;
use Tfcloud\Models\AsbSurveyStatus;
use Tfcloud\Models\AsbSurveyHazard;
use Tfcloud\Models\GenTable;
use Tfcloud\Models\Report;
use Tfcloud\Services\Admin\General\SiteGroupsService;
use Tfcloud\Services\Assessment\AsbRiskAssessmentService;
use Tfcloud\Services\PermissionService;

class AsbestosReportForFifeService extends WkHtmlToPdfReportBaseService
{
    private $pageIndex = 1;
    private $generatedPath = null;
    private $asbRiskAssessmentService = null;
    private $siteGroupsService = null;
    private $cssPaging = ['fontSize' => 8, 'x' => 750, 'y' => 14, 'customY2' => 19];
    private $filter = null;
    public $filterText = '';
    private $reportBoxFilterLoader = null;

    private const ASB_CLI_FIF01_REPORT_ID = 310;

    public function __construct(PermissionService $permissionService, Report $report = null)
    {
        $this->permissionService = $permissionService;
        $this->asbRiskAssessmentService = new AsbRiskAssessmentService($this->permissionService);
        $this->siteGroupsService = new SiteGroupsService($this->permissionService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function runReportSingleAsbSurvey($id)
    {
        //call report from button not report view
        $inputs['asb_survey_id'] = $id;
        $inputs['singleOnly'] = true;
        $repId = self::ASB_CLI_FIF01_REPORT_ID;

        return $this->runReport(
            $this->getTempFile(),
            $inputs,
            $repId
        );
    }

    public function runReport($repOutPutPdfFile, $inputs, $repId)
    {
        $arrSurveyPdfMerge = [];
        $childHeaderRender = null;
        $headerTemplate = 'header';
        $childHeaderData = [];

        if (array_key_exists('singleOnly', $inputs)) {
            $repOutPutPdfFile .= '.pdf';
        }
        $this->generatedPath = $this->generateWkhtmltopdfTempPath($repId);
        \Log::info("Generate the report from the html.");

        $whereCodes = [];               // The codes which have been used in the filter
        $whereTexts = [];               // Plain text about filtering applied
        $orCodes = [];                  // csv string of OR codes
        $bFilterDetail = false;

        $reportFilterQuery = null;
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $operator = $reportFilterQuery->getOperatorParser();
            $operator->setPrefixTables([
                'site' => ['site_id', 'site_code', 'site_desc'],
                'asb_survey_type' => [
                    'survey_type_id',
                    'survey_type_code'
                ],
                'building' => ['building_id', 'building_code', 'building_desc'],
            ]);

            $asbSurveyList = $reportFilterQuery->filterAll($this->all())->get();
            $this->filterText = $reportFilterQuery->getFilterDetailText();
        } else {
            $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
            $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $this->filterText);
            $asbSurveyList = $this->filter($this->all(), $inputs)->get();
        }
        $asbSurveyFirst = current($asbSurveyList);
        if (($asbSurveyList->isEmpty())) {
            $childHeaderData = $this->childHeaderData(null);
            $this->generateLastPage($childHeaderData, $repOutPutPdfFile, true);
            if (array_key_exists('singleOnly', $inputs)) {
                return $repOutPutPdfFile;
            }

            return true;
        }

        foreach ($asbSurveyList as $asbSurvey) {
            $arrPdfMerge = [];
            $surveyId = $asbSurvey->asb_survey_id;
            $pagePart = $this->generatedPath . "/" . $this->pageIndex . '.html';
            $header = $this->generateHeader();

            $childHeaderData = $this->childHeaderData($asbSurvey);
            $childHeaderRender = $this->getView($childHeaderData, 'child-header');
            file_put_contents($pagePart, $childHeaderRender, FILE_APPEND | LOCK_EX);
            $surveyRender = $this->getView($childHeaderData, 'survey');
            file_put_contents($pagePart, $surveyRender, FILE_APPEND | LOCK_EX);
            $childHeaderData['isRedFooterText'] = false;
            $footerRender = $this->getView($childHeaderData, 'footer');

            $options = [
                'footer-html' => $footerRender,
                'header-html' => $header,
                'orientation' => 'landscape',
                //'footer-spacing' => 4
            ];
            $pdf = $this->generateMultiHtmlToPdf(
                [$pagePart],
                $this->generatedPath,
                $repOutPutPdfFile,
                $options,
                $this->pageIndex,
                false
            );
            $arrPdfMerge = array_merge($arrPdfMerge, $pdf);

            $pagePart = $this->generatedPath . "/" . $this->pageIndex . '.html';
            $isRoom = $this->fillRooms($asbSurvey->asb_survey_id, $pagePart);
            if ($isRoom) {
                $childHeaderData['isFirstPageOfBlock'] = false;
                $childHeaderRender = $this->getView($childHeaderData, 'child-header');
                $childHeaderData['isRedFooterText'] = true;
                $footerRender = $this->getView($childHeaderData, 'footer');

                $options = [
                    'header-html' => $childHeaderRender,
                    'footer-html' => $footerRender,
                    'orientation' => 'landscape'
                ];

                $pdf = $this->generateMultiHtmlToPdf(
                    [$pagePart],
                    $this->generatedPath,
                    $repOutPutPdfFile,
                    $options,
                    $this->pageIndex,
                    false
                );
                $arrPdfMerge = array_merge($arrPdfMerge, $pdf);
            }

            $pdf = $this->generateLastPage($childHeaderData, $repOutPutPdfFile);
            $arrPdfMerge = array_merge($arrPdfMerge, $pdf);

            if (!empty($arrPdfMerge)) {
                $surveyPdfFile = $this->generatedPath . "/surveyId_" . $surveyId . '.pdf';

                $this->mergerPdf($arrPdfMerge, $surveyPdfFile, ['cssPaging' => $this->cssPaging]);
                array_push($arrSurveyPdfMerge, $surveyPdfFile);
            }
        }

        $this->mergerPdf($arrSurveyPdfMerge, $repOutPutPdfFile, ['cssPaging' => $this->cssPaging, 'isPaging' => false]);
        \Log::info("END Generate the report from the html.");

        if (array_key_exists('singleOnly', $inputs)) {
            return $repOutPutPdfFile;
        } else {
            return true;
        }
    }

    private function getTempFile()
    {
        return tempnam(Common::getTempFolder(), 'ASBFIF01SingleFullAsbSurvey');
    }

    private function generateHeader()
    {
        $siteGroupImagePath = false;
        $siteGroupId = \Auth::user()->site_group_id;

        try {
            $siteGroupImagePath = $this->siteGroupsService->get($siteGroupId, false)
                ->siteGroupReportLogoPath();
            if (strpos($siteGroupImagePath, CommonConstant::REPORT_LOGO_FILE) === false) {
                $siteGroupImagePath = false;
            }
        } catch (\Exception $ex) {
            \Log::error('AsbestosReportForFifeService: Generation Report - ' . $ex->getMessage());
        }

        return $this->getView(['siteGroupImagePath' => $siteGroupImagePath], 'header');
    }

    private function generateLastPage($childHeaderData, $repOutPutPdfFile, $isNoSurvey = false)
    {
        $childHeaderData['isRedFooterText'] = false;
        $footerRender = $this->getView($childHeaderData, 'footer');
        $options = [
            'header-spacing' => 10,
            'footer-html' => $footerRender,
            'orientation' => 'landscape'
        ];
        if ($isNoSurvey) {
            $headerPart = $this->generatedPath . "/" . $this->pageIndex . '.html';
            $header = $this->generateHeader();
            file_put_contents($headerPart, $header, FILE_APPEND | LOCK_EX);
            $childHeaderRender = $this->getView($childHeaderData, 'child-header');
            file_put_contents($headerPart, $childHeaderRender, FILE_APPEND | LOCK_EX);

            $this->pageIndex++;
            $pagePart = $this->generatedPath . "/" . $this->pageIndex . '.html';
            $lastSurveyRender = $this->getView($childHeaderData, 'last-survey');
            file_put_contents($pagePart, $lastSurveyRender, FILE_APPEND | LOCK_EX);

            $arrPdf = $this->generateMultiHtmlToPdf(
                [$headerPart, $pagePart],
                $this->generatedPath,
                $repOutPutPdfFile,
                $options,
                $this->pageIndex,
                false
            );

            $this->mergerPdf($arrPdf, $repOutPutPdfFile, ['cssPaging' => $this->cssPaging]);
            return true;
        } else {
            $pagePart = $this->generatedPath . "/" . $this->pageIndex . '.html';

            $childHeaderData['isRedFooterText'] = false;
            $lastSurveyRender = $this->getView($childHeaderData, 'last-survey');
            file_put_contents($pagePart, $lastSurveyRender, FILE_APPEND | LOCK_EX);

            $pdf = $this->generateMultiHtmlToPdf(
                [$pagePart],
                $this->generatedPath,
                $repOutPutPdfFile,
                $options,
                $this->pageIndex,
                false
            );

            return $pdf;
        }
    }

    private function fillRooms($asbSurveyId, &$pagePart)
    {
        $asbRooms = $this->formatAllRooms($this->getAllRooms($asbSurveyId), $asbSurveyId);

        foreach ($asbRooms as $asbRoom) {
            $roomRender = $this->getView($asbRoom, 'room');
            file_put_contents($pagePart, $roomRender, FILE_APPEND | LOCK_EX);
        }

        return empty($asbRooms) ? false : true;
    }

    private function formatAllRooms($asbRooms, $asbSurveyId)
    {
        $result = [];

        $first = true;
        $hazCount = 0;

        foreach ($asbRooms as $asbRoom) {
            $alltheAcms = $this->getAllACM($asbSurveyId, $asbRoom->room_id);

            if ($alltheAcms) {
                foreach ($alltheAcms as &$tmp) {
                    $acm = \Tfcloud\Models\AsbHazard::find($tmp->asb_hazard_id);
                    \log::error($acm->photoPath());
                    \log::error($acm->asb_hazard_code);
                    $tmp['photoPath'] = $acm->photoPath();
                }
            }

            $result[] = [
                'first' => $first,  // only include css for first room, else causes timeouts when many hundred of rooms
                'roomNonAsbComment' => $asbRoom->non_asbestos_comment,
                'roomCode' => $asbRoom->room_number,
                'roomDescription' => $asbRoom->room_desc,
                'roomTypeDescription' => $asbRoom->room_type_desc,
                'roomStatus' => $asbRoom->asb_room_status_id,
                'roomComment' => $asbRoom->asb_room_comment,
                'ACMs' => $this->formatACM($alltheAcms, $hazCount),
                'hazCount' => $hazCount
            ];
            $first = false;
        }

        return $result;
    }

    private function formatACM($asbHazards, &$hazCount)
    {
        $result = [];
        $hazCount = 0;

        foreach ($asbHazards as $asbHazard) {
            $result[]  = [
                'acmCode' => $asbHazard->asb_hazard_code,
                'location' => $asbHazard->asb_hazard_position,
                'description' => $asbHazard->asb_hazard_desc,
                'material' => $asbHazard->asb_material_type_desc,
                'subType'  => $asbHazard->asb_material_subtype_desc,
                'status'   => $asbHazard->asb_hazard_status_desc,
                'asbestosType'   => $asbHazard->asb_asbestos_type_desc,
                'quality'   => $asbHazard->asb_hazard_quantity,
                'qualityUnit' => $asbHazard->unit_of_measure_code,
                'risk' => $this->getRiskAssessment($asbHazard->asb_hazard_id),
                'comment' => $asbHazard->asb_hazard_comment,
                'photoPath' => $asbHazard->photoPath
            ];

            $hazCount++;
        }

        return $result;
    }


    private function getRiskAssessment($asbHazardId)
    {
        $result = '';

        $riskAssessments = AsbRiskAssessment::select('asb_risk_assessment.total_overall_score')
            ->where('asb_hazard_id', $asbHazardId)
            ->where('asb_risk_assessment.asb_risk_status_id', AsbRiskStatus::CURRENT)
            ->orderBy('asb_risk_assessment.created_date', 'DESC');

        $riskAssessmentObj = $riskAssessments->first();

        if ($riskAssessmentObj instanceof AsbRiskAssessment) {
            if (is_null($riskAssessmentObj->total_overall_score)) {
                return $result;
            }
            $overallRisk = $this->asbRiskAssessmentService->getOverallRiskByScore(
                $riskAssessmentObj->total_overall_score
            );

            if ($overallRisk instanceof AsbOverallRisk) {
                return $overallRisk->overall_risk_code;
            }
        }

        return $result;
    }

    private function getAllRooms($asbSurveyId)
    {
        $select = [
            'asb_room.non_asbestos_comment',
            'asb_room.room_id',
            'asb_room.asb_room_status_id',
            'asb_room.asb_room_comment',
            'building_asb_room.building_code',
            'room_asb_room.room_number',
            'room_asb_room.room_desc',
            'room_type.room_type_desc'

        ];
        $asbRooms = AsbRoom::select($select)
            ->where('asb_room.asb_survey_id', $asbSurveyId)
            ->join(
                'building AS building_asb_room',
                'building_asb_room.building_id',
                '=',
                'asb_room.building_id'
            )
            ->join(
                'room AS room_asb_room',
                'room_asb_room.room_id',
                '=',
                'asb_room.room_id'
            )
            ->leftJoin(
                'room_type',
                'room_asb_room.room_type_id',
                '=',
                'room_type.room_type_id'
            )
            ->orderBy('building_asb_room.building_code', 'ASC')
            ->orderBy('room_asb_room.room_number', 'ASC');

        if (!empty($this->filter->property_status)) {
            $asbRooms->where('room_asb_room.active', '=', $this->filter->property_status);
        }


        return $asbRooms->get();
    }

    private function getAllACM($asbSurveyId, $roomId)
    {
        $select = [
            'asb_hazard.asb_hazard_id',
            'asb_hazard.asb_hazard_position',
            'asb_hazard.asb_hazard_desc',
            'asb_hazard_status.asb_hazard_status_desc',
            'asb_hazard.asb_hazard_code',
            'asb_material_type.asb_material_type_desc',
            'asb_material_subtype.asb_material_subtype_desc',
            'asb_asbestos_type.asb_asbestos_type_desc',
            'asb_hazard.asb_hazard_quantity',
            'unit_of_measure.unit_of_measure_code',
            'asb_hazard.asb_hazard_comment'
        ];

        $asbACMs = AsbSurveyHazard::with('acm')
            ->where('asb_survey_hazard.asb_survey_id', '=', $asbSurveyId)
            ->join(
                'asb_hazard',
                'asb_hazard.asb_hazard_id',
                '=',
                'asb_survey_hazard.asb_hazard_id'
            )
            ->where('asb_hazard.room_id', '=', $roomId)
            ->leftJoin(
                'asb_material_type',
                'asb_material_type.asb_material_type_id',
                '=',
                'asb_hazard.asb_material_type_id'
            )
            ->leftJoin(
                'asb_material_subtype',
                'asb_material_subtype.asb_material_subtype_id',
                '=',
                'asb_hazard.asb_material_subtype_id'
            )
            ->leftJoin(
                'asb_hazard_status',
                'asb_hazard_status.asb_hazard_status_id',
                '=',
                'asb_hazard.asb_hazard_status_id'
            )
            ->leftJoin(
                'asb_asbestos_type',
                'asb_asbestos_type.asb_asbestos_type_id',
                '=',
                'asb_hazard.asb_asbestos_type_id'
            )
            ->leftJoin(
                'unit_of_measure',
                'unit_of_measure.unit_of_measure_id',
                '=',
                'asb_hazard.unit_of_measure_id'
            )
            ->orderBy('asb_hazard.asb_hazard_code', 'ASC');

        return $asbACMs->get();
    }

    private function childHeaderData($asbSurvey)
    {
        if (empty($asbSurvey)) {
            $commonData = [
                'isFirstPageOfBlock' => true,
                'reportPublished' => $this->getDate(),
                'registerExpiryDate' => $this->getDate(['isRegister' => true]),
                'surveyCode' => '',
                'surveyDate' => '',
                'siteCode' => '',
                'siteAddress' => '',
                'surveyType' => '',
                'surveyTypeCode' => '',
                'buildingCode' => '',
                'buildingDesc' => '',
                'buildingColor' => ''
            ];
        } else {
            $commonData = [
                'isFirstPageOfBlock' => true,
                'reportPublished' => $this->getDate(),
                'registerExpiryDate' => $this->getDate(['isRegister' => true]),
                'surveyCode' => $asbSurvey->asb_survey_client_ref,
                'surveyDate' => Common::dateFieldDisplayFormat($asbSurvey->asb_survey_date),
                'siteCode' => $asbSurvey->site_code,
                'siteAddress' => '',
                'surveyType' => $asbSurvey->survey_type_desc,
                'surveyTypeCode' => $asbSurvey->survey_type_code,
                'buildingCode' => $asbSurvey->building_code,
                'buildingDesc' => $asbSurvey->building_desc,
                'buildingColor' => $this->getColorForBuilding($asbSurvey->building_code)
            ];

            $addressObj = Address::find($asbSurvey->address_id);
            if ($addressObj instanceof Address) {
                $commonData['siteAddress'] = $addressObj->getAddressAsString(['isRegion' => true]);
            }
        }

        return $commonData;
    }

    private function filter($query, $inputs)
    {
        $surveyTypeCodeFilter = [
            SurveyConstant::ASB_SURVEY_TYPE_HSG264_MAMAGEMENT_CODE,
            SurveyConstant::ASB_SURVEY_TYPE_HSG264_REFURB_DEMOLITION
        ];

        $query->whereIn('asb_survey_type.survey_type_code', $surveyTypeCodeFilter);

        $query->whereNotNull('asb_survey.building_id');

        $this->filterAsbestosSurvey($query, $inputs);

        return $query;
    }

    private function filterAsbestosSurvey($query, $inputs)
    {

        $this->filter = new \Tfcloud\Lib\Filters\AsbSurveyFilter($inputs);

        if (!is_null($this->filter->asb_survey_id)) {
            $query->where('asb_survey.asb_survey_id', $this->filter->asb_survey_id);
        }

        if (!empty($this->filter->nextReviewFrom)) {
            $nextReviewFrom = DateTime::createFromFormat('d/m/Y', $this->filter->nextReviewFrom);
            if ($nextReviewFrom) {
                $query->where(
                    \DB::raw("DATE_FORMAT(" . "asb_survey.asb_survey_next_review_date, '%Y-%m-%d')"),
                    '>=',
                    $nextReviewFrom->format('Y-m-d')
                );
            }
        }

        if (!empty($this->filter->nextReviewTo)) {
            $nextReviewFrom = DateTime::createFromFormat('d/m/Y', $this->filter->nextReviewTo);
            if ($nextReviewFrom) {
                $query->where(
                    \DB::raw("DATE_FORMAT(" . "asb_survey.asb_survey_next_review_date, '%Y-%m-%d')"),
                    '<=',
                    $nextReviewFrom->format('Y-m-d')
                );
            }
        }

        $status = [];
        if (!empty($this->filter->draft)) {
            array_push($status, $this->filter->draft);
        }
        if (!empty($this->filter->current)) {
            array_push($status, $this->filter->current);
        }
        if (!empty($this->filter->archive)) {
            array_push($status, $this->filter->archive);
        }

        if (!empty($status)) {
            $query->whereIn('asb_survey.survey_status_id', $status);
        }

        if (!empty($this->filter->surveyType)) {
            $query->where('asb_survey.survey_type_id', '=', $this->filter->surveyType);
        }

        if (!empty($this->filter->owner)) {
            $query->where('asb_survey.owner_user_id', '=', $this->filter->owner);
        }

        if (!empty($this->filter->site_id)) {
            $query->where('asb_survey.site_id', '=', $this->filter->site_id);
        }
        if (!empty($this->filter->building_id)) {
            $query->where('asb_survey.building_id', '=', $this->filter->building_id);
        }

        if (!empty($this->filter->search)) {
            $query->where('asb_survey.asb_survey_code', 'like', '%' . $this->filter->search . '%');
        }

        if (!empty($this->filter->property_status)) {
            $query->where('building.active', '=', $this->filter->property_status);
        }

        return $query;
    }

    private function all()
    {
        $select = array(
            'asb_survey.asb_survey_id',
            'asb_survey.asb_survey_code',
            'asb_survey.asb_survey_client_ref',
            'asb_survey.asb_survey_date',
            'asb_survey.site_id',
            'asb_survey.site_group_id',
            'site.site_code',
            'site.address_id',
            'asb_survey_type.survey_type_code',
            'asb_survey_type.survey_type_desc',
            'building.building_code',
            'building.building_desc',
            'gen_userdef_group.user_date1',
            \DB::raw('CASE WHEN asb_survey.building_id is not null
                        THEN building.active ELSE site.active
                        END AS `property_active`')
        );

        $query = AsbSurvey::UserSiteGroup()->select($select)
            ->join(
                'site',
                'site.site_id',
                '=',
                'asb_survey.site_id'
            )
            ->leftJoin(
                'building',
                'asb_survey.building_id',
                '=',
                'building.building_id'
            )
            ->leftJoin(\DB::raw("(SELECT asb_survey_id,
                CASE WHEN asb_survey.building_id is not null
                THEN building.active ELSE site.active END AS 'property_active'
                FROM asb_survey
                    LEFT JOIN site on site.site_id = asb_survey.site_id
                    LEFT JOIN building on building.building_id = asb_survey.building_id
                ) as asb_survey_sub"), function ($join) {
                $join->on("asb_survey_sub.asb_survey_id", "=", "asb_survey.asb_survey_id");
            })
            ->leftJoin(
                'gen_userdef_group',
                'gen_userdef_group.gen_userdef_group_id',
                '=',
                'asb_survey.gen_userdef_group_id'
            )
            ->join(
                'asb_survey_type',
                'asb_survey.survey_type_id',
                '=',
                'asb_survey_type.survey_type_id'
            )
            ->orderBy('site.site_code', 'ASC')
            ->orderBy('building.building_code', 'ASC');

        return $query;
    }

    private function getColorForBuilding($buildingCode)
    {
        $color = 'rgb(255,255,255)';
        $colorArray = [
            '001' => 'rgb(127,255,159)',
            '002' => 'rgb(255,223,127)',
            '003' => 'rgb(127,255,255)',
            '004' => 'rgb(255,159,127)',
            '005' => 'rgb(127,191,255)',
            '006' => 'rgb(191,255,127)',
            '007' => 'rgb(255,127,223)',
            '008' => 'rgb(255,255,127)',
            '009' => 'rgb(255,127,159)',
            '010' => 'rgb(127,127,0)',
            '011' => 'rgb(127,95,63)',
            '012' => 'rgb(159,127,255)',
            '013' => 'rgb(63,63,127)',
            '014' => 'rgb(0,127,31)',
            '015' => 'rgb(63,255,0)',
            '016' => 'rgb(255,127,0)',
            '017' => 'rgb(31,127,0)'
        ];

        return isset($colorArray[$buildingCode]) ? $colorArray[$buildingCode] : $color;
    }

    private function getView($data, $templateName)
    {
        return \View::make('reports.wkhtmltopdf.asbestos.fife.' . $templateName, $data)->render();
    }

    private function getDate($option = [], $format = 'd/m/Y')
    {
        $date = new \DateTime();
        if (array_get($option, 'isRegister', '')) {
            $date->add(new \DateInterval('P1Y'));
        }

        return $date->format($format);
    }

    protected function setPagingCustom(&$page, $key, $totalPage, $x, $y, $customY2 = null)
    {
        if (($key == 0) || ($key == ($totalPage - 1))) {
            $page->drawText("Page " . ($key + 1) . " of $totalPage", $x, $y, 'UTF-8');
        } else {
            $page->drawText("Page " . ($key + 1) . " of $totalPage", $x, $customY2, 'UTF-8');
        }
    }

    private function generateFilterData($filterData, &$whereCodes, &$whereTexts, &$orCodes)
    {
        if ($val = Common::iset($filterData['nextReviewFrom'])) {
            array_push($whereTexts, "Asbestos Survey Review From contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['nextReviewTo'])) {
            array_push($whereTexts, "Asbestos Survey Review To contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['surveyType'])) {
            array_push(
                $whereCodes,
                ['asb_survey_type', 'survey_type_id', 'survey_type_code', $val, 'Survey Type']
            );
        }

        if ($val = Common::iset($filterData['search'])) {
            array_push($whereTexts, "Asbestos Survey Filter on Code contains '" . $val . "'");
        }

        //show filter status
        $surveyStatus = [];
        $val = array_get($filterData, 'draft', null);
        if ($val == AsbSurveyStatus::DRAFT) {
            $surveyStatus[] = 'DRAFT';
        }

        $val = array_get($filterData, 'current', null);
        if ($val == AsbSurveyStatus::CURRENT) {
            $surveyStatus[] = 'CURRENT';
        }

        $val = array_get($filterData, 'archive', null);
        if ($val == AsbSurveyStatus::ARCHIVE) {
            $surveyStatus[] = 'ARCHIVE';
        }

        $val = array_get($filterData, 'qa', null);
        if ($val == AsbSurveyStatus::QA) {
            $surveyStatus[] = 'Q/A';
        }

        if (! empty($surveyStatus)) {
            array_push($whereTexts, "Asbestos Survey status = '" . implode(', ', $surveyStatus) . "'");
        }

        if ($val = Common::iset($filterData['owner'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, \Lang::get('text.owner')]);
        }

        if ($siteId = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $siteId, "Site Code"]);

            if ($buildingId = Common::iset($filterData['building_id'])) {
                array_push(
                    $whereCodes,
                    ['building', 'building_id', 'building_code', $buildingId, "Building Code"]
                );
            }
        }

        if ($val = Common::iset($filterData['property_status'])) {
            $text = "Property Status is ";
            if ($val == 'Y') {
                $text .= 'Active';
            } elseif ($val == 'N') {
                $text .= 'Inactive';
            }
            array_push($whereTexts, $text);
        }
    }
}
