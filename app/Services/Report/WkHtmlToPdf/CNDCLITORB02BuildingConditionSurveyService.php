<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\ReportConstant;
use Tfcloud\Lib\Filters\ConditionFilter;
use Tfcloud\Lib\Math;
use Tfcloud\Models\CondLocationScore;
use Tfcloud\Models\CondLocationType;
use Tfcloud\Models\Condsurvey;
use Tfcloud\Models\CondsurveyStatus;
use Tfcloud\Models\Idwork;
use Tfcloud\Models\IdworkElement;
use Tfcloud\Models\IdworkStatusType;
use Tfcloud\Models\Report;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\ImageService;

class CNDCLITORB02BuildingConditionSurveyService extends WkHtmlToPdfReportBaseService
{
    public $filterText = '';
    public const PRIORITY_1 = 1;
    public const PRIORITY_2 = 2;
    public const PRIORITY_3 = 3;
    public const PRIORITY_4 = 4;
    private $reportBoxFilterLoader = '';

    public function __construct(PermissionService $permissionService, Report $report = null)
    {
        parent::__construct($permissionService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function validateFilter($inputs)
    {
        $this->formatInputData($inputs);
        return \Validator::make($inputs, $this->validateRules(), $this->validateMessages());
    }

    public function validateRules()
    {
        $rules = [
            'site_id' => ['required'],
        ];

        return $rules;
    }

    public function validateMessages()
    {
        $messages = [
            'site_id.required' => 'Location is required.'
        ];

        return $messages;
    }

    public function runReport($repPdfFile, $inputs, $repId, &$errMsg = [])
    {
        $this->formatInputData($inputs);
        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);
        $arrPdfs = [];
        $siteGroupId = \Auth::user()->site_group_id;
        if ($condsurveyId = array_get($inputs, 'condsurvey_id')) {
            $conditionObj = Condsurvey::find($condsurveyId);
            if ($conditionObj instanceof Condsurvey) {
                $reportFileName = ReportConstant::SYSTEM_REPORT_CND_CLI_TORB02 . '_' . $conditionObj->condsurvey_code;
                $repPdfFile = $this->generateReportOutPutPath($reportFileName);
            } else {
                $errMsg[] = 'No condition survey found';
                return false;
            }
        }
        $conditionList = $this->getAll($inputs);
        if (!$conditionList->count()) {
            $errMsg[] = 'No condition survey found';
            if (!array_get($inputs, 'condsurvey_id')) {
                $this->mergerPdf($arrPdfs, $repPdfFile, ['isPaging' => false]);
                return true;
            } else {
                return false;
            }
        }

        foreach ($conditionList as $condition) {
            $photoName = $condition->building_photo_name;
            if ($photoName) {
                //get the image
                $buildingPhotoPath = $this->getPhotoStoragePathway(
                    $siteGroupId,
                    $condition->condsurvey_building_id,
                    'building'
                );
                //pathway checking in wrong folder, redirect
                $splitString = explode("building/", $buildingPhotoPath);
                $firstHalf = $splitString[0];
                $buildingPhotoPath = $firstHalf . "building/" . $condition->condsurvey_building_id . "/" . $photoName;
                $condition->building_photo = $buildingPhotoPath;
            } else {
                $condition->building_photo = null;
            }
            $content = \View::make(
                'reports.wkhtmltopdf.condition.cndCliTorb02.content',
                [
                    'condition' => $condition
                ]
            )->render();
            $contentPath = $generatedPath . "/content.html";
            file_put_contents($contentPath, $content, LOCK_EX);
            $firstSectionsContentPdf = $generatedPath . "/firstSection_{$condition->getKey()}.pdf";


            $options = [
                "margin-bottom" => 0,
                "margin-left" => 0,
                "margin-right" => 0,
                "margin-top" => 0,
                'orientation' => 'portrait',
            ];

            $this->generateSingleHtmlToPdf($contentPath, $firstSectionsContentPdf, $options);
            array_push($arrPdfs, $firstSectionsContentPdf);

            $secondSectionPdf = $this->generateStaticPages($condition, $generatedPath);

            array_push($arrPdfs, $secondSectionPdf);

            $appendixAPdf = $this->generateAppendixA($condition, $generatedPath);

            array_push($arrPdfs, $appendixAPdf);

            $appendixAPdfPt2 = $this->generateAppendixAPt2($condition, $generatedPath);

            array_push($arrPdfs, $appendixAPdfPt2);


            $appendixBPdf = $this->generateAppendixB($condition, $generatedPath);

            array_push($arrPdfs, $appendixBPdf);
        }
        $this->mergerPdf($arrPdfs, $repPdfFile, ['isPaging' => false]);



        $whereCodes = [];               // The codes which have been used in the filter
        $whereTexts = [];               // Plain text about filtering applied
        $orCodes = [];                  // csv string of OR codes
        $bFilterDetail = false;

        $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
        $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $this->filterText);

        if (!array_get($inputs, 'condsurvey_id')) {
            return true;
        } else {
            return $repPdfFile;
        }
    }

    private function generateStaticPages($condition, $generatedPath)
    {
        $header = \View::make('reports.wkhtmltopdf.condition.cndCliTorb02.header')->render();
        $headerPath = $generatedPath . "/header.html";
        file_put_contents($headerPath, $header, LOCK_EX);

        $footer = \View::make('reports.wkhtmltopdf.condition.cndCliTorb02.footer')->render();
        $footerPath = $generatedPath . "/footer.html";
        file_put_contents($footerPath, $footer, LOCK_EX);

        $content = \View::make(
            'reports.wkhtmltopdf.condition.cndCliTorb02.static_contents',
            [
                'condition' => $condition
            ]
        )->render();
        $contentPath = $generatedPath . "/content.html";
        file_put_contents($contentPath, $content, LOCK_EX);
        $secondSectionsContentPdf = $generatedPath . "/secondSection_{$condition->getKey()}.pdf";
        $xslStyleSheetPath = base_path() . '/resources/views/reports/wkhtmltopdf/condition/cndCliTorb02/toc.xsl';

        $options = [
            'header-html' => $headerPath,
            'footer-html' => $footerPath,
            'header-spacing' => 4,
            'footer-spacing' => 4,
            'orientation' => 'portrait',
            'toc' => true,
            'xsl-style-sheet' => $xslStyleSheetPath,
        ];

        $this->generateSingleHtmlToPdf($contentPath, $secondSectionsContentPdf, $options);
        return $secondSectionsContentPdf;
    }

    private function generateAppendixA($condition, $generatedPath)
    {
        $elements = IdworkElement::userSiteGroup()->orderBy('idwork_element_code')->get();
        $elementSummary = $this->formatElementSumData($condition->condsurvey_id, $condition->condsurvey_building_id);
        $conditionScoreData = $this->getElementScore($condition->condsurvey_id, $condition->condsurvey_building_id)
            ->groupBy('idwork_element_id');
        $totalP1 = $elementSummary->sum('p1_total_cost_sum');
        $totalP2 = $elementSummary->sum('p2_total_cost_sum');
        $totalP3 = $elementSummary->sum('p3_total_cost_sum');
        $totalP4 = $elementSummary->sum('p4_total_cost_sum');
        $totalAll = Math::add(2, [$totalP1, $totalP2, $totalP3, $totalP4]);
        $content = \View::make(
            'reports.wkhtmltopdf.condition.cndCliTorb02.appendix_A',
            [
                'condition' => $condition,
                'elementSummary' => $elementSummary->toArray(),
                'conditionScoreData' => $conditionScoreData->toArray(),
                'elements' => $elements,
                'totalP1' => $totalP1,
                'totalP2' => $totalP2,
                'totalP3' => $totalP3,
                'totalP4' => $totalP4,
                'totalAll' => $totalAll
            ]
        )->render();
        $contentPath = $generatedPath . "/appendix_A.html";
        file_put_contents($contentPath, $content, LOCK_EX);
        $appendixAPdf = $generatedPath . "/appendix_A_{$condition->getKey()}.pdf";

        $options = [
            'header-spacing' => 4,
            'footer-spacing' => 4,
            'orientation' => 'landscape',
        ];

        $this->generateSingleHtmlToPdf($contentPath, $appendixAPdf, $options);
        return $appendixAPdf;
    }

    private function generateAppendixAPt2($condition, $generatedPath)
    {
        $elementAllData = $this->getIdWorks($condition->condsurvey_id)
            ->groupBy(['idwork_element_desc', 'idwork_subelement_desc'])->toArray();

        $content = \View::make(
            'reports.wkhtmltopdf.condition.cndCliTorb02.appendix_A_pt_2',
            [
                'elementAllData' => $elementAllData
            ]
        )->render();
        $contentPath = $generatedPath . "/appendix_A_pt_2.html";
        file_put_contents($contentPath, $content, LOCK_EX);
        $appendixAPdf = $generatedPath . "/appendix_A_pt_2_{$condition->getKey()}.pdf";

        $options = [
            'header-spacing' => 4,
            'footer-spacing' => 4,
            'orientation' => 'landscape',
        ];

        $this->generateSingleHtmlToPdf($contentPath, $appendixAPdf, $options);
        return $appendixAPdf;
    }



    private function formatElementSumData($conId, $buildingId)
    {
        $elementSummary = $this->getElementData($conId, $buildingId);
        $outputData = [];
        foreach ($elementSummary as $item) {
            $outputData[$item->element_id] = $item;
        }

        return collect($outputData);
    }

    private function generateAppendixB($condition, $generatedPath)
    {
        $condId = $condition->getKey();
        $imageService = new ImageService($this->permissionService);

        $idWorkPhotoPath = '';

        $idWorks = $this->getIdWorkWithPhoto($condId);
        $siteGroupId = \Auth::user()->site_group_id;
        foreach ($idWorks as $idWork) {
            $photoName = $idWork->photo;
            //get the image
            $idWorkPhotoPath = $this->getPhotoStoragePathway(
                $siteGroupId,
                $idWork->getKey(),
                'idwork'
            );
            //pathway checking in wrong folder, redirect
            $splitString = explode("idwork/", $idWorkPhotoPath);
            $firstHalf = $splitString[0];
            $idWorkPhotoPath = $firstHalf . "idwork/" . $idWork->getKey() . "/" . $photoName;
            $idWorkPhotoListPath = $firstHalf . "idwork/" . $idWork->getKey() . "/images/";
            $idWork->photo_full_path = $idWorkPhotoPath;

            $storagePathway = $idWork->getImagesStoragePathway();
            $images = $imageService->findImages($storagePathway);

            foreach ($images as $key => $image) {
                $images[$key] = $idWorkPhotoListPath . $image;
            }

            $idWork->images = $images;

            $routeParams = [
                $idWork->condsurvey_id,
                $idWork->getKey()
            ];
        }
        $content = \View::make(
            'reports.wkhtmltopdf.condition.cndCliTorb02.appendix_B',
            [
                'routeParams' => $idWorkPhotoPath,
                'condId'      => $condId,
                'idWorks'     => $idWorks
            ]
        )->render();
        $contentPath = $generatedPath . "/content.html";
        file_put_contents($contentPath, $content, LOCK_EX);
        $appendixBPdf = $generatedPath . "/appendix_B_{$condId}.pdf";

        $options = [
            'header-spacing' => 4,
            'footer-spacing' => 4,
            'orientation' => 'landscape',
        ];

        $this->generateSingleHtmlToPdf($contentPath, $appendixBPdf, $options);
        return $appendixBPdf;
    }

    private function getIdWorkWithPhoto($condId)
    {
        $query = Idwork::userSiteGroup()
            ->where('condsurvey_id', $condId)
            ->whereNotNull('photo')
            ->select([
                'idwork.idwork_id',
                'idwork.idwork_code',
                'idwork.defect',
                'idwork.remedy',
                'idwork.photo'
            ]);

        return $query->get();
    }

    private function getIdWorks($condId)
    {
        $query = Idwork::userSiteGroup()
            ->leftJoin('idwork_priority', 'idwork_priority.idwork_priority_id', '=', 'idwork.idwork_priority_id')
            ->leftJoin('idwork_element', 'idwork.element_id', '=', 'idwork_element.idwork_element_id')
            ->leftJoin('idwork_subelement', 'idwork.sub_element_id', '=', 'idwork_subelement.idwork_subelement_id')
            ->join('idwork_status', 'idwork_status.idwork_status_id', '=', 'idwork.idwork_status_id')
            ->leftJoin('room', 'room.room_id', '=', 'idwork.room_id')
            ->leftJoin('room_floor', 'room_floor.room_floor_id', '=', 'room.room_floor_id')
            ->leftJoin(
                'idwork_condition',
                'idwork_condition.idwork_condition_id',
                '=',
                'idwork.idwork_condition_id'
            )
            ->where('idwork.condsurvey_id', $condId)
            ->where('idwork_status.idwork_status_type_id', IdworkStatusType::DRAFT)
            ->select([
                'idwork.idwork_code',
                'idwork.defect',
                'idwork.location',
                'idwork.remedy',
                'idwork.total_cost',
                'idwork_condition.idwork_condition_code',
                'room.room_number',
                'room_floor.room_floor_code',
                'idwork_priority.idwork_priority_code',
                'idwork_element.idwork_element_desc',
                'idwork_subelement.idwork_subelement_desc'
            ])
            ->orderBy('idwork_element.idwork_element_code')
            ->orderBy('idwork_subelement.idwork_subelement_code');

        return $query->get();
    }

    private function getElementData($conditionId, $buildingId)
    {
        $query = Idwork::userSiteGroup()
            ->leftJoin('idwork_priority', 'idwork_priority.idwork_priority_id', '=', 'idwork.idwork_priority_id')
            ->join('idwork_status', 'idwork_status.idwork_status_id', '=', 'idwork.idwork_status_id')
            ->where('idwork.condsurvey_id', $conditionId)
            ->where('idwork.building_id', $buildingId)
            ->where('idwork_status.idwork_status_type_id', IdworkStatusType::DRAFT)
            ->select([
                'idwork.element_id',
                \DB::raw("IFNULL(SUM(CASE WHEN idwork_priority.idwork_priority_code = "
                    . self::PRIORITY_1 . " THEN idwork.total_cost END), 0) as p1_total_cost_sum"),
                \DB::raw("IFNULL(SUM(CASE WHEN idwork_priority.idwork_priority_code = "
                    . self::PRIORITY_2 . " THEN idwork.total_cost END), 0) as p2_total_cost_sum"),
                \DB::raw("IFNULL(SUM(CASE WHEN idwork_priority.idwork_priority_code = "
                    . self::PRIORITY_3 . " THEN idwork.total_cost END), 0) as p3_total_cost_sum"),
                \DB::raw("IFNULL(SUM(CASE WHEN idwork_priority.idwork_priority_code = "
                    . self::PRIORITY_4 . " THEN idwork.total_cost END), 0) as p4_total_cost_sum"),
            ])
            ->groupBy('idwork.element_id');

        return $query->get();
    }

    private function getElementScore($conId, $buildingId)
    {
        $query = CondLocationScore::leftJoin(
            'cond_location_element_score',
            'cond_location_element_score.cond_location_score_id',
            '=',
            'cond_location_score.cond_location_score_id'
        )
            ->leftJoin(
                'score',
                'score.score_id',
                '=',
                'cond_location_element_score.score_id'
            )
            ->where('cond_location_score.condsurvey_id', $conId)
            ->where('cond_location_score.location_record_id', $buildingId)
            ->where('cond_location_score.cond_location_type_id', CondLocationType::TYPE_BUILDING)
            ->select([
                'score.score_code',
                'cond_location_element_score.idwork_element_id'
            ]);

        return $query->get();
    }

    private function filterAll($query, $inputs, $view = false)
    {
        $condsurveyTable = !$view ? 'condsurvey' : $view;

        $filter = new ConditionFilter($inputs);

        if (!is_null($filter->condsurvey_id)) {
            $query->where("{$condsurveyTable}.condsurvey_id", $filter->condsurvey_id);
        }

        if (!is_null($filter->site_id)) {
            $query->where("{$condsurveyTable}.site_id", $filter->site_id);

            if (!is_null($filter->building_id)) {
                $query->where("{$condsurveyTable}.condsurvey_building_id", $filter->building_id);
            }
        }

        if (!is_null($filter->code)) {
            $query->where("{$condsurveyTable}.condsurvey_code", 'like', "%{$filter->code}%");
        }

        if (!is_null($filter->reference)) {
            $query->where("{$condsurveyTable}.reference", 'like', "%{$filter->reference}%");
        }
        if (!is_null($filter->establishment)) {
            $query->where('site.establishment_id', $filter->establishment);
        }

        if (!is_null($filter->surveyor)) {
            $query->where("{$condsurveyTable}.surveyor", $filter->surveyor);
        }
        return $query;
    }

    private function formatInputData(&$inputs)
    {
        foreach ($inputs as $key => $value) {
            if ($value === "") {
                unset($inputs[$key]);
            }
        }
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $siteValue = $reportFilterQuery->getValueFilterField('site_id');
            $buildingValue = $reportFilterQuery->getValueFilterField('building_id');
            $establishmentValue = $reportFilterQuery->getValueFilterField('establishment_id');
            $referenceValue = $reportFilterQuery->getValueFilterField('reference');
            $surveyorValue = $reportFilterQuery->getValueFilterField('surveyor');
            $codeValue = $reportFilterQuery->getValueFilterField('condsurvey_code');

            $inputs['establishment'] = empty($establishmentValue) ? null : $establishmentValue[0];
            $inputs['site_id'] = empty($siteValue) ? null : $siteValue[0];
            $inputs['building_id'] = empty($buildingValue) ? null : $buildingValue[0];
            $inputs['code'] = empty($codeValue) ? null : $codeValue[0];
            $inputs['surveyor'] = empty($surveyorValue) ? null : $surveyorValue[0];
            $inputs['reference'] = empty($referenceValue) ? null : $referenceValue[0];
        }
    }

    private function all()
    {
        $query = Condsurvey::select(
            [
                'condsurvey.condsurvey_id',
                'condsurvey.condsurvey_code',
                'condsurvey.condsurvey_desc',
                'condsurvey.survey_date',
                'condsurvey.site_id',
                'condsurvey.condsurvey_building_id',
                'site.site_code',
                'site.site_desc',
                'building.building_code',
                'building.building_desc',
                \DB::raw('building.photo AS building_photo_name'),
                'address.second_addr_obj',
                'address.first_addr_obj',
                'address_street.street',
                'address_street.locality',
                'address_street.town',
                'address_street.region',
                'address.postcode',
                \DB::raw('contact.contact_name AS surveyor_contact_name'),
                \DB::raw('udf_contact1.contact_name AS udf_contact1_name'),
                \DB::raw('udf_contact2.contact_name AS udf_contact2_name'),
                'gen_userdef_group.user_text1'
            ]
        )
            ->join(
                'condsurvey_status',
                "condsurvey.condsurvey_status_id",
                '=',
                "condsurvey_status.condsurvey_status_id"
            )
            ->join('site', 'site.site_id', '=', "condsurvey.site_id")
            ->leftjoin('score', "condsurvey.score_id", '=', 'score.score_id')
            ->leftjoin('building', "building.building_id", '=', 'condsurvey.condsurvey_building_id')
            ->leftjoin('address', 'address.address_id', '=', 'site.address_id')
            ->leftjoin('address_street', 'address_street.address_street_id', '=', 'address.address_street_id')
            ->leftjoin('contact', 'contact.contact_id', '=', "condsurvey.surveyor")
            ->leftJoin(
                'gen_userdef_group',
                'condsurvey.gen_userdef_group_id',
                '=',
                'gen_userdef_group.gen_userdef_group_id'
            )
            ->leftJoin('contact AS udf_contact2', 'udf_contact2.contact_id', '=', 'gen_userdef_group.user_contact2')
            ->leftJoin('contact AS udf_contact1', 'udf_contact1.contact_id', '=', 'gen_userdef_group.user_contact1')
            ->where("condsurvey.site_group_id", \Auth::user()->site_group_id)
            ->whereNotNull('condsurvey.condsurvey_building_id')
            ->whereNull('condsurvey.condsurvey_building_block_id')
            ->where('condsurvey.condsurvey_status_id', CondsurveyStatus::COMPLETE)
            ->orderBy('condsurvey.survey_date', 'DESC')
            ->groupBy('condsurvey.condsurvey_building_id');

        $query = $this->permissionService->listViewSiteAccess($query, 'site.site_id');

        return $query;
    }

    private function getAll($inputs)
    {
        $conditionList = $this->filterAll($this->all(), $inputs)->get();

        return $conditionList;
    }

    private function generateFilterData($filterData, &$whereCodes, &$whereTexts, &$orCodes)
    {
        if ($val = array_get($filterData, 'reference')) {
            array_push($whereTexts, "Reference contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Code contains '{$filterData['code']}'");
        }

        if ($siteId = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $siteId, "Site Code"]);

            if ($buildingId = Common::iset($filterData['building_id'])) {
                array_push(
                    $whereCodes,
                    ['building', 'building_id', 'building_code', $buildingId, "Building Code"]
                );
            }
        }

        if ($val = Common::iset($filterData['surveyor'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Surveyor']);
        }

        if ($val = array_get($filterData, 'establishment')) {
            array_push(
                $whereCodes,
                ['establishment', 'establishment_id', 'establishment_code', $val, 'Establishment']
            );
        }
    }
}
