<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use Carbon\Carbon;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Models\Report;
use Tfcloud\Services\PermissionService;

class PRCLITUN01PropertyAssetReportService extends WkHtmlToPdfReportBaseService
{
    public $filterText = '';
    private $filterQuery = null;

    public function __construct(PermissionService $permissionService, Report $report = null, $inputs = [])
    {
        parent::__construct($permissionService);
        $this->filterQuery = (new ClassMapReportLoader($report))->getFilterQuery($inputs);
    }

    public function runReport($repOutPutPdfFile, $repId, $options = [])
    {
        $assetCategories = $this->filterQuery->getAll();

        $this->filterText = $this->filterQuery->getFilterDetailText();

        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);
        $headerLogo = $this->getHeaderLogo();
        $title = 'Tunbridge Wells Borough Council Property Asset Report';

        $content = \View::make('reports.wkhtmltopdf.property.prclitun01.content', [
            'assetCats' => $assetCategories
        ])->render();
        $contentFile = $generatedPath . '/content.html';
        file_put_contents($contentFile, $content, LOCK_EX);

        $header = \View::make('reports.wkhtmltopdf.property.prclitun01.header', [
            'logo' => $headerLogo,
            'title' => $title
        ])->render();
        $headerPath = $generatedPath . "/header.html";
        file_put_contents($headerPath, $header, LOCK_EX);

        $user = \Auth::user()->display_name;
        $now = Carbon::now()->format('d/m/Y H:i:s');
        $footer = \View::make('reports.wkhtmltopdf.property.prclitun01.footer', [
            'footerTitle' => 'PR-CLI-TUN01 - Property Asset Report',
            'info' => "Created by $user @ $now"
        ])->render();

        $footerPath = $generatedPath . "/footer.html";
        file_put_contents($footerPath, $footer, LOCK_EX);

        $this->generateSingleHtmlToPdf(
            $contentFile,
            $repOutPutPdfFile,
            [
                'header-html' => $headerPath,
                'footer-html' => $footerPath,
                'header-spacing' => 25,
                'footer-spacing' => 10,
                'orientation' => 'landscape',
            ]
        );

        return true;
    }
}
