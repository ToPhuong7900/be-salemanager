<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use Carbon\Carbon;
use Tfcloud\Lib\Common;
use Tfcloud\Models\Building;
use Tfcloud\Models\PropExternal;
use Tfcloud\Models\Report;
use Tfcloud\Models\Site;
use Tfcloud\Services\PermissionService;

class PRCLITUN02PropertyAssetReportService extends WkHtmlToPdfReportBaseService
{
    public $filterText = '';

    public function __construct(PermissionService $permissionService, Report $report = null, $inputs = [])
    {
        parent::__construct($permissionService);
    }

    public function runReport($repOutPutPdfFile, $repId, $options = [])
    {
        $assetCatTypes = $this->getAll();

        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);
        $headerLogo = $this->getHeaderLogo();
        $title = 'Tunbridge Wells Borough Council';
        $subtitle = 'Asset Register - ';

        $content = \View::make('reports.wkhtmltopdf.property.prclitun02.content', [
            'assetCatTypes' => $assetCatTypes,
            'subtitle' => $subtitle,
        ])->render();
        $contentFile = $generatedPath . '/content.html';
        file_put_contents($contentFile, $content, LOCK_EX);

        $header = \View::make('reports.wkhtmltopdf.property.prclitun02.header', [
            'logo' => $headerLogo,
            'title' => $title
        ])->render();
        $headerPath = $generatedPath . "/header.html";
        file_put_contents($headerPath, $header, LOCK_EX);

        $now = Carbon::now()->format('d/m/Y');
        $footer = \View::make('reports.wkhtmltopdf.property.prclitun02.footer', [
            'generated' => "Report Generated: $now"
        ])->render();

        $footerPath = $generatedPath . "/footer.html";
        file_put_contents($footerPath, $footer, LOCK_EX);

        $this->generateSingleHtmlToPdf(
            $contentFile,
            $repOutPutPdfFile,
            [
                'header-html' => $headerPath,
                'footer-html' => $footerPath,
                'header-spacing' => 20,
                'footer-spacing' => 10,
                'orientation' => 'landscape',
            ]
        );

        return true;
    }

    private function getAll()
    {
        $records = $this->getQuery()->get();
        return $this->gotAll($records);
    }

    private function getQuery()
    {
        $query = $this->siteQuery();

        $query->union($this->buildingQuery());
        $query->union($this->assetQuery());

        $query->orderByRaw('ISNULL(prop_facility_asset_category_id)')
            ->orderBy('prop_facility_asset_category_type_desc')
            ->orderBy('prop_facility_asset_category_desc')
            ->orderBy('code');

        return $query;
    }

    private function assetQuery()
    {
        $addressQuery = $this->addressQuery();

        $queryAsset = Site::userSiteGroup()
            ->select([
                'site.prop_facility_asset_category_id',
                'prop_facility_asset_category.prop_facility_asset_category_desc',
                'prop_facility_asset_category_type.prop_facility_asset_category_type_id',
                'prop_facility_asset_category_type.prop_facility_asset_category_type_desc',
                'address.address as address',
                "address.address as site_address",
                \DB::Raw('prop_external_code AS code'),
                \DB::Raw("CONCAT(prop_tenure.prop_tenure_code, ' - ' , prop_tenure.prop_tenure_desc) AS tenure"),
                \DB::Raw('prop_external_desc as description'),
                'site.uprn',
                'gen_gis.easting',
                'gen_gis.northing'
            ])
            ->leftjoin(
                'prop_external',
                'prop_external.site_id',
                '=',
                'site.site_id'
            )
            ->leftjoin(
                'prop_facility_asset_category',
                'prop_facility_asset_category.prop_facility_asset_category_id',
                '=',
                'site.prop_facility_asset_category_id'
            )
            ->leftjoin(
                'prop_facility_asset_category_type',
                'prop_facility_asset_category_type.prop_facility_asset_category_type_id',
                '=',
                'prop_facility_asset_category.prop_facility_asset_category_type_id',
            )
            ->leftjoin(
                'gen_gis',
                'prop_external.gen_gis_id',
                '=',
                'gen_gis.gen_gis_id'
            )
            ->leftjoin(
                'prop_tenure',
                'site.prop_tenure_id',
                '=',
                'prop_tenure.prop_tenure_id'
            )
            ->leftjoin(
                $addressQuery,
                'site.address_id',
                '=',
                'address.address_id'
            )
            ->where('prop_external.active', 'Y')
            ->where(function ($query) {
                $query->where('prop_facility_asset_category.active', 'Y')
                    ->orWhereNull('prop_facility_asset_category.active');
            });

        $queryAsset = $this->permissionService->listViewSiteAccess($queryAsset, "site.site_id");

        return $queryAsset;
    }

    private function buildingQuery()
    {
        $addressQuery = $this->addressQuery();
        $addressSiteQuery = $this->addressQuery('site_address');

        $queryBuilding = Building::userSiteGroup()
            ->select([
                'building.prop_facility_asset_category_id',
                'prop_facility_asset_category.prop_facility_asset_category_desc',
                'prop_facility_asset_category_type.prop_facility_asset_category_type_id',
                'prop_facility_asset_category_type.prop_facility_asset_category_type_desc',
                'address.address as address',
                'site_address.address as site_address',
                \DB::Raw('building_code as code'),
                \DB::Raw("CONCAT(prop_tenure.prop_tenure_code, ' - ' , prop_tenure.prop_tenure_desc) AS tenure"),
                \DB::Raw('building_desc as description'),
                'building.uprn',
                'gen_gis.easting',
                'gen_gis.northing'
            ])
            ->join(
                'site',
                'building.site_id',
                '=',
                'site.site_id'
            )
            ->leftjoin(
                'prop_facility_asset_category',
                'prop_facility_asset_category.prop_facility_asset_category_id',
                '=',
                'building.prop_facility_asset_category_id'
            )
            ->leftjoin(
                'prop_facility_asset_category_type',
                'prop_facility_asset_category_type.prop_facility_asset_category_type_id',
                '=',
                'prop_facility_asset_category.prop_facility_asset_category_type_id',
            )
            ->leftjoin(
                'gen_gis',
                'building.gen_gis_id',
                '=',
                'gen_gis.gen_gis_id'
            )
            ->leftjoin(
                'prop_tenure',
                'site.prop_tenure_id',
                '=',
                'prop_tenure.prop_tenure_id'
            )
            ->leftjoin(
                $addressQuery,
                'building.address_id',
                '=',
                'address.address_id'
            )
            ->leftjoin(
                $addressSiteQuery,
                'site.address_id',
                '=',
                'site_address.address_id'
            )
            ->where('building.active', 'Y')
            ->where(function ($queryBuilding) {
                $queryBuilding->where('prop_facility_asset_category.active', 'Y')
                    ->orWhereNull('prop_facility_asset_category.active');
            });

        $queryBuilding = $this->permissionService->listViewSiteAccess($queryBuilding, "site.site_id");

        return $queryBuilding;
    }

    private function siteQuery()
    {
        $addressQuery = $this->addressQuery();

        $query = Site::userSiteGroup()
            ->select([
                'site.prop_facility_asset_category_id',
                'prop_facility_asset_category.prop_facility_asset_category_desc',
                'prop_facility_asset_category_type.prop_facility_asset_category_type_id',
                'prop_facility_asset_category_type.prop_facility_asset_category_type_desc',
                'address.address as address',
                "address.address as site_address",
                \DB::Raw('site_code AS code'),
                \DB::Raw("CONCAT(prop_tenure.prop_tenure_code, ' - ' , prop_tenure.prop_tenure_desc) AS tenure"),
                \DB::Raw('site_desc as description'),
                'site.uprn',
                'gen_gis.easting',
                'gen_gis.northing'
            ])
            ->leftjoin(
                'prop_facility_asset_category',
                'prop_facility_asset_category.prop_facility_asset_category_id',
                '=',
                'site.prop_facility_asset_category_id'
            )
            ->leftjoin(
                'prop_facility_asset_category_type',
                'prop_facility_asset_category_type.prop_facility_asset_category_type_id',
                '=',
                'prop_facility_asset_category.prop_facility_asset_category_type_id',
            )
            ->leftjoin(
                'gen_gis',
                'site.gen_gis_id',
                '=',
                'gen_gis.gen_gis_id'
            )
            ->leftjoin(
                'prop_tenure',
                'site.prop_tenure_id',
                '=',
                'prop_tenure.prop_tenure_id'
            )
            ->leftjoin(
                $addressQuery,
                'site.address_id',
                '=',
                'address.address_id'
            )
            ->where('site.active', 'Y')
            ->where(function ($query) {
                $query->where('prop_facility_asset_category.active', 'Y')
                    ->orWhereNull('prop_facility_asset_category.active');
            });

        $query = $this->permissionService->listViewSiteAccess($query, "site.site_id");

        return $query;
    }

    private function addressQuery($alias = 'address')
    {
        return \DB::Raw(""
            . "("
            . " SELECT TTT.address_id, TRIM(IF(LEFT(TTT.Address, 2) = ', ', SUBSTR(TTT.Address, 2), TTT.Address))"
            . " AS Address"
            . " FROM ("
            . " SELECT TT.address_id, REPLACE(REPLACE(TT.AddressFull, ', NULL', ''), 'NULL', '') AS Address"
            . " FROM ("
            . " SELECT T.address_id,"
            . " CONCAT(T.address1, ', ', T.address2, ', ', T.address3, ', ',"
            . " T.address4, ', ', T.address5, ', ', T.address6) AS AddressFull"
            . " FROM ("
            . " SELECT	address.address_id,"
            . " IF (COALESCE(address.second_addr_obj, '') = '', 'NULL', address.second_addr_obj) AS address1,"
            . " IF (COALESCE(address.first_addr_obj, '') = '', 'NULL', address.first_addr_obj) AS address2,"
            . " IF (COALESCE(address_street.street, '') = '', 'NULL', address_street.street) AS address3,"
            . " IF (COALESCE(address_street.town, '') = '', 'NULL', address_street.town) AS address4,"
            . " IF (COALESCE(address.postcode, '') = '', 'NULL', address.postcode) AS address5,"
            . " IF (COALESCE(address.country, '') = '', 'NULL', address.country) AS address6"
            . " FROM address"
            . " LEFT JOIN address_street ON address.address_street_id = address_street.address_street_id"
            . " ) T"
            . " ) TT"
            . " ) TTT"
            . " ) " . $alias);
    }

    private function gotAll($records)
    {
        $assetCatTypes = [];

        //Group by asset category types
        foreach ($records as $site) {
            $site->asset_grouping = $site->prop_facility_asset_category_desc ?? 'None';

            if ($site->address == '') {
                $site->address = $site->site_address;
            }
            $assetCatTypeId = $site->prop_facility_asset_category_type_id;
            $assetCatTypes[$assetCatTypeId]['desc'] = $site->prop_facility_asset_category_type_desc ?? 'None';
            $assetCatTypes[$assetCatTypeId]['sites'][] = $site;
        }

        return $assetCatTypes;
    }
}
