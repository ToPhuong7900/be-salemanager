<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\ReportConstant;
use Tfcloud\Lib\Filters\Asbestos\AsbHazardFilter;
use Tfcloud\Lib\Filters\AsbSurveyFilter;
use Tfcloud\Lib\Reporting\ReportCommon;
use Tfcloud\Models\AsbAction;
use Tfcloud\Models\AsbHazard;
use Tfcloud\Models\AsbHazardStatusType;
use Tfcloud\Models\AsbRiskAssessment;
use Tfcloud\Models\AsbRiskStatus;
use Tfcloud\Models\AsbRoom;
use Tfcloud\Models\AsbRoomStatus;
use Tfcloud\Models\AsbSample;
use Tfcloud\Models\AsbSampleRequirement;
use Tfcloud\Models\AsbSamplevarType;
use Tfcloud\Models\AsbSurveyStatus;
use Tfcloud\Models\Building;
use Tfcloud\Models\Contact;
use Tfcloud\Models\Report;
use Tfcloud\Models\Room;
use Tfcloud\Models\Site;
use Tfcloud\Services\PermissionService;

class AsbestosReportForCBService extends WkHtmlToPdfReportBaseService
{
    protected $asbHazard = [];
    private $pageIndex = null;
    private $generatedPath = null;
    private $arrayInputHtml = null;
    public $filterText = '';
    private $reportBoxFilterLoader;

    public function __construct(PermissionService $permissionService, Report $report = null)
    {
        $this->permissionService = $permissionService;
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function runReportSingleAsbSurvey($survey)
    {
        //call report from button not report view
        $inputs['asb_survey_id'] = $survey->asb_survey_id;
        $inputs['site_id'] = $survey->site_id;
        $inputs['singleOnly'] = true;

        $repId = ReportCommon::getSystemReportByCode(
            $this->permissionService,
            ReportConstant::SYSTEM_REPORT_ASB_CLI_CB01
        )->system_report_id;

        return $this->runReport(
            $this->getTempFile(),
            $inputs,
            $repId
        );
    }

    public function runReport($repOutPutPdfFile, $inputs, $repId)
    {
        $reportFilterQuery = null;
        if ($this->isReportBoxFilter()) {
            $inputs = $this->formatInputs($inputs, true);
        }

        $this->pageIndex = 1;
        $numberStaticHtml = 5;

        //create wkhtmltopdf temp folder to store generated files
        $this->generatedPath = $this->generateWkhtmltopdfTempPath($repId);

        //generate static html
        $this->arrayInputHtml = $this->getStaticHtml($numberStaticHtml, $inputs);

        //generate dynamic report html
        $filter = new AsbSurveyFilter($inputs);
        $siteId = $buildingId = $roomId = false;
        if (!is_null($filter->site_id) && $filter->site_id) {
            $siteId = $filter->site_id;

            if (!is_null($filter->building_id) && $filter->building_id) {
                $buildingId = $filter->building_id;

                if (!is_null($filter->room_id) && $filter->room_id) {
                    $roomId = $filter->room_id;
                }
            }
        }

        $asbHazardFilter = new AsbHazardFilter($inputs);
        $addAsbLocations = true;
        if (
            $asbHazardFilter->presumed ||
            $asbHazardFilter->strpresumed ||
            $asbHazardFilter->detected ||
            $asbHazardFilter->removed ||
            $asbHazardFilter->nad
        ) {
            // Herts only want to see the ASR statuses they have filtered for to be present in the report, so don't add
            // location cases (which are output as No Asbestos / No Access).
            $addAsbLocations = false;
        }


        if (!$roomId) {
            //first get all buildings for that site
            $buildingQuery = Building::where('building.site_id', '=', $siteId);
            if ($buildingId) {
                $buildingQuery->where('building.building_id', '=', $buildingId);
            }
            $buildingQuery->orderBy('building.building_code', 'ASC');
            $buildings = $buildingQuery->get();
            if (count($buildings) > 0) {
                foreach ($buildings as $building) {
                    $currentBuildingId = $building->building_id;

                    //then get ACMS for the building only
                    $asbQuery = $this->getAsbACMByLocation($siteId, $currentBuildingId, $inputs);
                    $asbBuildOnlyACMs = $asbQuery->get();
                    if (count($asbBuildOnlyACMs) > 0) {
                        //ACMs exist, find other information
                        foreach ($asbBuildOnlyACMs as $asbHazard) {
                            //get other asb ACM information
                            $this->handleACMS($asbHazard);

                            //push found ACM to view
                            $reducedPhotoPath = $this->reducedPhotoPath(
                                'asbhaz',
                                $asbHazard->asb_hazard_id,
                                $asbHazard->photo,
                                $asbHazard->site_group_id
                            );
                            $photoPath = $reducedPhotoPath;

                            $storeHtmlPart = $this->generatedPath . "/" . $this->pageIndex . '.html';
                            $viewRender = $this->getView($asbHazard, $photoPath, $asbHazard->TYPE);
                            $this->pageIndex++;

                            file_put_contents($storeHtmlPart, $viewRender, FILE_APPEND | LOCK_EX);
                            array_push($this->arrayInputHtml, $storeHtmlPart);
                        } //end foreach asb hazard
                    } //end if count acms is greater than 0

                    //then find location cases against building only
                    if ($addAsbLocations) {
                        $this->asbAddLocations(
                            $currentBuildingId
                        );
                    }

                    //then check for ACMs and location records against each room within the building
                    $relevantRooms = Room::where(
                        'room.building_id',
                        '=',
                        $currentBuildingId
                    )
                    ->orderBy('room.room_number');
                    $rooms = $relevantRooms->get();
                    //if rooms exist, check for ACMs and room cases against each one
                    if (count($rooms) > 0) {
                        foreach ($rooms as $room) {
                            $currentRoomId = $room->room_id;
                            //then get ACMS for the room only

                            $asbBuildingRoomQuery = $this->getAsbACMByLocation(
                                $siteId,
                                $currentBuildingId,
                                $inputs,
                                $currentRoomId
                            );
                            $asbRoomAcms = $asbBuildingRoomQuery->get();
                            if (count($asbRoomAcms) > 0) {
                                foreach ($asbRoomAcms as $asbHazard) {
                                    $this->handleACMS($asbHazard);
                                    //push found ACM to view
                                    $reducedPhotoPath = $this->reducedPhotoPath(
                                        'asbhaz',
                                        $asbHazard->asb_hazard_id,
                                        $asbHazard->photo,
                                        $asbHazard->site_group_id
                                    );
                                    $photoPath = $reducedPhotoPath;

                                    $storeHtmlPart = $this->generatedPath . "/" . $this->pageIndex . '.html';
                                    $viewRender = $this->getView($asbHazard, $photoPath, $asbHazard->TYPE);
                                    $this->pageIndex++;

                                    file_put_contents($storeHtmlPart, $viewRender, FILE_APPEND | LOCK_EX);
                                    array_push($this->arrayInputHtml, $storeHtmlPart);
                                } //end foreach ACM with room id
                            }

                            //then check for location data against the room
                            if ($addAsbLocations) {
                                $this->asbAddLocations(
                                    $currentBuildingId,
                                    $currentRoomId
                                );
                            }
                        } //end foreach room
                    }
                } //end foreach building
            } //end building query count
        } else {
            //room id is present, only check for room information
            //first get ACMs for room
            $asbRoomAcmsQuery = $this->getAsbACMByLocation(
                $siteId,
                $buildingId,
                $inputs,
                $roomId
            );
            $asbRoomAcms = $asbRoomAcmsQuery->get();
            if (count($asbRoomAcms) > 0) {
                foreach ($asbRoomAcms as $asbHazard) {
                    $this->handleACMS($asbHazard);
                    //push found ACM to view
                    $reducedPhotoPath = $this->reducedPhotoPath(
                        'asbhaz',
                        $asbHazard->asb_hazard_id,
                        $asbHazard->photo,
                        $asbHazard->site_group_id
                    );
                    $photoPath = $reducedPhotoPath;

                    $storeHtmlPart = $this->generatedPath . "/" . $this->pageIndex . '.html';
                    $viewRender = $this->getView($asbHazard, $photoPath, $asbHazard->TYPE);
                    $this->pageIndex++;

                    file_put_contents($storeHtmlPart, $viewRender, FILE_APPEND | LOCK_EX);
                    array_push($this->arrayInputHtml, $storeHtmlPart);
                } //end foreach ACM with room id
            }
            //then get asb locations against room
            if ($addAsbLocations) {
                $this->asbAddLocations(
                    $buildingId,
                    $roomId
                );
            }
        }

        $whereCodes = [];               // The codes which have been used in the filter
        $whereTexts = [];               // Plain text about filtering applied
        $orCodes = [];                  // csv string of OR codes
        $bFilterDetail = false;

        if ($this->isReportBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $this->filterText = $reportFilterQuery->getFilterDetailText();
        } else {
            $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
            $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $this->filterText);
        }

        /*
         * Set options and generate html to pdf
         * */
        \Log::info("Generate the report from the html.");
        $header = \View::make('reports.wkhtmltopdf.asbestos.centralBedfordshire.header')->render();
        $headerPath = $this->generatedPath . "/header.html";
        file_put_contents($headerPath, $header, FILE_APPEND | LOCK_EX);

        $options = [
            'header-html' => $headerPath,
        ];
        \Log::info("Generate the report from the html.");
        if (array_key_exists('singleOnly', $inputs)) {
            $repOutPutPdfFile .= '.pdf';
            $this->generateMultiHtmlToPdf($this->arrayInputHtml, $this->generatedPath, $repOutPutPdfFile, $options);
            return $repOutPutPdfFile;
        } else {
            $this->generateMultiHtmlToPdf($this->arrayInputHtml, $this->generatedPath, $repOutPutPdfFile, $options);
            return true;
        }
    }

    public function validateFilter($inputs)
    {
        return \Validator::make($this->formatInputs($inputs), $this->validateRules(), $this->validateMessages());
    }

    public function validateRules()
    {
        \Validator::extend('site_required_one', function ($attribute, $value, $parameters) {
            if (is_array($value)) {
                return count($value) == 1;
            }
            return !empty($value);
        });

        \Validator::extend('single_building', function ($attribute, $value, $parameters) {
            if ($value && is_array($value)) {
                return count($value) == 1;
            }
            return true;
        });

        \Validator::extend('single_room', function ($attribute, $value, $parameters) {
            if ($value && is_array($value)) {
                return count($value) == 1;
            }
            return true;
        });

        $rules = [
            'site_id' => ['site_required_one'],
            'building_id' => ['required_with:room_id', 'single_building'],
            'room_id' => ['single_room'],
        ];

        return $rules;
    }

    public function validateMessages()
    {
        $messages = [
            'site_id.site_required_one' => 'Only 1 location can be selected for this report.',
            'building_id.single_building' => 'Only 1 location can be selected for this report.',
            'room_id.single_room' => 'Only 1 location can be selected for this report.',
        ];

        return $messages;
    }

    private function getTempFile()
    {
        return tempnam(Common::getTempFolder(), 'ASBHRT01SingleFullAsbSurvey');
    }

    private function getView($item, $photoPath, $templateType = 1)
    {
        switch ($templateType) {
            case 1:
                $templateName = 'noRoom';
                break;
            case 2:
                $templateName = 'noAccess';
                break;
            case 3:
                $templateName = 'asbestosTechnical';
                break;
            default:
                $templateName = 'noRoom';
                break;
        }
        return \View::make('reports.wkhtmltopdf.asbestos.centralBedfordshire.' . $templateName, [
            'item'          => $item,
            'photoPath'     => $photoPath
        ])->render();
    }

    private function getStaticHtml($numberStaticHtml, $inputs)
    {
        $arrayInputHtml = [];
        $datePrinted = date("d/m/Y");

        $site = Site::select('site.site_id', 'site.site_group_id', 'site.photo', 'site.site_code', 'site.site_desc')
            ->where('site.site_id', '=', array_get($inputs, 'site_id', false))
            ->first();

        $singleSurvey = array_get($inputs, 'singleOnly', false);

        if ($singleSurvey === true) {
            $asbSurveyId = array_get($inputs, 'asb_survey_id');

            if ($asbSurveyId) {
                $asbSurvey = \Tfcloud\Models\AsbSurvey::find($asbSurveyId);
                ;
                if ($asbSurvey) {
                    $surveyType = $asbSurvey->type->survey_type_code;
                    $surveyDate = new \DateTime($asbSurvey->asb_survey_date);
                    $leadSurveyor  = $asbSurvey->leadSurveyor ?
                        Contact::find($asbSurvey->surveyor_id)->getContactOrganistationAttribute() : '';
                }
            }
        }


        $storeHtmlPart = $this->generatedPath . '/' . $this->pageIndex . '.html';
        $staticHtml = \View::make('reports.wkhtmltopdf.asbestos.centralBedfordshire.intro_1', [
            'datePrinted' => $datePrinted,
            'site' => $site,
            'sitePhoto' => $this->getSitePhotoPath($site),
            'surveyType' => $singleSurvey && $surveyType ? $surveyType : '',
            'asb_survey_date' => $surveyDate->format('d/m/Y'),
            'lead_surveyor' => $leadSurveyor

        ])->render();
        file_put_contents($storeHtmlPart, $staticHtml, FILE_APPEND | LOCK_EX);
        array_push($arrayInputHtml, $storeHtmlPart);

        $this->pageIndex++;
        for ($this->pageIndex; $this->pageIndex <= $numberStaticHtml; $this->pageIndex++) {
            $storeHtmlPart = $this->generatedPath . '/' . $this->pageIndex . '.html';

            if ($this->pageIndex == 5 && $singleSurvey === true) {
                $this->retrieveHazards($asbSurveyId);
                $asrs = $this->asbHazard;

                $survey =
                    ['asb_survey_date' => $surveyDate->format('d/m/Y'),
                     'site_text' => Common::concatFields([$asbSurvey->site->site_code, $asbSurvey->site->site_desc])
                    ];
            } else {
                $asrs = [];
                $survey = [];
            }

            $staticHtml =
                \View::make('reports.wkhtmltopdf.asbestos.centralBedfordshire.intro_'
                . $this->pageIndex, ['asbHazards' => $asrs, 'asbSurvey' => $survey])->render();
            file_put_contents($storeHtmlPart, $staticHtml, FILE_APPEND | LOCK_EX);
            array_push($arrayInputHtml, $storeHtmlPart);
        }
        return $arrayInputHtml;
    }

    private function handleACMS($asbHazard)
    {
        //set action information
        $asbAction = $this->getAsbHazardActions($asbHazard->asb_hazard_id);
        $asbHazard['ACTIONS'] = $asbAction;
        //set type to asbTechnical
        $asbHazard['TYPE'] = 3;

        //get sample information
        $asbSamples = $this->getAsbHazardSample($asbHazard->asb_sample_id)->first();
        if (!empty($asbSamples)) {
            $asbHazard['SAMPLE_ID'] = $asbSamples->SAMPLE_ID;
            $asbHazard['lims_reference'] = $asbSamples->lims_reference;
            $asbHazard['SAMPLED_ON'] = $asbSamples->SAMPLED_ON;
            $asbHazard['SAMPLED_BY'] = $asbSamples->SAMPLED_BY;
            $asbHazard['ANALYSED_ON'] = $asbSamples->ANALYSED_ON;
            $asbHazard['ANALYSED_BY'] = $asbSamples->ANALYSED_BY;
            $asbHazard['ANALYSIS'] = $asbSamples->ANALYSIS;
        } else {
            $asbHazard['SAMPLE_ID'] = "";
            $asbHazard['SAMPLED_ON'] = "";
            $asbHazard['SAMPLED_BY'] = "";
            $asbHazard['ANALYSED_ON'] = "";
            $asbHazard['ANALYSED_BY'] = "";
            $asbHazard['ANALYSIS'] = "";
        }

        //get risk assessment information
        $asbRiskSample = $this->getRiskAssessments($asbHazard->asb_hazard_id, AsbRiskStatus::CURRENT)->first();
        if (empty($asbRiskSample)) {
            //no current risk status assessments, try for archived ones
            $asbRiskSample = $this->getRiskAssessments($asbHazard->asb_hazard_id, AsbRiskStatus::ARCHIVE)->first();
        }

        if (!empty($asbRiskSample)) {
            $asbHazard['ASSESSMENT_NO'] = $asbRiskSample->ASSESSMENT_NO;
            if ($asbHazard['HAZARD_STATUS_CODE'] <> 'HS7' && $asbHazard['HAZARD_STATUS_CODE'] <> 'HS6') {
                $asbHazard['RISK_CAT_CODE'] = $asbRiskSample->RISK_CAT_CODE;
                $asbHazard['RISK_CAT_DESC'] = $asbRiskSample->RISK_CAT_DESC;
            } else {
                $asbHazard['RISK_CAT_CODE'] = '';
                $asbHazard['RISK_CAT_DESC'] = '';
            }
            $asbHazard['PRODUCT_TYPE_SCORE_CODE'] = $asbRiskSample->PRODUCT_TYPE_SCORE_CODE;
            $asbHazard['PRODUCT_TYPE_DISPLAY'] = $asbRiskSample->PRODUCT_TYPE_DISPLAY;
            $asbHazard['EXTENT_DAMAGE_SCORE_CODE'] = $asbRiskSample->EXTENT_DAMAGE_SCORE_CODE;
            $asbHazard['EXTENT_DAMAGE_DISPLAY'] = $asbRiskSample->EXTENT_DAMAGE_DISPLAY;
            $asbHazard['SURFACE_TREATMENT_SCORE_CODE'] = $asbRiskSample->SURFACE_TREATMENT_SCORE_CODE;
            $asbHazard['SURFACE_TREATMENT_DISPLAY'] = $asbRiskSample->SURFACE_TREATMENT_DISPLAY;
            $asbHazard['TOTAL_SCORE'] = $asbRiskSample->TOTAL_SCORE;
            $asbHazard['RISK_ASS_SURVEYOR'] = $asbRiskSample->RISK_ASS_SURVEYOR;
            $asbHazard['RISK_ASS_DESC'] = $asbRiskSample->RISK_ASS_DESC;
            $asbHazard['RISK_ASS_REVIEW_DATE'] = $asbRiskSample->RISK_ASS_REVIEW_DATE;
        } else {
            $asbHazard['ASSESSMENT_NO'] = "";
            $asbHazard['RISK_CAT_CODE'] = "";
            $asbHazard['RISK_CAT_DESC'] = "";
            $asbHazard['PRODUCT_TYPE_SCORE_CODE'] = "";
            $asbHazard['PRODUCT_TYPE_DISPLAY'] = "";
            $asbHazard['EXTENT_DAMAGE_SCORE_CODE'] = "";
            $asbHazard['EXTENT_DAMAGE_DISPLAY'] = "";
            $asbHazard['SURFACE_TREATMENT_SCORE_CODE'] = "";
            $asbHazard['SURFACE_TREATMENT_DISPLAY'] = "";
            $asbHazard['TOTAL_SCORE'] = "";
            $asbHazard['RISK_ASS_SURVEYOR'] = "";
            $asbHazard['RISK_ASS_DESC'] = "";
            $asbHazard['RISK_ASS_REVIEW_DATE'] = "";
        }
    }

    private function handleAsbRooms($asbRoom)
    {
        $enterItem = 'false';
        if ($asbRoom->asb_room_status_id != AsbRoomStatus::SURVEYED) {
            $asbRoom['TYPE'] = 2;

            //If building / room are NOT Active and are NOT surveyed don't show location blades.
            if ($asbRoom->room_active == 'Y') {
                $enterItem = 'true';
            }

            if ($asbRoom->room_active == '' && $asbRoom->building_active == 'Y') {
                $enterItem = 'true';
            }
        } else {
            //if room has no ACMS, add asbestos NAD blade in
            $acmQuery = \Tfcloud\Models\AsbHazard::select()
                ->leftJoin(
                    'asb_survey_hazard',
                    'asb_survey_hazard.asb_hazard_id',
                    '=',
                    'asb_hazard.asb_hazard_id'
                )
                ->where('asb_survey_hazard.asb_survey_id', '=', $asbRoom->asb_survey_id)
                ->where('asb_hazard.room_id', '=', $asbRoom->room_id);
            $acmResults = $acmQuery->get();

            //if survey count is 0, show asbestos NAD blade
            if (count($acmResults) === 0) {
                $enterItem = 'true';
                $asbRoom['TYPE'] = 1;
            }
        }
        return $enterItem;
    }

    /**
     * asbAddLocations - Outputs Location information for the supplied building or room
     * @param string $buildingId
     * @param string $roomId - required if data for single room required
     * @return void
     */
    private function asbAddLocations($buildingId, $roomId = false)
    {
        //then find location cases against building only
        $asbRoomQuery = $this->getAsbRoomsByLocation($buildingId, $roomId);
        $asbRooms = $asbRoomQuery->get();
        if (count($asbRooms) > 0) {
            foreach ($asbRooms as $asbRoom) {
                $enterItem = $this->handleAsbRooms($asbRoom);

                if ($enterItem === 'true') {
                    $photoPath = "";
                    $storeHtmlPart = $this->generatedPath . "/" . $this->pageIndex . '.html';
                    $viewRender = $this->getView($asbRoom, $photoPath, $asbRoom['TYPE']);
                    $this->pageIndex++;

                    file_put_contents($storeHtmlPart, $viewRender, FILE_APPEND | LOCK_EX);
                    array_push($this->arrayInputHtml, $storeHtmlPart);
                }
            } //end foreach asb room
        } //end if count asb_rooms greater than zero
        return;
    }

    public function getAsbHazardActions($asbHazardId)
    {
        $asbActionsQuery = AsbAction::select()
            ->leftJoin('asb_action_type', 'asb_action_type.asb_action_type_id', '=', 'asb_action.asb_action_type_id')
            ->where('asb_action.asb_hazard_id', '=', $asbHazardId);
        $asbActions = $asbActionsQuery->get();
        $actionString = "";
        if (count($asbActions) > 0) {
            foreach ($asbActions as $asbAction) {
                $actionString = $actionString . " " . $asbAction->asb_action_type_desc . ".";
            }
        } else {
            $actionString = "";
        }
        return $actionString;
    }

    public function getRiskAssessments($asbHazardId, $asbRiskStatusId)
    {
        $asbRiskAssessment = AsbRiskAssessment::select(
            'asb_overall_risk.overall_risk_code AS RISK_CAT_CODE',
            'asb_overall_risk.overall_risk_desc AS RISK_CAT_DESC',
            'material_product_type.score_code AS PRODUCT_TYPE_SCORE_CODE',
            'material_product_type.display AS PRODUCT_TYPE_DISPLAY',
            'material_extent_of_damage.score_code AS EXTENT_DAMAGE_SCORE_CODE',
            'material_extent_of_damage.display AS EXTENT_DAMAGE_DISPLAY',
            'material_surface_treatment.score_code AS SURFACE_TREATMENT_SCORE_CODE',
            'material_surface_treatment.display AS SURFACE_TREATMENT_DISPLAY',
            "asb_risk_assessment.asb_risk_assessment_code AS ASSESSMENT_NO",
            \DB::raw(
                '(IFNULL(asb_risk_assessment.total_overall_score,0)) AS TOTAL_SCORE'
            ),
            "contact.contact_name AS RISK_ASS_SURVEYOR",
            "asb_risk_assessment.asb_risk_assessment_desc AS RISK_ASS_DESC",
            \DB::raw("DATE_FORMAT(asb_risk_assessment.next_review_date, '%d/%m/%Y') AS RISK_ASS_REVIEW_DATE")
        )
            ->leftJoin('contact', 'contact.contact_id', '=', 'asb_risk_assessment.surveyor_id')
            ->leftJoin('asb_overall_risk', function ($subjoin) {
                $subjoin->on(
                    \DB::raw(
                        'asb_risk_assessment.total_overall_score'
                    ),
                    '>=',
                    'from_score'
                );
                $subjoin->on(
                    \DB::raw(
                        'asb_risk_assessment.total_overall_score'
                    ),
                    '<=',
                    'to_less_than_score'
                );
            })
            ->leftJoin('asb_samplevar_type_score AS material_product_type', function ($join) {
                $join->on(
                    'material_product_type.asb_samplevar_type_score_id',
                    '=',
                    'asb_risk_assessment.product_type_sid'
                )
                ->on('material_product_type.asb_samplevar_type_id', '=', \DB::raw(AsbSamplevarType::PRODUCT_TYPE));
            })
            // Material Assessment | Extent of Damage
            ->leftJoin('asb_samplevar_type_score AS material_extent_of_damage', function ($join) {
                $join->on(
                    'material_extent_of_damage.asb_samplevar_type_score_id',
                    '=',
                    'asb_risk_assessment.extent_damage_sid'
                )
                ->on('material_extent_of_damage.asb_samplevar_type_id', '=', \DB::raw(AsbSamplevarType::EXTENT_DAMAGE));
            })
            // Material Assessment | Surface Treatment
            ->leftJoin('asb_samplevar_type_score AS material_surface_treatment', function ($join) {
                $join->on(
                    'material_surface_treatment.asb_samplevar_type_score_id',
                    '=',
                    'asb_risk_assessment.surface_treatment_sid'
                )
                ->on(
                    'material_surface_treatment.asb_samplevar_type_id',
                    '=',
                    \DB::raw(AsbSamplevarType::SURFACE_TREATMENT)
                );
            })
            ->where('asb_risk_assessment.asb_hazard_id', '=', $asbHazardId)
            ->where('asb_risk_assessment.asb_risk_status_id', '=', $asbRiskStatusId);

        return $asbRiskAssessment;
    }

    public function getAsbHazardSample($asbSampleId)
    {
        $asbSampleQuery = AsbSample::select(
            'asb_sample.asb_sample_id',
            'asb_sample.asb_sample_code as SAMPLE_ID',
            'asb_sample.lims_reference',
            \DB::raw("DATE_FORMAT(asb_sample.asb_sample_sampled_date, '%d/%m/%Y') AS SAMPLED_ON"),
            'contact.contact_name AS SAMPLED_BY',
            \DB::raw("DATE_FORMAT(asb_sample.asb_sample_analysed_date, '%d/%m/%Y') AS ANALYSED_ON"),
            'asb_sample_analyser_contact.contact_name AS ANALYSED_BY',
            'asb_sample.asb_sample_analysis AS ANALYSIS'
        )
            ->leftJoin('contact', 'contact_id', '=', 'asb_sample.sampler_id')
            ->leftJoin(
                'contact as asb_sample_analyser_contact',
                'asb_sample_analyser_contact.contact_id',
                '=',
                'asb_sample.analyser_id'
            )
            ->where('asb_sample.asb_sample_id', '=', $asbSampleId);
        return $asbSampleQuery;
    }

    public function getAsbACMByLocation($siteId, $buildingId, $inputs, $roomId = false)
    {
        $asbHazards = AsbHazard::select(
            'asb_survey_type.survey_type_code AS SURVEY_TYPE',
            'asb_survey.asb_survey_code AS SURVEY_CODE',
            'asb_survey.asb_survey_client_ref AS SURVEY_REFERENCE',
            'asb_hazard.asb_hazard_id as asb_hazard_id',
            'asb_hazard.site_group_id as site_group_id',
            'asb_hazard.photo as photo',
            'asb_hazard_status.asb_hazard_status_code AS HAZARD_STATUS_CODE',
            'asb_hazard_status.asb_hazard_status_desc AS HAZARD_STATUS',
            'asb_hazard.asb_hazard_desc AS DESCRIPTION',
            'asb_hazard.asb_hazard_code AS HAZARD',
            'asb_hazard.asb_sample_id',
            'site.site_code AS SITE_CODE',
            'site.site_desc AS SITE_DESC',
            'building.building_code AS HAZ_BUILDING_CODE',
            'building.building_desc AS HAZ_BUILDING_DESC',
            'asbSurveyBuilding.building_code as BUILDING_CODE',
            'asbSurveyBuilding.building_desc as BUILDING_DESC',
            \DB::raw("DATE_FORMAT(NOW(), '%d/%m/%Y') AS DATE_PRINTED"),
            'room.room_number AS HAZ_ROOM_NUMBER',
            'room.room_desc AS HAZ_ROOM_DESC',
            'asb_asbestos_type.asb_type_category_id AS ASBESTOS_TYPE_CATEGORY_ID',
            'asb_asbestos_type.asb_asbestos_type_code AS ASBESTOS_TYPE',
            'asb_asbestos_type.asb_asbestos_type_desc as ASBESTOS_TYPE_DESC',
            'asb_hazard.asb_hazard_position AS POSITION',
            'asb_hazard.asb_hazard_comment AS COMMENTS',
            'asb_sample_requirement.asb_sample_requirement_code AS SAMPLE_REQUIRED',
            'asb_sample_requirement.asb_sample_requirement_desc AS SAMPLE_REQUIRED_DESC',
            'asb_hazard.asb_hazard_quantity AS QUANTITY',
            'unit_of_measure.unit_of_measure_code AS UNIT_OF_MEASURE',
            \DB::raw("DATE_FORMAT(NOW(), '%d/%m/%Y') AS DATE_PRINTED"),
            'contact.contact_name AS SURVEYOR',
            'asb_survey.asb_survey_id as asb_survey_id',
            \DB::raw("DATE_FORMAT(asb_survey.asb_survey_date, '%d/%m/%Y') AS SURVEY_DATE"),
            'asb_hazard_status_type.asb_hazard_status_type_id AS ASBESTOS_TYPE_CATEGORY_ID',
            'gen_userdef_group.user_text1',
            'gen_userdef_group.user_text2'
        )
            ->leftJoin('asb_survey_hazard', 'asb_survey_hazard.asb_hazard_id', '=', 'asb_hazard.asb_hazard_id')
            ->leftJoin('asb_survey', 'asb_survey.asb_survey_id', '=', 'asb_survey_hazard.asb_survey_id')
            ->leftJoin('asb_survey_type', 'asb_survey_type.survey_type_id', '=', 'asb_survey.survey_type_id')
            ->leftJoin('contact', 'contact.contact_id', '=', 'asb_survey.surveyor_id')
            ->leftJoin(
                'asb_hazard_status',
                'asb_hazard_status.asb_hazard_status_id',
                '=',
                'asb_hazard.asb_hazard_status_id'
            )
            ->leftJoin(
                'asb_hazard_status_type',
                'asb_hazard_status_type.asb_hazard_status_type_id',
                '=',
                'asb_hazard_status.asb_hazard_status_type_id'
            )
            ->leftJoin('site', 'site.site_id', '=', 'asb_hazard.site_id')
            ->leftJoin('building', 'building.building_id', '=', 'asb_hazard.building_id')
            ->leftJoin('building as asbSurveyBuilding', 'asbSurveyBuilding.building_id', '=', 'asb_survey.building_id')
            ->leftJoin('room', 'room.room_id', '=', 'asb_hazard.room_id')
            ->leftJoin(
                'asb_sample_requirement',
                'asb_sample_requirement.asb_sample_requirement_id',
                '=',
                'asb_hazard.asb_sample_requirement_id'
            )
            ->leftJoin(
                'asb_asbestos_type',
                'asb_asbestos_type.asb_asbestos_type_id',
                '=',
                'asb_hazard.asb_asbestos_type_id'
            )
            ->leftJoin('unit_of_measure', 'unit_of_measure.unit_of_measure_id', '=', 'asb_hazard.unit_of_measure_id')
            ->leftJoin(
                'gen_userdef_group',
                'gen_userdef_group.gen_userdef_group_id',
                '=',
                'asb_hazard.gen_userdef_group_id'
            )
            ->where('asb_hazard.site_id', '=', $siteId)
            ->where('asb_hazard.building_id', '=', $buildingId)
            ->where('asb_survey.survey_status_id', '=', AsbSurveyStatus::CURRENT);
        if ($roomId) {
            $asbHazards->where('asb_hazard.room_id', '=', $roomId);
        } else {
            $asbHazards->whereNull('asb_hazard.room_id');
        }
        if (array_key_exists('singleOnly', $inputs)) {
            $asbHazards->where('asb_survey.asb_survey_id', '=', $inputs['asb_survey_id']);
        }
        $asbHazards->orderBy('building.building_code')
                   ->orderBy('room.room_number')
                   ->orderBy('asb_hazard.asb_hazard_code');

        if ($this->isReportBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $operator = $reportFilterQuery->getOperatorParser();
            $operator->setPrefixTables([
                'site' => ['site_id', 'site_code', 'site_desc'],
                'building' => ['building_id', 'building_code', 'building_desc'],
                'room' => ['room_id', 'room_number', 'room_desc'],
                'asb_hazard_status' => ['asb_hazard_status_type_id'],
                'asb_hazard' => ['asb_asbestos_type_id', 'asb_sample_requirement_id'],
            ]);
            $filteredAsbHazardQuery = $reportFilterQuery->filterAll($asbHazards);
        } else {
            $filteredAsbHazardQuery = $this->addHazardFilters($asbHazards, $inputs);
        }
        return $filteredAsbHazardQuery;
    }

    public function getAsbACMs($asbSurveyId, $inputs)
    {
        $asbHazards = AsbHazard::select(
            'asb_hazard.asb_hazard_id as asb_hazard_id',
            'asb_hazard.site_group_id as site_group_id',
            'asb_hazard.photo as photo',
            'asb_hazard_status.asb_hazard_status_code AS HAZARD_STATUS_CODE',
            'asb_hazard_status.asb_hazard_status_desc AS HAZARD_STATUS',
            'asb_hazard.asb_hazard_desc AS DESCRIPTION',
            'asb_hazard.asb_hazard_code AS HAZARD',
            'site.site_code AS SITE_CODE',
            'site.site_desc AS SITE_DESC',
            'building.building_code AS HAZ_BUILDING_CODE',
            'building.building_desc AS HAZ_BUILDING_DESC',
            'room.room_number AS HAZ_ROOM_NUMBER',
            'room.room_desc AS HAZ_ROOM_DESC',
            'asb_asbestos_type.asb_type_category_id AS ASBESTOS_TYPE_CATEGORY_ID',
            'asb_asbestos_type.asb_asbestos_type_code AS ASBESTOS_TYPE',
            'asb_asbestos_type.asb_asbestos_type_desc as ASBESTOS_TYPE_DESC',
            'asb_hazard.asb_hazard_position AS POSITION',
            'asb_hazard.asb_hazard_comment AS COMMENTS',
            'asb_sample_requirement.asb_sample_requirement_code AS SAMPLE_REQUIRED',
            'asb_sample_requirement.asb_sample_requirement_desc AS SAMPLE_REQUIRED_DESC',
            'asb_hazard.asb_hazard_quantity AS QUANTITY',
            'unit_of_measure.unit_of_measure_code AS UNIT_OF_MEASURE',
            \DB::raw("DATE_FORMAT(NOW(), '%d/%m/%Y') AS DATE_PRINTED")
        )
            ->leftJoin('asb_survey_hazard', 'asb_survey_hazard.asb_hazard_id', '=', 'asb_hazard.asb_hazard_id')
            ->leftJoin('asb_survey', 'asb_survey.asb_survey_id', '=', 'asb_survey_hazard.asb_survey_id')
            ->leftJoin(
                'asb_hazard_status',
                'asb_hazard_status.asb_hazard_status_id',
                '=',
                'asb_hazard.asb_hazard_status_id'
            )
            ->leftJoin(
                'asb_hazard_status_type',
                'asb_hazard_status_type.asb_hazard_status_type_id',
                '=',
                'asb_hazard_status.asb_hazard_status_type_id'
            )
            ->leftJoin('site', 'site.site_id', '=', 'asb_hazard.site_id')
            ->leftJoin('building', 'building.building_id', '=', 'asb_hazard.building_id')
            ->leftJoin('room', 'room.room_id', '=', 'asb_hazard.room_id')
            ->leftJoin(
                'asb_sample_requirement',
                'asb_sample_requirement.asb_sample_requirement_id',
                '=',
                'asb_hazard.asb_sample_requirement_id'
            )
            ->leftJoin(
                'asb_asbestos_type',
                'asb_asbestos_type.asb_asbestos_type_id',
                '=',
                'asb_hazard.asb_asbestos_type_id'
            )
            ->leftJoin('unit_of_measure', 'unit_of_measure.unit_of_measure_id', '=', 'asb_hazard.unit_of_measure_id')
            ->where('asb_survey.asb_survey_id', '=', $asbSurveyId)
            ->orderBy('building.building_code')
            ->orderBy('room.room_number');
        $filteredAsbHazardQuery = $this->addHazardFilters($asbHazards, $inputs);
        return $filteredAsbHazardQuery;
    }

    public function addHazardFilters($query, $inputs)
    {
        $filter = new AsbHazardFilter($inputs);

        if (
            !is_null($filter->presumed) || !is_null($filter->strpresumed) || !is_null($filter->detected)
            || !is_null($filter->removed) || !is_null($filter->nad)
        ) {
            $options = [];

            if (!is_null($filter->presumed)) {
                array_push($options, AsbHazardStatusType::PRESUMED);
            }
            if (!is_null($filter->strpresumed)) {
                array_push($options, AsbHazardStatusType::STRPRESUMED);
            }
            if (!is_null($filter->detected)) {
                array_push($options, AsbHazardStatusType::DETECTED);
            }

            if (!is_null($filter->removed)) {
                array_push($options, AsbHazardStatusType::REMOVED);
            }

            if (!is_null($filter->nad)) {
                array_push($options, AsbHazardStatusType::NAD);
            }
            $query->whereIn('asb_hazard_status.asb_hazard_status_type_id', $options);
        }

        if (!is_null($filter->asbestosType) && $filter->asbestosType) {
            $query->where('asb_hazard.asb_asbestos_type_id', $filter->asbestosType);
        }

        if (!is_null($filter->materialType) && $filter->materialType) {
            $query->where('asb_hazard.asb_material_type_id', $filter->materialType);
        }

        if (!is_null($filter->materialSubtype) && $filter->materialSubtype) {
            $query->where('asb_hazard.asb_material_subtype_id', $filter->materialSubtype);
        }

//        if (!is_null($filter->site_id) && $filter->site_id) {
//            $query->where('asb_hazard.site_id', $filter->site_id);
//
//            if (!is_null($filter->building_id) && $filter->building_id) {
//                $query->where('asb_hazard.building_id', $filter->building_id);
//
//                if (!is_null($filter->room_id && $filter->room_id)) {
//                    $query->where('asb_hazard.room_id', $filter->room_id);
//                }
//            }
//        } //handled elsewhere

        if (
            !is_null($filter->sampleYes) || !is_null($filter->sampleNo) || !is_null($filter->sampleTaken)
            || !is_null($filter->sampleRepresentative)
        ) {
            $options = [];

            if (!is_null($filter->sampleYes)) {
                array_push($options, AsbSampleRequirement::REQUIRED);
            }
            if (!is_null($filter->sampleNo)) {
                array_push($options, AsbSampleRequirement::NOT_REQUIRED);
            }
            if (!is_null($filter->sampleTaken)) {
                array_push($options, AsbSampleRequirement::TAKEN);
            }

            if (!is_null($filter->sampleRepresentative)) {
                array_push($options, AsbSampleRequirement::REPRESENTATIVE);
            }

            $query->whereIn('asb_hazard.asb_sample_requirement_id', $options);
        }

        $asbHazardTable = 'asb_hazard';
        if (!is_null($filter->search) && $filter->search) {
            $query->where(function ($subQuery) use ($asbHazardTable, $filter) {
                $subQuery->where($asbHazardTable . '.asb_hazard_code', 'like', '%' . $filter->search . '%');
                $subQuery->orWhere($asbHazardTable . '.asb_hazard_desc', 'like', '%' . $filter->search . '%');
            });
        }

        return $query;
    }

    public function getAsbRooms($asbSurveyId)
    {
        $roomQuery = AsbRoom::select()
            ->where('asb_room.asb_survey_id', '=', $asbSurveyId)
            ->leftJoin('asb_room_status', 'asb_room_status.asb_room_status_id', '=', 'asb_room.asb_room_status_id')
            ->leftJoin('room', 'room.room_id', '=', 'asb_room.room_id');
        return $roomQuery;
    }

    public function getAsbRoomsByLocation($buildingId, $roomId = false)
    {
        $roomQuery = AsbRoom::select(
            'room.room_number AS SURVEY_ROOM_NUMBER',
            'room.room_desc AS SURVEY_ROOM_DESC',
            'room.active as room_active',
            'asb_room.asb_room_comment AS ROOM_COMMENTS',
            'asb_survey_type.survey_type_code AS SURVEY_TYPE',
            'asb_survey.asb_survey_code AS SURVEY_CODE',
            'asb_survey.asb_survey_client_ref AS SURVEY_REFERENCE',
            'asb_survey.site_group_id as site_group_id',
            'site.site_code AS SITE_CODE',
            'site.site_desc AS SITE_DESC',
            'building.building_code AS BUILDING_CODE',
            'building.building_desc AS BUILDING_DESC',
            'building.active as building_active',
            'asb_room.asb_room_status_id as asb_room_status_id',
            'asb_room.asb_room_id as asb_room_id',
            'asb_room.room_id as room_id',
            'asb_room.asb_survey_id as asb_survey_id',
            \DB::raw("DATE_FORMAT(NOW(), '%d/%m/%Y') AS DATE_PRINTED"),
            'contact.contact_name AS SURVEYOR',
            'asb_survey.asb_survey_id as asb_survey_id',
            \DB::raw("DATE_FORMAT(asb_survey.asb_survey_date, '%d/%m/%Y') AS SURVEY_DATE")
        )
            ->leftJoin(
                'asb_room_status',
                'asb_room_status.asb_room_status_id',
                '=',
                'asb_room.asb_room_status_id'
            )
            ->leftJoin('asb_survey', 'asb_survey.asb_survey_id', '=', 'asb_room.asb_survey_id')
            ->leftJoin('asb_survey_type', 'asb_survey_type.survey_type_id', '=', 'asb_survey.survey_type_id')
            ->leftJoin('site', 'site.site_id', '=', 'asb_survey.site_id')
            ->leftJoin('building', 'building.building_id', '=', 'asb_room.building_id')
            ->leftJoin('room', 'room.room_id', '=', 'asb_room.room_id')
            ->leftJoin('contact', 'contact.contact_id', '=', 'asb_survey.surveyor_id')
            ->where('asb_room.building_id', '=', $buildingId)
            ->where('asb_survey.survey_status_id', '=', AsbSurveyStatus::CURRENT);

        if ($roomId) {
            $roomQuery->where('asb_room.room_id', '=', $roomId);
        } else {
            $roomQuery->whereNull('asb_room.room_id');
        }

        return $roomQuery;
    }

    private function generateFilterData($filterData, &$whereCodes, &$whereTexts, &$orCodes)
    {
        $asbHazardStatusType = [];

        if (Common::iset($filterData['presumed'])) {
            $asbHazardStatusType[] = 'Presumed';
        }

        if (Common::iset($filterData['strpresumed'])) {
            $asbHazardStatusType[] = 'Strongly Presumed';
        }

        if (Common::iset($filterData['detected'])) {
            $asbHazardStatusType[] = 'Asbestos Detected';
        }

        if (Common::iset($filterData['removed'])) {
            $asbHazardStatusType[] = 'Asbestos Removed';
        }

        if (Common::iset($filterData['nad'])) {
            $asbHazardStatusType[] = 'Not Asbestos';
        }

        if (!empty($asbHazardStatusType)) {
            array_push($whereTexts, "Asbestos Hazard status type = '" . implode(', ', $asbHazardStatusType) . "'");
        }

        if ($val = Common::iset($filterData['asbestosType'])) {
            array_push(
                $whereCodes,
                ['asb_asbestos_type', 'asb_asbestos_type_id', 'asb_asbestos_type_code', $val, "Asbestos Type"]
            );
        }

        if ($val = Common::iset($filterData['materialType'])) {
            array_push(
                $whereCodes,
                ['asb_material_type', 'asb_material_type_id', 'asb_material_type_code', $val, "Material Type"]
            );
        }

        if ($val = Common::iset($filterData['materialSubtype'])) {
            array_push(
                $whereCodes,
                ['asb_material_subtype', 'asb_material_subtype_id', 'asb_material_subtype_code', $val,
                    "Material Subtype"]
            );
        }

        if ($siteId = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $siteId, "Site Code"]);

            if ($buildingId = Common::iset($filterData['building_id'])) {
                array_push(
                    $whereCodes,
                    ['building', 'building_id', 'building_code', $buildingId, "Building Code"]
                );
            }
        }

        $asbSampleRequirement = [];

        if (Common::iset($filterData['sampleYes'])) {
            $asbSampleRequirement[] = 'Yes';
        }

        if (Common::iset($filterData['sampleNo'])) {
            $asbSampleRequirement[] = 'No';
        }

        if (Common::iset($filterData['sampleTaken'])) {
            $asbSampleRequirement[] = 'Taken';
        }

        if (Common::iset($filterData['sampleRepresentative'])) {
            $asbSampleRequirement[] = 'Representative';
        }

        if (!empty($asbSampleRequirement)) {
            array_push($whereTexts, "Sample Required = '" . implode(', ', $asbSampleRequirement) . "'");
        }

        if ($val = Common::iset($filterData['search'])) {
            array_push($whereTexts, "Filter on Text contains '" . $val . "'");
        }
    }

    private function formatInputs($inputs, $getFirstLocation = false)
    {
        if ($this->isReportBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);

            $siteIds = $reportFilterQuery->getValueFilterField('site_id');
            $buildingIds = $reportFilterQuery->getValueFilterField('building_id');
            $roomIds = $reportFilterQuery->getValueFilterField('room_id');
            $hazardStatusType = $reportFilterQuery->getValueFilterField('asb_hazard_status_type_id');
            $hazardStatusTypeList = empty($hazardStatusType) ? [] : $hazardStatusType;

            $inputs = $this->createHazardStatusType($inputs, $hazardStatusTypeList);

            $inputs['site_id'] = empty($siteIds) ? null : $this->getLocation($siteIds, $getFirstLocation) ;
            $inputs['building_id'] = empty($buildingIds) ? null : $this->getLocation($buildingIds, $getFirstLocation);
            $inputs['room_id'] = empty($roomIds) ? null : $this->getLocation($roomIds, $getFirstLocation);
        }

        return $inputs;
    }

    private function getLocation($list, $getFirstLocation)
    {
        return $getFirstLocation ? $list[0] : $list;
    }

    private function isReportBoxFilter()
    {
        return $this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter();
    }

    private function createHazardStatusType($inputs, $hazardStatusType)
    {
        $keysMap = [
            AsbHazardStatusType::PRESUMED => 'presumed',
            AsbHazardStatusType::STRPRESUMED => 'strpresumed',
            AsbHazardStatusType::DETECTED => 'detected',
            AsbHazardStatusType::REMOVED => 'removed',
            AsbHazardStatusType::NAD => 'nad',
        ];

        foreach ($hazardStatusType as $item) {
            $key = $keysMap[$item];
            $inputs[$key] = true;
        }

        return $inputs;
    }

    private function getSitePhotoPath(Site $site)
    {
        $siteId = $site->site_id;

        $siteImagePath = $this->getPhotoStoragePathway($site->site_group_id, $siteId, 'site');

        $splitString = explode("site/", $siteImagePath);
        $sitePhotoPath = false;
        if ($site->photo) {
            $firstHalf = $splitString[0];
            $sitePhotoPath = $firstHalf . "site/" . $siteId . "/" . $site->photo;

            if (!file_exists($sitePhotoPath)) {
                // Client doesnt want to see the defauolt no photo image on the report.
                $sitePhotoPath = '';
            } else {
                list($width, $height) = getimagesize($sitePhotoPath);
                $newHeight = 250;
                $newWidth = $newHeight * $width / $height;
                $tmpPhotoFile = tempnam(Common::getTempFolder(), 'rlg');
                $thumb = imagecreatetruecolor($newWidth, $newHeight);
                $source = imagecreatefromjpeg($sitePhotoPath);
                imagecopyresized($thumb, $source, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
                imagejpeg($thumb, $tmpPhotoFile, 100);
                return $tmpPhotoFile;
            }
        }


        return $sitePhotoPath;
    }

    private function retrieveHazards($asbSurveyId)
    {
        $sqlSelect = "SELECT
               asb_hazard.asb_hazard_id,
               asb_survey_hazard.asb_survey_id,
               asb_hazard.building_id,
               asb_hazard.room_id,
               asb_hazard.plant_id,
               asb_hazard.identifier_id,
               DATE_FORMAT(asb_hazard.asb_hazard_identified_date,
               '%d/%m/%Y') AS `asb_hazard_identified_date`,
               asb_hazard.asb_hazard_status_id,
               asb_hazard.asb_hazard_code,
               asb_hazard.asb_hazard_desc,
               asb_hazard.asb_hazard_position,
               asb_hazard.asb_material_type_id,
               asb_hazard.asb_material_subtype_id,
               asb_hazard.asb_hazard_quantity,
               asb_hazard.unit_of_measure_id,
               DATE_FORMAT(asb_hazard.asb_hazard_created_date,
               '%d/%m/%Y') AS `asb_hazard_created_date`,
               asb_hazard.asb_asbestos_type_id,
               asb_hazard.asb_sample_requirement_id,
               asb_hazard.asb_sample_id,
               asb_risk_assessment.total_material_score,
               asb_risk_assessment.total_priority_score,
               asb_risk_assessment.extent_damage_sid,
               asb_survey.site_id,
               asb_survey.building_id AS `survey_building_id`,
               asb_survey.asb_survey_code,
               asb_survey.survey_status_id,
               asb_survey.surveyor_id,
 	       survey_surveyor.contact_name AS 'surveyor_name',
	       survey_surveyor.organisation AS 'surveyor_organisation',
 	       site.site_id,
               site.site_code,
               site.site_desc,
               survey_building.building_code AS `survey_building_code`,
               survey_building.building_desc AS `survey_building_desc`,
               building.building_code,
               building.building_desc,
               room.room_number,
               room.room_desc,
               plant.plant_code,
               plant.plant_desc,
               plant.plant_group_id,
               plant.plant_subgroup_id,
               plant.plant_type_id,
               plant.warranty_end_date,
               plant_group.plant_group_code,
               plant_subgroup.plant_subgroup_code,
               plant_type.plant_type_code,
               contact.contact_name AS `identifier_name`,
               contact.organisation AS `identifier_organisation`,
               COALESCE(asb_hazard_status.asb_hazard_status_code, '') AS asb_hazard_status_code,
               asb_hazard_status.asb_hazard_status_type_id,
               COALESCE(asb_material_type.asb_material_type_desc, '') AS asb_material_type_desc,
               COALESCE(asb_material_subtype.asb_material_subtype_desc, '') AS asb_material_subtype_desc,
               COALESCE(unit_of_measure.unit_of_measure_code, '') AS unit_of_measure_code,
               COALESCE(asb_asbestos_type.asb_asbestos_type_code, '') AS asb_asbestos_type_code,
               COALESCE(asb_sample_requirement.asb_sample_requirement_code, '') AS asb_sample_requirement_code

            FROM
               asb_hazard
            INNER JOIN
               asb_survey_hazard
                  ON asb_survey_hazard.asb_hazard_id = asb_hazard.asb_hazard_id
            INNER JOIN
               asb_survey
                  ON asb_survey.asb_survey_id = asb_survey_hazard.asb_survey_id
            INNER JOIN
               asb_hazard_status
                  ON asb_hazard_status.asb_hazard_status_id = asb_hazard.asb_hazard_status_id
            INNER JOIN
               site
                  ON site.site_id = asb_survey.site_id
            LEFT JOIN
               building AS survey_building
                  ON survey_building.building_id = asb_survey.building_id
            INNER JOIN
               building
                  ON building.building_id = asb_hazard.building_id
            LEFT JOIN
               room
                  ON room.room_id = asb_hazard.room_id
            LEFT JOIN
               plant
                  ON plant.plant_id = asb_hazard.plant_id
            LEFT JOIN
               plant_type
                  ON plant_type.plant_type_id = plant.plant_type_id
            LEFT JOIN
               plant_subgroup
                  ON plant_subgroup.plant_subgroup_id = plant.plant_subgroup_id
            LEFT JOIN
               plant_group
                  ON plant_group.plant_group_id = plant.plant_group_id
            LEFT JOIN
               contact
                  ON contact.contact_id = asb_hazard.identifier_id
            LEFT JOIN
                contact AS survey_surveyor
                  ON survey_surveyor.contact_id = asb_survey.surveyor_id
            LEFT JOIN
               asb_material_type
                  ON asb_material_type.asb_material_type_id = asb_hazard.asb_material_type_id
            LEFT JOIN
               asb_material_subtype
                  ON asb_material_subtype.asb_material_subtype_id = asb_hazard.asb_material_subtype_id
            LEFT JOIN
               unit_of_measure
                  ON unit_of_measure.unit_of_measure_id = asb_hazard.unit_of_measure_id
            LEFT JOIN
               asb_asbestos_type
                  ON asb_asbestos_type.asb_asbestos_type_id = asb_hazard.asb_asbestos_type_id
            LEFT JOIN
               asb_sample_requirement
                  ON asb_sample_requirement.asb_sample_requirement_id = asb_hazard.asb_sample_requirement_id
            LEFT JOIN
               asb_risk_assessment
                  ON (
                     (
                        asb_hazard.asb_hazard_id = asb_risk_assessment.asb_hazard_id
                     )
                     AND (
                        asb_risk_assessment.asb_risk_status_id = 2
                     )
                  )
            WHERE
               asb_survey_hazard.asb_survey_id = " . $asbSurveyId . "  ";

            /** Exclude removed asbestos from the list view */
        if (!\Auth::user()->siteGroup->includeRemovedAsbestos()) {
            $sqlSelect .= " AND asb_hazard_status.asb_hazard_status_type_id != " . AsbHazardStatusType::REMOVED . " ";
        }

        $sqlSelect .= " ORDER BY building.building_code, room.room_number, plant.plant_code, " .
                      "asb_hazard.asb_hazard_code ASC";

        $results = \DB::select(\DB::raw($sqlSelect));

        if (count($results) > 0) {
            foreach ($results as $result) {
                $this->asbHazard[] = (array) $result;
            }
        }
        return $this->asbHazard;
    }
}
