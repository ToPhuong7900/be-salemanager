<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\ReportConstant;
use Tfcloud\Models\Address;
use Tfcloud\Models\CondLocationScore;
use Tfcloud\Models\CondLocationSubelementScore;
use Tfcloud\Models\Condsurvey;
use Tfcloud\Models\Idwork;
use Tfcloud\Models\IdworkElement;
use Tfcloud\Models\IdworkPriority;
//use Tfcloud\Models\IdworkStatus;
use Tfcloud\Services\ImageService;
use Tfcloud\Services\PermissionService;

class ConditionReportForTorbayService extends WkHtmlToPdfReportBaseService
{
    private $generatedPath = null;
    private $pageIndex = 1;
    protected $idworkService;
    protected $imageService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
        $this->idworkService = \Illuminate\Support\Facades\App::make(
            'Tfcloud\Services\Condition\IdworkService'
        );
        $this->imageService = new ImageService();
    }

    public function runReport($repId, $repOutPutPdfFile = null, $inputs = [], $conditionId = null, &$errMsg = [])
    {
        if (!is_null($conditionId)) {
            $inputs['conditionId'] = $conditionId;
            $conditionObj = Condsurvey::find($conditionId);
            if ($conditionObj instanceof Condsurvey) {
                $reportFileName = ReportConstant::SYSTEM_REPORT_CND_CLI_TORB01 . '_' . $conditionObj->condsurvey_code;
                $repOutPutPdfFile = $this->generateReportOutPutPath($reportFileName);
            } else {
                $errMsg[] = 'No condition survey found';
                return false;
            }
        }

        $this->generatedPath = $this->generateWkhtmltopdfTempPath($repId);
        \Log::info("Generate the report from the html.");
        $arrPdf = [];

        $siteGroupId = \Auth::user()->site_group_id;
        $condSurveyObjList = $this->filter($this->all(), $inputs)->get();
        if ($condSurveyObjList->isEmpty()) {
            $condSurveyData = $this->formatCondSurvey(null);
            $footerRender = $this->getView([], 'footer');
            $arrCoverPdf = $this->generateCoverPage($condSurveyData, $footerRender, $repOutPutPdfFile);
            $arrPdf = array_merge($arrPdf, $arrCoverPdf);
        }

        foreach ($condSurveyObjList as $condSurveyObj) {
            $condSurveyData = $this->formatCondSurvey($condSurveyObj);
            $footerRender = $this->getView($condSurveyData, 'footer');
            $firstPdf = $this->generateCoverPage($condSurveyData, $footerRender, $repOutPutPdfFile);

            $idWorkObjList = $this->getIdWorks(
                $condSurveyObj->condsurvey_id,
                $condSurveyObj->condsurvey_building_block_id
            )->get();
            $idWorkPhotoList = [];
            $idWorks = $this->formatIdWorks($idWorkObjList, $idWorkPhotoList);

            //Element summary
            $summaryElementForSite = $this->getElementSummary($condSurveyObj->condsurvey_id);
            $priorityDropdown = $this->getPriorityDropdown(true);

            $data = [
                'condSurveyData' => $condSurveyData,
                'summaryElementForSite' => $summaryElementForSite,
                'idWorks' => $idWorks,
                'siteMap' => $this->getPhoto(
                    $condSurveyObj->site_id,
                    $siteGroupId,
                    'location/' . $condSurveyObj->site->location_map
                ),
                'idWorkPhotoList' => $idWorkPhotoList,
                'priorityDropdown' => $priorityDropdown
            ];
            $headerCoverRender = $this->generateHeader($condSurveyData);
            $options = [
                'footer-html' => $footerRender,
                'header-html' => $headerCoverRender,
                'toc' => true,
                'header-spacing' => 4,
                'footer-spacing' => 4,
            ];
            $pagePart = $this->generatedPath . "/" . '2.html';
            $cover2PageRender = $this->getView($data, 'content');
            file_put_contents($pagePart, $cover2PageRender, FILE_APPEND | LOCK_EX);

            $secondPdf =  $this->generateMultiHtmlToPdf(
                [$pagePart],
                $this->generatedPath,
                $repOutPutPdfFile,
                $options,
                $this->pageIndex,
                false
            );

            $arrPdf = array_merge($firstPdf, $secondPdf);
        }
        if (!empty($arrPdf)) {
            $cssPaging = [
                'fontSize' => 8,
                'x' => 755,
                'y' => 5
            ];
            $this->mergerPdf($arrPdf, $repOutPutPdfFile, ['cssPaging' => $cssPaging]);
        }

        \Log::info("END Generate the report from the html.");

        if (is_null($conditionId)) {
            return true;
        } else {
            return $repOutPutPdfFile;
        }
    }

    private function getPriorityDropdown($isList = false, $isIndex = false)
    {
        $result = [];
        $idWorkPriority = IdworkPriority::userSiteGroup()
            ->select(['idwork_priority_code', 'idwork_priority_id', 'ipi_desc'])
            ->orderBy('idwork_priority_code')
            ->get();

        if ($isList) {
            if ($isIndex) {
                foreach ($idWorkPriority as $item) {
                    $result[] = $item->idwork_priority_code;
                }
            } else {
                foreach ($idWorkPriority as $item) {
                    $result[$item->idwork_priority_code] = $item->ipi_desc;
                }
            }
        } else {
            $result = $idWorkPriority;
        }


        return $result;
    }

    protected function getPhotoStoragePathway($siteGroupId, $itemKey, $photoFolder)
    {
        $uploadPath = \Config::get('cloud.legacy_paths.uploads');
        $modelName = $photoFolder . '/' ;
        return $uploadPath . '/' . $siteGroupId . '/' . $modelName .
            $itemKey . '/';
    }


    private function getElementSummary($condSurveyId)
    {
        $priorityIdList = [
            'priority_1' => null,
            'priority_2' => null,
            'priority_3' => null,
            'priority_4' => null,
        ];

        $idWorkPriority = $this->getPriorityDropdown();
        foreach ($idWorkPriority as $key => $priority) {
            $priorityIdList['priority_' . ($key + 1)] = $priority->idwork_priority_id;
        }

        $summaryElementForSite = $this->getSummaryByElementQueryBuilder($condSurveyId, $priorityIdList)->get();

        return $summaryElementForSite;
    }

    private function getSummaryByElementQueryBuilder(
        $condSurveyId,
        $priorityIdList,
        $buildingId = null
    ) {
        $query = IdworkElement::userSiteGroup()->orderBy('idwork_element.idwork_element_code');
        $select = [
            'idwork_element.idwork_element_code',
            'idwork_element.idwork_element_desc'
        ];
        foreach ($priorityIdList as $key => $priorityId) {
            array_push(
                $select,
                $this->countIdWorkQueryBuilder(
                    $condSurveyId,
                    $key,
                    $priorityId,
                    $buildingId
                )
            );
        }
        $query->select($select);
        return $query;
    }

    private function countIdWorkQueryBuilder($condSurveyId, $aliasPriority, $idWorkPriorityId, $buildingId = null)
    {
        $buildingWhere = '';
        if ($buildingId) {
            $buildingWhere = ' AND idwork.building_id = ' . $buildingId;
        }
        //$idWorkStatusDraft = IdworkStatusType::DRAFT;
        $query = \DB::raw(
            "(
            SELECT SUM(idwork.total_cost)
            FROM idwork
              JOIN idwork_priority ON idwork_priority.idwork_priority_id = idwork.idwork_priority_id
            WHERE idwork.element_id = idwork_element.idwork_element_id
                  AND idwork.condsurvey_id = {$condSurveyId}
                  AND idwork_priority.idwork_priority_id = {$idWorkPriorityId}
                  {$buildingWhere}
          ) AS '{$aliasPriority}'"
        );
        return $query;
    }


    private function generateCoverPage($data, $footerRender, $repOutPutPdfFile)
    {
        $headerCoverRender = $this->generateHeader($data);
        $options = [
            'margin-bottom' => 0,
            'margin-left' => 0,
            'margin-right' => 0,
            'margin-top' => 0
        ];
        $pagePart = $this->generatedPath . "/" . $this->pageIndex . '.html';
        $coverPageRender = $this->getView($data, 'cover_page_single');
        file_put_contents($pagePart, $coverPageRender, FILE_APPEND | LOCK_EX);

        $firstPdf =  $this->generateMultiHtmlToPdf(
            [$pagePart],
            $this->generatedPath,
            $repOutPutPdfFile,
            $options,
            $this->pageIndex,
            false
        );

        return $firstPdf;
    }

    private function generateElementsPage($data, $dataHeader, $footerRender, $repOutPutPdfFile)
    {
        $dataHeader['isFirstPage'] = false;
        $dataHeader['isIdWork'] = false;
        $headerRender = $this->generateHeader($dataHeader);
        $options = [
            'footer-html' => $footerRender,
            'header-html' => $headerRender,
            'orientation' => 'landscape'
        ];
        $pagePart = $this->generatedPath . "/" . $this->pageIndex . '.html';
        $data['weightedScoreTotal'] = $dataHeader['weightedScoreTotal'];
        $data['scoreLocationCode'] = $dataHeader['condition'];
        $data['weightingTotal'] = $dataHeader['weightingTotal'];
        $elementPageRender = $this->getView($data, 'elements');
        file_put_contents($pagePart, $elementPageRender, FILE_APPEND | LOCK_EX);

        $pdf =  $this->generateMultiHtmlToPdf(
            [$pagePart],
            $this->generatedPath,
            $repOutPutPdfFile,
            $options,
            $this->pageIndex,
            false
        );

        return $pdf;
    }

    private function generateIdWorksPage($data, $dataHeader, $footerRender, $repOutPutPdfFile)
    {
        $dataHeader['isFirstPage'] = false;
        $dataHeader['isIdWork'] = true;
        $dataHeader['isOrphaned'] = $data['isOrphaned'];
        $headerRender = $this->generateHeader($dataHeader);
        $options = [
            'footer-html' => $footerRender,
            'header-html' => $headerRender,
            'orientation' => 'landscape'
        ];
        $pagePart = $this->generatedPath . "/" . $this->pageIndex . '.html';
        $elementPageRender = $this->getView($data, 'idWorks');
        file_put_contents($pagePart, $elementPageRender, FILE_APPEND | LOCK_EX);

        $pdf =  $this->generateMultiHtmlToPdf(
            [$pagePart],
            $this->generatedPath,
            $repOutPutPdfFile,
            $options,
            $this->pageIndex,
            false
        );

        return $pdf;
    }

    //private function generate
    private function generateHeader($data)
    {
        return $this->getView($data, 'header');
    }

    private function getView($data, $templateName)
    {
        return \View::make('reports.wkhtmltopdf.condition.cndCliTorb01.' . $templateName, $data)->render();
    }

    private function formatIdWorks($idWorkObjList, &$idWorkPhotoList)
    {
        $result = [];
        $now = new \DateTime();
        $priorityDropdown = $this->getPriorityDropdown(true, true);

        foreach ($idWorkObjList as $idWorkObj) {
            if ($idWorkObj instanceof Idwork) {
                if (!isset($result[$idWorkObj->idwork_element_id])) {
                    $result[$idWorkObj->idwork_element_id] = [
                        'idWorkElementCode' => $idWorkObj->idwork_element_code,
                        'idWorkElementDesc' => $idWorkObj->idwork_element_desc,
                        'subElements' => []
                    ];
                }
                if (
                    !isset(
                        $result[$idWorkObj->idwork_element_id]['subElements'][$idWorkObj->idwork_subelement_id]
                    )
                ) {
                    $result[$idWorkObj->idwork_element_id]['subElements'][$idWorkObj->idwork_subelement_id] = [
                        'idWorkSubElementCode' => $idWorkObj->idwork_subelement_code,
                        'idWorkSubElementDesc' => $idWorkObj->idwork_subelement_desc,
                        'items' => []
                    ];
                }

                $result[$idWorkObj->idwork_element_id]['subElements'][$idWorkObj->idwork_subelement_id]['items'][] =
                    [
                    'idWorkItemCode' => $idWorkObj->idwork_item_code,
                    'idWorkItemDesc' => $idWorkObj->idworkitem_desc,
                    'idWorkCondition' => $idWorkObj->idwork_condition_code,
                    'idWorkRemedy' => $idWorkObj->remedy,
                    'idWorkCode' => $idWorkObj->idwork_code,
                    'priority' =>  $this->getPriorityData($priorityDropdown, $idWorkObj),
                    'idWorkPriorityId' => $idWorkObj->idwork_priority_id,
                    'idWorkPriorityCode' => $idWorkObj->idwork_priority_code,
                    'idWorkPriorityDesc' => $idWorkObj->ipi_desc,
                    'total_cost' => $idWorkObj->total_cost
                    ];

                $siteGroupId = \Auth::user()->site_group_id;
                $idWorkPhotoList[$idWorkObj->idwork_code] =
//                    $this->getPhotoStoragePathway($siteGroupId, $idWorkObj->idwork_id, 'idwork') . $idWorkObj->photo;
                    $this->getIdWorkPhotosById($idWorkObj->idwork_id, $idWorkObj->photo);
            }
        }

        return $result;
    }

    private function getPriorityData($priorityDropdown, $idWorkObj)
    {
        $priorityCode = $idWorkObj->idwork_priority_code;
        $position = array_search($priorityCode, $priorityDropdown) + 1;
        return [
            'p1' => ($position == 1) ? $idWorkObj->total_cost : '',
            'p2' => ($position == 2) ? $idWorkObj->total_cost : '',
            'p3' => ($position == 3) ? $idWorkObj->total_cost : '',
            'p4' => ($position == 4) ? $idWorkObj->total_cost : '',
        ];
    }

    private function getIdWorkPhotosById($id, $mainPhoto)
    {
        $result = [];
        $siteGroupId = \Auth::user()->site_group_id;
        $photoPath = $this->getPhotoStoragePathway($siteGroupId, $id, 'idwork');
        $imagePath = $photoPath . 'images/';
        if ($mainPhoto) {
            $result[] = $photoPath . $mainPhoto;
        }
        $images = $this->imageService->findImages($imagePath);
        foreach ($images as $image) {
            $result[] = $photoPath . 'images/' . $image;
        }

        return $result;
    }

    private function getIdWorks($condSurveyId, $buildingBlockId)
    {
        $select = [
            'idwork.idwork_id',
            'idwork.photo',
            'idwork.idwork_code',
            'idwork.defect',
            'idwork.location',
            'idwork.remedy',
            'idwork.created_date',
            'idwork.comment',
            'idwork.cost',
            'idwork.cnd_prelim_cost',
            'idwork.cnd_contin_cost',
            'idwork.cnd_fee_cost',
            'idwork.total_cost',
            'idwork.cyclical_maintenance',
            'idwork.cycle_length',
            'fin_year.fin_year_code',
            'fin_year.year_end_date',
            'idwork_condition.idwork_condition_code',
            'idwork_priority.idwork_priority_id',
            'idwork_priority.idwork_priority_code',
            'idwork_priority.ipi_desc',
            'idwork_status.idwork_status_code',
            'gen_userdef_sel.gen_userdef_sel_code',
            'gen_userdef_sel.gen_userdef_sel_desc',
            'gen_userdef_group.user_text1',
            'idwork_element.idwork_element_id',
            'idwork_element.idwork_element_code',
            'idwork_element.idwork_element_desc',
            'idwork_subelement.idwork_subelement_id',
            'idwork_subelement.idwork_subelement_code',
            'idwork_subelement.idwork_subelement_desc',
            'idwork_item.idwork_item_id',
            'idwork_item.idwork_item_code',
            'idwork_item.idworkitem_desc'
        ];

        $query = Idwork::select($select)
            ->leftJoin(
                'idwork_element',
                'idwork_element.idwork_element_id',
                '=',
                'idwork.element_id'
            )
            ->leftJoin(
                'idwork_subelement',
                'idwork_subelement.idwork_subelement_id',
                '=',
                'idwork.sub_element_id'
            )
            ->leftJoin(
                'idwork_item',
                'idwork_item.idwork_item_id',
                '=',
                'idwork.item_id'
            )
            ->leftJoin(
                'idwork_condition',
                'idwork_condition.idwork_condition_id',
                '=',
                'idwork.idwork_condition_id'
            )
            ->leftJoin(
                'idwork_priority',
                'idwork_priority.idwork_priority_id',
                '=',
                'idwork.idwork_priority_id'
            )
            ->leftJoin(
                'idwork_status',
                'idwork_status.idwork_status_id',
                '=',
                'idwork.idwork_status_id'
            )
            ->leftJoin(
                'gen_userdef_group',
                'gen_userdef_group.gen_userdef_group_id',
                '=',
                'idwork.gen_userdef_group_id'
            )
            ->leftJoin(
                'gen_userdef_sel',
                'gen_userdef_sel.gen_userdef_sel_id',
                '=',
                'gen_userdef_group.sel1_gen_userdef_sel_id'
            )
            ->leftJoin(
                'fin_year',
                'fin_year.fin_year_id',
                '=',
                'idwork.target_fin_year_id'
            )
            ->where('idwork.condsurvey_id', '=', $condSurveyId)
            ->where('idwork.building_block_id', '=', $buildingBlockId)
            ->whereNull('idwork.room_id')
            ->orderBy('idwork_element.idwork_element_code', 'ASC')
            ->orderBy('idwork_subelement.idwork_subelement_code', 'ASC')
            ->orderBy('idwork.idwork_code', 'ASC');

        return $query;
    }

    private function getSubElements($condLocationScoreId, $idworkElementId)
    {
        $select = [
            'idwork_subelement.idwork_subelement_code',
            'idwork_subelement.idwork_subelement_desc',
            'score.score_code',
            'cond_location_subelement_score.comment'
        ];

        $query = CondLocationSubelementScore::select($select)
            ->join(
                'idwork_subelement',
                'idwork_subelement.idwork_subelement_id',
                '=',
                'cond_location_subelement_score.idwork_subelement_id'
            )
            ->leftJoin(
                'score',
                'score.score_id',
                '=',
                'cond_location_subelement_score.score_id'
            )
            ->where('cond_location_subelement_score.cond_location_score_id', '=', $condLocationScoreId)
            ->where('cond_location_subelement_score.idwork_element_id', '=', $idworkElementId)
            ->orderBy('idwork_subelement.idwork_subelement_code', 'ASC');

        return $query;
    }

    private function formatElement($condLocationScoreObj, &$weightingTotal)
    {
        $result = [];

        if ($condLocationScoreObj instanceof CondLocationScore) {
            if ($condLocationScoreObj->condLocationElementScoreId > 0) {
                $weightingTotal = $weightingTotal + $condLocationScoreObj->weighting;
            }
            $result =  [
                'elementCode' => $condLocationScoreObj->idwork_element_code,
                'elementDescription' => $condLocationScoreObj->idwork_element_desc,
                'elementComment' => $condLocationScoreObj->comment,
                'elementGrade' => $condLocationScoreObj->score_code,
                'elementScore' => $condLocationScoreObj->weighted_score_value,
                'elementWeighting' => $condLocationScoreObj->weighting
            ];

            $result['subElements'] = [];
            $subElementObjList  = $this->getSubElements(
                $condLocationScoreObj->cond_location_score_id,
                $condLocationScoreObj->idwork_element_id
            )->get();
            foreach ($subElementObjList as $subElementObj) {
                if ($subElementObj instanceof CondLocationSubelementScore) {
                    $subElementData = [
                        'subElementCode' => $subElementObj->idwork_subelement_code,
                        'subElementDescription' => $subElementObj->idwork_subelement_desc,
                        'subElementGrade' => $subElementObj->score_code,
                        'subElementComment' => $subElementObj->comment
                    ];
                    array_push($result['subElements'], $subElementData);
                }
            }
        }
        return $result;
    }

    private function getElements($condSurveyId, $buildingBlockId)
    {
        $select = [
            'cond_location_score.cond_location_score_id',
            'idwork_element.idwork_element_id',
            'idwork_element.idwork_element_code',
            'idwork_element.idwork_element_desc',
            'idwork_element.weighting',
            'cond_location_element_score.weighted_score_value',
            'cond_location_element_score.comment',
            'cond_location_element_score.score_id as condLocationElementScoreId',
            'score.score_code'
            ];

        $query = CondLocationScore::select($select)
            ->join(
                'cond_location_element_score',
                'cond_location_element_score.cond_location_score_id',
                '=',
                'cond_location_score.cond_location_score_id'
            )
            ->join(
                'idwork_element',
                'idwork_element.idwork_element_id',
                '=',
                'cond_location_element_score.idwork_element_id'
            )
            ->leftJoin(
                'score',
                'score.score_id',
                '=',
                'cond_location_element_score.score_id'
            )
            ->where('cond_location_score.condsurvey_id', '=', $condSurveyId)
      //      ->where('cond_location_score.location_record_id', '=', $buildingBlockId)
            ->orderBy('idwork_element.idwork_element_code', 'ASC');

        return $query;
    }

    private function formatCondSurvey($condSurveyObj)
    {
        $condSurveyData =  [
            'siteContactName' => '',
            'siteCode' => '',
            'siteDescription' => '',
            'sitePhoto' => '',
            'addressString' => '',

            'buildingBlockCode' => '',
            'buildingBlockDescription' => '',
            'surveyDate' => '',
            'surveyCode' => '',
            'surveyStatus' => '',
            'weather' => '',
            'gia' => '',
            'listed' => '',
            'surveyStatus' => '',
            'condition' => '',
            'built' => '',
            'conservationArea' => '',
            'elecSummary' => '',
            'genSummary' => '',
            'occupancyClassCode' => '',
            'occupancyClassDescription' => ''
        ];

        $siteGroupId = \Auth::user()->site_group_id;

        if ($condSurveyObj instanceof Condsurvey) {
            $surveyDateObj = new \DateTime($condSurveyObj->survey_date);
            $condSurveyData = [
                'siteContactName' => $condSurveyObj->siteContactName,
                'siteCode' => $condSurveyObj->site_code,
                'siteDescription' => $condSurveyObj->site_desc,
                'sitePhoto' => $this->getPhoto($condSurveyObj->site_id, $siteGroupId, $condSurveyObj->photo),
                'siteMap' => $this->getPhoto($condSurveyObj->site_id, $siteGroupId, $condSurveyObj->location_map),
                'siteTypeCode' => $condSurveyObj->site_type_code,
                'siteTypeDesc' => $condSurveyObj->site_type_desc,
                'buildingBlockCode' => $condSurveyObj->building_block_code,
                'buildingBlockDescription' => $condSurveyObj->building_block_desc,
                'age' => $condSurveyObj->building_age_desc,
                'gia' => $condSurveyObj->gia,
                'surveyDate' => $surveyDateObj->format('l jS F Y'),
                'genSummary' => $condSurveyObj->gen_summary,
                'surveyorName' => $condSurveyObj->surveyorContactName,
                'udfContact1Name' => $condSurveyObj->udfContact1Name,
                'udfContact2Name' => $condSurveyObj->udfContact2Name,
                'udfContact3Name' => $condSurveyObj->udfContact3Name,
                'udfDate1' => $condSurveyObj->user_date1,
                'buildingDesc' => $condSurveyObj->building_desc,
                'buildingCode' => $condSurveyObj->building_code,
            ];

            $addressObj = Address::find($condSurveyObj->address_id);
            if ($addressObj instanceof Address) {
                $condSurveyData['addressString'] = $addressObj->getAddressAsString();
            }
        }


        return $condSurveyData;
    }

    private function all()
    {
        $select = [
            'condsurvey.condsurvey_id',
            'condsurvey.condsurvey_building_block_id',
            'condsurvey.condsurvey_code',
            'condsurvey.survey_date',
            'condsurvey.elec_summary',
            'condsurvey.mech_summary',
            'condsurvey.gen_summary',
            'site.site_id',
            'site.site_code',
            'site.site_desc',
            'site.address_id',
            'site.photo',
            'site.location_map',
            'site_type.site_type_code',
            'site_type.site_type_desc',
            'contact.contact_name as siteContactName',
            'contact.organisation',
            'building_block.building_block_code',
            'building_block.building_block_desc',
            'building_block.year_built',
            'building_block.gia',
            'building_age.building_age_desc',
            'building.building_code',
            'building.building_desc',
            'surveyor.contact_name as surveyorContactName',
            'surveyor.organisation',
            'gen_userdef_group.user_date1',
            'udfContact1.contact_name as udfContact1Name',
            'udfContact2.contact_name as udfContact2Name',
            'udfContact3.contact_name as udfContact3Name',

        ];
        $query = Condsurvey::select($select)
            ->leftJoin(
                'site',
                'site.site_id',
                '=',
                'condsurvey.site_id'
            )
            ->leftJoin(
                'contact',
                'contact.contact_id',
                '=',
                'site.contact_id'
            )
            ->leftJoin(
                'site_type',
                'site_type.site_type_id',
                '=',
                'site.site_type_id'
            )
            ->leftJoin(
                'building',
                'building.building_id',
                '=',
                'condsurvey.condsurvey_building_id'
            )
            ->leftJoin(
                'building_block',
                'building_block.building_id',
                '=',
                'building.building_id'
            )
            ->leftJoin(
                'building_age',
                'building_age.building_age_id',
                '=',
                'building_block.building_age_id'
            )
            ->leftJoin(
                'contact as surveyor',
                'surveyor.contact_id',
                '=',
                'condsurvey.surveyor'
            )
            ->leftJoin(
                'gen_userdef_group',
                'gen_userdef_group.gen_userdef_group_id',
                '=',
                'condsurvey.gen_userdef_group_id'
            )
            ->leftJoin(
                'contact as  udfContact1',
                'udfContact1.contact_id',
                '=',
                'gen_userdef_group.user_contact1'
            )
            ->leftJoin(
                'contact as  udfContact2',
                'udfContact2.contact_id',
                '=',
                'gen_userdef_group.user_contact2'
            )
            ->leftJoin(
                'contact as  udfContact3',
                'udfContact3.contact_id',
                '=',
                'gen_userdef_group.user_contact3'
            );

        return $query;
    }

    private function filter($query, $inputs)
    {
        $query->whereNotNull('condsurvey.condsurvey_building_id');
    //        $query->whereNotNull('condsurvey.condsurvey_building_block_id');
    //        $query->whereNull('condsurvey.room_id');

        $conditionId = array_get($inputs, 'conditionId', null);

        if (!is_null($conditionId)) {
            $query->where('condsurvey.condsurvey_id', '=', $conditionId);
        }

        $filter = new \Tfcloud\Lib\Filters\ConditionFilter($inputs);

        if (!empty($filter->site_id)) {
            $query->where('condsurvey.site_id', '=', $filter->site_id);
        }
        if (!empty($filter->building_id)) {
            $query->where('condsurvey.condsurvey_building_id', '=', $filter->building_id);
        }
        if (!empty($filter->block_id)) {
            $query->where('condsurvey.condsurvey_building_block_id', '=', $filter->block_id);
        }

        if (!empty($filter->surveyor)) {
            $query->where('condsurvey.surveyor', '=', $filter->surveyor);
        }

        $status = [];
        if (!empty($filter->open)) {
            array_push($status, $filter->open);
        }
        if (!empty($filter->qa)) {
            array_push($status, $filter->qa);
        }
        if (!empty($filter->complete)) {
            array_push($status, $filter->complete);
        }
        if (!empty($filter->historic)) {
            array_push($status, $filter->historic);
        }
        if (!empty($status)) {
            $query->whereIn('condsurvey.condsurvey_status_id', $status);
        }

        if (!empty($filter->code)) {
            $query->where('condsurvey.condsurvey_code', 'like', '%' . trim($filter->code) . '%');
        }

        if (!empty($filter->description)) {
            $query->where('condsurvey.condsurvey_desc', 'like', '%' . $filter->description . '%');
        }

        if (!empty($filter->property_status)) {
            $query->where('building_block.active', '=', $filter->property_status);
        }

        return $query;
    }


    private function allSurveySiteLevel($siteId)
    {
        $select = [
            'condsurvey.condsurvey_id'
        ];
        $query = Condsurvey::select($select)
            ->where('condsurvey.site_id', '=', $siteId)
            ->whereNull('condsurvey.condsurvey_building_id')
            ->whereNull('condsurvey.condsurvey_building_block_id')
            ->orderBy('condsurvey.condsurvey_code', 'ASC');

        return $query;
    }

    private function getPhoto($siteId, $siteGroupId, $sitePhoto)
    {
        if (!$sitePhoto) {
            return false;
        }
        $photoPath = $this->getPhotoStoragePathway(
            $siteGroupId,
            $siteId,
            'site'
        );
        $photoPath = $photoPath . "/" . $sitePhoto;
        if (file_exists($photoPath) && getimagesize($photoPath)) {
            $photoPath = str_replace("/", "\\", $photoPath);
            return $photoPath;
        } else {
            return false;
        }
    }
}
