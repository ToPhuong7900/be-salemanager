<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use iio\libmergepdf\Merger;
use Knp\Snappy\Pdf;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\FileUploads;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\ReportGenerateService;
use ZendPdf\Color\GrayScale;
use ZendPdf\Font;
use ZendPdf\PdfDocument;
use ZendPdf\Style;

class WkHtmlToPdfReportBaseService extends BaseService
{
    protected $permissionService;
    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    /**
     * Generate HTML file to PDF using Wkhtmltopdf with snappy wrapper
     * @param array $arrHtmlFile
     * @param string $generatedPath
     * @param string $outputPdfFile
     * @param array $optionss
     * @param int $pdfFileIndex
     * @param bool $mergeable
     * @return array
     */
    public function generateMultiHtmlToPdf(
        $arrHtmlFile,
        $generatedPath,
        $outputPdfFile,
        $optionss = [],
        &$pdfFileIndex = 1,
        $mergeable = true
    ) {
        $arrPdfReport = [];

        $wkhtmltopdfPath = '"' . \Config::get('cloud.wkhtmltopdf.wkhtmltopdf_exe_path') . '"';
        $pagePerPdf = \Config::get('cloud.wkhtmltopdf.page_per_pdf', 20);

        if (empty($arrHtmlFile)) {
            return $arrPdfReport;
        }

        $arrayChunk = array_chunk($arrHtmlFile, $pagePerPdf);
        $snappy = new Pdf($wkhtmltopdfPath);

        /*
        * Set optionss for wkhtmltopdf
        * */
        $snappy->setOption('enable-local-file-access', true);
        //Extended this timeout as 300 was failing for some reports, with loading times up to 20 mins.
        $snappy->setTimeout(3600);
        $this->setSnappyOptions($snappy, $optionss);

        foreach ($arrayChunk as $arrInputHtml) {
            $reportPath = $generatedPath . '/' . $pdfFileIndex . '.pdf';

            $pdfFile = $this->generatePdf($snappy, $arrInputHtml, $reportPath);

            array_push($arrPdfReport, $reportPath);

            file_put_contents($reportPath, $pdfFile);
            $pdfFileIndex++;
        }

        if ($mergeable) {
            $this->mergerPdf($arrPdfReport, $outputPdfFile);
        }

        return $arrPdfReport;
    }

    /**
     * Use wkhtmltopdf to generate single html file to pdf
     * Options base on http://wkhtmltopdf.org/usage/wkhtmltopdf.txt
     * @param string $htmlFile
     * @param string $outputPdfFile
     * @param array $optionss
     */
    public function generateSingleHtmlToPdf(
        $htmlFile,
        $outputPdfFile,
        $options = []
    ) {
        $wkhtmltopdfPath = '"' . \Config::get('cloud.wkhtmltopdf.wkhtmltopdf_exe_path') . '"';

        /*
        * Generate html to pdf use snappy wrapper
        * */
        $snappy = new Pdf($wkhtmltopdfPath);
        $snappy->setOption('enable-local-file-access', true);
        //Extended this timeout as 300 was failing for some reports, with loading times up to 20 mins.
        $snappy->setTimeout(3600);
        /*
        * Set optionss for wkhtmltopdf
        * */
        $this->setSnappyOptions($snappy, $options);

        $pdfFile = $this->generatePdf($snappy, $htmlFile, $outputPdfFile);

        file_put_contents($outputPdfFile, $pdfFile);
    }

    /**
     * Attempt up to 10 times to convert the html to pdf.
     * We reduce the zoom factor by 0.01 on each attempt.
     *
     * Note. Sometimes simply re-running the the conversion with the same zoom factor will
     * work.  We don't do this though.
     *
     * @param type $snappy
     * @param type $html
     * @param type $reportPath
     * @return type
     * @throws \Exception
     */
    protected function generatePdf($snappy, $html, $reportPath)
    {
        $zoom = 1;
        $snappy->setOption('zoom', $zoom);

        $attemptCount = 1;
        while (true) {
            $error = false;
            try {
                $pdfFile = $snappy->getOutput($html);
            } catch (\Exception $ex) {
                // Error code isn't populated.  Attempt to prevent us handling different exceptions.
                $errText = "The exit status code '-1073741819' says something went wrong:";
                if (!(substr($ex->getMessage(), 0, strlen($errText)) === $errText)) {
                    throw $ex;
                }

                \Log::error("Failed to generate pdf file [$reportPath] using zoom [$zoom]");

                $zoom -= 0.01;
                $snappy->setOption('zoom', $zoom);

                $attemptCount += 1;
                $error = true;

                if ($attemptCount > 10) {
                    throw $ex;
                }

                \Log::error("Re-attempt to generate pdf file [$reportPath] using zoom [$zoom]");
            }

            if (!$error) {
                $snappy->removeTemporaryFiles();
                break;
            }
        }

        return $pdfFile;
    }

    /**
     * @param Pdf $snappy
     * @param $optionss
     */
    protected function setSnappyOptions(Pdf &$snappy, $optionss)
    {
        if (!empty($optionss)) {
            foreach ($optionss as $key => $value) {
                $snappy->setOption($key, $value);
            }
        }
    }

    /**
     * Merger pdf into one file
     *
     * @param $arrPdfReport
     * @param $outputPdfFile
     * @param array $options
     */
    public function mergerPdf($arrPdfReport, $outputPdfFile, $options = [])
    {
        $cssPaging = array_get($options, 'cssPaging', []);
        $isPaging = array_get($options, 'isPaging', true);


        $merger = new Merger();
        $merger->addIterator($arrPdfReport);
        file_put_contents($outputPdfFile, $merger->merge());

        /*
        * Set paging number footer or header for final pdf file
        * */
        $pdf = PdfDocument::load($outputPdfFile);
        if ($isPaging) {
            $this->setPagingHeader($pdf);
            $this->setPagingFooter($pdf, $cssPaging);
        }
        $pdf->save($outputPdfFile);
    }

    /**
     * Set paging number for footer using zendPdf
     *
     * @param PdfDocument $pdf
     * @param array $options
     */
    protected function setPagingFooter(PdfDocument &$pdf, $options = [])
    {
        $fontSize = array_get($options, 'fontSize', '10');
        $font = array_get($options, 'font', Font::FONT_HELVETICA);
        $x = array_get($options, 'x', '450');
        $y = array_get($options, 'y', '50');
        $customY2 = array_get($options, 'customY2', null);
        $totalPage = count($pdf->pages);

        $style = new Style();
        $style->setFillColor(new GrayScale(0));
        $style->setLineWidth(3);
        $fontH = Font::fontWithName($font);
        $style->setFont($fontH, $fontSize);

        foreach ($pdf->pages as $key => $page) {
            $page->setStyle($style);
            $this->setPagingCustom($page, $key, $totalPage, $x, $y, $customY2);
        }
    }

    protected function setPagingCustom(&$page, $key, $totalPage, $x, $y, $customY2 = null)
    {
        $page->drawText("Page " . ($key + 1) . " of $totalPage", $x, $y, 'UTF-8');
    }
    /**
     * Set paging number for header using zendPdf
     * @param PdfDocument $pdf
     */
    protected function setPagingHeader(PdfDocument &$pdf)
    {
    }

    /**
     * Create wkhtmltopdf temp folder to store generated files
     * @param $repId
     * @return string
     */
    public static function generateWkhtmltopdfTempPath($repId)
    {

        $report = \Tfcloud\Models\SystemReport::findOrFail($repId);
        $reportId = $report->getKey();

        $generatedPath = \Config::get('cloud.legacy_paths.generated');
        if (!file_exists($generatedPath)) {
            mkdir($generatedPath, 0755);
        }
        $generatedPath = $generatedPath . '/' .
            \Auth::User()->site_group_id;
        if (!file_exists($generatedPath)) {
            mkdir($generatedPath, 0755);
        }
        $generatedPath = $generatedPath . '/' . \Auth::User()->id;
        if (!file_exists($generatedPath)) {
            mkdir($generatedPath, 0755);
        }
        $generatedPath = $generatedPath . '/' . \Config::get('cloud.wkhtmltopdf.wkhtmltopdf_temp_folder');
        if (!file_exists($generatedPath)) {
            mkdir($generatedPath, 0755);
        }

        $generatedPath = $generatedPath . '/' . $reportId;
        if (!file_exists($generatedPath)) {
            mkdir($generatedPath, 0755);
        }

        /**
         * Generate a unique folder so even if the same user runs the same
         * report from different tabs, there won't be a problem.
         * Date prefix added to make it easier to identify/cleanup old folders.
         */
        $generatedPath = $generatedPath . '/' . uniqid(date('Y-m-d'), true);
        \Log::info("wkhtmltopdf temp path: " . $generatedPath);

        if (!file_exists($generatedPath)) {
            mkdir($generatedPath, 0755);
        }

        return $generatedPath;
    }

    /**
     * Create wkhtmltopdf temp folder to store printed report files
     * @param $moduleCode
     * @param $reportCode
     * @return string
     */
    protected function generateWkhtmltopdfTempPathForPrintReport($moduleCode, $reportCode)
    {
        $generatedPath = \Config::get('cloud.legacy_paths.generated');
        if (!file_exists($generatedPath)) {
            mkdir($generatedPath, 0755);
        }
        $generatedPath = $generatedPath . '/' .
            \Auth::User()->site_group_id;
        if (!file_exists($generatedPath)) {
            mkdir($generatedPath, 0755);
        }
        $generatedPath = $generatedPath . '/' . \Auth::User()->id;
        if (!file_exists($generatedPath)) {
            mkdir($generatedPath, 0755);
        }
        $generatedPath = $generatedPath . '/' . \Config::get('cloud.wkhtmltopdf.wkhtmltopdf_temp_folder');
        if (!file_exists($generatedPath)) {
            mkdir($generatedPath, 0755);
        }
        $generatedPath = $generatedPath . '/' . \Config::get('cloud.wkhtmltopdf.wkhtmltopdf_print_report_temp_folder');
        if (!file_exists($generatedPath)) {
            mkdir($generatedPath, 0755);
        }
        $generatedPath = $generatedPath . '/' . $moduleCode;
        if (!file_exists($generatedPath)) {
            mkdir($generatedPath, 0755);
        }
        $generatedPath = $generatedPath . '/' . $reportCode;
        if (!file_exists($generatedPath)) {
            mkdir($generatedPath, 0755);
        } else {
            /*
            * Delete all old files in wkhtmltopdf folder
            * */
            array_map('unlink', glob("$generatedPath/*.*"));
        }

        return $generatedPath;
    }

    /**
     * @param $reportFileName
     * @return string
     */
    protected function generateReportOutPutPath($reportFileName)
    {
        // Create a reports directory for the site group
        $reportDir = storage_path();
        $reportDir .= '/reports/';
        $reportDir .= \Auth::User()->site_group_id;
        if (!is_dir($reportDir)) {
            mkdir($reportDir, 0755);
        }
        // Create a reports directory for the userid
        $reportDir .= '/' . \Auth::User()->id;
        if (!is_dir($reportDir)) {
            mkdir($reportDir, 0755);
        }

        $cleanReportFileName = \Tfcloud\Lib\File\FileCommon::parseName($reportFileName);

        $reportDir = $reportDir . '/' . $cleanReportFileName . '.pdf';
        return $reportDir;
    }

    /**
     * @param $photoFolder
     * @param $itemKey
     * @param $photoName
     * @param $siteGroupId
     * @param bool $useDefaultPhoto
     * @return null|string
     */
    protected function photoPath($photoFolder, $itemKey, $photoName, $siteGroupId, $useDefaultPhoto = true)
    {
        if ($photoName) {
            return $this->getPhotoStoragePathway($siteGroupId, $itemKey, $photoFolder) . $photoName;
        } elseif ($useDefaultPhoto === false) {
            return null;
        } else {
            return $this->fallBackImagePath($useDefaultPhoto);
        }
    }

    protected function reducedPhotoPath($photoFolder, $itemKey, $photoName, $siteGroupId, $useDefaultPhoto = true)
    {
        $usableSizeBytes = 5000;                   // Over 5K is still acceptable quality for the report

        if ($photoName) {
            $origFilePath = $this->getPhotoStoragePathway($siteGroupId, $itemKey, $photoFolder);
            $newFilePath = $origFilePath . "small";
            $photoPath = $newFilePath . "/" . $photoName;
            if (is_dir($newFilePath) && file_exists($photoPath)) {
                $photoPath = str_replace("/", "\\", $photoPath);
                $size = filesize($photoPath);
                if ($size > $usableSizeBytes) {
                    // Small image still usable
                    return $photoPath;
                } else {
                    //use original photo
                    return $origFilePath . $photoName;
                }
            } else {
                $success = true;

                if (!is_dir($newFilePath)) {
                    $success = mkdir($newFilePath);
                }

                $newImage = $newFilePath . "/" . $photoName;
                $newImage = str_replace("/", "\\", $newImage);
                $imageToCompress = $origFilePath . DIRECTORY_SEPARATOR . $photoName;
                $percent = 0.25;
                $mimeType = mime_content_type($imageToCompress);

                if (
                    $success
                    && $mimeType != "image/jpeg"
                    && $mimeType != "image/jpg"
                    && $mimeType != "image/pjpeg"
                ) {
                    $success = false;
                }

                if ($success) {
                    $size = getimagesize($imageToCompress);
                    if ($size) {
                        list($width, $height) = $size;
                        // Get new sizes
                        $newwidth = $width * $percent;
                        $newheight = $height * $percent;
                    } else {
                        $success = false;
                    }
                }

                // Load
                if ($success) {
                    $thumb = imagecreatetruecolor($newwidth, $newheight);
                    if (!$thumb) {
                        $success = false;
                    }
                }
                if ($success) {
                    $source = imagecreatefromjpeg($imageToCompress);
                    if (!$source) {
                        $success = false;
                    }
                }
                if ($success) {
                    // Resize
                    $success = imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
                }
                if ($success) {
                    // Output
                    $success = imagejpeg($thumb, $newImage, 75);
                }

                if (!$success) {
                    //use original photo
                    return $this->getPhotoStoragePathway($siteGroupId, $itemKey, $photoFolder) . $photoName;
                } else {
                    $size = filesize($newImage);
                    if ($size > $usableSizeBytes) {
                        // Small image still usable
                        return $newImage;
                    } else {
                        //use original photo
                        return $this->getPhotoStoragePathway($siteGroupId, $itemKey, $photoFolder) . $photoName;
                    }
                }
            }
        } elseif ($useDefaultPhoto === false) {
            return null;
        } else {
            return $this->fallBackImagePath($useDefaultPhoto);
        }
    }

    /**
     * @param $siteGroupId
     * @param $itemKey
     * @param $photoFolder
     * @return string
     */
    protected function getPhotoStoragePathway($siteGroupId, $itemKey, $photoFolder)
    {
        $uploadPath = \Config::get('cloud.legacy_paths.uploads');
        $fileUpload = new FileUploads();
        $folder     = rtrim($fileUpload->calculateFolder($itemKey), '/');
        $modelName = $photoFolder . '/' . $folder;

        return $uploadPath . '/' . $siteGroupId . '/' . $modelName . '/' .
        $itemKey . '/';
    }

    /**
     * @param $useDefaultPhoto
     * @return string
     */
    protected function fallBackImagePath($useDefaultPhoto)
    {
        if ($useDefaultPhoto === true) {
            return public_path() . '/assets/images/no-photo.png';
        } else {
            return public_path() . '/assets/images/' . $useDefaultPhoto; // no_user_default.jpg
        }
    }

    protected function getHeaderLogo()
    {
        $siteGroupReportLogo = "reportLogo.jpg";
        $siteGroupReportLogoPath = \Auth::user()->site_group_id . DIRECTORY_SEPARATOR;
        $siteGroupReportLogoPath .= "sitegroup" . DIRECTORY_SEPARATOR . $siteGroupReportLogo;
        $legacyPath = \Config::get('cloud.legacy_paths.uploads');
        $reportLogo = $legacyPath . DIRECTORY_SEPARATOR . $siteGroupReportLogoPath;
        $headerLogo = public_path() . '/assets/images/tfcloud-banner.jpg';
        if (file_exists($reportLogo)) {
            // Create a temporary file and copy the file across onto this machine
            // This is the only way the pdf tool can cope with it.
            $tmpBannerFile = tempnam(Common::getTempFolder(), 'rlg');
            copy($reportLogo, $tmpBannerFile);
            $headerLogo = $tmpBannerFile;
        }
        return $headerLogo;
    }

    public function getFiltering($whereCodes, $orCodes, $whereTexts, &$bFilterDetail, &$filterText)
    {
        ReportGenerateService::reGetFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $filterText);
    }
}
