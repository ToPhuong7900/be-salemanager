<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use Illuminate\Support\Arr;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Math;
use Tfcloud\Lib\Filters\Estate\EstateSalesInvoiceFilter;
use Tfcloud\Models\Report;
use Tfcloud\Services\PermissionService;
use Tfcloud\Models\EstUnitType;
use Tfcloud\Services\Estate\FeeReceiptService;
use Tfcloud\Services\Estate\EstateSalesInvoiceService;

class AccountStatementReportService extends WkHtmlToPdfReportBaseService
{
    private $feeReceiptService;
    private $invoiceService;
    public $filterText = '';
    private $reportBoxFilterLoader;

    public function __construct(PermissionService $permissionService, Report $report)
    {
        parent::__construct($permissionService);
        $this->feeReceiptService = new FeeReceiptService($permissionService);
        $this->invoiceService = new EstateSalesInvoiceService($permissionService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function runReport($repOutPutPdfFile, $inputs, $repId)
    {
        $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
        $inputs = $this->formatInputData($reportFilterQuery);
        $reportData = $this->getData($inputs);

        $data = [];
        foreach ($reportData as $key => $row) {
            if (!isset($data['contacts'][$row->contact_id])) {
                $unallocatedFeeReceipts = $this->getUnallocatedFeeReceipts(['contact_id' => $row->contact_id]);

                $data['contacts'][$row->contact_id] = [
                    'contact_id'   => $unallocatedFeeReceipts->contact_id,
                    'contact'      => Common::concatFields(
                        [$unallocatedFeeReceipts->contact_name, $unallocatedFeeReceipts->organisation]
                    ),
                    'payment_type' => $unallocatedFeeReceipts->payment_type,
                    'amount'       => $unallocatedFeeReceipts->amount
                ];
            }

            /** Add each lease out to the array, identified by the lease out id */
            $data[$row->lease_out_id]['lease_out_code'] = $row->lease_out_code;
            $data[$row->lease_out_id]['contact_id'] = $row->contact_id;
            $data[$row->lease_out_id]['contact'] = Common::concatFields([$row->contact_name, $row->organisation]);
            /** Add each iteration of the query results to our new array */
            $data[$row->lease_out_id]['rows'][$key] = [
                'code'         => $row->code,
                'date'         => $row->amount_date,
                'payment_type' => $row->payment_type,
                'description'  => $row->description,
                'amount'       => $row->amount
            ];

            /** If an total balance amount has been defined, set it up at 0 */
            if (!isset($data[$row->lease_out_id]['total_balance'])) {
                $data[$row->lease_out_id]['total_balance'] = 0.00;
            }

            /** Setup a line balance for each iteration, this will get set by either add or subtracting the amount from
             *  the total balance */
            $data[$row->lease_out_id]['rows'][$key]['line_balance'] = 0.00;

            /** Add the amounts to the balance, as the amounts are both positive and negative we can just add them */
            $data[$row->lease_out_id]['rows'][$key]['line_balance'] = Math::addCurrency(
                [$data[$row->lease_out_id]['total_balance'], $row->amount]
            );

            /** Add the current line balance to the total balance to get the new total */
            $totalBalance = is_array($row->lease_out_id) ? $row->lease_out_id['total_balance'] : null;
            $data[$row->lease_out_id]['total_balance'] = Math::addCurrency([
                $totalBalance,
                $data[$row->lease_out_id]['rows'][$key]['line_balance']
            ]);
        }

        $unallocated = array_pull($data, 'contacts');

        // Create wkhtmltopdf temp folder to store generated files
        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);

        $header = \View::make('reports.wkhtmltopdf.estates.es33.header')->render();
        $headerPath = $generatedPath . "/header.html";
        file_put_contents($headerPath, $header, LOCK_EX);

        $footer = \View::make('reports.wkhtmltopdf.estates.es33.footer')->render();
        $footerPath = $generatedPath . "/footer.html";
        file_put_contents($footerPath, $footer, LOCK_EX);

        $content = \View::make(
            'reports.wkhtmltopdf.estates.es33.content',
            ['data'        => $data,
                'unallocated' => $unallocated]
        )->render();
        $contentPath = $generatedPath . "/content.html";

        file_put_contents($contentPath, $content, LOCK_EX);

        $options = [
            'header-html' => $headerPath,
            'footer-html' => $footerPath,
            'header-spacing' => 4,
            'footer-spacing' => 4,
            'orientation' => 'portrait',
        ];

        $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $options);

        $whereCodes = [];               // The codes which have been used in the filter
        $whereTexts = [];               // Plain text about filtering applied
        $orCodes = [];                  // csv string of OR codes
        $bFilterDetail = false;

        $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
        $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $this->filterText);

        return true;
    }

    private function getUnallocatedFeeReceipts(array $inputs)
    {
        return \DB::table('est_fee_receipt')
            ->join('contact', 'contact.contact_id', '=', 'est_fee_receipt.tenant_contact_id')
            ->leftJoin(
                'est_fee_receipt_invoice',
                'est_fee_receipt_invoice.est_fee_receipt_id',
                '=',
                'est_fee_receipt.est_fee_receipt_id'
            )
            ->where('est_fee_receipt.fully_allocated', 'N')
            ->where('contact.contact_id', array_get($inputs, 'contact_id'))
            ->where('est_fee_receipt.site_group_id', \Auth::user()->site_group_id)->select(
                ['contact.contact_id',
                    'contact.contact_name',
                    'contact.organisation',
                    \DB::raw('"Unallocated" as payment_type'),
                    \DB::raw(
                        '(coalesce(sum(est_fee_receipt.amount_received),0) - ' .
                        ' coalesce(sum(est_fee_receipt_invoice.receipt_value), 0))*-1 as amount'
                    )]
            )->first();
    }

    private function getData(array $inputs)
    {
        $unions = $this->getSalesInvoices()->union($this->getFeeReceipts());

        $query = \DB::table(\DB::raw('(' . $unions->toSql() . ') as union_tables'))
            ->where('site_group_id', \Auth::user()->site_group_id);

        $filter = new EstateSalesInvoiceFilter($inputs);

        if (!is_null($filter->invoiceNo)) {
            $query->where('code', 'like', '%' . $filter->invoiceNo . '%');
        }

        if (!is_null($filter->contact)) {
            $query->where('contact_id', $filter->contact);
        }

        if (!is_null($filter->leaseOutCode)) {
            $query->where('lease_out_code', 'like', '%' . $filter->leaseOutCode . '%');
        }

        if (!is_null($filter->reference)) {
            $query->where('reference', 'like', '%' . $filter->reference . '%');
        }

        if (!is_null($filter->description)) {
            $query->where('description', 'like', '%' . $filter->description . '%');
        }

        if (!is_null($filter->accountId)) {
            $query->where('customer_sales_account_ref', 'like', '%' . $filter->accountId . '%');
        }

        if (!is_null($filter->lease_out_id)) {
            $query->where('lease_out_id', $filter->lease_out_id);
        }

        if (!is_null($filter->taxDateFrom)) {
            $taxDate = \DateTime::createFromFormat('d/m/Y', $filter->taxDateFrom);
            if ($taxDate) {
                $query->where(
                    \DB::raw("DATE_FORMAT(amount_date , '%Y-%m-%d')"),
                    '>=',
                    $taxDate->format('Y-m-d')
                );
            }
        }

        if (!is_null($filter->taxDateTo)) {
            $taxDateTo = \DateTime::createFromFormat('d/m/Y', $filter->taxDateTo);
            if ($taxDateTo) {
                $query->where(
                    \DB::raw("DATE_FORMAT(amount_date, '%Y-%m-%d')"),
                    '<=',
                    $taxDateTo->format('Y-m-d')
                );
            }
        }

        if (!is_null($filter->owner)) {
            $query->where('lease_owner_user_id', $filter->owner);
        }

        if (!is_null($filter->landlord)) {
            $query->where('landlord_contact_id', $filter->landlord);
        }

        if (!is_null($filter->leaseType)) {
            $query->where('lease_type_id', $filter->leaseType);
        }

        if (!is_null($filter->leaseSubType)) {
            $query->where('est_lease_sub_type_id', $filter->leaseSubType);
        }

        $filter->site = !is_null($filter->site) ?
            $filter->site : (!is_null($filter->site_id) ? $filter->site_id : null);
        if (!is_null($filter->site)) {
            $list = \Tfcloud\Models\LeaseOutLetu::leftJoin('lettable_unit', function ($join) {
                $join->on('lease_out_letu.lettable_unit_id', '=', 'lettable_unit.lettable_unit_id')
                    ->where('lettable_unit.est_unit_type_id', '=', EstUnitType::EST_UT_LU);
            })
                ->leftJoin(
                    'lettable_unit_item',
                    'lettable_unit.lettable_unit_id',
                    '=',
                    'lettable_unit_item.lettable_unit_id'
                )
                ->where('lettable_unit.site_group_id', \Auth::user()->site_group_id)
                ->where('lettable_unit_item.site_id', $filter->site)
                ->pluck('lease_out_id')
                ->toArray();

            if (!empty($list)) {
                $query->whereIn("lease_out_id", $list);
            } else {
                $query->whereIn("lease_out_id", ['']);
            }
        }

        if (!is_null($filter->establishment)) {
            $list = \Tfcloud\Models\LeaseOutLetu::leftJoin('lettable_unit', function ($join) {
                $join->on('lease_out_letu.lettable_unit_id', '=', 'lettable_unit.lettable_unit_id')
                    ->where('lettable_unit.est_unit_type_id', '=', EstUnitType::EST_UT_LU);
            })
                ->leftJoin(
                    'lettable_unit_item',
                    'lettable_unit.lettable_unit_id',
                    '=',
                    'lettable_unit_item.lettable_unit_id'
                )
                ->leftJoin(
                    'site',
                    'site.site_id',
                    '=',
                    'lettable_unit_item.site_id'
                )

                ->where('lettable_unit.site_group_id', \Auth::user()->site_group_id)
                ->where('site.establishment_id', $filter->establishment)
                ->pluck('lease_out_id')
                ->toArray();

            if (!empty($list)) {
                $query->whereIn("lease_out_id", $list);
            } else {
                $query->whereIn("lease_out_id", ['']);
            }
        }

        return $query->orderBy('lease_out_id', 'asc')->orderBy('amount_date', 'asc')->orderBy('payment_type', 'asc')
            ->get();
    }

    public function getSalesInvoices()
    {
        return \DB::table('est_sales_invoice')
            ->join('lease_out', 'lease_out.lease_out_id', '=', 'est_sales_invoice.lease_out_id')
            ->join('contact', 'contact.contact_id', '=', 'est_sales_invoice.customer_contact_id')
            ->select([
                'est_sales_invoice.site_group_id as site_group_id',
                'est_sales_invoice.lease_out_id as lease_out_id',
                'lease_out.owner_user_id as lease_owner_user_id',
                'lease_out.landlord_contact_id as landlord_contact_id',
                'lease_out.lease_type_id as lease_type_id',
                'lease_out.est_lease_sub_type_id as est_lease_sub_type_id',
                'lease_out.lease_out_code as lease_out_code',
                'contact.contact_id',
                'contact.sales_acount_ref',
                'contact.contact_name',
                'contact.organisation',
                'est_sales_invoice.est_sales_invoice_code as code',
                'est_sales_invoice.reference as reference',
                'est_sales_invoice.invoice_due_date as amount_date',
                \DB::raw('"Invoice" as payment_type'),
                'est_sales_invoice.est_sales_invoice_desc as description',
                'est_sales_invoice.gross_total as amount',
                \DB::raw('"" as fully_allocated')
            ])
            ->whereRaw(
                'est_sales_invoice.est_sales_invoice_status_id <> '
                . CommonConstant::EST_SALE_INVOICE_STATUS_CANCELED
            );
    }

    public function getFeeReceipts()
    {
        return \DB::table('est_fee_receipt')
            ->join(
                'est_fee_receipt_invoice',
                'est_fee_receipt_invoice.est_fee_receipt_id',
                '=',
                'est_fee_receipt.est_fee_receipt_id'
            )
            ->join('lease_out', 'lease_out.lease_out_id', '=', 'est_fee_receipt_invoice.lease_out_id')
            ->join(
                'est_sales_invoice',
                'est_sales_invoice.est_sales_invoice_id',
                '=',
                'est_fee_receipt_invoice.est_sales_invoice_id'
            )
            ->join('contact', 'contact.contact_id', '=', 'est_sales_invoice.customer_contact_id')
            ->select([
                'est_fee_receipt.site_group_id as site_group_id',
                'est_fee_receipt_invoice.lease_out_id as lease_out_id',

                'lease_out.owner_user_id as lease_owner_user_id',
                'lease_out.landlord_contact_id as landlord_contact_id',
                'lease_out.lease_type_id as lease_type_id',
                'lease_out.est_lease_sub_type_id as est_lease_sub_type_id',
                'lease_out.lease_out_code as lease_out_code',
                'contact.contact_id',
                'contact.sales_acount_ref',
                'contact.contact_name',
                'contact.organisation',
                'est_fee_receipt.est_fee_receipt_code as code',
                'est_sales_invoice.reference as reference',
                'est_fee_receipt.paid_date as amount_date',
                \DB::raw('"Receipt" as payment_type'),
                'est_fee_receipt.est_fee_receipt_desc as description',
                \DB::raw('(est_fee_receipt_invoice.receipt_value)*-1 as amount'),
                'est_fee_receipt.fully_allocated as fully_allocated'
            ]);
    }

    private function formatInputData($reportFilterQuery)
    {
        $inputs['invoiceNo'] = $reportFilterQuery->getFirstValueFilterField('est_sales_invoice_code');
        $inputs['leaseOutCode'] = $reportFilterQuery->getFirstValueFilterField('lease_out_code');
        $inputs['description'] = $reportFilterQuery->getFirstValueFilterField('est_sales_invoice_desc');
        $inputs['establishment'] = $reportFilterQuery->getFirstValueFilterField('establishment_id');
        $inputs['owner'] = $reportFilterQuery->getFirstValueFilterField('owner_user_id');
        $inputs['reference'] = $reportFilterQuery->getFirstValueFilterField('reference');
        $inputs['contact'] = $reportFilterQuery->getFirstValueFilterField('tenant_contact_id');
        $inputs['landlord'] = $reportFilterQuery->getFirstValueFilterField('landlord_contact_id');
        $inputs['site'] = $reportFilterQuery->getFirstValueFilterField('site_id');
        $inputs['leaseType'] = $reportFilterQuery->getFirstValueFilterField('lease_type_id');
        $inputs['leaseSubType'] = $reportFilterQuery->getFirstValueFilterField('est_lease_sub_type_id');

        $taxDate = $reportFilterQuery->getFirstValueFilterField('tax_date');
        $inputs['taxDateFrom'] = Common::dateFieldDisplayFormat(Arr::first($taxDate));
        $inputs['taxDateTo'] = Common::dateFieldDisplayFormat(Arr::last($taxDate));

        return $inputs;
    }

    private function generateFilterData($filterData, &$whereCodes, &$whereTexts, &$orCodes)
    {
        if ($val = Common::iset($filterData['invoiceNo'])) {
            array_push($whereTexts, "Invoice No. contains '{$val}'");
        }

        if ($val = Common::iset($filterData['leaseOutCode'])) {
            array_push($whereTexts, "Lease Out Code contains '{$val}'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Description contains '{$val}'");
        }

        if ($val = Common::iset($filterData['reference'])) {
            array_push($whereTexts, "Reference contains '{$val}'");
        }

        if ($val = Common::iset($filterData['contact'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Tenant']);
        }

        if ($val = Common::iset($filterData['taxDateFrom'])) {
            array_push($whereTexts, "Tax Date From = {$val}");
        }

        if ($val = Common::iset($filterData['taxDateTo'])) {
            array_push($whereTexts, "Tax Date To = {$val}");
        }

        // Special location
        if ($val = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);
        }

        if ($val = array_get($filterData, 'establishment', null)) {
            array_push(
                $whereCodes,
                [
                    'establishment',
                    'establishment_id',
                    'establishment_code',
                    $val,
                    "Establishment"
                ]
            );
        }

        if ($val = Common::iset($filterData['owner'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Owner']);
        }

        if ($val = Common::iset($filterData['landlord_contact_id'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Contact']);
        }

        if ($val = Common::iset($filterData['leaseType'])) {
            array_push($whereCodes, ['lease_type', 'lease_type_id', 'lease_type_code', $val, 'Lease Type']);
        }

        if ($val = Common::iset($filterData['leaseSubType'])) {
            array_push(
                $whereCodes,
                ['est_lease_sub_type', 'est_lease_sub_type_id', 'est_lease_sub_type_code', $val, 'Lease Sub Type']
            );
        }
    }
}
