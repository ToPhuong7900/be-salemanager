<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use Tfcloud\Models\CaseManagement\CmType;
use Tfcloud\Models\GenTable;

class CM03Casemgmt3ProgressService extends BaseCMx03CaseProgressService
{
    protected $cmType = CmType::CM_TYPE_CASEMGMT3;
    protected $gentTable = GenTable::CM3_CASE;
}
