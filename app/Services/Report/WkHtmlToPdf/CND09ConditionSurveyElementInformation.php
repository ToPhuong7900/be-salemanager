<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use DateTime;
use Tfcloud\Controllers\Admin\General\SiteGroupsController;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Math;
use Tfcloud\Models\Building;
use Tfcloud\Models\CondLocationType;
use Tfcloud\Models\Condsurvey;
use Tfcloud\Models\CondLocationScore;
use Tfcloud\Models\CondLocationElementScore;
use Tfcloud\Models\CondsurveyStatus;
use Tfcloud\Models\FinYear;
use Tfcloud\Models\Idwork;
use Tfcloud\Models\IdworkElement;
use Tfcloud\Models\IdworkStatusType;
use Tfcloud\Models\Report;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Admin\General\FinYearService;
use Tfcloud\Services\Admin\General\SiteGroupsService;
use Tfcloud\Services\Condition\ConditionService;

class CND09ConditionSurveyElementInformation extends WkHtmlToPdfReportBaseService
{
    private $condService;
    private $sgController;
    private $sgService;
    public $filterText = '';
    private $reportBoxFilterLoader = null;

    public function __construct(PermissionService $permissionService, Report $report = null)
    {
        parent::__construct($permissionService);
        $this->condService = new ConditionService($permissionService);
        $this->sgService = new SiteGroupsService($permissionService);
        $this->sgController = new SiteGroupsController($permissionService, $this->sgService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function validateFilter($inputs)
    {
        return \Validator::make($this->formatInputs($inputs), $this->validateRules(), $this->validateMessages());
    }

    public function validateRules()
    {
        $this->customValidation();
        return [
            'code_validation' => ['check_configured_fin_years'],
        ];
    }

    public function validateMessages()
    {
        return [
            'code_validation.check_configured_fin_years' =>
                'In order to run this report, at least five financial years must be set up.'
        ];
    }

    public function runReportSingleCondSurvey($id)
    {
        //call report from button not report view

        $inputs['id'] = $id;
        $inputs['singleOnly'] = true;
        $repId = 255;

        return $this->runReport(
            $this->getTempFile(),
            $inputs,
            $repId
        );
    }

    public function runReport($repOutPutPdfFile, $inputs, $repId)
    {
        $inputs = $this->formatInputs($inputs);

        $siteGroupId = \Auth::User()->site_group_id;
        $photoPath = $this->sgController->getPhotoPath($siteGroupId);

        $condQuery = $this->getCondSurveyData($inputs);

        $conditionSurveys = $condQuery->get();
        $surveyCount = $condQuery->get()->count();

        //get all elements for the report
        $idworkElementsQuery = $this->getAllElements($siteGroupId);
        $idworkElements = $idworkElementsQuery->get();

        $foundCondData = "";
        if ($surveyCount > 0) {
            $noFinYear = 'true';
            $foundCondData = 'true';

            // Get all the Fin Year Ids.
            $yearIds = $this->getFinYearIds();

            $currentFinancialYearId = array_get($yearIds, 'currentFinancialYearId', null);
            $backlogCaption = array_get($yearIds, 'backlogCaption', null);
            $currentFinYearCaption = array_get($yearIds, 'currentFinYearCaption', null);
            $year2Caption = array_get($yearIds, 'year2Caption', null);
            $year3Caption = array_get($yearIds, 'year3Caption', null);
            $year4Caption = array_get($yearIds, 'year4Caption', null);
            $year5Caption = array_get($yearIds, 'year5Caption', null);
            $futureCaption = array_get($yearIds, 'futureCaption', null);
            $backlogYears = array_get($yearIds, 'backlogYears', null);
            $year2Id = array_get($yearIds, 'year2Id', null);
            $year3Id = array_get($yearIds, 'year3Id', null);
            $year4Id = array_get($yearIds, 'year4Id', null);
            $year5Id = array_get($yearIds, 'year5Id', null);
            $futureYears = array_get($yearIds, 'futureYears', null);

            foreach ($conditionSurveys as $condSurvey) {
                $buildingRefs = "";
                $gia = 0;
                $id = $condSurvey->site_id;
                $siteImagePath = $this->getPhotoStoragePathway($siteGroupId, $condSurvey->site_id, 'site');
                //pathway checking in wrong folder, redirect
                $splitString = explode("site/", $siteImagePath);


                if ($condSurvey->photo) {
                    $firstHalf = $splitString[0];
                    $sitePhotoPath = $firstHalf . "site/" . $id . "/" . $condSurvey->photo;

                    $condSurvey['hasSiteImg'] = 'true';
                    $condSurvey['sitePhotoPath'] = $sitePhotoPath;
                    //check image exists
                    if (!file_exists($sitePhotoPath)) {
                        //show no picture available file
                        $useDefaultPhoto = true;
                        $sitePhotoPath = $this->fallBackImagePath($useDefaultPhoto);
                        $condSurvey['sitePhotoPath'] = $sitePhotoPath;
                    }
                } else {
                    $condSurvey['hasSiteImg'] = 'false';
                }

                //check if there are any extra aerial images to be included
                $aerialImgQuery = $this->getOtherImagesQuery($condSurvey->site_id);
                $aerialImages = [];
                $aerialImages = $aerialImgQuery->get();
                //get document file path

                if ($aerialImages) {
                    $condSurvey['extraImages'] = 'true';

                    foreach ($aerialImages as $aerialImg) {
                        $filePath = $splitString[0] .
                                '/document/' . $aerialImg->document_id . '/' . $aerialImg->document_name;
                        if (!file_exists($filePath)) {
                            //show no picture available file
                            $useDefaultPhoto = true;
                            $filePath = $this->fallBackImagePath($useDefaultPhoto);
                            $aerialImg['imagePath'] = $filePath;
                        } else {
                            $aerialImg['imagePath'] = $filePath;
                        }
                    }
                    $condSurvey['aerialImages'] = $aerialImages;
                } else {
                    $condSurvey['extraImages'] = 'false';
                }

                //join up available address data into one line
                $condSurvey['addressLine'] = $condSurvey->first_addr_obj . " " . $condSurvey->second_addr_obj
                    . " " . $condSurvey->town . " " . $condSurvey->region . " " . $condSurvey->postcode;

                //get all building refs
                $buildings = CondLocationScore::leftJoin(
                    'building as b1',
                    function ($join) {
                        $join->on('b1.building_id', 'cond_location_score.location_record_id');
                        $join->on(
                            'cond_location_score.cond_location_type_id',
                            \DB::raw(CondLocationType::TYPE_BUILDING)
                        );
                    }
                )
                ->leftJoin(
                    'building_block',
                    function ($join) {
                        $join->on('building_block.building_block_id', 'cond_location_score.location_record_id');
                        $join->on(
                            'cond_location_score.cond_location_type_id',
                            \DB::raw(CondLocationType::TYPE_BLOCK)
                        );
                    }
                )
                ->leftJoin('building as b2', 'b2.building_id', '=', 'building_block.building_id')
                ->where('cond_location_score.condsurvey_id', $condSurvey->condsurvey_id)
                ->whereIn(
                    'cond_location_score.cond_location_type_id',
                    [CondLocationType::TYPE_BUILDING, CondLocationType::TYPE_BLOCK]
                )
                ->distinct()
                ->select(
                    [
                        \DB::raw("coalesce(b1.building_code, b2.building_code) as building_code"),
                        \DB::raw("coalesce(b1.gia, b2.gia) as gia")
                    ]
                );

                $buildingData = $buildings->get();

                foreach ($buildingData as $building) {
                    //build a string to attach to the cond.survey
                    $buildingRefs = $buildingRefs . " " . $building->building_code;

                    if ($building->gia) {
                        $gia = Math::addCurrency([$gia, $building->gia]);
                    } else {
                        $gia = Math::addCurrency([$gia, 0]);
                    }
                }
                $condSurvey['buildingRefs'] = $buildingRefs;
                $condSurvey['gia'] = $gia;

                //get all idworks related to the condsurvey
                $idworksQuery = $this->getIdworks($condSurvey->condsurvey_id);
                $idworks = $idworksQuery->get();
                $condSurvey['idworks'] = $idworks;

                $tableData[0] = 'Element Description';
                $tableData[1] = "Backlog $backlogCaption";
                $tableData[2] = "Year 1 $currentFinYearCaption";
                $tableData[3] = "Year 2 $year2Caption";
                $tableData[4] = "Year 3 $year3Caption";
                $tableData[5] = "Year 4 $year4Caption";
                $tableData[6] = "Year 5 $year5Caption";
                $tableData[7] = "Years 6+ $futureCaption";
                $tableData[8] = 'Element Cost';

                //get all associated elements to populate the codes
                $distinctIdworkElements = IdworkElement::select()
                        ->join(
                            'idwork',
                            'idwork.element_id',
                            '=',
                            'idwork_element.idwork_element_id'
                        )
                        ->where(
                            'idwork.condsurvey_id',
                            '=',
                            $condSurvey->condsurvey_id
                        )
                        ->orderBy('idwork.element_id')
                        ->groupBy('idwork.element_id');
                $distinctIdElements = $distinctIdworkElements->get();


                $i = 0;
                $criticalTotal = $yearOneTotal = $yearTwoTotal = $yearThreeTotal = $yearFourTotal = $yearFiveTotal =
                    $year6PlusTotal = $fullTotal = 0;

                $elementData = [];
                $yearOneTotalData = [];
                $yearTwoData = [];
                $yearThreeData = [];
                $yearFourData = [];
                $yearFiveData = [];
                $sixPlusData = [];
                $elementTotal = [];
                $criticalData = [];

                foreach ($distinctIdElements as $dIdElement) {
                    $criticalData[$i] = 0;
                    $yearOneTotalData[$i] = 0;
                    $yearTwoData[$i] = 0;
                    $yearThreeData[$i] = 0;
                    $yearFourData[$i] = 0;
                    $yearFiveData[$i] = 0;
                    $sixPlusData[$i] = 0;

                    $elementData[$i] = $dIdElement->idwork_element_code . " " . $dIdElement->idwork_element_desc;
                    if ($currentFinancialYearId > 0) {
                        $noFinYear = 'false';

                        // Get Backlog costs.
                        if ($backlogYears) {
                            $backlogQuery = $this->idworkForYears(
                                $dIdElement,
                                $condSurvey,
                                $backlogYears
                            );
                            $criticalData[$i] = $backlogQuery->sum('idwork.total_cost');
                            $criticalTotal = Math::addCurrency([$criticalTotal, $criticalData[$i]]);
                        }

                        // Get data for the first year.
                        $idworkQuery = $this->idworkForYear(
                            $dIdElement,
                            $condSurvey,
                            $currentFinancialYearId
                        );
                        $yearOneTotalData[$i] = $idworkQuery->sum('idwork.total_cost');
                        $yearOneTotal = Math::addCurrency([$yearOneTotal, $yearOneTotalData[$i]]);


                        // Get total for second year.
                        if ($year2Id) {
                            $secondIdworkQuery = $this->idworkForYear($dIdElement, $condSurvey, $year2Id);
                            $yearTwoData[$i] = $secondIdworkQuery->sum('idwork.total_cost');
                            $yearTwoTotal = Math::addCurrency([$yearTwoTotal, $yearTwoData[$i]]);
                        }

                        // Get total for third year.
                        if ($year3Id) {
                            $thirdIdworkQuery = $this->idworkForYear($dIdElement, $condSurvey, $year3Id);
                            $yearThreeData[$i] = $thirdIdworkQuery->sum('idwork.total_cost');
                            $yearThreeTotal = Math::addCurrency([$yearThreeTotal, $yearThreeData[$i]]);
                        }

                        // Get total for Fourth year.
                        if ($year4Id) {
                            $fourthIdworkQuery = $this->idworkForYear($dIdElement, $condSurvey, $year4Id);
                            $yearFourData[$i] = $fourthIdworkQuery->sum('idwork.total_cost');
                            $yearFourTotal = Math::addCurrency([$yearFourTotal, $yearFourData[$i]]);
                        }

                        // Get total for Fifth year.
                        if ($year5Id) {
                            $fifthIdworkQuery = $this->idworkForYear($dIdElement, $condSurvey, $year5Id);
                            $yearFiveData[$i] = $fifthIdworkQuery->sum('idwork.total_cost');
                            $yearFiveTotal = Math::addCurrency([$yearFiveTotal, $yearFiveData[$i]]);
                        }


                        // Get cost for year 6 and beyond
                        if ($futureYears) {
                            $sixthIdwQuery = $this->idworkForYears($dIdElement, $condSurvey, $futureYears);
                            $sixPlusData[$i] = $sixthIdwQuery->sum('idwork.total_cost');
                            $year6PlusTotal = Math::addCurrency([$year6PlusTotal, $sixPlusData[$i]]);
                        }

                        $elementTotal[$i] = Math::addCurrency([
                            $criticalData[$i],
                            $yearOneTotalData[$i],
                            $yearTwoData[$i],
                            $yearThreeData[$i],
                            $yearFourData[$i],
                            $yearFiveData[$i],
                            $sixPlusData[$i]
                        ]);

                        $fullTotal = Math::addCurrency([$fullTotal, $elementTotal[$i]]);
                    } else {
                        $noFinYear = 'true';
                        $idworkQuery = $this->idworkForYear($dIdElement, $condSurvey, 'None');
                        $criticalData[$i] = $idworkQuery->sum('idwork.total_cost');
                        $criticalTotal = Math::addCurrency([$criticalTotal, $criticalData[$i]]);
                        $yearTwoTotal = $yearThreeTotal = $yearFourTotal = $yearFiveTotal =
                            $year6PlusTotal = 0;
                        $fullTotal = Math::addCurrency([$fullTotal, $yearOneTotal]);
                    }
                    $i++;
                } //end foreach distinct element

                $condSurvey['elementCodes'] = $elementData;
                $condSurvey['yearOneData'] = $yearOneTotalData;
                $condSurvey['critical'] = $criticalData;
                $condSurvey['yearTwoData'] = $yearTwoData;
                $condSurvey['yearThreeData'] = $yearThreeData;
                $condSurvey['yearFourData'] = $yearFourData;
                $condSurvey['yearFiveData'] = $yearFiveData;
                $condSurvey['sixPlusData'] = $sixPlusData;
                $condSurvey['totals'] = $elementTotal;
                $condSurvey['fullTotal'] = $fullTotal;

                //totals
                $condSurvey['criticalTotal'] = $criticalTotal;
                $condSurvey['yearOneTotal'] = $yearOneTotal;
                $condSurvey['yearTwoTotal'] = $yearTwoTotal;
                $condSurvey['yearThreeTotal'] = $yearThreeTotal;
                $condSurvey['yearFourTotal'] = $yearFourTotal;
                $condSurvey['yearFiveTotal'] = $yearFiveTotal;
                $condSurvey['yearsSixPlusTotal'] = $year6PlusTotal;

                $siteScores = [];
                $allScores = [];
                $condLocScore = [];
                $condLocScore = CondLocationScore::select()
                    ->where('cond_location_score.condsurvey_id', '=', $condSurvey->condsurvey_id)
                    ->orderBy('cond_location_score.cond_location_score_id', 'desc')
                    ->get();

                $condScoreLocId = $condScoreComment = "";
                $check = 0;
                foreach ($condLocScore as $locScore) {
                    if ($check === 0) {
                        $condScoreLocId = $locScore->cond_location_score_id;
                        $condScoreComment = $locScore->cond_location_score_comment;
                    } else {
                        break;
                    }
                    $check++;
                }
                $condSurvey['condScoreComment'] = $condScoreComment;
                //get the scores for that location
                $siteScores = CondLocationElementScore::select()
                    ->where(
                        'cond_location_element_score.cond_location_score_id',
                        '=',
                        $condScoreLocId
                    )
                    ->join(
                        'idwork_element',
                        'idwork_element.idwork_element_id',
                        '=',
                        'cond_location_element_score.idwork_element_id'
                    )
                    ->orderBy('cond_location_element_score.idwork_element_id')
                    ->get();
                $allScores = \Tfcloud\Models\Score::select()
                    ->orderBy('score.score_id', 'ASC')->get();


                if (count($siteScores) > 0) {
                    $condSurvey['hasScores'] = 'true';

                    foreach ($siteScores as $siteScore) {
                        $count = 1; //start at 1 as N/A will be 0 case
                        foreach ($allScores as $score) {
                            if (($siteScore->score_id === '0') || ($siteScore->score_id == null)) {
                                $siteScore['0'] = 'true';
                            } else {
                                $siteScore['0'] = 'false';
                            }

                            if ($score->score_id === $siteScore->score_id) {
                                $siteScore[$count] = 'true';
                            } else {
                                $siteScore[$count] = 'false';
                            }
                            $count++;
                        } //end foreach all scores
                    } //end foreach all elements
                } else {
                    $condSurvey['hasScores'] = 'false'; //set an N/A flag if there are no scores
                    foreach ($siteScores as $siteScore) {
                        $siteScore[0] = 'true';
                    } //end foreach site score
                }

                $condSurvey['elementScores'] = $siteScores;
                $amountOfScores = count($allScores);
            } //end foreach condsurvey
        } else {
            $foundCondData = 'false';
            $noFinYear = 'false';
            $conditionSurveys = $tableData = $elementData = $amountOfScores = $allScores = "";
        }
        // Create wkhtmltopdf temp folder to store generated files
        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);

        $header = \View::make(
            'reports.wkhtmltopdf.condition.cnd09.header',
            [
                'photoPath' => $photoPath
            ]
        )->render();

        $headerPath = $generatedPath . "/header.html";
        file_put_contents($headerPath, $header, LOCK_EX);

        $footer = \View::make('reports.wkhtmltopdf.condition.cnd09.footer')->render();
        $footerPath = $generatedPath . "/footer.html";
        file_put_contents($footerPath, $footer, LOCK_EX);

        $content = \View::make(
            'reports.wkhtmltopdf.condition.cnd09.content',
            [
                'conditionSurveys' => $conditionSurveys,
                'photoPath'        => $photoPath,
                'expenditureTable' => $tableData,
                'elementData'      => $elementData,
                'amountOfScores'   => $amountOfScores,
                'scores'           => $allScores,
                'foundCondData'    => $foundCondData,
                'noFinancialYear'  => $noFinYear

            ]
        )->render();

        $contentPath = $generatedPath . "/content.html";

        file_put_contents($contentPath, $content, LOCK_EX);

        $whereCodes = [];               // The codes which have been used in the filter
        $whereTexts = [];               // Plain text about filtering applied
        $orCodes = [];                  // csv string of OR codes
        $bFilterDetail = false;

        $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
        $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $this->filterText);

        $options = [
            'header-html' => $headerPath,
            'footer-html' => $footerPath,
            'header-spacing' => 4,
            'footer-spacing' => 4,
            'orientation' => 'portrait',
        ];

        if (array_key_exists('singleOnly', $inputs)) {
            $repOutPutPdfFile .= '.pdf';
            $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $options);
            return $repOutPutPdfFile;
        } else {
            $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $options);
            return true;
        }
    }

    private function getFinYearIds()
    {
        $finYear = new FinYearService($this->permissionService);
        $currentFinYear = $finYear->getFinYearFromDate();
        if (!$currentFinYear) {
            return false;
        }
        $currentFinancialYearId = $currentFinYear->fin_year_id;
        $currentFinYearCaption = $this->getYearColCaption($currentFinYear);

        $backlogYears = $this->getBacklogFinYearIds($currentFinYear->year_start_date);
        $backLogCaption = $this->getYearColCaption($currentFinYear, ['start-only' => true]);

        // Year 2 Costs.
        $date = new DateTime('now');
        $date->modify('1 year');
        $year2 = $finYear->getFinYearFromDate($date);
        if (!$year2) {
            return false;
        }
        $year2Caption = $this->getYearColCaption($year2);

        // Year 3 Costs.
        $date->modify('1 year');
        $year3 = $finYear->getFinYearFromDate($date);
        if (!$year3) {
            return false;
        }
        $year3Caption = $this->getYearColCaption($year3);

        // Year 4 Costs.
        $date->modify('1 year');
        $year4 = $finYear->getFinYearFromDate($date);
        if (!$year4) {
            return false;
        }
        $year4Caption = $this->getYearColCaption($year4);

        // Year 5 Costs.
        $date->modify('1 year');
        $year5 = $finYear->getFinYearFromDate($date);
        if (!$year5) {
            return false;
        }
        $year5Caption = $this->getYearColCaption($year5);

        $years3To5 = array();

        if ($year3->fin_year_id) {
            $years3To5[] = $year3->fin_year_id;
        }

        if ($year4->fin_year_id) {
            $years3To5[] = $year4->fin_year_id;
        }

        if ($year5->fin_year_id) {
            $years3To5[] = $year5->fin_year_id;
        }

        // Year 6 and beyond.
        $futureYears = $this->getFutureFinYearIds($year5->year_start_date);
        $futureCaption = $this->getYearColCaption($year5, ['end-only' => true]);

        return [
            'currentFinancialYearId'      => $currentFinancialYearId,
            'backlogCaption'              => $backLogCaption,
            'currentFinYearCaption'       => $currentFinYearCaption,
            'year2Caption'                => $year2Caption,
            'year3Caption'                => $year3Caption,
            'year4Caption'                => $year4Caption,
            'year5Caption'                => $year5Caption,
            'futureCaption'               => $futureCaption,
            'backlogYears'                => $backlogYears,
            'year2Id'                     => $year2->fin_year_id,
            'year3Id'                     => $year3->fin_year_id,
            'year4Id'                     => $year4->fin_year_id,
            'year5Id'                     => $year5->fin_year_id,
            'years3To5'                   => $years3To5,
            'futureYears'                 => $futureYears
        ];
    }

    private function getYearColCaption($year, $options = [])
    {
        $startOnly = array_get($options, 'start-only', false);
        $endOnly = array_get($options, 'end-only', false);

        $start = $this->getDateFromDbDate($year->year_start_date);
        $end = $this->getDateFromDbDate($year->year_end_date);

        if ($startOnly) {
            return "(...{$start->format('Y')})";
        } elseif ($endOnly) {
            return "({$end->format('Y')}...)";
        } else {
            return "({$start->format('Y')}/{$end->format('Y')})";
        }
    }

    private function getDateFromDbDate($date)
    {
        if ($date) {
            return new \DateTime($date);
        } else {
            return null;
        }
    }
    private function getBacklogFinYearIds($currentYearStart)
    {

        return FinYear::userSiteGroup()
           ->where('year_start_date', '<', $currentYearStart)
           ->pluck('fin_year_id')
           ->toArray();
    }

    private function getFutureFinYearIds($yearStart)
    {

        return FinYear::userSiteGroup()
           ->where('year_start_date', '>', $yearStart)
           ->pluck('fin_year_id')
           ->toArray();
    }

    private function getTempFile()
    {
        return tempnam(Common::getTempFolder(), 'CND09SingleCondSurvey');
    }

    private function idworkForYears($dIdElement, $condSurvey, $years)
    {
        return Idwork::select()
            ->join('idwork_status', 'idwork_status.idwork_status_id', '=', 'idwork.idwork_status_id')
            ->where(
                'idwork.element_id',
                '=',
                $dIdElement->element_id
            )
            ->where(
                'idwork.condsurvey_id',
                '=',
                $condSurvey->condsurvey_id
            )
            ->whereIn(
                'idwork_status.idwork_status_type_id',
                $this->validIWStatusIds()
            )
            ->whereIn('idwork.target_fin_year_id', $years);
    }

    private function idworkForYear($dIdElement, $condSurvey, $finYearId)
    {
        $idworkQuery = Idwork::select()
            ->join('idwork_status', 'idwork_status.idwork_status_id', '=', 'idwork.idwork_status_id')
            ->where(
                'idwork.element_id',
                '=',
                $dIdElement->element_id
            )
            ->where(
                'idwork.condsurvey_id',
                '=',
                $condSurvey->condsurvey_id
            )
            ->whereIn(
                'idwork_status.idwork_status_type_id',
                $this->validIWStatusIds()
            );

        if ($finYearId !== 'None') {
            $idworkQuery->where(
                'idwork.target_fin_year_id',
                '=',
                $finYearId
            );
        }

        return $idworkQuery;
    }

    private function validIWStatusIds()
    {
        return [
            IdworkStatusType::DRAFT,
            IdworkStatusType::PLAN,
            IdworkStatusType::WIP
        ];
    }

    private function getCondSurveyData($inputs)
    {
        $query = Condsurvey::select()
                ->leftjoin(
                    'site',
                    'site.site_id',
                    '=',
                    'condsurvey.site_id'
                )
                ->leftjoin(
                    'user',
                    'user.id',
                    '=',
                    'condsurvey.owner_user_id'
                )
                ->leftjoin(
                    'address',
                    'address.address_id',
                    '=',
                    'site.address_id'
                )
                ->leftjoin(
                    'address_street',
                    'address_street.address_street_id',
                    '=',
                    'address.address_street_id'
                )
                ->leftjoin(
                    'gen_gis',
                    'gen_gis.gen_gis_id',
                    '=',
                    'site.gen_gis_id'
                )
                ->leftjoin(
                    'score',
                    'score.score_id',
                    '=',
                    'condsurvey.score_id'
                );

        if (\Auth::User()->isSelfServiceUser()) {
            $query->where('condsurvey.condsurvey_status_id', '<>', CondsurveyStatus::OPEN)
                ->where('condsurvey.condsurvey_status_id', '<>', CondsurveyStatus::QA);
        }

        if (
            array_key_exists('code', $inputs) &&
            ($inputs['code'] != "") &&
            ($inputs['code'] != " ")
        ) {
            $query->where('condsurvey.condsurvey_code', '=', $inputs['code']);
        }

        if (array_key_exists('singleOnly', $inputs)) {
            $query->where('condsurvey.condsurvey_id', '=', $inputs['id']);
        }

        $query->orderBy('condsurvey.condsurvey_code');

        return $query;
    }

    private function getIdworks($condsurveyId)
    {
        return Idwork::select()
                ->where(
                    'idwork.condsurvey_id',
                    '=',
                    $condsurveyId
                )
                ->leftjoin(
                    'condsurvey',
                    'condsurvey.condsurvey_id',
                    '=',
                    'idwork.condsurvey_id'
                )
                ->leftjoin(
                    'idwork_element',
                    'idwork_element.idwork_element_id',
                    '=',
                    'idwork.element_id'
                );
    }

    private function getAllElements($siteGroupId)
    {
        return IdworkElement::select()
            ->where(
                'site_group_id',
                '=',
                $siteGroupId
            )
            ->where(
                'active',
                '=',
                'Y'
            );
    }

    private function getOtherImagesQuery($siteId)
    {
        return \Tfcloud\Models\Document::select()
            ->leftjoin(
                'doc_record_type',
                'doc_record_type.doc_record_type_id',
                '=',
                'document.doc_record_type_id'
            )
            ->leftjoin(
                'doc_group',
                'doc_group.doc_group_id',
                '=',
                'document.doc_group_id'
            )
            ->where('doc_record_type.doc_record_type_code', '=', 'Property Site')
            ->where('doc_group.doc_group_code', '=', 'Aerial Images')
            ->where('document.record_id', '=', $siteId);
    }

    private function formatInputs($inputs)
    {
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $code = $reportFilterQuery->getValueFilterField('condsurvey_code');
            $inputs['code'] = empty($code) ? null : $code[0];
            $inputs['code_validation'] = 'code';
        }
        return $inputs;
    }

    private function customValidation()
    {
        \Validator::extend('check_configured_fin_years', function ($attribute, $value, $parameters) {
            if (!$this->getFinYearIds()) {
                return false;
            }

            return true;
        });
    }

    private function generateFilterData($filterData, &$whereCodes, &$whereTexts, &$orCodes)
    {
        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Condition Survey Code = '" . $val . "'");
        }
    }
}
