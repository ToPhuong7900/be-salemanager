<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use DateTime;
use Illuminate\Database\Eloquent\Model;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Constant\ReportConstant;
use Tfcloud\Lib\Filters\Project\ProjectFilter;
use Tfcloud\Lib\Traits\EmailTrait;
use Tfcloud\Models\Project;
use Tfcloud\Models\ProjectAction;
use Tfcloud\Models\ProjectChangeRequest;
use Tfcloud\Models\ProjectIssue;
use Tfcloud\Models\ProjectIssueStatus;
use Tfcloud\Models\ProjectProgress;
use Tfcloud\Models\ProjectRecordType;
use Tfcloud\Models\ProjectRiskItem;
use Tfcloud\Models\ProjectStage;
use Tfcloud\Models\Report;
use Tfcloud\Services\NotesService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Project\ProjectAccessService;
use Tfcloud\Services\Project\ProjectActionService;

class ProjectHighLightService extends WkHtmlToPdfReportBaseService
{
    use EmailTrait;

    private $projectActionService;
    private $notesService;
    private $projectAccessService;
    public $filterText = '';
    private $reportBoxFilterLoader = null;

    public function __construct(
        PermissionService $permissionService,
        Report $report = null
    ) {
        parent::__construct($permissionService);
        $this->projectActionService = new ProjectActionService($permissionService);
        $this->notesService = new NotesService($permissionService);
        $this->projectAccessService = new ProjectAccessService($permissionService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function validateFilter($inputs)
    {
        return \Validator::make($this->formatInputs($inputs), $this->validateRules(), $this->validateMessages());
    }

    public function validateRules()
    {
        \Validator::extend('project_required_one', function ($attribute, $value, $parameters) {
            if (is_array($value)) {
                return count($value) == 1;
            }
            return !empty($value);
        });
        $rules = [
            'project' => ['project_required_one'],
        ];

        return $rules;
    }

    public function validateMessages()
    {
        $messages = [
            'project.project_required_one' => 'A single project must be selected on the filter.'
        ];

        return $messages;
    }

    public function runReport($repPdfFile, $inputs, $repId, $options = [])
    {
        $inputs = $this->formatProjectInput($this->formatInputs($inputs));
        // Create wkhtmltopdf temp folder to store generated files
        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);

        $header = \View::make('reports.wkhtmltopdf.project.prj13.header')->render();
        $headerPath = $generatedPath . "/header.html";
        file_put_contents($headerPath, $header, LOCK_EX);

        $footer = \View::make('reports.wkhtmltopdf.project.prj13.footer')->render();
        $footerPath = $generatedPath . "/footer.html";
        file_put_contents($footerPath, $footer, LOCK_EX);

        //Check if run report from project form then create a report file name
        $isPrintReport = array_get($options, 'print-report', false);
        if ($isPrintReport && !$repPdfFile) {
            $projectCode = array_get($options, 'project_code', 'no_code_found');
            $reportFileName = ReportConstant::SYSTEM_REPORT_PRJ13 . '_' . $projectCode;
            $repPdfFile = $this->generateReportOutPutPath($reportFileName);
        }

        //Get Project
        $projectId = array_get($inputs, 'project');
        $project = $this->getProject($projectId);
        if (!$project) {
            return false;
        }

        if (empty($inputs['dateFrom'])) {
            $dateFrom = $project->created_date;
            $inputs['dateFrom'] = $dateFrom;
            $inputs['dateFrom_systemGenerated'] = " (system generated from the project creation date)";
        } else {
            $dateFrom = $inputs['dateFrom'];
        }
        $filter = new ProjectFilter($inputs);

        //Get Project Stage with Notes
        $projectStages = $this->getProjectStages($projectId);
        $projectStageNotes = $this->generateNotes($projectStages, $inputs);

        //Get Project Progress with Notes
        $projectProgress = $this->getProjectProgress($projectId, $filter);
        $projectProgressNotes = $this->generateNotes($projectProgress, $inputs);

        //Get Project Action with Notes
        $projectActions = $this->getProjectAction($projectId, $filter);
        $projectActionNotes = $this->generateNotes($projectActions, $inputs);

        //Get Project Issue with Notes
        $projectIssues = $this->getProjectIssue($projectId, $filter);
        $projectIssueNotes = $this->generateNotes($projectIssues, $inputs);

        //Get Project Change Request with Notes
        $projectChangeRequests = $this->getProjectChangeRequest($projectId);
        $projectChangeRequestNotes = $this->generateNotes($projectChangeRequests, $inputs);

        //Get Project Risk with Notes
        $projectRisks = $this->getProjectRisk($projectId);
        $projectRiskNotes = $this->generateNotes($projectRisks, $inputs);

        $content = \View::make('reports.wkhtmltopdf.project.prj13.content', [
            'project' => $project,
            'dateFrom' => $dateFrom,
            'projectStages' => $projectStages,
            'projectStageNotes' => $projectStageNotes,
            'projectProgress' => $projectProgress,
            'projectProgressNotes' => $projectProgressNotes,
            'projectActions' => $projectActions,
            'projectActionNotes' => $projectActionNotes,
            'projectIssues' => $projectIssues,
            'projectIssueNotes' => $projectIssueNotes,
            'projectChangeRequests' => $projectChangeRequests,
            'projectChangeRequestNotes' => $projectChangeRequestNotes,
            'projectRisks' => $projectRisks,
            'projectRiskNotes' => $projectRiskNotes,
        ])->render();

        $contentPath = $generatedPath . "/content.html";
        file_put_contents($contentPath, $content, LOCK_EX);

        $whereCodes = [];               // The codes which have been used in the filter
        $whereTexts = [];               // Plain text about filtering applied
        $orCodes = [];                  // csv string of OR codes
        $bFilterDetail = false;

        $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
        $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $this->filterText);

        $options = [
            'footer-html' => $footerPath,
            'header-html' => $headerPath,
            'footer-spacing' => 4,
            'header-spacing' => 4,
            'orientation' => 'landscape',
        ];

        $this->generateSingleHtmlToPdf($contentPath, $repPdfFile, $options);
        if (array_get($inputs, 'send_email', false)) {
            $user = \Auth::user();
            $fileName = ReportConstant::SYSTEM_REPORT_PRJ13;
            $logItem = Project::find($projectId);
            $emailData = array(
                'mailTo'        => $user->email_reply_to,
                'displayName'   => $user->siteGroup->email_return_address,
                'repTitle'      => $fileName,
                'subject'       => $fileName . ' Report',
            );
            return $this->sendEmail($repPdfFile, $emailData, $logItem);
        }

        if ($isPrintReport) {
            return $repPdfFile;
        }

        return true;
    }

    private function formatInputs($inputs)
    {
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);

            $projectIds = $reportFilterQuery->getValueFilterField('project_id');
            $dateFrom = $reportFilterQuery->getValueFilterField('date_from');
            $hidePrivateNotes = $reportFilterQuery->getValueFilterField('hide_private_notes');

            $inputs['project'] = empty($projectIds) ? null : $projectIds;
            $inputs['dateFrom'] = empty($dateFrom) ? null : Common::dateFieldDisplayFormat($dateFrom[0]);
            $inputs['hide_private_notes'] = empty($hidePrivateNotes) ? CommonConstant::DATABASE_VALUE_YES :
                $hidePrivateNotes[0];
        }
        return $inputs;
    }

    private function formatProjectInput($inputs)
    {
        if ($projectInput = array_get($inputs, 'project')) {
            $inputs['project'] = is_array($projectInput) && count($projectInput) ? $projectInput[0] : $projectInput;
        }
        return $inputs;
    }

    private function getProject($projectId)
    {
        $query = Project::userSiteGroup()
            ->join('site', 'site.site_id', '=', 'project.site_id')
            ->join('project_status', 'project_status.project_status_id', '=', 'project.project_status_id')
            ->leftjoin('project_stage', 'project.project_stage_id', '=', 'project_stage.project_stage_id')
            ->leftjoin('gen_rag_status', 'gen_rag_status.gen_rag_status_id', '=', 'project.gen_rag_status_id')
            ->select([
                'project.project_code',
                'project.project_desc',
                \DB::raw("date_format(project.created_date,'%d/%m/%Y') AS created_date"),
                'project_status.project_status_code',
                'project_stage.project_stage_status_id',
                'project_stage.project_stage_name',
                'gen_rag_status.gen_rag_status_desc',
                'site.site_code',
                'site.site_desc'
            ])
            ->where('project.project_id', $projectId);
        $query = $this->projectAccessService->projectUserAccess($query, 'project');
        return $query->first();
    }

    private function getProjectStages($projectId)
    {
        $query = ProjectStage::join(
            'project_stage_status',
            'project_stage_status.project_stage_status_id',
            '=',
            'project_stage.project_stage_status_id'
        )
            ->select([
                'project_stage_id',
                'project_stage_code',
                'project_stage_name',
                'project_stage_desc',
                \DB::raw("date_format(project_stage.original_planned_date,'%d/%m/%Y') AS original_planned_date"),
                \DB::raw("date_format(project_stage.current_planned_date,'%d/%m/%Y') AS current_planned_date"),
                \DB::raw("date_format(project_stage.actual_date,'%d/%m/%Y') AS actual_date"),
                'project_stage_status.project_stage_status_code',
            ])->where('project_stage.project_id', $projectId);

        $query->orderBy('project_stage.order', 'asc');

        $projectStages = $query->get();

        foreach ($projectStages as $projectStage) {
            $projectStage->percentage_complete = Common::numberFormat(
                $projectStage->percentageHour()->percentage_complete
            );
        }

        return $projectStages;
    }

    private function getProjectProgress($projectId, ProjectFilter $filter)
    {
        $query = ProjectProgress::join('user', 'user.id', '=', 'project_progress.logged_by_user_id')
            ->select([
                'project_progress_id',
                'project_progress_code',
                'project_progress_desc',
                \DB::raw("date_format(project_progress.progress_date,'%d/%m/%Y') AS progress_date"),
                'user.display_name as created_by'
            ])->where('project_progress.project_id', $projectId);

        if (!is_null($filter->dateFrom)) {
            $dateFrom = DateTime::createFromFormat('d/m/Y', $filter->dateFrom);
            if ($dateFrom) {
                $query->whereRaw(
                    "DATE_FORMAT(project_progress.created_date, '%Y-%m-%d') >= '{$dateFrom->format('Y-m-d')}'"
                );
            }
        }
        $query->orderBy('project_progress.progress_date', 'desc');
        return $query->get();
    }

    private function getProjectAction($projectId, ProjectFilter $filter)
    {
        $query = ProjectAction::select([
            'project_action_id',
            'project_action_code',
            'project_action_desc',
            \DB::raw("date_format(project_action.next_review_date,'%d/%m/%Y') AS next_review_date"),
            \DB::raw("IF(project_action.completed = 'N', 'No', 'Yes') AS completed"),
            \DB::raw("date_format(project_action.completed_date,'%d/%m/%Y') AS completed_date"),
        ])
            ->join('project', function ($join) {
                $join->on('project.project_id', '=', 'project_action.project_id')
                    ->on('project.site_group_id', '=', \DB::raw(\Auth::user()->site_group_id));
            })
            ->where('project_action.project_id', $projectId);

        if (!is_null($filter->dateFrom)) {
            $dateFrom = DateTime::createFromFormat('d/m/Y', $filter->dateFrom);
            if ($dateFrom) {
                $query->whereRaw(
                    "(
                        (project_action.completed = 'N' &&
                        DATE_FORMAT(project_action.created_date, '%Y-%m-%d') >= '{$dateFrom->format('Y-m-d')}') OR
                        (project_action.completed = 'Y' &&
                        DATE_FORMAT(project_action.completed_date, '%Y-%m-%d') >= '{$dateFrom->format('Y-m-d')}')
                    )"
                );
            }
        }

        $query = $this->projectAccessService->projectUserAccessByProjectRecordType(
            $query,
            ProjectRecordType::TYPE_ACTION,
            'project',
            'project_action',
            'project_action_id'
        );
        $query->orderBy('project_action.project_action_code', 'desc');
        return $query->get();
    }

    private function getProjectIssue($projectId, ProjectFilter $filter)
    {
        $query = ProjectIssue::join('user', 'user.id', '=', 'project_issue.owner_user_id')->join(
            'project_issue_status',
            'project_issue_status.project_issue_status_id',
            '=',
            'project_issue.project_issue_status_id'
        )->select([
            'project_issue_id',
            'project_issue_code',
            'project_issue_desc',
            \DB::raw("date_format(project_issue.next_review_date,'%d/%m/%Y') AS next_review_date"),
            \DB::raw("date_format(project_issue.closed_date,'%d/%m/%Y') AS closed_date"),
            'user.display_name as issue_owner',
            'project_issue_status.project_issue_status_code'
        ])
            ->join('project', function ($join) {
                $join->on('project.project_id', '=', 'project_issue.project_id')
                    ->on('project.site_group_id', '=', \DB::raw(\Auth::user()->site_group_id));
            })
            ->where('project_issue.project_id', $projectId);

        if (!is_null($filter->dateFrom)) {
            $dateFrom = DateTime::createFromFormat('d/m/Y', $filter->dateFrom);
            $closedStatusId = ProjectIssueStatus::CLOSED;
            $onHoldStatusId = ProjectIssueStatus::ON_HOLD;
            $openStatusId = ProjectIssueStatus::OPEN;
            if ($dateFrom) {
                $query->whereRaw(
                    "(
                        (project_issue_status.project_issue_status_id = $openStatusId) OR
                        (project_issue_status.project_issue_status_id = $onHoldStatusId) OR
                        (project_issue_status.project_issue_status_id = $closedStatusId &&
                        DATE_FORMAT(project_issue.closed_date, '%Y-%m-%d') >= '{$dateFrom->format('Y-m-d')}')
                    )"
                );
            }
        }
        $query = $this->projectAccessService->projectUserAccessByProjectRecordType(
            $query,
            ProjectRecordType::TYPE_ISSUE,
            'project',
            'project_issue',
            'project_issue_id'
        );
        $query->orderBy('project_issue.project_issue_code', 'asc');
        return $query->get();
    }

    private function getProjectChangeRequest($projectId)
    {
        $query = ProjectChangeRequest::join(
            'project_change_req_status',
            'project_change_req_status.project_change_req_status_id',
            '=',
            'project_change_request.project_change_req_status_id'
        )->leftJoin(
            'project_change_req_type',
            'project_change_req_type.project_change_req_type_id',
            '=',
            'project_change_request.project_change_req_type_id'
        )->leftJoin(
            'contact',
            'contact.contact_id',
            '=',
            'project_change_request.approved_contact_id'
        )->select([
            'project_change_request_id',
            'project_change_request_code',
            'project_change_request_desc',
            'decision',
            'impact_cost',
            'project_change_req_status.project_change_req_status_code',
            'project_change_req_type.project_change_req_type_code',
            'contact.contact_name as approved_by',
        ])->where('project_change_request.project_id', $projectId);

        $query->orderBy('project_change_request.project_change_request_code', 'desc');
        return $query->get();
    }

    private function getProjectRisk($projectId)
    {
        $query = ProjectRiskItem::join('user', 'user.id', '=', 'project_risk_item.owner_user_id')
        ->join('project', function ($join) {
            $join->on('project.project_id', '=', 'project_risk_item.project_id')
                ->on('project.site_group_id', '=', \DB::raw(\Auth::user()->site_group_id));
        })
        ->leftJoin(
            'project_risk_likelihood',
            'project_risk_likelihood.project_risk_likelihood_id',
            '=',
            'project_risk_item.project_risk_likelihood_id'
        )->leftJoin(
            'project_risk_severity',
            'project_risk_severity.project_risk_severity_id',
            '=',
            'project_risk_item.project_risk_severity_id'
        )->leftJoin(
            'project_risk_type',
            'project_risk_type.project_risk_type_id',
            '=',
            'project_risk_item.project_risk_type_id'
        )->select([
            'project_risk_item_id',
            'project_risk_item_code',
            'activity',
            'project_risk_likelihood.project_risk_likelihood_code',
            'project_risk_severity.project_risk_severity_code',
            'degree_of_risk',
            'risk_control_measures',
            'user.display_name as owner',
            'project_risk_type.project_risk_type_code',
            'project_risk_type.project_risk_type_desc',
        ])->where('project_risk_item.project_id', $projectId);

        $query = $this->projectAccessService->projectUserAccessByProjectRecordType(
            $query,
            ProjectRecordType::TYPE_RISK,
            'project',
            'project_risk_item',
            'project_risk_item_id'
        );
        $query->orderBy('project_risk_item.project_risk_item_code', 'desc');
        return $query->get();
    }


    private function getNotes(Model $parentModel, $inputs = null)
    {
        $query = $this->notesService->getNotesByParent($parentModel);
        if ($inputs['hide_private_notes'] == CommonConstant::DATABASE_VALUE_YES) {
            $query->where('internal_note', CommonConstant::DATABASE_VALUE_NO);
        }
        $query->select([
            'updated_date',
            'detail',
            'author_user_id'
            ])->orderBy('updated_date', 'desc');

        return $query->get();
    }

    private function generateNotes($parentModels, $inputs = null)
    {
        $arrayNotes = [];
        foreach ($parentModels as $model) {
            $notes = $this->getNotes($model, $inputs);

            if ($notes->count()) {
                $arrayNotes = array_add($arrayNotes, $model->getKey(), $notes);
            }
        }
        return $arrayNotes;
    }

    private function generateFilterData($filterData, &$whereCodes, &$whereTexts, &$orCodes)
    {
        if ($val = array_get($filterData, 'project')) {
            array_push(
                $whereCodes,
                ['project', 'project_id', 'project_code', $val, 'Project']
            );
        }

        if ($val = array_get($filterData, 'dateFrom', null)) {
            $add = array_get($filterData, 'dateFrom_systemGenerated', '');
            array_push($whereTexts, "Date from '$val'$add");
        }

        if ($val = array_get($filterData, 'hide_private_notes', null)) {
            $text = '';
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_NO:
                    $text = \Lang::get('text.no');
                    break;
                case CommonConstant::DATABASE_VALUE_YES:
                    $text = \Lang::get('text.yes');
                    break;
                default:
                    break;
            }
            if (!empty($text)) {
                array_push($whereTexts, "Hide Private Notes = '" . $text . "'");
            }
        }
    }
}
