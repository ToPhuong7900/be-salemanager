<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use DateTime;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Models\AsbRoomStatus;
use Tfcloud\Models\AsbSurvey;
use Tfcloud\Models\AsbSurveyStatus;
use Tfcloud\Models\Report;
use Tfcloud\Models\Site;
use Tfcloud\Services\PermissionService;

class AsbestosReportForBradfordService extends WkHtmlToPdfReportBaseService
{
    private $pageIndex = 1;
    private $siteId = null;
    private $buildingIds = [];
    public $filterText = '';
    protected $reportBoxFilterLoader;
    protected $reportFilterQuery;

    public function __construct(PermissionService $permissionService, Report $report = null)
    {
        $this->permissionService = $permissionService;
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function runReport($repOutPutPdfFile, $inputs, $repId)
    {
        $data = $this->getReportData($inputs);

        if ($data === false) {
            return false;
        }

        // Create wkhtmltopdf temp folder to store generated files
        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);

        $header = \View::make('reports.wkhtmltopdf.asbestos.bradford.header', $data)->render();
        $headerFile = $generatedPath . "/header.html";
        file_put_contents($headerFile, $header, LOCK_EX);

        $footer = \View::make('reports.wkhtmltopdf.asbestos.bradford.footer')->render();
        $footerFile = $generatedPath . "/footer.html";
        file_put_contents($footerFile, $footer, LOCK_EX);

        $content = \View::make('reports.wkhtmltopdf.asbestos.bradford.content', $data)->render();
        $contentFile = $generatedPath . "/" . $this->pageIndex . '.html';
        file_put_contents($contentFile, $content, LOCK_EX);

        $options = [
            'footer-html' => $footer,
            'header-html' => $header,
            'orientation' => 'landscape',
            'margin-top'  => 50,
            'margin-bottom' => 30,
            'margin-left' => 3,
            'margin-right' => 3,
        ];

        $pdf = $this->generateMultiHtmlToPdf(
            [$contentFile],
            $generatedPath,
            $repOutPutPdfFile,
            $options,
            $this->pageIndex,
            true
        );

        $whereCodes = [];               // The codes which have been used in the filter
        $whereTexts = [];               // Plain text about filtering applied
        $orCodes = [];                  // csv string of OR codes
        $bFilterDetail = false;

        $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
        $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $this->filterText);

        return true;
    }

    public function validateFilter($inputs)
    {
        $inputs = $this->formatInputs($inputs);
        return \Validator::make($inputs, $this->validateRules(), $this->validateMessages());
    }

    public function validateRules()
    {
        \Validator::extend('one_location', function ($attribute, $value, $parameters) {
            if ($value && is_array($value)) {
                return count($value) == 1;
            }
            $required = isset($parameters[0]) ? $parameters[0] : false;
            if ($required) {
                return !empty($value);
            }
            return true;
        });

        $rules = [
            'site_id' => ['one_location:required'],
            'building_id' => ['one_location'],
        ];

        return $rules;
    }

    public function validateMessages()
    {
        $messages = [
            'one_location' => 'Only 1 location can be selected for this report.',
        ];

        return $messages;
    }

    private function formatInputs($inputs)
    {
        $newInputs = [];
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $this->reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $siteId = $this->reportFilterQuery->getValueFilterField('site_id');
            $buildingId = $this->reportFilterQuery->getValueFilterField('building_id');
            $newInputs['site_id'] = $siteId ?: [];
            $newInputs['building_id'] = $buildingId ?: [];
        }

        return $newInputs ?: $inputs;
    }

    private function getReportData($inputs)
    {
        $data = [];

        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $this->siteId = $this->reportFilterQuery->getValueFilterField('site_id')[0];
            $this->buildingIds = $this->reportFilterQuery->getValueFilterField('building_id');
            $this->filterText = $this->reportFilterQuery->getFilterDetailText();
        } else {
            $this->siteId = $inputs['site_id'];
        }
        // get site property details
        $site = $this->getSite();

        if (!$site) {
            return false;
        }

        // header date
        $data['date'] = (new DateTime())->format('d/m/Y');

        // header property
        $data['property'] = $site->property_code . " - " . $site->property;

        // get surveys for location
        $asbSurveys = $this->getAsbSurveys();

        // header survey type and job number
        if ($asbSurveys->count()) {
            $data['asbSurveyData'] = true;
            $data['survey_type'] = $asbSurveys[0]->survey_type;
            $data['job_number'] = $asbSurveys[0]->job_number;
        } else {
            $data['asbSurveyData'] = false;
            $data['survey_type'] = '';
            $data['job_number'] = '';
        }

        // survey data
        $data['asbSurveys'] = $asbSurveys;

        return $data;
    }

    private function getSite()
    {
        return Site::select([
            'site.site_code as property_code',
            'site.site_desc as property',
        ])
        ->where(
            'site.site_id',
            '=',
            $this->siteId
        )
        ->first();
    }

    private function getAsbSurveys()
    {
        $query = $this->getNoRoom();

        $query = $query->union(
            $this->getRoom()->getQuery()
        );

        $query = $query->union(
            $this->getNoHazard()->getQuery()
        );

        $query = $query->union(
            $this->getNoAccess()->getQuery()
        );

        $querySql = $query->toSql();

        $query = \DB::table(\DB::raw("($querySql) as a"))
            ->mergeBindings($query->getQuery());

        $query->orderBy('block', 'ASC');
        $query->orderByRaw("case when plan_no = '' then 1 else plan_no end ASC");

        $asbSurveys = $query->get();

        return $asbSurveys;
    }

    private function getNoRoom()
    {
        $query = AsbSurvey::select([
            \DB::raw("1 as record_type"),
            'building.building_code as block',
            'asb_survey.asb_survey_code as job_number',
            'asb_survey_type.survey_type_desc as survey_type',
            \DB::raw("'' as floor"),
            \DB::raw("'' as plan_no"),
            'asb_hazard.asb_hazard_position as general_location',
            \DB::raw("'Y' as access"),
            'asb_hazard.asb_hazard_desc as immediate_location',
            'asb_material_type.asb_material_type_desc as form_of_material',
            \DB::raw("case when asb_sample.asb_asbestos_type_id is not null then "
                . "asb_sample_asbestos_type.asb_asbestos_type_desc "
                . "when asb_hazard.asb_asbestos_type_id is not null then "
                . "asb_hazard_asbestos_type.asb_asbestos_type_desc "
                . "else 'NAD' end as type_of_asbestos"),
            \DB::raw("coalesce(asb_sample.asb_sample_code,'Presumed') as sample_no"),
            \DB::raw("case when asb_hazard.unit_of_measure_id is null then "
                . "asb_hazard.asb_hazard_quantity "
                . "else "
                . "concat(asb_hazard.asb_hazard_quantity, ' - ', unit_of_measure.unit_of_measure_desc) "
                . "end as approx_qty"),
            'gen_userdef_sel1.gen_userdef_sel_code as risk_category',
            'asb_hazard.asb_hazard_comment as comments',
            \DB::raw("case when asb_hazard.asb_hazard_comment like '%no access%' then"
                . "'no-access' else '' end as no_access"),
        ])
        ->join(
            'asb_survey_type',
            'asb_survey_type.survey_type_id',
            '=',
            'asb_survey.survey_type_id'
        )
        ->join(
            'building',
            'building.building_id',
            '=',
            'asb_survey.building_id'
        )
        ->join(
            'site',
            'site.site_id',
            '=',
            'asb_survey.site_id'
        )
        ->join(
            'asb_survey_hazard',
            'asb_survey_hazard.asb_survey_id',
            '=',
            'asb_survey.asb_survey_id'
        )
        ->join(
            'asb_hazard',
            'asb_hazard.asb_hazard_id',
            '=',
            'asb_survey_hazard.asb_hazard_id'
        )
        ->join(
            'asb_hazard_status',
            'asb_hazard_status.asb_hazard_status_id',
            '=',
            'asb_hazard.asb_hazard_status_id'
        )
        ->leftjoin(
            'asb_material_type',
            'asb_material_type.asb_material_type_id',
            '=',
            'asb_hazard.asb_material_type_id'
        )
        ->leftjoin(
            'asb_sample',
            'asb_sample.asb_sample_id',
            '=',
            'asb_hazard.asb_sample_id'
        )
        ->leftjoin(
            'asb_asbestos_type as asb_sample_asbestos_type',
            'asb_sample_asbestos_type.asb_asbestos_type_id',
            '=',
            'asb_sample.asb_asbestos_type_id'
        )
        ->leftjoin(
            'asb_asbestos_type as asb_hazard_asbestos_type',
            'asb_hazard_asbestos_type.asb_asbestos_type_id',
            '=',
            'asb_hazard.asb_asbestos_type_id'
        )
        ->leftjoin(
            'unit_of_measure',
            'unit_of_measure.unit_of_measure_id',
            '=',
            'asb_hazard.unit_of_measure_id'
        )
        ->leftjoin(
            'gen_userdef_group',
            'gen_userdef_group.gen_userdef_group_id',
            '=',
            'asb_hazard.gen_userdef_group_id'
        )
        ->leftJoin(
            'gen_userdef_sel as gen_userdef_sel1',
            'gen_userdef_sel1.gen_userdef_sel_id',
            '=',
            'gen_userdef_group.sel1_gen_userdef_sel_id'
        )
        ->userSiteGroup()
        ->where(
            'asb_survey.survey_status_id',
            '=',
            AsbSurveyStatus::CURRENT
        )
        ->whereNull(
            'asb_hazard.room_id'
        )
        ->where(
            'asb_hazard_status.asb_hazard_status_code',
            '<>',
            'REMOVED'
        )
        ->where(
            'site.site_id',
            '=',
            $this->siteId
        );

        if ($this->buildingIds) {
            $query->whereIn(
                'building.building_id',
                $this->buildingIds
            );
        }

        return $query;
    }

    private function getRoom()
    {
        $query = AsbSurvey::select([
            \DB::raw("2 as record_type"),
            'building.building_code as block',
            'asb_survey.asb_survey_code as job_number',
            'asb_survey_type.survey_type_desc as survey_type',
            \DB::raw("case when left(room.room_number, 1) = 'E' then "
                . "'External' else room_floor.room_floor_desc end as floor"),
            'room.room_number as plan_no',
            \DB::raw("concat(coalesce(room.room_desc,''), ' ',"
                . "coalesce(asb_hazard.asb_hazard_position,'')) as general_location"),
            \DB::raw("'Y' as access"),
            'asb_hazard.asb_hazard_desc as immediate_location',
            'asb_material_type.asb_material_type_desc as form_of_material',
            \DB::raw("case when asb_sample.asb_asbestos_type_id is not null then "
                . "asb_sample_asbestos_type.asb_asbestos_type_desc "
                . "when asb_hazard.asb_asbestos_type_id is not null then "
                . "asb_hazard_asbestos_type.asb_asbestos_type_desc "
                . "else 'NAD' end as type_of_asbestos"),
            \DB::raw("coalesce(asb_sample.asb_sample_code,'Presumed') as sample_no"),
            \DB::raw("case when asb_hazard.unit_of_measure_id is null then "
                . "asb_hazard.asb_hazard_quantity "
                . "else "
                . "concat(asb_hazard.asb_hazard_quantity, ' - ', unit_of_measure.unit_of_measure_desc) "
                . "end as approx_qty"),
            'gen_userdef_sel1.gen_userdef_sel_code as risk_category',
            'asb_hazard.asb_hazard_comment as comments',
            \DB::raw("case when asb_hazard.asb_hazard_comment like '%no access%' then"
                . "'no-access' else '' end as no_access"),
        ])
        ->join(
            'asb_survey_type',
            'asb_survey_type.survey_type_id',
            '=',
            'asb_survey.survey_type_id'
        )
        ->join(
            'building',
            'building.building_id',
            '=',
            'asb_survey.building_id'
        )
        ->join(
            'site',
            'site.site_id',
            '=',
            'asb_survey.site_id'
        )
        ->join(
            'room',
            'room.building_id',
            '=',
            'building.building_id'
        )
        ->join(
            'asb_survey_hazard',
            'asb_survey_hazard.asb_survey_id',
            '=',
            'asb_survey.asb_survey_id'
        )
        ->join('asb_hazard', function ($join) {
            $join->on('asb_hazard.asb_hazard_id', '=', 'asb_survey_hazard.asb_hazard_id');
            $join->on('asb_hazard.site_id', '=', 'site.site_id');
            $join->on('asb_hazard.building_id', '=', 'building.building_id');
            $join->on('asb_hazard.room_id', '=', 'room.room_id');
        })
        ->join(
            'asb_hazard_status',
            'asb_hazard_status.asb_hazard_status_id',
            '=',
            'asb_hazard.asb_hazard_status_id'
        )
        ->leftjoin(
            'asb_material_type',
            'asb_material_type.asb_material_type_id',
            '=',
            'asb_hazard.asb_material_type_id'
        )
        ->leftjoin(
            'asb_sample',
            'asb_sample.asb_sample_id',
            '=',
            'asb_hazard.asb_sample_id'
        )
        ->leftjoin(
            'asb_asbestos_type as asb_sample_asbestos_type',
            'asb_sample_asbestos_type.asb_asbestos_type_id',
            '=',
            'asb_sample.asb_asbestos_type_id'
        )
        ->leftjoin(
            'asb_asbestos_type as asb_hazard_asbestos_type',
            'asb_hazard_asbestos_type.asb_asbestos_type_id',
            '=',
            'asb_hazard.asb_asbestos_type_id'
        )
        ->leftjoin(
            'unit_of_measure',
            'unit_of_measure.unit_of_measure_id',
            '=',
            'asb_hazard.unit_of_measure_id'
        )
        ->leftjoin(
            'gen_userdef_group',
            'gen_userdef_group.gen_userdef_group_id',
            '=',
            'asb_hazard.gen_userdef_group_id'
        )
        ->leftJoin(
            'gen_userdef_sel as gen_userdef_sel1',
            'gen_userdef_sel1.gen_userdef_sel_id',
            '=',
            'gen_userdef_group.sel1_gen_userdef_sel_id'
        )
        ->leftjoin(
            'room_floor',
            'room_floor.room_floor_id',
            '=',
            'room.room_floor_id'
        )
        ->userSiteGroup()
        ->where(
            'asb_survey.survey_status_id',
            '=',
            AsbSurveyStatus::CURRENT
        )
        ->where(
            'room.active',
            '=',
            CommonConstant::DATABASE_VALUE_YES
        )
        ->where(
            'asb_hazard_status.asb_hazard_status_code',
            '<>',
            'REMOVED'
        )
        ->where(
            'site.site_id',
            '=',
            $this->siteId
        );

        if ($this->buildingIds) {
            $query->whereIn(
                'building.building_id',
                $this->buildingIds
            );
        }

        return $query;
    }

    private function getNoHazard()
    {
        $query = AsbSurvey::select([
            \DB::raw("3 as record_type"),
            'building.building_code as block',
            'asb_survey.asb_survey_code as job_number',
            'asb_survey_type.survey_type_desc as survey_type',
            \DB::raw("case when left(room.room_number, 1) = 'E' then "
                . "'External' else room_floor.room_floor_desc end as floor"),
            'room.room_number as plan_no',
            'room.room_desc as general_location',
            \DB::raw("'Y' as access"),
            \DB::raw("'AAE' as immediate_location"),
            \DB::raw("'' as form_of_material"),
            \DB::raw("'NSM' as type_of_asbestos"),
            \DB::raw("'' as sample_no"),
            \DB::raw("'' as approx_qty"),
            \DB::raw("'' as risk_category"),
            'asb_room.asb_room_comment as comments',
            \DB::raw("case when asb_room.asb_room_comment like '%no access%' then"
                . "'no-access' else '' end as no_access"),
        ])
        ->join(
            'asb_survey_type',
            'asb_survey_type.survey_type_id',
            '=',
            'asb_survey.survey_type_id'
        )
        ->join(
            'building',
            'building.building_id',
            '=',
            'asb_survey.building_id'
        )
        ->join(
            'site',
            'site.site_id',
            '=',
            'asb_survey.site_id'
        )
        ->join(
            'room',
            'room.building_id',
            '=',
            'building.building_id'
        )
        ->join('asb_room', function ($join) {
            $join->on('asb_room.asb_survey_id', '=', 'asb_survey.asb_survey_id');
            $join->on('asb_room.room_id', '=', 'room.room_id');
            $join->where('asb_room_status_id', '=', AsbRoomStatus::SURVEYED);
        })
        ->leftjoin(
            'room_floor',
            'room_floor.room_floor_id',
            '=',
            'room.room_floor_id'
        )
        ->userSiteGroup()
        ->where(
            'asb_survey.survey_status_id',
            '=',
            AsbSurveyStatus::CURRENT
        )
        ->where(
            'room.active',
            '=',
            CommonConstant::DATABASE_VALUE_YES
        )
        ->whereNotExists(function ($notExists) {
            $notExists->select(\DB::raw(1))
            ->from('asb_survey_hazard')
            ->join(
                'asb_hazard',
                'asb_hazard.asb_hazard_id',
                '=',
                'asb_survey_hazard.asb_hazard_id'
            )
            ->whereRaw('asb_survey_hazard.asb_survey_id = asb_survey.asb_survey_id')
            ->whereRaw('asb_hazard.site_id = site.site_id')
            ->whereRaw('asb_hazard.building_id = building.building_id')
            ->whereRaw('asb_hazard.room_id = room.room_id');
        })
        ->where(
            'site.site_id',
            '=',
            $this->siteId
        );

        if ($this->buildingIds) {
            $query->whereIn(
                'building.building_id',
                $this->buildingIds
            );
        }

        return $query;
    }

    private function getNoAccess()
    {
        $query = AsbSurvey::select([
            \DB::raw("4 as record_type"),
            'building.building_code as block',
            'asb_survey.asb_survey_code as job_number',
            'asb_survey_type.survey_type_desc as survey_type',
            \DB::raw("case when left(room.room_number, 1) = 'E' then "
                . "'External' else room_floor.room_floor_desc end as floor"),
            'room.room_number as plan_no',
            'room.room_desc as general_location',
            \DB::raw("'N' as access"),
            \DB::raw("'' as immediate_location"),
            \DB::raw("'' as form_of_material"),
            \DB::raw("'' as type_of_asbestos"),
            \DB::raw("'' as sample_no"),
            \DB::raw("'' as approx_qty"),
            \DB::raw("'' as risk_category"),
            'asb_room.asb_room_comment as comments',
            \DB::raw("case when asb_room.asb_room_comment like '%no access%' then"
                . "'no-access' else '' end as no_access"),
        ])
        ->join(
            'asb_survey_type',
            'asb_survey_type.survey_type_id',
            '=',
            'asb_survey.survey_type_id'
        )
        ->join(
            'building',
            'building.building_id',
            '=',
            'asb_survey.building_id'
        )
        ->join(
            'site',
            'site.site_id',
            '=',
            'asb_survey.site_id'
        )
        ->join(
            'room',
            'room.building_id',
            '=',
            'building.building_id'
        )
        ->join('asb_room', function ($join) {
            $join->on('asb_room.asb_survey_id', '=', 'asb_survey.asb_survey_id');
            $join->on('asb_room.room_id', '=', 'room.room_id');
            $join->whereIn(
                'asb_room.asb_room_status_id',
                [
                    AsbRoomStatus::NO_ACCESS,
                    AsbRoomStatus::LIMITED_ACCESS
                ]
            );
        })
        ->leftjoin(
            'room_floor',
            'room_floor.room_floor_id',
            '=',
            'room.room_floor_id'
        )
        ->userSiteGroup()
        ->where(
            'asb_survey.survey_status_id',
            '=',
            AsbSurveyStatus::CURRENT
        )
        ->where(
            'room.active',
            '=',
            CommonConstant::DATABASE_VALUE_YES
        )
        ->where(
            'site.site_id',
            '=',
            $this->siteId
        );

        if ($this->buildingIds) {
            $query->whereIn(
                'building.building_id',
                $this->buildingIds
            );
        }

        return $query;
    }

    private function generateFilterData($filterData, &$whereCodes, &$whereTexts, &$orCodes)
    {
        if ($siteId = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $siteId, "Site Code"]);

            if ($buildingId = Common::iset($filterData['building_id'])) {
                array_push(
                    $whereCodes,
                    ['building', 'building_id', 'building_code', $buildingId, "Building Code"]
                );
            }
        }
    }
}
