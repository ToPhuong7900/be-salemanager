<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use Illuminate\Support\Arr;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\BoxFilterQuery\IReportFilterQuery;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\ReportConstant;
use Tfcloud\Lib\Math;
use Tfcloud\Lib\Traits\EmailTrait;
use Tfcloud\Models\Contact;
use Tfcloud\Models\ContractInvoice;
use Tfcloud\Models\CpContract;
use Tfcloud\Models\CpSalesCredit;
use Tfcloud\Models\CpSalesCreditStatus;
use Tfcloud\Models\CpSalesInvoice;
use Tfcloud\Models\CpSalesInvoiceStatus;
use Tfcloud\Models\DloJobStatusType;
use Tfcloud\Models\DloSalesInvoice;
use Tfcloud\Models\DloSalesInvoiceStatusType;
use Tfcloud\Models\InspectionSFG20\InspSFG20InspectionStatusType;
use Tfcloud\Models\InstructionParentType;
use Tfcloud\Models\InstructionStatusType;
use Tfcloud\Models\InvoiceFinAccount;
use Tfcloud\Models\InvoiceStatusType;
use Tfcloud\Models\Report;
use Tfcloud\Services\PermissionService;

class CPC04CostPlusSalesInvoiceStatementService extends WkHtmlToPdfReportBaseService
{
    use EmailTrait;

    /*
     * Tfcloud\Services\CostPlus\CpSalesInvoiceLineService
     */
    public $filterText = '';
    private $reportFilterQuery = null;
    private $reportBoxFilterLoader = '';

    public function __construct(
        PermissionService $permissionService,
        Report $report = null
    ) {
        parent::__construct($permissionService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function runReport($repPdfFile, $inputs, $repId, $options = [])
    {
        $repCode = '';
        // Create wkhtmltopdf temp folder to store generated files
        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);

        $header = \View::make('reports.wkhtmltopdf.costplus.cpc04.header', [])
            ->render();
        $headerPath = $generatedPath . "/header.html";
        file_put_contents($headerPath, $header, LOCK_EX);

        $footer = \View::make('reports.wkhtmltopdf.costplus.cpc04.footer')->render();
        $footerPath = $generatedPath . "/footer.html";
        file_put_contents($footerPath, $footer, LOCK_EX);

        $isPrintReport = array_get($options, 'print-report', false);
        if ($isPrintReport && !$repPdfFile) {
            $cpContractCode = array_get($options, 'cp_contract_code', 'no_code_found');
            $reportCode = $inputs['report_code'];

            $reportFileName = $reportCode . '_' . $cpContractCode;
            $repPdfFile = $this->generateReportOutPutPath($reportFileName);
        }
        $inputs = $this->formatInput($inputs);
        $data = $this->getReportData($inputs);

        if ($data === false) {
            return false;
        }

        if ($inputs['report_code'] == ReportConstant::SYSTEM_REPORT_CPC04) {
            $view = 'reports.wkhtmltopdf.costplus.cpc04.content';
        } else {
            $view = 'reports.wkhtmltopdf.costplus.cpc05.content';
        }

        $content = \View::make($view, [
            'repData' => $data,
            'cpContract' => array_get($data, 'cpContract')
        ])->render();

        $contentPath = $generatedPath . "/content.html";
        file_put_contents($contentPath, $content, LOCK_EX);

        $whereCodes = [];               // The codes which have been used in the filter
        $whereTexts = [];               // Plain text about filtering applied
        $orCodes = [];                  // csv string of OR codes
        $bFilterDetail = false;

        if ($this->reportFilterQuery instanceof IReportFilterQuery) {
            $this->filterText = $this->reportFilterQuery->getFilterDetailText();
        } else {
            $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
            $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $this->filterText);
        }

        $generateOptions = [
            'header-spacing' => 4,
            'footer-spacing' => 4
        ];

        $this->generateSingleHtmlToPdf($contentPath, $repPdfFile, $generateOptions);
        if (array_get($inputs, 'send_email', false)) {
            $user = \Auth::user();

            $fileName = $reportCode;
            $id = $data['cpContract']['cp_contract_id'];
            $emailData = array(
                'mailTo'        => $user->email_reply_to,
                'displayName'   => $user->siteGroup->email_return_address,
                'repTitle'      => $fileName,
                'subject'       => $fileName . ' Report',
            );
            $logItem = CpContract::find($id);
            return $this->sendEmail($repPdfFile, $emailData, $logItem);
        }
        if ($isPrintReport) {
            return $repPdfFile;
        }

        return true;
    }

    public function validateFilter($inputs)
    {
        $inputs = $this->formatInput($inputs);
        return \Validator::make($inputs, $this->validateRules(), $this->validateMessages());
    }

    public function validateRules()
    {
        return [
            'cpContract' => ['required'],
        ];
    }

    public function validateMessages()
    {
        return [
            'cpContract.required' => 'Cost Plus Contract is required.'
        ];
    }

    private function getReportData($params = [])
    {
        $outputData = [];
        $cpContractId = array_get($params, 'cpContract');

        // Get Contract
        $query = CpContract::userSiteGroup()->where(
            'cp_contract.cp_contract_id',
            $cpContractId
        );
        $cpContract = $query->first();
        if (!$cpContract) {
            return false;
        }

        $reactiveUpliftPercentage = $cpContract->reactive_uplift_percentage ?: 0;
        $plannedUpliftPercentage  = $cpContract->planned_uplift_percentage ?: 0;

        $outputData['cpContract'] = $cpContract;
        $outputData['headerSection'] = $this->getContactAddressForReport(
            $cpContract->customer_contact_id
        );

        // Get all contract items for this contract id - helpcalls, inspections,
        // ad hoc instructions and contract instructions.
        $cpContractItems = $this->getCpContractItems($cpContract);

        // Build report lines based on each contract item
        $lines = [];
        $total = [];
        $total['commitment_sub'] = 0;
        $total['paid_sub'] = 0;
        $total['commitment_reactive'] = 0;
        $total['commitment_planned'] = 0;
        $total['paid_reactive'] = 0;
        $total['paid_planned'] = 0;

        foreach ($cpContractItems as $cpContractItem) {
            $line = [];

            // get invoice data
            $invoice = $this->getInvoiceDetails($cpContractItem);

            // set report line data
            $line['date']           = $cpContractItem->date;
            $line['reference']      = $cpContractItem->reference;
            $line['description']    = $cpContractItem->description;
            $line['commitment']     = $invoice['commitment'];
            $line['paid']           = $invoice['paid'];
            $lines [] = $line;

            // add to sub totals
            $total['commitment_sub'] += $line['commitment'];
            $total['paid_sub']       += $line['paid'];

            // total reactive or planned cost in order to calculate fee totals
            $total['commitment_' . $cpContractItem->cost_type]
                += $line['commitment'];
            $total['paid_' . $cpContractItem->cost_type]
                += $line['paid'];
        }

        // Calculate Fee total for commitment
        $total['commitment_fee'] = Math::addCurrency([
            Math::mulCurrency([
                $total['commitment_reactive'],
                Math::percentageAsDecimal($reactiveUpliftPercentage)
            ]),
            Math::mulCurrency([
                $total['commitment_planned'],
                Math::percentageAsDecimal($plannedUpliftPercentage)
            ])
        ]);

        // Calculate Fee total for paid
        $total['paid_fee'] = Math::addCurrency([
            Math::mulCurrency([
                $total['paid_reactive'],
                Math::percentageAsDecimal($reactiveUpliftPercentage)
            ]),
            Math::mulCurrency([
                $total['paid_planned'],
                Math::percentageAsDecimal($plannedUpliftPercentage)
            ])
        ]);

        // Retrieve management fees and vat from sales invoices for this
        // contract. Also gets summary data for the CPC05 report.
        $salesInvoice = $this->getSalesInvoiceDetails($cpContractId);

        // The sales invoice totals give the paid amounts only, commitment
        // amounts are unknown.
        $total['commitment_management_fee'] = 0;
        $total['paid_management_fee'] = $salesInvoice['total_management_fee'];
        $total['commitment_vat'] = 0;
        $total['paid_vat'] = $salesInvoice['total_vat'];

        // Calculate Net total for commitment
        $total['commitment_net'] = Math::addCurrency([
            $total['commitment_sub'],
            $total['commitment_fee'],
            $total['commitment_management_fee'],
        ]);

        // Calculate Net total for paid
        $total['paid_net'] = Math::addCurrency([
            $total['paid_sub'],
            $total['paid_fee'],
            $total['paid_management_fee'],

        ]);

        // Calculate Gross total for commitment
        $total['commitment'] = Math::addCurrency([
            $total['commitment_net'],
            $total['commitment_vat'],
        ]);

        // Calculate Gross total for paid
        $total['paid'] = Math::addCurrency([
            $total['paid_net'],
            $total['paid_vat'],
        ]);

        // Set report data for CPC04
        $outputData['lines'] = $lines;
        $outputData['totalCommitmentSub']   = $total['commitment_sub'];
        $outputData['totalPaidSub']         = $total['paid_sub'];
        $outputData['totalCommitmentFee']   = $total['commitment_fee'];
        $outputData['totalPaidFee']         = $total['paid_fee'];
        $outputData['totalCommitmentManagementFee']
            = $total['commitment_management_fee'];
        $outputData['totalPaidManagementFee'] = $total['paid_management_fee'];
        $outputData['totalCommitmentNet']   = $total['commitment_net'];
        $outputData['totalPaidNet']         = $total['paid_net'];
        $outputData['totalCommitmentVat']   = $total['commitment_vat'];
        $outputData['totalPaidVat']         = $total['paid_vat'];
        $outputData['totalCommitment']      = $total['commitment'];
        $outputData['totalPaid']            = $total['paid'];

        // Total commitment and paid for CPC05
        $totalValueOfWork = Math::addCurrency([
            $total['commitment_sub'],
            $total['paid_sub'],
        ]);

        // Total fees
        $totalFees = Math::addCurrency([
            $total['commitment_fee'],
            $total['paid_fee'],
        ]);

        // Total management fees
        $totalManagementFees = Math::addCurrency([
            $total['commitment_management_fee'],
            $total['paid_management_fee'],
        ]);

        // Total Vat
        $totalVat = Math::addCurrency([
            $total['commitment_vat'],
            $total['paid_vat'],
        ]);

        // Retrieve sales credits total for this contract.
        $salesCredit = $this->getSalesCreditData($cpContractId);

        // Set report data for CPC05
        $outputData['summary'] = [
            'totalValueOfWork'      => $totalValueOfWork,
            'totalFees'             => $totalFees,
            'totalManagementFees'   => $totalManagementFees,
            'totalVat'              => $totalVat,
            'previouslyCharged'     => $salesInvoice['previously_charged'],
            'creditedTotal'        => $salesCredit['credited_total'],
            'balanceNowDue'         => $salesInvoice['balance_due'],
        ];

        return $outputData;
    }

    private function getContactAddressForReport($contactId)
    {
        $headerSections = [
            'contactName' => '',
            'contactOrg' => '',
            'contactAddress' => '',
            'contactPostCode' => '',
        ];
        $contact = Contact::userSiteGroup()
            ->leftJoin('address', 'address.address_id', '=', 'contact.address_id')
            ->leftJoin('address_street', 'address_street.address_street_id', '=', 'address.address_street_id')
            ->where('contact_id', $contactId)
            ->select([
                'contact.contact_name',
                'contact.organisation',
                'address.second_addr_obj',
                'address.first_addr_obj',
                'address.postcode',
                'address_street.street',
                'address_street.locality',
                'address_street.town',
                'address_street.region'
            ])->first();

        if ($contact) {
            $headerSections['contactName'] = $contact->contact_name;
            $headerSections['contactOrg'] = $contact->organisation;
            $headerSections['contactPostCode'] = $contact->postcode;
            $arrAddress = [
                $contact->second_addr_obj,
                $contact->first_addr_obj,
                $contact->street,
                $contact->locality,
                $contact->town,
                $contact->region
            ];
            $headerSections['contactAddress'] = implode(', ', array_filter($arrAddress));
        }

        return $headerSections;
    }

    /**
     * Gets all related items for this Cost Plus contract in date order
     */
    private function getCpContractItems($cpContract)
    {
        $query = $this->getHelpcalls($cpContract);

        $query = $query->union(
            $this->getInstructions($cpContract)->getQuery()
        );

        $query = $query->union(
            $this->getInspections($cpContract)->getQuery()
        );

        $query = $query->union(
            $this->getDloJobs($cpContract)->getQuery()
        );

        $querySql = $query->toSql();

        $query = \DB::table(\DB::raw("($querySql) as a"))
            ->mergeBindings($query->getQuery());

        $query->orderBy('date', 'ASC');

        $cpContractItems = $query->get();

        return $cpContractItems;
    }

    private function getDloJobs($cpContract)
    {
        $cpContractId = $cpContract->getKey();

        $query = CpContract::userSiteGroup()
        ->join(
            'cp_contract_item',
            'cp_contract_item.cp_contract_id',
            '=',
            'cp_contract.cp_contract_id'
        )
        ->join(
            'dlo_job',
            'dlo_job.dlo_job_id',
            '=',
            'cp_contract_item.dlo_job_id'
        )
        ->join(
            'dlo_job_status',
            'dlo_job_status.dlo_job_status_id',
            '=',
            'dlo_job.dlo_job_status_id'
        )->join(
            'dlo_sales_invoice',
            'dlo_sales_invoice.parent_id',
            '=',
            'dlo_job.dlo_job_id'
        )
        ->where(
            'cp_contract.cp_contract_id',
            '=',
            $cpContractId
        )
        ->where(
            'dlo_job_status.dlo_job_status_type_id',
            '<>',
            DloJobStatusType::CANCELLED
        )
        ->select([
            \DB::raw('"dlo-job" AS item_type'),
            'dlo_job.dlo_job_id AS id',
            \DB::raw('CASE dlo_job.dlo_parent_type_id WHEN 2 THEN "planned" ELSE "reactive" END AS cost_type'),
            \DB::raw("DATE_FORMAT(dlo_job.created_on, '%d/%m/%Y') AS date"),
            'dlo_job.dlo_job_code AS reference',
            'dlo_job.dlo_job_desc AS description',
            \DB::raw(
                '(SELECT sum(labour_total + material_total + plant_total + subcontract_total) '
                    . 'FROM dlo_sales_invoice '
                    . 'INNER JOIN dlo_sales_invoice_status '
                    . 'ON dlo_sales_invoice_status.dlo_sales_invoice_status_id = '
                    . 'dlo_sales_invoice.dlo_sales_invoice_status_id '
                    . 'WHERE dlo_sales_invoice.parent_id = dlo_job.dlo_job_id '
                    . 'AND dlo_sales_invoice_status_type_id <> ' . DloSalesInvoiceStatusType::CANCELLED
                . ') AS estimate_value'
            ),
            \DB::raw('NULL AS contract_invoice_actual'),
            \DB::raw('NULL AS contract_invoice_id'),
            \DB::raw(
                "CASE " .
                "WHEN dlo_job_status.dlo_job_status_type_id = " .
                DloJobStatusType::WORKS_COMP .
                " THEN 1 " .
                "WHEN dlo_job_status.dlo_job_status_type_id = " .
                DloJobStatusType::WORKS_EXP_COMP .
                " THEN 1 " .
                "WHEN dlo_job_status.dlo_job_status_type_id = " .
                DloJobStatusType::POSTED .
                " THEN 1 " .
                "ELSE 0 " .
                "END AS closed"
            ),
        ]);

        return $query;
    }

    /**
     * Gets all related Help Calls for this Cost Plus contract (not cancelled).
     * The selected columns must match getInstructions and getInspections.
     */
    private function getHelpcalls($cpContract)
    {
        $cpContractId = $cpContract->getKey();

        $query = CpContract::userSiteGroup()
        // Get contract items for contract
        ->join(
            'cp_contract_item',
            'cp_contract_item.cp_contract_id',
            '=',
            'cp_contract.cp_contract_id'
        )
        // Get helpcalls linked to contract
        ->join(
            'helpcall',
            'helpcall.helpcall_id',
            '=',
            'cp_contract_item.helpcall_id'
        )
        // Get instructions linked to helpcall
        ->join(
            'instruction',
            function ($join) {
                $join->on(
                    'instruction.parent_id',
                    '=',
                    'helpcall.helpcall_id'
                )
                ->on(
                    'instruction.parent_type_id',
                    '=',
                    \DB::raw(InstructionParentType::HELPCALL)
                );
            }
        )
        // Get instruction status
        ->join(
            'instruction_status',
            'instruction_status.instruction_status_id',
            '=',
            'instruction.instruction_status_id'
        )
        // Get instruction status type
        ->join(
            'instruction_status_type',
            'instruction_status_type.instruction_status_type_id',
            '=',
            'instruction_status.instruction_status_type_id'
        )
        ->where(
            'cp_contract.cp_contract_id',
            '=',
            $cpContractId
        )
        ->where(
            'instruction_status_type.instruction_status_type_id',
            '<>',
            InstructionStatusType::CANCELLED
        )
        ->select([
            \DB::raw('"instruction" AS item_type'),
            'instruction.instruction_id AS id',
            \DB::raw('"reactive" AS cost_type'),
            \DB::raw("DATE_FORMAT(instruction.created_on, '%d/%m/%Y') AS date"),
            'instruction.instruction_code AS reference',
            'instruction.instruction_desc AS description',
            'instruction.est_net_total AS estimate_value',
            \DB::raw('NULL AS contract_invoice_actual'),
            \DB::raw('NULL AS contract_invoice_id'),
            \DB::raw(
                "CASE " .
                "WHEN instruction_status_type.instruction_status_type_id = " .
                InstructionStatusType::COMPLETE .
                " THEN 1 " .
                "WHEN instruction_status_type.instruction_status_type_id = " .
                InstructionStatusType::CLOSED .
                " THEN 1 " .
                "ELSE 0 " .
                "END AS closed"
            ),
        ]);

        return $query;
    }

    /**
     * Gets all related Instructions for this Cost Plus contract (not cancelled).
     * Ad Hoc Instructions and Contract Instructions.
     * The selected columns must match getHelpcalls and getInspections.
     */
    private function getInstructions($cpContract)
    {
        $cpContractId = $cpContract->getKey();

        $query = CpContract::userSiteGroup()
        // Get contract items for contract
        ->join(
            'cp_contract_item',
            'cp_contract_item.cp_contract_id',
            '=',
            'cp_contract.cp_contract_id'
        )
        // Get instructions linked to contract
        ->join(
            'instruction',
            'instruction.instruction_id',
            '=',
            'cp_contract_item.instruction_id'
        )
        // Get instruction status
        ->join(
            'instruction_status',
            'instruction_status.instruction_status_id',
            '=',
            'instruction.instruction_status_id'
        )
        // Get instruction status type
        ->join(
            'instruction_status_type',
            'instruction_status_type.instruction_status_type_id',
            '=',
            'instruction_status.instruction_status_type_id'
        )
        ->where(
            'cp_contract.cp_contract_id',
            '=',
            $cpContractId
        )
        ->where(
            'instruction_status_type.instruction_status_type_id',
            '<>',
            InstructionStatusType::CANCELLED
        )
        ->select([
            \DB::raw('"instruction" AS item_type'),
            'instruction.instruction_id AS id',
            \DB::raw(
                "CASE " .
                "WHEN instruction.parent_type_id = " .
                InstructionParentType::NONE .
                " THEN 'reactive' " .
                "WHEN instruction.parent_type_id  = " .
                InstructionParentType::CONTRACT .
                " THEN 'planned' " .
                "ELSE 'reactive' " .
                "END AS cost_type"
            ),
            \DB::raw(
                "DATE_FORMAT(instruction.created_on, '%d/%m/%Y') AS date"
            ),
            'instruction.instruction_code AS reference',
            'instruction.instruction_desc AS description',
            \DB::raw('COALESCE(instruction.est_net_total,0) AS estimate_value'),
            \DB::raw('NULL AS contract_invoice_actual'),
            \DB::raw('NULL AS contract_invoice_id'),
            \DB::raw(
                "CASE " .
                "WHEN instruction_status_type.instruction_status_type_id = " .
                InstructionStatusType::COMPLETE .
                " THEN 1 " .
                "WHEN instruction_status_type.instruction_status_type_id = " .
                InstructionStatusType::CLOSED .
                " THEN 1 " .
                "ELSE 0 " .
                "END AS closed"
            ),
        ]);

        return $query;
    }

    /**
     * Gets all related Inspections for this Cost Plus contract (not cancelled).
     * The selected columns must match getHelpcalls and getInstructions.
     */
    private function getInspections($cpContract)
    {
        $cpContractId = $cpContract->getKey();

        $query = CpContract::userSiteGroup()
        // Get contract items for contract
        ->join(
            'cp_contract_item',
            'cp_contract_item.cp_contract_id',
            '=',
            'cp_contract.cp_contract_id'
        )
        // Get inspections linked to contract
        ->join(
            'insp_sfg20_scheduled_inspection',
            'insp_sfg20_scheduled_inspection.insp_sfg20_scheduled_inspection_id',
            '=',
            'cp_contract_item.insp_sfg20_scheduled_inspection_id'
        )
        ->leftJoin(
            'contract_instruction',
            'contract_instruction.contract_instruction_id',
            '=',
            'insp_sfg20_scheduled_inspection.contract_instruction_id'
        )
        // Get inspection status
        ->join(
            'inspection_status',
            'inspection_status.inspection_status_id',
            '=',
            'insp_sfg20_scheduled_inspection.inspection_status_id'
        )
        // Get inspection status type
        ->join(
            'inspection_status_type',
            'inspection_status_type.inspection_status_type_id',
            '=',
            'inspection_status.inspection_status_type_id'
        )
        ->where(
            'cp_contract.cp_contract_id',
            '=',
            $cpContractId
        )
        ->where(
            'inspection_status_type.inspection_status_type_id',
            '<>',
            InspSFG20InspectionStatusType::CANCELLED
        )
        ->select([
            \DB::raw('"scheduledInspection" AS item_type'),
            'insp_sfg20_scheduled_inspection.insp_sfg20_scheduled_inspection_id AS id',
            \DB::raw('"planned" AS cost_type'),
            \DB::raw(
                "DATE_FORMAT(COALESCE(insp_sfg20_scheduled_inspection.completed_date, "
                . "insp_sfg20_scheduled_inspection.inspection_due_date), '%d/%m/%Y') AS date"
            ),
            'insp_sfg20_scheduled_inspection.insp_sfg20_scheduled_inspection_code AS reference',
            'contract_instruction.contract_instruction_desc AS description',
            \DB::raw(
                '(COALESCE(insp_sfg20_scheduled_inspection.labour_cost,0) + '
                . 'COALESCE(insp_sfg20_scheduled_inspection.material_cost,0) + '
                . 'COALESCE(insp_sfg20_scheduled_inspection.other_cost,0)) '
                . 'AS estimate_value'
            ),
            'insp_sfg20_scheduled_inspection.contract_invoice_actual',
            'insp_sfg20_scheduled_inspection.contract_invoice_id',
            \DB::raw(
                "CASE " .
                "WHEN inspection_status_type.inspection_status_type_id = " .
                InspSFG20InspectionStatusType::COMPLETE .
                " THEN 1 " .
                "WHEN inspection_status_type.inspection_status_type_id = " .
                InspSFG20InspectionStatusType::CLOSED .
                " THEN 1 " .
                "ELSE 0 " .
                "END AS closed"
            ),
        ]);

        return $query;
    }

    /**
     * Gets the related invoice details for a contract item.
     */
    private function getInvoiceDetails($cpContractItem)
    {
        $invoice = [];
        $sumInvoiceAmounts = 0;
        $allInvoicesApproved = false;

        // Get invoices for this item
        if ($cpContractItem->item_type == 'instruction') {
            $invoiceResults = $this->getInvoices($cpContractItem);
            $sumInvoiceAmounts = $invoiceResults['sum_approved_amounts'];
            $allInvoicesApproved = $invoiceResults['all_approved'];
        } elseif ($cpContractItem->item_type == 'scheduledInspection') {
            $invoiceResults = $this->getContractInvoices($cpContractItem);
            $sumInvoiceAmounts = $invoiceResults['sum_approved_amounts'];
            $allInvoicesApproved = $invoiceResults['all_approved'];
        } elseif ($cpContractItem->item_type == 'dlo-job') {
            $invoiceResults = $this->getDloSalesInvoices($cpContractItem);
            $sumInvoiceAmounts = $invoiceResults['sum_approved_amounts'];
            $allInvoicesApproved = $invoiceResults['all_approved'];
        }

        // 'Paid' is the sum of all approved invoice amounts
        $invoice['paid'] = $sumInvoiceAmounts;

        // 'Commitment' is the estimated amount minus 'Paid' amount.
        // If the contract item is marked as closed and all invoices
        // are set to approved then commitment will be zero.
        if ($cpContractItem->closed && $allInvoicesApproved) {
            $invoice['commitment'] = 0;
        } else {
            $invoice['commitment'] = Math::subCurrency(
                [$cpContractItem->estimate_value, $invoice['paid']]
            );
            // Don't allow commitment to be negative
            if ($invoice['commitment'] < 0) {
                $invoice['commitment'] = 0;
            }
        }

        return $invoice;
    }

    private function getDloSalesInvoices($cpContractItem)
    {
        $dloSalesInvoices = DloSalesInvoice::join(
            'dlo_sales_invoice_status',
            'dlo_sales_invoice_status.dlo_sales_invoice_status_id',
            '=',
            'dlo_sales_invoice.dlo_sales_invoice_status_id'
        )
        ->where(
            'dlo_sales_invoice.parent_id',
            '=',
            $cpContractItem->id
        )
        ->select(
            \DB::raw('labour_total + material_total + plant_total + subcontract_total AS total'),
            'dlo_sales_invoice_status.dlo_sales_invoice_status_type_id'
        )
        ->get();

        $sumAmounts = 0;
        $allApproved = true;

        foreach ($dloSalesInvoices as $invoice) {
            if (
                $invoice->dlo_sales_invoice_status_type_id == DloSalesInvoiceStatusType::APPROVED ||
                $invoice->dlo_sales_invoice_status_type_id == DloSalesInvoiceStatusType::PAID ||
                $invoice->dlo_sales_invoice_status_type_id == DloSalesInvoiceStatusType::POSTED
            ) {
                $sumAmounts = Math::addCurrency(
                    [$sumAmounts, $invoice->total]
                );
            } else {
                $allApproved = false;
            }
        }

        $results = [
            'sum_approved_amounts'  => $sumAmounts,
            'all_approved'          => $allApproved,
        ];

        return $results;
    }

    /**
     * Gets relevant invoice data for an instruction.
     */
    private function getInvoices($cpContractItem)
    {
        $invoices = InvoiceFinAccount::join(
            'invoice',
            'invoice.invoice_id',
            '=',
            'invoice_fin_account.invoice_id'
        )
        ->join(
            'invoice_status',
            'invoice_status.invoice_status_id',
            '=',
            'invoice.invoice_status_id'
        )
        // Get all invoices linked to an instruction
        ->where(
            'invoice_fin_account.instruction_id',
            '=',
            $cpContractItem->id
        )
        ->select(
            'invoice_fin_account.amount',
            'invoice_status.invoice_status_type_id'
        )
        ->get();

        $sumAmounts = 0;
        $allApproved = true;

        foreach ($invoices as $invoice) {
            if ($invoice->invoice_status_type_id == InvoiceStatusType::APPROVED) {
                $sumAmounts = Math::addCurrency(
                    [$sumAmounts, $invoice->amount]
                );
            } else {
                $allApproved = false;
            }
        }

        $results = [
            'sum_approved_amounts'  => $sumAmounts,
            'all_approved'          => $allApproved,
        ];

        return $results;
    }

    /**
     * Gets relevant contract invoice data for an inspection.
     */
    private function getContractInvoices($cpContractItem)
    {
        $invoice = ContractInvoice::join(
            'contract_invoice_status',
            'contract_invoice_status.contract_invoice_status_id',
            '=',
            'contract_invoice.contract_invoice_status_id'
        )
        // Get contract invoice linked to an inspection (a single invoice)
        ->where(
            'contract_invoice.contract_invoice_id',
            '=',
            $cpContractItem->contract_invoice_id
        )
        ->select(
            'contract_invoice_status.invoice_status_type_id'
        )
        ->first();

        $sumAmounts = 0;
        $allApproved = true;

        if (
            $invoice &&
            $invoice->invoice_status_type_id == InvoiceStatusType::APPROVED
        ) {
            $sumAmounts = Math::addCurrency(
                [$sumAmounts, $cpContractItem->contract_invoice_actual]
            );
        } else {
            $allApproved = false;
        }

        $results = [
            'sum_approved_amounts'  => $sumAmounts,
            'all_approved'          => $allApproved,
        ];

        return $results;
    }

    /**
     * Gets relevant details from sales invoices for this contract.
     * Management fees and vat details are obtained from sales invoices.
     */
    private function getSalesInvoiceDetails($cpContractId)
    {
        $cpSalesInvoices = CpSalesInvoice::userSiteGroup()
        ->where(
            'cp_sales_invoice.cp_contract_id',
            '=',
            $cpContractId
        )
        // get all approved status invoices only
        ->where(
            'cp_sales_invoice.cp_sales_invoice_status_id',
            '=',
            CpSalesInvoiceStatus::APPROVED
        )
        ->get();

        $totalManagementFee = 0;
        $totalVat = 0;
        $balanceDue = 0;
        $previouslyCharged = 0;

        foreach ($cpSalesInvoices as $cpSalesInvoice) {
            // Sum management fees
            $totalManagementFee = Math::addCurrency([
                $totalManagementFee,
                $cpSalesInvoice->management_fee
            ]);
            // Sum Vat
            $totalVat = Math::addCurrency([
                $totalVat,
                $cpSalesInvoice->vat_total
            ]);

            if ($cpSalesInvoice->paid_date === null) {
                // Sum balance due if paid date is not set
                $balanceDue = Math::addCurrency([
                    $balanceDue,
                    $cpSalesInvoice->gross_total,
                ]);
            } else {
                // Sum previously charged if paid date is set
                $previouslyCharged  = Math::addCurrency([
                    $previouslyCharged,
                    $cpSalesInvoice->gross_total,
                ]);
            }
        }

        $results = [
            'total_management_fee'  => $totalManagementFee,
            'total_vat'             => $totalVat,
            'previously_charged'    => $previouslyCharged,
            'balance_due'           => $balanceDue,
        ];

        return $results;
    }

   /**
     * Gets sales credit data for this contract.
     */
    private function getSalesCreditData($cpContractId)
    {
        $cpSalesCredits = CpSalesCredit::userSiteGroup()
        ->where(
            'cp_sales_credit.cp_contract_id',
            '=',
            $cpContractId
        )
        // get all approved status invoices only
        ->whereIn(
            'cp_sales_credit.cp_sales_credit_status_id',
            [CpSalesCreditStatus::APPROVED, CpSalesCreditStatus::PAID]
        )
        ->get();

        $creditedTotal = 0;
        foreach ($cpSalesCredits as $cpSalesCredit) {
            $creditedTotal = Math::addCurrency([$creditedTotal, $cpSalesCredit->gross_total]);
        };

        $results = [
            'credited_total' => $creditedTotal,
        ];

        return $results;
    }

    private function generateFilterData($filterData, &$whereCodes, &$whereTexts, &$orCodes)
    {
        if ($val = Common::iset($filterData['cpContract'])) {
            array_push($whereCodes, ['cp_contract', 'cp_contract_id', 'cp_contract_code', $val, 'Cost Plus Contract']);
        }
    }

    public function formatInput($inputs)
    {
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $cpContract = $reportFilterQuery->getValueFilterField('cp_contract_id');
            $inputs['cpContract'] = empty($cpContract) ? null : Arr::first($cpContract);
        }
        return $inputs;
    }
}
