<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use DateTime;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Filters\Estate\EstChargeFilter;
use Tfcloud\Lib\Math;
use Tfcloud\Models\EstCharge;
use Tfcloud\Models\GenTable;
use Tfcloud\Models\Report;
use Tfcloud\Services\Estate\EstChargeService;
use Tfcloud\Services\Estate\LeaseOutService;
use Tfcloud\Services\Estate\LeaseOutLetuService;
use Tfcloud\Services\PermissionService;

class ES35TenancyRentReportService extends WkHtmlToPdfReportBaseService
{
    private $leaseOutLetuService;
    private $leaseOutService;
    private $chargeService;
    public $filterText = '';
    private $reportBoxFilterLoader = null;

    public function __construct(PermissionService $permissionService, Report $report = null)
    {
        parent::__construct($permissionService);
        $this->leaseOutLetuService = new LeaseOutLetuService($permissionService);
        $this->leaseOutService = new LeaseOutService($permissionService);
        $this->chargeService = new EstChargeService($permissionService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function runReport($repOutPutPdfFile, $inputs, $repId)
    {
        $inputs['callOrderBy'] = true;

        $runningTotal = 0;
        $janTotal = $febTotal = $marTotal = $aprTotal = $mayTotal = $junTotal = $julTotal = $augTotal = 0;
        $sepTotal = $octTotal = $novTotal = $decTotal = 0;

        //initially set indexes to 0. Array will hold the index of the month i.e. report may start in Jun, so Jun = 1.
        $monthValues = array(
            0 => array(
                0 => 'Jan',
                1 => 0,
                2 => ''
            ),
            1 => array(
                0 => 'Feb',
                1 => 0,
                2 => ''
            ),
            2 => array(
                0 => 'Mar',
                1 => 0,
                2 => ''
            ),
            3 => array(
                0 => 'Apr',
                1 => 0,
                2 => ''
            ),
            4 => array(
                0 => 'May',
                1 => 0,
                2 => ''
            ),
            5 => array(
                0 => 'Jun',
                1 => 0,
                2 => 'test'
            ),
            6 => array(
                0 => 'Jul',
                1 => 0,
                2 => ''
            ),
            7 => array(
                0 => 'Aug',
                1 => 0,
                2 => ''
            ),
            8 => array(
                0 => 'Sep',
                1 => 0,
                2 => ''
            ),
            9 => array(
                0 => 'Oct',
                1 => 0,
                2 => ''
            ),
            10 => array(
                0 => 'Nov',
                1 => 0,
                2 => ''
            ),
            11 => array(
                0 => 'Dec',
                1 => 0,
                2 => ''
            ),
        );

        $oldContact = false;
        $initial = true;

        //set month indexes according to present month
        $month[] = array();
        $nextMonthValue = 0;
        $inputs = $this->formatInputs($inputs);

        if ($inputs['nextReviewFrom'] != "") {
            $nextMonthValue = substr($inputs['nextReviewFrom'], -7, 2);
        } else {
            $nextMonthValue = date('m');
        }

        for ($i = 0; $i < 12; $i++) {
            $monthTotalValue = $i + 1;
            if ($nextMonthValue == 13) {
                //reset when index passes Dec
                $nextMonthValue = 1;
                $month[$i] = $nextMonthValue;
                $monthValues[0][1] = $monthTotalValue;
                $nextMonthValue = Math::addInt([$nextMonthValue, 1]);
            } else {
                $month[$i] = $nextMonthValue;

                switch ($nextMonthValue) {
                    case 1:
                        $monthValues[0][1] = $monthTotalValue;
                        break;
                    case 2:
                        $monthValues[1][1] = $monthTotalValue;
                        break;
                    case 3:
                        $monthValues[2][1] = $monthTotalValue;
                        break;
                    case 4:
                        $monthValues[3][1] = $monthTotalValue;
                        break;
                    case 5:
                        $monthValues[4][1] = $monthTotalValue;
                        break;
                    case 6:
                        $monthValues[5][1] = $monthTotalValue;
                        break;
                    case 7:
                        $monthValues[6][1] = $monthTotalValue;
                        break;
                    case 8:
                        $monthValues[7][1] = $monthTotalValue;
                        break;
                    case 9:
                        $monthValues[8][1] = $monthTotalValue;
                        break;
                    case 10:
                        $monthValues[9][1] = $monthTotalValue;
                        break;
                    case 11:
                        $monthValues[10][1] = $monthTotalValue;
                        break;
                    case 12:
                        $monthValues[11][1] = $monthTotalValue;
                        break;
                }

                $nextMonthValue = Math::addInt([$nextMonthValue, 1]);
            }
        }

        for ($i = 0; $i < 12; $i++) {
            $index = $monthValues[$i][1];

            switch ($index) {
                case 1:
                    $monthValues[$i][2] = 'startMonth';
                    break;
                case 2:
                    $monthValues[$i][2] = 'secondMonth';
                    break;
                case 3:
                    $monthValues[$i][2] = 'thirdMonth';
                    break;
                case 4:
                    $monthValues[$i][2] = 'fourthMonth';
                    break;
                case 5:
                    $monthValues[$i][2] = 'fifthMonth';
                    break;
                case 6:
                    $monthValues[$i][2] = 'sixthMonth';
                    break;
                case 7:
                    $monthValues[$i][2] = 'seventhMonth';
                    break;
                case 8:
                    $monthValues[$i][2] = 'eigthMonth';
                    break;
                case 9:
                    $monthValues[$i][2] = 'ninthMonth';
                    break;
                case 10:
                    $monthValues[$i][2] = 'tenthMonth';
                    break;
                case 11:
                    $monthValues[$i][2] = 'eleventhMonth';
                    break;
                case 12:
                    $monthValues[$i][2] = 'twelfthMonth';
                    break;
            }
        }

        $query = EstCharge::select()
            ->leftjoin(
                'est_time_period',
                'est_time_period.est_time_period_id',
                '=',
                'est_charge.repeat_est_time_period_id'
            )
            ->leftjoin(
                'lease_out',
                'lease_out.lease_out_id',
                '=',
                'est_charge.lease_out_id'
            )
            ->leftjoin(
                'contact',
                'contact.contact_id',
                '=',
                'lease_out.tenant_contact_id'
            )
            ->whereNotNull('est_charge.lease_out_id')
            ->where(
                'est_charge.credit_note',
                '=',
                'N'
            )
            ->where(
                'lease_out.lease_out_status_id',
                '=',
                '1'
            );

        //add filters in where applicable
        if ($inputs['status'] == 'Y') {
            $query->where(
                'est_charge.active',
                '=',
                'Y'
            );
        }
        if ($inputs['status'] == 'N') {
            $query->where(
                'est_charge.active',
                '=',
                'N'
            );
        }

        if ($inputs['chargeType'] == 'N') {
            $query->where(
                'est_charge.rent',
                '=',
                'N'
            );
            $query->where(
                'est_charge.est_charge_type_id',
                '=',
                $inputs['estChargeTypeId']
            );
        }

        if ($inputs['chargeType'] == 'Y') {
            $query->where(
                'est_charge.rent',
                '=',
                'Y'
            );
        }

        //credit note will always be N for charge/rent
        $query->where(
            'est_charge.credit_note',
            '=',
            'N'
        );

        if (($inputs['code'] != "") && ($inputs['code'] != " ")) {
            $query->where(
                'est_charge.est_charge_code',
                '=',
                $inputs['code']
            );
        }

        if (($inputs['landlord'] != "")) {
            $query->where(
                'lease_out.landlord_contact_id',
                '=',
                $inputs['landlord']
            );
        }

        if (($inputs['peppercornRent'] != "")) {
            $query->where(
                'est_charge.peppercorn_rent',
                '=',
                $inputs['peppercornRent']
            );
        }

        if (($inputs['leaseOwner'] != "")) {
            $query->where(
                'lease_out.owner_user_id',
                '=',
                $inputs['leaseOwner']
            );
        }

        if (($inputs['rentChargeOwner'] != "")) {
            $query->where(
                'est_charge.owner_user_id',
                '=',
                $inputs['rentChargeOwner']
            );
        }

        if (($inputs['paymentMethodId'] != "")) {
            $query->where(
                'est_charge.payment_method_id',
                '=',
                $inputs['paymentMethodId']
            );
        }

        if (($inputs['account'] != "")) {
            $query->where(
                'est_charge.fin_account_id',
                '=',
                $inputs['account']
            );
        }

        if (($inputs['tenant'] != "")) {
            $query->where(
                'lease_out.tenant_contact_id',
                '=',
                $inputs['tenant']
            );
        }

        if (($inputs['chargeStartDateFrom'] != "")) {
            $query->where(
                'est_charge.est_charge_start_date',
                '>=',
                DateTime::createFromFormat('d/m/Y', $inputs['chargeStartDateFrom'])->format('Y-m-d')
            );
        }

        if (($inputs['chargeStartDateTo'] != "")) {
            $query->where(
                'est_charge.est_charge_start_date',
                '<=',
                DateTime::createFromFormat('d/m/Y', $inputs['chargeStartDateTo'])->format('Y-m-d')
            );
        }

        if (($inputs['chargeEndDateFrom'] != "")) {
            $query->where(
                'est_charge.est_charge_end_date',
                '>=',
                DateTime::createFromFormat('d/m/Y', $inputs['chargeEndDateFrom'])->format('Y-m-d')
            );
        }

        if (($inputs['chargeEndDateTo'] != "")) {
            $query->where(
                'est_charge.est_charge_end_date',
                '<=',
                DateTime::createFromFormat('d/m/Y', $inputs['chargeEndDateTo'])->format('Y-m-d')
            );
        }

        if (($inputs['paymentStartDateFrom'] != "")) {
            $query->where(
                'est_charge.regular_payment_start_date',
                '>=',
                DateTime::createFromFormat('d/m/Y', $inputs['paymentStartDateFrom'])->format('Y-m-d')
            );
        }

        if (($inputs['paymentStartDateTo'] != "")) {
            $query->where(
                'est_charge.regular_payment_start_date',
                '<=',
                DateTime::createFromFormat('d/m/Y', $inputs['paymentStartDateTo'])->format('Y-m-d')
            );
        }

        if (($inputs['paymentEndDateFrom'] != "")) {
            $query->where(
                'est_charge.regular_payment_end_date',
                '>=',
                DateTime::createFromFormat('d/m/Y', $inputs['paymentEndDateFrom'])->format('Y-m-d')
            );
        }

        if (($inputs['paymentEndDateTo'] != "")) {
            $query->where(
                'est_charge.regular_payment_end_date',
                '<=',
                DateTime::createFromFormat('d/m/Y', $inputs['paymentEndDateTo'])->format('Y-m-d')
            );
        }

        if (($inputs['annualValueFrom'] != "")) {
            $query->where(
                'est_charge.annualised_rent',
                '>=',
                $inputs['annualValueFrom']
            );
        }

        if (($inputs['annualValueTo'] != "")) {
            $query->where(
                'est_charge.annualised_rent',
                '<=',
                $inputs['annualValueTo']
            );
        }

        if ($inputs['include_in_annual_rent_calc'] == 'Y') {
            $query->where(
                'est_charge.include_in_annual_rent_calc',
                '=',
                'Y'
            );
        } elseif ($inputs['include_in_annual_rent_calc'] == 'N') {
            $query->where(
                'est_charge.include_in_annual_rent_calc',
                '=',
                'N'
            );
        }

        if ($inputs['sales_acount_ref'] != "") {
            $query->where(
                'contact.sales_acount_ref',
                'LIKE',
                '%' . $inputs['sales_acount_ref'] . '%'
            );
        }

        // filter by user defined fields
        $filter = new EstChargeFilter($inputs);
        $table = 'est_charge';
        $this->filterByUserDefines(GenTable::EST_CHARGE, $query, $filter, $table);

        $query->orderBy('contact.contact_name', 'ASC');
        $charges = $query->get();

        $startYYMM = ""; //overwritten by user provided values; else system chooses date automatically

        if ($inputs['nextReviewFrom'] != "") {
            $startDate = $inputs['nextReviewFrom'];
            $startYear = substr($startDate, -4, 4);
            $startMonth = substr($startDate, -7, 2);
            $startYYMM = $startYear . $startMonth;
        } else {
            $startYear = date('Y');
            $startMonth = date('m');
            $startYYMM = $startYear . $startMonth; //get date in format 201606 for calculating differences
        }

        $endYYMM = "";

//        if($inputs['nextReviewTo'] != "") {
//            \Log::info("true");
//            $endDate = $inputs['nextReviewTo'];
//            $endYear = substr($endDate, -4, 4);
//            $endMonth = substr($endDate, -7, 2);
//            $endYYMM = $endYear . $endMonth;
//        }
        //default to a year ahead of the start date
        $endYear = date('Y', strtotime('+11 months'));
        $endMonth = date('m', strtotime('+11 months'));
        $endYYMM = $endYear . $endMonth; //get date in format 201606 for calculating differences


        foreach ($charges as $charge) {
            //check when contact needs to be shown on different
            if (($initial === true) || ($oldContact != $charge->contact_id)) {
                \Log::info("New contact (" . $charge->contact_id . ")");
                $initial = false;
                $charge['newContact'] = true;
                $oldContact = $charge->contact_id;
            } else {
                \Log::info("Same(" . $charge->contact_id . ")");
                $charge['newContact'] = false;
            }

            $chargeStartYear = substr($charge->regular_payment_start_date, 0, 4);
            $chargeStartMonth = substr($charge->regular_payment_start_date, -5, 2);
            $chargeStartYYMM = $chargeStartYear . $chargeStartMonth;

            $chargeEndYear = substr($charge->regular_payment_end_date, 0, 4);
            $chargeEndMonth = substr($charge->regular_payment_end_date, -5, 2);
            $chargeEndYYMM = $chargeEndYear . $chargeEndMonth;

            if (!$charge->regular_payment_end_date) {
                $chargeEndYYMM = $endYYMM;
            }

            $endDifferenceIndex = 0;
            $startDifferenceIndex = 0;

            if ($chargeEndYYMM < $startYYMM) {
                //if charge ends before report begins, exclude it
                $endDifferenceIndex = 'exclude';
            }

            //early finish index
            if (($endYYMM > $chargeEndYYMM) && ($endDifferenceIndex !== 'exclude')) {
                //if report ends after charge, then charge needs zero indexes for months after end
                //get difference index

                //check end years are the same first
                //since difference between e.g. end report of 201705 and 201612 requires different calculation
                if ($endYear == $chargeEndYear) {
                    $endDifferenceIndex = $endYYMM - $chargeEndYYMM;
                    $endDifferenceIndex = 12 - $endDifferenceIndex; //find the index to start the zeroes from
                } else {
                    $endInitialYear = $startYear . '12'; //calculate difference from December
                    $initialEndIndex = 0;
                    $initialEndIndex = $endInitialYear - $chargeEndYYMM; //eg 201612 - 201611 = +1 index
                    $initialEndIndex = $initialEndIndex + $endMonth;
                    $endDifferenceIndex = $initialEndIndex;
                    $endDifferenceIndex = 12 - $endDifferenceIndex;
                }
            }

            //late start case e.g. 201606 report start, 201609 charge start
            if ($startYYMM < $chargeStartYYMM) {
                //if report starts after report begins, irrelevant months need to be zero indexed
                //get difference index

                //check years are the same first
                if ($startYear == $chargeStartYear) {
                    $startDifferenceIndex = $chargeStartYYMM - $startYYMM;
                } else {
                    if ($chargeStartYear == $endYear) {
                        //eg charge starts 201701, report starts 201606
                        $initialMonthDifference = 12 - $startMonth;
                        $initialMonthDifference = $initialMonthDifference + $chargeStartMonth;
                        $startDifferenceIndex = $initialMonthDifference;
                    }

                    if ($chargeStartYYMM > $endYYMM) {
                        //if charge starts after end of report, initialise whole row to 0
                        $startDifferenceIndex = 'exclude';
                    }
                }
            }

            //set the month orders from the index for totalling up
            $janMonth = $monthValues[0][2];
            $febMonth = $monthValues[1][2];
            $marMonth = $monthValues[2][2];
            $aprMonth = $monthValues[3][2];
            $mayMonth = $monthValues[4][2];
            $junMonth = $monthValues[5][2];
            $julMonth = $monthValues[6][2];
            $augMonth = $monthValues[7][2];
            $sepMonth = $monthValues[8][2];
            $octMonth = $monthValues[9][2];
            $novMonth = $monthValues[10][2];
            $decMonth = $monthValues[11][2];

            if ($charge->est_time_period_code == 'ADHOC') {
                //remove ad hoc for now
                $startDifferenceIndex = 'exclude';
            }

            if (($charge->est_time_period_code == 'M')) {
                //month time period

                if ($charge->period_freq == 1) {
                    $charge[$janMonth] = $charge->regular_payment_amount;
                    $charge[$febMonth] = $charge->regular_payment_amount;
                    $charge[$marMonth] = $charge->regular_payment_amount;
                    $charge[$aprMonth] = $charge->regular_payment_amount;
                    $charge[$mayMonth] = $charge->regular_payment_amount;
                    $charge[$junMonth] = $charge->regular_payment_amount;
                    $charge[$julMonth] = $charge->regular_payment_amount;
                    $charge[$augMonth] = $charge->regular_payment_amount;
                    $charge[$sepMonth] = $charge->regular_payment_amount;
                    $charge[$octMonth] = $charge->regular_payment_amount;
                    $charge[$novMonth] = $charge->regular_payment_amount;
                    $charge[$decMonth] = $charge->regular_payment_amount;
                }
                if ($charge->period_freq == 2) {
                    $charge['startMonth'] = $charge->regular_payment_amount;
                    $charge['secondMonth'] = 0;
                    $charge['thirdMonth'] = $charge->regular_payment_amount;
                    $charge['fourthMonth'] = 0;
                    $charge['fifthMonth'] = $charge->regular_payment_amount;
                    $charge['sixthMonth'] = 0;
                    $charge['seventhMonth'] = $charge->regular_payment_amount;
                    $charge['eigthMonth'] = 0;
                    $charge['ninthMonth'] = $charge->regular_payment_amount;
                    $charge['tenthMonth'] = 0;
                    $charge['eleventhMonth'] = $charge->regular_payment_amount;
                    $charge['twelfthMonth'] = 0;
                }
                if ($charge->period_freq == 3) {
                    $charge['startMonth'] = $charge->regular_payment_amount;
                    $charge['secondMonth'] = 0;
                    $charge['thirdMonth'] = 0;
                    $charge['fourthMonth'] = $charge->regular_payment_amount;
                    $charge['fifthMonth'] = 0;
                    $charge['sixthMonth'] = 0;
                    $charge['seventhMonth'] = $charge->regular_payment_amount;
                    $charge['eightMonth'] = 0;
                    $charge['ninthMonth'] = 0;
                    $charge['tenthMonth'] = $charge->regular_payment_amount;
                    $charge['eleventhMonth'] = 0;
                    $charge['twelfthMonth'] = 0;
                }
                if ($charge->period_freq == 4) {
                    $charge['startMonth'] = $charge->regular_payment_amount;
                    $charge['secondMonth'] = 0;
                    $charge['thirdMonth'] = 0;
                    $charge['fourthMonth'] = 0;
                    $charge['fifthMonth'] = $charge->regular_payment_amount;
                    $charge['sixthMonth'] = 0;
                    $charge['seventhMonth'] = 0;
                    $charge['eigthMonth'] = 0;
                    $charge['ninthMonth'] = $charge->regular_payment_amount;
                    $charge['tenthMonth'] = 0;
                    $charge['eleventhMonth'] = 0;
                    $charge['twelfthMonth'] = $charge->regular_payment_amount;
                }
                if ($charge->period_freq == 5) {
                    $charge['startMonth'] = $charge->regular_payment_amount;
                    $charge['secondMonth'] = 0;
                    $charge['thirdMonth'] = 0;
                    $charge['fourthMonth'] = 0;
                    $charge['fifthMonth'] = 0;
                    $charge['sixthMonth'] = $charge->regular_payment_amount;
                    $charge['seventhMonth'] = 0;
                    $charge['eigthMonth'] = 0;
                    $charge['ninthMonth'] = 0;
                    $charge['tenthMonth'] = 0;
                    $charge['eleventhMonth'] = $charge->regular_payment_amount;
                    $charge['twelfthMonth'] = 0;
                }
            }

            if (
                ($charge->est_time_period_code == 'EQ') ||
                ($charge->est_time_period_code == 'MQ') ||
                ($charge->est_time_period_code == 'Q')
            ) {
                $charge[$marMonth] = $charge->regular_payment_amount; //march
                $charge[$junMonth] = $charge->regular_payment_amount; //june
                $charge[$sepMonth] = $charge->regular_payment_amount; //sep
                $charge[$decMonth] = $charge->regular_payment_amount; //dec

                $charge[$janMonth] = 0;
                $charge[$febMonth] = 0;
                $charge[$aprMonth] = 0;
                $charge[$mayMonth] = 0;
                $charge[$julMonth] = 0;
                $charge[$augMonth] = 0;
                $charge[$octMonth] = 0;
                $charge[$novMonth] = 0;
            }

            if ($charge->est_time_period_code == 'SQ') {
                //feb, may, aug, nov
                $charge[$augMonth] = $charge->regular_payment_amount; //aug
                $charge[$novMonth] = $charge->regular_payment_amount;//nov
                $charge[$febMonth] = $charge->regular_payment_amount; //feb
                $charge[$mayMonth] = $charge->regular_payment_amount; //may

                $charge[$janMonth] = 0;
                $charge[$marMonth] = 0;
                $charge[$aprMonth] = 0;
                $charge[$junMonth] = 0;
                $charge[$julMonth] = 0;
                $charge[$sepMonth] = 0;
                $charge[$octMonth] = 0;
                $charge[$decMonth] = 0;
            }

            if ($charge->est_time_period_code == 'W') {
                if ($charge->period_freq > 1) {
                    //divide annualised rent by number of weeks to get 1 week
                    $monthlyPayment = Math::divCurrency([$charge->annualised_rent, 12]);
                } else {
                    $monthlyPayment = Math::mulCurrency([$charge->regular_payment_amount, 4]);
                }

                $charge[$janMonth] = $monthlyPayment;
                $charge[$febMonth] = $monthlyPayment;
                $charge[$marMonth] = $monthlyPayment;
                $charge[$aprMonth] = $monthlyPayment;
                $charge[$mayMonth] = $monthlyPayment;
                $charge[$junMonth] = $monthlyPayment;
                $charge[$julMonth] = $monthlyPayment;
                $charge[$augMonth] = $monthlyPayment;
                $charge[$sepMonth] = $monthlyPayment;
                $charge[$octMonth] = $monthlyPayment;
                $charge[$novMonth] = $monthlyPayment;
                $charge[$decMonth] = $monthlyPayment;
            } //end week setting


            if ($charge->est_time_period_code == 'A') {
                //get the month the annual charge started
                $chargeAnnualStart = substr($charge->regular_payment_start_date, 0, 4) ? : false;

                $timestamp = mktime(0, 0, 0, $chargeAnnualStart, 1);
                $monthName = date('M', $timestamp); //get the three letter month code to match against
                $freqCheck = 'true'; //if period freq. is larger than one and charge only fires every few years
                //first checks if this year should be included

                if ($charge->period_freq > 1) {
                    //if period freq is greater than one, charge repeats only once every few years
                    $chargeFrequency = $charge->period_freq;
                    $yearDifference = $startYear - $chargeAnnualStart; //find difference between current year
                    //and start year of charge
                    $modresult = $yearDifference % $chargeFrequency;

                    if ($modresult == 0) {
                        //no remained, result stays true
                        $freqCheck = 'true';
                    } else {
                        $freqCheck = 'false';
                    }
                }

                if ($freqCheck === 'true') {
                    for ($i = 0; $i < 12; $i++) {
                        if ($monthValues[$i][0] == $monthName) {
                            $charge[$monthValues[$i][2]] = $charge->annualised_rent;
                        } else {
                            $charge[$monthValues[$i][2]] = 0;
                        }
                    }
                } else {
                    //if the charge does not repeat that year, exclude all lines
                    for ($i = 0; $i < 12; $i++) {
                        $charge[$monthValues[$i][2]] = 0;
                    } //end for
                } //end else
            } //end annual charge

            //English half years Jun&Dec
            if ($charge->est_time_period_code == 'EHY(JD)') {
                $charge[$junMonth] = $charge->regular_payment_amount;
                $charge[$decMonth] = $charge->regular_payment_amount;

                $charge[$janMonth] = 0;
                $charge[$febMonth] = 0;
                $charge[$marMonth] = 0;
                $charge[$aprMonth] = 0;
                $charge[$mayMonth] = 0;
                $charge[$julMonth] = 0;
                $charge[$augMonth] = 0;
                $charge[$sepMonth] = 0;
                $charge[$octMonth] = 0;
                $charge[$novMonth] = 0;
            }

            //English half years Mar&Sep
            if ($charge->est_time_period_code == 'EHY(MS)') {
                $charge[$marMonth] = $charge->regular_payment_amount;
                $charge[$sepMonth] = $charge->regular_payment_amount;

                $charge[$janMonth] = 0;
                $charge[$febMonth] = 0;
                $charge[$aprMonth] = 0;
                $charge[$mayMonth] = 0;
                $charge[$junMonth] = 0;
                $charge[$julMonth] = 0;
                $charge[$augMonth] = 0;
                $charge[$octMonth] = 0;
                $charge[$novMonth] = 0;
                $charge[$decMonth] = 0;
            }

            //exclude any reports falling outside the range of the dates
            if ($startDifferenceIndex === 'exclude') {
                $charge['startMonth'] = 0;
                $charge['secondMonth'] = 0;
                $charge['thirdMonth'] = 0;
                $charge['fourthMonth'] = 0;
                $charge['fifthMonth'] = 0;
                $charge['sixthMonth'] = 0;
                $charge['seventhMonth'] = 0;
                $charge['eigthMonth'] = 0;
                $charge['ninthMonth'] = 0;
                $charge['tenthMonth'] = 0;
                $charge['eleventhMonth'] = 0;
                $charge['twelfthMonth'] = 0;
            }

            if ($endDifferenceIndex === 'exclude') {
                $charge['startMonth'] = 0;
                $charge['secondMonth'] = 0;
                $charge['thirdMonth'] = 0;
                $charge['fourthMonth'] = 0;
                $charge['fifthMonth'] = 0;
                $charge['sixthMonth'] = 0;
                $charge['seventhMonth'] = 0;
                $charge['eigthMonth'] = 0;
                $charge['ninthMonth'] = 0;
                $charge['tenthMonth'] = 0;
                $charge['eleventhMonth'] = 0;
                $charge['twelfthMonth'] = 0;
            }

            //overwrite any late starts/early finish charge months with 0s
            if ($startDifferenceIndex != 0) {
                if ($startDifferenceIndex == 1) {
                    //everything up to first month is a 0
                    $charge['startMonth'] = 0;
                }

                if ($startDifferenceIndex == 2) {
                    //everything up to second month is a 0
                    $charge['startMonth'] = 0;
                    $charge['secondMonth'] = 0;
                }

                if ($startDifferenceIndex == 3) {
                    //everything up to third month is a 0
                    $charge['startMonth'] = 0;
                    $charge['secondMonth'] = 0;
                    $charge['thirdMonth'] = 0;
                }

                if ($startDifferenceIndex == 4) {
                    //everything up to fourth month is a 0
                    $charge['startMonth'] = 0;
                    $charge['secondMonth'] = 0;
                    $charge['thirdMonth'] = 0;
                    $charge['fourthMonth'] = 0;
                }

                if ($startDifferenceIndex == 5) {
                    //everything up to 5th month is a 0
                    $charge['startMonth'] = 0;
                    $charge['secondMonth'] = 0;
                    $charge['thirdMonth'] = 0;
                    $charge['fourthMonth'] = 0;
                    $charge['fifthMonth'] = 0;
                }

                if ($startDifferenceIndex == 6) {
                    //everything up to 6th month is a 0
                    $charge['startMonth'] = 0;
                    $charge['secondMonth'] = 0;
                    $charge['thirdMonth'] = 0;
                    $charge['fourthMonth'] = 0;
                    $charge['fifthMonth'] = 0;
                    $charge['sixthMonth'] = 0;
                }

                if ($startDifferenceIndex == 7) {
                    //everything up to 7th month is a 0
                    $charge['startMonth'] = 0;
                    $charge['secondMonth'] = 0;
                    $charge['thirdMonth'] = 0;
                    $charge['fourthMonth'] = 0;
                    $charge['fifthMonth'] = 0;
                    $charge['sixthMonth'] = 0;
                    $charge['seventhMonth'] = 0;
                }

                if ($startDifferenceIndex == 8) {
                    //everything up to 8th month is a 0
                    $charge['startMonth'] = 0;
                    $charge['secondMonth'] = 0;
                    $charge['thirdMonth'] = 0;
                    $charge['fourthMonth'] = 0;
                    $charge['fifthMonth'] = 0;
                    $charge['sixthMonth'] = 0;
                    $charge['seventhMonth'] = 0;
                    $charge['eigthMonth'] = 0;
                }

                if ($startDifferenceIndex == 9) {
                    //everything up to 9th month is a 0
                    $charge['startMonth'] = 0;
                    $charge['secondMonth'] = 0;
                    $charge['thirdMonth'] = 0;
                    $charge['fourthMonth'] = 0;
                    $charge['fifthMonth'] = 0;
                    $charge['sixthMonth'] = 0;
                    $charge['seventhMonth'] = 0;
                    $charge['ninthMonth'] = 0;
                }

                if ($startDifferenceIndex == 10) {
                    //everything up to 10th month is a 0
                    $charge['startMonth'] = 0;
                    $charge['secondMonth'] = 0;
                    $charge['thirdMonth'] = 0;
                    $charge['fourthMonth'] = 0;
                    $charge['fifthMonth'] = 0;
                    $charge['sixthMonth'] = 0;
                    $charge['seventhMonth'] = 0;
                    $charge['ninthMonth'] = 0;
                    $charge['tenthMonth'] = 0;
                }

                if ($startDifferenceIndex == 11) {
                    //everything up to 11th month is a 0
                    $charge['startMonth'] = 0;
                    $charge['secondMonth'] = 0;
                    $charge['thirdMonth'] = 0;
                    $charge['fourthMonth'] = 0;
                    $charge['fifthMonth'] = 0;
                    $charge['sixthMonth'] = 0;
                    $charge['seventhMonth'] = 0;
                    $charge['ninthMonth'] = 0;
                    $charge['tenthMonth'] = 0;
                    $charge['eleventhMonth'] = 0;
                }
            }

            if ($endDifferenceIndex != 0) {
                //if charge ends early, set anything after index to a 0

                if ($endDifferenceIndex == 1) {
                    //everything after 1st month is a 0
                    $charge['secondMonth'] = 0;
                    $charge['thirdMonth'] = 0;
                    $charge['fourthMonth'] = 0;
                    $charge['fifthMonth'] = 0;
                    $charge['sixthMonth'] = 0;
                    $charge['seventhMonth'] = 0;
                    $charge['eigthMonth'] = 0;
                    $charge['ninthMonth'] = 0;
                    $charge['tenthMonth'] = 0;
                    $charge['eleventhMonth'] = 0;
                    $charge['twelfthMonth'] = 0;
                }

                if ($endDifferenceIndex == 2) {
                    //everything after 2nd month is a 0
                    $charge['thirdMonth'] = 0;
                    $charge['fourthMonth'] = 0;
                    $charge['fifthMonth'] = 0;
                    $charge['sixthMonth'] = 0;
                    $charge['seventhMonth'] = 0;
                    $charge['eigthMonth'] = 0;
                    $charge['ninthMonth'] = 0;
                    $charge['tenthMonth'] = 0;
                    $charge['eleventhMonth'] = 0;
                    $charge['twelfthMonth'] = 0;
                }

                if ($endDifferenceIndex == 3) {
                    //everything after 3rd month is a 0
                    $charge['fourthMonth'] = 0;
                    $charge['fifthMonth'] = 0;
                    $charge['sixthMonth'] = 0;
                    $charge['seventhMonth'] = 0;
                    $charge['eigthMonth'] = 0;
                    $charge['ninthMonth'] = 0;
                    $charge['tenthMonth'] = 0;
                    $charge['eleventhMonth'] = 0;
                    $charge['twelfthMonth'] = 0;
                }

                if ($endDifferenceIndex == 4) {
                    //everything after 4th month is a 0
                    $charge['fifthMonth'] = 0;
                    $charge['sixthMonth'] = 0;
                    $charge['seventhMonth'] = 0;
                    $charge['eigthMonth'] = 0;
                    $charge['ninthMonth'] = 0;
                    $charge['tenthMonth'] = 0;
                    $charge['eleventhMonth'] = 0;
                    $charge['twelfthMonth'] = 0;
                }

                if ($endDifferenceIndex == 5) {
                    //everything after 5th month is a 0
                    $charge['sixthMonth'] = 0;
                    $charge['seventhMonth'] = 0;
                    $charge['eigthMonth'] = 0;
                    $charge['ninthMonth'] = 0;
                    $charge['tenthMonth'] = 0;
                    $charge['eleventhMonth'] = 0;
                    $charge['twelfthMonth'] = 0;
                }

                if ($endDifferenceIndex == 6) {
                    //everything after 6th month is a 0
                    $charge['seventhMonth'] = 0;
                    $charge['eigthMonth'] = 0;
                    $charge['ninthMonth'] = 0;
                    $charge['tenthMonth'] = 0;
                    $charge['eleventhMonth'] = 0;
                    $charge['twelfthMonth'] = 0;
                }

                if ($endDifferenceIndex == 9) {
                    //everything after 9th month is a 0
                    $charge['tenthMonth'] = 0;
                    $charge['eleventhMonth'] = 0;
                    $charge['twelfthMonth'] = 0;
                }

                if ($endDifferenceIndex == 10) {
                    //everything after 10th month is a 0
                    $charge['eleventhMonth'] = 0;
                    $charge['twelfthMonth'] = 0;
                }

                if ($endDifferenceIndex == 11) {
                    //everything after 11th month is a 0
                    $charge['twelfthMonth'] = 0;
                }
            }

            if ($charge->est_time_period_code) {
                //create column value for charge code and frequency by concatenating values together
                $charge['chargeCodeAndFreq'] = $charge->est_charge_code . "/" . $charge->est_time_period_code;
            } else {
                $charge['chargeCodeAndFreq'] = $charge->est_charge_code;
            }

            $charge['contactAndOrg'] = $charge->contact_name . ' - ' . $charge->organisation;
            if (strlen($charge['contactAndOrg']) < 18) {
                //add an extra line flag to keep report aligned
                $charge['extraLine'] = true;
            }

            //work out totals for months
            $junTotal = Math::addCurrency([$junTotal, $charge[$junMonth]]);
            $julTotal = Math::addCurrency([$julTotal, $charge[$julMonth]]);
            $augTotal = Math::addCurrency([$augTotal, $charge[$augMonth]]);
            $sepTotal = Math::addCurrency([$sepTotal, $charge[$sepMonth]]);
            $octTotal = Math::addCurrency([$octTotal, $charge[$octMonth]]);
            $novTotal = Math::addCurrency([$novTotal, $charge[$novMonth]]);
            $decTotal = Math::addCurrency([$decTotal, $charge[$decMonth]]);
            $janTotal = Math::addCurrency([$janTotal, $charge[$janMonth]]);
            $febTotal = Math::addCurrency([$febTotal, $charge[$febMonth]]);
            $marTotal = Math::addCurrency([$marTotal, $charge[$marMonth]]);
            $aprTotal = Math::addCurrency([$aprTotal, $charge[$aprMonth]]);
            $mayTotal = Math::addCurrency([$mayTotal, $charge[$mayMonth]]);
            //single line total
            $charge['total'] = Math::addCurrency([
                $charge['startMonth'],
                $charge['secondMonth'],
                $charge['thirdMonth'],
                $charge['fourthMonth'],
                $charge['fifthMonth'],
                $charge['sixthMonth'],
                $charge['seventhMonth'],
                $charge['eigthMonth'],
                $charge['ninthMonth'],
                $charge['tenthMonth'],
                $charge['eleventhMonth'],
                $charge['twelfthMonth']
            ]);
            $runningTotal = Math::addCurrency([$runningTotal, $charge['total']]);
        }

        $month1Heading = $month2Heading = $month3Heading = $month4Heading = $month5Heading = "";
        $month6Heading = $month7Heading = $month8Heading = $month9Heading = $month10Heading = "";
        $month11Heading = $month12Heading = "";
        //set table headers so months go forward from correct month
        for ($i = 0; $i < 12; $i++) {
            $monthName = $monthValues[$i][2];

            switch ($monthName) {
                case 'startMonth':
                    $month1Heading = $monthValues[$i][0];
                    break;
                case 'secondMonth':
                    $month2Heading = $monthValues[$i][0];
                    break;
                case 'thirdMonth':
                    $month3Heading = $monthValues[$i][0];
                    break;
                case 'fourthMonth':
                    $month4Heading = $monthValues[$i][0];
                    break;
                case 'fifthMonth':
                    $month5Heading = $monthValues[$i][0];
                    break;
                case 'sixthMonth':
                    $month6Heading = $monthValues[$i][0];
                    break;
                case 'seventhMonth':
                    $month7Heading = $monthValues[$i][0];
                    break;
                case 'eigthMonth':
                    $month8Heading = $monthValues[$i][0];
                    break;
                case 'ninthMonth':
                    $month9Heading = $monthValues[$i][0];
                    break;
                case 'tenthMonth':
                    $month10Heading = $monthValues[$i][0];
                    break;
                case 'eleventhMonth':
                    $month11Heading = $monthValues[$i][0];
                    break;
                case 'twelfthMonth':
                    $month12Heading = $monthValues[$i][0];
                    break;
            }
        }

        // Create wkhtmltopdf temp folder to store generated files
        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);

        $header = \View::make(
            'reports.wkhtmltopdf.estates.es35.header',
            [
                'month1Heading' => $month1Heading,
                'month2Heading' => $month2Heading,
                'month3Heading' => $month3Heading,
                'month4Heading' => $month4Heading,
                'month5Heading' => $month5Heading,
                'month6Heading' => $month6Heading,
                'month7Heading' => $month7Heading,
                'month8Heading' => $month8Heading,
                'month9Heading' => $month9Heading,
                'month10Heading' => $month10Heading,
                'month11Heading' => $month11Heading,
                'month12Heading' => $month12Heading
            ]
        )->render();
        $headerPath = $generatedPath . "/header.html";
        file_put_contents($headerPath, $header, LOCK_EX);

        $footer = \View::make('reports.wkhtmltopdf.estates.es35.footer')->render();
        $footerPath = $generatedPath . "/footer.html";
        file_put_contents($footerPath, $footer, LOCK_EX);

        $content = \View::make(
            'reports.wkhtmltopdf.estates.es35.content',
            [
                'lettableUnits' => $charges,
                'runningTotal' => $runningTotal,
                'janTotal'  => $janTotal,
                'febTotal'  => $febTotal,
                'marTotal'  => $marTotal,
                'aprTotal'  => $aprTotal,
                'mayTotal'  => $mayTotal,
                'junTotal'  => $junTotal,
                'julTotal'  => $julTotal,
                'augTotal'  => $augTotal,
                'sepTotal'  => $sepTotal,
                'octTotal'  => $octTotal,
                'novTotal'  => $novTotal,
                'decTotal'  => $decTotal
            ]
        )->render();

        $contentPath = $generatedPath . "/content.html";

        file_put_contents($contentPath, $content, LOCK_EX);

        $options = [
            'header-html' => $headerPath,
            'footer-html' => $footerPath,
            'header-spacing' => 4,
            'footer-spacing' => 4,
            'orientation' => 'landscape',
        ];

        $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $options);

        $whereCodes = [];               // The codes which have been used in the filter
        $whereTexts = [];               // Plain text about filtering applied
        $orCodes = [];                  // csv string of OR codes
        $bFilterDetail = false;
        $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
        $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $this->filterText);
        return true;
    }

    private function generateFilterData($filterData, &$whereCodes, &$whereTexts, &$orCodes)
    {
        if ($val = Common::iset($filterData['status'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, "Status = 'Active'");
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, "Status = 'Inactive'");
                    break;
                case 'all':
                    array_push($whereTexts, "Status = 'All'");
                    break;
            }
        }

        if ($val = Common::iset($filterData['chargeType'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, "Charge/Rent = 'Rent'");
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, "Charge/Rent = 'Charge'");
                    break;
                default:
                    array_push($whereTexts, "Charge/Rent = 'All'");
                    break;
            }
        }

        if ($val = Common::iset($filterData['estChargeTypeId'])) {
            array_push(
                $whereCodes,
                ['est_charge_type', 'est_charge_type_id', 'est_charge_type_code', $val, "Charge Type"]
            );
        }

        if ($val = Common::iset($filterData['peppercornRent'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, "Peppercorn Rent = 'Yes'");
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, "Peppercorn Rent = 'No'");
                    break;
            }
        }

        if ($val = Common::iset($filterData['creditNote'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, "Credit Note = 'No'");
                    break;
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, "Credit Note = 'Yes'");
                    break;
                default:
                    array_push($whereTexts, "Credit Note = 'All'");
                    break;
            }
        }

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Code match '" . $val . "'");
        }

        if ($val = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);
        }

        if ($val = Common::iset($filterData['leaseOwner'])) {
            array_push($whereCodes, ['user', 'id', 'username', $val, 'Lease Owner']);
        }

        if ($val = Common::iset($filterData['rentChargeOwner'])) {
            array_push($whereCodes, ['user', 'id', 'username', $val, 'Rent Charge Owner']);
        }

        if ($val = Common::iset($filterData['paymentMethodId'])) {
            array_push(
                $whereCodes,
                ['payment_method', 'payment_method_id', 'payment_method_desc', $val, 'Payment Method']
            );
        }

        if ($val = Common::iset($filterData['account'])) {
            array_push(
                $whereCodes,
                ['fin_account', 'fin_account_id', 'fin_account_code', $val, 'Account Code']
            );
        }

        if ($val = Common::iset($filterData['landlord'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Landlord']);
        }

        if ($val = Common::iset($filterData['tenant'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Tenant']);
        }

        if ($val = Common::iset($filterData['nextReviewFrom'])) {
            array_push($whereTexts, "Review Date from {$val}");
        }

        if ($val = Common::iset($filterData['nextReviewTo'])) {
            array_push($whereTexts, "Review Date to {$val}");
        }

        if ($val = Common::iset($filterData['chargeStartDateFrom'])) {
            array_push($whereTexts, "Charge Start Date from {$val}");
        }

        if ($val = Common::iset($filterData['chargeStartDateTo'])) {
            array_push($whereTexts, "Charge Start Date to {$val}");
        }

        if ($val = Common::iset($filterData['chargeEndDateFrom'])) {
            array_push($whereTexts, "Charge End Date from {$val}");
        }

        if ($val = Common::iset($filterData['chargeEndDateTo'])) {
            array_push($whereTexts, "Charge End Date to {$val}");
        }

        if ($val = Common::iset($filterData['paymentStartDateFrom'])) {
            array_push($whereTexts, "Payment Start Date from {$val}");
        }

        if ($val = Common::iset($filterData['paymentStartDateTo'])) {
            array_push($whereTexts, "Payment Start Date to {$val}");
        }

        if ($val = Common::iset($filterData['paymentEndDateFrom'])) {
            array_push($whereTexts, "Payment End Date from {$val}");
        }

        if ($val = Common::iset($filterData['paymentEndDateTo'])) {
            array_push($whereTexts, "Payment End Date to {$val}");
        }

        if ($val = Common::iset($filterData['annualValueFrom'])) {
            array_push($whereTexts, "Annual Value from {$val}");
        }

        if ($val = Common::iset($filterData['annualValueTo'])) {
            array_push($whereTexts, "Annual Value to {$val}");
        }

        $this->reAddUserDefinesQuery(GenTable::EST_CHARGE, $filterData, $whereCodes, $whereTexts);
    }

    private function formatInputs($inputs)
    {
        $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
        $inputs['annualised_rent'] = $reportFilterQuery->getFirstValueFilterField('annualised_rent');
        $inputs['est_charge_end_date'] = $reportFilterQuery->getFirstValueFilterField('est_charge_end_date');
        $inputs['est_charge_start_date'] = $reportFilterQuery->getFirstValueFilterField('est_charge_start_date');
        $inputs['code'] = $reportFilterQuery->getFirstValueFilterField('est_charge_code');
        $inputs['regular_payment_end_date'] = $reportFilterQuery->getFirstValueFilterField('regular_payment_end_date');
        $inputs['regular_payment_start_date'] = $reportFilterQuery
            ->getFirstValueFilterField('regular_payment_start_date');
        $inputs['next_review_date'] = $reportFilterQuery->getFirstValueFilterField('next_review_date');
        $inputs['sales_acount_ref'] = $reportFilterQuery->getFirstValueFilterField('sales_acount_ref');
        $inputs['account'] = $reportFilterQuery->getFirstValueFilterField('fin_account_id');
        $inputs['estChargeTypeId'] = $reportFilterQuery->getFirstValueFilterField('est_charge_type_id');
        $inputs['chargeType'] = $reportFilterQuery->getFirstValueFilterField('rent');
        $inputs['include_in_annual_rent_calc'] = $reportFilterQuery
            ->getFirstValueFilterField('include_in_annual_rent_calc');
        $inputs['landlord'] = $reportFilterQuery->getFirstValueFilterField('landlord_contact_id');
        $inputs['leaseOwner'] = $reportFilterQuery->getFirstValueFilterField('lease_owner_user_id');
        $inputs['paymentMethodId'] = $reportFilterQuery->getFirstValueFilterField('payment_method_id');
        $inputs['peppercornRent'] = $reportFilterQuery->getFirstValueFilterField('peppercorn_rent');
        $inputs['rentChargeOwner'] = $reportFilterQuery->getFirstValueFilterField('rent_charge_owner_user_id');
        $inputs['status'] = $reportFilterQuery->getFirstValueFilterField('status');
        $inputs['tenant'] = $reportFilterQuery->getFirstValueFilterField('tenant_contact_id');
        $inputs['nextReviewFrom'] = $inputs['next_review_date'] ?
            Common::dateFieldDisplayFormat(array_first($inputs['next_review_date'])) : null;
        $inputs['nextReviewTo'] = $inputs['next_review_date'] ?
            Common::dateFieldDisplayFormat(array_last($inputs['next_review_date'])) : null;
        $inputs['chargeStartDateFrom'] = $inputs['est_charge_start_date'] ?
            Common::dateFieldDisplayFormat(array_first($inputs['est_charge_start_date'])) : null;
        $inputs['chargeStartDateTo'] = $inputs['est_charge_start_date'] ?
            Common::dateFieldDisplayFormat(array_last($inputs['est_charge_start_date'])) : null;

        $inputs['chargeEndDateFrom'] = $inputs['est_charge_end_date'] ?
            Common::dateFieldDisplayFormat(array_first($inputs['est_charge_end_date'])) : null;
        $inputs['chargeEndDateTo'] = $inputs['est_charge_end_date'] ?
            Common::dateFieldDisplayFormat(array_last($inputs['est_charge_end_date'])) : null;

        $inputs['paymentStartDateFrom'] = $inputs['regular_payment_start_date'] ?
            Common::dateFieldDisplayFormat(array_first($inputs['regular_payment_start_date'])) : null;
        $inputs['paymentStartDateTo'] = $inputs['regular_payment_start_date'] ?
            Common::dateFieldDisplayFormat(array_last($inputs['regular_payment_start_date'])) : null;

        $inputs['paymentEndDateFrom'] = $inputs['regular_payment_end_date'] ?
            Common::dateFieldDisplayFormat(array_first($inputs['regular_payment_end_date'])) : null;
        $inputs['paymentEndDateTo'] = $inputs['regular_payment_end_date'] ?
            Common::dateFieldDisplayFormat(array_last($inputs['regular_payment_end_date'])) : null;

        $inputs['annualValueFrom'] = $inputs['annualised_rent'] ?
            array_first($inputs['annualised_rent']) : null;
        $inputs['annualValueTo'] = $inputs['annualised_rent'] ?
            array_last($inputs['annualised_rent']) : null;
        return $this->getUDValueFilterQuery($reportFilterQuery, $inputs);
    }
}
