<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use Tfcloud\Models\CaseManagement\CmType;
use Tfcloud\Models\GenTable;

class CM03Casemgmt5ProgressService extends BaseCMx03CaseProgressService
{
    protected $cmType = CmType::CM_TYPE_CASEMGMT5;
    protected $gentTable = GenTable::CM5_CASE;
}
