<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use Carbon\CarbonPeriod;
use DateTime;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\BoxFilterQuery\IReportFilterQuery;
use Tfcloud\Lib\Common;
use Tfcloud\Models\DloHourType;
use Tfcloud\Models\DloJobLabour;
use Tfcloud\Models\DloJobType;
use Tfcloud\Models\Report;
use Tfcloud\Services\Admin\System\Dlo\DloRateDaysService;
use Tfcloud\Services\Dlo\JobLabourService;
use Tfcloud\Services\PermissionService;

class DLO15WeeklyTimesheet extends WkHtmlToPdfReportBaseService
{
    public $filterText = '';
    private $jobType = null;
    private $jobTypeId = null;
    private $reportBoxFilterLoader = null;
    private $allLabours;
    private $jobLabourService;
    private $rateDays;

    public function __construct(PermissionService $permissionService, Report $report = null)
    {
        parent::__construct($permissionService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
        $this->allLabours = collect();
        $this->jobLabourService = new JobLabourService($this->permissionService);
        $rateDaysService = new DloRateDaysService($this->permissionService);
        $this->rateDays = $rateDaysService->getBySiteGroup();
    }

    protected function getAllDloLaboursByTimeSheet($userIDs, $minStartDate, $maxStartDate)
    {
        $query = DloJobLabour::query()
            ->select(
                DB::raw('WEEKDAY(DATE(start_date)) AS week_day'),
                'user.id as user_id',
                'dlo_job_code',
                'dlo_job.dlo_job_id',
                DB::raw('(DATE(start_date)) AS start_date'),
                'start_time',
                DB::raw('(DATE(finish_date)) AS finish_date'),
                'finish_time',
                DB::raw('WEEKDAY(DATE(start_date)) AS start_day'),
                DB::raw('WEEKDAY(DATE(finish_date)) AS finish_day'),
                DB::raw('DATEDIFF(finish_date, start_date) AS diff'),
                'labour',
                'labour_1_3',
                'labour_1_5',
                'labour_2',
            )
            ->join('dlo_job_operative', function ($join) {
                $join->on(
                    'dlo_job_operative.dlo_job_operative_id',
                    '=',
                    'dlo_job_labour.dlo_job_operative_id'
                )
                    ->on('dlo_job_operative.dlo_job_id', 'dlo_job_labour.dlo_job_id');
            })
            ->join(
                'user',
                'user.id',
                '=',
                'dlo_job_operative.user_id'
            )
            ->join(
                'dlo_job',
                'dlo_job.dlo_job_id',
                '=',
                'dlo_job_labour.dlo_job_id'
            )
            ->whereIn('user.id', $userIDs)
            ->where(function ($query) use ($minStartDate, $maxStartDate) {
                $query->whereBetween(
                    DB::raw("DATE_FORMAT(`start_date`, '%Y-%m-%d')"),
                    [$minStartDate, $maxStartDate]
                )
                    ->orWhereBetween(
                        DB::raw("DATE_FORMAT(`finish_date`, '%Y-%m-%d')"),
                        [$minStartDate, $maxStartDate]
                    );
            })
            ->whereNotNull('finish_date')
            ->orderBy('dlo_job_code');

        if ($this->jobTypeId) {
            $query->where('dlo_job.dlo_job_type_id', '=', $this->jobTypeId);
        }

        return $query->get();
    }

    protected function getDloLaboursByTimeSheet($timesheet)
    {
        return $this->allLabours
            ->where('user_id', $timesheet->user_id)
            ->filter(function ($item) use ($timesheet) {
                $startDate = Carbon::parse($item->start_date)->format('Y-m-d');
                $finishDate = Carbon::parse($item->finish_date)->format('Y-m-d');
                return
                    (
                        $startDate >= $timesheet->week_start_date
                        && $startDate <= $timesheet->week_end_date
                    )
                    ||
                    (
                        $finishDate >= $timesheet->week_start_date
                        && $finishDate <= $timesheet->week_end_date
                    )
                ;
            });
    }

    protected function calculateSplitDate($labours, $timesheet)
    {
        $tsFromDate = $timesheet->week_start_date;
        $tsToDate = $timesheet->week_end_date;
        $splitDates = [];
        $tsStart = DateTime::createFromFormat('!Y-m-d', $tsFromDate);
        $tsFinish = DateTime::createFromFormat('!Y-m-d', $tsToDate);
        $item = 0;
        foreach ($labours as $labour) {
            $start = Carbon::createFromFormat('Y-m-d H:i', $labour->start_date . " " . $labour->start_time);
            $finish = Carbon::createFromFormat('Y-m-d H:i', $labour->finish_date . " " .
                ($labour->finish_time ?? '00:00'));
            if ($labour->start_date != $labour->finish_date) {
                $labourStartDay = $labour->start_day;
                if ($start >= $tsStart) {
                    $splitDates[strtolower($start->format('d/m/Y'))][$item] = [
                        'start_date' => $start->format('d/m/Y'),
                        'end_date' => $start->format('d/m/Y'),
                        'week_day' => $labour->start_day,
                        'dlo_job' => $labour->dlo_job_code,
                        'dlo_job_id' => $labour->dlo_job_id,
                        'start' => $start->format('d/m/Y') . ' ' . $start->format('H:i:s'),
                        'finish' => $start->format('d/m/Y') . ' 23:59:00',
                    ];
                    $labourStartDay++;
                }
                if ($labour->diff > 1) {
                    for ($i = 0; $i < $labour->diff; $i++) {
                        $start = $start->addDay();
                        if ($start >= $tsStart && $start <= $tsFinish) {
                            if (
                                ($labourStartDay !== $labour->start_day && $labourStartDay !== $labour->finish_day) &&
                                ($labourStartDay < $labour->start_day || $labourStartDay > $labour->finish_day)
                            ) {
                                continue;
                            } else {
                                $splitDates[strtolower($start->format('d/m/Y'))][$item] = [
                                    'start_date' => $start->format('d/m/Y'),
                                    'end_date' => $start->format('d/m/Y'),
                                    'week_day' => $start->format('N') - 1,
                                    'dlo_job' => $labour->dlo_job_code,
                                    'dlo_job_id' => $labour->dlo_job_id,
                                    'start' => $start->format('d/m/Y') . ' 00:00:00',
                                    'finish' => $start->format('d/m/Y') . ' 23:59:00',
                                ];
                                $labourStartDay++;
                            }
                        }
                    }
                }
                if ($finish <= $tsFinish) {
                    $labourStartDay--;
                    if (
                        ($labourStartDay !== $labour->start_day && $labourStartDay !== $labour->finish_day) &&
                        ($labourStartDay < $labour->start_day || $labourStartDay > $labour->finish_day)
                    ) {
                        continue;
                    } else {
                        $splitDates[strtolower($finish->format('d/m/Y'))][$item] = [
                            'start_date' => $finish->format('d/m/Y'),
                            'end_date' => $finish->format('d/m/Y'),
                            'week_day' => $labour->finish_day,
                            'dlo_job' => $labour->dlo_job_code,
                            'dlo_job_id' => $labour->dlo_job_id,
                            'start' => $finish->format('d/m/Y') . ' 00:00:00',
                            'finish' => $finish->format('d/m/Y') . ' ' . $finish->format('H:i:s'),
                        ];
                    }
                }
            } else {
                $splitDates[strtolower($start->format('d/m/Y'))][$item] = [
                    'start_date' => $start->format('d/m/Y'),
                    'end_date' => $start->format('d/m/Y'),
                    'week_day' => $labour->start_day,
                    'dlo_job' => $labour->dlo_job_code,
                    'dlo_job_id' => $labour->dlo_job_id,
                    'labour' => max((float)$labour->labour, 0),
                    'labour_1_3' => max((float)$labour->labour_1_3, 0),
                    'labour_1_5' => max((float)$labour->labour_1_5, 0),
                    'labour_2' => max((float)$labour->labour_2, 0),
                ];
            }
            $item++;
        }
        return $splitDates;
    }

    private function calculateTime($splitTimes)
    {
        foreach ($splitTimes as $key => $splitTime) {
            foreach ($splitTime as $k => $split) {
                if (!array_key_exists('labour', $split)) {
                    $hours = $this->jobLabourService->calcStandardHoursForMobile(
                        \DateTime::createFromFormat('d/m/Y H:i:s', $split['start']),
                        \DateTime::createFromFormat('d/m/Y H:i:s', $split['finish']),
                        $this->rateDays
                    );
                    $splitTimes[$key][$k] = array_merge($split, $hours);
                }
            }
        }
        return $splitTimes;
    }

    private function addWeekDate($timesheet): array
    {
        $period = CarbonPeriod::create($timesheet->week_start_date, $timesheet->week_end_date)->toArray();
        foreach ($period as $key => $date) {
            $period[$key] = $date->format('d/m/Y');
        }
        return $period;
    }

    private function formatDataTimesheet($timesheet, $calculateTime)
    {
        $timesheet->total = 0;
        $data = [];
        foreach ($calculateTime as $date => $datum) {
            $data[$date] = [
                'total' => 0
            ];
            array_walk($datum, function ($record) use ($timesheet, &$data, $date) {
                if (!empty($record['labour'])) {
                    $data[$date]['labour'][$record['dlo_job']] = ($data[$date]['labour'][$record['dlo_job']] ?? 0)
                        + $record['labour'];
                }
                if (!empty($record['labour_1_3'])) {
                    $data[$date]['labour_1_3'][$record['dlo_job']] =
                        ($data[$date]['labour_1_3'][$record['dlo_job']] ?? 0) + $record['labour_1_3'];
                }
                if (!empty($record['labour_1_5'])) {
                    $data[$date]['labour_1_5'][$record['dlo_job']] =
                        ($data[$date]['labour_1_5'][$record['dlo_job']] ?? 0) + $record['labour_1_5'];
                }
                if (!empty($record['labour_2'])) {
                    $data[$date]['labour_2'][$record['dlo_job']] =
                        ($data[$date]['labour_2'][$record['dlo_job']] ?? 0) + $record['labour_2'];
                }
                $data[$date]['total'] = $data[$date]['total'] + $record['labour'] + $record['labour_1_3']
                    + $record['labour_1_5'] + $record['labour_2'];
            });
            $timesheet->total_week += $data[$date]['total'];
        }
        return $data;
    }

    private function getContentData($timeSheets)
    {
        foreach ($timeSheets as $timesheet) {
            $labours = $this->getDloLaboursByTimeSheet($timesheet);
            $splitDates = $this->calculateSplitDate($labours, $timesheet);
            $calculateTime = $this->calculateTime($splitDates);
            $timesheet->data = $this->formatDataTimesheet($timesheet, $calculateTime);
            $timesheet->weekDates = $this->addWeekDate($timesheet);
            $timesheet->weekStartDate = Common::dateTimeFieldDisplayFormat($timesheet->week_start_date);
            $timesheet->weekEndDate = Common::dateTimeFieldDisplayFormat($timesheet->week_end_date);
        }
        return $timeSheets;
    }

    public function validateFilter($inputs)
    {
        $inputs = $this->formatInputs($inputs);
        return Validator::make($inputs, $this->validateRules(), $this->validateMessages($inputs));
    }

    public function validateRules()
    {
        return [
            'startDateFrom' => ['bail', 'required', 'date', 'date_format:d/m/Y'],
            'startDateTo' => ['bail', 'required', 'date', 'date_format:d/m/Y', 'before_or_equal:startDateToValid'],
        ];
    }

    public function validateMessages()
    {
        return [
            'startDateTo.before_or_equal' => "Week commencing filter is required and the maximum" .
                " date range can not exceed " . config('cloud.dlo.max_date_range_DLO_15_in_days') . " days.",
        ];
    }

    public function runReport($repOutPutPdfFile, $inputs, $repId)
    {
        $inputs = $this->formatInputs($inputs);
        $reportFilterQuery = null;
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
        }
        $timesheetData = $this->getTimesheetData($inputs);

        $userIDs = $timesheetData->pluck('user_id')->unique();
        $minStartDate = $timesheetData->min('week_start_date');
        $maxStartDate = $timesheetData->max('week_end_date');

        $this->allLabours = $this->getAllDloLaboursByTimeSheet($userIDs, $minStartDate, $maxStartDate);

        $timesheets = $this->getContentData($timesheetData);

        // Create wkhtmltopdf temp folder to store generated files
        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);

        // Header.
        $header = \View::make(
            'reports.wkhtmltopdf.dlo.dlo15.header',
            []
        )->render();

        $headerPath = $generatedPath . "/header.html";
        file_put_contents($headerPath, $header, LOCK_EX);

        // Footer.
        $runDate = date('d/m/Y H:i');
        $footer = \View::make(
            'reports.wkhtmltopdf.dlo.dlo15.footer',
            ['runDate' => $runDate]
        )->render();
        $footerPath = $generatedPath . "/footer.html";
        file_put_contents($footerPath, $footer, LOCK_EX);

        $hourTypes = $this->getDLoHourTypes()->get()->toArray();

        // Contents.
        $content = \View::make(
            'reports.wkhtmltopdf.dlo.dlo15.content',
            [
                'timesheets'       => $timesheets,
                'hourTypes'        => $hourTypes,
                'jobType'          => $this->jobType
            ]
        )->render();

        $contentPath = $generatedPath . "/content.html";

        file_put_contents($contentPath, $content, LOCK_EX);

        $whereCodes = [];               // The codes which have been used in the filter
        $whereTexts = [];               // Plain text about filtering applied
        $orCodes = [];                  // csv string of OR codes
        $bFilterDetail = false;
        if ($reportFilterQuery instanceof IReportFilterQuery) {
            $this->filterText = $reportFilterQuery->getFilterDetailText();
        } else {
            $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
            $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $this->filterText);
        }

        $options = [
            'header-html' => $headerPath,
            'footer-html' => $footerPath,
            'header-spacing' => 4,
            'footer-spacing' => 4,
            'orientation' => 'landscape',
        ];

        $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $options);
        return true;
    }

    private function baseTSQuery($dateField)
    {
        return DloJobLabour::select(
            DB::raw("WEEK(($dateField),3) AS week_no"),
            DB::raw("SUBDATE(DATE($dateField), WEEKDAY(DATE($dateField))) AS week_start_date"),
            DB::raw("ADDDATE(SUBDATE(DATE($dateField), WEEKDAY(DATE($dateField))), 6) AS week_end_date"),
            'id AS user_id',
            'display_name'
        )
        ->join('dlo_job_operative', function ($join) {
            $join->on(
                'dlo_job_operative.dlo_job_operative_id',
                '=',
                'dlo_job_labour.dlo_job_operative_id'
            )
            ->on('dlo_job_operative.dlo_job_id', 'dlo_job_labour.dlo_job_id');
        })
        ->join(
            'user',
            'user.id',
            '=',
            'dlo_job_operative.user_id'
        )
        ->join(
            'dlo_job',
            'dlo_job.dlo_job_id',
            '=',
            'dlo_job_labour.dlo_job_id'
        )
        ->leftjoin(
            'team_member',
            'team_member.dlo_operative_user_id',
            '=',
            'dlo_job_operative.user_id'
        )
        ->where('dlo_job.site_group_id', '=', \Auth::User()->site_group_id)
        ->whereNotNull($dateField);
    }

    private function applyFilters($inputs, $date, &$query)
    {
        if (array_key_exists('startDateFrom', $inputs)) {
            if (($inputs['startDateFrom'] != "") && ($inputs['startDateFrom'] != " ")) {
                $wcDate = $this->wcDate($inputs['startDateFrom']);
                $query->where(DB::raw("SUBDATE(DATE($date), weekday(DATE($date)))"), '>=', $wcDate);
            }
        }

        if (array_key_exists('startDateTo', $inputs)) {
            if (($inputs['startDateTo'] != "") && ($inputs['startDateTo'] != " ")) {
                $toDate = DateTime::createFromFormat('!d/m/Y', $inputs['startDateTo'])->format('Y-m-d');
                $query->where(DB::raw("DATE($date)"), '<=', $toDate);
            }
        }

        if (array_key_exists('team_id', $inputs)) {
            if (($inputs['team_id'] != "") && ($inputs['team_id'] != " ")) {
                $query->where('team_id', $inputs['team_id']);
            }
        }

        if (array_key_exists('dlo_job_type', $inputs)) {
            if (($inputs['dlo_job_type'] != "") && ($inputs['dlo_job_type'] != " ")) {
                $query->where('dlo_job_type_id', $inputs['dlo_job_type']);

                $this->jobType = DloJobType::find((int)$inputs['dlo_job_type'])->dlo_job_type_code;
                $this->jobTypeId = DloJobType::find((int)$inputs['dlo_job_type'])->dlo_job_type_id;
            }
        }


        if (array_key_exists('user_id', $inputs)) {
            if (($inputs['user_id'] != "") && ($inputs['user_id'] != " ")) {
                $query->whereIn('user_id', $inputs['user_id']);
            }
        }
    }

    private function getTimesheetData($inputs)
    {
        $startQuery = $this->baseTSQuery('start_date');
        $this->applyFilters($inputs, 'start_date', $startQuery);

        $finishQuery = $this->baseTSQuery('finish_date');
        $this->applyFilters($inputs, 'finish_date', $finishQuery);

        $query = $startQuery->union($finishQuery);

        $query->orderBy('display_name')
            ->orderBy('week_start_date');

        return $query->distinct()->get();
    }

    private function wcDate($filterDate)
    {
        $date = DateTime::createFromFormat('!d/m/Y', $filterDate);
        $dayNum = $date->format('N') - 1;

        $date->modify("-$dayNum days");

        return $date->format('Y-m-d');
    }

    private function getDLoHourTypes()
    {
        return DloHourType::userSiteGroup()
            ->select('dlo_hour_type_desc')
            ->where('display_order', '>=', 2)
            ->orderBy('display_order');
    }

    private function generateFilterData($filterData, &$whereCodes, &$whereTexts, &$orCodes)
    {
        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Condition Survey Code = '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Condition Survey Description = '" . $val . "'");
        }

        if ($val = Common::iset($filterData['superseded'])) {
            array_push($whereTexts, "Superseded = '" . $val . "'");
        }

        if ($val = Common::iset($filterData['team_id'])) {
            array_push($whereCodes, [
                'team',
                'team_id',
                'team_desc',
                $val,
                'Team'
            ]);
        }

        if ($val = Common::iset($filterData['dlo_job_type'])) {
            array_push($whereCodes, [
                'dlo_job_type',
                'dlo_job_type_id',
                'dlo_job_type_code',
                $val,
                'Job Type'
            ]);
        }

        if ($siteId = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $siteId, "Site Code"]);

            if ($buildingId = Common::iset($filterData['building_id'])) {
                array_push(
                    $whereCodes,
                    ['building', 'building_id', 'building_code', $buildingId, "Building Code"]
                );

                if ($val = Common::iset($filterData['block_id'])) {
                    array_push($whereCodes, [
                        'building_block',
                        'building_block_id',
                        'building_block_code',
                        $val,
                        "Building Block Code"
                    ]);
                }

                if ($val = Common::iset($filterData['room_id'])) {
                    array_push($whereCodes, ['room', 'room_id', 'room_number', $val, "Room Number"]);
                }
            }
        }

        if ($val = Common::iset($filterData['startDateFrom'])) {
            array_push($whereTexts, "Week Commencing from {$val}");
        }

        if ($val = Common::iset($filterData['startDateTo'])) {
            array_push($whereTexts, "Week Commencing to {$val}");
        }

        if ($val = Common::iset($filterData['user_id'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, "Operative"]);
        }
    }

    private function formatInputs($inputs)
    {
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);

            $dateFilter = $reportFilterQuery->getValueFilterField('start_date');
            $jobTypeIds = $reportFilterQuery->getValueFilterField('dlo_job_type_id');
            $jobOperativeIds = $reportFilterQuery->getValueFilterField('dlo_job_operative_id');
            $teamIds = $reportFilterQuery->getValueFilterField('team_id');

            if (!empty($dateFilter)) {
                list($startDateFrom, $startDateTo) = $dateFilter[0];

                $inputs['startDateFrom'] = Common::dateFieldDisplayFormat($startDateFrom);
                $inputs['startDateTo'] = Common::dateFieldDisplayFormat($startDateTo);
                $inputs['startDateToValid'] = Carbon::parse($startDateFrom)
                    ->addDays(config('cloud.dlo.max_date_range_DLO_15_in_days'))->format('d/m/Y');
            }

            if (!empty($jobTypeIds)) {
                $inputs['dlo_job_type'] = $jobTypeIds[0];
            }

            if (!empty($jobOperativeIds)) {
                $inputs['user_id'] = $jobOperativeIds;
            }

            if (!empty($teamIds)) {
                $inputs['team_id'] = $teamIds[0];
            }
        }
        return $inputs;
    }
}
