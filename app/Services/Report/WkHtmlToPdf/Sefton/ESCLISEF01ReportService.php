<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf\Sefton;

use Illuminate\Support\Arr;
use Tfcloud\Controllers\Admin\General\SiteGroupsController;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Common;
use Tfcloud\Models\CrudAccessLevel;
use Tfcloud\Models\EstValuation;
use Tfcloud\Models\GenTable;
use Tfcloud\Models\Module;
use Tfcloud\Models\Report;
use Tfcloud\Services\Admin\General\SiteGroupsService;
use Tfcloud\Services\Estate\InsuranceReinstatementValuationCalculatationService;
use Tfcloud\Services\Estate\ResidualValuationAdditionalCostService;
use Tfcloud\Services\Estate\ValuationService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\WkHtmlToPdf\WkHtmlToPdfReportBaseService;

class ESCLISEF01ReportService extends WkHtmlToPdfReportBaseService
{
    private $sgController;
    private $sgService;
    private $valuationService;

    private $irCalculatorService;
    private $addCostService;


    public $filterText = '';
    private ?ClassMapReportLoader $reportBoxFilterLoader;

    public function __construct(PermissionService $permissionService, Report $report)
    {
        parent::__construct($permissionService);
        $this->sgService = new SiteGroupsService($permissionService);
        $this->sgController = new SiteGroupsController($permissionService, $this->sgService);
        $this->valuationService = new ValuationService($permissionService);
        $this->irCalculatorService = new InsuranceReinstatementValuationCalculatationService($permissionService);
        $this->addCostService = new ResidualValuationAdditionalCostService($permissionService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function runReport($repOutPutPdfFile, $inputs, $repId)
    {
        $footerDate = new \DateTime();
        $siteGroupId = \Auth::User()->site_group_id;
        $photoPath = $this->sgController->getPhotoPath($siteGroupId); //get logo for outputting at top of report

        $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
        $inputs = $this->formatInputs($inputs, $reportFilterQuery);
        $data = $this->getReportData($inputs);

//        foreach ($data as $val) {
//            //Residual Valuation Values
//            $val->total_devt_value = null;
//            $val->total_costs = null;
//            $val->profit_value = null;
//
//            if ($val->valuation_fair_value == 'Y') {
//                $valuation = \Tfcloud\Models\EstValuation::find($val->est_valuation_id);
//                $val->building_value = $valuation->building_value_fair_value;
//            } elseif ($val->valuation_insurance_reinstatement == 'Y') {
//                $irCalculatedTotals = $this->irCalculatorService->calculateTotals($val->est_valuation_id);
//                $val->reinstatement_only = array_get($irCalculatedTotals, 'totalReinstatementOnly', false);
//                $val->rent_value = array_get($irCalculatedTotals, 'lossOfIncome2Years', false);
//            } elseif ($val->valuation_residual == 'Y') {
//                $residualTotals = $this->addCostService->calculateAdditionalCostTotals($val->est_valuation_id);
//                $val->total_devt_value = array_get($residualTotals, 'totalDevtValue', false);
//                $val->total_costs = array_get($residualTotals, 'totalCosts', false);
//                $val->profit_value = array_get($residualTotals, 'profitValue', false);
//            }
//        }

        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);

        // Header.
        $header = \View::make(
            'reports.wkhtmltopdf.estates.sefton.esCliSef01.header',
            [
                'reportLogo' => $this->getHeaderLogo(),
                'title'      => "VALUATION CERTIFICATE ISSUED TO SEFTON COUNCIL "
                    . "FOR CAPITAL ACCOUNTING PURPOSES"
            ]
        )->render();

        $headerPath = $generatedPath . "/header.html";
        file_put_contents($headerPath, $header, LOCK_EX);

        // Footer.
        $footer = \View::make(
            'reports.wkhtmltopdf.estates.sefton.esCliSef01.footer',
            [
                'footerDate' => $footerDate->format('Y/m/d H:i')
            ]
        )->render();
        $footerPath = $generatedPath . "/footer.html";
        file_put_contents($footerPath, $footer, LOCK_EX);

        $content = \View::make(
            'reports.wkhtmltopdf.estates.sefton.esCliSef01.content',
            [
                'data'             => $data,
                'photoPath'        => $photoPath

            ]
        )->render();

        $contentPath = $generatedPath . "/content.html";

        file_put_contents($contentPath, $content, LOCK_EX);

        $whereCodes = [];               // The codes which have been used in the filter
        $whereTexts = [];               // Plain text about filtering applied
        $orCodes = [];                  // csv string of OR codes
        $bFilterDetail = false;

        $this->filterText = $reportFilterQuery->getFilterDetailText();

        $options = [
            'header-spacing' => 4,
            'footer-spacing' => 4,
            'orientation' => 'portrait',
            'header-html' => $headerPath,
            'footer-html' => $footerPath,
        ];

        if (array_key_exists('singleOnly', $inputs)) {
            $repOutPutPdfFile .= '.pdf';
            $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $options);
            return $repOutPutPdfFile;
        } else {
            $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $options);
            return true;
        }
    }

    private function getReportData($inputs)
    {
        $filterData = $inputs;
        foreach ($filterData as $key => $value) {
            if ((empty($value) && !is_numeric($value)) || $value == 'null') {
                unset($filterData[$key]);
            }
        }
        return $this->getAll($filterData)->get();
    }


    private function getAll($inputs)
    {
        if (
            !$this->permissionService->hasModuleAccess(
                Module::MODULE_ESTATES,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
            )
        ) {
            $msg = "Not met minimum read access for estate module in class" .
                " (" . __CLASS__ . ") method (" . __METHOD__ . "). " .
                " See line (" . __LINE__ . ")";

            throw new PermissionException($msg);
        }

        return $this->valuationService->filterAll($this->all(), $inputs);
    }


    private function all()
    {
        $query = EstValuation::userSiteGroup()
            ->leftjoin('site', 'est_valuation.site_id', "=", "site.site_id")
            ->leftjoin(
                'building',
                'est_valuation.building_id',
                "=",
                "building.building_id"
            )

            ->leftJoin('gen_userdef_group as val_gen_userdef_group', function ($join) {
                $join->on('val_gen_userdef_group.gen_userdef_group_id', '=', 'est_valuation.gen_userdef_group_id')
                    ->on('val_gen_userdef_group.gen_table_id', '=', \DB::raw(GenTable::EST_VALUATION));
            })

//            ->leftjoin('asset_category', 'asset_category.asset_category_id', "=", "site.asset_category_id")
//            ->leftjoin('asset_subcategory', function ($join) {
//                $join->on('asset_subcategory.asset_category_id', 'asset_category.asset_category_id');
//                $join->on('asset_subcategory.asset_subcategory_id', 'site.asset_subcategory_id');
//            })

            ->leftjoin(
                'valuation_basis',
                'est_valuation.valuation_basis_id',
                "=",
                "valuation_basis.valuation_basis_id"
            )


            ->leftjoin(
                'address as site_addr',
                'site_addr.address_id',
                "=",
                "site.address_id"
            )

            ->leftjoin(
                'address_street as site_addr_street',
                'site_addr_street.address_street_id',
                "=",
                "site_addr.address_street_id"
            )

            ->leftjoin(
                'address as building_addr',
                'building_addr.address_id',
                "=",
                "building.address_id"
            )

            ->leftjoin(
                'address_street as building_addr_street',
                'building_addr_street.address_street_id',
                "=",
                "building_addr.address_street_id"
            )

            ->leftjoin(
                'prop_tenure as site_prop_tenure',
                'site_prop_tenure.prop_tenure_id',
                "=",
                "site.prop_tenure_id"
            )

            ->leftjoin(
                'prop_tenure as building_prop_tenure',
                'building_prop_tenure.prop_tenure_id',
                "=",
                "building.prop_tenure_id"
            )

            ->leftjoin(
                'site_usage',
                'site_usage.site_usage_id',
                "=",
                "site.site_usage_id"
            )
            ->leftjoin(
                'building_usage',
                'building_usage.building_usage_id',
                "=",
                "building.building_usage_id"
            )

            ->leftjoin(
                'site_designation',
                'site_designation.site_designation_id',
                "=",
                "site.site_designation_id"
            )

            ->leftjoin(
                'site_type',
                'site_type.site_type_id',
                "=",
                "site.site_type_id"
            )

            ->leftjoin(
                'building_type',
                'building_type.building_type_id',
                "=",
                "building.building_type_id"
            )

            ->leftjoin(
                'site_classification',
                'site_classification.site_classification_id',
                "=",
                "site.site_classification_id"
            )
        ;

        $query->select(['est_valuation.est_valuation_id',

            //Filter
            'est_valuation.costs_value',
            'est_valuation.marriage_value',
            'est_valuation.building_value',
            'est_valuation.building_contents_value',
            'est_valuation.land_value',
            'est_valuation.total_value',

            //drc
            'est_valuation.net_replacement_cost_say',
            'est_valuation.capital_value_of_land_say',

            //fair
            'est_valuation.building_value_say',

            //reinstatement
            'est_valuation.reinstatement_only_say',
            'est_valuation.rent_value_say',

            //residual
            'est_valuation.total_devt_value_say',
            'est_valuation.total_costs_say',

            //market value
            'est_valuation.term_total_say',
            'est_valuation.reversion_total_say',

            'est_valuation.say_value',
            'est_valuation.valuer_contact_id',
            'est_valuation.landlord_contact_id',
            'est_valuation.owner_user_id',
            'est_valuation.next_valuation_date',
            'est_valuation.valuation_effective_date',
            'site.asset_subcategory_id',

            'est_valuation.est_valuation_code',
            'est_valuation.est_valuation_desc',
            'est_valuation.valuation_drc',
            'est_valuation.valuation_fair_value',
            'est_valuation.valuation_insurance_reinstatement',
            'est_valuation.valuation_residual',
            'est_valuation.valuation_market_value',
            'est_valuation.total_value',
            'valuation_basis.valuation_basis_code',
            'valuation_basis.valuation_basis_desc',
            'site.site_id',
            'site.site_code',
            'site.site_desc',
            'building.building_id',
            'building.building_code',
            'building.building_desc',

            //Site address fields
            'site_addr.second_addr_obj as site_sub_dwelling',
            'site_addr.first_addr_obj as site_num_name',
            'site_addr_street.street as site_street',
            'site_addr_street.locality as site_locality',
            'site_addr_street.town as site_town',
            'site_addr.postcode as site_postcode',

            //Building address fields
            'building_addr.second_addr_obj as building_sub_dwelling',
            'building_addr.first_addr_obj as building_num_name',
            'building_addr_street.street as building_street',
            'building_addr_street.locality as building_locality',
            'building_addr_street.town as building_town',
            'building_addr.postcode as building_postcode',

            'site_prop_tenure.prop_tenure_code as site_prop_tenure_code',
            'site_prop_tenure.prop_tenure_desc as site_prop_tenure_desc',

            'building_prop_tenure.prop_tenure_code as building_prop_tenure_code',
            'building_prop_tenure.prop_tenure_desc as building_prop_tenure_desc',

            'site_usage.site_usage_code',
            'site_usage.site_usage_desc',

            'building_usage.building_usage_code',
            'building_usage.building_usage_desc',

            'site_type.site_type_code',
            'site_type.site_type_desc',

            'building_type.building_type_code',
            'building_type.building_type_desc',

            'site_designation.site_designation_code',
            'site_designation.site_designation_desc',

            'site_classification.site_classification_code',
            'site_classification.site_classification_desc',

            "valuation_date",
            "val_gen_userdef_group.user_text1 as life_of_property"
        ]);

        return $query;
    }

    private function generateFilterData($filterData, &$whereCodes, &$whereTexts, &$orCodes)
    {
    }

    private function formatInputs($inputs, $reportFilterQuery)
    {
        $inputs['code'] = $reportFilterQuery->getFirstValueFilterField('est_valuation_code');
        $inputs['description'] = $reportFilterQuery->getFirstValueFilterField('est_valuation_desc');
        $inputs['active'] = $reportFilterQuery->getFirstValueFilterField('active');
        $inputs['landlord'] = $reportFilterQuery->getFirstValueFilterField('landlord_contact_id');
        $inputs['owner'] = $reportFilterQuery->getFirstValueFilterField('owner_user_id');
        $inputs['site_id'] = $reportFilterQuery->getFirstValueFilterField('site_id');
        $inputs['building_id'] = $reportFilterQuery->getFirstValueFilterField('building_id');
        $inputs['status'] = $reportFilterQuery->getFirstValueFilterField('est_valuation_status_id');
        $inputs['type'] = $reportFilterQuery->getFirstValueFilterField('valuation_type_id');
        $inputs['current'] = $reportFilterQuery->getFirstValueFilterField('current_val');
        $inputs['ifrsSubCategory'] = $reportFilterQuery->getFirstValueFilterField('asset_subcategory_id');
        $inputs['basis'] = $reportFilterQuery->getFirstValueFilterField('valuation_basis_id');
        $inputs['valuationMode'] = $reportFilterQuery->getFirstValueFilterField('est_valuation_mode_id');
        $inputs['contact'] = $reportFilterQuery->getFirstValueFilterField('valuer_contact_id');
        $inputs['site_analysis'] = $reportFilterQuery->getFirstValueFilterField('site_analysis_id');
        // Address
        $inputs['address'] = $reportFilterQuery->getFirstValueFilterField("address");
        $inputs['addressSub']['sub_dwelling'] = $reportFilterQuery->getFirstValueFilterField("second_addr_obj");
        $inputs['addressSub']['number_name'] = $reportFilterQuery->getFirstValueFilterField("first_addr_obj");
        $inputs['addressSub']['street'] = $reportFilterQuery->getFirstValueFilterField("street");
        $inputs['addressSub']['locality'] = $reportFilterQuery->getFirstValueFilterField("locality");
        $inputs['addressSub']['town'] = $reportFilterQuery->getFirstValueFilterField("town");
        $inputs['addressSub']['region'] = $reportFilterQuery->getFirstValueFilterField("region");
        $inputs['addressSub']['postcode'] = $reportFilterQuery->getFirstValueFilterField("postcode");
        $inputs['addressSub']['country'] = $reportFilterQuery->getFirstValueFilterField("country");

        $buildingValueSay = $reportFilterQuery->getFirstValueFilterField('building_value_say');
        $inputs['buildingValueSayFrom'] = Arr::first($buildingValueSay);
        $inputs['buildingValueSayTo'] = Arr::last($buildingValueSay);

        $capitalValueLandSay = $reportFilterQuery->getFirstValueFilterField('capital_value_of_land_say');
        $inputs['capitalValueLandSayFrom'] = Arr::first($capitalValueLandSay);
        $inputs['capitalValueLandSayTo'] = Arr::last($capitalValueLandSay);

        $contentsValue = $reportFilterQuery->getFirstValueFilterField('building_contents_value');
        $inputs['contentsValueFrom'] = Arr::first($contentsValue);
        $inputs['contentsValueTo'] = Arr::last($contentsValue);

        $costValue = $reportFilterQuery->getFirstValueFilterField('costs_value');
        $inputs['costFrom'] = Arr::first($costValue);
        $inputs['costTo'] = Arr::last($costValue);

        $landValue = $reportFilterQuery->getFirstValueFilterField('land_value');
        $inputs['landValueFrom'] = Arr::first($landValue);
        $inputs['landValueTo'] = Arr::last($landValue);

        $locationValue = $reportFilterQuery->getFirstValueFilterField('location_value');
        $inputs['locationValueFrom'] = Arr::first($locationValue);
        $inputs['locationValueTo'] = Arr::last($locationValue);

        $marriageValue = $reportFilterQuery->getFirstValueFilterField('marriage_value');
        $inputs['marriageValueFrom'] = Arr::first($marriageValue);
        $inputs['marriageValueTo'] = Arr::last($marriageValue);

        $netReplacementSay = $reportFilterQuery->getFirstValueFilterField('net_replacement_cost_say');
        $inputs['netReplacementSayFrom'] = Arr::first($netReplacementSay);
        $inputs['netReplacementSayTo'] = Arr::last($netReplacementSay);

        $nextValuationDate = $reportFilterQuery->getFirstValueFilterField('next_valuation_date');
        $inputs['nextValuationDateFrom'] = Common::dateFieldDisplayFormat(Arr::first($nextValuationDate));
        $inputs['nextValuationDateTo'] = Common::dateFieldDisplayFormat(Arr::last($nextValuationDate));

        $reinstatementOnlySay = $reportFilterQuery->getFirstValueFilterField('reinstatement_only_say');
        $inputs['reinstatementOnlySayFrom'] = Arr::first($reinstatementOnlySay);
        $inputs['reinstatementOnlySayTo'] = Arr::last($reinstatementOnlySay);

        $rentValueSay = $reportFilterQuery->getFirstValueFilterField('rent_value_say');
        $inputs['rentValueSayFrom'] = Arr::first($rentValueSay);
        $inputs['rentValueSayTo'] = Arr::last($rentValueSay);

        $reversionTotalSay = $reportFilterQuery->getFirstValueFilterField('reversion_total_say');
        $inputs['reversionTotalSayFrom'] = Arr::first($reversionTotalSay);
        $inputs['reversionTotalSayTo'] = Arr::last($reversionTotalSay);

        $sayValue = $reportFilterQuery->getFirstValueFilterField('say_value');
        $inputs['sayValueFrom'] = Arr::first($sayValue);
        $inputs['sayValueTo'] = Arr::last($sayValue);

        $termTotalSay = $reportFilterQuery->getFirstValueFilterField('term_total_say');
        $inputs['termTotalSayFrom'] = Arr::first($termTotalSay);
        $inputs['termTotalSayTo'] = Arr::last($termTotalSay);

        $totalCostsSay = $reportFilterQuery->getFirstValueFilterField('total_costs_say');
        $inputs['totalCostsSayFrom'] = Arr::first($totalCostsSay);
        $inputs['totalCostsSayTo'] = Arr::last($totalCostsSay);

        $totalDevtValueSay = $reportFilterQuery->getFirstValueFilterField('total_devt_value_say');
        $inputs['totalDevtValueSayFrom'] = Arr::first($totalDevtValueSay);
        $inputs['totalDevtValueSayTo'] = Arr::last($totalDevtValueSay);

        $totalValue = $reportFilterQuery->getFirstValueFilterField('total_value');
        $inputs['totalValueFrom'] = Arr::first($totalValue);
        $inputs['totalValueTo'] = Arr::last($totalValue);

        $valuationDate = $reportFilterQuery->getFirstValueFilterField('valuation_date');
        $inputs['valuationDateFrom'] = Common::dateFieldDisplayFormat(Arr::first($valuationDate));
        $inputs['valuationDateTo'] = Common::dateFieldDisplayFormat(Arr::last($valuationDate));

        $valuationEffectiveDate = $reportFilterQuery->getFirstValueFilterField('valuation_effective_date');
        $inputs['valuationEffectiveDateFrom'] = Common::dateFieldDisplayFormat(Arr::first($valuationEffectiveDate));
        $inputs['valuationEffectiveDateTo'] = Common::dateFieldDisplayFormat(Arr::last($valuationEffectiveDate));

        $inputs = $this->getUDValueFilterQuery($reportFilterQuery, $inputs);

        return $inputs;
    }
}
