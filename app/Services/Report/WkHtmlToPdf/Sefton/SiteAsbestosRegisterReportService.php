<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf\Sefton;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Exceptions\PermissionException;
use Tfcloud\Lib\Filters\AsbSurveyFilter;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Models\AsbHazard;
use Tfcloud\Models\AsbHazardStatusType;
use Tfcloud\Models\AsbRiskStatus;
use Tfcloud\Models\AsbRoom;
use Tfcloud\Models\AsbRoomStatus;
use Tfcloud\Models\AsbSurveyStatus;
use Tfcloud\Models\CrudAccessLevel;
use Tfcloud\Models\Module;
use Tfcloud\Models\Report;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\WkHtmlToPdf\WkHtmlToPdfReportBaseService;
use Tfcloud\Services\Assessment\Sefton;

class SiteAsbestosRegisterReportService extends WkHtmlToPdfReportBaseService
{
    private $hazardData = [];
    private $totalHazards = 0;
    private $buildingCount = 0 ;
    private $siteText = "";
    public $filterText = '';
    protected $reportBoxFilterLoader;
    protected $reportFilterQuery;

    public function __construct(PermissionService $permissionService, Report $report = null)
    {
        $this->permissionService = $permissionService;
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function runReportSingleSite($site, $reportId, $inputs = [])
    {
        //call report from button not report view
        $inputs['site_id'] = $site->site_id;

        $inputs['building_id'] = null;
        $inputs['room_id'] = null;

        $inputs['singleOnly'] = true;

        return $this->runReport(
            $this->getTempFile(),
            $inputs,
            $reportId
        );
    }

    public function runReport($repOutPutPdfFile, $inputs, $repId)
    {
        $footerDate = new \DateTime();
        $arrayContentHtml = [];

        $this->initialData($inputs);

        $data = [];
        $data['site_text'] = $this->siteText;
        $data['total_hazards'] = $this->totalHazards;

        if (!empty($this->hazardData)) {
            // Create wkhtmltopdf temp folder to store generated files
            $generatedPath = $this->generateWkhtmltopdfTempPath($repId);

            // Header.
            $header = \View::make(
                'reports.wkhtmltopdf.asbestos.sefton.register.header',
                [
                    'reportLogo' => $this->getHeaderLogo(),
                    'title'      => "Sefton Asbestos Register: " . $this->siteText
                ]
            )->render();

            $headerPath = $generatedPath . "/header.html";
            file_put_contents($headerPath, $header, LOCK_EX);

            // Footer.
            $footer = \View::make(
                'reports.wkhtmltopdf.asbestos.sefton.register.footer',
                [
                    'footerDate' => $footerDate->format('Y/m/d H:i'),
                    'siteCode'   => $this->siteText,
                    'user'       => \Auth::user()->display_name
                ]
            )->render();
            $footerPath = $generatedPath . "/footer.html";
            file_put_contents($footerPath, $footer, LOCK_EX);

            // Static text.
            $staticHtml = \View::make(
                'reports.wkhtmltopdf.asbestos.sefton.register.static_text',
                []
            )->render();

            $staticHTMLPath = $generatedPath . "/static.html";
            file_put_contents($staticHTMLPath, $staticHtml, LOCK_EX);
            array_push($arrayContentHtml, $staticHTMLPath);


            $content = \View::make(
                'reports.wkhtmltopdf.asbestos.sefton.register.asbHazardList',
                [
                    'data'         => $data,
                    'siteCode'     => $this->siteText,
                    'hazardData'   => $this->hazardData,
                    'bldgCount'    => $this->buildingCount
                ]
            )->render();

            $contentPath = $generatedPath . "/asbHazardList.html";
            file_put_contents($contentPath, $content, LOCK_EX);
            array_push($arrayContentHtml, $contentPath);

            $options = [
                'header-html' => $headerPath,
                'footer-html' => $footerPath,
                'header-spacing' => 4,
                'footer-spacing' => 4,
                'orientation' => 'landscape',
            ];

            if (array_key_exists('singleOnly', $inputs)) {
                $repOutPutPdfFile .= '.pdf';
                $this->generateSingleHtmlToPdf($arrayContentHtml, $repOutPutPdfFile, $options);
                return $repOutPutPdfFile;
            } else {
                $this->generateSingleHtmlToPdf($arrayContentHtml, $repOutPutPdfFile, $options);
                return true;
            }
        }
        return false;
    }

    public function validateFilter($inputs)
    {
        $inputs = $this->formatInputs($inputs);
        return \Validator::make($inputs, $this->validateRules(), $this->validateMessages());
    }

    public function validateRules()
    {
        \Validator::extend('one_location', function ($attribute, $value, $parameters) {
            if ($value && is_array($value)) {
                return count($value) == 1;
            }
            $required = isset($parameters[0]) ? $parameters[0] : false;
            if ($required) {
                return !empty($value);
            }
            return true;
        });

        $rules = [
            'site_id' => ['one_location:required'],
            'building_id' => ['one_location'],
            'room_id' => ['one_location'],
        ];

        return $rules;
    }

    public function validateMessages()
    {
        $messages = [
            'one_location' => 'Only 1 location can be selected for this report.',
        ];

        return $messages;
    }

    private function formatInputs($inputs)
    {
        $newInputs = [];
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $this->reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $siteId = $this->reportFilterQuery->getValueFilterField('site_id') ?: [];
            $buildingId = $this->reportFilterQuery->getValueFilterField('building_id') ?: [];
            $roomId = $this->reportFilterQuery->getValueFilterField('room_id') ?: [];
            $newInputs['site_id'] = $siteId ?: [];
            $newInputs['building_id'] = $buildingId ?: [];
            $newInputs['room_id'] = $roomId ?: [];
        }

        return $newInputs ?: $inputs;
    }

    private function getTempFile()
    {
        return tempnam(Common::getTempFolder(), 'ASBHRT01SingleFullAsbSurvey');
    }

    private function initialData($inputs)
    {
        if (
            !$this->permissionService->hasModuleAccess(
                Module::MODULE_ASBESTOS,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
            )
        ) {
            $msg = "Not met minimum readonly access for asbestos module in class" .
                " (" . __CLASS__ . ") method (" . __METHOD__ . "). " .
                " See line (" . __LINE__ . ")";
            throw new PermissionException($msg);
        }

        $query = $this->all();

        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $operator = $reportFilterQuery->getOperatorParser();
            $operator->setPrefixTables([
                'asb_hazard' => '*',
                'asb_hazard_status' => ['asb_hazard_status_type_id']
            ]);

            $query = $reportFilterQuery->filterAll($query);
            $siteId = $reportFilterQuery->getValueFilterField('site_id') ?: [];
            $buildingId = $reportFilterQuery->getValueFilterField('building_id') ?: [];
            $roomId = $reportFilterQuery->getValueFilterField('room_id') ?: [];
        } else {
            $query = $this->filterAll($query, $inputs);
            $siteId = $inputs['site_id'];
            $buildingId = $inputs['building_id'];
            $roomId = $inputs['room_id'];
        }

        // The code below is commented out until we decide how to store No Hazard records.
        $query->unionAll($this->noAccessSelect($siteId, $buildingId, $roomId));

        $query->orderBy('site')
            ->orderBy('building')
            ->orderBy('room')
            ->orderBy('asb_hazard_code');

        $this->prepareReportData($query->get());
    }

    private function filterAll($query, $inputs)
    {
        $filter = new AsbSurveyFilter($inputs);
        $siteId = $buildingId = $roomId = false;

        if (!is_null($filter->site_id) && $filter->site_id) {
            $siteId = $filter->site_id;

            if (!is_null($filter->building_id) && $filter->building_id) {
                $buildingId = $filter->building_id;

                if (!is_null($filter->room_id) && $filter->room_id) {
                    $roomId = $filter->room_id;
                }
            }
        }

        if (!is_null($filter->surveyType) && $filter->surveyType) {
            $sub = \DB::table('asb_survey_hazard')
                ->join(
                    'asb_survey',
                    'asb_survey.asb_survey_id',
                    '=',
                    'asb_survey_hazard.asb_survey_id'
                )
                ->select('asb_survey_hazard.asb_hazard_id')
                ->where('asb_survey.survey_type_id', $filter->surveyType)
                ->distinct('asb_survey_hazard.asb_hazard_id');

            $query->join(\DB::raw("( {$sub->toSql()} ) as ST"), 'ST.asb_hazard_id', '=', 'asb_hazard.asb_hazard_id')
                ->mergeBindings($sub);
        }

        if (!is_null($filter->surveyor) && $filter->surveyor) {
            $sub = \DB::table('asb_survey_hazard')
                ->join(
                    'asb_survey',
                    'asb_survey.asb_survey_id',
                    '=',
                    'asb_survey_hazard.asb_survey_id'
                )
                ->select('asb_survey_hazard.asb_hazard_id')
                ->where('asb_survey.surveyor_id', $filter->surveyor)
                ->distinct('asb_survey_hazard.asb_hazard_id');

            $query->join(\DB::raw("( {$sub->toSql()} ) as SURV"), 'SURV.asb_hazard_id', '=', 'asb_hazard.asb_hazard_id')
                ->mergeBindings($sub);
        }


        if (!is_null($filter->completed)) {
            $sub = \DB::table('asb_action')
                ->select('asb_action.asb_hazard_id', 'asb_action.completed')
                ->where('asb_action.completed', $filter->completed)
                ->distinct('asb_action.asb_hazard_id');

            $query->join(\DB::raw("( {$sub->toSql()} ) as COMP"), 'COMP.asb_hazard_id', '=', 'asb_hazard.asb_hazard_id')
                ->mergeBindings($sub);
           // $query->where($asbActionTable . '.completed', $filter->completed);
        }

        if (
            !is_null($filter->presumed) || !is_null($filter->strpresumed) || !is_null($filter->detected)
            || !is_null($filter->removed) || !is_null($filter->nad)
        ) {
            $options = [];

            if (!is_null($filter->presumed)) {
                array_push($options, AsbHazardStatusType::PRESUMED);
            }
            if (!is_null($filter->strpresumed)) {
                array_push($options, AsbHazardStatusType::STRPRESUMED);
            }
            if (!is_null($filter->detected)) {
                array_push($options, AsbHazardStatusType::DETECTED);
            }

            if (!is_null($filter->removed)) {
                array_push($options, AsbHazardStatusType::REMOVED);
            }

            if (!is_null($filter->nad)) {
                array_push($options, AsbHazardStatusType::NAD);
            }

            $query->whereIn('asb_hazard_status.asb_hazard_status_type_id', $options);
        }

        $query->where('site.site_id', $siteId)
            ->where('asb_hazard.site_group_id', \Auth::user()->site_group_id);

        $this->permissionService->listViewSiteAccess($query, "asb_hazard.site_id", $siteAccess);

        if ($buildingId) {
            $query->where('building.building_id', $buildingId);
        }
        if ($roomId) {
            $query->where('room.room_id', $roomId);
        }

        // CLD-13305: Restrict to asr records against current surveys
        $query->whereExists(function ($query) {
            $query->select(\DB::raw(1))
                ->from('asb_survey')
                ->join('asb_survey_hazard', 'asb_survey_hazard.asb_survey_id', '=', 'asb_survey.asb_survey_id')
                ->where('asb_survey.survey_status_id', AsbSurveyStatus::CURRENT)
                ->where('asb_survey_hazard.asb_hazard_id', \DB::raw('asb_hazard.asb_hazard_id'));
        });

        return $query;
    }

    private function all()
    {
        $selects = [
            'asb_hazard.site_id',
            'asb_hazard.building_id',
            \DB::raw("COALESCE(asb_hazard.room_id, -1) AS room_id"),
            'asb_hazard.asb_hazard_id',
            \DB::raw("CONCAT(site.site_code, ' - ', site.site_desc) AS site"),
            \DB::raw("CONCAT(building.building_code, ' - ', building.building_desc) AS building") ,

            \DB::raw("(
                        CASE
                            WHEN room.room_number IS NULL
                            THEN NULL
                            WHEN room.room_number is not null and room.room_desc is null
                            THEN room.room_number
                            ELSE  CONCAT(room.room_number, ' - ', room.room_desc)
                        END
                      ) AS room"),

//            \DB::raw("CONCAT(room.room_number, ' - ', room.room_desc) AS room"),

            'asb_hazard.asb_hazard_code',
            'asb_hazard.asb_hazard_desc',
            'asb_hazard.asb_hazard_position',
            'asb_material_type.asb_material_type_desc',
            'asb_material_subtype.asb_material_subtype_desc',
            'asb_asbestos_type.asb_asbestos_type_desc',
            \DB::raw("CASE asb_hazard_status.asb_hazard_status_type_id
		WHEN 4 THEN NULL
                ELSE asb_overall_risk.overall_risk_code
                END AS overall_risk_code"),
            //'asb_overall_risk.overall_risk_code',
            'asb_hazard_status.asb_hazard_status_desc',
            \DB::raw("'' AS asb_room_status_code"),
            \DB::raw("'' AS non_asbestos_comment"),
            \DB::raw("'' AS asb_room_comment")
        ];

        $query = AsbHazard
            ::leftJoin(
                'asb_material_type',
                'asb_material_type.asb_material_type_id',
                '=',
                'asb_hazard.asb_material_type_id'
            )
            ->leftJoin(
                'asb_material_subtype',
                'asb_material_subtype.asb_material_subtype_id',
                '=',
                'asb_hazard.asb_material_subtype_id'
            )
            ->leftJoin('asb_sample', 'asb_sample.asb_sample_id', '=', 'asb_hazard.asb_sample_id')
            ->leftJoin(
                'asb_asbestos_type',
                'asb_asbestos_type.asb_asbestos_type_id',
                '=',
                'asb_sample.asb_asbestos_type_id'
            )
            ->join('site', 'site.site_id', '=', 'asb_hazard.site_id')
            ->leftJoin('building', 'building.building_id', '=', 'asb_hazard.building_id')
            ->join(
                'asb_hazard_status',
                'asb_hazard_status.asb_hazard_status_id',
                '=',
                'asb_hazard.asb_hazard_status_id'
            )
            ->leftJoin('room', 'room.room_id', '=', 'asb_hazard.room_id')
            ->leftJoin('asb_risk_assessment', function ($join) {
                $join->on('asb_hazard.asb_hazard_id', '=', 'asb_risk_assessment.asb_hazard_id')
                    ->where('asb_risk_assessment.asb_risk_status_id', '=', AsbRiskStatus::CURRENT);
            })
            ->leftJoin('asb_overall_risk', function ($join) {
                $join->on('asb_overall_risk.from_score', '<=', 'asb_risk_assessment.total_overall_score')
                    ->on('asb_overall_risk.to_less_than_score', '>', 'asb_risk_assessment.total_overall_score');
            })

//            ->leftJoin('asb_action', 'asb_action.asb_hazard_id', '=', 'asb_action.asb_hazard_id')

            ->select($selects);

        return $query;
    }

    private function noAccessSelect($siteId = false, $buildingId = false, $roomId = false)
    {
        $selects = [
            'building.site_id',
            'asb_room.building_id',
            'asb_room.room_id',
            \DB::raw("NULL AS asb_hazard_id"),
            \DB::raw("CONCAT(site.site_code, ' - ', site.site_desc) AS site"),
            \DB::raw("CONCAT(building.building_code, ' - ', building.building_desc) AS building"),
            \DB::raw("CONCAT(room.room_number, ' - ', room.room_desc) AS room"),
            \DB::raw("'' AS asb_hazard_code"),
            \DB::raw("'' AS asb_hazard_desc"),
            \DB::raw("'' AS asb_hazard_position"),
            \DB::raw("'' AS asb_material_type_desc"),
            \DB::raw("'' AS asb_material_subtype_desc"),
            \DB::raw("'' AS asb_asbestos_type_desc"),
            \DB::raw("'' AS overall_risk_code"),
            \DB::raw("'' AS asb_hazard_status_code"),
            'asb_room_status.asb_room_status_code',
            'asb_room.non_asbestos_comment',
            'asb_room.asb_room_comment',
        ];

        $query = AsbRoom
            ::join(
                'building',
                'building.building_id',
                '=',
                'asb_room.building_id'
            )
            ->join(
                'site',
                'site.site_id',
                '=',
                'building.site_id'
            )
            ->join('room', 'room.room_id', '=', 'asb_room.room_id')
            ->join(
                'asb_room_status',
                'asb_room_status.asb_room_status_id',
                '=',
                'asb_room.asb_room_status_id'
            )
            ->join('asb_survey', 'asb_survey.asb_survey_id', '=', 'asb_room.asb_survey_id');

        $query
            ->whereIn(
                'asb_room.asb_room_status_id',
                [
                    AsbRoomStatus::NOT_SURVEYED,
                    AsbRoomStatus::NO_ACCESS,
                    AsbRoomStatus::LIMITED_ACCESS,
                    Sefton\ImportSurvey::NO_HAZARD
                ]
            )
            ->where('site.site_group_id', \Auth::user()->site_group_id);

        $this->permissionService->listViewSiteAccess($query, "building.site_id", $siteAccess);

        if ($siteId) {
            if (is_array($siteId)) {
                $query->whereIn('site.site_id', $siteId);
            } else {
                $query->where('site.site_id', $siteId);
            }
        }

        if ($buildingId) {
            if (is_array($buildingId)) {
                $query->whereIn('building.building_id', $buildingId);
            } else {
                $query->where('building.building_id', $buildingId);
            }
        }
        if ($roomId) {
            if (is_array($roomId)) {
                $query->whereIn('room.room_id', $roomId);
            } else {
                $query->where('room.room_id', $roomId);
            }
        }

        // CLD-13305: Restrict to room records against current surveys
        $query->where('asb_survey.survey_status_id', AsbSurveyStatus::CURRENT);

        return $query->select($selects);
    }

    private function prepareReportData($data)
    {
        $currentBuildingId = false;
        $currentRoomId = false;
        $currentHazardId = false;

        $hazardData = [];
        $hazardData['building_recs'] = [];

        foreach ($data as $row) {
            $this->siteText     = $row->site;

            // Buildings.
            if ($currentBuildingId !== $row->building_id) {
                $buildingRec = [];
                $currentBuildingId = $row->building_id;
                $buildingRec['building'] = $row->building;
                $buildingRec['room_recs'] = [];
                $hazardData['building_recs'][$currentBuildingId] = $buildingRec;
                $currentRoomId = false;
            }

            // Rooms.
            if ($currentRoomId !== $row->room_id) {
                $roomRec = [];
                if ($row->room_id) {
                    $currentRoomId = $row->room_id;
                    $roomRec['room'] = $row->room;
                }

                if ($row->room_id === -1) {
                    $roomRec['room_text'] = 'No Room';
                    $roomRec['room'] = null;
                }

                if (!$row->asb_hazard_id) {
                    $roomRec['asb_room_status'] = $row->asb_room_status_code;
                    $roomRec['non_asbestos_comment'] = $row->non_asbestos_comment;
                    $roomRec['asb_room_comment'] = $row->asb_room_comment;
                }

                $roomRec['haz_recs'] = [];
                $hazardData['building_recs'][$currentBuildingId]['room_recs'][$currentRoomId] = $roomRec;
            }

            // Add the hazards to the rooms.
            array_push(
                $hazardData['building_recs'][$currentBuildingId]['room_recs'][$currentRoomId]['haz_recs'],
                $row->toArray()
            );
        }

        $this->hazardData = $hazardData;
    }
}
