<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf\Sefton;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Exceptions\PermissionException;
use Tfcloud\Lib\Filters\AsbSurveyFilter;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Models\AsbAction;
use Tfcloud\Models\AsbHazardStatusType;
use Tfcloud\Models\AsbRiskStatus;
use Tfcloud\Models\AsbSurveyStatus;
use Tfcloud\Models\CrudAccessLevel;
use Tfcloud\Models\Module;
use Tfcloud\Models\Report;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\WkHtmlToPdf\WkHtmlToPdfReportBaseService;

class SiteAsbestosActionReportService extends WkHtmlToPdfReportBaseService
{
    private $hazardData = [];
    private $totalHazards = 0;
    private $buildingCount = 0 ;
    private $siteText = "";
    public $filterText = '';
    protected $reportBoxFilterLoader;
    protected $reportFilterQuery;

    public function __construct(PermissionService $permissionService, Report $report = null)
    {
        $this->permissionService = $permissionService;
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function runReportSingleSite($site, $reportId)
    {
        //call report from button not report view
        $inputs = [];
        $inputs['site_id'] = $site->site_id;
        $inputs['singleOnly'] = true;

        return $this->runReport(
            $this->getTempFile(),
            $inputs,
            $reportId
        );
    }

    public function runReport($repOutPutPdfFile, $inputs, $repId)
    {
        $footerDate = new \DateTime();
        $arrayContentHtml = [];

        $this->initialData($inputs);

        $data = [];
        $data['site_text'] = $this->siteText;
        $data['total_hazards'] = $this->totalHazards;

        if (!empty($this->hazardData)) {
            // Create wkhtmltopdf temp folder to store generated files
            $generatedPath = $this->generateWkhtmltopdfTempPath($repId);

            // Header.
            $header = \View::make(
                'reports.wkhtmltopdf.asbestos.sefton.action.header',
                [
                    'reportLogo' => $this->getHeaderLogo(),
                    'title'      => "Asbestos Actions by Location: " . $this->siteText
                ]
            )->render();

            $headerPath = $generatedPath . "/header.html";
            file_put_contents($headerPath, $header, LOCK_EX);

            // Footer.
            $footer = \View::make(
                'reports.wkhtmltopdf.asbestos.sefton.action.footer',
                [
                    'footerDate' => $footerDate->format('Y/m/d H:i'),
                    'siteCode'   => $this->siteText,
                    'user'       => \Auth::user()->display_name
                ]
            )->render();
            $footerPath = $generatedPath . "/footer.html";
            file_put_contents($footerPath, $footer, LOCK_EX);

            // Detail.
            $content = \View::make(
                'reports.wkhtmltopdf.asbestos.sefton.action.asbHazardActionList',
                [
                    'data'         => $data,
                    'hazardData'   => $this->hazardData,
                    'bldgCount'    => $this->buildingCount
                ]
            )->render();

            $contentPath = $generatedPath . "/asbHazardList.html";
            file_put_contents($contentPath, $content, LOCK_EX);
            array_push($arrayContentHtml, $contentPath);

            $options = [
                'header-html' => $headerPath,
                'footer-html' => $footerPath,
                'header-spacing' => 4,
                'footer-spacing' => 4,
                'orientation' => 'landscape',
            ];

            if (array_key_exists('singleOnly', $inputs)) {
                $repOutPutPdfFile .= '.pdf';
                $this->generateSingleHtmlToPdf($arrayContentHtml, $repOutPutPdfFile, $options);
                return $repOutPutPdfFile;
            } else {
                $this->generateSingleHtmlToPdf($arrayContentHtml, $repOutPutPdfFile, $options);
                return true;
            }
        }
        return false;
    }

    public function validateFilter($inputs)
    {
        $inputs = $this->formatInputs($inputs);
        return \Validator::make($inputs, $this->validateRules(), $this->validateMessages());
    }

    public function validateRules()
    {
        \Validator::extend('one_location', function ($attribute, $value, $parameters) {
            if ($value && is_array($value)) {
                return count($value) == 1;
            }
            $required = isset($parameters[0]) ? $parameters[0] : false;
            if ($required) {
                return !empty($value);
            }
            return true;
        });

        return [
            'site_id' => ['one_location:required'],
            'building_id' => ['one_location'],
            'room_id' => ['one_location'],
        ];
    }

    public function validateMessages()
    {
        return [
            'one_location' => 'Only 1 location can be selected for this report.',
        ];
    }

    private function formatInputs($inputs)
    {
        $newInputs = [];
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $this->reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $siteId = $this->reportFilterQuery->getValueFilterField('site_id') ?: [];
            $buildingId = $this->reportFilterQuery->getValueFilterField('building_id') ?: [];
            $roomId = $this->reportFilterQuery->getValueFilterField('room_id') ?: [];
            $newInputs['site_id'] = $siteId ?: [];
            $newInputs['building_id'] = $buildingId ?: [];
            $newInputs['room_id'] = $roomId ?: [];
        }

        return $newInputs ?: $inputs;
    }

    private function getTempFile()
    {
        return tempnam(Common::getTempFolder(), 'ASBHRT01SingleFullAsbSurvey');
    }

    private function initialData($inputs)
    {
        if (
            !$this->permissionService->hasModuleAccess(
                Module::MODULE_ASBESTOS,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
            )
        ) {
            $msg = "Not met minimum readonly access for asbestos module in class" .
                " (" . __CLASS__ . ") method (" . __METHOD__ . "). " .
                " See line (" . __LINE__ . ")";
            throw new PermissionException($msg);
        }

        $query = $this->all();
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $operator = $reportFilterQuery->getOperatorParser();
            $operator->setPrefixTables([
                'asb_hazard' => '*',
                'asb_hazard_status' => ['asb_hazard_status_type_id']
            ]);

            $query = $reportFilterQuery->filterAll($query);
        } else {
            $query = $this->filterAll($query, $inputs);
        }

        $query->groupBy('asb_hazard.site_id')
            ->groupBy('asb_hazard.building_id')
            ->groupBy('asb_hazard.room_id')
            ->groupBy('asb_hazard.asb_hazard_id')
            ->groupBy('asb_action.asb_action_id');

            $query->orderBy('site')
            ->orderBy('building')
            ->orderBy('room.room_number')
            ->orderBy('asb_hazard.asb_hazard_code')
            ->orderBy('asb_action.asb_action_code');

        $this->prepareReportData($query->get());
    }

    private function filterAll($query, $inputs)
    {
        $filter = new AsbSurveyFilter($inputs);
        $siteId = $buildingId = $roomId = false;

        if (!is_null($filter->site_id) && $filter->site_id) {
            $siteId = $filter->site_id;

            if (!is_null($filter->building_id) && $filter->building_id) {
                $buildingId = $filter->building_id;

                if (!is_null($filter->room_id) && $filter->room_id) {
                    $roomId = $filter->room_id;
                }
            }
        }

        if (!is_null($filter->surveyType) && $filter->surveyType) {
            $sub = \DB::table('asb_survey_hazard')
                ->join(
                    'asb_survey',
                    'asb_survey.asb_survey_id',
                    '=',
                    'asb_survey_hazard.asb_survey_id'
                )
                ->select('asb_survey_hazard.asb_hazard_id')
                ->where('asb_survey.survey_type_id', $filter->surveyType)
                ->distinct('asb_survey_hazard.asb_hazard_id');

            $query->join(\DB::raw("( {$sub->toSql()} ) as ST"), 'ST.asb_hazard_id', '=', 'asb_hazard.asb_hazard_id')
                ->mergeBindings($sub);
        }

        if (!is_null($filter->surveyor) && $filter->surveyor) {
            $sub = \DB::table('asb_survey_hazard')
                ->join(
                    'asb_survey',
                    'asb_survey.asb_survey_id',
                    '=',
                    'asb_survey_hazard.asb_survey_id'
                )
                ->select('asb_survey_hazard.asb_hazard_id')
                ->where('asb_survey.surveyor_id', $filter->surveyor)
                ->distinct('asb_survey_hazard.asb_hazard_id');

            $query->join(\DB::raw("( {$sub->toSql()} ) as SURV"), 'SURV.asb_hazard_id', '=', 'asb_hazard.asb_hazard_id')
                ->mergeBindings($sub);
        }

        if (
            !is_null($filter->presumed) || !is_null($filter->strpresumed) || !is_null($filter->detected)
            || !is_null($filter->removed) || !is_null($filter->nad)
        ) {
            $options = [];

            if (!is_null($filter->presumed)) {
                array_push($options, AsbHazardStatusType::PRESUMED);
            }
            if (!is_null($filter->strpresumed)) {
                array_push($options, AsbHazardStatusType::STRPRESUMED);
            }
            if (!is_null($filter->detected)) {
                array_push($options, AsbHazardStatusType::DETECTED);
            }

            if (!is_null($filter->removed)) {
                array_push($options, AsbHazardStatusType::REMOVED);
            }

            if (!is_null($filter->nad)) {
                array_push($options, AsbHazardStatusType::NAD);
            }

            $query->whereIn('asb_hazard_status.asb_hazard_status_type_id', $options);
        } else {
            $query->whereIn(
                'asb_hazard_status_type_id',
                [AsbHazardStatusType::PRESUMED, AsbHazardStatusType::STRPRESUMED, AsbHazardStatusType::DETECTED]
            );
        }

        $query->where('site.site_id', $siteId)
            ->where('asb_hazard.site_group_id', \Auth::user()->site_group_id);


        $this->permissionService->listViewSiteAccess($query, "asb_hazard.site_id", $siteAccess);

        if ($buildingId) {
            $query->where('building.building_id', $buildingId);
        }
        if ($roomId) {
            $query->where('room.room_id', $roomId);
        }

        if (!is_null($filter->completed)) {
            $query->where('asb_action.completed', $filter->completed);
        }

        // CLD-13305: Restrict to action records against current surveys
        $query->whereExists(function ($query) {
            $query->select(\DB::raw(1))
                ->from('asb_survey')
                ->join('asb_survey_hazard', 'asb_survey_hazard.asb_survey_id', '=', 'asb_survey.asb_survey_id')
                ->where('asb_survey.survey_status_id', AsbSurveyStatus::CURRENT)
                ->where('asb_survey_hazard.asb_hazard_id', \DB::raw('asb_hazard.asb_hazard_id'));
        });

        return $query;
    }

    private function all()
    {
        $selects = [
            'asb_hazard.site_id',
            'asb_hazard.building_id',
            'asb_hazard.room_id',
            'asb_hazard.asb_hazard_id',
            'asb_action_type.asb_action_type_id',
            \DB::raw("CONCAT(site.site_code, ' - ', site.site_desc) AS site"),
            \DB::raw("CONCAT(building.building_code, ' - ', building.building_desc) AS building"),
            \DB::raw("CONCAT(site.site_desc, ' / ', building.building_desc) AS location"),
            \DB::raw("CONCAT(room.room_number, ' - ', room.room_desc) AS room"),
            'asb_hazard.asb_hazard_code',
            'asb_hazard.asb_hazard_desc',
            'asb_hazard.asb_hazard_position',
            'asb_action.asb_action_code',
            'asb_action_type.asb_action_type_desc',
            \DB::raw("DATE_FORMAT(asb_action.target_date, '%d/%m/%Y') AS target_date"),
            \DB::raw("CASE asb_action.completed WHEN 'Y' THEN 'Yes' ELSE 'No' END AS completed"),
            'asb_overall_risk.overall_risk_code'
        ];

        $query = AsbAction
            ::join('asb_action_type', 'asb_action_type.asb_action_type_id', '=', 'asb_action.asb_action_type_id')
            ->join('asb_hazard', 'asb_hazard.asb_hazard_id', '=', 'asb_action.asb_hazard_id')
            ->join('site', 'site.site_id', '=', 'asb_hazard.site_id')
            ->join('building', 'building.building_id', '=', 'asb_hazard.building_id')
            ->join(
                'asb_hazard_status',
                'asb_hazard_status.asb_hazard_status_id',
                '=',
                'asb_hazard.asb_hazard_status_id'
            )
            ->leftJoin('room', 'room.room_id', '=', 'asb_hazard.room_id')
            ->leftJoin('asb_risk_assessment', function ($join) {
                $join->on('asb_hazard.asb_hazard_id', '=', 'asb_risk_assessment.asb_hazard_id')
                    ->where('asb_risk_assessment.asb_risk_status_id', '=', AsbRiskStatus::CURRENT);
            })
            ->leftJoin('asb_overall_risk', function ($join) {
                $join->on('asb_overall_risk.from_score', '<=', 'asb_risk_assessment.total_overall_score')
                    ->on('asb_overall_risk.to_less_than_score', '>', 'asb_risk_assessment.total_overall_score');
            });

        return $query->select($selects);
    }

    private function prepareReportData($data)
    {
        $currentBuildingId = false;
        $currentRoomId = false;
        $currentHazardId = false;

        $hazardData = [];
        $hazardData['building_recs'] = [];

        foreach ($data as $row) {
            $this->siteText     = $row->location;

            // Buildings.
            if ($currentBuildingId !== $row->building_id) {
                $buildingRec = [];
                $currentBuildingId = $row->building_id;
                $buildingRec['location'] = $row->location;
                $buildingRec['room_recs'] = [];
                $hazardData['building_recs'][$currentBuildingId] = $buildingRec;
            }

            // Rooms.
            if ($currentRoomId !== $row->room_id) {
                $roomRec = [];
                if ($row->room_id) {
                    $currentRoomId = $row->room_id;
                    $roomRec['room_text'] = $row->room;
                } else {
                    $currentRoomId = -1;
                    $roomRec['room_text'] = 'No Room';
                }
                // When a room is null, $currentRoomId will be -1 and still drop
                //  into this if statement.
                // So first check that we dont already have data at the current
                // level before resetting and wiping out any
                // existing Hazard data, as the if statement below will not re
                // add the same hazard again.
                // Previously this would then result in the push array failing
                // trying to merge the action array with a null value.
                if (empty($hazardData['building_recs'][$currentBuildingId]['room_recs'][$currentRoomId])) {
                    $roomRec['haz_recs'] = [];
                    $hazardData['building_recs'][$currentBuildingId]['room_recs'][$currentRoomId] = $roomRec;
                }
            }

            // Hazards.
            if ($currentHazardId !== $row->asb_hazard_id) {
                $hazRec = [];
                $currentHazardId = $row->asb_hazard_id;
                $hazRec['hazard_code'] = $row->asb_hazard_code;
                $hazRec['hazard_desc'] = $row->asb_hazard_desc;
                $hazRec['hazard_pos'] = $row->asb_hazard_position;
                $hazRec['action_recs'] = [];

                $hazardData['building_recs'][$currentBuildingId]['room_recs'][$currentRoomId]['haz_recs']
                    [$currentHazardId] = $hazRec;
            }

            // Add the actions to the hazards.
            array_push(
                $hazardData['building_recs'][$currentBuildingId]['room_recs'][$currentRoomId]['haz_recs']
                    [$currentHazardId]['action_recs'],
                $row->toArray()
            );
        }

        $this->hazardData = $hazardData;
    }
}
