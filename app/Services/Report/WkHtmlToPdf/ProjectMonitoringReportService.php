<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use Tfcloud\Lib\BoxFilterQuery\IReportFilterQuery;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Math;
use Tfcloud\Models\FinYear;
use Tfcloud\Models\FinYearPeriod;
use Tfcloud\Models\GenRagStatus;
use Tfcloud\Models\Project;
use Tfcloud\Models\ProjectStatusType;
use Tfcloud\Models\Report;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Project\ProjectService;

class ProjectMonitoringReportService extends WkHtmlToPdfReportBaseService
{
    private $projectService;
    public $filterText = '';
    private $reportBoxFilterLoader = null;

    public function __construct(PermissionService $permissionService, Report $report = null)
    {
        parent::__construct($permissionService);
        $this->projectService = new ProjectService($permissionService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function validateFilter($inputs)
    {
        return \Validator::make($inputs, $this->validateRules(), $this->validateMessages());
    }

    public function validateRules()
    {
        $rules = [];
        if (!FinYear::current()) {
            $rules = [
                'finYear' => ['required'],
            ];
        }

        return $rules;
    }

    public function validateMessages()
    {
        $messages = [
            'finYear.required' => 'There is no current financial year.'
        ];

        return $messages;
    }

    public function runReport($repOutPutPdfFile, $inputs, $repId)
    {
        $reportFilterQuery = null;
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $projects = $reportFilterQuery->filterAll($this->getProjectMonitoring())->get();
        } else {
            $this->formatInputData($inputs);
            $projects = $this->projectService->filterAll($this->getProjectMonitoring(), $inputs)->get();
        }

        $totals = [
            'total_revised_budget' => 0,
            'total_actual_expenditure' => 0,
            'total_current_fin_year' => 0,
            'total_next_1_fin_year' => 0,
            'total_next_2_fin_year' => 0,
        ];
        $currentYear = FinYear::current();
        $next1Year =  $currentYear ? $currentYear->next(1) : null;
        $next2Year =  $currentYear ? $currentYear->next(2) : null;
        $currentYearPeriod = FinYearPeriod::current($currentYear->fin_year_id);

        $finYears = [
            'current_year' => $currentYear->fin_year_desc,
            'current_period_code' => $currentYearPeriod ? $currentYearPeriod->fin_year_period_code : '',
            'current_period_start' => $currentYearPeriod ? $currentYearPeriod->period_start_date : '',
            'next_1_year' => $next1Year ? $next1Year->fin_year_desc : '',
            'next_2_year' => $next2Year ? $next2Year->fin_year_desc : '',
        ];

        $siteGroupDescription = strtoupper(\Auth::user()->siteGroup->description);

        foreach ($projects as $project) {
            $totals['total_revised_budget'] = Math::addCurrency([
                $totals['total_revised_budget'],
                $project->total_project_budget_line
            ]);
            $totals['total_actual_expenditure'] = Math::addCurrency([
                $totals['total_actual_expenditure'],
                $project->sum_approved_certificated_payment,
                $project->sum_approved_invoice,
                $project->sum_dlo_invoice,
            ]);
            $totals['total_current_fin_year'] = Math::addCurrency([
                $totals['total_current_fin_year'],
                $project->total_cash_flow_curent_year
            ]);
            $totals['total_next_1_fin_year'] = Math::addCurrency([
                $totals['total_next_1_fin_year'],
                $project->total_cash_flow_next_1_year
            ]);
            $totals['total_next_2_fin_year'] = Math::addCurrency([
                $totals['total_next_2_fin_year'],
                $project->total_cash_flow_next_2_year
            ]);
        }

        // Create wkhtmltopdf temp folder to store generated files
        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);

        $header = \View::make(
            'reports.wkhtmltopdf.project.pprj11.header',
            ['siteGroupDescription' => $siteGroupDescription, 'finYears' => $finYears]
        )->render();

        $headerPath = $generatedPath . "/header.html";
        file_put_contents($headerPath, $header, LOCK_EX);

        $footer = \View::make('reports.wkhtmltopdf.project.pprj11.footer')->render();
        $footerPath = $generatedPath . "/footer.html";
        file_put_contents($footerPath, $footer, LOCK_EX);

        $content = \View::make(
            'reports.wkhtmltopdf.project.pprj11.content',
            ['projects' => $projects, 'totals' => $totals, 'finYears' => $finYears]
        )->render();
        $contentPath = $generatedPath . "/content.html";
        file_put_contents($contentPath, $content, LOCK_EX);

        $options = [
            'header-html' => $headerPath,
            'footer-html' => $footerPath,
            'header-spacing' => 4,
            'footer-spacing' => 4,
            'orientation' => 'landscape',
        ];

        $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $options);

        $whereCodes = [];               // The codes which have been used in the filter
        $whereTexts = [];               // Plain text about filtering applied
        $orCodes = [];                  // csv string of OR codes
        $bFilterDetail = false;

        if ($reportFilterQuery instanceof IReportFilterQuery) {
            $this->filterText = $reportFilterQuery->getFilterDetailText();
        } else {
            $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
            $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $this->filterText);
        }

        return true;
    }

    private function getProjectMonitoring()
    {
        $query = Project::userSiteGroup()
            ->select([
                'project.*',
                'project_type.*',
                'project_portfolio.*',
                'project_status.*',
                \DB::raw(
                    "(
                    SELECT project_progress.project_progress_desc
                    FROM project_progress
                    WHERE project_progress.project_id = project.project_id
                      ORDER BY project_progress.progress_date DESC
                    LIMIT 1
                    ) AS latest_project_progress"
                ),
                \DB::raw(
                    "(
                    SELECT SUM(project_budget_line.total_amount)
                    FROM project_budget_line
                    WHERE project_budget_line.project_id = project.project_id
                    ) AS total_project_budget_line"
                ),
                \DB::raw(
                    "(
                    SELECT SUM(certificated_payment.net_payment)
                    FROM certificated_payment
                    WHERE certificated_payment.project_id = project.project_id
                        AND certificated_payment.certificated_payment_status_id = 2
                    ) AS sum_approved_certificated_payment"
                ),
                \DB::raw(
                    "(
                    SELECT SUM(
                        IF(invoice.credit_note = 'Y' ,-1 * invoice_fin_account.amount , invoice_fin_account.amount )
                    )
                    FROM instruction
                      INNER JOIN invoice_fin_account ON invoice_fin_account.instruction_id = instruction.instruction_id
                      INNER JOIN invoice ON invoice.invoice_id = invoice_fin_account.invoice_id
                      INNER JOIN invoice_status ON invoice_status.invoice_status_id = invoice.invoice_status_id
                    WHERE instruction.parent_id = project.project_id
                        AND instruction.parent_type_id = 4
                        AND invoice_status.invoice_status_type_id = 4
                    ) AS sum_approved_invoice"
                ),
                \DB::raw(
                    "(
                    SELECT SUM(
                      IF(dlo_sales_invoice.credit = 'Y'
                        , -1 * (
                          dlo_sales_invoice.labour_total
                        + dlo_sales_invoice.material_total
                        + dlo_sales_invoice.material_item_total
                        + dlo_sales_invoice.plant_total
                        + dlo_sales_invoice.subcontract_total
                        + dlo_sales_invoice.dlo_call_out_total
                        + dlo_sales_invoice.dlo_minimum_total
                        + dlo_sales_invoice.other_charge
                        + dlo_sales_invoice.quoted_total
                        )
                        , (
                          dlo_sales_invoice.labour_total
                        + dlo_sales_invoice.material_total
                        + dlo_sales_invoice.material_item_total
                        + dlo_sales_invoice.plant_total
                        + dlo_sales_invoice.subcontract_total
                        + dlo_sales_invoice.dlo_call_out_total
                        + dlo_sales_invoice.dlo_minimum_total
                        + dlo_sales_invoice.other_charge
                        + dlo_sales_invoice.quoted_total
                        )
                      )
                    )
                    FROM dlo_job
                      INNER JOIN dlo_sales_invoice ON dlo_sales_invoice.parent_id = dlo_job.dlo_job_id
                      INNER JOIN dlo_sales_invoice_status
                              ON dlo_sales_invoice_status.dlo_sales_invoice_status_id =
                                 dlo_sales_invoice.dlo_sales_invoice_status_id
                    WHERE dlo_job.dlo_parent_type_id = 3
                      AND dlo_job.parent_id = project.project_id
                      AND dlo_sales_invoice_status.dlo_sales_invoice_status_type_id in (2, 3, 4)
                    ) AS sum_dlo_invoice"
                ),
                \DB::raw(
                    "(
                    SELECT SUM(vw_prj10.Total)
                    FROM vw_prj10
                      INNER JOIN fin_year ON vw_prj10.fin_year_id = fin_year.fin_year_id
                    WHERE vw_prj10.project_id = project.project_id
                          AND fin_year.year_start_date <= CURDATE() AND fin_year.year_end_date >= CURDATE()
                    ) AS total_cash_flow_curent_year"
                ),
                \DB::raw(
                    "(
                    SELECT SUM(vw_prj10.Total)
                    FROM vw_prj10
                      INNER JOIN fin_year ON vw_prj10.fin_year_id = fin_year.fin_year_id
                    WHERE vw_prj10.project_id = project.project_id
                          AND fin_year.year_start_date <= DATE_ADD(CURDATE(), INTERVAL 1 YEAR)
                          AND fin_year.year_end_date >= DATE_ADD(CURDATE(), INTERVAL 1 YEAR)
                    ) AS total_cash_flow_next_1_year"
                ),
                \DB::raw(
                    "(
                    SELECT SUM(vw_prj10.Total)
                    FROM vw_prj10
                      INNER JOIN fin_year ON vw_prj10.fin_year_id = fin_year.fin_year_id
                    WHERE vw_prj10.project_id = project.project_id
                          AND fin_year.year_start_date <= DATE_ADD(CURDATE(), INTERVAL 2 YEAR)
                          AND fin_year.year_end_date >= DATE_ADD(CURDATE(), INTERVAL 2 YEAR)
                    ) AS total_cash_flow_next_2_year"
                )
            ])
            ->join('site', 'site.site_id', '=', 'project.site_id')
            ->leftJoin('project_type', 'project_type.project_type_id', '=', 'project.project_type_id')
            ->leftJoin(
                'project_portfolio',
                'project_portfolio.project_portfolio_id',
                '=',
                'project.project_portfolio_id'
            )
            ->leftJoin('project_status', 'project_status.project_status_id', '=', 'project.project_status_id')
            ->orderBy('project.project_type_id', 'asc')
            ->orderBy('project.project_portfolio_id', 'asc');
        return $query;
    }

    private function formatInputData(&$inputs)
    {
        foreach ($inputs as $key => $value) {
            if ($value === "") {
                unset($inputs[$key]);
            }
        }
    }

    private function generateFilterData($filterData, &$whereCodes, &$whereTexts, &$orCodes)
    {
        if ($val = array_get($filterData, 'project')) {
            array_push(
                $whereCodes,
                ['project', 'project_id', 'project_code', $val, 'Project']
            );
        }

        if ($val = array_get($filterData, 'project_status_id')) {
            array_push(
                $whereCodes,
                ['project_status', 'project_status_id', 'project_status_code', $val, 'Project Status']
            );
        }

        $projectStatusTypes = [];
        if (Common::iset($filterData['draft'])) {
            $projectStatusTypes[] = ProjectStatusType::DRAFT;
        }
        if (Common::iset($filterData['active'])) {
            $projectStatusTypes[] = ProjectStatusType::ACTIVE;
        }
        if (Common::iset($filterData['complete'])) {
            $projectStatusTypes[] = ProjectStatusType::COMPLETE;
        }
        if (Common::iset($filterData['rejected'])) {
            $projectStatusTypes[] = ProjectStatusType::REJECTED;
        }
        if (count($projectStatusTypes)) {
            array_push($orCodes, [
                'project_status_type',
                'project_status_type_id',
                'project_status_type_code',
                implode(',', $projectStatusTypes),
                "Project Status Type"
            ]);
        }

        if ($val = array_get($filterData, 'clientRef')) {
            array_push($whereTexts, "Client ref contains '" . $val . "'");
        }

        $ragStatus = [];
        if (Common::iset($filterData['overallRagRed'])) {
            $ragStatus[] = GenRagStatus::RED;
        }
        if (Common::iset($filterData['overallRagAmber'])) {
            $ragStatus[] = GenRagStatus::AMBER;
        }
        if (Common::iset($filterData['overallRagGreen'])) {
            $ragStatus[] = GenRagStatus::GREEN;
        }
        if (count($ragStatus)) {
            array_push($orCodes, [
                'gen_rag_status',
                'gen_rag_status_id',
                'gen_rag_status_code',
                implode(',', $ragStatus),
                "Overall RAG Status"
            ]);
        }

        $ragStatus = [];
        if (Common::iset($filterData['qualityRagRed'])) {
            $ragStatus[] = GenRagStatus::RED;
        }
        if (Common::iset($filterData['qualityRagAmber'])) {
            $ragStatus[] = GenRagStatus::AMBER;
        }
        if (Common::iset($filterData['qualityRagGreen'])) {
            $ragStatus[] = GenRagStatus::GREEN;
        }
        if (count($ragStatus)) {
            array_push($orCodes, [
                'gen_rag_status',
                'gen_rag_status_id',
                'gen_rag_status_code',
                implode(',', $ragStatus),
                "Quality RAG Status"
            ]);
        }

        $ragStatus = [];
        if (Common::iset($filterData['costRagRed'])) {
            $ragStatus[] = GenRagStatus::RED;
        }
        if (Common::iset($filterData['costRagAmber'])) {
            $ragStatus[] = GenRagStatus::AMBER;
        }
        if (Common::iset($filterData['costRagGreen'])) {
            $ragStatus[] = GenRagStatus::GREEN;
        }
        if (count($ragStatus)) {
            array_push($orCodes, [
                'gen_rag_status',
                'gen_rag_status_id',
                'gen_rag_status_code',
                implode(',', $ragStatus),
                "Cost RAG Status"
            ]);
        }

        $ragStatus = [];
        if (Common::iset($filterData['timeRagRed'])) {
            $ragStatus[] = GenRagStatus::RED;
        }
        if (Common::iset($filterData['timeRagAmber'])) {
            $ragStatus[] = GenRagStatus::AMBER;
        }
        if (Common::iset($filterData['timeRagGreen'])) {
            $ragStatus[] = GenRagStatus::GREEN;
        }
        if (count($ragStatus)) {
            array_push($orCodes, [
                'gen_rag_status',
                'gen_rag_status_id',
                'gen_rag_status_code',
                implode(',', $ragStatus),
                "Time RAG Status"
            ]);
        }

        if ($val = array_get($filterData, 'owner')) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Owner']);
        }

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Code contains '{$filterData['code']}'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Description contains '{$filterData['description']}'");
        }

        if ($siteId = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $siteId, "Site Code"]);

            if ($buildingId = Common::iset($filterData['building_id'])) {
                array_push(
                    $whereCodes,
                    ['building', 'building_id', 'building_code', $buildingId, "Building Code"]
                );
            }
        }

        if ($val = array_get($filterData, 'projectPortfolio')) {
            array_push(
                $whereCodes,
                ['project_portfolio', 'project_portfolio_id', 'project_portfolio_code', $val, 'Project Portfolio']
            );
        }

        if ($val = array_get($filterData, 'establishment')) {
            array_push(
                $whereCodes,
                ['establishment', 'establishment_id', 'establishment_code', $val, 'Establishment']
            );
        }
    }
}
