<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\BoxFilterQuery\IReportFilterQuery;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\ReportConstant;
use Tfcloud\Models\CondLocationElementScore;
use Tfcloud\Models\CondLocationScore;
use Tfcloud\Models\CondLocationType;
use Tfcloud\Models\Condsurvey;
use Tfcloud\Models\Idwork;
use Tfcloud\Models\IdworkCondition;
use Tfcloud\Models\IdworkElement;
use Tfcloud\Models\IdworkPriority;
use Tfcloud\Models\Report;
use Tfcloud\Services\Condition\ConditionService;
use Tfcloud\Services\PermissionService;

class CNDNELincsSurveyBuildingService extends WkHtmlToPdfReportBaseService
{
    private $conditionService;
    public $filterText = '';

    public const REPORT_ID = 278;
    private $reportBoxFilterLoader = null;
    private $reportFilterQuery = null;

    public function __construct(PermissionService $permissionService, Report $report = null)
    {
        parent::__construct($permissionService);
        $this->conditionService = new ConditionService($permissionService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function runReport($repOutPutPdfFile, $inputs, $repId, $condSurveyId = null, &$errMsg = [])
    {
        if (!is_null($condSurveyId)) {
            $inputs['condSurveyId'] = $condSurveyId;
            $runById = true;
        } else {
            $runById = false;
        }

        /*Get condition survey*/
        $query = $this->getCondSurveyDataQuery($inputs);

        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $this->reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $condSurveyData = $this->reportFilterQuery->filterAll($query)->first();
        } else {
            $condSurveyData = $query->first();
        }
        // Create wkhtmltopdf temp folder to store generated files
        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);
        $contentPath = $generatedPath . "/content.html";
        $contentOptions = [
            'header-spacing' => 4,
            'footer-spacing' => 4,
            'orientation' => 'landscape',
            'toc' => false,
        ];
        $contentPdfPath = $generatedPath . '/contentPage.pdf';

        if (isset($condSurveyData)) {
            $siteGroupId = $condSurveyData->site_group_id;
            $condSurveyId = $condSurveyData->condsurvey_id;

            $sitePhoto = $this->getPhoto(
                'site',
                $condSurveyData->site_id,
                $siteGroupId,
                $condSurveyData->site_photo
            );

            $condLocationScore = $this->getCondLocationScore($condSurveyId);
            $buildingId = $condLocationScore ? $condLocationScore->location_record_id : 0;
            $condLocationScoreId = $condLocationScore ? $condLocationScore->cond_location_score_id : 0;

            $IWConditionPriorityHeaders = $this->getIWConditionPriorityHeader();

            $IWElements = $this->getDilapidationBreakdown(
                $IWConditionPriorityHeaders,
                $condSurveyId,
                $buildingId
            );

            $condLocationElementScore = $this->getCondLocationElementScore(
                $condLocationScoreId,
                $siteGroupId
            );
            $IWPhotoCount = 0;
            $IWData = $this->getIWSurveyDefectData($condSurveyId, $buildingId);
            $arrIWPhoto = $this->getIWPhoto($IWData, $siteGroupId, $IWPhotoCount);

            $reportFileName = ReportConstant::SYSTEM_REPORT_CND_CLI_NEL01 . '_' . $condSurveyData->condsurvey_code;

            if (is_null($repOutPutPdfFile)) {
                $repOutPutPdfFile = $this->generateReportOutPutPath($reportFileName);
            }

            $header = \View::make(
                'reports.wkhtmltopdf.condition.cndNELincsSurveyBuilding.header',
                ['siteDescription' => $condSurveyData->site_desc]
            )->render();
            $headerPath = $generatedPath . "/header.html";
            file_put_contents($headerPath, $header, LOCK_EX);

            $footer = \View::make('reports.wkhtmltopdf.condition.cndNELincsSurveyBuilding.footer')->render();
            $footerPath = $generatedPath . "/footer.html";
            file_put_contents($footerPath, $footer, LOCK_EX);

            $coverPage = \View::make(
                'reports.wkhtmltopdf.condition.cndNELincsSurveyBuilding.cover',
                [
                    'condSurveyData' => $condSurveyData,
                    'sitePhoto' => $sitePhoto
                ]
            )->render();
            $coverPath = $generatedPath . "/cover.html";
            file_put_contents($coverPath, $coverPage, LOCK_EX);
            $coverPdfPath = $generatedPath . '/coverPage.pdf';
            $coverOptions = [
                'header-html' => $headerPath,
                'header-spacing' => 4,
                'orientation' => 'landscape'
            ];

            $content = \View::make(
                'reports.wkhtmltopdf.condition.cndNELincsSurveyBuilding.content',
                [
                    'condSurveyData' => $condSurveyData,
                    'sitePhoto' => $sitePhoto,
                    'IWConditionPriorityHeaders' => $IWConditionPriorityHeaders,
                    'IWElements' => $IWElements,
                    'condLocationScore' => $condLocationScore,
                    'condLocationElementScore' => $condLocationElementScore,
                    'IWData' => $IWData,
                    'IWPhotoCount' => $IWPhotoCount,
                    'arrIWPhoto' => $arrIWPhoto,
                ]
            )->render();
            file_put_contents($contentPath, $content, LOCK_EX);
            $contentOptions = array_merge($contentOptions, [
                'header-html' => $headerPath,
                'footer-html' => $footerPath,
                'toc' => true
            ]);
            $this->generateSingleHtmlToPdf($coverPath, $coverPdfPath, $coverOptions);
            $arrPdfReports = [$coverPdfPath, $contentPdfPath];
        } else {
            $content = \View::make(
                'reports.wkhtmltopdf.condition.cndNELincsSurveyBuilding.content'
            )->render();
            file_put_contents($contentPath, $content, LOCK_EX);
            $arrPdfReports = [$contentPdfPath];
        }
        $this->generateSingleHtmlToPdf($contentPath, $contentPdfPath, $contentOptions);
        $this->mergerPdf($arrPdfReports, $repOutPutPdfFile);

        if ($runById) {
            return $repOutPutPdfFile;
        } else {
            $whereCodes = [];               // The codes which have been used in the filter
            $whereTexts = [];               // Plain text about filtering applied
            $orCodes = [];                  // csv string of OR codes
            $bFilterDetail = false;
            if ($this->reportFilterQuery instanceof IReportFilterQuery) {
                $this->filterText = $this->reportFilterQuery->getFilterDetailText();
            } else {
                $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
                $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $this->filterText);
            }
            return true;
        }
    }

    protected function getPhotoStoragePathway($siteGroupId, $itemKey, $photoFolder, $bImages = false)
    {
        $uploadPath = \Config::get('cloud.legacy_paths.uploads');
        $path =  $uploadPath . '/' . $siteGroupId . '/' . $photoFolder . '/' . $itemKey . '/';
        if ($bImages) {
            $path = $path . 'images/';
        }
        return $path;
    }

    private function getCondSurveyDataQuery($inputs)
    {
        $query = Condsurvey::userSiteGroup()
            ->join('site', 'site.site_id', '=', 'condsurvey.site_id')
            ->leftJoin('address', 'address.address_id', '=', 'site.address_id')
            ->leftJoin('address_street', 'address_street.address_street_id', '=', 'address.address_street_id')
            ->leftJoin('contact', 'contact.contact_id', '=', 'condsurvey.surveyor')
            ->leftJoin('score', 'score.score_id', '=', 'condsurvey.score_id')
            ->select([
                'condsurvey.condsurvey_id',
                'condsurvey.site_group_id',
                'condsurvey.condsurvey_code',
                'condsurvey.condsurvey_desc',
                'condsurvey.site_id',
                \DB::raw("DATE_FORMAT(condsurvey.survey_date, '%d/%m/%Y') AS survey_date"),
                'condsurvey.surveyor',
                'condsurvey.gen_summary',
                'condsurvey.score_comment',
                'score.score_code',
                'score.score_desc',
                'contact.contact_name AS surveyor_name',
                'contact.organisation AS surveyor_organisation',
                'site.site_code',
                'site.site_desc',
                'site.uprn',
                'site.photo AS site_photo',
                'address.first_addr_obj AS site_address_number',
                'address_street.street AS site_street',
                'address_street.locality AS site_locality',
                'address_street.town AS site_town',
                'address_street.region AS site_region',
                'address.postcode AS site_postcode'
            ]);

        if (isset($inputs['condsurvey_code'])) {
            $query->where('condsurvey.condsurvey_code', $inputs['condsurvey_code']);
        }

        if (isset($inputs['condSurveyId'])) {
            $query->where('condsurvey.condsurvey_id', $inputs['condSurveyId']);
        }

        return $query;
    }

    private function getPhoto($folderName, $keyId, $siteGroupId, $photoName)
    {
        if (!$photoName) {
            return false;
        }
        $photoPath = $this->getPhotoStoragePathway(
            $siteGroupId,
            $keyId,
            $folderName
        );
        $photoPath = $photoPath . "/" . $photoName;
        if (file_exists($photoPath) && getimagesize($photoPath)) {
            $photoPath = str_replace("/", "\\", $photoPath);
            return $photoPath;
        } else {
            return false;
        }
    }

    private function getCondElementImages($elementId, $siteGroupId)
    {
        $imagePath = $this->getPhotoStoragePathway(
            $siteGroupId,
            $elementId,
            'condlocationelementscore',
            true
        );

        $images = [];
        if (is_writable($imagePath)) {
            $files = new \FilesystemIterator($imagePath, \FilesystemIterator::SKIP_DOTS);
            if (iterator_count($files) > 0) {
                foreach ($files as $file) {
                    $image = $imagePath . "/" . $file->getFilename();
                    if (file_exists($image) && getimagesize($image)) {
                        list($width, $height) = getimagesize($image);
                        if ($width > $height) {
                            $cssClass = 'horizontal';
                        } else {
                            $cssClass = 'vertical';
                        }
                        $image = str_replace("/", "\\", $image);
                        array_push($images, ['src' => $image, 'cssClass' => $cssClass]);
                    }
                }
            }
        }
        return $images;
    }

    private function getIWConditionPriorityHeader()
    {
        $condition = IdworkCondition::userSiteGroup()->select([
            'idwork_condition_id',
            'idwork_condition_code'
        ])->orderBy('idwork_condition_code', 'DESC')->get();
        $priority = IdworkPriority::userSiteGroup()->select([
            'idwork_priority_id',
            'idwork_priority_code'
        ])->orderBy('idwork_priority_code', 'ASC')->get();
        $headers = [];
        foreach ($condition as $conditionItem) {
            $header = [];
            foreach ($priority as $priorityItem) {
                $header['name'] = $conditionItem->idwork_condition_code . $priorityItem->idwork_priority_code;
                $header['conditionId'] = $conditionItem->idwork_condition_id;
                $header['priorityId'] = $priorityItem->idwork_priority_id;
                $header['totalCost'] = 0;
                array_push($headers, $header);
            }
        }
        return $headers;
    }

    private function getDilapidationBreakdown(array &$headers, $condSurveyId, $buildingId)
    {
        $iwElementQuery = IdworkElement::userSiteGroup()->orderBy('idwork_element.idwork_element_code');
        $selects = [
            \DB::raw(
                "CONCAT_WS(' - ', idwork_element.idwork_element_code, idwork_element.idwork_element_desc) AS 'element'"
            ),
            \DB::raw("'' AS 'total_cost'")
        ];
        foreach ($headers as $header) {
            array_push(
                $selects,
                \DB::raw(
                    "(
                        SELECT SUM(idwork.total_cost) FROM idwork
                            WHERE idwork.element_id = idwork_element.idwork_element_id
                                AND idwork.condsurvey_id = $condSurveyId
                                AND idwork.idwork_condition_id = {$header['conditionId']}
                                AND idwork.idwork_priority_id = {$header['priorityId']}
                                AND idwork.building_id = $buildingId
                                AND idwork.room_id IS NULL
                                AND idwork.building_block_id IS NULL
                                AND idwork.plant_id IS NULL
                    ) AS '{$header['name']}'"
                )
            );
        }
        $iwElements = $iwElementQuery->select($selects)->get();
        /*
         * Sum total cost for each row and column
         * */
        foreach ($iwElements as $element) {
            $totalCostForRow = 0.00;
            foreach ($headers as &$header) {
                // needs curly brackets to prevent Array to string conversion errorq
                $totalCostForRow += $element->{$header['name']};
                $header['totalCost'] += $element->{$header['name']};
            }
            $element->total_cost = $totalCostForRow;
        }

        /*
         * Remove empty column
         * */
        $count = count($headers);
        for ($i = 0; $i < $count; $i++) {
            if ($headers[$i]['totalCost'] == 0) {
                unset($headers[$i]);
            }
        }

        return $iwElements;
    }

    private function getCondLocationScore($condSurveyId)
    {
        $condLocationScore = CondLocationScore::select([
            'cond_location_score_id',
            'location_record_id',
            'cond_location_score_comment',
        ])
        ->where('condsurvey_id', $condSurveyId)
        ->where('cond_location_type_id', CondLocationType::TYPE_BUILDING)
        ->orderBy('cond_location_score_id');
        return $condLocationScore->first();
    }

    private function getCondLocationElementScore($condLocationScoreId, $siteGroupId)
    {
        $condLocationElementScoreQuery = CondLocationElementScore::leftJoin(
            'idwork_element',
            'idwork_element.idwork_element_id',
            '=',
            'cond_location_element_score.idwork_element_id'
        )->select([
            'cond_location_element_score.cond_location_element_score_id',
            'cond_location_element_score.comment',
            'idwork_element.idwork_element_id',
            'idwork_element.idwork_element_code',
            'idwork_element.idwork_element_desc',
            \DB::raw("'' AS idwork_element_images")
        ])->where('cond_location_element_score.cond_location_score_id', $condLocationScoreId)
        ->orderBy('idwork_element.idwork_element_code');
        $condLocationElementScore = $condLocationElementScoreQuery->get();
        foreach ($condLocationElementScore as $element) {
            $element->idwork_element_images = $this->getCondElementImages(
                $element->cond_location_element_score_id,
                $siteGroupId
            );
        }

        return $condLocationElementScore;
    }

    private function getIWSurveyDefectData($condSurveyId, $buildingId)
    {
        $idWorkQuery = Idwork::with([
            'idworkRemery',
            'idworkRemery.idworkItemRemedy',
            'idworkRemery.idworkItemRemedy.unitOfMeasure',
        ])
        ->join('idwork_element', 'idwork_element.idwork_element_id', '=', 'idwork.element_id')
        ->leftJoin('unit_of_measure', 'unit_of_measure.unit_of_measure_id', '=', 'idwork.remedy_unit_of_measure_id')
        ->leftJoin('idwork_condition', 'idwork_condition.idwork_condition_id', '=', 'idwork.idwork_condition_id')
        ->leftJoin('idwork_priority', 'idwork_priority.idwork_priority_id', '=', 'idwork.idwork_priority_id')
        ->select([
            'idwork_element.idwork_element_id',
            'idwork_element.idwork_element_code',
            'idwork_element.idwork_element_desc',
            \DB::raw("CONCAT_WS(' ', idwork_element.idwork_element_code, idwork_element.idwork_element_desc) AS
            'idwork_element_code_des'"),
            'idwork.idwork_id',
            'idwork.idwork_code',
            "idwork.location AS defect_location",
            'idwork.defect',
            'idwork.remedy',
            'idwork.comment',
            'idwork.remedy_quantity',
            'idwork.photo',
            'unit_of_measure.unit_of_measure_code as remedy_unit_of_measure_code',
            "idwork.cost AS remedy_cost",
            \DB::raw("CONCAT_WS('', idwork_condition.idwork_condition_code, idwork_priority.idwork_priority_code) AS
            'idwork_condition_priority'")
        ])
        ->where('idwork.condsurvey_id', $condSurveyId)
        ->where('idwork.building_id', $buildingId)
        ->whereNull('idwork.room_id')
        ->whereNull('idwork.building_block_id')
        ->whereNull('idwork.plant_id')
        ->orderBy('idwork_element.idwork_element_code');
        $idWorks = $idWorkQuery->get();
        return $idWorks;
    }

    private function getIWPhoto($idWorks, $siteGroupId, &$photoCount)
    {
        $photoCount = 0;
        $idworkElementGroupID = 0;
        $arrIWPhoto = [];
        $elementDesc = '';
        foreach ($idWorks as $idWork) {
            if ($idWork->idwork_element_id != $idworkElementGroupID) {
                $elementDesc = $idWork->idwork_element_desc;
                $arrIWPhoto["$elementDesc"] = [];
                $idworkElementGroupID = $idWork->idwork_element_id;
            }
            $photoPath = $this->getPhoto(
                'idwork',
                $idWork->idwork_id,
                $siteGroupId,
                $idWork->photo
            );

            /*$photoPath = $this->reducedPhotoPath(
                'idwork',
                $idWork->idwork_id,
                $idWork->photo,
                $siteGroupId
            );*/
            if (file_exists($photoPath) && list($width, $height) = getimagesize($photoPath)) {
                if ($width > $height) {
                    $cssClass = 'horizontal';
                } else {
                    $cssClass = 'vertical';
                }
                $iwPhotoItem = [
                    'iwCode' => $idWork->idwork_code,
                    'src' => $photoPath,
                    'cssClass' => $cssClass,
                    'comment' => $idWork->comment
                ];
                array_push($arrIWPhoto["$elementDesc"], $iwPhotoItem);
                $photoCount++;
            }
        }
        return $arrIWPhoto;
    }

    public function validateFilter($inputs)
    {
        return \Validator::make($this->formatInputs($inputs), $this->validateRules(), $this->validateMessages());
    }

    public function validateRules()
    {
        return [
            'condsurvey_code' => ['required'],
        ];
    }

    public function validateMessages()
    {
        return [
            'condsurvey_code.required' => 'Code is required.'
        ];
    }

    private function generateFilterData($filterData, &$whereCodes, &$whereTexts, &$orCodes)
    {
        if ($val = Common::iset($filterData['condsurvey_code'])) {
            array_push($whereTexts, "Code is {$val}");
        }
    }

    private function formatInputs($inputs)
    {
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $this->reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $conditionCode = $this->reportFilterQuery->getValueFilterField('condsurvey_code');
            $inputs['condsurvey_code'] = $conditionCode ?? null;
        }
        return $inputs;
    }
}
