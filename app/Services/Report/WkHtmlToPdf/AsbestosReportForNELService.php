<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\BoxFilterQuery\IReportFilterQuery;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Filters\Asbestos\AsbHazardFilter;
use Tfcloud\Models\AsbAction;
use Tfcloud\Models\AsbHazardStatusType;
use Tfcloud\Models\AsbRiskStatus;
use Tfcloud\Models\AsbSampleRequirement;
use Tfcloud\Models\AsbSamplevarType;
use Tfcloud\Models\AsbSurveyStatus;
use Tfcloud\Models\Report;
use Tfcloud\Models\Site;
use Tfcloud\Models\Views\VwAsb01;
use Tfcloud\Services\PermissionService;
use ZendPdf\Color\GrayScale;
use ZendPdf\Font;
use ZendPdf\PdfDocument;
use ZendPdf\Style;

class AsbestosReportForNELService extends WkHtmlToPdfReportBaseService
{
    private const ASB_HAZARD_STATUS_ID_NAD = 5;
    private const PAGING_MARGIN_RIGHT = 100;
    private const PAGING_MARGIN_BOTTOM = 12;
    private const NUMBER_PAGE_NOT_INDEX = 1;
    private const PAGING_TEXT = "Page %d of %d";
    protected $site;
    protected $buildingId = null;
    public $filterText = '';
    private $reportBoxFilterLoader = null;

    public function __construct(PermissionService $permissionService, Report $report = null)
    {
        $this->permissionService = $permissionService;
        $this->acmService = \Illuminate\Support\Facades\App::make(
            'Tfcloud\Services\Assessment\ACMService'
        );
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function runReport($repOutPutPdfFile, $inputs, $repId)
    {
        // Create wkhtmltopdf temp folder to store generated files
        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);

        $header = \View::make('reports.wkhtmltopdf.asbestos.nel.header')->render();
        $headerPath = $generatedPath . "/header.html";
        file_put_contents($headerPath, $header, LOCK_EX);

        $footer = \View::make('reports.wkhtmltopdf.asbestos.nel.footer')->render();
        $footerPath = $generatedPath . "/footer.html";
        file_put_contents($footerPath, $footer, LOCK_EX);

        $pdfFileIndex = 1;
        $pageIndex = 1;

        $inputs = $this->formatInputs($inputs);
        $siteId = array_get($inputs, 'site_id');
        if (is_array($siteId)) {
            $siteId = array_get($siteId, 0);
        }

        $this->site = Site::select(
            'site.site_id',
            'site.site_code',
            'site.site_desc',
            'site.uprn',
            'site.photo',
            'vw_site_address.address_combined'
        )
            ->leftJoin('vw_site_address', 'vw_site_address.site_id', '=', 'site.site_id')
            ->where('site.site_id', '=', $siteId)
            ->first();

        //Cover Page
        $arrayCoverInputHtml = $this->getCoverHtml($generatedPath, $pageIndex, $inputs);
        $arrPdfCover = $this->generateMultiHtmlToPdf(
            $arrayCoverInputHtml,
            $generatedPath,
            $repOutPutPdfFile,
            [],
            $pdfFileIndex,
            false
        );

        // Objectives & Scope
        $staticOptions = [
            'header-html' => $headerPath,
            'footer-html' => $footerPath,
            'header-spacing' => 4,
            'footer-spacing' => 4
        ];
        $introStaticHtmlViewPaths = ['reports.wkhtmltopdf.asbestos.nel.intro_3'];
        $introStaticHtmls = $this->getStaticHtml($generatedPath, $introStaticHtmlViewPaths, $pageIndex);
        $arrPdfIntro = $this->generateMultiHtmlToPdf(
            $introStaticHtmls,
            $generatedPath,
            $repOutPutPdfFile,
            $staticOptions,
            $pdfFileIndex,
            false
        );

        // Audit & Review
        $landscapeStaticOptions = array_merge($staticOptions, ['orientation' => 'landscape']);

        $auditHtmlViewPaths = [
            'reports.wkhtmltopdf.asbestos.nel.intro_4',
            'reports.wkhtmltopdf.asbestos.nel.intro_5',
        ];
        $auditHtmls = $this->getStaticHtml($generatedPath, $auditHtmlViewPaths, $pageIndex);
        $arrPdfAudit = $this->generateMultiHtmlToPdf(
            $auditHtmls,
            $generatedPath,
            $repOutPutPdfFile,
            $landscapeStaticOptions,
            $pdfFileIndex,
            false
        );

        $reportFilterQuery = null;
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $reportFilterQuery->getOperatorParser()->setPrefixTables($this->getPrefixTables());
            $acms = $reportFilterQuery->filterAll($this->samplesQuery())->get();
        } else {
            $acms = $this->filterAll($this->samplesQuery(), $inputs)->get();
        }

        $asbSurveys = $this->asbSurveys($inputs)->get();

        // Sample List
        $arraySampleListInputHtml = [];
        $storeHtmlPart = $generatedPath . "/" . $pageIndex . '.html';
        $viewRender = \View::make('reports.wkhtmltopdf.asbestos.nel.sample_list', [
            'acms' => $acms,
        ])->render();
        $pageIndex++;
        file_put_contents($storeHtmlPart, $viewRender, LOCK_EX);
        array_push($arraySampleListInputHtml, $storeHtmlPart);

        $sampleListOptions = [
            'header-html' => $headerPath,
            'footer-html' => $footerPath,
            'header-spacing' => 4,
            'footer-spacing' => 4,
            'orientation' => 'landscape'
        ];
        $arrPdfSampleList = $this->generateMultiHtmlToPdf(
            $arraySampleListInputHtml,
            $generatedPath,
            $repOutPutPdfFile,
            $sampleListOptions,
            $pdfFileIndex,
            false
        );

        // Sample Details
        $actionsArr = $this->actionsQuery();
        $arraySampleDetailsInputHtml = [];
        $firstItem = true;
        foreach ($acms as $item) {
            $photoPath = $this->photoPath('asbhaz', $item->asb_hazard_id, $item->photo, $item->site_group_id, false);
            $images = $this->getACMImagesById($item->asb_hazard_id, false);
            $storeHtmlPart = $generatedPath . "/" . $pageIndex . '.html';

            $actions = array_get($actionsArr, $item->asb_hazard_id);

            $viewRender = \View::make('reports.wkhtmltopdf.asbestos.nel.sample_details', [
                'item'          => $item,
                'isFirstPage'   => $firstItem,
                'actions'       => $actions,
                'photoPath'     => $photoPath,
                'images'          => $images
            ])->render();

            $pageIndex++;

            file_put_contents($storeHtmlPart, $viewRender, LOCK_EX);
            array_push($arraySampleDetailsInputHtml, $storeHtmlPart);
            $firstItem = false;
        }

        $sampleDetailsOptions = [
            'header-html' => $headerPath,
            'footer-html' => $footerPath,
            'header-spacing' => 4,
            'footer-spacing' => 4
        ];
        $arrPdfSampleDetails = $this->generateMultiHtmlToPdf(
            $arraySampleDetailsInputHtml,
            $generatedPath,
            $repOutPutPdfFile,
            $sampleDetailsOptions,
            $pdfFileIndex,
            false
        );

        // Static Pages (after Sample Details)
        // Removed at client request CLD-5921: 'reports.wkhtmltopdf.asbestos.nel.static_5_2',
        $staticHtmlViewPaths = [
            'reports.wkhtmltopdf.asbestos.nel.static_5_1',
            'reports.wkhtmltopdf.asbestos.nel.static_5_3',
        ];
        $arrStaticHtmlAfterSampleDetails = $this->getStaticHtml($generatedPath, $staticHtmlViewPaths, $pageIndex);
        $arrPdfStaticAfterSampleDetails = $this->generateMultiHtmlToPdf(
            $arrStaticHtmlAfterSampleDetails,
            $generatedPath,
            $repOutPutPdfFile,
            $staticOptions,
            $pdfFileIndex,
            false
        );

        // Emergency Plans
        $emerPlanHtmlViewPaths = ['reports.wkhtmltopdf.asbestos.nel.intro_6'];
        $emerPlanHtmls = $this->getStaticHtml($generatedPath, $emerPlanHtmlViewPaths, $pageIndex);
        $arrPdfEmerPlan = $this->generateMultiHtmlToPdf(
            $emerPlanHtmls,
            $generatedPath,
            $repOutPutPdfFile,
            $staticOptions,
            $pdfFileIndex,
            false
        );

        // Asbestos Survey
        $arrayAsbSurveyInputHtml = [];
        $storeHtmlPart = $generatedPath . "/" . $pageIndex . '.html';
        $viewRender = \View::make('reports.wkhtmltopdf.asbestos.nel.asbestos_survey', [
            'asbSurveys' => $asbSurveys,
        ])->render();
        $pageIndex++;
        file_put_contents($storeHtmlPart, $viewRender, LOCK_EX);
        array_push($arrayAsbSurveyInputHtml, $storeHtmlPart);

        $arrPdfAsbSurvey = $this->generateMultiHtmlToPdf(
            $arrayAsbSurveyInputHtml,
            $generatedPath,
            $repOutPutPdfFile,
            $staticOptions,
            $pdfFileIndex,
            false
        );

        // End Static Pages
        $endStaticHtmlViewPaths = [
            'reports.wkhtmltopdf.asbestos.nel.static_7',
            'reports.wkhtmltopdf.asbestos.nel.static_8'
        ];
        $arrEndStaticHtml = $this->getStaticHtml($generatedPath, $endStaticHtmlViewPaths, $pageIndex);
        $arrPdfEndStatic = $this->generateMultiHtmlToPdf(
            $arrEndStaticHtml,
            $generatedPath,
            $repOutPutPdfFile,
            $staticOptions,
            $pdfFileIndex,
            false
        );

        // Appendix
        $arrPdfAppendix = [
            public_path() . '/assets/images/reports/asbestos/nel/'
                . 'BPS 4.3.5 - Asbestos Management Procedure (002).pdf',
        ];

        // Merge all PDF files
        $arrPdfMerge = array_merge(
            $arrPdfCover,
            $arrPdfIntro,
            $arrPdfAudit,
            $arrPdfSampleList,
            $arrPdfSampleDetails,
            $arrPdfStaticAfterSampleDetails,
            $arrPdfEmerPlan,
            $arrPdfAsbSurvey,
            $arrPdfEndStatic,
            $arrPdfAppendix
        );

        $this->mergerPdf($arrPdfMerge, $repOutPutPdfFile);

        $whereCodes = [];               // The codes which have been used in the filter
        $whereTexts = [];               // Plain text about filtering applied
        $orCodes = [];                  // csv string of OR codes
        $bFilterDetail = false;

        if ($reportFilterQuery instanceof IReportFilterQuery) {
            $this->filterText = $reportFilterQuery->getFilterDetailText();
        } else {
            $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
            $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $this->filterText);
        }

        return true;
    }

    public function validateFilter($inputs)
    {
        return \Validator::make($this->formatInputs($inputs), $this->validateRules(), $this->validateMessages());
    }

    public function validateRules()
    {
        \Validator::extend('site_required_one', function ($attribute, $value, $parameters) {
            if (is_array($value)) {
                return count($value) == 1;
            }
            return !empty($value);
        });

        \Validator::extend('single_building', function ($attribute, $value, $parameters) {
            if ($value && is_array($value)) {
                return count($value) == 1;
            }
            return true;
        });

        \Validator::extend('single_room', function ($attribute, $value, $parameters) {
            if ($value && is_array($value)) {
                return count($value) == 1;
            }
            return true;
        });

        $rules = [
            'site_id' => ['site_required_one'],
            'building_id' => ['required_with:room_id', 'single_building'],
            'room_id' => ['single_room'],
        ];

        return $rules;
    }

    public function validateMessages()
    {
        $messages = [
            'site_id.site_required_one' => 'Only 1 location can be selected for this report.',
            'building_id.single_building' => 'Only 1 location can be selected for this report.',
            'room_id.single_room' => 'Only 1 location can be selected for this report.',
        ];

        return $messages;
    }

    private function getCoverHtml($generatedPath, &$pageIndex, $inputs)
    {
        $arrayInputHtml = [];
        $datePrinted = date("d/m/Y");

        $storeHtmlPart = $generatedPath . '/' . $pageIndex . '.html';
        $coverHtml = \View::make('reports.wkhtmltopdf.asbestos.nel.cover', [
            'datePrinted' => $datePrinted,
            'site' => $this->site,
            'siteGroupImagePath' =>  $this->site->photoPath(false)
        ])->render();
        file_put_contents($storeHtmlPart, $coverHtml, LOCK_EX);
        array_push($arrayInputHtml, $storeHtmlPart);
        $pageIndex++;

        $storeHtmlPart = $generatedPath . '/' . $pageIndex . '.html';
        $staticHtml = \View::make('reports.wkhtmltopdf.asbestos.nel.intro_2')->render();
        file_put_contents($storeHtmlPart, $staticHtml, LOCK_EX);
        array_push($arrayInputHtml, $storeHtmlPart);
        $pageIndex++;

        return $arrayInputHtml;
    }

    private function getStaticHtml($generatedPath, $staticHtmlViewPaths, &$pageIndex)
    {
        $arrInputHtmls = [];

        foreach ($staticHtmlViewPaths as $staticHtmlViewPath) {
            $storeHtmlPart = $generatedPath . '/' . $pageIndex . '.html';
            $staticHtml = \View::make($staticHtmlViewPath)->render();
            file_put_contents($storeHtmlPart, $staticHtml, LOCK_EX);
            array_push($arrInputHtmls, $storeHtmlPart);
            $pageIndex++;
        }

        return $arrInputHtmls;
    }

    protected function setPagingFooter(PdfDocument &$pdf, $options = [])
    {
        $totalPage = count($pdf->pages);
        $pageNotIndex = self::NUMBER_PAGE_NOT_INDEX;
        $totalPageIndex = $totalPage - $pageNotIndex;

        $style = new Style();
        $style->setFillColor(new GrayScale(0));
        $style->setLineWidth(3);
        $fontH = Font::fontWithName(Font::FONT_HELVETICA);
        $style->setFont($fontH, 10);

        for ($i = $pageNotIndex; $i < $totalPage; $i++) {
            $pdf->pages[$i]->setStyle($style);
            $width = $pdf->pages[$i]->getWidth();
            $pdf->pages[$i]->drawText(
                sprintf(self::PAGING_TEXT, $i, $totalPageIndex),
                $width - self::PAGING_MARGIN_RIGHT,
                self::PAGING_MARGIN_BOTTOM,
                'UTF-8'
            );
        }
    }

    public function filterAll($query, $inputs)
    {
        $filter = new AsbHazardFilter($inputs);
        $asbHazardTable = 'asb_hazard';
        $asbHazardStatusTable = 'asb_hazard_status';

        if (
            !is_null($filter->presumed) || !is_null($filter->strpresumed) || !is_null($filter->detected)
            || !is_null($filter->removed) || !is_null($filter->nad)
        ) {
            $options = [];

            if (!is_null($filter->presumed)) {
                array_push($options, AsbHazardStatusType::PRESUMED);
            }
            if (!is_null($filter->strpresumed)) {
                array_push($options, AsbHazardStatusType::STRPRESUMED);
            }
            if (!is_null($filter->detected)) {
                array_push($options, AsbHazardStatusType::DETECTED);
            }

            if (!is_null($filter->removed)) {
                array_push($options, AsbHazardStatusType::REMOVED);
            }

            if (!is_null($filter->nad)) {
                array_push($options, AsbHazardStatusType::NAD);
            }

            $query->whereIn($asbHazardStatusTable . '.asb_hazard_status_type_id', $options);
        }

        if (!is_null($filter->asbestosType) && $filter->asbestosType) {
            $query->where($asbHazardTable . '.asb_asbestos_type_id', $filter->asbestosType);
        }

        if (!is_null($filter->materialType) && $filter->materialType) {
            $query->where($asbHazardTable . '.asb_material_type_id', $filter->materialType);
        }

        if (!is_null($filter->materialSubtype) && $filter->materialSubtype) {
            $query->where($asbHazardTable . '.asb_material_subtype_id', $filter->materialSubtype);
        }

        if (!is_null($filter->site) && $filter->site) {
            $query->where($asbHazardTable . '.site_id', $filter->site);

            if (!is_null($filter->building) && $filter->building) {
                $query->where($asbHazardTable . '.building_id', $filter->building);

                if (!is_null($filter->room) && $filter->room) {
                    $query->where($asbHazardTable . '.room_id', $filter->room);
                }
            }
        }

        if (!is_null($filter->site_id) && $filter->site_id) {
            $query->where($asbHazardTable . '.site_id', $filter->site_id);

            if (!is_null($filter->building_id) && $filter->building_id) {
                $query->where($asbHazardTable . '.building_id', $filter->building_id);
                $this->buildingId = $filter->building_id;

                if (!is_null($filter->room_id) && $filter->room_id) {
                    $query->where($asbHazardTable . '.room_id', $filter->room_id);
                }
            }
        }

        if (
            !is_null($filter->sampleYes) || !is_null($filter->sampleNo) || !is_null($filter->sampleTaken)
            || !is_null($filter->sampleRepresentative)
        ) {
            $options = [];

            if (!is_null($filter->sampleYes)) {
                array_push($options, AsbSampleRequirement::REQUIRED);
            }
            if (!is_null($filter->sampleNo)) {
                array_push($options, AsbSampleRequirement::NOT_REQUIRED);
            }
            if (!is_null($filter->sampleTaken)) {
                array_push($options, AsbSampleRequirement::TAKEN);
            }

            if (!is_null($filter->sampleRepresentative)) {
                array_push($options, AsbSampleRequirement::REPRESENTATIVE);
            }

            $query->whereIn($asbHazardTable . '.asb_sample_requirement_id', $options);
        }

        if (!is_null($filter->search) && $filter->search) {
            $query->where(function ($subQuery) use ($asbHazardTable, $filter) {
                $subQuery->where($asbHazardTable . '.asb_hazard_code', 'like', '%' . $filter->search . '%');
                $subQuery->orWhere($asbHazardTable . '.asb_hazard_desc', 'like', '%' . $filter->search . '%');
            });
        }

        return $query;
    }

    public function samplesQuery()
    {
        $selectFields = [
            // Filter & Data fields
            "asb_hazard.site_group_id",
            "asb_hazard.asb_hazard_id",
            "asb_hazard.asb_hazard_code",
            "asb_hazard.asb_hazard_desc",
            "asb_hazard.photo",
            "asb_hazard.asb_material_type_id",
            "asb_hazard.asb_material_subtype_id",
            "asb_hazard.building_id",
            "asb_hazard.room_id",
            "asb_hazard.asb_sample_requirement_id",
            "asb_hazard_status.asb_hazard_status_type_id",
            "site.site_code",
            "building.building_code",
            "room.room_number",

            /* Report Fields */
            // Hazard
            "asb_hazard.asb_hazard_position AS ACM_POSITION",
            "asb_hazard.asb_hazard_desc AS ACM_DESC",
            "asb_hazard.asb_hazard_quantity AS QUANTITY",
            "unit_of_measure.unit_of_measure_code AS UNIT_OF_MEASURE",
            "asb_hazard.asb_hazard_comment AS COMMENT",
            "asb_hazard_status.asb_hazard_status_desc AS HAZARD_STATUS",
            // Sample
            "asb_sample.asb_sample_code AS SAMPLE_CODE",
            \DB::raw("DATE_FORMAT(asb_sample.asb_sample_sampled_date, '%d.%m.%Y') AS SAMPLED_DATE"),
            \DB::raw("DATE_FORMAT(asb_sample.asb_sample_sampled_date, '%D %M %Y') AS SAMPLED_DATE_ENG"),
            "asb_sample_sampler_contact.contact_name AS SAMPLED_BY",
            // Location
            "building.building_code AS BUILDING_CODE",
            "building.building_desc AS BUILDING_DESC",
            "room.room_number AS ROOM_NUMBER",
            "room.room_desc AS ROOM_DESC",
            "room_floor.room_floor_code AS FLOOR_CODE",
            "room_floor.room_floor_desc AS FLOOR_DESC",
            //
            "asb_material_type.asb_material_type_desc AS PRODUCT_TYPE",

            // Material Assessment
            "material_product_type.score_code AS PRODUCT_TYPE_SCORE_CODE",
            "material_product_type.display AS PRODUCT_TYPE_DISPLAY",
            "material_extent_of_damage.score_code AS EXTENT_DAMAGE_SCORE_CODE",
            "material_extent_of_damage.display AS EXTENT_DAMAGE_DISPLAY",
            "material_surface_treatment.score_code AS SURFACE_TREATMENT_SCORE_CODE",
            "material_surface_treatment.display AS SURFACE_TREATMENT_DISPLAY",
            "asb_asbestos_type.score AS ASBESTOS_TYPE_SCORE",
            "asb_asbestos_type.asb_asbestos_type_desc AS ASBESTOS_TYPE_DESC",
            \DB::raw("IFNULL(asb_risk_assessment.total_material_score, 'N/A') AS MATERIAL_SCORE"),

            // Priority Assessment
            \DB::raw(
                "IF(asb_risk_assessment.normal_activity_main_sid IS NULL"
                . ", 'N/A'"
                . ", IF(asb_risk_assessment.normal_activity_secondary_sid IS NULL"
                .   ", 'N/A'"
                .   ", ROUND(("
                .           "normal_activity_main.score"
                .           " + normal_activity_secondary.score"
                .       ") / 2, 0)"
                .   ")"
                . ") AS NORMAL_OCCUPANT_ACTIVITY"
            ),
            \DB::raw(
                "IF(asb_risk_assessment.likelihood_disturb_location_sid IS NULL"
                . ", 'N/A'"
                . ", IF(asb_risk_assessment.likelihood_disturb_accessibility_sid IS NULL"
                .   ", 'N/A'"
                .   ", IF(asb_risk_assessment.likelihood_disturb_extent_sid IS NULL"
                .       ", 'N/A'"
                .       ", ROUND(("
                .               "likelihood_disturb_location.score"
                .               " + likelihood_disturb_accessibility.score"
                .               " + likelihood_disturb_extent.score"
                .           ") / 3, 0)"
                .       ")"
                .   ")"
                . ") AS LIKELIHOOD_OF_DISTURBANCE"
            ),
            \DB::raw(
                "IF(asb_risk_assessment.human_exp_number_sid IS NULL"
                . ", 'N/A'"
                . ", IF(asb_risk_assessment.human_exp_freq_use_sid IS NULL"
                .   ", 'N/A'"
                .   ", IF(asb_risk_assessment.human_exp_average_time_sid IS NULL"
                .       ", 'N/A'"
                .       ", ROUND(("
                .               "human_exp_number.score"
                .               " + human_exp_freq_use.score"
                .               " + human_exp_average_time.score"
                .           ") / 3, 0)"
                .       ")"
                .   ")"
                . ") AS HUMAN_EXPOSURE_POTENTIAL"
            ),
            \DB::raw(
                "IF(asb_risk_assessment.normal_activity_main_sid IS NULL"
                . ", 'N/A'"
                . ", IF(asb_risk_assessment.normal_activity_secondary_sid IS NULL"
                .   ", 'N/A'"
                .   ", ROUND(("
                .           "maint_activity_type.score"
                .           " + maint_activity_freq.score"
                .       ") / 2, 0)"
                .   ")"
                . ") AS MAINTENANCE_ACTIVITY"
            ),
            \DB::raw("IFNULL(asb_risk_assessment.total_priority_score, 'N/A') AS PRIORITY_SCORE"),

            \DB::raw(
                "IF(asb_risk_assessment.total_material_score IS NULL"
                . ", 'N/A'"
                . ", IF(asb_risk_assessment.normal_activity_secondary_sid IS NULL"
                .   ", 'N/A'"
                .   ", asb_risk_assessment.total_material_score + asb_risk_assessment.total_priority_score"
                .   ")"
                . ") AS TOTAL_SCORE"
            ),
            "asb_overall_risk.overall_risk_code AS TOTAL_RISK",

            // Action
            //\DB::raw("GROUP_CONCAT(asb_action_type.asb_action_type_desc SEPARATOR ', ') AS ACTIONS"),
            \DB::raw(
                "GROUP_CONCAT("
                .   " DISTINCT CONCAT_WS(' - ', asb_action_type.asb_action_type_desc, asb_action.comment)"
                .   " ORDER BY asb_action.asb_action_code"
                .   " SEPARATOR '; '"
                . ") AS ACTIONS"
            ),

            \DB::raw("DATE_FORMAT(NOW(), '%d/%m/%Y') AS DATE_PRINTED")
        ];


        $query = \DB::table('asb_hazard')->select($selectFields)
            // Location
            ->join('site', 'site.site_id', '=', 'asb_hazard.site_id')
            ->leftJoin('building', 'building.building_id', '=', 'asb_hazard.building_id')
            ->leftJoin('room', 'room.room_id', '=', 'asb_hazard.room_id')
            ->leftJoin('room_floor', 'room_floor.room_floor_id', '=', 'room.room_floor_id')
            // Asbestos Type
            ->leftJoin(
                'asb_asbestos_type',
                'asb_asbestos_type.asb_asbestos_type_id',
                '=',
                'asb_hazard.asb_asbestos_type_id'
            )
            ->leftJoin(
                'asb_hazard_status',
                'asb_hazard_status.asb_hazard_status_id',
                '=',
                'asb_hazard.asb_hazard_status_id'
            )
            ->leftJoin(
                'asb_sample_requirement',
                'asb_sample_requirement.asb_sample_requirement_id',
                '=',
                'asb_hazard.asb_sample_requirement_id'
            )
            // Just get the CURRENT Risk Assessment
            ->leftJoin('asb_risk_assessment', function ($join) {
                $join->on('asb_risk_assessment.asb_hazard_id', '=', 'asb_hazard.asb_hazard_id')
                    ->on('asb_risk_assessment.asb_risk_status_id', '=', \DB::raw(AsbRiskStatus::CURRENT));
            })
            // Get Risk Category
            ->leftJoin('asb_overall_risk', function ($join) {
                $join->on(
                    'asb_overall_risk.from_score',
                    '<=',
                    \DB::raw(
                        '(IFNULL(asb_risk_assessment.total_material_score,0)'
                        . ' + IFNULL(asb_risk_assessment.total_priority_score,0))'
                    )
                )
                ->on(
                    'asb_overall_risk.to_less_than_score',
                    '>',
                    \DB::raw(
                        '(IFNULL(asb_risk_assessment.total_material_score,0)'
                        . ' + IFNULL(asb_risk_assessment.total_priority_score,0))'
                    )
                );
            })
            // Material Assessment | Product Type
            ->leftJoin('asb_samplevar_type_score AS material_product_type', function ($join) {
                $join->on(
                    'material_product_type.asb_samplevar_type_score_id',
                    '=',
                    'asb_risk_assessment.product_type_sid'
                )
                ->on('material_product_type.asb_samplevar_type_id', '=', \DB::raw(AsbSamplevarType::PRODUCT_TYPE));
            })
            // Material Assessment | Extent of Damage
            ->leftJoin('asb_samplevar_type_score AS material_extent_of_damage', function ($join) {
                $join->on(
                    'material_extent_of_damage.asb_samplevar_type_score_id',
                    '=',
                    'asb_risk_assessment.extent_damage_sid'
                )
                ->on('material_extent_of_damage.asb_samplevar_type_id', '=', \DB::raw(AsbSamplevarType::EXTENT_DAMAGE));
            })
            // Material Assessment | Surface Treatment
            ->leftJoin('asb_samplevar_type_score AS material_surface_treatment', function ($join) {
                $join->on(
                    'material_surface_treatment.asb_samplevar_type_score_id',
                    '=',
                    'asb_risk_assessment.surface_treatment_sid'
                )
                ->on(
                    'material_surface_treatment.asb_samplevar_type_id',
                    '=',
                    \DB::raw(AsbSamplevarType::SURFACE_TREATMENT)
                );
            })
            // Priority Assessment | Normal Occupant Activity
            ->leftJoin(
                'asb_assessfactor_type_score AS normal_activity_main',
                'normal_activity_main.asb_assessfactor_type_score_id',
                '=',
                'asb_risk_assessment.normal_activity_main_sid'
            )
            ->leftJoin(
                'asb_assessfactor_type_score AS normal_activity_secondary',
                'normal_activity_secondary.asb_assessfactor_type_score_id',
                '=',
                'asb_risk_assessment.normal_activity_secondary_sid'
            )
            // Priority Assessment | Likelihood of disturbance
            ->leftJoin(
                'asb_assessfactor_type_score AS likelihood_disturb_location',
                'likelihood_disturb_location.asb_assessfactor_type_score_id',
                '=',
                'asb_risk_assessment.likelihood_disturb_location_sid'
            )
            ->leftJoin(
                'asb_assessfactor_type_score AS likelihood_disturb_accessibility',
                'likelihood_disturb_accessibility.asb_assessfactor_type_score_id',
                '=',
                'asb_risk_assessment.likelihood_disturb_accessibility_sid'
            )
            ->leftJoin(
                'asb_assessfactor_type_score AS likelihood_disturb_extent',
                'likelihood_disturb_extent.asb_assessfactor_type_score_id',
                '=',
                'asb_risk_assessment.likelihood_disturb_extent_sid'
            )
            // Priority Assessment | Human Exposure Potential
            ->leftJoin(
                'asb_assessfactor_type_score AS human_exp_number',
                'human_exp_number.asb_assessfactor_type_score_id',
                '=',
                'asb_risk_assessment.human_exp_number_sid'
            )
            ->leftJoin(
                'asb_assessfactor_type_score AS human_exp_freq_use',
                'human_exp_freq_use.asb_assessfactor_type_score_id',
                '=',
                'asb_risk_assessment.human_exp_freq_use_sid'
            )
            ->leftJoin(
                'asb_assessfactor_type_score AS human_exp_average_time',
                'human_exp_average_time.asb_assessfactor_type_score_id',
                '=',
                'asb_risk_assessment.human_exp_average_time_sid'
            )
            // Priority Assessment | Maintenance Activity
            ->leftJoin(
                'asb_assessfactor_type_score AS maint_activity_type',
                'maint_activity_type.asb_assessfactor_type_score_id',
                '=',
                'asb_risk_assessment.maint_activity_type_sid'
            )
            ->leftJoin(
                'asb_assessfactor_type_score AS maint_activity_freq',
                'maint_activity_freq.asb_assessfactor_type_score_id',
                '=',
                'asb_risk_assessment.maint_activity_freq_sid'
            )
            // Current Action
            ->leftJoin('asb_action', function ($join) {
                $join->on(
                    'asb_action.asb_hazard_id',
                    '=',
                    'asb_hazard.asb_hazard_id'
                )
                ->on(
                    'asb_action.completed',
                    '=',
                    \DB::raw("'" . CommonConstant::DATABASE_VALUE_NO . "'")
                );
            })
            ->leftJoin('asb_action_type', 'asb_action_type.asb_action_type_id', '=', 'asb_action.asb_action_type_id')
            // Quantity
            ->leftJoin('unit_of_measure', 'unit_of_measure.unit_of_measure_id', '=', 'asb_hazard.unit_of_measure_id')
            // Sample
            ->leftJoin('asb_sample', 'asb_sample.asb_sample_id', '=', 'asb_hazard.asb_sample_id')
            // Sampler
            ->leftJoin(
                'contact as asb_sample_sampler_contact',
                'asb_sample_sampler_contact.contact_id',
                '=',
                'asb_sample.sampler_id'
            )
            // Analyser
            ->leftJoin(
                'contact as asb_sample_analyser_contact',
                'asb_sample_analyser_contact.contact_id',
                '=',
                'asb_sample.analyser_id'
            )
            // Material Type
            ->leftJoin(
                'asb_material_type',
                'asb_material_type.asb_material_type_id',
                '=',
                'asb_hazard.asb_material_type_id'
            )

            // Group by Hazard and scope by Site Group
            ->where('asb_hazard.site_group_id', \DB::raw(\Auth::user()->site_group_id))
            ->where('asb_hazard.asb_hazard_status_id', '<>', self::ASB_HAZARD_STATUS_ID_NAD)    // exclude NAD
            ->orderBy('asb_hazard.asb_hazard_code', 'ASC')
            ->groupBy('asb_hazard.asb_hazard_id')
        ;

        return $query;
    }

    public function actionsQuery($toArray = true)
    {
        $actions = AsbAction::join('asb_hazard', 'asb_hazard.asb_hazard_id', '=', 'asb_action.asb_hazard_id')
            ->leftJoin('asb_action_type', 'asb_action_type.asb_action_type_id', '=', 'asb_action.asb_action_type_id')
            ->where('asb_hazard.site_group_id', \Auth::user()->site_group_id)
            ->select([
                'asb_action.asb_hazard_id',
                'asb_action.asb_action_id',
                \DB::raw("DATE_FORMAT(asb_action.target_date, '%d/%m/%Y') AS target_date"),
                \DB::raw("DATE_FORMAT(asb_action.completed_date, '%d/%m/%Y') AS completed_date"),
                'asb_action.completed',
                'asb_action_type.asb_action_type_desc'
            ])
            ->get();

        if ($toArray) {
            $actions->toArray();

            $ret = [];
            foreach ($actions as $action) {
                $ret[$action->asb_hazard_id][] = $action;
            }

            $actions = $ret;
        }

        return $actions;
    }

    private function getACMImagesById($id, $mainPhoto)
    {
        $result = [];
        $siteGroupId = \Auth::user()->site_group_id;

        if ($mainPhoto) {
            $result[] = $this->getPhotoStoragePathway($siteGroupId, $id, 'asbhaz') . $mainPhoto;
        }
        $images = $this->acmService->findImages($id);

        foreach ($images as $image) {
            $reducedPhotoPath = $this->reducedPhotoPath(
                'asbhaz',
                $id,
                $image,
                $siteGroupId,
                true,
                true
            );
            $result[] = $reducedPhotoPath;
        }

        return $result;
    }

    protected function reducedPhotoPath(
        $photoFolder,
        $itemKey,
        $photoName,
        $siteGroupId,
        $useDefaultPhoto = true,
        $img = false
    ) {
        $usableSizeBytes = 5000;                   // Over 5K is still acceptable quality for the report
        if ($img) {
            $filePath = $this->getPhotoStoragePathway($siteGroupId, $itemKey, $photoFolder) . 'images/';
            $reducedFilePath = $this->getPhotoStoragePathway($siteGroupId, $itemKey, $photoFolder) . "img_small";
        } else {
            $filePath = $this->getPhotoStoragePathway($siteGroupId, $itemKey, $photoFolder);
            $reducedFilePath = $this->getPhotoStoragePathway($siteGroupId, $itemKey, $photoFolder) . "small";
        }

        if ($photoName) {
            $origFilePath = $filePath;
            $newFilePath = $reducedFilePath;
            if (is_dir($newFilePath)) {
                $photoPath = $newFilePath . "/" . $photoName;
                if (file_exists($photoPath)) {
                    $photoPath = str_replace("/", "\\", $photoPath);
                    $size = filesize($photoPath);
                    if ($size > $usableSizeBytes) {
                        // Small image still usable
                        return $photoPath;
                    } else {
                        //use original photo
                        return $origFilePath . $photoName;
                    }
                } else {
                    $success = true;
                    $imageToCompress = $origFilePath . DIRECTORY_SEPARATOR . $photoName;
                    $percent = 0.25;
                    switch (exif_imagetype($imageToCompress)) {
                        case IMAGETYPE_JPEG:
                            return $this->createJPGImage(
                                $success,
                                $filePath,
                                $newFilePath,
                                $photoName,
                                $imageToCompress,
                                $percent,
                                $usableSizeBytes
                            );
                            break;
                        case IMAGETYPE_PNG:
                            return $this->createPNGImage(
                                $success,
                                $filePath,
                                $newFilePath,
                                $photoName,
                                $imageToCompress,
                                $percent,
                                $usableSizeBytes
                            );
                            break;
                    }
                }
            } else {
                $success = mkdir($newFilePath);
                $imageToCompress = $origFilePath . DIRECTORY_SEPARATOR . $photoName;
                $percent = 0.25;

                switch (exif_imagetype($imageToCompress)) {
                    case IMAGETYPE_JPEG:
                        return $this->createJPGImage(
                            $success,
                            $filePath,
                            $newFilePath,
                            $photoName,
                            $imageToCompress,
                            $percent,
                            $usableSizeBytes
                        );
                        break;
                    case IMAGETYPE_PNG:
                        return $this->createPNGImage(
                            $success,
                            $filePath,
                            $newFilePath,
                            $photoName,
                            $imageToCompress,
                            $percent,
                            $usableSizeBytes
                        );
                        break;
                }
            }
        } elseif ($useDefaultPhoto === false) {
            return null;
        } else {
            return $this->fallBackImagePath($useDefaultPhoto);
        }
    }

    private function createPNGImage(
        $success,
        $filePath,
        $newFilePath,
        $photoName,
        $imageToCompress,
        $percent,
        $usableSizeBytes
    ) {
        $newImage = $newFilePath . "/" . $photoName;
        $newImage = str_replace("/", "\\", $newImage);


        if ($success) {
            $size = getimagesize($imageToCompress);
            if ($size) {
                list($width, $height) = $size;
                // Get new sizes
                $newwidth = $width * $percent;
                $newheight = $height * $percent;
            } else {
                $success = false;
            }
        }

        // Load
        if ($success) {
            $thumb = imagecreatetruecolor($newwidth, $newheight);
            if (!$thumb) {
                $success = false;
            }
        }
        if ($success) {
            $source = imagecreatefrompng($imageToCompress);
            if (!$source) {
                $success = false;
            }
        }
        if ($success) {
            // Resize
            $success = imagecopyresized(
                $thumb,
                $source,
                0,
                0,
                0,
                0,
                $newwidth,
                $newheight,
                $width,
                $height
            );
        }
        if ($success) {
            // Output
            $success = imagepng($thumb, $newImage, 7);
        }

        if (!$success) {
            //use original photo
            return $filePath . $photoName;
        } else {
            $size = filesize($newImage);
            if ($size > $usableSizeBytes) {
                // Small image still usable
                return $newImage;
            } else {
                //use original photo
                return $filePath . $photoName;
            }
        }
    }

    private function createJPGImage(
        $success,
        $filePath,
        $newFilePath,
        $photoName,
        $imageToCompress,
        $percent,
        $usableSizeBytes
    ) {
        $newImage = $newFilePath . "/" . $photoName;
        $newImage = str_replace("/", "\\", $newImage);


        if ($success) {
            $size = getimagesize($imageToCompress);
            if ($size) {
                list($width, $height) = $size;
                // Get new sizes
                $newwidth = $width * $percent;
                $newheight = $height * $percent;
            } else {
                $success = false;
            }
        }

        // Load
        if ($success) {
            $thumb = imagecreatetruecolor($newwidth, $newheight);
            if (!$thumb) {
                $success = false;
            }
        }
        if ($success) {
            $source = imagecreatefromjpeg($imageToCompress);
            if (!$source) {
                $success = false;
            }
        }
        if ($success) {
            // Resize
            $success = imagecopyresized(
                $thumb,
                $source,
                0,
                0,
                0,
                0,
                $newwidth,
                $newheight,
                $width,
                $height
            );
        }
        if ($success) {
            // Output
            $success = imagejpeg($thumb, $newImage, 75);
        }

        if (!$success) {
            //use original photo
            return $filePath . $photoName;
        } else {
            $size = filesize($newImage);
            if ($size > $usableSizeBytes) {
                // Small image still usable
                return $newImage;
            } else {
                //use original photo
                return $filePath . $photoName;
            }
        }
    }

    private function generateFilterData($filterData, &$whereCodes, &$whereTexts, &$orCodes)
    {
        $asbHazardStatusType = [];

        if (Common::iset($filterData['presumed'])) {
            $asbHazardStatusType[] = 'Presumed';
        }

        if (Common::iset($filterData['strpresumed'])) {
            $asbHazardStatusType[] = 'Strongly Presumed';
        }

        if (Common::iset($filterData['detected'])) {
            $asbHazardStatusType[] = 'Asbestos Detected';
        }

        if (Common::iset($filterData['removed'])) {
            $asbHazardStatusType[] = 'Asbestos Removed';
        }

        if (Common::iset($filterData['nad'])) {
            $asbHazardStatusType[] = 'Not Asbestos';
        }

        if (!empty($asbHazardStatusType)) {
            array_push($whereTexts, "Asbestos Hazard status type = '" . implode(', ', $asbHazardStatusType) . "'");
        }

        if ($val = Common::iset($filterData['asbestosType'])) {
            array_push(
                $whereCodes,
                ['asb_asbestos_type', 'asb_asbestos_type_id', 'asb_asbestos_type_code', $val, "Asbestos Type"]
            );
        }

        if ($val = Common::iset($filterData['materialType'])) {
            array_push(
                $whereCodes,
                ['asb_material_type', 'asb_material_type_id', 'asb_material_type_code', $val, "Material Type"]
            );
        }

        if ($val = Common::iset($filterData['materialSubtype'])) {
            array_push(
                $whereCodes,
                ['asb_material_subtype', 'asb_material_subtype_id', 'asb_material_subtype_code', $val,
                    "Material Subtype"]
            );
        }

        if ($siteId = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $siteId, "Site Code"]);

            if ($buildingId = Common::iset($filterData['building_id'])) {
                array_push(
                    $whereCodes,
                    ['building', 'building_id', 'building_code', $buildingId, "Building Code"]
                );
            }
        }

        $asbSampleRequirement = [];

        if (Common::iset($filterData['sampleYes'])) {
            $asbSampleRequirement[] = 'Yes';
        }

        if (Common::iset($filterData['sampleNo'])) {
            $asbSampleRequirement[] = 'No';
        }

        if (Common::iset($filterData['sampleTaken'])) {
            $asbSampleRequirement[] = 'Taken';
        }

        if (Common::iset($filterData['sampleRepresentative'])) {
            $asbSampleRequirement[] = 'Representative';
        }

        if (!empty($asbSampleRequirement)) {
            array_push($whereTexts, "Sample Required = '" . implode(', ', $asbSampleRequirement) . "'");
        }

        if ($val = Common::iset($filterData['search'])) {
            array_push($whereTexts, "Filter on Text contains '" . $val . "'");
        }
    }

    private function asbSurveys($inputs = [])
    {
        $buildingId = array_get($inputs, 'building_id');
        if (is_array($buildingId)) {
            $buildingId = array_get($buildingId, 0);
        }
        $query = VwAsb01::userSiteGroup()
        ->where('site_id', $this->site->site_id)
        ->where('survey_status_id', AsbSurveyStatus::CURRENT);      // get current surveys only

        if ($buildingId) {
            $query->where('building_id', $buildingId);
        }

        return $query;
    }

    private function formatInputs($inputs)
    {
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);

            $siteIds = $reportFilterQuery->getValueFilterField('site_id');
            $buildingIds = $reportFilterQuery->getValueFilterField('building_id');
            $roomIds = $reportFilterQuery->getValueFilterField('room_id');

            $inputs['site_id'] = empty($siteIds) ? null : $siteIds;
            $inputs['building_id'] = empty($buildingIds) ? null : $buildingIds;
            $inputs['room_id'] = empty($roomIds) ? null : $roomIds;
        }
        return $inputs;
    }

    private function getPrefixTables()
    {
        return [
            'asb_survey' => '*',
            'asb_hazard' => [
                'asb_hazard_code',
                'asb_hazard_desc',
                'asb_sample_requirement_id',
                'asb_material_type_id',
                'asb_material_subtype_id',
                'asb_asbestos_type_id'
            ],
            'site' => ['site_id', 'site_code', 'site_desc'],
            'building' => ['building_id', 'building_code', 'building_desc'],
            'room' => ['room_id', 'room_number', 'room_desc'],
            'asb_survey_type' => [
                'survey_type_code'
            ],
            'asb_hazard_status' => [
                'asb_hazard_status_code',
                'asb_hazard_status_desc',
                'asb_hazard_status_type_id'
            ],
        ];
    }
}
