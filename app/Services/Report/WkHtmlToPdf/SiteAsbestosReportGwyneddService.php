<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Exceptions\PermissionException;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Models\AsbHazard;
use Tfcloud\Models\AsbRiskStatus;
use Tfcloud\Models\AsbSurveyStatus;
use Tfcloud\Models\CrudAccessLevel;
use Tfcloud\Models\Module;
use Tfcloud\Models\Report;
use Tfcloud\Services\PermissionService;

class SiteAsbestosReportGwyneddService extends WkHtmlToPdfReportBaseService
{
    private $hazards = [];
    private $totalHazards = 0;
    private $buildingCount = 0 ;
    private $siteText = "";
    public $filterText = '';
    private $reportBoxFilterLoader = null;

    public function __construct(PermissionService $permissionService, Report $report = null)
    {
        $this->permissionService = $permissionService;
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function runReportSingleSite($site, $reportId, $inputs = [])
    {
        //call report from button not report view
        $inputs['site_id'] = $site->site_id;
        $inputs['singleOnly'] = true;

        return $this->runReport(
            $this->getTempFile(),
            $inputs,
            $reportId
        );
    }

    public function runReport($repOutPutPdfFile, $inputs, $repId)
    {
        $footerDate = new \DateTime();
        $arrayContentHtml = [];

        $this->initialData($inputs);

        $data = [];
        $data['site_text'] = $this->siteText;
        $data['total_hazards'] = $this->totalHazards;

        if (!empty($this->hazards)) {
            // Create wkhtmltopdf temp folder to store generated files
            $generatedPath = $this->generateWkhtmltopdfTempPath($repId);

            // Header.
            $header = \View::make(
                'reports.wkhtmltopdf.asbestos.gwynedd.header',
                [
                    'reportLogo' => $this->getHeaderLogo(),
                    'title'      => "Gwynedd Asbestos Register: " . $this->siteText
                ]
            )->render();

            $headerPath = $generatedPath . "/header.html";
            file_put_contents($headerPath, $header, LOCK_EX);

            // Footer.
            $footer = \View::make(
                'reports.wkhtmltopdf.asbestos.gwynedd.footer',
                [
                    'footerDate' => $footerDate->format('Y/m/d H:i'),
                    'siteCode'   => $this->siteText,
                    'user'       => \Auth::user()->display_name
                ]
            )->render();
            $footerPath = $generatedPath . "/footer.html";
            file_put_contents($footerPath, $footer, LOCK_EX);


            // Static text.
            $staticHtml = \View::make(
                'reports.wkhtmltopdf.asbestos.gwynedd.static_text',
                []
            )->render();

            $staticHTMLPath = $generatedPath . "/static.html";
            file_put_contents($staticHTMLPath, $staticHtml, LOCK_EX);
            array_push($arrayContentHtml, $staticHTMLPath);


            $content = \View::make(
                'reports.wkhtmltopdf.asbestos.gwynedd.asbHazardList',
                [
                    'data'         => $data,
                    'hazards'      => $this->hazards,
                    'bldgCount'    => $this->buildingCount
                ]
            )->render();

            $contentPath = $generatedPath . "/asbHazardList.html";
            file_put_contents($contentPath, $content, LOCK_EX);
            array_push($arrayContentHtml, $contentPath);

            $options = [
                'header-html' => $headerPath,
                'footer-html' => $footerPath,
                'header-spacing' => 4,
                'footer-spacing' => 4,
                'orientation' => 'landscape',
            ];

            if (array_key_exists('singleOnly', $inputs)) {
                $repOutPutPdfFile .= '.pdf';
                $this->generateSingleHtmlToPdf($arrayContentHtml, $repOutPutPdfFile, $options);
                return $repOutPutPdfFile;
            } else {
                $this->generateSingleHtmlToPdf($arrayContentHtml, $repOutPutPdfFile, $options);
                return true;
            }
        }
        return false;
    }

    public function validateFilter($inputs)
    {
        return \Validator::make($this->formatInputs($inputs), $this->validateRules(), $this->validateMessages());
    }

    public function validateRules()
    {
        \Validator::extend('site_required_one', function ($attribute, $value, $parameters) {
            if (is_array($value)) {
                return count($value) == 1;
            }
            return !empty($value);
        });

        \Validator::extend('single_building', function ($attribute, $value, $parameters) {
            if ($value && is_array($value)) {
                return count($value) == 1;
            }
            return true;
        });

        \Validator::extend('single_room', function ($attribute, $value, $parameters) {
            if ($value && is_array($value)) {
                return count($value) == 1;
            }
            return true;
        });

        $rules = [
            'site_id' => ['site_required_one'],
            'building_id' => ['required_with:room_id', 'single_building'],
            'room_id' => ['single_room'],
        ];

        return $rules;
    }

    public function validateMessages()
    {
        return [
            'site_id.site_required_one' => 'Only 1 location can be selected for this report.',
            'building_id.single_building' => 'Only 1 location can be selected for this report.',
            'room_id.single_room' => 'Only 1 location can be selected for this report.',
        ];
    }

    private function getTempFile()
    {
        return tempnam(Common::getTempFolder(), 'ASBHRT01SingleFullAsbSurvey');
    }

    private function initialData($inputs)
    {
        if (
            !$this->permissionService->hasModuleAccess(
                Module::MODULE_ASBESTOS,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
            )
        ) {
            $msg = "Not met minimum readonly access for asbestos module in class" .
                " (" . __CLASS__ . ") method (" . __METHOD__ . "). " .
                " See line (" . __LINE__ . ")";
            throw new PermissionException($msg);
        }

        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $reportFilterQuery->getOperatorParser()->setPrefixTables($this->getPrefixTables());
            $result = $reportFilterQuery->filterAll($this->all())->get();
        } else {
            $result = $this->filterAll($this->all(), $inputs)->get();
        }

        $currentBuildingId = false;
        $bldgHazCount = 0;
        $this->buildingCount = 0;

        foreach ($result as $row) {
            $this->siteText     = $row->site_text;

            if ($row->asb_risk_assessment_id) {
                $row->total_material_score_text =
                    is_numeric($row->total_material_score) ? $row->total_material_score : 'N/A - incomplete';
                $row->total_priority_score_text =
                    is_numeric($row->total_priority_score) ? $row->total_priority_score : 'N/A - incomplete';
                if (!is_numeric($row->total_overall_score)) {
                    $row->overall_risk_code = 'N/A';
                }
            } else {
                $row->total_material_score_text = '';
                $row->total_priority_score_text = '';
                $row->overall_risk_code = '';
            }

            if ($currentBuildingId !== $row->building_id) {
                ++$this->buildingCount;
                if ($bldgHazCount > 0) {
                    $this->hazards[$currentBuildingId]['bldg_haz_count'] = $bldgHazCount;
                }
                $bldgHazCount = 0;

                $buildingHazards = [];
                $buildingHazards['site_text'] = $row->site_text;
                $buildingHazards['building_text'] = $row->building_text;
                $buildingHazards['haz_recs'] = [];
                $currentBuildingId = $row->building_id;
                $this->hazards[$currentBuildingId] = $buildingHazards;
            }
            array_push($this->hazards[$currentBuildingId]['haz_recs'], $row->toArray());
            ++$bldgHazCount;
            ++$this->totalHazards;
        }

        $this->hazards[$currentBuildingId]['bldg_haz_count'] = $bldgHazCount;
    }

    private function all()
    {
        $selects = [
            "asb_survey.asb_survey_id",
            "asb_hazard.asb_hazard_id",
            "asb_hazard.building_id",
            "asb_hazard.room_id",
            "room.room_floor_id",
            "asb_hazard.asb_hazard_status_id",
            "asb_hazard.asb_hazard_code",
            "asb_hazard.asb_hazard_desc",
            "asb_hazard.asb_hazard_comment",
            "asb_hazard.asb_hazard_position",
            "asb_hazard.asb_hazard_quantity",
            "asb_hazard.unit_of_measure_id",
            "asb_hazard.asb_asbestos_type_id",
            "asb_hazard.asb_sample_requirement_id",
            \DB::raw('DATE_FORMAT(asb_hazard.asb_hazard_identified_date, "%d/%m/%Y") AS asb_hazard_identified_date'),
            \DB::raw('DATE_FORMAT(asb_hazard.asb_hazard_removal_date, "%d/%m/%Y") AS asb_hazard_removal_date'),
            "asb_sample.asb_sample_code",
            "asb_risk_assessment.asb_risk_assessment_id",
            "asb_risk_assessment.total_material_score",
            "asb_risk_assessment.total_priority_score",
            "asb_risk_assessment.total_overall_score",
            "asb_risk_assessment.extent_damage_sid",
            \DB::raw(
                "CASE asb_risk_assessment.extent_damage_sid
		WHEN 4
                THEN 'Good'
                WHEN 5
                THEN 'Low Damage'
                WHEN 6
                THEN 'Medium Damage'
                WHEN 7
                THEN 'High Damage'
		END AS sample_condition"
            ),
            "asb_survey.site_id",
            "asb_survey.asb_survey_id",
            "asb_survey.asb_survey_code",
            "asb_survey.survey_status_id",
            \DB::raw('DATE_FORMAT(asb_survey.asb_survey_date, "%d/%m/%Y") AS asb_survey_date'),
            "site.site_code",
            "site.site_desc",
            \DB::raw("CONCAT(site.site_code, ' - ', site.site_desc) AS site_text"),
            "building.building_code",
            "building.building_desc",
            \DB::raw("CONCAT(building.building_code, ' - ', building.building_desc) AS building_text"),
            "room.room_number",
            "room.room_desc",
            \DB::raw("CONCAT(room.room_number,  ' - ', room.room_desc) AS room_text"),
            \DB::raw("CONCAT(plant.plant_code,  ' - ', plant.plant_desc) AS plant_text"),
            "room_floor.room_floor_code",
            "asb_hazard_status.asb_hazard_status_code",
            "unit_of_measure.unit_of_measure_code",
            \DB::raw("CONCAT(asb_hazard.asb_hazard_quantity, ' ', unit_of_measure.unit_of_measure_code) AS unit_text"),
            "asb_asbestos_type.asb_asbestos_type_code",
            "asb_overall_risk.overall_risk_code",
            "product_type.display AS product_type",
            "asb_condition.display AS asb_condition",
            "surface_treatment.display AS surface_treatment",
            "likelihood_disturb_accessibility.display AS accessibility",
            "likelihood_disturb_extent.display as extent"
        ];

        $query = AsbHazard
            ::join('asb_survey_hazard', 'asb_survey_hazard.asb_hazard_id', '=', 'asb_hazard.asb_hazard_id')
            ->join('asb_survey', 'asb_survey.asb_survey_id', '=', 'asb_survey_hazard.asb_survey_id')
            ->join(
                'asb_hazard_status',
                'asb_hazard_status.asb_hazard_status_id',
                '=',
                'asb_hazard.asb_hazard_status_id'
            )
            ->join('site', 'site.site_id', '=', 'asb_hazard.site_id')
            ->leftjoin('building', 'building.building_id', '=', 'asb_hazard.building_id')
            ->leftJoin('room', 'room.room_id', '=', 'asb_hazard.room_id')
            ->leftJoin('room_floor', 'room_floor.room_floor_id', '=', 'room.room_floor_id')
            ->leftJoin('plant', 'plant.plant_id', '=', 'asb_hazard.plant_id')
            ->leftJoin('plant_type', 'plant_type.plant_type_id', '=', 'plant.plant_type_id')
            ->leftJoin('plant_subgroup', 'plant_subgroup.plant_subgroup_id', '=', 'plant.plant_subgroup_id')
            ->leftJoin('plant_group', 'plant_group.plant_group_id', '=', 'plant.plant_group_id')
            ->leftJoin('contact', 'contact.contact_id', '=', 'asb_hazard.identifier_id')
            ->leftJoin(
                'asb_material_type',
                'asb_material_type.asb_material_type_id',
                '=',
                'asb_hazard.asb_material_type_id'
            )
            ->leftJoin(
                'asb_material_subtype',
                'asb_material_subtype.asb_material_subtype_id',
                '=',
                'asb_hazard.asb_material_subtype_id'
            )
            ->leftJoin('unit_of_measure', 'unit_of_measure.unit_of_measure_id', '=', 'asb_hazard.unit_of_measure_id')
            ->leftJoin(
                'asb_asbestos_type',
                'asb_asbestos_type.asb_asbestos_type_id',
                '=',
                'asb_hazard.asb_asbestos_type_id'
            )
            ->leftJoin(
                'asb_sample_requirement',
                'asb_sample_requirement.asb_sample_requirement_id',
                '=',
                'asb_hazard.asb_sample_requirement_id'
            )
            ->leftJoin('asb_sample', 'asb_sample.asb_sample_id', '=', 'asb_hazard.asb_sample_id')
            ->leftJoin('asb_risk_assessment', function ($join) {
                $join->on('asb_hazard.asb_hazard_id', '=', 'asb_risk_assessment.asb_hazard_id')
                    ->where('asb_risk_assessment.asb_risk_status_id', '=', AsbRiskStatus::CURRENT);
            })
            ->leftJoin('asb_overall_risk', function ($join) {
                $join->on('asb_overall_risk.from_score', '<=', 'asb_risk_assessment.total_overall_score')
                    ->on('asb_overall_risk.to_less_than_score', '>', 'asb_risk_assessment.total_overall_score');
            })
            ->leftJoin(
                'asb_samplevar_type_score AS product_type',
                'product_type.asb_samplevar_type_score_id',
                '=',
                'asb_risk_assessment.product_type_sid'
            )
            ->leftJoin(
                'asb_samplevar_type_score AS asb_condition',
                'asb_condition.asb_samplevar_type_score_id',
                '=',
                'asb_risk_assessment.extent_damage_sid'
            )
            ->leftJoin(
                'asb_samplevar_type_score AS surface_treatment',
                'surface_treatment.asb_samplevar_type_score_id',
                '=',
                'asb_risk_assessment.surface_treatment_sid'
            )
            ->leftJoin(
                'asb_assessfactor_type_score AS likelihood_disturb_accessibility',
                'likelihood_disturb_accessibility.asb_assessfactor_type_score_id',
                '=',
                'asb_risk_assessment.likelihood_disturb_accessibility_sid'
            )
            ->leftJoin(
                'asb_assessfactor_type_score AS likelihood_disturb_extent',
                'likelihood_disturb_extent.asb_assessfactor_type_score_id',
                '=',
                'asb_risk_assessment.likelihood_disturb_extent_sid'
            );
        $query->where('asb_survey.survey_status_id', AsbSurveyStatus::CURRENT);
        /** CLD 8918 - Changed to remove 'Data Input In Error' from the list view - JC*/
        $query->where('asb_hazard_status.asb_hazard_status_code', '!=', 'DE');

        $query->orderBy('building.building_code')
            ->orderBy('room_floor.room_floor_code')
            ->orderBy('room.room_number')
            ->orderBy('asb_hazard.asb_hazard_code')
            ->groupBy('asb_hazard.asb_hazard_id')
            ->select($selects);

        return $query;
    }

    private function filterAll($query, $inputs = [])
    {
        $filter = new \Tfcloud\Lib\Filters\AsbSurveyFilter($inputs);
        $siteId = $buildingId = $roomId = false;
        if (!is_null($filter->site_id) && $filter->site_id) {
            $siteId = $filter->site_id;

            if (!is_null($filter->building_id) && $filter->building_id) {
                $buildingId = $filter->building_id;

                if (!is_null($filter->room_id) && $filter->room_id) {
                    $roomId = $filter->room_id;
                }
            }
        }

        $query->where('site.site_id', $siteId);
        if ($buildingId) {
            $query->where('building.building_id', $buildingId);
        }
        if ($roomId) {
            $query->where('room.room_id', $roomId);
        }

        return $query;
    }

    private function formatInputs($inputs)
    {
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);

            $siteIds = $reportFilterQuery->getValueFilterField('site_id');
            $buildingIds = $reportFilterQuery->getValueFilterField('building_id');
            $roomIds = $reportFilterQuery->getValueFilterField('room_id');

            $inputs['site_id'] = empty($siteIds) ? null : $siteIds;
            $inputs['building_id'] = empty($buildingIds) ? null : $buildingIds;
            $inputs['room_id'] = empty($roomIds) ? null : $roomIds;
        }
        return $inputs;
    }

    private function getPrefixTables()
    {
        return [
            'asb_survey' => '*',
            'site' => ['site_id', 'site_code', 'site_desc'],
            'building' => ['building_id', 'building_code', 'building_desc'],
            'room' => ['room_id', 'room_number', 'room_desc'],
            'asb_hazard_status' => ['asb_hazard_status_type_id'],
            'asb_hazard' => ['asb_hazard_position']
        ];
    }
}
