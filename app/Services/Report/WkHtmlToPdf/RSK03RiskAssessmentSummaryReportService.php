<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use Carbon\Carbon;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Traits\EmailTrait;
use Tfcloud\Models\Report;
use Tfcloud\Models\RiskSurvey;
use Tfcloud\Models\RiskSurveyStatus;
use Tfcloud\Services\Assessment\Risk\RiskSurveyItemService;
use Tfcloud\Services\Assessment\Risk\RiskSurveyService;
use Tfcloud\Services\PermissionService;

class RSK03RiskAssessmentSummaryReportService extends WkHtmlToPdfReportBaseService
{
    use EmailTrait;

    protected $riskSurveyService;
    protected $riskSurveyItemService;
    protected $hasBoxFilter;
    protected $reportBoxFilterLoader;
    protected $reportFilterQuery;
    public $filterText = '';

    public function __construct(PermissionService $permissionService, Report $report = null)
    {
        parent::__construct($permissionService);
        $this->permissionService = $permissionService;
        $this->riskSurveyService = new RiskSurveyService($permissionService);
        $this->riskSurveyItemService = new RiskSurveyItemService($permissionService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
        $this->hasBoxFilter = $this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter();
    }

    public function runReport($repPdfFile, $inputs, $repId, &$errMsg = [], $options = [])
    {
        if ($this->hasBoxFilter) {
            $this->reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $query = $this->riskSurveyService->all()->select(['risk_survey.*']);
            $riskSurveys = $this->reportFilterQuery->filterAll($query)->get();
        } else {
            $this->formatInputData($inputs);
            $riskSurveys = $this->riskSurveyService->getAll($inputs)
            ->select(['risk_survey.*'])->get();
        }

        $arrPdf = [];
        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);
        if (array_get($options, 'print-report')) {
            $repPdfFile = $this->generateReportOutPutPath(array_get($options, 'risk_survey_code'));
        }

        if ($riskSurveys->count()) {
            foreach ($riskSurveys as $riskSurvey) {
                $reportPath = $generatedPath . '/' . $riskSurvey->risk_survey_id . '.pdf';

                $riskItems = $this->riskSurveyItemService->getAll(['risk_survey_id' => $riskSurvey->risk_survey_id])
                    ->get();
                $content = \View::make(
                    'reports.wkhtmltopdf.riskAssessment.rsk03.content',
                    [
                        'riskSurvey' => $riskSurvey,
                        'riskSurveyItems' => $riskItems,
                    ]
                )->render();
                $contentPath = $generatedPath . "/content_{$riskSurvey->risk_survey_id}.html";
                file_put_contents($contentPath, $content, LOCK_EX);

                $header = \View::make(
                    'reports.wkhtmltopdf.riskAssessment.rsk03.header',
                    [
                        'riskSurvey' => $riskSurvey,
                        'logo' => $this->getHeaderLogo()
                    ]
                )->render();
                $headerPath = $generatedPath . "/header_{$riskSurvey->risk_survey_id}.html";
                file_put_contents($headerPath, $header, LOCK_EX);
                $footer = \View::make('reports.wkhtmltopdf.project.prj17.footer')->render();
                $footerPath = $generatedPath . "/footer_{$riskSurvey->risk_survey_id}.html";
                file_put_contents($footerPath, $footer, LOCK_EX);
                $outputOptions = [
                    'header-html' => $headerPath,
                    'orientation' => 'landscape',
                    'margin-top'  => 30,
                    'margin-bottom' => 10,
                    'margin-left' => 3,
                    'margin-right' => 3,
                ];

                $this->generateSingleHtmlToPdf($contentPath, $reportPath, $outputOptions);

                array_push($arrPdf, $reportPath);
            }
        }

        $cssPaging = [
            'fontSize' => 8,
            'x' => 660,
            'y' => 8
        ];
        $this->mergerPdf($arrPdf, $repPdfFile, ['cssPaging' => $cssPaging]);

        if (array_get($inputs, 'send_email', false)) {
            $user = \Auth::user();
            $riskSurveyId = array_get($inputs, 'risk_survey_id', false);
            $logItem = RiskSurvey::find($riskSurveyId);
            $fileName = array_get($options, 'risk_survey_code');
            $emailData = array(
                'mailTo'        => $user->email_reply_to,
                'displayName'   => $user->siteGroup->email_return_address,
                'repTitle'      => $fileName,
                'subject'       => $fileName . ' Report',
            );
            return $this->sendEmail($repPdfFile, $emailData, $logItem);
        }

        $whereCodes = [];               // The codes which have been used in the filter
        $whereTexts = [];               // Plain text about filtering applied
        $orCodes = [];                  // csv string of OR codes
        $bFilterDetail = false;


        if ($this->hasBoxFilter) {
            $this->filterText = $this->reportFilterQuery->getFilterDetailText();
        } else {
            $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
            $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $this->filterText);
        }

        if (array_get($options, 'print-report')) {
            return $repPdfFile;
        }
        return true;
    }

    protected function getHeaderLogo()
    {
        $siteGroupReportLogo = "reportLogo.jpg";
        $siteGroupReportLogoPath = \Auth::user()->site_group_id . DIRECTORY_SEPARATOR;
        $siteGroupReportLogoPath .= "sitegroup" . DIRECTORY_SEPARATOR . $siteGroupReportLogo;
        $legacyPath = \Config::get('cloud.legacy_paths.uploads');
        $reportLogo = $legacyPath . DIRECTORY_SEPARATOR . $siteGroupReportLogoPath;
        $headerLogo = public_path() . '/assets/images/tfcloud-banner.jpg';
        if (file_exists($reportLogo)) {
            // Create a temporary file and copy the file across onto this machine
            // This is the only way the pdf tool can cope with it.
            $tmpBannerFile = tempnam(Common::getTempFolder(), 'rlg');
            copy($reportLogo, $tmpBannerFile);
            $headerLogo = $tmpBannerFile;
        }
        return $headerLogo;
    }

    protected function setPagingCustom(&$page, $key, $totalPage, $x, $y, $customY2 = null)
    {
        $user = \Auth::user()->display_name;
        $now = Carbon::now()->format('d/m/Y H:i:s');
        $left = 20;
        $center = 400;
        $page->drawText("RSK03: Risk Assessment Summary", $left, $y, 'UTF-8');
        $page->drawText("Page " . ($key + 1) . " of $totalPage", $center, $y, 'UTF-8');
        $page->drawText("Created by $user at $now", $x, $y, 'UTF-8');
    }

    private function formatInputData(&$inputs)
    {
        foreach ($inputs as $key => $value) {
            if ($value === "") {
                unset($inputs[$key]);
            }
        }
    }

    private function generateFilterData($filterData, &$whereCodes, &$whereTexts, &$orCodes)
    {
        if ($val = array_get($filterData, 'code')) {
            array_push($whereTexts, "Code contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'description')) {
            array_push($whereTexts, "Description contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'owner')) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Owner']);
        }

        if ($val = array_get($filterData, 'surveyType')) {
            array_push(
                $whereCodes,
                ['risk_survey_type', 'risk_survey_type_id', 'risk_survey_type_code', $val, 'Survey Type']
            );
        }

        if ($val = array_get($filterData, 'jobRole')) {
            array_push(
                $whereCodes,
                ['job_role', 'job_role_id', 'job_role_code', $val, 'Job Role']
            );
        }

        if ($val = array_get($filterData, 'site_id')) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);

            if ($val = array_get($filterData, 'building_id')) {
                array_push($whereCodes, ['building', 'building_id', 'building_code', $val, "Building Code"]);

                if ($val = array_get($filterData, 'room_id')) {
                    array_push($whereCodes, ['room', 'room_id', 'room_number', $val, "Room Number"]);
                }
            }
        }

        $riskSurveyStatus = [];
        if (Common::iset($filterData['draft'])) {
            $riskSurveyStatus[] = RiskSurveyStatus::DRAFT;
        }
        if (Common::iset($filterData['current'])) {
            $riskSurveyStatus[] = RiskSurveyStatus::CURRENT;
        }
        if (Common::iset($filterData['archive'])) {
            $riskSurveyStatus[] = RiskSurveyStatus::ARCHIVE;
        }
        if (count($riskSurveyStatus)) {
            array_push($orCodes, [
                'risk_survey_status',
                'risk_survey_status_id',
                'risk_survey_status_code',
                implode(',', $riskSurveyStatus),
                "Status"
            ]);
        }
    }
}
