<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use Carbon\Carbon;
use Tfcloud\Lib\Exceptions\InvalidRecordException;
use Tfcloud\Lib\Exceptions\PermissionException;
use Tfcloud\Models\Contract;
use Tfcloud\Models\ContractInstruction;
use Tfcloud\Models\ContractInstructionType;
use Tfcloud\Models\CrudAccessLevel;
use Tfcloud\Models\GenTable;
use Tfcloud\Models\GenUserdefLabel;
use Tfcloud\Models\InstructionStatusType;
use Tfcloud\Models\Module;
use Tfcloud\Services\Contract\ContractService;
use Tfcloud\Services\Contract\InstructionService;
use Tfcloud\Services\PermissionService;

class CONT17ContractInvoiceSummaryReportService extends WkHtmlToPdfReportBaseService
{
    private $recordId;
    private $detailPath;
    private $financialSummaryTablePath;
    private $reportHeaderTitle = '';
    private $reportFooterTitle = '';
    private $reportData;
    private $contractInstructionType = null;
    private $genTable;

    public const CONTRACT_MODE = 1;
    public const CONTRACT_INSTRUCTION_MODE = 2;


    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
    }

    public function printReport($repId, $options = [])
    {
        $repOutPutPdfFile = $this->generateReportOutPutPath(array_get($options, 'code'));
        return $this->runReport($repOutPutPdfFile, $repId, $options);
    }

    public function runReport($repOutPutPdfFile, $repId, $options = [])
    {
        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);

        $this->initReportData($options);

        $header = \View::make(
            'reports.wkhtmltopdf.contract.cont17.header',
            [
                'title'  => $this->reportHeaderTitle,
                'logo' => $this->getHeaderLogo()
            ]
        )->render();
        $headerPath = $generatedPath . "/header_{$this->recordId}.html";
        file_put_contents($headerPath, $header, LOCK_EX);

        $footer = \View::make(
            'reports.wkhtmltopdf.contract.cont17.footer',
            [
                'title'  => $this->reportFooterTitle,
                'user' => \Auth::user()->display_name,
                'currentTime' => Carbon::now()->format('d/m/Y H:i:s')
            ]
        )->render();
        $footerPath = $generatedPath . "/footer.html";
        file_put_contents($footerPath, $footer, LOCK_EX);

        $content = \View::make(
            'reports.wkhtmltopdf.contract.cont17.content',
            array_merge($this->reportData, [
                'detailPath' => $this->detailPath,
                'financialSummaryTablePath' => $this->financialSummaryTablePath,
                'label' => GenUserdefLabel::userSiteGroup()->where('gen_table_id', $this->genTable)->first()
            ])
        )->render();
        $contentPath = $generatedPath . "/content_{$this->recordId}.html";
        file_put_contents($contentPath, $content, LOCK_EX);

        $outputOptions = [
            'header-html' => $headerPath,
            'footer-html' => $footerPath,
            'orientation' => 'portrait',
            'header-spacing' => 4,
            'footer-spacing' => 4,
        ];

        $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $outputOptions);

        if (array_get($options, 'print-report')) {
            return $repOutPutPdfFile;
        }
        return true;
    }

    private function initReportData($option)
    {
        $reportMode = array_get($option, 'report_mode');
        $this->recordId = array_get($option, 'record_id');

        if ($reportMode == self::CONTRACT_MODE) {
            $this->reportData = $this->getContractReportData($this->recordId);
            $this->genTable = GenTable::CONTRACT;
            $this->reportHeaderTitle = 'Contract Details';
            $this->reportFooterTitle = 'Contract Summary';
            $this->detailPath = 'reports.wkhtmltopdf.contract.cont17.contract._detail-contract';
            $this->financialSummaryTablePath =
                'reports.wkhtmltopdf.contract.cont17.contract._contract-financial-summary-table';
        } elseif ($reportMode == self::CONTRACT_INSTRUCTION_MODE) {
            $this->reportData = $this->getContractInstructionReportData($this->recordId);
            $this->genTable = GenTable::CONTRACT_INSTR;
            $this->detailPath =
                'reports.wkhtmltopdf.contract.cont17.contractInstruction._detail-contract-instruction';
            if ($this->contractInstructionType === ContractInstructionType::PLANNED) {
                $this->reportHeaderTitle = 'Planned Contract Instruction';
                $this->financialSummaryTablePath =
                    'reports.wkhtmltopdf.contract.cont17.contractInstruction._planned-financial-summary';
            } elseif ($this->contractInstructionType === ContractInstructionType::REACTIVE) {
                $this->reportHeaderTitle = 'Reactive Contract Instruction';
                $this->financialSummaryTablePath =
                    'reports.wkhtmltopdf.contract.cont17.contractInstruction._reactive-financial-summary-table';
            }
            $this->reportFooterTitle = 'Contract Instruction Summary';
        }
    }

    private function getContractReportData($contractId): array
    {
        $contract = $this->getContract($contractId);

        $contractService = new ContractService($this->permissionService);
        $summaryData = $contractService->getContractSummaryData($contractId);

        return array_merge(
            [
                'contract' => $contract,
            ],
            $summaryData
        );
    }

    private function getContractInstructionReportData($contractInstructionId): array
    {
        $contractInstruction = $this->getContractInstruction($contractInstructionId);
        $this->contractInstructionType = $contractInstruction->contract_instruction_type_id;

        $instructionService = new InstructionService($this->permissionService);
        $summaryData = $instructionService->getFinSummary($contractInstruction);

        return [
            'contractInstruction' => $contractInstruction,
            'finSummary' => $summaryData
        ];
    }

    private function getContract($contractId)
    {
        $contract = Contract::userSiteGroup()
            ->leftJoin('contact', 'contact.contact_id', '=', 'contract.supplier_contact_id')
            ->leftJoin('contract_type', 'contract_type.contract_type_id', '=', 'contract.contract_type_id')
            ->leftJoin('user', 'user.id', '=', 'contract.owner_user_id')
            ->select([
                'contract.contract_id',
                'contract.contract_type_id',
                'contract.gen_userdef_group_id',
                'contract.contract_code',
                'contract.contract_name',
                'contract.start_date',
                'contract.end_date',
                'contract.contract_value',
                'contract.contract_desc',
                'contract.archive',
                'contract.supplier_contact_id',
                'contact.contact_name',
                'contact.organisation',
                'contract_type.contract_type_name',
                'user.display_name AS owner_user_name',
            ])
            ->with(
                [
                    'genUserdefGroup',
                    'genUserdefGroup.contact1',
                    'genUserdefGroup.contact2',
                    'genUserdefGroup.contact3',
                    'genUserdefGroup.contact4',
                    'genUserdefGroup.contact5',
                    'genUserdefGroup.select1',
                    'genUserdefGroup.select2',
                    'genUserdefGroup.select3',
                    'genUserdefGroup.select4',
                ]
            )
            ->where('contract_id', $contractId)
            ->first();

        if (is_null($contract)) {
            throw new InvalidRecordException("No contract found");
        }

        /** Prevents a contractor from seeing other contracts that have not been assigned to them */
        if (
            \Auth::user()->isContractor() &&
            \Auth::user()->contractor->getKey() !== $contract->supplier_contact_id
        ) {
            throw new PermissionException("Contractor access restricted by supplier.");
        }

        return $contract;
    }

    private function getContractInstruction($contractInstructionId)
    {
        $instruction = ContractInstruction::userSiteGroup()
            ->leftJoin('contract', 'contract.contract_id', '=', 'contract_instruction.contract_id')
            ->leftJoin('contact', 'contact.contact_id', '=', 'contract.supplier_contact_id')
            ->leftJoin(
                'contract_instruction_status',
                'contract_instruction_status.contract_instruction_status_id',
                '=',
                'contract_instruction.contract_instruction_status_id'
            )
            ->leftJoin('trade_code', 'trade_code.trade_code_id', '=', 'contract_instruction.trade_code_id')
            ->leftJoin('fin_year', 'fin_year.fin_year_id', '=', 'contract_instruction.fin_year_id')
            ->leftJoin(
                'fin_year_period',
                'fin_year_period.fin_year_period_id',
                '=',
                'contract_instruction.fin_year_period_id'
            )
            ->select([
                'contract.contract_code',
                'contract.contract_name',
                'contact.contact_name',
                'contact.organisation',
                'contract_instruction.contract_instruction_id',
                'contract_instruction.contract_instruction_status_id',
                'contract_instruction.contract_id',
                'contract_instruction.contract_instruction_type_id',
                'contract_instruction.gen_userdef_group_id',
                'contract_instruction.contract_instruction_code',
                'contract_instruction.reference',
                'contract_instruction.contract_instruction_desc',
                'contract_instruction.completed_date',
                'contract_instruction.completed_time',
                'contract_instruction_status.contract_instruction_status_code',
                'contract_instruction_status.contract_instruction_status_desc',
                'trade_code.trade_code_code',
                'trade_code.trade_code_desc',
                'fin_year.fin_year_code',
                'fin_year.fin_year_desc',
                'fin_year_period.fin_year_period_code',
                'fin_year_period.fin_year_period_desc',
                'contract_instruction_type_id'
            ])
            ->with(
                [
                    'contract',
                    'genUserdefGroup',
                    'genUserdefGroup.contact1',
                    'genUserdefGroup.contact2',
                    'genUserdefGroup.contact3',
                    'genUserdefGroup.contact4',
                    'genUserdefGroup.contact5',
                    'genUserdefGroup.select1',
                    'genUserdefGroup.select2',
                    'genUserdefGroup.select3',
                    'genUserdefGroup.select4',
                ]
            )
            ->where('contract_instruction_id', $contractInstructionId)->first();

        if (is_null($instruction)) {
            throw new InvalidRecordException("No contract instruction found");
        }

        if (\Auth::user()->isContractor()) {
            $contract = $instruction->contract;

            if (is_null($contract) || $contract->supplier_contact_id !== \Auth::user()->contractor_contact_id) {
                throw new PermissionException("Contractor access restricted by supplier.");
            }

            /**
             * Contractors can only view Printed, Complete, & Closed instructions.
             */
            $instructionStatusTypeId = $instruction->contractInstructionStatus->instruction_status_type_id;
            if (
                $instructionStatusTypeId != InstructionStatusType::PRINTED &&
                $instructionStatusTypeId != InstructionStatusType::COMPLETE &&
                $instructionStatusTypeId != InstructionStatusType::CLOSED
            ) {
                throw new InvalidRecordException(
                    "Instruction status must be Printed, Complete, or Closed for access by contractor."
                );
            }
        }

        return $instruction;
    }
}
