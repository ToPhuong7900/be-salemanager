<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\ReportConstant;
use Tfcloud\Models\CondLocationType;
use Tfcloud\Models\Condsurvey;
use Tfcloud\Models\CondsurveyStatus;
use Tfcloud\Models\DocRecordType;
use Tfcloud\Models\Document;
use Tfcloud\Models\Idwork;
use Tfcloud\Models\IdworkElement;
use Tfcloud\Models\IdworkPriority;
use Tfcloud\Models\IdworkStatusType;
use Tfcloud\Models\Module;
use Tfcloud\Services\Condition\ConditionService;
use Tfcloud\Services\PermissionService;

class CNDCLIINGW01ConditionSurveyService extends WkHtmlToPdfReportBaseService
{
    private $conditionService;

    public const REPORT_ID = 278;

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->conditionService = new ConditionService($permissionService);
    }

    public function runReport($repId, $condSurveyId, &$errMsg = [])
    {

        /*Get condition survey*/
        $condSurveyData = $this->getCondSurveyDataQuery($condSurveyId)->first();

        if (!$condSurveyData) {
            $errMsg[] = 'No condition survey found';
            return false;
        }

        if ($condSurveyData->condsurvey_status_id == CondsurveyStatus::HISTORIC) {
            $errMsg[] = 'Not able to print Historic Condition Survey.';
            return false;
        }

        $sitePhoto = $this->getSitePhoto(
            $condSurveyData->site_id,
            $condSurveyData->site_group_id,
            $condSurveyData->site_photo
        );

        $logoContact = $this->getUserContactLogoForSite($condSurveyData->user_contact1);
        if (!$logoContact) {
            \Log::info('Bespoke Condition Survey Report', ['msg' => "Condition Report: Unable to find client logo.
            No document of type LOGO found for contact id = " . $condSurveyData->user_contact1]);
        }


        /*Get building survey*/
        $buildingSurveyData = $this->getBuildingSurveyQuery($condSurveyId)->get();

        /*Get summary by element for site*/
        $idWorkPriority = IdworkPriority::userSiteGroup()
            ->select(['idwork_priority_code', 'idwork_priority_id'])
            ->orderBy('idwork_priority_code')
            ->get();

        $priorityIdList = [
            'priority_1' => null,
            'priority_2' => null,
            'priority_3' => null,
            'priority_4' => null,
        ];

        foreach ($idWorkPriority as $key => $priority) {
            $priorityIdList['priority_' . ($key + 1)] = $priority->idwork_priority_id;
        }

        $summaryElementForSite = $this->getSummaryByElementQueryBuilder($condSurveyId, $priorityIdList)->get();

        /*Get summary by element for building*/
        $summaryElementForBuildings = $this->getSummaryByElementForBuilding(
            $condSurveyId,
            $priorityIdList,
            $buildingSurveyData
        );

        /*Get Identified Work Breakdown*/
        $idWorkBreakDownP1 = $this->getIdWorkByPriority($condSurveyId, $priorityIdList['priority_1']);
        $idWorkBreakDownP2 = $this->getIdWorkByPriority($condSurveyId, $priorityIdList['priority_2']);
        $idWorkBreakDownP3 = $this->getIdWorkByPriority($condSurveyId, $priorityIdList['priority_3']);
        $idWorkBreakDownP4 = $this->getIdWorkByPriority($condSurveyId, $priorityIdList['priority_4']);

        /*Get Photo Schedule*/
        $idWorkPhotos = $this->getIdWorkPhotos($condSurveyId);

        $reportFileName = ReportConstant::SYSTEM_REPORT_CND_CLI_INGW01 . '_' . $condSurveyData->condsurvey_code;
        $repOutPutPdfFile = $this->generateReportOutPutPath($reportFileName);

        // Create wkhtmltopdf temp folder to store generated files
        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);

        $header = \View::make('reports.wkhtmltopdf.condition.cndCliIngW01.header', ['logoContact' => $logoContact])
            ->render();
        $headerPath = $generatedPath . "/header.html";
        file_put_contents($headerPath, $header, LOCK_EX);

        $footer = \View::make('reports.wkhtmltopdf.condition.cndCliIngW01.footer')->render();
        $footerPath = $generatedPath . "/footer.html";
        file_put_contents($footerPath, $footer, LOCK_EX);

        $content = \View::make(
            'reports.wkhtmltopdf.condition.cndCliIngW01.content',
            [
                'condSurveyData' => $condSurveyData,
                'sitePhoto' => $sitePhoto,
                'buildingSurveyData' => $buildingSurveyData,
                'summaryElementForSite' => $summaryElementForSite,
                'summaryElementForBuildings' => $summaryElementForBuildings,
                'idWorkBreakDownP1' => $idWorkBreakDownP1,
                'idWorkBreakDownP2' => $idWorkBreakDownP2,
                'idWorkBreakDownP3' => $idWorkBreakDownP3,
                'idWorkBreakDownP4' => $idWorkBreakDownP4,
                'idWorkPhotos' => $idWorkPhotos,
            ]
        )->render();
        $contentPath = $generatedPath . "/content.html";
        file_put_contents($contentPath, $content, LOCK_EX);

        $options = [
            'header-html' => $headerPath,
            'footer-html' => $footerPath,
            'header-spacing' => 4,
            'footer-spacing' => 4
        ];

        $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $options);

        return $repOutPutPdfFile;
    }

    protected function getPhotoStoragePathway($siteGroupId, $itemKey, $photoFolder)
    {
        $uploadPath = \Config::get('cloud.legacy_paths.uploads');
        $modelName = $photoFolder . '/' ;
        return $uploadPath . '/' . $siteGroupId . '/' . $modelName . '/' .
        $itemKey . '/';
    }

    private function getCondSurveyDataQuery($condSurveyId)
    {
        $query = Condsurvey::userSiteGroup()
            ->join('site', 'site.site_id', '=', 'condsurvey.site_id')
            ->join(
                'condsurvey_status',
                'condsurvey_status.condsurvey_status_id',
                '=',
                'condsurvey.condsurvey_status_id'
            )
            ->leftJoin('site_type', 'site_type.site_type_id', '=', 'site.site_type_id')
            ->leftJoin('gen_userdef_group', 'gen_userdef_group.gen_userdef_group_id', '=', 'site.gen_userdef_group_id')
            ->leftJoin(
                'gen_userdef_group as cond_gen_userdef_group',
                'cond_gen_userdef_group.gen_userdef_group_id',
                '=',
                'condsurvey.gen_userdef_group_id'
            )
            ->leftJoin(
                'contact as cond_gen_userdef_group_contact1',
                'cond_gen_userdef_group_contact1.contact_id',
                '=',
                'cond_gen_userdef_group.user_contact1'
            )
            ->leftJoin(
                'contact as cond_gen_userdef_group_contact2',
                'cond_gen_userdef_group_contact2.contact_id',
                '=',
                'cond_gen_userdef_group.user_contact2'
            )
            ->leftJoin('address', 'address.address_id', '=', 'site.address_id')
            ->leftJoin('address_street', 'address_street.address_street_id', '=', 'address.address_street_id')
            ->leftJoin('building', 'building.building_id', '=', 'condsurvey.condsurvey_building_id')
            ->leftJoin('contact', 'contact.contact_id', '=', 'condsurvey.surveyor')
            ->where('condsurvey.condsurvey_id', $condSurveyId)
            ->select([
                'condsurvey.condsurvey_id',
                'condsurvey.site_group_id',
                'condsurvey.condsurvey_code',
                'condsurvey.condsurvey_desc',
                'condsurvey.site_id',
                \DB::raw("DATE_FORMAT(condsurvey.survey_date, '%d/%m/%Y') AS survey_date"),
                'condsurvey.surveyor',
                'condsurvey.gen_summary',
                'condsurvey.mech_summary',
                'condsurvey.elec_summary',
                'condsurvey_status.condsurvey_status_id',
                'condsurvey_status.condsurvey_status_code',
                'condsurvey_status.condsurvey_status_desc',
                'contact.contact_name AS surveyor_name',
                'contact.organisation AS surveyor_organisation',
                'site.site_code',
                'site.site_desc',
                'site.uprn',
                'site.photo AS site_photo',
                'site_type.site_type_code',
                'site_type.site_type_desc',
                'address.first_addr_obj AS site_address_number',
                'address_street.street AS site_street',
                'address_street.locality AS site_locality',
                'address_street.town AS site_town',
                'address_street.region AS site_region',
                'address.postcode AS site_postcode',
                'gen_userdef_group.user_text1',
                'gen_userdef_group.user_text2',
                'gen_userdef_group.user_text3',
                'gen_userdef_group.user_contact1',
                'cond_gen_userdef_group.user_text1 AS cond_user_text1',
                'cond_gen_userdef_group.user_text2 AS cond_user_text2',
                'cond_gen_userdef_group.user_contact1 AS cond_user_contact1',
                'cond_gen_userdef_group.user_contact2 AS cond_user_contact1',
                'cond_gen_userdef_group_contact1.contact_name AS cond_userdef_contact1_name',
                'cond_gen_userdef_group_contact1.organisation AS cond_userdef_contact1_org',
                'cond_gen_userdef_group_contact2.contact_name AS cond_userdef_contact2_name',
                'cond_gen_userdef_group_contact2.organisation AS cond_userdef_contact2_org',
            ]);
        return $query;
    }

    private function getSitePhoto($siteId, $siteGroupId, $sitePhoto)
    {
        if (!$sitePhoto) {
            return false;
        }
        $photoPath = $this->getPhotoStoragePathway(
            $siteGroupId,
            $siteId,
            'site'
        );
        $photoPath = $photoPath . "/" . $sitePhoto;
        if (file_exists($photoPath) && getimagesize($photoPath)) {
            $photoPath = str_replace("/", "\\", $photoPath);
            return $photoPath;
        } else {
            return false;
        }
    }
    private function getUserContactLogoForSite($userContactId)
    {
        $document = Document::userSiteGroup()
            ->join('doc_record_type', 'doc_record_type.doc_record_type_id', '=', 'document.doc_record_type_id')
            ->leftJoin('doc_group', 'doc_group.doc_group_id', '=', 'document.doc_group_id')
            ->where('document.doc_record_type_id', DocRecordType::CONTACT)
            ->where('doc_record_type.module_id', Module::MODULE_GENERAL)
            ->where('document.record_id', $userContactId)
            ->where('doc_group.doc_group_code', 'LOGO ')
            ->orderBy('document.document_id', 'desc')
            ->select(['document.document_id', 'document.site_group_id', 'document.document_name'])
            ->first();
        if ($document) {
            $logoPath = $this->getPhotoStoragePathway(
                $document->site_group_id,
                $document->document_id,
                'document'
            );
            $logoPath = $logoPath . "/" . $document->document_name;
            if (file_exists($logoPath) && getimagesize($logoPath)) {
                $photoPath = str_replace("/", "\\", $logoPath);
                return $photoPath;
            } else {
                return false;
            }
        }
        return false;
    }
    private function getBuildingSurveyQuery($condSurveyId)
    {
        $condLocationQuery = \DB::table('cond_location_score')->join(
            'condsurvey',
            'condsurvey.condsurvey_id',
            '=',
            'cond_location_score.condsurvey_id'
        )->leftJoin('building', 'building.building_id', '=', 'cond_location_score.location_record_id')
        ->where('condsurvey.site_group_id', \Auth::user()->site_group_id)
        ->where('condsurvey.condsurvey_id', $condSurveyId)
        ->where('cond_location_score.cond_location_type_id', CondLocationType::TYPE_BUILDING)
        ->select([
            'building.building_id',
            'building.building_code',
            'building.building_desc',
            'building.gia',
            'building.gea',
            \DB::raw("(SELECT GROUP_CONCAT(building_age.building_age_desc SEPARATOR ', ') FROM
            building_block LEFT JOIN building_age ON building_age.building_age_id = building_block.building_age_id
            WHERE building_block.building_id = building.building_id
            GROUP BY building_block.building_id) AS building_age_desc")
        ]);

        $idWorkBuildingQuery = \DB::table('building')->join('idwork', 'idwork.building_id', '=', 'building.building_id')
            ->join('condsurvey', 'condsurvey.condsurvey_id', '=', 'idwork.condsurvey_id')
            ->where('condsurvey.site_group_id', \Auth::user()->site_group_id)
            ->where('condsurvey.condsurvey_id', $condSurveyId)
            ->select([
                'building.building_id',
                'building.building_code',
                'building.building_desc',
                'building.gia',
                'building.gea',
                \DB::raw("(SELECT GROUP_CONCAT(building_age.building_age_desc SEPARATOR ', ') FROM
            building_block LEFT JOIN building_age ON building_age.building_age_id = building_block.building_age_id
            WHERE building_block.building_id = building.building_id
            GROUP BY building_block.building_id) AS building_age_desc")
            ]);
        $query = $condLocationQuery->union($idWorkBuildingQuery)->orderBy('building_code')->distinct();

        return $query;
    }
    private function getSummaryByElementForBuilding($condSurveyId, $priorityIdList, $buildingSurveyData)
    {
        $summaryByElementForBuilding = [];

        foreach ($buildingSurveyData as $building) {
            $item = ['buildingCodeDesc' => null, 'summaryElements' => null];
            $item['buildingCodeDesc'] = Common::concatFields([$building->building_code, $building->building_desc]);
            $item['summaryElements'] = $this->getSummaryByElementQueryBuilder(
                $condSurveyId,
                $priorityIdList,
                $building->building_id
            )->get();
            array_push($summaryByElementForBuilding, $item);
        }
        return $summaryByElementForBuilding;
    }

    /**
     * @param $condSurveyId
     * @param $priorityIdList
     * @param null $buildingId
     * @internal param $idWorkPriorityCodeList
     * @return mixed
     */
    private function getSummaryByElementQueryBuilder(
        $condSurveyId,
        $priorityIdList,
        $buildingId = null
    ) {
        $query = IdworkElement::userSiteGroup()->orderBy('idwork_element.idwork_element_code');
        $select = [
            \DB::raw("CONCAT_WS(' - ', idwork_element.idwork_element_code, idwork_element.idwork_element_desc)
            AS element_code_desc")
        ];
        foreach ($priorityIdList as $key => $priorityId) {
            array_push(
                $select,
                $this->countIdWorkQueryBuilder(
                    $condSurveyId,
                    $key,
                    $priorityId,
                    $buildingId
                )
            );
        }
        $query->select($select);
        return $query;
    }

    private function countIdWorkQueryBuilder($condSurveyId, $aliasPriority, $idWorkPriorityId, $buildingId = null)
    {
        $buildingWhere = '';
        if ($buildingId) {
            $buildingWhere = ' AND idwork.building_id = ' . $buildingId;
        }
        $idWorkStatusDraft = IdworkStatusType::DRAFT;
        $query = \DB::raw(
            "(
            SELECT SUM(idwork.total_cost)
            FROM idwork
              JOIN idwork_priority ON idwork_priority.idwork_priority_id = idwork.idwork_priority_id
              JOIN idwork_status ON idwork_status.idwork_status_id = idwork.idwork_status_id
            WHERE idwork.element_id = idwork_element.idwork_element_id
                  AND idwork.condsurvey_id = {$condSurveyId}
                  AND idwork_status.idwork_status_type_id = {$idWorkStatusDraft}
                  AND idwork_priority.idwork_priority_id = {$idWorkPriorityId}
                  {$buildingWhere}
          ) AS '{$aliasPriority}'"
        );
        return $query;
    }

    private function getIdWorkByPriority($condSurveyId, $idWorkPriorityId)
    {
        $query = Idwork::join('idwork_priority', 'idwork_priority.idwork_priority_id', '=', 'idwork.idwork_priority_id')
            ->join('building', 'building.building_id', '=', 'idwork.building_id')
            ->leftJoin('idwork_condition', 'idwork_condition.idwork_condition_id', '=', 'idwork.idwork_condition_id')
            ->leftJoin('room', 'room.room_id', '=', 'idwork.room_id')
            ->leftJoin('idwork_element', 'idwork_element.idwork_element_id', '=', 'idwork.element_id')
            ->join('idwork_status', 'idwork_status.idwork_status_id', '=', 'idwork.idwork_status_id')
            ->where('idwork.condsurvey_id', $condSurveyId)
            ->where('idwork.idwork_priority_id', $idWorkPriorityId)
            ->where('idwork_status.idwork_status_type_id', IdworkStatusType::DRAFT)
            ->orderBy('idwork_priority.idwork_priority_code')
            ->orderBy('building.building_code')
            ->orderBy('room.room_number')
            ->select([
                'idwork.idwork_id',
                'idwork.idwork_code',
                'idwork_priority.idwork_priority_code',
                'building.building_id',
                'building.building_code',
                'building.building_desc',
                'room.room_number',
                'room.room_desc',
                'idwork_condition.idwork_condition_code',
                'idwork.idwork_condition_id',
                'idwork.remedy',
                'idwork.defect',
                'idwork.total_cost',
                'idwork.photo',
                'idwork_element.idwork_element_code',
                'idwork_element.idwork_element_desc',
            ]);
        return $query->get();
    }

    private function getIdWorkPhotos($condSurveyId)
    {
        $idWorks = Idwork::leftJoin('building', 'building.building_id', '=', 'idwork.building_id')
            ->leftJoin('idwork_element', 'idwork_element.idwork_element_id', '=', 'idwork.element_id')
            ->join('idwork_status', 'idwork_status.idwork_status_id', '=', 'idwork.idwork_status_id')
            ->where('idwork.condsurvey_id', $condSurveyId)
            ->where('idwork_status.idwork_status_type_id', IdworkStatusType::DRAFT)
            ->whereNotNull('idwork.photo')
            ->select([
                'idwork.idwork_id',
                'idwork.idwork_code',
                'building.building_code',
                'idwork.photo',
                'idwork_element.idwork_element_code',
                'idwork_element.idwork_element_desc',
            ])
            ->orderBy('building.building_code')
            ->orderBy('idwork.idwork_code')
            ->get();
        $siteGroupId = \Auth::user()->site_group_id;
        $idWorkArr = [];
        foreach ($idWorks as $idWork) {
            $reducedPhotoPath = $this->reducedPhotoPath(
                'idwork',
                $idWork->idwork_id,
                $idWork->photo,
                $siteGroupId
            );
            $idWork->photo = $reducedPhotoPath;
            $idWorkArr['idwork_id_' . $idWork->idwork_id] = [
                'idwork_code' => $idWork->idwork_code,
                'photo' => $idWork->photo,
                'idwork_element_code' => $idWork->idwork_element_code,
                'idwork_element_desc' => $idWork->idwork_element_desc,
            ];
        }
        return $idWorkArr;
    }
}
