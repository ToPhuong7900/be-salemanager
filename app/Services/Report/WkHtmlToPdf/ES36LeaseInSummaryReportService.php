<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use DateTime;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\BoxFilterQuery\IReportFilterQuery;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Constant\ReportConstant;
use Tfcloud\Lib\Estates\PaymentSummary;
use Tfcloud\Lib\Exceptions\PermissionException;
use Tfcloud\Lib\Filters\Estate\LeaseInFilter;
use Tfcloud\Models\CrudAccessLevel;
use Tfcloud\Models\Estate\EstBreakDateParentType;
use Tfcloud\Models\Estate\EstChargeCalcEndPoint;
use Tfcloud\Models\EstAgreement;
use Tfcloud\Models\EstCharge;
use Tfcloud\Models\FinAccount;
use Tfcloud\Models\GenTable;
use Tfcloud\Models\LeaseIn;
use Tfcloud\Models\LeaseInProperty;
use Tfcloud\Models\LeaseOut;
use Tfcloud\Models\Module;
use Tfcloud\Models\Report;
use Tfcloud\Services\AddressFilterService;
use Tfcloud\Services\Admin\General\ContactService;
use Tfcloud\Services\Admin\System\Estate\AgreementOnusService;
use Tfcloud\Services\Admin\System\Estate\AgreementTypeService;
use Tfcloud\Services\Estate\AgreementService;
use Tfcloud\Services\Estate\EstBreakDateService;
use Tfcloud\Services\Estate\EstChargeService;
use Tfcloud\Services\Estate\InvoiceGenerationPlanService;
use Tfcloud\Services\Estate\LeaseInSubLeaseService;
use Tfcloud\Services\Estate\LeaseOutService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Estate\LeaseInService;
use Tfcloud\Services\Property\BuildingService;
use Tfcloud\Services\Property\SiteService;
use Tfcloud\Services\UserService;

class ES36LeaseInSummaryReportService extends WkHtmlToPdfReportBaseService
{
    private $leaseInService;
    private $leaseOutService;
    private $userService;
    private $contactService;
    private $siteService;
    private $buildingService;
    private $agreementService;
    private $agreementTypeService;
    private $agreementOnusService;
    private $leaseInSubLeaseService;
    private $invoiceGenerationPlanService;
    private $estBreakDateService;
    public $filterText = '';
    private $reportFilterQuery = null;
    private $reportBoxFilterLoader = null;

    public function __construct(PermissionService $permissionService, Report $report)
    {
        parent::__construct($permissionService);
        $this->permissionService = $permissionService;
        $this->leaseInService = new LeaseInService($permissionService);
        $this->leaseOutService = new LeaseOutService($permissionService);
        $this->userService = new UserService();
        $this->contactService = new ContactService($permissionService);
        $this->siteService = new SiteService($permissionService);
        $this->buildingService = new BuildingService($permissionService);
        $this->agreementService = new AgreementService($permissionService);
        $this->agreementTypeService = new AgreementTypeService($permissionService);
        $this->agreementOnusService = new AgreementOnusService($permissionService);
        $this->leaseInSubLeaseService = new LeaseInSubLeaseService($permissionService, $this->leaseOutService);
        $this->invoiceGenerationPlanService = new InvoiceGenerationPlanService(
            $permissionService,
            ['checkPermission' => false]
        );
        $this->estBreakDateService = new EstBreakDateService($permissionService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function runReportSingleLease($lease)
    {
        //call report from button not report view
        $inputs['id'] = $lease->lease_in_id;
        $inputs['singleOnly'] = true;
        $repId = Common::getSystemReportId(ReportConstant::SYSTEM_REPORT_ES36);

        return $this->runReport(
            $this->getTempFile(),
            $inputs,
            $repId
        );
    }


    public function runReport($repOutPutPdfFile, $inputs, $repId)
    {
        $leaseIns = $this->getData($inputs);
        // Create wkhtmltopdf temp folder to store generated files
        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);

        $header = \View::make('reports.wkhtmltopdf.estates.es36.header', [
            'logo' => $this->getHeaderLogo()
        ])->render();
        $headerPath = $generatedPath . "/header.html";
        file_put_contents($headerPath, $header, LOCK_EX);

        $footer = \View::make('reports.wkhtmltopdf.estates.es36.footer')->render();
        $footerPath = $generatedPath . "/footer.html";
        file_put_contents($footerPath, $footer, LOCK_EX);

        $content = \View::make(
            'reports.wkhtmltopdf.estates.es36.content',
            [
                'leaseIns' => $leaseIns,
            ]
        )->render();
        $contentPath = $generatedPath . "/content.html";
        file_put_contents($contentPath, $content, LOCK_EX);

        $options = [
            'header-html' => $headerPath,
            'footer-html' => $footerPath,
            'header-spacing' => 4,
            'footer-spacing' => 4,
            'orientation' => 'portrait',
        ];

        if (array_key_exists('singleOnly', $inputs)) {
            $repOutPutPdfFile .= '.pdf';
            $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $options);
        } else {
            $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $options);
        }


        $whereCodes = [];               // The codes which have been used in the filter
        $whereTexts = [];               // Plain text about filtering applied
        $orCodes = [];                  // csv string of OR codes
        $bFilterDetail = false;

        if ($this->reportFilterQuery instanceof IReportFilterQuery) {
            $this->filterText = $this->reportFilterQuery->getFilterDetailText();
        } else {
            $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
            $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $this->filterText);
        }

        if (array_key_exists('singleOnly', $inputs)) {
            return $repOutPutPdfFile;
        } else {
            return true;
        }
    }

    private function getTempFile()
    {
        return Common::getTempFolder() . DIRECTORY_SEPARATOR . 'ES36_SingleLeaseSummary';
    }

    private function formatInputData(&$inputs)
    {
        foreach ($inputs as $key => $value) {
            if ($value === "") {
                unset($inputs[$key]);
            }
        }
    }

    private function formatInputBoxFilterData($inputs)
    {
        $inputs['code'] = $this->reportFilterQuery->getFirstValueFilterField("lease_in_code");
        $inputs['description'] = $this->reportFilterQuery->getFirstValueFilterField("lease_in_desc");
        $inputs['leaseType'] = $this->reportFilterQuery->getFirstValueFilterField("lease_type_id");
        $inputs['leaseSubType'] = $this->reportFilterQuery->getFirstValueFilterField("est_lease_sub_type_id");
        $inputs['holdingOver'] = $this->reportFilterQuery->getFirstValueFilterField("holding_over");
        $inputs['rates_liability_id'] = $this->reportFilterQuery->getFirstValueFilterField("rates_liability_id");
        $inputs['insce_liability_id'] = $this->reportFilterQuery->getFirstValueFilterField("insce_liability_id");
        $inputs['estate_maint_id'] = $this->reportFilterQuery->getFirstValueFilterField("estate_maint_id");
        $inputs['periodOfLeaseFlt'] = $this->reportFilterQuery->getFirstValueFilterField("period_of_lease_id");
        $inputs['site_id'] = $this->reportFilterQuery->getFirstValueFilterField("site_id");
        $inputs['building_id'] = $this->reportFilterQuery->getFirstValueFilterField("building_id");
        $inputs['room_id'] = $this->reportFilterQuery->getFirstValueFilterField("room_id");
        $inputs['contact'] = $this->reportFilterQuery->getFirstValueFilterField("landlord_contact_id");
        $inputs['owner'] = $this->reportFilterQuery->getFirstValueFilterField("owner_user_id");
        $inputs['leaseInStatus'] = $this->reportFilterQuery->getFirstValueFilterField("lease_in_status_id");
        $inputs['landlordTenantAct'] = $this->reportFilterQuery->getFirstValueFilterField("landlord_tenant_act");
        $inputs['vatIncluded'] = $this->reportFilterQuery->getFirstValueFilterField("vat_included");
        $inputs['applicable'] = $this->reportFilterQuery->getFirstValueFilterField("applicable");
        // address
        $inputs['address'] = $this->reportFilterQuery->getFirstValueFilterField("address");
        $inputs['addressSub']['sub_dwelling'] = $this->reportFilterQuery->getFirstValueFilterField("second_addr_obj");
        $inputs['addressSub']['number_name'] = $this->reportFilterQuery->getFirstValueFilterField("first_addr_obj");
        $inputs['addressSub']['street'] = $this->reportFilterQuery->getFirstValueFilterField("street");
        $inputs['addressSub']['locality'] = $this->reportFilterQuery->getFirstValueFilterField("locality");
        $inputs['addressSub']['town'] = $this->reportFilterQuery->getFirstValueFilterField("town");
        $inputs['addressSub']['region'] = $this->reportFilterQuery->getFirstValueFilterField("region");
        $inputs['addressSub']['postcode'] = $this->reportFilterQuery->getFirstValueFilterField("postcode");
        $inputs['addressSub']['country'] = $this->reportFilterQuery->getFirstValueFilterField("country");

        $valueHoldingOverDate = $this->reportFilterQuery->getFirstValueFilterField("holding_over_date");
        $inputs['holdingOverFrom'] = Common::dateFieldDisplayFormat(Arr::first($valueHoldingOverDate));
        $inputs['holdingOverTo'] = Common::dateFieldDisplayFormat(Arr::last($valueHoldingOverDate));

        $valueAgreementDate = $this->reportFilterQuery->getFirstValueFilterField("agreement_date");
        $inputs['agreementDateFrom'] = Common::dateFieldDisplayFormat(Arr::first($valueAgreementDate));
        $inputs['agreementDateTo'] = Common::dateFieldDisplayFormat(Arr::last($valueAgreementDate));

        $valueAuthorisedDate = $this->reportFilterQuery->getFirstValueFilterField("authorised_date");
        $inputs['authorisedDateFrom'] = Common::dateFieldDisplayFormat(Arr::first($valueAuthorisedDate));
        $inputs['authorisedDateTo'] = Common::dateFieldDisplayFormat(Arr::last($valueAuthorisedDate));

        $valueTerminationDate = $this->reportFilterQuery->getFirstValueFilterField("termination_date");
        $inputs['terminationDateFrom'] = Common::dateFieldDisplayFormat(Arr::first($valueTerminationDate));
        $inputs['terminationDateTo'] = Common::dateFieldDisplayFormat(Arr::last($valueTerminationDate));

        $valueStartDate = $this->reportFilterQuery->getFirstValueFilterField("start_date");
        $inputs['startDateFrom'] = Common::dateFieldDisplayFormat(Arr::first($valueStartDate));
        $inputs['startDateTo'] = Common::dateFieldDisplayFormat(Arr::last($valueStartDate));

        $valueEndDate = $this->reportFilterQuery->getFirstValueFilterField("end_date");
        $inputs['endDateFrom'] = Common::dateFieldDisplayFormat(Arr::first($valueEndDate));
        $inputs['endDateTo'] = Common::dateFieldDisplayFormat(Arr::last($valueEndDate));

        return $inputs;
    }

    public function getAll($inputs)
    {
        if (
            !$this->permissionService->hasModuleAccess(
                Module::MODULE_ESTATES,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
            )
        ) {
            $msg = "Not met minimum readonly access for estates module in class" .
                " (" . __CLASS__ . ") method (" . __METHOD__ . "). " .
                " See line (" . __LINE__ . ")";
            throw new PermissionException($msg);
        }

        $query = $this->all();
        $query = $this->permissionService->listViewSiteAccess($query, "vw_lease_in_property.site_id", $siteAccess);
        $query = $this->filterAll($query, $inputs);

        return $query;
    }

    public function filterAll($query, $inputs, $view = false)
    {
        $filter = new LeaseInFilter($inputs);

        if ($view) {
            $table = $view;
        } else {
            $table = 'lease_in';
            $query->orderBy($filter->sort, $filter->sortOrder);
        }

        if (!is_null($filter->id)) {
            $query->where($table . '.lease_in_id', $filter->id);
        }

        if (!is_null($filter->code)) {
            $query->where($table . '.lease_in_code', 'like', '%' . trim($filter->code) . '%');
        }

        if (!is_null($filter->description)) {
            $query->where($table . '.lease_in_desc', 'like', '%' . trim($filter->description) . '%');
        }

        if (!is_null($filter->leaseInStatus)) {
            $query->where($table . '.lease_in_status_id', $filter->leaseInStatus);
        }

        if (!is_null($filter->leaseType)) {
            $query->where($table . '.lease_type_id', $filter->leaseType);
        }

        if (!is_null($filter->leaseSubType)) {
            $query->where($table . '.est_lease_sub_type_id', $filter->leaseSubType);
        }

        if (!is_null($filter->periodOfLeaseFlt)) {
            $query->where($table . '.period_of_lease_id', $filter->periodOfLeaseFlt);
        }

        if (!is_null($filter->contact)) {
            $query->where($table . '.landlord_contact_id', $filter->contact);
        }

        if (!is_null($filter->owner)) {
            $query->where($table . '.owner_user_id', $filter->owner);
        }

        $filter->site = !is_null($filter->site) ?
            $filter->site : (!is_null($filter->site_id) ? $filter->site_id : null);
        $filter->building = !is_null($filter->building) ?
            $filter->building : (!is_null($filter->building_id) ? $filter->building_id : null);

        if (!is_null($filter->site)) {
            $leaseInIds = $this->getLeaseInIdBySite($filter->site);
            if (!empty($leaseInIds)) {
                $query->whereIn($table . '.lease_in_id', $leaseInIds);
            }
        }

        if (!is_null($filter->building)) {
            $leaseInIds = $this->getLeaseInIdByBuilding($filter->building);
            if (!empty($leaseInIds)) {
                $query->whereIn($table . '.lease_in_id', $leaseInIds);
            }
        }

        $addressFilterService = new AddressFilterService();
        $query = $addressFilterService->buildQuery($filter, $query, $view);

        if (
            !is_null($filter->holdingOver) &&
            in_array(
                $filter->holdingOver,
                [
                    CommonConstant::DATABASE_VALUE_YES,
                    CommonConstant::DATABASE_VALUE_NO
                ]
            )
        ) {
            $query->where($table . '.holding_over', $filter->holdingOver);
        }

        if (!is_null($filter->holdingOverFrom)) {
            $holdingOverFrom = DateTime::createFromFormat(CommonConstant::FORMAT_DATE, $filter->holdingOverFrom);
            if ($holdingOverFrom) {
                $query->where(
                    DB::raw("DATE_FORMAT({$table}.holding_over_date , '%Y-%m-%d')"),
                    '>=',
                    $holdingOverFrom->format('Y-m-d')
                );
            }
        }

        if (!is_null($filter->holdingOverTo)) {
            $holdingOverTo = DateTime::createFromFormat(CommonConstant::FORMAT_DATE, $filter->holdingOverTo);
            if ($holdingOverTo) {
                $query->where(
                    DB::raw("DATE_FORMAT({$table}.holding_over_date, '%Y-%m-%d')"),
                    '<=',
                    $holdingOverTo->format('Y-m-d')
                );
            }
        }

        if (!is_null($filter->agreementDateFrom)) {
            $agreementDateFrom = DateTime::createFromFormat(CommonConstant::FORMAT_DATE, $filter->agreementDateFrom);
            if ($agreementDateFrom) {
                $query->where(
                    DB::raw("DATE_FORMAT({$table}.agreement_date , '%Y-%m-%d')"),
                    '>=',
                    $agreementDateFrom->format('Y-m-d')
                );
            }
        }

        if (!is_null($filter->agreementDateTo)) {
            $agreementDateTo = DateTime::createFromFormat(CommonConstant::FORMAT_DATE, $filter->agreementDateTo);
            if ($agreementDateTo) {
                $query->where(
                    DB::raw("DATE_FORMAT({$table}.agreement_date, '%Y-%m-%d')"),
                    '<=',
                    $agreementDateTo->format('Y-m-d')
                );
            }
        }

        if (!is_null($filter->authorisedDateFrom)) {
            $authorisedDateFrom = DateTime::createFromFormat(CommonConstant::FORMAT_DATE, $filter->authorisedDateFrom);
            if ($authorisedDateFrom) {
                $query->where(
                    DB::raw("DATE_FORMAT({$table}.authorised_date , '%Y-%m-%d')"),
                    '>=',
                    $authorisedDateFrom->format('Y-m-d')
                );
            }
        }

        if (!is_null($filter->authorisedDateTo)) {
            $authorisedDateTo = DateTime::createFromFormat(CommonConstant::FORMAT_DATE, $filter->authorisedDateTo);
            if ($authorisedDateTo) {
                $query->where(
                    DB::raw("DATE_FORMAT({$table}.authorised_date, '%Y-%m-%d')"),
                    '<=',
                    $authorisedDateTo->format('Y-m-d')
                );
            }
        }

        if (!is_null($filter->terminationDateFrom)) {
            $terminationDateFrom = DateTime::createFromFormat(
                CommonConstant::FORMAT_DATE,
                $filter->terminationDateFrom
            );
            if ($terminationDateFrom) {
                $query->where(
                    DB::raw("DATE_FORMAT({$table}.termination_date , '%Y-%m-%d')"),
                    '>=',
                    $terminationDateFrom->format('Y-m-d')
                );
            }
        }

        if (!is_null($filter->terminationDateTo)) {
            $terminationDateTo = DateTime::createFromFormat(CommonConstant::FORMAT_DATE, $filter->terminationDateTo);
            if ($terminationDateTo) {
                $query->where(
                    DB::raw("DATE_FORMAT({$table}.termination_date, '%Y-%m-%d')"),
                    '<=',
                    $terminationDateTo->format('Y-m-d')
                );
            }
        }

        if (!is_null($filter->startDateFrom)) {
            $startDateFrom = DateTime::createFromFormat(CommonConstant::FORMAT_DATE, $filter->startDateFrom);
            if ($startDateFrom) {
                $query->where(
                    DB::raw("DATE_FORMAT({$table}.start_date , '%Y-%m-%d')"),
                    '>=',
                    $startDateFrom->format('Y-m-d')
                );
            }
        }

        if (!is_null($filter->startDateTo)) {
            $startDateTo = DateTime::createFromFormat(CommonConstant::FORMAT_DATE, $filter->startDateTo);
            if ($startDateTo) {
                $query->where(
                    DB::raw("DATE_FORMAT({$table}.start_date, '%Y-%m-%d')"),
                    '<=',
                    $startDateTo->format('Y-m-d')
                );
            }
        }

        if (!is_null($filter->endDateFrom)) {
            $endDateFrom = DateTime::createFromFormat(CommonConstant::FORMAT_DATE, $filter->endDateFrom);
            if ($endDateFrom) {
                $query->where(
                    DB::raw("DATE_FORMAT({$table}.end_date , '%Y-%m-%d')"),
                    '>=',
                    $endDateFrom->format('Y-m-d')
                );
            }
        }

        if (!is_null($filter->endDateTo)) {
            $endDateTo = DateTime::createFromFormat(CommonConstant::FORMAT_DATE, $filter->endDateTo);
            if ($endDateTo) {
                $query->where(
                    DB::raw("DATE_FORMAT({$table}.end_date, '%Y-%m-%d')"),
                    '<=',
                    $endDateTo->format('Y-m-d')
                );
            }
        }

        if (!is_null($filter->rates_liability_id)) {
            $query->where($table . '.rates_liability_id', $filter->rates_liability_id);
        }

        if (!is_null($filter->insce_liability_id)) {
            $query->where($table . '.insce_liability_id', $filter->insce_liability_id);
        }

        if (!is_null($filter->estate_maint_id)) {
            $query->where($table . '.estate_maint_id', $filter->estate_maint_id);
        }

        if (
            !is_null($filter->landlordTenantAct) &&
            in_array(
                $filter->landlordTenantAct,
                [
                    CommonConstant::DATABASE_VALUE_YES,
                    CommonConstant::DATABASE_VALUE_NO
                ]
            )
        ) {
            $query->where($table . '.landlord_tenant_act', $filter->landlordTenantAct);
        }

        return $query;
    }

    private function all()
    {
        return LeaseIn::userSiteGroup()
            ->leftJoin(
                'est_lease_sub_type',
                'lease_in.est_lease_sub_type_id',
                '=',
                'est_lease_sub_type.est_lease_sub_type_id'
            )
            ->leftJoin('period_of_lease', 'lease_in.period_of_lease_id', '=', 'period_of_lease.period_of_lease_id')
            ->leftJoin('lease_type', 'lease_in.lease_type_id', '=', 'lease_type.lease_type_id')
            ->leftJoin('deed_packet', 'lease_in.deed_packet_id', '=', 'deed_packet.deed_packet_id')
            ->leftJoin('lease_in_status', 'lease_in.lease_in_status_id', '=', 'lease_in_status.lease_in_status_id')
            ->leftJoin('contact', 'lease_in.landlord_contact_id', '=', 'contact.contact_id')
            ->leftJoin('vw_lease_in_property', 'vw_lease_in_property.lease_in_id', '=', 'lease_in.lease_in_id')
            ->leftJoin('site', 'vw_lease_in_property.site_id', '=', 'site.site_id')
            ->leftJoin('building', 'vw_lease_in_property.building_id', '=', 'building.building_id')
            ->leftJoin('unit_of_area', 'lease_in.area_unit_of_area_id', '=', 'unit_of_area.unit_of_area_id')
            ->leftJoin('rates_liability', 'lease_in.rates_liability_id', '=', 'rates_liability.rates_liability_id')
            ->leftJoin('insce_liability', 'lease_in.insce_liability_id', '=', 'insce_liability.insce_liability_id')
            ->leftJoin('estate_maint', 'lease_in.estate_maint_id', '=', 'estate_maint.estate_maint_id')
            ->select([
                'lease_in.lease_in_id',
                'lease_in.lease_in_code',
                'lease_in.lease_in_desc',
                'lease_in.landlord_contact_id',
                'lease_in.owner_user_id',
                'lease_in.holding_over',
                'lease_in.holding_over_date',
                'lease_in.inv_on_hold',
                'lease_in.inv_on_hold_date',
                'lease_in.in_perpetuity',
                'period_of_lease.period_of_lease_desc',
                'lease_in.rates_liability_id',
                'lease_in.insce_liability_id',
                'lease_in.estate_maint_id',
                'lease_in.area_value',
                'lease_in.comment',
                'lease_in.time_of_essence',
                'lease_in.late_rent_grace_days',
                'lease_in.late_rent_interest_above_base',
                'lease_in.late_rent_minimum_interest',
                'lease_in.late_rent_type_simple',
                'lease_in.late_review_interest_above_base',
                'lease_in.late_review_minimum_interest',
                'lease_in.late_review_type_simple',
                'lease_in.annual_rent',
                'lease_in.deposit_amount',
                'lease_in.length_years',
                'lease_in.length_months',
                'lease_in.length_days',
                'lease_type.lease_type_code',
                'lease_type.lease_type_desc',
                'est_lease_sub_type.est_lease_sub_type_code',
                'est_lease_sub_type.est_lease_sub_type_desc',
                'deed_packet.deed_packet_id',
                'deed_packet.deed_packet_code',
                'deed_packet.deed_packet_desc',
                'lease_in.start_date AS start_date',
                'lease_in.end_date',
                'lease_in.agreement_date',
                'lease_in_status.lease_in_status_code',
                'lease_in_status.lease_in_status_desc',
                Common::getSqlSelectConcatContactNameOrg(),
                'contact.contact_name AS landlord_name',
                'contact.organisation AS landlord_org',
                'contact.contact_id AS landlord_id',
                'contact.phone_main AS landlord_phone_main',
                'contact.phone_mobile AS landlord_phone_mobile',
                'contact.email_addr AS landlord_email_addr',
                'vw_lease_in_property.site_code_combined',
                'vw_lease_in_property.site_desc_combined',
                'vw_lease_in_property.building_code_combined',
                'vw_lease_in_property.building_desc_combined',
                'site.site_id',
                'site.site_code',
                'site.site_desc',
                'building.building_id',
                'building.building_code',
                'building.building_desc',
                'lease_in.lease_in_status_id',
                'lease_in.head_lease',
                'unit_of_area.unit_of_area_desc',
                'rates_liability.rates_liability_code',
                'rates_liability.rates_liability_desc',
                'insce_liability.insce_liability_code',
                'insce_liability.insce_liability_desc',
                'estate_maint.estate_maint_code',
                'estate_maint.estate_maint_desc'
            ]);
    }

    private function getAgreements($recordId)
    {
        if (
            !$this->permissionService->hasModuleAccess(
                Module::MODULE_ESTATES,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
            )
        ) {
            $msg = "Not met minimum readonly access for estates module in class" .
                " (" . __CLASS__ . ") method (" . __METHOD__ . "). " .
                " See line (" . __LINE__ . ")";
            throw new PermissionException($msg);
        }
        return EstAgreement::userSiteGroup()
            ->where('lease_in_id', $recordId)
            ->leftJoin('est_agr_type', 'est_agreement.est_agr_type_id', '=', 'est_agr_type.est_agr_type_id')
            ->leftJoin('est_agr_onus', 'est_agreement.est_agr_onus_id', '=', 'est_agr_onus.est_agr_onus_id')
            ->orderBy('est_agreement.est_agreement_code', 'ASC');
    }

    private function querySubLeases()
    {
        return LeaseOut::userSiteGroup()
            ->leftJoin(
                'est_lease_sub_type',
                'lease_out.est_lease_sub_type_id',
                '=',
                'est_lease_sub_type.est_lease_sub_type_id'
            )
            ->leftJoin('lease_type', 'lease_out.lease_type_id', '=', 'lease_type.lease_type_id')
            ->leftJoin('lease_out_status', 'lease_out.lease_out_status_id', '=', 'lease_out_status.lease_out_status_id')
            ->leftJoin('contact', 'lease_out.tenant_contact_id', '=', 'contact.contact_id')
            ->select([
                'lease_out.*',
                'lease_type.lease_type_code',
                'est_lease_sub_type.est_lease_sub_type_code',
                'est_lease_sub_type.est_lease_sub_type_desc',
                'lease_out_status.lease_out_status_code',
                Common::getSqlSelectConcatContactNameOrg(),
                'contact.contact_id',
                'contact.contact_name AS tenant_name',
                'contact.organisation AS tenant_org',
                'contact.contact_id AS tenant_id',
            ]);
    }

    private function getLeaseInCharges($leaseInId)
    {
        return EstCharge::userSiteGroup()
            ->leftJoin('lease_in', 'lease_in.lease_in_id', '=', 'est_charge.lease_in_id')
            ->leftJoin('est_charge_type', 'est_charge.est_charge_type_id', '=', 'est_charge_type.est_charge_type_id')
            ->leftJoin(
                'est_time_period',
                'est_charge.repeat_est_time_period_id',
                '=',
                'est_time_period.est_time_period_id'
            )
            ->leftJoin('tax_code', 'tax_code.tax_code_id', '=', 'est_charge.tax_code_id')
            ->where('est_charge.lease_in_id', $leaseInId)
            ->select([
                'est_charge.est_charge_id',
                'est_charge.est_charge_code',
                'est_charge.est_charge_desc',
                'est_charge.rent',
                'est_charge.annualised_rent',
                'est_charge.credit_note',
                DB::raw(
                    "IF((ISNULL(est_charge_type.est_charge_type_code) AND est_charge.rent = 'Y'),
                    'Rent', est_charge_type.est_charge_type_code) AS est_charge_type_code"
                ),
                'est_charge_type.est_charge_type_desc',
                'est_charge_start_date',
                'est_charge_end_date',
                'est_charge.regular_payment_start_date',
                'est_charge.regular_payment_end_date',
                'est_charge.initial_payment_amount',
                'est_charge.initial_payment_due_date',
                'est_charge.fin_account_id',
                'est_charge.tax_code_id',
                'tax_code.rate AS tax_rate',
                'est_time_period.est_time_period_code',
                'est_charge.period_freq',
                'est_time_period.est_time_period_desc',
                'est_charge.regular_payment_amount',
                DB::raw(
                    'IF(est_charge.credit_note = "' . CommonConstant::DATABASE_VALUE_YES . '",
                    est_charge.initial_payment_amount, est_charge.regular_payment_amount) as instalment'
                ),
                'est_charge.initial_payment_due_date',
                DB::raw(
                    'IF(est_charge.active ="'
                    . CommonConstant::DATABASE_VALUE_YES
                    . '","Active", "Inactive") as status'
                ),
                DB::raw(
                    'IF(est_charge.payment_in_advance ="'
                    . CommonConstant::DATABASE_VALUE_YES
                    . '","In Advance", "In Arrears") as terms'
                ),
                DB::raw(
                    'IF(ISNULL(est_charge.tax_code_id) ="'
                    . CommonConstant::DATABASE_VALUE_YES
                    . '","In Advance", "In Arrears") as terms'
                ),
                DB::raw(
                    "IF((ISNULL(est_charge.tax_code_id)),
                                0, est_charge.regular_payment_amount * tax_code.rate / 100) AS vat"
                )
            ]);
    }

    private function getTotalCharge($leaseIn, $nextYearDate)
    {
        $invoicePlanItem = $this->invoiceGenerationPlanService->getPlanItem([
            'lease_out_id' => null,
            'lease_in_id' => $leaseIn->lease_in_id,
            'process_up_to_date' => $nextYearDate
        ]);
        $months = $this->getMonths();
        $total = 0;

        foreach ($months as &$month) {
            foreach ($invoicePlanItem as $item) {
                $dueDate = new DateTime($item['charge_due_date']);
                $dueDateMonth = $dueDate->format('M');
                $dueDateYear = $dueDate->format('Y');
                if (($dueDateMonth == $month['month']) && ($dueDateYear == $month['year'])) {
                    $month['amount'] += $item['amount'];
                    $total += $item['amount'];
                }
            }
        }
        return [$months, $total];
    }

    private function getMonths()
    {
        $months = [];
        for ($i = 0; $i < 12; $i++) {
            $now = new DateTime(date('1-m-Y'));
            $month = $now->add(\DateInterval::createFromDateString("{$i} month"));
            $monthValue = $month->format('M');
            $yearValue = $month->format('Y');
            array_push(
                $months,
                [
                    'month' => $monthValue,
                    'year'  => $yearValue,
                    'amount' => 0
                ]
            );
        }
        return $months;
    }

    private function generateFilterData($filterData, &$whereCodes, &$whereTexts, &$orCodes)
    {
        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Lease In Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Lease In Description contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['leaseType'])) {
            array_push($whereCodes, ['lease_type', 'lease_type_id', 'lease_type_code', $val, 'Lease Type']);
        }

        if ($val = Common::iset($filterData['leaseSubType'])) {
            array_push(
                $whereCodes,
                ['est_lease_sub_type', 'est_lease_sub_type_id', 'est_lease_sub_type_code', $val, 'Lease Sub Type']
            );
        }

        if ($val = Common::iset($filterData['holdingOver'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, Lang::get('text.report_texts.holding_over_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, Lang::get('text.report_texts.holding_over_no'));
                    break;
                default:
                    array_push($whereTexts, Lang::get('text.report_texts.holding_over_all'));
                    break;
            }
        }

        if ($val = array_get($filterData, 'holdingOverFrom', null)) {
            array_push($whereTexts, sprintf(Lang::get('text.report_texts.holding_over_from'), $val));
        }

        if ($val = array_get($filterData, 'holdingOverTo', null)) {
            array_push($whereTexts, sprintf(Lang::get('text.report_texts.holding_over_to'), $val));
        }

        if ($val = array_get($filterData, 'agreementDateFrom', null)) {
            array_push($whereTexts, sprintf(Lang::get('text.report_texts.agreement_date_from'), $val));
        }

        if ($val = array_get($filterData, 'agreementDateTo', null)) {
            array_push($whereTexts, sprintf(Lang::get('text.report_texts.agreement_date_to'), $val));
        }

        if ($val = array_get($filterData, 'authorisedDateFrom', null)) {
            array_push($whereTexts, sprintf(Lang::get('text.report_texts.authorised_date_from'), $val));
        }

        if ($val = array_get($filterData, 'authorisedDateTo', null)) {
            array_push($whereTexts, sprintf(Lang::get('text.report_texts.authorised_date_to'), $val));
        }

        if ($val = array_get($filterData, 'terminationDateFrom', null)) {
            array_push($whereTexts, sprintf(Lang::get('text.report_texts.termination_date_from'), $val));
        }

        if ($val = array_get($filterData, 'terminationDateTo', null)) {
            array_push($whereTexts, sprintf(Lang::get('text.report_texts.termination_date_to'), $val));
        }

        if ($val = array_get($filterData, 'startDateFrom', null)) {
            array_push($whereTexts, sprintf(Lang::get('text.report_texts.start_date_from'), $val));
        }

        if ($val = array_get($filterData, 'startDateTo', null)) {
            array_push($whereTexts, sprintf(Lang::get('text.report_texts.start_date_to'), $val));
        }

        if ($val = array_get($filterData, 'endDateFrom', null)) {
            array_push($whereTexts, sprintf(Lang::get('text.report_texts.end_date_from'), $val));
        }

        if ($val = array_get($filterData, 'endDateTo', null)) {
            array_push($whereTexts, sprintf(Lang::get('text.report_texts.end_date_to'), $val));
        }

        if ($val = Common::iset($filterData['rates_liability_id'])) {
            array_push($whereCodes, ['rates_liability', 'rates_liability_id', 'rates_liability_code', $val, "Rates"]);
        }

        if ($val = Common::iset($filterData['insce_liability_id'])) {
            array_push(
                $whereCodes,
                [
                    'insce_liability',
                    'insce_liability_id',
                    'insce_liability_code',
                    $val,
                    "Insurance"
                ]
            );
        }

        if ($val = Common::iset($filterData['estate_maint_id'])) {
            array_push($whereCodes, ['estate_maint', 'estate_maint_id', 'estate_maint_code', $val, "Maintenance"]);
        }

        if ($val = Common::iset($filterData['address'])) {
            array_push($whereTexts, "Address contains '" . $val . "'");
        }
        $this->getAddressSub($filterData, $whereTexts);

        if ($siteId = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $siteId, "Site Code"]);

            if ($buildingId = Common::iset($filterData['building_id'])) {
                array_push(
                    $whereCodes,
                    ['building', 'building_id', 'building_code', $buildingId, "Building Code"]
                );
            }
        }

        if ($val = Common::iset($filterData['contact'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Landlord']);
        }

        if ($val = Common::iset($filterData['owner'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Owner']);
        }

        if ($val = Common::iset($filterData['leaseInStatus'])) {
            array_push(
                $whereCodes,
                ['lease_in_status', 'lease_in_status_id', 'lease_in_status_code', $val, 'Status']
            );
        }

        if ($val = Common::iset($filterData['landlordTenantAct'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, Lang::get('text.report_texts.landlord_tenant_act_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, Lang::get('text.report_texts.landlord_tenant_act_no'));
                    break;
                default:
                    array_push($whereTexts, Lang::get('text.report_texts.landlord_tenant_act_all'));
                    break;
            }
        }

        if ($val = Common::iset($filterData['areaFrom'])) {
            array_push($whereTexts, "Area contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['areaTo'])) {
            array_push($whereTexts, "Area to contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['unit_of_area_id'])) {
            array_push(
                $whereCodes,
                [
                    'unit_of_area',
                    'unit_of_area_id',
                    'unit_of_area_code',
                    $val,
                    "Unit of Area"
                ]
            );
        }

        if ($val = Common::iset($filterData['vatIncluded'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, Lang::get('text.report_texts.vat_included_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, Lang::get('text.report_texts.vat_included_no'));
                    break;
                default:
                    array_push($whereTexts, Lang::get('text.report_texts.vat_included_all'));
                    break;
            }
        }

        $this->reAddUserDefinesQuery(GenTable::LEASE_IN, $filterData, $whereCodes, $whereTexts);
    }

    private function getLeaseInIdBySite($site)
    {
        return LeaseInProperty::select('lease_in_id')->where('site_id', $site)
            ->pluck('lease_in_id')->toArray();
    }

    private function getLeaseInIdByBuilding($building)
    {
        return LeaseInProperty::select('lease_in_id')->where('building_id', $building)
            ->pluck('lease_in_id')->toArray();
    }

    public function getData($inputs)
    {
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $this->reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $inputs = $this->formatInputBoxFilterData($inputs);
        } else {
            $this->formatInputData($inputs);
        }

        $leaseIns = $this->getAll($inputs)->get();
        $firstDateOfCurrentMonth = (new DateTime(date('1-m-Y')));
        $nextYearDate = clone $firstDateOfCurrentMonth;
        $nextYearDate = $nextYearDate->add(\DateInterval::createFromDateString('1 year'));
        $nextYearDate = $nextYearDate->sub(\DateInterval::createFromDateString('1 day'))->format('Y-m-d');
        if (!is_null($leaseIns)) {
            foreach ($leaseIns as $leaseIn) {
                $ownerId = $leaseIn->owner_user_id;
                if (isset($ownerId)) {
                    $leaseIn->owner = !is_null($ownerId) ? $this->userService->get($ownerId, false) : null;
                }

                $landlordContactId = $leaseIn->landlord_contact_id;
                if (isset($landlordContactId)) {
                    $leaseIn->landlord = !is_null($landlordContactId) ?
                        $this->contactService->get($landlordContactId, false) : null;
                }

                $siteId = $leaseIn->siteId();
                if (isset($siteId)) {
                    $leaseIn->site = !is_null($siteId) ? $this->siteService->get($siteId, false) : null;
                }

                $occupiers = [];
                if ($leaseIn->head_lease == CommonConstant::DATABASE_VALUE_YES) {
                    $leaseOuts = $this->querySubLeases()
                        ->where('lease_out.head_lease_in_id', $leaseIn->lease_in_id)
                        ->get();
                    if ($leaseOuts->count()) {
                        foreach ($leaseOuts as $leaseOut) {
                            if (!empty($leaseOut->tenant_name)) {
                                array_push($occupiers, $leaseOut->tenant_name);
                            }
                        }
                    }
                }
                $leaseIn->occupiers = $occupiers;

                // Get Lease Address(es)
                $leasePropertyList = LeaseIn::userSiteGroup()
                    ->select('lease_in_property.site_id', 'lease_in_property.building_id')
                    ->leftjoin('lease_in_property', 'lease_in.lease_in_id', 'lease_in_property.lease_in_id')
                    ->leftjoin('site', 'site.site_id', 'lease_in_property.site_id')
                    ->leftjoin('building', 'building.building_id', 'lease_in_property.building_id')
                    ->where('lease_in.lease_in_id', '=', $leaseIn->lease_in_id)
                    ->orderBy('site.site_code', 'ASC')
                    ->orderBy('building.building_code', 'ASC')
                    ->get();

                if (isset($leasePropertyList) && ($leasePropertyList->count() > 0)) {
                    // Determine whether we have more than one different address
                    $oneAddress = true;
                    for ($i = 1; $i < $leasePropertyList->count(); $i++) {
                        if (
                            $leasePropertyList[0]->site_id != $leasePropertyList[$i]->site_id
                            || $leasePropertyList[0]->building_id != $leasePropertyList[$i]->building_id
                        ) {
                            $oneAddress = false;
                            break;
                        }
                    }

                    $leaseIn->one_address = $oneAddress;

                    if ($leaseIn->one_address && $leasePropertyList[0]->site_id) {
                        if ($leasePropertyList[0]->building_id) {
                            $building = $this->buildingService->get($leasePropertyList[0]->building_id, false);
                            $leaseIn->lease_address = implode(
                                "<br>",
                                array_filter(
                                    [
                                        $building->address->second_addr_obj,
                                        $building->address->first_addr_obj,
                                        $building->address->addressStreet->street,
                                        $building->address->addressStreet->locality,
                                        $building->address->addressStreet->town,
                                        $building->address->addressStreet->region,
                                        $building->address->postcode,
                                        $building->address->country,
                                    ]
                                )
                            );
                        } else {
                            $site = $this->siteService->get($leasePropertyList[0]->site_id, false);
                            $leaseIn->lease_address = implode(
                                "<br>",
                                array_filter(
                                    [
                                        $site->address->second_addr_obj,
                                        $site->address->first_addr_obj,
                                        $site->address->addressStreet->street,
                                        $site->address->addressStreet->locality,
                                        $site->address->addressStreet->town,
                                        $site->address->addressStreet->region,
                                        $site->address->postcode,
                                        $site->address->country,
                                    ]
                                )
                            );
                        }
                    } else {
                        $leaseAddress = [];
                        foreach ($leasePropertyList as $leaseProperty) {
                            if ($leaseProperty->building_id) {
                                $building = $this->buildingService->get($leaseProperty->building_id, false);
                                $buildingAddress = array_filter(
                                    [
                                        $building->address->second_addr_obj,
                                        $building->address->first_addr_obj,
                                        $building->address->addressStreet->street,
                                        $building->address->addressStreet->locality,
                                        $building->address->addressStreet->town,
                                        $building->address->addressStreet->region,
                                        $building->address->postcode,
                                        $building->address->country
                                    ]
                                );
                                $renderedAddress = implode(", ", $buildingAddress);
                                array_push($leaseAddress, $renderedAddress);
                            } elseif ($leaseProperty->site_id) {
                                $site = $this->siteService->get($leaseProperty->site_id, false);
                                $siteAddress = array_filter(
                                    [
                                        $site->address->second_addr_obj,
                                        $site->address->first_addr_obj,
                                        $site->address->addressStreet->street,
                                        $site->address->addressStreet->locality,
                                        $site->address->addressStreet->town,
                                        $site->address->addressStreet->region,
                                        $site->address->postcode,
                                        $site->address->country
                                    ]
                                );
                                $renderedAddress = implode(", ", $siteAddress);
                                array_push($leaseAddress, $renderedAddress);
                            }
                        }
                        $leaseAddress = array_unique($leaseAddress);
                        $renderedLeaseAddress = implode(
                            "<br><br>",
                            array_filter($leaseAddress)
                        );
                        $leaseIn->lease_address = $renderedLeaseAddress;
                    }
                }

                $leaseIn->activeRents = $this->getLeaseInCharges($leaseIn->lease_in_id)
                    ->where('est_charge.rent', CommonConstant::DATABASE_VALUE_YES)
                    ->where('est_charge.active', CommonConstant::DATABASE_VALUE_YES)
                    ->where('est_charge.credit_note', CommonConstant::DATABASE_VALUE_NO)
                    ->get();

                // Account ID: if all the active rents have the same Account, that's it.
                // Rent Start: if all the active rents have the same Start Date, that's it.
                $accountId = null;
                $startDate = null;
                if ($leaseIn->activeRents->count()) {
                    if (!is_null($leaseIn->activeRents[0]->fin_account_id)) {
                        $accountId = $leaseIn->activeRents[0]->fin_account_id;
                        for ($i = 1; $i < $leaseIn->activeRents->count(); $i++) {
                            if ($leaseIn->activeRents[$i]->fin_account_id != $accountId) {
                                $accountId = null;
                                break;
                            }
                        }
                    }
                    if (!is_null($leaseIn->activeRents[0]->regular_payment_start_date)) {
                        $startDate = $leaseIn->activeRents[0]->regular_payment_start_date;
                        for ($i = 1; $i < $leaseIn->activeRents->count(); $i++) {
                            if ($leaseIn->activeRents[$i]->regular_payment_start_date != $startDate) {
                                $startDate = null;
                                break;
                            }
                        }
                    }
                }
                if (!is_null($accountId)) {
                    $leaseIn->accountId = FinAccount::find($accountId)->fin_account_code;
                }
                $leaseIn->rentStart = $startDate;

                $length_years = null;
                $length_months = null;
                $length_days = null;
                if (!is_null($leaseIn->length_years)) {
                    if ($leaseIn->length_years > 1) {
                        $length_years = $leaseIn->length_years . ' Years';
                    } elseif ($leaseIn->length_years == 1) {
                        $length_years = $leaseIn->length_years . ' Year';
                    }
                }
                if (!is_null($leaseIn->length_months)) {
                    if ($leaseIn->length_months > 1) {
                        $length_months = $leaseIn->length_months . ' Months';
                    } elseif ($leaseIn->length_months == 1) {
                        $length_months = $leaseIn->length_months . ' Month';
                    }
                }
                if (!is_null($leaseIn->length_days)) {
                    if ($leaseIn->length_days > 1) {
                        $length_days = $leaseIn->length_days . ' Days';
                    } elseif ($leaseIn->length_days == 1) {
                        $length_days = $leaseIn->length_days . ' Day';
                    }
                }
                $length = [$length_years, $length_months, $length_days];
                $length = implode(
                    ", ",
                    array_filter($length)
                );
                $leaseIn->length = $length;

                // Annual Rent Net: sum of Annualised Rent of all active rents.
                // Annual VAT: sum of (tax_rate * Annualised Rent) of all active rents.
                $sum = 0;
                $vat = 0;
                if (isset($leaseIn->activeRents) && $leaseIn->activeRents->count() > 0) {
                    foreach ($leaseIn->activeRents as $activeRent) {
                        // CLD-15497/15728: Does the charge cover the current date.
                        // We don't want to include historic active or future active charges.
                        $date = date('Y-m-d');

                        // Have we got a regular payment start date.
                        if ($activeRent->regular_payment_start_date) {
                            // Do we have both regular payment start date and end date
                            if ($activeRent->regular_payment_start_date && $activeRent->regular_payment_end_date) {
                                $regStartDate = $activeRent->regular_payment_start_date;
                                $regEndDate = $activeRent->regular_payment_end_date;
                            } else {
                                // Only start date so need to cater for 'further notice'
                                $regStartDate = $activeRent->regular_payment_start_date;
                                // Just set end date to today to satisify the date range logic below.
                                $regEndDate = $date;
                            }
                        } else {
                            // use charge start and end date instead.
                            $regStartDate = $activeRent->est_charge_start_date;
                            $regEndDate = $activeRent->est_charge_end_date;
                            // If we have no reg start date we need to check that its a manual payment that is realy
                            // until further notice.  Can't rely on value from query.
                            $calEndPoint = $this->getEndPoint($activeRent->est_charge_id);
                            if ($calEndPoint == EstChargeCalcEndPoint::FURTHER_NOTICE) {
                                // Just set end date to today to satisify the date range logic below.
                                $regEndDate = $date;
                            }
                        }

                        if ($date >= $regStartDate && $date <= $regEndDate) {
                            $sum += $activeRent->annualised_rent;
                            if (!is_null($activeRent->tax_code_id)) {
                                $vat = $vat + ($activeRent->tax_rate / 100 * $activeRent->annualised_rent);
                            }
                        }
                    }
                }
                $leaseIn->annualRentNet = $sum;
                $leaseIn->annualVAT = $vat;

                $leaseIn->chargesRents = $this->getLeaseInCharges($leaseIn->lease_in_id)
                    ->where('est_charge.active', CommonConstant::DATABASE_VALUE_YES)
                    ->where('est_charge.credit_note', CommonConstant::DATABASE_VALUE_NO)
                    ->orderBy('est_charge.rent', 'DESC')
                    ->orderBy('est_charge.regular_payment_start_date', 'ASC')
                    ->get();
                if ($leaseIn->chargesRents->count() > 0) {
                    $leaseIn->chargesRents->startDate = $firstDateOfCurrentMonth->format(CommonConstant::FORMAT_DATE);
                    $leaseIn->chargesRents->endDate = $nextYearDate;
                }
                $allCharges = $this->getTotalCharge($leaseIn, $nextYearDate);
                $leaseIn->allCharges = $allCharges[0];
                $leaseIn->allChargesTotalGross = $allCharges[1];

                $leaseIn->agreements = $this->getAgreements($leaseIn->lease_in_id)->get();

                $leaseIn->rentReviews = $this->leaseInService->getLeaseInReviews($leaseIn->lease_in_id, $inputs)
                    ->where('est_review_type.financial_review', CommonConstant::DATABASE_VALUE_YES)
                    ->get();

                // Only get general review in current year and the next 2 (e.g., 2017 2018 2019).
                $thisYear = new DateTime(date('1-1-Y'));
                $endYear = clone $thisYear;
                $endYear->add(\DateInterval::createFromDateString('3 year'));
                $endYear->sub(\DateInterval::createFromDateString('1 day'));
                $reviewStartDate = Common::dateFieldToSqlFormat(
                    $thisYear->format(CommonConstant::FORMAT_DATE),
                    ['includeTime' => false]
                );
                $reviewEndDate = Common::dateFieldToSqlFormat(
                    $endYear->format(CommonConstant::FORMAT_DATE),
                    ['includeTime' => false]
                );
                $leaseIn->reviews = $this->leaseInService->getLeaseInReviews($leaseIn->lease_in_id, $inputs)
                    ->where('est_review_type.financial_review', CommonConstant::DATABASE_VALUE_NO)
                    ->where('est_review.review_date', '>=', $reviewStartDate)
                    ->where('est_review.review_date', '<=', $reviewEndDate)
                    ->get();

                // Rolling Break Dates.
                $bdInputs['est_break_parent_type_id'] = EstBreakDateParentType::EST_BD_PARENT_LI;
                $bdInputs['parent_id'] = $leaseIn->lease_in_id;

                $leaseIn->breakDates = $this->estBreakDateService->getAll($bdInputs)->get();
            }
        }

        return $leaseIns;
    }

    private function getEndPoint($id)
    {
        $paymentSummary = new PaymentSummary();

        $estChargeService = new EstChargeService(new PermissionService());

        $estCharge = $estChargeService->get($id);

        $paymentSummary->initialAmount = $estCharge->initialPaymentAmount;
        $paymentSummary->initialDate = $estCharge->initialDate;
        $paymentSummary->daysBefore = $estCharge->daysBefore;
        $paymentSummary->initialDays = $estCharge->initialPeriodDays;
        $paymentSummary->finalAmount = $estCharge->finalPaymentAmount;
        $paymentSummary->finalDate = $estCharge->finalDate;
        $paymentSummary->daysAfter = $estCharge->daysAfter;
        $paymentSummary->finalDays = $estCharge->finalPeriodDays;
        $paymentSummary->ptAdvance = $estCharge->rpAdvance;
        $paymentSummary->rpPeriodFreq = $estCharge->rpPeriodFreq;
        $paymentSummary->rpTimePeriodId = $estCharge->rpTimePeriod;
        $paymentSummary->actualInstalmentAmount = $estCharge->actualInstalmentAmount;
        $paymentSummary->anniversaryPayment = $estCharge->anniversaryPayment;
        $paymentSummary->anniversary = $estCharge->anniversary;
        $paymentSummary->ptCalcEndPoint = $estCharge->ptCalcEndPoint;
        $paymentSummary->periods = $estCharge->periods;
        $paymentSummary->paymentDate = $estCharge->paymentStartDate;
        $paymentSummary->lastPaymentDate = $estCharge->lastRegPaymentDate;
        $paymentSummary->luFromAnnualRent = $estCharge->luOriginalAnnualRent;
        $paymentSummary->luToAnnualRent = $estCharge->luAnnualRent;

        $paymentSummary->getSummmary(true);

        return $paymentSummary->ptCalcEndPoint;
    }
}
