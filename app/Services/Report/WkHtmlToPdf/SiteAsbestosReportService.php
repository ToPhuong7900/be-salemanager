<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use Tfcloud\Lib\BoxFilterQuery\FilterQueryBootstrap;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Exceptions\PermissionException;
use Tfcloud\Models\AsbHazard;
use Tfcloud\Models\AsbHazardStatusType;
use Tfcloud\Models\AsbRiskStatus;
use Tfcloud\Models\AsbSurveyStatus;
use Tfcloud\Models\Building;
use Tfcloud\Models\CrudAccessLevel;
use Tfcloud\Models\Filter\FilterName;
use Tfcloud\Models\Module;
use Tfcloud\Services\PermissionService;

class SiteAsbestosReportService extends WkHtmlToPdfReportBaseService
{
    private $hazards = [];
    private $totalHazards = 0;
    private $buildingCount = 0 ;
    private $siteText = "";
    private $filterQuery = null;

    public function __construct(PermissionService $permissionService, $inputs = [])
    {
        $this->permissionService = $permissionService;
        $this->filterQuery = FilterQueryBootstrap::getFilterQuery(FilterName::PROP_SITE_ASBESTOS_REGISTER, $inputs);
    }

    public function runReportSingleSite($site, $reportId)
    {
        //call report from button not report view
        $inputs = [];
        $inputs['site_id'] = $site->site_id;
        $inputs['singleOnly'] = true;

        return $this->runReport(
            $this->getTempFile(),
            $inputs,
            $reportId
        );
    }

    public function runReportSingleBuilding(Building $building, $reportId)
    {
        //call report from button not report view
        $inputs = [];
        $inputs['site_id'] = $building->site_id;
        $inputs['building_id'] = $building->building_id;
        $inputs['singleOnly'] = true;

        return $this->runReport(
            $this->getTempFile(),
            $inputs,
            $reportId
        );
    }

    public function runReport($repOutPutPdfFile, $inputs, $repId)
    {
        $siteId = array_get($inputs, 'site_id');
        $buildingId = array_get($inputs, 'building_id');
        $this->initialData($siteId, $buildingId);
        $footerDate = new \DateTime();

        $data = [];
        $data['site_text'] = $this->siteText;
        $data['total_hazards'] = $this->totalHazards;

        if (!empty($this->hazards)) {
            // Create wkhtmltopdf temp folder to store generated files
            $generatedPath = $this->generateWkhtmltopdfTempPath($repId);

            $header = \View::make(
                'reports.wkhtmltopdf.asbestos.siteRegister.header',
                [
                    'reportLogo' => $this->getHeaderLogo(),
                    'title'      => "Asbestos Register: " . $this->siteText
                ]
            )->render();

            $headerPath = $generatedPath . "/header.html";
            file_put_contents($headerPath, $header, LOCK_EX);

            $footer = \View::make(
                'reports.wkhtmltopdf.asbestos.siteRegister.footer',
                [
                    'footerDate' => $footerDate->format('Y/m/d H:i'),
                    'siteCode'   => $this->siteText,
                    'user'       => \Auth::user()->display_name
                ]
            )->render();
            $footerPath = $generatedPath . "/footer.html";
            file_put_contents($footerPath, $footer, LOCK_EX);

            $content = \View::make(
                'reports.wkhtmltopdf.asbestos.siteRegister.asbHazardList',
                [
                    'data'         => $data,
                    'hazards'      => $this->hazards,
                    'bldgCount'    => $this->buildingCount
                ]
            )->render();

            $contentPath = $generatedPath . "/asbHazardList.html";

            file_put_contents($contentPath, $content, LOCK_EX);

            $options = [
                'header-html' => $headerPath,
                'footer-html' => $footerPath,
                'header-spacing' => 4,
                'footer-spacing' => 4,
                'orientation' => 'landscape',
            ];

            if (array_key_exists('singleOnly', $inputs)) {
                $repOutPutPdfFile .= '.pdf';
                $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $options);
                return $repOutPutPdfFile;
            } else {
                $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $options);
                return true;
            }
        }
        return false;
    }

    public function validateFilter($inputs)
    {
        return \Validator::make($inputs, $this->validateRules(), $this->validateMessages());
    }

    public function validateRules()
    {
        $rules = [
            'site_id' => ['required'],
            'building_id' => ['required_with:room_id'],
        ];

        return $rules;
    }

    public function validateMessages()
    {
        return [
            'site_id.required' => 'Location is required.'
        ];
    }

    private function getTempFile()
    {
        return tempnam(Common::getTempFolder(), "SITE_ASB_REG");
    }

    private function initialData($siteId = false, $buildingId = false)
    {
        if (
            !$this->permissionService->hasModuleAccess(
                Module::MODULE_ASBESTOS,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
            )
        ) {
            $msg = "Not met minimum readonly access for asbestos module in class" .
                " (" . __CLASS__ . ") method (" . __METHOD__ . "). " .
                " See line (" . __LINE__ . ")";
            throw new PermissionException($msg);
        }

        $selects = [
            "asb_survey.asb_survey_id",
            "asb_hazard.asb_hazard_id",
            "asb_hazard.building_id",
            "asb_hazard.room_id",
            "asb_hazard.asb_hazard_status_id",
            "asb_hazard.asb_hazard_code",
            "asb_hazard.asb_hazard_desc",
            "asb_hazard.asb_hazard_position",
            "asb_hazard.asb_hazard_quantity",
            "asb_hazard.unit_of_measure_id",
            "asb_hazard.asb_asbestos_type_id",
            "asb_hazard.asb_sample_requirement_id",
            \DB::raw('DATE_FORMAT(asb_hazard.asb_hazard_identified_date, "%d/%m/%Y") AS asb_hazard_identified_date'),
            \DB::raw('DATE_FORMAT(asb_hazard.asb_hazard_removal_date, "%d/%m/%Y") AS asb_hazard_removal_date'),
            "asb_sample.asb_sample_code",
            "asb_risk_assessment.asb_risk_assessment_id",
            "asb_risk_assessment.total_material_score",
            "asb_risk_assessment.total_priority_score",
            "asb_risk_assessment.total_overall_score",
            "asb_risk_assessment.extent_damage_sid",
            \DB::raw(
                "CASE asb_risk_assessment.extent_damage_sid
		WHEN 4
                THEN 'Good'
                WHEN 5
                THEN 'Low Damage'
                WHEN 6
                THEN 'Medium Damage'
                WHEN 7
                THEN 'High Damage'
		END AS sample_condition"
            ),
            "asb_survey.site_id",
            "asb_survey.asb_survey_id",
            "asb_survey.asb_survey_code",
            "asb_survey.survey_status_id",
            \DB::raw('DATE_FORMAT(asb_survey.asb_survey_date, "%d/%m/%Y") AS asb_survey_date'),
            "site.site_code",
            "site.site_desc",
            \DB::raw("CONCAT(site.site_code, ' - ', site.site_desc) AS site_text"),
            "building.building_code",
            "building.building_desc",
            \DB::raw("CONCAT(building.building_code, ' - ', building.building_desc) AS building_text"),
            "room.room_number",
            "room.room_desc",
            \DB::raw("CONCAT(room.room_number,  ' - ', room.room_desc) AS room_text"),
            "asb_hazard_status.asb_hazard_status_code",
            "unit_of_measure.unit_of_measure_code",
            \DB::raw("CONCAT(asb_hazard.asb_hazard_quantity, ' ', unit_of_measure.unit_of_measure_code) AS unit_text"),
            "asb_asbestos_type.asb_asbestos_type_code",
            "asb_overall_risk.overall_risk_code"
        ];

        $query = AsbHazard
            ::join('asb_survey_hazard', 'asb_survey_hazard.asb_hazard_id', '=', 'asb_hazard.asb_hazard_id')
            ->join('asb_survey', 'asb_survey.asb_survey_id', '=', 'asb_survey_hazard.asb_survey_id')
            ->join(
                'asb_hazard_status',
                'asb_hazard_status.asb_hazard_status_id',
                '=',
                'asb_hazard.asb_hazard_status_id'
            )
            ->join('site', 'site.site_id', '=', 'asb_hazard.site_id')
            ->leftJoin('building', 'building.building_id', '=', 'asb_hazard.building_id')
            ->leftJoin('room', 'room.room_id', '=', 'asb_hazard.room_id')
            ->leftJoin('plant', 'plant.plant_id', '=', 'asb_hazard.plant_id')
            ->leftJoin('plant_type', 'plant_type.plant_type_id', '=', 'plant.plant_type_id')
            ->leftJoin('plant_subgroup', 'plant_subgroup.plant_subgroup_id', '=', 'plant.plant_subgroup_id')
            ->leftJoin('plant_group', 'plant_group.plant_group_id', '=', 'plant.plant_group_id')
            ->leftJoin('contact', 'contact.contact_id', '=', 'asb_hazard.identifier_id')
            ->leftJoin(
                'asb_material_type',
                'asb_material_type.asb_material_type_id',
                '=',
                'asb_hazard.asb_material_type_id'
            )
            ->leftJoin(
                'asb_material_subtype',
                'asb_material_subtype.asb_material_subtype_id',
                '=',
                'asb_hazard.asb_material_subtype_id'
            )
            ->leftJoin('unit_of_measure', 'unit_of_measure.unit_of_measure_id', '=', 'asb_hazard.unit_of_measure_id')
            ->leftJoin(
                'asb_asbestos_type',
                'asb_asbestos_type.asb_asbestos_type_id',
                '=',
                'asb_hazard.asb_asbestos_type_id'
            )
            ->leftJoin(
                'asb_sample_requirement',
                'asb_sample_requirement.asb_sample_requirement_id',
                '=',
                'asb_hazard.asb_sample_requirement_id'
            )
            ->leftJoin('asb_sample', 'asb_sample.asb_sample_id', '=', 'asb_hazard.asb_sample_id')
            ->leftJoin('asb_risk_assessment', function ($join) {
                $join->on('asb_hazard.asb_hazard_id', '=', 'asb_risk_assessment.asb_hazard_id')
                    ->where('asb_risk_assessment.asb_risk_status_id', '=', AsbRiskStatus::CURRENT);
            })
            ->leftJoin('asb_overall_risk', function ($join) {
                $join->on('asb_overall_risk.from_score', '<=', 'asb_risk_assessment.total_overall_score')
                    ->on('asb_overall_risk.to_less_than_score', '>', 'asb_risk_assessment.total_overall_score');
            });

        $query->where('site.site_id', $siteId);

        $this->filterQuery->filterAll($query);

        if ($buildingId) {
            $query->where('building.building_id', $buildingId);
        }

        $query->where('asb_survey.survey_status_id', AsbSurveyStatus::CURRENT);

        /** Exclude removed asbestos from the list view */
        if (!\Auth::user()->siteGroup->includeRemovedAsbestos()) {
            $query->where('asb_hazard_status.asb_hazard_status_type_id', '!=', AsbHazardStatusType::REMOVED);
        }

        $query->orderBy('building.building_code')->orderBy('room.room_number')
            ->groupBy('asb_hazard.asb_hazard_id')
            ->select($selects);

        $result = $query->get();

        $currentBuildingId = false;
        $bldgHazCount = 0;
        $this->buildingCount = 0;

        foreach ($result as $row) {
            $this->siteText     = $row->site_text;

            if ($row->asb_risk_assessment_id) {
                $row->total_material_score_text =
                    is_numeric($row->total_material_score) ? $row->total_material_score : 'N/A - incomplete';
                $row->total_priority_score_text =
                    is_numeric($row->total_priority_score) ? $row->total_priority_score : 'N/A - incomplete';
                if (!is_numeric($row->total_overall_score)) {
                    $row->overall_risk_code = 'N/A';
                }
            } else {
                $row->total_material_score_text = '';
                $row->total_priority_score_text = '';
                $row->overall_risk_code = '';
            }

            if ($currentBuildingId !== $row->building_id) {
                ++$this->buildingCount;
                if ($bldgHazCount > 0) {
                    $this->hazards[$currentBuildingId]['bldg_haz_count'] = $bldgHazCount;
                }
                $bldgHazCount = 0;

                $buildingHazards = [];
                $buildingHazards['site_text'] = $row->site_text;
                $buildingHazards['building_text'] = $row->building_text;
                $buildingHazards['haz_recs'] = [];
                $currentBuildingId = $row->building_id;
                $this->hazards[$currentBuildingId] = $buildingHazards;
            }
            array_push($this->hazards[$currentBuildingId]['haz_recs'], $row->toArray());
            ++$bldgHazCount;
            ++$this->totalHazards;
        }

        $this->hazards[$currentBuildingId]['bldg_haz_count'] = $bldgHazCount;
    }
}
