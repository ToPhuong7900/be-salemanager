<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use Tfcloud\Lib\BoxFilterQuery\IReportFilterQuery;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Models\ProjectStatusType;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Project\ProjectActionService;
use Tfcloud\Services\Project\ProjectService;

class ProjectActionsWithNotesReportService extends WkHtmlToPdfReportBaseService
{
    private $projectService;
    private $projectActionService;
    public $filterText = '';
    protected $reportBoxFilterLoader;

    public function __construct(PermissionService $permissionService, $report)
    {
        parent::__construct($permissionService);
        $this->projectService = new ProjectService($permissionService);
        $this->projectActionService = new ProjectActionService($permissionService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function runReport($repOutPutPdfFile, $inputs, $repId)
    {
        $reportFilterQuery = null;
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $projects = $reportFilterQuery->filterAll($this->projectActionService->all())->groupBy('project_id')->get();
            $projectActions = $reportFilterQuery->filterAll($this->projectActionService->all())->get();
            $privacyFilter = $reportFilterQuery->getInterNoteFilter();
        } else {
            $this->formatInputData($inputs);
            $projects = $this->projectActionService->getAll($inputs)->groupBy('project_id')->get();
            $projectActions = $this->projectActionService->getAll($inputs)->get();
            $privacyFilter = array_get($inputs, 'privacy');
        }

        $projectActionNotes = [];
        $noteFilterInputs = [
            'privacy' => $privacyFilter
        ];

        foreach ($projectActions as $projectAction) {
            $notes = $this->projectActionService->showNotes($projectAction->project_action_id, $noteFilterInputs)
                ->get();
            if ($notes->count()) {
                $projectActionNotes = array_add($projectActionNotes, $projectAction->project_action_id, $notes);
            }
        }

        // Create wkhtmltopdf temp folder to store generated files
        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);

        $header = \View::make('reports.wkhtmltopdf.project.pprj02.header')->render();
        $headerPath = $generatedPath . "/header.html";
        file_put_contents($headerPath, $header, LOCK_EX);

        $footer = \View::make('reports.wkhtmltopdf.project.pprj02.footer')->render();
        $footerPath = $generatedPath . "/footer.html";
        file_put_contents($footerPath, $footer, LOCK_EX);

        $content = \View::make(
            'reports.wkhtmltopdf.project.pprj02.content',
            ['projects' => $projects, 'projectActions' => $projectActions, 'projectActionNotes' => $projectActionNotes]
        )->render();
        $contentPath = $generatedPath . "/content.html";
        file_put_contents($contentPath, $content, LOCK_EX);

        $options = [
            'header-html' => $headerPath,
            'footer-html' => $footerPath,
            'header-spacing' => 4,
            'footer-spacing' => 4,
            'orientation' => 'landscape',
        ];

        $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $options);

        $whereCodes = [];               // The codes which have been used in the filter
        $whereTexts = [];               // Plain text about filtering applied
        $orCodes = [];                  // csv string of OR codes
        $bFilterDetail = false;

        if ($reportFilterQuery instanceof IReportFilterQuery) {
            $this->filterText = $reportFilterQuery->getFilterDetailText();
        } else {
            $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
            $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $this->filterText);
        }

        return true;
    }

    private function formatInputData(&$inputs)
    {
        foreach ($inputs as $key => $value) {
            if ($value === "") {
                unset($inputs[$key]);
            }
        }
    }

    private function generateFilterData($filterData, &$whereCodes, &$whereTexts, &$orCodes)
    {
        if ($val = array_get($filterData, 'nextReviewFrom', null)) {
            array_push($whereTexts, "Next Review From '" . $val . "'");
        }

        if ($val = array_get($filterData, 'nextReviewTo', null)) {
            array_push($whereTexts, "Next Review To '" . $val . "'");
        }

        // filter action
        if ($val = array_get($filterData, 'completed', null)) {
            $text = '';
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_NO:
                    $text = \Lang::get('text.no');
                    break;
                case CommonConstant::DATABASE_VALUE_YES:
                    $text = \Lang::get('text.yes');
                    break;
                default:
                    break;
            }
            if (! empty($text)) {
                array_push($whereTexts, "Completed = '" . $text . "'");
            }
        }

        if ($val = array_get($filterData, 'project')) {
            array_push(
                $whereCodes,
                ['project', 'project_id', 'project_code', $val, 'Project']
            );
        }

        if ($val = array_get($filterData, 'project_status_id')) {
            array_push(
                $whereCodes,
                ['project_status', 'project_status_id', 'project_status_code', $val, 'Project Status']
            );
        }

        $projectStatusTypes = [];
        if (Common::iset($filterData['draft'])) {
            $projectStatusTypes[] = ProjectStatusType::DRAFT;
        }
        if (Common::iset($filterData['active'])) {
            $projectStatusTypes[] = ProjectStatusType::ACTIVE;
        }
        if (Common::iset($filterData['complete'])) {
            $projectStatusTypes[] = ProjectStatusType::COMPLETE;
        }
        if (Common::iset($filterData['rejected'])) {
            $projectStatusTypes[] = ProjectStatusType::REJECTED;
        }
        if (count($projectStatusTypes)) {
            array_push($orCodes, [
                'project_status_type',
                'project_status_type_id',
                'project_status_type_code',
                implode(',', $projectStatusTypes),
                "Project Status Type"
            ]);
        }

        if ($val = array_get($filterData, 'owner')) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Owner']);
        }

        if ($val = Common::iset($filterData['group'])) {
            array_push(
                $whereCodes,
                [
                    'project_action_group',
                    'project_action_group_id',
                    'project_action_group_code',
                    $val,
                    \Form::fieldLang('group_action')
                ]
            );
        }

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Code contains '{$filterData['code']}'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Description contains '{$filterData['description']}'");
        }

        if ($siteId = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $siteId, "Site Code"]);

            if ($buildingId = Common::iset($filterData['building_id'])) {
                array_push(
                    $whereCodes,
                    ['building', 'building_id', 'building_code', $buildingId, "Building Code"]
                );
            }
        }

        if ($val = array_get($filterData, 'establishment')) {
            array_push(
                $whereCodes,
                ['establishment', 'establishment_id', 'establishment_code', $val, 'Establishment']
            );
        }

        if ($val = array_get($filterData, 'privacy')) {
            array_push($whereTexts, "Private Notes = '" . $val . "'");
        }
    }
}
