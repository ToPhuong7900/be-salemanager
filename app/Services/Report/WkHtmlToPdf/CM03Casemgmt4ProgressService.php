<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use Tfcloud\Models\CaseManagement\CmType;
use Tfcloud\Models\GenTable;

class CM03Casemgmt4ProgressService extends BaseCMx03CaseProgressService
{
    protected $cmType = CmType::CM_TYPE_CASEMGMT4;
    protected $gentTable = GenTable::CM4_CASE;
}
