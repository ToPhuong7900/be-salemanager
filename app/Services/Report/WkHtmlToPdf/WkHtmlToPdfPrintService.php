<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Exceptions\StateException;
use Tfcloud\Lib\File\FileCommon;
use Tfcloud\Models\BaseModel;
use Tfcloud\Services\PermissionService;

class WkHtmlToPdfPrintService extends WkHtmlToPdfReportBaseService
{
    public $title;
    public $headerString;

    public $tempPath;

    public $repPdfFile;

    public $options;

    protected $reportFileName;

    protected $tableName;
    protected $recordKey;

    protected $permissionService;

    private const PDF_HEADER_LOGO_WIDTH = 40;
    private const PDF_HEADER_LOGO_HEIGHT = 20;

    public function __construct(PermissionService $permissionService, $modelTable, $reportFileName, $options = [])
    {
        $this->permissionService = $permissionService;
        $this->reportFileName = $reportFileName;
        if ($modelTable instanceof BaseModel) {
            $this->tableName = $modelTable->getTable();
            $this->recordKey = $modelTable->getKey();
        } elseif ($modelTable && $recordKey = array_get($options, 'report-key')) {
            $this->tableName = $modelTable;
            $this->recordKey = $recordKey;
            unset($options['report-key']);
        } else {
            throw new StateException('Invalid input');
        }

        $this->generateTempPath();
        $this->generatePdfFile();
        $this->options = $options;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function setHeaderString($headerString)
    {
        $this->headerString = $headerString;
    }

    public function generatePdfContent($htmlTemplate, $data, $extraOptions = [])
    {
        // Get generated report options.
        $this->getOptions($extraOptions);

        $data['fontSize'] = array_get($this->options, 'font-size', '10pt');
        $data['fontFamily'] = array_get($this->options, 'font-family', 'Helvetica');
        unset($this->options['font-size']);
        unset($this->options['font-family']);

        // Generate file content.
        $content = \View::make($htmlTemplate, $data)->render();
        $contentPath = $this->tempPath . "/content.html";
        file_put_contents($contentPath, $content, LOCK_EX);
        $this->generateSingleHtmlToPdf($contentPath, $this->repPdfFile, $this->options);
        return $this->repPdfFile;
    }

    public function getOptions($extraOptions = [])
    {
        $isHeader = array_get($this->options, 'header-html', true);
        $isFooter = array_get($this->options, 'footer-html', true);

        // Get generated report options.
        if (!array_get($this->options, 'header-spacing')) {
            $this->options['header-spacing'] = 5;
        }

        if (!array_get($this->options, 'footer-spacing')) {
            $this->options['footer-spacing'] = 5;
        }

        if (!array_get($this->options, 'orientation')) {
            $this->options['orientation'] = 'portrait';
        }

        if ($isHeader) {
            $this->generateHeader($extraOptions);
        }

        if ($isFooter) {
            $this->generateFooter($extraOptions);
        }
    }

    public function setReportFileName($reportFileName)
    {
        $this->reportFileName = $reportFileName;
    }

    public function setOptions($options = [])
    {
        $this->options = $options;
    }

    protected function generateHeader($options = [])
    {
        $siteGroupReportLogo = "reportLogo.jpg";
        $siteGroupReportLogoPath = \Auth::user()->site_group_id . DIRECTORY_SEPARATOR;
        $siteGroupReportLogoPath .= "sitegroup" . DIRECTORY_SEPARATOR . $siteGroupReportLogo;
        $legacyPath = \Config::get('cloud.legacy_paths.uploads');
        $reportLogo = $legacyPath . DIRECTORY_SEPARATOR . $siteGroupReportLogoPath;
        $headerLogo = public_path() . '/assets/images/tfcloud-banner.jpg';
        list($width, $height) = getimagesize($headerLogo);
        $logoHeight = self::PDF_HEADER_LOGO_WIDTH * $height / $width;
        $logoWidth = self::PDF_HEADER_LOGO_WIDTH;
        if (file_exists($reportLogo)) {
            $restrictImgRatio = array_get($options, 'restrict-width-report-logo-by-ratio', true);
            list($width, $height) = getimagesize($reportLogo);
            $newHeight = self::PDF_HEADER_LOGO_HEIGHT;
            $newWidth = $newHeight * $width / $height;
            $logoHeight = $newHeight;
            $logoWidth = $newWidth;
            if ($restrictImgRatio) {
                $imgRatio = $width / $height;
                if ($imgRatio >= 2) {
                    $logoWidth = self::PDF_HEADER_LOGO_WIDTH;
                } else {
                    $logoWidth = $newWidth;
                }
            }
            // Create a temporary file and copy the file across onto this machine
            // This is the only way the pdf tool can cope with it.
            $tmpBannerFile = tempnam(Common::getTempFolder(), 'rlg');
            copy($reportLogo, $tmpBannerFile);
            $headerLogo = $tmpBannerFile;
        }

        if ($this->options == 'portrait') {
            $col1Width = "45";
            $col2Width = "135";
        } else {
            $col1Width = "45";
            $col2Width = "220";
        }

        //header-string
        $headerString = \Auth::user()->siteGroup->description;

        if (array_get($options, 'header-string', false)) {
            $headerString = $this->headerString;
        }

        $header = \View::make('reports.wkhtmltopdf._header', [
            'title' => $this->title,
            'headerString' => $headerString,
            'logo' => $headerLogo,
            'logoHeight' => $logoHeight,
            'logoWidth' => $logoWidth,
            'col1Width' => $col1Width,
            'col2Width' => $col2Width
        ])->render();
        $headerPath = $this->tempPath . "/header.html";
        file_put_contents($headerPath, $header, LOCK_EX);
        $this->options['header-html'] = $headerPath;
    }

    protected function generateFooter($extraOptions = [])
    {
        $footerText = "Report generated " . date("d/m/Y H:i");
        $footerText .= " by " . \Auth::user()->display_name;
        $footer = \View::make('reports.wkhtmltopdf._footer', [
            'footerText' => $footerText,
            'extraOptions' => $extraOptions
        ])->render();
        $footerPath = $this->tempPath . "/footer.html";
        file_put_contents($footerPath, $footer, LOCK_EX);
        $this->options['footer-html'] = $footerPath;
    }

    public function generatePdfFile()
    {
        // Create a reports directory for the site group
        $reportDir = storage_path() . '/reports/' . \Auth::User()->site_group_id;
        if (!is_dir($reportDir)) {
            mkdir($reportDir, 0755);
        }
        // Create a reports directory for the userid
        $reportDir .= '/' . \Auth::User()->id;
        if (!is_dir($reportDir)) {
            mkdir($reportDir, 0755);
        }

        $reportDir = $reportDir . '/' . $this->tableName;
        if (!is_dir($reportDir)) {
            mkdir($reportDir, 0755);
        }

        $reportDir = $reportDir . '/' . $this->recordKey;
        if (!is_dir($reportDir)) {
            mkdir($reportDir, 0755);
        }

        $reportFileName = FileCommon::parseName($this->reportFileName);

        $this->repPdfFile = $reportDir . '/' . $reportFileName . '.pdf';
    }

    protected function generateTempPath()
    {
        $generatedPath = \Config::get('cloud.legacy_paths.generated');
        if (!file_exists($generatedPath)) {
            mkdir($generatedPath, 0755);
        }
        $generatedPath = $generatedPath . '/' .
            \Auth::User()->site_group_id;
        if (!file_exists($generatedPath)) {
            mkdir($generatedPath, 0755);
        }
        $generatedPath = $generatedPath . '/' . \Auth::User()->id;
        if (!file_exists($generatedPath)) {
            mkdir($generatedPath, 0755);
        }
        $generatedPath = $generatedPath . '/' . \Config::get('cloud.wkhtmltopdf.wkhtmltopdf_temp_folder');
        if (!file_exists($generatedPath)) {
            mkdir($generatedPath, 0755);
        }

        $generatedPath = $generatedPath . '/' . $this->tableName;
        if (!file_exists($generatedPath)) {
            mkdir($generatedPath, 0755);
        }

        $generatedPath = $generatedPath . '/' . $this->recordKey;
        if (!file_exists($generatedPath)) {
            mkdir($generatedPath, 0755);
        } else {
            /*
            * Delete all old files in wkhtmltopdf folder
            * I've amended this code as we were having issues with files not
            * unlinking due to 'resource temporarily unavailable'. For each filename,
            * attempt the unlink and supress the error if we cannot unlink. If this happens,
            * wait one second and clear the status cache, check if the file still exists and if
            * so, try up to 9 more times to unlink. JC. CLD-10150.
            * */
            foreach (glob("$generatedPath/*.*") as $filename) {
                $gone = false;
                for ($trial = 0; $trial < 10; $trial++) {
                    if ($gone = @unlink($filename)) {
                        break;
                    }
                    usleep(1000000);
                    clearstatcache();
                    if (!file_exists($filename)) {
                        $gone = true;
                        break;
                    }
                }
            }
        }
        $this->tempPath = $generatedPath;
    }
}
