<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use Carbon\Carbon;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Models\Helpcall;
use Tfcloud\Models\HelpcallStatusType;
use Tfcloud\Models\InspectionSFG20\InspSFG20InspectionStatusType;
use Tfcloud\Models\InstructionParentType;
use Tfcloud\Models\InstructionStatusType;
use Tfcloud\Models\NoteRecordType;
use Tfcloud\Models\Report;
use Tfcloud\Models\Site;
use Tfcloud\Services\PermissionService;
use Tfcloud\Models\Inspection\Inspection;

class PRCLICEC01PropertyReportService extends WkHtmlToPdfReportBaseService
{
    private const INSPECTION_GROUP_CODE = 'SFG';   // inspections are limited to this group code

    private $pageIndex = 1;
    private $todayDbFormat;
    private $todayReportFormat;
    private $fromDate;
    private $toDate;
    private $fromDateReportFormat;
    private $toDateReportFormat;
    private $siteId;
    public $filterText = '';

    public function __construct(PermissionService $permissionService, Report $report = null, $inputs = [])
    {
        parent::__construct($permissionService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
        $this->todayDbFormat = Carbon::today()->format('Y-m-d');
        $this->todayReportFormat = Carbon::today()->format('d-m-Y');
    }

    public function validateFilter($inputs)
    {
        return \Validator::make($this->formatInputs($inputs), $this->validateRules(), $this->validateMessages());
    }

    private function formatInputs($inputs)
    {
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);

            $siteId = $reportFilterQuery->getValueFilterField('site_id');
            $this->siteId = $siteId[0] ?? null;

            $date = $reportFilterQuery->getValueFilterField('date');

            $this->fromDate = empty($date) ? null : $date[0][0];
            $this->toDate = empty($date) ? null : $date[0][1];

            $this->fromDateReportFormat = is_null($this->fromDate) ? '' : $this->dateReportFormat($this->fromDate);
            $this->toDateReportFormat = is_null($this->toDate) ? '' : $this->dateReportFormat($this->toDate);

            $inputs['site'] = $siteId;
            $inputs['fromDate'] = empty($date) ? null : $this->fromDate;
            $inputs['toDate'] = empty($date) ? null : $this->toDate;
        }
        return $inputs;
    }

    public function validateRules()
    {
        \Validator::extend('single_site', function ($attribute, $value, $parameters) {
            if (is_array($value)) {
                return count($value) == 1;
            }
            return !empty($value);
        });

        $rules = [
            'site' => ['single_site'],
            'fromDate' => ['required', 'date_format:Y-m-d', 'before_or_equal:' . $this->todayDbFormat],
            'toDate' => ['required', 'date_format:Y-m-d', 'after_or_equal:' . $this->todayDbFormat],
        ];

        return $rules;
    }

    public function validateMessages()
    {
        $messages = [
            'site.single_site' => 'Select a single site to run this report.',
            'fromDate.required' => 'From date is required.',
            'fromDate.date' => 'From date must be a valid date.',
            'fromDate.before_or_equal' => 'From date must not be in the future.',
            'toDate.required' => 'To date is required.',
            'toDate.date' => 'To date must be a valid date.',
            'toDate.after_or_equal' => 'To date must not be in the past.',
        ];

        return $messages;
    }

    public function runReport($repPdfFile, $inputs, $repId, $paramOptions = [])
    {
        $data = $this->getReportData($inputs);

        if ($data === false) {
            return false;
        }

        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);

        $header = \View::make('reports.wkhtmltopdf.property.prclicec01.header', $data)->render();
        $headerPath = $generatedPath . "/header.html";
        file_put_contents($headerPath, $header, LOCK_EX);

        $footer = \View::make('reports.wkhtmltopdf.property.prclicec01.footer', $data)->render();
        $footerFile = $generatedPath . "/footer.html";
        file_put_contents($footerFile, $footer, LOCK_EX);

        $summary = \View::make('reports.wkhtmltopdf.property.prclicec01.summary', $data)->render();
        $summaryFile = $generatedPath . '/summary.html';
        file_put_contents($summaryFile, $summary, LOCK_EX);

        $reactive = \View::make('reports.wkhtmltopdf.property.prclicec01.reactive', $data)->render();
        $reactiveFile = $generatedPath . '/reactive.html';
        file_put_contents($reactiveFile, $reactive, LOCK_EX);

        $ppm = \View::make('reports.wkhtmltopdf.property.prclicec01.ppm', $data)->render();
        $ppmFile = $generatedPath . '/ppm.html';
        file_put_contents($ppmFile, $ppm, LOCK_EX);

        $options = [
            'header-html'   => $header,
            'footer-html'   => $footer,
            'orientation'   => 'landscape',
            'margin-top'    => 32,
            'margin-bottom' => 15,
            'margin-left'   => 12,
            'margin-right'  => 12,
        ];

        $files = [$summaryFile];

        if ($data['hasReactiveData']) {
            $files[] = $reactiveFile;
        }

        if ($data['hasPpmData']) {
            $files[] = $ppmFile;
        }

        $arrPdfs = $this->generateMultiHtmlToPdf(
            $files,
            $generatedPath,
            $repPdfFile,
            $options,
            $this->pageIndex,
            false
        );

        $this->mergerPdf($arrPdfs, $repPdfFile, ['isPaging' => false]);

        return true;
    }

    private function getReportData($inputs)
    {
        $data = [];

        $inputs = $this->formatInputs($inputs);

        $site = $this->getSite();

        if (!$site) {
            return false;
        }

        $data['headerLogo'] = $this->getHeaderLogo();

        $data['date'] = $this->todayReportFormat;

        $data['repTitle'] = $inputs['repTitle'] ?? 'PR-CLI-CEC01: Site FM Report';

        $data['username'] = \Auth::User()->username;

        $data['sitePhotoPath'] = $site->photoPath();

        $data['fromDate'] = $this->fromDateReportFormat;

        $data['toDate'] = $this->toDateReportFormat;

        $data['site'] = $site;

        $helpcalls = $this->getHelpcalls();

        $helpcallsGrouped = $helpcalls->groupBy('helpcall_id');

        $helpcallsOpenGrouped = $helpcalls->where('section', 'open')->groupBy('helpcall_id');

        $summary = [];

        $summary['helpcallsTotal'] = $helpcallsGrouped->count();

        $summary['helpcallsOpen'] = $helpcallsOpenGrouped->count();

        $summary['helpcallsComplete'] = $helpcalls->where('section', 'complete')->groupBy('helpcall_id')->count();

        $inspections = $this->getInspections();

        $inspectionsPast = $inspections->where('inspection_due_date', '<', $this->todayDbFormat);

        $inspectionsFuture = $inspections->where('inspection_due_date', '>=', $this->todayDbFormat);

        $summary['inspectionsPastTotal'] = $inspectionsPast->count();

        $summary['inspectionsPastIssued'] = $inspectionsPast->where('section', 'issued')->count();

        $summary['inspectionsPastComplete'] = $inspectionsPast->where('section', 'complete')->count();

        $summary['inspectionsFutureTotal'] = $inspectionsFuture->count();

        $summary['inspectionsFutureIssued'] = $inspectionsFuture->where('section', 'issued')->count();

        $summary['inspectionsFutureComplete'] = $inspectionsFuture->where('section', 'complete')->count();

        $data['summary'] = $summary;

        $data['hasReactiveData'] = (bool) $summary['helpcallsTotal'];

        $data['hasPpmData'] = (bool) ($summary['inspectionsPastTotal'] + $summary['inspectionsFutureTotal']);

        $data['helpcallsOpenGrouped'] = $helpcallsOpenGrouped;

        $data['inspections'] = $inspections;

        return $data;
    }

    private function getSite()
    {
        return Site::select([
            'site.site_id',
            'site.site_code',
            'site.site_desc',
            'site.photo',
        ])
            ->where(
                'site.site_id',
                '=',
                $this->siteId
            )
            ->first();
    }

    private function getHelpcalls()
    {
        return Helpcall::userSiteGroup()
            ->join(
                'instruction',
                'instruction.parent_id',
                '=',
                'helpcall.helpcall_id'
            )
            ->join(
                'helpcall_status',
                'helpcall_status.helpcall_status_id',
                '=',
                'helpcall.helpcall_status_id'
            )
            ->join(
                'instruction_status',
                'instruction_status.instruction_status_id',
                '=',
                'instruction.instruction_status_id'
            )
            ->leftJoin(
                'site',
                'site.site_id',
                '=',
                'helpcall.site_id'
            )
            ->leftJoin(
                'building',
                'building.building_id',
                '=',
                'helpcall.building_id'
            )
            ->leftJoin(
                'priority',
                'priority.priority_id',
                '=',
                'helpcall.priority_id'
            )
            ->leftJoin('note', function ($join) {
                $join->on(
                    'note.record_id',
                    '=',
                    'instruction.instruction_id'
                )
                    ->where(
                        'note.record_type_id',
                        '=',
                        NoteRecordType::INSTRUCTION
                    );
            })
            ->where(
                'instruction.parent_type_id',
                '=',
                InstructionParentType::HELPCALL
            )
            ->where(
                'instruction_status.instruction_status_type_id',
                '<>',
                InstructionStatusType::CANCELLED
            )
            ->where(
                'helpcall.site_id',
                '=',
                $this->siteId
            )
            ->where(
                'helpcall.logged_date',
                '>=',
                $this->fromDate
            )
            ->where(
                'helpcall.logged_date',
                '<=',
                $this->todayDbFormat
            )
            ->whereNotExists(function ($query) {
                $query->select(\DB::raw(1))
                    ->from('note AS note_a')
                    ->whereRaw(
                        "note_a.record_type_id = " . NoteRecordType::INSTRUCTION .
                            " AND note_a.record_id = instruction.instruction_id" .
                            " AND note_a.note_id > note.note_id"
                    );
            })
            ->select([
                'helpcall.helpcall_id',
                'helpcall.helpcall_code',
                'helpcall.helpcall_desc',
                \DB::raw("DATE_FORMAT(helpcall.logged_date, '%d-%m-%Y') AS logged_date"),
                'helpcall.helpcall_desc',
                'helpcall_status.code AS helpcall_status_code',
                'helpcall_status.description AS helpcall_status_desc',
                \DB::raw(
                    "IF(helpcall_status.helpcall_status_type_id IN(" .
                        HelpcallStatusType::COMPLETE .
                        "," .
                        HelpcallStatusType::CLOSED .
                        "),'complete','open') AS section"
                ),
                \DB::raw("COALESCE(building.building_desc, site.site_desc) AS location_desc"),
                'priority.priority_name',
                'priority.priority_desc',
                'instruction.instruction_code',
                'instruction.instruction_desc',
                \DB::raw("DATE_FORMAT(instruction.issued_date, '%d-%m-%Y') AS instruction_issued_date"),
                'instruction_status.instruction_status_code',
                'instruction_status.instruction_status_desc',
                'instruction_status.instruction_status_type_id',
                'note.detail AS note_detail'
            ])
            ->orderBy('helpcall.helpcall_code')
            ->orderBy('instruction.instruction_code')
            ->get();
    }

    private function getInspections()
    {
        return Inspection::userSiteGroup()
            ->join(
                'inspection_type',
                'inspection_type.inspection_type_id',
                '=',
                'inspection.inspection_type_id'
            )
            ->join(
                'inspection_group',
                'inspection_group.inspection_group_id',
                '=',
                'inspection_type.inspection_group_id'
            )
            ->join(
                'inspection_status',
                'inspection_status.inspection_status_id',
                '=',
                'inspection.inspection_status_id'
            )
            ->whereNotIn(
                'inspection_status.inspection_status_type_id',
                [
                    InspSFG20InspectionStatusType::DRAFT,
                    InspSFG20InspectionStatusType::CANCELLED
                ]
            )
            ->where(
                'inspection_group.inspection_group_code',
                '=',
                self::INSPECTION_GROUP_CODE
            )
            ->where(
                'inspection.site_id',
                '=',
                $this->siteId
            )
            ->where(
                'inspection.inspection_due_date',
                '>=',
                $this->fromDate
            )
            ->where(
                'inspection.inspection_due_date',
                '<=',
                $this->toDate
            )
            ->select([
                'inspection.inspection_id',
                'inspection.inspection_code',
                'inspection_due_date AS inspection_due_date_order',
                \DB::raw("DATE_FORMAT(inspection.inspection_due_date, '%d-%m-%Y') AS inspection_due_date"),
                \DB::raw("DATE_FORMAT(inspection.completed_date, '%d-%m-%Y') AS inspection_completed_date"),
                'inspection_type.inspection_type_description',
                'inspection_status.code AS inspection_status_code',
                'inspection_status.description AS inspection_status_desc',
                \DB::raw(
                    "CASE inspection_status.inspection_status_type_id" .
                        " WHEN " . InspSFG20InspectionStatusType::ISSUED . " THEN 'issued'" .
                        " WHEN " . InspSFG20InspectionStatusType::COMPLETE . " THEN 'complete'" .
                        " WHEN " . InspSFG20InspectionStatusType::CLOSED . " THEN 'complete'" .
                        " ELSE 'none'" .
                        " END AS section"
                ),
            ])
            ->orderBy('section')
            ->orderBy('inspection_status_code')
            ->orderBy('inspection_due_date_order')
            ->orderBy('inspection_code')
            ->get();
    }

    private function dateReportFormat($date)
    {
        return (new Carbon($date))->format('d-m-Y');
    }
}
