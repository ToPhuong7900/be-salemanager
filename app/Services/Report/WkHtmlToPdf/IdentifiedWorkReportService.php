<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\BoxFilterQuery\IReportFilterQuery;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Math;
use Tfcloud\Models\IdworkStatusType;
use Tfcloud\Models\Report;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Condition\IdworkService;

class IdentifiedWorkReportService extends WkHtmlToPdfReportBaseService
{
    private $identifiedWorkService;
    public $filterText = '';
    private $reportBoxFilterLoader = null;

    public function __construct(PermissionService $permissionService, Report $report)
    {
        parent::__construct($permissionService);
        $this->identifiedWorkService = new IdworkService($permissionService, null, new \Tfcloud\Models\Idwork());
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function runReport($repOutPutPdfFile, $inputs, $repId)
    {
        $reportFilterQuery = null;
        $pagePer = 5000;
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $query = $this->identifiedWorkService->all()->orderBy('site.site_code', 'ASC')
                ->orderBy('condsurvey.condsurvey_code', 'ASC')
                ->orderBy('idwork_element.idwork_element_code', 'ASC')
                ->orderBy('idwork_code', 'ASC'); // default sort
            $idworkData = $reportFilterQuery->filterAll($query)->get();
        } else {
            $this->formatInputData($inputs);
            $inputs['callOrderBy'] = true;
            $idworkData = $this->identifiedWorkService->getAll($pagePer, $inputs)->get();
        }

        $newTotal = 0; //hold cumulative total
        $oldLocation = ""; //report orders by site code
        $oldBuilding = ""; //report orders by building
        $oldCondSurvey = ""; //report orders by condition

        foreach ($idworkData as $idworkItem) {
            $newTotal = Math::addCurrency([$newTotal, $idworkItem->total_cost]);
            //find total of all items for the end of the report

            //check when site code changes to display header
            if ($oldLocation != $idworkItem->site_code) {
                $idworkItem['siteCodeShow'] = true;
            } else {
                $idworkItem['siteCodeShow'] = false;
            }
            $oldLocation = $idworkItem->site_code;

            //check when building code changes to display header
            if ($oldBuilding != $idworkItem->building_code) {
                $idworkItem['buildingCodeShow'] = true;
            } else {
                $idworkItem['buildingCodeShow'] = false;
            }
            $oldBuilding = $idworkItem->building_code;

            //check when cond.survey code changes to display header
            if ($oldCondSurvey != $idworkItem->condsurvey_code) {
                $idworkItem['condSurveyCodeShow'] = true;
            } else {
                $idworkItem['condSurveyCodeShow'] = false;
            }
            $oldCondSurvey = $idworkItem->condsurvey_code;
            $idworkItem['showDefLoc'] = 'false';
            if ($idworkItem->location) {
                $idworkItem['showDefLoc'] = 'true';
            }
            $idworkItem['showTargetYear'] = 'false';
            if ($idworkItem->idwork_target_fin_year_code != '') {
                $idworkItem['showTargetYear'] = "Target year: " . $idworkItem->idwork_target_fin_year_code;
            }
        }

        // Create wkhtmltopdf temp folder to store generated files
        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);

        $header = \View::make('reports.wkhtmltopdf.condition.cnd08.header')->render();
        $headerPath = $generatedPath . "/header.html";
        file_put_contents($headerPath, $header, LOCK_EX);

        $footer = \View::make('reports.wkhtmltopdf.condition.cnd08.footer')->render();
        $footerPath = $generatedPath . "/footer.html";
        file_put_contents($footerPath, $footer, LOCK_EX);

        $content = \View::make(
            'reports.wkhtmltopdf.condition.cnd08.content',
            [
                'idworks' => $idworkData,
                'total' => $newTotal
            ]
        )->render();

        $contentPath = $generatedPath . "/content.html";

        file_put_contents($contentPath, $content, LOCK_EX);

        $options = [
            'header-html' => $headerPath,
            'footer-html' => $footerPath,
            'header-spacing' => 4,
            'footer-spacing' => 4,
            'orientation' => 'landscape',
        ];

        $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $options);

        $whereCodes = [];               // The codes which have been used in the filter
        $whereTexts = [];               // Plain text about filtering applied
        $orCodes = [];                  // csv string of OR codes
        $bFilterDetail = false;

        if ($reportFilterQuery instanceof IReportFilterQuery) {
            $this->filterText = $reportFilterQuery->getFilterDetailText();
        } else {
            $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
            $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $this->filterText);
        }

        return true;
    }

    private function formatInputData(&$inputs)
    {
        foreach ($inputs as $key => $value) {
            if ($value === "") {
                unset($inputs[$key]);
            }
        }
    }

    private function generateFilterData($filterData, &$whereCodes, &$whereTexts, &$orCodes)
    {
        if ($val = Common::iset($filterData['element_id'])) {
            array_push($whereCodes, [
                'idwork_element',
                'idwork_element_id',
                'idwork_element_code',
                $val,
                "Element Code"
            ]);

            if ($val = Common::iset($filterData['sub_element_id'])) {
                array_push($whereCodes, [
                    'idwork_subelement',
                    'idwork_subelement_id',
                    'idwork_subelement_code',
                    $val,
                    "Sub Element Code"
                ]);

                if ($val = Common::iset($filterData['item_id'])) {
                    array_push($whereCodes, [
                        'idwork_item',
                        'idwork_item_id',
                        'idwork_item_code',
                        $val,
                        "Item Code"
                    ]);
                }
            }
        }

        if ($val = Common::iset($filterData['category_id'])) {
            array_push($whereCodes, [
                'idwork_category',
                'idwork_category_id',
                'idwork_category_code',
                $val,
                "Category Code"
            ]);
        }

        if ($val = Common::iset($filterData['cyclical_maintenance'])) {
            switch ($val) {
                case CommonConstant::YES:
                    array_push($whereTexts, "Cyclical Maintenance = 'Yes'");
                    break;

                case CommonConstant::NO:
                    array_push($whereTexts, "Cyclical Maintenance = 'No'");
                    break;

                case CommonConstant::ALL:
                default:
                    break;
            }
        }

        if ($val = Common::iset($filterData['targetFinYear'])) {
            array_push(
                $whereCodes,
                ['fin_year', 'fin_year_id', 'fin_year_code', $val, 'Target Financial Year']
            );
        }

        if ($siteId = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $siteId, "Site Code"]);

            if ($buildingId = Common::iset($filterData['building_id'])) {
                array_push(
                    $whereCodes,
                    ['building', 'building_id', 'building_code', $buildingId, "Building Code"]
                );

                if ($val = Common::iset($filterData['block_id'])) {
                    array_push($whereCodes, [
                        'building_block',
                        'building_block_id',
                        'building_block_code',
                        $val,
                        "Building Block Code"
                    ]);
                }

                if ($val = Common::iset($filterData['room_id'])) {
                    array_push($whereCodes, ['room', 'room_id', 'room_number', $val, "Room Number"]);
                }
            }
        }

        if ($val = Common::iset($filterData['remainingLifeFrom'])) {
            array_push($whereTexts, "Remaining Life >= '" . $val . "'");
        }

        if ($val = Common::iset($filterData['remainingLifeTo'])) {
            array_push($whereTexts, "Remaining Life <= '" . $val . "'");
        }

        if ($val = Common::iset($filterData['idworkStatus'])) {
            array_push(
                $whereCodes,
                ['idwork_status', 'idwork_status_id', 'idwork_status_code', $val, 'Status']
            );
        }


        $statusType = [];
        if (Common::iset($filterData['draft'])) {
            $statusType[] = IdworkStatusType::DRAFT;
        }
        if (Common::iset($filterData['plan'])) {
            $statusType[] = IdworkStatusType::PLAN;
        }
        if (Common::iset($filterData['tender'])) {
            $statusType[] = IdworkStatusType::TENDER;
        }
        if (Common::iset($filterData['wip'])) {
            $statusType[] = IdworkStatusType::WIP;
        }
        if (Common::iset($filterData['complete'])) {
            $statusType[] = IdworkStatusType::COMPLETE;
        }
        if (Common::iset($filterData['superseded'])) {
            $statusType[] = IdworkStatusType::SUPERSEDED;
        }
        if (count($statusType)) {
            array_push($orCodes, [
                'idwork_status_type',
                'idwork_status_type_id',
                'idwork_status_type_code',
                implode(',', $statusType),
                "Identified Work Status Type"
            ]);
        }

        if (($val = Common::iset($filterData['idwork_condition_id'])) && !empty($val)) {
            array_push($orCodes, [
                'idwork_condition',
                'idwork_condition_id',
                'idwork_condition_code',
                implode(',', $val),
                "Condition Code"
            ]);
        }

        if (($val = Common::iset($filterData['idwork_priority_id'])) && !empty($val)) {
            array_push($orCodes, [
                'idwork_priority',
                'idwork_priority_id',
                'idwork_priority_code',
                implode(',', $val),
                "Priority Code"
            ]);
        }

        if ($val = Common::iset($filterData['filter_iw'])) {
            array_push($whereTexts, "Code, Remedy or Defect contains '" . $val . "'");
        }
    }
}
