<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use Auth;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\BoxFilterQuery\IReportFilterQuery;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Constant\ReportConstant;
use Tfcloud\Lib\Filters\Properties\SiteFilter;
use Tfcloud\Models\FinYear;
use Tfcloud\Models\Idwork;
use Tfcloud\Models\Report;
use Tfcloud\Services\Condition\IdworkService;
use Tfcloud\Services\PermissionService;

class IdWorkFMRReportService extends WkHtmlToPdfReportBaseService
{
    private $idWorkService;
    public $filterText = '';
    private $reportBoxFilterLoader = null;

    public function __construct(PermissionService $permissionService, Report $report)
    {
        parent::__construct($permissionService);
        $this->idWorkService = new IdworkService($permissionService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function validateFilter($inputs)
    {
        $inputs['finYear'] = 'RUN'; // set data to run validate
        $this->validateFinYear($inputs);
        $rules = [
            'finYear' => ['not_enough_fin_year']
        ];

        return \Validator::make($inputs, $rules, $this->validateMessages());
    }

    private function validateFinYear($inputs)
    {
        $reportYear = $this->mappingReport($inputs['repCode'])['reportYear'];
        \Validator::extend(
            'not_enough_fin_year',
            function ($attribute, $value, $parameters) use ($reportYear) {
                $listYear = $this->getFinYearListQuery($reportYear)->count();

                if ($listYear < $reportYear) {
                    return false;
                }

                return true;
            }
        );
    }

    private function getFinYearListQuery($reportYear)
    {
        $finYear = FinYear::current();
        $listYear = FinYear::userSiteGroup()
            ->where(
                'year_start_date',
                '>=',
                $finYear->year_start_date
            )
            ->where(
                'year_start_date',
                '<',
                \DB::raw("DATE_ADD('{$finYear->year_start_date}', INTERVAL {$reportYear} YEAR)")
            )->get();

        return $listYear;
    }

    private function validateMessages()
    {
        return [
            'finYear.not_enough_fin_year' => 'Not enough financial years are configured to be able to run this report.'
        ];
    }

    public function runReport($repOutPutPdfFile, $inputs, $repId)
    {
        $repCode = $inputs['repCode'];
        $reportFilterQuery = null;
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $query = $this->all($repCode);
            $result = $reportFilterQuery->filterAll($query)->get();
        } else {
            $result = $this->filterSites($inputs, $repCode);
        }
        $groupResult = $result->groupBy($this->mappingGroup($repCode));
        $reportInfo = $this->mappingReport($repCode);
        $finYearList = $this->genFinYearList($reportInfo['reportYear']);
        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);

        $content = \View::make('reports.wkhtmltopdf.condition.' . $reportInfo['reportFolder'] . '.content', [
            'data' => $groupResult,
            'finYearList' => $finYearList
        ])->render();
        $contentFile = $generatedPath . '/content.html';
        file_put_contents($contentFile, $content, LOCK_EX);

        $header = \View::make('reports.wkhtmltopdf.condition.' . $reportInfo['reportFolder'] . '.header', [
            'pageTitle' => $reportInfo['pageTitle'],
            'finYearList' => $finYearList
        ])->render();
        $headerPath = $generatedPath . "/header.html";
        file_put_contents($headerPath, $header, LOCK_EX);

        $footer = \View::make('reports.wkhtmltopdf.condition.' . $reportInfo['reportFolder'] . '.footer', [
            'repCode' => $repCode
        ])->render();
        $footerPath = $generatedPath . "/footer.html";
        file_put_contents($footerPath, $footer, LOCK_EX);

        $this->generateSingleHtmlToPdf(
            $contentFile,
            $repOutPutPdfFile,
            [
                'footer-html' => $footerPath,
                'header-html' => $headerPath,
                'header-spacing' => 1,
                'footer-spacing' => 1,
                'orientation' => 'landscape',
                'page-size' => $reportInfo['pageSize']
            ]
        );

        $whereCodes = [];               // The codes which have been used in the filter
        $whereTexts = [];               // Plain text about filtering applied
        $orCodes = [];                  // csv string of OR codes
        $bFilterDetail = false;

        if ($reportFilterQuery instanceof IReportFilterQuery) {
            $this->filterText = $reportFilterQuery->getFilterDetailText();
        } else {
            $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
            $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $this->filterText);
        }

        return true;
    }

    private function generateFilterData($filterData, &$whereCodes, &$whereTexts, &$orCodes)
    {
        if ($val = Common::iset($filterData['establishment'])) {
            array_push(
                $whereCodes,
                [
                    'establishment',
                    'establishment_id',
                    'establishment_code',
                    $val,
                    "Establishment"
                ]
            );
        }

        if ($siteId = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $siteId, "Site Code"]);

            if ($buildingId = Common::iset($filterData['building_id'])) {
                array_push(
                    $whereCodes,
                    ['building', 'building_id', 'building_code', $buildingId, "Building Code"]
                );

                if ($val = Common::iset($filterData['block_id'])) {
                    array_push($whereCodes, [
                        'building_block',
                        'building_block_id',
                        'building_block_code',
                        $val,
                        "Building Block Code"
                    ]);
                }

                if ($val = Common::iset($filterData['room_id'])) {
                    array_push($whereCodes, ['room', 'room_id', 'room_number', $val, "Room Number"]);
                }
            }
        }


        $val = Common::iset($filterData['prop_zone_id']);
        if ($val && $val !== 'null') {
            array_push($whereCodes, ['prop_zone', 'prop_zone_id', 'prop_zone_code', $val, 'Zone']);

            if ($val = Common::iset($filterData['prop_external_id'])) {
                array_push(
                    $whereCodes,
                    ['prop_external', 'prop_external_id', 'prop_external_code', $val, 'External Area']
                );
            }
        }

        if ($val = Common::iset($filterData['site_type'])) {
            array_push($whereCodes, [
                'site_type',
                'site_type_id',
                'site_type_code',
                $val, \Lang::get('text.report_texts.property.site_type_code')
            ]);
        }

        if ($val = Common::iset($filterData['element_id'])) {
            array_push($whereCodes, [
                'idwork_element',
                'idwork_element_id',
                'idwork_element_code',
                $val,
                "Element Code"
            ]);
        }

        if ($val = Common::iset($filterData['tenure'])) {
            array_push($whereCodes, [
                'prop_tenure',
                'prop_tenure_id',
                'prop_tenure_code',
                $val,
                \Lang::get('text.report_texts.tenure')
            ]);
        }

        if ($val = Common::iset($filterData['usage'])) {
            array_push($whereCodes, [
                'site_usage',
                'site_usage_id',
                'site_usage_code',
                $val,
                \Lang::get('text.report_texts.usage_code')
            ]);
        }

        if ($val = Common::iset($filterData['committee'])) {
            array_push(
                $whereCodes,
                [
                    'committee',
                    'committee_id',
                    'committee_code',
                    $val,
                    \Config::get('cloud.labels.site_committee') . " Code"
                ]
            );
        }

        if ($val = Common::iset($filterData['ward'])) {
            array_push($whereCodes, [
                'ward',
                'ward_id',
                'ward_code',
                $val,
                \Lang::get('text.report_texts.ward_code')
            ]);
        }

        if ($val = Common::iset($filterData['parish'])) {
            array_push($whereCodes, [
                'parish',
                'parish_id',
                'parish_code',
                $val,
                \Lang::get('text.report_texts.parish_code')
            ]);
        }

        if ($val = Common::iset($filterData['terrier_status'])) {
            array_push(
                $whereCodes,
                [
                    'terrier_status',
                    'terrier_status_id',
                    'terrier_status_code',
                    $val,
                    "Terrier Status"
                ]
            );
        }

        if ($val = Common::iset($filterData['site_designation'])) {
            array_push(
                $whereCodes,
                [
                    'site_designation',
                    'site_designation_id',
                    'site_designation_code',
                    $val,
                    "Site Designation"
                ]
            );
        }

        if ($val = Common::iset($filterData['site_classification'])) {
            array_push(
                $whereCodes,
                [
                    'site_classification',
                    'site_classification_id',
                    'site_classification_code',
                    $val,
                    "Site Classification"
                ]
            );
        }
    }

    private function filterSites($inputs, $repCode)
    {
        return $this->filterAll($this->all($repCode), $inputs);
    }

    private function all($repCode)
    {
        $select = $this->mappingSelect($repCode);
        $query = Idwork::userSiteGroup()
            ->select($select)
            ->leftJoin(
                'condsurvey',
                'condsurvey.condsurvey_id',
                '=',
                'idwork.condsurvey_id'
            )
            ->leftJoin(
                'idwork_condition',
                'idwork_condition.idwork_condition_id',
                '=',
                'idwork.idwork_condition_id'
            )
            ->leftJoin(
                'fin_year',
                'fin_year.fin_year_id',
                '=',
                'idwork.target_fin_year_id'
            )
            ->leftJoin(
                'unit_of_measure',
                'unit_of_measure.unit_of_measure_id',
                '=',
                'idwork.remedy_unit_of_measure_id'
            )
            ->leftJoin(
                'idwork_defect',
                'idwork_defect.idwork_defect_id',
                '=',
                'idwork.idwork_defect_id'
            )
            ->leftJoin(
                'idwork_item',
                'idwork_item.idwork_item_id',
                '=',
                'idwork.item_id'
            )
            ->leftJoin(
                'idwork_subelement',
                'idwork_subelement.idwork_subelement_id',
                '=',
                'idwork.sub_element_id'
            )
            ->leftJoin(
                'idwork_element',
                'idwork_element.idwork_element_id',
                '=',
                'idwork.element_id'
            )
            ->leftJoin(
                'idwork_remedy',
                'idwork_remedy.idwork_remedy_id',
                '=',
                'idwork.idwork_remedy_id'
            )
            ->leftJoin(
                'room',
                'room.room_id',
                '=',
                'idwork.room_id'
            )
            ->leftJoin(
                'building',
                'building.building_id',
                '=',
                'idwork.building_id'
            )
            ->leftJoin(
                'idwork_priority',
                'idwork_priority.idwork_priority_id',
                '=',
                'idwork.idwork_priority_id'
            )
            ->leftJoin('site', 'site.site_id', '=', 'condsurvey.site_id')
            ->leftJoin('site_type', 'site_type.site_type_id', '=', 'site.site_type_id');

        $query = $this->permissionService->listViewSiteAccess($query, 'site.site_id');

        return $query;
    }

    private function filterAll($query, $inputs)
    {
        $filter = new SiteFilter($inputs);

        if (!empty($filter->site_id)) {
            $query->where('condsurvey.site_id', $filter->site_id);
        }

        if (!empty($filter->building_id)) {
            $query->where('idwork.building_id', $filter->building_id);
        }

        if (!empty($filter->block_id)) {
            $query->where('idwork.building_block_id', $filter->block_id);
        }

        if (!empty($filter->room_id)) {
            $query->where('idwork.room_id', $filter->room_id);
        }

        if (!empty($filter->site_type)) {
            $query->where('site.site_type_id', $filter->site_type);
        }

        if (!empty($filter->usage)) {
            $query->where('site.site_usage_id', $filter->usage);
        }

        if (!empty($filter->committee)) {
            $query->where('site.committee_id', $filter->committee);
        }

        if (!empty($filter->ward)) {
            $query->where("site.ward_id", $filter->ward);
        }

        if (!empty($filter->site_classification)) {
            $query->where('site.site_classification_id', $filter->site_classification);
        }

        if (!empty($filter->site_designation)) {
            $query->where('site.site_designation_id', $filter->site_designation);
        }

        if (!empty($filter->terrier_status)) {
            $query->where('site.terrier_status_id', $filter->terrier_status);
        }

        if (!empty($filter->parish)) {
            $query->where('site.parish_id', $filter->parish);
        }

        if (!empty($filter->tenure)) {
            $query->where('site.prop_tenure_id', $filter->tenure);
        }

        if (!empty($filter->establishment)) {
            $query->where('site.establishment_id', $filter->establishment);
        }

        if (!empty($filter->prop_external_id)) {
            $query->where('idwork.prop_external_id', $filter->prop_external_id);
        }

        if (!empty($filter->prop_zone_id)) {
            $query->where('idwork.prop_zone_id', $filter->prop_zone_id);
        }

        if (!empty($filter->element_id)) {
            $query->where('idwork.element_id', $filter->element_id);
        }

        $query->whereNotNull('target_fin_year_id');

        return $query->orderByRaw(
            'site.site_desc ASC,building.building_desc ASC, idwork_element.idwork_element_code ASC'
        )->get();
    }

    private function genFinYearList($reportYear)
    {
        $yearList = [];
        $finYearList = $this->getFinYearListQuery($reportYear);

        foreach ($finYearList as $year) {
            $finYearStart = substr($year->year_start_date, 0, 4);
            $yearList[$finYearStart] = $year->fin_year_code;
        }

        return $yearList;
    }

    private function mappingReport($repCode)
    {
        $pageSize = 'A4';
        switch ($repCode) {
            case ReportConstant::SYSTEM_REPORT_CND15:
            case ReportConstant::SYSTEM_REPORT_CND16:
            case ReportConstant::SYSTEM_REPORT_CND17:
                $reportYear = 5;
                break;
            case ReportConstant::SYSTEM_REPORT_CND18:
            case ReportConstant::SYSTEM_REPORT_CND19:
            case ReportConstant::SYSTEM_REPORT_CND20:
                $reportYear = 10;
                break;
            case ReportConstant::SYSTEM_REPORT_CND21:
            case ReportConstant::SYSTEM_REPORT_CND22:
            case ReportConstant::SYSTEM_REPORT_CND23:
                $reportYear = 25;
                $pageSize = 'A3';
                break;
        }

        switch ($repCode) {
            case ReportConstant::SYSTEM_REPORT_CND15:
            case ReportConstant::SYSTEM_REPORT_CND18:
            case ReportConstant::SYSTEM_REPORT_CND21:
                $pageTitle = "Identified Work $reportYear Year Plan (including Cyclical Work)";
                break;
            case ReportConstant::SYSTEM_REPORT_CND16:
            case ReportConstant::SYSTEM_REPORT_CND19:
            case ReportConstant::SYSTEM_REPORT_CND22:
                $pageTitle = "Identified Work $reportYear Year Summary (including Cyclical Work)";
                break;
            case ReportConstant::SYSTEM_REPORT_CND17:
            case ReportConstant::SYSTEM_REPORT_CND20:
            case ReportConstant::SYSTEM_REPORT_CND23:
                $pageTitle = "Identified Work $reportYear Year Totals (including Cyclical Work)";
                break;
        }

        return [
            'pageTitle' => $pageTitle,
            'reportYear' => $reportYear,
            'reportFolder' => strtolower($repCode),
            'pageSize' => $pageSize
        ];
    }

    private function mappingSelect($repCode)
    {
        switch ($repCode) {
            case ReportConstant::SYSTEM_REPORT_CND15:
            case ReportConstant::SYSTEM_REPORT_CND18:
            case ReportConstant::SYSTEM_REPORT_CND21:
                $select = [
                    'idwork.idwork_code',
                    'idwork.cyclical_maintenance',
                    'idwork.cycle_length',
                    'room.room_number',
                    'idwork_priority.idwork_priority_code',
                    'idwork_condition.idwork_condition_code',
                    'site.site_id',
                    'site.site_code',
                    'site.site_desc',
                    'building.building_id',
                    'building.building_code',
                    'building.building_desc',
                    'idwork.remedy',
                    'idwork.defect',
                    'idwork.cost',
                    'idwork.cycle_length',
                    \DB::raw(
                        '(CASE WHEN
                        idwork.remedy_quantity IS NOT NULL AND  unit_of_measure.unit_of_measure_code IS NOT NULL
                            THEN
                            CONCAT(idwork.remedy_quantity, " ", unit_of_measure.unit_of_measure_code)
                            ELSE
                            "" END) AS qty'
                    ),
                    'fin_year.fin_year_id',
                    'fin_year.year_start_date',
                    'fin_year.year_end_date',
                    'idwork_element.idwork_element_id',
                    'idwork_element.idwork_element_code',
                    'idwork_element.idwork_element_desc',
                    'idwork_subelement.idwork_subelement_desc',
                    'idwork_subelement.idwork_subelement_code',
                    'idwork_item.idwork_item_code',
                    'idwork_item.idworkitem_desc',
                ];
                break;
            case ReportConstant::SYSTEM_REPORT_CND16:
            case ReportConstant::SYSTEM_REPORT_CND19:
            case ReportConstant::SYSTEM_REPORT_CND22:
                $select = [
                    'idwork.idwork_code',
                    'site.site_id',
                    'site.site_code',
                    'site.site_desc',
                    'building.building_id',
                    'building.building_code',
                    'building.building_desc',
                    'idwork_element.idwork_element_code',
                    'idwork_element.idwork_element_desc',
                    'idwork.cost',
                    'fin_year.year_start_date',
                    'fin_year.year_end_date',
                    'idwork.cyclical_maintenance',
                    'idwork.cycle_length',
                    'idwork_element.idwork_element_id'
                ];
                break;
            case ReportConstant::SYSTEM_REPORT_CND17:
            case ReportConstant::SYSTEM_REPORT_CND20:
            case ReportConstant::SYSTEM_REPORT_CND23:
                $select = [
                    'idwork.idwork_code',
                    'site.site_id',
                    'site.site_code',
                    'site.site_desc',
                    'building.building_id',
                    'building.building_code',
                    'building.building_desc',
                    'idwork.cost',
                    'fin_year.year_start_date',
                    'fin_year.year_end_date',
                    'idwork.cyclical_maintenance',
                    'idwork.cycle_length',
                ];
                break;
        }

        return $select;
    }

    private function mappingGroup($repCode)
    {
        switch ($repCode) {
            case ReportConstant::SYSTEM_REPORT_CND15:
            case ReportConstant::SYSTEM_REPORT_CND18:
            case ReportConstant::SYSTEM_REPORT_CND21:
            case ReportConstant::SYSTEM_REPORT_CND16:
            case ReportConstant::SYSTEM_REPORT_CND19:
            case ReportConstant::SYSTEM_REPORT_CND22:
                $group = ['site_id', 'building_id', 'idwork_element_id'];
                break;
            case ReportConstant::SYSTEM_REPORT_CND17:
            case ReportConstant::SYSTEM_REPORT_CND20:
            case ReportConstant::SYSTEM_REPORT_CND23:
                $group = ['site_id', 'building_id'];
                break;
        }

        return $group;
    }
}
