<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use Tfcloud\Controllers\Admin\General\SiteGroupsController;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Math;
use Tfcloud\Lib\Common;
use Tfcloud\Models\Building;
use Tfcloud\Models\Condsurvey;
use Tfcloud\Models\CondLocationScore;
use Tfcloud\Models\CondLocationType;
use Tfcloud\Models\CondLocationElementScore;
use Tfcloud\Models\CondsurveyStatus;
use Tfcloud\Models\Contact;
use Tfcloud\Models\GenTable;
use Tfcloud\Models\GenUserdefLabel;
use Tfcloud\Models\GenUserDefSel;
use Tfcloud\Models\Idwork;
use Tfcloud\Models\IdworkElement;
use Tfcloud\Models\Report;
use Tfcloud\Models\Room;
use Tfcloud\Models\SiteGroup;
use Tfcloud\Models\IdworkPriority;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Admin\General\SiteGroupsService;
use Tfcloud\Services\Condition\ConditionService;

class CND13FullConditionSurveyNoIWCostsService extends WkHtmlToPdfReportBaseService
{
    public const MAX_USER_DEFINED_FIELDS = 20;
    private $genUserdefTypes = ['text', 'check', 'date', 'selection', 'contact'];
    private $condService;
    private $sgController;
    private $sgService;
    public $filterText = '';
    private $reportBoxFilterLoader = '';

    public function __construct(PermissionService $permissionService, Report $report = null)
    {
        parent::__construct($permissionService);
        $this->condService = new ConditionService($permissionService);
        $this->sgService = new SiteGroupsService($permissionService);
        $this->sgController = new SiteGroupsController($permissionService, $this->sgService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function runReportSingleCondSurvey($id)
    {
        //call report from button not report view
        $inputs['id'] = $id;
        $inputs['singleOnly'] = true;
        $repId = 349;

        return $this->runReport(
            $this->getTempFile(),
            $inputs,
            $repId
        );
    }

    public function runReport($repOutPutPdfFile, $inputs, $repId)
    {
        $this->formatInputs($inputs);
        $siteGroupId = \Auth::User()->site_group_id;
        $photoPath = $this->sgController->getPhotoPath($siteGroupId); //get logo for outputting at top of report

        //get the site group name and desc. for the report header
        $siteGroup = SiteGroup::select()->where('site_group_id', '=', $siteGroupId)->first();
        $siteGroupHeader = $siteGroup->code . " - " . $siteGroup->description;

        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);

            $conditionSurveys = $reportFilterQuery->filterAll($this->getConditionSurveysQuery())
                ->orderBy('condsurvey.condsurvey_code')->get();


            $this->filterText = $reportFilterQuery->getFilterDetailText();
        } else {
            $condQuery = $this->getCondSurveyData($inputs);

            $conditionSurveys = [];
            $conditionSurveys = $condQuery->get();
            $foundCondData = "";

            $whereCodes = [];               // The codes which have been used in the filter
            $whereTexts = [];               // Plain text about filtering applied
            $orCodes = [];                  // csv string of OR codes
            $bFilterDetail = false;

            $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
            $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $this->filterText);
        }


        if (array_key_exists('singleOnly', $inputs)) {
            $singleCondSurvey = 'true';
        } else {
            $singleCondSurvey = 'false';
        }

        $reportType = \Input::get('type', 'full');
        $reportOrder = \Input::get('order', 'element');
        $bHighPriorityOnly = ($reportType == 'highPriority') ? true : false;
        $bByLocation = ($reportOrder == 'location') ? true : false;

        if (count($conditionSurveys) > 0) {
            $foundCondData = 'true';
            $idworkGenUserdefLabels = $this->setIdworkGenUserdefLabels($siteGroupId);
            foreach ($conditionSurveys as $condSurvey) {
                 //get all elements for the report
                $idworkElementsQuery = $this->getAllElements($siteGroupId);
                $idworkElements = $idworkElementsQuery->get();

                //get the number of buildings for the report
                $condSurvey['noBuildings'] = $this->getCountAllBuildings($condSurvey->site_id);
                $condSurvey['noRooms'] = $this->getCountAllRooms($condSurvey->site_id);

                $this->setCondsurveyIdworkData(
                    $condSurvey,
                    $siteGroupId,
                    $idworkGenUserdefLabels,
                    $bByLocation,
                    $bHighPriorityOnly
                );

                $this->getLocationScores($condSurvey, $idworkElements);
            } //end foreach condsurvey
        } else {
            $foundCondData = 'false';
        }
        // Create wkhtmltopdf temp folder to store generated files
        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);

        $header = \View::make(
            'reports.wkhtmltopdf.condition.cnd13.header',
            [
                'photoPath' => $photoPath,
                'singleCondSurvey' => $singleCondSurvey,
                'siteGroupHeader' => $siteGroupHeader
            ]
        )->render();

        $headerPath = $generatedPath . "/header.html";
        file_put_contents($headerPath, $header, LOCK_EX);

        $footer = \View::make('reports.wkhtmltopdf.condition.cnd13.footer')->render();
        $footerPath = $generatedPath . "/footer.html";
        file_put_contents($footerPath, $footer, LOCK_EX);

        $content = \View::make(
            'reports.wkhtmltopdf.condition.cnd13.content',
            [
                'foundCondData'    => $foundCondData,
                'conditionSurveys' => $conditionSurveys,
                'photoPath'        => $photoPath

            ]
        )->render();

        $contentPath = $generatedPath . "/content.html";

        file_put_contents($contentPath, $content, LOCK_EX);

        $whereCodes = [];               // The codes which have been used in the filter
        $whereTexts = [];               // Plain text about filtering applied
        $orCodes = [];                  // csv string of OR codes
        $bFilterDetail = false;

        $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
        $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $this->filterText);

        $options = [
            'header-html' => $headerPath,
            'footer-html' => $footerPath,
            'header-spacing' => 4,
            'footer-spacing' => 4,
            'orientation' => 'landscape',
        ];

        if (array_key_exists('singleOnly', $inputs)) {
            $repOutPutPdfFile .= '.pdf';
            $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $options);
            return $repOutPutPdfFile;
        } else {
            $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $options);
            return true;
        }
    }

    private function getTempFile()
    {
        return tempnam(Common::getTempFolder(), 'CND13SingleFullCondSurvey');
    }
    private function getConditionSurveysQuery()
    {
        $query = Condsurvey::select()
            ->leftjoin(
                'site',
                'site.site_id',
                '=',
                'condsurvey.site_id'
            )
            ->leftjoin(
                'user',
                'user.id',
                '=',
                'condsurvey.owner_user_id'
            )
            ->leftjoin(
                'address',
                'address.address_id',
                '=',
                'site.address_id'
            )
            ->leftjoin(
                'address_street',
                'address_street.address_street_id',
                '=',
                'address.address_street_id'
            )
            ->leftjoin(
                'gen_gis',
                'gen_gis.gen_gis_id',
                '=',
                'site.gen_gis_id'
            )
            ->leftjoin(
                'score',
                'score.score_id',
                '=',
                'condsurvey.score_id'
            )
            ->leftjoin(
                'condsurvey_status',
                'condsurvey_status.condsurvey_status_id',
                '=',
                'condsurvey.condsurvey_status_id'
            )
            ->leftjoin(
                'contact',
                'contact.contact_id',
                '=',
                'condsurvey.surveyor'
            )
            ->where('condsurvey.site_group_id', '=', \Auth::User()->site_group_id);

        if (\Auth::User()->isSelfServiceUser()) {
            $query->where('condsurvey.condsurvey_status_id', '<>', CondsurveyStatus::OPEN)
                ->where('condsurvey.condsurvey_status_id', '<>', CondsurveyStatus::QA);
        }

        return $query;
    }

    private function getCondSurveyData($inputs)
    {
        $query = $this->getConditionSurveysQuery();

        //apply filters
        if (array_key_exists('code', $inputs)) {
            if (($inputs['code'] != "") && ($inputs['code'] != " ")) {
                $query->where('condsurvey.condsurvey_code', '=', $inputs['code']);
            }
        }

        if (array_key_exists('singleOnly', $inputs)) {
            $query->where('condsurvey.condsurvey_id', '=', $inputs['id']);
        } else {
            //check other inputs passed in by filter

            if ((($inputs['description'] ?? "") != "") && ($inputs['description'] != " ")) {
                $query->where('condsurvey.condsurvey_desc', '=', $inputs['description']);
            }

            if (($inputs['site_id'] ?? "") != "" && ($inputs['site_id'] != " ")) {
                $query->where('condsurvey.site_id', '=', $inputs['site_id']);
                //check all levels
                if ((($inputs['building_id'] ?? "") != "") && ($inputs['building_id'] != " ")) {
                    $query->where('condsurvey.condsurvey_building_id', '=', $inputs['building_id']);

                    if (($inputs['room_id'] != "") && ($inputs['room_id'] != " ")) {
                        $query->where('condsurvey.room_id', '=', $inputs['room_id']);
                    }
                }
                //this location filter doesn't have building block level
//                if ((($inputs['building_id'] ?? "") != "") && ($inputs['building_id'] != " ")) {
//                    $query->where('condsurvey.condsurvey_building_block_id', '=', $inputs['block_id']);
//                }
            } //end site id check

            $status = array();
            foreach ((new CondsurveyStatus())->getStatusList() as $key => $value) {
                if (isset($inputs[$value])) {
                    $status[] = $key;
                }
            }
            if (count($status)) {
                $query->whereIn("condsurvey.condsurvey_status_id", $status);
            }
        }
        $query->orderBy('condsurvey.condsurvey_code');
        return $query;
    }

    private function getAllElements($siteGroupId)
    {
        return IdworkElement::select()
                ->where('site_group_id', '=', $siteGroupId)
                ->where('active', '=', 'Y');
    }

    private function getIdworkPriorityIdList()
    {
        return IdworkPriority::userSiteGroup()
            ->where('high_priority', 'Y')
            ->pluck('idwork_priority_id')
            ->toArray();
    }

    private function getCountAllBuildings($siteId)
    {
        $buildings = Building::select()->where('building.site_id', '=', $siteId)->get();
        return count($buildings);
    }

    private function getCountAllRooms($siteId)
    {
        $totalCount = $buildingCount = 0;
        $buildings = Building::select()->where('building.site_id', '=', $siteId)->get();
        foreach ($buildings as $building) {
            $rooms = Room::select()->where('room.building_id', '=', $building->building_id)->get();
            $buildingCount = count($rooms);
            $totalCount = Math::addInt([$totalCount, $buildingCount]);
        }
        return $totalCount;
    }

    private function setCondsurveyIdworkData(
        &$condSurvey,
        $siteGroupId,
        $idworkGenUserdefLabels,
        $bByLocation,
        $bHighPriorityOnly = false
    ) {

        //get all idwork data

        $condSurveyIdwork = Idwork::where('idwork.condsurvey_id', '=', $condSurvey->condsurvey_id)
            ->leftjoin('idwork_element', 'idwork_element.idwork_element_id', '=', 'idwork.element_id')
            ->leftjoin('idwork_subelement', 'idwork_subelement.idwork_subelement_id', '=', 'idwork.sub_element_id')
            ->leftjoin('idwork_item', 'idwork_item.idwork_item_id', '=', 'idwork.item_id')
            ->leftjoin('idwork_condition', 'idwork_condition.idwork_condition_id', '=', 'idwork.idwork_condition_id')
            ->leftjoin('idwork_priority', 'idwork_priority.idwork_priority_id', '=', 'idwork.idwork_priority_id')
            ->leftjoin('condsurvey', 'condsurvey.condsurvey_id', '=', 'idwork.condsurvey_id')
            ->leftjoin('site', 'site.site_id', '=', 'condsurvey.site_id')
            ->leftjoin('building', 'building.building_id', '=', 'idwork.building_id')
            ->leftjoin('room', 'room.room_id', '=', 'idwork.room_id')
            ->leftjoin('fin_year', "fin_year.fin_year_id", '=', 'idwork.target_fin_year_id')
            ->leftjoin(
                'gen_userdef_group',
                'gen_userdef_group.gen_userdef_group_id',
                '=',
                'idwork.gen_userdef_group_id'
            )
            // JA: Added select in order to retrieve idwork.comment which was
            // being overwritten by the comment column in the joined tables.
            ->select(
                'idwork.*',
                'idwork.comment as idwork_comment',
                'idwork_element.*',
                'idwork_subelement.*',
                'idwork_item.*',
                'idwork_condition.*',
                'idwork_priority.*',
                'fin_year.fin_year_code AS idwork_target_fin_year_code',
                'condsurvey.*',
                'site.*',
                'building.*',
                'room.*',
                'gen_userdef_group.*'
            );

        if ($bHighPriorityOnly) {
            $highPriorityList = $this->getIdworkPriorityIdList();
            if (!is_array($highPriorityList) || count($highPriorityList) == 0) {
                $highPriorityList = [0];
            }
            $condSurveyIdwork->whereIn('idwork.idwork_priority_id', $highPriorityList);
        }
        if ($bByLocation) {
            // Order by building, room, then element, sub element, item
            $condSurveyIdwork->orderBy('building.building_code', 'ASC')
                ->orderBy('room.room_number', 'ASC')
                ->orderBy('idwork_element.idwork_element_code', 'ASC')
                ->orderBy('idwork_subelement.idwork_subelement_code', 'ASC')
                ->orderBy('idwork_item.idwork_item_code', 'ASC')
                ->orderBy('idwork_priority.idwork_priority_code', 'ASC');
        } else {
            // element, sub element, item, then by building, room
            $condSurveyIdwork->orderBy('idwork_element.idwork_element_code', 'ASC')
                ->orderBy('idwork_subelement.idwork_subelement_code', 'ASC')
                ->orderBy('idwork_item.idwork_item_code', 'ASC')
                ->orderBy('building.building_code', 'ASC')
                ->orderBy('room.room_number', 'ASC')
                ->orderBy('idwork_priority.idwork_priority_code', 'ASC');
        }

        $condSurvey['idworks'] = $condSurveyIdwork->get();

        $firstHalf = "";                                    // No need to work this out for every piece of idwork
        foreach ($condSurvey['idworks'] as $idwork) {
            $idWorkId = $idwork->idwork_id;
            $photo = [];
            $photo = Idwork::select('photo')->where('idwork.idwork_id', '=', $idwork->idwork_id)->first();
            $photoName = $photo->photo;

            $idwork['showDefLoc'] = 'false';
            if ($idwork->location) {
                $idwork['showDefLoc'] = 'true';
            }

            $idwork['showTargetYear'] = 'false';
            if ($idwork->idwork_target_fin_year_code != '') {
                // This avoids problems when fin year code is 14-15 and html tries to do calculation on it
                $idwork['showTargetYear'] = "Target year: " . $idwork->idwork_target_fin_year_code;
            }

            if ($photoName) {
                //get the image
                if (!$firstHalf) {
                    $idwImagePath = $this->getPhotoStoragePathway($siteGroupId, $idWorkId, 'idwork');
                    //pathway checking in wrong folder, redirect
                    $splitString = explode("idwork/", $idwImagePath);
                    $firstHalf = $splitString[0];
                }
                $idworkPhotoPath = $firstHalf . "idwork/" . $idWorkId . "/" . $photoName;
                $idwork['hasImg'] = 'true';
                $idwork['photoPath'] = $idworkPhotoPath;

                //check image exists
                if (!file_exists($idworkPhotoPath)) {
                    //show no picture available file
                    $useDefaultPhoto = true;
                    $idworkPhotoPath = $this->fallBackImagePath($useDefaultPhoto);
                    $idwork['photoPath'] = $idworkPhotoPath;
                }
            } else {
                $idwork['hasImg'] = 'false';
            }
            $this->setIdworkGenUserdefValues($idwork, $idworkGenUserdefLabels);
        } //end foreach idwork
    }

    private function getElementScores($idworkElements, $condScoreLocId)
    {
        $idworkElementScores = array();
        foreach ($idworkElements as $idworkElement) {
            // get the score if there is one for that element
            $idworkElementScore = array();
            $idworkElementScore['idwork_element'] = $idworkElement->idwork_element_code;

            $idworkElementId = $idworkElement->idwork_element_id;
            $elementScore = CondLocationElementScore::select()
                ->where('cond_location_element_score.cond_location_score_id', '=', $condScoreLocId)
                ->where('cond_location_element_score.idwork_element_id', '=', $idworkElementId)
                ->leftjoin('score', 'score.score_id', '=', 'cond_location_element_score.score_id')
                ->first();
            if ($elementScore) {
                $idworkElementScore['score'] = $elementScore->score_code;
                $idworkElementScore['comment'] = $elementScore->comment;
            } else {
                $idworkElementScore['score'] = '';
                $idworkElementScore['comment'] = '';
            }
            array_push($idworkElementScores, $idworkElementScore);
        }
        return $idworkElementScores;
    }

    private function showData($condSurvey)
    {
        if ($condSurvey['sitelocationScore']) {
            \Log::error("Site is " . $condSurvey['sitelocationScore']->site_code);
            foreach ($condSurvey['siteLocationElementScore'] as $elem) {
                \Log::error("elem {$elem['idwork_element']}:{$elem['score']}:{$elem['comment']}.");
            }
        }
        foreach ($condSurvey['buildingLocationScores'] as $locScore) {
            \Log::error("Building is " . $locScore['building']->building_code);
            \Log::error("Building num idwork is " . $locScore['numBuildingIdwork']);
            foreach ($locScore['locationScore'] as $elem) {
                \Log::error("elem {$elem['idwork_element']}:{$elem['score']}:{$elem['comment']}.");
            }
            foreach ($locScore['blocks'] as $blocScore) {
                \Log::error("Block is " . $blocScore['block']->building_block_code);
                \Log::error("Block num idwork is " . $blocScore['numBlockIdwork']);
                foreach ($blocScore['locationScore'] as $elem) {
                    \Log::error("elem {$elem['idwork_element']}:{$elem['score']}:{$elem['comment']}.");
                }
            }
        }
    }
    /**
     * getLocationScores Obtains all the location scores for the supplied survey. The list will be in order
     * with the site location scores, then for each building, the building and all the block scores.
     *
     * @param array $condSurvey
     * @return void
     */
    private function getLocationScores(&$condSurvey, $idworkElements)
    {
        // Need to get all of the location scores, these can be site, building or block
        CondLocationType::TYPE_SITE;
        CondLocationType::TYPE_BUILDING;
        CondLocationType::TYPE_BLOCK;

        $buildingLocationScores = array();

        // Get the site location scores if there is one
        $sitelocationScore = CondLocationScore::select()
            ->where('cond_location_score.condsurvey_id', '=', $condSurvey->condsurvey_id)
            ->where('cond_location_score.cond_location_type_id', '=', CondLocationType::TYPE_SITE)
            ->leftjoin('score', 'score.score_id', '=', 'cond_location_score.score_id')
            ->leftjoin('site', 'site.site_id', '=', 'cond_location_score.location_record_id')
            ->leftjoin('address', 'address.address_id', '=', 'site.address_id')
            ->leftjoin('address_street', 'address_street.address_street_id', '=', 'address.address_street_id')
            ->first();

        if ($sitelocationScore) {
            $condSurvey['sitelocationScore'] = $sitelocationScore;
            if ($sitelocationScore->score_code) {
                $condSurvey['site_score_code'] = Common::concatFields(
                    [$sitelocationScore->score_code,
                    $sitelocationScore->score_desc]
                );
            } else {
                $condSurvey['site_score_code'] = 'Not Selected.';
            }
            $condScoreLocId = $sitelocationScore->cond_location_score_id;
            $idworkElementScores = $this->getElementScores($idworkElements, $condScoreLocId);
            $condSurvey['siteLocationElementScore'] = $idworkElementScores;
        }

        // Get the building location scores if there are any
        $locBuildingScoreData = CondLocationScore::select()
            ->where('cond_location_score.condsurvey_id', '=', $condSurvey->condsurvey_id)
            ->where('cond_location_score.cond_location_type_id', '=', CondLocationType::TYPE_BUILDING)
            ->leftjoin('score', 'score.score_id', '=', 'cond_location_score.score_id')
            ->leftjoin('building', 'building.building_id', '=', 'cond_location_score.location_record_id')
            ->leftjoin('site', 'site.site_id', '=', 'building.site_id')
            ->leftjoin('address', 'address.address_id', '=', 'building.address_id')
            ->leftjoin('address_street', 'address_street.address_street_id', '=', 'address.address_street_id')
            ->leftjoin('building_type', 'building_type.building_type_id', '=', 'building.building_type_id')
            ->leftjoin('listed_type', 'listed_type.listed_type_id', '=', 'building.listed_type_id')
            ->leftjoin('building_usage', 'building_usage.building_usage_id', '=', 'building.building_usage_id')
            ->leftjoin('ward', 'ward.ward_id', '=', 'building.ward_id')
            ->orderBy('cond_location_score.cond_location_score_id')
            ->get();

        foreach ($locBuildingScoreData as $locBuildScore) {
            \Log::info("Block Data ({$locBuildScore->building_code})");
            $buildingData = array();
            $buildingData['building'] = $locBuildScore;
            if ($locBuildScore->score_code) {
                $buildingData['building_score_code'] = Common::concatFields(
                    [$locBuildScore->score_code,
                    $locBuildScore->score_desc]
                );
            } else {
                $buildingData['building_score_code'] = 'Not Selected.';
            }
            $buildingData['blocks'] = array();
            $numIdwork = 0;
            foreach ($condSurvey['idworks'] as $idwork) {
                if ($idwork->building_id == $locBuildScore->building_id) {
                    ++$numIdwork;
                }
            }
            $buildingData['numBuildingIdwork'] = $numIdwork;

            $condScoreLocId = $locBuildScore->cond_location_score_id;
            $idworkElementScores = $this->getElementScores($idworkElements, $condScoreLocId);
            $buildingData['locationScore'] = $idworkElementScores;

            $locBlockScoreData = CondLocationScore::select()
                ->where('cond_location_score.condsurvey_id', '=', $condSurvey->condsurvey_id)
                ->where('cond_location_score.cond_location_type_id', '=', CondLocationType::TYPE_BLOCK)
                ->leftjoin('score', 'score.score_id', '=', 'cond_location_score.score_id')
                ->leftjoin(
                    'building_block',
                    'building_block.building_block_id',
                    '=',
                    'cond_location_score.location_record_id'
                )
                ->leftjoin('building', 'building.building_id', '=', 'building_block.building_id')
                ->leftjoin('site', 'site.site_id', '=', 'building.site_id')
                ->leftjoin('address', 'address.address_id', '=', 'building.address_id')
                ->leftjoin('address_street', 'address_street.address_street_id', '=', 'address.address_street_id')
                ->leftjoin(
                    'building_block_type',
                    'building_block_type.building_block_type_id',
                    '=',
                    'building_block.building_block_type_id'
                )
                ->leftjoin(
                    'building_age',
                    'building_age.building_age_id',
                    '=',
                    'building_block.building_age_id'
                )
                ->leftjoin(
                    'construction_type',
                    'construction_type.construction_type_id',
                    '=',
                    'building_block.construction_type_id'
                )
                ->orderBy('cond_location_score.cond_location_score_id')
                ->get();
            foreach ($locBlockScoreData as $locBlockScore) {
                \Log::info("Block Data ({$locBlockScore->building_block_code})");
                $blockData['block'] = $locBlockScore;

                if ($locBuildScore->score_code) {
                    $blockData['block_score_code'] = Common::concatFields(
                        [$locBuildScore->score_code,
                        $locBuildScore->score_desc]
                    );
                } else {
                    $blockData['block_score_code'] = 'Not Selected.';
                }

                $numIdwork = 0;
                foreach ($condSurvey['idworks'] as $idwork) {
                    if ($idwork->room_id && ($idwork->building_block_id == $locBuildScore->building_block_id)) {
                        ++$numIdwork;
                    }
                }
                $blockData['numBlockIdwork'] = $numIdwork;
                $condScoreLocId = $locBlockScore->cond_location_score_id;
                $idworkElementScores = $this->getElementScores($idworkElements, $condScoreLocId);
                $blockData['locationScore'] = $idworkElementScores;
                array_push($buildingData['blocks'], $blockData);
            }
            array_push($buildingLocationScores, $buildingData);
        }

        $condSurvey['buildingLocationScores'] = $buildingLocationScores;
    }

    /**
     * Setup all the valid Identified Work User Defined Labels in an array
     */
    private function setIdworkGenUserdefLabels($siteGroupId)
    {
        // get the ID work labels
        $idworkGenUserdefLabel = GenUserdefLabel::where('gen_userdef_label.site_group_id', '=', $siteGroupId)
            ->where('gen_userdef_label.gen_table_id', '=', GenTable::IDWORK)
            ->first()->toArray();

        $idworkGenUserdefLabels = [];

        foreach ($this->genUserdefTypes as $genUserdefType) {
            $this->setValidIdworkGenUserdefLabels($idworkGenUserdefLabel, $idworkGenUserdefLabels, $genUserdefType);
        }

        return $idworkGenUserdefLabels;
    }

    /**
     * Save all the valid, not null, user defined field labels, with the field type and number, for a specific type
     */
    private function setValidIdworkGenUserdefLabels($idworkGenUserdefLabel, &$idworkGenUserdefLabels, $genUserdefType)
    {
        // loop through the user defined labels for a specific type
        for ($i = 1; $i < self::MAX_USER_DEFINED_FIELDS; $i++) {
            // create the array key using type and number
            $key = "user_${genUserdefType}${i}_label";
            if (array_key_exists($key, $idworkGenUserdefLabel)) {
                // it is a valid user defined label
                if (!is_null($idworkGenUserdefLabel[$key])) {
                    // the user defined label is not null so save it
                    $idworkGenUserdefLabels [] = [
                        'type'      => $genUserdefType,
                        'number'    => $i,
                        'label'     => $idworkGenUserdefLabel[$key],
                    ];
                }
            } else {
                // no more valid user defined fields to be found so end loop
                break;
            }
        }
    }

    /**
     * Populate user defined array of values for all user defined fields that are turned on.
     */
    private function setIdworkGenUserdefValues($idwork, $idworkGenUserdefLabels)
    {
        $userDefValues = [];

        foreach ($idworkGenUserdefLabels as $idworkGenUserdefLabel) {
            $type = $idworkGenUserdefLabel['type'];
            $number = $idworkGenUserdefLabel['number'];
            if ($type === 'selection') {
                // for selections get the id column
                $column = "sel{$number}_gen_userdef_sel_id";
            } else {
                // create the column name using type and number
                $column = "user_{$type}{$number}";
            }
            $value = $idwork->$column;
            if (!$value) {
                // value is not set, so set ouput to blank
                $userDefValues [] = '';
                continue;
            }
            switch ($type) {
                case 'date':
                    // convert yyyy-mm-dd date to dd/mm/yyyy
                    $userDefValues [] = \DateTime::createFromFormat('Y-m-d', $value)->format('d/m/Y');
                    break;
                case 'selection':
                    // convert the selection id to code and description
                    $userDefValues [] = GenUserDefSel::find($value)->getCodeDescAttribute();
                    break;
                case 'contact':
                    // convert the contact id to name and organisation
                    $userDefValues [] = Contact::find($value)->getContactOrganistationAttribute();
                    break;
                default:
                    // the default action is to use the column value as is
                    $userDefValues [] = $idwork->$column;
            }
        }

        // update idwork with the user defined arrays
        $idwork->userDefLabels = array_column($idworkGenUserdefLabels, 'label');
        $idwork->userDefValues = $userDefValues;
    }

    private function generateFilterData($filterData, &$whereCodes, &$whereTexts, &$orCodes)
    {
        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Condition Survey Code = '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Condition Survey Description = '" . $val . "'");
        }

        if ($siteId = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $siteId, "Site Code"]);

            if ($buildingId = Common::iset($filterData['building_id'])) {
                array_push(
                    $whereCodes,
                    ['building', 'building_id', 'building_code', $buildingId, "Building Code"]
                );

                if ($val = Common::iset($filterData['block_id'])) {
                    array_push($whereCodes, [
                        'building_block',
                        'building_block_id',
                        'building_block_code',
                        $val,
                        "Building Block Code"
                    ]);
                }

                if ($val = Common::iset($filterData['room_id'])) {
                    array_push($whereCodes, ['room', 'room_id', 'room_number', $val, "Room Number"]);
                }
            }
        }
    }

    private function formatInputs(&$inputs)
    {
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $isSuperseded = $reportFilterQuery->getValueFilterField('superseded');

            if (isset($isSuperseded[0]) && Common::valueYorNtoBoolean($isSuperseded[0])) {
                $inputs['superseded'] = true;
            }
        }

        return $inputs;
    }
}
