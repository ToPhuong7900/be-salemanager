<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf\Modules\Personnel;

use Tfcloud\Models\Personnel\PersReasonType;
use Tfcloud\Services\Report\WkHtmlToPdf\WkHtmlToPdfReportBaseService;

class PersReportBaseService extends WkHtmlToPdfReportBaseService
{
    protected function setFilteringDetails($filterData, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $reasonTypeIds = array_get($filterData, 'reasonTypeIds');
        if ($reasonTypeIds && is_array($reasonTypeIds)) {
            $reasonTypes = PersReasonType::whereIn('pers_reason_type_id', $reasonTypeIds)
                ->pluck('pers_reason_type_code');

            if (in_array(0, $reasonTypeIds)) {
                $reasonTypes->push('Non-Working');
            }
            $reasonTypeAuditText = 'Reason Type = ' . $reasonTypes->implode(' or ');
            array_push($whereTexts, $reasonTypeAuditText);
        }

        if ($val = array_get($filterData, 'lineManagerId')) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Line Manager']);
        }

        if ($val = array_get($filterData, 'userGroupId')) {
            array_push($whereCodes, ['usergroup', 'usergroup_id', 'usergroup_code', $val, 'User Group']);
        }

        if ($val = array_get($filterData, 'dloTeamId')) {
            array_push($whereCodes, ['team', 'team_id', 'team_code', $val, 'Dlo Team']);
        }

        if ($val = array_get($filterData, 'userId')) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'User']);
        }
    }
}
