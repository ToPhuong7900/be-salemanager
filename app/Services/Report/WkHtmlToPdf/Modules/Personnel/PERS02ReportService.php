<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf\Modules\Personnel;

use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Models\Personnel\PersDayOff;
use Tfcloud\Models\Personnel\PersReasonType;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Personnel\TeamViewService;

class PERS02ReportService extends PersReportBaseService
{
    public $filterText = '';
    private $teamViewService;
    private $reasonTypeIds = [];

    private const NON_WORKING_DAY = 0;
    private const TIME = 'time';

    private const PERSONNEL_COLOR_MAPPING = [
        PersReasonType::HOLIDAY => 'holiday-color',
        PersReasonType::SICK => 'sick-color',
        PersReasonType::MISCELLANEOUS => 'miscellaneous-color',
        self::NON_WORKING_DAY => 'non-working-color',
        self::TIME => 'time-color',
    ];

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->teamViewService = new TeamViewService($permissionService);
    }

    public function runReport($repOutPutPdfFile, $inputs, $repId)
    {
        $filterInputs = $this->getFilterInputs($inputs);
        $data = $this->getReportData($filterInputs);
        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);

        $footer = \View::make('reports.wkhtmltopdf.personnel.pers02.footer')->render();
        $footerPath = $generatedPath . "/footer.html";
        file_put_contents($footerPath, $footer, LOCK_EX);

        $content = \View::make('reports.wkhtmltopdf.personnel.pers02.content', $data)->render();
        $contentPath = $generatedPath . "/content.html";
        file_put_contents($contentPath, $content, LOCK_EX);

        $whereCodes = [];               // The codes which have been used in the filter
        $whereTexts = [];               // Plain text about filtering applied
        $orCodes = [];                  // csv string of OR codes
        $bFilterDetail = false;
        $this->generateFilterData($filterInputs, $whereCodes, $orCodes, $whereTexts);
        $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $this->filterText);

        $options = [
            'margin-top' => 8,
            'margin-bottom' => 10,
            'orientation' => 'landscape',
            'page-size' => 'A2',
            'footer-html' => $footerPath,
        ];

        $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $options);

        return true;
    }

    private function getReportData($inputs)
    {
        $date = $inputs['date'];
        $calendarHeader = $this->teamViewService->getTeamViewCalendarHeader($date);

        $calendarData = $this->teamViewService->getCalendarData(
            $inputs,
            $calendarHeader['start'],
            $calendarHeader['end']
        );

        $this->generateReasonTypeIds(array_get($inputs, 'reasonTypeIds', []));
        $reformatCalendar = $this->reformatCalendar($calendarData);
        $numberOfRecordEachPage = 90;
        $chunkedCalendar = array_chunk($reformatCalendar, $numberOfRecordEachPage);

        return [
            'months' => $calendarHeader['months'],
            'weeks' => $calendarHeader['weeks'],
            'days' => $calendarHeader['days'],
            'startDate' => $calendarHeader['start'],
            'endDate' => $calendarHeader['end'],

            'chunkedCalendar' => $chunkedCalendar,

            'logo' => $this->getHeaderLogo(),
            'date' => $date
        ];
    }

    private function generateFilterData($filterData, &$whereCodes, &$orCodes, &$whereTexts)
    {
        if ($val = array_get($filterData, 'date')) {
            array_push($whereTexts, "Date is $val");
        }

        $this->setFilteringDetails($filterData, $whereCodes, $orCodes, $whereTexts);
    }

    private function reformatCalendar($calendarData)
    {
        foreach ($calendarData as &$user) {
            foreach ($user['detail'] as &$item) {
                if (!$item) {
                    $item = [];
                }
                $item = $this->getBookingClassesByItem($item);
            }
        }

        return $calendarData;
    }

    private function getBookingClassesByItem($item)
    {
        $bookingClass = [];
        ksort($item);
        foreach ($item as $key => $session) {
            if ($key == 'full_day') {
                continue;
            }

            $bookingClass[] = $this->getBookingClass($session);
        }

        return $bookingClass;
    }

    private function getBookingClass($session)
    {
        if (is_null($session)) {
            return null;
        }

        if ($this->reasonTypeIds[self::NON_WORKING_DAY] && $session === false) {
            return self::PERSONNEL_COLOR_MAPPING[self::NON_WORKING_DAY];
        }

        if (!empty($session[PersDayOff::TYPE_TIME_KEY])) {
            //filter with reason type
            foreach ($session['details'] as $dayOff) {
                if (!empty($this->reasonTypeIds[$dayOff['type']])) {
                    return self::PERSONNEL_COLOR_MAPPING[self::TIME];
                }
            }
        }

        if ($typeClass = $this->checkAndGetReasonTypeClass($session)) {
            return $typeClass;
        }

        return null;
    }

    private function getFilterInputs($inputs)
    {
        $cleanedFilter = array_filter($inputs, function ($filter) {
            return $filter;
        });

        $cleanedFilter['date'] = array_get($cleanedFilter, 'date', date(CommonConstant::FORMAT_DATE));

        if (!empty($cleanedFilter['userId'])) {
            $cleanedFilter['userIds'] = [$cleanedFilter['userId']];
        }

        return $cleanedFilter;
    }

    private function generateReasonTypeIds($reasonTypeIds)
    {
        $ids = array_values($reasonTypeIds);

        $this->reasonTypeIds = [
            PersReasonType::HOLIDAY => in_array(PersReasonType::HOLIDAY, $ids),
            PersReasonType::SICK => in_array(PersReasonType::SICK, $ids),
            PersReasonType::MISCELLANEOUS => in_array(PersReasonType::MISCELLANEOUS, $ids),
            self::NON_WORKING_DAY => in_array(self::NON_WORKING_DAY, $ids),
        ];
    }

    private function checkAndGetReasonTypeClass($session)
    {
        if (!isset($session['type'])) {
            return null;
        }

        $typeId = $session['type'];
        if (!empty($this->reasonTypeIds[$typeId]) && isset(self::PERSONNEL_COLOR_MAPPING[$typeId])) {
            return self::PERSONNEL_COLOR_MAPPING[$typeId];
        }

        return null;
    }
}
