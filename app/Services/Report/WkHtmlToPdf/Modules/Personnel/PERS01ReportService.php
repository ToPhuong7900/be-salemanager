<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf\Modules\Personnel;

use Carbon\Carbon;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Models\Personnel\PersDayOff;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Personnel\TeamViewService;

class PERS01ReportService extends PersReportBaseService
{
    public $filterText = '';
    private $teamViewService;

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->teamViewService = new TeamViewService($permissionService);
    }

    public function validateFilter($inputs)
    {
        return \Validator::make($inputs, $this->validateRules(), $this->validateMessages());
    }

    public function runReport($repOutPutPdfFile, $inputs, $repId)
    {
        $inputs = $this->formatInput($inputs);
        $data = $this->getReportData($inputs);
        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);

        $header = \View::make('reports.wkhtmltopdf.personnel.pers01.header', [
            'logo' => $this->getHeaderLogo(),
            'calendarYear' => array_get($inputs, 'year', '')
        ])->render();
        $headerPath = $generatedPath . "/header.html";
        file_put_contents($headerPath, $header, LOCK_EX);

        $footer = \View::make('reports.wkhtmltopdf.personnel.pers01.footer')->render();
        $footerPath = $generatedPath . "/footer.html";
        file_put_contents($footerPath, $footer, LOCK_EX);

        $content = \View::make('reports.wkhtmltopdf.personnel.pers01.content', [
            'calendarHeader' => $data['calendarHeader'],
            'userCalendars' => $data['userCalendars'],
        ])->render();
        $contentPath = $generatedPath . "/content.html";
        file_put_contents($contentPath, $content, LOCK_EX);

        $whereCodes = [];               // The codes which have been used in the filter
        $whereTexts = [];               // Plain text about filtering applied
        $orCodes = [];                  // csv string of OR codes
        $bFilterDetail = false;
        $this->generateFilterData($inputs, $whereCodes, $orCodes, $whereTexts);
        $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $this->filterText);

        $options = [
            'header-spacing' => 4,
            'footer-spacing' => 4,
            'header-html' => $headerPath,
            'footer-html' => $footerPath,
            'orientation' => 'landscape',
        ];

        $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $options);
        return true;
    }

    private function getReportData($inputs)
    {
        $filteredColorTypes = $this->formatReasonTypeFilter(array_get($inputs, 'reasonTypeIds', []));
        $calendarHeader = $this->teamViewService->getPersonViewCalendarHeader($inputs['date']);
        $outputData = [
            'calendarHeader' => array_get($calendarHeader, 'weekDay', []),
            'userCalendars' => []

        ];
        $users = array_get($inputs, 'users', []);

        foreach ($users as $user) {
            $userId = $user['id'];
            $userDisplayName = $user['text'];
            $inputs['userIds'] = [$userId];
            $data = $this->teamViewService->getCalendarData(
                $inputs,
                $calendarHeader['start'],
                $calendarHeader['end']
            );
            $idKey = '_' . $userId;
            if (isset($data[$idKey])) {
                $calendarData = $this->teamViewService->formatPersonalCalendar($data[$idKey], $calendarHeader);
                $outputData['userCalendars'][] = [
                    'userDisplayName' => $userDisplayName,
                    'calendarData' => $this->formatCalendarData($calendarData, $filteredColorTypes)
                ];
            }
        }

        return $outputData;
    }

    private function formatReasonTypeFilter(array $reasonTypeIds)
    {
        $colorTypes = [
            0 => 'non-working-color',
            1 => 'holiday-color',
            2 => 'sick-color',
            3 => 'miscellaneous-color',
        ];
        if (count($reasonTypeIds) < 4) {
            $filteredColorTypes = [];
            foreach ($colorTypes as $key => $colorType) {
                if (in_array($key, $reasonTypeIds)) {
                    $filteredColorTypes[$key] = $colorType;
                } else {
                    $filteredColorTypes[$key] = '';
                }
            }
            return $filteredColorTypes;
        }
        return $colorTypes;
    }

    private function formatCalendarData($calendarData, $colorTypes)
    {
        $spanClass = 'day-session ';
        foreach ($calendarData as $month => $dates) {
            foreach ($dates as $date => $value) {
                $cssValueData = [
                    'cellContent' => null,
                    'cellCSSClass' => null
                ];
                $cssItemData = [];
                if ($value !== false) {
                    $cssValueData['cellContent'] = intval(substr($date, 0, 2));
                }
                if (is_null($value)) {
                    $cssValueData['cellCSSClass'] = 'no-patent';
                } elseif ($value === false) {
                    $cssValueData['cellCSSClass'] = 'invalid-day invalid-day-span';
                } else {
                    ksort($value);
                    foreach ($value as $key => $item) {
                        if ($key != 'full_day') {
                            if ($item === false) {
                                $itemCSSClass = $spanClass . $colorTypes[0] ?? '';
                            } elseif ($item === true) {
                                $itemCSSClass = $spanClass;
                            } elseif (!empty($item['type'])) {
                                $itemCSSClass = $spanClass . $colorTypes[$item['type']] ?? '';
                            } elseif (!empty($item[PersDayOff::TYPE_TIME_KEY])) {
                                //filter with reason type
                                foreach ($item['details'] as $dayOff) {
                                    if (!empty($colorTypes[$dayOff['type']])) {
                                        $itemCSSClass = $spanClass . ' time-color';
                                        break;
                                    }
                                }
                            } else {
                                $itemCSSClass = $spanClass;
                            }
                            $cssItemData[] = $itemCSSClass;
                        }
                    }
                }
                $calendarData[$month][$date] = array_merge($cssValueData, ['items' => $cssItemData]);
            }
        }
        return $calendarData;
    }

    private function formatInput($inputs)
    {
        $year = array_get($inputs, 'year', date(CommonConstant::FORMAT_DATE));
        $inputs['date'] = Carbon::create($year)->format('d/m/Y');
        if ($userId = array_get($inputs, 'userId')) {
            $userArr = $this->teamViewService->getUserList(['userId' => $userId]);
        } else {
            $userArr = $this->teamViewService->getUserList();
        }
        $inputs['users'] = $userArr;
        return $inputs;
    }

    private function validateRules()
    {
        return [
            'year' => ['required', 'integer'],
        ];
    }

    private function validateMessages()
    {
        return [
            'year.required' => 'Year is required.',
            'year.integer' => 'Year must be an integer.',
        ];
    }

    private function generateFilterData($filterData, &$whereCodes, &$orCodes, &$whereTexts)
    {
        if ($val = array_get($filterData, 'year')) {
            array_push($whereTexts, "Year is $val");
        }
        $this->setFilteringDetails($filterData, $whereCodes, $orCodes, $whereTexts);
    }
}
