<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use DateTime;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Constant\ReportConstant;
use Tfcloud\Lib\Estates\PaymentSummary;
use Tfcloud\Lib\Exceptions\PermissionException;
use Tfcloud\Lib\Filters\Estate\LeaseOutFilter;
use Tfcloud\Lib\Permissions\RuralEstatesPermission;
use Tfcloud\Models\CrudAccessLevel;
use Tfcloud\Models\Estate\EstBreakDateParentType;
use Tfcloud\Models\Estate\EstChargeCalcEndPoint;
use Tfcloud\Models\EstAgreement;
use Tfcloud\Models\EstCharge;
use Tfcloud\Models\EstReview;
use Tfcloud\Models\EstUnitType;
use Tfcloud\Models\EstSalesInvoiceStatus;
use Tfcloud\Models\FinAccount;
use Tfcloud\Models\GenTable;
use Tfcloud\Models\LeaseOut;
use Tfcloud\Models\LeaseOutStatus;
use Tfcloud\Models\LeaseOutStatusType;
use Tfcloud\Models\LeaseOutLetu;
use Tfcloud\Models\Module;
use Tfcloud\Models\Report;
use Tfcloud\Services\Admin\General\ContactService;
use Tfcloud\Services\Estate\AgreementService;
use Tfcloud\Services\Estate\EstateSalesInvoiceService;
use Tfcloud\Services\Estate\EstBreakDateService;
use Tfcloud\Services\Estate\EstChargeService;
use Tfcloud\Services\Estate\InvoiceGenerationPlanService;
use Tfcloud\Services\Estate\LeaseOutLetuService;
use Tfcloud\Services\Estate\LeaseOutService;
use Tfcloud\Services\Estate\LettableUnitService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Property\BuildingService;
use Tfcloud\Services\Property\SiteService;
use Tfcloud\Services\UserService;

class LeaseOutSummaryReportService extends WkHtmlToPdfReportBaseService
{
    private $leaseOutService;
    private $agreementService;
    private $contactService;
    private $userService;
    private $invoiceGenerationPlanService;
    private $estateSalesInvoiceService;
    private $leaseOutLetuService;
    private $lettableUnitService;
    private $siteService;
    private $buildingService;
    private $estBreakDateService;
    protected $reportBoxFilterLoader;
    public $filterText = '';

    public function __construct(PermissionService $permissionService, Report $report)
    {
        parent::__construct($permissionService);
        $this->leaseOutService = new LeaseOutService($permissionService);
        $this->agreementService = new AgreementService($permissionService);
        $this->contactService = new ContactService($permissionService);
        $this->userService = new UserService();
        $this->invoiceGenerationPlanService = new InvoiceGenerationPlanService(
            $permissionService,
            ['checkPermission' => false]
        );
        $this->estateSalesInvoiceService = new EstateSalesInvoiceService($permissionService);
        $this->leaseOutLetuService = new LeaseOutLetuService($permissionService);
        $this->lettableUnitService  = new LettableUnitService($permissionService, $this->leaseOutLetuService);
        $this->siteService = new SiteService($permissionService);
        $this->buildingService = new BuildingService($permissionService);
        $this->estBreakDateService = new EstBreakDateService($permissionService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function runReportSingleLetting($letting)
    {
        //call report from button not report view
        $inputs['lease_out_id'] = $letting->lease_out_id;
        $inputs['singleOnly'] = true;
        $repId = Common::getSystemReportId(ReportConstant::SYSTEM_REPORT_ES37);

        return $this->runReport(
            $this->getTempFile(),
            $inputs,
            $repId
        );
    }

    public function runReport($repOutPutPdfFile, $inputs, $repId)
    {
        $this->formatInputData($inputs);
        $leaseOuts = $this->getData($inputs);
        // Create wkhtmltopdf temp folder to store generated files
        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);

        $header = \View::make('reports.wkhtmltopdf.estates.es37.header', [
            'logo' => $this->getHeaderLogo()
        ])->render();
        $headerPath = $generatedPath . "/header.html";
        file_put_contents($headerPath, $header, LOCK_EX);

        $footer = \View::make('reports.wkhtmltopdf.estates.es37.footer')->render();
        $footerPath = $generatedPath . "/footer.html";
        file_put_contents($footerPath, $footer, LOCK_EX);

        $content = \View::make(
            'reports.wkhtmltopdf.estates.es37.content',
            [
                'leaseOuts' => $leaseOuts,
            ]
        )->render();
        $contentPath = $generatedPath . "/content.html";
        file_put_contents($contentPath, $content, LOCK_EX);

        $options = [
            'header-html' => $headerPath,
            'footer-html' => $footerPath,
            'header-spacing' => 4,
            'footer-spacing' => 4,
            'orientation' => 'portrait',
        ];

        if (array_key_exists('singleOnly', $inputs)) {
            $repOutPutPdfFile .= '.pdf';
            $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $options);
        } else {
            $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $options);
        }

        $whereCodes = [];               // The codes which have been used in the filter
        $whereTexts = [];               // Plain text about filtering applied
        $orCodes = [];                  // csv string of OR codes
        $bFilterDetail = false;

        $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
        $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $this->filterText);

        if (array_key_exists('singleOnly', $inputs)) {
            return $repOutPutPdfFile;
        } else {
            return true;
        }
    }

    public function getAll($inputs)
    {
        if (
            !$this->permissionService->hasModuleAccess(
                Module::MODULE_ESTATES,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
            )
        ) {
            $msg = "Not met minimum readonly access for estates module in class" .
                " (" . __CLASS__ . ") method (" . __METHOD__ . "). " .
                " See line (" . __LINE__ . ")";
            throw new PermissionException($msg);
        }

        if (array_key_exists("callOrderBy", $inputs)) {
            return $this->filterAll(
                $this->all()
                    ->orderBy('contact.contact_name'),
                $inputs
            );
        } else {
            return $this->filterAll($this->all(), $inputs);
        }
    }

    private function getTempFile()
    {
        return Common::getTempFolder() . DIRECTORY_SEPARATOR . 'ES37_SingleLettingSummary';
    }

    private function all()
    {
        return LeaseOut::userSiteGroup()
            ->leftJoin(
                'est_lease_sub_type',
                'lease_out.est_lease_sub_type_id',
                '=',
                'est_lease_sub_type.est_lease_sub_type_id'
            )
            ->leftJoin('lease_type', 'lease_out.lease_type_id', '=', 'lease_type.lease_type_id')
            ->leftJoin('period_of_lease', 'period_of_lease.period_of_lease_id', '=', 'lease_out.period_of_lease_id')
            ->leftJoin('lease_out_status', 'lease_out.lease_out_status_id', '=', 'lease_out_status.lease_out_status_id')
            ->leftJoin('contact', 'lease_out.tenant_contact_id', '=', 'contact.contact_id')
            ->leftJoin('vw_lease_out_site', 'lease_out.lease_out_id', '=', 'vw_lease_out_site.lease_out_id')
            ->leftJoin('vw_lease_out_building', 'lease_out.lease_out_id', '=', 'vw_lease_out_building.lease_out_id')
            ->leftJoin('lease_in', 'lease_in.lease_in_id', '=', 'lease_out.head_lease_in_id')
            ->leftJoin('tenancy_type', 'tenancy_type.tenancy_type_id', '=', 'lease_out.tenancy_type_id')
            ->leftJoin('rates_liability', 'rates_liability.rates_liability_id', '=', 'lease_out.rates_liability_id')
            ->leftJoin('insce_liability', 'insce_liability.insce_liability_id', '=', 'lease_out.insce_liability_id')
            ->leftJoin('estate_maint', 'estate_maint.estate_maint_id', '=', 'lease_out.estate_maint_id')
            ->leftJoin('unit_of_area', 'unit_of_area.unit_of_area_id', '=', 'lease_out.area_unit_of_area_id')
            ->select([
                'lease_out.*',
                'period_of_lease.period_of_lease_desc',
                'lease_type.lease_type_code',
                'est_lease_sub_type.est_lease_sub_type_code',
                'est_lease_sub_type.est_lease_sub_type_desc',
                'lease_out_status.lease_out_status_code',
                Common::getSqlSelectConcatContactNameOrg(),
                'contact.contact_id',
                'contact.contact_name AS tenant_name',
                'contact.organisation AS tenant_org',
                'contact.contact_id AS tenant_id',
                'vw_lease_out_site.site_code_combined',
                'vw_lease_out_site.site_desc_combined',
                'vw_lease_out_site.site_id AS site_id',
                'vw_lease_out_building.building_code_combined',
                'vw_lease_out_building.building_desc_combined',
                'vw_lease_out_building.building_id AS building_id',
                'lease_in.lease_in_code',
                'lease_in.lease_in_desc',
                'tenancy_type.tenancy_type_code',
                'tenancy_type.tenancy_type_desc',
                'rates_liability.rates_liability_code',
                'rates_liability.rates_liability_desc',
                'insce_liability.insce_liability_code',
                'insce_liability.insce_liability_desc',
                'estate_maint.estate_maint_code',
                'estate_maint.estate_maint_desc',
                'unit_of_area.unit_of_area_code',
                'unit_of_area.unit_of_area_desc'
            ])
            ->orderBy('lease_out.lease_out_code');
    }

    private function filterAll(
        $query,
        $inputs,
        $view = false,
        $lettingPacketId = 0,
        $lettableUnitId = 0,
        $isUsingLettable = true
    ) {
        if (!array_key_exists('lease_out_id', $inputs)) {
            $filter = new LeaseOutFilter($inputs);

            if ($view) {
                $table = $view;
            } else {
                $table = 'lease_out';
                $query->orderBy($filter->sort, $filter->sortOrder);
            }

            if (!is_null($filter->code)) {
                $query->where($table . '.lease_out_code', 'like', '%' . trim($filter->code) . '%');
            }

            if (!is_null($filter->description)) {
                $query->where($table . '.lease_out_desc', 'like', '%' . trim($filter->description) . '%');
            }

            if (!is_null($filter->leaseOutStatus)) {
                $query->where($table . '.lease_out_status_id', $filter->leaseOutStatus);
            }

            if (!is_null($filter->periodOfLeaseFlt)) {
                $query->where($table . '.period_of_lease_id', $filter->periodOfLeaseFlt);
            }

            if (
                !is_null($filter->lostActive) ||
                !is_null($filter->lostInactive) ||
                !is_null($filter->lostArchive) ||
                !is_null($filter->lostTerminated)
            ) {
                $options = [];

                if (!is_null($filter->lostActive)) {
                    array_push($options, LeaseOutStatusType::ACTIVE);
                }

                if (!is_null($filter->lostInactive)) {
                    array_push($options, LeaseOutStatusType::INACTIVE);
                }

                if (!is_null($filter->lostArchive)) {
                    array_push($options, LeaseOutStatusType::ARCHIVE);
                }

                if (!is_null($filter->lostTerminated)) {
                    array_push($options, LeaseOutStatusType::TERMINATED);
                }

                $leaseOutStatus = LeaseOutStatus::whereIn('lease_out_status_type_id', $options)->get();

                $statusIds = [];

                foreach ($leaseOutStatus as $status) {
                    array_push($statusIds, $status->lease_out_status_id);
                }

                $query->whereIn("{$table}.lease_out_status_id", $statusIds);
            }

            if (!is_null($filter->leaseType)) {
                $query->where($table . '.lease_type_id', $filter->leaseType);
            }

            if (!is_null($filter->leaseSubType)) {
                $query->where($table . '.est_lease_sub_type_id', $filter->leaseSubType);
            }

            if (!is_null($filter->landlord)) {
                $query->where($table . '.landlord_contact_id', $filter->landlord);
            }

            if (!is_null($filter->contact)) {
                $query->where($table . '.tenant_contact_id', $filter->contact);
            }

            if (!is_null($filter->salesAcctRef)) {
                $query->where('contact.sales_acount_ref', 'like', '%' . $filter->salesAcctRef . '%');
            }

            if (!is_null($filter->owner)) {
                $query->where($table . '.owner_user_id', $filter->owner);
            }

            if (
                !is_null($filter->holdingOver) &&
                in_array(
                    $filter->holdingOver,
                    [
                        CommonConstant::DATABASE_VALUE_YES,
                        CommonConstant::DATABASE_VALUE_NO
                    ]
                )
            ) {
                $query->where($table . '.holding_over', $filter->holdingOver);
            }

            if (!is_null($filter->holdingOverFrom)) {
                $holdingOverFrom = DateTime::createFromFormat(CommonConstant::FORMAT_DATE, $filter->holdingOverFrom);
                if ($holdingOverFrom) {
                    $query->where(
                        DB::raw("DATE_FORMAT({$table}.holding_over_date , '%Y-%m-%d')"),
                        '>=',
                        $holdingOverFrom->format('Y-m-d')
                    );
                }
            }

            if (!is_null($filter->holdingOverTo)) {
                $holdingOverTo = DateTime::createFromFormat(CommonConstant::FORMAT_DATE, $filter->holdingOverTo);
                if ($holdingOverTo) {
                    $query->where(
                        DB::raw("DATE_FORMAT({$table}.holding_over_date, '%Y-%m-%d')"),
                        '<=',
                        $holdingOverTo->format('Y-m-d')
                    );
                }
            }

            if (!is_null($filter->agreementDateFrom)) {
                $agreementDateFrom = DateTime::createFromFormat(
                    CommonConstant::FORMAT_DATE,
                    $filter->agreementDateFrom
                );
                if ($agreementDateFrom) {
                    $query->where(
                        DB::raw("DATE_FORMAT({$table}.agreement_date , '%Y-%m-%d')"),
                        '>=',
                        $agreementDateFrom->format('Y-m-d')
                    );
                }
            }

            if (!is_null($filter->agreementDateTo)) {
                $agreementDateTo = DateTime::createFromFormat(CommonConstant::FORMAT_DATE, $filter->agreementDateTo);
                if ($agreementDateTo) {
                    $query->where(
                        DB::raw("DATE_FORMAT({$table}.agreement_date, '%Y-%m-%d')"),
                        '<=',
                        $agreementDateTo->format('Y-m-d')
                    );
                }
            }

            if (!is_null($filter->authorisedDateFrom)) {
                $authorisedDateFrom = DateTime::createFromFormat(
                    CommonConstant::FORMAT_DATE,
                    $filter->authorisedDateFrom
                );
                if ($authorisedDateFrom) {
                    $query->where(
                        DB::raw("DATE_FORMAT({$table}.authorised_date , '%Y-%m-%d')"),
                        '>=',
                        $authorisedDateFrom->format('Y-m-d')
                    );
                }
            }

            if (!is_null($filter->authorisedDateTo)) {
                $authorisedDateTo = DateTime::createFromFormat(CommonConstant::FORMAT_DATE, $filter->authorisedDateTo);
                if ($authorisedDateTo) {
                    $query->where(
                        DB::raw("DATE_FORMAT({$table}.authorised_date, '%Y-%m-%d')"),
                        '<=',
                        $authorisedDateTo->format('Y-m-d')
                    );
                }
            }

            if (!is_null($filter->terminationDateFrom)) {
                $terminationDateFrom = DateTime::createFromFormat(
                    CommonConstant::FORMAT_DATE,
                    $filter->terminationDateFrom
                );
                if ($terminationDateFrom) {
                    $query->where(
                        DB::raw("DATE_FORMAT({$table}.termination_date , '%Y-%m-%d')"),
                        '>=',
                        $terminationDateFrom->format('Y-m-d')
                    );
                }
            }

            if (!is_null($filter->terminationDateTo)) {
                $terminationDateTo = DateTime::createFromFormat(
                    CommonConstant::FORMAT_DATE,
                    $filter->terminationDateTo
                );
                if ($terminationDateTo) {
                    $query->where(
                        DB::raw("DATE_FORMAT({$table}.termination_date, '%Y-%m-%d')"),
                        '<=',
                        $terminationDateTo->format('Y-m-d')
                    );
                }
            }

            if (!is_null($filter->startDateFrom)) {
                $startDateFrom = DateTime::createFromFormat(CommonConstant::FORMAT_DATE, $filter->startDateFrom);
                if ($startDateFrom) {
                    $query->where(
                        DB::raw("DATE_FORMAT({$table}.start_date , '%Y-%m-%d')"),
                        '>=',
                        $startDateFrom->format('Y-m-d')
                    );
                }
            }

            if (!is_null($filter->startDateTo)) {
                $startDateTo = DateTime::createFromFormat(CommonConstant::FORMAT_DATE, $filter->startDateTo);
                if ($startDateTo) {
                    $query->where(
                        DB::raw("DATE_FORMAT({$table}.start_date, '%Y-%m-%d')"),
                        '<=',
                        $startDateTo->format('Y-m-d')
                    );
                }
            }

            if (!is_null($filter->endDateFrom)) {
                $endDateFrom = DateTime::createFromFormat(CommonConstant::FORMAT_DATE, $filter->endDateFrom);
                if ($endDateFrom) {
                    $query->where(
                        DB::raw("DATE_FORMAT({$table}.end_date , '%Y-%m-%d')"),
                        '>=',
                        $endDateFrom->format('Y-m-d')
                    );
                }
            }

            if (!is_null($filter->endDateTo)) {
                $endDateTo = DateTime::createFromFormat(CommonConstant::FORMAT_DATE, $filter->endDateTo);
                if ($endDateTo) {
                    $query->where(
                        DB::raw("DATE_FORMAT({$table}.end_date, '%Y-%m-%d')"),
                        '<=',
                        $endDateTo->format('Y-m-d')
                    );
                }
            }

            if (!is_null($filter->rates_liability_id)) {
                $query->where($table . '.rates_liability_id', $filter->rates_liability_id);
            }

            if (!is_null($filter->insce_liability_id)) {
                $query->where($table . '.insce_liability_id', $filter->insce_liability_id);
            }

            if (!is_null($filter->estate_maint_id)) {
                $query->where($table . '.estate_maint_id', $filter->estate_maint_id);
            }

            if (!is_null($filter->ruralLeases)) {
                if (
                    in_array(
                        $filter->ruralLeases,
                        [CommonConstant::DATABASE_VALUE_YES, CommonConstant::DATABASE_VALUE_NO]
                    )
                ) {
                    $query->where($table . '.ruralest_lease', $filter->ruralLeases);
                }
            } elseif (
                !RuralEstatesPermission::hasModuleLicence() ||
                !RuralEstatesPermission::hasModuleAccessRead(false)
            ) {
                // The Rural Leases filter is not displayed if the Rural Estates module is not enabled for the customer.
                // Also, the filter is disabled if the user does not have read access to Rural Estates module.
                // In these cases only select the Standard leases.
                $query->where($table . '.ruralest_lease', CommonConstant::DATABASE_VALUE_NO);
            }

            if (!is_null($filter->areaFrom)) {
                $query->where($table . '.area_value', '>=', $filter->areaFrom);
            }

            if (!is_null($filter->areaTo)) {
                $query->where($table . '.area_value', '<=', $filter->areaTo);
            }

            if (!is_null($filter->areaUnits)) {
                $query->where($table . '.area_unit_of_area_id', $filter->areaUnits);
            }

            if (
                !is_null($filter->vatIncluded)
                && in_array(
                    $filter->vatIncluded,
                    [CommonConstant::DATABASE_VALUE_YES, CommonConstant::DATABASE_VALUE_NO]
                )
            ) {
                $query->where($table . '.vat_included', $filter->vatIncluded);
            }

            if (
                !is_null($filter->landlordTenantAct) &&
                in_array(
                    $filter->landlordTenantAct,
                    [
                        CommonConstant::DATABASE_VALUE_YES,
                        CommonConstant::DATABASE_VALUE_NO
                    ]
                )
            ) {
                $query->where($table . '.landlord_tenant_act', $filter->landlordTenantAct);
            }

            $filter->site = !is_null($filter->site) ?
                $filter->site : (!is_null($filter->site_id) ? $filter->site_id : null);
            if (!is_null($filter->site)) {
                if ($view) {
                    $query->where("{$table}.site_id", $filter->site);
                } else {
                    $list = LeaseOutLetu::leftJoin('lettable_unit', function ($join) {
                        $join->on('lease_out_letu.lettable_unit_id', '=', 'lettable_unit.lettable_unit_id')
                            ->where('lettable_unit.est_unit_type_id', '=', EstUnitType::EST_UT_LU);
                    })
                        ->leftJoin(
                            'lettable_unit_item',
                            'lettable_unit.lettable_unit_id',
                            '=',
                            'lettable_unit_item.lettable_unit_id'
                        )
                        ->where('lettable_unit.site_group_id', \Auth::user()->site_group_id)
                        ->where('lettable_unit_item.site_id', $filter->site)
                        ->groupBy('lease_out_id')
                        ->pluck('lease_out_id')->toArray();

                    if (!empty($list)) {
                        $query->whereIn('lease_out.lease_out_id', $list);
                    } else {
                        $query->whereIn('lease_out.lease_out_id', ['']);
                    }
                }
            }

            if ($lettingPacketId != 0) {
                if ($lettingPacketId == -1) {
                    $query->whereNull('lease_out.est_letting_packet_id');
                } else {
                    $query->where('lease_out.est_letting_packet_id', $lettingPacketId);
                }
            }

            if ($lettableUnitId != 0) {
                if ($isUsingLettable) {
                    $query->where('lease_out_letu.lettable_unit_id', $lettableUnitId);
                } else {
                    $list = LeaseOutLetu::where('lettable_unit_id', $lettableUnitId)
                        ->pluck('lease_out_id')->toArray();
                    if (!empty($list)) {
                        $query->whereNotIn('lease_out.lease_out_id', $list);
                    }
                }
            }
            $this->filterByUserDefines(GenTable::LEASE_OUT, $query, $filter, $table, $view);
        } else {
            $query->where('lease_out.lease_out_id', $inputs['lease_out_id']);
        }

        return $query;
    }

    private function getAgreements($recordId)
    {
        if (
            !$this->permissionService->hasModuleAccess(
                Module::MODULE_ESTATES,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
            )
        ) {
            $msg = "Not met minimum readonly access for estates module in class" .
                " (" . __CLASS__ . ") method (" . __METHOD__ . "). " .
                " See line (" . __LINE__ . ")";
            throw new PermissionException($msg);
        }
        return EstAgreement::userSiteGroup()
            ->where('lease_out_id', $recordId)
            ->leftJoin('est_agr_type', 'est_agreement.est_agr_type_id', '=', 'est_agr_type.est_agr_type_id')
            ->leftJoin('est_agr_onus', 'est_agreement.est_agr_onus_id', '=', 'est_agr_onus.est_agr_onus_id')
            ->orderBy('est_agreement.est_agreement_code', 'ASC');
    }

    private function formatInputData(&$inputs)
    {
        foreach ($inputs as $key => $value) {
            if ($value === "") {
                unset($inputs[$key]);
            }
        }
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $valueAuthorisedDate = $reportFilterQuery->getFirstValueFilterField("authorised_date");
            $inputs['authorisedDateFrom'] = Common::dateFieldDisplayFormat(Arr::first($valueAuthorisedDate));
            $inputs['authorisedDateTo'] = Common::dateFieldDisplayFormat(Arr::last($valueAuthorisedDate));

            $valueAgreementDate = $reportFilterQuery->getFirstValueFilterField("agreement_date");
            $inputs['agreementDateFrom'] = Common::dateFieldDisplayFormat(Arr::first($valueAgreementDate));
            $inputs['agreementDateTo'] = Common::dateFieldDisplayFormat(Arr::last($valueAgreementDate));

            $valueTerminationDate = $reportFilterQuery->getFirstValueFilterField("termination_date");
            $inputs['terminationDateFrom'] = Common::dateFieldDisplayFormat(Arr::first($valueTerminationDate));
            $inputs['terminationDateTo'] = Common::dateFieldDisplayFormat(Arr::last($valueTerminationDate));

            $valueHoldingOverDate = $reportFilterQuery->getFirstValueFilterField("holding_over_date");
            $inputs['holdingOverFrom'] = Common::dateFieldDisplayFormat(Arr::first($valueHoldingOverDate));
            $inputs['holdingOverTo'] = Common::dateFieldDisplayFormat(Arr::last($valueHoldingOverDate));

            $valueAreaValue = $reportFilterQuery->getFirstValueFilterField("area_value");
            $inputs['areaFrom'] = !empty(Arr::first($valueAreaValue)) ? Arr::first($valueAreaValue) : null;
            $inputs['areaTo'] = !empty(Arr::last($valueAreaValue)) ? Arr::last($valueAreaValue) : null;

            $valueStartDate = $reportFilterQuery->getFirstValueFilterField("start_date");
            $inputs['startDateFrom'] = Common::dateFieldDisplayFormat(Arr::first($valueStartDate));
            $inputs['startDateTo'] = Common::dateFieldDisplayFormat(Arr::last($valueStartDate));

            $valueEndDate = $reportFilterQuery->getFirstValueFilterField("end_date");
            $inputs['endDateFrom'] = Common::dateFieldDisplayFormat(Arr::first($valueEndDate));
            $inputs['endDateTo'] = Common::dateFieldDisplayFormat(Arr::last($valueEndDate));


            $leaseOutStatusType = $reportFilterQuery->getValueFilterField("lease_out_status_type_id") ?? [];
            $inputs["lostActive"] = in_array(LeaseOutStatusType::ACTIVE, $leaseOutStatusType) ?
                LeaseOutStatusType::ACTIVE : null;
            $inputs["lostInactive"] = in_array(LeaseOutStatusType::INACTIVE, $leaseOutStatusType) ?
                LeaseOutStatusType::INACTIVE : null;
            $inputs["lostArchive"] = in_array(LeaseOutStatusType::ARCHIVE, $leaseOutStatusType) ?
                LeaseOutStatusType::ARCHIVE : null;
            $inputs["lostTerminated"] = in_array(LeaseOutStatusType::TERMINATED, $leaseOutStatusType) ?
                LeaseOutStatusType::TERMINATED : null;

            $inputs['code'] = $reportFilterQuery->getFirstValueFilterField("lease_out_code");
            $inputs['site_id'] = $reportFilterQuery->getFirstValueFilterField("site_id");
            $inputs['owner'] = $reportFilterQuery->getFirstValueFilterField("owner_user_id");
            $inputs['leaseType'] = $reportFilterQuery->getFirstValueFilterField("lease_type_id");
            $inputs['leaseSubType'] = $reportFilterQuery->getFirstValueFilterField("est_lease_sub_type_id");
            $inputs['areaUnits'] = $reportFilterQuery->getFirstValueFilterField("area_unit_of_area_id");
            $inputs['holdingOver'] = $reportFilterQuery->getFirstValueFilterField("holding_over");
            $inputs['insce_liability_id'] = $reportFilterQuery->getFirstValueFilterField("insce_liability_id");
            $inputs['landlord'] = $reportFilterQuery->getFirstValueFilterField("landlord_contact_id");
            $inputs['landlordTenantAct'] = $reportFilterQuery->getFirstValueFilterField("landlord_tenant_act");
            $inputs['description'] = $reportFilterQuery->getFirstValueFilterField("lease_out_desc");
            $inputs['estate_maint_id'] = $reportFilterQuery->getFirstValueFilterField("estate_maint_id");
            $inputs['rates_liability_id'] = $reportFilterQuery->getFirstValueFilterField("rates_liability_id");
            $inputs['ruralLeases'] = $reportFilterQuery->getFirstValueFilterField("ruralest_lease");
            $inputs['contact'] = $reportFilterQuery->getFirstValueFilterField("tenant_contact_id");
            $inputs['vatIncluded'] = $reportFilterQuery->getFirstValueFilterField("vat_included");
            $inputs['periodOfLeaseFlt'] = $reportFilterQuery->getFirstValueFilterField("period_of_lease_id");
            $inputs['leaseOutStatus'] = $reportFilterQuery->getFirstValueFilterField("lease_out_status_id");
        }
        return $inputs;
    }

    private function getLeaseOutCharges($leaseOutId)
    {
        return EstCharge::userSiteGroup()
            ->leftJoin('lease_out', 'lease_out.lease_out_id', '=', 'est_charge.lease_out_id')
            ->leftJoin('est_charge_type', 'est_charge.est_charge_type_id', '=', 'est_charge_type.est_charge_type_id')
            ->leftJoin(
                'est_time_period',
                'est_charge.repeat_est_time_period_id',
                '=',
                'est_time_period.est_time_period_id'
            )
            ->leftJoin(
                'tax_code',
                'tax_code.tax_code_id',
                '=',
                'est_charge.tax_code_id'
            )
            ->where('est_charge.lease_out_id', $leaseOutId)
            ->where('est_charge.active', CommonConstant::DATABASE_VALUE_YES)
            ->where('est_charge.credit_note', CommonConstant::DATABASE_VALUE_NO)
            ->select([
                'est_charge.est_charge_id',
                'est_charge.est_charge_code',
                'est_charge.est_charge_desc',
                'est_charge.rent',
                'est_charge.annualised_rent',
                'est_charge.credit_note',
                DB::raw(
                    "IF((ISNULL(est_charge_type.est_charge_type_code) AND est_charge.rent = 'Y'),
                    'Rent', est_charge_type.est_charge_type_code) AS est_charge_type_code"
                ),
                'est_charge_type.est_charge_type_desc',
                'est_charge_start_date',
                'est_charge_end_date',
                'est_charge.regular_payment_start_date',
                'est_charge.regular_payment_end_date',
                'est_charge.initial_payment_amount',
                'est_charge.initial_payment_due_date',
                'est_charge.fin_account_id',
                'est_charge.period_freq',
                'est_charge_calc_end_point_id',
                'est_time_period.est_time_period_code',
                'est_time_period.est_time_period_desc',
                'est_charge.regular_payment_amount',
                'est_charge.last_invoice_generated_date',
                'tax_code.tax_code_id',
                'tax_code.rate',
                DB::raw(
                    'IF(est_charge.credit_note = "' . CommonConstant::DATABASE_VALUE_YES . '",
                    est_charge.initial_payment_amount, est_charge.regular_payment_amount) as instalment'
                ),
                'est_charge.initial_payment_due_date',
                DB::raw(
                    'IF(est_charge.active ="'
                    . CommonConstant::DATABASE_VALUE_YES
                    . '","Active", "Inactive") as status'
                ),
                DB::raw(
                    'IF(est_charge.payment_in_advance ="'
                    . CommonConstant::DATABASE_VALUE_YES
                    . '","In Advance", "In Arrears") as terms'
                ),
                DB::raw(
                    'IF(est_charge.tax_code_id IS NOT NULL,
                    est_charge.regular_payment_amount * tax_code.rate / 100,
                    0) as vat_amount'
                )
            ])
            ->orderBy('est_charge.rent', 'DESC')
            ->orderBy('est_charge.regular_payment_start_date', 'ASC');
    }

    private function getTotalCharge($leaseOut, $nextYearDate)
    {
        $invoicePlanItem = $this->invoiceGenerationPlanService->getPlanItem([
            'lease_out_id' => $leaseOut->lease_out_id,
            'process_up_to_date' => $nextYearDate
        ]);
        $months = $this->getMonths();
        $totalGross = 0;
        $annualNet = 0;
        $annualVat = 0;

        foreach ($months as &$month) {
            foreach ($invoicePlanItem as $item) {
                $dueDate = new DateTime($item['charge_due_date']);
                $dueDateMonth = $dueDate->format('M');
                $dueDateYear = $dueDate->format('Y');
                if (($dueDateMonth == $month['month']) && ($dueDateYear == $month['year'])) {
                    $month['amount'] += $item['amount'];
                    $totalGross += $item['amount'];
                    if ($item['is_rent']) {
                        $annualNet += $item['net_amount'];
                        $annualVat += $item['vat_amount'];
                    }
                }
            }
        }
        return [$months, $totalGross, $annualNet, $annualVat];
    }

    private function getMonths()
    {
        $months = [];
        for ($i = 0; $i < 12; $i++) {
            $now = new DateTime(date('1-m-Y'));
            $month = $now->add(\DateInterval::createFromDateString("{$i} month"));
            $monthValue = $month->format('M');
            $yearValue = $month->format('Y');
            array_push(
                $months,
                [
                    'month' => $monthValue,
                    'year'  => $yearValue,
                    'amount' => 0
                ]
            );
        }
        return $months;
    }

    private function getLeaseOutReviews($leaseOutId)
    {
        $thisYear = new DateTime(date('1-1-Y'));
        $endYear = clone $thisYear;
        $endYear->add(\DateInterval::createFromDateString('3 year'));
        $endYear->sub(\DateInterval::createFromDateString('1 day'));
        $reviewStartDate = Common::dateFieldToSqlFormat(
            $thisYear->format(CommonConstant::FORMAT_DATE),
            ['includeTime' => false]
        );

        $reviewEndDate = Common::dateFieldToSqlFormat(
            $endYear->format(CommonConstant::FORMAT_DATE),
            ['includeTime' => false]
        );

        return EstReview::userSiteGroup()
            ->leftJoin('lease_out', 'lease_out.lease_out_id', '=', 'est_review.lease_out_id')
            ->leftJoin('user AS parentuser', 'lease_out.owner_user_id', '=', 'parentuser.id')
            ->leftJoin('user AS reviewuser', 'est_review.review_owner_user_id', '=', 'reviewuser.id')
            ->leftJoin('est_review_type', 'est_review.est_review_type_id', '=', 'est_review_type.est_review_type_id')
            ->leftJoin(
                'est_review_status',
                'est_review.est_review_status_id',
                '=',
                'est_review_status.est_review_status_id'
            )
            ->leftJoin('est_charge', 'est_charge.est_charge_id', '=', 'est_review.est_charge_id')
            ->leftJoin(
                'est_charge_type',
                'est_charge.est_charge_type_id',
                '=',
                'est_charge_type.est_charge_type_id'
            )
            ->where('est_review.lease_out_id', $leaseOutId)
            ->where(
                'est_review.review_date',
                '>=',
                $reviewStartDate
            )
            ->where(
                'est_review.review_date',
                '<=',
                $reviewEndDate
            )
            ->select([
                'est_review.est_review_id',
                'est_review.est_review_code',
                'est_review_status.est_review_status_code',
                'est_review_type.est_review_type_code',
                'est_review_type.est_review_type_desc',
                'est_review.notify_date',
                'est_review.review_date',
                'est_review.complete',
                'est_review.complete_date',
                'est_review.comments',
                'est_review.agreed_date',
                'est_review.agreed_amount',
                'review_owner_user_id',
                'parentuser.display_name',
                DB::raw(
                    "IF((ISNULL(est_charge_type.est_charge_type_code) AND est_charge.rent = 'Y'),
                    'Rent', est_charge_type.est_charge_type_code) AS est_charge_type_code"
                ),
                'est_charge_type.est_charge_type_desc',
                DB::raw("IFNULL(reviewuser.display_name, parentuser.display_name) owner")
            ]);
    }

    private function generateFilterData($filterData, &$whereCodes, &$whereTexts, &$orCodes)
    {
        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Lease Out Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Lease Out Description contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);
        }

        if ($val = Common::iset($filterData['owner'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Owner']);
        }

        if ($val = Common::iset($filterData['holdingOver'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, Lang::get('text.report_texts.holding_over_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, Lang::get('text.report_texts.holding_over_no'));
                    break;
                default:
                    array_push($whereTexts, Lang::get('text.report_texts.holding_over_all'));
                    break;
            }
        }

        if ($val = array_get($filterData, 'holdingOverFrom', null)) {
            array_push($whereTexts, sprintf(Lang::get('text.report_texts.holding_over_from'), $val));
        }

        if ($val = array_get($filterData, 'holdingOverTo', null)) {
            array_push($whereTexts, sprintf(Lang::get('text.report_texts.holding_over_to'), $val));
        }

        if ($val = array_get($filterData, 'agreementDateFrom', null)) {
            array_push($whereTexts, sprintf(Lang::get('text.report_texts.agreement_date_from'), $val));
        }

        if ($val = array_get($filterData, 'agreementDateTo', null)) {
            array_push($whereTexts, sprintf(Lang::get('text.report_texts.agreement_date_to'), $val));
        }

        if ($val = array_get($filterData, 'authorisedDateFrom', null)) {
            array_push($whereTexts, sprintf(Lang::get('text.report_texts.authorised_date_from'), $val));
        }

        if ($val = array_get($filterData, 'authorisedDateTo', null)) {
            array_push($whereTexts, sprintf(Lang::get('text.report_texts.authorised_date_to'), $val));
        }

        if ($val = array_get($filterData, 'terminationDateFrom', null)) {
            array_push($whereTexts, sprintf(Lang::get('text.report_texts.termination_date_from'), $val));
        }

        if ($val = array_get($filterData, 'terminationDateTo', null)) {
            array_push($whereTexts, sprintf(Lang::get('text.report_texts.termination_date_to'), $val));
        }

        if ($val = array_get($filterData, 'startDateFrom', null)) {
            array_push($whereTexts, sprintf(Lang::get('text.report_texts.start_date_from'), $val));
        }

        if ($val = array_get($filterData, 'startDateTo', null)) {
            array_push($whereTexts, sprintf(Lang::get('text.report_texts.start_date_to'), $val));
        }

        if ($val = array_get($filterData, 'endDateFrom', null)) {
            array_push($whereTexts, sprintf(Lang::get('text.report_texts.end_date_from'), $val));
        }

        if ($val = array_get($filterData, 'endDateTo', null)) {
            array_push($whereTexts, sprintf(Lang::get('text.report_texts.end_date_to'), $val));
        }

        if ($val = Common::iset($filterData['rates_liability_id'])) {
            array_push($whereCodes, ['rates_liability', 'rates_liability_id', 'rates_liability_code', $val, "Rates"]);
        }

        if ($val = Common::iset($filterData['insce_liability_id'])) {
            array_push(
                $whereCodes,
                [
                    'insce_liability',
                    'insce_liability_id',
                    'insce_liability_code',
                    $val,
                    "Insurance"
                ]
            );
        }

        if ($val = Common::iset($filterData['estate_maint_id'])) {
            array_push($whereCodes, ['estate_maint', 'estate_maint_id', 'estate_maint_code', $val, "Maintenance"]);
        }

        if ($val = Common::iset($filterData['ruralLeases'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, Lang::get('text.report_texts.rural_leases_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, Lang::get('text.report_texts.rural_leases_no'));
                    break;
                default:
                    array_push($whereTexts, Lang::get('text.report_texts.rural_leases_all'));
                    break;
            }
        }

        if ($val = Common::iset($filterData['leaseOutStatus'])) {
            array_push(
                $whereCodes,
                ['lease_out_status', 'lease_out_status_id', 'lease_out_status_code', $val, 'Status']
            );
        }

        if ($val = Common::iset($filterData['landlord'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Landlord']);
        }

        if ($val = Common::iset($filterData['contact'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Tenant']);
        }

        if ($val = Common::iset($filterData['leaseType'])) {
            array_push($whereCodes, ['lease_type', 'lease_type_id', 'lease_type_code', $val, 'Lease Type']);
        }

        if ($val = Common::iset($filterData['leaseSubType'])) {
            array_push(
                $whereCodes,
                ['est_lease_sub_type', 'est_lease_sub_type_id', 'est_lease_sub_type_code', $val, 'Lease Sub Type']
            );
        }

        if ($val = array_get($filterData, 'areaFrom', null)) {
            array_push($whereTexts, sprintf(Lang::get('text.report_texts.area_from'), $val));
        }

        if ($val = array_get($filterData, 'areaTo', null)) {
            array_push($whereTexts, sprintf(Lang::get('text.report_texts.area_to'), $val));
        }

        if ($val = Common::iset($filterData['areaUnits'])) {
            array_push($whereCodes, ['unit_of_area', 'unit_of_area_id', 'unit_of_area_code', $val, "Area Units"]);
        }

        if ($val = Common::iset($filterData['vatIncluded'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, Lang::get('text.report_texts.vat_included_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, Lang::get('text.report_texts.vat_included_no'));
                    break;
                default:
                    array_push($whereTexts, Lang::get('text.report_texts.vat_included_all'));
                    break;
            }
        }

        if ($val = Common::iset($filterData['landlordTenantAct'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, Lang::get('text.report_texts.landlord_tenant_act_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, Lang::get('text.report_texts.landlord_tenant_act_no'));
                    break;
                default:
                    array_push($whereTexts, Lang::get('text.report_texts.landlord_tenant_act_all'));
                    break;
            }
        }

        $this->reAddUserDefinesQuery(GenTable::LEASE_OUT, $filterData, $whereCodes, $whereTexts);
    }

    public function getData($inputs)
    {
        $leaseOuts = $this->getAll($inputs)->groupBy('lease_out_id')->get();
        $chargeStartDate = (new DateTime(date('1-m-Y')));
        $nextYearDate = clone $chargeStartDate;
        $nextYearDate = $nextYearDate->add(\DateInterval::createFromDateString('1 year'));
        $nextYearDate = $nextYearDate->sub(\DateInterval::createFromDateString('1 day'))->format('Y-m-d');
        foreach ($leaseOuts as $leaseOut) {
            $agreements = $this->getAgreements($leaseOut->lease_out_id)->get();
            $tenantDetail = !is_null($leaseOut->tenant_contact_id) ?
                $this->contactService->get($leaseOut->tenant_contact_id) : null;
            $owner = $this->userService->get($leaseOut->owner_user_id);
            $chargeRents = $this->getLeaseOutCharges($leaseOut->lease_out_id)->get();

            $allCharges = $this->getTotalCharge($leaseOut, $nextYearDate);
            $calcInvoices = $this->estateSalesInvoiceService
                ->getAll(['lease_out_id' => $leaseOut->lease_out_id], ['checkPermission' => false])
                ->where('est_sales_invoice.est_sales_invoice_status_id', '<>', EstSalesInvoiceStatus::CANCELLED)
                ->get()
                ->sum('os_amount');
            $lettableUnitList = $this->lettableUnitService
                ->getLettableUnitBelongToLeaseOut([], $leaseOut->lease_out_id)->get();

            $leaseOut->lettable_unit_codes = null;
            $leaseOut->lease_address = null;

            if (isset($lettableUnitList) && ($lettableUnitList->count() > 0)) {
                $lettableUnitCodes = [];

                foreach ($lettableUnitList as $lettableUnit) {
                    if (is_null($lettableUnit->lettable_unit_desc)) {
                        $leaseOutLetu = $lettableUnit->lettable_unit_code;
                    } else {
                        $leaseOutLetu = $lettableUnit->lettable_unit_code . ' - ' . $lettableUnit->lettable_unit_desc;
                    }
                    array_push($lettableUnitCodes, $leaseOutLetu);
                }

                $leaseOut->lettable_unit_codes = !empty($lettableUnitCodes) ? implode(", ", $lettableUnitCodes) : null;

                // Determine whether we have more than one different address
                $oneAddress = true;
                for ($i = 1; $i < $lettableUnitList->count(); $i++) {
                    if (
                        $lettableUnitList[0]->site_id != $lettableUnitList[$i]->site_id
                        || $lettableUnitList[0]->building_id != $lettableUnitList[$i]->building_id
                    ) {
                        $oneAddress = false;
                        break;
                    }
                }

                $leaseOut->one_address = $oneAddress;

                if ($leaseOut->one_address && $lettableUnitList[0]->site_id) {
                    if ($lettableUnitList[0]->building_id) {
                        $building = $this->buildingService->get($lettableUnitList[0]->building_id, false);
                        $leaseOut->lease_address = implode(
                            "<br>",
                            array_filter(
                                [
                                    $building->address->second_addr_obj,
                                    $building->address->first_addr_obj,
                                    $building->address->addressStreet->street,
                                    $building->address->addressStreet->locality,
                                    $building->address->addressStreet->town,
                                    $building->address->addressStreet->region,
                                    $building->address->postcode,
                                    $building->address->country,
                                ]
                            )
                        );
                    } else {
                        $site = $this->siteService->get($lettableUnitList[0]->site_id, false);
                        $leaseOut->lease_address = implode(
                            "<br>",
                            array_filter(
                                [
                                    $site->address->second_addr_obj,
                                    $site->address->first_addr_obj,
                                    $site->address->addressStreet->street,
                                    $site->address->addressStreet->locality,
                                    $site->address->addressStreet->town,
                                    $site->address->addressStreet->region,
                                    $site->address->postcode,
                                    $site->address->country,
                                ]
                            )
                        );
                    }
                } else {
                    $leaseAddress = [];
                    foreach ($lettableUnitList as $lettableUnit) {
                        if ($lettableUnit->building_id) {
                            $building = $this->buildingService->get($lettableUnit->building_id, false);
                            $lettableUnitAddress = array_filter(
                                [
                                    $building->address->second_addr_obj,
                                    $building->address->first_addr_obj,
                                    $building->address->addressStreet->street,
                                    $building->address->addressStreet->locality,
                                    $building->address->addressStreet->town,
                                    $building->address->addressStreet->region,
                                    $building->address->postcode,
                                    $building->address->country
                                ]
                            );
                            $renderedLetuAddress = implode(", ", $lettableUnitAddress);
                            array_push($leaseAddress, $renderedLetuAddress);
                        } elseif ($lettableUnit->site_id) {
                            $site = $this->siteService->get($lettableUnit->site_id, false);
                            $lettableUnitAddress = array_filter(
                                [
                                    $site->address->second_addr_obj,
                                    $site->address->first_addr_obj,
                                    $site->address->addressStreet->street,
                                    $site->address->addressStreet->locality,
                                    $site->address->addressStreet->town,
                                    $site->address->addressStreet->region,
                                    $site->address->postcode,
                                    $site->address->country
                                ]
                            );
                            $renderedLetuAddress = implode(", ", $lettableUnitAddress);
                            array_push($leaseAddress, $renderedLetuAddress);
                        }
                    }
                    $leaseAddress = array_unique($leaseAddress);
                    $renderedLeaseAddress = implode(
                        "<br><br>",
                        array_filter($leaseAddress)
                    );
                    $leaseOut->lease_address = $renderedLeaseAddress;
                }
            }
            if (isset($chargeRents) && ($chargeRents->count() > 0)) {
                foreach ($chargeRents as $chargeRent) {
                    $invoicePlanItem = $this->invoiceGenerationPlanService->getPlanItem([
                        'lease_out_id' => $leaseOut->lease_out_id,
                        'process_up_to_date' => $nextYearDate,
                        'est_charge_id' => $chargeRent->est_charge_id
                    ]);
                    $chargeRent->next_charge_date = null;
                    if (isset($invoicePlanItem) && (!empty($invoicePlanItem))) {
                        $chargeRent->next_charge_date =
                            $chargeRent->last_invoice_generated_date ? $invoicePlanItem[0]['charge_due_date'] : null;
                    }
                }
            }

            $leaseOut->os_amount = $calcInvoices;

            $leaseOut->allCharges = $allCharges[0];
            $leaseOut->totalGross = $allCharges[1];

            if (isset($agreements) && ($agreements->count() > 0)) {
                $leaseOut->agreements = $agreements;
            }

            if (isset($tenantDetail) && ($tenantDetail->count() > 0)) {
                $tenantAddress = implode(
                    "<br>",
                    array_filter(
                        [
                            $tenantDetail->address->second_addr_obj,
                            $tenantDetail->address->first_addr_obj,
                            $tenantDetail->address->addressStreet->street,
                            $tenantDetail->address->addressStreet->locality,
                            $tenantDetail->address->addressStreet->town,
                            $tenantDetail->address->addressStreet->region,
                            $tenantDetail->address->postcode,
                            $tenantDetail->address->country,
                        ]
                    )
                );
                $leaseOut->tennant_address = $tenantAddress;
                $leaseOut->tennant_phone_main = $tenantDetail->phone_main;
                $leaseOut->tennant_phone_mobile = $tenantDetail->phone_mobile;
                $leaseOut->tennant_email_addr = $tenantDetail->email_addr;
            }

            if (isset($owner) && ($owner->count() > 0)) {
                $leaseOut->owner = $owner;
            }
            $accountId = null;
            $startDate = null;
            $leaseOut->activeRents = $this->getLeaseOutCharges($leaseOut->lease_out_id)
                ->where('est_charge.rent', CommonConstant::DATABASE_VALUE_YES)
                ->get();
            if (isset($chargeRents) && ($chargeRents->count() > 0)) {
                $leaseOut->charge_rent = $chargeRents;
                $leaseOut->charge_rent_start_date = $chargeStartDate->format(CommonConstant::FORMAT_DATE);
                $leaseOut->charge_rent_end_date = $nextYearDate;
                if (isset($leaseOut->activeRents) && ($leaseOut->activeRents->count() > 0)) {
                    if (!is_null($leaseOut->activeRents[0]->fin_account_id)) {
                        $accountId = $leaseOut->activeRents[0]->fin_account_id;
                        for ($i = 1; $i < $leaseOut->activeRents->count(); $i++) {
                            if ($leaseOut->activeRents[$i]->fin_account_id != $accountId) {
                                $accountId = null;
                                break;
                            }
                        }
                    }
                    if (!is_null($leaseOut->activeRents[0]->regular_payment_start_date)) {
                        $startDate = $leaseOut->activeRents[0]->regular_payment_start_date;
                        for ($i = 1; $i < $leaseOut->activeRents->count(); $i++) {
                            if ($leaseOut->activeRents[$i]->regular_payment_start_date != $startDate) {
                                $startDate = null;
                                break;
                            }
                        }
                    }
                }
            }

            $leaseOut->rentStart = $startDate;

            $sum = 0;
            $vat = 0;
            if (isset($leaseOut->activeRents) && ($leaseOut->activeRents->count() > 0)) {
                foreach ($leaseOut->activeRents as $activeRent) {
                    // CLD-15497: Does the charge cover the current date.
                    // We don't want to include historic active or future active charges.
                    $date = date('Y-m-d');

                    // Have we got a regular payment start date.
                    if ($activeRent->regular_payment_start_date) {
                        // Do we have both regular payment start date and end date
                        if ($activeRent->regular_payment_start_date && $activeRent->regular_payment_end_date) {
                            $regStartDate = $activeRent->regular_payment_start_date;
                            $regEndDate = $activeRent->regular_payment_end_date;
                        } else {
                            // Only start date so need to cater for 'further notice'
                            $regStartDate = $activeRent->regular_payment_start_date;
                            // Just set end date to today to satisify the date range logic below.
                            $regEndDate = $date;
                        }
                    } else {
                        // use charge start and end date instead.
                        $regStartDate = $activeRent->est_charge_start_date;
                        $regEndDate = $activeRent->est_charge_end_date;
                        // If we have no reg start date we need to check that its a manual payment that is realy
                        // until further notice.  Can't rely on value from query.
                        $calEndPoint = $this->getEndPoint($activeRent->est_charge_id);
                        if ($calEndPoint == EstChargeCalcEndPoint::FURTHER_NOTICE) {
                            // Just set end date to today to satisify the date range logic below.
                            $regEndDate = $date;
                        }
                    }

                    // We only want to sum the annualised rent for charges that include todays date.
                    if ($date >= $regStartDate && $date <= $regEndDate) {
                        $sum += $activeRent->annualised_rent;
                        if (!is_null($activeRent->tax_code_id)) {
                            $vat = $vat + ($activeRent->rate / 100 * $activeRent->annualised_rent);
                        }
                    }
                }
            }

            $leaseOut->annual_net = $sum;
            $leaseOut->annual_vat = $vat;

            if (!is_null($accountId)) {
                $accountCode = FinAccount::find($accountId);
                $leaseOut->accountId = !empty($accountCode) ? $accountCode->fin_account_code : null;
            }

            $leaseOut->rentReviews = $this->getLeaseOutReviews($leaseOut->lease_out_id)
                ->where('est_review_type.financial_review', CommonConstant::DATABASE_VALUE_YES)
                ->orderBy('est_review.review_date', 'ASC')
                ->get();

            $leaseOut->reviews = $this->getLeaseOutReviews($leaseOut->lease_out_id)
                ->where('est_review_type.financial_review', CommonConstant::DATABASE_VALUE_NO)
                ->orderBy('est_review.review_date', 'ASC')
                ->get();

            if (isset($leaseOut->length_years) && ($leaseOut->length_years > 1)) {
                $leaseOut->length_years .= ' Years';
            } elseif (isset($leaseOut->length_years) && ($leaseOut->length_years == 1)) {
                $leaseOut->length_years .= ' Year';
            }

            if (isset($leaseOut->length_months) && ($leaseOut->length_months > 1)) {
                $leaseOut->length_months .= ' Months';
            } elseif (isset($leaseOut->length_months) & ($leaseOut->length_months == 1)) {
                $leaseOut->length_months .= ' Month';
            }

            if (isset($leaseOut->length_days) && ($leaseOut->length_days > 1)) {
                $leaseOut->length_days .= ' Days';
            } elseif (isset($leaseOut->length_days) && ($leaseOut->length_days == 1)) {
                $leaseOut->length_days .= ' Day';
            }

            $length = [$leaseOut->length_years, $leaseOut->length_months, $leaseOut->length_days];
            $length = implode(
                ", ",
                array_filter($length)
            );
            $leaseOut->length = $length;

            // Rolling Break Dates.
            $bdInputs['est_break_parent_type_id'] = EstBreakDateParentType::EST_BD_PARENT_LI;
            $bdInputs['parent_id'] = $leaseOut->lease_out_id;

            $leaseOut->breakDates = $this->estBreakDateService->getAll($bdInputs)->get();
        }

        return $leaseOuts;
    }

    private function getEndPoint($id)
    {
        $paymentSummary = new PaymentSummary();

        $estChargeService = new EstChargeService(new PermissionService());

        $estCharge = $estChargeService->get($id);

        $paymentSummary->initialAmount = $estCharge->initialPaymentAmount;
        $paymentSummary->initialDate = $estCharge->initialDate;
        $paymentSummary->daysBefore = $estCharge->daysBefore;
        $paymentSummary->initialDays = $estCharge->initialPeriodDays;
        $paymentSummary->finalAmount = $estCharge->finalPaymentAmount;
        $paymentSummary->finalDate = $estCharge->finalDate;
        $paymentSummary->daysAfter = $estCharge->daysAfter;
        $paymentSummary->finalDays = $estCharge->finalPeriodDays;
        $paymentSummary->ptAdvance = $estCharge->rpAdvance;
        $paymentSummary->rpPeriodFreq = $estCharge->rpPeriodFreq;
        $paymentSummary->rpTimePeriodId = $estCharge->rpTimePeriod;
        $paymentSummary->actualInstalmentAmount = $estCharge->actualInstalmentAmount;
        $paymentSummary->anniversaryPayment = $estCharge->anniversaryPayment;
        $paymentSummary->anniversary = $estCharge->anniversary;
        $paymentSummary->ptCalcEndPoint = $estCharge->ptCalcEndPoint;
        $paymentSummary->periods = $estCharge->periods;
        $paymentSummary->paymentDate = $estCharge->paymentStartDate;
        $paymentSummary->lastPaymentDate = $estCharge->lastRegPaymentDate;
        $paymentSummary->luFromAnnualRent = $estCharge->luOriginalAnnualRent;
        $paymentSummary->luToAnnualRent = $estCharge->luAnnualRent;

        $paymentSummary->getSummmary(true);

        return $paymentSummary->ptCalcEndPoint;
    }
}
