<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use Tfcloud\Controllers\Admin\General\SiteGroupsController;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Constant\ReportConstant;
use Tfcloud\Lib\Filters\Plant\PlantFilter;
use Tfcloud\Lib\Math;
use Tfcloud\Lib\Reporting\ReportCommon;
use Tfcloud\Models\GenTable;
use Tfcloud\Models\Plant;
use Tfcloud\Services\Admin\General\SiteGroupsService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Property\PlantCostService;
use Tfcloud\Services\Property\PlantService;

class PLPlantActivityReportService extends WkHtmlToPdfReportBaseService
{
    private $plantService;
    private $plantCostService;
    private $sgController;
    private $sgService;

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->plantService = new PlantService($permissionService);
        $this->plantCostService = new PlantCostService($permissionService);
        $this->sgService = new SiteGroupsService($permissionService);
        $this->sgController = new SiteGroupsController($permissionService, $this->sgService);
    }

    public function runReportSinglePlant($id)
    {
        //call report from button not report view
        $inputs['code'] = $id;
        $inputs['singleOnly'] = true;

        $repId = ReportCommon::getSystemReportByCode(
            $this->permissionService,
            ReportConstant::SYSTEM_REPORT_PL06
        )->system_report_id;

        return $this->runReport(
            $this->getTempFile(),
            $inputs,
            $repId
        );
    }

    public function runReport($repOutPutPdfFile, $inputs, $repId)
    {
        $siteGroupId = \Auth::User()->site_group_id;
        $photoPath = $this->sgController->getPhotoPath($siteGroupId);

        $plantQuery = Plant::select()
                ->orderBy('plant.plant_id');
        $filteredPlantQuery = $this->setFilters($plantQuery, $inputs);
        $plants = $filteredPlantQuery->get();

        $showSingleHeader = 'false';

        foreach ($plants as $plant) {
            $activities = $this->plantService->getPlantActivities(['plant_id' => $plant->getKey()]);

            $grandTotal = 0;

            foreach ($activities as $activity) {
                $totalCost = $this->plantCostService->getPlantItemCost($activity->id, $activity->record_type);
                $grandTotal = Math::addCurrency([$grandTotal, $totalCost]);
                $activity->total_cost = $totalCost;
            }
        }

        // Create wkhtmltopdf temp folder to store generated files
        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);

        $header = \View::make(
            'reports.wkhtmltopdf.plant.plantActivity.header',
            [
                'plants'      => $plants,
                'activities'  => $activities,
                'photoPath'   => $photoPath,
                'assetHeader' => $showSingleHeader
            ]
        )->render();

        $headerPath = $generatedPath . "/header.html";
        file_put_contents($headerPath, $header, LOCK_EX);

        $footer = \View::make('reports.wkhtmltopdf.plant.plantActivity.footer')->render();
        $footerPath = $generatedPath . "/footer.html";
        file_put_contents($footerPath, $footer, LOCK_EX);

        $content = \View::make(
            'reports.wkhtmltopdf.plant.plantActivity.content',
            [
                'plants'      => $plants,
                'activities'  => $activities,
                'grandTotal'  => $grandTotal
            ]
        )->render();

        $contentPath = $generatedPath . "/content.html";

        file_put_contents($contentPath, $content, LOCK_EX);

        $options = [
            'header-html' => $headerPath,
            'footer-html' => $footerPath,
            'header-spacing' => 4,
            'footer-spacing' => 4,
            'orientation' => 'landscape',
        ];

        $repOutPutPdfFile .= '.pdf';
        $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $options);
        return $repOutPutPdfFile;
    }

    private function getTempFile()
    {
        return tempnam(Common::getTempFolder(), 'PLPlantActivity');
    }

    private function setFilters($query, $inputs)
    {
        $filter = new PlantFilter($inputs);
        $table = 'plant';

        if (array_key_exists('singleOnly', $inputs)) {
            //running from single plant disqualifies other filters
            if ($inputs['code'] != "") {
                $query->where(
                    'plant.plant_id',
                    '=',
                    $inputs['code']
                );
            }
        } else {
            if ($inputs['code'] != "") {
                $query->where(
                    'plant.plant_code',
                    '=',
                    $inputs['code']
                );
            }

            if ($inputs['plant_group_id'] != "") {
                $query->where(
                    'plant.plant_group_id',
                    '=',
                    $inputs['plant_group_id']
                );

                if ($inputs['plant_subgroup_id'] != "") {
                    $query->where(
                        'plant.plant_subgroup_id',
                        '=',
                        $inputs['plant_subgroup_id']
                    );

                    if ($inputs['plant_type_id'] != "") {
                        $query->where(
                            'plant.plant_type_id',
                            '=',
                            $inputs['plant_type_id']
                        );
                    }
                }
            } //end group id if

            if ($inputs['description'] != "") {
                $query->where(
                    'plant.plant_desc',
                    '=',
                    $inputs['description']
                );
            }

            if ($inputs['site_id'] != "") {
                $query->where(
                    'plant.site_id',
                    '=',
                    $inputs['site_id']
                );

                if ($inputs['building_id'] != "") {
                    $query->where(
                        'plant.building_id',
                        '=',
                        $inputs['building_id']
                    );

                    if ($inputs['room_id'] != "") {
                        $query->where(
                            'plant.room_id',
                            '=',
                            $inputs['room_id']
                        );
                    } //end room if
                } //end building if
            } //end site if

            if ($inputs['plant_criticality_id'] != "") {
                $query->where(
                    'plant.plant_criticality_id',
                    '=',
                    $inputs['plant_criticality_id']
                );
            }

            if ($inputs['plant_condition_id'] != "") {
                $query->where(
                    'plant.plant_condition_id',
                    '=',
                    $inputs['plant_condition_id']
                );
            }

            if ($inputs['status'] != "all") {
                $query->where(
                    'plant.active',
                    '=',
                    $inputs['status']
                );
            }

            // plantKind will not be defined if Grounds Maintenance module is switched off
            $plantKind = $inputs['plantKind'] ?? Plant::KIND_ALL;
            if ($plantKind != Plant::KIND_ALL) {
                $gmPlant = null;
                if ($plantKind == Plant::KIND_GENERAL) {
                    $gmPlant = CommonConstant::DATABASE_VALUE_NO;
                } elseif ($plantKind == Plant::KIND_GROUNDS_MAINTENANCE) {
                    $gmPlant = CommonConstant::DATABASE_VALUE_YES;
                }
                if ($gmPlant) {
                    $query->where(
                        'plant.gm_plant',
                        '=',
                        $gmPlant
                    );
                }
            }

            if ($inputs['contact'] != "") {
                $query->where(
                    'plant.maint_contact_id',
                    '=',
                    $inputs['contact']
                );
            }

            if ($inputs['owner'] != "") {
                $query->where(
                    'plant.owner_user_id',
                    '=',
                    $inputs['owner']
                );
            }

            // Replacement Year
            $currentYear = date_format(new \DateTime(), 'Y');
            if (
                !is_null($inputs['replacementYear']) &&
                ($inputs['replacementYear'] != "") &&
                is_numeric($inputs['replacementYear']) &&
                $inputs['replacementYear'] < $currentYear + 10 &&
                $inputs['replacementYear'] >= $currentYear
            ) {
                $query->where("plant.remaining_life", '=', $inputs['replacementYear'] - $currentYear);
            }

            // Age RAG
            if ($inputs['ageRagFrom'] !== '' && is_numeric($inputs['ageRagFrom'])) {
                $query->where(
                    \DB::raw("round(plant.remaining_life / plant.expected_life * 100)"),
                    '>=',
                    $inputs['ageRagFrom']
                );
            }

            if ($inputs['ageRagTo'] !== '' && is_numeric($inputs['ageRagTo'])) {
                $query->where(
                    \DB::raw("round(plant.remaining_life / plant.expected_life * 100)"),
                    '<=',
                    $inputs['ageRagTo']
                );
            }
        }
        $this->filterByUserDefines(GenTable::PLANT, $query, $filter, $table);

        return $query;
    }
}
