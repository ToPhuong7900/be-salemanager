<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use Illuminate\Support\Arr;
use Tfcloud\Controllers\Admin\General\SiteGroupsController;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Math;
use Tfcloud\Lib\Exceptions\StateException;
use Tfcloud\Models\Contact;
use Tfcloud\Models\NhsCleaning\NhsCleanAudit;
use Tfcloud\Models\NhsCleaning\NhsCleanAuditAction;
use Tfcloud\Models\NhsCleaning\NhsCleanAuditRoom;
use Tfcloud\Models\NhsCleaning\NhsCleanResponsibility;
use Tfcloud\Models\Note;
use Tfcloud\Models\Report;
use Tfcloud\Models\SiteGroup;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Admin\General\SiteGroupsService;
use Tfcloud\Services\Report\WkHtmlToPdf\WkHtmlToPdfReportBaseService;

class NHSCLN07AuditReportService extends WkHtmlToPdfReportBaseService
{
    public $filterText = '';

    /**
     * Build the data array required for the report and pass it to constructTemplates to be processed.
     * Return the generated PDF as a result.
     * $nhsAuditId int
     * $repId int
     */
    public function runReport($nhsAuditId, $repId)
    {
        $cleanAudit = NhsCleanAudit::find($nhsAuditId);

        $auditItems = NhsCleanAuditRoom::join(
            'room',
            'room.room_id',
            '=',
            'nhs_clean_audit_room.room_id'
        )
        ->join(
            'building',
            'building.building_id',
            '=',
            'room.building_id'
        )
        ->join(
            'nhs_clean_element',
            'nhs_clean_element.nhs_clean_element_id',
            '=',
            'nhs_clean_audit_room.nhs_clean_element_id'
        )
        ->join(
            'nhs_clean_responsibility',
            'nhs_clean_responsibility.nhs_clean_responsibility_id',
            '=',
            'nhs_clean_audit_room.nhs_clean_responsibility_id'
        )
        ->where('nhs_clean_audit_room.nhs_clean_audit_id', $cleanAudit->getKey())
        ->select(
            [
                'building.site_id',
                'building.building_id',
                'room.room_id',
                'building.building_code',
                'building.building_desc',
                'room.room_number',
                'room.room_desc',
                'nhs_clean_element.nhs_clean_element_id',
                'nhs_clean_element.nhs_clean_element_code',
                'nhs_clean_element.nhs_clean_element_desc',
                'nhs_clean_responsibility.nhs_clean_responsibility_id',
                'nhs_clean_responsibility.nhs_clean_responsibility_code',
                'nhs_clean_audit_room.result',
            ]
        )
        ->orderBy('building_code')
        ->orderByRaw('CONVERT(room_number, SIGNED) asc')
        ->get();

        $data = ['rooms' => []];
        $elements = [];
        foreach ($auditItems as $auditItem) {
            $data['rooms'][$auditItem->room_id] = [
                'site_id' => $auditItem->site_id,
                'building_id' => $auditItem->building_id,
                'room_id' => $auditItem->room_id,
                'building_code' => $auditItem->building_code,
                'building_desc' => $auditItem->building_desc,
                'room_number' => $auditItem->room_number,
                'room_desc' => $auditItem->room_desc,
            ];
            $elements[$auditItem->nhs_clean_element_id] = [
                'nhs_clean_element_id' => $auditItem->nhs_clean_element_id,
                'nhs_clean_element_code' => $auditItem->nhs_clean_element_code,
                'nhs_clean_element_desc' => $auditItem->nhs_clean_element_desc,
                'nhs_clean_responsibility_id' => $auditItem->nhs_clean_responsibility_id,
                'nhs_clean_responsibility_code' => $auditItem->nhs_clean_responsibility_code,
            ];
            $data['results'][$auditItem->room_id][$auditItem->nhs_clean_element_id] = $auditItem->result;
        }

        $data['elements'] = $this->sortElements($elements);

        $this->roomScores($data);
        $this->elementScore($data);

        // Get me all the cleaning audit actions relating to this audit
        $cleaningAuditActions = NhsCleanAuditAction::where('nhs_clean_audit_id', '=', $nhsAuditId)
        ->join('room', 'room.room_id', '=', 'nhs_clean_audit_action.nhs_clean_audit_room_id')->get();

        return $this->constructTemplates($cleanAudit, $data, $repId, $cleaningAuditActions);
    }

    /**
     * Construct the wkhtmltopdf templates and populate with the data provided
     * $cleanAudit model data
     * $data report data
     * $repId int
     */
    private function constructTemplates($cleanAudit, $data, $repId, $cleaningAuditActions)
    {
        // Create wkhtmltopdf temp folder to store generated files
        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);

        $header = \View::make('reports.wkhtmltopdf.nhsCleaning.nhscln07.header', [
            'logo' => $this->getHeaderLogo()
        ])->render();
        $headerPath = $generatedPath . "/header.html";
        file_put_contents($headerPath, $header, LOCK_EX);

        $footer = \View::make('reports.wkhtmltopdf.nhsCleaning.nhscln07.footer')->render();
        $footerPath = $generatedPath . "/footer.html";
        file_put_contents($footerPath, $footer, LOCK_EX);

        $content = \View::make(
            'reports.wkhtmltopdf.nhsCleaning.nhscln07.content',
            [
                'cleanAudit'  => $cleanAudit,
                'summaryData' => $data,
                'auditActions' => $cleaningAuditActions
            ]
        )->render();
        $contentPath = $generatedPath . "/content.html";
        file_put_contents($contentPath, $content, LOCK_EX);

        $formatOptions = [
            'header-html' => $headerPath,
            'footer-html' => $footerPath,
            'header-spacing' => 4,
            'footer-spacing' => 4,
            'orientation' => 'landscape',
        ];

        $repOutPutPdfFile = $this->getTempFile();

        $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $formatOptions);

        return $repOutPutPdfFile;
    }

    private function getTempFile()
    {
        return Common::getTempFolder() . DIRECTORY_SEPARATOR . 'NHS07_Audit_Report.pdf';
    }

    private function sortElements(array &$elements)
    {
        return collect($elements)->sortBy('nhs_clean_element_code')->toArray();
    }

    private function roomScores(array &$data)
    {
        foreach ($data['rooms'] as $roomId => $room) {
            $pass = 0;
            $fail = 0;
            $notAnswered = 0;
            foreach ($data['results'][$roomId] as $result) {
                switch ($result) {
                    case NhsCleanAuditRoom::PASS:
                        $pass++;
                        break;
                    case NhsCleanAuditRoom::FAIL:
                        $fail++;
                        break;
                    case NhsCleanAuditRoom::NOT_ANSWERED:
                        $notAnswered++;
                        break;
                    default:
                        throw new StateException("Invalid result value [$result].");
                }
            }

            $data['rooms'][$roomId]['score'] = $pass;

            $total = $pass + $fail;

            if ($total === 0) {
                $percent = '';
            } else {
                $percent = number_format(Math::mulRound(2, [Math::div(8, [$pass, $total]), 100]), 2);
            }

            $data['rooms'][$roomId]['percent'] = $percent;
        }
    }

    private function elementScore(array &$data)
    {
        $totalPassNursing = 0;
        $totalFailNursing = 0;
        $totalPassEstates = 0;
        $totalFailEstates = 0;
        $totalPassCleaningService = 0;
        $totalFailCleaningService = 0;
        foreach ($data['elements'] as $index => $element) {
            $elementId = $element['nhs_clean_element_id'];
            $nhsCleanResponsibilityId = $element['nhs_clean_responsibility_id'];
            $pass = 0;
            $fail = 0;

            foreach ($data['results'] as $result) {
                switch ($result[$elementId]) {
                    case NhsCleanAuditRoom::PASS:
                        $pass++;
                        switch ($nhsCleanResponsibilityId) {
                            case NhsCleanResponsibility::ESTATES:
                                $totalPassEstates++;
                                break;
                            case NhsCleanResponsibility::NURSING:
                                $totalPassNursing++;
                                break;
                            case NhsCleanResponsibility::CLEANING_SERVICE:
                                $totalPassCleaningService++;
                                break;
                            default:
                                throw new StateException(
                                    "Invalid nhs_clean_responsibility_id [$nhsCleanResponsibilityId]"
                                );
                        }
                        break;
                    case NhsCleanAuditRoom::FAIL:
                        $fail++;
                        switch ($nhsCleanResponsibilityId) {
                            case NhsCleanResponsibility::ESTATES:
                                $totalFailEstates++;
                                break;
                            case NhsCleanResponsibility::NURSING:
                                $totalFailNursing++;
                                break;
                            case NhsCleanResponsibility::CLEANING_SERVICE:
                                $totalFailCleaningService++;
                                break;
                            default:
                                throw new StateException(
                                    "Invalid nhs_clean_responsibility_id [$nhsCleanResponsibilityId]"
                                );
                        }
                        break;
                    case NhsCleanAuditRoom::NOT_ANSWERED:
                        break;
                    default:
                        throw new StateException("Invalid result value [$result].");
                }
            }

            $data['elements'][$index]['achievableScore'] = $pass + $fail;
            $data['elements'][$index]['totalScore'] = $pass;
        }

        $totalPass = $totalPassNursing + $totalPassEstates + $totalPassCleaningService;
        $totalFail = $totalFailNursing + $totalFailEstates + $totalFailCleaningService;
        $data['actualAchievableScore'] = $totalPass + $totalFail;
        $data['actualTotalScore'] = $totalPass;


        $totalCleaningService = $totalPassCleaningService + $totalFailCleaningService;
        if ($totalCleaningService === 0) {
             $data['scoreCleaningService'] = '';
        } else {
            $data['scoreCleaningService'] = number_format(
                Math::mulRound(2, [Math::div(8, [$totalPassCleaningService, $totalCleaningService]), 100]),
                2
            );
        }

        $totalNursing = $totalPassNursing + $totalFailNursing;
        if ($totalNursing === 0) {
             $data['scoreNursing'] = '';
        } else {
             $data['scoreNursing'] =
                number_format(Math::mulRound(2, [Math::div(8, [$totalPassNursing, $totalNursing]), 100]), 2);
        }

        $totalEstates = $totalPassEstates + $totalFailEstates;
        if ($totalEstates === 0) {
             $data['scoreEstates'] = '';
        } else {
             $data['scoreEstates'] =
                number_format(Math::mulRound(2, [Math::div(8, [$totalPassEstates, $totalEstates]), 100]), 2);
        }

        $totalOverall = $totalPass + $totalFail;
        if ($totalOverall === 0) {
             $data['scoreOverall'] = '';
        } else {
             $data['scoreOverall'] =
                number_format(Math::mulRound(2, [Math::div(8, [$totalPass, $totalOverall]), 100]), 2);
        }
    }
}
