<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf\SouthAyrshire;

use Auth;
use Illuminate\Support\Facades\DB;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Exceptions\PermissionException;
use Tfcloud\Models\Building;
use Tfcloud\Models\CondsurveyStatus;
use Tfcloud\Models\CrudAccessLevel;
use Tfcloud\Models\Idwork;
use Tfcloud\Models\IdworkStatusType;
use Tfcloud\Models\Module;
use Tfcloud\Models\Note;
use Tfcloud\Models\NoteRecordType;
use Tfcloud\Models\Report;
use Tfcloud\Models\User;
use Tfcloud\Models\Site;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Property\SiteService;
use Tfcloud\Services\Report\WkHtmlToPdf\WkHtmlToPdfReportBaseService;

class PRCLISAYR01AssetInformationService extends WkHtmlToPdfReportBaseService
{
    public $filterText = '';
    public $permissionService = null;
    private $reportBoxFilterLoader = null;

    public function __construct(PermissionService $permissionService, Report $report = null)
    {
        parent::__construct($permissionService);
        $this->permissionService = $permissionService;
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    /**
     * @param $repOutPutPdfFile
     * @param $inputs
     * @param $repId
     * @return bool
     * @throws \Tfcloud\Lib\Exceptions\PermissionException
     */
    public function runReport($repOutPutPdfFile, $inputs, $repId)
    {
        $data = $this->getReportData($inputs);

        if ($data === false) {
            return false;
        }

        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);

        $logo = '/assets/images/civica_banner.png';
        $photoLeft = '/assets/images/reports/property/south_ayrshire/South_Ayrshire_Way_Full Logo_Positive_CMYK.png';
        $photoRight = '/assets/images/reports/property/south_ayrshire/sac_cmyk.png';

        // Create wkhtmltopdf temp folder to store generated files
        $content = \View::make('reports.wkhtmltopdf.property.prclisayr01.content', [
            'data' => $data,
            'logo' => public_path() . $logo,
            'pageTitle' => 'Asset Information Form',
            'photoLeft' => public_path() . $photoLeft,
            'photoRight' => public_path() . $photoRight
        ])->render();
        $contentFile = $generatedPath . '/content.html';
        file_put_contents($contentFile, $content, LOCK_EX);

        $footer = \View::make('reports.wkhtmltopdf.property.prclisayr01.footer')->render();
        $footerPath = $generatedPath . "/footer.html";
        file_put_contents($footerPath, $footer, LOCK_EX);

        $header = \View::make('reports.wkhtmltopdf.property.prclisayr01.header')->render();
        $headerPath = $generatedPath . "/header.html";
        file_put_contents($headerPath, $header, LOCK_EX);

        $this->generateSingleHtmlToPdf(
            $contentFile,
            $repOutPutPdfFile,
            [
                'footer-html' => $footerPath,
                'header-html' => $headerPath,
                'header-spacing' => 4,
                'footer-spacing' => 4
            ]
        );

        return true;
    }

    /**
     * @param $inputs
     * @return array
     * @throws \Tfcloud\Lib\Exceptions\PermissionException
     */
    private function getReportData($inputs)
    {
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $site = $reportFilterQuery->filterAll($this->all())->get();
        } else {
            $site = $this->getAll($inputs)->get();
        }

        $totalSite = Site::where('site_group_id', '=', Auth::User()->site_group_id)
            ->where('active', '=', CommonConstant::DATABASE_VALUE_YES)
            ->count();

        $siteIdList = array_unique(array_pluck($site, 'site_id'));

        $useWhereIn = true;

        if ($totalSite == count($siteIdList)) {
            $useWhereIn = false;
        }

        $siteTotalIdwCost = $this->getTotalIdworkCost($siteIdList, $useWhereIn);

        $siteNote = $this->getSiteNote($siteIdList, $useWhereIn);

        $siteGIA = $this->getTotalSiteGIA($siteIdList, $useWhereIn);

        $totalSiteValue = $this->getTotalSiteValue($siteIdList, $useWhereIn);

        // --------------------------------------------------------------------------------------------------
        // TC: Report needed to be hard coded to a particular fin year, but instead it was a hard coded offset to
        // the current financial year.
        // This meant that when the financial year moved on 1 year, so would the report.
        // As of 09/03/2021, client wants the report to be hard coded for 2019/2020.
        // --------------------------------------------------------------------------------------------------
        /*Gets the last fin year */
        //$currentFnYear = (new \Tfcloud\Services\Admin\General\FinYearService($this->permissionService))
        //    ->getFinYearFromDate();

        //$currentYear = $currentFnYear['year_start_date'];

        $fixedYearStartDate = new \DateTime('2021-04-01');
        //$time = new \DateTime($currentYear);
        //$previousYearStartDate = $time->modify('-1 year');
        // --------------------------------------------------------------------------------------------------

        $previousYearObject = (new \Tfcloud\Services\Admin\General\FinYearService($this->permissionService))
                ->getFinYearFromDate($fixedYearStartDate);
        /*fin year retrieve end*/

        if (!$site->isEmpty()) {
            foreach ($site as $item) {
                if (isset($siteTotalIdwCost[$item->site_id])) {
                    $item->site_total_idwork_cost = $siteTotalIdwCost[$item->site_id];
                }

                if (isset($siteNote[$item->site_id])) {
                    $item->site_note = $siteNote[$item->site_id];
                }

                if (isset($siteGIA[$item->site_id])) {
                    $item->site_gia = round($siteGIA[$item->site_id]);
                }

                if (isset($totalSiteValue[$item->site_id])) {
                    $item->site_gba = $totalSiteValue[$item->site_id];
                }
                $item->site_photo = $this->getSitePhoto($item->site_id, $item->site_group_id, $item->photo);

                $item->orderNo = "";

                if (isset($item->constituency_id)) {
                    $item->orderNo = $item->constituency_code;
                }

                $item->finYearCode = $previousYearObject->fin_year_code;

                $utilitySum = $this->runningCostCalcuation(
                    $item->site_id,
                    $previousYearObject->fin_year_id,
                    ['Electric','Gas', 'Oil', 'Pellets (Wood)',
                    'Water/Sewerage Metered',
                    'Water/Sewerage Unmetered'],
                    true,
                    true
                );

                $rates = $this->runningCostCalcuation(
                    $item->site_id,
                    $previousYearObject->fin_year_id,
                    'Business Rates',
                    false,
                    false
                );

                $generalMaintenance = $this->runningCostCalcuation(
                    $item->site_id,
                    $previousYearObject->fin_year_id,
                    ['Maint - General','Maint - Grounds'],
                    false,
                    true
                );

                $CRA = $this->runningCostCalcuation(
                    $item->site_id,
                    $previousYearObject->fin_year_id,
                    'Maint - CRA',
                    false,
                    false
                );

                $cleaning = $this->runningCostCalcuation(
                    $item->site_id,
                    $previousYearObject->fin_year_id,
                    'Maint - Cleaning',
                    false,
                    false
                );

                $security = $this->runningCostCalcuation(
                    $item->site_id,
                    $previousYearObject->fin_year_id,
                    'Security',
                    false,
                    false
                );

                $wasteCollection = $this->runningCostCalcuation(
                    $item->site_id,
                    $previousYearObject->fin_year_id,
                    'Waste Collection',
                    false,
                    false
                );

                $janitorOtLets = $this->runningCostCalcuation(
                    $item->site_id,
                    $previousYearObject->fin_year_id,
                    'School Lets - OT',
                    false,
                    false
                );

                $item->utilityData = Common::numberFormat(round($utilitySum), true, ',', 0);
                $item->rates = Common::numberFormat(round($rates), true, ',', 0);
                $item->generalMaintenance = Common::numberFormat(round($generalMaintenance), true, ',', 0);
                $item->cra = Common::numberFormat(round($CRA), true, ',', 0);
                $item->cleaning = Common::numberFormat(round($cleaning), true, ',', 0);
                $item->security = Common::numberFormat(round($security), true, ',', 0);
                $item->wasteCollection = Common::numberFormat(round($wasteCollection), true, ',', 0);
                $item->janitorOtLets = Common::numberFormat(round($janitorOtLets), true, ',', 0);

                $item->annualRunningCosts = 0;

                $annualRunningCosts = $utilitySum += $rates += $generalMaintenance += $CRA += $cleaning
                        += $security += $wasteCollection += $janitorOtLets;

                $item->annualRunningCosts = Common::numberFormat(round($annualRunningCosts), true, ',', 0);
            }
        }

        return $site;
    }

    private function runningCostCalcuation($siteId, $previousFinYear, $data, $isUtility = false, $useWhereIn = false)
    {
        $query = \DB::table('run_cost_val')
            ->join('run_cost_type', 'run_cost_type.run_cost_type_id', '=', 'run_cost_val.run_cost_type_id')
            ->join(
                'fin_year_period',
                'fin_year_period.fin_year_period_id',
                '=',
                'run_cost_val.fin_year_period_id'
            )
            ->where('site_id', '=', $siteId)
            ->whereNull('building_id')
            ->where('fin_year_period.fin_year_id', '=', $previousFinYear)
            ->where('run_cost_type.site_group_id', '=', \Auth::user()->site_group_id)
            ->where('run_cost_type.active', '=', CommonConstant::DATABASE_VALUE_YES);

        if ($isUtility) {
            $query->where('run_cost_type.utility', '=', CommonConstant::DATABASE_VALUE_YES);
        }

        if ($useWhereIn) {
            $query->whereIn('run_cost_type_label', $data);
        } else {
            $query->where('run_cost_type_label', $data);
        }

            $query->select(\DB::raw('IFNULL(SUM(run_cost_val.cost), 0) as run_cost_val_cost'));

            return $query->first()->run_cost_val_cost;
    }

    /**
     * @param $inputs
     * @return mixed
     * @throws PermissionException
     */
    public function getAll($inputs)
    {
        if (
            !$this->permissionService->hasModuleAccess(
                Module::MODULE_PROPERTY,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
            )
        ) {
            $msg = "Not met minimum read access for property module in class" .
                " (" . __CLASS__ . ") method (" . __METHOD__ . "). " .
                " See line (" . __LINE__ . ")";

            throw new PermissionException($msg);
        }

        $siteService = new SiteService($this->permissionService);

        $siteData = $siteService->filterAll($this->all(), $inputs, false, true);

        return $siteData;
    }

    private function all()
    {
        $siteAccess = Auth::user()->site_access;

        switch ($siteAccess) {
            case User::SITE_ACCESS_MODE_ALL_READ:
            case User::SITE_ACCESS_MODE_ALL_EDIT:
            case User::SITE_ACCESS_MODE_ALL_CREATE:
            case User::SITE_ACCESS_MODE_ALL_DELETE:
                $query = Site::userSiteGroup();
                break;
            case User::SITE_ACCESS_MODE_SELECTED:
                $sites = Auth::user()->userAccesses()
                    ->join('site', 'user_access.site_id', '=', 'site.site_id')
                    ->where('site.site_group_id', Auth::User()->site_group_id)
                    ->select('site.site_id')->get()->map(
                        function ($site) {
                            return $site->site_id;
                        }
                    );

                if (!$sites->isEmpty()) {
                    $query = Site::whereIn('site.site_id', $sites->toArray());
                } else {
                    // empty query
                    $query = Site::where(\DB::raw('false'));
                }
                break;
            default:
                throw new PermissionException("No access to sites");
                break;
        }

        $query->select(
            [
                'site.site_id',
                'site.site_code',
                'site.site_desc',
                'site.dfe_county_code',
                'site.uprn',
                'site.active',
                'site.photo',
                'site.site_group_id',
                'address.postcode',
                'address_street.town',
                'address_street.street',
                'address_street.locality',
                'address.first_addr_obj',
                'address.second_addr_obj',
                'address.address_street_id',
                'site_usage.site_usage_desc',
                'site_type.site_type_code',
                'site_type.site_type_desc',
                \Db::Raw('COALESCE(energy_epc_rating.energy_epc_rating_code, \'-\') AS energy_epc_rating_code'),
                'ward.ward_code',
                'ward.ward_desc',
                \Db::Raw('COALESCE(gen_userdef_sel.gen_userdef_sel_code, \'-\') as condition_gen_code'),
                \Db::Raw('COALESCE(suitability_rating.suitability_rating_code, \'-\') AS suitability_gen_code'),
                'constituency.constituency_id',
                'constituency.constituency_code',
                'constituency.constituency_desc'
            ]
        )
            ->join('address', 'site.address_id', '=', 'address.address_id')
            ->join('address_street', 'address.address_street_id', '=', 'address_street.address_street_id')
            ->leftJoin('gen_userdef_group', 'site.gen_userdef_group_id', 'gen_userdef_group.gen_userdef_group_id')
            ->leftJoin(
                'gen_userdef_sel',
                'gen_userdef_group.sel3_gen_userdef_sel_id',
                'gen_userdef_sel.gen_userdef_sel_id'
            )
            ->leftJoin('suitability_rating', 'site.suitability_rating_id', 'suitability_rating.suitability_rating_id')
            ->leftJoin('site_usage', 'site.site_usage_id', '=', 'site_usage.site_usage_id')
            ->leftJoin('ward', 'site.ward_id', '=', 'ward.ward_id')
            ->leftJoin('constituency', 'constituency.constituency_id', '=', 'site.constituency_id')
            ->leftJoin('npi', function ($join) {
                $join->on('site.site_id', '=', 'npi.site_id')
                    ->whereNull('npi.building_id')
                    ->where('npi.npi_excluded', '=', 'N');
            })
            ->leftJoin(
                'gen_rag_status as condition_gen',
                'npi.npi_condition_gen_rag_status_id',
                '=',
                'condition_gen.gen_rag_status_id'
            )
            ->leftJoin(
                'gen_rag_status as suitability_gen',
                'npi.npi_suitability_gen_rag_status_id',
                '=',
                'suitability_gen.gen_rag_status_id'
            )
            ->leftJoin('energy_field', function ($join) {
                $join->on(
                    'site.site_id',
                    'energy_field.site_id'
                );
                $join->whereNull(
                    'energy_field.building_id',
                );
            })
            ->leftJoin(
                'energy_epc_rating',
                'energy_field.energy_epc_current_rating_id',
                '=',
                'energy_epc_rating.energy_epc_rating_id'
            )
            ->leftJoin('site_type', 'site_type.site_type_id', '=', 'site.site_type_id')
            ->orderBy('constituency.constituency_code', 'ASC');

        return $query;
    }

    private function getSiteNote($siteIdList, $useWhereIn)
    {
        if (empty($siteIdList)) {
            return false;
        }

        $query = Note::select(
            'note.note_id',
            'note.record_id as site_id',
            'note.note_type_id',
            'note_type.note_type_code',
            'note.detail',
            'note.note_date',
            'site.site_group_id'
        )->leftJoin('note_type', 'note.note_type_id', '=', 'note_type.note_type_id')
            ->join('site', 'site.site_id', '=', 'note.record_id')
            ->whereRaw('note.note_date = (
                SELECT
                      MAX(d.note_date)
                    FROM
                      note d
                    WHERE
                      d.note_type_id = note.note_type_id
                    AND d.record_id = note.record_id)')
            ->where('note.record_type_id', '=', NoteRecordType::PROPERTY_SITE)
            ->where('site.site_group_id', '=', Auth::User()->site_group_id)
            ->groupBy('note.record_id', 'note.note_type_id');

        if ($useWhereIn) {
            $query->whereIn('note.record_id', $siteIdList);
        }

        $noteList = $query->get();
        $siteList = [];

        // get max note
        if (!empty($noteList)) {
            foreach ($noteList as $note) {
                // only get specific note type
                $noteTypeCode = '';
                if ($this->formatNoteType($note->note_type_code, $noteTypeCode)) {
                    if (isset($siteList[$note->site_id][$noteTypeCode])) {
                        $d1 = strtotime($note->note_date);
                        $d2 = strtotime($siteList[$note->site_id][$noteTypeCode]['note_date']);
                        if ($d1 > $d2) {
                            $this->setNoteSiteData(
                                $siteList,
                                $note->site_id,
                                $noteTypeCode,
                                $note->detail,
                                $note->note_date
                            );
                        }
                    } else {
                        $this->setNoteSiteData(
                            $siteList,
                            $note->site_id,
                            $noteTypeCode,
                            $note->detail,
                            $note->note_date
                        );
                    }
                }
            }
        }

        return $siteList;
    }

    private function setNoteSiteData(&$noteSiteList, $site_id, $noteTypeCode, $detail, $note_date)
    {
        $noteSiteList[$site_id][$noteTypeCode] = [
            'note_detail' => $detail,
            'note_date' => $note_date
        ];
    }

    private function formatNoteType($noteType, &$result)
    {
        //$listNoteType = ['AssetValue', 'BacklogMaint', 'GeneralComm', 'RunningCostComm'];
        $listNoteType = ['BUOOA-AV', 'BUOOA-BM', 'BUOOA-G', 'BUOOA-RC'];
        foreach ($listNoteType as $type) {
            if (strpos($noteType, $type) === 0) {
                $result = $type;
                return true;
            }
        }
        return false;
    }

    private function getTotalSiteGIA($siteIdList, $useWhereIn)
    {
        if (empty($siteIdList)) {
            return false;
        }

        $siteGIAList = [];

        $query = Building::select(DB::raw('SUM(gia) as total_gia, site_id'))
            ->where('active', '=', CommonConstant::DATABASE_VALUE_YES)
            ->groupBy('site_id')
            ->where('site_group_id', Auth::User()->site_group_id);

        if ($useWhereIn) {
            $query->whereIn('site_id', $siteIdList);
        }

        $totalGIAList = $query->get();

        if (!$totalGIAList->isEmpty()) {
            foreach ($totalGIAList as $item) {
                $siteGIAList[$item->site_id] = $item->total_gia;
            }
        }

        return $siteGIAList;
    }

    // asset value
    private function getTotalSiteValue($siteIdList, $useWhereIn)
    {
        if (empty($siteIdList)) {
            return false;
        }

        $siteGIAList = [];
        // TC Implementation according to documentation provided by client 27/07/2020.

        //  Asset Value

        // The Total Asset Value figure should be pulled from the Estates module for all CURRENT valuations
        // under valuation type ASSET. So for example Troon Municipal Buildings (L304) has the valuation code
        // EVAL002345 with a total value of £2,176,282.

        // In some cases however we may have two or more entries
        // set to CURRENT for a site in which case we would want the total for all of these entries to pull
        // through to this one field.

        $query = \Tfcloud\Models\EstValuation::select(
            DB::raw(
                "site_id,
                SUM(`total_value`) AS gbv"
            )
        )
        ->join('valuation_type', 'est_valuation.valuation_type_id', 'valuation_type.valuation_type_id')
        ->where('valuation_type.valuation_type_code', 'ASSET')
        ->where('est_valuation.site_group_id', Auth::User()->site_group_id)
        ->where('est_valuation.current_val', CommonConstant::DATABASE_VALUE_YES)
        ->groupBy('site_id');

        if ($useWhereIn) {
            $query->whereIn('site_id', $siteIdList);
        }

        $totalSiteGIAList = $query->get();

        if (!$totalSiteGIAList->isEmpty()) {
            foreach ($totalSiteGIAList as $item) {
                $siteGIAList[$item->site_id] = $item->gbv;
            }
        }

        return $siteGIAList;
    }

    private function getTotalIdworkCost($siteIdList, $useWhereIn)
    {
        if (empty($siteIdList)) {
            return false;
        }

        $siteCostList = [];
        $query = Idwork::select(DB::raw('sum(idwork.total_cost) total_cost, max_cond.site_id'))
            ->join('idwork_status', 'idwork_status.idwork_status_id', '=', 'idwork.idwork_status_id')
            ->join(DB::raw(
                "(
                 SELECT
                   condsurvey.site_id,
                   condsurvey.survey_date,
                   condsurvey.condsurvey_id
                 FROM
                    condsurvey
                        JOIN (
                            SELECT
                                max(condsurvey.survey_date) AS cond_max_date,
                                condsurvey.site_id          AS sub_site_id
                            FROM
                                condsurvey
                            WHERE
                                condsurvey.condsurvey_status_id = " . CondsurveyStatus::COMPLETE . "
                                AND condsurvey.condsurvey_building_id IS NULL
                            GROUP BY
                                condsurvey.site_id
                            ) AS cnd ON cnd.cond_max_date = condsurvey.survey_date
                                    AND cnd.sub_site_id = condsurvey.site_id
                                    AND condsurvey.condsurvey_status_id = " . CondsurveyStatus::COMPLETE . "
                 ) AS max_cond"
            ), function ($join) {
                $join->on('max_cond.condsurvey_id', '=', 'idwork.condsurvey_id')
                    ->where('idwork_status.idwork_status_type_id', '<>', IdworkStatusType::SUPERSEDED);
            })
            ->where('idwork.site_group_id', Auth::User()->site_group_id)
            ->groupBy('max_cond.site_id');

        if ($useWhereIn) {
            $query->whereIn('site_id', $siteIdList);
        }

        $totalCostList = $query->get();

        if (!$totalCostList->isEmpty()) {
            foreach ($totalCostList as $item) {
                $siteCostList[$item->site_id] = $item->total_cost;
            }
        }

        return $siteCostList;
    }

    private function getSitePhoto($siteId, $siteGroupId, $sitePhoto)
    {
        $siteImagePath = $this->getPhotoStoragePathway($siteGroupId, $siteId, 'site');
        $splitString = explode("site/", $siteImagePath);

        $firstHalf = $splitString[0];
        $sitePhotoPath = $firstHalf . "site/" . $siteId . "/" . $sitePhoto;

        if (!is_file($sitePhotoPath)) {
            $sitePhotoPath = $this->fallBackImagePath(true);
        }

        return $sitePhotoPath;
    }
}
