<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use Tfcloud\Models\CaseManagement\CmType;
use Tfcloud\Models\GenTable;

class CM03Casemgmt2ProgressService extends BaseCMx03CaseProgressService
{
    protected $cmType = CmType::CM_TYPE_CASEMGMT2;
    protected $gentTable = GenTable::CM2_CASE;
}
