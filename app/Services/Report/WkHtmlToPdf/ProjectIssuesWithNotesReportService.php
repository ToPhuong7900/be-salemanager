<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use Tfcloud\Lib\BoxFilterQuery\IReportFilterQuery;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Models\ProjectStatusType;
use Tfcloud\Models\Report;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Project\ProjectIssueService;
use Tfcloud\Services\Project\ProjectService;

class ProjectIssuesWithNotesReportService extends WkHtmlToPdfReportBaseService
{
    private $projectService;
    private $projectIssueService;
    private $reportBoxFilterLoader = null;
    public $filterText = '';

    public function __construct(PermissionService $permissionService, Report $report = null)
    {
        parent::__construct($permissionService);
        $this->projectService = new ProjectService($permissionService);
        $this->projectIssueService = new ProjectIssueService($permissionService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function runReport($repOutPutPdfFile, $inputs, $repId)
    {
        $reportFilterQuery = null;
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $projects = $reportFilterQuery->filterAll(
                $this->projectIssueService->all()->orderBy('project_issue.created_date', 'DESC')
            )->groupBy('project_id')->get();
            $projectIssues = $reportFilterQuery->filterAll(
                $this->projectIssueService->all()->orderBy('project_issue.created_date', 'DESC')
            )->with(['category', 'priority'])->get();
            $privacyValues = $reportFilterQuery->getValueFilterField('privacy');
            $privacyFilterInput = !empty($privacyValues) ? $privacyValues[0] : null;
        } else {
            $this->formatInputData($inputs);
            $projects = $this->projectIssueService->getAll($inputs)->groupBy('project_id')->get();
            $projectIssues = $this->projectIssueService->getAll($inputs)->with(['category', 'priority'])->get();
            $privacyFilterInput = array_get($inputs, 'privacy');
        }

        $projectIssueNotes = [];
        $noteFilterInputs = [
            'privacy' => $privacyFilterInput
        ];

        foreach ($projectIssues as $projectIssue) {
            $notes = $this->projectIssueService->showNotes($projectIssue->project_issue_id, $noteFilterInputs)->get();
            if ($notes->count()) {
                $projectIssueNotes = array_add($projectIssueNotes, $projectIssue->project_issue_id, $notes);
            }
        }

        // Create wkhtmltopdf temp folder to store generated files
        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);

        $header = \View::make('reports.wkhtmltopdf.project.prj12.header')->render();
        $headerPath = $generatedPath . "/header.html";
        file_put_contents($headerPath, $header, LOCK_EX);

        $footer = \View::make('reports.wkhtmltopdf.project.prj12.footer')->render();
        $footerPath = $generatedPath . "/footer.html";
        file_put_contents($footerPath, $footer, LOCK_EX);

        $content = \View::make(
            'reports.wkhtmltopdf.project.prj12.content',
            ['projects' => $projects, 'projectIssues' => $projectIssues, 'projectIssueNotes' => $projectIssueNotes]
        )->render();
        $contentPath = $generatedPath . "/content.html";
        file_put_contents($contentPath, $content, LOCK_EX);

        $options = [
            'header-html' => $headerPath,
            'footer-html' => $footerPath,
            'header-spacing' => 4,
            'footer-spacing' => 4,
            'orientation' => 'landscape',
        ];

        $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $options);

        $whereCodes = [];               // The codes which have been used in the filter
        $whereTexts = [];               // Plain text about filtering applied
        $orCodes = [];                  // csv string of OR codes
        $bFilterDetail = false;

        if ($reportFilterQuery instanceof IReportFilterQuery) {
            $this->filterText = $reportFilterQuery->getFilterDetailText();
        } else {
            $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
            $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $this->filterText);
        }

        return true;
    }

    private function formatInputData(&$inputs)
    {
        foreach ($inputs as $key => $value) {
            if ($value === "") {
                unset($inputs[$key]);
            }
        }
    }

    private function generateFilterData($filterData, &$whereCodes, &$whereTexts, &$orCodes)
    {
        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Code contains '{$filterData['code']}'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Description contains '{$filterData['description']}'");
        }

        if ($val = array_get($filterData, 'project')) {
            array_push(
                $whereCodes,
                ['project', 'project_id', 'project_code', $val, 'Project']
            );
        }

        if ($val = Common::iset($filterData['category'])) {
            array_push(
                $whereCodes,
                [
                    'project_issue_category',
                    'project_issue_category_id',
                    'project_issue_category_code',
                    $val,
                    \Lang::get('text.issue_category')
                ]
            );
        }

        if ($val = Common::iset($filterData['priority'])) {
            array_push(
                $whereCodes,
                [
                    'project_issue_priority',
                    'project_issue_priority_id',
                    'project_issue_priority_code',
                    $val,
                    \Lang::get('text.issue_priority')
                ]
            );
        }

        if ($val = array_get($filterData, 'project_status_id')) {
            array_push(
                $whereCodes,
                ['project_status', 'project_status_id', 'project_status_code', $val, 'Project Status']
            );
        }

        $projectStatusTypes = [];
        if (Common::iset($filterData['draft'])) {
            $projectStatusTypes[] = ProjectStatusType::DRAFT;
        }
        if (Common::iset($filterData['active'])) {
            $projectStatusTypes[] = ProjectStatusType::ACTIVE;
        }
        if (Common::iset($filterData['complete'])) {
            $projectStatusTypes[] = ProjectStatusType::COMPLETE;
        }
        if (Common::iset($filterData['rejected'])) {
            $projectStatusTypes[] = ProjectStatusType::REJECTED;
        }
        if (count($projectStatusTypes)) {
            array_push($orCodes, [
                'project_status_type',
                'project_status_type_id',
                'project_status_type_code',
                implode(',', $projectStatusTypes),
                "Project Status Type"
            ]);
        }

        if ($val = array_get($filterData, 'owner')) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Owner']);
        }

        $issueStatus = [];
        $val = array_get($filterData, 'issueStatusFilterOpen', null);
        if ($val == 1) {
            $issueStatus[] = 'OPEN';
        }

        $val = array_get($filterData, 'issueStatusFilterOnHold', null);
        if ($val == 1) {
            $issueStatus[] = 'ON HOLD';
        }

        $val = array_get($filterData, 'issueStatusFilterClosed', null);
        if ($val == 1) {
            $issueStatus[] = 'CLOSED';
        }

        if (! empty($issueStatus)) {
            array_push($whereTexts, "Project issue status = '" . implode(', ', $issueStatus) . "'");
        }

        if ($siteId = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $siteId, "Site Code"]);

            if ($buildingId = Common::iset($filterData['building_id'])) {
                array_push(
                    $whereCodes,
                    ['building', 'building_id', 'building_code', $buildingId, "Building Code"]
                );
            }
        }

        if ($val = array_get($filterData, 'closedDateFrom', null)) {
            array_push($whereTexts, "Closed Date from '$val'");
        }

        if ($val = array_get($filterData, 'closedDateTo', null)) {
            array_push($whereTexts, "Closed Date to '$val'");
        }

        if ($val = array_get($filterData, 'createdDateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.created_date_from'), $val));
        }

        if ($val = array_get($filterData, 'createdDateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.created_date_to'), $val));
        }

        if ($val = array_get($filterData, 'projectPortfolio')) {
            array_push(
                $whereCodes,
                ['project_portfolio', 'project_portfolio_id', 'project_portfolio_code', $val, 'Project Portfolio']
            );
        }

        if ($val = array_get($filterData, 'projectType')) {
            array_push(
                $whereCodes,
                ['project_type', 'project_type_id', 'project_type_code', $val, 'Type']
            );
        }

        if ($val = array_get($filterData, 'establishment')) {
            array_push(
                $whereCodes,
                ['establishment', 'establishment_id', 'establishment_code', $val, 'Establishment']
            );
        }

        if ($val = array_get($filterData, 'privacy')) {
            array_push($whereTexts, "Private Notes = '" . $val . "'");
        }
    }
}
