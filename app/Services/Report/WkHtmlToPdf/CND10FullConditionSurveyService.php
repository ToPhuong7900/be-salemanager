<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use Tfcloud\Controllers\Admin\General\SiteGroupsController;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Math;
use Tfcloud\Lib\Common;
use Tfcloud\Models\Building;
use Tfcloud\Models\Condsurvey;
use Tfcloud\Models\CondLocationScore;
use Tfcloud\Models\CondLocationType;
use Tfcloud\Models\CondLocationElementScore;
use Tfcloud\Models\CondLocationSubelementScore;
use Tfcloud\Models\CondLocationItemScore;
use Tfcloud\Models\CondsurveyStatus;
use Tfcloud\Models\Contact;
use Tfcloud\Models\GenTable;
use Tfcloud\Models\GenUserdefLabel;
use Tfcloud\Models\GenUserDefSel;
use Tfcloud\Models\Idwork;
use Tfcloud\Models\IdworkElement;
use Tfcloud\Models\IdworkSubElement;
use Tfcloud\Models\IdworkItem;
use Tfcloud\Models\IdworkStatusType;
use Tfcloud\Models\Report;
use Tfcloud\Models\Room;
use Tfcloud\Models\SiteGroup;
use Tfcloud\Models\IdworkPriority;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Admin\General\SiteGroupsService;
use Tfcloud\Services\Condition\ConditionService;

class CND10FullConditionSurveyService extends WkHtmlToPdfReportBaseService
{
    public const MAX_USER_DEFINED_FIELDS = 20;
    private $genUserdefTypes = ['text', 'check', 'date', 'selection', 'contact'];
    private $condService;
    private $sgController;
    private $sgService;
    public $filterText = '';
    private $reportBoxFilterLoader = null;

    public function __construct(PermissionService $permissionService, Report $report = null)
    {
        parent::__construct($permissionService);
        $this->condService = new ConditionService($permissionService);
        $this->sgService = new SiteGroupsService($permissionService);
        $this->sgController = new SiteGroupsController($permissionService, $this->sgService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function runReportSingleCondSurvey($id)
    {
        //call report from button not report view
        $inputs['id'] = $id;
        $inputs['singleOnly'] = true;
        $repId = 256;

        return $this->runReport(
            $this->getTempFile(),
            $inputs,
            $repId
        );
    }

    public function runReport($repOutPutPdfFile, $inputs, $repId)
    {
        $this->formatInputs($inputs);

        $siteGroupId = \Auth::User()->site_group_id;
        $photoPath = $this->sgController->getPhotoPath($siteGroupId); //get logo for outputting at top of report
        $singleCondSurvey = "";

        //get the site group name and desc. for the report header
        $siteGroup = SiteGroup::select()->where('site_group_id', '=', $siteGroupId)->first();
        $siteGroupHeader = $siteGroup->code . " - " . $siteGroup->description;

        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);

            $conditionSurveys = $reportFilterQuery->filterAll($this->getConditionSurveysQuery())
                ->orderBy('condsurvey.condsurvey_code')->get();

            $this->filterText = $reportFilterQuery->getFilterDetailText();
        } else {
            $condQuery = $this->getCondSurveyData($inputs);

            $conditionSurveys = [];
            $conditionSurveys = $condQuery->get();
            $foundCondData = "";

            $whereCodes = [];               // The codes which have been used in the filter
            $whereTexts = [];               // Plain text about filtering applied
            $orCodes = [];                  // csv string of OR codes
            $bFilterDetail = false;

            $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
            $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $this->filterText);
        }

        if (array_key_exists('singleOnly', $inputs)) {
            $singleCondSurvey = 'true';
        } else {
            $singleCondSurvey = 'false';
        }

        $reportType = \Input::get('type', 'full');
        $bHighPriorityOnly = ($reportType == 'highPriority') ? true : false;
        $reportOrder = \Input::get('order', 'element');
        $bByLocation = ($reportOrder == 'location') ? true : false;

        if (count($conditionSurveys) > 0) {
            $foundCondData = 'true';
            $idworkGenUserdefLabels = $this->setIdworkGenUserdefLabels($siteGroupId);
            foreach ($conditionSurveys as $condSurvey) {
                 //get all elements for the report
                $idworkElementsQuery = $this->getAllElements($siteGroupId);
                $idworkElements = [];
                $idworkElements = $idworkElementsQuery->get();

                //get the number of buildings for the report
                $condSurvey['noBuildings'] = $this->getCountAllBuildings($condSurvey->site_id);
                $condSurvey['noRooms'] = $this->getCountAllRooms($condSurvey->site_id);

                if ($condSurvey->score_code) {
                    $condSurvey->score_code = Common::concatFields(
                        [$condSurvey->score_code,
                        $condSurvey->score_desc]
                    );
                }
                $condSurvey['score_code'] = $condSurvey->score_code;

                //get all priorities for the headers
                $idworkPriorities = IdworkPriority::select()->where(
                    'site_group_id',
                    '=',
                    $siteGroupId
                )->orderBy('idwork_priority_code', 'ASC')->get();
                $priorityTotal = count($idworkPriorities);

                if (isset($inputs['superseded'])) {
                    $superseded = true;
                } else {
                    $superseded = false;
                }

                $this->setCondsurveyIdworkData(
                    $condSurvey,
                    $siteGroupId,
                    $idworkGenUserdefLabels,
                    $bByLocation,
                    $bHighPriorityOnly,
                    $superseded
                );

                $this->setIdworkElementTotals($condSurvey, $idworkElements, $idworkPriorities, $condSurvey['idworks']);

                //then get all building idworks against the site
                $idworkElements = $idworkElementsQuery->get();

                $idworkSubElements = false;
                $idworkItems = false;

                if (Common::valueYorNtoBoolean(\Auth::user()->siteGroup->cnd_scores_at_sub_element)) {
                    $idworkSubElementsQuery = $this->getAllSubElements($siteGroupId);
                    $idworkSubElements = $idworkSubElementsQuery->get();
                    if (Common::valueYorNtoBoolean(\Auth::user()->siteGroup->cnd_scores_at_item)) {
                        $idworkItemsQuery = $this->getAllItems($siteGroupId);
                        $idworkItems = $idworkItemsQuery->get();
                    }
                }

                $condSurvey['hasIdworkElements'] = 'true'; //default case
                if (!$idworkElements) {
                    //set null case
                    $idworkElements = 'none set';
                    $condSurvey['hasIdworkElements'] = 'false';
                }

                if (!$idworkSubElements) {
                    $idworkSubElementsQuery = false;
                }

                if (!$idworkItems) {
                    $idworkItemsQuery = false;
                }

                $this->setBuildingIdworkElementTotals(
                    $condSurvey,
                    $idworkPriorities,
                    $idworkElementsQuery,
                    $idworkSubElementsQuery,
                    $idworkItemsQuery
                );
            } //end foreach condsurvey
        } else {
            //filter provided may be wrong; prevent service hanging by forcing error to occur
            $idworkElements = $priorityTotal = $idworkPriorities = "";
            $foundCondData = 'false';
        }
        // Create wkhtmltopdf temp folder to store generated files
        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);

        $header = \View::make(
            'reports.wkhtmltopdf.condition.cnd10.header',
            [
                'photoPath' => $photoPath,
                'singleCondSurvey' => $singleCondSurvey,
                'siteGroupHeader' => $siteGroupHeader
            ]
        )->render();

        $headerPath = $generatedPath . "/header.html";
        file_put_contents($headerPath, $header, LOCK_EX);

        $footer = \View::make('reports.wkhtmltopdf.condition.cnd10.footer')->render();
        $footerPath = $generatedPath . "/footer.html";
        file_put_contents($footerPath, $footer, LOCK_EX);

        $content = \View::make(
            'reports.wkhtmltopdf.condition.cnd10.content',
            [
                'foundCondData'    => $foundCondData,
                'conditionSurveys' => $conditionSurveys,
                'photoPath'        => $photoPath,
                'idworkElements'   => $idworkElements,
                'priorityTotal'    => $priorityTotal,
                'idwPriorities'    => $idworkPriorities

            ]
        )->render();

        $contentPath = $generatedPath . "/content.html";

        file_put_contents($contentPath, $content, LOCK_EX);

        $options = [
            'header-html' => $headerPath,
            'footer-html' => $footerPath,
            'header-spacing' => 4,
            'footer-spacing' => 4,
            'orientation' => 'landscape',
        ];

        if (array_key_exists('singleOnly', $inputs)) {
            $repOutPutPdfFile .= '.pdf';
            $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $options);
            return $repOutPutPdfFile;
        } else {
            $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $options);
            return true;
        }
    }

    private function getTempFile()
    {
        return tempnam(Common::getTempFolder(), 'CND10SingleFullCondSurvey');
    }

    private function getConditionSurveysQuery()
    {
        $query = Condsurvey::select([
            'condsurvey.condsurvey_id',
            'condsurvey.condsurvey_code',
            'condsurvey.condsurvey_desc',
            'condsurvey.survey_date',
            'condsurvey.gen_summary',
            'condsurvey.mech_summary',
            'condsurvey.elec_summary',
            'condsurvey_status.condsurvey_status_code',
            'condsurvey.site_id',
            'contact.contact_name',
            'contact.organisation',
            'user.display_name',
            'user.description',
            'second_surveyor.second_surveyor_name',
            'electrical_surveyor.electrical_surveyor_name',
            'mechanical_surveyor.mechanical_surveyor_name',
            'site.site_code',
            'site.site_desc',
            'score.score_code',
            'score.score_desc',
            'address.second_addr_obj',
            'address.first_addr_obj',
            'address_street.street',
            'address_street.locality',
            'address_street.town',
            'address_street.region',
            'address.postcode',
        ])
            ->leftjoin(
                'site',
                'site.site_id',
                '=',
                'condsurvey.site_id'
            )
            ->leftjoin(
                'address',
                'address.address_id',
                '=',
                'site.address_id'
            )
            ->leftjoin(
                'address_street',
                'address_street.address_street_id',
                '=',
                'address.address_street_id'
            )
            ->leftjoin(
                'gen_gis',
                'gen_gis.gen_gis_id',
                '=',
                'site.gen_gis_id'
            )
            ->leftjoin(
                'score',
                'score.score_id',
                '=',
                'condsurvey.score_id'
            )
            ->leftjoin(
                'condsurvey_status',
                'condsurvey_status.condsurvey_status_id',
                '=',
                'condsurvey.condsurvey_status_id'
            )
            ->leftjoin(
                'contact',
                'contact.contact_id',
                '=',
                'condsurvey.surveyor'
            )
            ->leftjoin(
                'user',
                'user.id',
                '=',
                'condsurvey.owner_user_id'
            )
            ->leftjoin(
                \DB::Raw(
                    '(
                               SELECT	contact.contact_id,
                                        contact.contact_name AS second_surveyor_name
                               FROM contact
                        ) AS second_surveyor'
                ),
                'second_surveyor.contact_id',
                '=',
                'condsurvey.second_surveyor_id'
            )
            ->leftjoin(
                \DB::Raw(
                    '(
                               SELECT	contact.contact_id,
                                        contact.contact_name AS electrical_surveyor_name
                               FROM contact
                        ) AS electrical_surveyor'
                ),
                'electrical_surveyor.contact_id',
                '=',
                'condsurvey.electrical_surveyor_id'
            )
            ->leftjoin(
                \DB::Raw(
                    '(
                               SELECT	contact.contact_id,
                                        contact.contact_name AS mechanical_surveyor_name
                               FROM contact
                        ) AS mechanical_surveyor'
                ),
                'mechanical_surveyor.contact_id',
                '=',
                'condsurvey.mechanical_surveyor_id'
            )
            ->where('condsurvey.site_group_id', '=', \Auth::User()->site_group_id);

        if (\Auth::User()->isSelfServiceUser()) {
            $query->where('condsurvey.condsurvey_status_id', '<>', CondsurveyStatus::OPEN)
                ->where('condsurvey.condsurvey_status_id', '<>', CondsurveyStatus::QA);
        }

        return $query;
    }

    private function getCondSurveyData($inputs)
    {
        $query = $this->getConditionSurveysQuery();

        //apply filters
        if (array_key_exists('code', $inputs)) {
            if (($inputs['code'] != "") && ($inputs['code'] != " ")) {
                $query->where('condsurvey.condsurvey_code', '=', $inputs['code']);
            }
        }

        if (array_key_exists('singleOnly', $inputs)) {
            $query->where('condsurvey.condsurvey_id', '=', $inputs['id']);
        } else {
            //check other inputs passed in by filter
            if ((isset($inputs['description'])) && ($inputs['description'] != "") && ($inputs['description'] != " ")) {
                $query->where('condsurvey.condsurvey_desc', '=', $inputs['description']);
            }

            if ((isset($inputs['site_id'])) && $inputs['site_id'] != "" && ($inputs['site_id'] != " ")) {
                $query->where('condsurvey.site_id', '=', $inputs['site_id']);
                //check all levels
                if (($inputs['building_id'] != "") && ($inputs['building_id'] != " ")) {
                    $query->where('condsurvey.condsurvey_building_id', '=', $inputs['building_id']);

                    if (($inputs['room_id'] != "") && ($inputs['room_id'] != " ")) {
                        $query->where('condsurvey.room_id', '=', $inputs['room_id']);
                    }
                }

                if (($inputs['building_id'] != "") && ($inputs['building_id'] != " ")) {
                    $query->where('condsurvey.condsurvey_building_block_id', '=', $inputs['block_id']);
                }
            } //end site id check

            $status = array();
            foreach ((new CondsurveyStatus())->getStatusList() as $key => $value) {
                if (isset($inputs[$value])) {
                    $status[] = $key;
                }
            }
            if (count($status)) {
                $query->whereIn("condsurvey.condsurvey_status_id", $status);
            }
        }
        $query->orderBy('condsurvey.condsurvey_code');
        return $query;
    }

    private function getAllElements($siteGroupId)
    {
        $idworkQuery = IdworkElement::select()
                ->where('site_group_id', '=', $siteGroupId)
                ->where('active', '=', 'Y');
        return $idworkQuery;
    }

    private function getAllSubElements($siteGroupId)
    {
        $idworkQuery = IdworkSubElement::select()
                ->join('idwork_element', 'idwork_element.idwork_element_id', '=', 'idwork_subelement.idwork_element_id')
                ->where('idwork_subelement.active', '=', 'Y')
                ->where('site_group_id', '=', $siteGroupId);

        return $idworkQuery;
    }

    private function getAllItems($siteGroupId)
    {
        $idworkQuery = IdworkItem::select()
                ->join(
                    'idwork_subelement',
                    'idwork_subelement.idwork_subelement_id',
                    'idwork_item.idwork_subelement_id'
                )
                ->join('idwork_element', 'idwork_element.idwork_element_id', '=', 'idwork_subelement.idwork_element_id')
                ->where('idwork_item.active', '=', 'Y')
                ->where('site_group_id', '=', $siteGroupId);

        return $idworkQuery;
    }

    private function getIdworkPriorityIdList()
    {
        return IdworkPriority::userSiteGroup()
            ->where('high_priority', 'Y')
            ->pluck('idwork_priority_id')
            ->toArray();
    }
    private function getAllIdworks($condSurveyId, $bHighPriorityOnly = false)
    {
        $idworkQuery = Idwork::select()
                ->where(
                    'condsurvey_id',
                    '=',
                    $condSurveyId
                )
                ->orderBy('idwork.idwork_priority_id');

        if ($bHighPriorityOnly) {
            $highPriorityList = $this->getIdworkPriorityIdList();
            if (!is_array($highPriorityList) || count($highPriorityList) == 0) {
                $highPriorityList = [0];
            }
            $idworkQuery->whereIn('idwork.idwork_priority_id', $highPriorityList);
        }
        return $idworkQuery;
    }

    private function getCountAllBuildings($siteId)
    {
        return Building::select()
            ->where('building.site_id', '=', $siteId)
            ->where('building.active', '=', 'Y')
            ->get()
            ->count();
    }

    private function getCountAllRooms($siteId)
    {
        $totalCount = 0;
        $buildings = Building::select()->where('building.site_id', '=', $siteId)->get();
        foreach ($buildings as $building) {
            $roomsCount = Room::select()
                ->where('room.building_id', '=', $building->building_id)
                ->where('room.active', '=', 'Y')
                ->get()->count();
            $totalCount = Math::addInt([$totalCount, $roomsCount]);
        }
        return $totalCount;
    }

    private function setCondsurveyIdworkData(
        &$condSurvey,
        $siteGroupId,
        $idworkGenUserdefLabels,
        $bByLocation,
        $bHighPriorityOnly = false,
        $superseded = false
    ) {

        //get all idwork data

        $condSurveyIdwork = Idwork::where('idwork.condsurvey_id', '=', $condSurvey->condsurvey_id)
            ->leftjoin('idwork_element', 'idwork_element.idwork_element_id', '=', 'idwork.element_id')
            ->leftjoin('idwork_subelement', 'idwork_subelement.idwork_subelement_id', '=', 'idwork.sub_element_id')
            ->leftjoin('idwork_item', 'idwork_item.idwork_item_id', '=', 'idwork.item_id')
            ->leftjoin('idwork_condition', 'idwork_condition.idwork_condition_id', '=', 'idwork.idwork_condition_id')
            ->leftjoin('idwork_priority', 'idwork_priority.idwork_priority_id', '=', 'idwork.idwork_priority_id')
            ->join('idwork_status', 'idwork_status.idwork_status_id', '=', 'idwork.idwork_status_id')
            ->leftjoin('condsurvey', 'condsurvey.condsurvey_id', '=', 'idwork.condsurvey_id')
            ->leftjoin('site', 'site.site_id', '=', 'condsurvey.site_id')
            ->leftjoin('building', 'building.building_id', '=', 'idwork.building_id')
            ->leftjoin('room', 'room.room_id', '=', 'idwork.room_id')
            ->leftjoin('fin_year', "fin_year.fin_year_id", '=', 'idwork.target_fin_year_id')
            ->leftjoin(
                'gen_userdef_group',
                'gen_userdef_group.gen_userdef_group_id',
                '=',
                'idwork.gen_userdef_group_id'
            );

        if ($superseded == false) {
            $condSurveyIdwork->where('idwork_status.idwork_status_type_id', '!=', IdworkStatusType::SUPERSEDED);
        }

            // JA: Added select in order to retrieve idwork.comment which was
            // being overwritten by the comment column in the joined tables.
            $condSurveyIdwork->select(
                'idwork.idwork_id',
                'idwork.defect',
                'idwork.comment as idwork_comment',
                'idwork.building_id',
                'idwork.room_id',
                'idwork.building_block_id',
                'idwork.element_id',
                'idwork.idwork_priority_id',
                'idwork.remedy',
                'idwork.remedy',
                'idwork.total_cost',
                'idwork.location',
                'fin_year.fin_year_code AS idwork_target_fin_year_code',
                'idwork.idwork_code',
                'site.site_code',
                'site.site_desc',
                'idwork_element.idwork_element_code',
                'idwork_element.idwork_element_desc',
                'building.building_code',
                'idwork_subelement.idwork_subelement_code',
                'idwork_subelement.idwork_subelement_desc',
                'room.room_number',
                'idwork_item.idwork_item_code',
                'idwork_item.idworkitem_desc',
                'idwork_condition.idwork_condition_code',
                'idwork_condition.ici_desc',
                'idwork_priority.idwork_priority_code',
                'idwork_priority.ipi_desc',
                'gen_userdef_group.*'
            );

        if ($bHighPriorityOnly) {
            $highPriorityList = $this->getIdworkPriorityIdList();
            if (!is_array($highPriorityList) || count($highPriorityList) == 0) {
                $highPriorityList = [0];
            }
            $condSurveyIdwork->whereIn('idwork.idwork_priority_id', $highPriorityList);
        }
        if ($bByLocation) {
            // Order by building, room, then element, sub element, item
            $condSurveyIdwork->orderBy('building.building_code', 'ASC')
                ->orderBy('room.room_number', 'ASC')
                ->orderBy('idwork_element.idwork_element_code', 'ASC')
                ->orderBy('idwork_subelement.idwork_subelement_code', 'ASC')
                ->orderBy('idwork_item.idwork_item_code', 'ASC')
                ->orderBy('idwork_priority.idwork_priority_code', 'ASC');
        } else {
            // element, sub element, item, then by building, room
            $condSurveyIdwork->orderBy('idwork_element.idwork_element_code', 'ASC')
                ->orderBy('idwork_subelement.idwork_subelement_code', 'ASC')
                ->orderBy('idwork_item.idwork_item_code', 'ASC')
                ->orderBy('building.building_code', 'ASC')
                ->orderBy('room.room_number', 'ASC')
                ->orderBy('idwork_priority.idwork_priority_code', 'ASC');
        }
        $idworkTotal = 0; //provide idwork total at end of report

        $condSurvey['idworks'] = $condSurveyIdwork->get();

        $firstHalf = "";                                    // No need to work this out for every piece of idwork
        foreach ($condSurvey['idworks'] as $idwork) {
            $idWorkId = $idwork->idwork_id;
            $photo = [];
            $photo = Idwork::select('photo')->where('idwork.idwork_id', '=', $idwork->idwork_id)->first();
            $photoName = $photo->photo;

            $idworkTotal = Math::addCurrency([$idworkTotal, $idwork->total_cost]);

            $idwork['showDefLoc'] = 'false';
            if ($idwork->location) {
                $idwork['showDefLoc'] = 'true';
            }

            $idwork['showTargetYear'] = 'false';
            if ($idwork->idwork_target_fin_year_code != '') {
                // This avoids problems when fin year code is 14-15 and html tries to do calculation on it
                $idwork['showTargetYear'] = "Target year: " . $idwork->idwork_target_fin_year_code;
            }

            if ($photoName) {
                //get the image
                if (!$firstHalf) {
                    $idwImagePath = $this->getPhotoStoragePathway($siteGroupId, $idWorkId, 'idwork');
                    //pathway checking in wrong folder, redirect
                    $splitString = explode("idwork/", $idwImagePath);
                    $firstHalf = $splitString[0];
                }
                $idworkPhotoPath = $firstHalf . "idwork/" . $idWorkId . "/" . $photoName;
                $idwork['hasImg'] = 'true';
                $idwork['photoPath'] = $idworkPhotoPath;

                //check image exists
                if (!file_exists($idworkPhotoPath)) {
                    //show no picture available file
                    $useDefaultPhoto = true;
                    $idworkPhotoPath = $this->fallBackImagePath($useDefaultPhoto);
                    $idwork['photoPath'] = $idworkPhotoPath;
                }
            } else {
                $idwork['hasImg'] = 'false';
            }
            $this->setIdworkGenUserdefValues($idwork, $idworkGenUserdefLabels);
        } //end foreach idwork
        $condSurvey['idworkTotal'] = $idworkTotal;
    }

    private function getElementScores($idworkElements, $condScoreLocId)
    {
        $idworkElementScores = array();
        foreach ($idworkElements as $idworkElement) {
            // get the score if there is one for that element
            $idworkElementScore = array();
            $idworkElementScore['idwork_element'] = $idworkElement->code_description;

            $idworkElementId = $idworkElement->idwork_element_id;
            $elementScore = CondLocationElementScore::select()
                ->where('cond_location_element_score.cond_location_score_id', '=', $condScoreLocId)
                ->where('cond_location_element_score.idwork_element_id', '=', $idworkElementId)
                ->leftjoin('score', 'score.score_id', '=', 'cond_location_element_score.score_id')
                ->first();
            if ($elementScore) {
                $idworkElementScore['score'] = $elementScore->score_code;
                $idworkElementScore['comment'] = $elementScore->comment;
                $idworkElementScore['idwork_element_id'] = $idworkElementId;
            } else {
                $idworkElementScore['score'] = '';
                $idworkElementScore['comment'] = '';
                $idworkElementScore['idwork_element_id'] = $idworkElementId;
            }
            array_push($idworkElementScores, $idworkElementScore);
        }
        return $idworkElementScores;
    }

    private function getSubElementScores($idworkSubElements, $condScoreLocId)
    {
        $idworkSubElementScores = array();
        foreach ($idworkSubElements as $idworkSubElement) {
            // get the score if there is one for that element
            $idworkSubElementScore = array();
            $idworkSubElementScore['idwork_subelement'] = $idworkSubElement->code_description;

            $idworkElementId = $idworkSubElement->idwork_element_id;
            $idworkSubElementId = $idworkSubElement->idwork_subelement_id;

            $subElementScore = CondLocationSubElementScore::select()
                ->where('cond_location_subelement_score.cond_location_score_id', '=', $condScoreLocId)
                ->where('cond_location_subelement_score.idwork_element_id', '=', $idworkElementId)
                ->where('cond_location_subelement_score.idwork_subelement_id', '=', $idworkSubElementId)
                ->leftjoin('score', 'score.score_id', '=', 'cond_location_subelement_score.score_id')
                ->first();

            $itemScores = CondLocationItemScore::select()
                ->where('cond_location_item_score.cond_location_score_id', '=', $condScoreLocId)
                ->where('cond_location_item_score.idwork_subelement_id', '=', $idworkSubElementId)
                ->leftjoin('score', 'score.score_id', '=', 'cond_location_item_score.score_id')
                ->get();

            // Check if any items underneath current sub element has any scores to show.
            // If not then hide this subelement by not appending to master array.
            $skipSubElement = true;
            foreach ($itemScores as $itemScore) {
                if ($itemScore->score_code || $itemScore->comment) {
                    $skipSubElement = false;
                    break;
                }
            }

            if (!$skipSubElement) {
                if ($subElementScore) {
                    $idworkSubElementScore['score'] = $subElementScore->score_code;
                    $idworkSubElementScore['comment'] = $subElementScore->comment;
                    $idworkSubElementScore['idwork_element'] = $idworkSubElement->idworkElement->code_description;
                    $idworkSubElementScore['idwork_element_id'] = $idworkElementId;
                    $idworkSubElementScore['idwork_subelement_id'] = $idworkSubElementId;
                    array_push($idworkSubElementScores, $idworkSubElementScore);
                }
            }
        }

        return $idworkSubElementScores;
    }

    private function getItemScores($idworkItems, $condScoreLocId)
    {
        $idworkItemScores = array();
        foreach ($idworkItems as $idworkItem) {
            // get the score if there is one for that element
            $idworkItemScore = array();
            $idworkItemScore['idwork_item'] = $idworkItem->code_description;

            $idworkElementId = $idworkItem->idwork_element_id;
            $idworkSubElementId = $idworkItem->idwork_subelement_id;
            $idworkItemId = $idworkItem->idwork_item_id;

            $itemScore = CondLocationItemScore::select()
                ->where('cond_location_item_score.cond_location_score_id', '=', $condScoreLocId)
                ->where('cond_location_item_score.idwork_subelement_id', '=', $idworkSubElementId)
                ->where('cond_location_item_score.idwork_item_id', '=', $idworkItemId)
                ->leftjoin('score', 'score.score_id', '=', 'cond_location_item_score.score_id')
                ->first();

            if ($itemScore) {
                $idworkItemScore['score'] = $itemScore->score_code;
                $idworkItemScore['comment'] = $itemScore->comment;
                $idworkItemScore['idwork_element_id'] = $idworkElementId;
                $idworkItemScore['idwork_subelement_id'] = $idworkSubElementId;
                $idworkItemScore['idwork_subelement'] = $idworkItem->IdworkSubElement->code_description;
                $idworkItemScore['idwork_element'] = $idworkItem->IdworkSubElement->idworkElement->code_description;

                // Only bother appending if we have a score or a comment
                if ($itemScore->score_code || $itemScore->comment) {
                    array_push($idworkItemScores, $idworkItemScore);
                }
            }
        }

        return $idworkItemScores;
    }


    private function showData($condSurvey)
    {
        if ($condSurvey['sitelocationScore']) {
            \Log::error("Site is " . $condSurvey['sitelocationScore']->site_code);
            foreach ($condSurvey['siteLocationElementScore'] as $elem) {
                \Log::error("elem {$elem['idwork_element']}:{$elem['score']}:{$elem['comment']}.");
            }
        }
        foreach ($condSurvey['buildingLocationScores'] as $locScore) {
            \Log::error("Building is " . $locScore['building']->building_code);
            \Log::error("Building num idwork is " . $locScore['numBuildingIdwork']);
            \Log::error("Building cost is " . $locScore['totalBuildingCost']);
            foreach ($locScore['locationScore'] as $elem) {
                \Log::error("elem {$elem['idwork_element']}:{$elem['score']}:{$elem['comment']}.");
            }
            foreach ($locScore['blocks'] as $blocScore) {
                \Log::error("Block is " . $blocScore['block']->building_block_code);
                \Log::error("Block num idwork is " . $blocScore['numBlockIdwork']);
                \Log::error("Block cost is " . $blocScore['totalBlockCost']);
                foreach ($blocScore['locationScore'] as $elem) {
                    \Log::error("elem {$elem['idwork_element']}:{$elem['score']}:{$elem['comment']}.");
                }
            }
        }
    }
    /**
     * getLocationScores Obtains all the location scores for the supplied survey. The list will be in order
     * with the site location scores, then for each building, the building and all the block scores.
     *
     * @param object $condSurvey
     * @return void
     */
    private function getLocationScores(&$condSurvey, $idworkElements, $idworkSubElements = false, $idworkItems = false)
    {
        // Need to get all of the location scores, these can be site, building or block
        $buildingLocationScores = array();

        // Get the site location score if there is one
        $sitelocationScore = CondLocationScore::select()
            ->where('cond_location_score.condsurvey_id', '=', $condSurvey->condsurvey_id)
            ->where('cond_location_score.cond_location_type_id', '=', CondLocationType::TYPE_SITE)
            ->leftjoin('score', 'score.score_id', '=', 'cond_location_score.score_id')
            ->leftjoin('site', 'site.site_id', '=', 'cond_location_score.location_record_id')
            ->leftjoin('site_usage', 'site.site_usage_id', '=', 'site_usage.site_usage_id')
            ->leftjoin('site_type', 'site.site_type_id', '=', 'site_type.site_type_id')
            ->leftjoin('listed_type', 'site.listed_type_id', '=', 'listed_type.listed_type_id')
            ->leftjoin('ward', 'site.ward_id', '=', 'ward.ward_id')
            ->leftjoin('address', 'address.address_id', '=', 'site.address_id')
            ->leftjoin('address_street', 'address_street.address_street_id', '=', 'address.address_street_id')
            ->first();

        if ($sitelocationScore) {
            if ($sitelocationScore->score_code) {
                $sitelocationScore->site_score_code = Common::concatFields(
                    [$sitelocationScore->score_code,
                    $sitelocationScore->score_desc]
                );
            } else {
                $sitelocationScore->site_score_code = 'Not Selected.';
            }

            $condSurvey['sitelocationScore'] = $sitelocationScore;

            $totalCost = 0;
            $numIdwork = 0;
            foreach ($condSurvey['idworks'] as $idwork) {
                ++$numIdwork;
                $totalCost = Math::addCurrency([$totalCost, $idwork->total_cost]);
            }
            $condSurvey['sitelocationScore']['numBuildingIdwork'] = $numIdwork;
            $condSurvey['sitelocationScore']['totalBuildingCost'] = $totalCost;

            $condScoreLocId = $sitelocationScore->cond_location_score_id;
            $idworkElementScores = $this->getElementScores($idworkElements, $condScoreLocId);
            $condSurvey['siteLocationElementScore'] = $idworkElementScores;

            if ($idworkSubElements) {
                $idworkSubElementScores = $this->getSubElementScores($idworkSubElements, $condScoreLocId);
                $condSurvey['siteLocationSubElementScore'] = $idworkSubElementScores;

                if ($idworkItems) {
                    $idworkItemScores = $this->getItemScores($idworkItems, $condScoreLocId);
                    $condSurvey['siteLocationItemScore'] = $idworkItemScores;
                }
            }
        }

        // Get the building location scores if there are any
        $locBuildingScoreData = CondLocationScore::select()
            ->where('cond_location_score.condsurvey_id', '=', $condSurvey->condsurvey_id)
            ->where('cond_location_score.cond_location_type_id', '=', CondLocationType::TYPE_BUILDING)
            ->leftjoin('score', 'score.score_id', '=', 'cond_location_score.score_id')
            ->leftjoin('building', 'building.building_id', '=', 'cond_location_score.location_record_id')
            ->leftjoin('site', 'site.site_id', '=', 'building.site_id')
            ->leftjoin('address', 'address.address_id', '=', 'building.address_id')
            ->leftjoin('address_street', 'address_street.address_street_id', '=', 'address.address_street_id')
            ->leftjoin('building_type', 'building_type.building_type_id', '=', 'building.building_type_id')
            ->leftjoin('listed_type', 'listed_type.listed_type_id', '=', 'building.listed_type_id')
            ->leftjoin('building_usage', 'building_usage.building_usage_id', '=', 'building.building_usage_id')
            ->leftjoin('ward', 'ward.ward_id', '=', 'building.ward_id')
            ->orderBy('cond_location_score.cond_location_score_id')
            ->get();

        foreach ($locBuildingScoreData as $locBuildScore) {
            \Log::info("Block Data ({$locBuildScore->building_code})");
            $buildingData = array();
            $buildingData['building'] = $locBuildScore;
            if ($locBuildScore->score_code) {
                $buildingData['building_score_code'] = Common::concatFields(
                    [$locBuildScore->score_code,
                    $locBuildScore->score_desc]
                );
            } else {
                $buildingData['building_score_code'] = 'Not Selected.';
            }
            $buildingData['blocks'] = array();
            $totalCost = 0;
            $numIdwork = 0;
            foreach ($condSurvey['idworks'] as $idwork) {
                if ($idwork->building_id == $locBuildScore->building_id) {
                    ++$numIdwork;
                    $totalCost = Math::addCurrency([$totalCost, $idwork->total_cost]);
                }
            }
            $buildingData['numBuildingIdwork'] = $numIdwork;
            $buildingData['totalBuildingCost'] = $totalCost;

            $condScoreLocId = $locBuildScore->cond_location_score_id;
            $idworkElementScores = $this->getElementScores($idworkElements, $condScoreLocId);
            $buildingData['locationScore'] = $idworkElementScores;

            if ($idworkSubElements) {
                $idworkSubElementScores = $this->getSubElementScores($idworkSubElements, $condScoreLocId);
                $buildingData['buildingSubElementScores'] = $idworkSubElementScores;

                if ($idworkItems) {
                    $idworkItemScores = $this->getItemScores($idworkItems, $condScoreLocId);
                    $buildingData['buildingItemScores'] = $idworkItemScores;
                }
            }

            $locBlockScoreData = CondLocationScore::select()
                ->where('cond_location_score.condsurvey_id', '=', $condSurvey->condsurvey_id)
                ->where('cond_location_score.cond_location_type_id', '=', CondLocationType::TYPE_BLOCK)
                ->leftjoin('score', 'score.score_id', '=', 'cond_location_score.score_id')
                ->leftjoin(
                    'building_block',
                    'building_block.building_block_id',
                    '=',
                    'cond_location_score.location_record_id'
                )
                ->leftjoin('building', 'building.building_id', '=', 'building_block.building_id')
                ->leftjoin('site', 'site.site_id', '=', 'building.site_id')
                ->leftjoin('address', 'address.address_id', '=', 'building.address_id')
                ->leftjoin('address_street', 'address_street.address_street_id', '=', 'address.address_street_id')
                ->leftjoin(
                    'building_block_type',
                    'building_block_type.building_block_type_id',
                    '=',
                    'building_block.building_block_type_id'
                )
                ->leftjoin(
                    'building_age',
                    'building_age.building_age_id',
                    '=',
                    'building_block.building_age_id'
                )
                ->leftjoin(
                    'construction_type',
                    'construction_type.construction_type_id',
                    '=',
                    'building_block.construction_type_id'
                )
                ->orderBy('cond_location_score.cond_location_score_id')
                ->get();
            foreach ($locBlockScoreData as $locBlockScore) {
                \Log::info("Block Data ({$locBlockScore->building_block_code})");
                $blockData['block'] = $locBlockScore;

                if ($locBuildScore->score_code) {
                    $blockData['block_score_code'] = Common::concatFields(
                        [$locBuildScore->score_code,
                        $locBuildScore->score_desc]
                    );
                } else {
                    $blockData['block_score_code'] = 'Not Selected.';
                }

                $totalCost = 0;
                $numIdwork = 0;
                foreach ($condSurvey['idworks'] as $idwork) {
                    if ($idwork->room_id && ($idwork->building_block_id == $locBuildScore->building_block_id)) {
                        ++$numIdwork;
                        $totalCost = Math::addCurrency([$totalCost, $idwork->total_cost]);
                    }
                }
                $blockData['numBlockIdwork'] = $numIdwork;
                $blockData['totalBlockCost'] = $totalCost;
                $condScoreLocId = $locBlockScore->cond_location_score_id;
                $idworkElementScores = $this->getElementScores($idworkElements, $condScoreLocId);
                $blockData['locationScore'] = $idworkElementScores;

                if ($idworkSubElements) {
                    $idworkSubElementScores = $this->getSubElementScores($idworkSubElements, $condScoreLocId);
                    $blockData['blockSubElementScores'] = $idworkSubElementScores;

                    if ($idworkItems) {
                        $idworkItemScores = $this->getItemScores($idworkItems, $condScoreLocId);
                        $blockData['blockItemScores'] = $idworkItemScores;
                    }
                }

                array_push($buildingData['blocks'], $blockData);
            }
            array_push($buildingLocationScores, $buildingData);
        }

        $condSurvey['buildingLocationScores'] = $buildingLocationScores;

        // For Debug purposes only.
        //$this->showData($condSurvey);
    }


    private function setBuildingIdworkElementTotals(
        &$condSurvey,
        $idworkPriorities,
        $idworkElementsQuery,
        $idworkSubElementsQuery = false,
        $idworkItemsQuery = false
    ) {
        $idworkElements = $idworkElementsQuery->get();

        $idworkSubElements = false;
        $idworkItems = false;

        if ($idworkSubElementsQuery) {
            $idworkSubElements = $idworkSubElementsQuery->get();
            if ($idworkItemsQuery) {
                $idworkItems = $idworkItemsQuery->get();
            }
        }

        $this->getLocationScores($condSurvey, $idworkElements, $idworkSubElements, $idworkItems);

        $condSurveyBuildings = Building::select([
            'building.building_id',
            'building.building_code',
            'building.building_desc'
        ])
            ->where('building.site_id', '=', $condSurvey->site_id)
            ->leftjoin(
                'address',
                'address.address_id',
                '=',
                'building.address_id'
            )
            ->leftjoin(
                'address_street',
                'address_street.address_street_id',
                '=',
                'address.address_street_id'
            )
            ->leftjoin('building_type', 'building_type.building_type_id', '=', 'building.building_type_id')
            ->leftjoin('listed_type', 'listed_type.listed_type_id', '=', 'building.listed_type_id')
            ->leftjoin('building_usage', 'building_usage.building_usage_id', '=', 'building.building_usage_id')
            ->leftjoin('ward', 'ward.ward_id', '=', 'building.ward_id')
            ->get();

        $condSurvey['allBuildings'] = $condSurveyBuildings;

        foreach ($condSurvey['allBuildings'] as $building) {
            //get all idwork for the report
            $buildingIdworks = Idwork::select()
                ->where('idwork.building_id', '=', $building->building_id)
                ->where('idwork.condsurvey_id', '=', $condSurvey->condsurvey_id)
                ->orderBy('idwork.idwork_priority_id')->get();

            $bIdworkElements = $idworkElementsQuery->get();
            $building['completeTotal'] = 0;

            $building['idworkElements'] = $bIdworkElements;
            $building['NoData'] = 'true';

            if (count($buildingIdworks) > 0) {
                foreach ($building['idworkElements'] as $bIdworkElement) {
                    $bIdworkElement['lineTotal'] = 0;
                    $bIdworkElement['notSelected'] = 0;
                    $bIdworkElement['notSelectedTotal'] = 0;
                    $bIndex = 1;

                    foreach ($idworkPriorities as $priority) {
                        //set a flag that the building has no idwork
                        $building['NoData'] = 'false';
                        $bIdworkElement[$bIndex] = 0; //initialise and reset index
                        foreach ($buildingIdworks as $bIdwork) {
                            if ($bIdwork->element_id === $bIdworkElement->idwork_element_id) {
                                if ((!$bIdwork->idwork_priority_id) && ($bIndex === 1)) {
                                    $bIdworkElement['lineTotal'] = Math::addCurrency([
                                        $bIdworkElement['lineTotal'],
                                        $bIdwork->total_cost
                                        ]);

                                    $bIdworkElement['notSelected'] = Math::addCurrency([
                                        $bIdworkElement['notSelected'],
                                        $bIdwork->total_cost
                                    ]);
                                    $bIdworkElement['notSelectedTotal'] = Math::addCurrency([
                                        $bIdworkElement['notSelectedTotal'],
                                        $bIdwork->total_cost
                                        ]);
                                    $building['completeTotal'] = Math::addCurrency([
                                        $building['completeTotal'],
                                        $bIdwork->total_cost
                                    ]);
                                }
                            }
                            if ($bIdwork->idwork_priority_id === $priority->idwork_priority_id) {
                                if ($bIdwork->element_id === $bIdworkElement->idwork_element_id) {
                                    $building['completeTotal'] = Math::addCurrency([
                                        $building['completeTotal'],
                                        $bIdwork->total_cost
                                    ]);

                                    $bIdworkElement[$bIndex] = $bIdworkElement[$bIndex] + $bIdwork->total_cost;
                                    $bIdworkElement['lineTotal'] = Math::addCurrency([
                                        $bIdworkElement['lineTotal'],
                                        $bIdwork->total_cost
                                    ]);
                                    $building[$bIndex] = Math::addCurrency([
                                        $building[$bIndex],
                                        $bIdwork->total_cost
                                    ]);
                                } else {
                                    $bIdworkElement[$bIndex] = $bIdworkElement[$bIndex] + 0;
                                    $bIdworkElement['notSelected'] = Math::addCurrency([
                                        $bIdworkElement['notSelected'],
                                        0
                                    ]);
                                }
                            } else {
                                $building[$bIndex] = Math::addCurrency([
                                    $building[$bIndex],
                                    0
                                ]);
                            }
                        }
                        $bIndex++;
                    } //end foreach priority
                    $building['notSelectedTotal'] = Math::addCurrency([
                        $building['notSelectedTotal'],
                        $bIdworkElement['notSelectedTotal']
                        ]);
                } //end foreach idwork element
            }
        } //end foreach building

        foreach ($condSurvey['allBuildings'] as $building) {
            foreach ($building['idworkElements'] as $bIdworkElement) {
                //after element data, get score data
                $locScoreData = CondLocationScore::select()
                    ->where('cond_location_score.condsurvey_id', '=', $condSurvey->condsurvey_id)
                    ->where('cond_location_score.cond_location_type_id', '=', '2')
                    ->where('cond_location_score.location_record_id', '=', $building->building_id)
                    ->leftjoin('score', 'score.score_id', '=', 'cond_location_score.score_id')
                    ->orderBy('cond_location_score.cond_location_score_id')
                    ->get();

                $condScoreLocId = "";
                $condLocScoreComment = "None given.";
                $check = 0;
                $building['overallScore'] = 'Not Selected.';

                foreach ($locScoreData as $locScore) {
                    if ($check === 0) {
                        $condScoreLocId = $locScore->cond_location_score_id;
                        $condLocScoreComment = $locScore->cond_location_score_comment;
                        //set the overall score
                        if ($locScore->score_code) {
                            $building['overallScore'] = $locScore->score_code . " - " . $locScore->score_desc;
                        } else {
                            $building['overallScore'] = 'Not Selected.';
                        }
                    } else {
                        break;
                    }
                    $check++;
                } //get first result only
                $building['scoreComment'] = $condLocScoreComment;

                //then get building scores
                $buildingScores = CondLocationElementScore::select()
                    ->where(
                        'cond_location_element_score.cond_location_score_id',
                        '=',
                        $condScoreLocId
                    )
                    ->where(
                        'cond_location_element_score.idwork_element_id',
                        '=',
                        $bIdworkElement->idwork_element_id
                    )
                    ->leftjoin(
                        'score',
                        'score.score_id',
                        '=',
                        'cond_location_element_score.score_id'
                    )
                    ->first();

                if ($buildingScores) {
                    $building['hasScores'] = 'true';
                    if (!$buildingScores->score_code) {
                        $bIdworkElement['scoreItem'] = 'N/A';
                    } else {
                        $bIdworkElement['scoreItem'] = $buildingScores->score_code;
                    }
                    if (!$condLocScoreComment) {
                        $bIdworkElement['scoreComment'] = 'None given.';
                    } else {
                        $bIdworkElement['scoreComment'] = $condLocScoreComment;
                    }
                } else {
                    $bIdworkElement['scoreComment'] = $condLocScoreComment;
                    $building['hasScores'] = 'false';
                }
            }
        } //end foreach building for condition scores
        return;
    }

    private function setIdworkElementTotals(&$condSurvey, &$idworkElements, $idworkPriorities, $idworks)
    {
        $condSurvey['completeTotal'] = 0;
        foreach ($idworkElements as $idworkElement) {
            ///reset index at start of each loop
            $priorityIndex  = 1;
            $idworkElement['notSelected'] = 0;
            $idworkElement['notSelectedTotal'] = 0;
            $idworkElement['lineTotal'] = 0;

            foreach ($idworkPriorities as $idwPriority) {
                $idworkElement[$priorityIndex] = 0;

                foreach ($idworks as $idwork) {
                    if ($idworkElement->idwork_element_id === $idwork->element_id) {
                        if ((!$idwork->idwork_priority_id) && ($priorityIndex === 1)) {
                            //assign idworks with no selected priority to the priority totals column
                            //only fire once
                            $idworkElement['notSelected'] = Math::addCurrency([
                                $idworkElement['notSelected'],
                                $idwork->total_cost
                                ]);
                            $idworkElement['notSelectedTotal'] = Math::addCurrency([
                                $idworkElement['notSelectedTotal'],
                                $idwork->total_cost
                                ]);
                            $idworkElement['lineTotal'] = Math::addCurrency([
                                $idworkElement['lineTotal'],
                                $idwork->total_cost
                                ]);

                            $condSurvey['completeTotal'] = Math::addCurrency([
                                $condSurvey['completeTotal'],
                                $idwork->total_cost
                            ]);
                        } else {
                            $idworkElement['notSelected'] = Math::addCurrency([
                                $idworkElement['notSelected'],
                                0
                            ]);
                        }

                        if ($idwPriority->idwork_priority_id === $idwork->idwork_priority_id) {
                            $idworkElement[$priorityIndex] = Math::addCurrency(
                                [
                                    $idworkElement[$priorityIndex],
                                    $idwork->total_cost
                                ]
                            );
                            $idworkElement['lineTotal'] = Math::addCurrency([
                                $idworkElement['lineTotal'],
                                $idwork->total_cost
                                ]);
                            $condSurvey[$priorityIndex] = Math::addCurrency([
                                $condSurvey[$priorityIndex],
                                $idwork->total_cost
                            ]);
                            $condSurvey['completeTotal'] = Math::addCurrency([
                                $condSurvey['completeTotal'],
                                $idwork->total_cost
                            ]);
                        } else {
                            $idworkElement[$priorityIndex] = Math::addCurrency([$idworkElement[$priorityIndex], 0]);
                            $condSurvey[$priorityIndex] = Math::addCurrency([
                                $condSurvey[$priorityIndex],
                                0
                            ]);
                        }
                    } //end if element id == idwork element id
                }
                if (empty($idworks)) {
                    $condSurvey[$priorityIndex] = 0.00;
                }

                $priorityIndex = Math::addInt([$priorityIndex, 1]);
            } //end foreach priority
            $condSurvey['notSelectedTotal'] = Math::addCurrency([
                $condSurvey['notSelectedTotal'],
                $idworkElement['notSelectedTotal']
            ]);
        } //end foreach element
        $condSurvey['idworkElements'] = $idworkElements;
    }

    /**
     * Setup all the valid Identified Work User Defined Labels in an array
     */
    private function setIdworkGenUserdefLabels($siteGroupId)
    {
        // get the ID work labels
        $idworkGenUserdefLabel = GenUserdefLabel::where('gen_userdef_label.site_group_id', '=', $siteGroupId)
            ->where('gen_userdef_label.gen_table_id', '=', GenTable::IDWORK)
            ->first()->toArray();

        $idworkGenUserdefLabels = [];

        foreach ($this->genUserdefTypes as $genUserdefType) {
            $this->setValidIdworkGenUserdefLabels($idworkGenUserdefLabel, $idworkGenUserdefLabels, $genUserdefType);
        }

        return $idworkGenUserdefLabels;
    }

    /**
     * Save all the valid, not null, user defined field labels, with the field type and number, for a specific type
     */
    private function setValidIdworkGenUserdefLabels($idworkGenUserdefLabel, &$idworkGenUserdefLabels, $genUserdefType)
    {
        // loop through the user defined labels for a specific type
        for ($i = 1; $i < self::MAX_USER_DEFINED_FIELDS; $i++) {
            // create the array key using type and number
            $key = "user_${genUserdefType}${i}_label";
            if (array_key_exists($key, $idworkGenUserdefLabel)) {
                // it is a valid user defined label
                if (!is_null($idworkGenUserdefLabel[$key])) {
                    // the user defined label is not null so save it
                    $idworkGenUserdefLabels [] = [
                        'type'      => $genUserdefType,
                        'number'    => $i,
                        'label'     => $idworkGenUserdefLabel[$key],
                    ];
                }
            } else {
                // no more valid user defined fields to be found so end loop
                break;
            }
        }
    }

    /**
     * Populate user defined array of values for all user defined fields that are turned on.
     */
    private function setIdworkGenUserdefValues($idwork, $idworkGenUserdefLabels)
    {
        $userDefValues = [];

        foreach ($idworkGenUserdefLabels as $idworkGenUserdefLabel) {
            $type = $idworkGenUserdefLabel['type'];
            $number = $idworkGenUserdefLabel['number'];
            if ($type === 'selection') {
                // for selections get the id column
                $column = "sel${number}_gen_userdef_sel_id";
            } else {
                // create the column name using type and number
                $column = "user_${type}${number}";
            }
            $value = $idwork->$column;
            if (!$value) {
                // value is not set, so set ouput to blank
                $userDefValues [] = '';
                continue;
            }
            switch ($type) {
                case 'date':
                    // convert yyyy-mm-dd date to dd/mm/yyyy
                    $userDefValues [] = \DateTime::createFromFormat('Y-m-d', $value)->format('d/m/Y');
                    break;
                case 'selection':
                    // convert the selection id to code and description
                    $userDefValues [] = GenUserDefSel::find($value)->getCodeDescAttribute();
                    break;
                case 'contact':
                    // convert the contact id to name and organisation
                    $userDefValues [] = Contact::find($value)->getContactOrganistationAttribute();
                    break;
                default:
                    // the default action is to use the column value as is
                    $userDefValues [] = $idwork->$column;
            }
        }

        // update idwork with the user defined arrays
        $idwork->userDefLabels = array_column($idworkGenUserdefLabels, 'label');
        $idwork->userDefValues = $userDefValues;
    }

    private function generateFilterData($filterData, &$whereCodes, &$whereTexts, &$orCodes)
    {
        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Condition Survey Code = '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Condition Survey Description = '" . $val . "'");
        }

        if ($val = Common::iset($filterData['superseded'])) {
            array_push($whereTexts, "Superseded = '" . $val . "'");
        }

        if ($siteId = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $siteId, "Site Code"]);

            if ($buildingId = Common::iset($filterData['building_id'])) {
                array_push(
                    $whereCodes,
                    ['building', 'building_id', 'building_code', $buildingId, "Building Code"]
                );

                if ($val = Common::iset($filterData['block_id'])) {
                    array_push($whereCodes, [
                        'building_block',
                        'building_block_id',
                        'building_block_code',
                        $val,
                        "Building Block Code"
                    ]);
                }

                if ($val = Common::iset($filterData['room_id'])) {
                    array_push($whereCodes, ['room', 'room_id', 'room_number', $val, "Room Number"]);
                }
            }
        }
    }

    private function formatInputs(&$inputs)
    {
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $isSuperseded = $reportFilterQuery->getValueFilterField('superseded');

            if (isset($isSuperseded[0]) && Common::valueYorNtoBoolean($isSuperseded[0])) {
                $inputs['superseded'] = true;
            }
        }

        return $inputs;
    }
}
