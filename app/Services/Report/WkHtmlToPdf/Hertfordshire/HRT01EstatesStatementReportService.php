<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf\Hertfordshire;

use Illuminate\Support\Arr;
use Tfcloud\Controllers\Admin\General\SiteGroupsController;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Models\Contact;
use Tfcloud\Models\EstCharge;
use Tfcloud\Models\EstSalesInvoiceLine;
use Tfcloud\Models\FinYear;
use Tfcloud\Models\LeaseOut;
use Tfcloud\Models\Note;
use Tfcloud\Models\Report;
use Tfcloud\Models\SiteGroup;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Admin\General\SiteGroupsService;
use Tfcloud\Services\Estate\FeeReceiptService;
use Tfcloud\Services\Estate\EstateSalesInvoiceService;
use Tfcloud\Services\Report\WkHtmlToPdf\WkHtmlToPdfReportBaseService;

class HRT01EstatesStatementReportService extends WkHtmlToPdfReportBaseService
{
    public $filterText = '';

    private $feeReceiptService;
    private $invoiceService;

    private Report $report;

    public function __construct(PermissionService $permissionService, Report $report)
    {
        parent::__construct($permissionService);
        $this->sgService = new SiteGroupsService($permissionService);
        $this->sgController = new SiteGroupsController($permissionService, $this->sgService);
        $this->feeReceiptService = new FeeReceiptService($permissionService);
        $this->invoiceService = new EstateSalesInvoiceService($permissionService);
        $this->report = $report;
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function runReport($repOutPutFile, $inputs, $repId)
    {
        $exportType = array_get($inputs, 'exportType');
        $this->formatInputData($inputs);

        $this->tenant = Contact::where('contact_id', '=', $inputs['contact'])->first();
        $this->leaseOut = LeaseOut::where('tenant_contact_id', '=', $inputs['contact'])->first();

        $this->historicDebt = EstCharge::where('lease_out_id', '=', $this->leaseOut->lease_out_id)
                ->where('est_charge_type_id', '=', 8)->first();

        $this->tenantLease = LeaseOut::where('tenant_contact_id', '=', $inputs['contact'])->first();

        $this->timePeriod = EstSalesInvoiceLine::join(
            'est_sales_invoice',
            'est_sales_invoice.est_sales_invoice_id',
            '=',
            'est_sales_invoice_line.est_sales_invoice_id'
        )
        ->join(
            'est_charge',
            'est_charge.est_charge_id',
            '=',
            'est_sales_invoice_line.est_charge_id'
        )
        ->where(
            'est_sales_invoice.customer_contact_id',
            '=',
            $this->tenant->contact_id
        )
        ->where(
            'est_charge.rent',
            '=',
            'Y'
        )->get();

        $holdingArray = [];

        $pass = 0;

        foreach ($this->timePeriod as $period) {
            // Main query to build up data
            $this->mondayDate = $period->start_date;
            $this->endDate = $period->end_date;
            $this->lastMonday = date('Y-m-d', strtotime('-7 days', strtotime($period->start_date)));
            $this->lastEndDate = date('Y-m-d', strtotime('-7 days', strtotime($period->end_date)));
            $pass++;

            $query = \DB::table('est_sales_invoice_line')->select([
                'est_sales_invoice_line.start_date',
                'est_sales_invoice_line.net_total',
                \DB::raw('COALESCE(cuug.user_text2, "N/A") as ut2'),
                'ut_utility_bill.total_charge',
                'ut_utility_bill_tariff.unit_usage',
                'ut_utility_bill_tariff.unit_cost',
                'lease_out.lease_out_id'
            ])
            ->join(
                'est_sales_invoice',
                'est_sales_invoice.est_sales_invoice_id',
                '=',
                'est_sales_invoice_line.est_sales_invoice_id'
            )
            ->join(
                'lease_out',
                'lease_out.lease_out_id',
                '=',
                'est_sales_invoice.lease_out_id'
            )
            ->join(
                'lease_out_letu',
                'lease_out_letu.lease_out_id',
                '=',
                'lease_out.lease_out_id'
            )
            ->join('lettable_unit', function ($join) {
                $join->on('lettable_unit.lettable_unit_id', '=', 'lease_out_letu.lettable_unit_id');
                $join->on('lettable_unit.est_unit_type_id', '=', \DB::raw(1));
            })
            ->join('gen_userdef_group as luug', function ($join) {
                $join->on('luug.gen_userdef_group_id', '=', 'lettable_unit.gen_userdef_group_id');
                $join->on('luug.gen_table_id', '=', \DB::raw(8));
            })
            ->join(
                'est_charge',
                'est_charge.est_charge_id',
                '=',
                'est_sales_invoice_line.est_charge_id'
            )
            ->join(
                'contact',
                'contact.contact_id',
                '=',
                'lease_out.tenant_contact_id'
            )
            ->join('gen_userdef_group as cuug', function ($join) {
                $join->on('cuug.gen_userdef_group_id', '=', 'contact.gen_userdef_group_id');
                $join->on('cuug.gen_table_id', '=', \DB::raw(55));
            })
            ->join(
                'ut_utility',
                'ut_utility.serial_number',
                '=',
                'luug.user_text1'
            )
            ->leftJoin('ut_utility_bill', function ($join) {
                $join->on('ut_utility_bill.ut_utility_id', '=', 'ut_utility.ut_utility_id');
            })
            ->leftJoin(
                'ut_utility_bill_tariff',
                'ut_utility_bill_tariff.ut_utility_bill_id',
                '=',
                'ut_utility_bill.ut_utility_bill_id'
            )
            ->where(
                'lease_out.tenant_contact_id',
                '=',
                $this->tenant->contact_id
            )
            ->where(
                'est_sales_invoice_line.start_date',
                '>=',
                $this->mondayDate
            )
            ->where(
                'est_sales_invoice_line.end_date',
                '<=',
                $this->endDate
            )
            ->where(
                'est_charge.active',
                '=',
                'Y'
            )
            ->where(
                'est_charge.rent',
                '=',
                'Y'
            )
            ->where(
                'ut_utility_bill.period_start_date',
                '<=',
                $this->endDate
            )
            ->where(
                'ut_utility_bill.period_end_date',
                '>=',
                $this->mondayDate
            );

            $this->mainData = $query->get();

            $waterQuery = \DB::table('est_sales_invoice_line')->select([
                'est_charge.regular_payment_amount'
            ])
            ->join(
                'est_sales_invoice',
                'est_sales_invoice.est_sales_invoice_id',
                '=',
                'est_sales_invoice_line.est_sales_invoice_id'
            )
            ->join(
                'lease_out',
                'lease_out.lease_out_id',
                '=',
                'est_sales_invoice.lease_out_id'
            )
            ->join(
                'est_charge',
                'est_charge.est_charge_id',
                '=',
                'est_sales_invoice_line.est_charge_id'
            )
            ->join(
                'est_charge_type',
                'est_charge_type.est_charge_type_id',
                '=',
                'est_charge.est_charge_type_id'
            )
            ->where(
                'lease_out.tenant_contact_id',
                '=',
                $this->tenant->contact_id
            )
            ->where(
                'est_sales_invoice_line.start_date',
                '>=',
                $this->mondayDate
            )
            ->where(
                'est_sales_invoice_line.end_date',
                '<=',
                $this->endDate
            )
            ->where(
                'est_charge.active',
                '=',
                'Y'
            )
            ->where(
                'est_charge_type.est_charge_type_code',
                '=',
                'SCW'
            )->first();

            $electricQuery = \DB::table('est_sales_invoice_line')->select([
                'est_charge.regular_payment_amount'
            ])
            ->join(
                'est_sales_invoice',
                'est_sales_invoice.est_sales_invoice_id',
                '=',
                'est_sales_invoice_line.est_sales_invoice_id'
            )
            ->join(
                'lease_out',
                'lease_out.lease_out_id',
                '=',
                'est_sales_invoice.lease_out_id'
            )
            ->join(
                'est_charge',
                'est_charge.est_charge_id',
                '=',
                'est_sales_invoice_line.est_charge_id'
            )
            ->join(
                'est_charge_type',
                'est_charge_type.est_charge_type_id',
                '=',
                'est_charge.est_charge_type_id'
            )
            ->where(
                'lease_out.tenant_contact_id',
                '=',
                $this->tenant->contact_id
            )
            ->where(
                'est_sales_invoice_line.start_date',
                '>=',
                $this->mondayDate
            )
            ->where(
                'est_sales_invoice_line.end_date',
                '<=',
                $this->endDate
            )
            ->where(
                'est_charge.active',
                '=',
                'Y'
            )
            ->where(
                'est_charge_type.est_charge_type_code',
                '=',
                'SCE'
            )->first();

            $noteQuery = \DB::table('note')->select([
                'detail',
                \DB::raw('substr(detail,1,10) as trans_date'),
                \DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(detail, "[", -1), "]", 1) as debt_amount ')
            ])
            ->join(
                'lease_out',
                'lease_out.lease_out_id',
                '=',
                'note.record_id'
            )
            ->where(
                'note.note_type_id',
                '=',
                64
            )
            ->where(
                \DB::raw('substr(detail,1,10)'),
                '>=',
                $this->mondayDate
            )
            ->where(
                \DB::raw('substr(detail,1,10)'),
                '<=',
                $this->endDate
            )
            ->where(
                'lease_out_id',
                '=',
                $this->tenantLease->lease_out_id
            )->first();

            $lastWeek = \DB::table('note')->select([
                'detail',
                \DB::raw('substr(detail,1,10) as trans_date'),
                \DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(detail, "[", -1), "]", 1) as debt_amount ')
            ])
            ->join(
                'lease_out',
                'lease_out.lease_out_id',
                '=',
                'note.record_id'
            )
            ->where(
                'note.note_type_id',
                '=',
                64
            )
            ->where(
                \DB::raw('substr(detail,1,10)'),
                '>=',
                $this->lastMonday
            )
            ->where(
                \DB::raw('substr(detail,1,10)'),
                '<=',
                $this->lastEndDate
            )
            ->where(
                'lease_out_id',
                '=',
                $this->tenantLease->lease_out_id
            )->first();

            $transactionAmount = \DB::table('note')->select([
                'detail',
                \DB::raw('substr(detail,1,10) as trans_date'),
                \DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(detail, "[", -1), "]", 1) as amount_paid ')
            ])
            ->join(
                'lease_out',
                'lease_out.lease_out_id',
                '=',
                'note.record_id'
            )
            ->where(
                'note.note_type_id',
                '=',
                85
            )
            ->where(
                \DB::raw('substr(detail,1,10)'),
                '>=',
                $this->mondayDate
            )
            ->where(
                \DB::raw('substr(detail,1,10)'),
                '<=',
                $this->endDate
            )
            ->where(
                'lease_out_id',
                '=',
                $this->tenantLease->lease_out_id
            )->first();

            $this->first = $this->mainData->first();

            if (isset($this->first->start_date)) {
                $holdingArray[$this->mondayDate]['start_date'] = date('d/m/Y', strtotime($this->first->start_date));
                $holdingArray[$this->mondayDate]['net_total'] = $this->first->net_total;
                $holdingArray[$this->mondayDate]['ut2'] = $this->first->ut2;
                $holdingArray[$this->mondayDate]['total_charge'] = $this->first->total_charge;
                $holdingArray[$this->mondayDate]['unit_usage'] = $this->first->unit_usage;
                $holdingArray[$this->mondayDate]['unit_cost'] = $this->first->unit_cost;
            } else {
                $holdingArray[$this->mondayDate]['start_date'] = date('d/m/Y', strtotime($this->mondayDate));
                $holdingArray[$this->mondayDate]['net_total'] = 0;
                $holdingArray[$this->mondayDate]['ut2'] = 0;
                $holdingArray[$this->mondayDate]['total_charge'] = 0;
                $holdingArray[$this->mondayDate]['unit_usage'] = 0;
                $holdingArray[$this->mondayDate]['unit_cost'] = 0;
            }

            $holdingArray[$this->mondayDate]['water_charge'] = 0;
            $holdingArray[$this->mondayDate]['elec_charge'] = 0;

            if (isset($waterQuery->regular_payment_amount)) {
                $holdingArray[$this->mondayDate]['water_charge'] = $waterQuery->regular_payment_amount;
            }

            if (isset($electricQuery->regular_payment_amount)) {
                $holdingArray[$this->mondayDate]['elec_charge'] = $electricQuery->regular_payment_amount;
            }

            if (isset($noteQuery->debt_amount)) {
                $holdingArray[$this->mondayDate]['debt_amount'] = $noteQuery->debt_amount;
                if (isset($lastWeek->debt_amount)) {
                    $holdingArray[$this->mondayDate]['balance'] = $lastWeek->debt_amount;
                } else {
                    $holdingArray[$this->mondayDate]['balance'] = 0.00;
                }
                if (isset($this->first->net_total)) {
                    $holdingArray[$this->mondayDate]['rent_due'] =
                    $holdingArray[$this->mondayDate]['balance'] - $this->first->net_total;
                } else {
                    $holdingArray[$this->mondayDate]['rent_due'] = 0.00;
                }
            } else {
                $holdingArray[$this->mondayDate]['debt_amount'] = 0.00;
                $holdingArray[$this->mondayDate]['balance'] = 0.00;
                $holdingArray[$this->mondayDate]['rent_due'] = 0.00;
            }

            if (isset($transactionAmount->amount_paid)) {
                $holdingArray[$this->mondayDate]['amount_paid'] = $transactionAmount->amount_paid;
            } else {
                $holdingArray[$this->mondayDate]['amount_paid'] = 0;
            }
        }

        $this->holdingArray = $holdingArray;

        $this->electricCharge = EstCharge::join(
            'est_charge_type',
            'est_charge.est_charge_type_id',
            '=',
            'est_charge_type.est_charge_type_id'
        )
        ->where('lease_out_id', '=', $this->leaseOut->lease_out_id)
        ->where('est_charge_type_code', '=', 'SCE')
        ->first();

        $this->waterCharge = EstCharge::join(
            'est_charge_type',
            'est_charge.est_charge_type_id',
            '=',
            'est_charge_type.est_charge_type_id'
        )
        ->where('lease_out_id', '=', $this->leaseOut->lease_out_id)
        ->where('est_charge_type_code', '=', 'SCW')
        ->first();

        foreach ($this->timePeriod as $period) {
            $this->debtNotes = Note::join(
                'note_record_type',
                'note_record_type.note_record_type_id',
                '=',
                'note.record_type_id'
            )
            ->where('record_id', '=', $this->leaseOut->lease_out_id)
            ->where('note.record_type_id', '=', 28)
            ->where('note_record_type.note_record_type_code', '=', 'GY02')
            ->where('note_date', '>', $period->start_date)
            ->where('note_date', '<', $period->end_date)->get();

            $this->creditNotes = Note::join(
                'note_record_type',
                'note_record_type.note_record_type_id',
                '=',
                'note.record_type_id'
            )
            ->where('record_id', '=', $this->leaseOut->lease_out_id)
            ->where('note.record_type_id', '=', 28)
            ->where('note_record_type.note_record_type_code', '=', 'GY03')->get();
        }

        $siteGroupId = \Auth::User()->site_group_id;
        $this->photoPath = $this->sgController->getPhotoPath($siteGroupId); //get logo for outputting at top of report

        $siteGroup = SiteGroup::select()->where('site_group_id', '=', $siteGroupId)->first();
        $this->siteGroupHeader = $siteGroup->code . " - " . $siteGroup->description;

        //Gen file
        $this->genReport($exportType, $repOutPutFile, $inputs, $repId);

        return true;
    }

    private function genReport($reportType, $repOutPutFile, $inputs, $repId)
    {
        $this->genPdf($repOutPutFile, $repId);
    }

    private function formatInputData(&$inputs)
    {
        foreach ($inputs as $key => $value) {
            if ($value === "") {
                unset($inputs[$key]);
            }
        }
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $value = $reportFilterQuery->getValueFilterField("tenant_contact_id");
            $inputs['contact'] = Arr::first($value);
        }

        return $inputs;
    }
    public function validateFilter($inputs)
    {
        return \Validator::make($this->formatInputData($inputs), $this->validateRules(), $this->validateMessages());
    }

    public function validateRules()
    {
        $rules = [
            'contact' => ['required'],
        ];

        return $rules;
    }

    public function validateMessages()
    {
        $messages = [
            'contact.required' => 'Tenant is required.'
        ];

        return $messages;
    }

    private function genPdf($repOutPutPdfFile, $repId)
    {
        // Create wkhtmltopdf temp folder to store generated files
        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);

        $header = \View::make(
            'reports.wkhtmltopdf.estates.hrt01.header',
            [
                'photoPath' => $this->photoPath,
                'siteGroupHeader' => $this->siteGroupHeader
            ]
        )->render();

        $headerPath = $generatedPath . "/header.html";
        file_put_contents($headerPath, $header, LOCK_EX);

        $footer = \View::make('reports.wkhtmltopdf.estates.hrt01.footer')->render();
        $footerPath = $generatedPath . "/footer.html";
        file_put_contents($footerPath, $footer, LOCK_EX);

        $historicDebt = isset($this->historicDebt->amount) ? $this->historicDebt->amount : 0;

        $content = \View::make(
            'reports.wkhtmltopdf.estates.hrt01.content',
            [
                'tenant' => $this->tenant,
                'lease' => $this->tenantLease,
                'mainData' => $this->holdingArray,
                'historicDebt' => $historicDebt
            ]
        )->render();

        $contentPath = $generatedPath . "/content.html";

        file_put_contents($contentPath, $content, LOCK_EX);

        $options = [
            'header-html' => $headerPath,
            'footer-html' => $footerPath,
            'header-spacing' => 4,
            'footer-spacing' => 4,
            'orientation' => 'landscape',
        ];

        $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $options);
    }
}
