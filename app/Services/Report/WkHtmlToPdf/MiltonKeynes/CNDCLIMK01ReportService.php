<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf\MiltonKeynes;

use Tfcloud\Controllers\Admin\General\SiteGroupsController;
use Tfcloud\Lib\Common;
use Tfcloud\Models\Condsurvey;
use Tfcloud\Models\CondsurveyStatus;
use Tfcloud\Models\Idwork;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Admin\General\SiteGroupsService;
use Tfcloud\Services\Condition\ConditionService;
use Tfcloud\Services\Report\WkHtmlToPdf\WkHtmlToPdfReportBaseService;

class CNDCLIMK01ReportService extends WkHtmlToPdfReportBaseService
{
    private $condService;
    private $sgController;
    private $sgService;
    public $filterText = '';

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->condService = new ConditionService($permissionService);
        $this->sgService = new SiteGroupsService($permissionService);
        $this->sgController = new SiteGroupsController($permissionService, $this->sgService);
    }

    public function validateFilter($inputs)
    {
        return \Validator::make($inputs, $this->validateRules(), $this->validateMessages());
    }

    public function validateRules()
    {
        $rules = [
            'site_id' => ['required'],
        ];

        return $rules;
    }

    public function validateMessages()
    {
        $messages = [
            'site_id.required' => 'Location is required.'
        ];

        return $messages;
    }

    public function runReport($repOutPutPdfFile, $inputs, $repId)
    {
        $siteGroupId = \Auth::User()->site_group_id;
        $logoPath = $this->sgController->getPhotoPath($siteGroupId); //get logo for outputting at top of report
        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);
        $arrPdfs = [];

        $condQuery = $this->getCondSurveyData($inputs, $siteGroupId);
        $conditionSurveys = $condQuery->get();

        if ($conditionSurveys->count()) {
            // Generate the footer HTML.
            $footer = \View::make('reports.wkhtmltopdf.condition.cndCliMK01.footer')->render();
            $footerPath = $generatedPath . "/footer.html";
            file_put_contents($footerPath, $footer, LOCK_EX);
        }


        foreach ($conditionSurveys as $condition) {
            $idWorks = $this->getIdWork($condition->getKey());

            $photoName = $condition->building_photo_name;
            if ($photoName) {
                //get the image
                $buildingPhotoPath = $this->getPhotoStoragePathway(
                    $siteGroupId,
                    $condition->site_id,
                    'site'
                );
                //pathway checking in wrong folder, redirect
                $splitString = explode("site/", $buildingPhotoPath);
                $firstHalf = $splitString[0];
                $buildingPhotoPath = $firstHalf . "site/" . $condition->site_id . "/" . $photoName;
                $condition->building_photo = $buildingPhotoPath;
            } else {
                $condition->building_photo = null;
            }

            // Cover Page HTML.
            $content = \View::make(
                'reports.wkhtmltopdf.condition.cndCliMK01.content',
                [
                    'logoPath' => $logoPath,
                    'condition' => $condition
                ]
            )->render();
            $firstSectionsContentPath = $generatedPath . "/firstSection_{$condition->getKey()}.html";
            file_put_contents($firstSectionsContentPath, $content, LOCK_EX);

            $options = [
                'footer-html'    => $footerPath,
                'footer-spacing' => -10,
                "margin-bottom"  => 5,
                "margin-left"    => 5,
                "margin-right"   => 5,
                "margin-top"     => 5,
                'orientation'    => 'landscape',
            ];

            // Generate the content HTML
            $StaticContent1Path = $this->generateStaticPages($logoPath, $condition, $generatedPath, 'static_content');
            $StaticContent2Path = $this->generateStaticPages($logoPath, $condition, $generatedPath, 'static_contentp2');
            $StaticContent3Path = $this->generateStaticPages($logoPath, $condition, $generatedPath, 'static_contentp3');
            $StaticContent4Path = $this->generateStaticPages($logoPath, $condition, $generatedPath, 'static_contentp4');
            $StaticContent5Path = $this->generateStaticPages($logoPath, $condition, $generatedPath, 'static_contentp5');
            $idWorkPath = $this->generateIWPages($logoPath, $condition, $idWorks, $generatedPath);
            $StaticContent6Path = $this->generateStaticPages($logoPath, $condition, $generatedPath, 'static_contentp6');
            $StaticContent7Path = $this->generateStaticPages($logoPath, $condition, $generatedPath, 'static_contentp7');

            // Convert the HTML to PDF.
            $firstPdf =  $this->generateMultiHtmlToPdf(
                [
                    $firstSectionsContentPath,
                    $StaticContent1Path,
                    $StaticContent2Path,
                    $StaticContent3Path,
                    $StaticContent4Path,
                    $StaticContent5Path,
                    $idWorkPath,
                    $StaticContent6Path,
                    $StaticContent7Path

                ],
                $generatedPath,
                $repOutPutPdfFile,
                $options,
                $this->pageIndex,
                false
            );
            $arrPdfs = array_merge($arrPdfs, $firstPdf);
        }

        // Merge all PDFs into a single file.
        $this->mergerPdf($arrPdfs, $repOutPutPdfFile, ['isPaging' => false]);

        $whereCodes = [];               // The codes which have been used in the filter
        $whereTexts = [];               // Plain text about filtering applied
        $orCodes = [];                  // csv string of OR codes
        $bFilterDetail = false;

        $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
        $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $this->filterText);

        return true;
    }

    private function generateStaticPages($logoPath, $condition, $generatedPath, $page)
    {
        $content = \View::make(
            "reports.wkhtmltopdf.condition.cndCliMK01.$page",
            [
                'condition' => $condition,
                'logoPath'  => $logoPath
            ]
        )->render();
        $contentPath = $generatedPath . "/{$page}_{$condition->getKey()}.html"; //"/content.html";
        file_put_contents($contentPath, $content, LOCK_EX);
        $secondSectionsContentPdf = $generatedPath . "/{$page}_{$condition->getKey()}.pdf";

         return $contentPath;
    }

    private function generateIWPages($logoPath, $condition, $idWorks, $generatedPath)
    {
        $content = \View::make(
            "reports.wkhtmltopdf.condition.cndCliMK01.id_work",
            [
                'condition' => $condition,
                'logoPath'  => $logoPath,
                'idWorks'   => $idWorks,
                'idPhotos'  => $this->getIWPhotos($idWorks, \Auth::User()->site_group_id),
                'pageSeed'  => 6
            ]
        )->render();
        $contentPath = $generatedPath . "/idWork_{$condition->getKey()}.html";
        file_put_contents($contentPath, $content, LOCK_EX);

        return $contentPath;
    }

    private function getCondSurveyData($inputs, $siteGroupId)
    {
        $query = Condsurvey::select(
            [
                'condsurvey.condsurvey_id',
                'condsurvey.condsurvey_code',
                'condsurvey.condsurvey_desc',
                'condsurvey.survey_date',
                'condsurvey.site_id',
                'condsurvey.condsurvey_building_id',
                'condsurvey.score_comment',
                'condsurvey.gen_summary',
                'condsurvey.mech_summary',
                'site.site_code',
                'site.site_desc',
                \DB::raw('site.photo AS building_photo_name'),
                \DB::raw('contact.contact_name AS contact_name')
            ]
        )
            ->leftjoin(
                'site',
                'site.site_id',
                '=',
                'condsurvey.site_id'
            )

            ->leftjoin(
                'address',
                'address.address_id',
                '=',
                'site.address_id'
            )
            ->leftjoin(
                'address_street',
                'address_street.address_street_id',
                '=',
                'address.address_street_id'
            )
            ->leftjoin(
                'gen_gis',
                'gen_gis.gen_gis_id',
                '=',
                'site.gen_gis_id'
            )
            ->leftjoin(
                'score',
                'score.score_id',
                '=',
                'condsurvey.score_id'
            )
            ->leftjoin(
                'condsurvey_status',
                'condsurvey_status.condsurvey_status_id',
                '=',
                'condsurvey.condsurvey_status_id'
            )
            ->leftjoin(
                'contact',
                'contact.contact_id',
                '=',
                'condsurvey.surveyor'
            )
            ->where('condsurvey.site_group_id', '=', $siteGroupId);

        //apply filters
        if (array_key_exists('code', $inputs)) {
            if (($inputs['code'] != "") && ($inputs['code'] != " ")) {
                $query->where('condsurvey.condsurvey_code', '=', $inputs['code']);
            }
        }

        if (array_key_exists('singleOnly', $inputs)) {
            $query->where('condsurvey.condsurvey_id', '=', $inputs['id']);
        } else {
            //check other inputs passed in by filter
            if ((isset($inputs['description'])) && ($inputs['description'] != "") && ($inputs['description'] != " ")) {
                $query->where('condsurvey.condsurvey_desc', '=', $inputs['description']);
            }

            if ((isset($inputs['site_id'])) && $inputs['site_id'] != "" && ($inputs['site_id'] != " ")) {
                $query->where('condsurvey.site_id', '=', $inputs['site_id']);
                //check all levels
                if (($inputs['building_id'] != "") && ($inputs['building_id'] != " ")) {
                    $query->where('condsurvey.condsurvey_building_id', '=', $inputs['building_id']);

                    if (($inputs['room_id'] != "") && ($inputs['room_id'] != " ")) {
                        $query->where('condsurvey.room_id', '=', $inputs['room_id']);
                    }
                }

                if (($inputs['building_id'] != "") && ($inputs['building_id'] != " ")) {
                    $query->where('condsurvey.condsurvey_building_block_id', '=', $inputs['block_id']);
                }
            } //end site id check

            if (($inputs['surveyor'] != "") && ($inputs['surveyor'] != " ")) {
                $query->where("condsurvey.surveyor", $inputs['surveyor']);
            }

            $status = array();
            foreach ((new CondsurveyStatus())->getStatusList() as $key => $value) {
                if (isset($inputs[$value])) {
                    $status[] = $key;
                }
            }
            if (count($status)) {
                $query->whereIn("condsurvey.condsurvey_status_id", $status);
            }
        }
        $query->orderBy('condsurvey.condsurvey_code');
        return $query;
    }

    private function getIWPhotos($idWorks, $siteGroupId)
    {
        $arrIWPhoto = [];

        foreach ($idWorks as $idWork) {
            if ($idWork->photo) {
                $photoPath = $this->getPhotoStoragePathway(
                    'idwork',
                    $idWork->idwork_id,
                    $siteGroupId,
                    $idWork->photo
                );

                //pathway checking in wrong folder, redirect
                $splitString = explode("idwork/", $photoPath);

                $firstHalf = $splitString[0];
                $photoPath = $firstHalf . "$siteGroupId/idwork/" . $idWork->idwork_id . "/" . $idWork->photo;

                $arrIWPhoto["$idWork->idwork_code"] = $photoPath;
            }
        }
        return $arrIWPhoto;
    }

    private function getIdWork($conditionSurveyId)
    {
        $idwork = Idwork::userSiteGroup()
                  ->leftJoin(
                      'building',
                      'building.building_id',
                      '=',
                      'idwork.building_id'
                  )
                ->leftjoin(
                    'idwork_element',
                    'idwork_element.idwork_element_id',
                    '=',
                    'idwork.element_id'
                )
                ->leftjoin(
                    'idwork_subelement',
                    'idwork_subelement.idwork_subelement_id',
                    '=',
                    'idwork.sub_element_id'
                )
                ->leftjoin(
                    'idwork_item',
                    'idwork_item.idwork_item_id',
                    '=',
                    'idwork.item_id'
                )
                ->leftjoin(
                    'idwork_condition',
                    'idwork_condition.idwork_condition_id',
                    '=',
                    'idwork.idwork_condition_id'
                )
                ->leftjoin(
                    'idwork_priority',
                    'idwork_priority.idwork_priority_id',
                    '=',
                    'idwork.idwork_priority_id'
                )
                ->leftJoin(
                    'room',
                    'room.room_id',
                    '=',
                    'idwork.room_id'
                )
                ->where('condsurvey_id', $conditionSurveyId)

                ->select(['idwork.building_id',
                    'idwork.idwork_id',
                    'idwork.idwork_code',
                    'idwork_element.idwork_element_code',
                    'idwork_element.idwork_element_desc',
                    'idwork_subelement.idwork_subelement_code',
                    'idwork_subelement.idwork_subelement_desc',
                    'idwork_item.idwork_item_code',
                    'idwork_item.idworkitem_desc',
                    'idwork_condition.idwork_condition_code',
                    'idwork_priority.idwork_priority_code',
                    'idwork.total_cost',
                    'idwork.defect',
                    'idwork.location',
                    'idwork.remedy',
                    'idwork.photo',
                    'room.room_number',
                    'room.room_desc',
                    'idwork.condsurvey_id',
                    'idwork.location'
                    ])
                ->orderBy('idwork_element.idwork_element_code')
                ->orderBy('idwork_subelement.idwork_subelement_code')
                ->orderBy('idwork_item.idwork_item_code')
                ->orderBy('idwork_priority.idwork_priority_code', 'DESC');

        return $idwork->get();
    }

    private function generateFilterData($filterData, &$whereCodes, &$whereTexts, &$orCodes)
    {
        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Condition Survey Code = '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Condition Survey Description = '" . $val . "'");
        }

        if ($val = Common::iset($filterData['superseded'])) {
            array_push($whereTexts, "Superseded = '" . $val . "'");
        }

        if ($siteId = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $siteId, "Site Code"]);

            if ($buildingId = Common::iset($filterData['building_id'])) {
                array_push(
                    $whereCodes,
                    ['building', 'building_id', 'building_code', $buildingId, "Building Code"]
                );

                if ($val = Common::iset($filterData['block_id'])) {
                    array_push($whereCodes, [
                        'building_block',
                        'building_block_id',
                        'building_block_code',
                        $val,
                        "Building Block Code"
                    ]);
                }

                if ($val = Common::iset($filterData['room_id'])) {
                    array_push($whereCodes, ['room', 'room_id', 'room_number', $val, "Room Number"]);
                }
            }
        }
    }
}
