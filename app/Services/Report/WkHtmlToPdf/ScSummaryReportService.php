<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use Tfcloud\Lib\Common;
use Tfcloud\Models\Estate\EstScServiceCharge;
use Tfcloud\Services\Estate\ScServiceCharge\ScSummaryService;
use Tfcloud\Services\PermissionService;

class ScSummaryReportService extends WkHtmlToPdfReportBaseService
{
    public $filterText = '';

    public function runReport($scServiceChargeId, $repId)
    {
        $serviceCharge = $this->getServiceCharge($scServiceChargeId);

        $scSummaryService = new ScSummaryService();
        $budgetSummary = $scSummaryService->getBudgetSummary($serviceCharge);
        $budgetDetail = $scSummaryService->getBudgetDetail($serviceCharge);
        $expenditureSummary = $scSummaryService->getExpenditureSummary($serviceCharge);
        $expenditureDetail = $scSummaryService->getExpenditureDetail($serviceCharge);

        // Create wkhtmltopdf temp folder to store generated files
        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);

        $header = \View::make('reports.wkhtmltopdf.estates.es67.header', [
            'logo' => $this->getHeaderLogo()
        ])->render();
        $headerPath = $generatedPath . "/header.html";
        file_put_contents($headerPath, $header, LOCK_EX);

        $footer = \View::make('reports.wkhtmltopdf.estates.es67.footer')->render();
        $footerPath = $generatedPath . "/footer.html";
        file_put_contents($footerPath, $footer, LOCK_EX);

        $content = \View::make(
            'reports.wkhtmltopdf.estates.es67.content',
            [
                'serviceCharge' => $serviceCharge,
                'budgetSummary' => $budgetSummary,
                'budgetDetail' => $budgetDetail,
                'expenditureSummary' => $expenditureSummary,
                'expenditureDetail' => $expenditureDetail,
            ]
        )->render();
        $contentPath = $generatedPath . "/content.html";
        file_put_contents($contentPath, $content, LOCK_EX);

        $formatOptions = [
            'header-html' => $headerPath,
            'footer-html' => $footerPath,
            'header-spacing' => 4,
            'footer-spacing' => 4,
            'orientation' => 'portrait',
        ];

        $repOutPutPdfFile = $this->getTempFile();

        $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $formatOptions);

        return $repOutPutPdfFile;
    }

    private function getServiceCharge($estScServiceChargeId)
    {
        return EstScServiceCharge::userSiteGroup()
            ->join(
                'est_sc_service_charge_status',
                'est_sc_service_charge_status.est_sc_service_charge_status_id',
                '=',
                'est_sc_service_charge.est_sc_service_charge_status_id'
            )
            ->join(
                'est_sc_category_profile',
                'est_sc_category_profile.est_sc_category_profile_id',
                '=',
                'est_sc_service_charge.est_sc_category_profile_id'
            )
            ->join('user', 'user.id', '=', 'est_sc_service_charge.owner_user_id')
            ->leftJoin('establishment', 'establishment.establishment_id', '=', 'est_sc_service_charge.establishment_id')
            ->leftJoin('site', 'site.site_id', '=', 'est_sc_service_charge.site_id')
            ->leftJoin('building', 'building.building_id', '=', 'est_sc_service_charge.building_id')
            ->where('est_sc_service_charge.est_sc_service_charge_id', $estScServiceChargeId)
            ->first();
    }

    private function getTempFile()
    {
        return Common::getTempFolder() . DIRECTORY_SEPARATOR . 'ES67_ServiceChargeSummary.pdf';
    }
}
