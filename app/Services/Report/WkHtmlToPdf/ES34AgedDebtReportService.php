<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use Box\Spout\Writer\Common\Creator\Style\StyleBuilder;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Box\Spout\Writer\WriterInterface;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Constant\ReportConstant;
use Tfcloud\Lib\Filters\Estate\EstateSalesInvoiceFilter;
use Tfcloud\Lib\Math;
use Tfcloud\Models\EstSalesInvoiceStatus;
use Tfcloud\Models\Report;
use Tfcloud\Services\PermissionService;
use Tfcloud\Models\Views\VwEs32;
use Tfcloud\Services\Estate\FeeReceiptService;
use Tfcloud\Services\Estate\EstateSalesInvoiceService;
use Box\Spout\Common\Entity\Style\CellAlignment;

class ES34AgedDebtReportService extends WkHtmlToPdfReportBaseService
{
    public $filterText = '';

    private $feeReceiptService;
    private $invoiceService;

    private Report $report;

    private $salesInvoice;
    private $grandTotal;
    private $firstTotal;
    private $secondTotal;
    private $thirdTotal;
    private $fourthTotal;
    private $reportBoxFilterLoader = null;

    public function __construct(PermissionService $permissionService, Report $report = null)
    {
        parent::__construct($permissionService);
        $this->feeReceiptService = new FeeReceiptService($permissionService);
        $this->invoiceService = new EstateSalesInvoiceService($permissionService);
        $this->report = $report;
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function runReport($exportType, $repOutPutFile, $inputs, $repId)
    {
        $this->formatInputData($inputs);
        $inputs['callOrderBy'] = true;

        $query = VwEs32::select();

        $query = $this->filterAll($query, $inputs, 'vw_es32');

        $salesInvoiceData = $query
            ->where('os_amount', '>', '0')
            ->whereNotIn(
                'est_sales_invoice_status_id',
                [
                    CommonConstant::EST_SALE_INVOICE_STATUS_CANCELED
                ]
            )
            ->join(
                'contact',
                'contact.contact_id',
                '=',
                'vw_es32.customer_contact_id'
            )
            ->groupBy('customer_contact_id')
            ->get();


        $today = date('Y-m-d');
        //get invoices in the last thirty days
        $thirtyDaysAgo = date('Y-m-d', strtotime('-30 days'));

        $zeroToThirtyData = $this->filterAll(VwEs32::select(), $inputs, 'vw_es32')
            ->join(
                'contact',
                'contact.contact_id',
                '=',
                'vw_es32.customer_contact_id'
            )
            ->where('os_amount', '>', '0')
            ->whereNotIn(
                'est_sales_invoice_status_id',
                [
                    CommonConstant::EST_SALE_INVOICE_STATUS_CANCELED
                ]
            )
            ->whereBetween('tax_date', [
                date($thirtyDaysAgo),
                date($today)
            ])
            ->get();

        $clientTotal = 0;
        $grandTotal = 0;
        $firstTotal = 0;

        foreach ($salesInvoiceData as $tenant) {
            $clientTotal = 0;
            foreach ($zeroToThirtyData as $firstMonth) {
                if ($firstMonth->customer_contact_id == $tenant->customer_contact_id) {
                    $clientTotal = Math::addCurrency([$clientTotal,$firstMonth->os_amount]);
                    $tenant['oneMonthTotal'] = $clientTotal;
                    $grandTotal = Math::addCurrency([$grandTotal, $firstMonth->os_amount]);
                    $firstTotal = Math::addCurrency([$firstTotal, $firstMonth->os_amount]);
                }
            }
        }

        //get invoices in the last thirty-one to sixty days
        $thirtyOneDaysAgo = date('Y-m-d', strtotime('-31 days'));
        $sixtyDaysAgo = date('Y-m-d', strtotime('-60 days'));

        $thirtyOneToSixtyDaysData = $this->filterAll(VwEs32::select(), $inputs, 'vw_es32')
            ->where('os_amount', '>', '0')
            ->whereNotIn(
                'est_sales_invoice_status_id',
                [
                    CommonConstant::EST_SALE_INVOICE_STATUS_CANCELED
                ]
            )
            ->join(
                'contact',
                'contact.contact_id',
                '=',
                'vw_es32.customer_contact_id'
            )
            ->whereBetween('tax_date', [
                date($sixtyDaysAgo),
                date($thirtyOneDaysAgo)
            ])
            ->get();

        $secondClientTotal = 0;
        $secondTotal = 0;

        foreach ($salesInvoiceData as $tenant) {
            $secondClientTotal = 0;
            foreach ($thirtyOneToSixtyDaysData as $secondPeriod) {
                if ($secondPeriod->customer_contact_id == $tenant->customer_contact_id) {
                    $secondClientTotal = Math::addCurrency([$secondClientTotal,$secondPeriod->os_amount]);
                    $tenant['secondMonthTotal'] = $secondClientTotal;
                    $grandTotal = Math::addCurrency([$grandTotal, $secondPeriod->os_amount]);
                    $secondTotal = Math::addCurrency([$secondTotal, $secondPeriod->os_amount]);
                }
            }
        }

        //get invoices in the last thirty-one to sixty days
        $sixtyOneDaysAgo = date('Y-m-d', strtotime('-61 days'));
        $ninetyDaysAgo = date('Y-m-d', strtotime('-90 days'));

        $sixtyOneToNinetyDaysData = $this->filterAll(VwEs32::select(), $inputs, 'vw_es32')
            ->join(
                'contact',
                'contact.contact_id',
                '=',
                'vw_es32.customer_contact_id'
            )
            ->where('os_amount', '>', '0')
            ->whereNotIn(
                'est_sales_invoice_status_id',
                [
                    CommonConstant::EST_SALE_INVOICE_STATUS_CANCELED
                ]
            )
            ->whereBetween('tax_date', [
                date($ninetyDaysAgo),
                date($sixtyOneDaysAgo)
            ])
            ->get();

        $thirdClientTotal = 0;
        $thirdTotal = 0;

        foreach ($salesInvoiceData as $tenant) {
            $thirdClientTotal = 0;
            foreach ($sixtyOneToNinetyDaysData as $thirdPeriod) {
                if ($thirdPeriod->customer_contact_id == $tenant->customer_contact_id) {
                    $thirdClientTotal = Math::addCurrency([$thirdClientTotal,$thirdPeriod->os_amount]);
                    $tenant['thirdMonthTotal'] = $thirdClientTotal;
                    $grandTotal = Math::addCurrency([$grandTotal, $thirdPeriod->os_amount]);
                    $thirdTotal = Math::addCurrency([$thirdTotal, $thirdPeriod->os_amount]);
                }
            }
        }

        //get invoices in the last thirty-one to sixty days
        $ninetyOneDaysAgo = date('Y-m-d', strtotime('-91 days'));
        $last50Months = date('Y-m-d', strtotime('-50 months'));

        $ninetyDaysPlusData = $this->filterAll(VwEs32::select(), $inputs, 'vw_es32')
            ->join(
                'contact',
                'contact.contact_id',
                '=',
                'vw_es32.customer_contact_id'
            )
            ->where('os_amount', '>', '0')
            ->whereNotIn(
                'est_sales_invoice_status_id',
                [
                    CommonConstant::EST_SALE_INVOICE_STATUS_CANCELED
                ]
            )
            ->whereBetween('tax_date', [
                date($last50Months),
                date($ninetyOneDaysAgo)
            ])
            ->get();

        $fourthClientTotal = 0;
        $fourthTotal = 0;

        foreach ($salesInvoiceData as $tenant) {
            $fourthClientTotal = 0;
            foreach ($ninetyDaysPlusData as $fourthPeriod) {
                if ($fourthPeriod->customer_contact_id == $tenant->customer_contact_id) {
                    $fourthClientTotal = Math::addCurrency([$fourthClientTotal,$fourthPeriod->os_amount]);
                    $tenant['fourthMonthTotal'] = $fourthClientTotal;
                    $grandTotal = Math::addCurrency([$grandTotal, $fourthPeriod->os_amount]);
                    $fourthTotal = Math::addCurrency([$fourthTotal, $fourthPeriod->os_amount]);
                }
            }
        }

        //work out individual client totals
        foreach ($salesInvoiceData as $tenant) {
            if (!$tenant['oneMonthTotal']) {
                $tenant['oneMonthTotal'] = 0;
            }
            if (!$tenant['secondMonthTotal']) {
                $tenant['secondMonthTotal'] = 0;
            }
            if (!$tenant['thirdMonthTotal']) {
                $tenant['thirdMonthTotal'] = 0;
            }

            if (!$tenant['fourthMonthTotal']) {
                $tenant['fourthMonthTotal'] = 0;
            }

            $tenant['lineTotal'] = Math::addCurrency(
                [$tenant['oneMonthTotal'],
                    $tenant['secondMonthTotal'],
                    $tenant['thirdMonthTotal'],
                    $tenant['fourthMonthTotal']]
            );
        }

        $this->salesInvoice = $salesInvoiceData;
        $this->grandTotal = $grandTotal;
        $this->firstTotal = $firstTotal;
        $this->secondTotal = $secondTotal;
        $this->thirdTotal = $thirdTotal;
        $this->fourthTotal = $fourthTotal;

        //Filter detail
        $whereCodes = [];               // The codes which have been used in the filter
        $whereTexts = [];               // Plain text about filtering applied
        $orCodes = [];                  // csv string of OR codes
        $bFilterDetail = false;

        $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
        $this->filterText = $reportFilterQuery->getFilterDetailText();

        //Gen file
        $this->genReport($exportType, $repOutPutFile, $inputs, $repId);

        return true;
    }

    public function filterAll($query, $inputs, $view = false)
    {
        $table = !$view ? 'est_sales_invoice' : $view;

        $filter = new EstateSalesInvoiceFilter($inputs);

        if (!is_null($filter->invoiceNo)) {
            $query->where($table . '.est_sales_invoice_code', 'like', '%' . trim($filter->invoiceNo) . '%');
        }
        if (!is_null($filter->contact)) {
            $query->where($table . '.customer_contact_id', $filter->contact);
        }
        if (!is_null($filter->leaseOutCode)) {
            $query->where($table . '.lease_out_code', 'like', '%' . trim($filter->leaseOutCode) . '%');
        }

        if (!is_null($filter->lease_out_id)) {
            $query->where('est_sales_invoice.lease_out_id', $filter->lease_out_id);
        }

        if (
            is_null($filter->generated) && is_null($filter->printed) && is_null($filter->posted) &&
            is_null($filter->paid)      && is_null($filter->cancelled)
        ) {
            //
        } else {
            // One or more status types selected.  Only show records against the selected status types.
            $query->where(function ($query) use ($filter, $table) {
                if (!is_null($filter->generated)) {
                    $query->orWhere(
                        $table . '.est_sales_invoice_status_id',
                        EstSalesInvoiceStatus::GENERATED
                    );
                }

                if (!is_null($filter->printed)) {
                    $query->orWhere(
                        $table . '.est_sales_invoice_status_id',
                        EstSalesInvoiceStatus::PRINTED
                    );
                }

                if (!is_null($filter->posted)) {
                    $query->orWhere(
                        $table . '.est_sales_invoice_status_id',
                        EstSalesInvoiceStatus::POSTED
                    );
                }
            });
        }

        // only filter for fee receipt
        if ($filter->feeReceiptId) {
            $query->where('est_fee_receipt_invoice.est_fee_receipt_id', $filter->feeReceiptId);
        }

        if (!is_null($filter->taxDateFrom)) {
            $taxDate = \DateTime::createFromFormat('d/m/Y', $filter->taxDateFrom);
            if ($taxDate) {
                $query->where(
                    \DB::raw("DATE_FORMAT({$table}.tax_date , '%Y-%m-%d')"),
                    '>=',
                    $taxDate->format('Y-m-d')
                );
            }
        }

        if (!is_null($filter->taxDateTo)) {
            $taxDateTo = \DateTime::createFromFormat('d/m/Y', $filter->taxDateTo);
            if ($taxDateTo) {
                $query->where(
                    \DB::raw("DATE_FORMAT({$table}.tax_date, '%Y-%m-%d')"),
                    '<=',
                    $taxDateTo->format('Y-m-d')
                );
            }
        }

        if ($view) {
            if (!is_null($filter->landlord)) {
                $query->where('landlord_contact_id', $filter->landlord);
            }
            return $query;
        }
        return $query->orderBy($filter->sort, $filter->sortOrder);
    }

    private function formatInputData(&$inputs)
    {
        foreach ($inputs as $key => $value) {
            if ($value === "") {
                unset($inputs[$key]);
            }
        }

        $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
        $inputs['invoiceNo'] = $reportFilterQuery->getFirstValueFilterField('est_sales_invoice_code');
        $inputs['leaseOutCode'] = $reportFilterQuery->getFirstValueFilterField('lease_out_code');
        $inputs['contact'] = $reportFilterQuery->getFirstValueFilterField('customer_contact_id');
        $inputs['landlord'] = $reportFilterQuery->getFirstValueFilterField('landlord_contact_id');
        $inputs['taxDateFrom'] = Common::dateFieldDisplayFormat(
            array_first($reportFilterQuery->getFirstValueFilterField('tax_date'))
        );
        $inputs['taxDateTo'] = Common::dateFieldDisplayFormat(
            array_last($reportFilterQuery->getFirstValueFilterField('tax_date'))
        );
        $inputs['est_sales_invoice_status_id'] = $reportFilterQuery->getValueFilterField('est_sales_invoice_status_id');
        if ($inputs['est_sales_invoice_status_id']) {
            $inputs['generated'] = (in_array(
                EstSalesInvoiceStatus::GENERATED,
                $inputs['est_sales_invoice_status_id']
            )) ? 1 : null;
            $inputs['printed'] = (in_array(EstSalesInvoiceStatus::PRINTED, $inputs['est_sales_invoice_status_id'])) ?
                2 : null;
            $inputs['posted'] = (in_array(EstSalesInvoiceStatus::POSTED, $inputs['est_sales_invoice_status_id'])) ?
                3 : null;
        }
    }

    private function genReport($reportType, $repOutPutFile, $inputs, $repId)
    {
        switch ($reportType) {
            case ReportConstant::REPORT_OUTPUT_XLSX_FORMAT:
                $this->genExcel($repOutPutFile, $inputs, $repId);
                break;
            case ReportConstant::REPORT_OUTPUT_CSV_FORMAT:
                $this->genCSV($repOutPutFile, $inputs, $repId);
                break;
            default:
                $this->genPdf($repOutPutFile, $repId);
                break;
        }
    }

    private function formatNumber($number)
    {
        if ($number) {
            return $number * 1;
        }
        return 0;
    }

    private function genExcel($repOutPutFile, $inputs, $repId)
    {
        $writer = WriterEntityFactory::createXLSXWriter();
        $this->genBySpout($writer, $repOutPutFile);
    }

    private function genCSV($repOutPutFile, $inputs, $repId)
    {
        $writer = WriterEntityFactory::createCSVWriter();
        $this->genBySpout($writer, $repOutPutFile);
    }

    private function genBySpout(WriterInterface $writer, $repOutPutFile)
    {
        $writer->openToFile($repOutPutFile);

        $headerNumberStyle = (new StyleBuilder())
            ->setFontBold()
            ->setCellAlignment(CellAlignment::RIGHT)
            ->build();

        $headerStringStyle = (new StyleBuilder())
            ->setFontBold()
            ->build();

        $numberStyle = (new StyleBuilder())
            ->setCellAlignment(CellAlignment::RIGHT)
            ->build();

        //header
        $writer->addRow(
            WriterEntityFactory::createRow([
                WriterEntityFactory::createCell('Account ID', $headerStringStyle),
                WriterEntityFactory::createCell('Name', $headerStringStyle),
                WriterEntityFactory::createCell('0 - 30 Days', $headerNumberStyle),
                WriterEntityFactory::createCell('31 - 60 Days', $headerNumberStyle),
                WriterEntityFactory::createCell('61 - 90 Days', $headerNumberStyle),
                WriterEntityFactory::createCell('90+ Days', $headerNumberStyle),
                WriterEntityFactory::createCell('Total', $headerNumberStyle),
            ])
        );

        //body
        foreach ($this->salesInvoice as $invoice) {
            $writer->addRow(
                WriterEntityFactory::createRow([
                    WriterEntityFactory::createCell($invoice->sales_acount_ref),
                    WriterEntityFactory::createCell("{$invoice->contact_name}, {$invoice->organisation}"),
                    WriterEntityFactory::createCell($this->formatNumber($invoice->oneMonthTotal), $numberStyle),
                    WriterEntityFactory::createCell($this->formatNumber($invoice->secondMonthTotal), $numberStyle),
                    WriterEntityFactory::createCell($this->formatNumber($invoice->thirdMonthTotal), $numberStyle),
                    WriterEntityFactory::createCell($this->formatNumber($invoice->fourthMonthTotal), $numberStyle),
                    WriterEntityFactory::createCell($this->formatNumber($invoice->lineTotal), $numberStyle),
                ])
            );
        }

        //Grand Total
        $writer->addRow(
            WriterEntityFactory::createRow([
                WriterEntityFactory::createCell(null),
                WriterEntityFactory::createCell('Grand Total'),
                WriterEntityFactory::createCell($this->formatNumber($this->firstTotal), $numberStyle),
                WriterEntityFactory::createCell($this->formatNumber($this->secondTotal), $numberStyle),
                WriterEntityFactory::createCell($this->formatNumber($this->thirdTotal), $numberStyle),
                WriterEntityFactory::createCell($this->formatNumber($this->fourthTotal), $numberStyle),
                WriterEntityFactory::createCell($this->formatNumber($this->grandTotal), $numberStyle),
            ])
        );

        //filter detail
        $writer->addRows([
            WriterEntityFactory::createRowFromArray([]),
            WriterEntityFactory::createRowFromArray(['Report: ' . $this->report->getRepTitle()]),
        ]);

        if ($this->filterText) {
            $writer->addRows([
                WriterEntityFactory::createRowFromArray(['Filtering details:']),
                WriterEntityFactory::createRowFromArray([$this->filterText]),
            ]);
        }

        $writer->close();
    }

    private function genPdf($repOutPutPdfFile, $repId)
    {
        // Create wkhtmltopdf temp folder to store generated files
        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);

        $header = \View::make('reports.wkhtmltopdf.estates.es34.header')->render();
        $headerPath = $generatedPath . "/header.html";
        file_put_contents($headerPath, $header, LOCK_EX);

        $footer = \View::make('reports.wkhtmltopdf.estates.es34.footer')->render();
        $footerPath = $generatedPath . "/footer.html";
        file_put_contents($footerPath, $footer, LOCK_EX);

        $content = \View::make(
            'reports.wkhtmltopdf.estates.es34.content',
            [
                'salesInvoice' => $this->salesInvoice,
                'grandTotal'   => $this->grandTotal,
                'firstTotal'   => $this->firstTotal,
                'secondTotal'  => $this->secondTotal,
                'thirdTotal'   => $this->thirdTotal,
                'fourthTotal'  => $this->fourthTotal,
            ]
        )->render();

        $contentPath = $generatedPath . "/content.html";

        file_put_contents($contentPath, $content, LOCK_EX);

        $options = [
            'header-html' => $headerPath,
            'footer-html' => $footerPath,
            'header-spacing' => 4,
            'footer-spacing' => 4,
            'orientation' => 'landscape',
        ];

        $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $options);
    }
}
