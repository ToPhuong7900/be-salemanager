<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Constant\ReportConstant;
use Tfcloud\Lib\Exceptions\PermissionException;
use Tfcloud\Lib\Filters\CaseManagement\CaseRecordFilter;
use Tfcloud\Lib\Traits\EmailTrait;
use Tfcloud\Models\CaseManagement\CmCase;
use Tfcloud\Models\CaseManagement\CmCaseStatusType;
use Tfcloud\Models\CaseManagement\CmType;
use Tfcloud\Models\CrudAccessLevel;
use Tfcloud\Models\GenTable;
use Tfcloud\Models\GenUserdefLabel;
use Tfcloud\Models\Module;
use Tfcloud\Models\Report;
use Tfcloud\Models\UserSiteAccess;
use Tfcloud\Services\Admin\General\SiteGroupsService;
use Tfcloud\Services\CaseManagement\CaseMgmt\CaseRecordService;
use Tfcloud\Services\PermissionService;

abstract class BaseCMx03CaseProgressService extends WkHtmlToPdfReportBaseService
{
    use EmailTrait;

    public $filterText = '';
    protected $permissionService;
    private $generatedPath;
    private $siteGroupsService = null;
    private $reportBoxFilterLoader = null;
    protected $cmType;
    protected $gentTable;

    public function __construct(PermissionService $permissionService, Report $report = null)
    {
        parent::__construct($permissionService);
        $this->permissionService = $permissionService;
        $this->siteGroupsService = new SiteGroupsService($this->permissionService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function runReport($repOutPutPdfFile, $inputs, $repId, $options = [])
    {
        $inputs = array_filter($inputs);
        $inputs = $this->formatInputs($inputs);
        $cases = $this->getAll($inputs)->get();
        $this->generatedPath = $this->generateWkhtmltopdfTempPath($repId);
        $photoPath = $this->siteGroupReportLogoPath();
        $label = GenUserdefLabel::userSiteGroup()->where('gen_table_id', $this->gentTable)->first();
        $hidePrivateNotes = array_get($inputs, 'hide_private_notes', null);

        $footer = \View::make('reports.wkhtmltopdf.caseManagement.CM03.footer')->render();
        $footerPath = $this->generatedPath . "/footer.html";
        file_put_contents($footerPath, $footer, LOCK_EX);

        $isPrintReport = array_get($options, 'print-report', false);
        if ($isPrintReport && !$repOutPutPdfFile) {
            $caseCode = array_get($options, 'cm_case_code', 'no_code_found');
            $reportFileName = ReportConstant::SYSTEM_REPORT_CM03 . '_' . $caseCode;
            $repOutPutPdfFile = $this->generateReportOutPutPath($reportFileName);
        }

        $pageIndex = 1;
        $listCasePdf = [];
        foreach ($cases as $case) {
            $header = \View::make('reports.wkhtmltopdf.caseManagement.CM03.header', [
                'caseTitle' => Common::concatFields([$case->cm_case_code, $case->cm_case_desc]),
                'photoPath' => $photoPath
            ])->render();
            $headerPath = $this->generatedPath . "/header" . $pageIndex . ".html";
            file_put_contents($headerPath, $header, LOCK_EX);

            $content = \View::make('reports.wkhtmltopdf.caseManagement.CM03.content', [
                'case' => $case,
                'label' => $label,
                'hidePrivateNotes' => $hidePrivateNotes
            ])->render();
            $contentPath = $this->generatedPath . "/content" . $pageIndex . ".html";
            file_put_contents($contentPath, $content, LOCK_EX);

            $options = [
                'header-html' => $header,
                'footer-html' => $footer,
                'header-spacing' => 4,
                'footer-spacing' => 4,
            ];

            $casePdf = $this->generateMultiHtmlToPdf(
                [$contentPath],
                $this->generatedPath,
                $repOutPutPdfFile,
                $options,
                $pageIndex,
                false
            );
            $listCasePdf = array_merge($listCasePdf, $casePdf);
        }

        $cssPaging = [
            'fontSize' => 8,
            'x' => 520,
            'y' => 14
        ];

        $this->mergerPdf($listCasePdf, $repOutPutPdfFile, ['cssPaging' => $cssPaging]);

        $whereCodes = [];               // The codes which have been used in the filter
        $whereTexts = [];               // Plain text about filtering applied
        $orCodes = [];                  // csv string of OR codes
        $bFilterDetail = false;

        if ($this->isReportBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $this->filterText = $reportFilterQuery->getFilterDetailText();
        } else {
            $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
            $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $this->filterText);
        }

        if (array_get($inputs, 'send_email', false)) {
            $caseId = array_get($inputs, 'caseId', false);
            $logItem = CmCase::find($caseId);
            $user = \Auth::user();
            switch ($this->cmType) {
                case CmType::CM_TYPE_CASEMGMT:
                    $fileName = ReportConstant::SYSTEM_REPORT_CM03;
                    break;
                case CmType::CM_TYPE_CASEMGMT2:
                    $fileName = ReportConstant::SYSTEM_REPORT_CM2_03;
                    break;
                case CmType::CM_TYPE_CASEMGMT3:
                    $fileName = ReportConstant::SYSTEM_REPORT_CM3_03;
                    break;
                case CmType::CM_TYPE_CASEMGMT4:
                    $fileName = ReportConstant::SYSTEM_REPORT_CM4_03;
                    break;
                case CmType::CM_TYPE_CASEMGMT5:
                    $fileName = ReportConstant::SYSTEM_REPORT_CM5_03;
                    break;
            }
            $emailData = array(
                'mailTo' => $user->email_reply_to,
                'displayName' => $user->siteGroup->email_return_address,
                'repTitle' => $fileName,
                'subject' => $fileName . ' Report',
            );
            return $this->sendEmail($repOutPutPdfFile, $emailData, $logItem);
        }

        if ($isPrintReport) {
            return $repOutPutPdfFile;
        }

        return true;
    }

    private function generateFilterData($filterData, &$whereCodes, &$whereTexts, &$orCodes)
    {
        if ($val = array_get($filterData, 'code')) {
            array_push($whereTexts, "Code contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'description')) {
            array_push($whereTexts, "Description contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['owner'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Owner']);
        }

        if ($val = Common::iset($filterData['contact'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Contact']);
        }
        if ($val = array_get($filterData, 'plant_id', null)) {
            array_push($whereCodes, ['plant', 'plant_id', 'plant_code', $val, 'Plant']);
        }

        if ($val = array_get($filterData, 'type', null)) {
            array_push($whereCodes, [
                'cm_case_type',
                'cm_case_type_id',
                'cm_case_type_code',
                $val,
                'Type'
            ]);
        }

        if ($val = array_get($filterData, 'team', null)) {
            array_push($whereCodes, [
                'cm_team',
                'cm_team_id',
                'cm_team_code',
                $val,
                'Team'
            ]);
        }

        if ($val = array_get($filterData, 'priority', null)) {
            array_push($whereCodes, [
                'cm_priority',
                'cm_priority_id',
                'cm_priority_code',
                $val,
                'Priority'
            ]);
        }

        $statusType = [];
        if (Common::iset($filterData['status_draft'])) {
            $statusType[] = CmCaseStatusType::DRAFT;
        }
        if (Common::iset($filterData['status_open'])) {
            $statusType[] = CmCaseStatusType::OPEN;
        }
        if (Common::iset($filterData['status_complete'])) {
            $statusType[] = CmCaseStatusType::COMPLETE;
        }
        if (count($statusType)) {
            array_push(
                $orCodes,
                [
                    'cm_case_status',
                    'cm_case_status_id',
                    'cm_case_status_code',
                    implode(',', $statusType),
                    "Status"
                ]
            );
        }
        if ($val = array_get($filterData, 'nextReviewFrom', null)) {
            array_push($whereTexts, "Next Review From '" . " $val" . "'");
        }

        if ($val = array_get($filterData, 'nextReviewTo', null)) {
            array_push($whereTexts, "Next Review To '" . " $val" . "'");
        }

        if ($val = array_get($filterData, 'hide_private_notes', null)) {
            $text = '';
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_NO:
                    $text = \Lang::get('text.no');
                    break;
                case CommonConstant::DATABASE_VALUE_YES:
                    $text = \Lang::get('text.yes');
                    break;
                default:
                    break;
            }
            if (!empty($text)) {
                array_push($whereTexts, "Hide Private Notes = '" . $text . "'");
            }
        }
    }

    public function siteGroupReportLogoPath()
    {
        $siteGroupId = \Auth::user()->site_group_id;
        $siteGroupImagePath = $this->siteGroupsService->get($siteGroupId, false)
            ->siteGroupReportLogoPath();

        if (strpos($siteGroupImagePath, CommonConstant::REPORT_LOGO_FILE) === false) {
            $siteGroupImagePath = false;
        }

        return $siteGroupImagePath;
    }

    public function getAll($inputs)
    {
        if (\Auth::User()->isContractor() || \Auth::User()->isSelfServiceUser()) {
            $msg = "Case Progress Report not available to contactor or self service user groups.";
            throw new PermissionException($msg);
        }

        $moduleAccess = $this->getModuleAccess();

        if (!$moduleAccess) {
            $msg = "Not met minimum create access for case management module";
            throw new PermissionException($msg);
        }
        $siteAccess = \Auth::User()->site_access;

        $query = CmCase::UserSiteGroup()
            ->leftJoin('cm_case_status', 'cm_case_status.cm_case_status_id', '=', 'cm_case.cm_case_status_id')
            ->leftJoin('cm_case_type', 'cm_case_type.cm_case_type_id', '=', 'cm_case.cm_case_type_id')
            ->leftJoin('cm_team', 'cm_team.cm_team_id', '=', 'cm_case.cm_team_id')
            ->leftJoin('cm_priority', 'cm_priority.cm_priority_id', '=', 'cm_case.cm_priority_id')
            ->leftJoin('user as user', 'user.id', '=', 'owner_user_id')
            ->leftJoin('site', 'site.site_id', '=', 'cm_case.site_id')
            ->select([
                'cm_case.*',
                'site.site_code',
                'site.site_desc',
                'cm_case_status_code',
                'cm_case_status_desc',
                'cm_case_type_code',
                'cm_case_type_desc',
                'cm_team_code',
                'cm_team_desc',
                'cm_priority_code',
                'cm_priority_desc',
                'user.display_name AS owner',
            ])
            ->with(
                [
                    'cmType',
                    'genUserdefGroup',
                    'genUserdefGroup.contact1',
                    'genUserdefGroup.contact2',
                    'genUserdefGroup.contact3',
                    'genUserdefGroup.contact4',
                    'genUserdefGroup.contact5',
                    'genUserdefGroup.select1',
                    'genUserdefGroup.select2',
                    'genUserdefGroup.select3',
                    'genUserdefGroup.select4',
                    'stages',
                ]
            );

        if ($siteAccess == UserSiteAccess::USER_SITE_ACCESS_SELECTED) {
            $query->whereRaw(
                'IF((`user_access`.`user_id` is null and cm_case.site_id is null) OR
                 (`user_access`.`user_id` is not null and cm_case.site_id is not null), 1, 0)'
            );
        }
        $query->where('cm_case.cm_type_id', $this->cmType);

        $this->permissionService->listViewSiteAccess($query, "site.site_id", $siteAccess, ['showNoneSite' => true]);
        $caseRecordService = new CaseRecordService($this->permissionService);
        $query = $caseRecordService->caseUserAccessQuery($query);

        return $this->filter($query, $inputs);
    }

    public function filter($query, $inputs, $view = false)
    {
        $filter = new CaseRecordFilter($inputs);
        $caseTable = $view ? $view : 'cm_case';

        if (!is_null($filter->code)) {
            $query->where("{$caseTable}.cm_case_code", "LIKE", "%{$filter->code}%");
        }

        if (!is_null($filter->description)) {
            $query->where("{$caseTable}.cm_case_desc", "LIKE", "%{$filter->description}%");
        }

        if (!is_null($filter->type)) {
            $query->where("{$caseTable}.cm_case_type_id", "=", $filter->type);
        }

        if (!is_null($filter->team)) {
            $query->where("{$caseTable}.cm_team_id", "=", $filter->team);
        }

        if (!is_null($filter->priority)) {
            $query->where("{$caseTable}.cm_priority_id", "=", $filter->priority);
        }

        if ($filter->cm_case_status) {
            $query->whereIn("{$caseTable}.cm_case_status_id", $filter->cm_case_status);
        }

        if (!is_null($filter->site_id)) {
            $query->where("{$caseTable}.site_id", "=", $filter->site_id);
        }
        if (!is_null($filter->building_id)) {
            $query->where("{$caseTable}.building_id", "=", $filter->building_id);
        }
        if (!is_null($filter->room_id)) {
            $query->where("{$caseTable}.room_id", "=", $filter->room_id);
        }

        if (!is_null($filter->plant_id)) {
            $query->where("{$caseTable}.plant_id", "=", $filter->plant_id);
        }

        if (!is_null($filter->owner)) {
            $query->where("{$caseTable}.owner_user_id", "=", $filter->owner);
        }

        if (!is_null($filter->contact)) {
            $inputIdArr = explode('_', $filter->contact);
            if ($inputIdArr[0] == 'u') {
                $query->where("{$caseTable}.contact_user_id", "=", $inputIdArr[1]);
            } elseif ($inputIdArr[0] == 'c') {
                $query->where("{$caseTable}.contact_id", "=", $inputIdArr[1]);
            }
        }

        if (!is_null($filter->nextReviewTo)) {
            $nextReviewTo = DateTime::createFromFormat('d/m/Y', $filter->nextReviewTo);
            if ($nextReviewTo) {
                $query->where(
                    \DB::raw("DATE_FORMAT(" . $caseTable . ".next_review_date, '%Y-%m-%d')"),
                    '<=',
                    $nextReviewTo->format('Y-m-d')
                );
            }
        }

        if (!is_null($filter->nextReviewFrom)) {
            $nextReviewFrom = DateTime::createFromFormat('d/m/Y', $filter->nextReviewFrom);
            if ($nextReviewFrom) {
                $query->where(
                    \DB::raw("DATE_FORMAT(" . $caseTable . ".next_review_date, '%Y-%m-%d')"),
                    '>=',
                    $nextReviewFrom->format('Y-m-d')
                );
            }
        }

        return $view ? $query : $query->orderBy($filter->sort, $filter->sortOrder);
    }

    private function isReportBoxFilter()
    {
        return $this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter();
    }

    private function formatInputs($inputs)
    {
        if ($this->isReportBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $cmCaseCode = $reportFilterQuery->getValueFilterField('cm_case_code');
            $cmCaseDesc = $reportFilterQuery->getValueFilterField('cm_case_desc');
            $cmCaseStatusId = $reportFilterQuery->getValueFilterField('cm_case_status_id');
            $cmCaseTypeId = $reportFilterQuery->getValueFilterField('cm_case_type_id');
            $cmPriorityId = $reportFilterQuery->getValueFilterField('cm_priority_id');
            $cmTeamId = $reportFilterQuery->getValueFilterField('cm_team_id');
            $plantId = $reportFilterQuery->getValueFilterField('plant_id');
            $ownerUserId = $reportFilterQuery->getValueFilterField('owner_user_id');
            $contactId = $reportFilterQuery->getValueFilterField('contact_user_id');
            $siteIds = $reportFilterQuery->getValueFilterField('site_id');
            $buildingIds = $reportFilterQuery->getValueFilterField('building_id');
            $roomIds = $reportFilterQuery->getValueFilterField('room_id');
            $hidePrivateNotes = $reportFilterQuery->getValueFilterField('hide_private_notes');

            $inputs['code'] = empty($cmCaseCode) ? null : array_first($cmCaseCode);
            $inputs['description'] = empty($cmCaseDesc) ? null : array_first($cmCaseDesc);
            $inputs['type'] = empty($cmCaseTypeId) ? null : array_first($cmCaseTypeId);
            $inputs['priority'] = empty($cmPriorityId) ? null : array_first($cmPriorityId);
            $inputs['team'] = empty($cmTeamId) ? null : array_first($cmTeamId);
            $inputs['plant_id'] = empty($plantId) ? null : array_first($plantId);
            $inputs['owner'] = empty($ownerUserId) ? null : array_first($ownerUserId);
            $inputs['contact'] = empty($contactId) ? null : array_first($contactId);
            $inputs['site_id'] = empty($siteIds) ? null : array_first($siteIds);
            $inputs['building_id'] = empty($buildingIds) ? null : array_first($buildingIds);
            $inputs['room_id'] = empty($roomIds) ? null : array_first($roomIds);
            $inputs['hide_private_notes'] = empty($hidePrivateNotes) ? CommonConstant::DATABASE_VALUE_YES :
                array_first($hidePrivateNotes);
            $inputs['cm_case_status'] = empty($cmCaseStatusId) ? [] : $cmCaseStatusId;
        }
        return $inputs;
    }
    protected function getModuleAccess()
    {
        $module = null;
        switch ($this->cmType) {
            case CmType::CM_TYPE_CASEMGMT:
                $module = Module::MODULE_CASE_MANAGEMENT;
                break;
            case CmType::CM_TYPE_CASEMGMT2:
                $module = Module::MODULE_CASE_MANAGEMENT_2;
                break;
            case CmType::CM_TYPE_CASEMGMT3:
                $module = Module::MODULE_CASE_MANAGEMENT_3;
                break;
            case CmType::CM_TYPE_CASEMGMT4:
                $module = Module::MODULE_CASE_MANAGEMENT_4;
                break;
            case CmType::CM_TYPE_CASEMGMT5:
                $module = Module::MODULE_CASE_MANAGEMENT_5;
                break;
        }

        return $this->permissionService->hasModuleAccess(
            $module,
            CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
        );
    }
}
