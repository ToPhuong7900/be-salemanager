<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use Tfcloud\Controllers\Admin\General\SiteGroupsController;
use Tfcloud\Lib\Math;
use Tfcloud\Services\Assessment\AsbestosSurveyService;
use Tfcloud\Services\Assessment\ACMService;
use Tfcloud\Services\Assessment\AsbRiskAssessmentService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Admin\General\SiteGroupsService;

class AsbestosReportForEdinburghService extends WkHtmlToPdfReportBaseService
{
    private $asbService;
    private $acmService;
    private $sgController;
    private $sgService;
    public $filterText = '';

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->asbService = new AsbestosSurveyService($permissionService);
        $this->acmService = new ACMService($permissionService);
        $this->sgService = new SiteGroupsService($permissionService);
        $this->sgController = new SiteGroupsController($permissionService, $this->sgService);
    }

    public function runReport($repOutPutPdfFile, $inputs, $repId)
    {
        $siteGroupId = \Auth::User()->site_group_id;

        $flag = "";
        if (empty(($inputs['code']))) {
            $flag = 'true';
        } else {
            $flag = 'false';
        } //set flag to prevent user running report against multiple asb surveys at once

        $asbestosSurvey = "";
        $foundAsbSurvey = "";
        $conditionItems = "";
        $siteCode = "";
        $footerDate = "";

        $asbSurveyQuery = $this->asbestosFilterQuery($inputs);
        $asbestosSurvey = $asbSurveyQuery->get();

        if ((count($asbestosSurvey) > 0) && ($flag != 'true')) {
            $foundAsbSurvey = 'true';
            foreach ($asbestosSurvey as $asbSurvey) {
                $asbSurveyId = $asbSurvey->asb_survey_id;
                $siteCode = $asbSurvey->site_code; //address to be set in footer
                $this->setSitePhotoPath($asbSurvey, $siteGroupId);

                //set address line
                $asbSurvey['addressLine'] = $this->setAsbestosAddressLine($asbSurvey);
                //set date fields
                $asbSurvey['reportDate'] = date('d/m/Y');
                $asbSurvey['clientRef'] = $asbSurvey->asb_survey_client_ref;
                $asbSurvey['surveyDate'] = $asbSurvey->asb_survey_date;
                $footerDate = $asbSurvey->asb_survey_date;

                //get information of customer commissioning survey
                $requestorClientInfoQuery = $this->contactQuery($asbSurvey->requestor_contact_id);
                $customerInfo = $requestorClientInfoQuery->first();
                if (!empty($customerInfo)) {
                    $this->setCustomerContactLine($asbSurvey, $customerInfo);
                } else {
                    $check = 'true';
                    $this->setCustomerContactLine($asbSurvey, $customerInfo, $check); //set default values
                }

                //get address information of lead surveyor
                $leadSurveyorInfoQuery = $this->contactQuery($asbSurvey->surveyor_id);
                $surveyorInfo = $leadSurveyorInfoQuery->first();
                $this->setSurveyorContactLine($asbSurvey, $surveyorInfo); //required, no default needed

                //now get hazard information
                $hazardQuery = $this->asbestosHazardQuery($asbSurvey->asb_survey_id);
                $asbSurvey['asbHazards'] = $hazardQuery->get();
                $this->setAsbestosHazardIndex($asbSurvey, $siteGroupId);

                //get actions
                $actionsQuery = $this->getAllActions($asbSurveyId);
                $asbSurvey['actionItems'] = $actionsQuery->get();
                foreach ($asbSurvey['actionItems'] as $actionItem) {
                    if (is_null($actionItem->room_id)) {
                        $actionItem['locCode'] = $actionItem->building_code;
                        $actionItem['locationDesc'] = $actionItem->building_desc;
                    } else {
                        $actionItem['locCode'] = $actionItem->room_number;
                        $actionItem['locationDesc'] = $actionItem->room_desc;
                    }
                }

                //get areas not inspected info
                $this->getAreasNotInspectedInfo($asbSurvey);

                //get analyst info
                $analystQuery = $this->contactQuery($asbSurvey->analyser_id);
                $analystInfo = $analystQuery->first();
                if (!empty($analystInfo)) {
                    $this->setAnalystContactLine($asbSurvey, $analystInfo);
                } else {
                    $check = 'true';
                    $this->setAnalystContactLine($asbSurvey, $analystInfo, $check);
                }

                //if second surveyor exists, retrieve their information
                if (empty(($asbSurvey->second_surveyor_id))) {
                    $asbSurvey['secondSurveyorName'] = "";
                    $asbSurvey['secondSurveyorDesc'] = "";
                } else {
                    //retrieve the information
                    $secondSurveyorQuery = $this->contactQuery($asbSurvey->second_surveyor_id);
                    $secondSurveyorInfo = $secondSurveyorQuery->first();
                    $asbSurvey['secondSurveyorName'] = $secondSurveyorInfo->contact_name;
                    $asbSurvey['secondSurveyorDesc'] = $secondSurveyorInfo->contact_desc;
                }

                //get sample information
                $sampleQuery = $this->getAsbSampleQuery($asbSurvey->asb_survey_id);
                $asbSurvey['samples'] = $sampleQuery->get();
                $this->setSampleData($asbSurvey);
            } //end foreach asb survey
        } else {
            $foundAsbSurvey = 'false';
        }

        //get foreach condition/extent of damage info
        $conditionItemsQuery = $this->getConditionorExtentofDamageData();
        $conditionItems = $conditionItemsQuery->get();

        // Create wkhtmltopdf temp folder to store generated files
        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);

        $header = \View::make(
            'reports.wkhtmltopdf.asbestos.edinburgh.header',
            [

            ]
        )->render();

        $headerPath = $generatedPath . "/header.html";
        file_put_contents($headerPath, $header, LOCK_EX);

        $footer = \View::make(
            'reports.wkhtmltopdf.asbestos.edinburgh.footer',
            [
                'footerDate' => $footerDate,
                'siteCode'   => $siteCode
            ]
        )->render();
        $footerPath = $generatedPath . "/footer.html";
        file_put_contents($footerPath, $footer, LOCK_EX);

        $content = \View::make(
            'reports.wkhtmltopdf.asbestos.edinburgh.content',
            [
                'asbSurvey'        => $asbestosSurvey,
                'foundAsbSurvey'   => $foundAsbSurvey,
                'conditionItems'   => $conditionItems,
                'flag'             => $flag
            ]
        )->render();

        $contentPath = $generatedPath . "/content.html";

        file_put_contents($contentPath, $content, LOCK_EX);

        $options = [
            'header-html' => $headerPath,
            'footer-html' => $footerPath,
            'header-spacing' => 4,
            'footer-spacing' => 4,
            'orientation' => 'landscape',
        ];

        if (array_key_exists('singleOnly', $inputs)) {
            $repOutPutPdfFile .= '.pdf';
            $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $options);
            return $repOutPutPdfFile;
        } else {
            $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $options);
            return true;
        }
    }

    private function asbestosFilterQuery($inputs)
    {
        $asbestosQuery = \Tfcloud\Models\AsbSurvey::select()
            ->join('site', 'site.site_id', '=', 'asb_survey.site_id')
            ->leftJoin('building', 'building.building_id', '=', 'asb_survey.building_id')
            ->join('asb_survey_status', 'asb_survey_status.survey_status_id', '=', 'asb_survey.survey_status_id')
            ->leftJoin('asb_survey_type', 'asb_survey_type.survey_type_id', '=', 'asb_survey.survey_type_id')
            ->leftjoin('address', 'address.address_id', '=', 'site.address_id')
            ->leftjoin('address_street', 'address_street.address_street_id', '=', 'address.address_street_id');
        if ((!empty($inputs['code'])) && ($inputs['code'] != "") && ($inputs['code'] != " ")) {
            $asbestosQuery->where('asb_survey.asb_survey_code', '=', $inputs['code']);
        }
        return $asbestosQuery;
    }

    private function asbestosHazardQuery($surveyId)
    {
        return \Tfcloud\Models\AsbSurveyHazard::select()
            ->leftJoin('asb_hazard', 'asb_hazard.asb_hazard_id', '=', 'asb_survey_hazard.asb_hazard_id')
            ->leftJoin('building', 'building.building_id', '=', 'asb_hazard.building_id')
            ->leftJoin('room', 'room.room_id', '=', 'asb_hazard.room_id')
            ->leftJoin(
                'asb_hazard_status',
                'asb_hazard_status.asb_hazard_status_id',
                '=',
                'asb_hazard.asb_hazard_status_id'
            )
            ->leftJoin(
                'asb_asbestos_type',
                'asb_asbestos_type.asb_asbestos_type_id',
                '=',
                'asb_hazard.asb_asbestos_type_id'
            )
            ->leftJoin(
                'asb_material_type',
                'asb_material_type.asb_material_type_id',
                '=',
                'asb_hazard.asb_material_type_id'
            )
            ->where('asb_survey_hazard.asb_survey_id', '=', $surveyId);
    }

    private function getAsbSampleQuery($asbId)
    {
        return \Tfcloud\Models\AsbSample::select()
            ->leftJoin('asb_hazard', 'asb_hazard.asb_hazard_id', '=', 'asb_sample.asb_hazard_id')
            ->leftJoin('site', 'site.site_id', '=', 'asb_hazard.site_id')
            ->leftJoin('building', 'building.building_id', '=', 'asb_hazard.building_id')
            ->leftJoin('room', 'room.room_id', '=', 'asb_hazard.room_id')
            ->leftJoin(
                'asb_asbestos_type',
                'asb_asbestos_type.asb_asbestos_type_id',
                '=',
                'asb_sample.asb_asbestos_type_id'
            )
            ->leftJoin(
                'asb_survey_hazard',
                'asb_survey_hazard.asb_hazard_id',
                '=',
                'asb_hazard.asb_hazard_id'
            )
            ->leftJoin('asb_survey', 'asb_survey.asb_survey_id', '=', 'asb_survey_hazard.asb_survey_id')
            ->where('asb_survey_hazard.asb_survey_id', '=', $asbId);
    }

    private function setSampleData(&$asbSurvey)
    {
        foreach ($asbSurvey['samples'] as $sample) {
            if (is_null($sample->room_id)) {
                $sample['locCode'] = $sample->building_code;
                $sample['locationDesc'] = $sample->building_desc;
            } else {
                $sample['locCode'] = $sample->room_number;
                $sample['locationDesc'] = $sample->room_desc;
            }

            if (is_null($sample->asb_asbestos_type_id)) {
                $sample['asbestosCode'] = 'Not detected.';
            } else {
                $sample['asbestosCode'] = $sample->asb_asbestos_type_code;
            }
        }
        return true;
    }

    private function getRiskAssessmentData($hazardId)
    {
        return \Tfcloud\Models\AsbRiskAssessment::select()
            ->leftJoin(
                'asb_risk_status',
                'asb_risk_status.asb_risk_status_id',
                '=',
                'asb_risk_assessment.asb_risk_status_id'
            )
            ->where('asb_risk_status.asb_risk_status_code', '=', 'CURRENT')
            ->where('asb_risk_assessment.asb_hazard_id', '=', $hazardId);
    }

    private function getSampleVarDisplay($id)
    {
        return \Tfcloud\Models\AsbSamplevarTypeScore::select()
            ->where('asb_samplevar_type_score.asb_samplevar_type_score_id', '=', $id);
    }

    private function getAssessFactorDisplay($id)
    {
        return \Tfcloud\Models\AsbAssessfactorTypeScore::select()
            ->where('asb_assessfactor_type_score.asb_assessfactor_type_score_id', '=', $id);
    }

    private function getAllActions($asbSurveyId)
    {
        return \Tfcloud\Models\AsbAction::select()
            ->leftJoin('asb_hazard', 'asb_hazard.asb_hazard_id', '=', 'asb_action.asb_hazard_id')
            ->leftJoin('asb_survey_hazard', 'asb_survey_hazard.asb_hazard_id', '=', 'asb_hazard.asb_hazard_id')
            ->leftJoin('asb_survey', 'asb_survey.asb_survey_id', '=', 'asb_survey_hazard.asb_survey_id')
            ->leftJoin('asb_action_type', 'asb_action_type.asb_action_type_id', '=', 'asb_action.asb_action_type_id')
            ->leftJoin(
                'asb_action_priority',
                'asb_action_priority.asb_action_priority_id',
                '=',
                'asb_action.asb_action_priority_id'
            )
            ->leftJoin(
                'asb_material_type',
                'asb_material_type.asb_material_type_id',
                '=',
                'asb_hazard.asb_material_type_id'
            )
            ->leftJoin('building', 'building.building_id', '=', 'asb_hazard.building_id')
            ->leftJoin('room', 'room.room_id', '=', 'asb_hazard.room_id')
            ->where('asb_survey.asb_survey_id', '=', $asbSurveyId);
    }

    private function getActionRequired($asbHazardId)
    {
        $query = \Tfcloud\Models\AsbAction::select()
            ->leftJoin('asb_action_type', 'asb_action_type.asb_action_type_id', '=', 'asb_action.asb_action_type_id')
            ->where('asb_action.asb_hazard_id', '=', $asbHazardId)
            ->where('asb_action.completed', '=', 'N');
        $actions = $query->get();
        $actionString = "";

        if (count($actions) > 0) {
            foreach ($actions as $action) {
                if (!empty($action)) {
                    $actionString = $actionString . " " . $action->asb_action_type_desc . ".";
                }
            }
        }
        return $actionString;
    }

    private function contactQuery($contactId)
    {
        return \Tfcloud\Models\Contact::select()
            ->leftJoin('address', 'address.address_id', '=', 'contact.address_id')
            ->leftJoin('address_street', 'address_street.address_street_id', '=', 'address.address_street_id')
            ->where('contact.contact_id', '=', $contactId);
    }

    private function getConditionorExtentofDamageData()
    {
        $query = \Tfcloud\Models\AsbSamplevarTypeScore::select()
            ->join(
                'asb_samplevar_type',
                'asb_samplevar_type.asb_samplevar_type_id',
                '=',
                'asb_samplevar_type_score.asb_samplevar_type_id'
            )
            ->where('asb_samplevar_type.asb_samplevar_type_id', '=', 2);
        return $query;
    }

    private function setSitePhotoPath(&$asbSurvey, $siteGroupId)
    {
        //first get site image for top of report
        $siteDetails = \Tfcloud\Models\Site::select()
            ->where('site.site_code', '=', $asbSurvey->site_code)->first();
        $siteId = $siteDetails->site_id;
        $siteImagePath = $this->getPhotoStoragePathway($siteGroupId, $siteId, 'site');
        //pathway checking in wrong folder, redirect
        $splitString = explode("site/", $siteImagePath);

        if ($siteDetails->photo) {
            $firstHalf = $splitString[0];
            $sitePhotoPath = $firstHalf . "site/" . $siteId . "/" . $siteDetails->photo;

            $asbSurvey['hasSiteImg'] = 'true';
            $asbSurvey['sitePhotoPath'] = $sitePhotoPath;

            //check image exists
            if (!file_exists($sitePhotoPath)) {
                //show no picture available file if image does not exist
                $useDefaultPhoto = true;
                $sitePhotoPath = $this->fallBackImagePath($useDefaultPhoto);
                $asbSurvey['sitePhotoPath'] = $sitePhotoPath;
            }
        } else {
            $asbSurvey['hasSiteImg'] = 'false';
        }

        return true;
    }

    private function setAsbestosHazardIndex(&$asbSurvey, $siteGroupId)
    {
        $asbSurvey['totalSampleCount'] = 0;
        $asbSurvey['detectedAsbestosCount'] = 0;

        foreach ($asbSurvey['asbHazards'] as $asbHazard) {
            //get photo
            $photoQuery = \Tfcloud\Models\AsbHazard::select()
                ->where('asb_hazard.asb_hazard_id', '=', $asbHazard->asb_hazard_id)->first();
            $photoPath = $this->photoPath('asbhaz', $asbHazard->asb_hazard_id, $photoQuery->photo, $siteGroupId);

            if ($photoQuery->photo) {
                $asbHazard['hasMainPhoto'] = 'true';

                //check image exists
                if (!file_exists($photoPath)) {
                    //show no picture available file if image does not exist
                    $useDefaultPhoto = true;
                    $photoPath = $this->fallBackImagePath($useDefaultPhoto);
                    $asbHazard['mainPhotoPath'] = $photoPath;
                } else {
                    $asbHazard['mainPhotoPath'] = $photoPath;
                }
            } else {
                $asbHazard['hasMainPhoto'] = 'false';
            }

            //get other images
            $positivePath = $this->getPhotoStoragePathway($siteGroupId, $asbHazard->asb_hazard_id, 'asbhaz');
            $otherFilePath = $positivePath . "images/";
            $asbHazard['otherImages'] = $this->acmService->findImages($asbHazard->asb_hazard_id);
            $otherImages = [];
            foreach ($asbHazard['otherImages'] as $otherImage) {
                $fileName = $otherFilePath . $otherImage;
                array_push($otherImages, $fileName);
            }
            $asbHazard['otherImagePaths'] = $otherImages;

            //get risk assessment data
            $riskAssessmentQuery = $this->getRiskAssessmentData($asbHazard->asb_hazard_id);
            $riskData = $riskAssessmentQuery->first();
            $asbHazardId = $asbHazard->asb_hazard_id;

            $asbHazard['locCode'] = "";
            $productType = $extentDamage = $surfaceTreatment = $locationData = $quantity = $actionRequired = [];
            if (is_null($asbHazard->room_id)) {
                $asbHazard['locCode'] = $asbHazard->building_code;
                $asbHazard['locationDesc'] = $asbHazard->building_desc;
            } else {
                $asbHazard['locCode'] = $asbHazard->room_number;
                $asbHazard['locationDesc'] = $asbHazard->room_desc;
            }

            //get action information
            $actionString = $this->getActionRequired($asbHazardId);
            $asbHazard['actionNeeded'] = $actionString;

            //get the product type
            if (!empty($riskData)) {
                if (is_null($riskData->product_type_sid)) {
                    $asbHazard['productType'] = "";
                } else {
                    $productQuery = $this->getSampleVarDisplay($riskData->product_type_sid);
                    $productType = $productQuery->first();
                    $asbHazard['productType'] = $productType->display;
                }

                if (is_null($riskData->extent_damage_sid)) {
                    $asbHazard['extentDamage'] = "";
                } else {
                    $damageQuery = $this->getSampleVarDisplay($riskData->extent_damage_sid);
                    $extentDamage = $damageQuery->first();
                    $asbHazard['extentDamage'] = $extentDamage->display;
                }

                if (is_null($riskData->surface_treatment_sid)) {
                    $asbHazard['surfaceTreatment'] = "";
                } else {
                    $surfaceTreatmentQuery = $this->getSampleVarDisplay($riskData->surface_treatment_sid);
                    $surfaceTreatment = $surfaceTreatmentQuery->first();
                    $asbHazard['surfaceTreatment'] = $surfaceTreatment->display;
                }

                if (is_null($riskData->mdhs100_pri_loc_sid)) {
                    $asbHazard['locationDisplay'] = "";
                } else {
                    $locationQuery = $this->getAssessFactorDisplay($riskData->mdhs100_pri_loc_sid);
                    $locationData = $locationQuery->first();
                    $asbHazard['locationDisplay'] = $locationData->display;
                }

                if (is_null($riskData->mdhs100_pri_extent_sid)) {
                    $asbHazard['quantityDisplay'] = "";
                } else {
                    $quantityQuery = $this->getAssessFactorDisplay($riskData->mdhs100_pri_extent_sid);
                    $quantity = $quantityQuery->first();
                    $asbHazard['quantityDisplay'] = $quantity->display;
                }

                if (is_null($riskData->total_material_score)) {
                    $asbHazard['total_material_score'] = "";
                } else {
                    $asbHazard['total_material_score'] = $riskData->total_material_score;
                }
            } else {
                $asbHazard['productType'] = "";
                $asbHazard['extentDamage'] = "";
                $asbHazard['surfaceTreatment'] = "";
                $asbHazard['locationDisplay'] = "";
                $asbHazard['quantityDisplay'] = "";
                $asbHazard['actionRequired'] = "";
                $asbHazard['total_material_score'] = "";
            }


            //set counts for asbestos detection to be used in report
            $asbSurvey['totalSampleCount'] = $asbSurvey['totalSampleCount'] + 1;

            if (
                ($asbHazard->asb_hazard_status_code == 'DETECTED') ||
                    ($asbHazard->asb_hazard_status_code == 'REMOVED')
            ) {
                $asbSurvey['detectedAsbestosCount'] = $asbSurvey['detectedAsbestosCount'] + 1;
            }
        } //end foreach acm/hazard
        return true;
    }

    private function getAreasNotInspectedInfo(&$asbSurvey)
    {
        $surveyRoomsQuery = \Tfcloud\Models\AsbRoom::select()
            ->where('asb_room.asb_survey_id', '=', $asbSurvey->asb_survey_id)
            ->where('asb_room.asb_room_status_id', '=', \Tfcloud\Models\AsbRoomStatus::NOT_SURVEYED)
            ->leftJoin('room', 'room.room_id', '=', 'asb_room.room_id');
        $rooms = $surveyRoomsQuery->get();
        if (count($rooms) > 0) {
            $asbSurvey['hasUnInspectedAreas'] = 'true';
            $asbSurvey['areasNotInspected'] = $surveyRoomsQuery->get();
        } else {
            $asbSurvey['hasUnInspectedAreas'] = 'false';
        }
    }

    private function setAsbestosAddressLine($asbSurvey)
    {
        $addressLine = $asbSurvey->site_code;
        //create address object
        if (!empty(($asbSurvey->first_addr_obj))) {
            $addressLine = $addressLine . ", " . $asbSurvey->first_addr_obj;
        }

        if (!empty(($asbSurvey->second_addr_obj))) {
            $addressLine = $addressLine . ", " . $asbSurvey->second_addr_obj;
        }

        if (!empty(($asbSurvey->street))) {
            $addressLine = $addressLine . ", " . $asbSurvey->street;
        }

        if (!empty(($asbSurvey->town))) {
            $addressLine = $addressLine . ", " . $asbSurvey->town;
        }

        if (!empty(($asbSurvey->region))) {
            $addressLine = $addressLine . ", " . $asbSurvey->region;
        }

        if (!empty(($asbSurvey->country))) {
            $addressLine = $addressLine . ", " . $asbSurvey->country;
        }

        if (!empty(($asbSurvey->postcode))) {
            $addressLine = $addressLine . ", " . $asbSurvey->postcode;
        }

        return $addressLine;
    }

    private function setCustomerContactLine(&$asbSurvey, $contactInfo, $check = null)
    {
        $asbSurvey['customerName'] = "[None given]";
        $asbSurvey['customerOrganisation'] = "[None given]";
        $asbSurvey['customerFirstAddr'] = "";
        $asbSurvey['customerSecondAddr'] = "";
        $asbSurvey['customerStreet'] = "";
        $asbSurvey['customerTown'] = "";
        $asbSurvey['customerRegion'] = "";
        $asbSurvey['customerCountry'] = "";
        $asbSurvey['customerPostcode'] = "";

        if ($check === 'true') {
            return true;
        } else {
            if (!empty(($contactInfo->contact_name))) {
                $asbSurvey['customerName'] = $contactInfo->contact_name;
            }

            if (!empty(($contactInfo->organisation))) {
                $asbSurvey['customerOrganisation'] = $contactInfo->organisation;
            }

            //create address object
            if (!empty(($contactInfo->first_addr_obj))) {
                //create address line programatically
                $asbSurvey['customerFirstAddr'] = $contactInfo->first_addr_obj;
            }
            if (!empty(($contactInfo->second_addr_obj))) {
                //create address line programatically
                $asbSurvey['customerSecondAddr'] = $contactInfo->second_addr_obj;
            }

            if (!empty(($contactInfo->street))) {
                //create address line programatically
                $asbSurvey['customerStreet'] = $contactInfo->street;
            }

            if (!empty(($contactInfo->town))) {
                //create address line programatically
                $asbSurvey['customerTown'] = $contactInfo->town;
            }

            if (!empty(($contactInfo->region))) {
                //create address line programatically
                $asbSurvey['customerRegion'] = $contactInfo->region;
            }

            if (!empty(($contactInfo->country))) {
                $asbSurvey['customerCountry'] = $contactInfo->country;
            }

            if (!empty(($contactInfo->postcode))) {
                $asbSurvey['customerPostcode'] = $contactInfo->postcode;
            }

            return true;
        }
    }

    private function setSurveyorContactLine(&$asbSurvey, $contactInfo)
    {
        $asbSurvey['surveyorName'] = $contactInfo->contact_name;
        $asbSurvey['surveyorOrganisation'] = $contactInfo->organisation;
        $asbSurvey['surveyorDesc'] = $contactInfo->contact_desc;
        $asbSurvey['surveyorFirstAddr'] = "";
        $asbSurvey['surveyorSecondAddr'] = "";
        $asbSurvey['surveyorStreet'] = "";
        $asbSurvey['surveyorTown'] = "";
        $asbSurvey['surveyorRegion'] = "";
        $asbSurvey['surveyorCountry'] = "";
        $asbSurvey['surveyorPostcode'] = "";
        $asbSurvey['surveyorPhone'] = "";

        //create address object
        if (isNonEmptyString($contactInfo->first_addr_obj)) {
            //create address line programatically
            $asbSurvey['surveyorFirstAddr'] = $contactInfo->first_addr_obj;
        }
        if (isNonEmptyString($asbSurvey->second_addr_obj)) {
            $asbSurvey['surveyorSecondAddr'] = $contactInfo->second_addr_obj;
        }

        if (isNonEmptyString($asbSurvey->street)) {
            $asbSurvey['surveyorStreet'] = $contactInfo->street;
        }

        if (isNonEmptyString($asbSurvey->town)) {
            $asbSurvey['surveyorTown'] = $contactInfo->town;
        }

        if (isNonEmptyString($asbSurvey->region)) {
            $asbSurvey['surveyorRegion'] = $contactInfo->region;
        }

        if (isNonEmptyString($asbSurvey->country)) {
            $asbSurvey['surveyorCountry'] = $contactInfo->country;
        }

        if (isNonEmptyString($asbSurvey->postcode)) {
            $asbSurvey['surveyorPostcode'] = $contactInfo->postcode;
        }

        if (isNonEmptyString($contactInfo->phone_main)) {
            $asbSurvey['surveyorPhone'] = $contactInfo->phone_main;
        } else {
            //check all phone fields for data
            if (isNonEmptyString($contactInfo->phone_work)) {
                $asbSurvey['surveyorPhone'] = $contactInfo->phone_work;
            } else {
                if (isNonEmptyString($contactInfo->phone_mobile)) {
                    $asbSurvey['surveyorPhone'] = $contactInfo->phone_mobile;
                }
            }
        }

        return true;
    }

    private function setAnalystContactLine(&$asbSurvey, $contactInfo, $check = null)
    {
        $asbSurvey['analystName'] = '[No analyst found]';
        $asbSurvey['analystOrganisation'] = "[No analyst found]";
        $asbSurvey['analystAddressLine'] = "[No analyst address found]";

        if ($check === 'true') {
            return true;
        } else {
            //create address object
            $asbSurvey['analystName'] = $contactInfo->contact_name;
            $asbSurvey['analystOrganisation'] = $contactInfo->organisation;
            $asbSurvey['analystAddressLine'] = $contactInfo->organisation;

            if (!empty(($contactInfo->first_addr_obj))) {
                $asbSurvey['analystAddressLine'] =
                        $asbSurvey['analystAddressLine'] . ", " . $contactInfo->first_addr_obj;
            }

            if (!empty(($contactInfo->second_addr_obj))) {
                $asbSurvey['analystAddressLine'] =
                        $asbSurvey['analystAddressLine'] . ", " . $contactInfo->second_addr_obj;
            }

            if (!empty(($contactInfo->street))) {
                $asbSurvey['analystAddressLine'] = $asbSurvey['analystAddressLine'] . ", " . $contactInfo->street;
            }

            if (!empty(($contactInfo->town))) {
                $asbSurvey['analystAddressLine'] = $asbSurvey['analystAddressLine'] . ", " . $contactInfo->town;
            }

            if (!empty(($contactInfo->region))) {
                $asbSurvey['analystAddressLine'] = $asbSurvey['analystAddressLine'] . ", " . $contactInfo->region;
            }

            if (!empty(($contactInfo->country))) {
                $asbSurvey['analystAddressLine'] = $asbSurvey['analystAddressLine'] . ", " . $contactInfo->country;
            }

            if (!empty(($contactInfo->postcode))) {
                $asbSurvey['analystAddressLine'] = $asbSurvey['analystAddressLine'] . ", " . $contactInfo->postcode;
            }

            return true;
        }
    }
}
