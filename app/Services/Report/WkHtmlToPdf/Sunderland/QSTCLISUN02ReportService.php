<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf\Sunderland;

use DateTime;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\ReportConstant;
use Tfcloud\Lib\Filters\Questionnaire\FiledQuestionnaireFilter;
use Tfcloud\Models\FiledQst;
use Tfcloud\Models\Qst;
use Tfcloud\Models\QstStatusType;
use Tfcloud\Models\QstType;
use Tfcloud\Models\Report;
use Tfcloud\Models\SystemReport;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\WkHtmlToPdf\WkHtmlToPdfReportBaseService;

class QSTCLISUN02ReportService extends WkHtmlToPdfReportBaseService
{
    public $filterText = '';
    private $qstId = 0;
    private $reportBoxFilterLoader;

    public function __construct(
        PermissionService $permissionService,
        Report $report = null
    ) {
        parent::__construct($permissionService);
        $this->permissionService = $permissionService;
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    private function isReportBoxFilter()
    {
        return $this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter();
    }

    public function runReport($repOutPutPdfFile, $inputs, $repId)
    {
        $this->qstId = $this->getQstFromRepId($repId);
        $inputs = $this->formatInputs($inputs);
        $data = $this->getReportData($inputs);

        // Create wkhtmltopdf temp folder to store generated files
        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);
        $content = \View::make('reports.wkhtmltopdf.questionnaire.sunderland.sun02.content', $data)->render();
        $contentFile = $generatedPath . '/content.html';
        file_put_contents($contentFile, $content, LOCK_EX);

        $whereCodes = [];               // The codes which have been used in the filter
        $whereTexts = [];               // Plain text about filtering applied
        $orCodes = [];                  // csv string of OR codes
        $bFilterDetail = false;

        $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
        $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $this->filterText);

        $options = [
            'orientation' => 'landscape',
        ];

        $this->generateSingleHtmlToPdf($contentFile, $repOutPutPdfFile, $options);

        return true;
    }

    public function validateFilter($inputs)
    {
        return \Validator::make($this->formatInputs($inputs), $this->validateRules(), $this->validateMessages());
    }

    private function formatInputs($inputs)
    {
        if ($this->isReportBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $siteValue = $reportFilterQuery->getValueFilterField('site_id');
            $buildingValue = $reportFilterQuery->getValueFilterField('building_id');
            $codeValue = $reportFilterQuery->getValueFilterField('filed_qst_code');
            $ownerValue = $reportFilterQuery->getValueFilterField('owner_user_id');
            $date = $reportFilterQuery->getValueFilterField('filed_qst_date');

            $inputs['site_id'] = empty($siteValue) ? null : $siteValue[0];
            $inputs['building_id'] = empty($buildingValue) ? null : $buildingValue[0];
            $inputs['code'] = empty($codeValue) ? null : $codeValue[0];
            $inputs['owner'] = empty($ownerValue) ? null : $ownerValue[0];
            $inputs['status_type_id'] = $reportFilterQuery->getValueFilterField('status_type_id') ?? [];
            if (is_array($date)) {
                list($dataFrom, $dataTo) = array_pop($date);
                $inputs['dateFrom'] = Common::dateFieldDisplayFormat($dataFrom);
                $inputs['dateTo'] = Common::dateFieldDisplayFormat($dataTo);
            }
        }
        return $inputs;
    }

    public function validateRules()
    {
        return [
            'site_id' => ['required'],
            'building_id' => ['required_with:room_id'],
        ];
    }

    public function validateMessages()
    {
        return [
            'site_id.required' => 'Location is required.'
        ];
    }

    private function getQstFromRepId($repId)
    {
        $sysReport = SystemReport::find($repId);

        $query = Qst::userSiteGroup()
            ->select(['qst_id']);

        switch ($sysReport->system_report_code) {
            case ReportConstant::SYSTEM_REPORT_QST_CLI_SUN02:
                $query->where('qst_code', '02');
                break;
            case ReportConstant::SYSTEM_REPORT_QST_CLI_SUN03:
                $query->where('qst_code', '03');
                break;
            case ReportConstant::SYSTEM_REPORT_QST_CLI_SUN04:
                $query->where('qst_code', '04');
                break;
            default:
        }

        $qst = $query->get()->first();
        if ($qst) {
            return $qst->qst_id;
        }
        return 0;
    }

    private function getReportData($inputs)
    {
        $filedQsts = $this->filterAll($this->all(), $inputs);

        return ['filedQsts' => $filedQsts->get()];
    }

    private function filterAll($query, $inputs)
    {
        $filter = new FiledQuestionnaireFilter($inputs);

        if (\Auth::User()->isSelfServiceUser()) {
            $query->where('qst_status_type.status_type_id', '<>', QstStatusType::OPEN)
                ->where('qst_status_type.status_type_id', '<>', QstStatusType::QA);
        }

        if (!is_null($filter->code)) {
            $code = trim($filter->code);
            $query->where('filed_qst.filed_qst_code', 'like', "%{$code}%");
        }

        //required
        //if (!empty($filter->qst)) {
            $query->where('filed_qst.qst_id', '=', $this->qstId);
        //}

        if (!empty($filter->site_id)) {
            $query->where('filed_qst.site_id', '=', $filter->site_id);
        }

        if (!empty($filter->owner)) {
            $query->where('filed_qst.owner_user_id', '=', $filter->owner);
        }

        if (!empty($filter->dateFrom)) {
            $dateFrom = DateTime::createFromFormat('d/m/Y', $filter->dateFrom);
            if ($dateFrom) {
                $query->where(
                    \DB::raw("DATE_FORMAT(filed_qst.filed_qst_date, '%Y-%m-%d')"),
                    '>=',
                    $dateFrom->format('Y-m-d')
                );
            }
        }

        if (!empty($filter->dateTo)) {
            $dateTo = DateTime::createFromFormat('d/m/Y', $filter->dateTo);
            if ($dateTo) {
                $query->where(
                    \DB::raw("DATE_FORMAT(filed_qst.filed_qst_date, '%Y-%m-%d')"),
                    '<=',
                    $dateTo->format('Y-m-d')
                );
            }
        }

        if (!is_null($filter->status_type_id) && is_array($filter->status_type_id) && count($filter->status_type_id)) {
            $query->whereIn('qst_status_type.status_type_id', $filter->status_type_id);
        }
        return $query;
    }

    private function all()
    {
        $query = FiledQst
            ::join('qst', function ($join) {
                $join->on('qst.qst_id', '=', 'filed_qst.qst_id')
                     ->on('qst.site_group_id', '=', \DB::raw(\Auth::user()->site_group_id))
                     ->on('qst.qst_type_id', '=', \DB::raw(QstType::GENERAL));
            })

            ->join('site', 'site.site_id', '=', 'filed_qst.site_id')
            ->join('qst_status', function ($join) {
                $join->on('qst_status.qst_status_id', '=', 'filed_qst.qst_status_id')
                     ->on('qst_status.site_group_id', '=', \DB::raw(\Auth::user()->site_group_id));
            })
            ->join('qst_status_type', 'qst_status_type.status_type_id', '=', "qst_status.status_type_id")
            ->join('contact', 'filed_qst.contact_id', '=', "contact.contact_id")
            ->leftJoin('user', 'user.id', '=', "filed_qst.owner_user_id")

            //Always get Current = 1
            ->where('filed_qst.filed_qst_current', '=', 1)
            //don't get Any Question in Building level
            ->whereNull('filed_qst.building_id')
            ->select([
                'filed_qst.filed_qst_id',
                'filed_qst.filed_qst_current',
                'filed_qst.filed_qst_code',
                'filed_qst.filed_qst_comment',
                'filed_qst.filed_qst_date',
                'contact.contact_name',
                'user.display_name',
                'qst.qst_id',
                'qst.qst_desc',
                'site.site_id',
                'site.site_code',
                'site.site_desc',
                'qst_status.qst_status_code',
            ]);

        return $this->permissionService->listViewSiteAccess($query, "filed_qst.site_id", $siteAccess);
    }

    private function generateFilterData($filterData, &$whereCodes, &$whereTexts, &$orCodes)
    {
        if ($code = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Code = {$code}");
        }

        if ($qst = Common::iset($filterData['qst'])) {
            array_push($whereCodes, ['qst', 'qst_id', 'qst_code', $qst, "Questionnaire Code"]);
        }

        if ($siteId = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $siteId, "Site Code"]);
        }

        if ($dateForm = Common::iset($filterData['dateFrom'])) {
            array_push($whereTexts, "Date From = {$dateForm}");
        }

        if ($dateTo = Common::iset($filterData['dateTo'])) {
            array_push($whereTexts, "Date To = {$dateTo}");
        }

        if ($val = Common::iset($filterData['owner'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Owner']);
        }

        if (Common::iset($filterData['status_type_id'])) {
            foreach ($filterData['status_type_id'] as $typeId) {
                switch ($typeId) {
                    case QstStatusType::OPEN:
                    case QstStatusType::QA:
                    case QstStatusType::ISSUED:
                    case QstStatusType::ARCHIVE:
                    case QstStatusType::COMPLETE:
                    case QstStatusType::CANCELLED:
                        $status[] = $typeId;
                        break;
                    default:
                }
            }

            if (count($status)) {
                array_push($orCodes, [
                    'qst_status',
                    'qst_status_id',
                    'qst_status_code',
                    implode(',', $status),
                    "Status Type"
                ]);
            }
        }
    }
}
