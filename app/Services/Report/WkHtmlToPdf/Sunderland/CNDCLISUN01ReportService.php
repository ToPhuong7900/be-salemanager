<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf\Sunderland;

use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Filters\ConditionFilter;
use Tfcloud\Models\Building;
use Tfcloud\Models\Report;
use Tfcloud\Models\Site;
use Tfcloud\Models\CondsurveyStatus;
use Tfcloud\Models\FiledQst;
use Tfcloud\Models\Idwork;
use Tfcloud\Models\IdworkStatusType;
use Tfcloud\Models\QstType;
use Tfcloud\Models\QstStatusType;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\WkHtmlToPdf\WkHtmlToPdfReportBaseService;

class CNDCLISUN01ReportService extends WkHtmlToPdfReportBaseService
{
    public $filterText = '';
    private $reportBoxFilterLoader = '';

    public function __construct(PermissionService $permissionService, Report $report = null)
    {
        parent::__construct($permissionService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function runReport($repOutPutPdfFile, $inputs, $repId)
    {
        $inputs = $this->formatInputs($inputs);
        $condSurveys = $this->getReportData($inputs);
        if (!$condSurveys) {
            return false;
        }

        // Create wkhtmltopdf temp folder to store generated files
        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);

        $content = \View::make(
            'reports.wkhtmltopdf.condition.cndCliSun01.content',
            [
                'condSurveys' => $condSurveys
            ]
        )->render();
        $contentFile = $generatedPath . "/content.html";
        file_put_contents($contentFile, $content, LOCK_EX);

        $whereCodes = [];               // The codes which have been used in the filter
        $whereTexts = [];               // Plain text about filtering applied
        $orCodes = [];                  // csv string of OR codes
        $bFilterDetail = false;

        $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
        $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $this->filterText);

        $this->generateSingleHtmlToPdf($contentFile, $repOutPutPdfFile);
        return true;
    }

    public function validateFilter($inputs)
    {
        return \Validator::make($this->formatInputs($inputs), $this->validateRules(), $this->validateMessages());
    }

    public function validateRules()
    {
        $rules = [
            'site_id' => ['required'],
            'building_id' => ['required_with:room_id'],
        ];

        return $rules;
    }

    public function validateMessages()
    {
        $messages = [
            'site_id.required' => 'Location is required.'
        ];

        return $messages;
    }


    private function getReportData($inputs)
    {
        $buildingId = array_get($inputs, 'building_id', null);
        //$siteId = array_get($inputs, 'site_id', null);
        $surveys = $this->filter($this->all(($buildingId > 0)), $inputs);

        $buildingSurveys = $surveys->get();

        foreach ($buildingSurveys as $key => $survey) {
            $survey->filed_qsts = FiledQst::join('qst', function ($join) {
                    $join->on('qst.qst_id', '=', 'filed_qst.qst_id')
                    ->on('qst.qst_type_id', '=', \DB::raw(QstType::GENERAL));
            })
                ->where('filed_qst.site_id', $survey->site_id)
                ->whereNull('filed_qst.building_id')
                ->pluck('qst.qst_code')->unique()->toArray();

            $siteBuildings = Building::where('building.site_id', $survey->site_id)
                ->pluck('building_id')->toArray();
            $surveyedBuildings = Building::leftjoin(
                'condsurvey',
                'condsurvey.condsurvey_building_id',
                'building.building_id'
            )
                ->where('building.site_id', $survey->site_id)
                ->where('condsurvey.condsurvey_status_id', '>=', CondsurveyStatus::COMPLETE)
                ->pluck('building_id')->unique()->toArray();

            $survey->ampInspection = (count($siteBuildings) == count($surveyedBuildings));

            $idworks = $this->filterCndIdWork($this->cndIdWork(), $inputs)
                ->unionAll($this->filterActions($this->qstActions(), $inputs))
                ->orderBy('idwork_element_code')->get();

            if ($idworks->count() > 0) {
                $survey->idworks = $idworks;
            } else {
                // building survey that has no idwork.
                unset($buildingSurveys[$key]);
            }
        }

        return $buildingSurveys;
    }

    private function filterCndIdWork($query, $inputs)
    {
        $filter = new ConditionFilter($inputs);

        if (!empty($filter->building_id)) {
            $query->where('building.building_id', '=', $filter->building_id);
        } else {
            if (!empty($filter->site_id)) {
                $query->where('site.site_id', '=', $filter->site_id);
            }
        }

        if (!empty($filter->surveyor)) {
            $query->where('condsurvey.surveyor', '=', $filter->surveyor);
        }

        if (!empty($filter->code)) {
            $query->where('condsurvey.condsurvey_code', 'like', '%' . trim($filter->code) . '%');
        }

        if (!empty($filter->description)) {
            $query->where('condsurvey.condsurvey_desc', 'like', '%' . $filter->description . '%');
        }

        if (!empty($filter->reference)) {
            $query->where('condsurvey.reference', 'like', '%' . $filter->reference . '%');
        }

        if (!empty($filter->establishment)) {
            $query->where('establishment_id', '=', $filter->establishment);
        }


        $query->whereRaw(
            'condsurvey.survey_date IN(SELECT MAX(BS2.survey_date) AS survey_date FROM condsurvey AS BS2 '
                . 'WHERE BS2.condsurvey_building_id = condsurvey.condsurvey_building_id)'
        )
        ->where('condsurvey.condsurvey_status_id', '<>', CondsurveyStatus::HISTORIC)
        ->where('idwork.total_cost', '>', 0)
        ->where('idwork_status.idwork_status_type_id', '<', IdworkStatusType::COMPLETE)
        ->whereIn('idwork_priority.idwork_priority_code', ['1', '2', '3']);

        return $query;
    }


    private function cndIdWork()
    {
        return Idwork::select(
            [
                'idwork.idwork_code',
                'idwork.remedy',
                'idwork.defect',
                'idwork.total_cost',
                'idwork_element.idwork_element_code',
                'idwork_element.idwork_element_desc',
                'idwork_priority.idwork_priority_code',
                'building.building_code',
                'building.building_desc',
                'site.site_code',
                'site.site_desc',
                'room.room_number',
                'room.room_desc',
            ]
        )
            ->join('idwork_status', 'idwork_status.idwork_status_id', '=', 'idwork.idwork_status_id')
            ->leftjoin(
                'building',
                'building.building_id',
                '=',
                'idwork.building_id'
            )
            ->join(
                'site',
                'site.site_id',
                '=',
                'building.site_id'
            )
            ->leftjoin(
                'room',
                'room.room_id',
                '=',
                'idwork.room_id'
            )
            ->leftjoin(
                'idwork_element',
                'idwork_element.idwork_element_id',
                '=',
                'idwork.element_id'
            )
            ->leftjoin(
                'idwork_priority',
                'idwork_priority.idwork_priority_id',
                '=',
                'idwork.idwork_priority_id'
            )
            ->join(
                'condsurvey',
                'condsurvey.condsurvey_id',
                '=',
                'idwork.condsurvey_id'
            );
    }

    private function filterActions($query, $inputs)
    {
        $filter = new ConditionFilter($inputs);

        if (!empty($filter->building_id)) {
            $query->where('filed_qst.building_id', '=', $filter->building_id);
        } else {
            if (!empty($filter->site_id)) {
                $query->where('filed_qst.site_id', '=', $filter->site_id);
            }
        }

        if (!empty($filter->code)) {
            $query->where('filed_qst.filed_qst_code', 'like', '%' . trim($filter->code) . '%');
        }

        if (!empty($filter->establishment)) {
            $query->where('establishment_id', '=', $filter->establishment);
        }

        $query->where('site.site_group_id', '=', \Auth::User()->site_group_id)
            ->where('filed_qst_action.filed_action_est_cost', '>', 0)
            ->where('qst_status.status_type_id', '<>', QstStatusType::ARCHIVE)
            ->whereNull('filed_qst_action.filed_action_completed_date')
            ->whereIn('qst_action_priority.action_priority_code', ['01', '02', '03', '05', 'H', 'M', 'L']);

        return $query;
    }


    private function qstActions()
    {
        $selects = [
            \DB::raw("'Action' AS fqa_code"),
            'filed_qst_action.filed_action_problem',
            'filed_qst_action.filed_action_solution',
            'filed_qst_action.filed_action_est_cost',
            'gen_userdef_sel.gen_userdef_sel_code',
            'gen_userdef_sel.gen_userdef_sel_desc',
            \DB::raw("CASE qst_action_priority.action_priority_code
                WHEN '01'
                THEN 1
                WHEN '05'
                THEN 1
                WHEN 'H'
                THEN 1
                WHEN '02'
                THEN '2'
                WHEN 'M'
                THEN '2'
                WHEN '03'
                THEN '3'
                WHEN 'L'
                THEN '3'
                ELSE action_priority_code
            END AS action_priority_code"),
            'building.building_code',
            'building.building_desc',
            'site.site_code',
            'site.site_desc',
            \DB::raw("'' AS room_code"),
            \DB::raw("'' AS room_desc"),
        ];

        $query = FiledQst::select($selects)
            ->join(
                'filed_qst_action',
                'filed_qst_action.filed_qst_id',
                '=',
                'filed_qst.filed_qst_id'
            )
            ->join(
                'site',
                'site.site_id',
                '=',
                'filed_qst.site_id'
            )
            ->leftJoin(
                'building',
                'building.building_id',
                '=',
                'filed_qst.building_id'
            )
            ->join(
                'qst',
                'qst.qst_id',
                '=',
                'filed_qst.qst_id'
            )
            ->join(
                'qst_status',
                'qst_status.qst_status_id',
                '=',
                'filed_qst.qst_status_id'
            )
            ->join(
                'qst_action_priority',
                'qst_action_priority.action_priority_id',
                '=',
                'filed_qst_action.action_priority_id'
            )
            ->join(
                'gen_userdef_group',
                'gen_userdef_group.gen_userdef_group_id',
                '=',
                'filed_qst_action.gen_userdef_group_id'
            )
            ->leftJoin(
                'gen_userdef_sel',
                'gen_userdef_sel.gen_userdef_sel_id',
                '=',
                'gen_userdef_group.sel2_gen_userdef_sel_id'
            )
            ->leftJoin(\DB::raw('(SELECT 1 AS Major, 5000 AS ULimit) as major_capital'), function ($join) {
                $join->on('filed_qst_action.filed_action_est_cost', '>', 'major_capital.ULimit');
            })
            ->join(
                \DB::raw(
                    '(SELECT DISTINCT MAX(FQ2.filed_qst_date) AS survey_date, FQ2.site_id, FQ2.filed_qst_id
                    FROM filed_qst FQ2
                    INNER JOIN site ON site.site_id = FQ2.site_id
                    GROUP BY FQ2.site_id, FQ2.filed_qst_id) as LS'
                ),
                function ($join) {
                    $join->on('filed_qst.filed_qst_date', '=', 'LS.survey_date')
                    ->on('filed_qst.site_id', '=', 'LS.site_id')
                    ->on('filed_qst.filed_qst_id', '=', 'LS.filed_qst_id');
                }
            );

        return $query;
    }

    private function all($joinBuilding)
    {
        $selects = [
            'site.site_id',
            'site.site_desc',
            'site.dfe_number'
        ];

        if ($joinBuilding) {
            array_push($selects, 'building.building_id', 'building.building_desc');
        } else {
            array_push(
                $selects,
                \DB::raw('NULL AS building_id'),
                \DB::raw('NULL AS building_desc')
            );
        }

        $query = Site::select($selects)
            ->where('site.site_group_id', '=', \Auth::User()->site_group_id);

        if ($joinBuilding) {
            $query->leftJoin(
                'building',
                'building.site_id',
                '=',
                'site.site_id'
            );
        }

        return $query;
    }

    private function filter($query, $inputs)
    {
        $filter = new ConditionFilter($inputs);

        if (!empty($filter->building_id)) {
            $query->where('building.building_id', '=', $filter->building_id);
        } else {
            if (!empty($filter->site_id)) {
                $query->where('site.site_id', '=', $filter->site_id);
            }
        }

        return $query;
    }

    private function generateFilterData($filterData, &$whereCodes, &$whereTexts, &$orCodes)
    {
        if ($val = Common::iset($filterData['establishment'])) {
            array_push(
                $whereCodes,
                array('establishment', 'establishment_id', 'establishment_code', $val, 'Establishment')
            );
        }

        if ($siteId = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $siteId, "Site Code"]);

            if ($buildingId = Common::iset($filterData['building_id'])) {
                array_push(
                    $whereCodes,
                    ['building', 'building_id', 'building_code', $buildingId, "Building Code"]
                );
            }
        }

        if ($val = Common::iset($filterData['surveyor'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Surveyor']);
        }

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Description contains '" . $val . "'");
        }

        $status = [];
        if ($val = Common::iset($filterData['status'])) {
            $status = $val;
        }
        if (count($status)) {
            array_push($orCodes, [
                'condsurvey_status',
                'condsurvey_status_id',
                'condsurvey_status_code',
                implode(',', $status),
                "Survey Status"
            ]);
        }

        if ($val = Common::iset($filterData['reference'])) {
            array_push($whereTexts, "Reference contains '" . $val . "'");
        }
    }
    private function formatInputs($inputs)
    {
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $siteValue = $reportFilterQuery->getValueFilterField('site_id');
            $buildingValue = $reportFilterQuery->getValueFilterField('building_id');
            $establishmentValue = $reportFilterQuery->getValueFilterField('establishment_id');
            $codeValue = $reportFilterQuery->getValueFilterField('condsurvey_code');
            $descriptionValue = $reportFilterQuery->getValueFilterField('condsurvey_desc');
            $statusValue = $reportFilterQuery->getValueFilterField('condsurvey_status_id');
            $referenceValue = $reportFilterQuery->getValueFilterField('reference');
            $surveyorValue = $reportFilterQuery->getValueFilterField('surveyor');
            $inputs['site_id'] = empty($siteValue) ? null : $siteValue[0];
            $inputs['building_id'] = empty($buildingValue) ? null : $buildingValue[0];
            $inputs['establishment'] = empty($establishmentValue) ? null : $establishmentValue[0];
            $inputs['code'] = empty($codeValue) ? null : $codeValue[0];
            $inputs['status'] = empty($statusValue) ? null : $statusValue;
            $inputs['reference'] = empty($referenceValue) ? null : $referenceValue[0];
            $inputs['description'] = empty($descriptionValue) ? null : $descriptionValue[0];
            $inputs['surveyor'] = empty($surveyorValue) ? null : $surveyorValue[0];
        }

        return $inputs;
    }
}
