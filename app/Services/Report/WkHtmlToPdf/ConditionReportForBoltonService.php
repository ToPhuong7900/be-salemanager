<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Email\Email;
use Tfcloud\Lib\File\FileCommon;
use Tfcloud\Jobs\SendReportEmail;
use Tfcloud\Models\Address;
use Tfcloud\Models\AuditAction;
use Tfcloud\Models\Building;
use Tfcloud\Models\CondLocationType;
use Tfcloud\Models\Condsurvey;
use Tfcloud\Models\CondsurveyStatus;
use Tfcloud\Models\GenTable;
use Tfcloud\Models\Idwork;
use Tfcloud\Models\IdworkElement;
use Tfcloud\Models\IdworkPriority;
use Tfcloud\Models\IdworkStatusType;
use Tfcloud\Models\Site;
use Tfcloud\Models\UserAccess;
use Tfcloud\Services\AuditService;
use Tfcloud\Services\Condition\ConditionService;
use Tfcloud\Services\Condition\IdworkService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Property\BuildingService;
use Tfcloud\Services\Property\SiteService;
use Tfcloud\Services\Report\WkHtmlToPdf\WkHtmlToPdfPrintService;
use Tfcloud\Services\Report\WkHtmlToPdf\WkHtmlToPdfReportBaseService;
use Exception;
use ZendPdf\Font;

class ConditionReportForBoltonService extends WkHtmlToPdfReportBaseService
{
    private $photoAvailable = false;
    protected $conditionService;
    public $temporaryFiles = array();

    public function __construct(PermissionService $permissionService, ConditionService $conditionService)
    {
        $this->permissionService = $permissionService;
        $this->conditionService = $conditionService;
    }

    public function runReport(
        $condSurveyId,
        $inputs = [],
        $email = false,
        $fileDir = false,
        $timeLimit = 900
    ) {
        if (!\Tfcloud\Lib\Common::invokedFromCommandLine()) {
            set_time_limit($timeLimit);
        }
        $arrPdfMerge = [];
        $bHighPriorityOnly = $inputs['pdfReportType'] == 'highPriority' ? true : false;
        $bOrderByElement = $inputs['pdfReportOrder'] == 'element' ? true : false;
        $cond = $this->getCond($condSurveyId);

        $buildingDesc = $cond->building ? $cond->building->building_desc : null;

        $cond['surveyor_text'] = Common::concatFields([$cond['surveyor'], $cond['surveyororg']]);
        $cond['site_text'] = Common::concatFields([$cond['site_code'], $buildingDesc]);
        $cond['weather'] = array_get($cond, 'gen_weather_condition_desc', null);

        $siteInfo = $this->getSiteInfo($cond['site_id']);
        if ($siteInfo) {
            $cond['sitePhoto'] = $this->getSitePhotoPath($siteInfo);
            $cond['address'] = $this->getAddressInfo($siteInfo['address_id']);
            $siteInfo = $this->getSiteBuildingCount($cond['site_id']);
            $cond['total_buildings'] = $siteInfo['total_buildings'];
            $siteInfo = $this->getSiteRoomCount($cond['site_id']);
            $cond['total_rooms'] = $siteInfo['total_rooms'];
        } else {
            $cond['total_buildings'] = '';
            $cond['total_rooms'] = '';
            $cond['address'] = [];
        }
        $priorities = $this->getIdworkPriorityInfo($bHighPriorityOnly);
        $cond['priorities'] = $priorities;
        $cond = $this->printTotalSummary($cond, $condSurveyId, '', $priorities, $bHighPriorityOnly);

        $template = 'reports.wkhtmltopdf.condition.cndCliBol01.content';

        $dataArr = [
            'conditionData' => $cond,
            'bOrderByElement' => $bOrderByElement,
            'photoAvailable' => $this->photoAvailable
        ];
        $options = [
            'orientation' => 'landscape',
            'image-dpi' => 300,

        ];
        $report = new WkHtmlToPdfPrintService(
            $this->permissionService,
            $cond,
            $cond->condsurvey_code . "_conditionSummary",
            $options
        );
        if ($bHighPriorityOnly) {
            $report->setTitle(sprintf(
                \Lang::get('text.pdf_report.title_cond_survey_high_priority'),
                $cond->condsurvey_code
            ));
        } else {
            $report->setTitle(sprintf(\Lang::get('text.pdf_report.title_cond_survey'), $cond->condsurvey_code));
        }
        $repPdfFile = $report->generatePdfContent($template, $dataArr, [
            'custom-footer' => true,
            'restrict-width-report-logo-by-ratio' => false,
            'italics-style-footer' => false,
        ]);
        array_push($arrPdfMerge, $repPdfFile);
        // Print Property Condition Summary & Buildings
        $cond['buildings'] = [];
        $pdfs = $this->printPropertyConditionSummary($report, $cond, $condSurveyId, $bHighPriorityOnly);
        $arrPdfMerge = array_merge($arrPdfMerge, $pdfs);
        // Print IdWork
        $cond['idworkrecords'] = [];
        $arrPdfs = $this->printIdwork($report, $cond, $condSurveyId, $bHighPriorityOnly, $bOrderByElement);
        $arrPdfMerge = array_merge($arrPdfMerge, $arrPdfs);

        $report->setReportFileName($cond->condsurvey_code);
        $report->generatePdfFile();

        $finalRepPdfFile = $report->repPdfFile;
        $fileName = trim($cond->condsurvey_code);
        $this->mergerPdf($arrPdfMerge, $finalRepPdfFile, [
            'cssPaging' => ['fontSize' => 8, 'font' => Font::FONT_HELVETICA]
        ]);

        if (file_exists($finalRepPdfFile)) {
            $auditService = new AuditService();
            $auditService->addAuditEntry($cond, AuditAction::ACT_PRINT);
        }
        if (($email) || $fileDir) {
            if ($fileDir) {
                $pdfFile = $fileDir . '/' . $fileName . ".pdf";
                copy($finalRepPdfFile, $pdfFile);
            }
            if ($email) {
                $user = \Auth::user();

                $subject = sprintf(\Lang::get('text.pdf_report.subject_cond_survey'), $cond->condsurvey_code);
                $emailData = array(
                    'mailTo'        => $user->email_reply_to,
                    'displayName'   => $user->siteGroup->email_return_address,
                    'repTitle'      => $cond->condsurvey_code,
                    'subject'       => $subject,
                );
                return $this->sendEmail($finalRepPdfFile, $emailData, $cond);
            } else {
                return true;
            }
        } else {
            return $finalRepPdfFile;
        }
    }

    private function getSitePhotoPath(Site $site, $useDefaultPhoto = true)
    {
        $siteId = $site->site_id;
        $siteImagePath = $this->getPhotoStoragePathway($site->site_group_id, $siteId, 'site');
        $splitString = explode("site/", $siteImagePath);
        $sitePhotoPath = false;
        if ($site->photo) {
            $firstHalf = $splitString[0];
            $sitePhotoPath = $firstHalf . "site/" . $siteId . "/" . $site->photo;
            if (!file_exists($sitePhotoPath)) {
                $sitePhotoPath = $this->fallBackImagePath($useDefaultPhoto);
            } else {
                list($width, $height) = getimagesize($sitePhotoPath);
                $newHeight = 250;
                $newWidth = $newHeight * $width / $height;
                $tmpPhotoFile = tempnam(Common::getTempFolder(), 'rlg');
                $thumb = imagecreatetruecolor($newWidth, $newHeight);
                $source = imagecreatefromjpeg($sitePhotoPath);
                imagecopyresized($thumb, $source, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
                imagejpeg($thumb, $tmpPhotoFile, 100);
                return $tmpPhotoFile;
            }
        }
        return $sitePhotoPath;
    }

    private function getIdworkPriorityInfo($highPriority = false)
    {
        $res = [];
        $query = IdworkPriority::userSiteGroup()
            ->select(['idwork_priority_id', 'ipi_desc'])
            ->orderBy('idwork_priority_code', 'ASC');
        if ($highPriority) {
            $query->where('high_priority', CommonConstant::DATABASE_VALUE_YES);
        }
        $priorities = $query->get()->toArray();
        foreach ($priorities as $record) {
            $res[$record['idwork_priority_id']] = [
                'ipi_desc' => $record['ipi_desc'],
                'total' => 0.00
            ];
        }
        return $res;
    }

    public function printTotalSummary(&$conditionData, $condId, $siteId, $priorities, $bHighPriorityOnly = false)
    {
        $conditionData['totalSummary'] =
            $this->idworkElementsPriorityTotals($priorities, $condId, $siteId, $bHighPriorityOnly);
        $conditionData['bHighPriorityOnly'] = $bHighPriorityOnly;

        return $conditionData;
    }

    public function printPropertyConditionSummary($report, &$conditionData, $condId, $bHighPriorityOnly = false)
    {
        $arrPdfMerge = [];
        $conditionData['buildings'] = $this->getAllCondBuildingsInfo($condId, $bHighPriorityOnly);
        $conditionData['total_buildings'] = count($conditionData['buildings']);
        if ($conditionData['total_buildings'] > 0) {
            // Add summary page of buildings.
            $template = 'reports.wkhtmltopdf.condition.cndCliBol01.cndSurveyBuildingSummary';
            $dataArr = [
                'conditionData' => $conditionData
            ];
            $options = [
                'orientation' => 'portrait',
                'image-dpi' => 300,
            ];
            $report->setReportFileName($conditionData['condsurvey_code'] . "_propertySummary");
            $report->setOptions($options);
            $report->generatePdfFile();
            $repPdfFile = $report->generatePdfContent($template, $dataArr, [
                'custom-footer' => true,
                'restrict-width-report-logo-by-ratio' => false,
                'italics-style-footer' => false]);
            array_push($arrPdfMerge, $repPdfFile);
            // Add separate page for each building.
            $count = 1;
            foreach ($conditionData['buildings'] as $building) {
                $report->setReportFileName($building['building_code'] . "_" . $count);
                $report->generatePdfFile();
                $pdf = $this->printBuilding($report, $condId, $building, $bHighPriorityOnly, $conditionData);
                array_push($arrPdfMerge, $pdf);
                $count++;
            }
        }
        return $arrPdfMerge;
    }

    public function printIdwork($report, &$conditionData, $condId, $bHighPriorityOnly = false, $bOrderByElement = true)
    {
        $arrPdfs = [];
        if ($bOrderByElement) {
            $orderIdWorkBy = "idwork_element_id";
            $template = 'reports.wkhtmltopdf.condition.cndCliBol01.cndSurveyIdworkPic';
        } else {
            $orderIdWorkBy = "building_id";
            $template = 'reports.wkhtmltopdf.condition.cndCliBol01.cndSurveyIdwork';
        }

        $conditionData['idworkrecords']       = $this->formatIdworkByElements(
            $this->getIdwork($condId, $bHighPriorityOnly, $bOrderByElement),
            $orderIdWorkBy
        );
        if ($bOrderByElement) {
            $conditionData['idworkrecords'] = $this->groupIdWorkByElementsWithSub($conditionData['idworkrecords']);
        }
        $conditionData['total_idworkrecords'] = count($conditionData['idworkrecords']);
        $idWorkElements = IdworkElement::userSiteGroup()->orderBy('idwork_element_code')
            ->active()->get()->keyBy('idwork_element_id');

        $dataArr = [
            'conditionData' => $conditionData,
            'bOrderByElement' => $bOrderByElement,
            'photoAvailable' => $this->photoAvailable,
            'idWorkElements' => $idWorkElements
        ];
        $options = [
            'orientation' => 'landscape',
            'print-media-type' => true,
            'image-dpi' => 300,
        ];
        $report->setOptions($options);
        $report->setReportFileName($conditionData['condsurvey_code'] . "_IW");
        $report->generatePdfFile();

        $repPdfFile = $report->generatePdfContent($template, $dataArr, [
            'custom-footer' => true,
            'restrict-width-report-logo-by-ratio' => false,
            'italics-style-footer' => false
        ]);

        array_push($arrPdfs, $repPdfFile);

        $this->removeTemporaryFiles();

        return $arrPdfs;
    }

    private function groupIdWorkByElementsWithSub($idworkRecords)
    {
        if (!empty($idworkRecords)) {
            foreach ($idworkRecords as $key => $records) {
                $group = [];
                if (!empty($records['idwork_records'])) {
                    foreach ($records['idwork_records'] as $idwork) {
                        $subElement = Common::concatFields(
                            [$idwork['idwork_subelement_code'],
                                $idwork['idwork_subelement_desc']]
                        );
                        $group[$subElement][] = $idwork;
                    }
                    unset($idworkRecords[$key]['idwork_records']);
                }
                $idworkRecords[$key]['sub_elements'] = $group;
            }
        }
        return $idworkRecords;
    }

    protected function setPagingCustom(&$page, $key, $totalPage, $paramX, $paramY, $customY2 = null)
    {
        $y = 35;
        if ($page->getWidth() > $page->getHeight()) {
            $x = 750;
            $page->drawText("Page " . ($key + 1) . " / $totalPage", $x, $y, 'UTF-8');
        } else {
            $x = 500;
            $page->drawText("Page " . ($key + 1) . " / $totalPage", $x, $y, 'UTF-8');
        }
    }

    private function getCond($condId)
    {
        $query = Condsurvey::userSiteGroup()
            ->leftJoin('site', 'condsurvey.site_id', '=', 'site.site_id')
            ->leftJoin('building', 'condsurvey.condsurvey_building_id', '=', 'building.building_id')
            ->leftJoin('contact', 'condsurvey.surveyor', '=', 'contact.contact_id')
            ->leftJoin(
                'gen_userdef_group',
                'condsurvey.gen_userdef_group_id',
                '=',
                'gen_userdef_group.gen_userdef_group_id'
            )
            ->leftJoin('gen_userdef_label', function ($join) {
                $join->on('gen_userdef_label.site_group_id', '=', 'condsurvey.site_group_id')
                    ->on('gen_userdef_label.gen_table_id', '=', \DB::raw(GenTable::CONDITON));
            })
            ->leftJoin('score', 'condsurvey.score_id', '=', 'score.score_id')
            ->leftJoin(
                'condsurvey_status',
                'condsurvey.condsurvey_status_id',
                '=',
                'condsurvey_status.condsurvey_status_id'
            )
            ->leftJoin(
                'gen_weather_condition',
                'condsurvey.gen_weather_condition_id',
                '=',
                'gen_weather_condition.gen_weather_condition_id'
            )
            ->where('condsurvey.condsurvey_id', $condId)
            ->select([
                'condsurvey.condsurvey_id',
                'condsurvey.condsurvey_code',
                'condsurvey.condsurvey_desc',
                'condsurvey.site_id',
                'site.site_code',
                'site.site_desc',
                'building.building_id',
                'building.building_code',
                'condsurvey.condsurvey_building_id',
                'condsurvey.condsurvey_building_block_id',
                \DB::raw("DATE_FORMAT(NOW(), '%e') AS day_now"),
                \DB::raw("DATE_FORMAT(NOW(), '%c') AS month_now"),
                \DB::raw("DATE_FORMAT(NOW(), '%Y') AS year_now"),
                \DB::raw("DATE_FORMAT(condsurvey.survey_date, '%d/%m/%Y') AS survey_date"),
                'condsurvey.condsurvey_status_id',
                'condsurvey_status.condsurvey_status_code',
                'condsurvey.score_id',
                'score.score_code',
                'score.score_desc',
                'condsurvey.score_comment',
                'condsurvey.gen_summary',
                'condsurvey.mech_summary',
                'condsurvey.elec_summary',
                'contact.contact_name AS surveyor',
                'contact.organisation AS surveyororg',
                'contact.contact_id AS surveyor_id',
                'gen_weather_condition.gen_weather_condition_code',
                'gen_weather_condition.gen_weather_condition_desc',
                \DB::raw('gen_userdef_group.*'),
                \DB::raw('gen_userdef_label.*')
            ]);

        return $query->first();
    }

    private function idworkElementsPriorityTotals(
        array $priorities,
        $conditionId = false,
        $siteId = false,
        $highPriority = false
    ) {
        $totalsData = [];

        $idworkElements = IdworkElement::userSiteGroup()
            ->select(['idwork_element_id', 'idwork_element_code', 'idwork_element_desc'])
            ->get()->toArray();

        foreach ($idworkElements as $record) {
            $elementId = $record['idwork_element_id'];
            $totalsData[$elementId]['idwork_element_code'] = $record['idwork_element_code'];
            $totalsData[$elementId]['idwork_element_desc'] = $record['idwork_element_desc'];
            $totalsData[$elementId]['idwork_element_text'] =
                Common::concatFields([$record['idwork_element_code'], $record['idwork_element_desc']]);
            $totalsData[$elementId]['priorities'] = $priorities;
        }

        $query = Idwork::join('condsurvey', 'condsurvey.condsurvey_id', '=', 'idwork.condsurvey_id')
            ->join('idwork_status', 'idwork_status.idwork_status_id', '=', 'idwork.idwork_status_id')
            ->leftJoin('idwork_element', 'idwork_element.idwork_element_id', '=', 'idwork.element_id')
            ->leftJoin('idwork_priority', 'idwork_priority.idwork_priority_id', '=', 'idwork.idwork_priority_id')
            ->where('condsurvey.site_group_id', \Auth::user()->site_group_id)
            ->select([
                'idwork.element_id',
                'idwork.total_cost',
                \DB::raw("IFNULL(idwork.idwork_priority_id, 0) AS idwork_priority_id")
            ]);

        if ($conditionId) {
            // Just for the one particular condition survey
            $query->where('idwork.condsurvey_id', $conditionId)
                ->where('idwork_status.idwork_status_type_id', '!=', IdworkStatusType::SUPERSEDED);
        } else {
            // For all completed condition surveys for this site
            $query->where('condsurvey.site_id', $siteId)
                ->where('condsurvey.condsurvey_status_id', CondsurveyStatus::COMPLETE);
        }

        if ($highPriority) {
            $highPriorityList = $this->getIdworkPriorityIdList();
            if (!is_array($highPriorityList) || count($highPriorityList) == 0) {
                $highPriorityList = [0];
            }
            $query->whereIn('idwork.idwork_priority_id', $highPriorityList);
        }

        $idworks = $query->get()->toArray();
        foreach ($idworks as $record) {
            if ($record['idwork_priority_id'] == 0) {
                continue;
            }
            $curTotal = $totalsData[$record['element_id']]
            ['priorities'][$record['idwork_priority_id']]
            ['total'];
            $totalsData[$record['element_id']]
            ['priorities'][$record['idwork_priority_id']]
            ['total'] = $curTotal + $record['total_cost'];
        }

        array_multisort($totalsData, SORT_ASC);

        return $totalsData;
    }

    private function getSiteInfo($siteId)
    {
        // Fetch site text, total buildings and rooms
        $query = Site::where('site.site_id', $siteId)
            ->leftJoin('building', 'building.site_id', '=', 'site.site_id')
            ->leftJoin('room', 'room.building_id', '=', 'building.building_id')
            ->select([
                'site.site_group_id',
                'site.site_id',
                'site.photo',
                'site.site_code',
                'site.site_desc',
                'site.address_id',
                \DB::raw("COUNT(building.building_id) AS 'total_buildings'"),
                \DB::raw("COUNT(room.room_id) AS 'total_rooms'")
            ]);

        return $query->first();
    }

    private function getAddressInfo($addId)
    {
        $address = Address::find($addId);
        $res = [];
        if ($address) {
            $res = $address->getAddressAsArray();
        }
        return $res;
    }

    private function getSiteBuildingCount($siteId)
    {
        // Fetch site text, total buildings and rooms
        $query = Building::where('building.site_id', $siteId)
            ->where('building.active', '=', 'Y')
            ->select([
                \DB::raw("COUNT(building.building_id) AS 'total_buildings'"),
            ]);

        return $query->first();
    }

    private function getSiteRoomCount($siteId)
    {
        // Fetch site text, total buildings and rooms
        $query = Building::where('building.site_id', $siteId)
            ->where('building.active', '=', 'Y')
            ->where('room.active', '=', 'Y')
            ->leftJoin('room', 'room.building_id', '=', 'building.building_id')
            ->select([
                \DB::raw("COUNT(room.room_id) AS 'total_rooms'")
            ]);

        return $query->first();
    }

    private function getAllCondBuildingsInfo($condId, $bHighPriorityOnly = false)
    {
        $highPriorityList = '';
        if ($bHighPriorityOnly) {
            $highPriorityList = $this->getIdworkPriorityIdList();
            if (!is_array($highPriorityList) || count($highPriorityList) == 0) {
                $highPriorityList = [0];
            }
            $highPriorityList = implode(',', $highPriorityList);
        }

        /*
        . " AND idwork_status_id IN"
                        . " ('" . IdworkStatus::DRAFT . "'"
                        . ", '" . IdworkStatus::PLAN . "'"
                        . ", '" . IdworkStatus::WIP . "')"
         *
         */
        $subQuery =
            "(SELECT"
            . " building_id"
            . ", COUNT(idwork_id) AS total_idworks"
            . ", SUM(total_cost) as total_cost"
            . " FROM idwork"
            . " WHERE condsurvey_id = {$condId}"
            . ($highPriorityList ? " AND idwork.idwork_priority_id IN ($highPriorityList)" : '')
            . " AND idwork.building_id IS NOT NULL"
            . " GROUP BY building_id"
            . ") AS buildingTotals";

        $query = \Tfcloud\Models\CondLocationScore::where('condsurvey_id', $condId)
            ->where('cond_location_type_id', '=', CondLocationType::TYPE_BUILDING)
            ->join('building', 'cond_location_score.location_record_id', '=', 'building.building_id')
            ->leftJoin('building_type', 'building_type.building_type_id', '=', 'building.building_type_id')
            ->leftJoin('building_usage', 'building_usage.building_usage_id', '=', 'building.building_usage_id')
            ->leftJoin('address', 'address.address_id', '=', 'building.address_id')
            ->leftJoin('address_street', 'address_street.address_street_id', '=', 'address.address_street_id')
            ->leftJoin('listed_type', 'listed_type.listed_type_id', '=', 'building.listed_type_id')
            ->leftJoin('ward', 'ward.ward_id', '=', 'building.ward_id')
            ->leftJoin('score', 'cond_location_score.score_id', '=', 'score.score_id')
            ->leftJoin(\DB::raw($subQuery), function ($join) {
                $join->on('buildingTotals.building_id', '=', 'cond_location_score.location_record_id');
            })
            ->orderBy('building.building_code', 'ASC')
            ->select([
                'building.building_id',
                'building.building_code',
                'building.building_desc',
                'building.year_built',
                'building.gea',
                'building.gia',
                'building.nia',
                'building.photo',
                'building_type.building_type_code',
                'building_type.building_type_desc',
                'building_usage.building_usage_code',
                'building_usage.building_usage_desc',
                'address.first_addr_obj',
                'address.second_addr_obj',
                'address_street.street',
                'address_street.locality',
                'address_street.town',
                'address.postcode',
                'listed_type.listed_type_code',
                'listed_type.listed_type_desc',
                'ward.ward_code',
                'ward.ward_desc',
                'buildingTotals.total_idworks',
                'buildingTotals.total_cost',
                'score.score_code',
                'score.score_desc'
            ]);

        return $query->get();
    }

    public function printBuilding($report, $condId, &$building, $bHighPriorityOnly, &$conditionData)
    {
        // Empty elements array so none get carried over by mistake.
        $building['elements'] = [];
        $building['elements'] = $this->getCondBuildingInfo($condId, $building['building_id']);

        $building['total_elements'] = count($building['elements']);
        $building['building_text'] = Common::concatFields(
            [
                $building['building_code'],
                $building['building_desc']
            ]
        );
        $building['building_type'] =
            Common::concatFields([$building['building_type_code'], $building['building_type_desc']]);
        $building['listed_type'] =
            Common::concatFields([$building['listed_type_code'], $building['listed_type_desc']]);
        $building['building_usage'] =
            Common::concatFields([$building['building_usage_code'], $building['building_usage_desc']]);
        $building['ward'] = Common::concatFields([$building['ward_code'], $building['ward_desc']]);
        $building['address_first'] = $building['first_addr_obj'];
        $building['address_second'] = $building['second_addr_obj'];
        $building['address_street'] = $building['street'];
        $building['address_locality'] = $building['locality'];
        $building['address_town'] = $building['town'];
        $building['address_postcode'] = $building['postcode'];
        $building['building_year'] = $building['year_built'];
        $building['building_gea'] = $building['gea'];
        $building['building_gia'] = $building['gia'];
        $building['building_nia'] = $building['nia'];
        $building['building_score_code'] = $building['score_code'];
        $building['building_score_desc'] = $building['score_desc'];

        $photoPath = $this->getBuildingPhotoPath($building['building_id']);
        if ($photoPath && file_exists($photoPath)) {
            $photoPath = FileCommon::copyFileToTmpFolder($photoPath);
            $building['building_image_path'] = $photoPath;

            $newWidth = "auto";
            $newHeight = "auto";

            // Fixed maximum size for image: width 100mm or height 60mm.
            $size = getimagesize($photoPath);
            // If width > height then
            if ($size[0] > $size[1]) {
                $newWidth = "90mm";
                $x = 0;
            } else {
                $newHeight = "60mm";
                $x = 0;
            }
            $building['building_image_width'] = $newWidth;
            $building['building_image_height'] = $newHeight;
        }
        $template = 'reports.wkhtmltopdf.condition.cndCliBol01.cndSurveyBuilding';
        $dataArr = [
            'building' => $building,
            'conditionData' => $conditionData
        ];
        $options = [
            'orientation' => 'portrait',
            'image-dpi' => 300
        ];
        $report->setOptions($options);
        return $report->generatePdfContent($template, $dataArr, [
            'custom-footer' => true,
            'restrict-width-report-logo-by-ratio' => false,
            'italics-style-footer' => false
        ]);
    }

    private function formatIdworkByElements($idwork, $orderIdWorkBy = "idwork_element_id")
    {
        $usableSizeBytes = 5000;                   // Under 5K is still acceptable quality for this report.

        $result = $idwork->toArray();
        $this->photoAvailable = false;
        $idworkRecords = [];

        if (count($result) > 0) {
            $idworkId = null;
            foreach ($result as $record) {
                $idworkRecords[$record[$orderIdWorkBy]]['element_text'] =
                    Common::concatFields([$record['idwork_element_code'], $record['idwork_element_desc']]);
                $idworkRecords[$record[$orderIdWorkBy]]['building_text'] =
                    Common::concatFields([$record['building_code'], $record['building_desc']]);

                $photoPath = $this->getIdworkPhotoPath($record['idwork_id']);

                /*Begin Displaying Multiple IdWork Images*/
                $imageTempCollection = [];
                if (\Tfcloud\Lib\Common::valueYorNtoBoolean(\Auth::user()->siteGroup->cnd_idwork_multiple_images)) {
                    $condIdworkService = new IdworkService($this->permissionService);

                    //look in the images/ directory. The image outside the image/ folder is the main image.
                    $imgPath = $condIdworkService->getImagesStoragePathway($record['idwork_id']);

                    $imageCollection = [];

                    if ($photoPath && file_exists($photoPath)) {
                        $imageCollection[] = $photoPath;
                    }

                    if ($imgPath && file_exists($imgPath)) {
                        $normalIdworkImages = array_diff(scandir($imgPath), array('.', '..'));

                        foreach ($normalIdworkImages as $img) {
                            $imageCollection[] = $imgPath . $img; //Get the file names.
                        }
                    }

                    foreach ($imageCollection as $tempPath) {
                        $this->photoAvailable = true;

                        // Do we need to scale photo.
                        $tempPath = str_replace("/", "\\", $tempPath);
                        $size = filesize($tempPath);
                        if ($size > $usableSizeBytes) {
                            $tempPath = $this->scalePhoto($tempPath);
                        } else {
                            // Photo small enough to use as is.

                            $tempPath = FileCommon::copyFileToTmpFolder($tempPath);
                            $tempPath = str_replace(
                                DIRECTORY_SEPARATOR,
                                DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR,
                                $tempPath
                            );
                        }
                        $imageTempCollection[] = $tempPath;
                        $this->temporaryFiles[] = $tempPath;
                    }
                }
                /*End Displaying Multiple IdWork Images*/

                if ($photoPath && file_exists($photoPath)) {
                    $this->photoAvailable = true;
                    $photoPath = $this->scalePhoto($photoPath);
                    $this->temporaryFiles[] = $photoPath;
                } else {
                    $photoPath = "";
                }

                $record['idwork_photo_path'] = $photoPath;

                if ($record['idwork_id'] !== $idworkId) {
                    if ($record['idwork_item_remedy_code']) {
                        $remedyCount = 1;
                    } else {
                        $remedyCount = 0;
                    }

                    $idworkRecords[$record[$orderIdWorkBy]]['idwork_records'][$record['idwork_id']] = [
                        'idwork_all_images' => $imageTempCollection,
                        'idwork_id' => $record['idwork_id'],
                        'idwork_code' => $record['idwork_code'],
                        'building_code' => $record['building_code'],
                        'building_desc' => $record['building_desc'],
                        'building_block_code' => $record['building_block_code'],
                        'building_block_desc' => $record['building_block_desc'],
                        'room_number' => $record['room_number'],
                        'room_desc' => $record['room_desc'],
                        'condsurvey_id' => $record['condsurvey_id'],
                        'condsurvey_code' => $record['condsurvey_code'],
                        'defect' => $record['defect'],
                        'remedy' => $record['remedy'],
                        'remedy_quantity' => $record['remedy_quantity'],
                        'remedy_unit_of_measure_code' => $record['remedy_unit_of_measure_code'],
                        'cost' => $record['total_cost'],
                        'idwork_photo_path' => $record['idwork_photo_path'],
                        'idwork_element_id' => $record['idwork_element_id'],
                        'idwork_element_code' => $record['idwork_element_code'],
                        'idwork_element_desc' => $record['idwork_element_desc'],
                        'idwork_subelement_code' => $record['idwork_subelement_code'],
                        'idwork_subelement_desc' => $record['idwork_subelement_desc'],
                        'idwork_item_code' => $record['idwork_item_code'],
                        'idwork_item_desc' => $record['idwork_item_desc'],
                        'idworkitem_desc' => $record['idworkitem_desc'],
                        'idwork_condition_code' => $record['idwork_condition_code'],
                        'idwork_priority_code' => $record['idwork_priority_code'],
                        'idwork_priority_desc' => $record['ipi_desc'],
                        'cyclical_maintenance' => $record['cyclical_maintenance'],
                        'cycle_length' => $record['cycle_length'],
                        'probability' => $record['probability'],
                        'risk' => $record['risk'],
                        'severity' => $record['severity'],
                        'cnd_prelim_percent' => $record['cnd_prelim_percent'],
                        'cnd_prelim_cost' => $record['cnd_prelim_cost'],
                        'cnd_contin_percent' => $record['cnd_contin_percent'],
                        'cnd_contin_cost' => $record['cnd_contin_cost'],
                        'cnd_fee_percent' => $record['cnd_fee_percent'],
                        'cnd_fee_cost' => $record['cnd_fee_cost'],
                        'total_cost' => $record['total_cost'],
                        'location' => $record['location'],
                        'comment' => $record['comment'],
                        'idwork_status_code' => $record['idwork_status_code'],
                        'idwork_status_desc' => $record['idwork_status_desc'],
                        'idwork_category_code' => $record['idwork_category_code'],
                        'idwork_category_desc' => $record['idwork_category_desc'],
                        'remedy_count' => $remedyCount,
                        'remedies'   => []
                    ];
                }

                $remedy = [
                    'idwork_item_remedy_code' => $record['idwork_item_remedy_code'],
                    'idwork_item_remedy_desc' => $record['idwork_item_remedy_desc'],
                    'unit_of_measure_code'    => $record['unit_of_measure_code'],
                    'unit_of_measure_desc'    => $record['unit_of_measure_desc'],
                    'remedy_cost'             => $record['remedy_cost'],
                    'quantity'                => $record['quantity'],
                    'total_cost'              => $record['total_cost']
                ];

                array_push(
                    $idworkRecords[$record[$orderIdWorkBy]]['idwork_records'][$record['idwork_id']]['remedies'],
                    $remedy
                );

                $idworkId = $record['idwork_id'];
            }
        }

        return $idworkRecords;
    }

    private function scalePhoto($tempPath, $newHeight = 600)
    {
        // Scale image and copy to temp folder for use in report.
        $success = true;

        $size = getimagesize($tempPath);
        if ($size) {
            list($width, $height) = $size;
            // Get new sizes.
            // e.g. 5184 x 3888 => 800 x 600.  Approx 20 million pixels down to 480,000 pixels.
            // The report will scale to 150 pixels in height but if we scale down here to that level, the images look
            // too pixelated.  Compromise seems to be around 800 x 600 and then let the report scale from
            // 800 x 600 to 200 x 150.
            if ($newHeight < $height) {
                $scaleWidth = $height / $newHeight;
                $newWidth = (int)($width / $scaleWidth);
            } else {
                $newHeight = $height;
                $newWidth = $width;
            }

            $thumb = imagecreatetruecolor($newWidth, $newHeight);
            if (!$thumb) {
                $success = false;
            }
            $imageToCompress = $tempPath;
            if ($success) {
                $source = imagecreatefromjpeg($imageToCompress);
                if (!$source) {
                    $success = false;
                }
            }
            $success = imagecopyresized(
                $thumb,
                $source,
                0,
                0,
                0,
                0,
                $newWidth,
                $newHeight,
                $width,
                $height
            );
            if ($success) {
                // Output
                $tmpFileName = tempnam(Common::getTempFolder(), 'rlg');
                $success = imagejpeg($thumb, $tmpFileName, 75);
                $tempPath = $tmpFileName;
                $tempPath = str_replace(
                    DIRECTORY_SEPARATOR,
                    DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR,
                    $tempPath
                );
            }
        }
        return $tempPath;
    }

    /**
     * Removes all temporary files
     */
    public function removeTemporaryFiles()
    {
        foreach ($this->temporaryFiles as $file) {
            $this->unlink($file);
        }
    }

    /**
     * Wrapper for the "unlink" function
     *
     * @param string $filename
     *
     * @return boolean
     */
    protected function unlink($filename)
    {
        return file_exists($filename) ? unlink($filename) : false;
    }

    private function getCondBuildingInfo($condId, $buildingId)
    {
        $query = \Tfcloud\Models\CondLocationScore::where('cond_location_score.location_record_id', $buildingId)
            ->where('cond_location_type_id', '=', CondLocationType::TYPE_BUILDING)
            ->leftJoin(
                'cond_location_element_score',
                'cond_location_element_score.cond_location_score_id',
                '=',
                'cond_location_score.cond_location_score_id'
            )
            ->leftJoin('score', 'cond_location_element_score.score_id', '=', 'score.score_id')
            ->leftJoin(
                'idwork_element',
                'idwork_element.idwork_element_id',
                '=',
                'cond_location_element_score.idwork_element_id'
            )
            ->where('cond_location_score.condsurvey_id', $condId)
            ->orderBy('idwork_element.idwork_element_code', 'ASC')
            ->select([
                'idwork_element.idwork_element_code',
                'idwork_element.idwork_element_desc',
                'score.score_code AS element_score',
                'cond_location_element_score.comment'
            ]);

        return $query->get()->toArray();
    }

    private function getIdworkPhotoPath($idworkId)
    {
        $idworkService = new IdworkService($this->permissionService);
        return $idworkService->get($idworkId, false)->photoPath(false);
    }

    private function getIdworkPriorityIdList()
    {
        return IdworkPriority::userSiteGroup()
            ->where('high_priority', CommonConstant::DATABASE_VALUE_YES)
            ->pluck('idwork_priority_id')->toArray();
    }

    private function getIdwork($condId, $highPriority = false, $bOrderByElement = true)
    {
        /** Believe this data should be coming from the Idwork service layer, but getAllByParent is paginating on the
         *  service layer and time doesn't allow me to refactor this at the moment */
        $query = Idwork::where('idwork.condsurvey_id', $condId)
            ->join('condsurvey', 'condsurvey.condsurvey_id', '=', 'idwork.condsurvey_id')
            ->join('site', 'condsurvey.site_id', '=', 'site.site_id')
            ->join('idwork_element', 'idwork.element_id', '=', 'idwork_element.idwork_element_id')
            ->join('idwork_subelement', 'idwork.sub_element_id', '=', 'idwork_subelement.idwork_subelement_id')
            ->join('idwork_item', 'idwork.item_id', '=', 'idwork_item.idwork_item_id')
            ->join('idwork_status', 'idwork_status.idwork_status_id', '=', 'idwork.idwork_status_id')
            ->leftJoin('idwork_category', 'idwork_category.idwork_category_id', '=', 'idwork.idwork_category_id')
            ->leftJoin('idwork_condition', 'idwork.idwork_condition_id', '=', 'idwork_condition.idwork_condition_id')
            ->leftJoin('idwork_priority', 'idwork.idwork_priority_id', '=', 'idwork_priority.idwork_priority_id')
            ->leftJoin('building_block', 'building_block.building_block_id', '=', 'idwork.building_block_id')
            ->leftJoin('building', 'building.building_id', '=', 'idwork.building_id')
            ->leftJoin('room', 'room.room_id', '=', 'idwork.room_id')
            ->leftJoin('idwork_remedy', 'idwork_remedy.idwork_id', '=', 'idwork.idwork_id')
            ->leftJoin(
                'idwork_item_remedy',
                'idwork_item_remedy.idwork_item_remedy_id',
                '=',
                'idwork_remedy.idwork_item_remedy_id'
            )
            ->leftJoin(
                'unit_of_measure',
                'unit_of_measure.unit_of_measure_id',
                '=',
                'idwork_item_remedy.unit_of_measure_id'
            )
            ->leftJoin(
                'unit_of_measure as remedy_unit_of_measure',
                'remedy_unit_of_measure.unit_of_measure_id',
                '=',
                'idwork.remedy_unit_of_measure_id'
            )
            ->where('condsurvey.site_group_id', \Auth::user()->site_group_id)
            ->where('idwork_status.idwork_status_type_id', '!=', IdworkStatusType::SUPERSEDED);

        if ($bOrderByElement) {
            $query->orderBy('idwork_element.idwork_element_code', 'ASC')
                ->orderBy('idwork_subelement.idwork_subelement_code', 'ASC')
                ->orderBy('idwork_item.idwork_item_code', 'ASC')
                ->orderBy('building.building_code', 'ASC')
                ->orderBy('room.room_number', 'ASC');
        } else {
            $query->orderBy('building.building_code', 'ASC')
                ->orderBy('room.room_number', 'ASC')
                ->orderBy('idwork_element.idwork_element_code', 'ASC')
                ->orderBy('idwork_subelement.idwork_subelement_code', 'ASC')
                ->orderBy('idwork_item.idwork_item_code', 'ASC');
        }

        $query->orderBy('idwork_priority.idwork_priority_code', 'ASC');

        $query->select([
            'idwork.idwork_id',
            'idwork.idwork_code',
            'building.building_id',
            'building.building_code',
            'building.building_desc',
            'building_block.building_block_code',
            'building_block.building_block_desc',
            'room.room_number',
            'room.room_desc',
            'condsurvey.condsurvey_id',
            'condsurvey.condsurvey_code',
            'idwork.defect',
            'idwork.remedy',
            'idwork.remedy_quantity',
            'remedy_unit_of_measure.unit_of_measure_code AS remedy_unit_of_measure_code',
            'idwork.total_cost AS cost',
            'idwork.photo AS idwork_photo',
            'idwork_element.idwork_element_id',
            'idwork_element.idwork_element_code',
            'idwork_element.idwork_element_desc',
            'idwork_subelement.idwork_subelement_code',
            'idwork_subelement.idwork_subelement_desc',
            \DB::Raw(
                "CONCAT_WS('-', idwork_subelement.idwork_subelement_code, idwork_subelement.idwork_subelement_desc)" .
                " AS idwork_subelement_code_desc"
            ),
            'idwork_item.idwork_item_code AS idwork_item_code',
            'idwork_item.idworkitem_desc AS idwork_item_desc',
            'idwork_element.idwork_element_desc',
            'idwork_subelement.idwork_subelement_desc',
            'idwork_item.idworkitem_desc',
            'idwork_condition.idwork_condition_code',
            'idwork_priority.idwork_priority_code',
            'idwork_priority.ipi_desc',
            'idwork.cyclical_maintenance',
            'idwork.cycle_length',
            'idwork.probability',
            'idwork.risk',
            'idwork.severity',
            'idwork.cnd_prelim_percent',
            'idwork.cnd_prelim_cost',
            'idwork.cnd_contin_percent',
            'idwork.cnd_contin_cost',
            'idwork.cnd_fee_percent',
            'idwork.cnd_fee_cost',
            'idwork.total_cost',
            'idwork.location',
            'idwork.comment',
            'idwork_status.idwork_status_code',
            'idwork_status.idwork_status_desc',
            'idwork_category.idwork_category_code',
            'idwork_category.idwork_category_desc',
            'idwork_remedy.idwork_remedy_id',
            'idwork_item_remedy.idwork_item_remedy_code',
            'idwork_item_remedy.idwork_item_remedy_desc',
            'unit_of_measure.unit_of_measure_code',
            'unit_of_measure.unit_of_measure_desc',
            'idwork_remedy.remedy_cost',
            'idwork_remedy.quantity'
        ]);

        if ($highPriority) {
            $highPriorityList = $this->getIdworkPriorityIdList();
            if (!is_array($highPriorityList) || count($highPriorityList) == 0) {
                $highPriorityList = [0];
            }
            $query->whereIn('idwork.idwork_priority_id', $highPriorityList);
        }

        return $query->get();
    }

    private function getBuildingPhotoPath($buildingId)
    {
        $siteService = new SiteService($this->permissionService);
        $buildingService = new BuildingService($this->permissionService, $siteService);
        return $buildingService->get($buildingId, UserAccess::SITE_ACCESS_READONLY, false)->photoPath();
    }

    private function sendEmail($reportFile, $data, $cond)
    {
        if (realpath($reportFile)) {
            $user = \Auth::User();
            $emailReplyTo = $data['mailTo'];
            $emailReturnAddress = $user->siteGroup->email_return_address;
            $displayName = $data['displayName'];

            if ($emailReturnAddress == '') {
                $sErrMsg = "The Site Group 'Default Address' has not been defined in Email Settings.";
                $sErrMsg .= " Please contact your system administrator.";
                return $sErrMsg;
            }

            if ($emailReplyTo == '') {
                $sErrMsg = "The 'Email Reply To' has not been defined in your user details.";
                $sErrMsg .= " Please edit your user details to set your email address.";
                return $sErrMsg;
            }

            $emailData = [
                'subject'   => $data['subject'],
                'from'      => $emailReturnAddress,
                'to'        => $emailReplyTo,
                'toDisplayName' => $displayName,
                'attachment'    => $reportFile
            ];

            $email = new Email($emailData);

            SendReportEmail::dispatch($email, \Auth::User(), $data, [], $cond);

            \DB::transaction(function () use ($email, $cond) {
                $auditService = new AuditService();
                $msg = sprintf(
                    'Email queued from %s to %s, subject %s',
                    $email->from,
                    $email->to,
                    $email->subject
                );
                $auditService->auditMessages([$msg]);
                $auditService->addAuditEntry($cond, AuditAction::ACT_EMAIL);
            });
        } else {
            return $sErrMsg = "Cannot send email of the report " . $data['repTitle'] . ".";
        }
        return true;
    }
}
