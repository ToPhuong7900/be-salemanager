<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use Tfcloud\Controllers\Admin\General\SiteGroupsController;
use Tfcloud\Lib\Common;
use Tfcloud\Models\Report;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\BoxFilterQuery\IReportFilterQuery;
use Tfcloud\Services\Admin\General\SiteGroupsService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\PermitToWork\PermitToWorkService;

class Ptw02ReportService extends WkHtmlToPdfReportBaseService
{
    private $sgController;
    private $sgService;
    private $ptwService;
    public $filterText = '';
    private $reportBoxFilterLoader = null;

    public function __construct(PermissionService $permissionService, Report $report = null)
    {
        parent::__construct($permissionService);
        $this->sgService = new SiteGroupsService($permissionService);
        $this->sgController = new SiteGroupsController($permissionService, $this->sgService);
        $this->ptwService = new PermitToWorkService($permissionService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function runReport($repOutPutPdfFile, $inputs, $repId)
    {
        $siteGroupId = \Auth::User()->site_group_id;
        $photoPath = $this->sgController->getPhotoPath($siteGroupId); //get logo for outputting at top of report

        $reportFilterQuery = null;

        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $data = $reportFilterQuery->filterAll($this->ptwService->all())->get();
        } else {
            $data = $this->getReportData($inputs);
        }

        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);
        $content = \View::make(
            'reports.wkhtmltopdf.permitToWork.ptw02.content',
            [
                'data'             => $data,
                'photoPath'        => $photoPath

            ]
        )->render();

        $contentPath = $generatedPath . "/content.html";

        file_put_contents($contentPath, $content, LOCK_EX);

        $whereCodes = [];               // The codes which have been used in the filter
        $whereTexts = [];               // Plain text about filtering applied
        $orCodes = [];                  // csv string of OR codes
        $bFilterDetail = false;

        if ($reportFilterQuery instanceof IReportFilterQuery) {
            $this->filterText = $reportFilterQuery->getFilterDetailText();
        } else {
            $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
            $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $this->filterText);
        }

        $options = [
            'header-spacing' => 4,
            'footer-spacing' => 4,
            'orientation' => 'portrait',
        ];

        if (array_key_exists('singleOnly', $inputs)) {
            $repOutPutPdfFile .= '.pdf';
            $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $options);
            return $repOutPutPdfFile;
        } else {
            $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $options);
            return true;
        }
    }

    private function getReportData($inputs)
    {
        $filterData = $inputs;
        foreach ($filterData as $key => $value) {
            if ((empty($value) && !is_numeric($value)) || $value == 'null') {
                unset($filterData[$key]);
            }
        }
        return $this->ptwService->getAll($filterData)->get();
    }

    private function generateFilterData($filterData, &$whereCodes, &$whereTexts, &$orCodes)
    {
        if ($val = array_get($filterData, 'code')) {
            array_push($whereTexts, sprintf("Code contains '%s'", $val));
        }

        if ($val = array_get($filterData, 'validFromDateFrom')) {
            array_push($whereTexts, "Valid From From $val");
        }
        if ($val = array_get($filterData, 'validFromDateTo')) {
            array_push($whereTexts, "Valid From To $val");
        }

        if ($val = array_get($filterData, 'validToDateFrom')) {
            array_push($whereTexts, "Valid To From $val");
        }
        if ($val = array_get($filterData, 'validToDateTo')) {
            array_push($whereTexts, "Valid To To $val");
        }

        if ($val = array_get($filterData, 'status')) {
            array_push(
                $whereCodes,
                ['ptw_status', 'ptw_status_id', 'ptw_status_desc', $val, "Status"]
            );
        }

        if ($val = array_get($filterData, 'type')) {
            array_push(
                $whereCodes,
                ['ptw_type', 'ptw_type_id', 'ptw_type_desc', $val, "Type"]
            );
        }

        if ($val = array_get($filterData, 'requester')) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Requester']);
        }

        if ($val = array_get($filterData, 'supplier')) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Supplier']);
        }


        if ($val = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, 'Site Code']);
        }

        if ($val = array_get($filterData, 'building_id')) {
            array_push($whereCodes, ['building', 'building_id', 'building_code', $val, 'Building Code']);
        }

        if ($val = array_get($filterData, 'room_id')) {
            array_push(
                $whereCodes,
                ['room', 'room_id', 'room_number', $val, 'Room Number']
            );
        }

        if ($val = array_get($filterData, 'plant_group_id')) {
            array_push($whereCodes, ['plant_group', 'plant_group_id', 'plant_group_code', $val, "Plant Group"]);

            if ($val = array_get($filterData, 'plant_subgroup_id')) {
                array_push(
                    $whereCodes,
                    ['plant_subgroup', 'plant_subgroup_id', 'plant_subgroup_code', $val, "Plant Sub Group"]
                );

                if ($val = array_get($filterData, 'plant_type_id')) {
                    array_push(
                        $whereCodes,
                        ['plant_type', 'plant_type_id', 'plant_type_code', $val, "Plant Type"]
                    );
                }
            }
        }

        if ($val = array_get($filterData, 'plant_code')) {
            array_push($whereTexts, sprintf("Plant Code contains '%s'", $val));
        }
    }
}
