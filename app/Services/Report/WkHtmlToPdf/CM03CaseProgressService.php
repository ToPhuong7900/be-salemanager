<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use Tfcloud\Models\CaseManagement\CmType;
use Tfcloud\Models\GenTable;

class CM03CaseProgressService extends BaseCMx03CaseProgressService
{
    protected $cmType = CmType::CM_TYPE_CASEMGMT;
    protected $gentTable = GenTable::CM_CASE;
}
