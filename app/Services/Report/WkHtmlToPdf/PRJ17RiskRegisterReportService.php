<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Tfcloud\Lib\BoxFilterQuery\IReportFilterQuery;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Filters\Project\ProjectFilter;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Traits\EmailTrait;
use Tfcloud\Models\GenRagStatus;
use Tfcloud\Models\GenTable;
use Tfcloud\Models\Project;
use Tfcloud\Models\ProjectRiskItem;
use Tfcloud\Models\ProjectStatusType;
use Tfcloud\Models\Report;
use Tfcloud\Models\SiteGroup;
use Tfcloud\Services\PermissionService;
use Tfcloud\services\Project\ProjectAccessService;
use Tfcloud\Services\Project\ProjectRiskService;
use Tfcloud\Services\Project\ProjectService;

class PRJ17RiskRegisterReportService extends WkHtmlToPdfReportBaseService
{
    use EmailTrait;

    protected $projectRiskService;
    protected $projectService;
    protected $projectAccessService;
    public $filterText = '';
    private $pageIndex = 1;
    private $generatedPath = null;
    private $reportBoxFilterLoader = null;

    public function __construct(PermissionService $permissionService, Report $report = null)
    {
        parent::__construct($permissionService);
        $this->permissionService = $permissionService;
        $this->projectRiskService = new ProjectRiskService($permissionService);
        $this->projectService = new ProjectService($permissionService);
        $this->projectAccessService = new ProjectAccessService($permissionService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function runReport($repPdfFile, $inputs, $repId, &$errMsg = [], $options = [])
    {
        $reportFilterQuery = null;
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $projects = $reportFilterQuery->filterAll($this->all())->get();
        } else {
            $this->formatInputData($inputs);
            $projects = $this->filterAll($this->all(), $inputs)->get();
        }

        $arrPdf = [];
        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);
        if (array_get($options, 'print-report')) {
            $repPdfFile = $this->generateReportOutPutPath(array_get($options, 'project_code'));
        }

        if ($projects->count()) {
            foreach ($projects as $project) {
                $reportPath = $generatedPath . '/' . $project->project_id . '.pdf';

                $siteGroup = SiteGroup::get();

                $thresholdData = $this->getRiskThresholdData($siteGroup);
                $projectRisks = $this->getProjectRiskData($project->project_id);
                $projectRisks = $this->calculateRatingOfProjectRisk($projectRisks, $siteGroup);
                $projectRisks = $this->calculateResolvingOfProjectRisk($projectRisks);
                $riskCount = $this->countNumberOfRisks($project->project_id);
                $startDate = $this->getStartDate($project);
                $completedDate = $this->getCompletedDate($project);
                $address = $this->getAddress($project->site);

                $content = \View::make(
                    'reports.wkhtmltopdf.project.prj17.content',
                    [
                        'project' => $project,
                        'projectRisks' => $projectRisks,
                        'thresholdData' => $thresholdData,
                        'riskCount' => $riskCount,
                        'startDate' => $startDate,
                        'completedDate' => $completedDate,
                        'address' => $address
                    ]
                )->render();
                $contentPath = $generatedPath . "/content_{$project->project_id}.html";
                file_put_contents($contentPath, $content, LOCK_EX);

                $header = \View::make(
                    'reports.wkhtmltopdf.project.prj17.header',
                    [
                        'project' => $project,
                        'logo' => $this->getHeaderLogo()
                    ]
                )->render();
                $headerPath = $generatedPath . "/header_{$project->project_id}.html";
                file_put_contents($headerPath, $header, LOCK_EX);
                $footer = \View::make('reports.wkhtmltopdf.project.prj17.footer')->render();
                $footerPath = $generatedPath . "/footer_{$project->project_id}.html";
                file_put_contents($footerPath, $footer, LOCK_EX);
                $outputOptions = [
                    'header-html' => $headerPath,
                    'orientation' => 'landscape',
                    'margin-top'  => 30,
                    'margin-bottom' => 10,
                    'margin-left' => 3,
                    'margin-right' => 3,
                ];

                $this->generateSingleHtmlToPdf($contentPath, $reportPath, $outputOptions);

                array_push($arrPdf, $reportPath);
            }
        }

        $cssPaging = [
            'fontSize' => 8,
            'x' => 660,
            'y' => 8
        ];
        $this->mergerPdf($arrPdf, $repPdfFile, ['cssPaging' => $cssPaging]);

        if (array_get($inputs, 'send_email', false)) {
            $user = \Auth::user();
            $fileName = array_get($options, 'project_code');
            $emailData = array(
                'mailTo'        => $user->email_reply_to,
                'displayName'   => $user->siteGroup->email_return_address,
                'repTitle'      => $fileName,
                'subject'       => $fileName . ' Report',
            );
            return $this->sendEmail($repPdfFile, $emailData, $project);
        }

        $whereCodes = [];               // The codes which have been used in the filter
        $whereTexts = [];               // Plain text about filtering applied
        $orCodes = [];                  // csv string of OR codes
        $bFilterDetail = false;

        if ($reportFilterQuery instanceof IReportFilterQuery) {
            $this->filterText = $reportFilterQuery->getFilterDetailText();
        } else {
            $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
            $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $this->filterText);
        }

        if (array_get($options, 'print-report')) {
            return $repPdfFile;
        }
        return true;
    }

    protected function setPagingCustom(&$page, $key, $totalPage, $x, $y, $customY2 = null)
    {
        $user = \Auth::user()->display_name;
        $now = Carbon::now()->format('d/m/Y H:i:s');
        $left = 20;
        $center = 400;
        $page->drawText("PRJ17: Risk Register", $left, $y, 'UTF-8');
        $page->drawText("Page " . ($key + 1) . " of $totalPage", $center, $y, 'UTF-8');
        $page->drawText("Created by $user at $now", $x, $y, 'UTF-8');
    }

    private function generateFilterData($filterData, &$whereCodes, &$whereTexts, &$orCodes)
    {
        if ($val = array_get($filterData, 'code')) {
            array_push($whereTexts, "Code contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'title')) {
            array_push($whereTexts, "Title contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'description')) {
            array_push($whereTexts, "Description contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'clientRef')) {
            array_push($whereTexts, "Client ref contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'owner')) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Owner']);
        }

        if ($val = array_get($filterData, 'projectPortfolio')) {
            array_push(
                $whereCodes,
                ['project_portfolio', 'project_portfolio_id', 'project_portfolio_code', $val, 'Project Portfolio']
            );
        }

        if ($val = array_get($filterData, 'projectPriority')) {
            array_push(
                $whereCodes,
                ['project_priority', 'project_priority_id', 'project_priority_code', $val, 'Project Priority']
            );
        }

        if ($val = array_get($filterData, 'projectProgramme')) {
            array_push(
                $whereCodes,
                ['project_programme', 'project_programme_id', 'project_programme_code', $val, 'Project Programme']
            );
        }

        if ($val = array_get($filterData, 'project_status_id')) {
            array_push(
                $whereCodes,
                ['project_status', 'project_status_id', 'project_status_code', $val, 'Project Status']
            );
        }

        if ($val = array_get($filterData, 'establishment')) {
            array_push(
                $whereCodes,
                ['establishment', 'establishment_id', 'establishment_code', $val, 'Establishment']
            );
        }

        if ($val = array_get($filterData, 'project')) {
            array_push(
                $whereCodes,
                ['project', 'project_id', 'project_code', $val, 'Project']
            );
        }

        $projectStatusTypes = [];
        if (Common::iset($filterData['draft'])) {
            $projectStatusTypes[] = ProjectStatusType::DRAFT;
        }
        if (Common::iset($filterData['active'])) {
            $projectStatusTypes[] = ProjectStatusType::ACTIVE;
        }
        if (Common::iset($filterData['complete'])) {
            $projectStatusTypes[] = ProjectStatusType::COMPLETE;
        }
        if (Common::iset($filterData['rejected'])) {
            $projectStatusTypes[] = ProjectStatusType::REJECTED;
        }
        if (count($projectStatusTypes)) {
            array_push($orCodes, [
                'project_status_type',
                'project_status_type_id',
                'project_status_type_code',
                implode(',', $projectStatusTypes),
                "Project Status Type"
            ]);
        }

        $ragStatus = [];
        if (Common::iset($filterData['overallRagRed'])) {
            $ragStatus[] = GenRagStatus::RED;
        }
        if (Common::iset($filterData['overallRagAmber'])) {
            $ragStatus[] = GenRagStatus::AMBER;
        }
        if (Common::iset($filterData['overallRagGreen'])) {
            $ragStatus[] = GenRagStatus::GREEN;
        }
        if (count($ragStatus)) {
            array_push($orCodes, [
                'gen_rag_status',
                'gen_rag_status_id',
                'gen_rag_status_code',
                implode(',', $ragStatus),
                "Overall RAG Status"
            ]);
        }

        $ragStatus = [];
        if (Common::iset($filterData['qualityRagRed'])) {
            $ragStatus[] = GenRagStatus::RED;
        }
        if (Common::iset($filterData['qualityRagAmber'])) {
            $ragStatus[] = GenRagStatus::AMBER;
        }
        if (Common::iset($filterData['qualityRagGreen'])) {
            $ragStatus[] = GenRagStatus::GREEN;
        }
        if (count($ragStatus)) {
            array_push($orCodes, [
                'gen_rag_status',
                'gen_rag_status_id',
                'gen_rag_status_code',
                implode(',', $ragStatus),
                "Quality RAG Status"
            ]);
        }

        $ragStatus = [];
        if (Common::iset($filterData['costRagRed'])) {
            $ragStatus[] = GenRagStatus::RED;
        }
        if (Common::iset($filterData['costRagAmber'])) {
            $ragStatus[] = GenRagStatus::AMBER;
        }
        if (Common::iset($filterData['costRagGreen'])) {
            $ragStatus[] = GenRagStatus::GREEN;
        }
        if (count($ragStatus)) {
            array_push($orCodes, [
                'gen_rag_status',
                'gen_rag_status_id',
                'gen_rag_status_code',
                implode(',', $ragStatus),
                "Cost RAG Status"
            ]);
        }

        $ragStatus = [];
        if (Common::iset($filterData['timeRagRed'])) {
            $ragStatus[] = GenRagStatus::RED;
        }
        if (Common::iset($filterData['timeRagAmber'])) {
            $ragStatus[] = GenRagStatus::AMBER;
        }
        if (Common::iset($filterData['timeRagGreen'])) {
            $ragStatus[] = GenRagStatus::GREEN;
        }
        if (count($ragStatus)) {
            array_push($orCodes, [
                'gen_rag_status',
                'gen_rag_status_id',
                'gen_rag_status_code',
                implode(',', $ragStatus),
                "Time RAG Status"
            ]);
        }
        $this->reAddUserDefinesQuery(GenTable::PROJECT, $filterData, $whereCodes, $whereTexts);
    }

    private function getStartDate($project)
    {
        if (!empty($project->actual_plan_commit_to_prepare_proposals)) {
            $startDate = $project->actual_plan_commit_to_prepare_proposals;
        } elseif (!empty($project->current_plan_commit_to_prepare_proposals)) {
            $startDate = $project->current_plan_commit_to_prepare_proposals;
        } else {
            $startDate = $project->planned_commit_to_prepare_proposals;
        }
        return $startDate;
    }

    private function getCompletedDate($project)
    {
        if (!empty($project->actual_final_completion)) {
            $completedDate = $project->actual_final_completion;
        } elseif (!empty($project->current_final_completion)) {
            $completedDate = $project->current_final_completion;
        } else {
            $completedDate = $project->planned_final_completion;
        }
        return $completedDate;
    }

    private function getRiskThresholdData($siteGroup)
    {
        $residualRiskText = "Residual Risks <= {$siteGroup->proj_residual_risk_threshold_max}";
        if ($siteGroup->proj_low_risk_threshold_min == $siteGroup->proj_low_risk_threshold_max) {
            $lowRiskText = "Low Risks {$siteGroup->proj_low_risk_threshold_min}";
        } else {
            $lowRiskText =
                "Low Risks {$siteGroup->proj_low_risk_threshold_min} - {$siteGroup->proj_low_risk_threshold_max}";
        }
        if ($siteGroup->proj_medium_risk_threshold_min == $siteGroup->proj_medium_risk_threshold_max) {
            $mediumRiskText = "Medium Risks {$siteGroup->proj_medium_risk_threshold_min}";
        } else {
            $mediumRiskText =
                "Medium Risks {$siteGroup->proj_medium_risk_threshold_min} "
                . "- {$siteGroup->proj_medium_risk_threshold_max}";
        }
        $highRiskText = "High Risks > {$siteGroup->proj_medium_risk_threshold_max}";
        $data = [
            'residualRiskText' => $residualRiskText,
            'lowRiskText' => $lowRiskText,
            'mediumRiskText' => $mediumRiskText,
            'highRiskText' => $highRiskText
        ];
        return $data;
    }

    private function getProjectRiskData($projectId)
    {
        $query = ProjectRiskItem::userSiteGroup()
            ->leftJoin(
                \DB::raw('user raised_by_user'),
                'raised_by_user.id',
                '=',
                'project_risk_item.raised_by_user_id'
            )
            ->leftJoin(
                'project_risk_type',
                'project_risk_type.project_risk_type_id',
                '=',
                'project_risk_item.project_risk_type_id'
            )
            ->leftJoin(
                'project_risk_category',
                'project_risk_category.project_risk_category_id',
                '=',
                'project_risk_item.project_risk_category_id'
            )
            ->leftJoin(
                'project_risk_likelihood',
                'project_risk_likelihood.project_risk_likelihood_id',
                '=',
                'project_risk_item.project_risk_likelihood_id'
            )
            ->leftJoin(
                'project_risk_severity',
                'project_risk_severity.project_risk_severity_id',
                '=',
                'project_risk_item.project_risk_severity_id'
            )
            ->leftJoin(
                \DB::raw('user owner_user'),
                'owner_user.id',
                '=',
                'project_risk_item.owner_user_id'
            )
            ->select([
                'project_risk_item.*',
                'project_risk_category.project_risk_category_code',
                'project_risk_category.project_risk_category_desc',
                'project_risk_likelihood.project_risk_likelihood_value',
                'project_risk_severity.project_risk_severity_value',
                \DB::raw("IFNULL(project_risk_type.project_risk_type_code, 'Other') AS project_risk_type_code"),
                'project_risk_type.project_risk_type_desc',
                \DB::raw('raised_by_user.display_name AS raised_by_name'),
                \DB::raw('owner_user.display_name AS owner_name'),
            ])
            ->where('project_risk_item.project_id', $projectId)
            ->orderByRaw("(project_risk_type.project_risk_type_code != 'Other') desc")
            ->orderBy("project_risk_type.project_risk_type_code")
            ->orderBy("project_risk_item.project_risk_item_code");


        $projectRisks = $query->get()->groupBy('project_risk_type_code');

        return $projectRisks;
    }

    private function calculateRatingOfProjectRisk($projectRisks, $siteGroup)
    {
        $projectRisks->each(function ($risks) use ($siteGroup) {
            $risks->each(function ($risk) use ($siteGroup) {
                $degreeOfRisk = $risk->degree_of_risk;
                if ($degreeOfRisk) {
                    if (
                        ($siteGroup->proj_low_risk_threshold_min <= $degreeOfRisk)
                        && ($degreeOfRisk <= $siteGroup->proj_low_risk_threshold_max)
                    ) {
                        $risk->cssRatedClass = 'bg-green';
                    } elseif (
                        ($siteGroup->proj_medium_risk_threshold_min <= $degreeOfRisk)
                        && ($degreeOfRisk <= $siteGroup->proj_medium_risk_threshold_max)
                    ) {
                        $risk->cssRatedClass = 'bg-orange';
                    } elseif ($siteGroup->proj_medium_risk_threshold_max < $degreeOfRisk) {
                        $risk->cssRatedClass = 'bg-red';
                    }
                } else {
                    $risk->cssClass = 'bg-white';
                }
            });
        });

        return $projectRisks;
    }

    private function calculateResolvingOfProjectRisk($projectRisks)
    {
        $projectRisks->each(function ($risks) {
            $risks->each(function ($risk) {
                if ($risk->resolved == CommonConstant::DATABASE_VALUE_NO) {
                    $risk->cssResolvedClass = 'status-open';
                    $risk->resolvedStatus = 'Open';
                } else {
                    $risk->cssResolvedClass = 'status-resolved';
                    $risk->resolvedStatus = 'Resolved';
                }
            });
        });

        return $projectRisks;
    }

    private function countNumberOfRisks($projectId)
    {
        $siteGroup = SiteGroup::get();
        $residualMax = $siteGroup->proj_residual_risk_threshold_max;
        $lowRiskMin = $siteGroup->proj_low_risk_threshold_min;
        $lowRiskMax = $siteGroup->proj_low_risk_threshold_max;
        $medRiskMin = $siteGroup->proj_medium_risk_threshold_min;
        $medRiskMax = $siteGroup->proj_medium_risk_threshold_max;
        $riskCount = ProjectRiskItem::userSiteGroup()
            ->select([
                \DB::raw("COUNT(IF(degree_of_risk <= $residualMax,1,NULL)) 'residual'"),
                \DB::raw("COUNT(IF(degree_of_risk >= $lowRiskMin AND degree_of_risk <= $lowRiskMax,1,NULL)) 'low'"),
                \DB::raw("COUNT(IF(degree_of_risk >= $medRiskMin AND degree_of_risk <= $medRiskMax,1,NULL)) 'med'"),
                \DB::raw("COUNT(IF(degree_of_risk > $medRiskMax,1,NULL)) 'high'"),
                \DB::raw("COUNT(project_risk_item_id) 'total'")
            ])
            ->where('project_id', $projectId)
            ->first();
        return $riskCount;
    }

    private function getAddress($site)
    {
        $siteAddress = $site->address->getAddressAsArray();
        $addressArray = array_filter([
            $siteAddress['second_addr_obj'],
            $siteAddress['first_addr_obj'],
            $siteAddress['street'],
            $siteAddress['locality'],
            $siteAddress['town'],
        ]);
        $postCode = array_get($siteAddress, 'postcode');
        $region = array_get($siteAddress, 'region');
        $result = [
            'addressString' => implode("<br>", $addressArray),
            'postCode' => $postCode,
            'region' => $region
        ];

        return $result;
    }

    public function filterAll($query, $inputs, $view = false, $restrictSort = false)
    {
        $projectTable = !$view ? 'project' : $view;
        $siteTable = !$view ? 'site' : $view;

        $filter = new ProjectFilter($inputs);

        if (!is_null($filter->code)) {
            $query->where($projectTable . '.project_code', 'like', '%' . $filter->code . '%');
        }

        if (!is_null($filter->title)) {
            $query->where($projectTable . '.project_title', 'like', '%' . $filter->title . '%');
        }

        if (!is_null($filter->description)) {
            $query->where($projectTable . '.project_desc', 'like', '%' . $filter->description . '%');
        }

        if (!is_null($filter->clientRef)) {
            $query->where($projectTable . '.client_ref', 'like', '%' . $filter->clientRef . '%');
        }
        if (!is_null($filter->project_status_id)) {
            $query->where("{$projectTable}.project_status_id", $filter->project_status_id);
        }

        if (!is_null($filter->search)) {
            $query->where(function ($subQuery) use ($filter, $projectTable) {
                $subQuery->where($projectTable . '.project_code', 'like', '%' . $filter->search . '%')
                    ->orWhere($projectTable . '.project_desc', 'like', '%' . $filter->search  . '%');
            });
        }

        if (!is_null($filter->owner)) {
            $query->where($projectTable . '.owner_user_id', $filter->owner);
        }

        if (!is_null($filter->site_id)) {
            $query->where($projectTable . '.site_id', $filter->site_id);
        }

        if (!is_null($filter->building_id)) {
            $query->where($projectTable . '.building_id', $filter->building_id);
        }

        if (!is_null($filter->projectPortfolio)) {
            $query->where($projectTable . '.project_portfolio_id', $filter->projectPortfolio);
        }

        if (!is_null($filter->projectPriority)) {
            $query->where($projectTable . '.project_priority_id', $filter->projectPriority);
        }

        if (!is_null($filter->projectProgramme)) {
            $query->where($projectTable . '.project_programme_id', $filter->projectProgramme);
        }

        if (!is_null($filter->projectType) && !$view) {
            $query->where($projectTable . '.project_type_id', $filter->projectType);
        }

        if (!is_null($filter->project)) {
            $query->where($projectTable . '.project_id', $filter->project);
        }

        if (!is_null($filter->establishment)) {
            $query->where($siteTable . '.establishment_id', $filter->establishment);
        }


        if (!$view && !is_null($filter->excludeCaseId)) {
            $query->leftJoin(
                'cm_case_link',
                'cm_case_link.project_id',
                '=',
                "$projectTable.project_id"
            )
                ->whereNull('cm_case_link.cm_case_id');
        }

        $options = [];
        if (!is_null($filter->draft)) {
            array_push($options, ProjectStatusType::DRAFT);
        }
        if (!is_null($filter->active)) {
            array_push($options, ProjectStatusType::ACTIVE);
        }
        if (!is_null($filter->complete)) {
            array_push($options, ProjectStatusType::COMPLETE);
        }
        if (!is_null($filter->rejected)) {
            array_push($options, ProjectStatusType::REJECTED);
        }

        if (count($options)) {
            if ($view) {
                $query->whereIn("{$projectTable}.project_status_type_id", $options);
            } else {
                $query->whereIn('project_status.project_status_type_id', $options);
            }
        }

        $options = [];
        if (!is_null($filter->overallRagRed)) {
            array_push($options, GenRagStatus::RED);
        }
        if (!is_null($filter->overallRagAmber)) {
            array_push($options, GenRagStatus::AMBER);
        }
        if (!is_null($filter->overallRagGreen)) {
            array_push($options, GenRagStatus::GREEN);
        }

        if (is_array($options) && count($options)) {
            $query->whereIn($projectTable . '.gen_rag_status_id', $options);
        }

        $options = [];
        if (!is_null($filter->qualityRagRed)) {
            array_push($options, GenRagStatus::RED);
        }
        if (!is_null($filter->qualityRagAmber)) {
            array_push($options, GenRagStatus::AMBER);
        }
        if (!is_null($filter->qualityRagGreen)) {
            array_push($options, GenRagStatus::GREEN);
        }

        if (is_array($options) && count($options)) {
            $query->whereIn($projectTable . '.quality_gen_rag_status_id', $options);
        }

        $options = [];
        if (!is_null($filter->costRagRed)) {
            array_push($options, GenRagStatus::RED);
        }
        if (!is_null($filter->costRagAmber)) {
            array_push($options, GenRagStatus::AMBER);
        }
        if (!is_null($filter->costRagGreen)) {
            array_push($options, GenRagStatus::GREEN);
        }

        if (is_array($options) && count($options)) {
            $query->whereIn($projectTable . '.cost_gen_rag_status_id', $options);
        }

        $options = [];
        if (!is_null($filter->timeRagRed)) {
            array_push($options, GenRagStatus::RED);
        }
        if (!is_null($filter->timeRagAmber)) {
            array_push($options, GenRagStatus::AMBER);
        }
        if (!is_null($filter->timeRagGreen)) {
            array_push($options, GenRagStatus::GREEN);
        }

        if (is_array($options) && count($options)) {
            $query->whereIn($projectTable . '.time_gen_rag_status_id', $options);
        }

        $options = [];
        if (!is_null($filter->safetyRagRed)) {
            array_push($options, GenRagStatus::RED);
        }
        if (!is_null($filter->safetyRagAmber)) {
            array_push($options, GenRagStatus::AMBER);
        }
        if (!is_null($filter->safetyRagGreen)) {
            array_push($options, GenRagStatus::GREEN);
        }

        if (is_array($options) && count($options)) {
            $query->whereIn($projectTable . '.safety_gen_rag_status_id', $options);
        }

        $options = [];
        if (!is_null($filter->relationshipRagRed)) {
            array_push($options, GenRagStatus::RED);
        }
        if (!is_null($filter->relationshipRagAmber)) {
            array_push($options, GenRagStatus::AMBER);
        }
        if (!is_null($filter->relationshipRagGreen)) {
            array_push($options, GenRagStatus::GREEN);
        }

        if (is_array($options) && count($options)) {
            $query->whereIn($projectTable . '.relationship_gen_rag_status_id', $options);
        }

        $query = $this->projectAccessService->projectUserAccess($query, $projectTable);

        $this->filterByUserDefines(GenTable::PROJECT, $query, $filter, $projectTable, $view);

        if (!$view && $restrictSort == false) {
            $query->orderBy($filter->sort, $filter->sortOrder);
        }

        return $query;
    }

    private function all()
    {
        $query = Project::userSiteGroup()
            ->leftjoin('project_stage', 'project.project_stage_id', '=', 'project_stage.project_stage_id')
            ->leftjoin(
                'project_portfolio',
                'project.project_portfolio_id',
                '=',
                'project_portfolio.project_portfolio_id'
            )
            ->leftjoin('project_type', 'project.project_type_id', '=', 'project_type.project_type_id')
            ->join('site', 'site.site_id', '=', 'project.site_id');

        $query = $this->permissionService->listViewSiteAccess($query, "project.site_id", $siteAccess);

        $query->select([
            'project.*',
            'project_portfolio.project_portfolio_code',
            'project_portfolio.project_portfolio_desc',
            'project_type.project_type_code',
            'project_type.project_type_desc',
            \DB::raw('project_status.project_status_code AS project_status_code'),
            'project_stage.project_stage_name',
            'project_stage.current_planned_date',
            'project_stage.project_stage_status_id',
            'site.site_code',
            'site.site_desc',
            \DB::raw($siteAccess)
        ])->join(
            'project_status',
            'project_status.project_status_id',
            '=',
            'project.project_status_id'
        )
        ->orderBy('project.project_code', 'ASC');
        return $query;
    }

    private function formatInputData(&$inputs)
    {
        foreach ($inputs as $key => $value) {
            if ($value === "") {
                unset($inputs[$key]);
            }
        }
    }
}
