<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf\EastAyrshire;

use Auth;
use Tfcloud\Lib\Constant\ReportConstant;
use Tfcloud\Lib\Reporting\ReportCommon;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\WkHtmlToPdf\WkHtmlToPdfReportBaseService;

class EACBCApplicationForPaymentService extends WkHtmlToPdfReportBaseService
{
    public $filterText = '';
    public $permissionService = null;

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->permissionService = $permissionService;
    }


    public function runReportSingleHelpcall($helpcallId, $data)
    {
        // Call report from button not report view.
        //$inputs['helpcall_id'] = $helpcallId;
        //$inputs['singleOnly'] = true;
        $repId = ReportCommon::getSystemReportByCode(
            $this->permissionService,
            ReportConstant::SYSTEM_REPORT_HLP_CLI_CBC01
        )->system_report_id;

        $data['singleOnly'] = true;

        $exportDir = storage_path() . '/reports/' . \Auth::User()->site_group_id;
        $filename = "EAC Application {$data['client_ref']}.pdf";
        $repOutPutPdfFile = $exportDir . DIRECTORY_SEPARATOR . $filename;

        return $this->runReport(
            $repOutPutPdfFile,
            $data,
            $repId
        );
    }


    /**
     * @param $repOutPutPdfFile
     * @param $inputs
     * @param $repId
     * @return bool
     * @throws \Tfcloud\Lib\Exceptions\PermissionException
     */
    public function runReport($repOutPutPdfFile, $inputs, $repId)
    {
        // Create temp file
        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);


        // Page 1 content.
        //$this->setTitle("Application For Payment");
        $content1Path = $generatedPath . "/content1.html";
        $templateCbcAppPayment = 'reports.wkhtmltopdf.helpcall.CBC._cbcAppPayment';
        $contentCbcAppPayment = \View::make(
            $templateCbcAppPayment,
            $inputs
        )
        ->render();
        file_put_contents($content1Path, $contentCbcAppPayment, FILE_APPEND | LOCK_EX);

        // Page 2 content.
        $content2Path = $generatedPath . "/content2.html";
        $templateCbcAppPaymentSummary = 'reports.wkhtmltopdf.helpcall.CBC._cbcAppPaymentSummary';
        $contentCbcAppPaymentSummary = \View::make(
            $templateCbcAppPaymentSummary,
            $inputs
        )
        ->render();
        file_put_contents($content2Path, $contentCbcAppPaymentSummary, FILE_APPEND | LOCK_EX);

        $runDate = date('d/m/Y H:i');
        $footer = \View::make(
            'reports.wkhtmltopdf.helpcall.CBC.footer',
            ['runDate' => $runDate]
        )->render();
        $footerPath = $generatedPath . "/footer.html";
        file_put_contents($footerPath, $footer, LOCK_EX);

        $header = \View::make('reports.wkhtmltopdf.helpcall.CBC.header')->render();
        $headerPath = $generatedPath . "/header.html";
        file_put_contents($headerPath, $header, LOCK_EX);

        $this->generateMultiHtmlToPdf(
            [$content1Path, $content2Path],
            $generatedPath,
            $repOutPutPdfFile,
            [
                'footer-html' => $footerPath,
                'header-html' => $headerPath,
                'header-spacing' => 4,
                'footer-spacing' => 4
            ]
        );

        if (array_key_exists('singleOnly', $inputs)) {
            return $repOutPutPdfFile;
        } else {
            return true;
        }
    }
}
