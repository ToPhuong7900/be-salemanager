<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf\LBHFHousing;

use Tfcloud\Controllers\Admin\General\SiteGroupsController;
use Tfcloud\Lib\Common;
use Tfcloud\Models\CrudAccessLevel;
use Tfcloud\Models\Module;
use Tfcloud\Models\Site;
use Tfcloud\Services\Admin\General\SiteGroupsService;
use Tfcloud\Services\Questionnaire\FiledQstService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\WkHtmlToPdf\WkHtmlToPdfReportBaseService;

class QSTCLILBHFHousing01ReportService extends WkHtmlToPdfReportBaseService
{
    private $sgController;
    private $sgService;
    private $filedQstService;
    public $filterText = '';

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->sgService = new SiteGroupsService($permissionService);
        $this->sgController = new SiteGroupsController($permissionService, $this->sgService);
        $this->filedQstService = new FiledQstService($permissionService);
    }

    public function runReport($repOutPutPdfFile, $inputs, $repId)
    {
        $footerDate = new \DateTime();
        $siteGroupId = \Auth::User()->site_group_id;
        $photoPath = $this->sgController->getPhotoPath($siteGroupId); //get logo for outputting at top of report

        $data = $this->getReportData($inputs);

        $questionnaireData = [];

        foreach ($data as $fireData) {
            $inspectionSupplier = $fireData->inspection_id ?
                    Common::concatFields([$fireData->insp_supp_name, $fireData->insp_supp_org]) : '' ;

            $questionnaireData[$fireData->filed_qst_id]['inspection_supplier'] = $inspectionSupplier;

            $questionnaireData[$fireData->filed_qst_id]['questionnaire_code'] = $fireData->filed_qst_code;

            $site = Site::findOrFail($fireData->site_id);
            $questionnaireData[$fireData->filed_qst_id]['site_desc'] = $fireData->site_desc;
            $questionnaireData[$fireData->filed_qst_id]['site_addr']
                = $site->address ? $site->address->getAddressAsString() : '';
            $questionnaireData[$fireData->filed_qst_id]['author_of_assess'] =
                    Common::concatFields([
                    $fireData->contact_name, $fireData->organisation]);


            $questionnaireData[$fireData->filed_qst_id]['questionnaire_date'] = $fireData->filed_qst_date;

            $questionResponse = $this->getFiledQuestionValue($fireData);

            //For the first page on each questionnaire.
            if ($fireData->section_code == 'S12') {
                if ($fireData->question_code == 'S12-02') {
                    $questionnaireData[$fireData->filed_qst_id]['quality_assured_by'] = $questionResponse;
                }
            }

            if ($fireData->section_code == 'S1') {
                if ($fireData->question_code == 'S1-06') {
                    $questionnaireData[$fireData->filed_qst_id]['validTo'] = $questionResponse;
                }
            }

            if ($fireData->section_code == 'S10') {
                if ($fireData->question_code == 'S10-01') {
                    $questionnaireData[$fireData->filed_qst_id]['likelihood_of_fire'] = $questionResponse;
                }
            }

            if ($fireData->section_code == 'S10') {
                if ($fireData->question_code == 'S10-02') {
                    $questionnaireData[$fireData->filed_qst_id]['risk_rating_building'] = $questionResponse;
                }
            }

            if ($fireData->section_title == 'Executive Summary') {
                $this->setQuestionData($questionnaireData, $fireData, 1, $questionResponse);
            } elseif ($fireData->section_title == 'Building features') {
                $this->setQuestionData($questionnaireData, $fireData, 2, $questionResponse);
            } elseif ($fireData->section_title == 'Inspection information') {
                $this->setQuestionData($questionnaireData, $fireData, 3, $questionResponse);
            } elseif ($fireData->section_title == 'Compartmentation') {
                $this->setQuestionData($questionnaireData, $fireData, 4, $questionResponse);
            } elseif ($fireData->section_title == 'Means of escape') {
                $this->setQuestionData($questionnaireData, $fireData, 5, $questionResponse);
            } elseif ($fireData->section_title == 'Doors') {
                $this->setQuestionData($questionnaireData, $fireData, 6, $questionResponse);
            } elseif ($fireData->section_title == 'Fire hazards') {
                $this->setQuestionData($questionnaireData, $fireData, 7, $questionResponse);
            } elseif ($fireData->section_title == 'Fire detection and emergency lighting') {
                $this->setQuestionData($questionnaireData, $fireData, 8, $questionResponse);
            } elseif ($fireData->section_title == 'Fire safety signage and housekeeping') {
                $this->setQuestionData($questionnaireData, $fireData, 9, $questionResponse);
            } elseif ($fireData->section_title == 'Safety management') {
                $this->setQuestionData($questionnaireData, $fireData, 10, $questionResponse);
            } elseif ($fireData->section_title == 'Building safety risk rating') {
                $this->setQuestionData($questionnaireData, $fireData, 11, $questionResponse);
            } elseif ($fireData->section_title == 'Comments') {
                $this->setQuestionData($questionnaireData, $fireData, 12, $questionResponse);
            } elseif ($fireData->section_title == 'QA section') {
                $this->setQuestionData($questionnaireData, $fireData, 13, $questionResponse);
            }

            //Ksort To allow the sections to appear in the order provided by the setQiestionData method.
            ksort($questionnaireData[$fireData->filed_qst_id]['section']);
        }

        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);

        // Header.
        $header = \View::make(
            'reports.wkhtmltopdf.questionnaire.lbhfHousing.qstCliLBHFHousing01.header',
            [
                'reportLogo' => $this->getHeaderLogo(),
                'title'      => ""
            ]
        )->render();

        $headerPath = $generatedPath . "/header.html";
        file_put_contents($headerPath, $header, LOCK_EX);

        // Footer.
        $footer = \View::make(
            'reports.wkhtmltopdf.questionnaire.lbhfHousing.qstCliLBHFHousing01.footer',
            [
                'footerDate' => $footerDate->format('Y/m/d H:i')
            ]
        )->render();
        $footerPath = $generatedPath . "/footer.html";
        file_put_contents($footerPath, $footer, LOCK_EX);

        $content = \View::make(
            'reports.wkhtmltopdf.questionnaire.lbhfHousing.qstCliLBHFHousing01.content',
            [
                'data'             => $data,
                'questionnaireData' => $questionnaireData,

                'photoPath'        => $photoPath

            ]
        )->render();

        $contentPath = $generatedPath . "/content.html";

        file_put_contents($contentPath, $content, LOCK_EX);

        $whereCodes = [];               // The codes which have been used in the filter
        $whereTexts = [];               // Plain text about filtering applied
        $orCodes = [];                  // csv string of OR codes
        $bFilterDetail = false;

        $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
        $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $this->filterText);

        $options = [
            'header-spacing' => 4,
            'footer-spacing' => 4,
            'orientation' => 'portrait',
            'header-html' => $headerPath,
            'footer-html' => $footerPath,
        ];

        if (array_key_exists('singleOnly', $inputs)) {
            $repOutPutPdfFile .= '.pdf';
            $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $options);
            return $repOutPutPdfFile;
        } else {
            $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $options);
            return true;
        }
    }

    private function getReportData($inputs)
    {
        $filterData = $inputs;
        foreach ($filterData as $key => $value) {
            if ((empty($value) && !is_numeric($value)) || $value == 'null') {
                unset($filterData[$key]);
            }
        }
        return $this->getAll($filterData)->get();
    }

    private function getAll($inputs)
    {
        if (
            !$this->permissionService->hasModuleAccess(
                Module::MODULE_QST_FIRE,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
            )
        ) {
            $msg = "Not met minimum read access for estate module in class" .
                " (" . __CLASS__ . ") method (" . __METHOD__ . "). " .
                " See line (" . __LINE__ . ")";

            throw new PermissionException($msg);
        }

        return $this->filedQstService->filterAll($this->all(), $inputs);
    }

    private function all()
    {
        $query = \Tfcloud\Models\FiledQstQuestion::
            leftjoin(
                'filed_qst_section',
                'filed_qst_section.filed_section_id',
                "=",
                "filed_qst_question.filed_section_id"
            )
            ->leftjoin('filed_qst', 'filed_qst.filed_qst_id', "=", "filed_qst_section.filed_qst_id")
            ->leftjoin('qst_section', 'qst_section.section_id', "=", "filed_qst_section.section_id")
            ->leftjoin('qst_question', 'qst_question.question_id', "=", "filed_qst_question.question_id")
            ->leftjoin('qst_response_type', 'qst_response_type.response_type_id', "=", "qst_question.response_type_id")

            ->leftjoin('qst', 'qst.qst_id', "=", "qst_section.qst_id")
            ->leftJoin('site', 'site.site_id', 'filed_qst.site_id')
            ->leftJoin('contact', 'contact.contact_id', 'filed_qst.contact_id')
            ->leftJoin('inspection', 'inspection.inspection_id', 'filed_qst.inspection_id')
            ->leftJoin('contact as inspection_contact', 'inspection_contact.contact_id', 'inspection.supplier_id')

            //Required for filterall class
            ->leftJoin('qst_status', 'qst_status.qst_status_id', 'filed_qst.qst_status_id')
           // ->leftJoin('user', 'user.id', 'filed_qst.owner_user_id')

            ->where('qst.qst_code', 'FRA PAS79 Jan 2022');

        $query->select([
            'filed_qst.filed_qst_code',
            'filed_qst_question.filed_question_id',
            'filed_qst_question.filed_qst_id',
            'filed_qst_question.filed_question_value',
            'filed_qst_question.filed_question_comment',
            'filed_qst_question.filed_question_priority_value',
            'filed_qst_question.filed_question_hs_value',
            'filed_qst_question.filed_question_compliance_value',
            'filed_qst_section.filed_section_id',
            'filed_qst_section.section_id',
            'qst_section.section_code',
            'qst_section.section_title',
            'qst_question.question_code',
            'qst_question.question_desc',
            'qst_question.question_full_desc',
            'site.site_id',
            'site.site_code',
            'site.site_desc',
            'contact.contact_name',
            'contact.organisation',
            'inspection.inspection_id',
            'inspection_contact.contact_name as insp_supp_name',
            'inspection_contact.organisation as insp_supp_org',
            'qst_response_type.field_type_id',

           // 'user.display_name',
          //  'user.job_title'
          \DB::raw("DATE_FORMAT(filed_qst.filed_qst_date, '%d/%m/%Y') AS filed_qst_date"),
        ]);

        $query->orderBy('qst_question.question_order');

        //return $query->get();
        return $query;
    }


    private function setQuestionData(&$questionnaireData, $fireData, $sectionOrder, $questionResponse)
    {
        if ($fireData->section_title == 'Executive Summary') {
            $questionnaireData[$fireData->filed_qst_id]['section']
            [$sectionOrder]['section_title'] = 'Survey Findings';
        } elseif ($fireData->section_title == 'QA section') {
            $questionnaireData[$fireData->filed_qst_id]
            ['section'][$sectionOrder]['section_title'] = 'Quality assurance';
        } else {
            $questionnaireData[$fireData->filed_qst_id]
            ['section'][$sectionOrder]['section_title'] = $fireData->section_title;
        }

        $questionnaireData[$fireData->filed_qst_id]
            ['section'][$sectionOrder]['questions'][$fireData->filed_question_id][] = $fireData->question_code;
        $questionnaireData[$fireData->filed_qst_id]
            ['section'][$sectionOrder]['questions'][$fireData->filed_question_id][] = $fireData->question_desc;

        $questionnaireData[$fireData->filed_qst_id]
            ['section'][$sectionOrder]['questions'][$fireData->filed_question_id][] = $questionResponse;
    }


    private function getFiledQuestionValue($fireData)
    {
        $responseVal = $fireData->filed_question_value;

        if ($fireData->field_type_id == \Tfcloud\Models\QstResponseFieldType::DATE) {
            if ($responseVal) {
                $responseVal = \Carbon\Carbon::parse($responseVal)->format('d/m/Y');
            }
        } elseif ($fireData->field_type_id == \Tfcloud\Models\QstResponseFieldType::DROPDOWN) {
            if ($responseVal) {
                $respTypeValModel = \Tfcloud\Models\QstResponseTypeValue::find($responseVal);

                if ($respTypeValModel) {
                    $responseVal =  Common::concatFields([
                    $respTypeValModel->value_code, $respTypeValModel->value_desc]);
                }
            }
        } elseif ($fireData->field_type_id == \Tfcloud\Models\QstResponseFieldType::RADIO) {
            if ($responseVal) {
                $responseVal = Common::concatFields([
                    $fireData->filed_question_value, $fireData->filed_question_comment]);
            }
        }

        return $responseVal;
    }



    private function generateFilterData($filterData, &$whereCodes, &$whereTexts, &$orCodes)
    {
    }
}
