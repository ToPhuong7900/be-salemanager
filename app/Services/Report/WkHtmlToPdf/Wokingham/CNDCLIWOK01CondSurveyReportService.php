<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf\Wokingham;

use Tfcloud\Lib\Common;
use Tfcloud\Models\Condsurvey;
use Tfcloud\Services\Condition\ConditionService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\WkHtmlToPdf\WkHtmlToPdfReportBaseService;

class CNDCLIWOK01CondSurveyReportService extends WkHtmlToPdfReportBaseService
{
    private const REPORT_TYPE_SUMMARY = 'summary';
    private const REPORT_TYPE_DETAILED = 'detailed';
    protected $permissionService;
    public $filterText = '';

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->permissionService = $permissionService;
        $this->condSurveyService = new ConditionService($this->permissionService);
    }

    public function runReport($repOutPutPdfFile, $inputs, $repId)
    {
        $inputs['building_id'] = null;
        $inputs['block_id'] = null;
        $condSurveys = $this->condSurveyService->filterAll($this->all(), $inputs);
        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);
        $content = \View::make(
            'reports.wkhtmltopdf.condition.cndCliWok01.content',
            [

            ]
        )->render();
        $contentPath = $generatedPath . "/content.html";
        file_put_contents($contentPath, $content, LOCK_EX);
        $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile);
        return true;
    }

    private function all()
    {
        $query = Condsurvey::select()
        ->leftjoin(
            'condsurvey_status',
            "condsurvey.condsurvey_status_id",
            '=',
            "condsurvey_status.condsurvey_status_id"
        )
        ->leftjoin('site', 'site.site_id', '=', "condsurvey.site_id")
        ->leftjoin('score', "condsurvey.score_id", '=', 'score.score_id')
        ->leftjoin('building', "building.building_id", '=', 'condsurvey.condsurvey_building_id')
        ->leftjoin('building_block', "building_block.building_block_id", '=', 'condsurvey.condsurvey_building_block_id')
        ->leftjoin('prop_zone', "prop_zone.prop_zone_id", '=', 'condsurvey.prop_zone_id')
        ->leftjoin('prop_external', "prop_external.prop_external_id", '=', 'condsurvey.prop_external_id')
        ->leftjoin('contact', 'contact.contact_id', '=', "condsurvey.surveyor")
        ->where("condsurvey.site_group_id", \Auth::user()->site_group_id);

//        $query = $this->permissionService->listViewSiteAccess($query, 'site.site_id');

        return $query;
    }
}
