<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use DB;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\BoxFilterQuery\IReportFilterQuery;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Constant\ReportConstant;
use Tfcloud\Lib\Exceptions\StateException;
use Tfcloud\Models\Address;
use Tfcloud\Models\CondLocationScore;
//use Tfcloud\Models\CondLocationSubelementScore;
use Tfcloud\Models\Condsurvey;
use Tfcloud\Models\CondsurveyStatus;
use Tfcloud\Models\Idwork;
use Tfcloud\Models\IdworkStatusType;
use Tfcloud\Models\Report;
use Tfcloud\Services\PermissionService;

class ConditionReportForFifeService extends WkHtmlToPdfReportBaseService
{
    private $generatedPath = null;
    private $pageIndex = 1;
    public $filterText = '';
    private $reportBoxFilterLoader = null;

    public function __construct(PermissionService $permissionService, Report $report = null)
    {
        $this->permissionService = $permissionService;
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function runReport($repId, $repOutPutPdfFile = null, $inputs = [], $conditionId = null, &$errMsg = [])
    {
        \Log::info("Report start.");
        if (!is_null($conditionId)) {
            $inputs['conditionId'] = $conditionId;
            $conditionObj = Condsurvey::find($conditionId);
            if ($conditionObj instanceof Condsurvey) {
                $reportFileName = ReportConstant::SYSTEM_REPORT_CND_CLI_FIF01 . '_' . $conditionObj->condsurvey_code;
                $repOutPutPdfFile = $this->generateReportOutPutPath($reportFileName);
            } else {
                $errMsg[] = 'No condition survey found';
                return false;
            }
        }

        $this->generatedPath = $this->generateWkhtmltopdfTempPath($repId);
        \Log::info("Generate the report from the html.");
        $arrPdf = [];
        $reportFilterQuery = null;
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $condSurveyObjList = $reportFilterQuery->filterAll($this->all())->get();
        } else {
            $condSurveyObjList = $this->filter($this->all(), $inputs)->get();
        }
        if ($condSurveyObjList->isEmpty()) {
            $condSurveyData = $this->formatCondSurvey(null);
            $footerRender = $this->getView($condSurveyData, 'footer');
            $arrCoverPdf = $this->generateCoverPage($condSurveyData, $footerRender, $repOutPutPdfFile);
            $arrPdf = array_merge($arrPdf, $arrCoverPdf);
        } else {
            foreach ($condSurveyObjList as $condSurveyObj) {
                \Log::info("Survey code [{$condSurveyObj->condsurvey_code}]");
                \Log::info("Page index [{$this->pageIndex}]");
                $condSurveyData = $this->formatCondSurvey($condSurveyObj);
                $footerRender = $this->getView($condSurveyData, 'footer');
                $arrCoverPdf = $this->generateCoverPage($condSurveyData, $footerRender, $repOutPutPdfFile);
                $arrPdf = array_merge($arrPdf, $arrCoverPdf);

                $arrElements = [];
                $elementObjList = $this->getElements(
                    $condSurveyObj->condsurvey_id,
                    $condSurveyObj->condsurvey_building_block_id
                )->get();
                foreach ($elementObjList as $elementObj) {
                    $arrElements[] = $this->formatElement($elementObj);
                }
                if (!empty($arrElements)) {
                    $elementPdf = $this->generateElementsPage(
                        ['elements' => $arrElements],
                        $condSurveyData,
                        $footerRender,
                        $repOutPutPdfFile
                    );
                    $arrPdf = array_merge($arrPdf, $elementPdf);
                }

                $idWorkObjList = $this->getIdWorks(
                    $condSurveyObj->condsurvey_id,
                    $condSurveyObj->condsurvey_building_block_id
                )->get();
                $idWorks = $this->formatIdWorks($idWorkObjList);
                if (!empty($idWorks)) {
                    $data = ['idWorks' => $idWorks, 'isOrphaned' => false];
                    $idWorkPdf = $this->generateIdWorksPage(
                        $data,
                        $condSurveyData,
                        $footerRender,
                        $repOutPutPdfFile
                    );
                    $arrPdf = array_merge($arrPdf, $idWorkPdf);
                }

                $arrIdWorkPdf = $this->generateOrphanedIdWords(
                    $condSurveyObj->site_id,
                    $condSurveyObj->condsurvey_building_block_id,
                    $condSurveyData,
                    $footerRender,
                    $repOutPutPdfFile
                );
                $arrPdf = array_merge($arrPdf, $arrIdWorkPdf);
            }
        }
        if (!empty($arrPdf)) {
            $cssPaging = [
                'fontSize' => 8,
                'x' => 755,
                'y' => 3
            ];
            $this->mergerPdf($arrPdf, $repOutPutPdfFile, ['cssPaging' => $cssPaging]);
        }

        \Log::info("END Generate the report from the html.");

        $whereCodes = [];               // The codes which have been used in the filter
        $whereTexts = [];               // Plain text about filtering applied
        $orCodes = [];                  // csv string of OR codes
        $bFilterDetail = false;

        if ($reportFilterQuery instanceof IReportFilterQuery) {
            $this->filterText = $reportFilterQuery->getFilterDetailText();
        } else {
            $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
            $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $this->filterText);
        }

        if (is_null($conditionId)) {
            return true;
        } else {
            return $repOutPutPdfFile;
        }
    }

    private function generateCoverPage($data, $footerRender, $repOutPutPdfFile)
    {


        $data['isFirstPage'] = true;
        $data['isIdWork'] = false;
        $headerCoverRender = $this->generateHeader($data);
        $options = [
            'footer-html' => $footerRender,
            'header-html' => $headerCoverRender,
            'header-spacing' => 1,
            'footer-spacing' => 1,
            'orientation' => 'landscape'
        ];

        /**
         * Generate and store to disk the cover page.
         */
        $pagePart = $this->generatedPath . "/" . $this->pageIndex . '.html';
        $coverPageRender = $this->getView($data, 'cover');
        if (file_put_contents($pagePart, $coverPageRender, FILE_APPEND | LOCK_EX) === false) {
            throw new StateException("Failed to create/append data to file [$pagePart]");
        }
        \Log::info("Cover page [$pagePart]");

        $firstPdf =  $this->generateMultiHtmlToPdf(
            [$pagePart],
            $this->generatedPath,
            $repOutPutPdfFile,
            $options,
            $this->pageIndex,
            false
        );

        if ($data['surveyCode'] == '') {
            return $firstPdf;
        }

        $data['isFirstPage'] = false;
        $data['isIdWork'] = false;
        $headerCover2Render = $this->generateHeader($data);
        $options = [
            'footer-html' => $footerRender,
            'header-html' => $headerCover2Render,
            'header-spacing' => 1,
            'footer-spacing' => 1,
            'orientation' => 'landscape'
        ];
        $pagePart = $this->generatedPath . "/" . $this->pageIndex . '.html';
        $cover2PageRender = $this->getView($data, 'cover2');
        if (file_put_contents($pagePart, $cover2PageRender, FILE_APPEND | LOCK_EX) === false) {
            throw new StateException("Failed to create/append data to file [$pagePart]");
        }
        \Log::info("Cover 2 page [$pagePart]");

        $secondPdf =  $this->generateMultiHtmlToPdf(
            [$pagePart],
            $this->generatedPath,
            $repOutPutPdfFile,
            $options,
            $this->pageIndex,
            false
        );

        return array_merge($firstPdf, $secondPdf);
    }

    private function generateElementsPage($data, $dataHeader, $footerRender, $repOutPutPdfFile)
    {
        $dataHeader['isFirstPage'] = false;
        $dataHeader['isIdWork'] = false;
        $headerRender = $this->generateHeader($dataHeader);
        $options = [
            'footer-html' => $footerRender,
            'header-html' => $headerRender,
            'orientation' => 'landscape'
        ];
        $pagePart = $this->generatedPath . "/" . $this->pageIndex . '.html';
        $data['weightedScoreTotal'] = $dataHeader['weightedScoreTotal'];
        $data['scoreLocationCode'] = $dataHeader['condition'];
        $data['weightingTotal'] = $dataHeader['weightingTotal'];
        $data['weightedScorePercentage'] = $dataHeader['weightedScorePercentage'];
        $elementPageRender = $this->getView($data, 'elements');
        if (file_put_contents($pagePart, $elementPageRender, FILE_APPEND | LOCK_EX) === false) {
            throw new StateException("Failed to create/append data to file [$pagePart]");
        }
        \Log::info("Elements page [$pagePart]");

        $pdf =  $this->generateMultiHtmlToPdf(
            [$pagePart],
            $this->generatedPath,
            $repOutPutPdfFile,
            $options,
            $this->pageIndex,
            false
        );

        return $pdf;
    }

    private function generateIdWorksPage($data, $dataHeader, $footerRender, $repOutPutPdfFile)
    {
        $dataHeader['isFirstPage'] = false;
        $dataHeader['isIdWork'] = true;
        $dataHeader['isOrphaned'] = $data['isOrphaned'];
        $headerRender = $this->generateHeader($dataHeader);

        /**
         * Use to help debug the code.  Allows the header and footer html files to be accessed on the
         * server, allowing the pdf conversion to be run manually.
         *
            $header = $this->generatedPath . "/h" . $this->pageIndex . '.html';
            if (file_put_contents($header, $headerRender, FILE_APPEND | LOCK_EX) === false) {
                throw new StateException("Failed to create/append data to file [$header]");
            }

            $footer = $this->generatedPath . "/f" . $this->pageIndex . '.html';
            if (file_put_contents($footer, $footerRender, FILE_APPEND | LOCK_EX) === false) {
                throw new StateException("Failed to create/append data to file [$footer]");
            }
        */

        $options = [
            'footer-html' => $footerRender,
            'header-html' => $headerRender,
            'orientation' => 'landscape'
        ];
        $pagePart = $this->generatedPath . "/" . $this->pageIndex . '.html';
        $elementPageRender = $this->getView($data, 'idWorks');
        if (file_put_contents($pagePart, $elementPageRender, FILE_APPEND | LOCK_EX) === false) {
            throw new StateException("Failed to create/append data to file [$pagePart]");
        }
        \Log::info("IW page [$pagePart]");

        $pdf =  $this->generateMultiHtmlToPdf(
            [$pagePart],
            $this->generatedPath,
            $repOutPutPdfFile,
            $options,
            $this->pageIndex,
            false
        );

        return $pdf;
    }

    //private function generate
    private function generateHeader($data)
    {
        return $this->getView($data, 'header');
    }

    private function getView($data, $templateName)
    {
        return \View::make('reports.wkhtmltopdf.condition.cndCliFife01.' . $templateName, $data)->render();
    }

    private function formatIdWorks($idWorkObjList)
    {
        $result = [];
        $now = new \DateTime();

        foreach ($idWorkObjList as $idWorkObj) {
            if ($idWorkObj instanceof Idwork) {
                if (!isset($result[$idWorkObj->idwork_element_id])) {
                    $result[$idWorkObj->idwork_element_id] = [
                        'idWorkElementCode' => $idWorkObj->idwork_element_code,
                        'idWorkElementDesc' => $idWorkObj->idwork_element_desc,
                        'subElements' => []
                    ];
                }
                if (
                    !isset(
                        $result[$idWorkObj->idwork_element_id]['subElements'][$idWorkObj->idwork_subelement_id]
                    )
                ) {
                    $result[$idWorkObj->idwork_element_id]['subElements'][$idWorkObj->idwork_subelement_id] = [
                        'idWorkSubElementCode' => $idWorkObj->idwork_subelement_code,
                        'idWorkSubElementDesc' => $idWorkObj->idwork_subelement_desc,
                        'items' => []
                    ];
                }
                $targetYearEndDateObj = new \DateTime($idWorkObj->year_end_date);
                $idWorkInputObj = new \DateTime($idWorkObj->created_date);
                $result[$idWorkObj->idwork_element_id]['subElements'][$idWorkObj->idwork_subelement_id]['items']
                [$idWorkObj->idwork_item_id][] =
                    [
                    'idWorkItemCode' => $idWorkObj->idwork_item_code,
                    'idWorkItemDesc' => $idWorkObj->idworkitem_desc,
                    'idWorkCode' => $idWorkObj->idwork_code,
                    'idWorkDefect' => $idWorkObj->defect,
                    'idWorkLocation' => $idWorkObj->location,
                    'idWorkRemedy' => $idWorkObj->remedy,
                    'idWorkCondition' => $idWorkObj->idwork_condition_code,
                    'idWorkPriority' => $idWorkObj->idwork_priority_code,
                    'idWorkInput' => $idWorkInputObj->format('d/m/Y'),
                    'idWorkStatus' => $idWorkObj->idwork_status_code,
                    'idWorkFundingCode' => $idWorkObj->gen_userdef_sel_code,
                    'idWorkFundingDesc' => $idWorkObj->gen_userdef_sel_desc,
                    'idWorkNote' => $idWorkObj->comment,
                    'idWorkBase' => $idWorkObj->cost,
                    'idWorkPrelimCode' => $idWorkObj->cnd_prelim_cost,
                    'idWorkContinCost' => $idWorkObj->cnd_contin_cost,
                    'idWorkFeeCost' => $idWorkObj->cnd_fee_cost,
                    'idWorkEstimate' => $idWorkObj->total_cost,
                    'idWorkEstDate' => $idWorkObj->user_text1,
                    'idWorkTargetYearCode' => $idWorkObj->fin_year_code,
                    'idWorkTargetYearIsPast' => $targetYearEndDateObj < $now,
                    'idWorkCyclical' => $idWorkObj->cycle_length,
                    'idWorkCyclicalMaintenance' => $idWorkObj->cyclical_maintenance,
                    ];
            }
        }

        return $result;
    }

    private function getIdWorks($condSurveyId, $buildingBlockId)
    {
        $select = [
            'idwork.idwork_id',
            'idwork.idwork_code',
            'idwork.defect',
            'idwork.location',
            'idwork.remedy',
            'idwork.created_date',
            'idwork.comment',
            'idwork.cost',
            'idwork.cnd_prelim_cost',
            'idwork.cnd_contin_cost',
            'idwork.cnd_fee_cost',
            'idwork.total_cost',
            'idwork.cyclical_maintenance',
            'idwork.cycle_length',
            'fin_year.fin_year_code',
            'fin_year.year_end_date',
            'idwork_condition.idwork_condition_code',
            'idwork_priority.idwork_priority_code',
            'idwork_status.idwork_status_code',
            'gen_userdef_sel.gen_userdef_sel_code',
            'gen_userdef_sel.gen_userdef_sel_desc',
            'gen_userdef_group.user_text1',
            'idwork_element.idwork_element_id',
            'idwork_element.idwork_element_code',
            'idwork_element.idwork_element_desc',
            'idwork_subelement.idwork_subelement_id',
            'idwork_subelement.idwork_subelement_code',
            'idwork_subelement.idwork_subelement_desc',
            'idwork_item.idwork_item_id',
            'idwork_item.idwork_item_code',
            'idwork_item.idworkitem_desc'
        ];

        $query = Idwork::select($select)
            ->join('idwork_status', 'idwork_status.idwork_status_id', '=', 'idwork.idwork_status_id')
            ->leftJoin(
                'idwork_element',
                'idwork_element.idwork_element_id',
                '=',
                'idwork.element_id'
            )
            ->leftJoin(
                'idwork_subelement',
                'idwork_subelement.idwork_subelement_id',
                '=',
                'idwork.sub_element_id'
            )
            ->leftJoin(
                'idwork_item',
                'idwork_item.idwork_item_id',
                '=',
                'idwork.item_id'
            )
            ->leftJoin(
                'idwork_condition',
                'idwork_condition.idwork_condition_id',
                '=',
                'idwork.idwork_condition_id'
            )
            ->leftJoin(
                'idwork_priority',
                'idwork_priority.idwork_priority_id',
                '=',
                'idwork.idwork_priority_id'
            )
//            ->leftJoin(
//                'idwork_status',
//                'idwork_status.idwork_status_id',
//                '=',
//                'idwork.idwork_status_id'
//            )
            ->leftJoin(
                'gen_userdef_group',
                'gen_userdef_group.gen_userdef_group_id',
                '=',
                'idwork.gen_userdef_group_id'
            )
            ->leftJoin(
                'gen_userdef_sel',
                'gen_userdef_sel.gen_userdef_sel_id',
                '=',
                'gen_userdef_group.sel1_gen_userdef_sel_id'
            )
            ->leftJoin(
                'fin_year',
                'fin_year.fin_year_id',
                '=',
                'idwork.target_fin_year_id'
            )
            ->where('idwork.condsurvey_id', '=', $condSurveyId)
            ->where('idwork.building_block_id', '=', $buildingBlockId)
            // CLD-9250: Rooms should now be included.
            //->whereNull('idwork.room_id')
            ->whereNotIn(
                'idwork_status.idwork_status_type_id',
                [IdworkStatusType::COMPLETE, IdworkStatusType::SUPERSEDED]
            )
            ->orderBy('idwork_element.idwork_element_code', 'ASC')
            ->orderBy('idwork_subelement.idwork_subelement_code', 'ASC')
            ->orderBy('idwork.idwork_code', 'ASC');

        return $query;
    }

    /**
     * Get sub element and item data
     */
    private function getSubElements($condLocationScoreId, $idworkElementId)
    {
        $subelementQuery = DB::table('cond_location_subelement_score')->select(
            [
                'idwork_subelement.idwork_subelement_code as code',
                'idwork_subelement.idwork_subelement_desc as desc',
                'score.score_code as grade',
                'cond_location_subelement_score.comment as comment',
                DB::raw('0 as is_item'),
            ]
        )
            ->join(
                'idwork_subelement',
                'idwork_subelement.idwork_subelement_id',
                '=',
                'cond_location_subelement_score.idwork_subelement_id'
            )
            ->leftJoin(
                'score',
                'score.score_id',
                '=',
                'cond_location_subelement_score.score_id'
            )
            ->where('cond_location_subelement_score.cond_location_score_id', '=', $condLocationScoreId)
            ->where('cond_location_subelement_score.idwork_element_id', '=', $idworkElementId)
            ->where('idwork_subelement.show_on_property_scoring', '=', CommonConstant::DATABASE_VALUE_YES);

        $itemQuery = DB::table('cond_location_item_score')->select(
            [
                'idwork_item.idwork_item_code as code',
                'idwork_item.idworkitem_desc as desc',
                'score.score_code as grade',
                'cond_location_item_score.comment as comment',
                DB::raw('1 as is_item'),
            ]
        )
            ->join(
                'idwork_item',
                'idwork_item.idwork_item_id',
                '=',
                'cond_location_item_score.idwork_item_id'
            )
            ->join(
                'idwork_subelement',
                'idwork_subelement.idwork_subelement_id',
                '=',
                'idwork_item.idwork_subelement_id',
            )
            ->leftJoin(
                'score',
                'score.score_id',
                '=',
                'cond_location_item_score.score_id'
            )
            ->where('cond_location_item_score.cond_location_score_id', '=', $condLocationScoreId)
            ->where('cond_location_item_score.idwork_element_id', '=', $idworkElementId)
            ->where('idwork_subelement.show_on_property_scoring', '=', CommonConstant::DATABASE_VALUE_YES)
            ->where('idwork_item.show_on_property_scoring', '=', CommonConstant::DATABASE_VALUE_YES);

        $query = $subelementQuery->unionAll($itemQuery)
            ->orderBy('code', 'ASC');

        return $query;
    }

    private function formatElement($condLocationScoreObj)
    {
        $result = [];

        if ($condLocationScoreObj instanceof CondLocationScore) {
            $result =  [
                'elementCode' => $condLocationScoreObj->idwork_element_code,
                'elementDescription' => $condLocationScoreObj->idwork_element_desc,
                'elementComment' => $condLocationScoreObj->comment,
                'elementGrade' => $condLocationScoreObj->score_code,
                'elementScore' => $condLocationScoreObj->weighted_score_value,
            ];

            $result['subElements'] = [];
            $subElementObjList  = $this->getSubElements(
                $condLocationScoreObj->cond_location_score_id,
                $condLocationScoreObj->idwork_element_id
            )->get();
            foreach ($subElementObjList as $subElementObj) {
                if ($subElementObj) {
                    $subElementData = [
                        'subElementCode'        => $subElementObj->code,
                        'subElementDescription' => $subElementObj->desc,
                        'subElementGrade'       => $subElementObj->grade,
                        'subElementComment'     => $subElementObj->comment,
                        'subElementItem'        => $subElementObj->is_item,
                    ];
                    array_push($result['subElements'], $subElementData);
                }
            }
        }
        return $result;
    }

    private function getElements($condSurveyId, $buildingBlockId)
    {
        $select = [
            'cond_location_score.cond_location_score_id',
            'idwork_element.idwork_element_id',
            'idwork_element.idwork_element_code',
            'idwork_element.idwork_element_desc',
            'cond_location_element_score.weighted_score_value',
            'cond_location_element_score.comment',
            'cond_location_element_score.score_id as condLocationElementScoreId',
            'score.score_code'
            ];

        $query = CondLocationScore::select($select)
            ->join(
                'cond_location_element_score',
                'cond_location_element_score.cond_location_score_id',
                '=',
                'cond_location_score.cond_location_score_id'
            )
            ->join(
                'idwork_element',
                'idwork_element.idwork_element_id',
                '=',
                'cond_location_element_score.idwork_element_id'
            )
            ->leftJoin(
                'score',
                'score.score_id',
                '=',
                'cond_location_element_score.score_id'
            )
            ->where('cond_location_score.condsurvey_id', '=', $condSurveyId)
            ->where('cond_location_score.location_record_id', '=', $buildingBlockId)
            ->orderBy('idwork_element.idwork_element_code', 'ASC');

        return $query;
    }

    private function formatCondSurvey($condSurveyObj)
    {
        $condSurveyData =  [
            'siteCode' => '',
            'siteDescription' => '',
            'buildingBlockCode' => '',
            'buildingBlockDescription' => '',
            'surveyDate' => '',
            'surveyCode' => '',
            'surveyStatus' => '',
            'weather' => '',
            'gia' => '',
            'listed' => '',
            'surveyStatus' => '',
            'condition' => '',
            'addressArr' => '',
            'built' => '',
            'conservationArea' => '',
            'elecSummary' => '',
            'mechSummary' => '',
            'genSummary' => '',
            'scoreComment' => '',
            'occupancyClassCode' => '',
            'occupancyClassDescription' => ''
        ];

        if ($condSurveyObj instanceof Condsurvey) {
            $surveyDateObj = new \DateTime($condSurveyObj->survey_date);
            $condSurveyData = [
                'siteCode' => $condSurveyObj->site_code,
                'siteDescription' => $condSurveyObj->site_desc,
                'buildingBlockCode' => $condSurveyObj->building_block_code,
                'buildingBlockDescription' => $condSurveyObj->building_block_desc,
                'surveyDate' => $surveyDateObj->format('d/m/Y'),
                'surveyCode' => $condSurveyObj->condsurvey_code,
                'weather' => $condSurveyObj->gen_weather_condition_desc,
                'gia' => $condSurveyObj->gia,
                'listed' => $condSurveyObj->listed_type_code,
                'occupancyClassCode' => $condSurveyObj->gen_userdef_sel_code,
                'occupancyClassDescription' => $condSurveyObj->gen_userdef_sel_desc,
                'surveyStatus' => $condSurveyObj->condsurvey_status_desc,
                'built' => $condSurveyObj->year_built,
                'elecSummary' => $condSurveyObj->elec_summary,
                'mechSummary' => $condSurveyObj->mech_summary,
                'genSummary' => $condSurveyObj->gen_summary,
                'scoreComment' => $condSurveyObj->score_comment,
                'conservationArea' => $condSurveyObj->user_check1,
                'weightedScoreTotal' => $condSurveyObj->weighted_score_total,
                'weightingTotal' => $condSurveyObj->maximum_weighted_score,
                'weightedScorePercentage' => $condSurveyObj->weighted_score_percentage,
                'condition' => $condSurveyObj->score_code,
            ];

            $addressObj = Address::find($condSurveyObj->address_id);
            if ($addressObj instanceof Address) {
                $condSurveyData['addressArr'] = $addressObj->getAddressAsArray();
            }
        }

        return $condSurveyData;
    }

    private function all()
    {
        $select = [
            'condsurvey.condsurvey_id',
            'condsurvey.condsurvey_building_block_id',
            'condsurvey.condsurvey_code',
            'condsurvey.survey_date',
            'condsurvey.elec_summary',
            'condsurvey.mech_summary',
            'condsurvey.gen_summary',
            'condsurvey.score_comment',
            'site.site_id',
            'site.site_code',
            'site.site_desc',
            'site.address_id',
            'building_block.building_block_code',
            'building_block.building_block_desc',
            'building_block.gia',
            'building_block.year_built',
            'listed_type.listed_type_code',
            'gen_userdef_sel.gen_userdef_sel_code',
            'gen_userdef_sel.gen_userdef_sel_desc',
            'gen_userdef_group.user_check1',
            'condsurvey_status.condsurvey_status_desc',
            'gen_weather_condition.gen_weather_condition_desc',
            'cond_location_score.cond_location_score_id',
            'cond_location_score.weighted_score_total',
            'cond_location_score.maximum_weighted_score',
            'cond_location_score.weighted_score_percentage',
            'score.score_code'
        ];
        $query = Condsurvey::UserSiteGroup()->select($select)
            ->leftJoin(
                'site',
                'site.site_id',
                '=',
                'condsurvey.site_id'
            )
            ->leftJoin(
                'building',
                'building.building_id',
                '=',
                'condsurvey.condsurvey_building_id'
            )
            ->leftJoin(
                'building_block',
                'building_block.building_block_id',
                '=',
                'condsurvey.condsurvey_building_block_id'
            )
            ->leftJoin(
                'listed_type',
                'listed_type.listed_type_id',
                '=',
                'building.listed_type_id'
            )
            ->leftJoin(
                'gen_userdef_group',
                'gen_userdef_group.gen_userdef_group_id',
                '=',
                'building.gen_userdef_group_id'
            )
            ->leftJoin(
                'gen_userdef_sel',
                'gen_userdef_sel.gen_userdef_sel_id',
                '=',
                'gen_userdef_group.sel1_gen_userdef_sel_id'
            )
            ->leftJoin(
                'gen_weather_condition',
                'gen_weather_condition.gen_weather_condition_id',
                '=',
                'condsurvey.gen_weather_condition_id'
            )
            ->leftJoin(
                'condsurvey_status',
                'condsurvey_status.condsurvey_status_id',
                '=',
                'condsurvey.condsurvey_status_id'
            )
            ->leftJoin(
                'cond_location_score',
                function ($join) {
                    $join->on('cond_location_score.condsurvey_id', '=', 'condsurvey.condsurvey_id')
                        ->on('cond_location_score.location_record_id', '=', 'condsurvey.condsurvey_building_block_id');
                }
            )
            ->leftJoin(
                'score',
                'score.score_id',
                '=',
                'cond_location_score.score_id'
            )

        ->orderBy('site.site_code', 'ASC')
        ->orderBy('building_block.building_block_code', 'ASC')
        ->orderBy('condsurvey.condsurvey_code', 'ASC');

        return $query;
    }

    private function filter($query, $inputs)
    {
        $query->whereNotNull('condsurvey.condsurvey_building_id');
        $query->whereNotNull('condsurvey.condsurvey_building_block_id');
        $query->whereNull('condsurvey.room_id');

        $conditionId = array_get($inputs, 'conditionId', null);

        if (!is_null($conditionId)) {
            $query->where('condsurvey.condsurvey_id', '=', $conditionId);
        }

        $filter = new \Tfcloud\Lib\Filters\ConditionFilter($inputs);

        if (!empty($filter->site_id)) {
            $query->where('condsurvey.site_id', '=', $filter->site_id);
        }
        if (!empty($filter->building_id)) {
            $query->where('condsurvey.condsurvey_building_id', '=', $filter->building_id);
        }
        if (!empty($filter->block_id)) {
            $query->where('condsurvey.condsurvey_building_block_id', '=', $filter->block_id);
        }

        if (!empty($filter->surveyor)) {
            $query->where('condsurvey.surveyor', '=', $filter->surveyor);
        }

        $status = [];
        if (!empty($filter->open)) {
            array_push($status, $filter->open);
        }
        if (!empty($filter->qa)) {
            array_push($status, $filter->qa);
        }
        if (!empty($filter->complete)) {
            array_push($status, $filter->complete);
        }
        if (!empty($filter->historic)) {
            array_push($status, $filter->historic);
        }
        if (!empty($status)) {
            $query->whereIn('condsurvey.condsurvey_status_id', $status);
        }

        if (!empty($filter->code)) {
            $query->where('condsurvey.condsurvey_code', 'like', '%' . trim($filter->code) . '%');
        }

        if (!empty($filter->description)) {
            $query->where('condsurvey.condsurvey_desc', 'like', '%' . $filter->description . '%');
        }

        if (!empty($filter->property_status)) {
            $query->where('building_block.active', '=', $filter->property_status);
        }
        //echo '<pre>';
        $tmp = print_r($query->toSql(), true);
        \Log::error("SQL: $tmp");
        //print_r($var);
        //print_r(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 5));
        //\Log::error($var);
        //print_r($query->toSql());
      //  echo '<br>';
        //print_r($query->getBindings());
      //  echo '</pre>';
      //  dd('The buck stops here!');
        return $query;
    }


    private function allSurveySiteLevel($siteId)
    {
        $select = [
            'condsurvey.condsurvey_id'
        ];
        $query = Condsurvey::UserSiteGroup()->select($select)
            ->where('condsurvey.site_id', '=', $siteId)
            ->whereNull('condsurvey.condsurvey_building_id')
            ->whereNull('condsurvey.condsurvey_building_block_id')
            ->orderBy('condsurvey.condsurvey_code', 'ASC');

        return $query;
    }

    private function generateOrphanedIdWords($siteId, $buildingBlockId, $condSurveyData, $footerRender, $repOutPdfFile)
    {
        $arrIdWorkPdf = [];
        $dummySurveyList = $this->allSurveySiteLevel($siteId)->get();
        foreach ($dummySurveyList as $dummySurvey) {
            $idWorkObjList = $this->getIdWorks($dummySurvey->condsurvey_id, $buildingBlockId)->get();
            $idWorks = $this->formatIdWorks($idWorkObjList);
            if (!empty($idWorks)) {
                $data = ['idWorks' => $idWorks, 'isOrphaned' => true];
                $idWorkPdf = $this->generateIdWorksPage(
                    $data,
                    $condSurveyData,
                    $footerRender,
                    $repOutPdfFile
                );
                $arrIdWorkPdf = array_merge($arrIdWorkPdf, $idWorkPdf);
            }
        }

        return $arrIdWorkPdf;
    }

    private function generateFilterData($filterData, &$whereCodes, &$whereTexts, &$orCodes)
    {
        if ($siteId = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $siteId, "Site Code"]);

            if ($buildingId = Common::iset($filterData['building_id'])) {
                array_push(
                    $whereCodes,
                    ['building', 'building_id', 'building_code', $buildingId, "Building Code"]
                );

                if ($val = Common::iset($filterData['block_id'])) {
                    array_push($whereCodes, [
                        'building_block',
                        'building_block_id',
                        'building_block_code',
                        $val,
                        "Building Block Code"
                    ]);
                }

                if ($val = Common::iset($filterData['room_id'])) {
                    array_push($whereCodes, ['room', 'room_id', 'room_number', $val, "Room Number"]);
                }
            }
        }

        if ($val = Common::iset($filterData['surveyor'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Surveyor']);
        }

        if ($val = Common::iset($filterData['property_status'])) {
            switch ($val) {
                case 'Y':
                    array_push($whereTexts, "Property Status = 'Active'");
                    break;
                case 'N':
                    array_push($whereTexts, "Property Status = 'Inactive'");
                    break;
            }
        }

        $status = [];
        if (Common::iset($filterData['open'])) {
            $status[] = CondsurveyStatus::OPEN;
        }
        if (Common::iset($filterData['qa'])) {
            $status[] = CondsurveyStatus::QA;
        }
        if (Common::iset($filterData['complete'])) {
            $status[] = CondsurveyStatus::COMPLETE;
        }
        if (Common::iset($filterData['historic'])) {
            $status[] = CondsurveyStatus::HISTORIC;
        }
        if (count($status)) {
            array_push($orCodes, [
                'condsurvey_status',
                'condsurvey_status_id',
                'condsurvey_status_code',
                implode(',', $status),
                "Survey Status"
            ]);
        }

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Description contains '" . $val . "'");
        }
    }
}
