<?php

namespace Tfcloud\Services\Report\WkHtmlToPdf;

use DateTime;
use Illuminate\Support\Arr;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Filters\Estate\LeaseOutFilter;
use Tfcloud\Models\CrudAccessLevel;
use Tfcloud\Models\EstCharge;
use Tfcloud\Models\EstUnitType;
use Tfcloud\Models\FinYear;
use Tfcloud\Models\FinYearPeriod;
use Tfcloud\Models\GenTable;
use Tfcloud\Models\LeaseOutLetu;
use Tfcloud\Models\LeaseOutStatus;
use Tfcloud\Models\LeaseOutStatusType;
use Tfcloud\Models\Module;
use Tfcloud\Models\Report;
use Tfcloud\Services\Estate\InvoiceGenerationPlanService;
use Tfcloud\Services\Estate\LeaseOutService;
use Tfcloud\Services\PermissionService;

class LeaseOutCharge5YearForecastReportService extends WkHtmlToPdfReportBaseService
{
    private $leaseOutService;
    private $invoiceGenerationPlanService;
    public $filterText = '';
    private ?ClassMapReportLoader $reportBoxFilterLoader;

    public function __construct(PermissionService $permissionService, Report $report)
    {
        parent::__construct($permissionService);
        $this->leaseOutService = new LeaseOutService($permissionService);
        $this->invoiceGenerationPlanService = new InvoiceGenerationPlanService($permissionService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function runReport($repOutPutPdfFile, $inputs, $repId)
    {
        $this->formatInputData($inputs);
        $chargeRents = $this->getAllLeaseOutCharges($inputs)->get();
        $today = new DateTime(date('Y-m-d'));
        $currentFinYear = FinYear::userSiteGroup()->where('year_start_date', '<=', $today->format('Y-m-d'))
            ->where('year_end_date', '>=', $today->format('Y-m-d'))
            ->first();
        $currentFinYearPeriod = FinYearPeriod::where('fin_year_id', $currentFinYear->fin_year_id)
            ->where('period_start_date', '<=', $today)
            ->where('period_end_date', '>=', $today)
            ->first();
        $repStartDate = new DateTime($currentFinYearPeriod->period_start_date);
        $finYears = FinYear::userSiteGroup()
            ->select([
                '*',
                \DB::raw("'0' as amount")
            ])
            ->where('year_start_date', '>=', $currentFinYear->year_start_date)
            ->limit(5)
            ->get();
        $reportEndDate = new DateTime($finYears[count($finYears) - 1]->year_end_date);
        $allTotal = 0;
        $leaseOuts = [];
        if (!empty($chargeRents)) {
            foreach ($chargeRents as $chargeRent) {
                $leaseOutId = $chargeRent->lease_out_id;
                $chargeRent->chargeInYears = null;
                $totalInAllYears = 0;
                $chargeInYears = [];
                $invoicePlanItem = $this->invoiceGenerationPlanService->getPlanItem([
                    'lease_out_id' => $chargeRent->lease_out_id,
                    'process_up_to_date' => $reportEndDate->format('Y-m-d'),
                    'est_charge_id' => $chargeRent->est_charge_id,
                    'is_charge_report' => true
                ]);
                for ($i = 0; $i < count($finYears); $i++) {
                    $yearValue = (new DateTime($finYears[$i]->year_start_date))->format('Y');
                    $chargeInYears[$yearValue] = 0;

                    foreach ($invoicePlanItem as $item) {
                        $dueDate = new DateTime($item['charge_due_date']);
                        if ($i == 0) {
                            $finYearStartDate = $repStartDate;
                        } else {
                            $finYearStartDate = new DateTime($finYears[$i]->year_start_date);
                        }
                        $finYearEndDate = new DateTime($finYears[$i]->year_end_date);
                        if (($finYearStartDate <= $dueDate) && ($dueDate <= $finYearEndDate)) {
                            $chargeInYears[$yearValue] += $item['amount'];
                            $totalInAllYears += $item['amount'];
                            $finYears[$i]->amount += $item['amount'];
                            $allTotal += $item['amount'];
                        }
                    }
                }
                $chargeRent->chargeInYears = $chargeInYears;
                if ($totalInAllYears > 0) {
                    $chargeRent->totalInAllYears = $totalInAllYears;
                    $leaseOuts[$chargeRent->lease_out_id]['chargeRents'][] = $chargeRent;
                    $leaseOuts[$chargeRent->lease_out_id]['lease_out_code'] = $chargeRent->lease_out_code;
                    $leaseOuts[$chargeRent->lease_out_id]['lease_out_desc'] = $chargeRent->lease_out_desc;
                    $leaseOuts[$chargeRent->lease_out_id]['lease_out_id'] = $leaseOutId;
                }
            }
        }

        if (!empty($leaseOuts)) {
            foreach ($leaseOuts as &$leaseOut) {
                $leaseOut['total'] = 0;
                foreach ($finYears as $finYear) {
                    $yearValue = (new DateTime($finYear->year_start_date))->format('Y');
                    $leaseOut['totalInYears'][$yearValue] = 0;
                    foreach ($leaseOut['chargeRents'] as $chargeRent) {
                        $leaseOut['totalInYears'][$yearValue] += $chargeRent->chargeInYears[$yearValue];
                        $leaseOut['total'] += $chargeRent->chargeInYears[$yearValue];
                    }
                }
            }
        }

        // Create wkhtmltopdf temp folder to store generated files
        $generatedPath = $this->generateWkhtmltopdfTempPath($repId);

        $header = \View::make(
            'reports.wkhtmltopdf.estates.es40.header',
            [
                'repStartDate' => $repStartDate->format('d/m/Y')
            ]
        )->render();
        $headerPath = $generatedPath . "/header.html";
        file_put_contents($headerPath, $header, LOCK_EX);

        $footer = \View::make('reports.wkhtmltopdf.estates.es40.footer')->render();
        $footerPath = $generatedPath . "/footer.html";
        file_put_contents($footerPath, $footer, LOCK_EX);

        $content = \View::make(
            'reports.wkhtmltopdf.estates.es40.content',
            [
                'leaseOuts' => $leaseOuts,
                'finYears' => $finYears,
                'allTotal' => $allTotal
            ]
        )->render();
        $contentPath = $generatedPath . "/content.html";
        file_put_contents($contentPath, $content, LOCK_EX);

        $options = [
            'header-html' => $headerPath,
            'footer-html' => $footerPath,
            'header-spacing' => 4,
            'footer-spacing' => 4,
            'orientation' => 'landscape',
        ];

        $this->generateSingleHtmlToPdf($contentPath, $repOutPutPdfFile, $options);

        $whereCodes = [];               // The codes which have been used in the filter
        $whereTexts = [];               // Plain text about filtering applied
        $orCodes = [];                  // csv string of OR codes
        $bFilterDetail = false;

        $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
        $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $this->filterText);

        return true;
    }

    private function filterAll(
        $query,
        $inputs,
        $view = false,
        $lettingPacketId = 0,
        $lettableUnitId = 0,
        $isUsingLettable = true
    ) {
        if (!array_key_exists('lease_out_id', $inputs)) {
            $filter = new LeaseOutFilter($inputs);

            if ($view) {
                $table = $view;
            } else {
                $table = 'lease_out';
                $query->orderBy($filter->sort, $filter->sortOrder);
            }

            if (!is_null($filter->code)) {
                $query->where($table . '.lease_out_code', 'like', '%' . trim($filter->code) . '%');
            }

            if (!is_null($filter->description)) {
                $query->where($table . '.lease_out_desc', 'like', '%' . trim($filter->description) . '%');
            }

            if (!is_null($filter->est_charge_group_id)) {
                $query->leftJoin(
                    'est_charge_group_item',
                    'est_charge.est_charge_id',
                    '=',
                    'est_charge_group_item.est_charge_id'
                )
                    ->where('est_charge_group_item.est_charge_group_id', $filter->est_charge_group_id);
            }

            if (!is_null($filter->leaseOutStatus)) {
                $query->where($table . '.lease_out_status_id', $filter->leaseOutStatus);
            }

            if (
                !is_null($filter->lostActive) ||
                !is_null($filter->lostInactive) ||
                !is_null($filter->lostArchive) ||
                !is_null($filter->lostTerminated)
            ) {
                $options = [];

                if (!is_null($filter->lostActive)) {
                    array_push($options, LeaseOutStatusType::ACTIVE);
                }

                if (!is_null($filter->lostInactive)) {
                    array_push($options, LeaseOutStatusType::INACTIVE);
                }

                if (!is_null($filter->lostArchive)) {
                    array_push($options, LeaseOutStatusType::ARCHIVE);
                }

                if (!is_null($filter->lostTerminated)) {
                    array_push($options, LeaseOutStatusType::TERMINATED);
                }

                $leaseOutStatus = LeaseOutStatus::whereIn('lease_out_status_type_id', $options)->get();

                $statusIds = [];

                foreach ($leaseOutStatus as $status) {
                    array_push($statusIds, $status->lease_out_status_id);
                }

                $query->whereIn("{$table}.lease_out_status_id", $statusIds);
            }

            if (!is_null($filter->leaseType)) {
                $query->where($table . '.lease_type_id', $filter->leaseType);
            }

            if (!is_null($filter->leaseSubType)) {
                $query->where($table . '.est_lease_sub_type_id', $filter->leaseSubType);
            }

            if (!is_null($filter->landlord)) {
                $query->where($table . '.landlord_contact_id', $filter->landlord);
            }

            if (!is_null($filter->contact)) {
                $query->where($table . '.tenant_contact_id', $filter->contact);
            }

            if (!is_null($filter->owner)) {
                $query->where($table . '.owner_user_id', $filter->owner);
            }

            if (!is_null($filter->holdingOver)) {
                if (
                    in_array(
                        $filter->holdingOver,
                        [
                            CommonConstant::DATABASE_VALUE_YES,
                            CommonConstant::DATABASE_VALUE_NO
                        ]
                    )
                ) {
                    $query->where($table . '.holding_over', $filter->holdingOver);
                }
            }

            if (!is_null($filter->holdingOverFrom)) {
                $holdingOverFrom = \DateTime::createFromFormat('d/m/Y', $filter->holdingOverFrom);
                if ($holdingOverFrom) {
                    $query->where(
                        \DB::raw("DATE_FORMAT({$table}.holding_over_date , '%Y-%m-%d')"),
                        '>=',
                        $holdingOverFrom->format('Y-m-d')
                    );
                }
            }

            if (!is_null($filter->holdingOverTo)) {
                $holdingOverTo = \DateTime::createFromFormat('d/m/Y', $filter->holdingOverTo);
                if ($holdingOverTo) {
                    $query->where(
                        \DB::raw("DATE_FORMAT({$table}.holding_over_date, '%Y-%m-%d')"),
                        '<=',
                        $holdingOverTo->format('Y-m-d')
                    );
                }
            }

            if (!is_null($filter->agreementDateFrom)) {
                $agreementDateFrom = \DateTime::createFromFormat('d/m/Y', $filter->agreementDateFrom);
                if ($agreementDateFrom) {
                    $query->where(
                        \DB::raw("DATE_FORMAT({$table}.agreement_date , '%Y-%m-%d')"),
                        '>=',
                        $agreementDateFrom->format('Y-m-d')
                    );
                }
            }

            if (!is_null($filter->agreementDateTo)) {
                $agreementDateTo = \DateTime::createFromFormat('d/m/Y', $filter->agreementDateTo);
                if ($agreementDateTo) {
                    $query->where(
                        \DB::raw("DATE_FORMAT({$table}.agreement_date, '%Y-%m-%d')"),
                        '<=',
                        $agreementDateTo->format('Y-m-d')
                    );
                }
            }

            if (!is_null($filter->startDateFrom)) {
                $startDateFrom = \DateTime::createFromFormat('d/m/Y', $filter->startDateFrom);
                if ($startDateFrom) {
                    $query->where(
                        \DB::raw("DATE_FORMAT({$table}.start_date , '%Y-%m-%d')"),
                        '>=',
                        $startDateFrom->format('Y-m-d')
                    );
                }
            }

            if (!is_null($filter->startDateTo)) {
                $startDateTo = \DateTime::createFromFormat('d/m/Y', $filter->startDateTo);
                if ($startDateTo) {
                    $query->where(
                        \DB::raw("DATE_FORMAT({$table}.start_date, '%Y-%m-%d')"),
                        '<=',
                        $startDateTo->format('Y-m-d')
                    );
                }
            }

            if (!is_null($filter->endDateFrom)) {
                $endDateFrom = \DateTime::createFromFormat('d/m/Y', $filter->endDateFrom);
                if ($endDateFrom) {
                    $query->where(
                        \DB::raw("DATE_FORMAT({$table}.end_date , '%Y-%m-%d')"),
                        '>=',
                        $endDateFrom->format('Y-m-d')
                    );
                }
            }

            if (!is_null($filter->endDateTo)) {
                $endDateTo = \DateTime::createFromFormat('d/m/Y', $filter->endDateTo);
                if ($endDateTo) {
                    $query->where(
                        \DB::raw("DATE_FORMAT({$table}.end_date, '%Y-%m-%d')"),
                        '<=',
                        $endDateTo->format('Y-m-d')
                    );
                }
            }

            if (!is_null($filter->rates_liability_id)) {
                $query->where($table . '.rates_liability_id', $filter->rates_liability_id);
            }

            if (!is_null($filter->insce_liability_id)) {
                $query->where($table . '.insce_liability_id', $filter->insce_liability_id);
            }

            if (!is_null($filter->estate_maint_id)) {
                $query->where($table . '.estate_maint_id', $filter->estate_maint_id);
            }

            if (!is_null($filter->landlordTenantAct)) {
                if (
                    in_array(
                        $filter->landlordTenantAct,
                        [
                            CommonConstant::DATABASE_VALUE_YES,
                            CommonConstant::DATABASE_VALUE_NO
                        ]
                    )
                ) {
                    $query->where($table . '.landlord_tenant_act', $filter->landlordTenantAct);
                }
            }

            $filter->site = !is_null($filter->site) ?
                $filter->site : (!is_null($filter->site_id) ? $filter->site_id : null);
            if (!is_null($filter->site)) {
                if ($view) {
                    $query->where("{$table}.site_id", $filter->site);
                } else {
                    $list = LeaseOutLetu::leftJoin('lettable_unit', function ($join) {
                        $join->on('lease_out_letu.lettable_unit_id', '=', 'lease_out_letu.lettable_unit_id')
                            ->where('lettable_unit.est_unit_type_id', '=', EstUnitType::EST_UT_LU);
                    })
                        ->leftJoin(
                            'lettable_unit_item',
                            'lettable_unit.lettable_unit_id',
                            '=',
                            'lettable_unit_item.lettable_unit_id'
                        )
                        ->where('lettable_unit.site_group_id', \Auth::user()->site_group_id)
                        ->where('lettable_unit_item.site_id', $filter->site)
                        ->groupBy('lease_out_id')
                        ->pluck('lease_out_id')->toArray();

                    if (!empty($list)) {
                        $query->whereIn('lease_out.lease_out_id', $list);
                    } else {
                        $query->whereIn('lease_out.lease_out_id', ['']);
                    }
                }
            }

            if ($lettingPacketId != 0) {
                if ($lettingPacketId == -1) {
                    $query->whereNull('lease_out.est_letting_packet_id');
                } else {
                    $query->where('lease_out.est_letting_packet_id', $lettingPacketId);
                }
            }

            if ($lettableUnitId != 0) {
                if ($isUsingLettable) {
                    $query->where('lease_out_letu.lettable_unit_id', $lettableUnitId);
                } else {
                    $list = LeaseOutLetu::where('lettable_unit_id', $lettableUnitId)
                        ->pluck('lease_out_id')
                        ->toArray();
                    if (!empty($list)) {
                        $query->whereNotIn('lease_out.lease_out_id', $list);
                    }
                }
            }

            if ($filter->chargeType) {
                if ($filter->chargeType != 'all') {
                    $query->where("est_charge.rent", "=", $filter->chargeType);

                    if ($filter->chargeType == "N") {
                        if ($filter->estChargeTypeId) {
                            $query->where("est_charge.est_charge_type_id", $filter->estChargeTypeId);
                        }
                    }
                }
            }

            if (!is_null($filter->account)) {
                $query->where("fin_account_id", $filter->account);
            }
            $this->filterByUserDefines(GenTable::LEASE_OUT, $query, $filter, $table, $view);
        } else {
            $query->where('lease_out.lease_out_id', $inputs['lease_out_id']);
        }

        // Check permissions on Rural Estates.
        // If not read-only min. then exclude them.
        if (
            !$this->permissionService->hasModuleAccess(
                Module::MODULE_RURAL_ESTATES,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
            )
        ) {
            $query->where('lease_out.ruralest_lease', CommonConstant::DATABASE_VALUE_NO);
        }

        return $query;
    }

    private function formatInputData(&$inputs)
    {
        foreach ($inputs as $key => $value) {
            if ($value === "") {
                unset($inputs[$key]);
            }
        }
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);

            $valueAgreementDate = $reportFilterQuery->getFirstValueFilterField("agreement_date");
            $inputs['agreementDateFrom'] = Common::dateFieldDisplayFormat(Arr::first($valueAgreementDate));
            $inputs['agreementDateTo'] = Common::dateFieldDisplayFormat(Arr::last($valueAgreementDate));

            $valueAuthorisedDate = $reportFilterQuery->getFirstValueFilterField("authorised_date");
            $inputs['authorisedDateFrom'] = Common::dateFieldDisplayFormat(Arr::first($valueAuthorisedDate));
            $inputs['authorisedDateTo'] = Common::dateFieldDisplayFormat(Arr::last($valueAuthorisedDate));

            $valueStartDate = $reportFilterQuery->getFirstValueFilterField("start_date");
            $inputs['startDateFrom'] = Common::dateFieldDisplayFormat(Arr::first($valueStartDate));
            $inputs['startDateTo'] = Common::dateFieldDisplayFormat(Arr::last($valueStartDate));

            $valueEndDate = $reportFilterQuery->getFirstValueFilterField("end_date");
            $inputs['endDateFrom'] = Common::dateFieldDisplayFormat(Arr::first($valueEndDate));
            $inputs['endDateTo'] = Common::dateFieldDisplayFormat(Arr::last($valueEndDate));

            $valueHoldingOverDate = $reportFilterQuery->getFirstValueFilterField("holding_over_date");
            $inputs['holdingOverFrom'] = Common::dateFieldDisplayFormat(Arr::first($valueHoldingOverDate));
            $inputs['holdingOverTo'] = Common::dateFieldDisplayFormat(Arr::last($valueHoldingOverDate));

            $leaseOutStatusType = $reportFilterQuery->getValueFilterField("lease_out_status_type_id") ?? [];
            $inputs["lostActive"] = in_array(LeaseOutStatusType::ACTIVE, $leaseOutStatusType) ?
                LeaseOutStatusType::ACTIVE : null;
            $inputs["lostInactive"] = in_array(LeaseOutStatusType::INACTIVE, $leaseOutStatusType) ?
                LeaseOutStatusType::INACTIVE : null;
            $inputs["lostArchive"] = in_array(LeaseOutStatusType::ARCHIVE, $leaseOutStatusType) ?
                LeaseOutStatusType::ARCHIVE : null;
            $inputs["lostTerminated"] = in_array(LeaseOutStatusType::TERMINATED, $leaseOutStatusType) ?
                LeaseOutStatusType::TERMINATED : null;

            $inputs['code'] = $reportFilterQuery->getFirstValueFilterField("lease_out_code");
            $inputs['site_id'] = $reportFilterQuery->getFirstValueFilterField("site_id");
            $inputs['owner'] = $reportFilterQuery->getFirstValueFilterField("owner_user_id");
            $inputs['leaseType'] = $reportFilterQuery->getFirstValueFilterField("lease_type_id");
            $inputs['leaseSubType'] = $reportFilterQuery->getFirstValueFilterField("est_lease_sub_type_id");
            $inputs['holdingOver'] = $reportFilterQuery->getFirstValueFilterField("holding_over");
            $inputs['insce_liability_id'] = $reportFilterQuery->getFirstValueFilterField("insce_liability_id");
            $inputs['landlord'] = $reportFilterQuery->getFirstValueFilterField("landlord_contact_id");
            $inputs['landlordTenantAct'] = $reportFilterQuery->getFirstValueFilterField("landlord_tenant_act");
            $inputs['description'] = $reportFilterQuery->getFirstValueFilterField("lease_out_desc");
            $inputs['estate_maint_id'] = $reportFilterQuery->getFirstValueFilterField("estate_maint_id");
            $inputs['rates_liability_id'] = $reportFilterQuery->getFirstValueFilterField("rates_liability_id");
            $inputs['contact'] = $reportFilterQuery->getFirstValueFilterField("tenant_contact_id");
            $inputs['account'] = $reportFilterQuery->getFirstValueFilterField("fin_account_id");
            $inputs['chargeType'] = $reportFilterQuery->getFirstValueFilterField("rent");
            $inputs['estChargeTypeId'] = $reportFilterQuery->getFirstValueFilterField("est_charge_type_id");
            $inputs['est_charge_group_id'] = $reportFilterQuery->getFirstValueFilterField("est_charge_group_id");
            $inputs['leaseOutStatus'] = $reportFilterQuery->getFirstValueFilterField("lease_out_status_id");

            $inputs = $this->getUDValueFilterQuery($reportFilterQuery, $inputs);
        }

        return $inputs;
    }

    private function getAllLeaseOutCharges($inputs)
    {
        if (
            !$this->permissionService->hasModuleAccess(
                Module::MODULE_ESTATES,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
            )
        ) {
            return null;
        }

        return $this->filterAll($this->getLeaseOutCharges(), $inputs);
    }

    private function getLeaseOutCharges()
    {
        $query = EstCharge::userSiteGroup()
            ->leftJoin('lease_out', 'lease_out.lease_out_id', '=', 'est_charge.lease_out_id')
            ->leftJoin('est_charge_type', 'est_charge.est_charge_type_id', '=', 'est_charge_type.est_charge_type_id')
            ->leftJoin(
                'est_time_period',
                'est_charge.repeat_est_time_period_id',
                '=',
                'est_time_period.est_time_period_id'
            )
            ->leftJoin(
                'tax_code',
                'tax_code.tax_code_id',
                '=',
                'est_charge.tax_code_id'
            )
            ->where('est_charge.active', CommonConstant::DATABASE_VALUE_YES)
            ->where('est_charge.credit_note', CommonConstant::DATABASE_VALUE_NO)
            ->whereRaw('est_charge.lease_out_id IS NOT NULL')
            ->select([
                'lease_out.lease_out_id',
                'lease_out.lease_out_code',
                'lease_out.lease_out_desc',
                'est_charge.est_charge_id',
                'est_charge.est_charge_code',
                'est_charge.est_charge_desc',
                'est_charge.rent',
                'est_charge.annualised_rent',
                'est_charge.credit_note',
                \DB::raw(
                    "IF((ISNULL(est_charge_type.est_charge_type_code) AND est_charge.rent = 'Y'),
                    'Rent', est_charge_type.est_charge_type_desc) AS est_charge_type_desc"
                ),
                'est_charge.regular_payment_start_date',
                'est_charge.regular_payment_end_date',
                'est_charge.initial_payment_amount',
                'est_charge.initial_payment_due_date',
                'est_charge.fin_account_id',
                'est_charge.period_freq',
                'est_time_period.est_time_period_code',
                'est_time_period.est_time_period_desc',
                'est_charge.regular_payment_amount',
                'est_charge.last_invoice_generated_date',
                'tax_code.tax_code_id',
                'tax_code.rate',
                \DB::raw(
                    'IF(est_charge.credit_note = "' . CommonConstant::DATABASE_VALUE_YES . '",
                    est_charge.initial_payment_amount, est_charge.regular_payment_amount) as instalment'
                ),
                'est_charge.initial_payment_due_date',
                \DB::raw(
                    'IF(est_charge.active ="'
                    . CommonConstant::DATABASE_VALUE_YES
                    . '","Active", "Inactive") as status'
                ),
                \DB::raw(
                    'IF(est_charge.payment_in_advance ="'
                    . CommonConstant::DATABASE_VALUE_YES
                    . '","In Advance", "In Arrears") as terms'
                ),
                \DB::raw(
                    'IF(est_charge.tax_code_id IS NOT NULL,
                    est_charge.regular_payment_amount * tax_code.rate / 100,
                    0) as vat_amount'
                )
            ])
            ->orderBy('lease_out.lease_out_code')
            ->orderBy('est_charge.regular_payment_start_date', 'ASC');
        return $query;
    }

    private function generateFilterData($filterData, &$whereCodes, &$whereTexts, &$orCodes)
    {
        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Lease Out Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Lease Out Description contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);
        }

        if ($val = Common::iset($filterData['owner'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Owner']);
        }

        if ($val = Common::iset($filterData['holdingOver'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.holding_over_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.holding_over_no'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.holding_over_all'));
                    break;
            }
        }

        if ($val = array_get($filterData, 'holdingOverFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.holding_over_from'), $val));
        }

        if ($val = array_get($filterData, 'holdingOverTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.holding_over_to'), $val));
        }

        if ($val = array_get($filterData, 'agreementDateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.agreement_date_from'), $val));
        }

        if ($val = array_get($filterData, 'agreementDateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.agreement_date_to'), $val));
        }

        if ($val = array_get($filterData, 'startDateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.start_date_from'), $val));
        }

        if ($val = array_get($filterData, 'startDateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.start_date_to'), $val));
        }

        if ($val = array_get($filterData, 'endDateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.end_date_from'), $val));
        }

        if ($val = array_get($filterData, 'endDateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.end_date_to'), $val));
        }

        if ($val = Common::iset($filterData['rates_liability_id'])) {
            array_push($whereCodes, ['rates_liability', 'rates_liability_id', 'rates_liability_code', $val, "Rates"]);
        }

        if ($val = Common::iset($filterData['insce_liability_id'])) {
            array_push(
                $whereCodes,
                [
                    'insce_liability',
                    'insce_liability_id',
                    'insce_liability_code',
                    $val,
                    "Insurance"
                ]
            );
        }

        if ($val = Common::iset($filterData['estate_maint_id'])) {
            array_push($whereCodes, ['estate_maint', 'estate_maint_id', 'estate_maint_code', $val, "Maintenance"]);
        }

        if ($val = Common::iset($filterData['leaseOutStatus'])) {
            array_push(
                $whereCodes,
                ['lease_out_status', 'lease_out_status_id', 'lease_out_status_code', $val, 'Status']
            );
        }

        if ($val = Common::iset($filterData['landlord'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Landlord']);
        }

        if ($val = Common::iset($filterData['contact'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Tenant']);
        }

        if ($val = Common::iset($filterData['leaseType'])) {
            array_push($whereCodes, ['lease_type', 'lease_type_id', 'lease_type_code', $val, 'Lease Type']);
        }

        if ($val = Common::iset($filterData['leaseSubType'])) {
            array_push(
                $whereCodes,
                ['est_lease_sub_type', 'est_lease_sub_type_id', 'est_lease_sub_type_code', $val, 'Lease Sub Type']
            );
        }

        if ($val = Common::iset($filterData['chargeType'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, "Charge/Rent = 'Rent'");
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, "Charge/Rent = 'Charge'");
                    break;
                default:
                    array_push($whereTexts, "Charge/Rent = 'All'");
                    break;
            }
        }

        if ($val = Common::iset($filterData['estChargeTypeId'])) {
            array_push(
                $whereCodes,
                ['est_charge_type', 'est_charge_type_id', 'est_charge_type_code', $val, "Charge Type"]
            );
        }

        if ($val = Common::iset($filterData['account'])) {
            array_push(
                $whereCodes,
                ['fin_account', 'fin_account_id', 'fin_account_code', $val, 'Account Code']
            );
        }

        if ($val = Common::iset($filterData['landlordTenantAct'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.landlord_tenant_act_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.landlord_tenant_act_no'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.landlord_tenant_act_all'));
                    break;
            }
        }

        $this->reAddUserDefinesQuery(GenTable::LEASE_OUT, $filterData, $whereCodes, $whereTexts);
    }
}
