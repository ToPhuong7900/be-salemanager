<?php

namespace Tfcloud\Services\Report\PhpExcel;

use Carbon\Carbon;
use Box\Spout\Common\Entity\Style\Border as SpoutBorder;
use Box\Spout\Common\Entity\Style\Color;
use Box\Spout\Writer\Common\Creator\Style\BorderBuilder;
use Box\Spout\Writer\Common\Creator\Style\StyleBuilder;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Exception;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Constant\ReportConstant;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Traits\EmailTrait;
use Tfcloud\Models\IdworkStatusType;
use Tfcloud\Models\Report;
use Tfcloud\Services\Admin\General\FinYearService;
use Tfcloud\Services\PermissionService;

class CNDCLICFF01ElementService extends PhpExcelReportBaseService
{
    use EmailTrait;

    private $reportHeaders = [
        'element_type' => 'ELEMENT TYPE',
        'block' => 'BLOCK',
        'room' => 'ROOM'
    ];

    private $reportYearHeaders = [];

    private $finYearHeaders = [];

    //Spout engine
    private $writer;
    private $reportBoxFilterLoader = null;

    private $finYearService;

    private $whereCodes = [];
    private $orCodes = [];
    private $whereTexts = [];

    public function __construct(PermissionService $permissionService, Report $report = null)
    {
        parent::__construct($permissionService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
        $this->finYearService = new FinYearService($this->permissionService);
    }

    /**
     * @note: main function
     * @param $repExcelFile
     * @param $inputs
     * @param array $options
     * @param string $filterText
     * @return bool
     */
    public function generateReport($repExcelFile, $inputs, $options = [], &$filterText = '')
    {
        $result = true;
        try {
            $this->writer = WriterEntityFactory::createXLSXWriter();
            $this->writer->openToFile($repExcelFile);
            $data = $this->getidworkItemData($inputs, $filterText);

            $this->buildHeader();
            $this->writeBody($data, $inputs, $filterText);
            $this->writeFooter($data, $filterText);

            $this->writer->close();
        } catch (Exception $ex) {
            $result = false;
            \Log::error('CNDCLICFF01ElementService@generateReport: ' . $ex->getMessage());
        }

        if (array_get($inputs, 'send_email', false)) {
            $user = \Auth::user();
            $fileName = ReportConstant::SYSTEM_REPORT_CND_CLI_CFF01;
            $emailData = array(
                'mailTo'        => $user->email_reply_to,
                'displayName'   => $user->siteGroup->email_return_address,
                'repTitle'      => $fileName,
                'subject'       => $fileName . ' Report',
            );
            return $this->sendEmail($repExcelFile, $emailData);
        }

        return $result;
    }

    private function buildHeader()
    {
        $date = new \DateTime('now');
        $year1 = $this->finYearService->getFinYearFromDate($date);

        $date->modify('1 year');
        $year2 = $this->finYearService->getFinYearFromDate($date);

        $date->modify('1 year');
        $year3 = $this->finYearService->getFinYearFromDate($date);

        $date->modify('1 year');
        $year4 = $this->finYearService->getFinYearFromDate($date);

        $date->modify('1 year');
        $year5 = $this->finYearService->getFinYearFromDate($date);

        //Used for processing in the writeBody method.
        $this->finYearHeaders = [
               'year_current_1' => $year1 ? $year1->fin_year_code : '',
               'year_current_2' => $year2 ? $year2->fin_year_code : '',
               'year_current_3' => $year3 ? $year3->fin_year_code : '',
               'year_current_4' => $year4 ? $year4->fin_year_code : '',
               'year_current_5' => $year5 ? $year5->fin_year_code : '',
        ];

        //Format year headers
        $this->reportYearHeaders = [
               'year_current_1' => $year1 ? $this->fomatHeaders($year1->fin_year_code) : '',
               'year_current_2' => $year2 ? $this->fomatHeaders($year2->fin_year_code) : '',
               'year_current_3' => $year3 ? $this->fomatHeaders($year3->fin_year_code) : '',
               'year_current_4' => $year4 ? $this->fomatHeaders($year4->fin_year_code) : '',
               'year_current_5' => $year5 ? $this->fomatHeaders($year5->fin_year_code) : '',
        ];
    }


    private function writerHeader()
    {
        $headerStyle = (new StyleBuilder())->setFontBold()->build();
        $headers = array_values($this->reportHeaders);

        $reportHeaderYearValues = array_values($this->reportYearHeaders);

        $headers = array_values(array_merge($headers, $reportHeaderYearValues));

        $this->writer->addRow(WriterEntityFactory::createRowFromArray($headers, $headerStyle));
    }

    private function fomatHeaders($headerValue)
    {
        //Get everything before the slash.
        //Then get the last 2 characters.
        return  substr($headerValue, 0, strrpos($headerValue, '/')) . '/' . substr($headerValue, -2);
    }

    private function writeBody($data, $inputs, $filterText)
    {
        $condSurveyIdTrack = null;
        $elementIdTrack = null;

       // $condCounter = 0;//debug
        foreach ($data as $record) {
           // $condCounter++;

           // \log::error($condCounter);
           // \log::error('survey counter');

            if ($condSurveyIdTrack != $record['condsurvey_id']) {
                $this->writeLine();
                $condSurveyIdTrack = $record['condsurvey_id'];
                $condSurveyStyle = (new StyleBuilder())->setFontBold()->build();
                $this->writer->addRow(WriterEntityFactory
                    ::createRowFromArray([$record['school_name_site']], $condSurveyStyle));
                $this->writer->addRow(WriterEntityFactory
                    ::createRowFromArray([$record['cond_survey_user_sel1']], $condSurveyStyle));
                $this->writerHeader();
            }

            if ($elementIdTrack != $record['element_id']) {
                $elementIdTrack = $record['element_id'];
                $elementStyle = (new StyleBuilder())->setFontBold()->build();
                $this->writer->addRow(WriterEntityFactory
                    ::createRowFromArray([$record['element_desc']], $elementStyle));
            }

            $itemData = [0 => $record['idwork_item_desc'],
                        1 => $record['block_code_desc'],
                        2 => $record['room_user_text2'],
                        'year_current_1' => '',
                        'year_current_2' => '',
                        'year_current_3' => '',
                        'year_current_4' => '',
                        'year_current_5' => ''
                    ];

                //Cyclical logic for year column
                //Find the year column based on Target Year and put X as the value.
            if ($record['fin_year_code'] != "") {
                $cyclicalNoYearKey = null;
                foreach ($this->finYearHeaders as $key => $val) {
                    if ($record['fin_year_code'] == $val) {
                        $cyclicalNoYearKey = $key;
                        break;
                    }
                }

                if ($cyclicalNoYearKey) {
                    $itemData[$cyclicalNoYearKey] = html_entity_decode('&#x2713;');
                }


                $cycleLengthYrs = $record['cycle_length'];

                if ($record['cyclical_maintenance'] == 'Y') {
                    if ($cycleLengthYrs) {
                        //If you find direct match using the id work target year.
                        //E.g. 2022/2023 exists in the report and the target year matches.
                        if ($cyclicalNoYearKey) {
                            $pos = array_search($cyclicalNoYearKey, array_keys($this->finYearHeaders));
                            $newfYHeaders = array_slice($this->finYearHeaders, $pos);


                            $counter = 0;
                            foreach ($newfYHeaders as $key => $val) {
                                $counter++;

                                if ($key === array_key_first($newfYHeaders)) {
                                    //set the tick for first year
                                    $itemData[$key] =  html_entity_decode('&#x2713;');
                                    $counter = 0;
                                    continue; // go to next year in loop
                                }

                                if ($cycleLengthYrs == $counter) {
                                    $itemData[$key] =  html_entity_decode('&#x2713;');
                                    $counter = 0;
                                }
                            }
                        } else {
                            //Find the target year column to tick using the cycle_length sequence
                            //E.g. Current year = 2021/2022. Length years = 2.
                            //Look for 2023/2024 in the report columns and tick
                            $finyear = \Tfcloud\Models\FinYear::userSiteGroup()
                                    ->where('year_start_date', '>=', $record['year_start_date'])
                                    ->orderBy('year_start_date')
                                    ->get();
                            // \Log::info(print_r($finyear, true));

                            $newFyColumnKey = null;
                            foreach ($finyear as $fy) {
                                $date = \DateTime::createFromFormat('Y-m-d', $fy->year_start_date);
                                $date->modify($cycleLengthYrs . ' year');

                                $fyDate = $this->finYearService->getFinYearFromDate($date);

                                if ($fyDate) {
                                    if (in_array($fyDate->fin_year_code, $this->finYearHeaders)) {
                                        $newFyColumnKey = array_search($fyDate->fin_year_code, $this->finYearHeaders);
                                        break;
                                    }
                                }
                            }

                            if ($newFyColumnKey) {
                                //Now assign the ticks
                                $pos = array_search($newFyColumnKey, array_keys($this->finYearHeaders));
                                $newfYHeaders = array_slice($this->finYearHeaders, $pos);


                                $counter = 0;
                                foreach ($newfYHeaders as $key => $val) {
                                    $counter++;

                                    if ($key === array_key_first($newfYHeaders)) {
                                        //set the tick for first year
                                        $itemData[$key] =  html_entity_decode('&#x2713;');
                                        $counter = 0;
                                        continue; // go to next year in loop
                                    }

                                    if ($cycleLengthYrs == $counter) {
                                        $itemData[$key] =  html_entity_decode('&#x2713;');
                                        $counter = 0;
                                    }
                                }
                            }
                        }
                    }
                }
            }


            //Attempt at formatting the cell to text to allow spacing.
//            $row = WriterEntityFactory::createRowFromArray($itemData);
//
//            $cell = $row->getCellAtIndex(0);
//            $cell->setValue("\tTest");
//            $styleBuilder = new StyleBuilder();
//            $styleBuilder->setShouldWrapText(true);
//            $styleBuilder->setFormat('@');
//            $style = $styleBuilder->build();
//            $cell->setStyle($style);
//
//
//            $this->writer->addRow($row);

            $this->writer->addRow(WriterEntityFactory::createRowFromArray($itemData));
        }
    }

    private function getidworkItemData($inputs, &$filterText)
    {
        $select = [
            //Condition Survey Data
            'idwork.condsurvey_id',
            'site.site_desc AS school_name_site',

            \DB::raw("CONCAT_WS(' – ', "
                    . "gen_userdef_sel1.gen_userdef_sel_code, "
                    . "gen_userdef_sel1.gen_userdef_sel_desc) AS cond_survey_user_sel1"),
            //Condition Survey Data End

            //Element Data
            'idwork.element_id',
            'idwork_element.idwork_element_desc AS element_desc',
            //Element Data End

//            \DB::raw("CONCAT(char(9)+ idwork_item.idworkitem_desc, '"
//                    . " - IDENTIFIED WORK','-',idwork_code) as 'idwork_item_desc'"),

            'idwork_item.idworkitem_desc AS idwork_item_desc',

            \DB::raw("CONCAT_WS(' – ', "
                    . "building_block.building_block_code, "
                    . "building_block.building_block_desc) AS block_code_desc"),
            'iw_userdef_group.user_text2 as room_user_text2',

            'cyclical_maintenance',
            'fin_year.fin_year_code',
            'fin_year.year_start_date',
            'idwork.cycle_length',

            'site.site_id',
            'idwork.building_id',
            'site.site_type_id',
            'gen_userdef_sel.gen_userdef_sel_id'
        ];

        $query = \Tfcloud\Models\Idwork::UserSiteGroup()
            ->select($select)

            ->join('condsurvey', 'condsurvey.condsurvey_id', '=', 'idwork.condsurvey_id')
            ->leftJoin('site', 'site.site_id', '=', 'condsurvey.site_id')
            ->leftJoin(
                'gen_userdef_group AS cond_userdef_group',
                'cond_userdef_group.gen_userdef_group_id',
                '=',
                'condsurvey.gen_userdef_group_id'
            )
            ->leftJoin(
                'gen_userdef_sel AS gen_userdef_sel1',
                'gen_userdef_sel1.gen_userdef_sel_id',
                '=',
                'cond_userdef_group.sel1_gen_userdef_sel_id'
            )

            ->leftJoin(
                'gen_userdef_group AS iw_userdef_group',
                'iw_userdef_group.gen_userdef_group_id',
                '=',
                'idwork.gen_userdef_group_id'
            )
            ->leftJoin(
                'gen_userdef_sel',
                'gen_userdef_sel.gen_userdef_sel_id',
                '=',
                'iw_userdef_group.sel1_gen_userdef_sel_id'
            )

            ->join('idwork_element', 'idwork_element.idwork_element_id', '=', 'idwork.element_id')
            ->join('idwork_item', 'idwork_item.idwork_item_id', '=', 'idwork.item_id')
            ->leftJoin('building_block', 'building_block.building_block_id', '=', 'idwork.building_block_id')
            ->leftJoin('fin_year', 'fin_year.fin_year_id', '=', 'idwork.target_fin_year_id')

            //->where('idwork_id', 80466) //debug

            ->whereNotIn('idwork.idwork_status_id', [IdworkStatusType::SUPERSEDED, IdworkStatusType::COMPLETE])
            ->orderBy('condsurvey.condsurvey_code')
            ->orderBy('idwork_element.idwork_element_code')
            ->orderBy('idwork_item.idwork_item_code');

        //filter and log
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $data = $reportFilterQuery->filterAll($query)->get();
            $filterText = $reportFilterQuery->getFilterDetailText();
        }


        return $data;
    }


    private function writeFooter($data, &$filterText)
    {
        $bFilterDetail = false;
        $this->getFiltering(
            $this->whereCodes,
            $this->orCodes,
            $this->whereTexts,
            $bFilterDetail,
            $filterText
        );

        $reportTitle = array_get($this->returnedData, 'repTitle', ReportConstant::SYSTEM_REPORT_CND_CLI_CFF01);
        $this->writeLine();
        $this->writeLine("Report: $reportTitle");

        if ($filterText) {
            $this->writeLine('Filtering details:');
            $this->writeLine($filterText);
        }

        $this->writeLine('Ordering details:');
        $this->writeLine('`Condition Survey Code` (ascending), `Element Code` (ascending), Item Code` (ascending)');

        $total = count($data);
        if ($total) {
            $this->writeLine('Rows returned:');
            $this->writeLine("Returned $total rows (from 1 to $total) of $total rows.");
        } else {
            $this->writeLine();
            $this->writeLine('No data found.');
        }
    }

    private function writeLine($value = '')
    {
        $this->writer->addRow(WriterEntityFactory::createRowFromArray([$value]));
    }
}
