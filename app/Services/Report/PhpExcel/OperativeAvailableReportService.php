<?php

namespace Tfcloud\services\Report\PhpExcel;

use Box\Spout\Writer\Common\Creator\Style\StyleBuilder;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Carbon\Carbon;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Constant\ReportConstant;
use Tfcloud\Models\CrudAccessLevel;
use Tfcloud\Models\Module;
use Tfcloud\Models\Report;
use Tfcloud\Models\Views\VwDlo04;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Personnel\DayOffService;
use Tfcloud\Services\Personnel\UserWorkPatternService;
use Tfcloud\Services\Report\FilterQuery\DloJobsFilterQueryService;

class OperativeAvailableReportService extends PhpExcelReportBaseService
{
    private $reportHeaders = [
        'job_code' => 'Job Code',
        'job_type_code' => 'Job Type Code',
        'job_desc' => 'Job Description',
        'created_time' => 'Created Time',
        'external_reference' => 'External Reference',
        'site_code' => 'Site Code',
        'site_desc' => 'Site Description',
        'building_code' => 'Building Code',
        'building_desc' => 'Building Desc',
        'room_num' => 'Room Number',
        'room_desc' => 'Room Description',
        'plant_code' => 'Plant Code',
        'plant_desc' => 'Plant Description',
        'job_status_type_code' => 'Job Status Type Code',
        'job_status_type_desc' => 'Job Status Type Description',
        'job_status_code' => 'Job Status Code',
        'created_date' => 'Created Date',
        'job_status_desc' => 'Job Status Description',
        'owner_user_name' => 'Owner Username',
        'job_manager_username' => 'Job Manager Username',
        'surveyor_username' => 'Surveyor Username',
        'job_priority_code' => 'Job Priority Code',
        'job_priority_desc' => 'Job Priority Description',
        'target_respond_date' => 'Target Respond Date',
        'target_respond_time' => 'Target Respond Time',
        'actual_respond_date' => 'Actual Respond Date',
        'actual_respond_time' => 'Actual Respond Time',
        'target_complete_date' => 'Target Complete Date',
        'target_complete_time' => 'Target Complete Time',
        'actual_complete_date' => 'Actual Complete Date',
        'actual_complete_time' => 'Actual Complete Time',
        'estimate' => 'Estimate',
        'estimate_hrs' => 'Estimate (hrs)',
        'account_code' => 'Account Code',
        'account_desc' => 'Account Description',
        'comments' => 'Comments',
    ];

    private $reportOperativeHeader = [
        'operative_record_code' => 'Operative Record Code',
        'planned_start_date' => 'Planned Start Date',
        'planned_start_time' => 'Planned Start Time',
        'planned_end_date' => 'Planned End Date',
        'planned_end_time' => 'Planned End Time',
        'operative' => 'Operative',
        'operative_available' => 'Operative Available',
        'operative_not_available_reason' => 'Operative Not Available Reason',
        'completed' => 'Completed',
    ];

    private $writer;
    private $hasPersonnelAccess;
    private $whereCodes = [];
    private $orCodes = [];
    private $whereTexts = [];
    private $dloJobsFilterQueryService;
    private $dayOffService;
    private $userWorkPatternService;
    private $reportBoxFilterLoader = null;

    public function __construct(
        PermissionService $permissionService,
        Report $report = null
    ) {
        parent::__construct($permissionService);
        $this->dloJobsFilterQueryService = new DloJobsFilterQueryService($permissionService);
        $this->dayOffService = new DayOffService($permissionService);
        $this->userWorkPatternService = new UserWorkPatternService($permissionService);
        $this->hasPersonnelAccess = $this->permissionService->hasModuleAccess(
            Module::MODULE_PERSONNEL,
            CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
        );
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    protected function validateFilter($inputs)
    {
        $inputs = $this->formatInputData($inputs);
        $rules = [
            'plannedDateFrom' => ['required', 'date_format:d/m/Y'],
            'plannedDateTo' => ['required', 'date_format:d/m/Y'],
        ];

        $messages = [
            'plannedDateFrom.required' => 'Planned Date From is required.',
            'plannedDateTo.required' => 'Planned Date To is required.',
        ];
        $validator = \Validator::make($inputs, $rules, $messages);
        $validator->success = $validator->passes();

        if ($validator->success) {
            $maximumSearchPeriod = \Auth::user()->siteGroup()->select(['dlo_maximum_operative_search_period'])
                ->first()->dlo_maximum_operative_search_period;
            $fromDate = Carbon::createFromFormat('d/m/Y', $inputs['plannedDateFrom']);
            $inputs['plannedDateFrom'] = $fromDate->toDateString();
            $toDate = Carbon::createFromFormat('d/m/Y', $inputs['plannedDateTo']);
            $inputs['plannedDateTo'] = $toDate->toDateString();
            $maxEndDate = $fromDate->addDay($maximumSearchPeriod);
            $rules = [
                'plannedDateFrom' => ['before_or_equal:' . $toDate->toDateString()],
                'plannedDateTo' => ['before_or_equal:' . $maxEndDate->toDateString()],
            ];
            $messages = [
                'plannedDateFrom.before_or_equal' => 'Planned Date From must be before or equal Planned Date To.',
                'plannedDateTo.before_or_equal' => 'Planned Date range you provided is too large. ' .
                    'Planned Date To should be on or before ' . $maxEndDate->format('d/m/Y') . '.',
            ];
            $validator = \Validator::make($inputs, $rules, $messages);
        }

        return $validator;
    }

    public function generateReport($repExcelFile, $inputs, $options = [], &$filterText = '')
    {
        $result = true;
        try {
            $this->writer = WriterEntityFactory::createXLSXWriter();
            $this->writer->openToFile($repExcelFile);
            $data = $this->getData($inputs, $filterText);

            //write excel
            $this->writerHeader();
            $this->writeBody($data);
            $this->writeFooter($data, $filterText);

            $this->writer->close();
        } catch (\Exception $ex) {
            $result = false;
            \Log::error('OperativeAvailableReportService@generateReport: ' . $ex->getMessage());
        }
        return $result;
    }

    private function writerHeader()
    {
        if (!$this->hasPersonnelAccess) {
            unset($this->reportOperativeHeader['operative_not_available_reason']);
        }
        $headerStyle = (new StyleBuilder())->setFontBold()->build();
        $headers = array_values(array_merge($this->reportHeaders, $this->reportOperativeHeader));
        $this->writer->addRow(WriterEntityFactory::createRowFromArray($headers, $headerStyle));
    }

    private function writeBody($data)
    {
        foreach ($data as $record) {
            $this->writer->addRow(WriterEntityFactory::createRowFromArray($record));
        }
    }

    private function writeFooter($data, &$filterText)
    {
        $bFilterDetail = false;
        $this->getFiltering(
            $this->whereCodes,
            $this->orCodes,
            $this->whereTexts,
            $bFilterDetail,
            $filterText
        );

        $reportTitle = array_get($this->returnedData, 'repTitle', ReportConstant::SYSTEM_REPORT_DLO21);
        $this->writeLine();
        $this->writeLine("Report: $reportTitle");

        if ($filterText) {
            $this->writeLine('Filtering details:');
            $this->writeLine($filterText);
        }

        $this->writeLine('Ordering details:');
        $this->writeLine('`Job Code` (ascending)');

        $total = count($data);
        if ($total) {
            $this->writeLine('Rows returned:');
            $this->writeLine("Returned $total rows (from 1 to $total) of $total rows.");
        } else {
            $this->writeLine();
            $this->writeLine('No data found.');
        }
    }

    private function writeLine($value = '')
    {
        $this->writer->addRow(WriterEntityFactory::createRowFromArray([$value]));
    }

    private function getData($inputs, &$filterText)
    {
        $jobQuery = new VwDlo04();
        $jobQuery = $jobQuery->select(array_merge(array_values($this->reportHeaders), [
            'dlo_job_operative.dlo_job_operative_code',
            \DB::raw("DATE_FORMAT(dlo_job_operative.from_date, '%d/%m/%Y') as from_date"),
            'dlo_job_operative.from_time',
            \DB::raw("DATE_FORMAT(dlo_job_operative.to_date, '%d/%m/%Y') as to_date"),
            'dlo_job_operative.to_time',
            'user.display_name',
            \DB::raw("'Y' as operative_available"),
            \DB::raw("'' as operative_not_available_reason"),
            'dlo_job_operative.completed',
            'user.id'
        ]))
            ->join('dlo_job_operative', 'dlo_job_operative.dlo_job_id', '=', 'vw_dlo04.dlo_job_id')
            ->join('user', 'user.id', '=', 'dlo_job_operative.user_id');

        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $reportFilterQuery->setViewName('vw_dlo04');
            $reportFilterQuery->handleReportMode();
            $jobQuery = $reportFilterQuery->filterAll($jobQuery);
            $inputs = $this->formatInputData($inputs);
            $filterText = $reportFilterQuery->getFilterDetailText();
        } else {
            $jobQuery = $this->dloJobsFilterQueryService->reAddDloJobQuery(
                $inputs,
                'vw_dlo04',
                $jobQuery,
                $this->whereCodes,
                $this->orCodes,
                $this->whereTexts
            );
            $jobQuery = $this->filterPlannedDate($inputs, $jobQuery);
        }
        $jobQuery = $this->permissionService->listViewSiteAccess($jobQuery, "vw_dlo04.site_id");

        $jobQuery->where('vw_dlo04.site_group_id', \Auth::User()->site_group_id);

        $jobData = $jobQuery->orderBy('vw_dlo04.dlo_job_code')
            ->get();
        $data = $jobData->toArray();
        if (PermissionService::moduleIsLicensed('personnel')) {
            $ids = array_unique($jobData->pluck('id')->toArray());
            $startDate = array_get($inputs, 'plannedDateFrom') ?: date(CommonConstant::FORMAT_DATE);
            $endDate = array_get($inputs, 'plannedDateTo') ?: $startDate;
            $personnelInputs = [
                'userIds' => $ids,
                'startDate' => $startDate,
                'endDate' => $endDate
            ];
            $dayOffs = $this->dayOffService->getDayOff($personnelInputs, true);
            $userWorkPatterns = $this->userWorkPatternService->getDataForDloPlanning($personnelInputs);
            foreach ($data as &$record) {
                $this->userWorkPatternService->setIfOperativeAvailable($record, $dayOffs, $userWorkPatterns);
                $record['id'] = '';
                if (!$this->hasPersonnelAccess) {
                    unset($record['operative_not_available_reason']);
                }
            }
        } else {
            $data = array_map(function ($record) {
                unset($record['operative_not_available_reason']);
                unset($record['id']);
                return $record;
            }, $data);
        }

        return $this->filterOperativeAvailable($inputs, $data);
    }

    private function filterOperativeAvailable($inputs, $data)
    {
        $val = array_get($inputs, 'operative_available');
        if ($val) {
            $data = array_filter($data, function ($record) use ($val) {
                if ($record['operative_available'] == $val) {
                    return true;
                }
                return false;
            });
        }

        return $data;
    }

    private function filterPlannedDate($inputs, $jobQuery)
    {
        $from = array_get($inputs, 'plannedDateFrom') ?: date(CommonConstant::FORMAT_DATE);
        if ($from) {
            $dateFrom = Carbon::createFromFormat('d/m/Y', $from);
            $jobQuery->where(
                \DB::raw("DATE_FORMAT(dlo_job_operative.from_date, '%Y-%m-%d')"),
                '>=',
                $dateFrom->format('Y-m-d')
            );
            array_push($this->whereTexts, 'Planned Date From:' . $from);
        }

        $to = array_get($inputs, 'plannedDateTo') ?: $from;
        if ($to) {
            $dateTo = Carbon::createFromFormat('d/m/Y', $to);
            $jobQuery->where(
                \DB::raw("DATE_FORMAT(dlo_job_operative.from_date, '%Y-%m-%d')"),
                '<=',
                $dateTo->format('Y-m-d')
            );
            array_push($this->whereTexts, 'Planned Date To:' . $to);
        }
        return $jobQuery;
    }

    private function formatInputData($inputs)
    {
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $plannedDate = $reportFilterQuery->getValueFilterField('from_date');
            if ($plannedDate) {
                foreach ($plannedDate as $dates) {
                    $inputs['plannedDateFrom'] = $dates[0] ? Common::dateFieldDisplayFormat($dates[0]) : null;
                    $inputs['plannedDateTo'] = $dates[1] ? Common::dateFieldDisplayFormat($dates[1]) : null;
                }
            }

            $operativeAvailable = $reportFilterQuery->getValueFilterField('operative_available');
            $inputs['operative_available'] = $operativeAvailable[0] ?? null;
        }
        return $inputs;
    }
}
