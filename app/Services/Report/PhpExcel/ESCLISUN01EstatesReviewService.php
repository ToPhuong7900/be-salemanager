<?php

namespace Tfcloud\Services\Report\PhpExcel;

use Illuminate\Support\Arr;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Constant\ReportConstant;
use Tfcloud\Lib\Filters\Estate\LeaseOutFilter;
use Tfcloud\Lib\Permissions\RuralEstatesPermission;
use Tfcloud\Models\EstCharge;
use Tfcloud\Models\EstTimePeriod;
use Tfcloud\Models\EstUnitType;
use Tfcloud\Models\FinYear;
use Tfcloud\Models\GenTable;
use Tfcloud\Models\LeaseOutLetu;
use Tfcloud\Models\LettableUnit;
use Tfcloud\Models\Report;
use Tfcloud\Services\Admin\General\FinYearService;
use Tfcloud\Services\Estate\InvoiceGenerationPlanService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Lib\Traits\EmailTrait;
use DateTime;

class ESCLISUN01EstatesReviewService extends PhpExcelReportBaseService
{
    use EmailTrait;

    private $invoiceGenerationPlanService;
    private ?ClassMapReportLoader $reportBoxFilterLoader;

    public function __construct(
        PermissionService $permissionService,
        Report $report
    ) {
        parent::__construct($permissionService);

        $this->invoiceGenerationPlanService = new InvoiceGenerationPlanService($permissionService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function generateReport($repExcelFile, $inputs, $options = [], &$filterText = '')
    {
        $isPrintReport = array_get($options, 'print-report', false);
        if ($isPrintReport && !$repExcelFile) {
            $reportFileName = ReportConstant::SYSTEM_REPORT_ES_CLI_SUN01;
            $repExcelFile = $this->generateReportOutPutPath($reportFileName, 'xlsx');
        }

        $templateFile = app_path()
            . DIRECTORY_SEPARATOR . "templates"
            . DIRECTORY_SEPARATOR . "phpExcel"
            . DIRECTORY_SEPARATOR . 'ESCLISUN01EstatesReview.xlsx';
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader("Xlsx");
        $phpExcelObj = $reader->load($templateFile);
        if (is_readable($templateFile)) {
            copy($templateFile, $repExcelFile);
        }
        $this->setProperties($phpExcelObj);
        $phpExcelObj->getActiveSheet()->setTitle('Estate reviews Report');
        $this->formatInputData($inputs);
        $data = $this->getReportData($inputs);
        $financialRangesPrint = $data['financialRangesPrint'];
        $this->fillMonths($phpExcelObj, $financialRangesPrint);
        $lastRow = $this->fillDataToExcel($phpExcelObj, $data['rows']);
        $this->formatExcel($phpExcelObj, $lastRow);

        $errorMessages = null;
        if (is_null($errorMessages)) {
            $errorMessages = $this->writeExcelFile($phpExcelObj, $repExcelFile, '', true);
        }

        if ($errorMessages) {
            return false;
        }

        if (array_get($inputs, 'send_email', false)) {
            $user = \Auth::user();
            $fileName = ReportConstant::SYSTEM_REPORT_ES_CLI_SUN01;
            $emailData = array(
                'mailTo' => $user->email_reply_to,
                'displayName' => $user->siteGroup->email_return_address,
                'repTitle' => $fileName,
                'subject' => $fileName . ' Report',
            );
            return $this->sendEmail($repExcelFile, $emailData);
        }

        $whereCodes = [];               // The codes which have been used in the filter
        $whereTexts = [];               // Plain text about filtering applied
        $orCodes = [];                  // csv string of OR codes
        $bFilterDetail = false;

        $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
        $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $filterText);

        if ($isPrintReport) {
            return $repExcelFile;
        }

        return true;
    }

    private function formatExcel(Spreadsheet &$phpExcelObj, $lastRow)
    {
        $phpExcelObj->getActiveSheet()->getStyle("A4:AG$lastRow")
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
        $phpExcelObj->getActiveSheet()->getStyle("A4:AG$lastRow")
            ->getFont()->setSize(8);

        $phpExcelObj->getActiveSheet()->getStyle("O4:AA$lastRow")
            ->getNumberFormat()
            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

        $phpExcelObj->getActiveSheet()->getStyle("D4:E$lastRow")
            ->getNumberFormat()
            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
    }

    protected function validateFilter($inputs)
    {
        $this->formatInputData($inputs);
        return \Validator::make($inputs, $this->validateRules(), $this->validateMessages());
    }

    public function validateRules()
    {
        $rules = [];
        if (!FinYear::current()) {
            $rules = [
                'finYear' => ['required'],
            ];
        }

        return $rules;
    }

    public function validateMessages()
    {
        $messages = [
            'finYear.required' => 'There is no current financial year.'
        ];

        return $messages;
    }

    private function fillDataToExcel(Spreadsheet &$phpExcelObj, $rows)
    {
        $row = 4;
        $cellMapping = $this->getCellMapping();
        foreach ($rows as $rowData) {
            foreach ($cellMapping as $col => $key) {
                if ($key == 'months') {
                    $total = $this->fillMonths($phpExcelObj, $rowData[$key], $row, true);
                    $phpExcelObj->getActiveSheet()->setCellValue('AA' . $row, $total);
                } else {
                    $phpExcelObj->getActiveSheet()->setCellValue($col . $row, $rowData[$key]);
                }
            }
            $row++;
        }

        return $row;
    }

    private function getCellMapping()
    {
        return [
            'A' => 'lettableUnitCode',
            'B' => 'lettableUnitDesc',
            'C' => 'lettableUnitSAPNode',
            'D' => 'lettableUnitAnnualRent',
            'E' => 'lettableUnitMarketRent',
            'F' => 'lettableUnitVacant',
            'G' => 'lettableUnitAvailable',
            'H' => 'tenantAccountId',
            'I' => 'tenantName',
            'J' => 'leaseOutCode',
            'K' => 'leaseOutDesc',
            'L' => 'leaseOutComment',
            'M' => 'leaseOutStartDate',
            'N' => 'leaseOutEndDate',
            'O' => 'months',
            'AB' => 'costCentre',
            'AC' => 'accountCode',
            'AD' => 'paySchedule',
            'AE' => 'extAccountCode',
            'AF' => 'chargeSAPNode',
            //'AG' => 'chargeUserText3'
        ];
    }

    private function fillMonths(Spreadsheet &$phpExcelObj, $financialRanges, $row = 3, $isCost = false)
    {
        $total = 0;
        $col = 'O';
        foreach ($financialRanges as $value => $cost) {
            if ($isCost) {
                $value = $cost;
                $total += $cost;
            }
            $phpExcelObj->getActiveSheet()->setCellValue($col . $row, $value);
            $col++;
        }

        return $total;
    }

    private function getReportData($inputs)
    {
        $currentFnYear = (new FinYearService($this->permissionService))->getFinYearFromDate();
        $startDate = new DateTime($currentFnYear->year_start_date);
        $processUpToDate = $currentFnYear->year_end_date;
        $financialRanges = $this->getMonths($startDate, $processUpToDate);
        $chargeRents = $this->invoiceGenerationPlanService->getPlanItem([
            'process_up_to_date' => $processUpToDate,
            'is_charge_report' => false
        ]);

        $data = [];
        $timePeriodArr = EstTimePeriod::all()->keyBy('est_time_period_id');
        $estChargeQueryMain = $this->getEstChargeQuery($inputs);
        $isLettableFilter = false;
        $lettableUnitQueryMain = $this->getLettableUnitQuery($inputs, $isLettableFilter);

        foreach ($chargeRents as $chargeRent) {
            $lettableUnitQuery = clone $lettableUnitQueryMain;
            $estChargeQuery = clone $estChargeQueryMain;
            $chargeDueDate = new DateTime($chargeRent['charge_due_date']);
            if (($chargeDueDate >= $startDate) && ($chargeRent['inv_on_hold'] == CommonConstant::DATABASE_VALUE_NO)) {
                $estChargePaymentId = $chargeRent['est_charge_payment_id'];
                $estCharge = $estChargeQuery->where('est_charge_id', $chargeRent['est_charge_id'])
                    ->addSelect(\DB::raw("CASE $estChargePaymentId
                            WHEN '1' THEN est_charge.initial_payment_amount
                            WHEN '2' THEN est_charge.regular_payment_amount
                            WHEN '3' THEN est_charge.final_payment_amount
                            ELSE ''
                            END AS `amount`"))->first();

                if ($estCharge) {
                    if (!isset($data[$estCharge->est_charge_id])) {
                        $lettableUnit = $lettableUnitQuery
                            ->where('lel.lease_out_id', $estCharge->lease_out_id)->first();
                        if ($isLettableFilter && !$lettableUnit) {
                            continue;
                        }

                        $finAccount = ($estCharge->finAccount) ? Common::concatFields([
                            $estCharge->finAccount->fin_account_code,
                            $estCharge->finAccount->fin_account_desc
                        ]) : '';

                        $timePeriod = isset($timePeriodArr[$estCharge->repeat_est_time_period_id]) ?
                            $timePeriodArr[$estCharge->repeat_est_time_period_id]->est_time_period_code : '';

                        $data[$estCharge->est_charge_id] = [
                            'months' => $financialRanges,
                            'leaseOutCode' => $estCharge->lease_out_code,
                            'leaseOutDesc' => $estCharge->lease_out_desc,
                            'leaseOutComment' => $estCharge->comment,
                            'leaseOutStartDate' => $estCharge->start_date_formatted,
                            'leaseOutEndDate' => $estCharge->end_date_formatted,
                            'costCentre' => $estCharge->costCenter,
                            'paySchedule' =>
                                Common::concatFields([$estCharge->period_freq, $timePeriod], ['separator' => '.']),
                            'extAccountCode' => $estCharge->user_text3,
                            'accountCode' => $finAccount,
                            'chargeSAPNode' => $estCharge->user_text2,
                            //'chargeUserText3' => '',
                            'tenantAccountId' => $estCharge->supplier_account_ref,
                            'tenantName' => $estCharge->contact_name,
                        ];

                        $data[$estCharge->est_charge_id]['lettableUnitCode']
                            = ($lettableUnit) ? $lettableUnit->lettable_unit_code : '';
                        $data[$estCharge->est_charge_id]['lettableUnitDesc']
                            = ($lettableUnit) ? $lettableUnit->lettable_unit_desc : '';
                        $data[$estCharge->est_charge_id]['lettableUnitSAPNode']
                            = ($lettableUnit) ? $lettableUnit->user_text2 : '';
                        $data[$estCharge->est_charge_id]['lettableUnitAnnualRent']
                            = ($lettableUnit) ? $lettableUnit->annual_rent : '';
                        $data[$estCharge->est_charge_id]['lettableUnitMarketRent']
                            = ($lettableUnit) ? $lettableUnit->market_rent : '';
                        $data[$estCharge->est_charge_id]['lettableUnitVacant']
                            = ($lettableUnit) ? Common::valueYNToYesNo($lettableUnit->vacant) : '';
                        $data[$estCharge->est_charge_id]['lettableUnitAvailable']
                            = ($lettableUnit) ? Common::valueYNToYesNo($lettableUnit->available) : '';
                    }
                    $date = new DateTime($chargeRent['charge_due_date']);
                    $dateFormatted = $date->format('M Y');
                    $data[$estCharge->est_charge_id]['months'][$dateFormatted] += $estCharge->amount;
                }
            }
        }

        $financialRangesPrint = $this->getMonths($startDate, $processUpToDate, $currentFnYear->finYearPeriods()->get()
            ->toArray(), true);

        usort($data, [$this, 'compareByName']);
        return ['financialRangesPrint' => $financialRangesPrint, 'rows' => $data];
    }

    public function compareByName($a, $b, $key = 'leaseOutCode')
    {
        return strcmp($a[$key], $b[$key]);
    }

    private function getEstChargeQuery($inputs)
    {
        $estChargeQuery = EstCharge::userSiteGroup()
            ->join('lease_out', 'lease_out.lease_out_id', '=', 'est_charge.lease_out_id')
            ->leftjoin('gen_userdef_group as GUG', 'est_charge.gen_userdef_group_id', 'GUG.gen_userdef_group_id')
            ->leftjoin('gen_userdef_sel', 'gen_userdef_sel.gen_userdef_sel_id', '=', 'GUG.sel2_gen_userdef_sel_id')
            ->leftjoin('contact', 'lease_out.tenant_contact_id', '=', 'contact.contact_id')
            ->where('rent', CommonConstant::DATABASE_VALUE_YES)
            ->where('credit_note', CommonConstant::DATABASE_VALUE_NO)
            ->select([
                \DB::raw('CONCAT_WS(" - " , gen_userdef_sel.gen_userdef_sel_code, gen_userdef_sel.gen_userdef_sel_desc)
                 AS costCenter'),
                'GUG.user_text1',
                'GUG.user_text2',
                'GUG.user_text3',
                'lease_out.lease_out_code',
                'lease_out.lease_out_desc',
                'lease_out.comment',
                \DB::raw("DATE_FORMAT(lease_out.start_date , '%d/%m/%Y') AS start_date_formatted"),
                \DB::raw("DATE_FORMAT(lease_out.end_date , '%d/%m/%Y') AS end_date_formatted"),
                'contact.supplier_account_ref',
                'contact.contact_name',
                'est_charge.est_charge_id',
                'est_charge.lease_out_id',
                'est_charge.period_freq',
                'est_charge.repeat_est_time_period_id',
                'est_charge.fin_account_id',

            ]);

        return $this->filterAll($estChargeQuery, $inputs);
    }

    private function getLettableUnitQuery($inputs, &$isLettableFilter)
    {
        $lettableUnitQuery = LettableUnit::userSiteGroup()
            ->join('lease_out_letu AS lel', 'lel.lettable_unit_id', 'lettable_unit.lettable_unit_id')
            ->join(
                'gen_userdef_group as GUG',
                'lettable_unit.gen_userdef_group_id',
                'GUG.gen_userdef_group_id'
            )
            ->select([
                'lettable_unit_code',
                'lettable_unit_desc',
                'GUG.user_text2',
                'annual_rent',
                'market_rent',
                'vacant',
                'available',
            ])
            ->where('lettable_unit.est_unit_type_id', EstUnitType::EST_UT_LU);

        if (($inputs['lettable_unit_code'] != '') && !is_null($inputs['lettable_unit_code'])) {
            $lettableUnitQuery->where(
                "lettable_unit.lettable_unit_code",
                'like',
                '%' . $inputs['lettable_unit_code'] . '%'
            );

            $isLettableFilter = true;
        }
        if (($inputs['lettable_unit_desc'] != '') && !is_null($inputs['lettable_unit_desc'])) {
            $lettableUnitQuery->where(
                "lettable_unit.lettable_unit_desc",
                'like',
                '%' . $inputs['lettable_unit_desc'] . '%'
            );
            $isLettableFilter = true;
        }

        return $lettableUnitQuery;
    }

    private function getMonths($start, $end, $finYearPeriods = null, $isPrint = false)
    {
        $ranges = [];
        $dateRange = new \DatePeriod($start, new \DateInterval('P1M'), new DateTime($end));

        foreach ($dateRange as $key => $date) {
            if ($isPrint) {
                $ranges[$finYearPeriods[$key]['fin_year_period_code'] . ' ' . $date->format('Y')] = 0;
            } else {
                $ranges[$date->format('M Y')] = 0;
            }
        }

        return $ranges;
    }


    private function filterAll(
        &$query,
        $inputs,
        $view = false,
        $lettingPacketId = 0,
        $lettableUnitId = 0,
        $isUsingLettable = true
    ) {

        $filter = new LeaseOutFilter($inputs);

        if ($view) {
            $table = $view;
        } else {
            $table = 'lease_out';
            $query->orderBy($filter->sort, $filter->sortOrder);
        }

        if (($filter->code != '') && !is_null($filter->code)) {
            $query->where($table . '.lease_out_code', 'like', '%' . trim($filter->code) . '%');
        }

        if (($filter->description != '') && !is_null($filter->description)) {
            $query->where($table . '.lease_out_desc', 'like', '%' . trim($filter->description) . '%');
        }

        if (($filter->leaseOutStatus != '') && !is_null($filter->leaseOutStatus)) {
            $query->where($table . '.lease_out_status_id', $filter->leaseOutStatus);
        }

        if (($filter->leaseType != '') && !is_null($filter->leaseType)) {
            $query->where($table . '.lease_type_id', $filter->leaseType);
        }

        if (($filter->leaseSubType != '') && !is_null($filter->leaseSubType)) {
            $query->where($table . '.est_lease_sub_type_id', $filter->leaseSubType);
        }

        if (($filter->landlord != '') && !is_null($filter->landlord)) {
            $query->where($table . '.landlord_contact_id', $filter->landlord);
        }

        if (($filter->contact != '') && !is_null($filter->contact)) {
            $query->where($table . '.tenant_contact_id', $filter->contact);
        }

        if (($filter->owner != '') && !is_null($filter->owner)) {
            $query->where($table . '.owner_user_id', $filter->owner);
        }

        if (($filter->holdingOver != '') && !is_null($filter->holdingOver)) {
            if (
                in_array(
                    $filter->holdingOver,
                    [
                        CommonConstant::DATABASE_VALUE_YES,
                        CommonConstant::DATABASE_VALUE_NO
                    ]
                )
            ) {
                $query->where($table . '.holding_over', $filter->holdingOver);
            }
        }

        if (($filter->holdingOverFrom != '') && !is_null($filter->holdingOverFrom)) {
            $holdingOverFrom = DateTime::createFromFormat('d/m/Y', $filter->holdingOverFrom);
            if ($holdingOverFrom) {
                $query->where(
                    \DB::raw("DATE_FORMAT({$table}.holding_over_date , '%Y-%m-%d')"),
                    '>=',
                    $holdingOverFrom->format('Y-m-d')
                );
            }
        }

        if (($filter->holdingOverTo != '') && !is_null($filter->holdingOverTo)) {
            $holdingOverTo = DateTime::createFromFormat('d/m/Y', $filter->holdingOverTo);
            if ($holdingOverTo) {
                $query->where(
                    \DB::raw("DATE_FORMAT({$table}.holding_over_date, '%Y-%m-%d')"),
                    '<=',
                    $holdingOverTo->format('Y-m-d')
                );
            }
        }

        if (($filter->agreementDateFrom != '') && !is_null($filter->agreementDateFrom)) {
            $agreementDateFrom = DateTime::createFromFormat('d/m/Y', $filter->agreementDateFrom);
            if ($agreementDateFrom) {
                $query->where(
                    \DB::raw("DATE_FORMAT({$table}.agreement_date , '%Y-%m-%d')"),
                    '>=',
                    $agreementDateFrom->format('Y-m-d')
                );
            }
        }

        if (($filter->agreementDateTo != '') && !is_null($filter->agreementDateTo)) {
            $agreementDateTo = DateTime::createFromFormat('d/m/Y', $filter->agreementDateTo);
            if ($agreementDateTo) {
                $query->where(
                    \DB::raw("DATE_FORMAT({$table}.agreement_date, '%Y-%m-%d')"),
                    '<=',
                    $agreementDateTo->format('Y-m-d')
                );
            }
        }

        if (($filter->authorisedDateFrom != '') && !is_null($filter->authorisedDateFrom)) {
            $authorisedDateFrom = DateTime::createFromFormat('d/m/Y', $filter->authorisedDateFrom);
            if ($authorisedDateFrom) {
                $query->where(
                    \DB::raw("DATE_FORMAT({$table}.authorised_date , '%Y-%m-%d')"),
                    '>=',
                    $authorisedDateFrom->format('Y-m-d')
                );
            }
        }

        if (($filter->authorisedDateTo != '') && !is_null($filter->authorisedDateTo)) {
            $authorisedDateTo = DateTime::createFromFormat('d/m/Y', $filter->authorisedDateTo);
            if ($authorisedDateTo) {
                $query->where(
                    \DB::raw("DATE_FORMAT({$table}.authorised_date, '%Y-%m-%d')"),
                    '<=',
                    $authorisedDateTo->format('Y-m-d')
                );
            }
        }

        if (($filter->terminationDateFrom != '') && !is_null($filter->terminationDateFrom)) {
            $terminationDateFrom = DateTime::createFromFormat('d/m/Y', $filter->terminationDateFrom);
            if ($terminationDateFrom) {
                $query->where(
                    \DB::raw("DATE_FORMAT({$table}.termination_date , '%Y-%m-%d')"),
                    '>=',
                    $terminationDateFrom->format('Y-m-d')
                );
            }
        }

        if (($filter->terminationDateTo != '') && !is_null($filter->terminationDateTo)) {
            $terminationDateTo = DateTime::createFromFormat('d/m/Y', $filter->terminationDateTo);
            if ($terminationDateTo) {
                $query->where(
                    \DB::raw("DATE_FORMAT({$table}.termination_date, '%Y-%m-%d')"),
                    '<=',
                    $terminationDateTo->format('Y-m-d')
                );
            }
        }

        if (($filter->startDateFrom != '') && !is_null($filter->startDateFrom)) {
            $startDateFrom = DateTime::createFromFormat('d/m/Y', $filter->startDateFrom);
            if ($startDateFrom) {
                $query->where(
                    \DB::raw("DATE_FORMAT({$table}.start_date , '%Y-%m-%d')"),
                    '>=',
                    $startDateFrom->format('Y-m-d')
                );
            }
        }

        if (($filter->startDateTo != '') && !is_null($filter->startDateTo)) {
            $startDateTo = DateTime::createFromFormat('d/m/Y', $filter->startDateTo);
            if ($startDateTo) {
                $query->where(
                    \DB::raw("DATE_FORMAT({$table}.start_date, '%Y-%m-%d')"),
                    '<=',
                    $startDateTo->format('Y-m-d')
                );
            }
        }

        if (($filter->endDateFrom != '') && !is_null($filter->endDateFrom)) {
            $endDateFrom = DateTime::createFromFormat('d/m/Y', $filter->endDateFrom);
            if ($endDateFrom) {
                $query->where(
                    \DB::raw("DATE_FORMAT({$table}.end_date , '%Y-%m-%d')"),
                    '>=',
                    $endDateFrom->format('Y-m-d')
                );
            }
        }

        if (($filter->endDateTo != '') && !is_null($filter->endDateTo)) {
            $endDateTo = DateTime::createFromFormat('d/m/Y', $filter->endDateTo);
            if ($endDateTo) {
                $query->where(
                    \DB::raw("DATE_FORMAT({$table}.end_date, '%Y-%m-%d')"),
                    '<=',
                    $endDateTo->format('Y-m-d')
                );
            }
        }

        if (($filter->rates_liability_id != '') && !is_null($filter->rates_liability_id)) {
            $query->where($table . '.rates_liability_id', $filter->rates_liability_id);
        }

        if (($filter->insce_liability_id != '') && !is_null($filter->insce_liability_id)) {
            $query->where($table . '.insce_liability_id', $filter->insce_liability_id);
        }

        if (($filter->estate_maint_id != '') && !is_null($filter->estate_maint_id)) {
            $query->where($table . '.estate_maint_id', $filter->estate_maint_id);
        }

        if (($filter->ruralLeases != '') && !is_null($filter->ruralLeases)) {
            if (
                in_array(
                    $filter->ruralLeases,
                    [CommonConstant::DATABASE_VALUE_YES, CommonConstant::DATABASE_VALUE_NO]
                )
            ) {
                $query->where($table . '.ruralest_lease', $filter->ruralLeases);
            }
        } elseif (
            !RuralEstatesPermission::hasModuleLicence() ||
            !RuralEstatesPermission::hasModuleAccessRead(false)
        ) {
            // The Rural Leases filter is not displayed if the Rural Estates module is not enabled for the customer.
            // Also, the filter is disabled if the user does not have read access to Rural Estates module.
            // In these cases only select the Standard leases.
            $query->where($table . '.ruralest_lease', CommonConstant::DATABASE_VALUE_NO);
        }

        if (($filter->areaFrom != '') && !is_null($filter->areaFrom)) {
            $query->where($table . '.area_value', '>=', $filter->areaFrom);
        }

        if (($filter->areaTo != '') && !is_null($filter->areaTo)) {
            $query->where($table . '.area_value', '<=', $filter->areaTo);
        }

        if (($filter->areaUnits != '') && !is_null($filter->areaUnits)) {
            $query->where($table . '.area_unit_of_area_id', $filter->areaUnits);
        }

        if (
            ($filter->vatIncluded != '') && !is_null($filter->vatIncluded)
            && in_array(
                $filter->vatIncluded,
                [CommonConstant::DATABASE_VALUE_YES, CommonConstant::DATABASE_VALUE_NO]
            )
        ) {
            $query->where($table . '.vat_included', $filter->vatIncluded);
        }

        if (($filter->landlordTenantAct != '') && !is_null($filter->landlordTenantAct)) {
            if (
                in_array(
                    $filter->landlordTenantAct,
                    [
                        CommonConstant::DATABASE_VALUE_YES,
                        CommonConstant::DATABASE_VALUE_NO
                    ]
                )
            ) {
                $query->where($table . '.landlord_tenant_act', $filter->landlordTenantAct);
            }
        }

        $filter->site = !is_null($filter->site) ?
            $filter->site : (!is_null($filter->site_id) ? $filter->site_id : null);
        if (($filter->site != '') && !is_null($filter->site)) {
            if ($view) {
                $query->where("{$table}.site_id", $filter->site);
            } else {
                $list = LeaseOutLetu::leftJoin(
                    'lettable_unit',
                    'lease_out_letu.lettable_unit_id',
                    '=',
                    'lettable_unit.lettable_unit_id'
                )
                    ->leftJoin(
                        'lettable_unit_item',
                        'lettable_unit.lettable_unit_id',
                        '=',
                        'lettable_unit_item.lettable_unit_id'
                    )
                    ->where('lettable_unit.site_group_id', \Auth::user()->site_group_id)
                    ->where('lettable_unit_item.site_id', $filter->site)
                    ->pluck('lease_out_id')->toArray();

                if (!empty($list)) {
                    $query->whereIn('lease_out.lease_out_id', $list);
                } else {
                    $query->whereIn('lease_out.lease_out_id', ['']);
                }
            }
        }

        if ($lettingPacketId != 0) {
            if ($lettingPacketId == -1) {
                $query->whereNull('lease_out.est_letting_packet_id');
            } else {
                $query->where('lease_out.est_letting_packet_id', $lettingPacketId);
            }
        }

        if ($lettableUnitId != 0) {
            if ($isUsingLettable) {
                $query->where('lease_out_letu.lettable_unit_id', $lettableUnitId);
            } else {
                $list = LeaseOutLetu::where('lettable_unit_id', $lettableUnitId)
                    ->pluck('lease_out_id')->toArray();
                if (!empty($list)) {
                    $query->whereNotIn('lease_out.lease_out_id', $list);
                }
            }
        }
        $this->filterByUserDefines(GenTable::LEASE_OUT, $query, $filter, $table, $view);


        return $query;
    }

    private function generateFilterData($filterData, &$whereCodes, &$whereTexts, &$orCodes)
    {
        if ($val = Common::iset($filterData['lettable_unit_code'])) {
            array_push($whereTexts, "Lettable Unit Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['lettable_unit_desc'])) {
            array_push($whereTexts, "Lettable Unit Description contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Lease In Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Lease In Description contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['leaseType'])) {
            array_push($whereCodes, ['lease_type', 'lease_type_id', 'lease_type_code', $val, 'Lease Type']);
        }

        if ($val = Common::iset($filterData['leaseSubType'])) {
            array_push(
                $whereCodes,
                ['est_lease_sub_type', 'est_lease_sub_type_id', 'est_lease_sub_type_code', $val, 'Lease Sub Type']
            );
        }

        if ($val = Common::iset($filterData['holdingOver'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.holding_over_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.holding_over_no'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.holding_over_all'));
                    break;
            }
        }

        if ($val = array_get($filterData, 'holdingOverFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.holding_over_from'), $val));
        }

        if ($val = array_get($filterData, 'holdingOverTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.holding_over_to'), $val));
        }

        if ($val = array_get($filterData, 'agreementDateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.agreement_date_from'), $val));
        }

        if ($val = array_get($filterData, 'agreementDateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.agreement_date_to'), $val));
        }

        if ($val = array_get($filterData, 'authorisedDateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.authorised_date_from'), $val));
        }

        if ($val = array_get($filterData, 'authorisedDateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.authorised_date_to'), $val));
        }

        if ($val = array_get($filterData, 'terminationDateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.termination_date_from'), $val));
        }

        if ($val = array_get($filterData, 'terminationDateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.termination_date_to'), $val));
        }

        if ($val = array_get($filterData, 'startDateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.start_date_from'), $val));
        }

        if ($val = array_get($filterData, 'startDateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.start_date_to'), $val));
        }

        if ($val = array_get($filterData, 'endDateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.end_date_from'), $val));
        }

        if ($val = array_get($filterData, 'endDateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.end_date_to'), $val));
        }

        if ($val = Common::iset($filterData['rates_liability_id'])) {
            array_push($whereCodes, ['rates_liability', 'rates_liability_id', 'rates_liability_code', $val, "Rates"]);
        }

        if ($val = Common::iset($filterData['insce_liability_id'])) {
            array_push(
                $whereCodes,
                [
                    'insce_liability',
                    'insce_liability_id',
                    'insce_liability_code',
                    $val,
                    "Insurance"
                ]
            );
        }

        if ($val = Common::iset($filterData['estate_maint_id'])) {
            array_push($whereCodes, ['estate_maint', 'estate_maint_id', 'estate_maint_code', $val, "Maintenance"]);
        }

        if ($val = Common::iset($filterData['address'])) {
            array_push($whereTexts, "Address contains '" . $val . "'");
        }
        $this->getAddressSub($filterData, $whereTexts);

        if ($siteId = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $siteId, "Site Code"]);

            if ($buildingId = Common::iset($filterData['building_id'])) {
                array_push(
                    $whereCodes,
                    ['building', 'building_id', 'building_code', $buildingId, "Building Code"]
                );
            }
        }

        if ($val = Common::iset($filterData['contact'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Landlord']);
        }

        if ($val = Common::iset($filterData['owner'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Owner']);
        }

        if ($val = Common::iset($filterData['leaseInStatus'])) {
            array_push(
                $whereCodes,
                ['lease_in_status', 'lease_in_status_id', 'lease_in_status_code', $val, 'Status']
            );
        }

        if ($val = Common::iset($filterData['landlordTenantAct'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.landlord_tenant_act_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.landlord_tenant_act_no'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.landlord_tenant_act_all'));
                    break;
            }
        }

        if ($val = Common::iset($filterData['areaFrom'])) {
            array_push($whereTexts, "Area contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['areaTo'])) {
            array_push($whereTexts, "Area to contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['unit_of_area_id'])) {
            array_push(
                $whereCodes,
                [
                    'unit_of_area',
                    'unit_of_area_id',
                    'unit_of_area_code',
                    $val,
                    "Unit of Area"
                ]
            );
        }

        if ($val = Common::iset($filterData['vatIncluded'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.vat_included_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.vat_included_no'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.vat_included_all'));
                    break;
            }
        }

        $this->reAddUserDefinesQuery(GenTable::LEASE_IN, $filterData, $whereCodes, $whereTexts);
    }

    private function formatInputData(&$inputs)
    {
        foreach ($inputs as $key => $value) {
            if ($value === "") {
                unset($inputs[$key]);
            }
        }
        $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);

        $valueAgreementDate = $reportFilterQuery->getFirstValueFilterField("agreement_date");
        $valueAreaValue = $reportFilterQuery->getFirstValueFilterField("area_value");
        $valueAuthorisedDate = $reportFilterQuery->getFirstValueFilterField("authorised_date");
        $valueEndDate = $reportFilterQuery->getFirstValueFilterField("end_date");
        $valueHoldingOverDate = $reportFilterQuery->getFirstValueFilterField("holding_over_date");
        $valueStartDate = $reportFilterQuery->getFirstValueFilterField("start_date");
        $valueTerminationDate = $reportFilterQuery->getFirstValueFilterField("termination_date");

        $inputs['code'] = $reportFilterQuery->getFirstValueFilterField("lease_out_code");
        $inputs['lettable_unit_code'] = $reportFilterQuery->getFirstValueFilterField("lettable_unit_code");
        $inputs['site_id'] = $reportFilterQuery->getFirstValueFilterField("site_id");
        $inputs['owner'] = $reportFilterQuery->getFirstValueFilterField("owner_user_id");
        $inputs['leaseType'] = $reportFilterQuery->getFirstValueFilterField("lease_type_id");
        $inputs['leaseSubType'] = $reportFilterQuery->getFirstValueFilterField("est_lease_sub_type_id");
        $inputs['agreementDateFrom'] = Common::dateFieldDisplayFormat(Arr::first($valueAgreementDate));
        $inputs['agreementDateTo'] = Common::dateFieldDisplayFormat(Arr::last($valueAgreementDate));
        $inputs['areaFrom'] = !empty(Arr::first($valueAreaValue)) ? Arr::first($valueAreaValue) : null;
        $inputs['areaTo'] = !empty(Arr::last($valueAreaValue)) ? Arr::last($valueAreaValue) : null;
        $inputs['unit_of_area_id'] = $reportFilterQuery->getFirstValueFilterField("area_unit_of_area_id");
        $inputs['authorisedDateFrom'] = Common::dateFieldDisplayFormat(Arr::first($valueAuthorisedDate));
        $inputs['authorisedDateTo'] = Common::dateFieldDisplayFormat(Arr::last($valueAuthorisedDate));
        $inputs['endDateFrom'] = Common::dateFieldDisplayFormat(Arr::first($valueEndDate));
        $inputs['endDateTo'] = Common::dateFieldDisplayFormat(Arr::last($valueEndDate));
        $inputs['holdingOver'] = $reportFilterQuery->getFirstValueFilterField("holding_over");
        $inputs['holdingOverFrom'] = Common::dateFieldDisplayFormat(Arr::first($valueHoldingOverDate));
        $inputs['holdingOverTo'] = Common::dateFieldDisplayFormat(Arr::last($valueHoldingOverDate));
        $inputs['insce_liability_id'] = $reportFilterQuery->getFirstValueFilterField("insce_liability_id");
        $inputs['landlord'] = $reportFilterQuery->getFirstValueFilterField("landlord_contact_id");
        $inputs['landlordTenantAct'] = $reportFilterQuery->getFirstValueFilterField("landlord_tenant_act");
        $inputs['description'] = $reportFilterQuery->getFirstValueFilterField("lease_out_desc");
        $inputs['lettable_unit_desc'] = $reportFilterQuery->getFirstValueFilterField("lettable_unit_desc");
        $inputs['estate_maint_id'] = $reportFilterQuery->getFirstValueFilterField("estate_maint_id");
        $inputs['rates_liability_id'] = $reportFilterQuery->getFirstValueFilterField("rates_liability_id");
        $inputs['ruralLeases'] = $reportFilterQuery->getFirstValueFilterField("ruralest_lease");
        $inputs['startDateFrom'] = Common::dateFieldDisplayFormat(Arr::first($valueStartDate));
        $inputs['startDateTo'] = Common::dateFieldDisplayFormat(Arr::last($valueStartDate));
        $inputs['contact'] = $reportFilterQuery->getFirstValueFilterField("tenant_contact_id");
        $inputs['terminationDateFrom'] = Common::dateFieldDisplayFormat(Arr::first($valueTerminationDate));
        $inputs['terminationDateTo'] = Common::dateFieldDisplayFormat(Arr::last($valueTerminationDate));
        $inputs['vatIncluded'] = $reportFilterQuery->getFirstValueFilterField("vat_included");
        $inputs['leaseOutStatus'] = $reportFilterQuery->getFirstValueFilterField("lease_out_status_id");

        $inputs = $this->getUDValueFilterQuery($reportFilterQuery, $inputs);

        return $inputs;
    }
}
