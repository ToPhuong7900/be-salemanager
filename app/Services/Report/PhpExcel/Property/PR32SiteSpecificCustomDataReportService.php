<?php

namespace Tfcloud\Services\Report\PhpExcel\Property;

use Box\Spout\Writer\Common\Creator\Style\StyleBuilder;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Exception;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Constant\ReportConstant;
use Tfcloud\Models\Report;
use Tfcloud\Models\Site;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\FilterQuery\PropertiesFilterQueryService;
use Tfcloud\Services\Report\PhpExcel\PhpExcelReportBaseService;
use Tfcloud\Models\Property\SiteFieldType;

/**
 * Class PRJ19ProjectSpecificCustomDataReportService
 * @package Tfcloud\Services\Report\PhpExcel
 * @reportName: Project Specific Custom Data Report
 * @reportCode: PRJ19
 */
class PR32SiteSpecificCustomDataReportService extends PhpExcelReportBaseService
{
    private $reportHeaders = [
        'establishment_code' => 'Establishment Code',
        'establishment_desc' => 'Establishment Description',
        'site_code' => 'Code',
        'site_desc' => 'Description',
        'site_alias' => 'Site Aliases',
        'active' => 'Site Active',
        'site_type_code' => 'Site Type Code',
        'site_type_desc' => 'Site Type Desc',
        'site_designation_code' => 'Site Designation Code',
        'site_designation_desc' => 'Site Designation Desc',
        'site_classification_code' => 'Site Classification Code',
        'site_classification_desc' => 'Site Classification Desc',
        'dfe_county_code' => 'LA Code',
        'dfe_number' => 'DFE Number',
        'seed_number' => 'SEED Number',
        'uprn' => 'UPRN',
        'suff_type_code' => 'Sufficiency Type',
        'contact_name' => 'Contact',
        'contact_desc' => 'Contact Organisation',
        'default_dlo_job_display_name' => 'Default Job Manager',
        'site_email_addr' => 'Site Email Address',
        'second_addr_obj' => 'Sub Dwelling',
        'first_addr_obj' => 'Number/Name',
        'street' => 'Street',
        'locality' => 'Locality',
        'town' => 'Town/City',
        'region' => 'County/Region',
        'postcode' => 'Postcode',
        'country' => 'Country',
        'phone_main' => 'Main Phone',
        'opening_hours' => 'Opening Hours',
        'easting' => 'Easting',
        'northing' => 'Northing',
        'gis_ref' => 'GIS Ref',
        'lat' => 'GIS Lat',
        'lng' => 'GIS Long',
        'prop_tenure_code' => 'Tenure Code',
        'prop_tenure_desc' => 'Tenure Description',
        'energy_dec_rating_code' => 'DEC Rating',
        'energy_epc_rating_code' => 'Current EPC Rating',
        'energy_potential_epc_rating_code' => 'Potential EPC Rating',
        'epc_expiry_date' => 'EPC Expiry Date',
        'suitability_rating_code' => 'Suitability Rating Code',
        'suitability_rating_desc' => 'Suitability Rating Description',
        'site_owner_name' => 'Site Owner',
        'ward_code' => 'Ward Code',
        'ward_desc' => 'Ward Description',
        'parish_code' => 'Parish Code',
        'parish_desc' => 'Parish Description',
        'district_code' => 'District Code',
        'district_desc' => 'District Description',
        'constituency_code' => 'Constituency Code',
        'constituency_desc' => 'Constituency Description',
        'terrier_status_code' => 'Terrier Status Code',
        'terrier_status_desc' => 'Terrier Status Description',
        'listed_type_code' => 'Listed Code',
        'listed_type_desc' => 'Listed Description',
        'asset_category_code' => 'IFRS Category Code',
        'asset_category_desc' => 'IFRS Category Description',
        'asset_subcategory_code' => 'IFRS Sub Category Code',
        'asset_subcategory_desc' => 'IFRS Sub Category Description',
        'asset_category' => 'Asset Category',
        'site_usage_code' => 'Usage Code',
        'site_usage_desc' => 'Usage Description',
        'committee_code' => 'Portfolio Code',
        'committee_desc' => 'Portfolio Description',
        'area' => 'Area',
        'unit_of_area_code' => 'Unit of area',
        'conservation_area' => 'Conservation Area',
        'ancient_monument' => 'Ancient Monument',
        'area_of_natural_beauty' => 'Area of Natural Beauty',
        'world_heritage' => 'World Heritage',
        'tree_preservation_area' => 'Tree Preservation Area',
        'site_of_special_scientific_interest' => 'Site of Special Scientific Interest',
        'age_of_structures' => 'Age of Structures',
        'staff_parking' => 'Staff Parking',
        'visitor_parking' => 'Visitor Parking',
        'disabled_parking' => 'Disabled Parking',
        'vacant' => 'Vacant',
        'site_vacant_date' => 'Vacant Date',
        'comment' => 'Comment',
        'svc_drainage' => 'Drainage',
        'svc_electric' => 'Electric',
        'svc_gas' => 'Gas',
        'svc_sewerage' => 'Sewerage',
        'svc_water' => 'Water',
        'svc_water_meter' => 'Water Meter',
        'svc_wide_area_network' => 'Wide Area Network (WAN)',
        'svc_solar' => 'Solar',
        'svc_renewable' => 'Renewable',
        'svc_number_of_meters' => 'Number of Meters',
        'svc_meter_locations' => 'Meter Locations',
        'svc_electric_car_charging_points' => 'Electric Car Charging Points',
        'prop_svc_billing_type_desc' => 'Billing Type',
        'account_code_link_mask' => 'Account Code Link',
        'site_field_group_code' => 'Field Group Code',
        'site_field_group_desc' => 'Field Group Description',
        'site_field_code' => 'Field Code',
        'site_field_desc' => 'Field Description',
        'label' => 'Form Label',
        'report_value' => 'Value',
    ];

    //Spout engine
    private $writer;
    private $filterQueryService;

    private $whereCodes = [];
    private $orCodes = [];
    private $whereTexts = [];
    private $reportBoxFilterLoader = null;


    public function __construct(PermissionService $permissionService, Report $report = null)
    {
        parent::__construct($permissionService);
        $this->filterQueryService = new PropertiesFilterQueryService($this->permissionService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    /**
     * @note: main function
     * @param $repExcelFile
     * @param $inputs
     * @param array $options
     * @param string $filterText
     * @return bool
     */
    public function generateReport($repExcelFile, $inputs, $options = [], &$filterText = '')
    {
        $result = true;
        try {
            $this->writer = WriterEntityFactory::createXLSXWriter();
            $this->writer->openToFile($repExcelFile);
            $data = $this->getData($inputs, $filterText);

            //write excel
            $this->writerHeader();
            $this->writeBody($data);
            $this->writeFooter($data, $filterText);

            $this->writer->close();
        } catch (Exception $ex) {
            $result = false;
            \Log::error('PR32SiteSpecificCustomDataReportService@generateReport: ' . $ex->getMessage());
        }
        return $result;
    }

    private function writerHeader()
    {
        $headerStyle = (new StyleBuilder())->setFontBold()->build();
        $headers = array_values($this->reportHeaders);
        $this->writer->addRow(WriterEntityFactory::createRowFromArray($headers, $headerStyle));
    }

    private function writeBody($data)
    {
        $colKeys = array_keys($this->reportHeaders);
        $data->map(function ($record) use ($colKeys) {
            return $record->only($colKeys);
        })->each(function ($record) {
            $this->writer->addRow(WriterEntityFactory::createRowFromArray($record));
        });
    }

    private function writeFooter($data, &$filterText)
    {
        $bFilterDetail = false;
        $this->getFiltering(
            $this->whereCodes,
            $this->orCodes,
            $this->whereTexts,
            $bFilterDetail,
            $filterText
        );

        $reportTitle = array_get($this->returnedData, 'repTitle', ReportConstant::SYSTEM_REPORT_PR32);
        $this->writeLine();
        $this->writeLine("Report: $reportTitle");

        if ($filterText) {
            $this->writeLine('Filtering details:');
            $this->writeLine($filterText);
        }

        $this->writeLine('Ordering details:');
        $this->writeLine('`Site Code` (ascending)');

        $total = count($data);
        if ($total) {
            $this->writeLine('Rows returned:');
            $this->writeLine("Returned $total rows (from 1 to $total) of $total rows.");
        } else {
            $this->writeLine();
            $this->writeLine('No data found.');
        }
    }

    private function getData($inputs, &$filterText)
    {
        $select = [
            'establishment.establishment_code',
            'establishment.establishment_desc',
            'site.site_code',
            'site.site_desc',
            'site.site_alias',
            'site.active',
            'site_type.site_type_code',
            'site_type.site_type_desc',
            'site_designation.site_designation_code',
            'site_designation.site_designation_desc',
            'site_classification.site_classification_code',
            'site_classification.site_classification_desc',
            'site.dfe_county_code',
            'site.dfe_number',
            'site.seed_number',
            'site.uprn',
            'suff_type.suff_type_code',
            'contact.contact_name',
            'contact.contact_desc',
            \DB::raw('default_job_manager_user.display_name as default_dlo_job_display_name'),
            'site.site_email_addr',
            'address.second_addr_obj',
            'address.first_addr_obj',
            'address_street.street',
            'address_street.locality',
            'address_street.town',
            'address_street.region',
            'address.postcode',
            'address.country',
            'address.phone_main',
            'site.opening_hours',
            'gen_gis.easting',
            'gen_gis.northing',
            'gen_gis.gis_ref',
            'gen_gis.lat',
            'gen_gis.lng',
            'prop_tenure.prop_tenure_code',
            'prop_tenure.prop_tenure_desc',
            'energy_dec_rating.energy_dec_rating_code',
            'energy_current_epc_rating.energy_epc_rating_code',
            \DB::raw('energy_potential_epc_rating.energy_epc_rating_code AS energy_potential_epc_rating_code'),
            \DB::raw("DATE_FORMAT(`energy_field`.`energy_epc_expiry_date`,'%d/%m/%Y') as epc_expiry_date"),
            'suitability_rating.suitability_rating_code',
            'suitability_rating.suitability_rating_desc',
            \DB::raw('owner_user.display_name as site_owner_name'),
            'ward.ward_code',
            'ward.ward_desc',
            'district.district_code',
            'district.district_desc',
            'parish.parish_desc',
            'parish.parish_desc',
            'constituency.constituency_code',
            'constituency.constituency_desc',
            'terrier_status.terrier_status_code',
            'terrier_status.terrier_status_desc',
            'listed_type.listed_type_code',
            'listed_type.listed_type_desc',
            'asset_category.asset_category_code',
            'asset_category.asset_category_desc',
            'asset_subcategory.asset_subcategory_code',
            'asset_subcategory.asset_subcategory_desc',
            \DB::raw("
                CASE WHEN `site`.`prop_facility_asset_category_id` IS NOT NULL THEN
                    CASE
                        WHEN `prop_facility_asset_category`.`prop_facility_asset_subcategory_id` IS NOT NULL
                        THEN CONCAT_WS(' - ', prop_facility_asset_category.prop_facility_asset_category_desc,
                        prop_facility_asset_category_type.prop_facility_asset_category_type_desc,
                        prop_facility_asset_subcategory.prop_facility_asset_subcategory_desc)
                        WHEN `prop_facility_asset_category`.`prop_facility_asset_subcategory_id` IS NULL
                        THEN CONCAT_WS(' - ',
                        prop_facility_asset_category.prop_facility_asset_category_desc,
                        prop_facility_asset_category_type.prop_facility_asset_category_type_desc)
                    END
                ELSE '' END AS `asset_category`
            "),
            'site_usage.site_usage_code',
            'site_usage.site_usage_desc',
            'committee.committee_code',
            'committee.committee_desc',
            'site.area',
            'unit_of_area.unit_of_area_code',
            'site.conservation_area',
            'site.ancient_monument',
            'site.area_of_natural_beauty',
            'site.world_heritage',
            'site.tree_preservation_area',
            'site.site_of_special_scientific_interest',
            'site.age_of_structures',
            'site.staff_parking',
            'site.visitor_parking',
            'site.disabled_parking',
            'site.vacant',
            \Db::raw("DATE_FORMAT(`site`.`vacant_date`,'%d/%m/%Y') as site_vacant_date"),
            'site.svc_drainage',
            'site.comment',
            'site.svc_electric',
            'site.svc_gas',
            'site.svc_sewerage',
            'site.svc_water',
            'site.svc_water_meter',
            'site.svc_wide_area_network',
            'site.svc_solar',
            'site.svc_renewable',
            'site.svc_number_of_meters',
            'site.svc_meter_locations',
            'site.svc_electric_car_charging_points',
            'prop_svc_billing_type.prop_svc_billing_type_desc',
            'site.account_code_link_mask',
            'site_field_group.site_field_group_code',
            'site_field_group.site_field_group_desc',
            'site_field.site_field_code',
            'site_field.site_field_desc',
            'site_field.label',
            'site_field_value.report_value',
            \Db::raw(" CASE WHEN site_field.site_field_type_id = "
                . SiteFieldType::BOOLEAN . " THEN "
                . "  COALESCE(site_field_value.report_value, 'N') "
                . " WHEN site_field.site_field_type_id = "
                . SiteFieldType::SELECTION . " THEN "
                . " CONCAT_WS(' - ', site_selection_field.site_selection_field_code,"
                    . " site_selection_field.site_selection_field_desc) "
                . " ELSE "
                . "  site_field_value.report_value "
                . " END AS report_value "),
        ];

        $query = Site::userSiteGroup()
            ->select($select)
            ->leftJoin('establishment', 'establishment.establishment_id', 'site.establishment_id')
            ->leftJoin('site_designation', 'site_designation.site_designation_id', 'site.site_designation_id')
            ->leftJoin(
                'site_classification',
                'site_classification.site_classification_id',
                'site.site_classification_id'
            )
            ->leftJoin(
                'suff_type',
                'suff_type.suff_type_id',
                'site.suff_type_id'
            )
            ->leftJoin('contact', 'contact.contact_id', 'site.contact_id')
            ->leftJoin('gen_gis', 'gen_gis.gen_gis_id', 'site.gen_gis_id')
            ->leftJoin(
                'user as default_job_manager_user',
                'default_job_manager_user.id',
                'site.default_dlo_manager_user_id'
            )
            ->leftJoin('address', 'address.address_id', 'site.address_id')
            ->leftJoin('address_street', 'address.address_street_id', 'address_street.address_street_id')
            ->leftJoin('prop_tenure', 'prop_tenure.prop_tenure_id', 'site.prop_tenure_id')
            ->leftJoin('energy_field', function ($join) {
                $join->on(
                    'site.site_id',
                    'energy_field.site_id'
                );
                $join->whereNull(
                    'energy_field.building_id',
                );
            })
            ->leftJoin(
                'energy_dec_rating',
                'energy_dec_rating.energy_dec_rating_id',
                'energy_field.energy_dec_rating_id'
            )
            ->leftJoin(
                'energy_epc_rating as energy_current_epc_rating',
                'energy_current_epc_rating.energy_epc_rating_id',
                'energy_field.energy_epc_current_rating_id'
            )
            ->leftJoin(
                'energy_epc_rating as energy_potential_epc_rating',
                'energy_potential_epc_rating.energy_epc_rating_id',
                'energy_field.energy_epc_potential_rating_id'
            )
            ->leftJoin('suitability_rating', 'site.suitability_rating_id', 'suitability_rating.suitability_rating_id')
            ->leftJoin('user AS owner_user', 'owner_user.id', '=', 'site.fm_user_id')
            ->leftJoin('ward', 'site.ward_id', 'ward.ward_id')
            ->leftJoin('parish', 'site.parish_id', 'parish.parish_id')
            ->leftJoin('district', 'site.district_id', 'district.district_id')
            ->leftJoin('constituency', 'site.constituency_id', 'constituency.constituency_id')
            ->leftJoin('terrier_status', 'terrier_status.terrier_status_id', 'site.terrier_status_id')
            ->leftJoin('listed_type', 'listed_type.listed_type_id', 'site.listed_type_id')
            ->leftjoin('asset_category', 'asset_category.asset_category_id', "site.asset_category_id")
            ->leftjoin('asset_subcategory', function ($join) {
                $join->on('asset_subcategory.asset_category_id', 'asset_category.asset_category_id');
                $join->on('asset_subcategory.asset_subcategory_id', 'site.asset_subcategory_id');
            })
            ->leftJoin(
                'prop_facility_asset_category',
                'site.prop_facility_asset_category_id',
                'prop_facility_asset_category.prop_facility_asset_category_id'
            )
            ->leftJoin(
                'prop_facility_asset_category_type',
                'prop_facility_asset_category.prop_facility_asset_category_type_id',
                'prop_facility_asset_category_type.prop_facility_asset_category_type_id'
            )
            ->leftJoin(
                'prop_facility_asset_subcategory',
                'prop_facility_asset_category.prop_facility_asset_subcategory_id',
                'prop_facility_asset_subcategory.prop_facility_asset_subcategory_id'
            )
            ->leftJoin('site_usage', 'site.site_usage_id', 'site_usage.site_usage_id')
            ->leftJoin('committee', 'committee.committee_id', 'site.committee_id')
            ->leftJoin('unit_of_area', 'unit_of_area.unit_of_area_id', 'site.unit_of_area_id')
            ->leftJoin(
                'prop_svc_billing_type',
                'prop_svc_billing_type.prop_svc_billing_type_id',
                'site.prop_svc_billing_type_id'
            )
            ->join('site_type', 'site_type.site_type_id', 'site.site_type_id')
            ->join(
                'site_field_group',
                'site_field_group.site_field_group_id',
                'site_type.site_field_group_id'
            )
            ->join(
                'site_field',
                'site_field.site_field_group_id',
                'site_field_group.site_field_group_id'
            )
            ->leftJoin('site_field_value', function ($join) {
                $join->on('site_field_value.site_field_id', 'site_field.site_field_id');
                $join->on('site_field_value.site_id', 'site.site_id');
            })
            ->leftJoin('site_selection_group', function ($join) {
                $join->on(
                    'site_selection_group.site_selection_group_id',
                    'site_field.site_selection_group_id'
                );
                $join->where(
                    'site_field.site_field_type_id',
                    SiteFieldType::SELECTION
                );
            })
            ->leftJoin('site_selection_field', function ($join) {
                $join->on(
                    'site_selection_field.site_selection_group_id',
                    'site_selection_group.site_selection_group_id'
                );
                $join->on('site_field_value.report_value', 'site_selection_field.site_selection_field_id');
            })
            ->where('site_field.active', CommonConstant::DATABASE_VALUE_YES)
            ->where('site_field_group.active', CommonConstant::DATABASE_VALUE_YES);

        $query = $this->permissionService->listViewSiteAccess($query, "site.site_id");

        //filter and log
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $reportFilterQuery->setViewName(false);
            $query = $reportFilterQuery->filterAll($query);
            $filterText = $reportFilterQuery->getFilterDetailText();
        } else {
            $query = $this->filterQueryService->reAddSiteQuery(
                $inputs,
                false,
                $query,
                $this->whereCodes,
                $this->orCodes,
                $this->whereTexts
            );
        }

        return $query->get();
    }

    private function writeLine($value = '')
    {
        $this->writer->addRow(WriterEntityFactory::createRowFromArray([$value]));
    }
}
