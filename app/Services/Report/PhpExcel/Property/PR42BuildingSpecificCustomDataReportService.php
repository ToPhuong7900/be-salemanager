<?php

namespace Tfcloud\Services\Report\PhpExcel\Property;

use Box\Spout\Writer\Common\Creator\Style\StyleBuilder;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Exception;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Constant\ReportConstant;
use Tfcloud\Models\Building;
use Tfcloud\Models\Property\BuildingFieldType;
use Tfcloud\Models\Report;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\FilterQuery\PropertiesFilterQueryService;
use Tfcloud\Services\Report\PhpExcel\PhpExcelReportBaseService;

class PR42BuildingSpecificCustomDataReportService extends PhpExcelReportBaseService
{
    private $reportHeaders = [
        'site_code' => 'Site Code',
        'site_desc' => 'Site Description',
        'site_active' => 'Site Active',
        'building_code' => 'Building Code',
        'building_desc' => 'Building Description',
        'building_alias' => 'Building Aliases',
        'building_active' => 'Building Active',
        'uprn' => 'UPRN',
        'second_addr_obj' => 'Sub Dwelling',
        'first_addr_obj' => 'Number/Name',
        'street' => 'Street',
        'locality' => 'Locality',
        'town' => 'Town/City',
        'region' => 'County/Region',
        'postcode' => 'Postcode',
        'country' => 'Country',
        'phone_main' => 'Main Phone',
        'prop_tenure_code' => 'Tenure Code',
        'prop_tenure_desc' => 'Tenure Description',
        'building_owner_name' => 'Building Owner',
        'contact_name' => 'Building Contact',
        'contact_desc' => 'Building Contact Organisation',
        'ward_code' => 'Ward Code',
        'ward_desc' => 'Ward Description',
        'parish_code' => 'Parish Code',
        'parish_desc' => 'Parish Description',
        'district_code' => 'District Code',
        'district_desc' => 'District Description',
        'constituency_code' => 'Constituency Code',
        'constituency_desc' => 'Constituency Description',
        'building_type_code' => 'Building Type Code',
        'building_type_desc' => 'Building Type Description',
        'building_field_group_code' => 'Group Code',
        'building_field_group_desc' => 'Group Description',
        'building_field_code' => 'Field Code',
        'building_field_desc' => 'Field Description',
        'label' => 'Form Label',
        'report_value' => 'Value',
    ];

    //Spout engine
    private $writer;
    private $filterQueryService;

    private $whereCodes = [];
    private $orCodes = [];
    private $whereTexts = [];
    private $reportBoxFilterLoader = null;


    public function __construct(PermissionService $permissionService, Report $report = null)
    {
        parent::__construct($permissionService);
        $this->filterQueryService = new PropertiesFilterQueryService($this->permissionService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function generateReport($repExcelFile, $inputs, $options = [], &$filterText = '')
    {
        $result = true;
        try {
            $this->writer = WriterEntityFactory::createXLSXWriter();
            $this->writer->openToFile($repExcelFile);
            $data = $this->getData($inputs, $filterText);

            //write excel
            $this->writerHeader();
            $this->writeBody($data);
            $this->writeFooter($data, $filterText);

            $this->writer->close();
        } catch (Exception $ex) {
            $result = false;
            \Log::error('PR42BuildingSpecificCustomDataReportService@generateReport: ' . $ex->getMessage());
        }
        return $result;
    }

    private function writerHeader()
    {
        $headerStyle = (new StyleBuilder())->setFontBold()->build();
        $headers = array_values($this->reportHeaders);
        $this->writer->addRow(WriterEntityFactory::createRowFromArray($headers, $headerStyle));
    }

    private function writeBody($data)
    {
        $colKeys = array_keys($this->reportHeaders);
        $data->map(function ($record) use ($colKeys) {
            return $record->only($colKeys);
        })->each(function ($record) {
            $this->writer->addRow(WriterEntityFactory::createRowFromArray($record));
        });
    }

    private function writeFooter($data, &$filterText)
    {
        $bFilterDetail = false;
        $this->getFiltering(
            $this->whereCodes,
            $this->orCodes,
            $this->whereTexts,
            $bFilterDetail,
            $filterText
        );

        $reportTitle = array_get($this->returnedData, 'repTitle', ReportConstant::SYSTEM_REPORT_PR42);
        $this->writeLine();
        $this->writeLine("Report: $reportTitle");

        if ($filterText) {
            $this->writeLine('Filtering details:');
            $this->writeLine($filterText);
        }

        $this->writeLine('Ordering details:');
        $this->writeLine('`Building Code` (ascending)');

        $total = count($data);
        if ($total) {
            $this->writeLine('Rows returned:');
            $this->writeLine("Returned $total rows (from 1 to $total) of $total rows.");
        } else {
            $this->writeLine();
            $this->writeLine('No data found.');
        }
    }

    private function getData($inputs, &$filterText)
    {
        $select = [
            'site.site_code',
            'site.site_desc',
            'site.active as site_active',
            'building.building_code',
            'building.building_desc',
            'building.building_alias',
            'building.active as building_active',
            'site.uprn',
            'address.second_addr_obj',
            'address.first_addr_obj',
            'address_street.street',
            'address_street.locality',
            'address_street.town',
            'address_street.region',
            'address.postcode',
            'address.country',
            'address.phone_main',
            'prop_tenure.prop_tenure_code',
            'prop_tenure.prop_tenure_desc',
            \DB::raw('owner_user.display_name as building_owner_name'),
            'contact.contact_name',
            'contact.contact_desc',
            'ward.ward_code',
            'ward.ward_desc',
            'parish.parish_code',
            'parish.parish_desc',
            'district.district_code',
            'district.district_desc',
            'constituency.constituency_code',
            'constituency.constituency_desc',
            'building_type.building_type_code',
            'building_type.building_type_desc',
            'building_field_group.building_field_group_code',
            'building_field_group.building_field_group_desc',
            'building_field.building_field_code',
            'building_field.building_field_desc',
            'building_field.label',
            'building_field_value.report_value',
            \Db::raw(" CASE WHEN building_field.building_field_type_id = "
                . BuildingFieldType::BOOLEAN . " THEN "
                . "  COALESCE(building_field_value.report_value, 'N') "
                . " WHEN building_field.building_field_type_id = "
                . BuildingFieldType::SELECTION . " THEN "
                . " CONCAT_WS(' - ', building_selection_field.building_selection_field_code,"
                    . " building_selection_field.building_selection_field_desc) "
                . " ELSE "
                . "  building_field_value.report_value "
                . " END AS report_value "),
        ];

        $query = Building::userSiteGroup()
            ->select($select)
            ->join('site', 'site.site_id', 'building.site_id')
            ->leftJoin('contact', 'contact.contact_id', 'site.contact_id')
            ->leftJoin('address', 'address.address_id', 'site.address_id')
            ->leftJoin('address_street', 'address.address_street_id', 'address_street.address_street_id')
            ->leftJoin('prop_tenure', 'prop_tenure.prop_tenure_id', 'site.prop_tenure_id')
            ->leftJoin('user AS owner_user', 'owner_user.id', '=', 'site.fm_user_id')
            ->leftJoin('ward', 'building.ward_id', 'ward.ward_id')
            ->leftJoin('parish', 'building.parish_id', 'parish.parish_id')
            ->leftJoin('district', 'site.district_id', 'district.district_id')
            ->leftJoin('constituency', 'site.constituency_id', 'constituency.constituency_id')
            ->leftJoin('occupier', 'occupier.occupier_id', 'building.occupier_id')
            ->leftJoin('room', 'room.building_id', 'building.building_id')
            ->leftJoin('prop_hazard_warning_icon', 'prop_hazard_warning_icon.building_id', 'building.building_id')
            ->leftJoin(
                'hazard_warning_icon',
                'hazard_warning_icon.hazard_warning_icon_id',
                'prop_hazard_warning_icon.hazard_warning_icon_id'
            )
            ->leftJoin('committee', 'committee.committee_id', 'building.committee_id')
            ->leftJoin('building_usage', 'building_usage.building_usage_id', 'building.building_usage_id')
            ->join('building_type', 'building_type.building_type_id', 'building.building_type_id')
            ->join(
                'building_field_group',
                'building_field_group.building_field_group_id',
                'building_type.building_field_group_id'
            )
            ->join(
                'building_field',
                'building_field.building_field_group_id',
                'building_field_group.building_field_group_id'
            )
            ->leftJoin('building_field_value', function ($join) {
                $join->on('building_field_value.building_field_id', 'building_field.building_field_id');
                $join->on('building_field_value.building_id', 'building.building_id');
            })
            ->leftJoin('building_selection_group', function ($join) {
                $join->on(
                    'building_selection_group.building_selection_group_id',
                    'building_field.building_selection_group_id'
                );
                $join->where(
                    'building_field.building_field_type_id',
                    BuildingFieldType::SELECTION
                );
            })
            ->leftJoin('building_selection_field', function ($join) {
                $join->on(
                    'building_selection_field.building_selection_group_id',
                    'building_selection_group.building_selection_group_id'
                );
                $join->on('building_field_value.report_value', 'building_selection_field.building_selection_field_id');
            })
            ->leftjoin('asset_category', 'asset_category.asset_category_id', "site.asset_category_id")
            ->leftjoin('asset_subcategory', function ($join) {
                $join->on('asset_subcategory.asset_category_id', 'asset_category.asset_category_id');
                $join->on('asset_subcategory.asset_subcategory_id', 'site.asset_subcategory_id');
            })
            ->leftJoin(
                'prop_facility_asset_category',
                'site.prop_facility_asset_category_id',
                'prop_facility_asset_category.prop_facility_asset_category_id'
            )
            ->leftJoin(
                'prop_facility_asset_category_type',
                'prop_facility_asset_category.prop_facility_asset_category_type_id',
                'prop_facility_asset_category_type.prop_facility_asset_category_type_id'
            )
            ->leftJoin(
                'prop_facility_asset_subcategory',
                'prop_facility_asset_category.prop_facility_asset_subcategory_id',
                'prop_facility_asset_subcategory.prop_facility_asset_subcategory_id'
            )
            ->where('building_field.active', CommonConstant::DATABASE_VALUE_YES)
            ->where('building_field_group.active', CommonConstant::DATABASE_VALUE_YES)
            ->distinct();

        $query = $this->permissionService->listViewSiteAccess($query, "site.site_id");
        $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
        $reportFilterQuery->setViewName(false);
        $query = $reportFilterQuery->filterAll($query);
        $filterText = $reportFilterQuery->getFilterDetailText();
        return $query->get();
    }

    private function writeLine($value = '')
    {
        $this->writer->addRow(WriterEntityFactory::createRowFromArray([$value]));
    }
}
