<?php

namespace Tfcloud\Services\Report\PhpExcel;

use PhpOffice\PhpSpreadsheet\IOFactory;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Traits\EmailTrait;
use Tfcloud\Models\SiteGroup;
use Tfcloud\Models\SuffRoomType;
use Tfcloud\Models\SuffType;
use Tfcloud\Services\PermissionService;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use Tfcloud\Services\Education\SufficiencyService;

class SufficienciesService extends PhpExcelReportBaseService
{
    use EmailTrait;

    protected $sufficiencyService;

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->sufficiencyService = new SufficiencyService($permissionService);
    }

    public function generateReport($sufficiency, $inputs = [])
    {
        $formatCode = str_replace('/', '-', $sufficiency->sufficiency_code);
        $reportFileName = 'Sufficiency-' . $formatCode;
        $repExcelFile = $this->generateReportOutPutPath($reportFileName, 'xlsx');
        $sufficiencyCode = $sufficiency->sufficiency_code;

        if ($sufficiency->suff_type_id == SuffType::PRIMARY) {
            $templateName = 'PrimarySchoolSufficiency.xlsx';
        } else {
            $templateName = 'SecondarySchoolSufficiency.xlsx';
        }


        $templateFile = app_path()
            . DIRECTORY_SEPARATOR . "templates"
            . DIRECTORY_SEPARATOR . "phpExcel"
            . DIRECTORY_SEPARATOR . $templateName;
        $phpExcelObj = $this->readFile($templateFile);

        if (is_readable($templateFile)) {
            copy($templateFile, $repExcelFile);
        }

        if ($sufficiency->suff_type_id == SuffType::PRIMARY) {
            $summaryData = $this->getPrimaryReportData($sufficiency, $inputs);
        } else {
            $summaryData = $this->getSecondaryReportData($sufficiency, $inputs);
        }

        $this->fillDataToExcel($phpExcelObj, $summaryData);

        $errorMessages = null;
        if (is_null($errorMessages)) {
            $errorMessages = $this->writeExcelFile($phpExcelObj, $repExcelFile, 'Xlsx', true);
        }

        if ($errorMessages) {
            return false;
        }

        if (array_get($inputs, 'email_address', false)) {
            $user = \Auth::user();
            $fileName = $sufficiencyCode;
            $emailData = array(
                'mailTo' => array_get($inputs, 'email_address'),
                'displayName' => $user->siteGroup->email_return_address,
                'repTitle' => $fileName,
                'subject' => $fileName . ' Report',
            );
            return $this->sendEmail($repExcelFile, $emailData, $sufficiency);
        }

        return $repExcelFile;
    }

    private function getPrimaryReportData($sufficiency, $inputs = [])
    {
        $data = [
            'G12' => $sufficiency->site->dfe_county_code . '/' . $sufficiency->site->dfe_number,
            'M12' => date('M-y', strtotime($sufficiency->assessment_date)),
            'G14' => $sufficiency->site->site_desc,
            'G16' => $sufficiency->age_range_from,
            'I16' => $sufficiency->age_range_to,
            'S16' => $sufficiency->num_sites,
            'S18' => $sufficiency->total_site_area,
            'G25' => $sufficiency->adm_first_norm_year,
            'G27' => $sufficiency->adm_first_num_years,
            'G29' => $sufficiency->adm_first_planned_num,
            'Q25' => $sufficiency->adm_second_norm_year,
            'Q27' => $sufficiency->adm_second_num_years,
            'Q29' => $sufficiency->adm_second_planned_num,
            'S25' => $sufficiency->adm_third_norm_year,
            'S27' => $sufficiency->adm_third_num_years,
            'S29' => $sufficiency->adm_third_planned_num,
            'B40' => $sufficiency->lea_prov_early_year,
            'B45' => $sufficiency->lea_prov_special_res,
            'B51' => $sufficiency->lea_prov_adult_learn,
        ];

        $index = 73;
        $incBuildCode = SiteGroup::get()->suff_inc_build_code_in_export === 'Y';
        $rooms = $sufficiency->suffRooms()->orderBy('building_code')->orderBy('room_number')->get();


        foreach ($rooms as $room) {
            if (array_get($inputs, 'include_excluded', false) == 'N') {
                if ($room->exclude == 'Y') {
                    continue;
                }
            }

            $data["A$index"] = $room->building_code; //$buildingCode;
            if ($incBuildCode) {
                $data["B$index"] = Common::concatFields(
                    [$room->building_code, $room->room_number],
                    ['separator' => '/']
                );
            } else {
                $data["B$index"] = $room->room_number;
            }
            $data["D$index"] = $room->room_desc;
            $data["I$index"] = $room->suff_non_net_area;
            $data["K$index"] = $room->suff_net_area > 0 ? $room->suff_net_area : null;
            $data["U$index"] = $room->suffRoomStatus ? $room->suffRoomStatus->suff_room_status_code : null;

            if (
                $room->suffRoomType
                && $room->suffRoomType->suff_room_type_id
                && $room->suffRoomType->suff_room_type_id == SuffRoomType::SPECIALIST_SPACE
            ) {
                $data["N$index"] = 1;
            }

            $index++;
        }

        return $data;
    }

    private function getSecondaryReportData($sufficiency, $inputs = [])
    {
        $data = [
            'G10' => $sufficiency->site->dfe_county_code . '/' . $sufficiency->site->dfe_number,
            'M10' => date('M-y', strtotime($sufficiency->assessment_date)),
            'G12' => $sufficiency->site->site_desc,
            'G14' => $sufficiency->age_range_from,
            'I14' => $sufficiency->age_range_to,
            'T14' => $sufficiency->num_sites,
            'T16' => $sufficiency->total_site_area,
            'G25' => $sufficiency->adm_first_norm_year,
            'G27' => $sufficiency->adm_first_num_years,
            'G29' => $sufficiency->adm_first_planned_num,
            'R25' => $sufficiency->adm_second_norm_year,
            'R27' => $sufficiency->adm_second_num_years,
            'R29' => $sufficiency->adm_second_planned_num,
            'T25' => $sufficiency->adm_third_norm_year,
            'T27' => $sufficiency->adm_third_num_years,
            'T29' => $sufficiency->adm_third_planned_num,
            'G38' => $sufficiency->sixform_last_yr_eleven_size == 0 ? null : $sufficiency->sixform_last_yr_eleven_size,
            'G40' => $sufficiency->sixform_prev_last_yr_eleven == 0 ? null : $sufficiency->sixform_prev_last_yr_eleven,
            'I36' => $sufficiency->sixform_this_yr_stay_on == 0 ? null : $sufficiency->sixform_this_yr_stay_on,
            'I38' => $sufficiency->sixform_last_yr_stay_on == 0 ? null : $sufficiency->sixform_last_yr_stay_on,
            'B50' => $sufficiency->lea_prov_early_year,
            'B54' => $sufficiency->lea_prov_special_res,
            'B60' => $sufficiency->lea_prov_adult_learn,
        ];

        $index = 80;
        $incBuildCode = SiteGroup::get()->suff_inc_build_code_in_export === 'Y';

        $rooms = $sufficiency->suffRooms()->orderBy('building_code')->orderBy('room_number')->get();

        foreach ($rooms as $room) {
            if (array_get($inputs, 'include_excluded', false) == 'N') {
                if ($room->exclude == 'Y') {
                    continue;
                }
            }

            $data["A$index"] = $room->building_code;
            if ($incBuildCode) {
                $data["B$index"] = Common::concatFields(
                    [$room->building_code, $room->room_number],
                    ['separator' => '/']
                );
            } else {
                $data["B$index"] = $room->room_number;
            }
            $data["D$index"] = $room->room_desc;
            $data["I$index"] = $room->suff_non_net_area;
            $data["K$index"] = $room->suff_net_area;
            $data["V$index"] = $room->suffRoomStatus ? $room->suffRoomStatus->suff_room_status_code : null;

            if ($room->suffRoomType && $room->suffRoomType->suff_room_type_id) {
                switch ($room->suffRoomType->suff_room_type_id) {
                    case SuffRoomType::LIGHT_PRACTICAL_SPACE:
                        $data["N$index"] = 1;
                        break;
                    case SuffRoomType::HEAVY_PRACTICAL_SPACE:
                        $data["O$index"] = 1;
                        break;
                    case SuffRoomType::LARGE_AND_PEFORMANCE_SPACE:
                        $data["P$index"] = 1;
                        break;
                }
            }

            $index++;
        }

        return $data;
    }

    private function fillDataToExcel(Spreadsheet &$phpExcelObj, $data)
    {
        foreach ($data as $cell => $value) {
            $phpExcelObj->getActiveSheet()->setCellValue($cell, $value);
        }
    }

    protected function readFile($templateFile)
    {
        $reader = IOFactory::createReader("Xlsx");
        $phpExcelObj = $reader->load($templateFile);

        $this->setProperties($phpExcelObj);
        return $phpExcelObj;
    }
}
