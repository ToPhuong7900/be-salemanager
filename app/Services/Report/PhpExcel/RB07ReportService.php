<?php

namespace Tfcloud\Services\Report\PhpExcel;

use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Collection;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Filters\ResourceBooking\RbBookableResourceSearchFilter;
use Tfcloud\Models\RbBookableResourceLayout;
use Tfcloud\Models\RbBookableResourceType;
use Tfcloud\Models\RbResourceBooking;
use Tfcloud\Models\RbResourceBookingStatusType;
use Tfcloud\Services\PermissionService;

class RB07ReportService extends PhpExcelReportBaseService
{
    private const AVAILABILITY_SLOT_TYPE = 'A';
    private const BOOKING_SLOT_TYPE = 'B';
    private const START_TIME_MARK = 'S';
    private const END_TIME_MARK = 'E';

    private $reportHeaders = [
        'site_code' => 'Site Code',
        'site_desc' => 'Site Description',
        'building_code' => 'Building Code',
        'building_desc' => 'Building Description',
        'room_number' => 'Room Number',
        'room_desc' => 'Room Description',
        'plant_code' => 'Plant Code',
        'plant_desc' => 'Plant Description',
        'rb_bookable_resource_code' => 'Bookable Resource Code',
        'rb_bookable_resource_desc' => 'Bookable Resource Description',
        'bookable_resource_type' => 'Bookable Resource Type',
        'availability_day' => 'Day',
        'booking_date' => 'Booking Date',
        'booking_prep_start_time' => 'Booking Prep Start Time',
        'booking_start_time' => 'Booking Start Time',
        'booking_end_time' => 'Booking End Time',
        'booking_restore_end_time' => 'Booking Restore End Time',
        'rb_resource_booking_code' => 'Booking Code',
        'booked_for_name' => 'Booked For Name',
        'rb_resource_booking_desc' => 'Booking Description',
        'booking_layout' => 'Booking Layout',
        'booking_status' => 'Status',
    ];

    /**
     * @var WriterEntityFactory
     */
    private $writer;
    /**
     * @var RbBookableResourceSearchFilter
     */
    private $filters = null;

    public function __construct(
        PermissionService $permissionService
    ) {
        parent::__construct($permissionService);
    }

    protected function validateFilter($inputs)
    {
        $inputs = $this->formatInputData($inputs);
        $this->filters = new RbBookableResourceSearchFilter($inputs);
        $rules = [
            'targetFrom' => ['required', 'date_format:Y-m-d'],
            'targetTo' => ['required', 'date_format:Y-m-d'],
            'days' => ['required', 'array', 'min:1'],
        ];

        $messages = [
            'targetFrom.required' => 'Date from is required.',
            'targetTo.required' => 'Date to is required.',
            'days.required' => 'Days is required.',
        ];

        $validator = \Validator::make($inputs, $rules, $messages);
        $validator->success = $validator->passes();

        if ($validator->success) {
            $rules = [];
            $messages = [];
            $maximumSearchPeriod = \Auth::user()->siteGroup()->select(['rb_maximum_search_date_period'])
                ->first()->rb_maximum_search_date_period;
            $fromDate = Carbon::createFromFormat('Y-m-d', $inputs['targetFrom']);
            $maxEndDate = $fromDate->addDay($maximumSearchPeriod);
            $rules['targetFrom'] = ['before_or_equal:targetTo'];
            $messages['targetFrom.before_or_equal'] = 'Date From must be before or equal Date To.';
            $rules['targetTo'] = ['before_or_equal:' . $maxEndDate->toDateString()];
            $messages['targetTo.before_or_equal'] = 'Date range you provided is too large. ' .
                'To Date should be on or before ' . $maxEndDate->format('d/m/Y') . '.';
            $validator = \Validator::make($inputs, $rules, $messages);
        }

        return $validator;
    }

    public function generateReport($repExcelFile, $inputs, $options = [], &$filterText = '')
    {
        $result = true;
        if (!($this->filters instanceof RbBookableResourceSearchFilter)) {
            $this->filters = new RbBookableResourceSearchFilter($this->formatInputData($inputs));
        }
        $this->formatFilterData();
        try {
            $this->writer = WriterEntityFactory::createXLSXWriter();
            $this->writer->openToFile($repExcelFile);
            $this->writerHeader();
            $resourceAvailabilities = $this->filterResourceAvailabilities()->get();
            $colKeys = array_keys($this->reportHeaders);
            if ($resourceAvailabilities->count()) {
                $availabilitiesByResource = $resourceAvailabilities->groupBy('rb_bookable_resource_code');
                $bookings = $this->filterBooking()->get();
                $startDate = Carbon::createFromFormat('Y-m-d', $this->filters->targetFrom);
                $endDate = Carbon::createFromFormat('Y-m-d', $this->filters->targetTo);
                while ($startDate->lessThanOrEqualTo($endDate)) {
                    if (in_array($startDate->dayOfWeek, $this->filters->days)) {
                        $availabilityDay = $startDate->format('D');
                        $dateString = $startDate->format('d/m/Y');
                        foreach ($availabilitiesByResource as $resourceCode => $availabilities) {
                            $availabilitiesFilteredByDay = $availabilities->filter(function (
                                $value,
                                $key
                            ) use ($startDate) {
                                return $this->checkDayInAvailability($value, $startDate->dayOfWeek);
                            });

                            if ($availabilitiesFilteredByDay->count()) {
                                $bookingsFilteredByDay = $bookings->filter(
                                    function ($value, $key) use ($startDate, $resourceCode, $bookings) {
                                        if (
                                            $value->booking_date == $startDate->format('Y-m-d') &&
                                            $value->rb_bookable_resource_code == $resourceCode
                                        ) {
                                            $bookings->forget($key);
                                            return true;
                                        }
                                        return false;
                                    }
                                );
                                $timeSlots = ($this->buildTimeSlots(
                                    $availabilitiesFilteredByDay,
                                    $bookingsFilteredByDay
                                ));
                                $resource = $availabilitiesFilteredByDay->first();
                                $resource->availability_day = $availabilityDay;
                                foreach ($timeSlots as $timeSlot) {
                                    $fromTime = $this->convertIntTimeToString($timeSlot['from']);
                                    $toTime = $this->convertIntTimeToString($timeSlot['to']);
                                    if ($timeSlot['isBooking']) {
                                        $booking = $bookingsFilteredByDay->firstWhere(
                                            'rb_resource_booking_id',
                                            $timeSlot['id']
                                        );
                                        $booking->booking_date = $dateString;
                                        $booking = $booking->toArray();
                                    } else {
                                        $booking = [
                                            'booking_start_time' => $fromTime,
                                            'booking_end_time' => $toTime,
                                            'booking_date' => $dateString,
                                            'rb_resource_booking_desc' => 'Available',
                                        ];
                                    }
                                    $row = $this->getReportRowData(
                                        array_merge($resource->toArray(), $booking),
                                        $colKeys
                                    );
                                    $this->writer->addRow(WriterEntityFactory::createRowFromArray($row));
                                }
                            }
                        }
                    }
                    $startDate->addDay(1);
                }
            }
            $this->writer->close();
            $whereCodes = [];               // The codes which have been used in the filter
            $whereTexts = [];               // Plain text about filtering applied
            $orCodes = [];                  // csv string of OR codes
            $bFilterDetail = false;
            $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
            $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $filterText);
        } catch (Exception $ex) {
            $result = false;
            \Log::error('RB07ReportService@generateReportt: ' . $ex->getMessage());
        }
        return $result;
    }

    private function buildTimeLine(Collection $availabilities, Collection $bookings)
    {
        $timeSlots = [];
        $availabilityTimeSlots = [];
        $availabilitySlotType = self::AVAILABILITY_SLOT_TYPE;
        $bookingSlotType = self::BOOKING_SLOT_TYPE;
        $startTimeMark = self::START_TIME_MARK;
        $endTimeMark = self::END_TIME_MARK;
        foreach ($availabilities as $availability) {
            $availabilityTimeSlots[] = [
                'time' => $availability->available_from_int,
                'slot_type' => $availabilitySlotType,
                'time_type' => $startTimeMark,
                'id' => $availability->rb_bookable_resource_availability_id,
            ];
            $availabilityTimeSlots[] = [
                'time' => $availability->available_to_int,
                'slot_type' => $availabilitySlotType,
                'time_type' => $endTimeMark,
                'id' => $availability->rb_bookable_resource_availability_id,
            ];
        }
        foreach ($availabilityTimeSlots as $timeSlot) {
            if (!array_key_exists($timeSlot['time'], $timeSlots)) {
                $timeSlots[$timeSlot['time']] = $timeSlot;
            } else {
                unset($timeSlots[$timeSlot['time']]);
            }
        }
        foreach ($bookings as $booking) {
            $timeSlots[] = [
                'time' => $booking->start_time_including_prep_int,
                'slot_type' => $bookingSlotType,
                'time_type' => $startTimeMark,
                'id' => $booking->rb_resource_booking_id,
            ];
            $timeSlots[] = [
                'time' => $booking->end_time_including_restore_int,
                'slot_type' => $bookingSlotType,
                'time_type' => $endTimeMark,
                'id' => $booking->rb_resource_booking_id,
            ];
        }

        $sorted = array_values(array_sort($timeSlots, function ($value) {
            return $value['time'];
        }));

        return $this->sortTimeSlot($sorted);
    }

    private function sortTimeSlot($timeSlots)
    {
        $bookingStartMark = self::BOOKING_SLOT_TYPE . self::START_TIME_MARK;
        $bookingEndMark = self::BOOKING_SLOT_TYPE . self::END_TIME_MARK;
        $totalTimeSlot = count($timeSlots);
        for ($i = 0; $i < $totalTimeSlot - 1; $i++) {
            $timeSlot = $timeSlots[$i];
            $nextTimeSlot = $timeSlots[$i + 1];
            $timeSlotMark = $timeSlot['slot_type'] . $timeSlot['time_type'];
            $nextTimeSlotMark = $nextTimeSlot['slot_type'] . $nextTimeSlot['time_type'];
            if ($timeSlotMark == $bookingStartMark && $nextTimeSlotMark != $bookingEndMark) {
                for ($j = $i + 2; $j < $totalTimeSlot; $j++) {
                    $jTimeSlot = $timeSlots[$j];
                    $jTimeSlotMark = $jTimeSlot['slot_type'] . $jTimeSlot['time_type'];
                    if ($jTimeSlotMark == $bookingEndMark && $jTimeSlot['id'] == $timeSlot['id']) {
                        $timeSlots[$i + 1] = $jTimeSlot;
                        $timeSlots[$j] = $nextTimeSlot;
                    }
                }
            }
        }
        return $timeSlots;
    }

    private function buildTimeSlots(Collection $availabilities, Collection $bookings)
    {
        $timeLine = $this->buildTimeLine($availabilities, $bookings);
        $timeSlots = [];
        $invalidMarks = [
            self::BOOKING_SLOT_TYPE . self::END_TIME_MARK . self::AVAILABILITY_SLOT_TYPE . self::START_TIME_MARK,
            self::AVAILABILITY_SLOT_TYPE . self::END_TIME_MARK . self::AVAILABILITY_SLOT_TYPE . self::START_TIME_MARK,
        ];
        $bookingMark = self::BOOKING_SLOT_TYPE . self::START_TIME_MARK;
        for ($i = 0; $i < (count($timeLine) - 1); $i++) {
            $currentSlot = $timeLine[$i];
            $nextSlot = $timeLine[$i + 1];
            $from = $currentSlot['time'];
            $to = $nextSlot['time'];
            $mark = $currentSlot['slot_type'] . $currentSlot['time_type'] .
                $nextSlot['slot_type'] . $nextSlot['time_type'];
            if ($from != $to && !in_array($mark, $invalidMarks)) {
                $isBooking = $currentSlot['slot_type'] . $currentSlot['time_type'] == $bookingMark;
                $timeSlots[] = [
                    'from' => $from,
                    'to' => $to,
                    'isBooking' => $isBooking,
                    'id' => $isBooking ? $currentSlot['id'] : ''
                ];
            }
        }
        return $timeSlots;
    }

    private function getReportRowData($data, $colKeys)
    {
        $row = [];
        foreach ($colKeys as $col) {
            $row[] = array_get($data, $col, '');
        }
        return $row;
    }

    private function writerHeader()
    {
        $headers = array_flatten($this->reportHeaders);
        $this->writer->addRow(WriterEntityFactory::createRowFromArray($headers));
    }

    private function filterCommonFields($query)
    {
        $query->where('rb_bookable_resource.site_group_id', \Auth::user()->site_group_id);
        if (!empty($this->filters->site_id)) {
            $query->where('rb_bookable_resource.site_id', $this->filters->site_id);
        }
        if (!empty($this->filters->building_id)) {
            $query->whereRaw("IFNULL(room_building.building_id, plant_building.building_id) = " .
                $this->filters->building_id);
        }
        if (!empty($this->filters->room_id)) {
            $query->where(function ($subQuery) {
                $subQuery->where("rb_bookable_resource.room_id", $this->filters->room_id);
                $subQuery->orWhere("room_plant.room_id", $this->filters->room_id);
            });
        }
        if (!empty($this->filters->plant_id)) {
            $query->where('rb_bookable_resource.plant_id', $this->filters->plant_id);
        }
        if (!empty($this->filters->resourceType)) {
            $query->where('rb_bookable_resource.rb_bookable_resource_type_id', $this->filters->resourceType);
        }
        return $query;
    }

    private function getLocationQuery($query)
    {
        $query->leftJoin('room', 'room.room_id', '=', 'rb_bookable_resource.room_id')
            ->leftJoin('plant', 'plant.plant_id', '=', 'rb_bookable_resource.plant_id')
            ->leftJoin('room AS room_plant', 'room_plant.room_id', '=', 'plant.room_id')
            ->leftJoin('building AS room_building', 'room_building.building_id', '=', 'room.building_id')
            ->leftJoin('building AS plant_building', 'plant_building.building_id', '=', 'plant.building_id');
        return $query;
    }

    private function filterResourceAvailabilities()
    {
        $query = $this->filterCommonFields($this->getResourceAvailabilitiesQuery());
        if (!empty($this->filters->days)) {
            if (count($this->filters->days) < 7) {
                $query->where(function ($sub) {
                    foreach ($this->filters->days as $day) {
                        switch ($day) {
                            case 0:
                                $sub->orWhere('rb_bookable_resource_availability.bookable_on_sun', 'Y');
                                break;
                            case 1:
                                $sub->orWhere('rb_bookable_resource_availability.bookable_on_mon', 'Y');
                                break;
                            case 2:
                                $sub->orWhere('rb_bookable_resource_availability.bookable_on_tue', 'Y');
                                break;
                            case 3:
                                $sub->orWhere('rb_bookable_resource_availability.bookable_on_wed', 'Y');
                                break;
                            case 4:
                                $sub->orWhere('rb_bookable_resource_availability.bookable_on_thu', 'Y');
                                break;
                            case 5:
                                $sub->orWhere('rb_bookable_resource_availability.bookable_on_fri', 'Y');
                                break;
                            case 6:
                                $sub->orWhere('rb_bookable_resource_availability.bookable_on_sat', 'Y');
                                break;
                        }
                    }
                });
            }
        } else {
            $query->whereRaw('1 <> 1');
        }
        return $query;
    }

    private function filterBooking()
    {
        $query = $this->filterCommonFields($this->getBookingQuery());
        $query->where(
            'rb_resource_booking_status.rb_resource_booking_status_type_id',
            '<>',
            RbResourceBookingStatusType::CANCELLED
        )->where(
            'rb_resource_booking_status.rb_resource_booking_status_type_id',
            '<>',
            RbResourceBookingStatusType::NO_SHOW
        )->where(
            \DB::raw("DATE_FORMAT(rb_resource_booking.start_time_including_prep, '%Y-%m-%d')"),
            '>=',
            $this->filters->targetFrom
        )->where(
            \DB::raw("DATE_FORMAT(rb_resource_booking.end_time_including_restore, '%Y-%m-%d')"),
            '<=',
            $this->filters->targetTo
        );
        if (count($this->filters->days) < 7) {
            /*
             * DAYOFWEEK MySQL start from 1 for Sunday, while $filters->days use PHP rule which start from 0 for Sunday
             * */
            $query->whereIn(
                \DB::raw("DAYOFWEEK(rb_resource_booking.start_time_including_prep) - 1"),
                $this->filters->days
            );
        }

        return $query;
    }

    private function getResourceAvailabilitiesQuery()
    {
        $query = RbBookableResourceLayout::
            join(
                'rb_bookable_resource',
                'rb_bookable_resource.rb_bookable_resource_id',
                '=',
                'rb_bookable_resource_layout.rb_bookable_resource_id'
            )
            ->leftJoin(
                'rb_bookable_resource_type',
                'rb_bookable_resource_type.rb_bookable_resource_type_id',
                '=',
                'rb_bookable_resource.rb_bookable_resource_type_id'
            )
            ->join(
                'rb_layout_availability',
                'rb_layout_availability.rb_bookable_resource_layout_id',
                '=',
                'rb_bookable_resource_layout.rb_bookable_resource_layout_id'
            )
            ->join('site', 'site.site_id', '=', 'rb_bookable_resource.site_id')
            ->join(
                'rb_bookable_resource_availability',
                'rb_bookable_resource_availability.rb_bookable_resource_availability_id',
                '=',
                'rb_layout_availability.rb_bookable_resource_availability_id'
            );
        $query = $this->getLocationQuery($query);
        $query->select([
            'site.site_code',
            'site.site_desc',
            \DB::raw('IFNULL(room_building.building_code, plant_building.building_code) AS building_code'),
            \DB::raw('IFNULL(room_building.building_id, plant_building.building_id) AS building_id'),
            \DB::raw("IF(room_building.building_id is NOT NULL, room_building.building_desc, " .
                "IF(plant_building.building_id IS NOT NULL, plant_building.building_desc, '')) AS building_desc"),
            'room.room_number',
            'room.room_desc',
            'plant.plant_code',
            'plant.plant_desc',
            'rb_bookable_resource.rb_bookable_resource_id',
            'rb_bookable_resource.rb_bookable_resource_code',
            'rb_bookable_resource.rb_bookable_resource_desc',
            'rb_bookable_resource.rb_bookable_resource_type_id',
            'rb_bookable_resource_type.rb_bookable_resource_type_code AS bookable_resource_type',
            'rb_bookable_resource.site_id',
            'rb_bookable_resource.room_id',
            'rb_bookable_resource.plant_id',
            'rb_bookable_resource_availability.rb_bookable_resource_availability_id',
            \DB::raw("TIME_FORMAT(rb_bookable_resource_availability.available_from, '%H:%i') AS available_from"),
            \DB::raw("TIME_FORMAT(rb_bookable_resource_availability.available_to, '%H:%i') AS available_to"),
            \DB::raw("TIME_FORMAT(rb_bookable_resource_availability.available_from, '%k%i') AS available_from_int"),
            \DB::raw("TIME_FORMAT(rb_bookable_resource_availability.available_to, '%k%i') AS available_to_int"),
            'rb_bookable_resource_availability.bookable_on_mon',
            'rb_bookable_resource_availability.bookable_on_tue',
            'rb_bookable_resource_availability.bookable_on_wed',
            'rb_bookable_resource_availability.bookable_on_thu',
            'rb_bookable_resource_availability.bookable_on_fri',
            'rb_bookable_resource_availability.bookable_on_sat',
            'rb_bookable_resource_availability.bookable_on_sun',
            \DB::raw("'' AS availability_day"),
        ]);
        $query->groupBy('rb_bookable_resource_availability.rb_bookable_resource_availability_id');
        $query->orderBy('rb_bookable_resource.rb_bookable_resource_code')
            ->orderBy('rb_bookable_resource_availability.available_from');
        return $this->permissionService->listViewSiteAccess($query, "rb_bookable_resource.site_id", $siteAccess);
    }

    private function getBookingQuery()
    {
        $query = RbResourceBooking::join(
            'rb_bookable_resource',
            'rb_bookable_resource.rb_bookable_resource_id',
            '=',
            'rb_resource_booking.layout_rb_bookable_resource_id'
        )->join(
            'rb_resource_booking_status',
            'rb_resource_booking_status.rb_resource_booking_status_id',
            '=',
            'rb_resource_booking.rb_booking_status_id'
        )->join(
            'rb_bookable_resource_layout',
            'rb_bookable_resource_layout.rb_bookable_resource_layout_id',
            '=',
            'rb_resource_booking.rb_bookable_resource_layout_id'
        );
        $query = $this->getLocationQuery($query);
        $query->orderBy('rb_bookable_resource.rb_bookable_resource_code')
            ->orderBy('rb_resource_booking.start_time_including_prep');
        $query->select([
            'rb_resource_booking.rb_resource_booking_id',
            'rb_resource_booking.rb_bookable_resource_layout_id',
            'rb_resource_booking.layout_rb_bookable_resource_id AS rb_bookable_resource_id',
            'rb_bookable_resource.rb_bookable_resource_code',
            'rb_resource_booking.rb_resource_booking_code',
            'rb_resource_booking.rb_resource_booking_desc',
            'rb_resource_booking.booked_for_name',
            'rb_resource_booking.start_time_including_prep',
            'rb_resource_booking.start_time',
            'rb_resource_booking.end_time',
            'rb_resource_booking.end_time_including_restore',
            \DB::raw("DATE_FORMAT(rb_resource_booking.start_time_including_prep, '%Y-%m-%d') AS booking_date"),
            \DB::raw("DATE_FORMAT(rb_resource_booking.start_time_including_prep, '%H:%i') AS booking_prep_start_time"),
            \DB::raw("DATE_FORMAT(rb_resource_booking.start_time, '%H:%i') AS booking_start_time"),
            \DB::raw("DATE_FORMAT(rb_resource_booking.end_time, '%H:%i') AS booking_end_time"),
            \DB::raw("DATE_FORMAT(rb_resource_booking.end_time_including_restore,'%H:%i') AS booking_restore_end_time"),
            \DB::raw("TIME_FORMAT(rb_resource_booking.start_time_including_prep, '%k%i') AS " .
                "start_time_including_prep_int"),
            \DB::raw("TIME_FORMAT(rb_resource_booking.end_time_including_restore, '%k%i') AS " .
                "end_time_including_restore_int"),
            'rb_resource_booking_status.rb_resource_booking_status_code',
            'rb_resource_booking_status.rb_resource_booking_status_desc',
            'rb_bookable_resource_layout.rb_bookable_resource_layout_code',
            'rb_bookable_resource_layout.rb_bookable_resource_layout_desc',
            \DB::raw("(CONCAT_WS(' - ', rb_bookable_resource_layout.rb_bookable_resource_layout_code, " .
                "rb_bookable_resource_layout.rb_bookable_resource_layout_desc)) AS booking_layout"),
            \DB::raw("(CONCAT_WS(' - ', rb_resource_booking_status.rb_resource_booking_status_code, " .
                "rb_resource_booking_status.rb_resource_booking_status_desc)) AS booking_status"),
        ]);
        return $this->permissionService->listViewSiteAccess($query, "rb_bookable_resource.site_id", $siteAccess);
    }

    private function formatInputData($inputs)
    {
        if ($targetFrom = array_get($inputs, 'targetFrom')) {
            $inputs['targetFrom'] =  Common::dateFieldToSqlFormat($targetFrom, ['includeTime' => false]);
        }
        if ($targetTo = array_get($inputs, 'targetTo')) {
            $inputs['targetTo'] =  Common::dateFieldToSqlFormat($targetTo, ['includeTime' => false]);
        }
        $days = array_get($inputs, 'days');
        if (is_array($days) && count($days)) {
            $mapDays = ['Mon' => 1, 'Tue' => 2, 'Wed' => 3, 'Thu' => 4, 'Fri' => 5, 'Sat' => 6, 'Sun' => 0];
            $formattedDays = [];
            foreach ($days as $day) {
                if (array_key_exists($day, $mapDays)) {
                    $formattedDays[] = $mapDays[$day];
                }
            }
            $inputs['days'] = $formattedDays;
        }
        return $inputs;
    }

    private function formatFilterData()
    {
        $startDate = Carbon::createFromFormat('Y-m-d', $this->filters->targetFrom);
        $endDate = Carbon::createFromFormat('Y-m-d', $this->filters->targetTo);
        if ($startDate->diffInDays($endDate) < 6) {
            $mergedDays = [];
            while ($startDate->lessThanOrEqualTo($endDate)) {
                $day = $startDate->dayOfWeek;
                if (in_array($day, $this->filters->days)) {
                    $mergedDays[] = $day;
                }
                $startDate = $startDate->addDay(1);
            }
            $this->filters->days = array_unique($mergedDays);
        }
    }

    private function checkDayInAvailability($availability, $day)
    {
        $yesValue = CommonConstant::DATABASE_VALUE_YES;
        switch ($day) {
            case 0:
                return $availability->bookable_on_sun == $yesValue;
                break;
            case 1:
                return $availability->bookable_on_mon == $yesValue;
                break;
            case 2:
                return $availability->bookable_on_tue == $yesValue;
                break;
            case 3:
                return $availability->bookable_on_wed == $yesValue;
                break;
            case 4:
                return $availability->bookable_on_thu == $yesValue;
                break;
            case 5:
                return $availability->bookable_on_fri == $yesValue;
                break;
            case 6:
                return $availability->bookable_on_sat == $yesValue;
                break;
        }
        return false;
    }
    private function convertIntTimeToString($time)
    {
        $time =  sprintf("%" . sprintf("0%s", 4) . "u", $time);
        return Carbon::createFromFormat('Gi', $time)->format('H:i');
    }

    private function generateFilterData($filterData, &$whereCodes, &$whereTexts, &$orCodes)
    {
        if ($val = Common::iset($filterData['targetFrom'])) {
            array_push($whereTexts, "Date From contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['targetTo'])) {
            array_push($whereTexts, "Date To contains '" . $val . "'");
        }

        if ($siteId = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $siteId, "Site Code"]);

            if ($buildingId = Common::iset($filterData['building_id'])) {
                array_push(
                    $whereCodes,
                    ['building', 'building_id', 'building_code', $buildingId, "Building Code"]
                );

                if ($val = Common::iset($filterData['block_id'])) {
                    array_push($whereCodes, [
                        'building_block',
                        'building_block_id',
                        'building_block_code',
                        $val,
                        "Building Block Code"
                    ]);
                }

                if ($val = Common::iset($filterData['room_id'])) {
                    array_push($whereCodes, ['room', 'room_id', 'room_number', $val, "Room Number"]);
                }
            }
        }

        if ($val = array_get($filterData, 'plant_id', null)) {
            array_push($whereCodes, ['plant', 'plant_id', 'plant_code', $val, 'Plant']);
        }

        if ($val = Common::iset($filterData['resourceType'])) {
            array_push(
                $whereCodes,
                [
                    'rb_bookable_resource_type',
                    'rb_bookable_resource_type_id',
                    'rb_bookable_resource_type_code',
                    $val,
                    "Resource Type"
                ]
            );
        }

        $days = array_get($filterData, 'days', []);
        if (count($days)) {
            $text = 'Available On = ' . implode(', ', $days);
            array_push($whereTexts, $text);
        }
    }
}
