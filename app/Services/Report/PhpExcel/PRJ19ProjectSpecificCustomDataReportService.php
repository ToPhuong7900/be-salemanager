<?php

namespace Tfcloud\Services\Report\PhpExcel;

use Box\Spout\Writer\Common\Creator\Style\StyleBuilder;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Exception;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Constant\ReportConstant;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Models\Project;
use Tfcloud\Models\Report;
use Tfcloud\Services\PermissionService;
use Tfcloud\services\Project\ProjectAccessService;
use Tfcloud\Services\Report\FilterQuery\ProjectsFilterQueryService;
use Tfcloud\Models\Project\ProjectFieldType;

/**
 * Class PRJ19ProjectSpecificCustomDataReportService
 * @package Tfcloud\Services\Report\PhpExcel
 * @reportName: Project Specific Custom Data Report
 * @reportCode: PRJ19
 */
class PRJ19ProjectSpecificCustomDataReportService extends PhpExcelReportBaseService
{
    private $reportHeaders = [
        'project_code' => 'Project Code',
        'project_desc' => 'Project Description',
        'site_code' => 'Site Code',
        'site_desc' => 'Site Description',
        'building_code' => 'Building Code',
        'building_desc' => 'Building Description',
        'project_status_code' => 'Project Status',
        'project_priority_code' => 'Project Priority Code',
        'project_priority_desc' => 'Project Priority Description',
        'project_programme_code' => 'Programme Code',
        'project_programme_desc' => 'Programme Description',
        'project_portfolio_code' => 'Project Portfolio Code',
        'project_portfolio_desc' => 'Project Portfolio Description',
        'project_type_code' => 'Project Type Code',
        'project_type_desc' => 'Project Type Description',
        'project_stage' => 'Project Stage',
        'project_field_group_code' => 'Project Group Code',
        'project_field_group_desc' => 'Project Group Description',
        'project_field_code' => 'Project Field Code',
        'project_field_desc' => 'Project Field Description',
        'label' => 'Form Label',
        'report_value' => 'Value',
    ];

    //Spout engine
    private $writer;
    private $projectAccessService;
    private $projectsFilterQueryService;
    private $reportBoxFilterLoader = null;

    private $whereCodes = [];
    private $orCodes = [];
    private $whereTexts = [];

    public function __construct(PermissionService $permissionService, Report $report = null)
    {
        parent::__construct($permissionService);
        $this->projectAccessService = new ProjectAccessService($this->permissionService);
        $this->projectsFilterQueryService = new ProjectsFilterQueryService($this->permissionService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    /**
     * @note: main function
     * @param $repExcelFile
     * @param $inputs
     * @param array $options
     * @param string $filterText
     * @return bool
     */
    public function generateReport($repExcelFile, $inputs, $options = [], &$filterText = '')
    {
        $result = true;
        try {
            $this->writer = WriterEntityFactory::createXLSXWriter();
            $this->writer->openToFile($repExcelFile);
            $data = $this->getData($inputs, $filterText);

            //write excel
            $this->writerHeader();
            $this->writeBody($data);
            $this->writeFooter($data, $filterText);

            $this->writer->close();
        } catch (Exception $ex) {
            $result = false;
            \Log::error('PRJ19ProjectSpecificCustomDataReportService@generateReport: ' . $ex->getMessage());
        }
        return $result;
    }

    private function writerHeader()
    {
        $headerStyle = (new StyleBuilder())->setFontBold()->build();
        $headers = array_values($this->reportHeaders);
        $this->writer->addRow(WriterEntityFactory::createRowFromArray($headers, $headerStyle));
    }

    private function writeBody($data)
    {
        $colKeys = array_keys($this->reportHeaders);
        $data->map(function ($record) use ($colKeys) {
            return $record->only($colKeys);
        })->each(function ($record) {
            $this->writer->addRow(WriterEntityFactory::createRowFromArray($record));
        });
    }

    private function writeFooter($data, &$filterText)
    {
        $bFilterDetail = false;
        $this->getFiltering(
            $this->whereCodes,
            $this->orCodes,
            $this->whereTexts,
            $bFilterDetail,
            $filterText
        );

        $reportTitle = array_get($this->returnedData, 'repTitle', ReportConstant::SYSTEM_REPORT_PRJ19);
        $this->writeLine();
        $this->writeLine("Report: $reportTitle");

        if ($filterText) {
            $this->writeLine('Filtering details:');
            $this->writeLine($filterText);
        }

        $this->writeLine('Ordering details:');
        $this->writeLine('`Site Code` (ascending), `Project Code` (ascending)');

        $total = count($data);
        if ($total) {
            $this->writeLine('Rows returned:');
            $this->writeLine("Returned $total rows (from 1 to $total) of $total rows.");
        } else {
            $this->writeLine();
            $this->writeLine('No data found.');
        }
    }

    private function getData($inputs, &$filterText)
    {
        $select = [
            'project.project_code',
            'project.project_desc',
            'site.site_code',
            'site.site_desc',
            'building.building_code',
            'building.building_desc',
            'project_status.project_status_code',
            'project_priority.project_priority_code',
            'project_priority.project_priority_desc',
            'project_programme.project_programme_code',
            'project_programme.project_programme_desc',
            'project_portfolio.project_portfolio_code',
            'project_portfolio.project_portfolio_desc',
            'project_type.project_type_code',
            'project_type.project_type_desc',
            \DB::raw(
                "CONCAT_WS(' - ', project_stage.project_stage_code, project_stage.project_stage_name) AS project_stage"
            ),
            'project_field_group.project_field_group_code',
            'project_field_group.project_field_group_desc',
            'project_field.project_field_code',
            'project_field.project_field_desc',
            'project_field.label',
            \Db::raw(" CASE WHEN project_field.project_field_type_id = "
                . ProjectFieldType::BOOLEAN . " THEN "
                . "  COALESCE(project_field_value.report_value, 'N') "
                . " WHEN project_field.project_field_type_id = "
                . ProjectFieldType::SELECTION . " THEN "
                . " CONCAT_WS(' - ', project_selection_field.project_selection_field_code,"
                    . " project_selection_field.project_selection_field_desc) "
                . " ELSE "
                . "  project_field_value.report_value "
                . " END AS report_value "),

        ];

        $query = Project::UserSiteGroup()
            ->select($select)
            ->join('site', 'site.site_id', '=', 'project.site_id')
            ->leftJoin('building', 'building.building_id', '=', 'project.building_id')
            ->leftJoin('project_status', 'project_status.project_status_id', 'project.project_status_id')
            ->leftJoin('project_priority', 'project_priority.project_priority_id', 'project.project_priority_id')
            ->leftJoin('project_programme', 'project_programme.project_programme_id', 'project.project_programme_id')
            ->leftJoin('project_portfolio', 'project_portfolio.project_portfolio_id', 'project.project_portfolio_id')
            ->leftJoin('project_stage', 'project_stage.project_stage_id', 'project.project_stage_id')
            ->join('project_type', 'project_type.project_type_id', 'project.project_type_id')
            ->join(
                'project_field_group',
                'project_field_group.project_field_group_id',
                'project_type.project_field_group_id'
            )
            ->join(
                'project_field',
                'project_field.project_field_group_id',
                'project_field_group.project_field_group_id'
            )
            ->leftJoin('project_field_value', function ($join) {
                $join->on('project_field_value.project_field_id', 'project_field.project_field_id');
                $join->on('project_field_value.project_id', 'project.project_id');
            })
            ->leftJoin('project_selection_group', function ($join) {
                $join->on(
                    'project_selection_group.project_selection_group_id',
                    'project_field.project_selection_group_id'
                );
                $join->where(
                    'project_field.project_field_type_id',
                    ProjectFieldType::SELECTION
                );
            })
            ->leftJoin('project_selection_field', function ($join) {
                $join->on(
                    'project_selection_field.project_selection_group_id',
                    'project_selection_group.project_selection_group_id'
                );
                $join->on('project_field_value.report_value', 'project_selection_field.project_selection_field_id');
            })
            ->where('project_field.active', CommonConstant::DATABASE_VALUE_YES)
            ->where('project_field_group.active', CommonConstant::DATABASE_VALUE_YES)

            ->orderBy('site.site_code')
            ->orderBy('project.project_code')
        ;

        $query = $this->permissionService->listViewSiteAccess($query, "project.site_id");
        $query = $this->projectAccessService->projectUserAccess($query, 'project');

        //filter and log
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $data = $reportFilterQuery->filterAll($query)->get();
            $filterText = $reportFilterQuery->getFilterDetailText();
        } else {
            $query = $this->projectsFilterQueryService->reAddProjectQuery(
                $inputs,
                false,
                $query,
                $this->whereCodes,
                $this->orCodes,
                $this->whereTexts
            );
            $data = $query->get();
        }

        return $data;
    }

    private function writeLine($value = '')
    {
        $this->writer->addRow(WriterEntityFactory::createRowFromArray([$value]));
    }
}
