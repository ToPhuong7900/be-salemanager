<?php

namespace Tfcloud\Services\Report\PhpExcel;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Traits\EmailTrait;
use Tfcloud\Models\CondLocationScore;
use Tfcloud\Models\CondLocationType;
use Tfcloud\Services\Condition\ConditionLocationScoreExcelBaseService;
use Tfcloud\Services\Condition\ConditionService;
use Tfcloud\Services\PermissionService;

class CondCoreFactsExportService extends ConditionLocationScoreExcelBaseService
{
    use EmailTrait;



    public function __construct(
        PermissionService $permissionService
    ) {
        parent::__construct($permissionService);
        $this->conditionService = new ConditionService($permissionService);
    }

    public function generateReport($conditionSurvey, $inputs = [], $options = [], &$indexedBlock = self::SHEET_INDEX_1)
    {
        $supportCalculate = array_get($options, 'support_calculate', false);
        $supportCalculateData = array_get($options, 'support_calculate_data', false);

        $blocks = $this->getAllCondSurveyBlocks($conditionSurvey)
            ->limit(8)->orderBy('building_code')->get();

        if ($supportCalculate && $supportCalculateData) {
            $blocks->each(function ($core, $index) use ($supportCalculateData, &$indexedBlock) {
                if ($core->cond_location_score_id == $supportCalculateData->cond_location_score_id) {
                    $core->score_id = $supportCalculateData->score_id;
                    $core->cond_location_score_comment = $supportCalculateData->cond_location_score_comment;
                    $core->weighted_score_total = $supportCalculateData->weighted_score_total;
                    $core->cond_weighted_score_grade_id = $supportCalculateData->cond_weighted_score_grade_id;
                    $indexedBlock = $index + 1;
                    return false;
                }
            });
        }

        $reportFileName = 'CoreFactsExport_' . $conditionSurvey->condsurvey_code;
        $repExcelFile = $this->generateReportOutPutPath($reportFileName, 'xlsx');
        $condSurveyCode = $conditionSurvey->condsurvey_code;

        $templateFile = app_path()
            . DIRECTORY_SEPARATOR . "templates"
            . DIRECTORY_SEPARATOR . "phpExcel"
            . DIRECTORY_SEPARATOR . "CoreFactExport.xlsx";
        $phpExcelObj = $this->readFile($templateFile);

        if (is_readable($templateFile)) {
            copy($templateFile, $repExcelFile);
        }

        $summaryData = $this->getReportSummaryData($conditionSurvey, $blocks);
        $this->fillDataToExcel($phpExcelObj, $summaryData);
        $this->fillConstantData($phpExcelObj);
        for ($i = 0; $i < $blocks->count(); $i++) {
            $blockIndex = $i + 1;
            $data = $this->getBlockData($blocks[$i], "Block {$blockIndex}");
            $phpExcelObj->setActiveSheetIndex($blockIndex);
            $this->fillDataToExcel($phpExcelObj, $data);
            $this->fillScore($phpExcelObj, $blocks[$i]);
        }

        if ($supportCalculate) {
            return $phpExcelObj;
        }

        $errorMessages = null;
        if (is_null($errorMessages)) {
            $errorMessages = $this->writeExcelFile($phpExcelObj, $repExcelFile, 'Xlsx', true);
        }

        if ($errorMessages) {
            return false;
        }

        if (array_get($inputs, 'send_email', false)) {
            $user = \Auth::user();
            $fileName = $condSurveyCode;
            $emailData = array(
                'mailTo'        => $user->email_reply_to,
                'displayName'   => $user->siteGroup->email_return_address,
                'repTitle'      => $fileName,
                'subject'       => $fileName . ' Report',
            );
            return $this->sendEmail($repExcelFile, $emailData, $conditionSurvey);
        }
        return $repExcelFile;
    }

    private function getReportSummaryData($condSurvey, $blocks)
    {
        $site = $condSurvey->site ? : null;
        $address = $site->address ? [
            $site->address->second_addr_obj,
            $site->address->first_addr_obj,
            $site->address->addressStreet->street,
            $site->address->addressStreet->locality,
            $site->address->addressStreet->town,
            $site->address->postcode
        ] : null;
        $siteAddress = $site ? Common::concatFields($address, ['separator' => ', ']) : '';
        $maxStorey = $site ? $site->buildings->where('active', CommonConstant::DATABASE_VALUE_YES)
            ->max('number_of_storeys') : '';
        $siteGroupDesc = \Auth::user()->siteGroup->description;
        $reportName = "{$siteGroupDesc} ASSET MANAGEMENT - PROPERTY CONDITION SURVEY";
        $data = [
            'A1' => $reportName,
            'B3' => $site ? $site->site_desc : '',
            'B4' => $siteAddress,
            'B6' => $site ? $site->dfe_county_code : '',
            'B7' => $site ? $site->dfe_number : '',
            'K4' => $maxStorey,
            'K5' => $condSurvey->condsurvey_desc,
            'K7' => $condSurvey->surveyorContact ? $condSurvey->surveyorContact->contact_name : '',
            'K8' => Common::dateFieldDisplayFormat($condSurvey->survey_date),
            'A17' =>
                $blocks->has(0) ?
                    Common::concatFields(
                        [
                            "Block 1",
                            $blocks[0]->building_code,
                            $blocks[0]->building_block_code
                        ]
                    ) : 'Block 1',
            'A18' =>
                $blocks->has(1) ?
                    Common::concatFields(
                        [
                            "Block 2",
                            $blocks[1]->building_code,
                            $blocks[1]->building_block_code
                        ]
                    ) : 'Block 2',
            'A19' =>
                $blocks->has(2) ?
                    Common::concatFields(
                        [
                            "Block 3",
                            $blocks[2]->building_code,
                            $blocks[2]->building_block_code
                        ]
                    ) : 'Block 3',
            'A20' =>
                $blocks->has(3) ?
                    Common::concatFields(
                        [
                            "Block 4",
                            $blocks[3]->building_code,
                            $blocks[3]->building_block_code
                        ]
                    ) : 'Block 4',
            'A21' =>
                $blocks->has(4) ?
                    Common::concatFields(
                        [
                            "Block 5",
                            $blocks[4]->building_code,
                            $blocks[4]->building_block_code
                        ]
                    ) : 'Block 5',
            'A22' =>
                $blocks->has(5) ?
                    Common::concatFields(
                        [
                            "Block 6",
                            $blocks[5]->building_code,
                            $blocks[5]->building_block_code
                        ]
                    ) : 'Block 6',
            'A23' =>
                $blocks->has(6) ?
                    Common::concatFields(
                        [
                            "Block 7",
                            $blocks[6]->building_code,
                            $blocks[6]->building_block_code
                        ]
                    ) : 'Block 7',
            'A24' =>
                $blocks->has(7) ?
                    Common::concatFields(
                        [
                            "Block 8",
                            $blocks[7]->building_code,
                            $blocks[7]->building_block_code
                        ]
                    ) : 'Block 8',
        ];
        return $data;
    }

    private function fillDataToExcel(Spreadsheet &$phpExcelObj, $data)
    {
        foreach ($data as $cell => $value) {
            $phpExcelObj->getActiveSheet()->setCellValue($cell, $value);
        }
    }

    private function fillConstantData(Spreadsheet &$phpExcelObj)
    {
        $siteGroupDesc = \Auth::user()->siteGroup->description;
        $reportName = "{$siteGroupDesc} ASSET MANAGEMENT - PROPERTY CONDITION SURVEY";
        $allScoreCells = $this->getElements();
        for ($i = 0; $i <= self::MAX_BLOCK_COUNT; $i++) {
            $phpExcelObj->setActiveSheetIndex($i);
            $phpExcelObj->getActiveSheet()->setCellValue('A1', $reportName);
            foreach ($allScoreCells as $cell => $values) {
                $phpExcelObj->getActiveSheet()->getStyle($cell)->applyFromArray([
                    'fill' => [
                        'type' => Fill::FILL_SOLID,
                        'color' => ['rgb' => 'E6FFFF']
                    ]
                ]);
            }
        }
    }

    private function getBlockData($block, $blockTitle)
    {
        $yearBuilt = $this->getYearBuilt($block);
        $maxStorey = $block->cond_location_type_id == CondLocationType::TYPE_BLOCK ?
            $block->buildingBlock->building->number_of_storeys : $block->building->number_of_storeys;
        $gia = $block->cond_location_type_id == CondLocationType::TYPE_BLOCK ?
            $block->buildingBlock->gia : $block->building->gia;
        $data = [
            'B9' => Common::concatFields([$blockTitle, $block->building_code, $block->building_block_code]),
            'B10' => $yearBuilt,
            'T3' => $gia,
            'T4' => $maxStorey,
            'T5' => $block->condsurvey->condsurvey_desc,
            'T9' => $block->condsurvey->surveyorContact ? $block->condsurvey->surveyorContact->contact_name : '',
            'T10' => Common::dateFieldDisplayFormat($block->condsurvey->survey_date),
        ];
        return $data;
    }

    private function getYearBuilt($block)
    {
        $yearBuilt = '';
        if ($block->cond_location_type_id == CondLocationType::TYPE_BLOCK) {
            $yearBuilt = $block->buildingBlock->year_built;
        } else {
            $buildingBlocks = $block->building->buildingBlocks;
            if ($buildingBlocks->count() == 1) {
                $yearBuilt = $buildingBlocks[0]->year_built;
            }
        }

        return $yearBuilt;
    }
}
