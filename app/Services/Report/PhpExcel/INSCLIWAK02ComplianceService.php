<?php

namespace Tfcloud\Services\Report\PhpExcel;

use Box\Spout\Common\Entity\Style\Border as SpoutBorder;
use Box\Spout\Common\Entity\Style\Color;
use Box\Spout\Writer\Common\Creator\Style\BorderBuilder;
use Box\Spout\Writer\Common\Creator\Style\StyleBuilder;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\BoxFilterQuery\FilterQueryBootstrap;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Debug;
use Tfcloud\Lib\Filters\Properties\PR21ComplianceFilter;
use Tfcloud\Models\AsbHsg264Type;
use Tfcloud\Models\AsbSurveyStatus;
use Tfcloud\Models\ComplianceExemption;
use Tfcloud\Models\Filter\FilterName;
use Tfcloud\Models\InspectionSFG20\InspSFG20InspectionStatusType;
use Tfcloud\Models\InspectionSFG20\InspSFG20ScheduledInspection;
use Tfcloud\Models\InspectionSFG20\InspSFG20Type;
use Tfcloud\Models\Report;
use Tfcloud\Models\Site;
use Tfcloud\Models\User;
use Tfcloud\Services\PermissionService;

class INSCLIWAK02ComplianceService extends PhpExcelReportBaseService
{
    private const HEADER_COLOR_HEX = 'DCEAF5';
    private const WHITE_COLOR_HEX = 'FFFFFF';
    private const RED_HEX = 'FF0000';
    private const AMBER_HEX = 'FFBF00';
    private const GREEN_HEX = '71AB48';
    private const GRAY_HEX = 'bfbfbf';
    private const RAG_COLOR_RED = 'RED';
    private const RAG_COLOR_AMBER = 'AMBER';
    private const RAG_COLOR_GREEN = 'GREEN';
    private const RAG_COLOR_RED_CSS = 'center background-red';
    private const RAG_COLOR_AMBER_CSS = 'center background-amber';
    private const RAG_COLOR_GREEN_CSS = 'center background-green';
    private const RAG_COLOR_GREY = 'GREY';
    private const RAG_COLOR_GREY_CSS = 'center background-grey';

    private $siteGroupId = null;

    private $filterQuery = null;
    private $reportBoxFilterLoader = null;

    public function __construct(
        PermissionService $permissionService,
        Report $report = null
    ) {
        parent::__construct($permissionService);
        $this->siteGroupId = \Auth::user()->site_group_id;
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }
    public function validateFilter($inputs)
    {
        $this->formatInputData($inputs);

        return \Validator::make($inputs, [], []);
    }

    private function formatInputData(&$inputs)
    {
        foreach ($inputs as $key => $value) {
            if ($value === "") {
                unset($inputs[$key]);
            }
        }
        return $inputs;
    }

    private function initFilterQuery($inputs)
    {
        $exportFromNewFilter = false;

        if (array_key_exists('exportAs', $inputs)) {
            $exportFromNewFilter = strtolower(array_get($inputs['exportAs'], 'label', false));
        } elseif (array_key_exists('fileExtension', $inputs)) {
            $exportFromNewFilter = array_get($inputs, 'fileExtension', false);
        }

        if ($exportFromNewFilter) {
            if (
                $this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()
                && array_key_exists('exportAs', $inputs)
            ) {
                $this->filterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            } else {
                $this->filterQuery = FilterQueryBootstrap::getFilterQuery(
                    FilterName::INSPECTION_COMPLIANCE_BY_LOCATION,
                    $inputs
                );
            }
        }

        return $exportFromNewFilter === 'html';
    }

    public function generateReport($repExcelFile, $inputs, $options = [], &$filterText = '')
    {
        $result = true;
        $exportFromNewFilter = $this->initFilterQuery($inputs);
        $html = (array_get($inputs, 'exportHTML', false) || $exportFromNewFilter);

        try {
            $reportData = $this->fetchData($filterText, $inputs);
            $errorMessages = $this->generateBoxSpoutObject($repExcelFile, $inputs, $reportData);

            if (is_array($errorMessages) && count($errorMessages)) {
                $result = false;
            }
        } catch (\Exception $ex) {
            $result = false;
        }

        $whereCodes = [];               // The codes which have been used in the filter
        $whereTexts = [];               // Plain text about filtering applied
        $orCodes = [];                  // csv string of OR codes
        $bFilterDetail = false;

        $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
        $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $filterText);
        return $result;
    }

    private function fetchData(&$filterText, $inputs = [])
    {
        // Filter by Location to scope data
        $filter = new PR21ComplianceFilter($inputs);
        $siteId = null;
        $buildingId = null;
        // Get data with filter by
        return $this->getReportData($siteId, $buildingId, $filter);
    }
    private function generateBoxSpoutObject($repExcelFile, $inputs = [], $reportData = [], $options = [])
    {
        $errorMessages = null;
        try {
            $writer = WriterEntityFactory::createXLSXWriter();
            $writer->openToFile($repExcelFile); // write data to a file or to a PHP stream

            //define Styles
            $borderStyle = (new BorderBuilder())
                ->setBorderTop(Color::BLACK, SpoutBorder::WIDTH_THIN, SpoutBorder::STYLE_SOLID)
                ->setBorderRight(Color::BLACK, SpoutBorder::WIDTH_THIN, SpoutBorder::STYLE_SOLID)
                ->setBorderBottom(Color::BLACK, SpoutBorder::WIDTH_THIN, SpoutBorder::STYLE_SOLID)
                ->setBorderLeft(Color::BLACK, SpoutBorder::WIDTH_THIN, SpoutBorder::STYLE_SOLID)
                ->build();

            $headerStyle = (new StyleBuilder())->setBorder($borderStyle)
                ->setBackgroundColor(self::HEADER_COLOR_HEX)
                ->setFontBold()
                ->build();

            $normalStyle = (new StyleBuilder())
                ->setBorder($borderStyle)
                ->setBackgroundColor(self::WHITE_COLOR_HEX)
                ->build();
            $redStyle =  (new StyleBuilder())
                ->setBorder($borderStyle)
                ->setBackgroundColor(self::RED_HEX)
                ->build();
            $greenStyle = (new StyleBuilder())
                ->setBorder($borderStyle)
                ->setBackgroundColor(self::GREEN_HEX)
                ->build();
            $emberStyle = (new StyleBuilder())
                ->setBorder($borderStyle)
                ->setBackgroundColor(self::AMBER_HEX)
                ->build();
            $grayStyle = (new StyleBuilder())
                ->setBorder($borderStyle)
                ->setBackgroundColor(self::GRAY_HEX)
                ->build();

            //prepare header
            $fixedHeaderArray = $this->getFixedHeaders();
            $dynamicHeaderArray = $this->getReportHeaderData();

            $firstHeaderRow = [null, null];
            $secondHeaderRow = $fixedHeaderArray;

            foreach ($dynamicHeaderArray as $inspectionType) {
                $firstHeaderRow[] = $inspectionType;
                $firstHeaderRow[] = null;
                $secondHeaderRow[] = 'Last Scheduled Inspection';
                $secondHeaderRow[] = 'Next Scheduled Inspection';
            }

            $writer->addRow(WriterEntityFactory::createRowFromArray($firstHeaderRow, $headerStyle));
            $writer->addRow(WriterEntityFactory::createRowFromArray($secondHeaderRow, $headerStyle));

            $bodyRows = [];

            foreach ($reportData as $recordArr) {
                if (!is_array($recordArr) || !count($recordArr)) {
                    continue;
                }
                $cellsOfARow = [];
                foreach ($recordArr as $cellData) {
                    $value = array_get($cellData, 'value');
                    $RAGColor = array_get($cellData, 'color');
                    $cellStyle = null;

                    switch ($RAGColor) {
                        case self::RAG_COLOR_RED:
                            $cellStyle = $redStyle;
                            break;
                        case self::RAG_COLOR_AMBER:
                            $cellStyle = $emberStyle;
                            break;
                        case self::RAG_COLOR_GREEN:
                            $cellStyle = $greenStyle;
                            break;
                        case self::RAG_COLOR_GREY:
                            $cellStyle = $grayStyle;
                            break;
                        default:
                            $cellStyle = $normalStyle;
                            break;
                    }

                    $cell = WriterEntityFactory::createCell($value, $cellStyle);
                    $cellsOfARow[] = $cell;
                }
                $bodyRows[] = WriterEntityFactory::createRow($cellsOfARow);
            }

            $writer->addRows($bodyRows);
            $writer->close();
        } catch (\Exception $ex) {
            $errorMessages = $ex->getMessage();
        }

        return $errorMessages;
    }

    public function getReportHeaderData($siteId = null, $buildingId = null)
    {
        $header = [];
        $inspectionTypes = $this->getInspectionTypes();

        foreach ($inspectionTypes as $inspectionType) {
            $header = array_add(
                $header,
                $inspectionType->insp_sfg20_type_version_id,
                $inspectionType->inspection_type_header
            );
        }

        return $header;
    }

    public function getReportData($siteId = null, $buildingId = null, $filter = null)
    {
        $inspectionTypes = $this->getInspectionTypes();
        // Refactor this when applied new filter for report PR21

        $siteQuery = $this->getSitesQuery();

        $sites = $siteQuery->get();

        $data = [];
        foreach ($sites as $site) {
            if (is_null($buildingId)) {
                $row = [
                    ['value' => $site->site_code],
                    ['value' => $site->site_desc]
                ];

                //build inspection data for site level including 2 columns
                $this->generateInspection($row, $inspectionTypes, $site->site_id);
                array_push($data, $row);
            }
        }

        return $data;
    }

    private function getFixedHeaders()
    {
        return [
            'A1' => 'Site Code',
            'B1' => 'Site Description'
        ];
    }

    public function buildAsbSurveyQuery(&$query): void
    {
        $query->join(
            'asb_survey_type',
            'asb_survey_type.survey_type_id',
            '=',
            'asb_survey.survey_type_id'
        )
            ->where('survey_status_id', AsbSurveyStatus::CURRENT)
            ->where('asb_survey_type.hsg264_type_id', AsbHsg264Type::MANAGEMENT);

            $query->whereNull('asb_survey.building_id');
    }

    private function getInspections(
        $siteId,
        $buildingId = false
    ) {
        $query = InspSFG20ScheduledInspection::userSiteGroup()
            ->join(
                'insp_sfg20_inspection_version',
                'insp_sfg20_inspection_version.insp_sfg20_inspection_version_id',
                '=',
                'insp_sfg20_scheduled_inspection.insp_sfg20_inspection_version_id'
            )
            ->join(
                'insp_sfg20_inspection',
                'insp_sfg20_inspection.insp_sfg20_inspection_id',
                '=',
                'insp_sfg20_inspection_version.insp_sfg20_inspection_id'
            )
            ->join(
                'insp_sfg20_type_version',
                'insp_sfg20_type_version.insp_sfg20_type_version_id',
                '=',
                'insp_sfg20_inspection_version.insp_sfg20_type_version_id'
            )
            ->join(
                'insp_sfg20_type',
                'insp_sfg20_type_version.insp_sfg20_type_id',
                '=',
                'insp_sfg20_type.insp_sfg20_type_id'
            )
            ->join(
                'inspection_status',
                'inspection_status.inspection_status_id',
                '=',
                'insp_sfg20_scheduled_inspection.inspection_status_id'
            )
        ->where('insp_sfg20_inspection.site_id', $siteId);
        if ($buildingId) {
            $query->where('insp_sfg20_inspection.building_id', $buildingId);
        } else {
            $query->whereNull('insp_sfg20_inspection.building_id');
        }

        return $query->get();
    }

    private function getInspectionExemptionArr($siteId, $buildingId = false)
    {
        $query = ComplianceExemption::where('site_id', $siteId);
        if ($buildingId) {
            $query->where('building_id', $buildingId);
        } else {
            $query->whereNull('building_id');
        }
        $complianceExemptions = $query->get();
        $inspectionExemptionArr = [];
        if ($complianceExemptions != null) {
            foreach ($complianceExemptions as $complianceExemption) {
                array_push($inspectionExemptionArr, $complianceExemption->inspection_type_id);
            }
        }

        return $inspectionExemptionArr;
    }

    public function generateInspection(&$row, $inspectionTypes, $siteId, $buildingId = false)
    {
        $inspectionExemptionArr = $this->getInspectionExemptionArr($siteId, $buildingId);

        $inspections = $this->getInspections($siteId, $buildingId);
        if ($inspections->count() > 0) {
            foreach ($inspectionTypes as $inspectionType) {
                $exempt = false;

                if (in_array($inspectionType->insp_sfg20_type_id, $inspectionExemptionArr)) {
                    array_push($row, [
                        'value' => '',
                        'color' => self::RAG_COLOR_GREY,
                        'cssClass' => self::RAG_COLOR_GREY_CSS]);
                    array_push($row, [
                        'value' => '',
                        'color' => self::RAG_COLOR_GREY,
                        'cssClass' => self::RAG_COLOR_GREY_CSS
                    ]);

                    $exempt = true;
                }

                if ($exempt == false) {
                    $inspection = $inspections
                    ->whereIn('inspection_status_type_id', [
                        InspSFG20InspectionStatusType::OPEN, InspSFG20InspectionStatusType::ISSUED
                    ])
                    ->where('insp_sfg20_type_version_id', $inspectionType->insp_sfg20_type_version_id)
                    ->sortBy('inspection_due_date')->first();

                    //last inspection date.
                    $lastInspection = $inspections
                    ->whereIn(
                        'inspection_status_type_id',
                        [InspSFG20InspectionStatusType::CLOSED, InspSFG20InspectionStatusType::COMPLETE]
                    )
                    ->where('insp_sfg20_type_version_id', $inspectionType->insp_sfg20_type_version_id)
                    ->sortBy('completed_date')->last();
                    if ($lastInspection) {
                        $completedDate = null;
                        if ($lastInspection->completed_date) {
                            $completedDate = Common::dateFieldDisplayFormat($lastInspection->completed_date);
                        }

                        $row[] = [
                            'value' => $completedDate . ' - ' . $lastInspection->insp_sfg20_scheduled_inspection_code
                        ];
                    } else {
                        array_push($row, ['value' => '']);
                    }
                    //next inspection date
                    if ($inspection && $inspection->inspection_due_date) {
                        $nextInspection =
                            $this->setColorForDate(
                                $inspection->inspection_due_date,
                                null,
                                $inspection->insp_sfg20_scheduled_inspection_code
                            );
                            array_push($row, array_merge($nextInspection, ['inspectionId' => $inspection->getKey()]));
                    } else {
                         array_push($row, ['value' => '']);
                    }
                }
            }
        } else {
            foreach ($inspectionTypes as $inspectionType) {
                if (in_array($inspectionType->insp_sfg20_type_id, $inspectionExemptionArr)) {
                    array_push($row, [
                        'value' => '',
                        'color' => self::RAG_COLOR_GREY,
                        'cssClass' => self::RAG_COLOR_GREY_CSS]);
                    array_push($row, [
                        'value' => '',
                        'color' => self::RAG_COLOR_GREY,
                        'cssClass' => self::RAG_COLOR_GREY_CSS
                    ]);

                    $exempt = true;
                } else {
                    array_push($row, ['value' => '']);
                    array_push($row, ['value' => '']);
                }
            }
        }

        Debug::log($row, '$row');
    }

    private function setColorForDate($date, $time = null, $nextInspectionCode = false)
    {
        $date = Common::dateFieldDisplayFormat($date);
        if ($time) {
            $dateObj = Common::stringToDateTime(['date' => $date, 'time' => '00:00:00']);
        } else {
            $dateObj = Common::stringToDateTime(['date' => $date]);
        }

        $currentDateObj = new \DateTime(date('Y-m-d 00:00:00'));
        if ($dateObj < $currentDateObj) {
            $color = self::RAG_COLOR_RED;
            $cssClass = self::RAG_COLOR_RED_CSS;
        } elseif (
            $dateObj >= $currentDateObj &&
            $dateObj <= $currentDateObj->add(new \DateInterval('P57D'))
        ) {
            $color = self::RAG_COLOR_AMBER;
            $cssClass = self::RAG_COLOR_AMBER_CSS;
        } else {
            $color = self::RAG_COLOR_GREEN;
            $cssClass = self::RAG_COLOR_GREEN_CSS;
        }


        return [
            'value' => $nextInspectionCode ? $date . ' - ' . $nextInspectionCode : $date,
            'color' => $color,
            'cssClass' => $cssClass
        ];
    }

    public function getInspectionTypes()
    {
        $query = InspSFG20Type::userSiteGroup()
            ->leftJoin(
                'insp_sfg20_type_version',
                'insp_sfg20_type_version.insp_sfg20_type_id',
                '=',
                'insp_sfg20_type.insp_sfg20_type_id'
            )
            ->whereIn(
                'insp_sfg20_type.insp_sfg20_type_code',
                ['09.03B', '09.15A', '08.03', '09.13A', '09.18B', '09.12B', '09.12E', '34.01A', '01.12', '34.01D',
                     '34.02', '34.07', '16.10B', '07.01C', '07.02', '07.05A', '07.03', '07.06B', '09.02', '14.04A',
                     '14.01', '14.07A', '14.07B', '14.08', '01.11', '30.26', '28.11A', '28.18', '30.25', '13.01',
                     '06.43', '06.17', '30.06A', '01.01', '15.02', '03.01', '26.01', '35.19A', '34.03', '10.01A',
                     '19.01A', '05.13', '18.03', '16.09A', '04.02', '30.23', '30.23A', '20.04B', '20.03E', '12.01A',
                     '06.01', '25.01', '15.01A', '37.03', '07.10', '11.01A', '21.01B'
                ]
            )

            ->select([
                \DB::raw("case 
                        when insp_sfg20_type.insp_sfg20_type_code = '09.03B'
                            Then CONCAT_WS(' - ','Fire Alarm System',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '09.15A'
                            Then CONCAT_WS(' - ','Fire Extinguishers',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '08.03'
                            Then CONCAT_WS(' - ','Fire Doors',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '09.13A'
                            Then CONCAT_WS(
                                ' - ','Fire Alarm System - Daily and Weekly',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '09.18B'
                            Then CONCAT_WS(
                                ' - ','Fire Risk Assessment (Annual Review)',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '09.12B'
                            Then CONCAT_WS(' - ','Sprinklers',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '09.12E'
                            Then CONCAT_WS(' - ','Sprinklers',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '34.01A'
                            Then CONCAT_WS(' - ','Annual Gas Servicing',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '01.12'
                            Then CONCAT_WS(
                                ' - ','F-Gas???',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '34.01D'
                            Then CONCAT_WS(
                                ' - ','Gas Catering Servicing (6 monthly)',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '34.02'
                            Then CONCAT_WS(' - ','Gas Shut Off Valve',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '34.07'
                            Then CONCAT_WS(' - ','LPG System',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '16.10B'
                            Then CONCAT_WS(
                                ' - ','Air to Water Heat Pump',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '07.01C'
                            Then CONCAT_WS(
                                ' - ','Water Risk Assessment & Annual L8',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '07.02'
                            Then CONCAT_WS(
                                ' - ','Monthly Water Checks',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '07.05A'
                            Then CONCAT_WS(' - ','TMV Servicing',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '07.03'
                            Then CONCAT_WS(' - ','Weekly Flushing',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '07.06B'
                            Then CONCAT_WS(' - ','Shower Descale',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '09.02'
                            Then CONCAT_WS(
                                ' - ','Emergency Lighting Maintained Fittings',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '14.04A'
                            Then CONCAT_WS(' - ','Electrical Installation Check',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '14.01'
                            Then CONCAT_WS(' - ','Fixed Wire Test 5 Yearly',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '14.07A'
                            Then CONCAT_WS(' - ','RCD and RCBO Test',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '14.07B'
                            Then CONCAT_WS(' - ','RCD and RCBO Test',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '14.08'
                            Then CONCAT_WS(
                                ' - ','Fixed Appliance Test (Annual)',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '01.11'
                            Then CONCAT_WS(
                                ' - ','Fridge Freezers Blast Chillers',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '30.26'
                            Then CONCAT_WS(' - ','Inst electric water heaters',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '28.11A'
                            Then CONCAT_WS(
                                ' - ','Access Control',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '28.18'
                            Then CONCAT_WS(' - ','Stage Lighting',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '30.25'
                            Then CONCAT_WS(' - ','Calorifiers LTHW',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '13.01'
                            Then CONCAT_WS(' - ','Fan Heaters LTHW',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)

                        when insp_sfg20_type.insp_sfg20_type_code = '06.43'
                            Then CONCAT_WS(' - ','Fat Fryer Electric',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '06.17'
                            Then CONCAT_WS(
                                ' - ','Kettle (Pressurised) Electric',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '30.06A'
                            Then CONCAT_WS(' - ','Pressurisation Unit',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '01.01'
                            Then CONCAT_WS(
                                ' - ','Split Systems/Heat Pumps (Air Con)',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '15.02'
                            Then CONCAT_WS(' - ','Roller Shutters',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '03.01'
                            Then CONCAT_WS(' - ','Asbestos Re-inspection Survey',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '26.01'
                            Then CONCAT_WS(' - ','Lightning Protection',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '35.19A'
                            Then CONCAT_WS(' - ','RPZ Valve',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '34.03'
                            Then CONCAT_WS(' - ','Flue - Annual',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '10.01A'
                            Then CONCAT_WS(' - ','Expansion Vessels',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
            
                        when insp_sfg20_type.insp_sfg20_type_code = '19.01A'
                            Then CONCAT_WS(
                                ' - ','Kitchen Extraction',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '05.13'
                            Then CONCAT_WS(' - ','Gym Equipment - All',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '18.03'
                            Then CONCAT_WS(' - ','Intruder Alarm',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '16.09A'
                            Then CONCAT_WS(' - ','Chimney',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '04.02'
                            Then CONCAT_WS(' - ','Auto Doors',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '30.23'
                            Then CONCAT_WS(' - ','Changing Table Service',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '30.23A'
                            Then CONCAT_WS(' - ','Platform Hoist',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '20.04B'
                            Then CONCAT_WS(' - ','Platform Lift',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '20.03E'
                            Then CONCAT_WS(' - ','Hydraulic Lift',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '12.01A'
                            Then CONCAT_WS(' - ','Mansafe',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)

                        when insp_sfg20_type.insp_sfg20_type_code = '06.01'
                            Then CONCAT_WS(' - ','Cold Rooms',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '25.01'
                            Then CONCAT_WS(' - ','Workshop Equipment',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '15.01A'
                            Then CONCAT_WS(' - ','Automative Vehicle Access',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '37.03'
                            Then CONCAT_WS(' - ','Pool Pumps',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '07.10'
                            Then CONCAT_WS(' - ','Pool Water Sampling',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '11.01A'
                            Then CONCAT_WS(' - ','Fans',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)
                        when insp_sfg20_type.insp_sfg20_type_code = '21.01B'
                            Then CONCAT_WS(' - ','Sewage/drainage pumps',insp_sfg20_type.insp_sfg20_type_code,
                            insp_sfg20_type_version.version_code)


                        else insp_sfg20_type_code END AS 'inspection_type_header'"),


                \DB::raw("case
                        when insp_sfg20_type.insp_sfg20_type_code = '09.03B' Then 1
                        when insp_sfg20_type.insp_sfg20_type_code = '09.15A' Then 2
                        when insp_sfg20_type.insp_sfg20_type_code = '08.03' Then 3
                        when insp_sfg20_type.insp_sfg20_type_code = '09.13A' Then 4
                        when insp_sfg20_type.insp_sfg20_type_code = '09.18B' Then 5
                        when insp_sfg20_type.insp_sfg20_type_code = '09.12B' Then 6
                        when insp_sfg20_type.insp_sfg20_type_code = '09.12E' Then 7
                        when insp_sfg20_type.insp_sfg20_type_code = '34.01A' Then 8
                        when insp_sfg20_type.insp_sfg20_type_code = '01.12' Then 9
                        when insp_sfg20_type.insp_sfg20_type_code = '34.01D' Then 10
                        when insp_sfg20_type.insp_sfg20_type_code = '34.02' Then 11
                        when insp_sfg20_type.insp_sfg20_type_code = '34.07' Then 12
                        when insp_sfg20_type.insp_sfg20_type_code = '16.10B' Then 13
                        when insp_sfg20_type.insp_sfg20_type_code = '07.01C' Then 14
                        when insp_sfg20_type.insp_sfg20_type_code = '07.02' Then 15
                        when insp_sfg20_type.insp_sfg20_type_code = '07.05A' Then 16
                        when insp_sfg20_type.insp_sfg20_type_code = '07.03' Then 17
                        when insp_sfg20_type.insp_sfg20_type_code = '07.06B' Then 18
                        when insp_sfg20_type.insp_sfg20_type_code = '09.02' Then 19
                        when insp_sfg20_type.insp_sfg20_type_code = '14.04A' Then 20
                        
                        when insp_sfg20_type.insp_sfg20_type_code = '14.01' Then 21
                        when insp_sfg20_type.insp_sfg20_type_code = '14.07A' Then 22
                        when insp_sfg20_type.insp_sfg20_type_code = '14.07B' Then 23
                        when insp_sfg20_type.insp_sfg20_type_code = '14.08' Then 24
                        when insp_sfg20_type.insp_sfg20_type_code = '01.11' Then 25
                        when insp_sfg20_type.insp_sfg20_type_code = '30.26' Then 26
                        when insp_sfg20_type.insp_sfg20_type_code = '28.11A' Then 27
                        when insp_sfg20_type.insp_sfg20_type_code = '28.18' Then 28
                        when insp_sfg20_type.insp_sfg20_type_code = '30.25' Then 29
                        when insp_sfg20_type.insp_sfg20_type_code = '13.01' Then 30
                        when insp_sfg20_type.insp_sfg20_type_code = '06.43' Then 31
                        when insp_sfg20_type.insp_sfg20_type_code = '06.17' Then 32
                        when insp_sfg20_type.insp_sfg20_type_code = '30.06A' Then 33
                        when insp_sfg20_type.insp_sfg20_type_code = '01.01' Then 34
                        when insp_sfg20_type.insp_sfg20_type_code = '15.02' Then 35
                        when insp_sfg20_type.insp_sfg20_type_code = '03.01' Then 36
                        when insp_sfg20_type.insp_sfg20_type_code = '26.01' Then 37
                        when insp_sfg20_type.insp_sfg20_type_code = '35.19A' Then 38
                        when insp_sfg20_type.insp_sfg20_type_code = '34.03' Then 39
                        when insp_sfg20_type.insp_sfg20_type_code = '10.01A' Then 40
                        
                        when insp_sfg20_type.insp_sfg20_type_code = '19.01A' Then 41
                        when insp_sfg20_type.insp_sfg20_type_code = '05.13' Then 42
                        when insp_sfg20_type.insp_sfg20_type_code = '18.03' Then 43
                        when insp_sfg20_type.insp_sfg20_type_code = '16.09A' Then 44
                        when insp_sfg20_type.insp_sfg20_type_code = '04.02' Then 45
                        when insp_sfg20_type.insp_sfg20_type_code = '30.23' Then 46
                        when insp_sfg20_type.insp_sfg20_type_code = '30.23A' Then 47
                        when insp_sfg20_type.insp_sfg20_type_code = '20.04B' Then 48
                        when insp_sfg20_type.insp_sfg20_type_code = '20.03E' Then 49
                        when insp_sfg20_type.insp_sfg20_type_code = '12.01A' Then 50
                        when insp_sfg20_type.insp_sfg20_type_code = '06.01' Then 51
                        when insp_sfg20_type.insp_sfg20_type_code = '25.01' Then 52
                        when insp_sfg20_type.insp_sfg20_type_code = '15.01A' Then 53
                        when insp_sfg20_type.insp_sfg20_type_code = '37.03' Then 54
                        when insp_sfg20_type.insp_sfg20_type_code = '07.10' Then 55
                        when insp_sfg20_type.insp_sfg20_type_code = '11.01A' Then 56
                        when insp_sfg20_type.insp_sfg20_type_code = '21.01B' Then 57

                        else '' END AS 'output_order'"),

                'insp_sfg20_type.insp_sfg20_type_id',
                'insp_sfg20_type_version.insp_sfg20_type_version_id',
            ]);

            $query->orderByRaw('CONVERT(`output_order`, int) ASC');

            return $query->get();
    }

    private function generateFilterData($filterData, &$whereCodes, &$whereTexts, &$orCodes)
    {
        if ($siteId = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $siteId, "Site Code"]);
        }
    }

    private function getSitesQuery()
    {
        $siteQuery = Site::userSiteGroup()
            ->select([
                'site.site_id',
                'site.site_code',
                'site.site_desc',
                'site.asbestos_survey_required as site_asbestos_survey_required',
                'site.include_in_compliance_report',
                'site.active',

                \DB::raw("case 
                        when site.site_code = '2152' Then 1
                        when site.site_code = '3301' THEN 2
                        when site.site_code = '3022' THEN 3
                        when site.site_code = '2191' THEN 4
                        when site.site_code = '1005' THEN 5
                        when site.site_code = '3019' THEN 6
                        when site.site_code = '2039' THEN 7
                        when site.site_code = '2156' THEN 8
                        when site.site_code = '2091' THEN 9
                        when site.site_code = '2023' THEN 10
                        when site.site_code = '3316' THEN 11
                        when site.site_code = '2060' THEN 12
                        when site.site_code = '2179' THEN 13
                        when site.site_code = '1002' THEN 14
                        when site.site_code = '2162' THEN 15
                        when site.site_code = '2092' THEN 16
                        when site.site_code = '7002' THEN 17
                        when site.site_code = '3319' THEN 18
                        when site.site_code = '3343' THEN 19
                        when site.site_code = '2142' THEN 20
                        when site.site_code = '4006' THEN 21
                        when site.site_code = '7053' THEN 22
                        when site.site_code = '7054' THEN 23
                        when site.site_code = '2155' THEN 24
                        when site.site_code = '2054' THEN 25
                        when site.site_code = '2192' THEN 26
                        when site.site_code = '2070' THEN 27
                        when site.site_code = '2206' THEN 28
                        when site.site_code = '2079' THEN 29
                        when site.site_code = '3318' THEN 30
                        when site.site_code = '2150' THEN 31
                        when site.site_code = '2189' THEN 32
                        when site.site_code = '7055' THEN 33
                        when site.site_code = '2014' THEN 34
                        when site.site_code = 'L/03284' THEN 35
                        when site.site_code = 'L/03263' THEN 36
                        when site.site_code = '7000' THEN 37
                        when site.site_code = '2154' THEN 38
                        when site.site_code = '2133' THEN 39
                        when site.site_code = '2195' THEN 40
                        when site.site_code = '3339' THEN 41
                        when site.site_code = '2203' THEN 42
                        when site.site_code = '2208' THEN 43
                        when site.site_code = '2186' THEN 44
                        when site.site_code = '3341' THEN 45
                        when site.site_code = '3010' THEN 46
                        when site.site_code = '2007' THEN 47
                        when site.site_code = '3305' THEN 48
                        when site.site_code = '3338' THEN 49
                        when site.site_code = '3018' THEN 50
                        when site.site_code = '2076' THEN 51
                        when site.site_code = '3015' THEN 52
                        when site.site_code = '1007' THEN 53
                        when site.site_code = '2159' THEN 54
                        when site.site_code = '1101' THEN 55
                        when site.site_code = '2100' THEN 56
                        when site.site_code = '2173' THEN 57
                        when site.site_code = '2193' THEN 58
                        when site.site_code = '2084' THEN 59
                        when site.site_code = '2180' THEN 60

                        else '' END AS 'output_order'")
            ]);

        if (\Auth::user()->site_access == User::SITE_ACCESS_MODE_SELECTED) {
            $siteQuery->join('user_access', 'user_access.site_id', '=', 'site.site_id')
                ->where('user_access.user_id', \Auth::user()->getKey());
        }

        $siteQuery->whereIn('site_code', [
            '2152','3301', '3022', '2191', '1005', '3019', '2039', '2156','2091', '2023',
            '3316', '2060', '2179', '1002', '2162','2092', '7002', '3319', '3343', '2142',
            '4006', '7053', '7054', '2155', '2054', '2192', '2070', '2206', '2079','3318',
            '2150', '2189', '7055', '2014', 'L/03284', 'L/03263', '7000', '2154', '2133','2195',
            '3339', '2203', '2208', '2186', '3341', '3010', '2007','3305', '3338','3018',
            '2076', '3015', '1007', '2159', '1101', '2100', '2173', '2193', '2084', '2180'
            ]);

        $siteQuery->with([
            'asbSurveys' => function ($query) {
                $this->buildAsbSurveyQuery($query);
            },
        ]);

        $siteQuery->orderByRaw('CONVERT(`output_order`, int) ASC');

        return $siteQuery;
    }
}
