<?php

namespace Tfcloud\Services\Report\PhpExcel;

use PhpOffice\PhpSpreadsheet\IOFactory;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\ReportConstant;
use Tfcloud\Models\Education\SuitcfArea;
use Tfcloud\Models\Education\SuitcfFactors;
use Tfcloud\Models\Education\SuitCFSurvey;
use Tfcloud\Models\Education\SuitcfSurveyArea;
use Tfcloud\Models\SuffType;
use Tfcloud\Services\Education\SuitCFSurveyService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Lib\Traits\EmailTrait;

class SCF01SuitCoreFactsSurveyService extends PhpExcelReportBaseService
{
    use EmailTrait;

    protected $suitCFSurveyService;

    public function __construct(
        PermissionService $permissionService,
        SuitCFSurveyService $suitCFSurveyService
    ) {
        parent::__construct($permissionService);
        $this->suitCFSurveyService = $suitCFSurveyService;
    }

    public function generateReport($repExcelFile, $inputs, $options = [], &$filterText = '')
    {
        $isPrintReport = array_get($options, 'print-report', false);
        if ($isPrintReport && !$repExcelFile) {
            $suitcfSurveyCode = array_get($options, 'suitcf_survey_code', 'no_code_found');
            $reportFileName = ReportConstant::SYSTEM_REPORT_SCF01 . '_' . $suitcfSurveyCode;
            $repExcelFile = $this->generateReportOutPutPath($reportFileName);
        }

        $suitcfId = array_get($inputs, 'suitcf');
        $suitcf = $this->suitCFSurveyService->get($suitcfId, false);
        if (!$suitcf) {
            return false;
        }

        $reportData = $this->getReportData($suitcf);
        $templateFile = $this->getTemplateFile($suitcf->suff_type_id);

        if (is_readable($templateFile)) {
            copy($templateFile, $repExcelFile);
        }

        //$phpExcelObj = \PhpOffice\PhpSpreadsheet\IOFactory::load($templateFile);
        $fileType = IOFactory::identify($repExcelFile);
        $objReader = IOFactory::createReader($fileType);
        $phpExcelObj = $objReader->load($repExcelFile);

        // Set cells's value
        $errorMessages = $this->setWorkSheetCellsValue($phpExcelObj, $reportData);
        if (is_null($errorMessages)) {
            $errorMessages = $this->writeExcelFile($phpExcelObj, $repExcelFile, '', true);
        }

        if ($errorMessages) {
            return false;
        }

        $whereCodes = [];               // The codes which have been used in the filter
        $whereTexts = [];               // Plain text about filtering applied
        $orCodes = [];                  // csv string of OR codes
        $bFilterDetail = false;

        $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
        $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $filterText);

        if (array_get($inputs, 'send_email', false)) {
            $user = \Auth::user();
            $fileName = ReportConstant::SYSTEM_REPORT_SCF01;
            $logItem = SuitCFSurvey::find($suitcfId);
            $emailData = array(
                'mailTo'        => $user->email_reply_to,
                'displayName'   => $user->siteGroup->email_return_address,
                'repTitle'      => $fileName,
                'subject'       => $fileName . ' Report',
            );
            return $this->sendEmail($repExcelFile, $emailData, $logItem);
        }

        if ($isPrintReport) {
            return $repExcelFile;
        }

        return true;
    }

    protected function validateFilter($inputs)
    {
        $rules = [
            'suitcf' => ['required'],
        ];

        $messages = [];

        $validator = \Validator::make($inputs, $rules, $messages);

        return $validator;
    }

    private function getReportData(SuitCFSurvey $suitcf)
    {
        $data = [];
        $cellMapping = [
            'site_name' => 'B7',
            'area_comment' => 'B42',
            1 => 'G27',
            2 => 'G29',
            3 => 'G31',
            4 => 'G33',
            5 => 'G35',
        ];
        if ($suitcf->suff_type_id == SuffType::PRIMARY) {
            $data['Learning _ Teaching General'] = $this->getSheetData(
                $suitcf->suitcf_survey_id,
                SuitcfArea::PRIMARY_LEARN
            );
            $data['Internal Social'] = $this->getSheetData(
                $suitcf->suitcf_survey_id,
                SuitcfArea::PRIMARY_INT_SOC
            );
            $data['Internal Facilities'] = $this->getSheetData(
                $suitcf->suitcf_survey_id,
                SuitcfArea::PRIMARY_INT_FAC
            );
            $data['External Social'] = $this->getSheetData(
                $suitcf->suitcf_survey_id,
                SuitcfArea::PRIMARY_EXT_SOC,
                $cellMapping
            );
            $data['External Facilities'] = $this->getSheetData(
                $suitcf->suitcf_survey_id,
                SuitcfArea::PRIMARY_EXT_FAC,
                $cellMapping
            );
        } else {
            $data['Learning _ Teaching General'] = $this->getSheetData(
                $suitcf->suitcf_survey_id,
                SuitcfArea::SECONDARY_LEARN_GEN
            );
            $data['Learning _ Teaching Practical'] = $this->getSheetData(
                $suitcf->suitcf_survey_id,
                SuitcfArea::SECONDARY_LEARN_PRAC
            );
            $data['Internal Social'] = $this->getSheetData(
                $suitcf->suitcf_survey_id,
                SuitcfArea::SECONDARY_INT_SOC
            );
            $data['Internal Facilities'] = $this->getSheetData(
                $suitcf->suitcf_survey_id,
                SuitcfArea::SECONDARY_INT_FAC
            );
            $data['External Social'] = $this->getSheetData(
                $suitcf->suitcf_survey_id,
                SuitcfArea::SECONDARY_EXT_SOC,
                $cellMapping
            );
            $data['External Facilities'] = $this->getSheetData(
                $suitcf->suitcf_survey_id,
                SuitcfArea::SECONDARY_EXT_FAC,
                $cellMapping
            );
        }
        return $data;
    }

    private function getSheetData(
        $suitcfSurveyId,
        $suitcfAareaId,
        $cellMapping = [
            'site_name' => 'B7',
            'area_comment' => 'B43',
            1 => 'G28',
            2 => 'G30',
            3 => 'G32',
            4 => 'G34',
            5 => 'G36',
        ]
    ) {
        $query = SuitcfSurveyArea::join(
            'suitcf_survey',
            'suitcf_survey.suitcf_survey_id',
            '=',
            'suitcf_survey_area.suitcf_survey_id'
        )->join(
            'site',
            'site.site_id',
            '=',
            'suitcf_survey.site_id'
        )->where(
            'suitcf_survey.site_group_id',
            \Auth::user()->site_group_id
        )->where(
            'suitcf_survey_area.suitcf_survey_id',
            $suitcfSurveyId
        )->where(
            'suitcf_survey_area.suitcf_area_id',
            $suitcfAareaId
        )->select([
            "site.site_desc AS " . $cellMapping['site_name'],
            $this->getFactorRating(
                $suitcfSurveyId,
                $suitcfAareaId,
                SuitcfFactors::FUNC,
                $cellMapping[SuitcfFactors::FUNC]
            ),
            $this->getFactorRating(
                $suitcfSurveyId,
                $suitcfAareaId,
                SuitcfFactors::ACCESS,
                $cellMapping[SuitcfFactors::ACCESS]
            ),
            $this->getFactorRating(
                $suitcfSurveyId,
                $suitcfAareaId,
                SuitcfFactors::ENV,
                $cellMapping[SuitcfFactors::ENV]
            ),
            $this->getFactorRating(
                $suitcfSurveyId,
                $suitcfAareaId,
                SuitcfFactors::SAFETY,
                $cellMapping[SuitcfFactors::SAFETY]
            ),
            $this->getFactorRating(
                $suitcfSurveyId,
                $suitcfAareaId,
                SuitcfFactors::FIX_FIT,
                $cellMapping[SuitcfFactors::FIX_FIT]
            ),
            "suitcf_survey_area.area_comment AS " . $cellMapping['area_comment']
        ]);

        return $query->first()->toArray();
    }

    private function getFactorRating($suitcfSurveyId, $suitcfAareaId, $suitcfFactorId, $cellIndex)
    {
        $query = "(
            SELECT suitcf_survey_area_factor.factor_rating
                FROM suitcf_survey_area_factor WHERE
                    suitcf_survey_area_factor.suitcf_survey_id = $suitcfSurveyId
                    AND suitcf_survey_area_factor.suitcf_area_id = $suitcfAareaId
                    AND suitcf_survey_area_factor.suitcf_factor_id = $suitcfFactorId
        ) AS '$cellIndex'";
        return \DB::raw($query);
    }

    private function getTemplateFile($suffTypeId)
    {
        if ($suffTypeId == SuffType::PRIMARY) {
            $templateFileName =  "SuitabilityCoreFact_Primary.xlsx";
        } else {
            $templateFileName = "SuitabilityCoreFact_Secondary.xlsx";
        }

        $templateFile = app_path()
            . DIRECTORY_SEPARATOR . "templates"
            . DIRECTORY_SEPARATOR . "phpExcel"
            . DIRECTORY_SEPARATOR . $templateFileName;

        return $templateFile;
    }

    private function generateFilterData($filterData, &$whereCodes, &$whereTexts, &$orCodes)
    {
        if ($val = Common::iset($filterData['suitcf'])) {
            array_push(
                $whereCodes,
                ['suitcf_survey', 'suitcf_survey_id', 'suitcf_survey_code', $val, "Suitability Survey"]
            );
        }
    }
}
