<?php

namespace Tfcloud\Services\Report\PhpExcel;

use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Traits\EmailTrait;
use Tfcloud\Models\ProjectRiskItem;
use Tfcloud\Models\SiteGroup;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Project\ProjectRiskService;
use Tfcloud\Services\Project\ProjectService;

class ProjectRiskRegisterReportService extends PhpExcelReportBaseService
{
    use EmailTrait;

    protected $permissionService;
    protected $projectRiskService;
    protected $projectService;

    public function __construct(
        PermissionService $permissionService
    ) {
        $this->permissionService = $permissionService;
        $this->projectRiskService = new ProjectRiskService($permissionService);
        $this->projectService = new ProjectService($permissionService);
    }

    protected function validateFilter($inputs)
    {
        $rules = [
            'project' => ['required'],
        ];

        $messages = [];

        $validator = \Validator::make($inputs, $rules, $messages);

        return $validator;
    }

    public function generateReport($repExcelFile, $inputs, $options = [], &$filterText = '')
    {
        $projectId = array_get($inputs, 'project');
        $project = $this->projectService->get($projectId, false);
        $reportFileName = $project->project_code;
        if (array_get($options, 'print-report')) {
            $repExcelFile = $this->generateReportOutPutPath($reportFileName, 'xlsx');
        }

        $templateFile = app_path()
            . DIRECTORY_SEPARATOR . "templates"
            . DIRECTORY_SEPARATOR . "phpExcel"
            . DIRECTORY_SEPARATOR . "RiskRegisterTemplate.xlsx";
        $phpExcelObj = $this->readFile($templateFile);
        if (is_readable($templateFile)) {
            copy($templateFile, $repExcelFile);
        }
        $projectData = $this->getProjectData($project);
        $this->fillDataToExcel($phpExcelObj, $projectData);
        $thresholdData = $this->getRiskThresholdData();
        $this->fillDataToExcel($phpExcelObj, $thresholdData);
        $this->fillRiskCount($phpExcelObj, $project->project_id);
        $projectRisks = $this->getProjectRiskData($project->project_id);
        $this->fillProjectRiskData($phpExcelObj, $projectRisks);
        $this->calculateCellHeight($phpExcelObj);
        $errorMessages = null;
        if (is_null($errorMessages)) {
            $errorMessages = $this->writeExcelFile($phpExcelObj, $repExcelFile, 'Xlsx', true);
        }
        if ($errorMessages) {
            return false;
        }

        if (array_get($inputs, 'send_email', false)) {
            $user = \Auth::user();
            $fileName = $project->project_code;
            $emailData = array(
                'mailTo'        => $user->email_reply_to,
                'displayName'   => $user->siteGroup->email_return_address,
                'repTitle'      => $fileName,
                'subject'       => $fileName . ' Report',
            );
            return $this->sendEmail($repExcelFile, $emailData, $project);
        }

        if (array_get($options, 'print-report')) {
            return $repExcelFile;
        }
        return true;
    }

    private function getProjectData($project)
    {
        $site = $project->site;
        $mappedAddressData = $this->fillAddress($site);
        $data = [
            'B2' => Carbon::now()->format('d/m/Y'),
            'E2' => $project->project_title,
            'J2' => $project->client_ref,
            'B4' => $site->code_description,
            'E4' => $project->project_code,
            'G4' => $project->owner ? $project->owner->display_name : '',
            'J4' => $project->client ? $project->client->contact_name : '',
            'E5' => $site->contact ? $site->contact->contact_name : '',
            'E7' => $site->address ? $site->address->phone_main : '',
            'E10' => $site->site_email_addr,
            'G5' => Common::dateFieldDisplayFormat($this->getStartDate($project)),
            'G7' => Common::dateFieldDisplayFormat($this->getCompletedDate($project))
        ];

        return array_merge($data, $mappedAddressData);
    }

    private function fillAddress($site)
    {
        $data = [];
        $siteAddress = $site->address->getAddressAsArray();
        $addressArray = array_filter([
            $siteAddress['second_addr_obj'],
            $siteAddress['first_addr_obj'],
            $siteAddress['street'],
            $siteAddress['locality'],
            $siteAddress['town'],
        ]);
        $row = 5;
        if (count($addressArray)) {
            foreach ($addressArray as $item) {
                $data["B{$row}"] = $item;
                $row++;
            }
            $data['B10'] = array_get($siteAddress, 'postcode');
            $data['B11'] = array_get($siteAddress, 'region');
        }
        return $data;
    }

    private function getStartDate($project)
    {
        if (!empty($project->actual_plan_commit_to_prepare_proposals)) {
            $startDate = $project->actual_plan_commit_to_prepare_proposals;
        } elseif (!empty($project->current_plan_commit_to_prepare_proposals)) {
            $startDate = $project->current_plan_commit_to_prepare_proposals;
        } else {
            $startDate = $project->planned_commit_to_prepare_proposals;
        }
        return $startDate;
    }

    private function getCompletedDate($project)
    {
        if (!empty($project->actual_final_completion)) {
            $completedDate = $project->actual_final_completion;
        } elseif (!empty($project->current_final_completion)) {
            $completedDate = $project->current_final_completion;
        } else {
            $completedDate = $project->planned_final_completion;
        }
        return $completedDate;
    }

    private function getRiskThresholdData()
    {
        $siteGroup = SiteGroup::get();
        $residualRiskText = "Residual Risks <= {$siteGroup->proj_residual_risk_threshold_max}";
        if ($siteGroup->proj_low_risk_threshold_min == $siteGroup->proj_low_risk_threshold_max) {
            $lowRiskText = "Low Risks {$siteGroup->proj_low_risk_threshold_min}";
        } else {
            $lowRiskText =
                "Low Risks {$siteGroup->proj_low_risk_threshold_min} - {$siteGroup->proj_low_risk_threshold_max}";
        }
        if ($siteGroup->proj_medium_risk_threshold_min == $siteGroup->proj_medium_risk_threshold_max) {
            $mediumRiskText = "Low Risks {$siteGroup->proj_medium_risk_threshold_min}";
        } else {
            $mediumRiskText =
                "Low Risks {$siteGroup->proj_medium_risk_threshold_min} - {$siteGroup->proj_medium_risk_threshold_max}";
        }
        $highRiskText = "High Risks > {$siteGroup->proj_medium_risk_threshold_max}";
        $data = [
            'B15' => $residualRiskText,
            'B16' => $lowRiskText,
            'B17' => $mediumRiskText,
            'B18' => $highRiskText
        ];
        return $data;
    }

    private function getProjectRiskData($projectId)
    {
        $data = collect();
        $query = ProjectRiskItem::userSiteGroup()
            ->leftJoin(
                \DB::raw('user raised_by_user'),
                'raised_by_user.id',
                '=',
                'project_risk_item.raised_by_user_id'
            )
            ->leftJoin(
                'project_risk_type',
                'project_risk_type.project_risk_type_id',
                '=',
                'project_risk_item.project_risk_type_id'
            )
            ->leftJoin(
                'project_risk_category',
                'project_risk_category.project_risk_category_id',
                '=',
                'project_risk_item.project_risk_category_id'
            )
            ->leftJoin(
                'project_risk_likelihood',
                'project_risk_likelihood.project_risk_likelihood_id',
                '=',
                'project_risk_item.project_risk_likelihood_id'
            )
            ->leftJoin(
                'project_risk_severity',
                'project_risk_severity.project_risk_severity_id',
                '=',
                'project_risk_item.project_risk_severity_id'
            )
            ->leftJoin(
                \DB::raw('user owner_user'),
                'owner_user.id',
                '=',
                'project_risk_item.owner_user_id'
            )
            ->select([
                'project_risk_item.*',
                'project_risk_category.project_risk_category_code',
                'project_risk_category.project_risk_category_desc',
                'project_risk_likelihood.project_risk_likelihood_value',
                'project_risk_severity.project_risk_severity_value',
                \DB::raw("IFNULL(project_risk_type.project_risk_type_code, 'Other') AS project_risk_type_code"),
                'project_risk_type.project_risk_type_desc',
                \DB::raw('raised_by_user.display_name AS raised_by_name'),
                \DB::raw('owner_user.display_name AS owner_name'),
            ])
            ->where('project_risk_item.project_id', $projectId)
            ->orderByRaw("(project_risk_type.project_risk_type_code != 'Other') desc")
            ->orderBy("project_risk_type.project_risk_type_code")
            ->orderBy("project_risk_item.project_risk_item_code");


        $projectRisks = $query->get()->groupBy('project_risk_type_code');

        foreach ($projectRisks as $type) {
            $typeCodeDesc = Common::concatFields(
                [$type[0]->project_risk_type_code, $type[0]->project_risk_type_desc]
            );
            $type->prepend($typeCodeDesc);
            $data->push($type);
        }
        return $data->flatten();
    }

    private function fillProjectRiskData(Spreadsheet &$phpExcelObj, $projectRisks)
    {
        $output = [];
        $row = 23;
        foreach ($projectRisks as $risk) {
            if (is_string($risk)) {
                $output["A{$row}"] = $risk;
                $phpExcelObj->getActiveSheet()->setCellValue("A{$row}", $risk);
                if ($row != 23) {
                    $cellToStyle = $this->getCellToStyle(23, $row);
                    $this->applyStyle($phpExcelObj, $cellToStyle);
                }
            } else {
                $output["A{$row}"] = $risk->project_risk_item_code;
                $output["B{$row}"] = Common::dateFieldDisplayFormat($risk->date_raised);
                $output["C{$row}"] = $risk->raised_by_name;
                $output["D{$row}"] = Common::concatFields(
                    [$risk->project_risk_category_code, $risk->project_risk_category_desc]
                );
                $output["E{$row}"] = $risk->activity;
                $output["F{$row}"] = $risk->significant_hazards;
                $output["G{$row}"] = $risk->project_risk_likelihood_value;
                $output["H{$row}"] = $risk->project_risk_severity_value;
                $output["I{$row}"] = $risk->degree_of_risk;
                $output["J{$row}"] = $risk->risk_control_measures;
                $output["K{$row}"] = $risk->owner_name;
                $output["L{$row}"] = Common::dateFieldDisplayFormat($risk->next_review_date);
                $output["M{$row}"] = $risk->resolved == CommonConstant::DATABASE_VALUE_NO ? 'Open' : 'Resolved';
                $cellToStyle = $this->getCellToStyle(24, $row);
                $this->applyStyle($phpExcelObj, $cellToStyle);
                $this->getDegreeOfRiskStyle($phpExcelObj, $row, $risk->degree_of_risk);
                if ($risk->resolved == CommonConstant::DATABASE_VALUE_YES) {
                    $phpExcelObj->getActiveSheet()->getStyle("A{$row}:M{$row}")->getFont()->setStrikethrough(true);
                } else {
                    $phpExcelObj->getActiveSheet()->getStyle("A{$row}:M{$row}")->getFont()->setStrikethrough(false);
                }
            }
            $row++;
        }
        $row -= 1;
        $phpExcelObj->getActiveSheet()->getStyle("A{$row}:M{$row}")
            ->getBorders()->getBottom()->setBorderStyle(Border::BORDER_THICK);
        $this->fillDataToExcel($phpExcelObj, $output);
    }

    private function countNumberOfRisks($projectId)
    {
        $siteGroup = SiteGroup::get();
        $residualMax = $siteGroup->proj_residual_risk_threshold_max;
        $lowRiskMin = $siteGroup->proj_low_risk_threshold_min;
        $lowRiskMax = $siteGroup->proj_low_risk_threshold_max;
        $medRiskMin = $siteGroup->proj_medium_risk_threshold_min;
        $medRiskMax = $siteGroup->proj_medium_risk_threshold_max;
        $riskCount = ProjectRiskItem::userSiteGroup()
            ->select([
                \DB::raw("COUNT(IF(degree_of_risk <= $residualMax,1,NULL)) 'residual'"),
                \DB::raw("COUNT(IF(degree_of_risk >= $lowRiskMin AND degree_of_risk <= $lowRiskMax,1,NULL)) 'low'"),
                \DB::raw("COUNT(IF(degree_of_risk >= $medRiskMin AND degree_of_risk <= $medRiskMax,1,NULL)) 'med'"),
                \DB::raw("COUNT(IF(degree_of_risk > $medRiskMax,1,NULL)) 'high'"),
                \DB::raw("COUNT(project_risk_item_id) 'total'")
            ])
            ->where('project_id', $projectId)
            ->first();
        return $riskCount;
    }

    private function fillRiskCount(Spreadsheet &$phpExcelObj, $projectId)
    {
        $riskCount = $this->countNumberOfRisks($projectId);
        $data = [
            'I15' => $riskCount->residual,
            'I16' => $riskCount->low,
            'I17' => $riskCount->med,
            'I18' => $riskCount->high,
            'I19' => $riskCount->total,
        ];
        $this->fillDataToExcel($phpExcelObj, $data);
    }

    private function applyStyle(Spreadsheet &$phpExcelObj, $cellToStyle)
    {
        foreach ($cellToStyle as $styleCell => $cell) {
            $phpExcelObj->getActiveSheet()->duplicateStyle(
                $phpExcelObj->getActiveSheet()->getStyle($styleCell),
                $cell
            );
        }
    }

    private function getCellToStyle($styleRow, $row)
    {
        $result = [
            "A{$styleRow}" => "A{$row}",
            "B{$styleRow}" => "B{$row}",
            "C{$styleRow}" => "C{$row}",
            "D{$styleRow}" => "D{$row}",
            "E{$styleRow}" => "E{$row}",
            "F{$styleRow}" => "F{$row}",
            "G{$styleRow}" => "G{$row}",
            "H{$styleRow}" => "H{$row}",
            "I{$styleRow}" => "I{$row}",
            "J{$styleRow}" => "J{$row}",
            "K{$styleRow}" => "K{$row}",
            "L{$styleRow}" => "L{$row}",
            "M{$styleRow}" => "M{$row}",
        ];

        return $result;
    }

    private function getDegreeOfRiskStyle(Spreadsheet &$phpExcelObj, $row, $value)
    {
        $siteGroup = SiteGroup::get();
        if ($value) {
            if (
                ($siteGroup->proj_low_risk_threshold_min <= $value)
                && ($value <= $siteGroup->proj_low_risk_threshold_max)
            ) {
                $phpExcelObj->getActiveSheet()->duplicateStyle(
                    $phpExcelObj->getActiveSheet()->getStyle("B16"),
                    "I{$row}"
                );
            } elseif (
                ($siteGroup->proj_medium_risk_threshold_min <= $value)
                && ($value <= $siteGroup->proj_medium_risk_threshold_max)
            ) {
                $phpExcelObj->getActiveSheet()->duplicateStyle(
                    $phpExcelObj->getActiveSheet()->getStyle("B17"),
                    "I{$row}"
                );
            } elseif ($siteGroup->proj_medium_risk_threshold_max < $value) {
                $phpExcelObj->getActiveSheet()->duplicateStyle(
                    $phpExcelObj->getActiveSheet()->getStyle("B18"),
                    "I{$row}"
                );
            }
        } else {
            $phpExcelObj->getActiveSheet()->duplicateStyle(
                $phpExcelObj->getActiveSheet()->getStyle("B15"),
                "I{$row}"
            );
        }
    }

    private function readFile($templateFile)
    {
        $reader = IOFactory::createReader("Xlsx");
        $phpExcelObj = $reader->load($templateFile);

        $this->setProperties($phpExcelObj);
        return $phpExcelObj;
    }

    private function fillDataToExcel(Spreadsheet &$phpExcelObj, $data)
    {
        foreach ($data as $cell => $value) {
            $phpExcelObj->getActiveSheet()->setCellValue($cell, $value);
        }
    }

    private function getRowCount($cellData, $width = 10)
    {
        $rowCount = 0;
        $dataLines = explode("\n", $cellData);
        foreach ($dataLines as $line) {
            $rowCount += intval((strlen($line) / $width) + 1);
        }
        return $rowCount;
    }

    private function calculateCellHeight(Spreadsheet &$phpExcelObj)
    {
        $result = [
            2 => ['E' => 88.28],
            4 => ['B' => 28.85, 'G' => 20.86, 'J' => 70.99],
            5 => ['B' => 28.85 , 'E' => 33.71],
            6 => ['B' => 28.85],
            7 => ['B' => 28.85],
            8 => ['B' => 28.85],
            9 => ['B' => 28.85],
            10 => ['B' => 28.85, 'E' => 33.71],
            11 => ['B' => 28.85]
        ];

        foreach ($result as $row => $colArr) {
            $rowCountArr = [];
            foreach ($colArr as $col => $width) {
                $cellData = $phpExcelObj->getActiveSheet()->getCell($col . $row)->getCalculatedValue();
                $rowCount = $this->getRowCount($cellData, $width);
                array_push($rowCountArr, $rowCount);
            }
            $maxRowCount = max($rowCountArr);
            $phpExcelObj->getActiveSheet()->getRowDimension($row)->setRowHeight($maxRowCount * 14 + 2.25);
        }
    }
}
