<?php

namespace Tfcloud\Services\Report\PhpExcel;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\ReportGenerateService;

class PhpExcelReportBaseService extends BaseService
{
    protected $permissionService;
    protected $returnedData;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    public function runReportWithValidate($repExcelFile, $inputs, $repId, &$returnData)
    {
        $this->returnedData = $returnData;
        $this->formatInputData($inputs);
        $validator = $this->validateFilter($inputs);
        if ($validator->passes()) {
            $filterText = '';
            $returnData['result'] = $this->generateReport($repExcelFile, $inputs, [], $filterText);
            $returnData['filterText'] = $filterText;
        } else {
            $returnData['result'] = false;
            $returnData['repFile'] = '';
            $returnData['filterValidation'] = $validator->messages()->toArray();
        }
    }

    protected function setProperties(Spreadsheet &$phpExcelObj, $params = [])
    {
        // Set properties
        $phpExcelObj->getProperties()
            ->setCreator(array_get($params, 'creator', 'TF Cloud'))
            ->setLastModifiedBy(array_get($params, 'lastModifiedBy', 'TF Cloud'))
            ->setTitle(array_get($params, 'title', 'TF Report'))
            ->setSubject(array_get($params, 'subject', 'TF Report'))
            ->setDescription(array_get($params, 'description', 'TF Report'))
            ->setKeywords(array_get($params, 'keywords', 'report'))
            ->setCategory(array_get($params, 'category', 'report'));
    }

    /**
     * Set values to excel cells
     * @param array $data Array of data. Structure:
     *          $data = [
     *              'sheet_1' => [
     *                  'A1' => 'A1 value',
     *                  'A2' => 'A2 value'
     *              ],
     *              'sheet_2' => [
     *                  'A1' => 'A1 value',
     *              ]
     *          ]
     */
    protected function setWorkSheetCellsValue(Spreadsheet &$phpExcelObj, $data = [])
    {
        $errorMessages = null;

        try {
            foreach ($data as $workSheetName => $workSheetData) {
                $phpExcelObj->setActiveSheetIndexByName($workSheetName);

                if (!is_array($workSheetData) || !count($workSheetData)) {
                    continue;
                }

                foreach ($workSheetData as $key => $value) {
                    $phpExcelObj->getActiveSheet()->setCellValue($key, $value);
                }
            }
        } catch (\Exception $ex) {
            $errorMessages = $ex->getMessage();
        }

        return $errorMessages;
    }

    /**
     * @param $reportFileName
     * @return string
     */
    protected function generateReportOutPutPath($reportFileName, $reportFileExtension = 'xls')
    {
        // Create a reports directory for the site group
        $reportDir = storage_path()
                . DIRECTORY_SEPARATOR . 'reports' . DIRECTORY_SEPARATOR
                . \Auth::User()->site_group_id;
        if (!is_dir($reportDir)) {
            mkdir($reportDir, 0755);
        }
        // Create a reports directory for the userid
        $reportDir .= DIRECTORY_SEPARATOR . \Auth::User()->id;
        if (!is_dir($reportDir)) {
            mkdir($reportDir, 0755);
        }

        if (!in_array($reportFileExtension, ['xls', 'xlsx'])) {
            $reportFileExtension = 'xls';
        }

        $reportDir = $reportDir . DIRECTORY_SEPARATOR . $reportFileName . '.' . $reportFileExtension;
        return $reportDir;
    }

    protected function writeExcelFile(Spreadsheet $phpExcelObj, $filePath, $fileType = 'Xls', $isUpdateMode = false)
    {
        $errorMessages = null;

        try {
            if ($isUpdateMode) {
                $fileType = IOFactory::identify($filePath);
                $writerObj = IOFactory::createWriter($phpExcelObj, $fileType);
            } else {
                switch ($fileType) {
                    case 'Xlsx':
                        $writerObj = new Xlsx($phpExcelObj);
                        break;
                    case 'Xls':
                    default:
                        $writerObj = new Xls($phpExcelObj);
                        break;
                }
            }
            $writerObj->setOffice2003Compatibility(true);
            $writerObj->save($filePath);
        } catch (\Exception $ex) {
            $errorMessages = $ex->getMessage();
        }

        return $errorMessages;
    }

    protected function writeBoxSpoutFile(\Box\Spout $boxSpout, $filePath, $fileType = 'Xls', $isUpdateMode = false)
    {
        $errorMessages = null;

        try {
            if ($isUpdateMode) {
                $fileType = IOFactory::identify($filePath);
                $writerObj = IOFactory::createWriter($boxSpout, $fileType);
            } else {
                switch ($fileType) {
                    case 'Xlsx':
                        $writerObj = new Xlsx($boxSpout);
                        break;
                    case 'Xls':
                    default:
                        $writerObj = new Xls($boxSpout);
                        break;
                }
            }

            $writerObj->save($filePath);
        } catch (\Exception $ex) {
            $errorMessages = $ex->getMessage();
        }

        return $errorMessages;
    }

    protected function validateFilter($inputs)
    {
        $rules = [];

        $messages = [];

        $validator = \Validator::make($inputs, $rules, $messages);

        return $validator;
    }

    public function getFiltering($whereCodes, $orCodes, $whereTexts, &$bFilterDetail, &$filterText)
    {
        ReportGenerateService::reGetFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $filterText);
    }

    private function formatInputData(&$inputs)
    {
        $selectorInputs = ['site_id', 'building_id', 'room_id','plant_id', 'project', 'cpContract'];
        foreach ($inputs as $key => $value) {
            if (($value === "") || ((in_array($key, $selectorInputs)) && ($value === "null"))) {
                $inputs[$key] = null;
            }
        }
    }
}
