<?php

namespace Tfcloud\Services\Report\PhpExcel;

use Box\Spout\Common\Entity\Style\Border as SpoutBorder;
use Box\Spout\Common\Entity\Style\Color;
use Box\Spout\Writer\Common\Creator\Style\BorderBuilder;
use Box\Spout\Writer\Common\Creator\Style\StyleBuilder;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use Tfcloud\Lib\BoxFilter\Constants\OperatorTypeConstant;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\BoxFilterQuery\FilterQueryBootstrap;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Debug;
use Tfcloud\Lib\Filters\Properties\PR21ComplianceFilter;
use Tfcloud\Models\AsbHsg264Type;
use Tfcloud\Models\AsbSurveyStatus;
use Tfcloud\Models\ComplianceExemption;
use Tfcloud\Models\Filter\FilterName;
use Tfcloud\Models\Inspection\InspectionType;
use Tfcloud\Models\InspectionSFG20\InspSFG20InspectionStatusType;
use Tfcloud\Models\InspectionSFG20\InspSFG20ScheduledInspection;
use Tfcloud\Models\InspectionSFG20\InspSFG20Type;
use Tfcloud\Models\Report;
use Tfcloud\Models\Site;
use Tfcloud\Models\User;
use Tfcloud\Services\PermissionService;

class PR21ComplianceService extends PhpExcelReportBaseService
{
    private const NEXT_ASBESTOS_INSPECTION_COL = 'E';
    private const START_DYNAMIC_COL = 'F';
    private const START_DYNAMIC_COL_INDEX = 5;
    private const START_DYNAMIC_ROW_INDEX = 3;
    private const HEADER_COLOR_HEX = 'DCEAF5';
    private const WHITE_COLOR_HEX = 'FFFFFF';
    private const RED_HEX = 'FF0000';
    private const AMBER_HEX = 'FFBF00';
    private const GREEN_HEX = '71AB48';
    private const GRAY_HEX = 'bfbfbf';
    private const RAG_COLOR_RED = 'RED';
    private const RAG_COLOR_AMBER = 'AMBER';
    private const RAG_COLOR_GREEN = 'GREEN';
    private const RAG_COLOR_RED_CSS = 'center background-red';
    private const RAG_COLOR_AMBER_CSS = 'center background-amber';
    private const RAG_COLOR_GREEN_CSS = 'center background-green';
    private const RAG_COLOR_GREY = 'GREY';
    private const RAG_COLOR_GREY_CSS = 'center background-grey';

    private $siteGroupId = null;

    private $filterQuery = null;
    private $reportBoxFilterLoader = null;

    public function __construct(
        PermissionService $permissionService,
        Report $report = null
    ) {
        parent::__construct($permissionService);
        $this->siteGroupId = \Auth::user()->site_group_id;
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }
    public function validateFilter($inputs)
    {
        $this->formatInputData($inputs);
        \Validator::extend('required_if_without', function ($attribute, $value, $parameters) use ($inputs) {
            if (
                empty($inputs['establishment']) &&
                empty($inputs['site_id']) &&
                empty($inputs['building_id']) &&
                empty($inputs['site_type_id']) &&
                empty($inputs['parish_id']) &&
                empty($inputs['committee_id']) &&
                empty($inputs['in_active']) &&
                empty($inputs['site_designation_id']) &&
                empty($inputs['prop_tenure_id']) &&
                empty($inputs['site_usage_id']) &&
                empty($inputs['ward_id'])
            ) {
                return false;
            }
            return true;
        });

        return \Validator::make($inputs, $this->validateRules(), $this->validateMessages());
    }

    public function validateRules()
    {
        $rules = [
            'site_id' => ['required_if_without'],
        ];

        return $rules;
    }

    public function validateMessages()
    {
        $messages = [
            'site_id.required_if_without' => 'At least one filter is required to run this report.'
        ];

        return $messages;
    }
    private function formatInputData(&$inputs)
    {
        foreach ($inputs as $key => $value) {
            if ($value === "") {
                unset($inputs[$key]);
            }
        }
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $siteValue = $reportFilterQuery->getValueFilterField('site_id');
            $buildingValue = $reportFilterQuery->getValueFilterField('building_id');
            $inActiveValue = $reportFilterQuery->getValueFilterField('in_active');
            $establishmentFilterField = $reportFilterQuery->getFilterField('establishment_id');
            $siteTypeFilterField = $reportFilterQuery->getFilterField('site_type_id');
            $parishFilterField = $reportFilterQuery->getFilterField('parish_id');
            $committeeFilterField = $reportFilterQuery->getFilterField('committee_id');
            $siteDesignationFilterField = $reportFilterQuery->getFilterField('site_designation_id');
            $propTenureFilterField = $reportFilterQuery->getFilterField('prop_tenure_id');
            $siteUsageFilterField = $reportFilterQuery->getFilterField('site_usage_id');
            $wardFilterField = $reportFilterQuery->getFilterField('ward_id');

            $inputs['establishment'] = $this->getValueRequired($establishmentFilterField);
            $inputs['site_id'] = empty($siteValue) ? request('site_id') : $siteValue[0];
            $inputs['building_id'] = empty($buildingValue) ? request('building_id') : $buildingValue[0];
            $inputs['site_type_id'] = $this->getValueRequired($siteTypeFilterField);
            $inputs['parish_id'] = $this->getValueRequired($parishFilterField);
            $inputs['committee_id'] = $this->getValueRequired($committeeFilterField);
            $inputs['in_active'] = empty($inActiveValue) ? null : $inActiveValue[0];
            $inputs['site_designation_id'] = $this->getValueRequired($siteDesignationFilterField);
            $inputs['prop_tenure_id'] = $this->getValueRequired($propTenureFilterField);
            $inputs['site_usage_id'] = $this->getValueRequired($siteUsageFilterField);
            $inputs['ward_id'] = $this->getValueRequired($wardFilterField);
        }
        return $inputs;
    }

    private function getValueRequired($filterField)
    {
        if (!empty($filterField)) {
            foreach ($filterField->getFilterOperators() as $filterOperator) {
                $values = $filterOperator->getValues();
                switch ($filterOperator->getOperator()) {
                    case OperatorTypeConstant::NOT_BLANK:
                    case OperatorTypeConstant::BLANK:
                        return 1;
                        break;
                    default:
                        return empty($values) ? null : $values[0];
                }
            }
        }
        return null;
    }

    private function initFilterQuery($inputs)
    {
        $exportFromNewFilter = false;

        if (array_key_exists('exportAs', $inputs)) {
            $exportFromNewFilter = strtolower(array_get($inputs['exportAs'], 'label', false));
        } elseif (array_key_exists('fileExtension', $inputs)) {
            $exportFromNewFilter = array_get($inputs, 'fileExtension', false);
        }

        if ($exportFromNewFilter) {
            if (
                ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()
                && array_key_exists('exportAs', $inputs)) || array_get($inputs, 'exportFromLandingPage', false)
            ) {
                $this->filterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            } else {
                $this->filterQuery = FilterQueryBootstrap::getFilterQuery(
                    FilterName::INSPECTION_SFG20_SCHEDULED_INSPECTION_COMPLIANCE_BY_LOCATION,
                    $inputs
                );
            }
        }

        return $exportFromNewFilter === 'html';
    }

    public function generateReport($repExcelFile, $inputs, $options = [], &$filterText = '')
    {
        $result = true;
        $exportFromNewFilter = $this->initFilterQuery($inputs);
        $html = (array_get($inputs, 'exportHTML', false) || $exportFromNewFilter);

        try {
            $reportData = $this->fetchData($filterText, $inputs);

            if ($html) {
                $html = $this->generateHtmlObject($inputs, $reportData);
                file_put_contents($repExcelFile, $html);
                $errorMessages = array();
            } else {
                $errorMessages = $this->generateBoxSpoutObject($repExcelFile, $inputs, $reportData);
            }

            if (is_array($errorMessages) && count($errorMessages)) {
                $result = false;
            }
        } catch (\Exception $ex) {
            $result = false;
        }

        $whereCodes = [];               // The codes which have been used in the filter
        $whereTexts = [];               // Plain text about filtering applied
        $orCodes = [];                  // csv string of OR codes
        $bFilterDetail = false;

        $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
        $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $filterText);

        return $result;
    }

    private function fetchData(&$filterText, $inputs = [])
    {
        // Filter by Location to scope data
        $filter = new PR21ComplianceFilter($inputs);
        $siteId = null;
        $buildingId = null;

        // If export from box filter then get siteId and buildingId from filter
        if ($this->filterQuery) {
            $filter = null;
            $siteIds = $this->filterQuery->getValueFilterField('site_id');
            $buildingIds = $this->filterQuery->getValueFilterField('building_id');
            $filterText = $this->filterQuery->getFilterDetailText();

            $siteId = empty($siteIds) ? null : $siteIds[0];
            $buildingId = empty($buildingIds) ? null : $buildingIds[0];
        } else {
            if ($filter->site_id) {
                $siteId = $filter->site_id;
            }

            if ($filter->building_id) {
                $buildingId = $filter->building_id;
            }
        }

        // Get data with filter by
        $dataArr = $this->getReportData($siteId, $buildingId, $filter);
        //\log::error($dataArr);
        return $dataArr;
    }
    private function generateBoxSpoutObject($repExcelFile, $inputs = [], $reportData = [], $options = [])
    {
        $errorMessages = null;
        try {
            $writer = WriterEntityFactory::createXLSXWriter();
            $writer->openToFile($repExcelFile); // write data to a file or to a PHP stream

            //define Styles
            $borderStyle = (new BorderBuilder())
                ->setBorderTop(Color::BLACK, SpoutBorder::WIDTH_THIN, SpoutBorder::STYLE_SOLID)
                ->setBorderRight(Color::BLACK, SpoutBorder::WIDTH_THIN, SpoutBorder::STYLE_SOLID)
                ->setBorderBottom(Color::BLACK, SpoutBorder::WIDTH_THIN, SpoutBorder::STYLE_SOLID)
                ->setBorderLeft(Color::BLACK, SpoutBorder::WIDTH_THIN, SpoutBorder::STYLE_SOLID)
                ->build();

            $headerStyle = (new StyleBuilder())->setBorder($borderStyle)
                ->setBackgroundColor(self::HEADER_COLOR_HEX)
                ->setFontBold()
                ->build();

            $normalStyle = (new StyleBuilder())
                ->setBorder($borderStyle)
                ->setBackgroundColor(self::WHITE_COLOR_HEX)
                ->build();
            $redStyle =  (new StyleBuilder())
                ->setBorder($borderStyle)
                ->setBackgroundColor(self::RED_HEX)
                ->build();
            $greenStyle = (new StyleBuilder())
                ->setBorder($borderStyle)
                ->setBackgroundColor(self::GREEN_HEX)
                ->build();
            $emberStyle = (new StyleBuilder())
                ->setBorder($borderStyle)
                ->setBackgroundColor(self::AMBER_HEX)
                ->build();
            $grayStyle = (new StyleBuilder())
                ->setBorder($borderStyle)
                ->setBackgroundColor(self::GRAY_HEX)
                ->build();

            //prepare header
            $fixedHeaderArray = $this->getFixedHeaders();
            $dynamicHeaderArray = $this->getReportHeaderData();

            $firstHeaderRow = [null, null, null, null];
            $secondHeaderRow = $fixedHeaderArray;

            foreach ($dynamicHeaderArray as $inspectionType) {
                $firstHeaderRow[] = $inspectionType;
                $firstHeaderRow[] = null;
                $secondHeaderRow[] = 'Next Scheduled Inspection';
                $secondHeaderRow[] = 'Last Report Received';
            }

            $writer->addRow(WriterEntityFactory::createRowFromArray($firstHeaderRow, $headerStyle));
            $writer->addRow(WriterEntityFactory::createRowFromArray($secondHeaderRow, $headerStyle));

            $bodyRows = [];

            foreach ($reportData as $recordArr) {
                if (!is_array($recordArr) || !count($recordArr)) {
                    continue;
                }

                $cellsOfARow = [];
                foreach ($recordArr as $cellData) {
                    $value = array_get($cellData, 'value');
                    $RAGColor = array_get($cellData, 'color');
                    $cellStyle = null;

                    switch ($RAGColor) {
                        case self::RAG_COLOR_RED:
                            $cellStyle = $redStyle;
                            break;
                        case self::RAG_COLOR_AMBER:
                            $cellStyle = $emberStyle;
                            break;
                        case self::RAG_COLOR_GREEN:
                            $cellStyle = $greenStyle;
                            break;
                        case self::RAG_COLOR_GREY:
                            $cellStyle = $grayStyle;
                            break;
                        default:
                            $cellStyle = $normalStyle;
                            break;
                    }

                    $cell = WriterEntityFactory::createCell($value, $cellStyle);
                    $cellsOfARow[] = $cell;
                }
                $bodyRows[] = WriterEntityFactory::createRow($cellsOfARow);
            }

            $writer->addRows($bodyRows);
            $writer->close();
        } catch (\Exception $ex) {
            $errorMessages = $ex->getMessage();
        }

        return $errorMessages;
    }
    private function generateHtmlObject($inputs = [], $reportData = [], $options = [])
    {
        // Fixed headers
        $fixedHeaderArr = $this->getFixedHeaders();

        $html = "";

        $style = "<style>";

        $style .= "body {margin:1px; height:100%; font-family: Calibri;}"
                . "table {border-collapse: collapse;}"
                . "table, td, th { border: 1px solid black;}"
                . "th {white-space: nowrap; vertical-align:bottom; "
                .   "padding-left:10px; padding-right:10px; background-color:#dceaf5; }";

        $style .= "</style>";

        $html .= $style;

        $html .= "<table><thead>";
        foreach ($fixedHeaderArr as $key => $value) {
              $html .= "<th";

            if (($value == 'Site Description') || ($value == 'Building Description')) {
                $html .= ' style=min-width:250px;';
            }
            $html .= " rowspan='2'>$value</th>";
        }

        // Dynamic headers
        $dynamicHeaderArr = $this->getReportHeaderData();
        $this->setDynamicHtmlHeaders($html, $dynamicHeaderArr);
        $html .= "</thead>";

        // For debugging
        //$startDynamicCol = self::START_DYNAMIC_COL;
        //$endDynamicCol = chr(ord($startDynamicCol) + (2 * count($elementArr)) - 1);

        // Fill data to cells
        // Two first rows for the header, so data starts at row 3
        $html .= "\n<tbody>";

        foreach ($reportData as $recordArr) {
            if (!is_array($recordArr) || !count($recordArr)) {
                continue;
            }

            $dataColumn = 0; // Start at first col
            $html .= "\n<tr>";
            foreach ($recordArr as $cellData) {
                $value = array_get($cellData, 'value');
                $colorRag = array_get($cellData, 'color');

                $html .= "<td";
                switch ($colorRag) {
                    case "RED":
                        $html .= ' style="text-align:center; background-color:' . CommonConstant::RED_HEX . ';"';
                        break;
                    case "AMBER":
                        $html .= ' style="text-align:center; background-color:' . CommonConstant::AMBER_HEX . ';"';
                        break;
                    case "GREEN":
                        $html .= ' style="text-align:center; background-color:' . CommonConstant::GREEN_HEX . ';"';
                        break;
                    case "GREY":
                        $html .= ' style="text-align:center; background-color:' . CommonConstant::GREY_HEX . ';"';
                        break;
                }

                $html .= ">$value</td>";
                $dataColumn++;
            }
            $html .= "</tr>";
        }
        $html .= "</tbody>";

        return $html;
    }

    public function getReportHeaderData($siteId = null, $buildingId = null)
    {
        $header = [];
        $inspectionTypes = $this->getInspectionTypeVersion($siteId, $buildingId);

        //Debug::log(count($inspectionTypes), 'count($inspectionTypes)');
        foreach ($inspectionTypes as $inspectionType) {
            $header = array_add(
                $header,
                $inspectionType->insp_sfg20_type_version_id,
                $inspectionType->inspection_type_header
            );
//            Debug::log(
//                $inspectionType . "",
//                "inspectionType: $inspectionType->inspection_type_id"
//            );
        }
       // Debug::log($header, '$header');
//        natcasesort($header);

        return $header;
    }

    public function getReportData($siteId = null, $buildingId = null, $filter = null)
    {
        $inspectionTypes = $this->getInspectionTypeVersion($siteId, $buildingId);

        $isFilterInactive = false;
        // Refactor this when applied new filter for report PR21
        if ($this->filterQuery) {
            $siteQuery = $this->filterQuery->getSitesQueryFromBoxFilter();

            if (!is_null($this->filterQuery->getValueFilterField('in_active'))) {
                $isFilterInactive = true;
            }
        } else {
            $siteQuery = $this->getSitesQuery($siteId, $buildingId, $filter);
        }

        $sites = $siteQuery->get();

        $data = [];
        foreach ($sites as $site) {
            if (is_null($buildingId)) {
                if ($site->include_in_compliance_report == CommonConstant::DATABASE_VALUE_YES) {
                    $row = [
                        ['value' => $site->site_code],
                        ['value' => $site->site_desc],
                        ['value' => ''],
                        ['value' => ''],
                    ];

                    $this->generateInspection($row, $inspectionTypes, $site->site_id);

                    if ($isFilterInactive) {
                        $inActiveFilterValue = $this->filterQuery->getValueFilterField('in_active')[0];

                        $showInactive = Common::valueYorNtoBoolean($inActiveFilterValue);
                        if ($showInactive) {
                            if ($site->active == 'N') {
                                array_push($data, $row);
                            }
                        } else {
                            if ($site->active == 'Y') {
                                array_push($data, $row);
                            }
                        }
                    } else {
                        array_push($data, $row);
                    }
                }
            }

            foreach ($site->buildings as $building) {
                if ($building->include_in_compliance_report == CommonConstant::DATABASE_VALUE_YES) {
                    $row = [
                        ['value' => $site->site_code],
                        ['value' => $site->site_desc],
                        ['value' => $building->building_code],
                        ['value' => $building->building_desc]
                    ];

                    //build inspection data for building level including 2 columns
                    $this->generateInspection($row, $inspectionTypes, $site->site_id, $building->building_id);
                    array_push($data, $row);
                }
            }
        }

        return $data;
    }

    private function getFixedHeaders()
    {
        return [
            'A1' => 'Site Code',
            'B1' => 'Site Description',
            'C1' => 'Building Code',
            'D1' => 'Building Description',
//            'E1' => 'Next Asbestos Inspection'
        ];
    }

    private function getFixedHeaderStyles()
    {
        return [
            'font'  => [
                'bold'  => true,
                'color' => ['rgb' => self::WHITE_COLOR_HEX],
                'size'  => 11,
                'name'  => 'Calibri'
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_BOTTOM
            ],
            'fill' => [
                'type' => Fill::FILL_SOLID,
                'color' => ['rgb' => self::HEADER_COLOR_HEX],
            ]
        ];
    }

    private function getHeaderMergingCells($startDynamicCol, $endDynamicCol)
    {
        $mergingHeader = [
            'A1' => 'A1:A2',
            'B1' => 'B1:B2',
            'C1' => 'C1:C2',
            'D1' => 'D1:D2',
//            'E1' => 'E1:E2'
        ];

        foreach (range($startDynamicCol, $endDynamicCol, 2) as $columnIndex) {
            $col1 = Coordinate::stringFromColumnIndex($columnIndex);
            $col2 = Coordinate::stringFromColumnIndex($columnIndex + 1);

            $mergingHeader[$col1 . '1'] = "{$col1}1:{$col2}1";
        }

        return $mergingHeader;
    }

    private function setCellColor(Spreadsheet &$phpExcelObj, $cells, $color)
    {
        $phpExcelObj->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray([
            'type' => Fill::FILL_SOLID,
            'startcolor' => [
                'rgb' => $color
            ]
        ]);
    }

    private function setCellColumnRowColor(
        Spreadsheet &$phpExcelObj,
        $color,
        $pColumn = 0,
        $pRow = 1,
        $pColumn2 = null,
        $pRow2 = null
    ) {
        $phpExcelObj->getActiveSheet()->getStyleByColumnAndRow($pColumn, $pRow, $pColumn2, $pRow2)
            ->getFill()->applyFromArray([
                'type' => Fill::FILL_SOLID,
                'startcolor' => [
                    'rgb' => $color
                ]
            ]);
    }


    private function setCellColumnRowColorRag(Spreadsheet &$phpExcelObj, $column, $row, $ragColor = '')
    {
        $color = null;
        switch ($ragColor) {
            case self::RAG_COLOR_RED:
                $color = CommonConstant::RED_HEX;
                break;
            case self::RAG_COLOR_AMBER:
                $color = CommonConstant::AMBER_HEX;
                break;
            case self::RAG_COLOR_GREEN:
                $color = CommonConstant::GREEN_HEX;
                break;
            case self::RAG_COLOR_GREY:
                $color = CommonConstant::GREY_HEX;
                break;
            default:
                break;
        }

        if ($color) {
            $this->setCellColumnRowColor($phpExcelObj, $color, $column, $row);
        }
    }

    private function setDynamicHeaders(
        Spreadsheet &$phpExcelObj,
        $elementArr,
        $startCol = self::START_DYNAMIC_COL_INDEX
    ) {
        $firstColText = 'Next Scheduled Inspection';
        $secondColText = 'Last Report Received';
        $col = $startCol;

        foreach ($elementArr as $element) {
            $row = 1;
            $phpExcelObj->getActiveSheet()
                ->setCellValueByColumnAndRow($col, $row++, $element)
                ->setCellValueByColumnAndRow($col++, $row, $firstColText)
                ->setCellValueByColumnAndRow($col++, $row, $secondColText);
        }
    }
    private function setDynamicHtmlHeaders(&$html, $elementArr)
    {
        $firstColText = 'Next Scheduled Inspection';
        $secondColText = 'Last Report Received';

        foreach ($elementArr as $element) {
            $html .= "<th colspan='2'>$element</th>";
        }

        $html .= "<tr>";

        foreach ($elementArr as $element) {
            $html .= "<th> $firstColText</th>";
            $html .= "<th> $secondColText</th>";
        }

        $html .= "</tr>";
    }

    public function buildAsbSurveyQuery(&$query, $buildingLevel = false)
    {
        $query->join(
            'asb_survey_type',
            'asb_survey_type.survey_type_id',
            '=',
            'asb_survey.survey_type_id'
        )
            ->where('survey_status_id', AsbSurveyStatus::CURRENT)
            ->where('asb_survey_type.hsg264_type_id', AsbHsg264Type::MANAGEMENT);
        if ($buildingLevel) {
            $query->whereNotNull('asb_survey.building_id');
        } else {
            $query->whereNull('asb_survey.building_id');
        }
    }



    private function getScheduledInspections(
        $siteId,
        $buildingId = false
    ) {
        $query = InspSFG20ScheduledInspection::userSiteGroup()
        ->join(
            'insp_sfg20_inspection_version',
            'insp_sfg20_inspection_version.insp_sfg20_inspection_version_id',
            '=',
            'insp_sfg20_scheduled_inspection.insp_sfg20_inspection_version_id'
        )
        ->join(
            'insp_sfg20_inspection',
            'insp_sfg20_inspection.insp_sfg20_inspection_id',
            '=',
            'insp_sfg20_inspection_version.insp_sfg20_inspection_id'
        )
        ->join(
            'insp_sfg20_type_version',
            'insp_sfg20_type_version.insp_sfg20_type_version_id',
            '=',
            'insp_sfg20_inspection_version.insp_sfg20_type_version_id'
        )
        ->join(
            'insp_sfg20_type',
            'insp_sfg20_type_version.insp_sfg20_type_id',
            '=',
            'insp_sfg20_type.insp_sfg20_type_id'
        )
        ->join(
            'inspection_status',
            'inspection_status.inspection_status_id',
            '=',
            'insp_sfg20_scheduled_inspection.inspection_status_id'
        )
        ->where('insp_sfg20_type_version.include_in_compliance_report', CommonConstant::DATABASE_VALUE_YES)
        ->where('insp_sfg20_inspection.site_id', $siteId);
        if ($buildingId) {
            $query->where('insp_sfg20_inspection.building_id', $buildingId);
        } else {
            $query->whereNull('insp_sfg20_inspection.building_id');
        }

        return $query->get();
    }

    private function getInspectionExemptionArr($siteId, $buildingId = false)
    {
        $query = ComplianceExemption::where('site_id', $siteId);
        if ($buildingId) {
            $query->where('building_id', $buildingId);
        } else {
            $query->whereNull('building_id');
        }
        $complianceExemptions = $query->get();
        $inspectionExemptionArr = [];
        if ($complianceExemptions != null) {
            foreach ($complianceExemptions as $complianceExemption) {
                array_push($inspectionExemptionArr, $complianceExemption->inspection_type_id);
            }
        }

        return $inspectionExemptionArr;
    }

    private function generateAsbNextReviewDate($asbestosSurveyRequired, $modelRelateWithAsbSurveys)
    {
        if ($asbestosSurveyRequired == CommonConstant::DATABASE_VALUE_YES) {
            $asbSurvey = $modelRelateWithAsbSurveys->asbSurveys;

            if (count($asbSurvey) == 1 && $asbSurveyNextReviewDate = $asbSurvey[0]->asb_survey_next_review_date) {
                $nextReviewItem = $this->setColorForDate($asbSurveyNextReviewDate);
            } elseif (
                (count($asbSurvey) == 1 && !$asbSurvey[0]->asb_survey_next_review_date) ||
                (count($asbSurvey) > 1)
            ) {
                $nextReviewItem = [
                    'value' => '',
                    'color' => self::RAG_COLOR_RED,
                    'cssClass' => self::RAG_COLOR_RED_CSS
                ];
            } else {
                $nextReviewItem = [
                    'value' => '',
                    'color' => self::RAG_COLOR_RED,
                    'cssClass' => self::RAG_COLOR_RED_CSS,
                    'surveyNotFound' => true
                ];
            }
        } else {
            $nextReviewItem = [
                'value' => ''
            ];
        }

        return $nextReviewItem;
    }

    public function generateInspection(&$row, $inspectionTypes, $siteId, $buildingId = false)
    {
        $inspectionExemptionArr = $this->getInspectionExemptionArr($siteId, $buildingId);
        $inspections = $this->getScheduledInspections($siteId, $buildingId);
        if ($inspections->count() > 0) {
            foreach ($inspectionTypes as $inspectionType) {
                $exempt = false;

                if (in_array($inspectionType->insp_sfg20_type_version_id, $inspectionExemptionArr)) {
                    array_push($row, [
                        'value' => '',
                        'color' => self::RAG_COLOR_GREY,
                        'cssClass' => self::RAG_COLOR_GREY_CSS]);
                    array_push($row, [
                        'value' => '',
                        'color' => self::RAG_COLOR_GREY,
                        'cssClass' => self::RAG_COLOR_GREY_CSS
                    ]);

                    $exempt = true;
                }
                if ($exempt == false) {
                    $inspection = $inspections
                    ->whereIn('inspection_status_type_id', [
                        InspSFG20InspectionStatusType::OPEN, InspSFG20InspectionStatusType::ISSUED
                    ])
                    ->where('insp_sfg20_type_version_id', $inspectionType->insp_sfg20_type_version_id)
                    ->sortBy('inspection_due_date')->first();

                    if ($inspection && $inspectionDueDate = $inspection->inspection_due_date) {
                        $nextInspection = $this->setColorForDate($inspection->inspection_due_date);
                        array_push($row, array_merge($nextInspection, ['inspectionId' => $inspection->getKey()]));
                        $inspectionReceived = $inspections
                        ->whereIn(
                            'inspection_status_type_id',
                            [InspSFG20InspectionStatusType::CLOSED, InspSFG20InspectionStatusType::COMPLETE]
                        )
                        ->where('insp_sfg20_type_version_id', $inspectionType->insp_sfg20_type_version_id)
                        ->sortByDesc('inspection_due_date')->first();
                        /*
                         * Case: found next inspection
                         * Get highest inspection
                         * And certificate loaded is Yes
                         * And due date less than due date of next inspection
                         * */
                        if (
                            $inspectionReceived &&
                            $inspectionReceived->certificate_loaded == CommonConstant::DATABASE_VALUE_ONE &&
                            $this->compareDates($inspectionReceived->inspection_due_date, $inspectionDueDate, '<')
                        ) {
                            array_push($row, [
                                'inspectionId' => $inspectionReceived->getKey(),
                                'value' => 'Y',
                                'color' => self::RAG_COLOR_GREEN,
                                'cssClass' => self::RAG_COLOR_GREEN_CSS
                            ]);
                        } else {
                            array_push($row, ['value' => '']);
                        }
                    } else {
                        array_push($row, ['value' => '']);
                        $inspectionReceived = $inspections->whereIn(
                            'inspection_status_type_id',
                            [InspSFG20InspectionStatusType::CLOSED, InspSFG20InspectionStatusType::COMPLETE]
                        )
                        ->where('insp_sfg20_type_version_id', $inspectionType->insp_sfg20_type_version_id)
                        ->sortByDesc('inspection_due_date')->first();

                        /*
                         * Case: Next scheduled inspection not found
                         * Get highest scheduled inspection
                         * And certificate loaded is Yes
                         * */
                        if (
                            $inspectionReceived &&
                            $inspectionReceived->certificate_loaded == CommonConstant::DATABASE_VALUE_YES
                        ) {
                            array_push($row, [
                                'inspectionId' => $inspectionReceived->getKey(),
                                'value' => 'Y',
                                'color' => self::RAG_COLOR_GREEN,
                                'cssClass' => self::RAG_COLOR_GREEN_CSS
                            ]);
                        } else {
                            array_push($row, ['value' => '']);
                        }
                    }
                }
            }
        } else {
            foreach ($inspectionTypes as $inspectionType) {
                if (in_array($inspectionType->insp_sfg20_type_version_id, $inspectionExemptionArr)) {
                    array_push($row, [
                        'value' => '',
                        'color' => self::RAG_COLOR_GREY,
                        'cssClass' => self::RAG_COLOR_GREY_CSS]);
                    array_push($row, [
                        'value' => '',
                        'color' => self::RAG_COLOR_GREY,
                        'cssClass' => self::RAG_COLOR_GREY_CSS
                    ]);

                    $exempt = true;
                } else {
                    array_push($row, ['value' => '']);
                    array_push($row, ['value' => '']);
                }
            }
        }

        Debug::log($row, '$row');
    }

    private function setColorForDate($date, $time = null)
    {
        $date = Common::dateFieldDisplayFormat($date);
        if ($time) {
            $dateObj = Common::stringToDateTime(['date' => $date, 'time' => '00:00:00']);
        } else {
            $dateObj = Common::stringToDateTime(['date' => $date]);
        }

        $currentDateObj = new \DateTime(date('Y-m-d 00:00:00'));
        if ($dateObj < $currentDateObj) {
            $color = self::RAG_COLOR_RED;
            $cssClass = self::RAG_COLOR_RED_CSS;
        } elseif (
            $dateObj >= $currentDateObj &&
            $dateObj <= $currentDateObj->add(new \DateInterval('P57D'))
        ) {
            $color = self::RAG_COLOR_AMBER;
            $cssClass = self::RAG_COLOR_AMBER_CSS;
        } else {
            $color = self::RAG_COLOR_GREEN;
            $cssClass = self::RAG_COLOR_GREEN_CSS;
        }
        $result = [
            'value' => $date,
            'color' => $color,
            'cssClass' => $cssClass
        ];
        return $result;
    }

    public function getInspectionTypes($siteId = null, $buildingId = null)
    {
        $query = InspectionType::parentUserSiteGroup()
            ->where('inspection_group.site_group_id', $this->siteGroupId)
            ->where('inspection_type.include_in_compliance_report', CommonConstant::DATABASE_VALUE_ONE)
            ->select([
                \DB::raw("CONCAT_WS(' - ',
                IFNULL (inspection_group.inspection_group_desc, inspection_group.inspection_group_code),
                IFNULL (inspection_type.inspection_type_description, inspection_type.inspection_type_code)) AS
                'inspection_type_header'"),
                'inspection_type.inspection_type_id'
            ])

            ->orderBy('inspection_group.inspection_group_code')
            ->orderBy('inspection_type.inspection_type_code')
        ;

        if (\Auth::user()->getSourceController() != User::SOURCE_REPORT) {
            $inspectionTypes = $query->get();
            $site = Site::userSiteGroup()->find($siteId);
            if ($site) {
                if (!$buildingId) {
                    $buildingIds = $site->buildings->pluck('building_id');
                    $allSiteAndBuilding = count($buildingIds) + 1;
                    foreach ($inspectionTypes as $key => $inspectionType) {
                        $complianceExemptions = ComplianceExemption::userSiteGroup()
                            ->where(function ($sub) use ($siteId, $buildingIds) {
                                $sub->where('site_id', $siteId);
                                $sub->orWhereIn('building_id', $buildingIds);
                            })
                            ->where('inspection_type_id', $inspectionType->inspection_type_id)
                            ->count();
                        if ($complianceExemptions == $allSiteAndBuilding) {
                            $inspectionTypes->forget($key);
                        }
                    }
                } elseif ($buildingId) {
                    foreach ($inspectionTypes as $key => $inspectionType) {
                        $complianceExemptions = ComplianceExemption::userSiteGroup()
                            ->where('site_id', $siteId)
                            ->where('building_id', $buildingId)
                            ->where('inspection_type_id', $inspectionType->inspection_type_id)
                            ->count();
                        if ($complianceExemptions > 0) {
                            $inspectionTypes->forget($key);
                        }
                    }
                }

                return $inspectionTypes;
            }
        }

        return $query->get();
    }

    public function getInspectionTypeVersion($siteId = null, $buildingId = null)
    {
        $query = InspSFG20Type::userSiteGroup()
            ->join(
                'insp_sfg20_type_version',
                'insp_sfg20_type_version.insp_sfg20_type_id',
                '=',
                'insp_sfg20_type.insp_sfg20_type_id'
            )
            ->leftjoin(
                'insp_sfg20_group_level_1',
                'insp_sfg20_group_level_1.insp_sfg20_group_level_1_id',
                '=',
                'insp_sfg20_type_version.insp_sfg20_group_level_1_id'
            )
            ->where('insp_sfg20_type_version.include_in_compliance_report', CommonConstant::DATABASE_VALUE_YES)
            ->select([
                \DB::raw("CONCAT_WS(' - ',
                IFNULL (insp_sfg20_group_level_1.insp_sfg20_group_level_1_desc, insp_sfg20_group_level_1."
                . "insp_sfg20_group_level_1_code),
                IFNULL (insp_sfg20_type.insp_sfg20_type_code, insp_sfg20_type_version.insp_sfg20_type_desc)) AS
                'inspection_type_header'"),
                'insp_sfg20_type_version.insp_sfg20_type_version_id',
                'insp_sfg20_type.insp_sfg20_type_code',
                'insp_sfg20_type.insp_sfg20_type_id'
            ])
            ->groupBy('insp_sfg20_type_version.insp_sfg20_type_version_id')
            ->orderBy('insp_sfg20_type_version.insp_sfg20_type_desc');

        if (\Auth::user()->getSourceController() != User::SOURCE_REPORT) {
            $inspectionTypes = $query->get();

            $site = Site::userSiteGroup()->find($siteId);
            if ($site) {
                if (!$buildingId) {
                    $buildingIds = $site->buildings->pluck('building_id');
                    $allSiteAndBuilding = count($buildingIds) + 1;
                    foreach ($inspectionTypes as $key => $inspectionType) {
                        $complianceExemptions = ComplianceExemption::userSiteGroup()
                            ->where(function ($sub) use ($siteId, $buildingIds) {
                                $sub->where('site_id', $siteId);
                                $sub->orWhereIn('building_id', $buildingIds);
                            })
                            ->where('inspection_type_version_id', $inspectionType->insp_sfg20_type_version_id)
                            ->count();
                        if ($complianceExemptions == $allSiteAndBuilding) {
                            $inspectionTypes->forget($key);
                        }
                    }
                } elseif ($buildingId) {
                    foreach ($inspectionTypes as $key => $inspectionType) {
                        $complianceExemptions = ComplianceExemption::userSiteGroup()
                            ->where('site_id', $siteId)
                            ->where('building_id', $buildingId)
                            ->where('inspection_type_version_id', $inspectionType->insp_sfg20_type_version_id)
                            ->count();
                        if ($complianceExemptions > 0) {
                            $inspectionTypes->forget($key);
                        }
                    }
                }

                return $inspectionTypes;
            }
        }

        return $query->get();
    }

    private function compareDates($date1, $date2, $operator)
    {
        $date1 = Common::dateFieldDisplayFormat($date1);
        $date2 = Common::dateFieldDisplayFormat($date2);
        $date1Obj = Common::stringToDateTime(['date' => $date1]);
        $date2Obj = Common::stringToDateTime(['date' => $date2]);
        $result = null;
        switch ($operator) {
            case '<':
                $result = $date1Obj < $date2Obj;
                break;
            case '>':
                $result = $date1Obj > $date2Obj;
                break;
            case '=':
                $result = $date1Obj == $date2Obj;
                break;
            case '<=':
                $result = $date1Obj <= $date2Obj;
                break;
            case '>=':
                $result = $date1Obj >= $date2Obj;
                break;
        }
        return $result;
    }

    private function generateFilterData($filterData, &$whereCodes, &$whereTexts, &$orCodes)
    {
        if ($siteId = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $siteId, "Site Code"]);

            if ($buildingId = Common::iset($filterData['building_id'])) {
                array_push(
                    $whereCodes,
                    ['building', 'building_id', 'building_code', $buildingId, "Building Code"]
                );
            }
        }

        if ($val = Common::iset($filterData['establishment'])) {
            array_push(
                $whereCodes,
                [
                    'establishment',
                    'establishment_id',
                    'establishment_code',
                    $val,
                    "Establishment"
                ]
            );
        }

        if ($val = Common::iset($filterData['site_type'])) {
            array_push($whereCodes, [
                'site_type',
                'site_type_id',
                'site_type_code',
                $val, \Lang::get('text.report_texts.property.site_type_code')
            ]);
        }

        if ($val = Common::iset($filterData['tenure'])) {
            array_push($whereCodes, [
                'prop_tenure',
                'prop_tenure_id',
                'prop_tenure_code',
                $val,
                \Lang::get('text.report_texts.tenure')
            ]);
        }

        if ($val = Common::iset($filterData['usage'])) {
            array_push($whereCodes, [
                'site_usage',
                'site_usage_id',
                'site_usage_code',
                $val,
                \Lang::get('text.report_texts.usage_code')
            ]);
        }

        if ($val = Common::iset($filterData['ward'])) {
            array_push($whereCodes, [
                'ward',
                'ward_id',
                'ward_code',
                $val,
                \Lang::get('text.report_texts.ward_code')
            ]);
        }

        if ($val = Common::iset($filterData['parish'])) {
            array_push($whereCodes, [
                'parish',
                'parish_id',
                'parish_code',
                $val,
                \Lang::get('text.report_texts.parish_code')
            ]);
        }

        if ($val = Common::iset($filterData['committee'])) {
            array_push(
                $whereCodes,
                [
                    'committee',
                    'committee_id',
                    'committee_code',
                    $val,
                    \Config::get('cloud.labels.site_committee') . " Code"
                ]
            );
        }
    }

    private function applyFilter($filter)
    {
        $query = "";

        if (! empty($filter->establishment)) {
            $query .= ' AND site.establishment_id = ' . $filter->establishment;
        }

        if (!empty($filter->site_id)) {
            $query .= ' AND site.site_id = ' . $filter->site_id;
        }

        if (!empty($filter->building_id)) {
            $query .= ' AND building.building_id = ' . $filter->building_id;
        }

        if (!empty($filter->siteType)) {
            $query .= ' AND site.site_type_id = ' . $filter->siteType;
        }

        if (!empty($filter->usage)) {
            $query .= ' AND site.site_usage_id = ' . $filter->usage;
        }

        if (!empty($filter->siteDesignation)) {
            $query .= ' AND site.site_designation_id = ' . $filter->siteDesignation;
        }

        if (!empty($filter->committee)) {
            $query .= ' AND site.committee_id = ' . $filter->committee;
        }

        if (!empty($filter->ward)) {
            $query .= ' AND site.ward_id = ' . $filter->ward;
        }

        if (! empty($filter->parish)) {
            $query .= ' AND site.parish_id = ' . $filter->parish;
        }
        return $query;
    }

    private function getSitesQuery($siteId, $buildingId, $filter)
    {
        $siteQuery = Site::userSiteGroup()
            ->select([
                'site.site_id',
                'site.site_code',
                'site.site_desc',
                'site.asbestos_survey_required as site_asbestos_survey_required',
                'site.include_in_compliance_report',
                'site.active'
            ]);

        if (\Auth::user()->site_access == User::SITE_ACCESS_MODE_SELECTED) {
            $siteQuery->join('user_access', 'user_access.site_id', '=', 'site.site_id')
                ->where('user_access.user_id', \Auth::user()->getKey());
        }

        if ($siteId) {
            $siteQuery->where('site.site_id', $siteId);
        }

        if ($filter) {
            if (! empty($filter->establishment)) {
                $siteQuery->where('site.establishment_id', $filter->establishment);
            }

            if (!empty($filter->site_type)) {
                $siteQuery->where("site.site_type_id", $filter->site_type);
            }

            if (! empty($filter->tenure)) {
                $siteQuery->where('site.prop_tenure_id', $filter->tenure);
            }

            if (!empty($filter->usage)) {
                $siteQuery->where("site.site_usage_id", $filter->usage);
            }

            if (!empty($filter->committee)) {
                $siteQuery->where('committee_id', $filter->committee);
            }

            if (!empty($filter->ward)) {
                $siteQuery->where("site.ward_id", $filter->ward);
            }

            if (! empty($filter->parish)) {
                $siteQuery->where('parish_id', $filter->parish);
            }
        }

        if ($buildingId) {
            $siteQuery->with([
                'asbSurveys' => function ($query) {
                    $this->buildAsbSurveyQuery($query);
                },
                'buildings' => function ($query) use ($buildingId, $filter) {

                    if ($filter) {
                        if (! empty($filter->in_active)) {
                            if ($filter->in_active == 'Y') {
                                $query->where('building.active', 'N');
                            } elseif ($filter->in_active == 'N') {
                                $query->where('building.active', 'Y');
                            }
                        }
                    }

                    $query->where('building_id', $buildingId);
                },
                'buildings.asbSurveys' => function ($query) {
                    $this->buildAsbSurveyQuery($query, true);
                },
            ]);
        } else {
            $siteQuery->with([
                'asbSurveys' => function ($query) {
                    $this->buildAsbSurveyQuery($query);
                },
                'buildings' => function ($query) use ($filter) {

                    if ($filter) {
                        if (! empty($filter->in_active)) {
                            if ($filter->in_active == 'Y') {
                                $query->where('building.active', 'N');
                            } elseif ($filter->in_active == 'N') {
                                $query->where('building.active', 'Y');
                            }
                        }
                    }
                    $query->orderBy('building.building_code');
                },
                'buildings.asbSurveys' => function ($query) {
                    $this->buildAsbSurveyQuery($query, true);
                },
            ]);
        }

        $siteQuery->orderBy('site.site_code');

        return $siteQuery;
    }

    public function convertClassStyleToInlineStyle($className)
    {
        $color = null;
        switch ($className) {
            case self::RAG_COLOR_RED_CSS:
                $color = self::RED_HEX;
                break;
            case self::RAG_COLOR_AMBER_CSS:
                $color = self::AMBER_HEX;
                break;
            case self::RAG_COLOR_GREEN_CSS:
                $color = self::GREEN_HEX;
                break;
            case self::RAG_COLOR_GREY_CSS:
                $color = self::GRAY_HEX;
                break;
            default:
                break;
        }

        return collect([
            'background' =>  '#' . $color,
            'text-align' => 'center'
        ]);
    }
}
