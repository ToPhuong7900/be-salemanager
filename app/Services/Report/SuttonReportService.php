<?php

namespace Tfcloud\Services\Report;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Models\Note;
use Tfcloud\Models\NoteRecordType;
use Tfcloud\Models\Project;
use Tfcloud\Models\ProjectStatusType;
use Tfcloud\Models\ProjectTimesheetStatus;
use Tfcloud\Models\Report;
use Tfcloud\Models\Views\VwPrjCliSut01;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\NotesService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Project\ProjectService;
use ZipArchive;

class SuttonReportService extends BaseService
{
    protected $reportBoxFilterLoader;
    public function __construct(PermissionService $permissionService, Report $report = null)
    {
        $this->permissionService = $permissionService;

        $this->noteService = new NotesService($permissionService);
        $this->projectService = new ProjectService($permissionService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
        if ($report) {
            $report->setRepFilterable(false);
        }
    }

    public function runReport($filterData)
    {
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($filterData);
            $filterData['Billing_To'] = $reportFilterQuery->getBillingToDate();
        }

        $fromDate = Common::dateFieldToSqlFormatWithoutTime($filterData['Billing_To']);

        \Session::put('reportDate', $fromDate);

        $query = \DB::table('project AS pro')->select([
            'pro.site_group_id',
            'pro.project_id',
            'pro.project_code',
            \DB::raw('replace(pro.project_title, ",", " ") AS project_title'),
            'pro.project_code AS Project Code',
            \DB::raw('replace(pro.project_title, ",", " ") AS `Project Title`'),
            'gen_userdef_sel.gen_userdef_sel_code AS Group',
            'pro.client_ref AS Contact',
            'gen_userdef_group.user_text2 AS Charge Code',
            'gen_userdef_group.user_text3 AS Income Code',
            'gen_userdef_group.user_text2 AS charge_code',
            'gen_userdef_group.user_text3 AS income_code',
            \DB::raw('CASE WHEN pro.use_fee_billing = "Y" THEN "Fee" '
            . 'WHEN pro.timesheet_billing = "Y" then "TS" ELSE "" END as `Basis`'),
            'gen_userdef_group.user_text4 AS pc_complete',
            'gen_userdef_group.user_text4 AS % Complete',
            \DB::raw('CASE WHEN pro.timesheet_billing = "Y" then "" ELSE'
                    . ' gen_userdef_group.user_text1 * (pro.fee_percentage / 100) END as `Fee Value`'),
            \DB::raw('CASE WHEN pro.timesheet_billing = "Y" then "" ELSE'
                    . ' gen_userdef_group.user_text1 * (pro.fee_percentage / 100) END as `fee_value`'),
            \DB::raw('CASE WHEN pro.fee_cap > 0
                AND pro.fee_cap < round((user_text1 * (pro.fee_percentage / 100)) * (COALESCE(user_text4, 0) / 100), 2)
                THEN pro.fee_cap
                WHEN pro.fee_percentage > 0
                THEN round((user_text1 * (pro.fee_percentage / 100)) * (COALESCE(user_text4, 0) / 100), 2)
                ELSE
                (select sum(project_timesheet_hours.hours_total *
                project_task_discipline.hourly_rate) from `project_timesheet`
                inner join `project_timesheet_hours` on `project_timesheet_hours`.`project_timesheet_id`
                = `project_timesheet`.`project_timesheet_id`
                inner join `project_package_task` on `project_package_task`.`project_package_task_id`
                = `project_timesheet_hours`.`project_package_task_id`
                inner join `project_task_discipline` on `project_task_discipline`.`project_task_discipline_id`
                = `project_package_task`.`project_task_discipline_id`
                inner join `project_package` on `project_package`.`project_package_id`
                = `project_package_task`.`project_package_id`
                where project_package.project_id = pro.project_id
                AND project_timesheet.start_date <= ' . "'" . $fromDate . "'" . ')
                END as `Cumulative`'),
            \DB::raw('replace(user_text5, ",", " ") AS `Previous`'),
            \DB::raw('CASE WHEN pro.fee_cap > 0'
                    . ' AND pro.fee_cap < round((user_text1 * (pro.fee_percentage / 100))'
                    . ' * (COALESCE(user_text4, 0) / 100), 2) THEN'
                    . ' pro.fee_cap - user_text5'
                    . ' WHEN pro.fee_percentage > 0'
                    . ' THEN truncate(((user_text1 * (pro.fee_percentage / 100))'
                    . ' * (COALESCE(user_text4, 0) / 100)) - user_text5, 2)'
                    . ' ELSE '
                    . '(select sum(project_timesheet_hours.hours_total * project_task_discipline.hourly_rate)'
                    . ' from `project_timesheet` '
                    . 'inner join `project_timesheet_hours` on `project_timesheet_hours`.`project_timesheet_id`'
                    . ' = `project_timesheet`.`project_timesheet_id` '
                    . 'inner join `project_package_task` on `project_package_task`.`project_package_task_id`'
                    . ' = `project_timesheet_hours`.`project_package_task_id` '
                    . 'inner join `project_task_discipline` on'
                    . ' `project_task_discipline`.`project_task_discipline_id` ='
                    . ' `project_package_task`.`project_task_discipline_id` '
                    . 'inner join `project_package` on `project_package`.`project_package_id` ='
                    . ' `project_package_task`.`project_package_id` '
                    . 'where project_package.project_id = pro.project_id '
                    . 'AND project_timesheet.start_date <= ' . "'" . $fromDate . "'" . ') - user_text5 '
                    . 'END as `Charge for Period`'),
            \DB::raw('CASE WHEN pro.fee_cap > 0'
                    . ' AND pro.fee_cap < round((user_text1 * (pro.fee_percentage / 100))'
                    . ' * (COALESCE(user_text4, 0) / 100), 2) THEN'
                    . ' pro.fee_cap - user_text5'
                    . ' WHEN pro.fee_percentage > 0'
                    . ' THEN truncate(((user_text1 * (pro.fee_percentage / 100))'
                    . ' * (COALESCE(user_text4, 0) / 100)) - user_text5, 2)'
                    . ' ELSE '
                    . '(select sum(project_timesheet_hours.hours_total * project_task_discipline.hourly_rate)'
                    . ' from `project_timesheet` '
                    . 'inner join `project_timesheet_hours` on `project_timesheet_hours`.`project_timesheet_id`'
                    . ' = `project_timesheet`.`project_timesheet_id` '
                    . 'inner join `project_package_task` on `project_package_task`.`project_package_task_id`'
                    . ' = `project_timesheet_hours`.`project_package_task_id` '
                    . 'inner join `project_task_discipline` on'
                    . ' `project_task_discipline`.`project_task_discipline_id` ='
                    . ' `project_package_task`.`project_task_discipline_id` '
                    . 'inner join `project_package` on `project_package`.`project_package_id` ='
                    . ' `project_package_task`.`project_package_id` '
                    . 'where project_package.project_id = pro.project_id '
                    . 'AND project_timesheet.start_date <= ' . "'" . $fromDate . "'" . ') - user_text5 '
                    . 'END as `charge_for_period`'),
        ])
        ->leftJoin(
            'gen_userdef_group',
            'pro.gen_userdef_group_id',
            '=',
            'gen_userdef_group.gen_userdef_group_id'
        )
        ->leftJoin(
            'gen_userdef_sel',
            'gen_userdef_sel.gen_userdef_sel_id',
            '=',
            'gen_userdef_group.sel3_gen_userdef_sel_id'
        )
        ->leftJoin(
            'project_package',
            'pro.project_id',
            '=',
            'project_package.project_id'
        )
        ->leftJoin(
            'project_package_task',
            'project_package_task.project_package_id',
            '=',
            'project_package.project_package_id'
        )
        ->leftJoin(
            'project_task_discipline',
            'project_task_discipline.project_task_discipline_id',
            '=',
            'project_package_task.project_task_discipline_id'
        )
        ->leftJoin(
            'project_timesheet_hours',
            'project_timesheet_hours.project_package_task_id',
            '=',
            'project_package_task.project_package_task_id'
        )
        ->leftJoin(
            'project_timesheet',
            'project_timesheet.project_timesheet_id',
            '=',
            'project_timesheet_hours.project_timesheet_id'
        )
        ->leftJoin(
            'project_status',
            'project_status.project_status_id',
            '=',
            'pro.project_status_id'
        )
        ->where([
            ['project_status.project_status_type_id',
            '=',
            ProjectStatusType::ACTIVE],
            ['pro.use_fee_billing',
            '<>',
            'N']
        ])
        ->orWhere([
            ['project_status.project_status_type_id',
            '=',
            ProjectStatusType::ACTIVE],
            ['pro.timesheet_billing',
            '<>',
            'N']
        ])
        ->groupBy(
            'pro.project_code'
        );

        $getResults = $query->get();

        $generatedFilePath = \Config::get('cloud.legacy_paths.generated')
                . '/' . \Auth::User()->site_group_id
                . '/' . \Auth::User()->id;

        $reportDir = storage_path()
                . DIRECTORY_SEPARATOR . 'reports'
                . DIRECTORY_SEPARATOR . \Auth::User()->site_group_id
                . DIRECTORY_SEPARATOR . \Auth::User()->id;

        if (!file_exists($generatedFilePath)) {
            \File::makeDirectory($generatedFilePath, 0775, true);
        }

        $csvFilePath = $reportDir . "/suttonCharges.csv";

        $csvFile = fopen($csvFilePath, 'w');

        fwrite($csvFile, "Billing Charges Report\r\n");
        fwrite($csvFile, "\r\n");
        fwrite($csvFile, "Date,");
        fwrite($csvFile, $filterData['Billing_To'] . ",");
        fwrite($csvFile, "Charges processed up to this date,");
        fwrite($csvFile, "\r\n");
        fwrite($csvFile, "\r\n");
        fwrite($csvFile, "Project Code,");
        fwrite($csvFile, "Project Title,");
        fwrite($csvFile, "Group,");
        fwrite($csvFile, "Contact,");
        fwrite($csvFile, "Charge Code,");
        fwrite($csvFile, "Income Code,");
        fwrite($csvFile, "Funding,");
        fwrite($csvFile, "Basis,");
        fwrite($csvFile, "% Complete,");
        fwrite($csvFile, "Fee Value,");
        fwrite($csvFile, "Cumulative,");
        fwrite($csvFile, "Previous,");
        fwrite($csvFile, "Charge for Period,");
        fwrite($csvFile, "\r\n");

        foreach ($getResults as $q1) {
            if (isset($q1->project_id)) {
                fwrite($csvFile, $q1->project_code . ",");
                fwrite($csvFile, $q1->project_title . ",");
                fwrite($csvFile, $q1->Group . ",");
                fwrite($csvFile, ',');
                fwrite($csvFile, $q1->charge_code . ",");
                fwrite($csvFile, $q1->income_code . ",");
                fwrite($csvFile, '' . ',');
                fwrite($csvFile, $q1->Basis . ",");
                fwrite($csvFile, $q1->pc_complete . ",");
                fwrite($csvFile, $q1->fee_value . ",");
                fwrite($csvFile, $q1->Cumulative . ",");
                fwrite($csvFile, $q1->Previous . ",");
                fwrite($csvFile, $q1->charge_for_period . "\r\n");
            }
        }

        fclose($csvFile);

        return $query;
    }

    public function runProfitAndLossReport($filterData)
    {
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($filterData);
            $filterData['Billing_To'] = $reportFilterQuery->getBillingToDate();
        }

        $fromDate = Common::dateFieldToSqlFormatWithoutTime($filterData['Billing_To']);

        $query = \DB::table('project AS pro')->select([
            'pro.site_group_id',
            'pro.project_id',
            'pro.project_code',
            \DB::raw('replace(pro.project_title, ",", " ") AS project_title'),
            'gen_userdef_sel.gen_userdef_sel_code AS group',
            \DB::raw('CASE WHEN pro.use_fee_billing = "Y" THEN "Fee" '
            . 'WHEN pro.timesheet_billing = "Y" then "TS" ELSE "" END as `basis`'),
            'gen_userdef_group.user_text4 AS pc_complete',
            \DB::raw('CASE WHEN pro.timesheet_billing = "Y" then "" ELSE'
                    . ' gen_userdef_group.user_text1 * (pro.fee_percentage / 100) END as `fee_value`'),
            \DB::raw('CASE WHEN pro.fee_percentage > 0 THEN
                (select sum(total_amount) from project_budget_line
                WHERE project_budget_line.project_id = pro.project_id)
                WHEN pro.fee_cap is not null AND pro.fee_cap < (project_task_discipline.hourly_rate
                * (select sum(project_timesheet_hours.hours_total) from project_timesheet_hours
                left join project_package_task
                on project_package_task.project_package_task_id = project_timesheet_hours.project_package_task_id
                left join project_package
                on project_package_task.project_package_id = project_package.project_package_id
                left join project_timesheet
                on project_timesheet.project_timesheet_id = project_timesheet_hours.project_timesheet_id
                WHERE project_package.project_id = pro.project_id
                AND project_timesheet.start_date <= ' . "'" . $fromDate . "'" . ')) THEN pro.fee_cap ELSE
                (select sum(project_timesheet_hours.hours_total *
                project_task_discipline.hourly_rate) from `project_timesheet`
                inner join `project_timesheet_hours` on `project_timesheet_hours`.`project_timesheet_id`
                = `project_timesheet`.`project_timesheet_id`
                inner join `project_package_task` on `project_package_task`.`project_package_task_id`
                = `project_timesheet_hours`.`project_package_task_id`
                inner join `project_task_discipline` on `project_task_discipline`.`project_task_discipline_id`
                = `project_package_task`.`project_task_discipline_id`
                inner join `project_package` on `project_package`.`project_package_id`
                = `project_package_task`.`project_package_id`
                WHERE project_package.project_id = pro.project_id
                AND project_timesheet.start_date <= ' . "'" . $fromDate . "'" . ')
                END as `fees_to_date`'),
            \DB::raw('(select sum(project_timesheet_hours.hours_total *
                project_task_discipline.hourly_rate) from `project_timesheet`
                inner join `project_timesheet_hours` on `project_timesheet_hours`.`project_timesheet_id`
                = `project_timesheet`.`project_timesheet_id`
                inner join `project_package_task` on `project_package_task`.`project_package_task_id`
                = `project_timesheet_hours`.`project_package_task_id`
                inner join `project_task_discipline` on `project_task_discipline`.`project_task_discipline_id`
                = `project_package_task`.`project_task_discipline_id`
                inner join `project_package` on `project_package`.`project_package_id`
                = `project_package_task`.`project_package_id`
                where project_package.project_id = pro.project_id
                AND project_timesheet.start_date <= ' . "'" . $fromDate . "'" . ')
                as `ts_costs`'),
            \DB::raw('CASE WHEN pro.fee_percentage > 0 THEN
                (select sum(total_amount) - (select sum(project_timesheet_hours.hours_total *
                project_task_discipline.hourly_rate) from `project_timesheet`
                inner join `project_timesheet_hours` on `project_timesheet_hours`.`project_timesheet_id`
                = `project_timesheet`.`project_timesheet_id`
                inner join `project_package_task` on `project_package_task`.`project_package_task_id`
                = `project_timesheet_hours`.`project_package_task_id`
                inner join `project_task_discipline` on `project_task_discipline`.`project_task_discipline_id`
                = `project_package_task`.`project_task_discipline_id`
                inner join `project_package` on `project_package`.`project_package_id`
                = `project_package_task`.`project_package_id`
                WHERE project_package.project_id = pro.project_id
                AND project_timesheet.start_date <= ' . "'" . $fromDate . "'" . ')
                FROM project_budget_line where project_budget_line.project_id = pro.project_id)
                WHEN pro.fee_cap is not null AND pro.fee_cap <
                (select sum(project_timesheet_hours.hours_total *
                project_task_discipline.hourly_rate) from `project_timesheet`
                inner join `project_timesheet_hours` on `project_timesheet_hours`.`project_timesheet_id`
                = `project_timesheet`.`project_timesheet_id`
                inner join `project_package_task` on `project_package_task`.`project_package_task_id`
                = `project_timesheet_hours`.`project_package_task_id`
                inner join `project_task_discipline` on `project_task_discipline`.`project_task_discipline_id`
                = `project_package_task`.`project_task_discipline_id`
                inner join `project_package` on `project_package`.`project_package_id`
                = `project_package_task`.`project_package_id`
                WHERE project_package.project_id = pro.project_id
                AND project_timesheet.start_date <= ' . "'" . $fromDate . "'" . ') THEN
                pro.fee_cap - (select sum(project_timesheet_hours.hours_total *
                project_task_discipline.hourly_rate) from `project_timesheet`
                inner join `project_timesheet_hours` on `project_timesheet_hours`.`project_timesheet_id`
                = `project_timesheet`.`project_timesheet_id`
                inner join `project_package_task` on `project_package_task`.`project_package_task_id`
                = `project_timesheet_hours`.`project_package_task_id`
                inner join `project_task_discipline` on `project_task_discipline`.`project_task_discipline_id`
                = `project_package_task`.`project_task_discipline_id`
                inner join `project_package` on `project_package`.`project_package_id`
                = `project_package_task`.`project_package_id`
                WHERE project_package.project_id = pro.project_id
                AND project_timesheet.start_date <= ' . "'" . $fromDate . "'" . ') ELSE
                "0.00"
                END as `pnl`'),
        ])
        ->leftJoin(
            'gen_userdef_group',
            'pro.gen_userdef_group_id',
            '=',
            'gen_userdef_group.gen_userdef_group_id'
        )
        ->leftJoin(
            'gen_userdef_sel',
            'gen_userdef_sel.gen_userdef_sel_id',
            '=',
            'gen_userdef_group.sel3_gen_userdef_sel_id'
        )
        ->leftJoin(
            'project_package',
            'pro.project_id',
            '=',
            'project_package.project_id'
        )
        ->leftJoin(
            'project_package_task',
            'project_package_task.project_package_id',
            '=',
            'project_package.project_package_id'
        )
        ->leftJoin(
            'project_task_discipline',
            'project_task_discipline.project_task_discipline_id',
            '=',
            'project_package_task.project_task_discipline_id'
        )
        ->leftJoin(
            'project_timesheet_hours',
            'project_timesheet_hours.project_package_task_id',
            '=',
            'project_package_task.project_package_task_id'
        )
        ->leftJoin(
            'project_timesheet',
            'project_timesheet.project_timesheet_id',
            '=',
            'project_timesheet_hours.project_timesheet_id'
        )
        ->leftJoin(
            'project_status',
            'project_status.project_status_id',
            '=',
            'pro.project_status_id'
        )
        ->where([
            ['project_status.project_status_type_id',
            '=',
            ProjectStatusType::ACTIVE],
            ['pro.use_fee_billing',
            '<>',
            'N']
        ])
        ->orWhere([
            ['project_status.project_status_type_id',
            '=',
            ProjectStatusType::ACTIVE],
            ['pro.timesheet_billing',
            '<>',
            'N']
        ])
        ->groupBy(
            'pro.project_code'
        )->get();

        $generatedFilePath = \Config::get('cloud.legacy_paths.generated')
                . '/' . \Auth::User()->site_group_id
                . '/' . \Auth::User()->id;

        $reportDir = storage_path()
                . DIRECTORY_SEPARATOR . 'reports'
                . DIRECTORY_SEPARATOR . \Auth::User()->site_group_id
                . DIRECTORY_SEPARATOR . \Auth::User()->id;

        if (!file_exists($generatedFilePath)) {
            \File::makeDirectory($generatedFilePath, 0775, true);
        }

        $csvFilePath = $reportDir . "/suttonPnL.csv";

        $csvFile = fopen($csvFilePath, 'w');

        fwrite($csvFile, "Project Profit and Loss Summary\r\n");
        fwrite($csvFile, "\r\n");
        fwrite($csvFile, "Date,");
        fwrite($csvFile, $filterData['Billing_To'] . ",");
        fwrite($csvFile, "Charges processed up to this date,");
        fwrite($csvFile, "\r\n");
        fwrite($csvFile, "\r\n");
        fwrite($csvFile, "Project Code,");
        fwrite($csvFile, "Project Title,");
        fwrite($csvFile, "Group,");
        fwrite($csvFile, "Funding,");
        fwrite($csvFile, "Basis,");
        fwrite($csvFile, "% Complete,");
        fwrite($csvFile, "Estimated Fees,");
        fwrite($csvFile, "Fees to Date,");
        fwrite($csvFile, "TS Costs,");
        fwrite($csvFile, "P&L,");
        fwrite($csvFile, "\r\n");

        foreach ($query as $q1) {
            if (isset($q1->project_id)) {
                fwrite($csvFile, $q1->project_code . ",");
                fwrite($csvFile, $q1->project_title . ",");
                fwrite($csvFile, $q1->group . ",");
                fwrite($csvFile, ',');
                fwrite($csvFile, $q1->basis . ",");
                fwrite($csvFile, $q1->pc_complete . ",");
                fwrite($csvFile, '0' . ',');
                fwrite($csvFile, $q1->fees_to_date . ",");
                fwrite($csvFile, $q1->ts_costs . ",");
                fwrite($csvFile, $q1->pnl . "\r\n");
            }
        }

        fclose($csvFile);

        // zip it up
        $zip = new ZipArchive();
        $zipFileName = "billingCharges.zip";
        $zipFilePath = $generatedFilePath . '/' . $zipFileName;

        if (file_exists($zipFilePath)) {
            unlink($zipFilePath);
        }

        $zip->open($zipFilePath, ZipArchive::CREATE);

        if (file_exists($csvFilePath)) {
            $zip->addFile($csvFilePath, "suttonPnL.csv");
        }

        $zip->close();

        return $query;
    }

    //Add a note with this period's calculations
    public function addNote()
    {
        $valid = true;

        $fromDate = \Session::get('reportDate');

        $activeProjects = \DB::table('project AS pro')->select([
            'pro.site_group_id',
            'pro.project_id',
            'pro.project_code AS Project Code',
            \DB::raw('replace(pro.project_title, ",", " ") AS project_title'),
            'gen_userdef_sel.gen_userdef_sel_code AS Group',
            'pro.client_ref AS Contact',
            'gen_userdef_group.user_text2 AS Charge Code',
            'gen_userdef_group.user_text3 AS Income Code',
            \DB::raw('CASE WHEN pro.use_fee_billing = "Y" THEN "Fee" '
            . 'WHEN pro.timesheet_billing = "Y" then "TS" ELSE "" END as `Basis`'),
            'gen_userdef_group.user_text4 AS % Complete',
            \DB::raw('CASE WHEN pro.timesheet_billing = "Y" then "" ELSE'
                    . ' gen_userdef_group.user_text1 * (pro.fee_percentage / 100) END as `Fee Value`'),
            \DB::raw('CASE WHEN pro.timesheet_billing = "Y" then "" ELSE'
                    . ' gen_userdef_group.user_text1 * (pro.fee_percentage / 100) END as `fee_value`'),
            \DB::raw('CASE WHEN pro.fee_cap > 0
                AND pro.fee_cap < round((user_text1 * (pro.fee_percentage / 100)) * (COALESCE(user_text4, 0) / 100), 2)
                THEN pro.fee_cap
                WHEN pro.fee_percentage > 0
                THEN round((user_text1 * (pro.fee_percentage / 100)) * (COALESCE(user_text4, 0) / 100), 2)
                ELSE
                (select sum(project_timesheet_hours.hours_total *
                project_task_discipline.hourly_rate) from `project_timesheet`
                inner join `project_timesheet_hours` on `project_timesheet_hours`.`project_timesheet_id`
                = `project_timesheet`.`project_timesheet_id`
                inner join `project_package_task` on `project_package_task`.`project_package_task_id`
                = `project_timesheet_hours`.`project_package_task_id`
                inner join `project_task_discipline` on `project_task_discipline`.`project_task_discipline_id`
                = `project_package_task`.`project_task_discipline_id`
                inner join `project_package` on `project_package`.`project_package_id`
                = `project_package_task`.`project_package_id`
                where project_package.project_id = pro.project_id
                AND project_timesheet.start_date <= ' . "'" . $fromDate . "'" . ')
                END as `cumulative`'),
            \DB::raw('replace(user_text5, ",", " ") AS `Previous`'),
            \DB::raw('CASE WHEN pro.fee_cap > 0'
                    . ' AND pro.fee_cap < round((user_text1 * (pro.fee_percentage / 100))'
                    . ' * (COALESCE(user_text4, 0) / 100), 2) THEN'
                    . ' pro.fee_cap - user_text5'
                    . ' WHEN pro.fee_percentage > 0'
                    . ' THEN truncate(((user_text1 * (pro.fee_percentage / 100))'
                    . ' * (COALESCE(user_text4, 0) / 100)) - user_text5, 2)'
                    . ' ELSE '
                    . '(select sum(project_timesheet_hours.hours_total * project_task_discipline.hourly_rate)'
                    . ' from `project_timesheet` '
                    . 'inner join `project_timesheet_hours` on `project_timesheet_hours`.`project_timesheet_id` ='
                    . ' `project_timesheet`.`project_timesheet_id` '
                    . 'inner join `project_package_task` on `project_package_task`.`project_package_task_id` ='
                    . ' `project_timesheet_hours`.`project_package_task_id` '
                    . 'inner join `project_task_discipline` on'
                    . ' `project_task_discipline`.`project_task_discipline_id` ='
                    . ' `project_package_task`.`project_task_discipline_id` '
                    . 'inner join `project_package` on `project_package`.`project_package_id` ='
                    . ' `project_package_task`.`project_package_id` '
                    . 'where project_package.project_id = pro.project_id '
                    . 'AND project_timesheet.start_date <= ' . "'" . $fromDate . "'" . ') - user_text5 '
                    . 'END as `Charge for Period`'),
            \DB::raw('CASE WHEN pro.fee_cap > 0'
                    . ' AND pro.fee_cap < round((user_text1 * (pro.fee_percentage / 100))'
                    . ' * (COALESCE(user_text4, 0) / 100), 2) THEN'
                    . ' pro.fee_cap - user_text5'
                    . ' WHEN pro.fee_percentage > 0'
                    . ' THEN truncate(((user_text1 * (pro.fee_percentage / 100))'
                    . ' * (COALESCE(user_text4, 0) / 100)) - user_text5, 2)'
                    . ' ELSE '
                    . '(select sum(project_timesheet_hours.hours_total * project_task_discipline.hourly_rate)'
                    . ' from `project_timesheet` '
                    . 'inner join `project_timesheet_hours` on `project_timesheet_hours`.`project_timesheet_id` ='
                    . ' `project_timesheet`.`project_timesheet_id` '
                    . 'inner join `project_package_task` on `project_package_task`.`project_package_task_id` ='
                    . ' `project_timesheet_hours`.`project_package_task_id` '
                    . 'inner join `project_task_discipline` on'
                    . ' `project_task_discipline`.`project_task_discipline_id` ='
                    . ' `project_package_task`.`project_task_discipline_id` '
                    . 'inner join `project_package` on `project_package`.`project_package_id` ='
                    . ' `project_package_task`.`project_package_id` '
                    . 'where project_package.project_id = pro.project_id '
                    . 'AND project_timesheet.start_date <= ' . "'" . $fromDate . "'" . ') - user_text5 '
                    . 'END as `charge_for_period`'),
        ])
        ->leftJoin(
            'gen_userdef_group',
            'pro.gen_userdef_group_id',
            '=',
            'gen_userdef_group.gen_userdef_group_id'
        )
        ->leftJoin(
            'gen_userdef_sel',
            'gen_userdef_sel.gen_userdef_sel_id',
            '=',
            'gen_userdef_group.sel3_gen_userdef_sel_id'
        )
        ->leftJoin(
            'project_package',
            'pro.project_id',
            '=',
            'project_package.project_id'
        )
        ->leftJoin(
            'project_package_task',
            'project_package_task.project_package_id',
            '=',
            'project_package.project_package_id'
        )
        ->leftJoin(
            'project_task_discipline',
            'project_task_discipline.project_task_discipline_id',
            '=',
            'project_package_task.project_task_discipline_id'
        )
        ->leftJoin(
            'project_timesheet_hours',
            'project_timesheet_hours.project_package_task_id',
            '=',
            'project_package_task.project_package_task_id'
        )
        ->leftJoin(
            'project_timesheet',
            'project_timesheet.project_timesheet_id',
            '=',
            'project_timesheet_hours.project_timesheet_id'
        )
        ->leftJoin(
            'project_status',
            'project_status.project_status_id',
            '=',
            'pro.project_status_id'
        )
        ->where([
            ['project_status.project_status_type_id',
            '=',
            ProjectStatusType::ACTIVE],
            ['pro.use_fee_billing',
            '<>',
            'N']
        ])
        ->orWhere([
            ['project_status.project_status_type_id',
            '=',
            ProjectStatusType::ACTIVE],
            ['pro.timesheet_billing',
            '<>',
            'N']
        ])
        ->groupBy(
            'pro.project_code'
        )->get();

        $noteRecordType = NoteRecordType::PROJECT_NOTES;

        foreach ($activeProjects as $activeProject) {
            if (isset($activeProject->project_id) && $activeProject->cumulative != '') {
                $text = 'Actual fees committed for period up to ' . \Session::get('reportDate') .
                    ' Charges for this period = ' . $activeProject->charge_for_period .
                    ' New Actual Fees Total = ' . $activeProject->cumulative;

                $note = [
                    'note_type_id' => 0,
                    'detail' => $text,
                    'private' => 'N'
                ];

                $currentProject = Project::where('project_id', '=', $activeProject->project_id)->first();

                $validator = $this->noteService->storeNote($note, $currentProject, $noteRecordType);

                $id = $currentProject->project_id;

                $data = [
                    'user_text5' => $activeProject->cumulative
                ];

                if ($validator->success) {
                    $validator = $this->projectService->updateProject($id, $data);
                } else {
                    $valid = false;
                }
            }
        }

        return $valid;
    }

    public function createExportDataFile()
    {
        $generatedFilePath = \Config::get('cloud.legacy_paths.generated')
                . '/' . \Auth::User()->site_group_id
                . '/' . \Auth::User()->id;

        $reportDir = storage_path()
                . DIRECTORY_SEPARATOR . 'reports'
                . DIRECTORY_SEPARATOR . \Auth::User()->site_group_id
                . DIRECTORY_SEPARATOR . \Auth::User()->id;

        // zip it up
        $zip = new ZipArchive();
        $zipFileName = "billingCharges.zip";
        $zipFilePath = $generatedFilePath . '/' . $zipFileName;

        if (file_exists($zipFilePath)) {
            unlink($zipFilePath);
        }

        $zip->open($zipFilePath, ZipArchive::CREATE);

        $csvFilePath = $reportDir . "/suttonCharges.csv";
        $csvProfitFilePath = $reportDir . "/suttonPnL.csv";

        if (file_exists($csvFilePath)) {
            $zip->addFile($csvFilePath, "suttonCharges.csv");
        }

        if (file_exists($csvProfitFilePath)) {
            $zip->addFile($csvProfitFilePath, "suttonPnL.csv");
        }

        $zip->close();

        return $zipFilePath;
    }
}
