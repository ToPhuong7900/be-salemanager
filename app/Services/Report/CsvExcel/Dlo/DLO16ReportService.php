<?php

namespace Tfcloud\Services\Report\CsvExcel\Dlo;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Models\Personnel\PersDayOff;
use Tfcloud\Models\Personnel\PersPublicHoliday;
use Tfcloud\Models\Personnel\PersReasonType;
use Tfcloud\Models\Personnel\PersWorkPatternDetail;
use Tfcloud\Models\Report;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\CsvExcel\CsvExcelReportBaseService;
use App\SBoxFilter\DLO\Report\DLO16FilterQuery;

class DLO16ReportService extends CsvExcelReportBaseService
{
    private $reportBoxFilterLoader;

    public function __construct(PermissionService $permissionService, Report $report)
    {
        $this->permissionService = $permissionService;
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function getReportData($inputs, &$filterText)
    {
        $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
        $records = $reportFilterQuery->filterAll(0)->get();
        $filterText = $reportFilterQuery->getFilterDetailText();
        $hourTypes = $reportFilterQuery->getHourTypes();

        $includeExtraHours = false;
        $extraData = [];
        if (
            $reportFilterQuery->getFirstValueFilterField('include_availability')
            ===
            CommonConstant::DATABASE_VALUE_YES
        ) {
            $includeExtraHours = true;
            $extraData = $this->getExtraReportData($reportFilterQuery);
        }
        return $this->createOutputData($records, $hourTypes, $includeExtraHours, $extraData);
    }

    public function createOutputData($records, $hourTypes, $includeExtraHours, $extraData = [])
    {
        $result = [];
        foreach ($records as $record) {
            $line['Operative']                      = $record->operative;
            $line['Standard Hours']                 = $record->standard_hours;
            $line["Over Time $hourTypes[0] Hours"]  = $record->overtime_0;
            $line["Over Time $hourTypes[1] Hours"]  = $record->overtime_1;
            $line["Over Time $hourTypes[2] Hours"]  = $record->overtime_2;
            $line['Total Hours']                    = $record->total_hours;

            if ($includeExtraHours) {
                $userExtraData = array_get($extraData, $record->user_id, ['','','','','']);
                list(
                    $line['Holiday Days'],
                    $line['Sick Days'],
                    $line['Miscellaneous Days'],
                    $line['Timed Absence Hours'],
                    $line['Work Pattern Available Hours']
                    ) =  $userExtraData;
            }

            $result[] = $line;
        }

        return $result;
    }

    public function getExtraReportData(DLO16FilterQuery $reportFilterQuery)
    {
        $userWP = $this->getUserWPQuery($reportFilterQuery)->get();

        $users = $userWP->groupBy('id');

        $userIds = $userWP->pluck('id')->unique();

        $wpIds = $userWP->pluck('pers_work_pattern_id')->unique();

        $persDayOf = $this->getDayOffQuery($reportFilterQuery, $userIds)->get();

        $workPatternList = PersWorkPatternDetail::ofWorkPattern($wpIds)->get()->groupBy('pers_work_pattern_id');

        $workPatternHours = $this->calculateWPDetailHour($workPatternList);

        $persPublicHoliday = PersPublicHoliday::userSiteGroup()->first();

        $persHolidayDates = [];

        if ($persPublicHoliday) {
            $persHolidayDates = array_map(function ($date) {
                return Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
            }, explode(',', $persPublicHoliday->pers_holiday_dates));
        }

        $data = $this->combineData($users, $persDayOf, $workPatternHours, $persHolidayDates, $reportFilterQuery);

        return $data;
    }

    private function getDayOffQuery(DLO16FilterQuery $reportFilterQuery, $userIds)
    {
        $dayOffQuery = PersDayOff::join(
            'pers_reason',
            'pers_reason.pers_reason_id',
            '=',
            'pers_day_off.pers_reason_id'
        )
            ->whereIn('pers_day_off.user_id', $userIds)
            ->select([
                'pers_day_off.*',
                'pers_reason.pers_reason_type_id',
            ]);

        $startDateFilter = $reportFilterQuery->getValueFilterField('start_date');

        if (!empty($startDateFilter)) {
            $startDateRange = array_first($startDateFilter);
            list($from, $to) = $this->formatDateRange($startDateRange);
            $dayOffQuery->whereBetween('date', [$from, $to]);
        } else {
            $dayOffQuery->where('date', '<=', Date('Y-m-d'));
        }

        return $dayOffQuery;
    }

    private function calculateWPDetailHour($wpDetails)
    {
        $wpHours = [];
        foreach ($wpDetails as $wpId => $wpDetail) {
            $minuteTotal = 0;
            foreach ($wpDetail->groupBy('day_of_week') as $dayOfWeek => $days) {
                $minuteDayOfWeek = 0;
                foreach ($days as $record) {
                    if ($record['from'] && $record['to']) {
                        $fromTime = Carbon::createFromTimeString($record['from']);
                        $toTime = Carbon::createFromTimeString($record['to']);
                        $minuteTotal += $minuteDayOfWeek += $toTime->diffInMinutes($fromTime);
                    }
                }
                $wpHours[$wpId][$dayOfWeek] = $this->convertMinuteToHour($minuteDayOfWeek);
            }
            $wpHours[$wpId]['total'] = $this->convertMinuteToHour($minuteTotal);
        }
        return $wpHours;
    }

    private function combineData(
        Collection $users,
        Collection $persDayOff,
        $workPatternHours,
        $persHolidayDates,
        DLO16FilterQuery $reportFilterQuery
    ) {
        $data = [];

        $startDateFilter = $reportFilterQuery->getValueFilterField('start_date');

        if (!empty($startDateFilter)) {
            $startDateRange = array_first($startDateFilter);
            list($from, $to) = $this->formatDateRange($startDateRange);
        } else {
            $from = Carbon::createFromTimestamp(0)->format('Y-m-d');
            $to = Carbon::now()->format('Y-m-d');
        }

        foreach ($users as $userId => $wPs) {
            $wPs = $this->changeWPDate($wPs->sortBy('start_date'), $to);
            $wPs = $this->changeWPDateWithFilterDate($wPs, $from, $to);

            $totalHour = 0;
            $holidayHour = 0;
            $countHolidayOffHour = 0;
            $countSickDayHour = 0;
            $countMiscellaneousHour = 0;
            $totalTimedDayOffHour = 0;
            if ($wPs->count() > 0) {
                foreach ($wPs as $wp) {
                    // calculate holidayOff
                    $holidayDayOff = $this->getDayOffByReasonType(
                        $persDayOff,
                        $userId,
                        PersReasonType::HOLIDAY,
                        $wp->start_date,
                        $wp->end_date
                    );

                    $sickDayOff = $this->getDayOffByReasonType(
                        $persDayOff,
                        $userId,
                        PersReasonType::SICK,
                        $wp->start_date,
                        $wp->end_date
                    );

                    $miscellaneousDayOff = $this->getDayOffByReasonType(
                        $persDayOff,
                        $userId,
                        PersReasonType::MISCELLANEOUS,
                        $wp->start_date,
                        $wp->end_date
                    );

                    $timedDayOff = $this->getTimeDayOff($persDayOff, $userId, $wp->start_date, $wp->end_date);

                    $countHolidayOffHour += $this->countDayOff($holidayDayOff);

                    $countSickDayHour += $this->countDayOff($sickDayOff);

                    $countMiscellaneousHour += $this->countDayOff($miscellaneousDayOff);

                    $totalTimedDayOffHour += $this->calculateTime(
                        $timedDayOff
                    );

                    // calculate all time
                    $weeklyDayNumbers = Common::getWeeklyDayNumbers($wp->start_date, $wp->end_date);
                    foreach ($weeklyDayNumbers as $key => $day) {
                        $totalHour += $day * $workPatternHours[$wp->pers_work_pattern_id][$key];
                    }

                    if ($wp->works_bank_holidays == CommonConstant::DATABASE_VALUE_NO) {
                        // calculate holiday time
                        foreach ($persHolidayDates as $holiday) {
                            if ($wp->start_date <= $holiday && $holiday <= $wp->end_date) {
                                $dayOfWeek = Carbon::createFromFormat('Y-m-d', $holiday)->dayOfWeek;
                                $holidayHour += $workPatternHours[$wp->pers_work_pattern_id][$dayOfWeek];
                            }
                        }
                    }
                }
            }

            $data[$userId] = [
                $countHolidayOffHour,
                $countSickDayHour,
                $countMiscellaneousHour,
                $totalTimedDayOffHour,
                $totalHour - $holidayHour
            ];
        }

        return $data;
    }

    private function getUserWPQuery(DLO16FilterQuery $reportFilterQuery)
    {
        $query = $reportFilterQuery->getOperativeQuery(true);
        $query->select(
            [
                'user.id',
                'pers_user_work_pattern.pers_work_pattern_id',
                'start_date',
                'end_date',
                'works_bank_holidays'
            ]
        );
        $query->join(
            'pers_user_work_pattern',
            'pers_user_work_pattern.user_id',
            '=',
            'user.id'
        );
        $query->join(
            'pers_work_pattern',
            'pers_work_pattern.pers_work_pattern_id',
            '=',
            'pers_user_work_pattern.pers_work_pattern_id'
        );
        $query->orderBy('start_date');

        $startDateFilter = $reportFilterQuery->getValueFilterField('start_date');

        if (!empty($startDateFilter)) {
            $startDateRange = array_first($startDateFilter);
            list($from, $to) = $this->formatDateRange($startDateRange);

            $query->where(function ($query) use ($from, $to) {
                $query->where(function ($query2) use ($from, $to) {
                    $query2->where('pers_user_work_pattern.start_date', '<=', $from)
                        ->where(function ($query3) use ($to) {
                            $query3->where('pers_user_work_pattern.end_date', '>=', $to)
                                ->orWhereNull('pers_user_work_pattern.end_date');
                        });
                });
                $query->orWhere(function ($query4) use ($from, $to) {
                    $query4->where('pers_user_work_pattern.start_date', '>', $from)
                        ->where('pers_user_work_pattern.start_date', '<=', $to);
                });
                $query->orWhere(function ($query5) use ($from, $to) {
                    $query5->where('pers_user_work_pattern.end_date', '>=', $from)
                        ->where('pers_user_work_pattern.end_date', '<', $to);
                });
            });
        }
        return $query;
    }

    private function calculateTime($dayOffs)
    {
        $totalTime = 0;
        foreach ($dayOffs as $dayOff) {
            $startTime = Carbon::createFromTimeString($dayOff->start_time);
            $endTime = Carbon::createFromTimeString($dayOff->end_time);
            $totalTime += $this->convertMinuteToHour($startTime->diffInMinutes($endTime));
        }
        return $totalTime;
    }

    private function countDayOff($dayOffs)
    {
        $halfDayOff = $dayOffs->whereIn('time_type', [PersDayOff::TIME_TYPE_MORNING,
            PersDayOff::TIME_TYPE_AFTERNOON])->count();
        $fullDayOff = $dayOffs->whereIn('time_type', [PersDayOff::TIME_TYPE_ALL_DAY])->count();
        return $fullDayOff + ($halfDayOff / 2);
    }

    private function convertMinuteToHour($minute): float
    {
        return round($minute / 60, 2);
    }

    private function getDayOffByReasonType(
        Collection $persDayOff,
        $userId,
        $reasonType,
        $startDate,
        $endDate
    ): Collection {
        return $persDayOff->where('user_id', $userId)
            ->where('pers_reason_type_id', $reasonType)
            ->where('time_type', '<>', PersDayOff::TIME_TYPE_TIME)
            ->whereBetween('date', [$startDate, $endDate]);
    }

    private function getTimeDayOff(
        Collection $persDayOff,
        $userId,
        $startDate,
        $endDate
    ): Collection {
        return $persDayOff->where('user_id', $userId)
            ->where('time_type', PersDayOff::TIME_TYPE_TIME)
            ->whereBetween('date', [$startDate, $endDate]);
    }

    private function formatDateRange($startDateRange)
    {
        list($from, $to) = $startDateRange;

        if (!$from) {
            $from = Carbon::createFromTimestamp(0)->format('Y-m-d');
        } elseif (!$to) {
            if (Carbon::createFromFormat('Y-m-d', $from) > Carbon::now()) {
                $to = $from;
            } else {
                $to = Carbon::now()->format('Y-m-d');
            }
        }

        return [
            $from, $to
        ];
    }

    private function changeWPDate($wps, $toDate)
    {
        $subStartDate = false;
        foreach ($wps as $key => $wp) {
            if ($subStartDate) {
                $wp->start_date = Carbon::createFromFormat('Y-m-d', $wp->start_date)->addDay()->format('Y-m-d');
            }
            if ($key < $wps->count() - 1) {
                $subStartDate = $wp->end_date == $wps[$key + 1]->start_date;
            } else {
                $subStartDate = false;
            }

            if (!$subStartDate) {
                if ($wp->end_date) {
                    $wp->end_date = Carbon::createFromFormat('Y-m-d', $wp->end_date)->subDay()->format('Y-m-d');
                } else {
                    $wp->end_date = $toDate;
                }
            }
        }
        return $wps;
    }

    private function changeWPDateWithFilterDate($wps, $fromDate, $toDate)
    {
        foreach ($wps as $key => $wp) {
            $outStartDate = false;
            $outEndDate = false;
            if ($wp->start_date <= $fromDate && $wp->end_date >= $fromDate) {
                $wp->start_date = $fromDate;
            } elseif (! ($wp->start_date > $fromDate && $wp->end_date < $toDate)) {
                $outStartDate = true;
            }

            if ($wp->start_date <= $toDate && $wp->end_date >= $toDate) {
                $wp->end_date = $toDate;
            } elseif (! ($wp->start_date > $fromDate && $wp->end_date < $toDate)) {
                $outEndDate = true;
            }

            if ($outStartDate && $outEndDate) {
                unset($wps[$key]);
            }
        }
        return $wps;
    }
}
