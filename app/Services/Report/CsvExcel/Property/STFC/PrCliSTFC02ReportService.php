<?php

namespace Tfcloud\Services\Report\CsvExcel\Property\STFC;

use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Math;
use Tfcloud\Models\Report;
use Tfcloud\Services\PermissionService;

class PrCliSTFC02ReportService extends StfcPropertyOverallCostsReportService
{
    protected $reportBoxFilterLoader;

    public function __construct(
        PermissionService $permissionService,
        Report $report
    ) {
        parent::__construct($permissionService);
        $this->reportBoxFilterLoader = new ClassMapReportLoader($report);
    }

    protected function createOutputData($propertyRows, $finYears)
    {
        $data = [];

        foreach ($finYears as $finYear) {
            foreach ($propertyRows as $propertyRow) {
                $instructionActuals = $this->getInstructionActuals($propertyRow, $finYear);
                $inspectionsActuals = $this->getInspectionsActuals($propertyRow, $finYear);
                $dloActuals = $this->getDloActuals($propertyRow, $finYear);
                $total = Math::addCurrency([$instructionActuals, $inspectionsActuals, $dloActuals]);

                $line = [];
                $line['Financial Year Code']        = $finYear->fin_year_code;
                $line['Financial Year Description'] = $finYear->fin_year_desc;
                $line['Site Code']                  = $propertyRow->site_code;
                $line['Site Description']           = $propertyRow->site_desc;
                $line['Instruction Actuals (£)']    = $instructionActuals;
                $line['Inspections Actuals (£)']    = $inspectionsActuals;
                $line['DLO Actuals (£)']            = $dloActuals;
                $line['Site Total (£)']             = $total;

                $data[] = $line;
            }
        }

        return $data;
    }

    protected function linkToProperty($query, $table, $propertyRow)
    {
        return $query->where(
            "{$table}.site_id",
            '=',
            $propertyRow->site_id
        );
    }

    protected function getPrefixTables()
    {
        return [
            'site' => '*'
        ];
    }
}
