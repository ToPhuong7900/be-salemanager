<?php

namespace Tfcloud\Services\Report\CsvExcel\Property\STFC;

use Carbon\Carbon;
use Tfcloud\Lib\Math;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Models\Contract;
use Tfcloud\Models\DloJob;
use Tfcloud\Models\DloJobStatusType;
use Tfcloud\Models\FinYear;
use Tfcloud\Models\Instruction;
use Tfcloud\Models\InstructionPOType;
use Tfcloud\Models\InstructionParentType;
use Tfcloud\Models\InstructionType;
use Tfcloud\Models\InstructionStatusType;
use Tfcloud\Models\InvoiceStatusType;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\CsvExcel\CsvExcelReportBaseService;

abstract class StfcPropertyOverallCostsReportService extends CsvExcelReportBaseService
{
    protected $startDate;
    protected $endDate;
    protected $finYearIds;

    protected $permissionService = null;

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        $this->setFilterProperties($inputs);

        $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);

        $reportFilterQuery->getOperatorParser()->setPrefixTables($this->getPrefixTables());

        $propertyRows = $reportFilterQuery->getAll(0);

        $filterText = $reportFilterQuery->getFilterDetailText();

        $finYears = $this->getFinYears();

        return $this->createOutputData($propertyRows, $finYears);
    }

    protected function setFilterProperties($inputs)
    {
        $this->startDate = null;
        $this->endDate = null;
        $this->finYearIds = [];

        $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);

        $date = $reportFilterQuery->getValueFilterField('date');
        if (!empty($date)) {
            list($this->startDate, $this->endDate) =  $date[0];
        }

        $finYearId = $reportFilterQuery->getValueFilterField('fin_year_id');
        if (!empty($finYearId)) {
            $this->finYearIds = $finYearId;
        }
    }

    protected function getFinYears($currentYearOnly = false)
    {
        $todayDate = Carbon::createMidnightDate();
        $today = $todayDate->toDateString();

        $query = FinYear::userSiteGroup()
            ->orderBy('fin_year_code')
            ->select([
                'fin_year_id',
                'fin_year_code',
                'fin_year_desc',
                'year_start_date',
                'year_end_date',
            ]);

        if ($this->finYearIds) {
            $query->whereIn(
                'fin_year_id',
                $this->finYearIds
            );
        }

        if ($currentYearOnly) {
            $query->where(
                'year_start_date',
                '<=',
                $today
            )
            ->where(
                'year_end_date',
                '>=',
                $today
            );
        }

        $finYears = $query->get();

        foreach ($finYears as $finYear) {
            if ($today < $finYear->year_start_date) {
                $finYear->months = 0;
            } elseif ($today > $finYear->year_end_date) {
                $finYear->months = 12;
            } else {
                $startDate = Carbon::create($finYear->year_start_date);
                $finYear->months = $todayDate->diffInMonths($startDate) + 1;
            }
        }

        return $finYears;
    }

    protected function getInstructionActuals($propertyRow, $finYear)
    {
        $query = Instruction::userSiteGroup()
            ->join(
                'invoice_fin_account',
                'invoice_fin_account.instruction_id',
                '=',
                'instruction.instruction_id'
            )
            ->join(
                'invoice',
                'invoice.invoice_id',
                '=',
                'invoice_fin_account.invoice_id'
            )
            ->join(
                'invoice_status',
                'invoice_status.invoice_status_id',
                '=',
                'invoice.invoice_status_id',
            )
            ->where(
                'instruction.parent_type_id',
                '<>',
                InstructionParentType::CONTRACT
            )
            ->whereRaw(
                'NOT ('
                . ' instruction.parent_type_id = ' . InstructionParentType::DLO_JOB
                . ' AND instruction.instruction_type_id = ' . InstructionType::PURCHASE_ORDER
                . ' AND instruction.instruction_po_type_id = ' . InstructionPOType::MATERIAL
                . ')'
            )
            ->whereIn(
                'invoice_status.invoice_status_type_id',
                [
                    InvoiceStatusType::APPROVED,
                    InvoiceStatusType::POSTED,
                ]
            )
            ->where(
                'invoice.fin_year_id',
                '=',
                $finYear->fin_year_id
            );

        if ($this->startDate) {
            $query->where(
                'invoice.last_changed_to_approved_date',
                '>=',
                $this->startDate
            );
        }

        if ($this->endDate) {
            $query->where(
                'invoice.last_changed_to_approved_date',
                '<=',
                $this->endDate
            );
        }

        $query = $this->linkToProperty($query, 'instruction', $propertyRow);

        return $query->sum(
            'act_net_total'
        );
    }

    protected function getInspectionsActuals($propertyRow, $finYear)
    {
        $query = Contract::userSiteGroup()
            ->join(
                'instruction',
                'instruction.parent_id',
                '=',
                'contract.contract_id'
            )
            ->join(
                'gen_userdef_group',
                'gen_userdef_group.gen_userdef_group_id',
                '=',
                'contract.gen_userdef_group_id'
            )
            ->where(
                'contract.archive',
                '=',
                CommonConstant::DATABASE_VALUE_NO
            )
            ->where(
                'instruction.parent_type_id',
                '=',
                InstructionParentType::CONTRACT
            )
            ->whereRaw(
                "gen_userdef_group.user_text1 REGEXP '^[0-9]+$'"  // No of months in contract
            )
            ->select(
                'contract.contract_code',
                \DB::raw(
                    "SUM(instruction.est_net_total) "
                    . "/ gen_userdef_group.user_text1 "
                    . "* {$finYear->months} as value"
                ),
            )
            ->groupBy(
                'contract.contract_code',
            );

        $query = $this->linkToProperty($query, 'instruction', $propertyRow);

        $values = $query->pluck('value')->toArray();

        return $this->sumArrayCurrency($values);
    }

    protected function getDloActuals($propertyRow, $finYear)
    {
        $query = DloJob::userSiteGroup()
            ->select(
                [
                    \DB::raw(
                        '(SELECT COALESCE(SUM(labour_total),0)'
                        . ' FROM dlo_job_labour'
                        . ' WHERE dlo_job_labour.dlo_job_id = dlo_job.dlo_job_id'
                        . ') AS labour'
                    ),
                    \DB::raw(
                        '(SELECT COALESCE(SUM(cost),0)'
                        . ' FROM dlo_job_material'
                        . ' WHERE dlo_job_material.dlo_job_id = dlo_job.dlo_job_id'
                        . ') AS material_item'
                    ),
                    \DB::raw(
                        '(SELECT COALESCE(SUM(est_net_total),0)'
                        . ' FROM instruction'
                        . ' JOIN instruction_status '
                        . '   ON instruction_status.instruction_status_id = instruction.instruction_status_id'
                        . ' WHERE instruction.parent_id = dlo_job.dlo_job_id'
                        . ' AND instruction.parent_type_id = ' . InstructionParentType::DLO_JOB
                        . ' AND instruction.instruction_type_id = ' . InstructionType::PURCHASE_ORDER
                        . ' AND instruction.instruction_po_type_id = ' . InstructionPOType::MATERIAL
                        . ' AND instruction_status.instruction_status_type_id <> ' . InstructionStatusType::CANCELLED
                        . ') AS material_order'
                    ),
                ]
            )
            ->join(
                'dlo_job_status',
                'dlo_job_status.dlo_job_status_id',
                '=',
                'dlo_job.dlo_job_status_id',
            )
            ->whereIn(
                'dlo_job_status.dlo_job_status_type_id',
                [
                    DloJobStatusType::WORKS_COMP,
                    DloJobStatusType::WORKS_EXP_COMP,
                ]
            )
            ->where(
                'dlo_job.actual_complete_date',
                '>=',
                $finYear->year_start_date
            )
            ->where(
                'dlo_job.actual_complete_date',
                '<=',
                $finYear->year_end_date
            );

        if ($this->startDate) {
            $query->where(
                'dlo_job.actual_complete_date',
                '>=',
                $this->startDate
            );
        }

        if ($this->endDate) {
            $query->where(
                'dlo_job.actual_complete_date',
                '<=',
                $this->endDate
            );
        }

        $query = $this->linkToProperty($query, 'dlo_job', $propertyRow);

        $rows = $query->get();

        $total = 0;

        foreach ($rows as $row) {
            $total += Math::addCurrency([
                $row->labour,
                $row->material_item,
                $row->material_order,
            ]);
        }

        return $total;
    }

    protected function sumArrayCurrency(array $values)
    {
        switch (count($values)) {
            case 0:
                return 0;
            case 1:
                $values [] = 0;
                return Math::addCurrency($values);
            default:
                return Math::addCurrency($values);
        }
    }

    abstract protected function createOutputData($propertyRows, $finYears);

    abstract protected function linkToProperty($query, $table, $propertyRow);

    abstract protected function getPrefixTables();
}
