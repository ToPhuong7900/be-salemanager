<?php

/**
 * Disposals
 */

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset;

use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Math;
use Tfcloud\Models\Report;
use Tfcloud\Models\SiteGroup;
use Tfcloud\Models\Views\VwFar14;
use Tfcloud\Services\PermissionService;

class Far25ReportService extends JournalPostingService
{
    protected $entity = false;

    private $reportBoxFilterLoader = null;

    public function __construct(PermissionService $permissionService, $filterData, Report $report = null)
    {
        parent::__construct($permissionService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;

        $this->setFilterProperties($filterData);
    }

    private function setFilterProperties($filterData)
    {
        $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($filterData);
        $entity = $reportFilterQuery->getFirstValueFilterField('ca_entity_id');

        // get filter values
        $this->entity = (!$entity) ? false : $entity;
    }

    public function getReportData(&$filterText, &$sOrderText)
    {
        // If no admin ca_posting_account record available, exit.
        if (!$this->postingAcc) {
            return [];
        }

        $far14 = $this->getData();

        $outputData = [];
        $totalRR_HRA = 0;
        $totalRR_GF = 0;

        foreach ($far14 as $row) {
            $outputRow = [];

            $commonGood = $row->common_good === 'Y';
            $hraAsset = $row->hra_asset === 'Y';

            /**
             * Receipts - income / Net Proceeds
             */
            if (!$commonGood) {
                $curNetConsideration = $row->net_consideration;
                $curCosts = $row->costs;
                $curProceeds = Math::subCurrency([$curNetConsideration, $curCosts]);

                if ($hraAsset) {
                    $this->writeCodeSection(
                        $outputRow,
                        $this->postingAcc->sub_addition_hra,
                        $this->postingAcc->sub_addition,
                        $this->postingAcc->prj_addition_hra,
                        $this->postingAcc->fund_blank
                    );
                    $this->writeDebit($outputRow, $curProceeds, "Disposal Proceeds Capital");
                    $this->newRptLine($outputData, $outputRow);

                    $this->writeCodeSection(
                        $outputRow,
                        $this->postingAcc->acc_balance_sheet,
                        $this->postingAcc->sub_capital_receipts_reserve,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_hra_code
                    );
                    $this->writeCredit($outputRow, $curProceeds, "Capital Receipts Reserve");
                    $this->newRptLine($outputData, $outputRow);

                    // Dr MIRS (Gain/Loss)
                    $this->writeCodeSection(
                        $outputRow,
                        $this->postingAcc->acc_hra_mirs,
                        $this->postingAcc->sub_disposal_receipts,
                        $this->postingAcc->prj_hra_mirs,
                        $this->postingAcc->fund_hra_mirs_disposal
                    );
                    $this->writeDebit($outputRow, $curProceeds, $row->fixed_asset_code);
                    $this->newRptLine($outputData, $outputRow);

                    // Cr I&E (Gain/Loss)
                    $this->writeCodeSection(
                        $outputRow,
                        $this->postingAcc->acc_hra_mirs,
                        $this->postingAcc->sub_disposal_loss,
                        $this->postingAcc->prj_hra_mirs,
                        $this->postingAcc->fund_hra_mirs_disposal_reverse
                    );
                    $this->writeCredit($outputRow, $curProceeds, "Capital Receipts Reserve");
                    $this->newRptLine($outputData, $outputRow);
                } else {
                    $this->writeCodeSection(
                        $outputRow,
                        $this->postingAcc->acc_addition_gf,
                        $this->postingAcc->sub_addition,
                        $this->postingAcc->prj_addition_gf,
                        $this->postingAcc->fund_blank
                    );
                    $this->writeDebit($outputRow, $curProceeds, "Disposal Proceeds Capital");
                    $this->newRptLine($outputData, $outputRow);

                    $this->writeCodeSection(
                        $outputRow,
                        $this->postingAcc->acc_balance_sheet,
                        $this->postingAcc->sub_capital_receipts_reserve,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_gf_code
                    );
                    $this->writeCredit($outputRow, $curProceeds, "Capital Receipts Reserve");
                    $this->newRptLine($outputData, $outputRow);

                    // Dr MIRS (Gain/loss)
                    $this->writeCodeSection(
                        $outputRow,
                        $this->postingAcc->acc_mirs,
                        $this->postingAcc->sub_disposal_receipts,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_blank
                    );
                    $this->writeDebit($outputRow, $curProceeds, $row->fixed_asset_code);
                    $this->newRptLine($outputData, $outputRow);

                    // Cr I&E (Gain/Loss)
                    $this->writeCodeSection(
                        $outputRow,
                        $this->postingAcc->acc_mirs,
                        $this->postingAcc->sub_disposal_loss,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_blank
                    );
                    $this->writeCredit($outputRow, $curProceeds, $row->fixed_asset_code);
                    $this->newRptLine($outputData, $outputRow);
                }
            }

            /**
             * Gross delete (Cr) from Fixed Asset Balance Sheet
             */
            $curGross = Math::negateCurrency($row->total_gbv);
            if (Math::notEqualCurrency($curGross, 0)) {
                if ($commonGood) {
                    $this->writeCodeSection(
                        $outputRow,
                        $row->account_code,
                        $row->account_code_1,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_blank
                    );
                } elseif ($hraAsset) {
                    $this->writeCodeSection(
                        $outputRow,
                        $this->postingAcc->acc_balance_sheet,
                        $row->account_code_1,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_hra_code
                    );
                } else {
                    $this->writeCodeSection(
                        $outputRow,
                        $this->postingAcc->acc_balance_sheet,
                        $row->account_code_1,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_gf_code
                    );
                }
                $text = "{$row->fixed_asset_code} Disposal ({$row->ca_ifrs_category_code})";
                $this->writeCreditBySign($outputRow, $curGross, $text);
                $this->newRptLine($outputData, $outputRow);
            }

            // Accumulated Depreciation Write Off
            $curDepnWo = $row->accum_dep;
            if (Math::notEqualCurrency($curDepnWo, 0)) {
                $this->writeCodeSection(
                    $outputRow,
                    $this->postingAcc->acc_balance_sheet,
                    $row->account_code_2,
                    $this->postingAcc->prj_blank_project,
                    $hraAsset ? $this->postingAcc->fund_hra_code : $this->postingAcc->fund_gf_code
                );
                $text = "{$row->fixed_asset_code} Depn. write off on Disposal";
                $this->writeDebitBySign($outputRow, $curDepnWo, $text);
                $this->newRptLine($outputData, $outputRow);
            }

            // Accumulated Impairment Write Off (Fife do this on the main gross code, no seperate code)
            $curImpWo = $row->accum_imp;
            if (Math::notEqualCurrency($curImpWo, 0)) {
                $this->writeCodeSection(
                    $outputRow,
                    $this->postingAcc->acc_balance_sheet,
                    $row->account_code_3,
                    $this->postingAcc->prj_blank_project,
                    $this->postingAcc->fund_gf_code
                );
                $text = "{$row->fixed_asset_code} Impairment write off on Disposal";
                $this->writeDebitBySign($outputRow, $curImpWo, $text);
                $this->newRptLine($outputData, $outputRow);
            }

            if (Math::notEqualCurrency($row->total_nbv, 0)) {
                // Net Charge to I&E
                $curSD = Math::negateCurrency($row->total_nbv);

                if (Math::notEqualCurrency($curSD, 0)) {
                    if ($commonGood) {
                        $this->writeCodeSection(
                            $outputRow,
                            $row->account_code,
                            $this->postingAcc->sub_disposal_loss,
                            $this->postingAcc->prj_blank_project,
                            $this->postingAcc->fund_blank
                        );
                        $text = "{$row->fixed_asset_code} Gains/Losses on Disposal (CG)";
                        $this->writeDebit($outputRow, $curSD, $text);
                        $this->newRptLine($outputData, $outputRow);
                    } elseif ($hraAsset) {
                        $this->writeCodeSection(
                            $outputRow,
                            $this->postingAcc->acc_hra_mirs,
                            $this->postingAcc->sub_disposal_loss,
                            $this->postingAcc->prj_hra_mirs,
                            $this->postingAcc->fund_hra_mirs_disposal_reverse
                        );
                        $text = "{$row->fixed_asset_code} Gains/Losses on Disposal (HRA)";
                        $this->writeDebit($outputRow, $curSD, $text);
                        $this->newRptLine($outputData, $outputRow);
                    } else {
                        $this->writeCodeSection(
                            $outputRow,
                            $this->postingAcc->acc_mirs,
                            $this->postingAcc->sub_disposal_loss,
                            $this->postingAcc->prj_blank_project,   // Corrected
                            $this->postingAcc->fund_blank
                        );
                        $text = "{$row->fixed_asset_code} Gains/Losses on Disposal (GF)";
                        $this->writeDebit($outputRow, $curSD, $text);
                        $this->newRptLine($outputData, $outputRow);
                    }

                    // Reversal to Cr MIRS
                    if ($commonGood) {
                        $this->writeCodeSection(
                            $outputRow,
                            $row->account_code,
                            $this->postingAcc->sub_disposal_loss_reversal,
                            $this->postingAcc->prj_blank_project,
                            $this->postingAcc->fund_blank
                        );
                    } elseif ($hraAsset) {
                        $this->writeCodeSection(
                            $outputRow,
                            $this->postingAcc->acc_hra_mirs,
                            $this->postingAcc->sub_disposal_loss_reversal,
                            $this->postingAcc->prj_hra_mirs,
                            $this->postingAcc->fund_hra_mirs_disposal
                        );
                    } else {
                        $this->writeCodeSection(
                            $outputRow,
                            $this->postingAcc->acc_mirs,
                            $this->postingAcc->sub_disposal_loss_reversal,
                            $this->postingAcc->prj_blank_project,
                            $this->postingAcc->fund_blank
                        );
                    }
                    $text = "{$row->fixed_asset_code} Net book value on disposal to Gain/Loss";
                    $this->writeCreditBySign($outputRow, $curSD, $text, "MIRS");
                    $this->newRptLine($outputData, $outputRow);

                    // Opposite entry Dr CAA
                    if ($commonGood) {
                        $this->writeCodeSection(
                            $outputRow,
                            $row->account_code,
                            $this->postingAcc->sub_capital_adjustment_acc,
                            $this->postingAcc->prj_blank_project,
                            $this->postingAcc->fund_blank
                        );
                    } elseif ($hraAsset) {
                        $this->writeCodeSection(
                            $outputRow,
                            $this->postingAcc->acc_balance_sheet,
                            $this->postingAcc->sub_capital_adjustment_acc,
                            $this->postingAcc->prj_blank_project,
                            $this->postingAcc->fund_hra_code
                        );
                    } else {
                        $this->writeCodeSection(
                            $outputRow,
                            $this->postingAcc->acc_balance_sheet,
                            $this->postingAcc->sub_capital_adjustment_acc,
                            $this->postingAcc->prj_blank_project,
                            $this->postingAcc->fund_gf_code
                        );
                    }
                    $text = "{$row->fixed_asset_code} CAA asset disposal";
                    $this->writeDebitBySign($outputRow, $curSD, $text, "CAA");
                    $this->newRptLine($outputData, $outputRow);
                }
            }

            // Write off of the Revaluation Reserve balance to the CAA account
            $curRR = Math::negateCurrency($row->reval_reserve);
            if (Math::notEqualCurrency($curRR, 0)) {
                if ($commonGood) {
                    // Loss Dr RR
                    $this->writeCodeSection(
                        $outputRow,
                        $row->account_code,
                        $this->postingAcc->sub_rr,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_blank
                    );
                    $text = "{$row->fixed_asset_code} Write out RR balance on disposal (CG)";
                    $this->writeDebit($outputRow, $curRR, $text, "RR");
                    $this->newRptLine($outputData, $outputRow);

                    // Opposite Cr CAA
                    $this->writeCodeSection(
                        $outputRow,
                        $row->account_code,
                        $this->postingAcc->sub_capital_adjustment_acc,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_blank
                    );
                    $text = "{$row->fixed_asset_code} CAA write out of RR balance on disposal (CG)";
                    $this->writeCredit($outputRow, $curRR, $text, "CAA");
                    $this->newRptLine($outputData, $outputRow);

                    /**
                     * An extra reversal for common good assets
                     */

                    // Loss Dr RR
                    $this->writeCodeSection(
                        $outputRow,
                        $row->account_code,
                        $this->postingAcc->sub_rr_write_down,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_blank
                    );
                    $text = "{$row->fixed_asset_code} Write out RR on disposal (CG)";
                    $this->writeDebit($outputRow, $curRR, $text, "Extra CG");
                    $this->newRptLine($outputData, $outputRow);

                    // Opposite Cr CAA
                    $this->writeCodeSection(
                        $outputRow,
                        $row->account_code,
                        $this->postingAcc->sub_disposal_rr_write_down,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_blank
                    );
                    $text = "{$row->fixed_asset_code} Write out RR on disposal (CG)";
                    $this->writeCredit($outputRow, $curRR, $text, "Extra CG");
                    $this->newRptLine($outputData, $outputRow);
                } elseif ($hraAsset) {
                    // Loss Dr RR
                    $this->writeCodeSection(
                        $outputRow,
                        $this->postingAcc->acc_balance_sheet,
                        $this->postingAcc->sub_rr,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_hra_code
                    );
                    $text = "{$row->fixed_asset_code} Write out RR balance on disposal (HRA)";
                    $this->writeDebit($outputRow, $curRR, $text, "RR");
                    $this->newRptLine($outputData, $outputRow);

                    // Opposite Cr CAA
                    $this->writeCodeSection(
                        $outputRow,
                        $this->postingAcc->acc_balance_sheet,
                        $this->postingAcc->sub_capital_adjustment_acc,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_hra_code
                    );
                    $text = "{$row->fixed_asset_code} CAA write out of RR on disposal (HRA)";
                    $this->writeCredit($outputRow, $curRR, $text, "CAA");
                    $this->newRptLine($outputData, $outputRow);

                    $totalRR_HRA = Math::addCurrency([$totalRR_HRA, $curRR]);
                } else {
                    // Loss Dr RR
                    $this->writeCodeSection(
                        $outputRow,
                        $this->postingAcc->acc_balance_sheet,
                        $this->postingAcc->sub_rr,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_gf_code
                    );
                    $text = "{$row->fixed_asset_code} Write out RR balance on disposal";
                    $this->writeDebit($outputRow, $curRR, $text, "RR");
                    $this->newRptLine($outputData, $outputRow);

                    // Opposite Cr CAA
                    $this->writeCodeSection(
                        $outputRow,
                        $this->postingAcc->acc_balance_sheet,
                        $this->postingAcc->sub_capital_adjustment_acc,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_gf_code
                    );
                    $text = "{$row->fixed_asset_code} CAA write out of RR on disposal";
                    $this->writeCredit($outputRow, $curRR, $text, "CAA");
                    $this->newRptLine($outputData, $outputRow);

                    $totalRR_GF = Math::addCurrency([$totalRR_GF, $curRR]);
                }
            }
        }

        /**
         * Write out total RR Disposals
         */

        // Dr 407210
        $this->writeCodeSection(
            $outputRow,
            $this->postingAcc->acc_mirs,
            $this->postingAcc->sub_rr_write_down,
            $this->postingAcc->prj_blank_project,
            $this->postingAcc->fund_blank
        );
        $this->writeDebit($outputRow, $totalRR_GF, "RR write out on disposal", "Extra (GF)");
        $this->newRptLine($outputData, $outputRow);

        // Cr 407115
        $this->writeCodeSection(
            $outputRow,
            $this->postingAcc->acc_mirs,
            $this->postingAcc->sub_disposal_rr_write_down,
            $this->postingAcc->prj_blank_project,
            $this->postingAcc->fund_blank
        );
        $this->writeCredit($outputRow, $totalRR_GF, "RR write out on disposal", "Extra (GF)");
        $this->newRptLine($outputData, $outputRow);

        // Dr 407210 (HRA)
        $this->writeCodeSection(
            $outputRow,
            $this->postingAcc->acc_hra_mirs,
            $this->postingAcc->sub_rr_write_down,
            $this->postingAcc->prj_hra_mirs,
            $this->postingAcc->fund_hra_mirs
        );
        $this->writeDebit($outputRow, $totalRR_HRA, "RR write out on disposal", "Extra (HRA)");
        $this->newRptLine($outputData, $outputRow);

        // Cr 407115 (HRA)
        $this->writeCodeSection(
            $outputRow,
            $this->postingAcc->acc_hra_mirs,
            $this->postingAcc->sub_disposal_rr_write_down,
            $this->postingAcc->prj_hra_mirs,
            $this->postingAcc->fund_hra_mirs
        );
        $this->writeCredit($outputRow, $totalRR_HRA, "RR write out on disposal", "Extra (HRA)");
        $this->newRptLine($outputData, $outputRow);

        return $outputData;
    }

    private function getData()
    {
        $query = VwFar14::join(
            'ca_ifrs_category',
            'ca_ifrs_category.ca_ifrs_category_id',
            '=',
            'vw_far14.ca_ifrs_category_id'
        )
        ->where('vw_far14.site_group_id', SiteGroup::get()->site_group_id)
        ->select(
            [
                'ca_ifrs_category.ca_ifrs_category_code',
                'ca_ifrs_category.common_good',
                'vw_far14.hra_asset',
                \DB::raw('vw_far14.`Asset Code` as fixed_asset_code'),
                \DB::raw('vw_far14.`Net Consideration` as net_consideration'),
                'vw_far14.Costs as costs',
                \DB::raw('vw_far14.`Total NBV` as total_nbv'),
                \DB::raw('vw_far14.`Total GBV` as total_gbv'),
                \DB::raw('vw_far14.`Account Code` as account_code'),
                'ca_ifrs_category.account_code_1',
                'ca_ifrs_category.account_code_2',
                'ca_ifrs_category.account_code_3',
                \DB::raw('vw_far14.`Accumulated Depreciation` as accum_dep'),
                \DB::raw('vw_far14.`Accumulated Impairment` as accum_imp'),
                \DB::raw('vw_far14.`Revaluation Reserve` as reval_reserve'),
            ]
        )
        ->orderBy(\DB::raw('vw_far14.`IFRS Code`'))
        ->orderBy(\DB::raw('vw_far14.`Asset Code`'));

        if ($this->entity) {
            $query->where('vw_far14.ca_entity_id', $this->entity);
        }
        return $query->get();
    }
}
