<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset;

use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Math;
use Tfcloud\Models\Report;
use Tfcloud\Models\SiteGroup;
use Tfcloud\Models\Views\VwFar16;
use Tfcloud\Services\PermissionService;

class Far23ReportService extends JournalPostingService
{
    protected $entity = false;

    private $reportBoxFilterLoader = null;

    public function __construct(PermissionService $permissionService, $filterData, Report $report = null)
    {
        parent::__construct($permissionService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;

        $this->setFilterProperties($filterData);
    }

    private function setFilterProperties($filterData)
    {
        $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($filterData);
        $entity = $reportFilterQuery->getFirstValueFilterField('ca_entity_id');

        // get filter values
        $this->entity = (!$entity) ? false : $entity;
    }

    public function getReportData(&$filterText, &$sOrderText)
    {
        // If no admin ca_posting_account record available, exit.
        if (!$this->postingAcc) {
            return [];
        }

        $far16 = $this->getData();

        $outputData = [];
        $curTotalGF = 0;
        $curTotalHRA = 0;

        foreach ($far16 as $row) {
            $outputRow = [];
            $commonGood = $row->common_good === 'Y';
            $hraAsset = $row->hra_asset === 'Y';
            $curGross = $row->total;
            $rowText = "{$row->fixed_asset_code} Addition ({$row->ca_ifrs_category_code})";

            if (Math::notEqualCurrency($curGross, 0)) {
                if ($commonGood) {
                    $this->writeCodeSection(
                        $outputRow,
                        $row->account_code,
                        $row->account_code_1,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_blank
                    );
                    $this->writeDebitBySign($outputRow, $curGross, $rowText);
                    $this->newRptLine($outputData, $outputRow);

                    $this->writeCodeSection(
                        $outputRow,
                        $row->account_code,
                        $this->postingAcc->sub_addition_common_good,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_blank
                    );
                    $this->writeCreditBySign($outputRow, $curGross, $rowText);
                } elseif ($hraAsset) {
                    $this->writeCodeSection(
                        $outputRow,
                        $this->postingAcc->acc_balance_sheet,
                        $row->account_code_1,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_hra_code
                    );
                    $this->writeDebitBySign($outputRow, $curGross, $rowText, 'HRA');
                    $curTotalHRA = Math::addCurrency([$curTotalHRA, $curGross]);
                } else {
                    $this->writeCodeSection(
                        $outputRow,
                        $this->postingAcc->acc_balance_sheet,
                        $row->account_code_1,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_gf_code
                    );
                    $this->writeDebitBySign($outputRow, $curGross, $rowText);
                    $curTotalGF = Math::addCurrency([$curTotalGF, $curGross]);
                }
                $outputData[] = $outputRow;
            }
        }

        $outputRow = [];

        // Write out total for GF
        $this->writeCodeSection(
            $outputRow,
            $this->postingAcc->acc_addition_gf,
            $this->postingAcc->sub_addition,
            $this->postingAcc->prj_addition_gf,
            $this->postingAcc->fund_blank
        );
        $this->writeCreditBySign($outputRow, $curTotalGF, "Additions (GF)");
        $this->newRptLine($outputData, $outputRow);

        // Write out total for HRA
        $this->writeCodeSection(
            $outputRow,
            $this->postingAcc->acc_addition_hra,
            $this->postingAcc->sub_addition,
            $this->postingAcc->prj_addition_hra,
            $this->postingAcc->fund_blank
        );
        $this->writeCreditBySign($outputRow, $curTotalHRA, "Additions (HRA)", "HRA");
        $this->newRptLine($outputData, $outputRow);

        return $outputData;
    }

    private function getData()
    {
        $query = VwFar16::join(
            'ca_ifrs_category',
            'ca_ifrs_category.ca_ifrs_category_id',
            '=',
            'vw_far16.ca_ifrs_category_id'
        )
        ->where('vw_far16.site_group_id', SiteGroup::get()->site_group_id)
        ->select(
            [
                'ca_ifrs_category.ca_ifrs_category_code',
                'ca_ifrs_category.common_good',
                'vw_far16.hra_asset',
                \DB::raw('vw_far16.`Asset Code` as fixed_asset_code'),
                \DB::raw('vw_far16.`Account Code` as account_code'),
                'ca_ifrs_category.account_code_1',
                'vw_far16.Total as total',
            ]
        )
        ->orderBy(\DB::raw('vw_far16.`IFRS Code`'))
        ->orderBy(\DB::raw('vw_far16.`Asset Code`'));

        if ($this->entity) {
            $query->where('vw_far16.ca_entity_id', $this->entity);
        }
        return $query->get();
    }
}
