<?php

/**
 * /////////////////////////////////////////////////////////////////////////
 * // Impairments
 * /////////////////////////////////////////////////////////////////////////
 *
 * fund: 9914 ???
 *
 *
 */

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset;

use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Math;
use Tfcloud\Models\Report;
use Tfcloud\Models\SiteGroup;
use Tfcloud\Models\Views\VwCaCurrentFinYear;
use Tfcloud\Models\Views\VwFar07;
use Tfcloud\Services\PermissionService;

class Far30ReportService extends JournalPostingService
{
    protected $entity = false;

    private $reportBoxFilterLoader = null;
    private $yearString = "";

    public function __construct(PermissionService $permissionService, $filterData, Report $report = null)
    {
        parent::__construct($permissionService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;

        $this->setFilterProperties($filterData);
    }

    private function setFilterProperties($filterData)
    {
        $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($filterData);
        $entity = $reportFilterQuery->getFirstValueFilterField('ca_entity_id');

        // get filter values
        $this->entity = (!$entity) ? false : $entity;
    }

    public function getReportData(&$filterText, &$sOrderText)
    {
        $outputData = [];
        $outputRow = [];

        $this->setYearString();

        // If no admin ca_posting_account record available, exit.
        if (!$this->postingAcc) {
            return [];
        }

        $subDepreciationReversal = $this->postingAcc->sub_depreciation_reversal;
        $hraDepreciationReversalCr = null;
        $hraDepreciationReversalDr = null;

        if (str_contains($this->postingAcc->sub_depreciation_reversal, ';')) {
            $revCodes = explode(';', $this->postingAcc->sub_depreciation_reversal);
            $subDepreciationReversal = $revCodes[0];
            $hraDepreciationReversalCr = $revCodes[1];
            $hraDepreciationReversalDr = $revCodes[2];
        }

        /**
         * ////////////////////////////////////////////////////////////
         * // Asset by Asset depreciation for all assets - GF+HRA+CG //
         * ////////////////////////////////////////////////////////////
         */
        $data = $this->getAllAssetData();
        foreach ($data as $row) {
            if (Math::notEqualCurrency($row->amount, 0)) {
                $outputRow = [];

                // Cost Centre - show error if not found
                $costCentre = strlen($row->account_code) > 0 ? $row->account_code : "NOT FOUND";

                // Subjective/Account Code - differs for Intangibles
                if ($row->bfwd_amortisation === 'Y') {
                    $subjective = $this->postingAcc->sub_amortisation_charge;
                    $comment = "INT";
                } else {
                    $subjective = $this->postingAcc->sub_depreciation_charge;
                    $comment = "";
                }

                // Project and Fund - different for HRA assets
                if ($row->hra_asset === 'Y') {
                    $project = $this->postingAcc->prj_hra_mirs;
                    $fund = "9914";
                    $comment = "HRA";
                } else {
                    $project = $this->postingAcc->prj_blank_project;
                    $fund = $this->postingAcc->fund_blank;
                }

                // Write out the code section
                $this->writeCodeSection(
                    $outputRow,
                    $costCentre,
                    $subjective,
                    $project,
                    $fund
                );

                // Dr Amount of Depreciation - should total to same as FAR07
                $text = "{$row->fixed_asset_code} {$row->fixed_asset_desc}";
                $this->writeDebit($outputRow, Math::negateCurrency($row->amount), $text, $comment);
                $this->newRptLine($outputData, $outputRow);
            }
        }

        /**
         * ///////////////////////////////////////////
         * -- Balance Sheet Entries
         * -- Accum. Depn section by IFRS Category ---
         * ///////////////////////////////////////////
         */
        $data = $this->getDepnDataByIfrsCategory();
        foreach ($data as $row) {
            if (Math::notEqualCurrency($row->amount, 0)) {
                if ($row->hra_asset === "Y") {
                    $this->writeCodeSection(
                        $outputRow,
                        $this->postingAcc->acc_balance_sheet,
                        $row->account_code_2,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_hra_code
                    );
                } else {
                    $this->writeCodeSection(
                        $outputRow,
                        $this->postingAcc->acc_balance_sheet,
                        $row->account_code_2,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_gf_code
                    );
                }
                $text = "{$row->previous_ifrs_code} depreciation  {$this->yearString}";
                $this->writeCredit($outputRow, Math::negateCurrency($row->amount), $text, $row->previous_ifrs_code);
                $this->newRptLine($outputData, $outputRow);
            }
        }

        /**
         * ////////////////////////////////////////////////
         * -- Balance Sheet Entries for Common Good Assets
         * ////////////////////////////////////////////////
         */
        $data = $this->getCommonGoodAssetData();
        foreach ($data as $row) {
            if (Math::notEqualCurrency($row->amount, 0)) {
                $this->writeCodeSection(
                    $outputRow,
                    $row->account_code,
                    $row->account_code_2,
                    $this->postingAcc->prj_blank_project,
                    $this->postingAcc->fund_blank
                );
                $text = "{$row->previous_ifrs_code} depreciation  {$this->yearString}";
                $this->writeCredit($outputRow, Math::negateCurrency($row->amount), $text, $row->previous_ifrs_code);
                $this->newRptLine($outputData, $outputRow);
            }
        }


        /**
         * ////////////////////////////////
         * --- Cr HRA MIRS and Dr CAA  ---
         * ////////////////////////////////
         */
        $data = $this->getHraMirsAndCaaData();
        foreach ($data as $row) {
            if (Math::notEqualCurrency($row->amount, 0)) {
                $text = "HRA Asset register depreciation  {$this->yearString}";

                // MIRS - HRA
                $this->writeCodeSection(
                    $outputRow,
                    $this->postingAcc->acc_hra_mirs,
                    $hraDepreciationReversalCr ? $hraDepreciationReversalCr : $subDepreciationReversal,
                    $this->postingAcc->prj_hra_mirs,
                    $this->postingAcc->fund_hra_revaluation
                );
                $this->writeCredit($outputRow, Math::negateCurrency($row->amount), $text, "HRA");
                $this->newRptLine($outputData, $outputRow);

                // CAA - HRA
                $this->writeCodeSection(
                    $outputRow,
                    $this->postingAcc->acc_balance_sheet,
                    $hraDepreciationReversalDr ?
                        $hraDepreciationReversalDr : $this->postingAcc->sub_capital_adjustment_acc,
                    $this->postingAcc->prj_blank_project,
                    $this->postingAcc->fund_hra_code
                );
                $this->writeDebit($outputRow, Math::negateCurrency($row->amount), $text, "CAA");
                $this->newRptLine($outputData, $outputRow);
            }
        }


        /**
         * //////////////////////////////////////
         * --- Cr MIRS **non** HRA and Dr CAA ---
         * //////////////////////////////////////
         */
        $data = $this->getMirsNoneHraAndCaaData();
        foreach ($data as $row) {
            if (Math::notEqualCurrency($row->amount, 0)) {
                $text = "Gen Fund Asset register depreciation  {$this->yearString}";

                $this->writeCodeSection(
                    $outputRow,
                    $this->postingAcc->acc_mirs,
                    $subDepreciationReversal, //$this->postingAcc->sub_depreciation_reversal,
                    $this->postingAcc->prj_blank_project,
                    $this->postingAcc->fund_blank
                );
                $this->writeCredit($outputRow, Math::negateCurrency($row->amount), $text, "MIRS");
                $this->newRptLine($outputData, $outputRow);

                $this->writeCodeSection(
                    $outputRow,
                    $this->postingAcc->acc_balance_sheet,
                    $this->postingAcc->sub_capital_adjustment_acc,
                    $this->postingAcc->prj_blank_project,
                    $this->postingAcc->fund_gf_code
                );
                $this->writeDebit($outputRow, Math::negateCurrency($row->amount), $text, "CAA");
                $this->newRptLine($outputData, $outputRow);
            }
        }


        /**
         * ///////////////////////////////////////
         * -- Cr MIRS and Dr CAA - Amortisation --
         * ///////////////////////////////////////
         */
        $data = $this->getAmortisationData();
        foreach ($data as $row) {
            if (Math::notEqualCurrency($row->amount, 0)) {
                $text = "Gen Fund Amortisation {$this->yearString}";

                $this->writeCodeSection(
                    $outputRow,
                    $this->postingAcc->acc_mirs,
                    $this->postingAcc->sub_amortisation_reversal,
                    $this->postingAcc->prj_blank_project,
                    $this->postingAcc->fund_blank
                );
                $this->writeCredit($outputRow, Math::negateCurrency($row->amount), $text, "MIRS");
                $this->newRptLine($outputData, $outputRow);

                $this->writeCodeSection(
                    $outputRow,
                    $this->postingAcc->acc_balance_sheet,
                    $this->postingAcc->sub_capital_adjustment_acc,
                    $this->postingAcc->prj_blank_project,
                    $this->postingAcc->fund_gf_code
                );
                $this->writeDebit($outputRow, Math::negateCurrency($row->amount), $text, "CAA");
                $this->newRptLine($outputData, $outputRow);
            }
        }


        /**
         * /////////////////////////////////////////////
         * -- Cr MIRS and Dr CAA - Common Good Assets --
         * /////////////////////////////////////////////
         */
        $data = $this->getMirsCaaCommonGoodAssetData();
        foreach ($data as $row) {
            if (Math::notEqualCurrency($row->amount, 0)) {
                $text = "Common Good Depreciation {$this->yearString}";

                $this->writeCodeSection(
                    $outputRow,
                    $row->account_code,
                    $subDepreciationReversal, //$this->postingAcc->sub_depreciation_reversal,
                    $this->postingAcc->prj_blank_project,
                    $this->postingAcc->fund_blank
                );
                $this->writeCredit($outputRow, Math::negateCurrency($row->amount), $text, "MIRS");
                $this->newRptLine($outputData, $outputRow);

                $this->writeCodeSection(
                    $outputRow,
                    $row->account_code,
                    $this->postingAcc->sub_capital_adjustment_acc,
                    $this->postingAcc->prj_blank_project,
                    $this->postingAcc->fund_blank
                );
                $this->writeDebit($outputRow, Math::negateCurrency($row->amount), $text, "CAA");
                $this->newRptLine($outputData, $outputRow);
            }
        }


        /**
         * //////////////////////
         * -- Dr RR and Cr CAA --
         * //////////////////////
         */


        /**
         * --- CAA to RR adjustment - NOT HRA AND NOT Common Good---
         */
        $data = $this->getCaaToRrAdjustmentNotHraCommonGoodData();
        foreach ($data as $row) {
            if (Math::notEqualCurrency($row->amount, 0)) {
                $text = "Gen Fund RR Write Down {$this->yearString}";

                // Dr RR
                $this->writeCodeSection(
                    $outputRow,
                    $this->postingAcc->acc_balance_sheet,
                    $this->postingAcc->sub_rr,
                    $this->postingAcc->prj_blank_project,
                    $this->postingAcc->fund_gf_code
                );
                $this->writeDebit($outputRow, $row->amount, $text, "RR");
                $this->newRptLine($outputData, $outputRow);

                // Cr CAA
                $this->writeCodeSection(
                    $outputRow,
                    $this->postingAcc->acc_balance_sheet,
                    $this->postingAcc->sub_capital_adjustment_acc,
                    $this->postingAcc->prj_blank_project,
                    $this->postingAcc->fund_gf_code
                );
                $this->writeCredit($outputRow, $row->amount, $text, "CAA");
                $this->newRptLine($outputData, $outputRow);

                // Dr 407210
                $this->writeCodeSection(
                    $outputRow,
                    $this->postingAcc->acc_mirs,
                    $this->postingAcc->sub_rr_write_down,
                    $this->postingAcc->prj_blank_project,
                    $this->postingAcc->fund_blank
                );
                $this->writeDebit($outputRow, $row->amount, $text, $this->postingAcc->sub_rr_write_down);
                $this->newRptLine($outputData, $outputRow);

                // Cr 407110
                $this->writeCodeSection(
                    $outputRow,
                    $this->postingAcc->acc_mirs,
                    $this->postingAcc->sub_rr_write_down_reversal,
                    $this->postingAcc->prj_blank_project,
                    $this->postingAcc->fund_blank
                );
                $this->writeCredit($outputRow, $row->amount, $text, $this->postingAcc->sub_rr_write_down_reversal);
                $this->newRptLine($outputData, $outputRow);
            }
        }


        /**
         * --- CAA to RR adjustment (HRA) ---
         */
        $data = $this->getCaaToRrAdjustmentHraData();
        foreach ($data as $row) {
            if (Math::notEqualCurrency($row->amount, 0)) {
                $text = "HRA RR Write Down {$this->yearString}";

                // Dr RR
                $this->writeCodeSection(
                    $outputRow,
                    $this->postingAcc->acc_balance_sheet,
                    $this->postingAcc->sub_rr,
                    $this->postingAcc->prj_blank_project,
                    $this->postingAcc->fund_hra_code
                );
                $this->writeDebit($outputRow, $row->amount, $text, "RR");
                $this->newRptLine($outputData, $outputRow);

                // Cr CAA
                $this->writeCodeSection(
                    $outputRow,
                    $this->postingAcc->acc_balance_sheet,
                    $this->postingAcc->sub_capital_adjustment_acc,
                    $this->postingAcc->prj_blank_project,
                    $this->postingAcc->fund_hra_code
                );
                $this->writeCredit($outputRow, $row->amount, $text, "CAA");
                $this->newRptLine($outputData, $outputRow);

                // Dr 407210
                $this->writeCodeSection(
                    $outputRow,
                    $this->postingAcc->acc_hra_mirs,
                    $this->postingAcc->sub_rr_write_down,
                    $this->postingAcc->prj_hra_mirs,
                    $this->postingAcc->fund_hra_mirs
                );
                $this->writeDebit($outputRow, $row->amount, $text, "Extra");
                $this->newRptLine($outputData, $outputRow);

                // Cr 407110
                $this->writeCodeSection(
                    $outputRow,
                    $this->postingAcc->acc_hra_mirs,
                    $this->postingAcc->sub_rr_write_down_reversal,
                    $this->postingAcc->prj_hra_mirs,
                    $this->postingAcc->fund_hra_mirs
                );
                $this->writeCredit($outputRow, $row->amount, $text, "Extra");
                $this->newRptLine($outputData, $outputRow);
            }
        }

        /**
         * --- CAA to RR adjustment (Common Good) ---
         */
        $data = $this->getCaaToRrAdjustmentCommonGoodData();
        foreach ($data as $row) {
            if (Math::notEqualCurrency($row->amount, 0)) {
                $text = "Common Good RR Write Down {$this->yearString}";

                $this->writeCodeSection(
                    $outputRow,
                    $row->account_code,
                    $this->postingAcc->sub_rr,
                    $this->postingAcc->prj_blank_project,
                    $this->postingAcc->fund_blank
                );
                $this->writeDebit($outputRow, $row->amount, $text, "RR");
                $this->newRptLine($outputData, $outputRow);

                $this->writeCodeSection(
                    $outputRow,
                    $row->account_code,
                    $this->postingAcc->sub_capital_adjustment_acc,
                    $this->postingAcc->prj_blank_project,
                    $this->postingAcc->fund_blank
                );
                $this->writeCredit($outputRow, $row->amount, $text, "CAA");
                $this->newRptLine($outputData, $outputRow);

                // Extra reversal - Dr 407210 Cr 407110
                $this->writeCodeSection(
                    $outputRow,
                    $row->account_code,
                    $this->postingAcc->sub_rr_write_down,
                    $this->postingAcc->prj_blank_project,
                    $this->postingAcc->fund_blank
                );
                $this->writeDebit($outputRow, $row->amount, $text, "Extra");
                $this->newRptLine($outputData, $outputRow);

                $this->writeCodeSection(
                    $outputRow,
                    $row->account_code,
                    $this->postingAcc->sub_rr_write_down_reversal,
                    $this->postingAcc->prj_blank_project,
                    $this->postingAcc->fund_blank
                );
                $this->writeCredit($outputRow, $row->amount, $text, "Extra");
                $this->newRptLine($outputData, $outputRow);
            }
        }

        return $outputData;
    }

    private function getAllAssetData()
    {
        $query = VwFar07::userSiteGroup()
        ->leftJoin(
            'ca_ifrs_category as bfwd_ca_ifrs_category',
            'bfwd_ca_ifrs_category.ca_ifrs_category_id',
            '=',
            'vw_far07.bfwd_ca_ifrs_category_id'
        )
        ->where('vw_far07.ca_balance_historic', 'N')
        ->select(
            [
                'vw_far07.hra_asset',
                \DB::raw('vw_far07.`Asset Code` as fixed_asset_code'),
                \DB::raw('vw_far07.`Asset Description` as fixed_asset_desc'),
                \DB::raw('vw_far07.`Account Code` as account_code'),
                \DB::raw('vw_far07.`CY Depreciation` as amount'),
                'bfwd_ca_ifrs_category.amortisation as bfwd_amortisation',
            ]
        )
        ->orderBy(\DB::raw('vw_far07.`IFRS Code`'))
        ->orderBy(\DB::raw('vw_far07.`Site Code`'))
        ->orderBy(\DB::raw('vw_far07.`Asset Code`'));

        if ($this->entity) {
            $query->where('vw_far07.ca_entity_id', $this->entity);
        }
        return $query->get();
    }

    private function getDepnDataByIfrsCategory()
    {
        $query = VwFar07::userSiteGroup()
        ->leftJoin(
            'ca_ifrs_category as bfwd_ca_ifrs_category',
            'bfwd_ca_ifrs_category.ca_ifrs_category_id',
            '=',
            'vw_far07.bfwd_ca_ifrs_category_id'
        )
        ->where('vw_far07.ca_balance_historic', 'N')
        ->where('bfwd_ca_ifrs_category.common_good', 'N')
        ->groupBy(
            'bfwd_ca_ifrs_category.ca_ifrs_category_code',
            'bfwd_ca_ifrs_category.account_code_2',
            'vw_far07.hra_asset'
        )
        ->select(
            [
                'bfwd_ca_ifrs_category.ca_ifrs_category_code as previous_ifrs_code',
                'bfwd_ca_ifrs_category.account_code_2',
                'vw_far07.hra_asset',
                \DB::raw('SUM(`CY Depreciation`) as amount')
            ]
        )
        ->orderBy(\DB::raw('vw_far07.`IFRS Code`'))
        ->orderBy(\DB::raw('vw_far07.`Site Code`'))
        ->orderBy(\DB::raw('vw_far07.`Asset Code`'));

        if ($this->entity) {
            $query->where('vw_far07.ca_entity_id', $this->entity);
        }
        return $query->get();
    }

    private function getCommonGoodAssetData()
    {
        $query = VwFar07::userSiteGroup()
        ->leftJoin(
            'ca_ifrs_category as bfwd_ca_ifrs_category',
            'bfwd_ca_ifrs_category.ca_ifrs_category_id',
            '=',
            'vw_far07.bfwd_ca_ifrs_category_id'
        )
        ->where('vw_far07.ca_balance_historic', 'N')
        ->where('bfwd_ca_ifrs_category.common_good', 'Y')
        ->groupBy(
            'bfwd_ca_ifrs_category.ca_ifrs_category_code',
            'bfwd_ca_ifrs_category.account_code_2',
            \DB::raw('vw_far07.`account code`')
        )
        ->select(
            [
                'bfwd_ca_ifrs_category.ca_ifrs_category_code as previous_ifrs_code',
                'bfwd_ca_ifrs_category.account_code_2',
                \DB::raw('vw_far07.`Account Code` as account_code'),
                \DB::raw('SUM(`CY Depreciation`) as amount'),
            ]
        )
        ->orderBy(\DB::raw('vw_far07.`IFRS Code`'))
        ->orderBy(\DB::raw('vw_far07.`Site Code`'))
        ->orderBy(\DB::raw('vw_far07.`Asset Code`'));

        if ($this->entity) {
            $query->where('vw_far07.ca_entity_id', $this->entity);
        }
        return $query->get();
    }

    private function getHraMirsAndCaaData()
    {
        $query = VwFar07::userSiteGroup()
        ->where('vw_far07.ca_balance_historic', 'N')
        ->whereRaw("vw_far07.`HRA Asset` = 'Y'")
        ->select(
            [
                \DB::raw('SUM(`CY Depreciation`) as amount'),
            ]
        )
        ->orderBy(\DB::raw('vw_far07.`IFRS Code`'))
        ->orderBy(\DB::raw('vw_far07.`Site Code`'))
        ->orderBy(\DB::raw('vw_far07.`Asset Code`'));

        if ($this->entity) {
            $query->where('vw_far07.ca_entity_id', $this->entity);
        }
        return $query->get();
    }

    private function getMirsNoneHraAndCaaData()
    {
        $query = VwFar07::userSiteGroup()
        ->leftJoin(
            'ca_ifrs_category as bfwd_ca_ifrs_category',
            'bfwd_ca_ifrs_category.ca_ifrs_category_id',
            '=',
            'vw_far07.bfwd_ca_ifrs_category_id'
        )
        ->where('vw_far07.ca_balance_historic', 'N')
        ->whereRaw("vw_far07.`HRA Asset` <> 'Y'")
        ->where('bfwd_ca_ifrs_category.amortisation', '<>', 'Y')
        ->where('bfwd_ca_ifrs_category.common_good', '<>', 'Y')
        ->select(
            [
                \DB::raw('SUM(`CY Depreciation`) as amount'),
            ]
        )
        ->orderBy(\DB::raw('vw_far07.`IFRS Code`'))
        ->orderBy(\DB::raw('vw_far07.`Site Code`'))
        ->orderBy(\DB::raw('vw_far07.`Asset Code`'));

        if ($this->entity) {
            $query->where('vw_far07.ca_entity_id', $this->entity);
        }
        return $query->get();
    }

    private function getAmortisationData()
    {
        $query = VwFar07::userSiteGroup()
        ->leftJoin(
            'ca_ifrs_category as bfwd_ca_ifrs_category',
            'bfwd_ca_ifrs_category.ca_ifrs_category_id',
            '=',
            'vw_far07.bfwd_ca_ifrs_category_id'
        )
        ->where('vw_far07.ca_balance_historic', 'N')
        ->whereRaw("vw_far07.`HRA Asset` <> 'Y'")
        ->where('bfwd_ca_ifrs_category.amortisation', 'Y')
        ->where('bfwd_ca_ifrs_category.common_good', 'N')
        ->select(
            [
                \DB::raw('SUM(`CY Depreciation`) as amount'),
            ]
        )
        ->orderBy(\DB::raw('vw_far07.`IFRS Code`'))
        ->orderBy(\DB::raw('vw_far07.`Site Code`'))
        ->orderBy(\DB::raw('vw_far07.`Asset Code`'));

        if ($this->entity) {
            $query->where('vw_far07.ca_entity_id', $this->entity);
        }
        return $query->get();
    }

    private function getMirsCaaCommonGoodAssetData()
    {
        $query = VwFar07::userSiteGroup()
        ->leftJoin(
            'ca_ifrs_category as bfwd_ca_ifrs_category',
            'bfwd_ca_ifrs_category.ca_ifrs_category_id',
            '=',
            'vw_far07.bfwd_ca_ifrs_category_id'
        )
        ->where('vw_far07.ca_balance_historic', 'N')
        ->where('bfwd_ca_ifrs_category.common_good', 'Y')
        ->groupBy(
            \DB::raw('vw_far07.`account code`')
        )
        ->select(
            [
                \DB::raw('vw_far07.`Account Code` as account_code'),
                \DB::raw('SUM(`CY Depreciation`) as amount'),
            ]
        )
        ->orderBy(\DB::raw('vw_far07.`IFRS Code`'))
        ->orderBy(\DB::raw('vw_far07.`Site Code`'))
        ->orderBy(\DB::raw('vw_far07.`Asset Code`'));

        if ($this->entity) {
            $query->where('vw_far07.ca_entity_id', $this->entity);
        }
        return $query->get();
    }

    private function getCaaToRrAdjustmentNotHraCommonGoodData()
    {
        $query = VwFar07::userSiteGroup()
        ->leftJoin(
            'ca_ifrs_category as bfwd_ca_ifrs_category',
            'bfwd_ca_ifrs_category.ca_ifrs_category_id',
            '=',
            'vw_far07.bfwd_ca_ifrs_category_id'
        )
        ->where('vw_far07.ca_balance_historic', 'N')
        ->where('bfwd_ca_ifrs_category.common_good', 'N')
        ->whereRaw("vw_far07.`HRA Asset` <> 'Y'")
        ->whereRaw("vw_far07.`RR Depreciation` <> 0")
        ->select(
            [
                \DB::raw('SUM(`RR Depreciation`) as amount'),
            ]
        )
        ->orderBy(\DB::raw('vw_far07.`IFRS Code`'))
        ->orderBy(\DB::raw('vw_far07.`Site Code`'))
        ->orderBy(\DB::raw('vw_far07.`Asset Code`'));

        if ($this->entity) {
            $query->where('vw_far07.ca_entity_id', $this->entity);
        }
        return $query->get();
    }

    private function getCaaToRrAdjustmentHraData()
    {
        $query = VwFar07::userSiteGroup()
        ->where('vw_far07.ca_balance_historic', 'N')
        ->whereRaw("vw_far07.`HRA Asset` = 'Y'")
        ->whereRaw("vw_far07.`RR Depreciation` <> 0")
        ->select(
            [
                \DB::raw('SUM(`RR Depreciation`) as amount'),
            ]
        )
        ->orderBy(\DB::raw('vw_far07.`IFRS Code`'))
        ->orderBy(\DB::raw('vw_far07.`Site Code`'))
        ->orderBy(\DB::raw('vw_far07.`Asset Code`'));

        if ($this->entity) {
            $query->where('vw_far07.ca_entity_id', $this->entity);
        }
        return $query->get();
    }

    private function getCaaToRrAdjustmentCommonGoodData()
    {
        $query = VwFar07::userSiteGroup()
        ->leftJoin(
            'ca_ifrs_category as bfwd_ca_ifrs_category',
            'bfwd_ca_ifrs_category.ca_ifrs_category_id',
            '=',
            'vw_far07.bfwd_ca_ifrs_category_id'
        )
        ->where('vw_far07.ca_balance_historic', 'N')
        ->where('bfwd_ca_ifrs_category.common_good', 'Y')
        ->whereRaw("vw_far07.`RR Depreciation` <> 0")
        ->groupBy(
            \DB::raw('vw_far07.`account code`')
        )
        ->select(
            [
                \DB::raw('vw_far07.`Account Code` as account_code'),
                \DB::raw('SUM(`RR Depreciation`) as amount'),
            ]
        )
        ->orderBy(\DB::raw('vw_far07.`IFRS Code`'))
        ->orderBy(\DB::raw('vw_far07.`Site Code`'))
        ->orderBy(\DB::raw('vw_far07.`Asset Code`'));

        if ($this->entity) {
            $query->where('vw_far07.ca_entity_id', $this->entity);
        }
        return $query->get();
    }

    private function setYearString()
    {
        $openCaFinYear = VwCaCurrentFinYear::userSiteGroup()
            ->select('ca_fin_year_code')
            ->first();

        if ($openCaFinYear) {
            $this->yearString = $openCaFinYear->ca_fin_year_code;
        }
    }
}
