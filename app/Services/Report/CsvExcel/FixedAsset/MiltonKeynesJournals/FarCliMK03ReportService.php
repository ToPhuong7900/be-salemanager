<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\MiltonKeynesJournals;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\FixedAsset\CaFinYearManager;
use Tfcloud\Lib\Math;
use Tfcloud\Models\CaBalance;
use Tfcloud\Models\Views\VwFar07;
use Tfcloud\Services\PermissionService;

class FarCliMK03ReportService extends MiltonKeynesJournalPostingService
{
    protected $yearString = "";
    private $journalData = [];
    // Option to total up the CAA to SMR reversal
    private const TOTAL_REVERSALS = false;

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->addJournalHeader($this->journalData, 'Depreciation');
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        /**
         * /////////////////////////////////////////////////
         * // Asset by Asset Depreciation for all assets  //
         * /////////////////////////////////////////////////
         */

        $this->prepareDepreciationData();

        return $this->journalData;
    }

    private function prepareDepreciationData()
    {
        $sCostCentre = '';
        $sAccountCode1 = '';
        $sAccountCode2 = '';
        $sAccountCode3 = '';
        $sAccountCode4 = '';

        $sIFRSSubType = '';
        $sIFRSCode = '';

        $CYDepn = 0;
        $CYHCDepn = 0;


        // Just used to implement grouping
        $sGroup = '';
        $sCurrentRecordGroup = '';

        $sGroupCostCentre = '';
        $curSubTotalDepn = 0;
        $curSubTotalRRDepn = 0;


        $lCount = 0;
        $curTotalAmount = 0;

        $bHasTransfer = false;

        $assetData = $this->getAllAssetData();

        foreach ($assetData as $asset) {
            $lCount++;
            $bHasTransfer = false;

            $sIFRSSubType = $asset->ca_ifrs_category_desc;
            $sIFRSCode = $asset->ca_ifrs_category_code;

            // 2nd part of group is cost centre
            $sCostCentre = $asset->account_code;

            // Starting Group.
            if ($lCount == 1) {
                $sGroup = "$sCostCentre/$sIFRSSubType";
                $sGroupCostCentre = $sCostCentre;
                $curSubTotalDepn = 0;
                $curSubTotalRRDepn = 0;
            }

            // Start of a new group?
            if ($sGroup != "$sCostCentre/$sIFRSSubType") {
                // Write out the last (previous) group then start new one

                // No point if both zero
                if (Math::notEqualCurrency($curSubTotalDepn, 0) || Math::notEqualCurrency($curSubTotalRRDepn, 0)) {
                    // Depreciation to Dr revenue (I&E Charge) and Cr accum depn.
                    if (Math::greaterThanCurrency($curSubTotalDepn, 0)) {
                        $sRowDesc = "$sGroup Depn $this->yearString";

                        // Add depn. to grand total.
                        $curTotalAmount = Math::addCurrency([$curTotalAmount, $curSubTotalDepn]);

                        // Add to total Amortisation if Infrastructure (can override choices above).
                        if ($this->textContains($sGroup, 'Intangible')) {
                            $this->addJournalRow(
                                $this->journalData,
                                self::DR,
                                self::AMORTISATION_CODE,
                                $sGroupCostCentre,
                                '',
                                $curSubTotalDepn,
                                $sRowDesc
                            );
                        } else {
                            $this->addJournalRow(
                                $this->journalData,
                                self::DR,
                                self::DEPRECIATION_CODE,
                                $sGroupCostCentre,
                                '',
                                $curSubTotalDepn,
                                $sRowDesc
                            );
                        }

                        // Charge line for depreciation
                        $this->addJournalRow(
                            $this->journalData,
                            self::CR,
                            $sAccountCode2,
                            $sGroupCostCentre,
                            '',
                            $curSubTotalDepn,
                            $sRowDesc
                        );

                        if (!self::TOTAL_REVERSALS) {
                            // Debit to CAA reverse through SMR
                            $this->addJournalRow(
                                $this->journalData,
                                self::DR,
                                self::CAPITAL_ADJUSTMENT_ACCOUNT,
                                $sGroupCostCentre,
                                '',
                                $curSubTotalDepn,
                                $sRowDesc
                            );
                            $this->addJournalRow(
                                $this->journalData,
                                self::CR,
                                self::SMR_ACCOUNT,
                                $sGroupCostCentre,
                                '',
                                $curSubTotalDepn,
                                $sRowDesc
                            );
                        }
                    }

                    // If there is a difference in depreciation then depreciate RR
                    if (Math::greaterThanCurrency($curSubTotalRRDepn, 0)) {
                        $this->addJournalRow(
                            $this->journalData,
                            self::DR,
                            self::REVAL_RESERVE_ACCOUNT,
                            $sGroupCostCentre,
                            '',
                            $curSubTotalRRDepn,
                            "$sGroup RR depn. $this->yearString"
                        );

                        $this->addJournalRow(
                            $this->journalData,
                            self::CR,
                            self::CAPITAL_ADJUSTMENT_ACCOUNT,
                            $sGroupCostCentre,
                            '',
                            $curSubTotalRRDepn,
                            "$sGroup RR depn. $this->yearString"
                        );
                    }
                }

                /////// Start new group - record this groups signature ///////
                $sGroup = "$sCostCentre/$sIFRSSubType";
                $sGroupCostCentre = $sCostCentre;

                // Reset totals for next group.
                $curSubTotalDepn = 0;
                $curSubTotalRRDepn = 0;
            }

            // Get sub-type accounts.
            $this->getIFRSAccount(
                $sIFRSCode,
                $sAccountCode1,
                $sAccountCode2,
                $sAccountCode3,
                $sAccountCode4
            );

            // Add on values to group totals
            $hcBalance = $this->retrieveBalance(
                $asset->ca_fixed_asset_id,
                CaFinYearManager::currentFinYearId(),
                CaBalance::TYPE_HISTORIC_VALUE
            );
            $CYDepn = $asset->amount;
            $CYHCDepn = $hcBalance->cy_depreciation;

            $curSubTotalDepn = Math::addCurrency([$curSubTotalDepn, $CYDepn]);
            $curSubTotalRRDepn = Math::addCurrency([$curSubTotalRRDepn, Math::subCurrency([$CYDepn, $CYHCDepn])]);
        }   //foreach asset.

        ///////////////////////////////////////////////
        // Write out the last group at EOF
        ///////////////////////////////////////////////

        // No point if both zero
        if (Math::notEqualCurrency($curSubTotalDepn, 0) || Math::notEqualCurrency($curSubTotalRRDepn, 0)) {
            if (Math::notEqualCurrency($curSubTotalDepn, 0)) {
                // Add depn. to grand total.
                $curTotalAmount = Math::addCurrency([$curTotalAmount, $curSubTotalDepn]);

                if ($this->textContains($sIFRSSubType, 'Intangible')) {
                    $this->addJournalRow(
                        $this->journalData,
                        self::DR,
                        self::AMORTISATION_CODE,
                        $sCostCentre,
                        '',
                        $curSubTotalDepn,
                        "$sGroup depn. $this->yearString"
                    );
                } else {
                    $this->addJournalRow(
                        $this->journalData,
                        self::DR,
                        self::DEPRECIATION_CODE,
                        $sCostCentre,
                        '',
                        $curSubTotalDepn,
                        "$sGroup depn. $this->yearString"
                    );
                }

                $this->addJournalRow(
                    $this->journalData,
                    self::CR,
                    $sAccountCode2,
                    $sCostCentre,
                    '',
                    $curSubTotalDepn,
                    "$sGroup depn. $this->yearString"
                );
            }

            if (!self::TOTAL_REVERSALS) {
                $this->addJournalRow(
                    $this->journalData,
                    self::DR,
                    self::CAPITAL_ADJUSTMENT_ACCOUNT,
                    $sGroupCostCentre,
                    '',
                    $curSubTotalDepn,
                    "$sGroup depn. $this->yearString"
                );
                $this->addJournalRow(
                    $this->journalData,
                    self::CR,
                    self::SMR_ACCOUNT,
                    $sGroupCostCentre,
                    '',
                    $curSubTotalDepn,
                    "$sGroup depn. $this->yearString"
                );
            }

            // If there is a difference in depreciation then depreciate RR.
            if (Math::notEqualCurrency($curSubTotalRRDepn, 0)) {
                $this->addJournalRow(
                    $this->journalData,
                    self::DR,
                    self::REVAL_RESERVE_ACCOUNT,
                    $sCostCentre,
                    '',
                    $curSubTotalRRDepn,
                    "$sCostCentre/$sIFRSSubType RR depn. $this->yearString"
                );

                $this->addJournalRow(
                    $this->journalData,
                    self::CR,
                    self::CAPITAL_ADJUSTMENT_ACCOUNT,
                    $sCostCentre,
                    '',
                    $curSubTotalRRDepn,
                    "$sCostCentre/$sIFRSSubType RR depn. $this->yearString"
                );
            }
        }

        // If we are doing reversals as grand total
        if (self::TOTAL_REVERSALS) {
            $this->addJournalRow(
                $this->journalData,
                self::DR,
                self::CAPITAL_ADJUSTMENT_ACCOUNT,
                self::ASSET_MANAGEMENT_COST_CENTRE,
                '',
                $curTotalAmount,
                "Depreciation. $this->yearString"
            );

            $this->addJournalRow(
                $this->journalData,
                self::CR,
                self::SMR_ACCOUNT,
                self::ASSET_MANAGEMENT_COST_CENTRE,
                '',
                $curTotalAmount,
                "Depreciation. $this->yearString"
            );
        }
    }

    private function getAllAssetData()
    {
        return VwFar07::userSiteGroup()
        ->leftJoin(
            'ca_ifrs_category as bfwd_ca_ifrs_category',
            'bfwd_ca_ifrs_category.ca_ifrs_category_id',
            '=',
            'vw_far07.bfwd_ca_ifrs_category_id'
        )
        ->join('ca_fixed_asset', 'ca_fixed_asset.ca_fixed_asset_id', '=', 'vw_far07.ca_fixed_asset_id')
        ->join(
            'gen_userdef_group',
            'gen_userdef_group.gen_userdef_group_id',
            '=',
            'ca_fixed_asset.gen_userdef_group_id'
        )
        ->where('vw_far07.ca_balance_historic', 'N')
        //->where('vw_far07.ca_fixed_asset_id', 3416)
        ->select(
            [
                \DB::raw('bfwd_ca_ifrs_category.ca_ifrs_category_desc AS bfwd_ca_ifrs_category_desc'),
                'vw_far07.hra_asset',
                'vw_far07.ca_fixed_asset_id',
                'vw_far07.deminimis_asset',
                //\DB::raw('gen_userdef_group.user_check2 AS rbc_deminimis'),
                \DB::raw('vw_far07.`IFRS Code` as ca_ifrs_category_code'),
                \DB::raw('vw_far07.`IFRS Description` as ca_ifrs_category_desc'),
                \DB::raw('vw_far07.`Previous IFRS Code` as bf_ca_ifrs_category_code'),
                \DB::raw('vw_far07.`Previous IFRS Description` as bf_ca_ifrs_category_desc'),
                \DB::raw('vw_far07.`Asset Code` as fixed_asset_code'),
                \DB::raw('vw_far07.`Asset Description` as fixed_asset_desc'),
                \DB::raw('vw_far07.`Account Code` as account_code'),
                \DB::raw('vw_far07.`CY Depreciation` * -1 as amount'),
                'bfwd_ca_ifrs_category.amortisation as bfwd_amortisation',
            ]
        )
        //->orderByRaw('bfwd_ca_ifrs_category.ca_ifrs_category_desc')
        ->orderByRaw('vw_far07.`IFRS Code`')
        ->orderByRaw('vw_far07.`Account Code`')
        ->get();
    }
}
