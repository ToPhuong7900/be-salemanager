<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\MiltonKeynesJournals;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Math;
use Tfcloud\Models\CaTransactionType;
use Tfcloud\Services\PermissionService;

class FarCliMK10ReportService extends MiltonKeynesJournalPostingService
{
    protected $yearString = "";
    private $journalData = [];

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->addJournalHeader($this->journalData, 'Transfers');
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        /**
         * /////////////////////////////////////////////////
         * // Asset by Asset transfers  //
         * /////////////////////////////////////////////////
         */

        $this->prepareTransferData();

        return $this->journalData;
    }

    private function prepareTransferData()
    {
        $sCostCentre = '';
        $sAccountCode1 = '';
        $sAccountCode2 = '';
        $sAccountCode3 = '';
        $sAccountCode4 = '';

        $sIFRSSubType = '';

        $curAssetValue = 0;
        $curLandValue = 0;
        $curValue = 0;

        // BFwd Accum Balances
        $curBfwdGBV = 0;
        $curBFwdDepn = 0;
        $curBFwdIMP = 0;
        $curBFwdRR = 0;

        // Current year depreciation
        $curCYDepn = 0;
        $curHCCYDepn = 0;


        $sMsg = '';
        $lCount = 0;
        $caFixedAssetId = 0;

        $bIsYearEnd = Common::valueYorNtoBoolean(\Auth::user()->siteGroup->ca_cal_mode_year_end);

        // Get additions data from database
        $assetData = $this->retrieveTransactionList(CaTransactionType::CATEGORY_TRANSFER)
            ->orderByRaw('ca_transaction.ca_fixed_asset_id, ca_transaction_code')
            ->get();

        $ttCode = $this->getTTCode(CaTransactionType::CATEGORY_TRANSFER);

        foreach ($assetData as $asset) {
            $lCount++;

            // Store ValuationKey for this transaction
            $caFixedAssetId = $asset->ca_fixed_asset_id;

            $sIFRSSubType = $asset->ca_ifrs_category_desc;
            $sIFRSCode = $asset->ca_ifrs_category_code;

            $sCostCentre = $asset->ca_account_code;
            $fixedAssetCode = $asset->ca_fixed_asset_code;

            // Get sub-type accounts.
            $this->getIFRSAccount(
                $sIFRSCode,
                $sAccountCode1,
                $sAccountCode2,
                $sAccountCode3,
                $sAccountCode4
            );

            // Get values and reverse -ves to +ves for posting.
            $curAssetValue = $asset->cv_asset;
            $curLandValue = $asset->cv_land;
            $curValue = Math::addCurrency([$curAssetValue, $curLandValue]);

            $sMsgLineEnd = "$sIFRSCode $this->yearString - $fixedAssetCode";

            // Get the BFwd GBV, BFwd Accum Impairment and Depreciation - so we can move the gross balances.
            $this->getBFwdAccumulatedBalances(
                $caFixedAssetId,
                $curBfwdGBV,
                $curBFwdDepn,
                $curBFwdIMP,
                $curCYDepn,
                $curHCCYDepn,
                $curBFwdRR
            );

            if ($bIsYearEnd) {
                $curBFwdDepn = Math::addCurrency([$curBFwdDepn, Math::negateCurrency($curCYDepn)]);
            }

            if ($lCount % 2 == 0) { // Transfer In
                $sMsg = "Tfr in to $sMsgLineEnd";
                $this->addJournalRow(
                    $this->journalData,
                    self::DR,
                    $sAccountCode1,
                    $sCostCentre,
                    $ttCode,
                    $curBfwdGBV,
                    $sMsg
                );

                if (Math::notEqualCurrency($curBFwdDepn, 0)) {
                    $this->addJournalRow(
                        $this->journalData,
                        self::CR,
                        $sAccountCode2,
                        $sCostCentre,
                        $ttCode,
                        $curBFwdDepn,
                        $sMsg
                    );
                }

                if (Math::notEqualCurrency($curBFwdIMP, 0)) {
                    $this->addJournalRow(
                        $this->journalData,
                        self::CR,
                        $sAccountCode3,
                        $sCostCentre,
                        $ttCode,
                        Math::negateCurrency($curBFwdIMP),
                        $sMsg
                    );
                }
            } else {
                $sMsg = "Tfr out of $sMsgLineEnd";
                $this->addJournalRow(
                    $this->journalData,
                    self::CR,
                    $sAccountCode1,
                    $sCostCentre,
                    $ttCode,
                    $curBfwdGBV,
                    $sMsg
                );

                if (Math::notEqualCurrency($curBFwdDepn, 0)) {
                    $this->addJournalRow(
                        $this->journalData,
                        self::DR,
                        $sAccountCode2,
                        $sCostCentre,
                        $ttCode,
                        $curBFwdDepn,
                        $sMsg
                    );
                }
                if (Math::notEqualCurrency($curBFwdIMP, 0)) {
                    $this->addJournalRow(
                        $this->journalData,
                        self::DR,
                        $sAccountCode3,
                        $sCostCentre,
                        $ttCode,
                        Math::negateCurrency($curBFwdIMP),
                        $sMsg
                    );
                }
            }
        }   //foreach asset.

        return;
    }
}
