<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\MiltonKeynesJournals;

use Tfcloud\Lib\Math;
use Tfcloud\Models\CaTransactionType;
use Tfcloud\Services\PermissionService;

class FarCliMK09ReportService extends MiltonKeynesJournalPostingService
{
    protected $yearString = "";
    private $journalData = [];

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->addJournalHeader($this->journalData, 'Reversal of Losses');
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        /**
         * /////////////////////////////////////////////////
         * // Asset by Asset reversal of loss             //
         * /////////////////////////////////////////////////
         */

        $this->prepareROLData();

        return $this->journalData;
    }

    private function prepareROLData()
    {
        $sCostCentre = '';
        $sAccountCode1 = '';
        $sAccountCode2 = '';
        $sAccountCode3 = '';
        $sAccountCode4 = '';

        $sIFRSSubType = '';

        $curAssetValue = 0;
        $curLandValue = 0;
        $curHCAssetValue = 0;
        $curHCLandValue = 0;
        $curValue = 0;
        $curHCValue = 0;
        $curRRValue = 0;

        $curIandECharge = 0;
        $curCAAValue = 0;

        $curDepnAdj = 0;
        $curHCDepnAdj = 0;
        $curRRDepnAdj = 0;

        //$curBFwdDepn = 0;
        $sMsg = '';
        $bHRA_Asset = false;
        $lCount = 0;
        $caFixedAssetId = 0;

        //$bIsYearEnd = Common::valueYorNtoBoolean(\Auth::user()->siteGroup->ca_cal_mode_year_end);

        // Get ROLS data from database
        $assetData = $this->retrieveTransactionList(CaTransactionType::REVERSAL_OF_LOSS)
            ->get();

        $ttCode = $this->getTTCode(CaTransactionType::REVERSAL_OF_LOSS);


        foreach ($assetData as $asset) {
            $lCount++;

            // Store ValuationKey for this transaction
            $caFixedAssetId = $asset->ca_fixed_asset_id;

            $sIFRSSubType = $asset->ca_ifrs_category_desc;
            $sIFRSCode = $asset->ca_ifrs_category_code;

            $sCostCentre = $asset->ca_account_code;

            // Get sub-type accounts.
            $this->getIFRSAccount(
                $sIFRSCode,
                $sAccountCode1,
                $sAccountCode2,
                $sAccountCode3,
                $sAccountCode4
            );

            $bHRA_Asset = $this->isHRA($sIFRSSubType, $sCostCentre);

            // Get values and reverse -ves to +ves for posting.
            $curAssetValue = $asset->cv_asset;
            $curLandValue = $asset->cv_land;
            $curValue = Math::addCurrency([$curAssetValue, $curLandValue]);
            $curHCAssetValue = $asset->hc_asset;
            $curHCLandValue = $asset->hc_land;
            $curHCValue = Math::addCurrency([$curHCAssetValue, $curHCLandValue]);

            // The depreciation reversed.
            $curHCDepnAdj = $asset->hc_depn_adjustment;

            if (Math::equalCurrency($curHCDepnAdj, 0)) {
                $curDepnAdj = 0;
                $curRRDepnAdj = 0;
            } else {
                $curDepnAdj = $asset->cv_depn_adjustment;
                $curRRDepnAdj = Math::subCurrency([$curDepnAdj, $curHCDepnAdj]);
            }

            $sMsgLineEnd = "$sIFRSCode $this->yearString $asset->ca_fixed_asset_code";

            /////// Reverse out the Loss - FA to I&E ///////
            $curIandECharge = 0;



            // Dr Fixed Carrying Amount (amount of loss reversed Gross).
            if (Math::notEqualCurrency($curValue, 0)) {
                $sMsg = "RoL $sMsgLineEnd";
                $this->addJournalRow(
                    $this->journalData,
                    self::DR,
                    $sAccountCode1,
                    $sCostCentre,
                    $ttCode,
                    $curValue,
                    $sMsg
                );
            }

            // Cr I&E with value of total gain (less depn. done below).
            $curIandECharge = $curHCValue;
            if (Math::notEqualCurrency($curIandECharge, 0)) {
                $sMsg = "RoL $sMsgLineEnd";

                $this->addJournalRow(
                    $this->journalData,
                    self::CR,
                    self::REVAL_LOSS_CODE,
                    $sCostCentre,
                    $ttCode,
                    $curIandECharge,
                    $sMsg
                );
            }

            // Cr Reverse out total via RR if from RR.
            //$curRRValue = Math::addCurrency([Math::subCurrency([$curValue, $curHCValue]), $curIandECharge]);
            $curRRValue = Math::subCurrency([$curValue, $curHCValue]);
            if (Math::notEqualCurrency($curRRValue, 0)) {
                $sMsg = "RoL (RR) $sMsgLineEnd";
                $this->addJournalRow(
                    $this->journalData,
                    self::CR,
                    self::REVAL_RESERVE_ACCOUNT,
                    $sCostCentre,
                    $ttCode,
                    $curRRValue,
                    $sMsg
                );
            }

            /////// Depreciation Adjustment ///////

            // Restore any Depreciation charged to I&E.
            if (Math::notEqualCurrency($curDepnAdj, 0)) {
                // Cr Accumulated Depn. for Category.
                $sMsg = "RoL Depn.Adj $sMsgLineEnd";

                $this->addJournalRow(
                    $this->journalData,
                    self::CR,
                    $sAccountCode2,
                    $sCostCentre,
                    $ttCode,
                    $curDepnAdj,
                    $sMsg
                );

                // Dr Service I&E DEPRECIATION with value of depn. adj.
                $this->addJournalRow(
                    $this->journalData,
                    self::DR,
                    self::REVERSAL_OF_DEPN_CODE,
                    $sCostCentre,
                    $ttCode,
                    $curDepnAdj,
                    $sMsg
                );
            }

            // Reverse out Depn. Adj. via RR if from RR.
            if (Math::notEqualCurrency($curRRDepnAdj, 0)) {
                // Cr Accumulated Depn. for Category.
                $sMsg = "RoL Depn.Adj $sMsgLineEnd";

                $this->addJournalRow(
                    $this->journalData,
                    self::CR,
                    self::CAPITAL_ADJUSTMENT_ACCOUNT,
                    $sCostCentre,
                    $ttCode,
                    $curRRDepnAdj,
                    $sMsg
                );

                // Adjust for depreciation in prior years - RR.
                $this->addJournalRow(
                    $this->journalData,
                    self::DR,
                    self::REVAL_RESERVE_ACCOUNT,
                    $sCostCentre,
                    $ttCode,
                    $curRRDepnAdj,
                    $sMsg
                );
            }

            /////// Reverse out any I&E charge to the CAA/SMR ///////
            $curCAAValue = Math::subCurrency([$curIandECharge, $curDepnAdj]);
            $sMsg = " RoL $sMsgLineEnd";

            if (Math::greaterThanCurrency($curCAAValue, 0)) {
                // Credit CAA - gain - depn. adj.
                $this->addJournalRow(
                    $this->journalData,
                    self::CR,
                    self::CAPITAL_ADJUSTMENT_ACCOUNT,
                    $sCostCentre,
                    $ttCode,
                    $curCAAValue,
                    $sMsg
                );

                // Debit SMR (reverse of CAA above).
                $this->addJournalRow(
                    $this->journalData,
                    self::DR,
                    self::SMR_ACCOUNT,
                    $sCostCentre,
                    $ttCode,
                    $curCAAValue,
                    $sMsg
                );
            }

            if (Math::lessThanCurrency($curCAAValue, 0)) {
                $curCAAValue = Math::negateCurrency($curCAAValue);

                // Debit CAA - gain - depn. adj.
                $this->addJournalRow(
                    $this->journalData,
                    self::DR,
                    self::CAPITAL_ADJUSTMENT_ACCOUNT,
                    $sCostCentre,
                    $ttCode,
                    $curCAAValue,
                    $sMsg
                );

                // Credit SMR (reverse of CAA above)
                $this->addJournalRow(
                    $this->journalData,
                    self::CR,
                    self::SMR_ACCOUNT,
                    $sCostCentre,
                    $ttCode,
                    $curCAAValue,
                    $sMsg
                );
            }
        }   //foreach asset.

        return;
    }
}
