<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\MiltonKeynesJournals;

use Tfcloud\Lib\Math;
use Tfcloud\Models\CaTransactionType;
use Tfcloud\Services\PermissionService;

class FarCliMK04ReportService extends MiltonKeynesJournalPostingService
{
    protected $yearString = "";
    private $journalData = [];

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->addJournalHeader($this->journalData, 'Derecognitions');
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        /**
         * /////////////////////////////////////////////////
         * // Asset by Asset derecognition for all assets  //
         * /////////////////////////////////////////////////
         */

        $this->prepareDrcData();

        return $this->journalData;
    }

    private function prepareDrcData()
    {
        $sCostCentre = '';
        $sAccountCode1 = '';
        $sAccountCode2 = '';
        $sAccountCode3 = '';
        $sAccountCode4 = '';

        $sIFRSSubType = '';

        $curAssetValue = 0;
        $curLandValue = 0;
        $curHCAssetValue = 0;
        $curHCLandValue = 0;
        $curValue = 0;
        $curHCValue = 0;
        $curRRValue = 0;

        $curGrossValue = 0;
        $curProceeds = 0;
        $curCosts = 0;

        $sMsg = '';
        $bHRA_Asset = false;
        $lCount = 0;
        $caFixedAssetId = 0;
        $sClass = '';


        // Get Disposals data from database
        $assetData = $this->retrieveTransactionList(CaTransactionType::DERECOGNITION)
            ->orderBy('ca_fixed_asset_id')
            ->get();

        $ttCode = $this->getTTCode(CaTransactionType::DERECOGNITION);

        foreach ($assetData as $asset) {
            $lCount++;

            // Store ValuationKey for this transaction
            $caFixedAssetId = $asset->ca_fixed_asset_id;

            $sIFRSSubType = $asset->ca_ifrs_category_desc;

            $sCostCentre = $asset->ca_account_code;

            // Get sub-type accounts.
            $this->getIFRSAccount(
                $asset->ca_ifrs_category_code,
                $sAccountCode1,
                $sAccountCode2,
                $sAccountCode3,
                $sAccountCode4
            );

            $bHRA_Asset = $this->isHRA($sIFRSSubType, $sCostCentre);

            // Get values and reverse -ves to +ves for posting.
            $curAssetValue = Math::negateCurrency($asset->cv_asset);
            $curLandValue = Math::negateCurrency($asset->cv_land);
            $curValue = Math::addCurrency([$curAssetValue, $curLandValue]);

            $curHCAssetValue = Math::negateCurrency($asset->hc_asset);
            $curHCLandValue = Math::negateCurrency($asset->hc_land);
            $curHCValue = Math::addCurrency([$curHCAssetValue, $curHCLandValue]);

            $sClass = "";

            // Start current value
            $curGrossValue = $curValue;

            $curProceeds = Math::subCurrency([$asset->disposal_proceeds, $asset->disposal_costs]);
            $curCosts = $asset->disposal_costs;

            // Dr Fixed Carrying Amount (amount of loss reversed Gross)
            // Add in amount of depn and impairment to get back to gross amount
            // Cr carrying amount
            // Use cost centre on housing assets so they can be grouped later if needed

            $sMsg = "Derec Fixed Asset $sIFRSSubType $asset->ca_fixed_asset_code";
            $this->addJournalRow(
                $this->journalData,
                self::CR,
                $sAccountCode1,
                $sCostCentre,
                $ttCode,
                $curGrossValue,
                $sMsg
            );

            // DR I&E.
            $sMsg = "$sCostCentre / $sIFRSSubType  Derec (I&E) $this->yearString";
            $this->addJournalRow(
                $this->journalData,
                self::DR,
                self::DISPOSAL_CODE,
                self::DEREC_COST_CENTRE,
                $ttCode,
                $curGrossValue,
                $sMsg
            );

            // Reverse charge to I&E out from GF to CAA
            if ($bHRA_Asset) {
                $sMsg = "Reversal of derec NBV I&E $sCostCentre";
            } else {
                $sMsg = "Reversal of derec NBV I&E $asset->ca_fixed_asset_code";
            }
            $this->addJournalRow(
                $this->journalData,
                self::CR,
                self::SMR_ACCOUNT,
                self::DEREC_COST_CENTRE,
                $ttCode,
                $curGrossValue,
                $sMsg
            );

            $this->addJournalRow(
                $this->journalData,
                self::DR,
                self::CAPITAL_ADJUSTMENT_ACCOUNT,
                $sCostCentre,
                $ttCode,
                $curGrossValue,
                $sMsg
            );

            /////// Clear out Revaluation Reserve ///////
            // Dr RR
            // Cr CAA
            $curRRValue = Math::subCurrency([$curValue, $curHCValue]);

            if (Math::notEqualCurrency($curRRValue, 0)) {
                // Clear RR.
                $sMsg = "$sCostCentre/$sIFRSSubType Disposal (RR) $this->yearString";
                $this->addJournalRow(
                    $this->journalData,
                    self::DR,
                    self::REVAL_RESERVE_ACCOUNT,
                    $sCostCentre,
                    $ttCode,
                    $curRRValue,
                    $sMsg
                );
                $this->addJournalRow(
                    $this->journalData,
                    self::CR,
                    self::CAPITAL_ADJUSTMENT_ACCOUNT,
                    $sCostCentre,
                    $ttCode,
                    $curRRValue,
                    $sMsg
                );
            }
        }   //foreach asset.

        return;
    }
}
