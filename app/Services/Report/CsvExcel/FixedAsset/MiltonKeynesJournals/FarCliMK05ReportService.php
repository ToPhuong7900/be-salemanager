<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\MiltonKeynesJournals;

use DateTime;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Math;
use Tfcloud\Models\CaTransactionType;
use Tfcloud\Services\PermissionService;

class FarCliMK05ReportService extends MiltonKeynesJournalPostingService
{
    protected $yearString = "";
    private $journalData = [];

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->addJournalHeader($this->journalData, 'Disposals');
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        /**
         * /////////////////////////////////////////////////
         * // Asset by Asset disposals for all assets  //
         * /////////////////////////////////////////////////
         */

        $this->prepareDspData();

        return $this->journalData;
    }

    private function prepareDspData()
    {
        $sCostCentre = '';
        $sAccountCode1 = '';
        $sAccountCode2 = '';
        $sAccountCode3 = '';
        $sAccountCode4 = '';

        $sIFRSSubType = '';

        $curAssetValue = 0;
        $curLandValue = 0;
        $curHCAssetValue = 0;
        $curHCLandValue = 0;
        $curValue = 0;
        $curHCValue = 0;
        $curRRValue = 0;

        $curGrossValue = 0;
        $curBFwdGBV = 0;

        $curBFwdImp = 0;
        $curBFwdDepn = 0;
        $curBFwdRR = 0;

        $curCYDepn = 0;
        $curCYHCDepn = 0;

        $sMsg = '';
        $bHRA_Asset = false;
        $lCount = 0;
        $caFixedAssetId = 0;
        $sClass = '';

        // Get Disposals data from database
        $assetData = $this->retrieveTransactionList(CaTransactionType::DISPOSAL)
            ->orderBy('ca_fixed_asset_id')
            ->get();

        $ttCode = $this->getTTCode(CaTransactionType::DISPOSAL);

        foreach ($assetData as $asset) {
            $lCount++;

            // Store ValuationKey for this transaction
            $caFixedAssetId = $asset->ca_fixed_asset_id;

            $sIFRSSubType = $asset->ca_ifrs_category_desc;
            $sIFRSCode = $asset->ca_ifrs_category_code;

            $sCostCentre = $asset->ca_account_code;

            $sMsgLineEnd = "$sIFRSCode $this->yearString - $asset->ca_fixed_asset_code";

            // Get sub-type accounts.
            $this->getIFRSAccount(
                $sIFRSCode,
                $sAccountCode1,
                $sAccountCode2,
                $sAccountCode3,
                $sAccountCode4
            );

            $bHRA_Asset = $this->isHRA($sIFRSSubType, $sCostCentre);

            // Get values and reverse -ves to +ves for posting.
            $curAssetValue = Math::negateCurrency($asset->cv_asset);
            $curLandValue = Math::negateCurrency($asset->cv_land);
            $curValue = Math::addCurrency([$curAssetValue, $curLandValue]);

            $curHCAssetValue = Math::negateCurrency($asset->hc_asset);
            $curHCLandValue = Math::negateCurrency($asset->hc_land);
            $curHCValue = Math::addCurrency([$curHCAssetValue, $curHCLandValue]);


            $curBFwdDepn = 0;
            $curBFwdImp = 0;
            $curCYDepn = 0;
            $curCYHCDepn = 0;

            $sClass = "";

            /////// Write out the carrying amount of the Fixed Asset  ///////
            $this->getBFwdAccumulatedBalances(
                $caFixedAssetId,
                $curBFwdGBV,
                $curBFwdDepn,
                $curBFwdImp,
                $curCYDepn,
                $curCYHCDepn,
                $curBFwdRR
            );

            // Start current value
            $curGrossValue = $curValue;

            if (Common::valueYorNtoBoolean($asset->effective_date_override)) {
                $effectiveDate = (new DateTime($asset->effective_date));
                $bIsYearEnd = ($effectiveDate->format('d/m') == '31/03');
            } else {
                $bIsYearEnd = Common::valueYorNtoBoolean(\Auth::user()->siteGroup->ca_cal_mode_year_end);
            }

            if ($bIsYearEnd) {
                $curCYDepn = Math::negateCurrency($curCYDepn);
                if (Math::notEqualCurrency($curCYDepn, 0)) {
                    $sMsg = "Disposal CYrDepn. W/Off $sMsgLineEnd";
                    $this->addJournalRow(
                        $this->journalData,
                        self::DR,
                        $sAccountCode2,
                        $sCostCentre,
                        $ttCode,
                        $curCYDepn,
                        $sMsg
                    );
                    $curGrossValue = Math::addCurrency([$curGrossValue, $curCYDepn]);
                }
            }

            // If asset has reval in year then that will clear bfwd imp/depn
            // otherwise we need to clear balances here.
            // Write out depreciation balances.
            if (!$this->hasReval($asset->ca_fixed_asset_id)) {
                if (Math::notEqualCurrency($curBFwdDepn, 0)) {
                    $sMsg = "Disposal CumDepn W/Off $sMsgLineEnd";
                    $this->addJournalRow(
                        $this->journalData,
                        self::DR,
                        $sAccountCode2,
                        $sCostCentre,
                        $ttCode,
                        $curBFwdDepn,
                        $sMsg
                    );
                }

                if (Math::notEqualCurrency($curBFwdImp, 0)) {
                    $sMsg = "Disposal AccumImp W/Off $sMsgLineEnd";
                    $this->addJournalRow(
                        $this->journalData,
                        self::DR,
                        $sAccountCode3,
                        $sCostCentre,
                        $ttCode,
                        $curBFwdImp,
                        $sMsg
                    );
                }

                $curGrossValue = Math::addCurrency([$curGrossValue, $curBFwdDepn, $curBFwdImp]);
            }

            // Dr Fixed Carrying Amount (amount of loss reversed Gross)
            // Add in amount of depn and impairment to get back to gross amount
            // Cr carrying amount.
            $sMsg = "Disposal $sMsgLineEnd";
            $this->addJournalRow(
                $this->journalData,
                self::CR,
                $sAccountCode1,
                $sCostCentre,
                $ttCode,
                $curGrossValue,
                $sMsg
            );

            // DR I&E
            $this->addJournalRow(
                $this->journalData,
                self::DR,
                self::CAPITAL_ADJUSTMENT_ACCOUNT,
                $sCostCentre,
                $ttCode,
                $curValue,
                $sMsg
            );

            // Reverse charge to I&E out from GF to CAA
            $sMsg = "C Value on Disposal $sMsgLineEnd";
            $this->addJournalRow(
                $this->journalData,
                self::CR,
                self::SMR_ACCOUNT,
                self::DEREC_COST_CENTRE,
                $ttCode,
                $curValue,
                $sMsg
            );
            $this->addJournalRow(
                $this->journalData,
                self::DR,
                self::DISPOSAL_CODE,
                self::DEREC_COST_CENTRE,
                $ttCode,
                $curValue,
                $sMsg
            );

            /////// Clear out Revaluation Reserve ///////
            // Dr RR
            // Cr CAA
            $curRRValue = Math::subCurrency([$curValue, $curHCValue]);

            if (Math::notEqualCurrency($curRRValue, 0)) {
                // Clear RR.
                $sMsg = "Disposal $this->yearString";
                $this->addJournalRow(
                    $this->journalData,
                    self::DR,
                    self::REVAL_RESERVE_ACCOUNT,
                    $sCostCentre,
                    $ttCode,
                    $curRRValue,
                    $sMsg
                );

                $sMsg = "Disposal RR $this->yearString";
                $this->addJournalRow(
                    $this->journalData,
                    self::CR,
                    self::CAPITAL_ADJUSTMENT_ACCOUNT,
                    $sCostCentre,
                    $ttCode,
                    $curRRValue,
                    $sMsg
                );
            }
        }   //foreach asset.
    }
}
