<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\MiltonKeynesJournals;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Math;
use Tfcloud\Lib\FixedAsset\CaFinYearManager;
use Tfcloud\Models\CaTransactionType;
use Tfcloud\Models\Views\VwFar07;
use Tfcloud\Services\PermissionService;

class FarCliMK07ReportService extends MiltonKeynesJournalPostingService
{
    protected $yearString = "";
    private $journalData = [];

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->addJournalHeader($this->journalData, 'Revaluations');
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        /**
         * /////////////////////////////////////////////////
         * // Valuation Transaction Journal               //
         * /////////////////////////////////////////////////
         */

        $this->prepareRevaluationData();

        return $this->journalData;
    }

    private function prepareRevaluationData()
    {
        $sCostCentre = '';
        $sAccountCode1 = '';
        $sAccountCode2 = '';
        $sAccountCode3 = '';
        $sAccountCode4 = '';

        $bNoHistoricCost = false;

        $sIFRSSubType = '';
        $sIFRSCode = '';

        $curRevAmountAsset = 0;
        $curRevAmountLand = 0;
        $curRevAmountHCAsset = 0;
        $curRevAmountHCLand = 0;

        $curBfwdDepn = 0;
        $curBfwdImp = 0;

        $curRRGain = 0;
        $curRRGainAsset = 0;
        $curRRGainLand = 0;

        $curIandECharge = 0;
        $curBalanceFixedAsset = 0;

        $curReverseDepnAdj = 0;
        //$curROIReverseDepnAdj = 0;

        $sMsgLine = " Impairment $this->yearString";
        //$bHRAAsset = false;
        $lCount = 0;

        $caFixedAssetId = 0;

        //$bHeritageAsset = false;

        $bHasYearEndReval = Common::valueYorNtoBoolean(\Auth::user()->siteGroup->ca_cal_mode_year_end);
        $bHasTransfer = false;
        $bHasDepnAdj = $this->hasCVDepnAdj();

        // Get valuation data from database
        $assetData = $this->getAllAssetData();

        $ttCode = $this->getTTCode(CaTransactionType::REVALUATION);

        foreach ($assetData as $asset) {
            $lCount++;

            //$bHasYearEndReval = false;   // Clear flags just to be safe
            $bHasTransfer = false;

            $caFixedAssetId = $asset->ca_fixed_asset_id;
            $fixedAssetCode = $asset->ca_fixed_asset_code;
            $historicAssetData = $this->getHistoricAssetData($caFixedAssetId);

            $curRevAmountAsset = $asset->CREVAV;
            $curRevAmountLand = $asset->CREVLV;
            $curRevAmountHCAsset = $historicAssetData->HREVAV;
            $curRevAmountHCLand = $historicAssetData->HREVLV;

            $sIFRSSubType = $asset->ca_ifrs_category_desc;
            $sIFRSCode = $asset->ca_ifrs_category_code;

            // Get sub-type accounts.
            $this->getIFRSAccount(
                $sIFRSCode,
                $sAccountCode1,
                $sAccountCode2,
                $sAccountCode3,
                $sAccountCode4,
                $bNoHistoricCost
            );

            // Get cost centre for asset/valuation record
            $sCostCentre = $asset->account_code;

            // Keep track of balance to post to fixed asset
            $curBalanceFixedAsset = 0;

            //$bHRAAsset = $this->isHRA($sIFRSSubType, $sCostCentre);
            //$bHeritageAsset = $this->isHeritage($sIFRSSubType);

            $sMsgLineEnd = " $sIFRSCode $this->yearString $fixedAssetCode";

            /////// Accumulated Depreciation W/Off ///////

            // Get BFwd Accum. Depn.
            if ($bHasYearEndReval) {
                $curBfwdDepn = Math::addCurrency([$asset->COpenDPN, Math::negateCurrency($asset->CYDEPN)]);
            } else {
                $curBfwdDepn = $asset->COpenDPN;
            }

            if (Math::notEqualCurrency($curBfwdDepn, 0)) {
                // Write off Accum. Depn.
                $sMsgLine = "W/Off depn $sMsgLineEnd";
                $this->addJournalRow(
                    $this->journalData,
                    self::DR,
                    $sAccountCode2,
                    $sCostCentre,
                    $ttCode,
                    $curBfwdDepn,
                    $sMsgLine
                );

                // Add to balance to post.
                $curBalanceFixedAsset = Math::addCurrency([$curBalanceFixedAsset, $curBfwdDepn]);
            }

            /////// Accumulated Impairment W/Off ///////
            // Get BFwd Accum. Impairment.
//            if ($bHasYearEndReval) {
//                $curBfwdImp = Math::addCurrency(
//                    [
//                        $asset->COpenIMPA,
//                        $asset->COpenIMPL,
//                        $asset->CIMPAV,
//                        $asset->CIMPLV,
//                        Math::negateCurrency($asset->CROIAV),
//                        Math::negateCurrency($asset->CROILV)
//                    ]
//                );
//                \Log::error("COpenIMPA: $asset->COpenIMPA");
//                \Log::error("COpenIMPL: $asset->COpenIMPL");
//                \Log::error("CIMPAV: $asset->CIMPAV");
//                \Log::error("CIMPLV: $asset->CIMPLV");
//                \Log::error("CROIAV: $asset->CROIAV");
//                \Log::error("CROILV: $asset->CROILV");
//
//            } else {
//                $curBfwdImp = Math::addCurrency(
//                    [
//                        $asset->COpenIMPA,
//                        $asset->COpenIMPL
//                    ]
//                );
//            }

            /// Get the W/Off Impairment for the asset from FAR06.
            $revalAmounts = $this->getRevalAmounts($fixedAssetCode);
            $curBfwdImp = Math::addCurrency([$revalAmounts->imp_woff_rr, $revalAmounts->imp_woff_sd]);


            // If we have a Year-End revaluation then write of current year impairment as well
            // and if we have ROI we have to include that as well
            // Note negative signs on impairments!

            $curReverseDepnAdj = 0;

            if ($bHasYearEndReval) {
                if (
                    Math::notEqualCurrency($asset->CROIAV, 0) ||
                    Math::notEqualCurrency($asset->CROILV, 0) ||
                    Math::notEqualCurrency($asset->CROLAV, 0) ||
                    Math::notEqualCurrency($asset->CROLLV, 0)
                ) {
                    //if ($bHasDepnAdj) {
                        $curReverseDepnAdj = $this->getCVDepnAdj(
                            $caFixedAssetId,
                            [CaTransactionType::REVERSAL_OF_IMPAIRMENT, CaTransactionType::REVERSAL_OF_LOSS]
                        );
                    //}
                }

                // Not required as amount included in FAR06 Calulations.
                // Only impairments.
//                if (
//                    Math::notEqualCurrency($asset->CROILV, 0) ||
//                    Math::notEqualCurrency($asset->CROIAV, 0)
//                ) {
//                    //if ($bHasDepnAdj) {
//                        $curROIReverseDepnAdj = $this->getCVDepnAdj(
//                            $caFixedAssetId,
//                            [CaTransactionType::REVERSAL_OF_IMPAIRMENT]
//                        );
//                    //}
//
//                    // The impairment reversal values above are net of depn. so add back on depn.
//                    //$curBfwdImp = Math::addCurrency([$curBfwdImp, Math::negateCurrency($curROIReverseDepnAdj)]);
//                }

                // If we have a depn. adjustment in year then add that in.
                if (Math::notEqualCurrency($curReverseDepnAdj, 0)) {
                    $this->addJournalRow(
                        $this->journalData,
                        self::DR,
                        $sAccountCode2,
                        $sCostCentre,
                        $ttCode,
                        $curReverseDepnAdj,
                        "W/off depn ROI/ROL $sMsgLineEnd"
                    );

                    // As we write off more depreciation this impacts the other half of journal.
                    $curBalanceFixedAsset = Math::addCurrency([$curBalanceFixedAsset, $curReverseDepnAdj]);
                }
            }

            // Write off any Accum. Imp.
            if (Math::notEqualCurrency($curBfwdImp, 0)) {
                // MiltonKeynes put impairment on FA account (hence account1 not 3).
                $sMsgLine = "W/Off impairment $sMsgLineEnd";

                $this->addJournalRow(
                    $this->journalData,
                    self::DR,
                    $sAccountCode3,
                    $sCostCentre,
                    $ttCode,
                    $curBfwdImp,
                    $sMsgLine
                );
                $curBalanceFixedAsset = Math::addCurrency([$curBalanceFixedAsset, $curBfwdImp]);
            }

            /////// Revaluation Reserve ///////

            // If Investment then no HC so no RR.
            if ($bNoHistoricCost) {
                $curRRGainAsset = 0;
                $curRRGainLand = 0;
            } else {
                // Work out any gain in RR.
                $curRRGainAsset = Math::subCurrency([$curRevAmountAsset, $curRevAmountHCAsset]);
                $curRRGainLand = Math::subCurrency([$curRevAmountLand, $curRevAmountHCLand]);
            }

            // DR any movement in RR.
            $curRRGain = Math::addCurrency([$curRRGainAsset, $curRRGainLand]);

            if (Math::notEqualCurrency($curRRGain, 0)) {
                if (Math::greaterThanCurrency($curRRGain, 0)) {
                    $sMsgLine = "Reval Gain RR $sMsgLineEnd";

                    $this->addJournalRow(
                        $this->journalData,
                        self::CR,
                        self::REVAL_RESERVE_ACCOUNT,
                        $sCostCentre,
                        $ttCode,
                        $curRRGain,
                        $sMsgLine
                    );
                } else {
                    $sMsgLine = "Reval Loss RR for asset $sMsgLineEnd";
                    $this->addJournalRow(
                        $this->journalData,
                        self::DR,
                        self::REVAL_RESERVE_ACCOUNT,
                        $sCostCentre,
                        $ttCode,
                        Math::negateCurrency($curRRGain),
                        $sMsgLine
                    );
                }
                $curBalanceFixedAsset = Math::subCurrency([$curBalanceFixedAsset, $curRRGain]);
            }

            /////// Charge to I&E ///////

            // Investment class Assets gain/loss always to I&E
            if ($bNoHistoricCost) {
                $curIandECharge = Math::addCurrency([$curRevAmountAsset, $curRevAmountLand]);

                if (Math::notEqualCurrency($curIandECharge, 0)) {
                    if (Math::greaterThanCurrency($curIandECharge, 0)) {
                        $sMsgLine = "Investment Gain I&E $sMsgLineEnd";
                        $this->addJournalRow(
                            $this->journalData,
                            self::CR,
                            self::REVAL_LOSS_CODE,
                            $sCostCentre,
                            $ttCode,
                            $curIandECharge,
                            $sMsgLine
                        );

                        $curBalanceFixedAsset = Math::subCurrency([$curBalanceFixedAsset, $curIandECharge]);

                        $this->addJournalRow(
                            $this->journalData,
                            self::CR,
                            self::CAPITAL_ADJUSTMENT_ACCOUNT,
                            $sCostCentre,
                            $ttCode,
                            $curIandECharge,
                            $sMsgLine
                        );
                        $this->addJournalRow(
                            $this->journalData,
                            self::DR,
                            self::SMR_ACCOUNT,
                            $sCostCentre,
                            $ttCode,
                            $curIandECharge,
                            $sMsgLine
                        );
                    } else {
                        $curIandECharge = Math::negateCurrency($curIandECharge);

                        $sMsgLine = "Investment Loss I&E $sMsgLineEnd";
                        $this->addJournalRow(
                            $this->journalData,
                            self::DR,
                            self::REVAL_LOSS_CODE,
                            $sCostCentre,
                            $ttCode,
                            $curIandECharge,
                            $sMsgLine
                        );

                        $curBalanceFixedAsset = Math::addCurrency([$curBalanceFixedAsset, $curIandECharge]);

                        $this->addJournalRow(
                            $this->journalData,
                            self::DR,
                            self::REVAL_LOSS_CODE,
                            $sCostCentre,
                            $ttCode,
                            $curIandECharge,
                            $sMsgLine
                        );
                        $this->addJournalRow(
                            $this->journalData,
                            self::CR,
                            self::SMR_ACCOUNT,
                            $sCostCentre,
                            $ttCode,
                            $curIandECharge,
                            $sMsgLine
                        );
                    }
                }
            } else {    // Non-investment assets //
                // DR any HC loss charge to I&E.
                $curIandECharge = Math::negateCurrency(Math::addCurrency([$curRevAmountHCAsset, $curRevAmountHCLand]));

                if (Math::notEqualCurrency($curIandECharge, 0)) {
                    $sMsgLine = "Reval Loss I&E $sMsgLineEnd";

                    $this->addJournalRow(
                        $this->journalData,
                        self::DR,
                        self::REVAL_LOSS_CODE,
                        $sCostCentre,
                        $ttCode,
                        $curIandECharge,
                        $sMsgLine
                    );

                    $curBalanceFixedAsset = Math::addCurrency([$curBalanceFixedAsset, $curIandECharge]);

                    // Reverse through CAA to SMR.
                    $this->addJournalRow(
                        $this->journalData,
                        self::DR,
                        self::CAPITAL_ADJUSTMENT_ACCOUNT,
                        $sCostCentre,
                        $ttCode,
                        $curIandECharge,
                        $sMsgLine
                    );

                    $this->addJournalRow(
                        $this->journalData,
                        self::CR,
                        self::SMR_ACCOUNT,
                        $sCostCentre,
                        $ttCode,
                        $curIandECharge,
                        $sMsgLine
                    );
                }
            }

            /////// Post Balance to Fixed Asset ///////

            $sMsgLine = "Reval $sMsgLineEnd";

            // = ADepn + AImp + I&E - RR
            if (Math::lessThanCurrency($curBalanceFixedAsset, 0)) {
                $curBalanceFixedAsset = Math::negateCurrency($curBalanceFixedAsset);

                $this->addJournalRow(
                    $this->journalData,
                    self::DR,
                    $sAccountCode1,
                    $sCostCentre,
                    $ttCode,
                    $curBalanceFixedAsset,
                    $sMsgLine
                );
            } else {
                $this->addJournalRow(
                    $this->journalData,
                    self::CR,
                    $sAccountCode1,
                    $sCostCentre,
                    $ttCode,
                    $curBalanceFixedAsset,
                    $sMsgLine
                );
            }
        }   //foreach asset.

        return;
    }

    private function getAllAssetData()
    {
        return VwFar07::userSiteGroup()
            ->leftJoin(
                'ca_ifrs_category as bfwd_ca_ifrs_category',
                'bfwd_ca_ifrs_category.ca_ifrs_category_id',
                '=',
                'vw_far07.bfwd_ca_ifrs_category_id'
            )
            ->where('vw_far07.ca_balance_historic', 'N')
            //->whereIn('vw_far07.ca_fixed_asset_id', [3809])
            ->whereRaw('ca_fixed_asset_id IN (SELECT DISTINCT MYCAT.ca_fixed_asset_id '
                . 'FROM ca_transaction MYCAT WHERE MYCAT.ca_transaction_type_id = '
                . CaTransactionType::REVALUATION . ' AND MYCAT.ca_fin_year_id = '
                . CaFinYearManager::currentFinYearId() . ')')
            ->select(
                [
                    \DB::raw('bfwd_ca_ifrs_category.ca_ifrs_category_desc AS bfwd_ca_ifrs_category_desc'),
                    'vw_far07.hra_asset',
                    'vw_far07.ca_fixed_asset_id',
                    'vw_far07.deminimis_asset',
                    \DB::raw('vw_far07.`REV Asset Value` as CREVAV'),
                    \DB::raw('vw_far07.`REV Land Value` as CREVLV'),
                    \DB::raw('vw_far07.`Accumulated Depreciation` as COpenDPN'),
                    \DB::raw('vw_far07.`CY Depreciation` as CDPN'),
                    \DB::raw('vw_far07.`CFwd Accumulated Depreciation` as CCFwdDPN'),
                    \DB::raw('vw_far07.`Accumulated Impairment Asset` as COpenIMPA'),
                    \DB::raw('vw_far07.`Accumulated Impairment Land` as COpenIMPL'),
                    \DB::raw('vw_far07.`IMP Land Value` as CIMPLV'),
                    \DB::raw('vw_far07.`IMP Asset Value` as CIMPAV'),
                    \DB::raw('vw_far07.`ROI Land Value` as CROILV'),
                    \DB::raw('vw_far07.`ROI Asset Value` as CROIAV'),
                    \DB::raw('vw_far07.`ROL Land Value` as CROLLV'),
                    \DB::raw('vw_far07.`ROL Asset Value` as CROLAV'),
                    \DB::raw('vw_far07.`IFRS Code` as ca_ifrs_category_code'),
                    \DB::raw('vw_far07.`IFRS Description` as ca_ifrs_category_desc'),
                    \DB::raw('vw_far07.`Previous IFRS Code` as bf_ca_ifrs_category_code'),
                    \DB::raw('vw_far07.`Previous IFRS Description` as bf_ca_ifrs_category_desc'),
                    \DB::raw('vw_far07.`Asset Code` as ca_fixed_asset_code'),
                    \DB::raw('vw_far07.`Asset Description` as ca_fixed_asset_desc'),
                    \DB::raw('vw_far07.`Account Code` as account_code'),
                    \DB::raw('vw_far07.`CY Depreciation` as CYDEPN'),
                    'bfwd_ca_ifrs_category.amortisation as bfwd_amortisation',
                ]
            )
            ->orderBy(\DB::raw('bfwd_ca_ifrs_category.ca_ifrs_category_desc'))
            ->orderBy(\DB::raw('vw_far07.`Account Code`'))
            ->get();
    }
}
