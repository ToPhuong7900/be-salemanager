<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\MiltonKeynesJournals;

use Tfcloud\Lib\Math;
use Tfcloud\Models\CaTransactionType;
use Tfcloud\Services\PermissionService;

class FarCliMK06ReportService extends MiltonKeynesJournalPostingService
{
    protected $yearString = "";
    private $journalData = [];

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->addJournalHeader($this->journalData, 'Impairments');
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        /**
         * /////////////////////////////////////////////////
         * // Impairments for all assets  //
         * /////////////////////////////////////////////////
         */

        $this->prepareImpairmentData();

        return $this->journalData;
    }

    private function prepareImpairmentData()
    {
        $sCostCentre = '';
        $sAccountCode1 = '';
        $sAccountCode2 = '';
        $sAccountCode3 = '';
        $sAccountCode4 = '';

        $curAssetValue = 0;
        $curLandValue = 0;
        $curHCAssetValue = 0;
        $curHCLandValue = 0;
        $curValue = 0;
        $curHCValue = 0;

        $lCount = 0;

        // Get additions data from database
        $assetData = $this->retrieveTransactionList(CaTransactionType::IMPAIRMENT)->get();

        $ttCode = $this->getTTCode(CaTransactionType::IMPAIRMENT);

        foreach ($assetData as $asset) {
            $lCount++;

            $sIFRSCode = $asset->ca_ifrs_category_code;

            $sCostCentre = $asset->ca_account_code;


            // Get sub-type accounts.
            $this->getIFRSAccount(
                $sIFRSCode,
                $sAccountCode1,
                $sAccountCode2,
                $sAccountCode3,
                $sAccountCode4
            );

            // Get values (already positive no need to negate).
            $curAssetValue = $asset->cv_asset;
            $curLandValue = $asset->cv_land;
            $curHCAssetValue = $asset->hc_asset;
            $curHCLandValue = $asset->hc_land;

            $curValue = Math::addCurrency([$curAssetValue, $curLandValue]);
            $curHCValue = Math::addCurrency([$curHCAssetValue, $curHCLandValue]);

            $sMsgLineEnd = "$sIFRSCode $this->yearString - $asset->ca_fixed_asset_code";
            $sMsg = "Impairment $sMsgLineEnd";


            // Credit Accumlated Impairment.
            $this->addJournalRow(
                $this->journalData,
                self::CR,
                $sAccountCode3,
                $sCostCentre,
                $ttCode,
                $curValue,
                $sMsg
            );

            // RR value = CV - HC values
            $curRRValue = Math::subCurrency([$curValue, $curHCValue]);

            // Debit Reval Reserve (if any debited)
            if (Math::greaterThanCurrency($curRRValue, 0)) {
                $this->addJournalRow(
                    $this->journalData,
                    self::DR,
                    self::REVAL_RESERVE_ACCOUNT,
                    $sCostCentre,
                    $ttCode,
                    $curRRValue,
                    $sMsg
                );
            }


            // Debit I&E if more than RR.
            if (Math::greaterThanCurrency($curHCValue, 0)) {
                $this->addJournalRow(
                    $this->journalData,
                    self::DR,
                    self::IMPAIRMENT_LOSS_CODE,
                    $sCostCentre,
                    $ttCode,
                    $curHCValue,
                    $sMsg
                );

                // Debit CAA.
                $this->addJournalRow(
                    $this->journalData,
                    self::DR,
                    self::CAPITAL_ADJUSTMENT_ACCOUNT,
                    $sCostCentre,
                    $ttCode,
                    $curHCValue,
                    $sMsg
                );

                // Credit SMR
                $this->addJournalRow(
                    $this->journalData,
                    self::CR,
                    self::SMR_ACCOUNT,
                    $sCostCentre,
                    $ttCode,
                    $curHCValue,
                    $sMsg
                );
            }
        }   //foreach asset.
    }
}
