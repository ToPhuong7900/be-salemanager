<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\MiltonKeynesJournals;

use Tfcloud\Lib\Math;
use Tfcloud\Models\CaTransactionType;
use Tfcloud\Services\PermissionService;

class FarCliMK01ReportService extends MiltonKeynesJournalPostingService
{
    protected $yearString = "";
    private $journalData = [];

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->addJournalHeader($this->journalData, 'Additions');
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        /**
         * /////////////////////////////////////////////////
         * // Asset by Asset additions for all assets  //
         * /////////////////////////////////////////////////
         */

        $this->prepareAdditionData();

        return $this->journalData;
    }

    private function prepareAdditionData()
    {
        $sCostCentre = '';
        $sAccountCode1 = '';
        $sAccountCode2 = '';
        $sAccountCode3 = '';
        $sAccountCode4 = '';

        $sIFRSSubType = '';

        $curAssetValue = 0;
        $curLandValue = 0;

        $curValue = 0;

        $lCount = 0;

        // Get additions data from database
        $assetData = $this->retrieveTransactionList(CaTransactionType::ADDITION)->get();

        $ttCode = $this->getTTCode(CaTransactionType::ADDITION);

        foreach ($assetData as $asset) {
            $lCount++;

            $sIFRSSubType = $asset->ca_ifrs_category_code;

            $sCostCentre = $asset->ca_account_code;

            // Get sub-type accounts.
            $this->getIFRSAccount(
                $asset->ca_ifrs_category_code,
                $sAccountCode1,
                $sAccountCode2,
                $sAccountCode3,
                $sAccountCode4
            );

            $curAssetValue = $asset->cv_asset;
            $curLandValue = $asset->cv_land;

            $curValue = Math::addCurrency([$curAssetValue, $curLandValue]);

            $journalText = "$sIFRSSubType $this->yearString - $asset->ca_fixed_asset_code";

            // If a credit (-ve) then swap sign and swap Cr/Dr indicators.
            if (Math::lessThanCurrency($curValue, 0)) {
                $this->addJournalRow(
                    $this->journalData,
                    'Cr',
                    $sAccountCode1,
                    $sCostCentre,
                    $ttCode,
                    Math::negateCurrency($curValue),
                    $journalText
                );

                $this->addJournalRow(
                    $this->journalData,
                    'Dr',
                    self::CASH_CREDITOR,
                    self::ASSET_MANAGEMENT_COST_CENTRE,
                    $ttCode,
                    Math::negateCurrency($curValue),
                    $journalText
                );
            } else {
                // Normal positive addition postings.
                $this->addJournalRow(
                    $this->journalData,
                    'Dr',
                    $sAccountCode1,
                    $sCostCentre,
                    $ttCode,
                    $curValue,
                    $journalText
                );

                $this->addJournalRow(
                    $this->journalData,
                    'Cr',
                    self::CASH_CREDITOR,
                    self::ASSET_MANAGEMENT_COST_CENTRE,
                    $ttCode,
                    $curValue,
                    $journalText
                );
            }
        }   //foreach asset.
    }
}
