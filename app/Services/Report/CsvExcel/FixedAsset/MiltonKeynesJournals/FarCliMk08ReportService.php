<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\MiltonKeynesJournals;

use Tfcloud\Lib\Math;
use Tfcloud\Models\CaTransactionType;
use Tfcloud\Services\PermissionService;

class FarCliMK08ReportService extends MiltonKeynesJournalPostingService
{
    protected $yearString = "";
    private $journalData = [];

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->addJournalHeader($this->journalData, 'Reversal of Impairments');
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        /**
         * /////////////////////////////////////////////////
         * // Asset by Asset Reversal of Impairment       //
         * /////////////////////////////////////////////////
         */

        $this->prepareROIData();

        return $this->journalData;
    }

    private function prepareROIData()
    {
        $sCostCentre = '';
        $sAccountCode1 = '';
        $sAccountCode2 = '';
        $sAccountCode3 = '';
        $sAccountCode4 = '';

        $sIFRSSubType = '';

        $curAssetValue = 0;
        $curLandValue = 0;
        $curHCAssetValue = 0;
        $curHCLandValue = 0;
        $curValue = 0;
        $curHCValue = 0;
        $curRRValue = 0;

        $curIandECharge = 0;
        $curCAAValue = 0;

        $curDepnAdj = 0;

        $bHRA_Asset = false;
        $lCount = 0;
        $caFixedAssetId = 0;

        // Get Impairments data from database
        $assetData = $this->retrieveTransactionList(CaTransactionType::REVERSAL_OF_IMPAIRMENT)
            ->get();

        $ttCode = $this->getTTCode(CaTransactionType::REVERSAL_OF_IMPAIRMENT);

        foreach ($assetData as $asset) {
            $lCount++;

            // Store ValuationKey for this transaction
            $caFixedAssetId = $asset->ca_fixed_asset_id;

            $sIFRSSubType = $asset->ca_ifrs_category_desc;
            $sIFRSCode = $asset->ca_ifrs_category_code;

            $sCostCentre = $asset->ca_account_code;

            // Get sub-type accounts.
            $this->getIFRSAccount(
                $sIFRSCode,
                $sAccountCode1,
                $sAccountCode2,
                $sAccountCode3,
                $sAccountCode4
            );

            $bHRA_Asset = $this->isHRA($sIFRSSubType, $sCostCentre);

            // Get values and reverse -ves to +ves for posting.
            $curAssetValue = $asset->cv_asset;
            $curLandValue = $asset->cv_land;
            $curValue = Math::addCurrency([$curAssetValue, $curLandValue]);
            $curHCAssetValue = $asset->hc_asset;
            $curHCLandValue = $asset->hc_land;
            $curHCValue = Math::addCurrency([$curHCAssetValue, $curHCLandValue]);

            $sMsgLineEnd = "$sIFRSCode $this->yearString $asset->ca_fixed_asset_code";

            // The depreciation reversed.
            $curDepnAdj = $asset->hc_depn_adjustment;

            /////// Reverse out the Impairment - Accum Imp to I&E ///////


            // Dr Fixed Carrying Amount (amount of loss reversed Gross).
            if (Math::notEqualCurrency($curValue, 0)) {
                $sMsg = "RoI Accum.Imp. $sMsgLineEnd";
                $this->addJournalRow(
                    $this->journalData,
                    self::DR,
                    $sAccountCode3,
                    $sCostCentre,
                    $ttCode,
                    $curValue,
                    $sMsg
                );
            }

            // Cr I&E with value of total gain (less depn. done below).
            $curIandECharge = $curHCValue;
            if (Math::notEqualCurrency($curIandECharge, 0)) {
                $sMsg = "RoI $sMsgLineEnd";

                $this->addJournalRow(
                    $this->journalData,
                    self::CR,
                    self::IMPAIRMENT_LOSS_CODE,
                    $sCostCentre,
                    $ttCode,
                    $curIandECharge,
                    $sMsg
                );
            }

            /////// Reverse out total via RR to change revaluation transaction errors which picks up total ///////
            $curRRValue = Math::subCurrency([$curValue, $curHCValue]);


            // RR with value of reversal.
            if (Math::notEqualCurrency($curRRValue, 0)) {
                // Cr Accumulated Depn. for Category.
                $sMsg = "RoI (RR Adj.) $sMsgLineEnd";

                $this->addJournalRow(
                    $this->journalData,
                    self::CR,
                    self::REVAL_RESERVE_ACCOUNT,
                    $sCostCentre,
                    $ttCode,
                    $curRRValue,
                    $sMsg
                );
            }

            /////// Depreciation Adjustment ///////

            if (Math::notEqualCurrency($curDepnAdj, 0)) {
                // Cr Accumulated Depn.
                $sMsg = "RoI Depn.Adj. $sMsgLineEnd";

                $this->addJournalRow(
                    $this->journalData,
                    self::CR,
                    $sAccountCode2,
                    $sCostCentre,
                    $ttCode,
                    $curDepnAdj,
                    $sMsg
                );

                // Dr I&E DEPRECIATION with value of depn. adj.

                $this->addJournalRow(
                    $this->journalData,
                    self::DR,
                    self::REVERSAL_OF_DEPN_CODE,
                    $sCostCentre,
                    $ttCode,
                    $curDepnAdj,
                    $sMsg
                );
            }

            /////// Reverse out I&E to the CAA/SMR ///////
            $curCAAValue = Math::subCurrency([$curIandECharge, $curDepnAdj]);


            if (Math::notEqualCurrency($curCAAValue, 0)) {
                // Credit CAA
                $sMsg = "RoI $sMsgLineEnd";

                $this->addJournalRow(
                    $this->journalData,
                    'Cr',
                    self::CAPITAL_ADJUSTMENT_ACCOUNT,
                    $sCostCentre,
                    $ttCode,
                    $curCAAValue,
                    $sMsg
                );

                // Debit SMR (reverse of CAA above)

                $this->addJournalRow(
                    $this->journalData,
                    self::DR,
                    self::SMR_ACCOUNT,
                    $sCostCentre,
                    $ttCode,
                    $curCAAValue,
                    $sMsg
                );
            }
        }   //foreach asset.

        return;
    }
}
