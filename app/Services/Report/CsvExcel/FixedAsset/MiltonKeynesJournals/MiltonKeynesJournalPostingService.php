<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\MiltonKeynesJournals;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\FixedAsset\CaFinYearManager;
use Tfcloud\Lib\Math;
use Tfcloud\Models\CaPostingAccount;
use Tfcloud\Models\CaIfrsCategory;
use Tfcloud\Models\CaTransaction;
use Tfcloud\Models\CaTransactionType;
use Tfcloud\Models\SiteGroup;
use Tfcloud\Models\Views\VwFar07;
use Tfcloud\Models\Views\VwFar06;
use Tfcloud\Models\Views\VwCaCurrentFinYear;
use Tfcloud\Services\FixedAsset\BalanceService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\CsvExcel\CsvExcelReportBaseService;

abstract class MiltonKeynesJournalPostingService extends CsvExcelReportBaseService
{
    private const DEBUG = false;

    // Used when posting to CAA
    protected const ASSET_MANAGEMENT_COST_CENTRE  = "50Z00140";
    protected const ASSET_MANAGEMENT_HRA_COST_CENTRE = "50H02025";
    protected const DEREC_COST_CENTRE = "50002024";

    // Fixed Accounts/Control Accounts. RB 22/07/2019 New codes for Agresso.
    protected const REVAL_RESERVE_ACCOUNT = "Y6000";
    protected const CAPITAL_ADJUSTMENT_ACCOUNT = "Y6100";
    protected const SMR_ACCOUNT  = "T2050";
    protected const CASH_CREDITOR = "8999991";

    // Charge Codes
    protected const REVAL_LOSS_CODE = "H1000";
    protected const IMPAIRMENT_LOSS_CODE = "H2000";
    protected const DEPRECIATION_CODE = "H0000";
    protected const REVERSAL_OF_DEPN_CODE = "H0000";
    protected const DISPOSAL_CODE = "H6000";
    protected const AMORTISATION_CODE = "H3000";



    protected const COL_ACCOUNT = 4;
    protected const COL_CAT1 = 5;
    protected const COL_CAT4 = 8;
    protected const COL_CURAMOUNT = 11;
    protected const COL_AMOUNT = 12;
    protected const COL_TEXT = 13;

    // Debit or Credit
    protected const DR = 'Dr';
    protected const CR = 'Cr';

    // Name of XLS journal template file
    protected const XLTEMPLATENAME = "MiltonKeynes Journal Sheet Template.xlt";
    protected const START_ROW = 10;


    protected $caPostingAccount = null;
    protected $yearString = null;
    protected $permissionService = null;

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->permissionService = $permissionService;
        $this->postingAcc = $this->getPostingAccount();
        $this->setYearString();
        $this->setControlAccounts();
    }

    protected function textContains($hayStack, $needle)
    {
        $pos = strpos($hayStack, $needle);
        if ($pos >= 1 || $pos === 0) {
            return true;
        } else {
            return false;
        }
    }

    protected function addJournalHeader(&$data, $journalType)
    {
        $data[] = $this->newJournalRow(
            '',
            'Client',
            'Milton Keynes (50)',
            'Date',
            date('d/m/Y'),
            '',
            '',
            ''
        );

        $data[] = $this->newJournalRow(
            '',
            'Journal Type',
            $journalType,
            'Lines to Load',
            '0',
            '',
            '',
            ''
        );

        $data[] = $this->newJournalRow(
            '',
            'Period',
            '',
            'Balance',
            '0.00',
            '',
            '',
            ''
        );

        $data[] = $this->newJournalRow(
            '',
            'Journal Number',
            '',
            '',
            '',
            '',
            '',
            ''
        );

        $data[] = $this->newJournalRow();

        $data[] = $this->newJournalRow(
            'Exclude',
            'Cost Centre',
            'Account',
            'Capital Project',
            'Analysis',
            'Amount',
            'Journal Text',
            'Line Errors'
        );


        return;
    }

    protected function addJournalRow(
        &$data,
        $drcr,
        $accountCode,
        $costCentre,
        $ttCode,
        $amount,
        $text,
        $rollUp = false
    ) {

        if ($drcr == self::DR) {
            $debugText = "$drcr (Positive)";
        } else {
            $debugText = "$drcr (Negative)";
        }

        if (!$rollUp) {
            if ($drcr == self::DR) {
                $amountFormatted = Common::numberFormat($amount, false, '', 2);
            } else {
                $amountFormatted = Common::numberFormat(Math::negateCurrency($amount), false, '', 2);
            }
        } else {
            $amountFormatted = Common::numberFormat($amount, false, '', 2);
        }

        $data[] = $this->newJournalRow(
            '',
            $costCentre,
            $accountCode,
            '',
            $ttCode,  // trans type code
            $amountFormatted,
            $text,
            self::DEBUG ? "$debugText" : ''
        );

        return;
    }

    protected function getIFRSAccount(
        $sIFRSSubType,
        &$accountCode1,
        &$accountCode2,
        &$accountCode3,
        &$accountCode4,
        &$noHC = false,
        &$shortCode = ''
    ) {
        $ifrsCat = CaIfrsCategory::where('ca_ifrs_category_code', $sIFRSSubType)
            ->where('site_group_id', SiteGroup::get()->site_group_id)
            ->first();

        $accountCode1 = $ifrsCat->account_code_1;
        $accountCode2 = $ifrsCat->account_code_2;
        $accountCode3 = $ifrsCat->account_code_3;
        $accountCode4 = $ifrsCat->account_code_4;
        $noHC = Common::valueYorNtoBoolean($ifrsCat->investment);
        $shortCode = $ifrsCat->ca_ifrs_category_code;
    }

    protected function retrieveTransactionList($transType)
    {
        return CaTransaction::join(
            'ca_ifrs_category',
            'ca_ifrs_category.ca_ifrs_category_id',
            '=',
            'ca_transaction.ca_ifrs_cat_id'
        )
        ->join(
            'ca_fixed_asset',
            'ca_fixed_asset.ca_fixed_asset_id',
            '=',
            'ca_transaction.ca_fixed_asset_id'
        )
        ->join(
            'ca_account',
            'ca_account.ca_account_id',
            '=',
            'ca_fixed_asset.ca_account_code_id'
        )
        ->where('ca_transaction_type_id', $transType)
        ->where('ca_fixed_asset.site_group_id', SiteGroup::get()->site_group_id)
        ->where('ca_fixed_asset.active', CommonConstant::DATABASE_VALUE_YES)
        ->where('ca_transaction.ca_fin_year_id', CaFinYearManager::currentFinYearId())
        ->select(
            [
                'ca_ifrs_category.ca_ifrs_category_code',
                'ca_ifrs_category.ca_ifrs_category_desc',
                'ca_account.ca_account_code',
                'ca_transaction.cv_asset',
                'ca_transaction.cv_land',
                'ca_transaction.hc_asset',
                'ca_transaction.hc_land',
                'ca_transaction.cv_depn_adjustment',
                'ca_transaction.hc_depn_adjustment',
                'ca_fixed_asset.ca_fixed_asset_code',
                'ca_fixed_asset.ca_fixed_asset_id',
                'ca_transaction.disposal_proceeds',
                'ca_transaction.disposal_costs',
                'ca_transaction.effective_date',
                'ca_transaction.effective_date_override'
            ]
        );
    }

    protected function hasHCDepnAdj()
    {
        $query = CaTransaction::where('hc_depn_adjustment', '<>', 0)
            ->whereNotNull('hc_depn_adjustment')
            ->where('site_group_id', SiteGroup::get()->site_group_id)
            ->where('ca_fin_year_id', CaFinYearManager::currentFinYearId());

        return $query->count() > 0;
    }

    protected function hasCVDepnAdj()
    {
        $query = CaTransaction::where('cv_depn_adjustment', '<>', 0)
            ->whereNotNull('cv_depn_adjustment')
            ->where('site_group_id', SiteGroup::get()->site_group_id)
            ->where('ca_fin_year_id', CaFinYearManager::currentFinYearId());

        return $query->count() > 0;
    }

    protected function hasReval($caFixedAssetId)
    {
        $query = CaTransaction::where('ca_transaction_type_id', CaTransactionType::REVALUATION)
            ->where('ca_fixed_asset_id', $caFixedAssetId)
            ->where('site_group_id', SiteGroup::get()->site_group_id)
            ->where('ca_fin_year_id', CaFinYearManager::currentFinYearId());

        return $query->count() > 0;
    }


    protected function getCVDepnAdj($caFixedAssetId, $reversalTypes)
    {
        $query = CaTransaction::where('ca_fixed_asset_id', $caFixedAssetId)
            ->whereIn('ca_transaction_type_id', $reversalTypes)
            ->where('site_group_id', SiteGroup::get()->site_group_id)
            ->where('ca_fin_year_id', CaFinYearManager::currentFinYearId());

        return $query->get()->sum('cv_depn_adjustment');
    }

    protected function getRevalAmounts($caFixedCode)
    {
        $query = VwFar06::where('Asset Code', $caFixedCode)
            ->where('site_group_id', SiteGroup::get()->site_group_id)
            ->select(['Imp WOff RR AS imp_woff_rr'  , 'Imp WOff SD AS imp_woff_sd']);
        return $query->first();
    }


    protected function retrieveBalance($fixedAssetId, $finYearId, $balanceType)
    {
        $balanceService = new BalanceService($this->permissionService);

        return $balanceService->retrieveByYear($fixedAssetId, $finYearId, $balanceType);
    }

    protected function getBFwdAccumulatedBalances(
        $caFixedAssetId,
        &$curBfwdGBV,
        &$curBFwdDepn,
        &$curBFwdImp,
        &$curCYDepn,
        &$curHCCYDepn,
        &$curBFwdRR
    ) {
        $cv = $this->getCurrentAssetData($caFixedAssetId);
        $hc = $this->getHistoricAssetData($caFixedAssetId);

        $curBfwdGBV = $cv->COpenGBV;
        $curBFwdDepn = $cv->COpenDPN;
        $curBFwdImp = Math::negateCurrency(Math::addCurrency([$cv->COpenIMPA, $cv->COpenIMPL]));
        $curCYDepn = $cv->CDPN;
        $curHCCYDepn = $hc->amount;
        $curBFwdRR = Math::addCurrency([$cv->BFAssetRR, $cv->BFLandRR]);
    }

    protected function getHistoricAssetData($caFixedAssetId)
    {
        return VwFar07::userSiteGroup()
            ->leftJoin(
                'ca_ifrs_category as bfwd_ca_ifrs_category',
                'bfwd_ca_ifrs_category.ca_ifrs_category_id',
                '=',
                'vw_far07.bfwd_ca_ifrs_category_id'
            )
            ->where('vw_far07.ca_balance_historic', 'Y')
            ->where('ca_fixed_asset_id', $caFixedAssetId)
            ->select(
                [
                    \DB::raw('bfwd_ca_ifrs_category.ca_ifrs_category_desc AS bfwd_ca_ifrs_category_desc'),
                    'vw_far07.hra_asset',
                    'vw_far07.ca_fixed_asset_id',
                    'vw_far07.deminimis_asset',
                    \DB::raw('vw_far07.`REV Asset Value` as HREVAV'),
                    \DB::raw('vw_far07.`REV Land Value` as HREVLV'),
                    \DB::raw('vw_far07.`IFRS Code` as ca_ifrs_category_code'),
                    \DB::raw('vw_far07.`IFRS Description` as ca_ifrs_category_desc'),
                    \DB::raw('vw_far07.`Previous IFRS Code` as bf_ca_ifrs_category_code'),
                    \DB::raw('vw_far07.`Previous IFRS Description` as bf_ca_ifrs_category_desc'),
                    \DB::raw('vw_far07.`Asset Code` as fixed_asset_code'),
                    \DB::raw('vw_far07.`Asset Description` as fixed_asset_desc'),
                    \DB::raw('vw_far07.`Account Code` as account_code'),
                    \DB::raw('vw_far07.`CY Depreciation` as amount'),
                    \DB::raw('vw_far07.`BFwd GBV` as HOpenGBV'),
                    'bfwd_ca_ifrs_category.amortisation as bfwd_amortisation',
                ]
            )
            ->orderBy(\DB::raw('bfwd_ca_ifrs_category.ca_ifrs_category_desc'))
            ->orderBy(\DB::raw('vw_far07.`Account Code`'))
            ->first();
    }

    protected function getCurrentAssetData($caFixedAssetId)
    {
        return VwFar07::userSiteGroup()
            ->leftJoin(
                'ca_ifrs_category as bfwd_ca_ifrs_category',
                'bfwd_ca_ifrs_category.ca_ifrs_category_id',
                '=',
                'vw_far07.bfwd_ca_ifrs_category_id'
            )
            ->where('vw_far07.ca_balance_historic', 'N')
            ->where('ca_fixed_asset_id', $caFixedAssetId)
            ->select(
                [
                    \DB::raw('bfwd_ca_ifrs_category.ca_ifrs_category_desc AS bfwd_ca_ifrs_category_desc'),
                    'vw_far07.hra_asset',
                    'vw_far07.ca_fixed_asset_id',
                    'vw_far07.deminimis_asset',
                    \DB::raw('vw_far07.`REV Asset Value` as CREVAV'),
                    \DB::raw('vw_far07.`REV Land Value` as CREVLV'),
                    \DB::raw('vw_far07.`IFRS Code` as ca_ifrs_category_code'),
                    \DB::raw('vw_far07.`IFRS Description` as ca_ifrs_category_desc'),
                    \DB::raw('vw_far07.`Previous IFRS Code` as bf_ca_ifrs_category_code'),
                    \DB::raw('vw_far07.`Previous IFRS Description` as bf_ca_ifrs_category_desc'),
                    \DB::raw('vw_far07.`Asset Code` as fixed_asset_code'),
                    \DB::raw('vw_far07.`Asset Description` as fixed_asset_desc'),
                    \DB::raw('vw_far07.`Account Code` as account_code'),
                    \DB::raw('vw_far07.`Accumulated Depreciation` as COpenDPN'),
                    \DB::raw('vw_far07.`CY Depreciation` as CDPN'),
                    \DB::raw('vw_far07.`CFwd Accumulated Depreciation` as CCFwdDPN'),
                    \DB::raw('vw_far07.`Accumulated Impairment Asset` as COpenIMPA'),
                    \DB::raw('vw_far07.`Accumulated Impairment Land` as COpenIMPL'),
                    \DB::raw('vw_far07.`BFwd GBV` as COpenGBV'),
                    \DB::raw('vw_far07.`BFwd RR Asset` as BFAssetRR'),
                    \DB::raw('vw_far07.`BFwd RR Land` as BFLandRR'),
                    'bfwd_ca_ifrs_category.amortisation as bfwd_amortisation',
                ]
            )
            ->orderBy(\DB::raw('bfwd_ca_ifrs_category.ca_ifrs_category_desc'))
            ->orderBy(\DB::raw('vw_far07.`Account Code`'))
            ->first();
    }


    protected function isHRA($sIFRSType, $costCentre)
    {
        // Is this an HRA asset?
        if (
            $sIFRSType == 'Council Dwellings' ||
            substr($costCentre, 0, 3) == 'COC' ||
            substr($costCentre, 0, 3) == 'UMR'
        ) {
            return true;
        } else {
            return false;
        }
    }

    protected function isHeritage($sIFRSType)
    {
        // Is this an Heritage Asset?
        if ($sIFRSType == 'Heritage Assets') {
            return true;
        } else {
            return false;
        }
    }

    protected function isDeminimis($deminimis)
    {
        // Is this an Deminimis Asset?
        return Common::valueYorNtoBoolean($deminimis);
    }

    protected function getTTCode($ttId)
    {
        return CaTransactionType::find($ttId)->ca_transaction_type_code;
    }

    protected function getPostingAccount()
    {
        return CaPostingAccount::where('site_group_id', SiteGroup::get()->site_group_id)->first();
    }

    protected function postGroupLikeJournalLines(
        &$journalData,
        $DrCr,
        $sAccountCode,
        $sSubAccountCode,
        $sCostCentre,
        $sText,
        $iTextLen
    ) {
        $curAmount = 0;
        $curTotalAmount  = 0;
        $replaceCount = 0;
        $count = 0;

        foreach ($journalData as $key => $journal) {
            $count++;
            // First 9 rows are headings.
            if ($count > 9) {
                $journalDrCr = Math::lessThanCurrency(array_get($journal, 'col_S'), 0) ? 'Cr' : 'Dr';

                // Is this one of the target rows - check all the key fields.
                if (array_get($journal, 'col_G') == $sAccountCode && array_get($journal, 'col_K') == $sSubAccountCode) {
                    if (array_get($journal, 'col_H') == $sCostCentre && $journalDrCr == $DrCr) {
                        // If we match then clear out the record and add its value to the total to put back at the end.
                        if (substr(array_get($journal, 'col_Y'), 0, $iTextLen) == substr($sText, 0, $iTextLen)) {
                            $curAmount = array_get($journal, 'col_S');
                            $curTotalAmount = Math::addCurrency([$curTotalAmount, $curAmount]);
                            //unset($journalData[$key]);
                            array_forget($journalData, $key);
                            $replaceCount++;
                        }
                    }
                }
            }
        }

        if ($replaceCount > 0) {
            $this->addJournalRow(
                $journalData,
                $DrCr,
                $sAccountCode,
                $sCostCentre,
                $sSubAccountCode,
                $curTotalAmount,
                $sText,
                true
            );
        }

        return;
    }

    private function setYearString()
    {
        $openCaFinYear = VwCaCurrentFinYear::userSiteGroup()
            ->select('ca_fin_year_code')
            ->first();

        if ($openCaFinYear) {
            $this->yearString = $openCaFinYear->ca_fin_year_code;
        }
    }

    private function setControlAccounts()
    {
        $this->capitalAdjustmentAccount = self::CAPITAL_ADJUSTMENT_ACCOUNT;
        $this->revalReserveAccount = self::REVAL_RESERVE_ACCOUNT;
        $this->sMRAccount = self::SMR_ACCOUNT;
    }

    private function newJournalRow(
        $col_A = '',
        $col_B = '',
        $col_C = '',
        $col_D = '',
        $col_E = '',
        $col_F = '',
        $col_G = '',
        $col_H = ''
    ) {
        return [
            'col_A'   => $col_A,
            'col_B'   => $col_B,
            'col_C'   => $col_C,
            'col_D'   => $col_D,
            'col_E'   => $col_E,
            'col_F'   => $col_F,
            'col_G'   => $col_G,
            'col_H'   => $col_H
        ];
    }
}
