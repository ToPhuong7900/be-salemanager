<?php

namespace  Tfcloud\Services\Report\CsvExcel\FixedAsset\SandwellJournals\Traits;

use Tfcloud\Services\Report\CsvExcel\FixedAsset\SandwellJournals\SandwellConstant as SC;

trait JournalOutputTrait
{
    protected function addJournalHeader(&$data, $accDate, $accYear)
    {
        $data[] = $this->newJournalRow(
            [
                'col_B' => 'Balance Type',
                'col_E' => 'Actual'
            ]
        );

        $data[] = $this->newJournalRow(
            [
                'col_B' => 'Database',
                'col_E' => 'OEBS-PRDB.SBSPROD',
                'col_I' => 'D'
            ]
        );

        $data[] = $this->newJournalRow(
            [
                'col_B' => 'Data Access Set',
                'col_E' => 'Sandwell Business System',
                'col_I' => 'C'
            ]
        );

        $data[] = $this->newJournalRow(
            [
                'col_B' => 'Chart of Accounts',
                'col_E' => 'Accounting Flexfield'
            ]
        );

        $data[] = $this->newJournalRow();

        $data[] = $this->newJournalRow(
            [
                'col_B' => 'Ledger',
                'col_F' => SC::LIST_TEXT,
                'col_G' => 'Sandwell Business System'
            ]
        );

        $data[] = $this->newJournalRow(
            [
                'col_B' => 'Category',
                'col_F' => SC::LIST_TEXT,
                'col_G' => 'Actuals'
            ]
        );

        $data[] = $this->newJournalRow(
            [
                'col_B' => 'Source',
                'col_F' => SC::LIST_TEXT,
                'col_G' => 'Actuals'
            ]
        );

        $data[] = $this->newJournalRow(
            [
                'col_B' => 'Currency',
                'col_F' => SC::LIST_TEXT,
                'col_G' => 'GBP'
            ]
        );

        $data[] = $this->newJournalRow(
            [
                'col_B' => 'Accouting Date',
                'col_F' => SC::LIST_DATE,
                'col_G' => $accDate
            ]
        );

        $data[] = $this->newJournalRow(
            [
                'col_B' => 'Batch Name',
                'col_F' => SC::LIST_TEXT
            ]
        );

        $data[] = $this->newJournalRow(
            [
                'col_B' => 'Batch Description',
                'col_F' => SC::LIST_TEXT
            ]
        );

        $data[] = $this->newJournalRow(
            [
                'col_B' => 'Journal Name',
                'col_F' => SC::LIST_TEXT
            ]
        );

        $data[] = $this->newJournalRow(
            [
                'col_B' => 'Journal Description',
                'col_F' => SC::LIST_TEXT,
                'col_G' => "$accYear"
            ]
        );
        $data[] = $this->newJournalRow(
            [
                'col_B' => 'Period',
                'col_F' => SC::LIST_TEXT,
                'col_G' => "$accYear"
            ]
        );

        $data[] = $this->newJournalRow();

        $data[] = $this->newJournalRow(
            [
                'col_B' => 'Upl',
                'col_C' => 'Cost Centre',
                'col_D' => 'Standard Subjective',
                'col_E' => 'Sub Analysis',
                'col_F' => 'First Spare Segment',
                'col_G' => 'Second Spare Segment',
                'col_H' => 'Organisation',
                'col_I' => 'Debit',
                'col_J' => 'Credit',
                'col_K' => 'Line Description',
                'col_N' => 'Messages'
            ]
        );

        $data[] = $this->newJournalRow(
            [
                'col_B' => SC::LIST_TEXT,
                'col_I' => '*Number',
                'col_J' => '*Number',
                'col_K' => 'Text'
            ]
        );
    }

    public static function addJournalRow(
        $costCentre,
        $standardSubjective,
        $subAnalysis,
        $debit,
        $credit,
        $lineDesc,
        $options
    ) {
        return self::newJournalRow(
            [
                'col_C' => $costCentre,
                'col_D' => $standardSubjective,
                'col_E' => $subAnalysis,
                'col_F' => SC::FIRST_SEGMENT,
                'col_G' => SC::SECOND_SEGMENT,
                'col_H' => SC::ORGANISATION,
                'col_I' => $debit,
                'col_J' => $credit,
                'col_K' => $lineDesc,
                'col_L' => array_get($options, 0, ''),
                'col_N' => array_get($options, 1, '')
            ]
        );
    }


    private static function newJournalRow($row = [])
    {
        return [
            'col_A'   => array_get($row, 'col_A', ''),
            'col_B'   => array_get($row, 'col_B', ''),
            'col_C'   => array_get($row, 'col_C', ''),
            'col_D'   => array_get($row, 'col_D', ''),
            'col_E'   => array_get($row, 'col_E', ''),
            'col_F'   => array_get($row, 'col_F', ''),
            'col_G'   => array_get($row, 'col_G', ''),
            'col_H'   => array_get($row, 'col_H', ''),
            'col_I'   => array_get($row, 'col_I', ''),
            'col_J'   => array_get($row, 'col_J', ''),
            'col_K'   => array_get($row, 'col_K', ''),
            'col_L'   => array_get($row, 'col_L', ''),
            'col_M'   => array_get($row, 'col_M', ''),
            'col_N'   => array_get($row, 'col_N', ''),
            'col_O'   => array_get($row, 'col_O', ''),
            'col_P'   => array_get($row, 'col_P', ''),
            'col_Q'   => array_get($row, 'col_Q', ''),
            'col_R'   => array_get($row, 'col_R', ''),
            'col_S'   => array_get($row, 'col_S', ''),
            'col_T'   => array_get($row, 'col_T', ''),
            'col_U'   => array_get($row, 'col_U', ''),
            'col_V'   => array_get($row, 'col_V', ''),
            'col_W'   => array_get($row, 'col_W', ''),
            'col_X'   => array_get($row, 'col_X', ''),
            'col_Y'   => array_get($row, 'col_Y', '')
        ];
    }
}
