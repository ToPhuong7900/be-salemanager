<?php

namespace  Tfcloud\Services\Report\CsvExcel\FixedAsset\SandwellJournals\Traits;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\FixedAsset\CaFinYearManager;
use Tfcloud\Models\CaBalance;
use Tfcloud\Models\Views\VwFar06;
use Tfcloud\Models\Views\VwFar07;
use Tfcloud\Models\Views\VwFar15;
use Tfcloud\Models\Views\VwFar13;
use Tfcloud\Models\Views\VwFar14;
use Tfcloud\Models\Views\VwFar18;
use Tfcloud\Services\FixedAsset\BalanceService;

trait JournalDataTrait
{
    protected function getAsssetsByIFRSSubType($ifrsCatCode)
    {
        $query = VwFar07::userSiteGroup()
            ->leftJoin(
                'ca_ifrs_category as bfwd_ca_ifrs_category',
                'bfwd_ca_ifrs_category.ca_ifrs_category_id',
                '=',
                'vw_far07.bfwd_ca_ifrs_category_id'
            )
            ->where('vw_far07.ca_balance_historic', 'N')
            ->whereNotNull('vw_far07.user_sel1_code')
            ->whereIn(\DB::raw('vw_far07.`IFRS Code`'), $ifrsCatCode)
            ->select(
                [
                    \DB::raw('bfwd_ca_ifrs_category.ca_ifrs_category_desc AS bfwd_ca_ifrs_category_desc'),
                    'vw_far07.hra_asset',
                    'vw_far07.ca_fixed_asset_id',
                    'vw_far07.deminimis_asset',
                    \DB::raw('vw_far07.user_sel1_code AS sub_ifrs_code'),
                    \DB::raw('vw_far07.`Asset Type Code` as asset_type_code'),
                    \DB::raw('vw_far07.`REV Asset Value` as CREVAV'),
                    \DB::raw('vw_far07.`REV Land Value` as CREVLV'),
                    \DB::raw('vw_far07.`IFRS Code` as ca_ifrs_category_code'),
                    \DB::raw('vw_far07.`IFRS Description` as ca_ifrs_category_desc'),
                    \DB::raw('vw_far07.`Previous IFRS Code` as bf_ca_ifrs_category_code'),
                    \DB::raw('vw_far07.`Previous IFRS Description` as bf_ca_ifrs_category_desc'),
                    \DB::raw('vw_far07.`Asset Code` as fixed_asset_code'),
                    \DB::raw('vw_far07.`Asset Description` as fixed_asset_desc'),
                    \DB::raw('vw_far07.`Account Code` as account_code'),
                    \DB::raw('vw_far07.`Accumulated Depreciation` as COpenDPN'),
                    \DB::raw('vw_far07.`CY Depreciation` as CDPN'),
                    \DB::raw('vw_far07.`CFwd Accumulated Depreciation` as CCFwdDPN'),
                    \DB::raw('vw_far07.`Accumulated Impairment Asset` as COpenIMPA'),
                    \DB::raw('vw_far07.`Accumulated Impairment Land` as COpenIMPL'),
                    \DB::raw('vw_far07.`BFwd GBV` as COpenGBV'),
                    \DB::raw('vw_far07.`BFwd RR Asset` as BFAssetRR'),
                    \DB::raw('vw_far07.`BFwd RR Land` as BFLandRR'),
                    'bfwd_ca_ifrs_category.amortisation as bfwd_amortisation',
                ]
            )
            ->orderBy(\DB::raw('bfwd_ca_ifrs_category.ca_ifrs_category_desc'))
            ->orderBy(\DB::raw('vw_far07.`Asset Code`'));

        return $query->get();
    }

    public static function getInYearDepreciation($caFixedAssetId, $permissionService)
    {
        $finYearId = CaFinYearManager::currentFinYearId();
        $balanceService = new BalanceService($permissionService);

        $cvBalance = $balanceService->retrieveByYear($caFixedAssetId, $finYearId, CaBalance::TYPE_CURRENT_VALUE);
        $hcBalance = $balanceService->retrieveByYear($caFixedAssetId, $finYearId, CaBalance::TYPE_HISTORIC_VALUE);
        $rrBalance = $balanceService->retrieveRRBalanceByYear($caFixedAssetId, $finYearId);

        return [
            'cvCYDepn' => Common::numberFormat($cvBalance->cy_depreciation, thousandsSep: ''),
            'hcCYDepn' => Common::numberFormat($hcBalance->cy_depreciation, thousandsSep: ''),
            'rrCYDepn' => Common::numberFormat($rrBalance->cy_rr_depreciation, thousandsSep: ''),
        ];
    }

    protected function getSubAnalysisCode($FixedAssetType, &$subjective)
    {
        $type = explode('.', $FixedAssetType);
        $subjective = $type[1];
        return $type[2];
    }

    protected static function getRevalData($caFixedAssetId, $up = true)
    {
        $query = VwFar06::userSiteGroup()
            ->where('vw_far06.ca_fixed_asset_id', $caFixedAssetId)
            ->select(
                [
                    \DB::raw('vw_far06.`Reval Asset Value` AS reval_asset_value'),
                    \DB::raw('vw_far06.`Reval Land Value` AS reval_land_value'),
                    \DB::raw('vw_far06.`Gross RR` AS gross_rr'),
                    \DB::raw('vw_far06.`Gross SD` AS gross_sd'),
                    \DB::raw('vw_far06.`Depn WOff RR` AS depn_wo_rr'),
                    \DB::raw('vw_far06.`Depn WOff SD` AS depn_wo_sd'),
                    \DB::raw('vw_far06.`Imp WOff RR` AS imp_wo_rr'),
                    \DB::raw('vw_far06.`Imp WOff SD` AS imp_wo_sd'),
                    \DB::raw('vw_far06.`Asset Loss Reversal` AS asset_loss_reversal'),
                    \DB::raw('vw_far06.`Land Loss Reversal` AS land_loss_reversal'),
                    \DB::raw('vw_far06.`Loss Depreciation Reversal` AS loss_depn_reversal'),
                    \DB::raw('vw_far06.`Asset Impairment Reversal` AS asset_imp_reversal'),
                    \DB::raw('vw_far06.`Land Impairment Reversal` AS land_imp_reversal'),
                    \DB::raw('vw_far06.`Impairment Depreciation Reversal` AS imp_depn_reversal'),
                    \DB::raw('vw_far06.`Reval Reserve` AS reval_reserve'),
                ]
            );

        if ($up) {
            $query->whereRaw('vw_far06.`Reval Asset Value` + vw_far06.`Reval Land Value` >= 0');
        } else {
            $query->whereRaw('vw_far06.`Reval Asset Value` + vw_far06.`Reval Land Value` < 0');
        }

        return $query->get();
    }

    protected static function getImpairmentData($caFixedAssetId)
    {
        $query = VwFar15::userSiteGroup()
            ->where('vw_far15.ca_fixed_asset_id', $caFixedAssetId)
            ->select(
                [
                    \DB::raw('vw_far15.`Impairment Value` AS impairment_value'),
                    \DB::raw('vw_far15.`Historic Cost Value` AS historic_cost_value'),
                    \DB::raw('vw_far15.`RR Value` AS rr_value'),
                    \DB::raw('vw_far15.`RR Transaction` AS rr_trans'),
                ]
            );

        return $query->get();
    }

    protected static function getDerecognitionData($caFixedAssetId)
    {
        $query = VwFar13::userSiteGroup()
            ->where('vw_far13.ca_fixed_asset_id', $caFixedAssetId)
            ->select(
                [
                    \DB::raw('vw_far13.`GBV` AS gbv'),
                    \DB::raw('vw_far13.`Accumulated Depreciation` AS acc_depn'),
                    \DB::raw('vw_far13.`Accumulated Impairment` AS acc_imp'),
                    \DB::raw('vw_far13.`NBV` AS nbv'),
                ]
            );

        return $query->get();
    }

    protected static function getDisposalData($caFixedAssetId)
    {
        $query = VwFar14::userSiteGroup()
            ->where('vw_far14.ca_fixed_asset_id', $caFixedAssetId)
            ->select(
                [
                    \DB::raw('vw_far14.`Total GBV` AS gbv'),
                    \DB::raw('vw_far14.`Accumulated Depreciation` AS acc_depn'),
                    \DB::raw('vw_far14.`Total NBV` AS nbv'),
                    \DB::raw('vw_far14.`Revaluation Reserve` AS rr'),
                ]
            );

        return $query->get();
    }

    protected static function getTransferData($caFixedAssetId)
    {
        $query = VwFar18::userSiteGroup()
            ->where('vw_far18.ca_fixed_asset_id', $caFixedAssetId)
            ->select(
                [
                    \DB::raw('vw_far18.`IFRS Code` AS subjective'),
                    \DB::raw('vw_far18.`Gross Movement` AS gross_movement'),
                    \DB::raw('vw_far18.`Depreciation Movement` AS depn_movement'),
                    \DB::raw('vw_far18.`Impairment Movement` AS imp_movement'),
                    \DB::raw('vw_far18.`RR Value` AS rr_value')
                ]
            );

        return $query->get();
    }
}
