<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\SandwellJournals;

use Tfcloud\Lib\Common;
use Tfcloud\Models\Views\VwCaCurrentFinYear;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\CsvExcel\CsvExcelReportBaseService;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\SandwellJournals\Traits\JournalOutputTrait;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\SandwellJournals\Traits\JournalDataTrait;

abstract class SandwellJournalBaseService extends CsvExcelReportBaseService
{
    use JournalOutputTrait;
    use JournalDataTrait;

    protected $yearString = '';
    protected $yearEndDate = '';
    protected $journalData = [];
    protected $permissionService = null;

    // New class goes here.
    abstract public function getReportData($inputs, &$filterText, &$sOrderText);
    abstract protected function prepareData();

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->permissionService = $permissionService;
        $this->setYearString();
    }



    private function setYearString()
    {
        $openCaFinYear = VwCaCurrentFinYear::userSiteGroup()
            ->select('ca_fin_year_code', 'year_end_date')
            ->first();

        if ($openCaFinYear) {
            $this->yearString = $openCaFinYear->ca_fin_year_code;
            $this->yearEndDate = Common::dateFieldDisplayFormat($openCaFinYear->year_end_date);
        }
    }
}
