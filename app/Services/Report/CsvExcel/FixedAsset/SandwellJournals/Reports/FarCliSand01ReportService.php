<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\SandwellJournals\Reports;

use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\SandwellJournals\SandwellConstant as SC;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\SandwellJournals\SandwellJournalBaseService;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\SandwellJournals\JournalData\DepreciationLines;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\SandwellJournals\JournalData\RevalGainLines;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\SandwellJournals\JournalData\RevalLossLines;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\SandwellJournals\JournalData\ImpairmentLines;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\SandwellJournals\JournalData\DerecognitionLines;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\SandwellJournals\JournalData\DisposalLines;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\SandwellJournals\JournalData\TransferLines;

class FarCliSand01ReportService extends SandwellJournalBaseService
{
    // OLB, VPE, INF, CA, AUC, SUR
    private const IFRS_CAT = ['X09002', 'X09003', 'X09004', 'X09005', 'X09006', 'X09011'];

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->addJournalHeader($this->journalData, $this->yearEndDate, $this->yearString);
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        // Asset by Asset OLB GF.

        $this->prepareData();

        return $this->journalData;
    }

    protected function prepareData()
    {
        $lCount = 0;
        $caFixedAssetId = 0;

        $assetData = $this->getAsssetsByIFRSSubType(self::IFRS_CAT);

        foreach ($assetData as $asset) {
            $lCount++;

            $caFixedAssetId = $asset->ca_fixed_asset_id;
            $costCentre = $asset->account_code;
            $ifrsSubCat  = $asset->sub_ifrs_code;
            $subAnalysis = $this->getSubAnalysisCode($asset->asset_type_code, $subjective);
            $caFixedAssetCode = $asset->fixed_asset_code;

            DepreciationLines::addLines(
                $this->journalData,
                $this->depnCodeList($costCentre, $subjective, $subAnalysis),
                $caFixedAssetId,
                $caFixedAssetCode,
                $ifrsSubCat,
                $this->permissionService
            );

            RevalGainLines::addLines(
                $this->journalData,
                $this->revalGainCodeList($costCentre, $subjective, $subAnalysis),
                $caFixedAssetId,
                $caFixedAssetCode
            );

            RevalLossLines::addLines(
                $this->journalData,
                $this->revalLossCodeList($costCentre, $subjective, $subAnalysis),
                $caFixedAssetId,
                $caFixedAssetCode,
                $ifrsSubCat
            );

            ImpairmentLines::addLines(
                $this->journalData,
                $this->impairmentCodeList($costCentre, $subjective, $subAnalysis),
                $caFixedAssetId,
                $caFixedAssetCode
            );

            DerecognitionLines::addLines(
                $this->journalData,
                $this->derecognitionCodeList($subjective, $subAnalysis),
                $caFixedAssetId,
                $caFixedAssetCode
            );

            DisposalLines::addLines(
                $this->journalData,
                $this->disposalCodeList($subjective, $subAnalysis),
                $caFixedAssetId,
                $caFixedAssetCode
            );

            TransferLines::addLines(
                $this->journalData,
                $this->transferCodeList($subjective, $subAnalysis),
                $caFixedAssetId,
                $caFixedAssetCode
            );
        }   //foreach asset.
    }

    private function depnCodeList($costCentre, $subjective, $subAnalysis)
    {
        return [
            SC::IFRS_SUB_OLB_GF => [
                [SC::GL_01999, $subjective, $subAnalysis],
                [SC::GL_01999, SC::SJ_X07006, 'X656'],
                [$costCentre, 'H01001', 'H001'],
                [SC::GL_52500, 'Y05007', 'Y182'],
                [SC::GL_01999, 'X07107', '0000'],
                [SC::GL_01999, 'X07108', '0000']
            ],
            SC::IFRS_SUB_VPE_GF_VEH => [
                [SC::GL_01999, $subjective, $subAnalysis],
                [SC::GL_01999, SC::SJ_X07006, 'X656'],
                [$costCentre, 'H01001', 'H001'],
                [SC::GL_52500, 'Y05007', 'Y184'],
                [SC::GL_01999, 'X07107', '0000'],
                [SC::GL_01999, 'X07108', '0000']
            ],
            SC::IFRS_SUB_VPE_GF_EQ => [
                [SC::GL_01999, $subjective, $subAnalysis],
                [SC::GL_01999, SC::SJ_X07006, 'X656'],
                [$costCentre, 'H01001', 'H001'],
                [SC::GL_52500, 'Y05007', 'Y185'],
                [SC::GL_01999, 'X07107', '0000'],
                [SC::GL_01999, 'X07108', '0000']
            ],
            SC::IFRS_SUB_INF => [
                [SC::GL_01999, $subjective, $subAnalysis],
                [SC::GL_01999, SC::SJ_X07006, 'X656'],
                [$costCentre, 'H01001', 'H001'],
                [SC::GL_52500, 'Y05007', 'Y183'],
                [SC::GL_01999, 'X07107', '0000'],
                [SC::GL_01999, 'X07108', '0000']
            ],
        ];
    }

    private function revalGainCodeList($costCentre, $subjective, $subAnalysis)
    {
        $row1 = [SC::GL_01999, $subjective, $subAnalysis];
        $row2 = [SC::GL_01999, $subjective, $subAnalysis];
        $row3 = [SC::GL_01999, SC::SJ_X07006, 'X651'];
        $row4 = [SC::GL_01999, SC::SJ_X07006, 'X652'];
        $row5 = [SC::GL_01999, SC::SJ_X07006, 'X657'];
        $row6 = [$costCentre, 'H02001', 'H054'];
        $row7 = [SC::GL_52500, 'Y05008', 'Y209'];
        $row8 = [SC::GL_01999, 'X07126', '0000'];

        return [$row1, $row2, $row3, $row4, $row5, $row6, $row7, $row8];
    }

    private function revalLossCodeList($costCentre, $subjective, $subAnalysis)
    {
        return [
            SC::IFRS_SUB_OLB_GF => [
                [SC::GL_01999, $subjective, $subAnalysis],
                [SC::GL_01999, $subjective, $subAnalysis],
                [SC::GL_01999, SC::SJ_X07006, 'X651'],
                [SC::GL_01999, SC::SJ_X07006, 'X653'],
                [$costCentre, 'H02001', 'H052'],
                [SC::GL_52500, 'Y05008', 'Y186'],
                [SC::GL_01999, 'X07104', '0000']
            ],
            SC::IFRS_SUB_SUR => [
                [SC::GL_01999, $subjective, $subAnalysis],
                [SC::GL_01999, $subjective, $subAnalysis],
                [SC::GL_01999, SC::SJ_X07006, 'X651'],
                [SC::GL_01999, SC::SJ_X07006, 'X653'],
                [$costCentre, 'H02001', 'H052'],
                [SC::GL_52500, 'Y05008', 'Y188'],
                [SC::GL_01999, 'X07104', '0000']
            ],
        ];
    }

    private function impairmentCodeList($costCentre, $subjective, $subAnalysis)
    {
        $row1 = [SC::GL_01999, $subjective, $subAnalysis];
        $row2 = [SC::GL_01999, $subjective, $subAnalysis];
        $row3 = [SC::GL_01999, SC::SJ_X07006, 'X651'];
        $row4 = [SC::GL_01999, SC::SJ_X07006, 'X654'];
        $row5 = [$costCentre, 'H02001', 'H053'];
        $row6 = ['52500', 'Y05008', 'Y180'];
        $row7 = [SC::GL_01999, 'X07105', '0000'];

        return [$row1, $row2, $row3, $row4, $row5, $row6, $row7];
    }

    private function derecognitionCodeList($subjective, $subAnalysis)
    {
        $row1 = [SC::GL_01999, $subjective, $subAnalysis];
        $row2 = [SC::GL_01999, $subjective, $subAnalysis];
        $row3 = [SC::GL_01999, SC::SJ_X07006, 'X351'];
        $row4 = [SC::GL_01999, 'X07139', '0000'];
        $row5 = [SC::GL_01999, 'X07140', '0000'];

        return [$row1, $row2, $row3, $row4, $row5];
    }

    private function disposalCodeList($subjective, $subAnalysis)
    {
        $row1 = [SC::GL_01999, $subjective, $subAnalysis];
        $row2 = [SC::GL_01999, $subjective, $subAnalysis];
        $row3 = [SC::GL_01999, SC::SJ_X07006, 'X655'];
        $row4 = [SC::GL_01999, 'X07101', '0000'];
        $row5 = [SC::GL_01999, 'X07102', '0000'];

        return [$row1, $row2, $row3, $row4, $row5];
    }

    private function transferCodeList($subjective, $subAnalysis)
    {
        $row1 = [SC::GL_01999, $subjective, $subAnalysis];
        $row2 = [SC::GL_01999, $subjective, $subAnalysis];
        $row3 = [SC::GL_01999, $subjective, $subAnalysis];
        $row4 = [SC::GL_01999, $subjective, $subAnalysis];
        $row5 = [SC::GL_01999, SC::SJ_X07006, 'X658'];
        $row6 = [SC::GL_01999, SC::SJ_X07006, 'X658'];
        $row7 = [SC::GL_01999, SC::SJ_X07006, 'X660'];
        $row8 = [SC::GL_01999, SC::SJ_X07006, 'X660'];
        $row9 = [SC::GL_01999, 'X07131', '0000'];
        $row10 = [SC::GL_01999, 'X07131', '0000'];

        return [$row1, $row2, $row3, $row4, $row5, $row6, $row7, $row8, $row9, $row10];
    }
}
