<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\SandwellJournals\JournalData;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Math;
use Tfcloud\Models\CaIfrsCategory;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\SandwellJournals\Traits\JournalOutputTrait;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\SandwellJournals\Traits\JournalDataTrait;

class TransferLines
{
    use JournalOutputTrait;
    use JournalDataTrait;

    public static function addLines(
        &$data,
        $codeList,
        $caFixedAssetId,
        $caFixedAssetCode
    ) {
        $transfers = self::getTransferData($caFixedAssetId);

        foreach ($transfers as $transfer) {
            $subjective = $transfer->subjective;
            $gross = Common::numberFormat($transfer->gross_movement, thousandsSep: '');
            $depn = Common::numberFormat($transfer->depn_movement, thousandsSep: '');
            $rr = Common::numberFormat($transfer->rr_value, thousandsSep: '');

            if (Math::greaterThanCurrency($transfer->gross_movement, 0)) {
                // Transfer to

                //Transfer of asset (to) - application from fixed asset registers:
                $data[] = self::addJournalRow(
                    $codeList[2][0],
                    $subjective,
                    $codeList[2][2],
                    0,
                    $depn,
                    'FAR Write out of accumulated depreciation (Tfr to Asset to FAR)',
                    ['TFR', $caFixedAssetCode]
                );

                $data[] = self::addJournalRow(
                    $codeList[3][0],
                    $subjective,
                    $codeList[3][2],
                    0,
                    $gross,
                    'FAR Transfer of Asset (to FAR)',
                    ['TFR', $caFixedAssetCode]
                );

                // Transfer of asset - application to revaluation reserve:
                $data[] = self::addJournalRow(
                    $codeList[4][0],
                    $subjective,
                    $codeList[4][2],
                    0,
                    $rr,
                    'RR Transfer of Asset (to FAR)',
                    ['TFR', $caFixedAssetCode]
                );

                // Going to Investment.
                if (self::isInvesmentCat($subjective)) {
                    $data[] = self::addJournalRow(
                        $codeList[6][0],
                        $subjective,
                        $codeList[6][2],
                        0,
                        $rr,
                        'RR Transfer of Investment Asset (to FAR)',
                        ['TFR', $caFixedAssetCode]
                    );

                    // Transfer of (investment) asset - application to CAA:
                    $data[] = self::addJournalRow(
                        $codeList[8][0],
                        $subjective,
                        $codeList[8][2],
                        0,
                        $gross,
                        'CAA Transfer of Investment Asset (to FAR)',
                        ['TFR', $caFixedAssetCode]
                    );
                }
            } else {
                // Transfer from
                $gross = Math::negateCurrency($gross);
                $rr = Math::negateCurrency($rr);

                //Transfer of asset (from) - application from fixed asset registers:
                $data[] = self::addJournalRow(
                    $codeList[0][0],
                    $subjective,
                    $codeList[0][2],
                    $depn,
                    0,
                    'FAR Write out of accumulated depreciation (Tfr of Asset from FAR)',
                    ['TFR', $caFixedAssetCode]
                );

                $data[] = self::addJournalRow(
                    $codeList[1][0],
                    $subjective,
                    $codeList[1][2],
                    $gross,
                    0,
                    'FAR Transfer of Asset (from FAR)',
                    ['TFR', $caFixedAssetCode]
                );

                // Transfer of asset - application to revaluation reserve:
                $data[] = self::addJournalRow(
                    $codeList[5][0],
                    $subjective,
                    $codeList[5][2],
                    $rr,
                    0,
                    'RR Transfer of Asset (from FAR)',
                    ['TFR', $caFixedAssetCode]
                );

                // Going to Investment.
                if (self::isInvesmentCat($subjective)) {
                    $data[] = self::addJournalRow(
                        $codeList[7][0],
                        $subjective,
                        $codeList[7][2],
                        $rr,
                        0,
                        'RR Transfer of Investment Asset (from FAR)',
                        ['TFR', $caFixedAssetCode]
                    );

                    // Transfer of (investment) asset - application to CAA:
                    $data[] = self::addJournalRow(
                        $codeList[9][0],
                        $subjective,
                        $codeList[9][2],
                        $gross,
                        0,
                        'CAA Transfer of Investment Asset (from FAR)',
                        ['TFR', $caFixedAssetCode]
                    );
                }
            }
        } // End
    }


    private static function isInvesmentCat($ifrsCat)
    {
        $ifrs = CaIfrsCategory::userSiteGroup()
            ->where('ca_ifrs_category_code', $ifrsCat)
            ->select('investment')
            ->first();

        return Common::valueYorNtoBoolean($ifrs->invesment);
    }
}
