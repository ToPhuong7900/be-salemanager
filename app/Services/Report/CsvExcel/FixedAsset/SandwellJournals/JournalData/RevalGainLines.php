<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\SandwellJournals\JournalData;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Math;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\SandwellJournals\Traits\JournalOutputTrait;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\SandwellJournals\Traits\JournalDataTrait;

class RevalGainLines
{
    use JournalOutputTrait;
    use JournalDataTrait;

    public static function addLines(
        &$data,
        $codeList,
        $caFixedAssetId,
        $caFixedAssetCode
    ) {
        $revals = self::getRevalData($caFixedAssetId);

        //Common::numberFormat($cvBalance->cy_depreciation, thousandsSep: '')
        foreach ($revals as $reval) {
            $depnWO = Common::numberFormat(
                Math::addCurrency([$reval->depn_wo_rr, $reval->depn_wo_sd]),
                thousandsSep: ''
            );

            $depnWoRR = Common::numberFormat($reval->depn_wo_rr, thousandsSep: '');
            $revalReserveGain = Common::numberFormat($reval->reval_reserve, thousandsSep: '');

            $revalGain = Common::numberFormat(
                Math::addCurrency([$reval->reval_asset_value, $reval->reval_land_value]),
                thousandsSep: ''
            );

            $impReversal = Common::numberFormat(
                Math::addCurrency([$reval->asset_imp_reversal, $reval->land_imp_reversal]),
                thousandsSep: ''
            );

            $lossReversal = Common::numberFormat(
                Math::addCurrency([$reval->asset_loss_reversal, $reval->land_loss_reversal]),
                thousandsSep: ''
            );

            // Revaluation gain - application to fixed asset registers.
            $data[] = self::addJournalRow(
                $codeList[0][0],
                $codeList[0][1],
                $codeList[0][2],
                $depnWO,
                0,
                'FAR Write out of accumulated depreciation',
                ['RG', $caFixedAssetCode]
            );
            $data[] = self::addJournalRow(
                $codeList[1][0],
                $codeList[1][1],
                $codeList[1][2],
                $revalGain,
                0,
                'FAR Revaluation Gain',
                ['RG', $caFixedAssetCode]
            );

            //Revaluation gain - application to revaluation reserve.
            $data[] = self::addJournalRow(
                $codeList[2][0],
                $codeList[2][1],
                $codeList[2][2],
                0,
                $depnWoRR,
                'RR Write out of accumulated depreciation',
                ['RG', $caFixedAssetCode]
            );

            $data[] = self::addJournalRow(
                $codeList[3][0],
                $codeList[3][1],
                $codeList[3][2],
                0,
                $revalReserveGain,
                'RR Revaluation Gain',
                ['RG', $caFixedAssetCode]
            );


            // Revaluation gain - impairment reversal application to revaluation reserve/CI&E/CAA
            // (where asset was previously valued down below historic value).
            $data[] = self::addJournalRow(
                $codeList[4][0],
                $codeList[4][1],
                $codeList[4][2],
                $impReversal,
                0,
                'RR Impairment Reversal',
                ['RG', $caFixedAssetCode]
            );
            $data[] = self::addJournalRow(
                $codeList[5][0],
                $codeList[5][1],
                $codeList[5][2],
                0,
                $impReversal,
                'CI&E Service Impairment Reversal (Asset Noted)',
                ['RG', $caFixedAssetCode]
            );
            $data[] = self::addJournalRow(
                $codeList[6][0],
                $codeList[6][1],
                $codeList[6][2],
                $lossReversal,
                0,
                'CI&E MRS Impairment Reversal',
                ['RG', $caFixedAssetCode]
            );
            $data[] = self::addJournalRow(
                $codeList[7][0],
                $codeList[7][1],
                $codeList[7][2],
                0,
                $lossReversal,
                'CAA Impairment Reversal',
                ['RG', $caFixedAssetCode]
            );
        }
    }
}
