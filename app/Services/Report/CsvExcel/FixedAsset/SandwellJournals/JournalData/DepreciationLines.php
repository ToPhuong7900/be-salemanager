<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\SandwellJournals\JournalData;

use Tfcloud\Services\Report\CsvExcel\FixedAsset\SandwellJournals\SandwellConstant;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\SandwellJournals\Traits\JournalOutputTrait;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\SandwellJournals\Traits\JournalDataTrait;

class DepreciationLines
{
    use JournalOutputTrait;
    use JournalDataTrait;

    public static function addLines(
        &$data,
        $codeList,
        $caFixedAssetId,
        $caFixedAssetCode,
        $subIFRS,
        $permissionService
    ) {
        // Some Sub Cats are treated the same.
        switch ($subIFRS) {
            case SandwellConstant::IFRS_SUB_OLB_PFI:
            case SandwellConstant::IFRS_SUB_OLB_GAR:
            case SandwellConstant::IFRS_SUB_SUR:
                $subIFRS = SandwellConstant::IFRS_SUB_OLB_GF;
                break;
            default:
                break;
        }

        $cyDepn = self::getInYearDepreciation($caFixedAssetId, $permissionService);

        // In Year Depn Applied to FA Registers.
        $data[] = self::addJournalRow(
            $codeList[$subIFRS][0][0],
            $codeList[$subIFRS][0][1],
            $codeList[$subIFRS][0][2],
            0,
            $cyDepn['cvCYDepn'],
            'FAR In Year Depreciation',
            ['DEPN', $caFixedAssetCode]
        );

        // In Year Depn Applied to RR.
        $data[] =  self::addJournalRow(
            $codeList[$subIFRS][1][0],
            $codeList[$subIFRS][1][1],
            $codeList[$subIFRS][1][2],
            $cyDepn['rrCYDepn'],
            0,
            'RR Revaluation Gain Depreciation',
            ['DEPN', $caFixedAssetCode]
        );

        // In Year Depn Applied to I and E.
        $data[] = self::addJournalRow(
            $codeList[$subIFRS][2][0],
            $codeList[$subIFRS][2][1],
            $codeList[$subIFRS][2][2],
            $cyDepn['cvCYDepn'],
            0,
            'CI&E Service In Year Depreciation (Asset Noted)',
            ['DEPN', $caFixedAssetCode]
        );
        $data[] = self::addJournalRow(
            $codeList[$subIFRS][3][0],
            $codeList[$subIFRS][3][1],
            $codeList[$subIFRS][3][2],
            0,
            $cyDepn['cvCYDepn'],
            'CI&E MRS In Year Depreciation',
            ['DEPN', $caFixedAssetCode]
        );

        // In Year Depn Applied to CAA.
        $data[] = self::addJournalRow(
            $codeList[$subIFRS][4][0],
            $codeList[$subIFRS][4][1],
            $codeList[$subIFRS][4][2],
            $cyDepn['cvCYDepn'],
            0,
            'CAA In Year Depreciation',
            ['DEPN', $caFixedAssetCode]
        );
        $data[] = self::addJournalRow(
            $codeList[$subIFRS][5][0],
            $codeList[$subIFRS][5][1],
            $codeList[$subIFRS][5][2],
            0,
            $cyDepn['rrCYDepn'],
            'CAA RR Revalation Gain Depreciation',
            ['DEPN', $caFixedAssetCode]
        );
    }
}
