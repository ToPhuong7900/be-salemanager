<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\SandwellJournals\JournalData;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Math;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\SandwellJournals\Traits\JournalOutputTrait;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\SandwellJournals\Traits\JournalDataTrait;

class DerecognitionLines
{
    use JournalOutputTrait;
    use JournalDataTrait;

    public static function addLines(
        &$data,
        $codeList,
        $caFixedAssetId,
        $caFixedAssetCode
    ) {
        $derecs = self::getDerecognitionData($caFixedAssetId);

        foreach ($derecs as $derec) {
            $gbv = Common::numberFormat(Math::negateCurrency($derec->gbv), thousandsSep: '');
            $accDepn = Common::numberFormat($derec->acc_depn, thousandsSep: '');
            $nbv = Common::numberFormat(Math::negateCurrency($derec->nbv), thousandsSep: '');

            // De-recognition of asset - application to fixed asset registers.
            $data[] = self::addJournalRow(
                $codeList[0][0],
                $codeList[0][1],
                $codeList[0][2],
                $accDepn,
                0,
                'FAR Write out of accumulated depreciation',
                ['DRC', $caFixedAssetCode]
            );
            $data[] = self::addJournalRow(
                $codeList[1][0],
                $codeList[1][1],
                $codeList[1][2],
                0,
                $gbv,
                'FAR De-recognition of Asset',
                ['DRC', $caFixedAssetCode]
            );

            // De-recognition of asset - application to revaluation reserve
            // (to the value of the credit on the RR account for the asset):

            $data[] = self::addJournalRow(
                $codeList[2][0],
                $codeList[2][1],
                $codeList[2][2],
                0,
                0,
                'RR De-recogntion of Asset',
                ['DRC', $caFixedAssetCode]
            );

            // De-recognition of asset - application to CAA:
            $data[] = self::addJournalRow(
                $codeList[3][0],
                $codeList[3][1],
                $codeList[3][2],
                $nbv,
                0,
                'CAA FAR De-recognition of Asset (Net Disposal)',
                ['DRC', $caFixedAssetCode]
            );

            $data[] = self::addJournalRow(
                $codeList[4][0],
                $codeList[4][1],
                $codeList[4][2],
                0,
                0,
                'CAA RR De-recognition of Asset',
                ['DRC', $caFixedAssetCode]
            );
        }
    }
}
