<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\SandwellJournals\JournalData;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Math;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\SandwellJournals\SandwellConstant;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\SandwellJournals\Traits\JournalOutputTrait;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\SandwellJournals\Traits\JournalDataTrait;

class RevalLossLines
{
    use JournalOutputTrait;
    use JournalDataTrait;

    public static function addLines(
        &$data,
        $codeList,
        $caFixedAssetId,
        $caFixedAssetCode,
        $subIFRS
    ) {
        $revals = self::getRevalData($caFixedAssetId, false);
        // Some Sub Cats are treated the same.
        switch ($subIFRS) {
            case SandwellConstant::IFRS_SUB_OLB_PFI:
            case SandwellConstant::IFRS_SUB_OLB_GAR:
            case SandwellConstant::IFRS_SUB_VPE_GF_EQ:
            case SandwellConstant::IFRS_SUB_VPE_GF_VEH:
            case SandwellConstant::IFRS_SUB_INF:
                $subIFRS = SandwellConstant::IFRS_SUB_OLB_GF;
                break;
            default:
                break;
        }

        //Common::numberFormat($cvBalance->cy_depreciation, thousandsSep: '')
        foreach ($revals as $reval) {
            $depnWORR = Common::numberFormat($reval->depn_wo_rr, thousandsSep: '');
            $depnWOSD = Common::numberFormat($reval->depn_wo_sd, thousandsSep: '');

            $revalReserveLoss = Common::numberFormat($reval->reval_reserve, thousandsSep: '');

            $revalLoss = Common::numberFormat(
                Math::negateCurrency(Math::addCurrency([$reval->reval_asset_value, $reval->reval_land_value])),
                thousandsSep: ''
            );

            // Revaluation loss - application to fixed asset registers.
            $data[] = self::addJournalRow(
                $codeList[$subIFRS][0][0],
                $codeList[$subIFRS][0][1],
                $codeList[$subIFRS][0][2],
                $depnWOSD,
                0,
                'FAR Write out of accumulated depreciation',
                ['RL', $caFixedAssetCode]
            );
            $data[] = self::addJournalRow(
                $codeList[$subIFRS][1][0],
                $codeList[$subIFRS][1][1],
                $codeList[$subIFRS][1][2],
                0,
                $revalLoss,
                'FAR Revaluation Loss',
                ['RL', $caFixedAssetCode]
            );

            // Revaluation loss - application to revaluation reserve
            // (to the value of the revaluation loss where the credit on the RR account equals or
            // exceeds that value for the asset):

            $data[] = self::addJournalRow(
                $codeList[$subIFRS][2][0],
                $codeList[$subIFRS][2][1],
                $codeList[$subIFRS][2][2],
                0,
                $depnWORR,
                'RR Write out of accumulated depreciation',
                ['RL', $caFixedAssetCode]
            );

            $data[] = self::addJournalRow(
                $codeList[$subIFRS][3][0],
                $codeList[$subIFRS][3][1],
                $codeList[$subIFRS][3][2],
                0,
                $revalReserveLoss,
                'RR Revaluation Loss',
                ['RL', $caFixedAssetCode]
            );


            // Revaluation loss - application to CI&E/CAA (where value of loss exceeds historic value)
            $data[] = self::addJournalRow(
                $codeList[$subIFRS][4][0],
                $codeList[$subIFRS][4][1],
                $codeList[$subIFRS][4][2],
                0,
                0,
                'CI&E Service Revaluation Loss (Asset Noted)',
                ['RL', $caFixedAssetCode]
            );

            $data[] = self::addJournalRow(
                $codeList[$subIFRS][5][0],
                $codeList[$subIFRS][5][1],
                $codeList[$subIFRS][5][2],
                0,
                0,
                'CI&E MRS Revaluation Loss',
                ['RL', $caFixedAssetCode]
            );

            $data[] = self::addJournalRow(
                $codeList[$subIFRS][6][0],
                $codeList[$subIFRS][6][1],
                $codeList[$subIFRS][6][2],
                0,
                0,
                'CAA Revaluation Loss',
                ['RL', $caFixedAssetCode]
            );
        }
    }
}
