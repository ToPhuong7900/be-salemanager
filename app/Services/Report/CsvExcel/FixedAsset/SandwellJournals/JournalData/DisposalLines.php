<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\SandwellJournals\JournalData;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Math;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\SandwellJournals\Traits\JournalOutputTrait;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\SandwellJournals\Traits\JournalDataTrait;

class DisposalLines
{
    use JournalOutputTrait;
    use JournalDataTrait;

    public static function addLines(
        &$data,
        $codeList,
        $caFixedAssetId,
        $caFixedAssetCode
    ) {
        $disposals = self::getDisposalData($caFixedAssetId);

        foreach ($disposals as $disposal) {
            $gbv = Common::numberFormat(Math::negateCurrency($disposal->gbv), thousandsSep: '');
            $accDepn = Common::numberFormat($disposal->acc_depn, thousandsSep: '');
            $rr = Common::numberFormat(Math::negateCurrency($disposal->rr), thousandsSep: '');
            $nbv = Common::numberFormat(Math::negateCurrency($disposal->nbv), thousandsSep: '');

            // Disposal of asset - application to fixed asset registers.
            $data[] = self::addJournalRow(
                $codeList[0][0],
                $codeList[0][1],
                $codeList[0][2],
                $accDepn,
                0,
                'FAR Write out of accumulated depreciation',
                ['DSP', $caFixedAssetCode]
            );
            $data[] = self::addJournalRow(
                $codeList[1][0],
                $codeList[1][1],
                $codeList[1][2],
                0,
                $gbv,
                'FAR Disposal of Asset',
                ['DSP', $caFixedAssetCode]
            );

            // Disposal of asset - application to revaluation reserve
            // (to the value of the credit on the RR account for the asset).

            $data[] = self::addJournalRow(
                $codeList[2][0],
                $codeList[2][1],
                $codeList[2][2],
                $rr,
                0,
                'RR Disposal of Asset',
                ['DSP', $caFixedAssetCode]
            );

            // Disposal of asset - application to CAA:
            $data[] = self::addJournalRow(
                $codeList[3][0],
                $codeList[3][1],
                $codeList[3][2],
                $nbv,
                0,
                'CAA FAR Disposal of Asset (Net Disposal)',
                ['DSP', $caFixedAssetCode]
            );

            $data[] = self::addJournalRow(
                $codeList[4][0],
                $codeList[4][1],
                $codeList[4][2],
                0,
                $rr,
                'CAA RR Disposal of Asset',
                ['DSP', $caFixedAssetCode]
            );
        }
    }
}
