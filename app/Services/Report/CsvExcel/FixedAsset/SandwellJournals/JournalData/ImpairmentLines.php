<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\SandwellJournals\JournalData;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Math;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\SandwellJournals\Traits\JournalOutputTrait;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\SandwellJournals\Traits\JournalDataTrait;

class ImpairmentLines
{
    use JournalOutputTrait;
    use JournalDataTrait;

    public static function addLines(
        &$data,
        $codeList,
        $caFixedAssetId,
        $caFixedAssetCode
    ) {
        $impairments = self::getImpairmentData($caFixedAssetId);

        //Common::numberFormat($cvBalance->cy_depreciation, thousandsSep: '')
        foreach ($impairments as $impairment) {
            $impValue = Common::numberFormat(Math::negateCurrency($impairment->impairment_value), thousandsSep: '');
            $hcValue = Common::numberFormat(Math::negateCurrency($impairment->historic_cost_value), thousandsSep: '');
            $rrValue = Common::numberFormat(Math::negateCurrency($impairment->rr_value), thousandsSep: '');

            // Impairment loss - application to fixed asset registers.
            $data[] = self::addJournalRow(
                $codeList[0][0],
                $codeList[0][1],
                $codeList[0][2],
                0,
                0,
                'FAR Write out of accumulated depreciation',
                ['IMP', $caFixedAssetCode]
            );
            $data[] = self::addJournalRow(
                $codeList[1][0],
                $codeList[1][1],
                $codeList[1][2],
                0,
                $impValue,
                'FAR Impairment Loss',
                ['IMP', $caFixedAssetCode]
            );

            // Impariment loss - application to revaluation reserve
            // (to the value of the impairment loss where the credit on the RR account equals or exceeds
            // that value for the asset):

            $data[] = self::addJournalRow(
                $codeList[2][0],
                $codeList[2][1],
                $codeList[2][2],
                0,
                0,
                'RR Write out of accumulated depreciation',
                ['IMP', $caFixedAssetCode]
            );

            $data[] = self::addJournalRow(
                $codeList[3][0],
                $codeList[3][1],
                $codeList[3][2],
                $rrValue,
                0,
                'RR Impairment Loss',
                ['IMP', $caFixedAssetCode]
            );


            // Impairment loss - application to CI&E/CAA (where value of loss exceeds historic value)
            $data[] = self::addJournalRow(
                $codeList[4][0],
                $codeList[4][1],
                $codeList[4][2],
                0,
                $hcValue,
                'CI&E Service Impairment Loss (Asset Noted)',
                ['IMP', $caFixedAssetCode]
            );

            $data[] = self::addJournalRow(
                $codeList[5][0],
                $codeList[5][1],
                $codeList[5][2],
                0,
                0,
                'CI&E MRS Impairment Loss',
                ['IMP', $caFixedAssetCode]
            );

            $data[] = self::addJournalRow(
                $codeList[6][0],
                $codeList[6][1],
                $codeList[6][2],
                0,
                0,
                'CAA Impairment Loss',
                ['IMP', $caFixedAssetCode]
            );
        }
    }
}
