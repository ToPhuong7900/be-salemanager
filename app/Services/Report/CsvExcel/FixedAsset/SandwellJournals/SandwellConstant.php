<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\SandwellJournals;

class SandwellConstant
{
    public const LIST_TEXT = '*List - Text';
    public const LIST_DATE = '*list - Date';

    public const GL_01999 = '01999';
    public const GL_52500 = '52500';
    public const SJ_X07006 = 'X07006';
    public const FIRST_SEGMENT = '0000';
    public const SECOND_SEGMENT = '0000';
    public const ORGANISATION = '01';

    public const IFRS_SUB_OLB_GF = 'OLB_OP_GF';
    public const IFRS_SUB_OLB_PFI = 'OLB_OP_GF_PFI';
    public const IFRS_SUB_OLB_GAR = 'OLB_OP_GF_GAR';

    public const IFRS_SUB_CD_SMBC = 'OLB_SMBC_CD';
    public const IFRS_SUB_CD_PFI = 'OLB_PFI_CD';
    public const IFRS_SUB_CD_HRA = 'OLB_OP_HRA';

    public const IFRS_SUB_VPE_GF_VEH = 'VPE_GF_VEH';
    public const IFRS_SUB_VPE_GF_EQ = 'VPE_GF_EQ';
    public const IFRS_SUB_VPE_HRA_EQ = 'VPE_HRA_EQ';

    public const IFRS_SUB_AHS_GF = 'CA_AHFS_GF';
    public const IFRS_SUB_AHS_HRA = 'CA_AHFS_HRA';

    public const IFRS_SUB_INF = 'INF';
    public const IFRS_SUB_SUR = 'SUR';
}
