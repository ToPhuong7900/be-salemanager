<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\CardiffJournals;

use Tfcloud\Lib\Math;
use Tfcloud\Models\CaTransactionType;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\CardiffJournals\CardiffConstant as CC;

class FarCliCFF03ReportService extends CardiffJournalPostingService
{
    protected $yearString = "";
    private $journalData = [];

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->addJournalHeader($this->journalData, CC::HDR_TXT_3, $this->yearString);
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        // Impairments for all assets

        $this->prepareImpairmentData();

        return $this->journalData;
    }

    private function prepareImpairmentData()
    {
        $sCostCentre = '';
        $curAssetValue = 0;
        $curLandValue = 0;
        $curHCAssetValue = 0;
        $curHCLandValue = 0;
        $curValue = 0;
        $curHCValue = 0;
        $lCount = 0;

        // Get additions data from database
        $assetData = $this->retrieveTransactionList(CaTransactionType::IMPAIRMENT)->get();

        foreach ($assetData as $asset) {
            $lCount++;

            $sIFRSCatId = $asset->ca_ifrs_category_id;

            $sIFRSSubType = $asset->ca_ifrs_category_desc;

            $ifrsCat = $asset->ifrsCat;

            $sCostCentre = $asset->ca_account_code;

            $caFixedAssetCode = $asset->ca_fixed_asset_code;

            $investment = false;
            $ahs = false;

            // Get sub-type accounts.
            $this->getIFRSAccount(
                $sIFRSCatId,
                $this->accountCode1,
                $this->accountCode2,
                $this->accountCode3,
                $this->accountCode4,
                $this->accountCode5,
                $this->accountCode6,
                $this->accountCode7,
                $investment,
                $ahs
            );

            // Get values (already positive no need to negate).
            $curAssetValue = $asset->cv_asset;
            $curLandValue = $asset->cv_land;
            $curHCAssetValue = $asset->hc_asset;
            $curHCLandValue = $asset->hc_land;

            $curValue = Math::addCurrency([$curAssetValue, $curLandValue]);
            $curHCValue = Math::addCurrency([$curHCAssetValue, $curHCLandValue]);

            // RR value = CV - HC values
            $curRRValue = Math::subCurrency([$curValue, $curHCValue]);

            // Debit Reval Reserve (if any debited)
            if (Math::greaterThanCurrency($curRRValue, 0)) {
                $sMsg = "Impairment to RR";
                $this->addJournalRow(
                    $this->journalData,
                    CC::RR,
                    $caFixedAssetCode,
                    '',
                    CC::PROF_CTR,
                    CC::DR,
                    $curRRValue,
                    $sMsg,
                    $ifrsCat
                );
            }


            // Debit I&E if more than RR.
            if (Math::greaterThanCurrency($curHCValue, 0)) {
                $sMsg = "Impairment to I&E";
                $this->addJournalRow(
                    $this->journalData,
                    $this->accountCode5,
                    $caFixedAssetCode,
                    $sCostCentre,
                    '',
                    CC::DR,
                    $curHCValue,
                    $sMsg,
                    $ifrsCat
                );
            }

            // Credit Accumlated Impairment.
            $sMsg = "Impairment";
            $this->addJournalRow(
                $this->journalData,
                $this->accountCode7,
                $caFixedAssetCode,
                '',
                CC::PROF_CTR,
                CC::CR,
                $curValue,
                $sMsg,
                $ifrsCat
            );
        }   //foreach asset.
    }
}
