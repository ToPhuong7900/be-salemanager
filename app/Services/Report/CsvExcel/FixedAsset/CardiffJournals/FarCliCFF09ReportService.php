<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\CardiffJournals;

use Tfcloud\Lib\Math;
use Tfcloud\Models\SiteGroup;
use Tfcloud\Models\Views\VwFar18;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\CardiffJournals\CardiffConstant as CC;

class FarCliCFF09ReportService extends CardiffJournalPostingService
{
    protected $yearString = "";
    private $journalData = [];

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->addJournalHeader($this->journalData, CC::HDR_TXT_9, $this->yearString);
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        // Asset by Asset Transfers

        $this->prepareTfrData();

        return $this->journalData;
    }

    private function prepareTfrData()
    {
        $curValue = 0;
        $curDepreciationMovent = 0;
        $curImpairmentMovement = 0;

        $sMsg = '';
        $lCount = 0;

        $assetData = $this->getData();

        foreach ($assetData as $asset) {
            // For transactions with zero value keep count of which side of the transfer we are in.
            // 1 for the donating transaction (Out).
            // 2 for the receiving transaction (In).
            if ($lCount >= 2) {
                $lCount = 0;
            }
            $lCount++;

            // Store ValuationKey for this transaction
            $sIFRSCatId = $asset->ca_ifrs_category_id;

            $ifrsCat = $asset->ifrsCat;

            $caFixedAssetCode = $asset->fixed_asset_code;

            $investment = false;
            $ahs = false;

            // Get sub-type accounts.
            $this->getIFRSAccount(
                $sIFRSCatId,
                $this->accountCode1,
                $this->accountCode2,
                $this->accountCode3,
                $this->accountCode4,
                $this->accountCode5,
                $this->accountCode6,
                $this->accountCode7,
                $investment,
                $ahs
            );

            // Get values.
            $curValue = $asset->gross_movement; // Math::addCurrency([$curAssetValue, $curLandValue]);
            $curDepreciationMovent = $asset->depreciation_movement;
            $curImpairmentMovement = $asset->impairment_movement;


            if (Math::equalCurrency($curValue, 0)) {
                $tfrDirection = $lCount == 1 ? 'out' : 'in';
                $tfrIn = $lCount == 1 ? false : true;
            } else {
                $tfrDirection = Math::greaterThanCurrency($curValue, 0) ? 'in' : 'out';
                $tfrIn = Math::greaterThanCurrency($curValue, 0) ? true : false;
            }

            // New IFRS Category
            if ($tfrIn) {
                if (Math::notEqualCurrency($curDepreciationMovent, 0)) {
                    $sMsg = "Transfer depreciation $tfrDirection";
                    $this->addJournalRow(
                        $this->journalData,
                        $this->accountCode2,
                        $caFixedAssetCode,
                        '',
                        CC::PROF_CTR,
                        CC::CR,
                        $curDepreciationMovent,
                        $sMsg,
                        $ifrsCat
                    );
                }

                if (Math::notEqualCurrency($curImpairmentMovement, 0)) {
                    $sMsg = "Transfer impairment $tfrDirection";
                    $this->addJournalRow(
                        $this->journalData,
                        $this->accountCode7,
                        $caFixedAssetCode,
                        '',
                        CC::PROF_CTR,
                        CC::CR,
                        $curImpairmentMovement,
                        $sMsg,
                        $ifrsCat
                    );
                }

                if (Math::notEqualCurrency($curValue, 0)) {
                    $sMsg = "Transfer GBV $tfrDirection";
                    $this->addJournalRow(
                        $this->journalData,
                        $this->accountCode1,
                        $caFixedAssetCode,
                        '',
                        CC::PROF_CTR,
                        CC::DR,
                        $curValue,
                        $sMsg,
                        $ifrsCat
                    );
                }
            } else {
                // Previous IFRS Category
                if (Math::notEqualCurrency($curDepreciationMovent, 0)) {
                    $sMsg = "Transfer depreciation $tfrDirection";
                    $this->addJournalRow(
                        $this->journalData,
                        $this->accountCode2,
                        $caFixedAssetCode,
                        '',
                        CC::PROF_CTR,
                        CC::DR,
                        Math::negateCurrency($curDepreciationMovent),
                        $sMsg,
                        $ifrsCat
                    );
                }

                if (Math::notEqualCurrency($curImpairmentMovement, 0)) {
                    $sMsg = "Transfer impairment $tfrDirection";
                    $this->addJournalRow(
                        $this->journalData,
                        $this->accountCode7,
                        $caFixedAssetCode,
                        '',
                        CC::PROF_CTR,
                        CC::DR,
                        Math::negateCurrency($curImpairmentMovement),
                        $sMsg,
                        $ifrsCat
                    );
                }

                if (Math::notEqualCurrency($curValue, 0)) {
                    $sMsg = "Transfer GBV $tfrDirection";
                    $this->addJournalRow(
                        $this->journalData,
                        $this->accountCode1,
                        $caFixedAssetCode,
                        '',
                        CC::PROF_CTR,
                        CC::CR,
                        Math::negateCurrency($curValue),
                        $sMsg,
                        $ifrsCat
                    );
                }
            }
        }   //foreach asset.

        return;
    }

    private function getData()
    {
        return VwFar18::join(
            'ca_ifrs_category',
            'ca_ifrs_category.ca_ifrs_category_id',
            '=',
            'vw_far18.ca_ifrs_category_id'
        )
        ->where('vw_far18.site_group_id', SiteGroup::get()->site_group_id)
        ->select(
            [
                'vw_far18.ca_fixed_asset_id',
                'vw_far18.ca_ifrs_category_id',
                'ca_ifrs_category.common_good',
                'vw_far18.hra_asset',
                'ca_ifrs_category.ca_ifrs_category_code',
                'ca_ifrs_category.ca_ifrs_category_desc',
                \DB::raw("CONCAT_WS('-', ca_ifrs_category_code, ca_ifrs_category_desc) AS ifrsCat"),
                \DB::raw('vw_far18.`Asset Code` as fixed_asset_code'),
                \DB::raw('vw_far18.`Gross Movement` as gross_movement'),
                \DB::raw('vw_far18.`Depreciation Movement` as depreciation_movement'),
                \DB::raw('vw_far18.`Impairment Movement` as impairment_movement'),
                \DB::raw('vw_far18.`Account Code` as account_code')
            ]
        )
        ->orderBy(\DB::raw('vw_far18.`Asset Code`'))
        ->get();
    }
}
