<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\CardiffJournals;

use DB;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\FixedAsset\CaFinYearManager;
use Tfcloud\Lib\Math;
use Tfcloud\Models\CaIfrsCategory;
use Tfcloud\Models\CaTransaction;
use Tfcloud\Models\SiteGroup;
use Tfcloud\Models\Views\VwFar06;
use Tfcloud\Models\Views\VwFar07;
use Tfcloud\Models\Views\VwCaCurrentFinYear;
use Tfcloud\Services\FixedAsset\BalanceService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\CsvExcel\CsvExcelReportBaseService;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\CardiffJournals\Traits\JournalOutputTrait;

abstract class CardiffJournalPostingService extends CsvExcelReportBaseService
{
    use JournalOutputTrait;

    protected $yearString = null;
    protected $permissionService = null;

    protected $accountCode1 = '';
    protected $accountCode2 = '';
    protected $accountCode3 = '';
    protected $accountCode4 = '';
    protected $accountCode5 = '';
    protected $accountCode6 = '';
    protected $accountCode7 = '';


    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->permissionService = $permissionService;
        $this->setYearString();
    }

    protected function textContains($toCheck, $toFind)
    {
        return strpos($toCheck, $toFind) > 0;
    }


    protected function getIFRSAccount(
        $caIFRSCatId,
        &$accountCode1,
        &$accountCode2,
        &$accountCode3,
        &$accountCode4,
        &$accountCode5,
        &$accountCode6,
        &$accountCode7,
        &$noHC = false,
        &$ahs = false,
        &$shortCode = '',
        &$ifrsCodeDesc = ''
    ) {
        $ifrsCat = CaIfrsCategory::where('ca_ifrs_category_id', $caIFRSCatId)
            ->where('site_group_id', SiteGroup::get()->site_group_id)
            ->first();

        $accountCode1 = $ifrsCat->account_code_1;
        $accountCode2 = $ifrsCat->account_code_2;
        $accountCode3 = $ifrsCat->account_code_3;
        $accountCode4 = $ifrsCat->account_code_4;

        // Account code 5 containd the codes for accum imp~in year imp.
        if ($ifrsCat->account_code_5) {
            $imp = explode('~', $ifrsCat->account_code_5);
            if (count($imp) > 1) {
                $accountCode5 = $imp[1];
                $accountCode7 = $imp[0];
            } else {
                $accountCode5 = $imp[0];
            }
        } else {
            $accountCode5 = $ifrsCat->account_code_5;
        }
        $accountCode6 = $ifrsCat->account_code_6;
        $noHC = Common::valueYorNtoBoolean($ifrsCat->investment);
        $ahs = Common::valueYorNtoBoolean($ifrsCat->asset_held_for_sale);
        $shortCode = $ifrsCat->ca_ifrs_category_code;
        $ifrsCodeDesc = Common::concatFields([$ifrsCat->ca_ifrs_category_code, $ifrsCat->ca_ifrs_category_desc]);
    }

    protected function retrieveTransactionList($transType, $transTypeTwo = null)
    {
        $query = CaTransaction::join(
            'ca_ifrs_category',
            'ca_ifrs_category.ca_ifrs_category_id',
            '=',
            'ca_transaction.ca_ifrs_cat_id'
        )
        ->join(
            'ca_fixed_asset',
            'ca_fixed_asset.ca_fixed_asset_id',
            '=',
            'ca_transaction.ca_fixed_asset_id'
        )
        ->join(
            'ca_account',
            'ca_account.ca_account_id',
            '=',
            'ca_fixed_asset.ca_account_code_id'
        )
        ->where('ca_fixed_asset.site_group_id', SiteGroup::get()->site_group_id)
        ->where('ca_fixed_asset.active', CommonConstant::DATABASE_VALUE_YES)
        ->where('ca_transaction.ca_fin_year_id', CaFinYearManager::currentFinYearId())
        ->select(
            [
                'ca_ifrs_category.ca_ifrs_category_id',
                'ca_ifrs_category.ca_ifrs_category_code',
                'ca_ifrs_category.ca_ifrs_category_desc',
                DB::raw("CONCAT_WS('-', ca_ifrs_category_code, ca_ifrs_category_desc) AS ifrsCat"),
                'ca_fixed_asset.ca_fixed_asset_code',
                'ca_account.ca_account_code',
                'ca_transaction.ca_transaction_type_id',
                'ca_transaction.cv_asset',
                'ca_transaction.cv_land',
                'ca_transaction.hc_asset',
                'ca_transaction.hc_land',
                'ca_transaction.depn_wo',
                'ca_transaction.cv_depn_adjustment',
                'ca_transaction.hc_depn_adjustment',
                'ca_fixed_asset.ca_fixed_asset_code',
                'ca_fixed_asset.ca_fixed_asset_id',
                'ca_transaction.disposal_proceeds',
                'ca_transaction.disposal_costs',
                'ca_transaction.effective_date',
                'ca_transaction.effective_date_override',
                'ca_transaction.disposal_mode_full'
            ]
        );

        if ($transTypeTwo) {
            $query->whereIn('ca_transaction_type_id', [$transType, $transTypeTwo]);
        } else {
            $query->where('ca_transaction_type_id', $transType);
        }

        return $query;
    }

    protected function hasHCDepnAdj()
    {
        $query = CaTransaction::where('hc_depn_adjustment', '<>', 0)
            ->whereNotNull('hc_depn_adjustment')
            ->where('site_group_id', SiteGroup::get()->site_group_id)
            ->where('ca_fin_year_id', CaFinYearManager::currentFinYearId());

        return $query->count() > 0;
    }

    protected function hasReval($caFixedAssetId)
    {
        $query = CaTransaction::where('ca_transaction_type_id', CaTransactionType::REVALUATION)
            ->where('ca_fixed_asset_id', $caFixedAssetId)
            ->where('site_group_id', SiteGroup::get()->site_group_id)
            ->where('ca_fin_year_id', CaFinYearManager::currentFinYearId());

        return $query->count() > 0;
    }

    protected function retrieveBalance($fixedAssetId, $finYearId, $balanceType)
    {
        $balanceService = new BalanceService($this->permissionService);

        return $balanceService->retrieveByYear($fixedAssetId, $finYearId, $balanceType);
    }

    protected function getBFwdAccumulatedBalances(
        $caFixedAssetId,
        &$curBfwdGBV,
        &$curBFwdDepn,
        &$curBFwdImp,
        &$curCYDepn,
        &$curHCCYDepn,
        &$curBFwdRR
    ) {
        $cv = $this->getCurrentAssetData($caFixedAssetId);
        $hc = $this->getHistoricAssetData($caFixedAssetId);

        $curBfwdGBV = $cv->COpenGBV;
        $curBFwdDepn = $cv->COpenDPN;
        $curBFwdImp = Math::negateCurrency(Math::addCurrency([$cv->COpenIMPA, $cv->COpenIMPL]));
        $curCYDepn = $cv->CDPN;
        $curHCCYDepn = $hc->amount;
        $curBFwdRR = Math::addCurrency([$cv->BFAssetRR, $cv->BFLandRR]);
    }

    protected function getRevalutionData($historic = false, $revalUp = true, $fixedAssetId = null)
    {
        $query = VwFar06::join(
            'ca_fixed_asset',
            'ca_fixed_asset.ca_fixed_asset_id',
            '=',
            'vw_far06.ca_fixed_asset_id'
        )
        ->join(
            'ca_ifrs_category',
            'ca_ifrs_category.ca_ifrs_category_id',
            '=',
            'ca_fixed_asset.ca_ifrs_cat_id'
        )
        ->join(
            'ca_transaction',
            'ca_transaction.ca_transaction_id',
            '=',
            'vw_far06.ca_transaction_id'
        )
        ->leftJoin(
            DB::raw(
                "(SELECT SUM(`Asset Value` + `Land Value`) AS nbv_add, ca_fixed_asset_id " .
                "FROM vw_far16 " .
                "GROUP BY ca_fixed_asset_id " .
                ") add_trans "
            ),
            'add_trans.ca_fixed_asset_id',
            '=',
            'ca_fixed_asset.ca_fixed_asset_id'
        )

        ->where('vw_far06.site_group_id', SiteGroup::get()->site_group_id)
        ->select(
            [
                'vw_far06.ca_fixed_asset_id',
                'ca_ifrs_category.ca_ifrs_category_code',
                'ca_ifrs_category.ca_ifrs_category_desc',
                'ca_ifrs_category.common_good',
                'vw_far06.hra_asset',
                DB::raw('COALESCE(add_trans.nbv_add, 0) AS nbv_add'),
                DB::raw('ca_fixed_asset.ca_ifrs_cat_id AS ca_ifrs_category_id'),
                DB::raw('vw_far06.`Asset Code` as fixed_asset_code'),
                DB::raw('vw_far06.`Account Code` as account_code'),
                'ca_ifrs_category.account_code_1',
                'ca_ifrs_category.account_code_2',
                'ca_ifrs_category.account_code_3',
                DB::raw('vw_far06.`Gross RR` as gross_rr'),
                DB::raw('vw_far06.`Gross SD` as gross_sd'),
                DB::raw('vw_far06.`Depn WOff RR` as depn_woff_rr'),
                DB::raw('vw_far06.`Depn WOff SD` as depn_woff_sd'),
                DB::raw('vw_far06.`Imp WOff RR` as imp_woff_rr'),
                DB::raw('vw_far06.`Imp WOff SD` as imp_woff_sd'),
                DB::raw('vw_far06.`Reval Reserve` as reval_reserve'),
                DB::raw('vw_far06.`Surplus/Deficit` as surplus_deficit'),
                DB::raw('vw_far06.`Asset Loss Reversal` + vw_far06.`Land Loss Reversal` as loss_reversal'),
                DB::raw(
                    'vw_far06.`Asset Impairment Reversal` + vw_far06.`Land Impairment Reversal` as impairment_reversal'
                ),
            ]
        )
        ->orderBy(DB::raw('vw_far06.`IFRS Code`'))
        ->orderBy(DB::raw('vw_far06.`Asset Code`'))
        ;

        $rawStatement = "vw_far06.cfwd_gbv - (vw_far06.`BFWD Total GBV` + COALESCE(add_trans.nbv_add, 0))";


        if ($revalUp) {
            $query->whereRaw("$rawStatement >= 0");
        } else {
            $query->whereRaw("$rawStatement < 0");
        }

        return $query;
    }

    // Is special processing required with a transfer.
    protected function checkIfrsCategory($caIfrsCategoryId)
    {
        $ifrsCat = CaIfrsCategory::where('ca_ifrs_category_id', $caIfrsCategoryId)->first();

        if (
            $ifrsCat->investment == CommonConstant::DATABASE_VALUE_YES
            || $ifrsCat->asset_held_for_sale == CommonConstant::DATABASE_VALUE_YES
            || $ifrsCat->ca_ifrs_category_code == 'CDwel'
        ) {
            return true;
        } else {
            return false;
        }
    }

    protected function getHistoricAssetData($caFixedAssetId)
    {
        return VwFar07::userSiteGroup()
            ->leftJoin(
                'ca_ifrs_category as bfwd_ca_ifrs_category',
                'bfwd_ca_ifrs_category.ca_ifrs_category_id',
                '=',
                'vw_far07.bfwd_ca_ifrs_category_id'
            )
            ->where('vw_far07.ca_balance_historic', 'Y')
            ->where('ca_fixed_asset_id', $caFixedAssetId)
            ->select(
                [
                    DB::raw('bfwd_ca_ifrs_category.ca_ifrs_category_desc AS bfwd_ca_ifrs_category_desc'),
                    'vw_far07.hra_asset',
                    'vw_far07.ca_fixed_asset_id',
                    'vw_far07.deminimis_asset',
                    DB::raw('vw_far07.`REV Asset Value` as HREVAV'),
                    DB::raw('vw_far07.`REV Land Value` as HREVLV'),
                    DB::raw('vw_far07.`IFRS Code` as ca_ifrs_category_code'),
                    DB::raw('vw_far07.`IFRS Description` as ca_ifrs_category_desc'),
                    DB::raw('vw_far07.`Previous IFRS Code` as bf_ca_ifrs_category_code'),
                    DB::raw('vw_far07.`Previous IFRS Description` as bf_ca_ifrs_category_desc'),
                    DB::raw('vw_far07.`Asset Code` as fixed_asset_code'),
                    DB::raw('vw_far07.`Asset Description` as fixed_asset_desc'),
                    DB::raw('vw_far07.`Account Code` as account_code'),
                    DB::raw('vw_far07.`CY Depreciation` as amount'),
                    DB::raw('vw_far07.`BFwd GBV` as HOpenGBV'),
                    'bfwd_ca_ifrs_category.amortisation as bfwd_amortisation',
                ]
            )
            ->orderBy(DB::raw('bfwd_ca_ifrs_category.ca_ifrs_category_desc'))
            ->orderBy(DB::raw('vw_far07.`Account Code`'))
            ->first();
    }

    protected function hasTransfer($caFixedAssetId, &$bfwdIfrsCatId)
    {
        $asset = VwFar07::userSiteGroup()
            ->leftJoin(
                'ca_ifrs_category as bfwd_ca_ifrs_category',
                'bfwd_ca_ifrs_category.ca_ifrs_category_id',
                '=',
                'vw_far07.bfwd_ca_ifrs_category_id'
            )
            ->where('vw_far07.ca_balance_historic', CommonConstant::DATABASE_VALUE_NO)
            ->where('ca_fixed_asset_id', $caFixedAssetId)
            ->select(
                [
                    'vw_far07.ca_ifrs_category_id',
                    'vw_far07.bfwd_ca_ifrs_category_id',
                    'bfwd_ca_ifrs_category.ca_ifrs_category_code',
                    'bfwd_ca_ifrs_category.ca_ifrs_category_desc',
                ]
            )
            ->first();

        $bfwdIfrsCatId = $asset->bfwd_ca_ifrs_category_id;

        return $asset->ca_ifrs_category_id != $asset->bfwd_ca_ifrs_category_id;
    }



    protected function getCurrentAssetData($caFixedAssetId)
    {
        return VwFar07::userSiteGroup()
            ->leftJoin(
                'ca_ifrs_category as bfwd_ca_ifrs_category',
                'bfwd_ca_ifrs_category.ca_ifrs_category_id',
                '=',
                'vw_far07.bfwd_ca_ifrs_category_id'
            )
            ->where('vw_far07.ca_balance_historic', 'N')
            ->where('ca_fixed_asset_id', $caFixedAssetId)
            ->select(
                [
                    DB::raw('bfwd_ca_ifrs_category.ca_ifrs_category_desc AS bfwd_ca_ifrs_category_desc'),
                    'vw_far07.hra_asset',
                    'vw_far07.ca_fixed_asset_id',
                    'vw_far07.deminimis_asset',
                    DB::raw('vw_far07.`REV Asset Value` as CREVAV'),
                    DB::raw('vw_far07.`REV Land Value` as CREVLV'),
                    DB::raw('vw_far07.`IFRS Code` as ca_ifrs_category_code'),
                    DB::raw('vw_far07.`IFRS Description` as ca_ifrs_category_desc'),
                    DB::raw('vw_far07.`Previous IFRS Code` as bf_ca_ifrs_category_code'),
                    DB::raw('vw_far07.`Previous IFRS Description` as bf_ca_ifrs_category_desc'),
                    DB::raw('vw_far07.`Asset Code` as fixed_asset_code'),
                    DB::raw('vw_far07.`Asset Description` as fixed_asset_desc'),
                    DB::raw('vw_far07.`Account Code` as account_code'),
                    DB::raw('vw_far07.`Accumulated Depreciation` as COpenDPN'),
                    DB::raw('vw_far07.`CY Depreciation` as CDPN'),
                    DB::raw('vw_far07.`CFwd Accumulated Depreciation` as CCFwdDPN'),
                    DB::raw('vw_far07.`Accumulated Impairment Asset` as COpenIMPA'),
                    DB::raw('vw_far07.`Accumulated Impairment Land` as COpenIMPL'),
                    DB::raw('vw_far07.`BFwd GBV` as COpenGBV'),
                    DB::raw('vw_far07.`BFwd RR Asset` as BFAssetRR'),
                    DB::raw('vw_far07.`BFwd RR Land` as BFLandRR'),
                    'bfwd_ca_ifrs_category.amortisation as bfwd_amortisation',
                ]
            )
            ->orderBy(DB::raw('bfwd_ca_ifrs_category.ca_ifrs_category_desc'))
            ->orderBy(DB::raw('vw_far07.`Account Code`'))
            ->first();
    }

    private function getDate()
    {
        $dateTime = new \DateTime('now');
        return "{$dateTime->format('d.m.Y')}";
    }

    private function setYearString()
    {
        $openCaFinYear = VwCaCurrentFinYear::userSiteGroup()
            ->select('ca_fin_year_code')
            ->first();

        if ($openCaFinYear) {
            $this->yearString = $openCaFinYear->ca_fin_year_code;
        }
    }
}
