<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\CardiffJournals;

use Tfcloud\Lib\Math;
use Tfcloud\Lib\FixedAsset\CaFinYearManager;
use Tfcloud\Models\CaTransactionType;
use Tfcloud\Models\Views\VwFar07;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\CardiffJournals\CardiffConstant as CC;

class FarCliCFF04ReportService extends CardiffJournalPostingService
{
    protected $yearString = "";
    private $journalData = [];

    private const MSG_REV_UP = 'Revaluation Up';

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->addJournalHeader($this->journalData, CC::HDR_TXT_5, $this->yearString);
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        // Valuation Transaction Journal

        $this->prepareRevaluationData();

        return $this->journalData;
    }

    private function prepareRevaluationData()
    {
        $sCostCentre = '';

        $bNoHistoricCost = false;

        $lCount = 0;

        $caFixedAssetId = 0;

        $bHasTransfer = false;

        // Get Reval Up data from database
        $assetData = $this->getRevalutionData(false, true)->get();

        foreach ($assetData as $asset) {
            $lCount++;

            $caFixedAssetId = $asset->ca_fixed_asset_id;
            $caIFRSCatId = $asset->ca_ifrs_category_id;
            $bfwdIfrsCatId = null;
            $ifrsCat = null;
            $caFixedAssetCode = $asset->fixed_asset_code;
            $ahs = false;
            $code = null;

            $bHasTransfer = $this->hasTransfer($caFixedAssetId, $bfwdIfrsCatId);

            if ($bHasTransfer) {
                $caIFRSCatId = $bfwdIfrsCatId;
            }

            // Get sub-type accounts.
            $this->getIFRSAccount(
                $caIFRSCatId,
                $this->accountCode1,
                $this->accountCode2,
                $this->accountCode3,
                $this->accountCode4,
                $this->accountCode5,
                $this->accountCode6,
                $this->accountCode7,
                $bNoHistoricCost,
                $ahs,
                $code,
                $ifrsCat
            );

            // Get cost centre for asset/valuation record
            $sCostCentre = $asset->account_code;

            $curRevalUpRR = Math::lessThanCurrency($asset->gross_rr, 0) ?
                Math::negateCurrency($asset->gross_rr) : $asset->gross_rr;
            $curRevalUpSD = Math::addCurrency([$asset->gross_sd, $asset->loss_reversal, $asset->impairment_reversal]);
            $curDepnWORR = $asset->depn_woff_rr;
            $curDepnWOSD = $asset->depn_woff_sd;
            $curImpWORR = $asset->imp_woff_rr;
            $curImpWOSD = $asset->imp_woff_sd;
            $movementInRR = $asset->reval_reserve;

            if (!$bNoHistoricCost) {
                // RR.
                if (Math::greaterThanCurrency($curRevalUpRR, 0)) {
                    $sMsg = self::MSG_REV_UP;
                    $this->addJournalRow(
                        $this->journalData,
                        $this->accountCode1,
                        $caFixedAssetCode,
                        '',
                        CC::PROF_CTR,
                        Math::greaterThanCurrency($curDepnWORR, $movementInRR) ? CC::CR : CC::DR,
                        $curRevalUpRR,
                        $sMsg,
                        $ifrsCat
                    );
                }

                if (Math::greaterThanCurrency($curRevalUpRR, 0)) {
                    $sMsg = "Revaluation Up RR";
                    $this->addJournalRow(
                        $this->journalData,
                        CC::RR,
                        $caFixedAssetCode,
                        '',
                        CC::PROF_CTR,
                        Math::greaterThanCurrency($curDepnWORR, $movementInRR) ? CC::DR : CC::CR,
                        $curRevalUpRR,
                        $sMsg,
                        $ifrsCat
                    );
                }

                // SD.
                if (Math::greaterThanCurrency($curRevalUpSD, 0)) {
                    $sMsg = self::MSG_REV_UP;
                    $this->addJournalRow(
                        $this->journalData,
                        $this->accountCode1,
                        $caFixedAssetCode,
                        '',
                        CC::PROF_CTR,
                        CC::DR,
                        $curRevalUpSD,
                        $sMsg,
                        $ifrsCat
                    );
                }

                if (Math::greaterThanCurrency($curRevalUpSD, 0)) {
                    $sMsg = "Reversal to I&E";
                    $this->addJournalRow(
                        $this->journalData,
                        $this->accountCode6,
                        $caFixedAssetCode,
                        $sCostCentre,
                        '',
                        CC::CR,
                        $curRevalUpSD,
                        $sMsg,
                        $ifrsCat
                    );
                }

                // Written off depreciation RR
                if (Math::greaterThanCurrency($curDepnWORR, 0)) {
                    $sMsg = "Accumulated Depreciation";
                    $this->addJournalRow(
                        $this->journalData,
                        $this->accountCode2,
                        $caFixedAssetCode,
                        '',
                        CC::PROF_CTR,
                        CC::DR,
                        $curDepnWORR,
                        $sMsg,
                        $ifrsCat
                    );
                    $sMsg = "Upward Revaluation";
                    $this->addJournalRow(
                        $this->journalData,
                        CC::RR,
                        $caFixedAssetCode,
                        '',
                        CC::PROF_CTR,
                        CC::CR,
                        $curDepnWORR,
                        $sMsg,
                        $ifrsCat
                    );
                }

                // Written off depreciation SD
                if (Math::greaterThanCurrency($curDepnWOSD, 0)) {
                    $sMsg = "Accumulated Depreciation";
                    $this->addJournalRow(
                        $this->journalData,
                        $this->accountCode2,
                        $caFixedAssetCode,
                        '',
                        CC::PROF_CTR,
                        CC::DR,
                        $curDepnWOSD,
                        $sMsg,
                        $ifrsCat
                    );
                    $sMsg = "Upward Revaluation";
                    $this->addJournalRow(
                        $this->journalData,
                        $this->accountCode6,
                        $caFixedAssetCode,
                        $sCostCentre,
                        '',
                        CC::CR,
                        $curDepnWOSD,
                        $sMsg,
                        $ifrsCat
                    );
                }

                // Written off impairment RR
                if (Math::greaterThanCurrency($curImpWORR, 0)) {
                    $sMsg = "Accumulated Impairment";
                    $this->addJournalRow(
                        $this->journalData,
                        $this->accountCode7,
                        $caFixedAssetCode,
                        '',
                        CC::PROF_CTR,
                        CC::DR,
                        $curImpWORR,
                        $sMsg,
                        $ifrsCat
                    );
                    $sMsg = "Impairment Writeback to RR";
                    $this->addJournalRow(
                        $this->journalData,
                        CC::RR,
                        $caFixedAssetCode,
                        '',
                        CC::PROF_CTR,
                        CC::CR,
                        $curImpWORR,
                        $sMsg,
                        $ifrsCat
                    );
                }

                // Written off impairment SD
                if (Math::greaterThanCurrency($curImpWOSD, 0)) {
                    $sMsg = "Accumulated Impairment";
                    $this->addJournalRow(
                        $this->journalData,
                        $this->accountCode7,
                        $caFixedAssetCode,
                        '',
                        CC::PROF_CTR,
                        CC::DR,
                        $curImpWOSD,
                        $sMsg,
                        $ifrsCat
                    );
                    $sMsg = "Impairment Writeback I&E";
                    $this->addJournalRow(
                        $this->journalData,
                        $this->accountCode5,
                        $caFixedAssetCode,
                        $sCostCentre,
                        '',
                        CC::CR,
                        $curImpWOSD,
                        $sMsg,
                        $ifrsCat
                    );
                }
            }

            /////// Charge to I&E ///////

            // Investment class Assets gain/loss always to I&E excluding ones transferred in this year.
            if ($bNoHistoricCost && Math::greaterThanCurrency($curRevalUpSD, 0)) {
                $sMsg = self::MSG_REV_UP;
                $this->addJournalRow(
                    $this->journalData,
                    $this->accountCode1,
                    $caFixedAssetCode,
                    '',
                    CC::PROF_CTR,
                    CC::DR,
                    $curRevalUpSD,
                    $sMsg,
                    $ifrsCat
                );
                $sMsg = "Revaluation Gain Investment";
                $this->addJournalRow(
                    $this->journalData,
                    $this->accountCode6,
                    $caFixedAssetCode,
                    $sCostCentre,
                    '',
                    CC::CR,
                    $curRevalUpSD,
                    $sMsg,
                    $ifrsCat
                );
            }
        }   //foreach asset.
    }
}
