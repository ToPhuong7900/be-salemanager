<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\CardiffJournals;

use DB;
use Tfcloud\Lib\Math;
use Tfcloud\Models\Views\VwFar07;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\CardiffJournals\CardiffConstant as CC;

class FarCliCFF06ReportService extends CardiffJournalPostingService
{
    protected $yearString = "";
    private $journalData = [];

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->addJournalHeader($this->journalData, CC::HDR_TXT_6, $this->yearString);
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        // Asset by Asset Depreciation for all assets

        $this->prepareDepreciationData();

        return $this->journalData;
    }

    private function prepareDepreciationData()
    {

        $sCostCentre = '';

        $lCount = 0;

        $assetData = $this->getAllAssetData();

        foreach ($assetData as $asset) {
            $lCount++;

            $caIFRSCatId = $asset->ca_ifrs_category_id;

            $caFixedAssetCode = $asset->fixed_asset_code;

            $ifrsCat = $asset->ifrsCat;

            $bNoHistoricCost = false;
            $ahs = false;

            // Get sub-type accounts.
            $this->getIFRSAccount(
                $caIFRSCatId,
                $this->accountCode1,
                $this->accountCode2,
                $this->accountCode3,
                $this->accountCode4,
                $this->accountCode5,
                $this->accountCode6,
                $this->accountCode7,
                $bNoHistoricCost,
                $ahs
            );

            // 2nd part of group is cost centre
            $sCostCentre = $asset->account_code;

            $cyDepn = $asset->amount;

            if (!$bNoHistoricCost && Math::greaterThanCurrency($cyDepn, 0)) {
                $sMsg = "In year Depreciation";
                $this->addJournalRow(
                    $this->journalData,
                    $this->accountCode3,
                    $caFixedAssetCode,
                    $sCostCentre,
                    '',
                    CC::DR,
                    $cyDepn,
                    $sMsg,
                    $ifrsCat
                );

                $sMsg = "Accumulated Depreciation";
                $this->addJournalRow(
                    $this->journalData,
                    $this->accountCode2,
                    $caFixedAssetCode,
                    '',
                    CC::PROF_CTR,
                    CC::CR,
                    $cyDepn,
                    $sMsg,
                    $ifrsCat
                );
            }
        }   //foreach asset.
    }

    private function getAllAssetData()
    {
        return VwFar07::userSiteGroup()
        ->leftJoin(
            'ca_ifrs_category as bfwd_ca_ifrs_category',
            'bfwd_ca_ifrs_category.ca_ifrs_category_id',
            '=',
            'vw_far07.bfwd_ca_ifrs_category_id'
        )
        ->leftJoin(
            'ca_ifrs_category',
            'ca_ifrs_category.ca_ifrs_category_id',
            '=',
            'vw_far07.ca_ifrs_category_id'
        )
        ->join('ca_fixed_asset', 'ca_fixed_asset.ca_fixed_asset_id', '=', 'vw_far07.ca_fixed_asset_id')
        ->where('vw_far07.ca_balance_historic', 'N')
        ->select(
            [
                DB::raw('bfwd_ca_ifrs_category.ca_ifrs_category_desc AS bfwd_ca_ifrs_category_desc'),
                'vw_far07.hra_asset',
                'vw_far07.ca_ifrs_category_id',
                'vw_far07.ca_fixed_asset_id',
                'vw_far07.deminimis_asset',
                DB::raw("CONCAT_WS('-', ca_ifrs_category.ca_ifrs_category_code, "
                    . "ca_ifrs_category.ca_ifrs_category_desc) AS ifrsCat"),
                DB::raw('vw_far07.`IFRS Code` as ca_ifrs_category_code'),
                DB::raw('vw_far07.`IFRS Description` as ca_ifrs_category_desc'),
                DB::raw('vw_far07.`Previous IFRS Code` as bf_ca_ifrs_category_code'),
                DB::raw('vw_far07.`Previous IFRS Description` as bf_ca_ifrs_category_desc'),
                DB::raw('vw_far07.`Asset Code` as fixed_asset_code'),
                DB::raw('vw_far07.`Asset Description` as fixed_asset_desc'),
                DB::raw('vw_far07.`Account Code` as account_code'),
                DB::raw('vw_far07.`CY Depreciation` * -1 as amount'),
                'bfwd_ca_ifrs_category.amortisation as bfwd_amortisation',
            ]
        )
        ->orderByRaw('bfwd_ca_ifrs_category.ca_ifrs_category_desc')
        ->orderByRaw('vw_far07.`IFRS Code`')
        ->orderByRaw('vw_far07.`Account Code`')
        ->get();
    }
}
