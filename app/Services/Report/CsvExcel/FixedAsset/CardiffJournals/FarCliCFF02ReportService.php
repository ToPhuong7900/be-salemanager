<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\CardiffJournals;

use Tfcloud\Lib\Math;
use Tfcloud\Models\SiteGroup;
use Tfcloud\Models\Views\VwFar14;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\CardiffJournals\CardiffConstant as CC;

class FarCliCFF02ReportService extends CardiffJournalPostingService
{
    protected $yearString = "";
    private $journalData = [];

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->addJournalHeader($this->journalData, CC::HDR_TXT_2, $this->yearString);
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        // Asset by Asset Disposals

        $this->prepareDspData();

        return $this->journalData;
    }

    private function prepareDspData()
    {
        $sCostCentre = '';

        $curAccDepreciation = 0;
        $curAccImpairment = 0;

        $sMsg = '';

        $lCount = 0;

        $assetData = $this->getData();

        foreach ($assetData as $asset) {
            $lCount++;

            // Store ValuationKey for this transaction

            $sIFRSCatId = $asset->ca_ifrs_category_id;

            $ifrsCat = $asset->ifrsCat;

            $sCostCentre = $asset->account_code;

            $caFixedAssetCode = $asset->fixed_asset_code;

            $investment = false;
            $ahs = false;

            // Get sub-type accounts.
            $this->getIFRSAccount(
                $sIFRSCatId,
                $this->accountCode1,
                $this->accountCode2,
                $this->accountCode3,
                $this->accountCode4,
                $this->accountCode5,
                $this->accountCode6,
                $this->accountCode7,
                $investment,
                $ahs
            );


            // Get values.
            $curGBV = Math::negateCurrency($asset->total_gbv);
            $curNBV = Math::negateCurrency($asset->total_nbv);
            $curAccDepreciation = $asset->accumulated_depreciation;
            $curAccImpairment = $asset->accumulated_impairment;

            // If asset has reval in year then that will clear bfwd imp/depn
            // otherwise we need to clear balances here.
            // Write out depreciation balances.
            if (!$investment) {
                if (Math::notEqualCurrency($curAccDepreciation, 0)) {
                    $sMsg = "Disposal writeback depreciation";
                    $this->addJournalRow(
                        $this->journalData,
                        $this->accountCode2,
                        $caFixedAssetCode,
                        '',
                        CC::PROF_CTR,
                        CC::DR,
                        $curAccDepreciation,
                        $sMsg,
                        $ifrsCat
                    );
                }
            }

            if (Math::notEqualCurrency($curAccImpairment, 0)) {
                $sMsg = "Disposal writeback impairment";
                $this->addJournalRow(
                    $this->journalData,
                    $this->accountCode7,
                    $caFixedAssetCode,
                    '',
                    CC::PROF_CTR,
                    CC::DR,
                    $curAccImpairment,
                    $sMsg,
                    $ifrsCat
                );
            }

            // DR I&E
            if (Math::notEqualCurrency($curNBV, 0)) {
                $sMsg = "Disposal to revenue";
                $this->addJournalRow(
                    $this->journalData,
                    CC::DSP,
                    $caFixedAssetCode,
                    $sCostCentre,
                    '',
                    CC::DR,
                    $curNBV,
                    $sMsg,
                    $ifrsCat
                );
            }


            // Dr Fixed Carrying Amount (amount of loss reversed Gross)
            // Add in amount of depn and impairment to get back to gross amount
            // Cr carrying amount.
            if (Math::notEqualCurrency($curGBV, 0)) {
                $sMsg = "Disposal NBV";
                $this->addJournalRow(
                    $this->journalData,
                    $this->accountCode1,
                    $caFixedAssetCode,
                    '',
                    CC::PROF_CTR,
                    CC::CR,
                    $curGBV,
                    $sMsg,
                    $ifrsCat
                );
            }
        }   //foreach asset.

        return;
    }

    private function getData()
    {
        return VwFar14::join(
            'ca_ifrs_category',
            'ca_ifrs_category.ca_ifrs_category_id',
            '=',
            'vw_far14.ca_ifrs_category_id'
        )
        ->where('vw_far14.site_group_id', SiteGroup::get()->site_group_id)
        ->select(
            [
                'vw_far14.ca_fixed_asset_id',
                'vw_far14.ca_ifrs_category_id',
                'ca_ifrs_category.common_good',
                'vw_far14.hra_asset',
                'ca_ifrs_category.ca_ifrs_category_code',
                'ca_ifrs_category.ca_ifrs_category_desc',
                \DB::raw("CONCAT_WS('-', ca_ifrs_category_code, ca_ifrs_category_desc) AS ifrsCat"),
                \DB::raw('vw_far14.`Full/Part` as full_part'),
                \DB::raw('vw_far14.`Asset Code` as fixed_asset_code'),
                \DB::raw('vw_far14.`Total GBV` as total_gbv'),
                \DB::raw('vw_far14.`Total NBV` as total_nbv'),
                \DB::raw('vw_far14.`Accumulated Depreciation` as accumulated_depreciation'),
                \DB::raw('vw_far14.`Accumulated Impairment` as accumulated_impairment'),
                \DB::raw('vw_far14.`Account Code` as account_code')
            ]
        )
        ->orderBy(\DB::raw('vw_far14.`Asset Code`'))
        ->get();
    }
}
