<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\CardiffJournals;

use Tfcloud\Lib\Math;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\CardiffJournals\CardiffConstant as CC;

class FarCliCFF05ReportService extends CardiffJournalPostingService
{
    protected $yearString = "";
    private $journalData = [];

    private const MSG_REV_DOWN = 'Revaluation Down';
    private const MSG_ACC_DPN = 'Accumulated Depreciation';
    private const MSG_ACC_IPM = 'Accumulated Impairment';

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->addJournalHeader($this->journalData, CC::HDR_TXT_4, $this->yearString);
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        // Valuation Down Transaction Journal

        $this->prepareRevaluationData();

        return $this->journalData;
    }

    private function prepareRevaluationData()
    {
        $sCostCentre = '';

        $bNoHistoricCost = false;

        $lCount = 0;

        $caFixedAssetId = 0;

        $bHasTransfer = false;

        // Get Reval Down data from database
        $assetData = $this->getRevalutionData(false, false)->get();

        foreach ($assetData as $asset) {
            $lCount++;

            $caFixedAssetId = $asset->ca_fixed_asset_id;
            $caIFRSCatId = $asset->ca_ifrs_category_id;
            $bfwdIfrsCatId = null;
            $ifrsCat = null;
            $caFixedAssetCode = $asset->fixed_asset_code;
            $ahs = false;
            $code = null;

            $bHasTransfer = $this->hasTransfer($caFixedAssetId, $bfwdIfrsCatId);

            if ($bHasTransfer) {
                $caIFRSCatId = $bfwdIfrsCatId;
            }

            // Get sub-type accounts.
            $this->getIFRSAccount(
                $caIFRSCatId,
                $this->accountCode1,
                $this->accountCode2,
                $this->accountCode3,
                $this->accountCode4,
                $this->accountCode5,
                $this->accountCode6,
                $this->accountCode7,
                $bNoHistoricCost,
                $ahs,
                $code,
                $ifrsCat
            );

            // Get cost centre for asset/valuation record
            $sCostCentre = $asset->account_code;

            // DR any movement in RR.
            $curRRLoss = Math::negateCurrency($asset->gross_rr);

            $curSDLoss = Math::negateCurrency(
                Math::addCurrency([$asset->gross_sd, $asset->loss_reversal, $asset->impairment_reversal])
            );

            $curDepnWORR = $asset->depn_woff_rr;
            $curDepnWOSD = $asset->depn_woff_sd;
            $curImpWORR = $asset->imp_woff_rr;
            $curImpWOSD = $asset->imp_woff_sd;
            $curImpairmentReversal = $asset->impairment_reversal;
            $curLossReversal = $asset->loss_reversal;
            $grossSD = $asset->gross_sd;

            if (!$bNoHistoricCost) {
                // RR
                if (Math::greaterThanCurrency($curRRLoss, 0)) {
                    $sMsg = "Revaluation Down RR";
                    $this->addJournalRow(
                        $this->journalData,
                        CC::RR,
                        $caFixedAssetCode,
                        '',
                        CC::PROF_CTR,
                        CC::DR,
                        $curRRLoss,
                        $sMsg,
                        $ifrsCat
                    );
                }
                if (Math::greaterThanCurrency($curRRLoss, 0)) {
                    $sMsg = self::MSG_REV_DOWN;
                    $this->addJournalRow(
                        $this->journalData,
                        $this->accountCode1,
                        $caFixedAssetCode,
                        '',
                        CC::PROF_CTR,
                        CC::CR,
                        $curRRLoss,
                        $sMsg,
                        $ifrsCat
                    );
                }

                if (Math::greaterThanCurrency($curLossReversal, 0) && Math::equalCurrency($grossSD, 0)) {
                    $sMsg = "Historic Losses";
                    $this->addJournalRow(
                        $this->journalData,
                        $this->accountCode1,
                        $caFixedAssetCode,
                        '',
                        CC::PROF_CTR,
                        CC::DR,
                        $curLossReversal,
                        $sMsg,
                        $ifrsCat
                    );
                    $this->addJournalRow(
                        $this->journalData,
                        $this->accountCode4,
                        $caFixedAssetCode,
                        '',
                        CC::PROF_CTR,
                        CC::CR,
                        $curLossReversal,
                        $sMsg,
                        $ifrsCat
                    );
                }

                // I&E
                if (Math::greaterThanCurrency($curSDLoss, 0)) {
                    $sMsg = "Revaluation Down I&E";
                    $this->addJournalRow(
                        $this->journalData,
                        $this->accountCode4,
                        $caFixedAssetCode,
                        $sCostCentre,
                        '',
                        CC::DR,
                        $curSDLoss,
                        $sMsg,
                        $ifrsCat
                    );
                }
                if (Math::greaterThanCurrency($curSDLoss, 0)) {
                    $sMsg = self::MSG_REV_DOWN;
                    $this->addJournalRow(
                        $this->journalData,
                        $this->accountCode1,
                        $caFixedAssetCode,
                        '',
                        CC::PROF_CTR,
                        CC::CR,
                        $curSDLoss,
                        $sMsg,
                        $ifrsCat
                    );
                }

                // Written off depreciation RR
                if (Math::greaterThanCurrency($curDepnWORR, 0)) {
                    $sMsg = self::MSG_ACC_DPN;
                    $this->addJournalRow(
                        $this->journalData,
                        $this->accountCode2,
                        $caFixedAssetCode,
                        '',
                        CC::PROF_CTR,
                        CC::DR,
                        $curDepnWORR,
                        $sMsg,
                        $ifrsCat
                    );
                    $sMsg = "Downward Revaluation";
                    $this->addJournalRow(
                        $this->journalData,
                        CC::RR,
                        $caFixedAssetCode,
                        $sCostCentre,
                        '',
                        CC::CR,
                        $curDepnWORR,
                        $sMsg,
                        $ifrsCat
                    );
                }

                // Written off depreciation I&E
                if (Math::greaterThanCurrency($curDepnWOSD, 0)) {
                    $sMsg = self::MSG_ACC_DPN;
                    $this->addJournalRow(
                        $this->journalData,
                        $this->accountCode2,
                        $caFixedAssetCode,
                        '',
                        CC::PROF_CTR,
                        CC::DR,
                        $curDepnWOSD,
                        $sMsg,
                        $ifrsCat
                    );
                    $sMsg = "Downward Revaluation";
                    $this->addJournalRow(
                        $this->journalData,
                        $this->accountCode4,
                        $caFixedAssetCode,
                        $sCostCentre,
                        '',
                        CC::CR,
                        $curDepnWOSD,
                        $sMsg,
                        $ifrsCat
                    );
                }

                // Written off impairment RR
                if (Math::greaterThanCurrency($curImpWORR, 0)) {
                    $sMsg = self::MSG_ACC_IPM;
                    $this->addJournalRow(
                        $this->journalData,
                        $this->accountCode7,
                        $caFixedAssetCode,
                        '',
                        CC::PROF_CTR,
                        CC::DR,
                        $curImpWORR,
                        $sMsg,
                        $ifrsCat
                    );
                    $sMsg = "Impairment Writeback to RR";
                    $this->addJournalRow(
                        $this->journalData,
                        CC::RR,
                        $caFixedAssetCode,
                        '',
                        CC::PROF_CTR,
                        CC::CR,
                        $curImpWORR,
                        $sMsg,
                        $ifrsCat
                    );
                }

                // Written off impairment SD
                if (Math::greaterThanCurrency($curImpWOSD, 0)) {
                    $sMsg = self::MSG_ACC_IPM;
                    $this->addJournalRow(
                        $this->journalData,
                        $this->accountCode7,
                        $caFixedAssetCode,
                        '',
                        CC::PROF_CTR,
                        CC::DR,
                        $curImpWOSD,
                        $sMsg,
                        $ifrsCat
                    );
                    $sMsg = "Impairment Writeback I&E";
                    $this->addJournalRow(
                        $this->journalData,
                        $this->accountCode5,
                        $caFixedAssetCode,
                        $sCostCentre,
                        '',
                        CC::CR,
                        $curImpWOSD,
                        $sMsg,
                        $ifrsCat
                    );
                }

                // Impairment Reversal
                if (Math::greaterThanCurrency($curImpairmentReversal, 0)) {
                    $sMsg = self::MSG_ACC_IPM;
                    $this->addJournalRow(
                        $this->journalData,
                        $this->accountCode7,
                        $caFixedAssetCode,
                        '',
                        CC::PROF_CTR,
                        CC::DR,
                        $curImpairmentReversal,
                        $sMsg,
                        $ifrsCat
                    );
                    $sMsg = "Impairment Reversal I&E";
                    $this->addJournalRow(
                        $this->journalData,
                        $this->accountCode5,
                        $caFixedAssetCode,
                        $sCostCentre,
                        '',
                        CC::CR,
                        $curImpairmentReversal,
                        $sMsg,
                        $ifrsCat
                    );
                }
            }

            /////// Charge to I&E ///////

            // Investment class Assets gain/loss always to I&E excluding ones transferred in this year
            if ($bNoHistoricCost && Math::greaterThanCurrency($curSDLoss, 0)) {
                $sMsg = self::MSG_REV_DOWN;
                $this->addJournalRow(
                    $this->journalData,
                    $this->accountCode4,
                    $caFixedAssetCode,
                    $sCostCentre,
                    '',
                    CC::DR,
                    $curSDLoss,
                    $sMsg,
                    $ifrsCat
                );
                $sMsg = "Revaluation Loss Investment";
                $this->addJournalRow(
                    $this->journalData,
                    $this->accountCode1,
                    $caFixedAssetCode,
                    '',
                    CC::PROF_CTR,
                    CC::CR,
                    $curSDLoss,
                    $sMsg,
                    $ifrsCat
                );
            }
        }   //foreach asset.
    }
}
