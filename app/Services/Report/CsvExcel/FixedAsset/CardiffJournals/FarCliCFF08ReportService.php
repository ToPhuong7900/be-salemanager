<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\CardiffJournals;

use DateTime;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Math;
use Tfcloud\Models\CaTransactionType;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\CardiffJournals\CardiffConstant as CC;

class FarCliCFF08ReportService extends CardiffJournalPostingService
{
    protected $yearString = "";
    private $journalData = [];

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->addJournalHeader($this->journalData, CC::HDR_TXT_8, $this->yearString);
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        // Asset by Asset Derecognitions

        $this->prepareDerecData();

        return $this->journalData;
    }

    private function prepareDerecData()
    {
        $sCostCentre = '';

        $curAssetValue = 0;
        $curLandValue = 0;
        $curHCAssetValue = 0;
        $curHCLandValue = 0;
        $curValue = 0;

        $curGrossValue = 0;
        $curBFwdGBV = 0;

        $curBFwdImp = 0;
        $curBFwdDepn = 0;
        $curBFwdRR = 0;

        $curCYDepn = 0;
        $curCYHCDepn = 0;

        $sMsg = '';
        $lCount = 0;
        $caFixedAssetId = 0;

        // Get Disposals data from database
        $assetData = $this->retrieveTransactionList(CaTransactionType::DERECOGNITION)
            ->orderBy('ca_fixed_asset_id')
            ->get();

        foreach ($assetData as $asset) {
            $lCount++;

            // Store ValuationKey for this transaction
            $caFixedAssetId = $asset->ca_fixed_asset_id;

            $sIFRSCatId = $asset->ca_ifrs_category_id;

            $ifrsCat = $asset->ifrsCat;

            $sCostCentre = $asset->ca_account_code;

            $caFixedAssetCode = $asset->ca_fixed_asset_code;

            $investment = false;
            $ahs = false;

            // Get sub-type accounts.
            $this->getIFRSAccount(
                $sIFRSCatId,
                $this->accountCode1,
                $this->accountCode2,
                $this->accountCode3,
                $this->accountCode4,
                $this->accountCode5,
                $this->accountCode6,
                $this->accountCode7,
                $investment,
                $ahs
            );

            // Get values.
            $curAssetValue = Math::negateCurrency($asset->cv_asset);
            $curLandValue = Math::negateCurrency($asset->cv_land);
            $curValue = Math::addCurrency([$curAssetValue, $curLandValue]);

            $curDepnWo = $asset->depn_wo;


            $curBFwdDepn = 0;
            $curBFwdImp = 0;
            $curCYDepn = 0;
            $curCYHCDepn = 0;

            /////// Write out the carrying amount of the Fixed Asset  ///////
            $this->getBFwdAccumulatedBalances(
                $caFixedAssetId,
                $curBFwdGBV,
                $curBFwdDepn,
                $curBFwdImp,
                $curCYDepn,
                $curCYHCDepn,
                $curBFwdRR
            );

            // Start current value
            $curGrossValue = $curValue;

            if (Common::valueYorNtoBoolean($asset->effective_date_override)) {
                $effectiveDate = (new DateTime($asset->effective_date));
                $bIsYearEnd = ($effectiveDate->format('d/m') == '31/03');
            } else {
                $bIsYearEnd = Common::valueYorNtoBoolean(\Auth::user()->siteGroup->ca_cal_mode_year_end);
            }


            // If asset has reval in year then that will clear bfwd imp/depn
            // otherwise we need to clear balances here.
            // Write out depreciation balances.
            if (!$investment) {
                if (!$this->hasReval($asset->ca_fixed_asset_id)) {
                    if (Math::notEqualCurrency($curDepnWo, 0)) {
                        $sMsg = "Derecognition writeback depreciation";
                        $this->addJournalRow(
                            $this->journalData,
                            $this->accountCode2,
                            $caFixedAssetCode,
                            '',
                            CC::PROF_CTR,
                            CC::DR,
                            $curDepnWo,
                            $sMsg,
                            $ifrsCat
                        );
                    }

                    $curGrossValue = Math::addCurrency([$curGrossValue, $curDepnWo]);
                }
            }

            // DR I&E
            $sMsg = "Derecognition to revenue";
            $this->addJournalRow(
                $this->journalData,
                CC::DSP,
                $caFixedAssetCode,
                $sCostCentre,
                '',
                CC::DR,
                $curValue,
                $sMsg,
                $ifrsCat
            );


            // Dr Fixed Carrying Amount (amount of loss reversed Gross)
            // Add in amount of depn and impairment to get back to gross amount
            // Cr carrying amount.
            $sMsg = "Derecognition NBV";
            $this->addJournalRow(
                $this->journalData,
                $this->accountCode1,
                $caFixedAssetCode,
                '',
                CC::PROF_CTR,
                CC::CR,
                $curGrossValue,
                $sMsg,
                $ifrsCat
            );
        }   //foreach asset.

        return;
    }
}
