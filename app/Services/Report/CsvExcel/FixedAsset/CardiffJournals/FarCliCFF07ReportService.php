<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\CardiffJournals;

use Tfcloud\Lib\Math;
use Tfcloud\Models\CaTransactionType;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\CardiffJournals\CardiffConstant as CC;

class FarCliCFF07ReportService extends CardiffJournalPostingService
{
    protected $yearString = "";
    private $journalData = [];

    private const MSG_ACC_DPN = 'Accumulated Depreciation';

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->addJournalHeader($this->journalData, CC::HDR_TXT_7, $this->yearString);
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        // Asset by Asset Written off Depreciation

        $this->prepareDepreciationData();

        return $this->journalData;
    }

    private function prepareDepreciationData()
    {

        $sCostCentre = '';

        $lCount = 0;

        $assetData = $this->retrieveTransactionList(
            CaTransactionType::REVALUATION,
            CaTransactionType::CATEGORY_TRANSFER
        )->get();

        foreach ($assetData as $asset) {
            $lCount++;

            $caIFRSCatId = $asset->ca_ifrs_category_id;

            $caFixedAssetCode = $asset->ca_fixed_asset_code;

            $ifrsCat = $asset->ifrsCat;

            $caTransactionTypeId = $asset->ca_transaction_type_id;

            $bNoHistoricCost = false;
            $ahs = false;

            // Get sub-type accounts.
            $this->getIFRSAccount(
                $caIFRSCatId,
                $this->accountCode1,
                $this->accountCode2,
                $this->accountCode3,
                $this->accountCode4,
                $this->accountCode5,
                $this->accountCode6,
                $this->accountCode7,
                $bNoHistoricCost,
                $ahs
            );

            $curAssetValue = $asset->cv_asset;
            $curLandValue = $asset->cv_land;

            $curValue = Math::addCurrency([$curAssetValue, $curLandValue]);


            // 2nd part of group is cost centre
            $sCostCentre = $asset->ca_account_code;

            $depnWo = $asset->depn_wo;

            if (Math::greaterThanCurrency($depnWo, 0)) {
                if ($caTransactionTypeId == CaTransactionType::REVALUATION) {
                    if (Math::lessThanCurrency($curValue, 0)) {
                        // Reval Loss.
                        $sMsg = self::MSG_ACC_DPN;
                        $this->addJournalRow(
                            $this->journalData,
                            $this->accountCode2,
                            $caFixedAssetCode,
                            '',
                            CC::PROF_CTR,
                            CC::DR,
                            $depnWo,
                            $sMsg,
                            $ifrsCat
                        );

                        $sMsg = "Downward Revaluation";
                        $this->addJournalRow(
                            $this->journalData,
                            $this->accountCode4,
                            $caFixedAssetCode,
                            $sCostCentre,
                            '',
                            CC::CR,
                            $depnWo,
                            $sMsg,
                            $ifrsCat
                        );
                    } else {
                        // Reval Gain.
                        $sMsg = self::MSG_ACC_DPN;
                        $this->addJournalRow(
                            $this->journalData,
                            $this->accountCode2,
                            $caFixedAssetCode,
                            '',
                            CC::PROF_CTR,
                            CC::DR,
                            $depnWo,
                            $sMsg,
                            $ifrsCat
                        );

                        $sMsg = "Upward Revaluation";
                        $this->addJournalRow(
                            $this->journalData,
                            $this->accountCode6,
                            $caFixedAssetCode,
                            $sCostCentre,
                            '',
                            CC::CR,
                            $depnWo,
                            $sMsg,
                            $ifrsCat
                        );
                    }
                } else {
                    $sMsg = self::MSG_ACC_DPN;
                    $this->addJournalRow(
                        $this->journalData,
                        $this->accountCode2,
                        $caFixedAssetCode,
                        '',
                        CC::PROF_CTR,
                        CC::DR,
                        $depnWo,
                        $sMsg,
                        $ifrsCat
                    );

                    $sMsg = "Reclassification";
                    $this->addJournalRow(
                        $this->journalData,
                        $this->accountCode3,
                        $caFixedAssetCode,
                        $sCostCentre,
                        '',
                        CC::CR,
                        $depnWo,
                        $sMsg,
                        $ifrsCat
                    );
                }
            }
        }   //foreach asset.
    }
}
