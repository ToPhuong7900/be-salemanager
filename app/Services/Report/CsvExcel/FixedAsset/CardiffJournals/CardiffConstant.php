<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\CardiffJournals;

class CardiffConstant
{
    // Asset Creation.
    public const RR = 98001;

    // Disposals.
    public const DSP = 63301;

    // Debit or Credit
    public const DR = 'D';
    public const CR = 'C';
    public const COMP_CODE = 1000;
    public const DOC_TYPE = 'TF';
    public const CURR = 'GBP';
    public const PROF_CTR = 'AZ999';
    public const REF = 'AA/XXX';

    public const HDR_TXT_1 = 'Asset Creation';
    public const HDR_TXT_2 = 'Disposals';
    public const HDR_TXT_3 = 'Impairments';
    public const HDR_TXT_4 = 'Revaluation Down';
    public const HDR_TXT_5 = 'Revaluation Up';
    public const HDR_TXT_6 = 'Depreciation';
    public const HDR_TXT_7 = 'Depreciation Writeback';
    public const HDR_TXT_8 = 'Derecognition';
    public const HDR_TXT_9 = 'Category Transfer';
}
