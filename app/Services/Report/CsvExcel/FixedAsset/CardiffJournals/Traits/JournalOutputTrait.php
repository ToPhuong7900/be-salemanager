<?php

namespace  Tfcloud\Services\Report\CsvExcel\FixedAsset\CardiffJournals\Traits;

use Tfcloud\Lib\Common;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\CardiffJournals\CardiffConstant as CC;

trait JournalOutputTrait
{
    protected function addJournalHeader(&$data, $headerText, $yearString)
    {
        $data[] = $this->newJournalRow(
            ['col_A' => 'GL Upload Data - Header']
        );

        $data[] = $this->newJournalRow(
            [
                'col_A' => 'Company Code',
                'col_B' => CC::COMP_CODE
            ]
        );

        $data[] = $this->newJournalRow(
            [
                'col_A' => 'Document Date',
                'col_B' => $this->getDate(),
                'col_I' => 'C'
            ]
        );

        $data[] = $this->newJournalRow(
            [
                'col_A' => 'Posting Date',
                'col_B' => $this->getDate()
            ]
        );

        $data[] = $this->newJournalRow(
            [
                'col_A' => 'Document Type',
                'col_B' => CC::DOC_TYPE
            ]
        );

        $data[] = $this->newJournalRow(
            [
                'col_A' => 'Currency',
                'col_B' => CC::CURR
            ]
        );

        $data[] = $this->newJournalRow(
            [
                'col_A' => 'Reference',
                'col_B' => CC::REF
            ]
        );

        $data[] = $this->newJournalRow(
            [
                'col_A' => 'Header Text',
                'col_B' => "$headerText $yearString"
            ]
        );

        $data[] = $this->newJournalRow();

        $data[] = $this->newJournalRow(
            ['col_A' => 'GL Upload - Line Items']
        );


        $data[] = $this->newJournalRow(
            [
                'col_A' => 'GL Account',
                'col_B' => 'Tax Code',
                'col_C' => 'Assignment',
                'col_D' => 'Company Code',
                'col_E' => 'Cost Centre',
                'col_F' => 'Profit Centre',
                'col_G' => 'Internal Order',
                'col_H' => 'WBS Element',
                'col_I' => 'D/C',
                'col_J' => 'Amount',
                'col_K' => 'Text',
                'col_L' => 'IFRS Category'
            ]
        );
    }

    public static function addJournalRow(
        &$data,
        $glAccountCode,
        $assignment,
        $costCentre,
        $profitCentre,
        $drcr,
        $amount,
        $text,
        $ifrsCode
    ) {

        $amountFormatted = Common::numberFormat($amount, false, ',', 2);

        $data[] = self::newJournalRow(
            [
                'col_A' => $glAccountCode,
                'col_C' => $assignment,
                'col_D' => CC::COMP_CODE,
                'col_E' => $costCentre,
                'col_F' => $profitCentre,
                'col_I' => $drcr,
                'col_J' => $amountFormatted,
                'col_K' => $text,
                'col_L' => $ifrsCode
            ]
        );
    }


    private static function newJournalRow($row = [])
    {
        return [
            'col_A'   => array_get($row, 'col_A', ''),
            'col_B'   => array_get($row, 'col_B', ''),
            'col_C'   => array_get($row, 'col_C', ''),
            'col_D'   => array_get($row, 'col_D', ''),
            'col_E'   => array_get($row, 'col_E', ''),
            'col_F'   => array_get($row, 'col_F', ''),
            'col_G'   => array_get($row, 'col_G', ''),
            'col_H'   => array_get($row, 'col_H', ''),
            'col_I'   => array_get($row, 'col_I', ''),
            'col_J'   => array_get($row, 'col_J', ''),
            'col_K'   => array_get($row, 'col_K', ''),
            'col_L'   => array_get($row, 'col_L', ''),
            'col_M'   => array_get($row, 'col_M', ''),
            'col_N'   => array_get($row, 'col_N', ''),
            'col_O'   => array_get($row, 'col_O', ''),
            'col_P'   => array_get($row, 'col_P', ''),
            'col_Q'   => array_get($row, 'col_Q', ''),
            'col_R'   => array_get($row, 'col_R', ''),
            'col_S'   => array_get($row, 'col_S', ''),
            'col_T'   => array_get($row, 'col_T', ''),
            'col_U'   => array_get($row, 'col_U', ''),
            'col_V'   => array_get($row, 'col_V', ''),
            'col_W'   => array_get($row, 'col_W', ''),
            'col_X'   => array_get($row, 'col_X', ''),
            'col_Y'   => array_get($row, 'col_Y', '')
        ];
    }
}
