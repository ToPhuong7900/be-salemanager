<?php

/**
 * /////////////////////////////////////////////////////////////////////////
 * // Impairments
 * /////////////////////////////////////////////////////////////////////////
 */

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset;

use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Math;
use Tfcloud\Models\Report;
use Tfcloud\Models\SiteGroup;
use Tfcloud\Models\Views\VwFar15;
use Tfcloud\Services\PermissionService;

class Far29ReportService extends JournalPostingService
{
    protected $entity = false;

    private $reportBoxFilterLoader = null;

    public function __construct(PermissionService $permissionService, $filterData, Report $report = null)
    {
        parent::__construct($permissionService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;

        $this->setFilterProperties($filterData);
    }

    private function setFilterProperties($filterData)
    {
        $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($filterData);
        $entity = $reportFilterQuery->getFirstValueFilterField('ca_entity_id');

        // get filter values
        $this->entity = (!$entity) ? false : $entity;
    }

    public function getReportData(&$filterText, &$sOrderText)
    {
        // If no admin ca_posting_account record available, exit.
        if (!$this->postingAcc) {
            return [];
        }

        $far15 = $this->getData();

        $outputData = [];
        $outputRow = [];

        $curTotalHRAMIRSReversal = 0;
        $curTotalMIRSReversal = 0;

        foreach ($far15 as $row) {
            $outputRow = [];

            $commonGood = $row->common_good === 'Y';
            $hraAsset = $row->hra_asset === 'Y';

            // Gross Move to Fixed Asset Balance Sheet
            $curGross = $row->impairment_value;
            $curRR = $row->rr_value;
            $curSD = $row->historic_cost_value;

            /**
             * ////////////////////////////////////////////////////////////////
             * // Credit accumulated impairment - then charge I&E or RR
             * ////////////////////////////////////////////////////////////////
             */
            if (Math::notEqualCurrency($curGross, 0)) {
                if ($hraAsset) {
                    $this->writeCodeSection(
                        $outputRow,
                        $this->postingAcc->acc_balance_sheet,
                        $row->account_code_3,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_hra_code
                    );
                } elseif ($commonGood) {
                    $this->writeCodeSection(
                        $outputRow,
                        $row->account_code,
                        $row->account_code_3,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_blank
                    );
                } else {
                    $this->writeCodeSection(
                        $outputRow,
                        $this->postingAcc->acc_balance_sheet,
                        $row->account_code_3,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_gf_code
                    );
                }
                $text = "{$row->fixed_asset_code} Impairment ({$row->ca_ifrs_category_code})";
                $this->writeCredit($outputRow, $curGross, $text);
                $this->newRptLine($outputData, $outputRow);
            }

            /**
             * ////////////////////////////////////////////////////////////////
             * // Dr the RR (if there is any)
             * ////////////////////////////////////////////////////////////////
             */
            if (Math::notEqualCurrency($curRR, 0)) {
                if ($hraAsset) {
                    $this->writeCodeSection(
                        $outputRow,
                        $this->postingAcc->acc_balance_sheet,
                        $this->postingAcc->sub_rr,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_hra_code
                    );
                    $text = "{$row->fixed_asset_code} Impairment to RR";
                    $this->writeDebit($outputRow, $curRR, $text, "RR");
                    $this->newRptLine($outputData, $outputRow);

                    $this->writeCodeSection(
                        $outputRow,
                        $this->postingAcc->acc_hra_mirs,
                        $this->postingAcc->sub_impairment_rr,
                        $this->postingAcc->prj_hra_mirs,
                        $this->postingAcc->fund_hra_rr
                    );
                    $this->writeCredit($outputRow, $curRR, "Impairment to RR");
                    $this->newRptLine($outputData, $outputRow);

                    $this->writeCodeSection(
                        $outputRow,
                        $this->postingAcc->acc_hra_mirs,
                        $this->postingAcc->sub_impairment_rr_downward,
                        $this->postingAcc->prj_hra_mirs,
                        $this->postingAcc->fund_hra_rr
                    );
                    $this->writeDebit($outputRow, $curRR, "Downward Impairment to RR");
                    $this->newRptLine($outputData, $outputRow);
                } elseif ($commonGood) {
                    $this->writeCodeSection(
                        $outputRow,
                        $row->account_code,
                        $this->postingAcc->sub_rr,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_blank
                    );
                    $text = "{$row->fixed_asset_code} Impairment to RR";
                    $this->writeDebit($outputRow, $curRR, $text, "RR (CG)");
                    $this->newRptLine($outputData, $outputRow);

                    $this->writeCodeSection(
                        $outputRow,
                        $row->account_code,
                        $this->postingAcc->sub_impairment_rr,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_blank
                    );
                    $this->writeCredit($outputRow, $curRR, "Impairment to RR");
                    $this->newRptLine($outputData, $outputRow);

                    $this->writeCodeSection(
                        $outputRow,
                        $row->account_code,
                        $this->postingAcc->sub_impairment_rr_downward,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_blank
                    );
                    $this->writeDebit($outputRow, $curRR, "Downward Impairment to RR");
                    $this->newRptLine($outputData, $outputRow);
                } else {
                    $this->writeCodeSection(
                        $outputRow,
                        $this->postingAcc->acc_balance_sheet,
                        $this->postingAcc->sub_rr,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_gf_code
                    );
                    $text = "{$row->fixed_asset_code} Impairment to RR";
                    $this->writeDebit($outputRow, $curRR, $text, "RR");
                    $this->newRptLine($outputData, $outputRow);

                    $this->writeCodeSection(
                        $outputRow,
                        $this->postingAcc->acc_mirs,
                        $this->postingAcc->sub_impairment_rr,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_blank
                    );
                    $this->writeCredit($outputRow, $curRR, "Impairment to RR");
                    $this->newRptLine($outputData, $outputRow);

                    $this->writeCodeSection(
                        $outputRow,
                        $this->postingAcc->acc_mirs,
                        $this->postingAcc->sub_impairment_rr_downward,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_blank
                    );
                    $this->writeDebit($outputRow, $curRR, "Downward Impairment to RR");
                    $this->newRptLine($outputData, $outputRow);
                }
            }

            /**
             * ////////////////////////////////////////////////////////////////
             * // Dr Charge to I&E (SURPLUS / DEFICIT)
             * ////////////////////////////////////////////////////////////////
             */
            if (Math::notEqualCurrency($curSD, 0)) {
                // Loss to I&E
                if ($hraAsset) {
                    $this->writeCodeSection(
                        $outputRow,
                        $row->account_code,
                        $this->postingAcc->sub_impairment_loss,
                        $this->postingAcc->prj_hra_mirs,
                        $this->postingAcc->fund_hra_impairment
                    );
                    $text = "{$row->fixed_asset_code} Impairment to Surplus/Deficit";
                    $this->writeDebit($outputRow, $curSD, $text);
                    $this->newRptLine($outputData, $outputRow);
                } else {
                    $this->writeCodeSection(
                        $outputRow,
                        $row->account_code,
                        $this->postingAcc->sub_impairment_loss,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_blank
                    );
                    $text = "{$row->fixed_asset_code} Impairment to Surplus/Deficit";
                    $this->writeDebit($outputRow, $curSD, $text);
                    $this->newRptLine($outputData, $outputRow);
                }

                // If common good reverse to its own account code directly
                if ($commonGood) {
                    // Write out total reversal to Cr MIRS for HRA
                    $this->writeCodeSection(
                        $outputRow,
                        $row->account_code,
                        $this->postingAcc->sub_impairment_loss_reversal,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_blank
                    );
                    $this->writeCredit($outputRow, $curSD, "Asset Impairment (CG)", "MIRS");
                    $this->newRptLine($outputData, $outputRow);

                    // Write out total Dr CAA line
                    $this->writeCodeSection(
                        $outputRow,
                        $row->account_code,
                        $this->postingAcc->sub_capital_adjustment_acc,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_blank
                    );
                    $this->writeDebit($outputRow, $curSD, "Asset Impairment (CG)", "CAA");
                    $this->newRptLine($outputData, $outputRow);
                } else {
                    // Reversal to MIRS (Cr) then CAA (Dr)
                    if ($hraAsset) {
                        $curTotalHRAMIRSReversal = Math::addCurrency([$curTotalHRAMIRSReversal, $curSD]);
                    } else {
                        $curTotalMIRSReversal = Math::addCurrency([$curTotalMIRSReversal, $curSD]);
                    }
                }
            }
        }

        // Write out total reversal to Cr GF via. MIRS
        $this->writeCodeSection(
            $outputRow,
            $this->postingAcc->acc_mirs,
            $this->postingAcc->sub_impairment_loss_reversal,
            $this->postingAcc->prj_blank_project,
            $this->postingAcc->fund_blank
        );
        $this->writeCredit($outputRow, $curTotalMIRSReversal, "Asset Impairment (GF)", "MIRS");
        $this->newRptLine($outputData, $outputRow);

        // Write out total Dr CAA line
        $this->writeCodeSection(
            $outputRow,
            $this->postingAcc->acc_balance_sheet,
            $this->postingAcc->sub_capital_adjustment_acc,
            $this->postingAcc->prj_blank_project,
            $this->postingAcc->fund_gf_code
        );
        $this->writeDebit($outputRow, $curTotalMIRSReversal, "Asset Impairment (GF)", "CAA");
        $this->newRptLine($outputData, $outputRow);

        // Write out total reversal to Cr MIRS for HRA
        $this->writeCodeSection(
            $outputRow,
            $this->postingAcc->acc_hra_mirs,
            $this->postingAcc->sub_impairment_loss_reversal,
            $this->postingAcc->prj_hra_mirs,
            $this->postingAcc->fund_hra_revaluation
        );
        $this->writeCredit($outputRow, $curTotalHRAMIRSReversal, "Asset Impairment (HRA)", "MIRS (HRA)");
        $this->newRptLine($outputData, $outputRow);

        // Write out total Dr CAA line
        $this->writeCodeSection(
            $outputRow,
            $this->postingAcc->acc_balance_sheet,
            $this->postingAcc->sub_capital_adjustment_acc,
            $this->postingAcc->prj_blank_project,
            $this->postingAcc->fund_hra_code
        );
        $this->writeDebit($outputRow, $curTotalHRAMIRSReversal, "Asset Impairment (HRA)", "CAA (HRA)");
        $this->newRptLine($outputData, $outputRow);

        return $outputData;
    }

    private function getData()
    {
        $query = VwFar15::join(
            'ca_ifrs_category',
            'ca_ifrs_category.ca_ifrs_category_id',
            '=',
            'vw_far15.ca_ifrs_category_id'
        )
        ->where('vw_far15.site_group_id', SiteGroup::get()->site_group_id)
        ->select(
            [
                'ca_ifrs_category.ca_ifrs_category_code',
                'ca_ifrs_category.common_good',
                'vw_far15.hra_asset',
                \DB::raw('vw_far15.`Asset Code` as fixed_asset_code'),
                \DB::raw('vw_far15.`Account Code` as account_code'),
                'ca_ifrs_category.account_code_3',
                \DB::raw('vw_far15.`Impairment Value` as impairment_value'),
                \DB::raw('vw_far15.`RR Value` as rr_value'),
                \DB::raw('vw_far15.`Historic Cost Value` as historic_cost_value'),
            ]
        )
        ->orderBy(\DB::raw('vw_far15.`IFRS Code`'))
        ->orderBy(\DB::raw('vw_far15.`Asset Code`'));

        if ($this->entity) {
            $query->where('vw_far15.ca_entity_id', $this->entity);
        }
        return $query->get();
    }
}
