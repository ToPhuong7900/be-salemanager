<?php

namespace  Tfcloud\Services\Report\CsvExcel\FixedAsset\GloucestershireJournals\Traits;

use Tfcloud\Services\Report\CsvExcel\FixedAsset\GloucestershireJournals\GloucestershireConstant;

trait JournalOutputTrait
{
    protected function addJournalHeader(&$data, $accDate)
    {
        $data[] = $this->newJournalRow();

        $data[] = $this->newJournalRow(
            [
                'col_B' => 'Gloucestershire County Council Journal Upload',
                'col_K' => 'Document Number:'
            ]
        );

        $data[] = $this->newJournalRow(
            [
                'col_K' => 'Originator:',
                'col_L' => 'Donna Townsend'
            ]
        );

        $data[] = $this->newJournalRow(
            [
                'col_E' => 'Defaults for the document header',
                'col_K' => 'Date Processed:'
            ]
        );

        $data[] = $this->newJournalRow(
            [
                'col_E' => 'Company Code',
                'col_F' => '1000',
                'col_K' => 'Authorised By:'
            ]
        );

        $data[] = $this->newJournalRow(
            [
                'col_E' => 'Document type',
                'col_F' => 'SA'
            ]
        );

        $data[] = $this->newJournalRow(
            [
                'col_E' => 'Currency',
                'col_F' => 'GBP'
            ]
        );

        $data[] = $this->newJournalRow(
            [
                'col_E' => 'Posting Date',
                'col_F' => $accDate,
                'col_H' => 'Debit'
            ]
        );

        $data[] = $this->newJournalRow(
            [
                'col_E' => 'Document Date',
                'col_F' => $accDate,
                'col_H' => 'Credit'
            ]
        );

        $data[] = $this->newJournalRow(
            [
                'col_E' => 'Posting Period',
                'col_F' => '15'
            ]
        );

        $data[] = $this->newJournalRow();

        $data[] = $this->newJournalRow(
            [
                'col_B' => 'Post Key',
                'col_C' => 'Account',
                'col_D' => 'Cost Centre',
                'col_E' => 'Internal Order',
                'col_F' => 'WBS Element',
                'col_G' => 'Profit Centre',
                'col_H' => 'Amount',
                'col_I' => 'Company Code',
                'col_J' => 'Tax Code',
                'col_K' => 'Assignment',
                'col_L' => 'Text (up to 50 characters)'
            ]
        );
    }

    public static function addJournalRow(
        &$data,
        $post,
        $account,
        $costCentre,
        $amount,
        $text
    ) {
        $data[] = self::newJournalRow(
            [
                'col_B' => $post,
                'col_C' => $account,
                'col_D' => $costCentre,
                'col_H' => $amount,
                'col_L' => substr($text, 0, 49)
            ]
        );
    }


    private static function newJournalRow($row = [])
    {
        return [
            'col_A'   => array_get($row, 'col_A', ''),
            'col_B'   => array_get($row, 'col_B', ''),
            'col_C'   => array_get($row, 'col_C', ''),
            'col_D'   => array_get($row, 'col_D', ''),
            'col_E'   => array_get($row, 'col_E', ''),
            'col_F'   => array_get($row, 'col_F', ''),
            'col_G'   => array_get($row, 'col_G', ''),
            'col_H'   => array_get($row, 'col_H', ''),
            'col_I'   => array_get($row, 'col_I', ''),
            'col_J'   => array_get($row, 'col_J', ''),
            'col_K'   => array_get($row, 'col_K', ''),
            'col_L'   => array_get($row, 'col_L', ''),
            'col_M'   => array_get($row, 'col_M', ''),
            'col_N'   => array_get($row, 'col_N', ''),
            'col_O'   => array_get($row, 'col_O', ''),
            'col_P'   => array_get($row, 'col_P', ''),
            'col_Q'   => array_get($row, 'col_Q', ''),
            'col_R'   => array_get($row, 'col_R', ''),
            'col_S'   => array_get($row, 'col_S', ''),
            'col_T'   => array_get($row, 'col_T', ''),
            'col_U'   => array_get($row, 'col_U', ''),
            'col_V'   => array_get($row, 'col_V', ''),
            'col_W'   => array_get($row, 'col_W', ''),
            'col_X'   => array_get($row, 'col_X', ''),
            'col_Y'   => array_get($row, 'col_Y', '')
        ];
    }
}
