<?php

namespace  Tfcloud\Services\Report\CsvExcel\FixedAsset\GloucestershireJournals\Traits;

use DB;
use Tfcloud\Models\Views\VwFar05;
use Tfcloud\Models\Views\VwFar06;
use Tfcloud\Models\Views\VwFar07;
use Tfcloud\Models\Views\VwFar14;
use Tfcloud\Models\Views\VwFar18;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\GloucestershireJournals\GloucestershireConstant as GC;

trait JournalDataTrait
{
    protected function getAsssetsByIFRSSubType($ifrsCatCode)
    {
        $query = VwFar07::userSiteGroup()
            ->leftJoin(
                'ca_ifrs_category as bfwd_ca_ifrs_category',
                'bfwd_ca_ifrs_category.ca_ifrs_category_id',
                '=',
                'vw_far07.bfwd_ca_ifrs_category_id'
            )
            ->where('vw_far07.ca_balance_historic', 'N')
            ->whereNotNull('vw_far07.user_sel1_code')
            ->whereIn(DB::raw('vw_far07.`IFRS Code`'), $ifrsCatCode)
            ->select(
                [
                    DB::raw('bfwd_ca_ifrs_category.ca_ifrs_category_desc AS bfwd_ca_ifrs_category_desc'),
                    'vw_far07.hra_asset',
                    'vw_far07.ca_fixed_asset_id',
                    'vw_far07.deminimis_asset',
                    DB::raw('vw_far07.user_sel1_code AS sub_ifrs_code'),
                    DB::raw('vw_far07.`Asset Type Code` as asset_type_code'),
                    DB::raw('vw_far07.`REV Asset Value` as CREVAV'),
                    DB::raw('vw_far07.`REV Land Value` as CREVLV'),
                    DB::raw('vw_far07.`IFRS Code` as ca_ifrs_category_code'),
                    DB::raw('vw_far07.`IFRS Description` as ca_ifrs_category_desc'),
                    DB::raw('vw_far07.`Previous IFRS Code` as bf_ca_ifrs_category_code'),
                    DB::raw('vw_far07.`Previous IFRS Description` as bf_ca_ifrs_category_desc'),
                    DB::raw('vw_far07.`Asset Code` as fixed_asset_code'),
                    DB::raw('vw_far07.`Asset Description` as fixed_asset_desc'),
                    DB::raw('vw_far07.`Account Code` as account_code'),
                    DB::raw('vw_far07.`Accumulated Depreciation` as COpenDPN'),
                    DB::raw('vw_far07.`CY Depreciation` as CDPN'),
                    DB::raw('vw_far07.`CFwd Accumulated Depreciation` as CCFwdDPN'),
                    DB::raw('vw_far07.`Accumulated Impairment Asset` as COpenIMPA'),
                    DB::raw('vw_far07.`Accumulated Impairment Land` as COpenIMPL'),
                    DB::raw('vw_far07.`BFwd GBV` as COpenGBV'),
                    DB::raw('vw_far07.`BFwd RR Asset` as BFAssetRR'),
                    DB::raw('vw_far07.`BFwd RR Land` as BFLandRR'),
                    'bfwd_ca_ifrs_category.amortisation as bfwd_amortisation',
                ]
            )
            ->orderBy(DB::raw('bfwd_ca_ifrs_category.ca_ifrs_category_desc'))
            ->orderBy(DB::raw('vw_far07.`Asset Code`'));

        return $query->get();
    }


    protected static function getDepreciationData($class, $ifrs, $byAccount = false)
    {
        $query = VwFar05::userSiteGroup()
            ->join(
                'ca_fixed_asset',
                'ca_fixed_asset.ca_fixed_asset_id',
                '=',
                'vw_far05.ca_fixed_asset_id'
            )
            ->leftJoin(
                'ca_account',
                'ca_account.ca_account_id',
                '=',
                'ca_fixed_asset.ca_account_code_id'
            )
            ->select(
                [
                    DB::raw('SUM(vw_far05.`CV (CY) Depreciation`) AS cy_depn'),
                    DB::raw('SUM(vw_far05.`RR (CY) Depreciation`) AS rr_depn')
                ]
            );

        switch ($ifrs) {
            case GC::IFRS_EFWOLB:
            case GC::IFRS_EFWVPE:
            case GC::IFRS_PFI:
                $query->where(DB::raw('vw_far05.`IFRS Code`'), $ifrs);
                break;
            default:
                $query->whereIn(DB::raw('vw_far05.`Asset Type Code`'), $class);
                break;
        }

        if ($byAccount) {
            $query->addSelect(DB::raw('vw_far05.`Account Code` AS account_code'))
                ->groupBy(DB::raw('vw_far05.`Account Code`'));

            return $query->get();
        } else {
            return $query->first();
        }
    }



    protected function getSubAnalysisCode($FixedAssetType, &$subjective)
    {
        $type = explode('.', $FixedAssetType);
        $subjective = $type[1];
        return $type[2];
    }

    protected static function getRevalData($class, $ifrs, $byAccount = false, $up = true)
    {
        $query = VwFar06::userSiteGroup()
            ->join(
                'ca_fixed_asset',
                'ca_fixed_asset.ca_fixed_asset_id',
                '=',
                'vw_far06.ca_fixed_asset_id'
            )
            ->join(
                'ca_fixed_asset_type',
                'ca_fixed_asset_type.ca_fixed_asset_type_id',
                '=',
                'ca_fixed_asset.ca_fixed_asset_type_id'
            )
            ->select(
                [
                    DB::raw('SUM(vw_far06.`Reval Asset Value`) AS reval_asset_value'),
                    DB::raw('SUM(vw_far06.`Reval Land Value`) AS reval_land_value'),
                    DB::raw('SUM(vw_far06.`Gross RR`) AS gross_rr'),
                    DB::raw('SUM(vw_far06.`Gross RR`) AS rr_gain'),
                    DB::raw('SUM(vw_far06.`Gross SD`) AS gross_sd'),
                    DB::raw('SUM(vw_far06.`Depn WOff RR`) AS depn_wo_rr'),
                    DB::raw('SUM(vw_far06.`Depn WOff SD`) AS depn_wo_sd'),
                    DB::raw('SUM(vw_far06.`Imp WOff RR`) AS imp_wo_rr'),
                    DB::raw('SUM(vw_far06.`Imp WOff SD`) AS imp_wo_sd'),
                    DB::raw('SUM(vw_far06.`Asset Loss Reversal`) AS asset_loss_reversal'),
                    DB::raw('SUM(vw_far06.`Land Loss Reversal`) AS land_loss_reversal'),
                    DB::raw('SUM(vw_far06.`Asset Loss Reversal` + vw_far06.`Land Loss Reversal`) AS loss_reversal'),
                    DB::raw('SUM(vw_far06.`Loss Depreciation Reversal`) AS loss_depn_reversal'),
                    DB::raw('SUM(vw_far06.`Asset Impairment Reversal`) AS asset_imp_reversal'),
                    DB::raw('SUM(vw_far06.`Land Impairment Reversal`) AS land_imp_reversal'),
                    DB::raw('SUM(vw_far06.`Impairment Depreciation Reversal`) AS imp_depn_reversal'),
                    DB::raw('SUM(vw_far06.`Reval Reserve`) AS reval_reserve'),
                ]
            );

        if ($up) {
            $query->whereRaw('vw_far06.`Reval Asset Value` + vw_far06.`Reval Land Value` >= 0');
        } else {
            $query->whereRaw('vw_far06.`Reval Asset Value` + vw_far06.`Reval Land Value` < 0');
        }

        switch ($ifrs) {
            case GC::IFRS_EFWOLB:
            case GC::IFRS_EFWVPE:
            case GC::IFRS_PFI:
                $query->where(DB::raw('vw_far06.`IFRS Code`'), $ifrs);
                break;
            default:
                $query->whereIn(DB::raw('ca_fixed_asset_type.ca_fixed_asset_type_code'), $class);
                break;
        }

        if ($byAccount) {
            $query->addSelect(DB::raw('vw_far06.`Account Code` AS account_code'))
                ->groupBy(DB::raw('vw_far06.`Account Code`'));
            return $query->get();
        } else {
            return $query->first();
        }
    }

    protected static function getDisposalData($class, $ifrs, $byAccount = false, $useRR = false)
    {
        $query = VwFar14::userSiteGroup()
            ->join(
                'ca_fixed_asset',
                'ca_fixed_asset.ca_fixed_asset_id',
                '=',
                'vw_far14.ca_fixed_asset_id'
            )
            ->join(
                'ca_fixed_asset_type',
                'ca_fixed_asset_type.ca_fixed_asset_type_id',
                '=',
                'ca_fixed_asset.ca_fixed_asset_type_id'
            )
            ->select(
                [
                    DB::raw('SUM(vw_far14.`Total GBV`) AS gbv'),
                    DB::raw(
                        'SUM(vw_far14.`Accumulated Depreciation` + vw_far14.`Accumulated Impairment`) AS acc_depn_imp'
                    ),
                    DB::raw('SUM(vw_far14.`Total NBV`) AS nbv'),
                    DB::raw('SUM(vw_far14.`Revaluation Reserve`) AS rr'),
                ]
            );

        if ($useRR) {
            return $query->first();
        } else {
            switch ($ifrs) {
                case GC::IFRS_EFWOLB:
                case GC::IFRS_EFWVPE:
                case GC::IFRS_PFI:
                    $query->where(DB::raw('vw_far14.`IFRS Code`'), $ifrs);
                    break;
                default:
                    $query->whereIn(DB::raw('ca_fixed_asset_type.ca_fixed_asset_type_code'), $class);
                    break;
            }

            if ($byAccount) {
                $query->addSelect(DB::raw('vw_far14.`Account Code` AS account_code'))
                    ->groupBy(DB::raw('vw_far14.`Account Code`'));
                return $query->get();
            } else {
                return $query->first();
            }
        }
    }

    protected static function getTransferData($ifrs, $from = true)
    {
        $query = VwFar18::userSiteGroup()
            ->join(
                'ca_fixed_asset',
                'ca_fixed_asset.ca_fixed_asset_id',
                '=',
                'vw_far18.ca_fixed_asset_id'
            )
            ->join(
                'ca_fixed_asset_type',
                'ca_fixed_asset_type.ca_fixed_asset_type_id',
                '=',
                'ca_fixed_asset.ca_fixed_asset_type_id'
            )
            ->select(
                [
                    DB::raw('SUM(vw_far18.`Gross Movement`) AS gross_movement'),
                    DB::raw('SUM(vw_far18.`Depreciation Movement` + vw_far18.`Impairment Movement`) AS depn_movement'),
                    DB::raw('SUM(vw_far18.`RR Value`) AS rr_value')
                ]
            );

        if ($from) {
            $query->whereRaw('vw_far18.`Gross Movement` < 0');
        } else {
            $query->whereRaw('vw_far18.`Gross Movement` >= 0');
        }

        $query->where(DB::raw('vw_far18.`IFRS Code`'), $ifrs);

        return $query->first();
    }
}
