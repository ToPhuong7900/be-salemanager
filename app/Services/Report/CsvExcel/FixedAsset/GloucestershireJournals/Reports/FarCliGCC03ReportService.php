<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\GloucestershireJournals\Reports;

use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\GloucestershireJournals\GloucestershireJournalBaseService;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\GloucestershireJournals\JournalData\RevalLossLines;

class FarCliGCC03ReportService extends GloucestershireJournalBaseService
{
    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->addJournalHeader($this->journalData, $this->yearEndDate, $this->yearString);
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        // Revaluation Loss Journal

        $this->prepareData();

        return $this->journalData;
    }

    protected function prepareData()
    {
        RevalLossLines::addLines($this->journalData);
    }
}
