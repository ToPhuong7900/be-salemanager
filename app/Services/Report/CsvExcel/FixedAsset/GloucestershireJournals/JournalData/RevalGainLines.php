<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\GloucestershireJournals\JournalData;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Math;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\GloucestershireJournals\GloucestershireConstant as GC;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\GloucestershireJournals\Traits\JournalOutputTrait;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\GloucestershireJournals\Traits\JournalDataTrait;

class RevalGainLines
{
    use JournalOutputTrait;
    use JournalDataTrait;

    private const GL_90080 = '90080';
    private const GL_97100 = '97100';
    private const GL_97110 = '97110';
    private const GL_97114 = '97114';
    private const GL_97115 = '97115';
    private const GL_90017 = '90017';
    private const GL_90072 = '90072';
    private const GL_90082 = '90082';

    private const GL_OLB_DR = self::GL_90017;
    private const GL_SUR_DR = '90615';
    private const GL_EFW_OLB_DR = self::GL_90072;
    private const GL_EFW_VPE_DR = self::GL_90072;
    private const GL_PFI_DR = self::GL_90082;

    private const GL_OLB_CR = self::GL_97110;
    private const GL_SUR_CR = self::GL_97110;
    private const GL_EFW_OLB_CR = self::GL_97114;
    private const GL_EFW_VPE_CR = self::GL_97114;
    private const GL_PFI_CR = self::GL_97115;

    private const GL_SD = '70100';

    private const CONTRA_SD = '97100';
    private const CONTRA_SD_BY_ASSET = '74202';

    private const GL_DEPN_WO_OLB_DR = '90016';
    private const GL_DEPN_WO_OLB_CR = '97110';

    private const GL_DEPN_WO_EFW_OLB_DR = self::GL_90072;
    private const GL_DEPN_WO_EFW_OLB_CR = self::GL_97114;

    private const GL_DEPN_WO_EFW_VPE_DR = self::GL_90072;
    private const GL_DEPN_WO_EFW_VPE_CR = self::GL_97114;

    private const GL_DEPN_WO_PFI_DR = self::GL_90080;
    private const GL_DEPN_WO_PFI_CR = self::GL_97115;

    private const GL_OLB_RR = self::GL_97100;
    private const GL_PFI_RR = self::GL_97100;
    private const GL_EFW_RR = self::GL_97100;

    public static function addLines(&$data)
    {
        self::addAssetRegLines($data);
        self::addRRLines($data);
        self::addImpRevLines($data);
        self::addDepnWoLines($data);
        self::addRRDepnLines($data);
        self::addRevLossLines($data);
    }

    private static function addAssetRegLines(&$data)
    {
        // Revaluation gain - application to fixed asset registers.
        self::printRRLine(
            $data,
            GC::DR,
            GC::CLASS_OLB,
            self::GL_OLB_DR,
            GC::IFRS_OLB,
            self::GL_90017
        );
        self::printRRLine(
            $data,
            GC::DR,
            GC::CLASS_SUR,
            self::GL_SUR_DR,
            GC::IFRS_SUR,
            self::GL_SUR_DR
        );

        self::printRRLine(
            $data,
            GC::DR,
            GC::CLASS_EFW_OLB,
            self::GL_90072,
            GC::IFRS_EFWOLB,
            self::GL_90072
        );

        self::printRRLine(
            $data,
            GC::DR,
            GC::CLASS_EFW_VPE,
            self::GL_90072,
            GC::IFRS_EFWVPE,
            self::GL_90072
        );

        self::printRRLine(
            $data,
            GC::DR,
            GC::CLASS_PFI,
            self::GL_90082,
            GC::IFRS_PFI,
            self::GL_90082
        );
    }

    private static function addRRLines(&$data)
    {
        // Revaluation gain - application to revaluation reserve.
        self::printRRLine(
            $data,
            GC::CR,
            GC::CLASS_OLB,
            self::GL_OLB_CR,
            GC::IFRS_OLB
        );
        self::printRRLine(
            $data,
            GC::CR,
            GC::CLASS_SUR,
            self::GL_SUR_CR,
            GC::IFRS_SUR
        );

        self::printRRLine(
            $data,
            GC::CR,
            GC::CLASS_EFW_OLB,
            self::GL_EFW_OLB_CR,
            GC::IFRS_EFWOLB
        );

        self::printRRLine(
            $data,
            GC::CR,
            GC::CLASS_EFW_VPE,
            self::GL_EFW_VPE_CR,
            GC::IFRS_EFWVPE
        );

        self::printRRLine(
            $data,
            GC::CR,
            GC::CLASS_PFI,
            self::GL_PFI_CR,
            GC::IFRS_PFI
        );
    }

    private static function addImpRevLines(&$data)
    {
        // Revaluation gain - impairment reversal application to revaluation reserve/CI&E/CAA (where asset was
        // previously valued down below historic value).
        // Credit S&D by Asset
        $totalContra = 0;

        self::printSDLine(
            $data,
            $totalContra,
            GC::CR,
            GC::CLASS_OLB,
            self::GL_SD,
            GC::IFRS_OLB
        );

        self::printSDLine(
            $data,
            $totalContra,
            GC::CR,
            GC::CLASS_SUR,
            self::GL_SD,
            GC::IFRS_SUR
        );

        self::printSDLine(
            $data,
            $totalContra,
            GC::CR,
            GC::CLASS_EFW_OLB,
            self::GL_SD,
            GC::IFRS_EFWOLB
        );

        self::printSDLine(
            $data,
            $totalContra,
            GC::CR,
            GC::CLASS_PFI,
            self::GL_SD,
            GC::IFRS_PFI
        );


        self::addJournalRow(
            $data,
            GC::CR,
            self::CONTRA_SD,
            '',
            $totalContra,
            "Contra Reval Increase to S/D - Gain"
        );

        self::addJournalRow(
            $data,
            GC::DR,
            self::CONTRA_SD_BY_ASSET,
            '802512',
            $totalContra,
            "Contra Reval Increase to S/D - Gain"
        );
    }

    private static function addDepnWoLines(&$data)
    {
        // W/O Accum Depr on Reval.
        self::printDepnWOLine(
            $data,
            GC::DR,
            GC::CLASS_OLB,
            self::GL_DEPN_WO_OLB_DR,
            GC::IFRS_OLB
        );
        self::printDepnWOLine(
            $data,
            GC::CR,
            GC::CLASS_OLB,
            self::GL_DEPN_WO_OLB_CR,
            GC::IFRS_OLB
        );

        self::printDepnWOLine(
            $data,
            GC::DR,
            GC::CLASS_EFW_OLB,
            self::GL_DEPN_WO_EFW_OLB_DR,
            GC::IFRS_EFWOLB
        );
        self::printDepnWOLine(
            $data,
            GC::CR,
            GC::CLASS_EFW_OLB,
            self::GL_DEPN_WO_EFW_OLB_CR,
            GC::IFRS_EFWOLB
        );

        self::printDepnWOLine(
            $data,
            GC::DR,
            GC::CLASS_EFW_VPE,
            self::GL_DEPN_WO_EFW_VPE_DR,
            GC::IFRS_EFWVPE
        );
        self::printDepnWOLine(
            $data,
            GC::CR,
            GC::CLASS_EFW_VPE,
            self::GL_DEPN_WO_EFW_VPE_CR,
            GC::IFRS_EFWVPE
        );

        self::printDepnWOLine(
            $data,
            GC::DR,
            GC::CLASS_PFI,
            self::GL_DEPN_WO_PFI_DR,
            GC::IFRS_PFI
        );
        self::printDepnWOLine(
            $data,
            GC::CR,
            GC::CLASS_PFI,
            self::GL_DEPN_WO_PFI_CR,
            GC::IFRS_PFI
        );
    }

    private static function addRRDepnLines(&$data)
    {
        // RR Depreciation.
        self::printRRDepnLine(
            $data,
            GC::CR,
            GC::CLASS_OLB,
            self::GL_OLB_RR,
            GC::IFRS_OLB
        );
        self::printRRDepnLine(
            $data,
            GC::CR,
            GC::CLASS_PFI,
            self::GL_PFI_RR,
            GC::IFRS_PFI
        );
        self::printRRDepnLine(
            $data,
            GC::CR,
            GC::CLASS_EFW_OLB,
            self::GL_EFW_RR,
            GC::IFRS_EFWOLB
        );
        self::printRRDepnLine(
            $data,
            GC::CR,
            GC::CLASS_EFW_VPE,
            self::GL_EFW_RR,
            GC::IFRS_EFWVPE
        );
        self::printRRDepnLine(
            $data,
            GC::DR,
            GC::CLASS_OLB,
            self::GL_97110,
            GC::IFRS_OLB
        );
        self::printRRDepnLine(
            $data,
            GC::DR,
            GC::CLASS_PFI,
            self::GL_97115,
            GC::IFRS_PFI
        );
        self::printRRDepnLine(
            $data,
            GC::DR,
            GC::CLASS_EFW_OLB,
            self::GL_97114,
            GC::IFRS_EFWOLB
        );
        self::printRRDepnLine(
            $data,
            GC::DR,
            GC::CLASS_EFW_VPE,
            self::GL_97114,
            GC::IFRS_EFWVPE
        );
    }

    private static function addRevLossLines(&$data)
    {
        // Reversal of Loss.
        self::printRevLossLine(
            $data,
            GC::DR,
            GC::CLASS_OLB,
            self::GL_90017,
            GC::IFRS_OLB
        );

        self::printRevLossLine(
            $data,
            GC::DR,
            GC::CLASS_EFW_OLB,
            self::GL_EFW_OLB_DR,
            GC::IFRS_EFWOLB
        );

        self::printRevLossLine(
            $data,
            GC::DR,
            GC::CLASS_EFW_VPE,
            self::GL_EFW_VPE_DR,
            GC::IFRS_EFWVPE
        );

        self::printRevLossLine(
            $data,
            GC::DR,
            GC::CLASS_PFI,
            self::GL_PFI_DR,
            GC::IFRS_PFI
        );

        // Reversal of Loss by Asset.
        self::printRevLossLines(
            $data,
            GC::CR,
            GC::CLASS_OLB,
            self::GL_SD,
            GC::IFRS_OLB
        );

        self::printRevLossLines(
            $data,
            GC::CR,
            GC::CLASS_EFW_OLB,
            self::GL_SD,
            GC::IFRS_EFWOLB
        );

        self::printRevLossLines(
            $data,
            GC::CR,
            GC::CLASS_EFW_VPE,
            self::GL_SD,
            GC::IFRS_EFWVPE
        );

        self::printRevLossLines(
            $data,
            GC::CR,
            GC::CLASS_PFI,
            self::GL_SD,
            GC::IFRS_PFI
        );
    }

    private static function printRRLine(&$data, $post, $class, $account, $ifrsCode, $sdAccount = '')
    {
        $revUp = self::getRevalData($class, $ifrsCode);
        $rrGain = Common::numberFormat($revUp->rr_gain, thousandsSep: ',');
        $sdGain = Common::numberFormat(Math::negateCurrency($revUp->gross_sd), thousandsSep: ',');

        self::addJournalRow(
            $data,
            $post,
            $account,
            '',
            $rrGain,
            "$ifrsCode Revaluation Increase to RR - Gain"
        );

        if ($post === GC::DR) {
            self::addJournalRow(
                $data,
                $post,
                $sdAccount,
                '',
                $sdGain,
                "$ifrsCode Revaluation Increase to S/D - Gain"
            );
        }
    }

    private static function printRRDepnLine(&$data, $post, $class, $account, $ifrsCode)
    {
        $depnData = self::getDepreciationData($class, $ifrsCode);
        $rrDepn = Common::numberFormat($depnData->rr_depn, thousandsSep: ',');

        self::addJournalRow(
            $data,
            $post,
            $account,
            '',
            $rrDepn,
            "RR Depreciation on Revaluation $ifrsCode"
        );
    }

    private static function printSDLine(&$data, &$totalContra, $post, $class, $account, $ifrsCode)
    {
        $revUp = self::getRevalData($class, $ifrsCode, true);

        foreach ($revUp as $rev) {
            $sdGain = Common::numberFormat(Math::negateCurrency($rev->gross_sd), thousandsSep: ',');
            $totalContra = Math::addCurrency([$totalContra, $rev->loss_reversal, Math::negateCurrency($rev->gross_sd)]);

            $accCode = $rev->account_code;

            self::addJournalRow(
                $data,
                $post,
                $account,
                $accCode,
                $sdGain,
                "$ifrsCode Revaluation Increase to S/D - Gain"
            );
        }
    }

    private static function printDepnWOLine(&$data, $post, $class, $account, $ifrsCode)
    {
        $revUp = self::getRevalData($class, $ifrsCode);
        $depnWO = Common::numberFormat($revUp->depn_wo_rr, thousandsSep: ',');

        self::addJournalRow(
            $data,
            $post,
            $account,
            '',
            $depnWO,
            "$ifrsCode Depr W/O to RR"
        );
    }

    private static function printRevLossLine(&$data, $post, $class, $account, $ifrsCode)
    {
        $revUp = self::getRevalData($class, $ifrsCode);
        $lossRev = Common::numberFormat($revUp->loss_reversal, thousandsSep: ',');

        self::addJournalRow(
            $data,
            $post,
            $account,
            '',
            $lossRev,
            "$ifrsCode Revaluation Inc to S/D - Reverse Prior Loss"
        );
    }

    private static function printRevLossLines(&$data, $post, $class, $account, $ifrsCode)
    {
        $revUp = self::getRevalData($class, $ifrsCode, true);

        foreach ($revUp as $rev) {
            $revLoss = Common::numberFormat($rev->loss_reversal, thousandsSep: ',');

            $accCode = $rev->account_code;

            self::addJournalRow(
                $data,
                $post,
                $account,
                $accCode,
                $revLoss,
                "$ifrsCode Revaluation Inc to S/D - Reverse Prior Loss"
            );
        }
    }
}
