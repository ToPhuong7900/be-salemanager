<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\GloucestershireJournals\JournalData;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Math;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\GloucestershireJournals\GloucestershireConstant as GC;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\GloucestershireJournals\Traits\JournalOutputTrait;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\GloucestershireJournals\Traits\JournalDataTrait;

class DisposalLines
{
    use JournalOutputTrait;
    use JournalDataTrait;

    private const GL_PER_ASSET = '70050';

    private const GL_OLB = '90015';
    private const GL_VPE = '90115';
    private const GL_FE = '90216';
    private const GL_INF = '90320';
    private const GL_SUR = '90615';
    private const GL_EFW_OLB = '90070';
    private const GL_EFW_VPE = '90070';
    private const GL_PFI = '90080';

    private const GL_OLB_DPN = '90016';
    private const GL_VPE_DPN = '90116';
    private const GL_FE_DPN = '90216';
    private const GL_INF_DPN = '90320';
    private const GL_EFW_OLB_DPN = '90071';
    private const GL_EFW_VPE_DPN = '90071';
    private const GL_PFI_DPN = '90081';

    private const GL_RR_DR = '97110';
    private const GL_RR_CR = '97100';

    private const CONTRA_DR = '74211';
    private const CONTRA_CR = '97100';

    public static function addLines(&$data)
    {
        $dspCredits = 0;
        $dspDebits = 0;

        self::addDspMovementLines($data, $dspCredits);
        self::addImpLines($data, $dspDebits);


        // RR
        self::printRRLine(
            $data,
            GC::DR,
            '',
            self::GL_RR_DR,
            ''
        );

        $contra = Math::subCurrency([$dspCredits, $dspDebits]);

        // Contras.
        self::printContraLine($data, GC::CR, self::CONTRA_DR, '802510', $contra);
        self::printContraLine($data, GC::DR, self::CONTRA_CR, '', $contra);
    }

    private static function addDspMovementLines(&$data, &$dspCredits)
    {
        // Disposal Movements.
        self::printDSPLine(
            $data,
            $dspCredits,
            GC::CR,
            GC::CLASS_OLB,
            self::GL_OLB,
            GC::IFRS_OLB
        );
        self::printDSPLineAsset(
            $data,
            GC::DR,
            GC::CLASS_OLB,
            self::GL_PER_ASSET,
            GC::IFRS_OLB
        );

        self::printDSPLine(
            $data,
            $dspCredits,
            GC::CR,
            GC::CLASS_INF,
            self::GL_INF,
            GC::IFRS_INF
        );
        self::printDSPLineAsset(
            $data,
            GC::DR,
            GC::CLASS_INF,
            self::GL_PER_ASSET,
            GC::IFRS_INF
        );

        self::printDSPLine(
            $data,
            $dspCredits,
            GC::CR,
            GC::CLASS_FE,
            self::GL_FE,
            GC::IFRS_FE
        );
        self::printDSPLineAsset(
            $data,
            GC::DR,
            GC::CLASS_FE,
            self::GL_PER_ASSET,
            GC::IFRS_FE
        );

        self::printDSPLine(
            $data,
            $dspCredits,
            GC::CR,
            GC::CLASS_SUR,
            self::GL_SUR,
            GC::IFRS_SUR
        );
        self::printDSPLineAsset(
            $data,
            GC::DR,
            GC::CLASS_SUR,
            self::GL_PER_ASSET,
            GC::IFRS_SUR
        );

        self::printDSPLine(
            $data,
            $dspCredits,
            GC::CR,
            GC::CLASS_VPE,
            self::GL_VPE,
            GC::IFRS_VPE
        );
        self::printDSPLineAsset(
            $data,
            GC::DR,
            GC::CLASS_VPE,
            self::GL_PER_ASSET,
            GC::IFRS_VPE
        );

        self::printDSPLine(
            $data,
            $dspCredits,
            GC::CR,
            GC::CLASS_EFW_OLB,
            self::GL_EFW_OLB,
            GC::IFRS_EFWOLB
        );
        self::printDSPLineAsset(
            $data,
            GC::DR,
            GC::CLASS_EFW_OLB,
            self::GL_PER_ASSET,
            GC::IFRS_EFWOLB
        );

        self::printDSPLine(
            $data,
            $dspCredits,
            GC::CR,
            GC::CLASS_EFW_VPE,
            self::GL_EFW_VPE,
            GC::IFRS_EFWVPE
        );
        self::printDSPLineAsset(
            $data,
            GC::DR,
            GC::CLASS_EFW_VPE,
            self::GL_PER_ASSET,
            GC::IFRS_EFWVPE
        );

        self::printDSPLine(
            $data,
            $dspCredits,
            GC::CR,
            GC::CLASS_PFI,
            self::GL_PFI,
            GC::IFRS_PFI
        );
        self::printDSPLineAsset(
            $data,
            GC::DR,
            GC::CLASS_PFI,
            self::GL_PER_ASSET,
            GC::IFRS_PFI
        );
    }

    private static function addImpLines(&$data, &$dspDebits)
    {
        // Impairment and Depreciation Movements.
        self::printImpDpnLine(
            $data,
            $dspDebits,
            GC::DR,
            GC::CLASS_OLB,
            self::GL_OLB_DPN,
            GC::IFRS_OLB
        );
        self::printImpDpnLineAsset(
            $data,
            GC::CR,
            GC::CLASS_OLB,
            self::GL_PER_ASSET,
            GC::IFRS_OLB
        );

        self::printImpDpnLine(
            $data,
            $dspDebits,
            GC::DR,
            GC::CLASS_INF,
            self::GL_INF_DPN,
            GC::IFRS_INF
        );
        self::printImpDpnLineAsset(
            $data,
            GC::CR,
            GC::CLASS_INF,
            self::GL_PER_ASSET,
            GC::IFRS_INF
        );

        self::printImpDpnLine(
            $data,
            $dspDebits,
            GC::DR,
            GC::CLASS_FE,
            self::GL_FE_DPN,
            GC::IFRS_FE
        );
        self::printImpDpnLineAsset(
            $data,
            GC::CR,
            GC::CLASS_FE,
            self::GL_PER_ASSET,
            GC::IFRS_FE
        );

        self::printImpDpnLine(
            $data,
            $dspDebits,
            GC::DR,
            GC::CLASS_VPE,
            self::GL_VPE_DPN,
            GC::IFRS_VPE
        );
        self::printImpDpnLineAsset(
            $data,
            GC::CR,
            GC::CLASS_VPE,
            self::GL_PER_ASSET,
            GC::IFRS_VPE
        );

        self::printImpDpnLine(
            $data,
            $dspDebits,
            GC::DR,
            GC::CLASS_EFW_OLB,
            self::GL_EFW_OLB_DPN,
            GC::IFRS_EFWOLB
        );
        self::printImpDpnLineAsset(
            $data,
            GC::DR,
            GC::CLASS_EFW_OLB,
            self::GL_PER_ASSET,
            GC::IFRS_EFWOLB
        );

        self::printImpDpnLine(
            $data,
            $dspDebits,
            GC::DR,
            GC::CLASS_EFW_VPE,
            self::GL_EFW_VPE_DPN,
            GC::IFRS_EFWVPE
        );
        self::printImpDpnLineAsset(
            $data,
            GC::CR,
            GC::CLASS_EFW_VPE,
            self::GL_PER_ASSET,
            GC::IFRS_EFWVPE
        );

        self::printImpDpnLine(
            $data,
            $dspDebits,
            GC::DR,
            GC::CLASS_PFI,
            self::GL_PFI_DPN,
            GC::IFRS_PFI
        );
        self::printImpDpnLineAsset(
            $data,
            GC::CR,
            GC::CLASS_PFI,
            self::GL_PER_ASSET,
            GC::IFRS_PFI
        );
    }

    private static function printDSPLine(&$data, &$dspCredits, $post, $class, $account, $ifrsCode)
    {
        $dsp = self::getDisposalData($class, $ifrsCode);
        $dspCredits = Math::addCurrency([$dspCredits, Math::negateCurrency($dsp->gbv)]);
        $gbv = Common::numberFormat(Math::negateCurrency($dsp->gbv), thousandsSep: ',');

        self::addJournalRow(
            $data,
            $post,
            $account,
            '',
            $gbv,
            "$ifrsCode Disposal"
        );
    }

    private static function printDSPLineAsset(&$data, $post, $class, $account, $ifrsCode)
    {
        $dspData = self::getDisposalData($class, $ifrsCode, true);

        foreach ($dspData as $dsp) {
            $gbv = Common::numberFormat(Math::negateCurrency($dsp->gbv), thousandsSep: ',');
            $accCode = $dsp->account_code;

            self::addJournalRow(
                $data,
                $post,
                $account,
                $accCode,
                $gbv,
                "$ifrsCode Disposal"
            );
        }
    }

    private static function printImpDpnLine(&$data, &$dspDebits, $post, $class, $account, $ifrsCode)
    {
        $dsp = self::getDisposalData($class, $ifrsCode);
        $dspDebits = Math::addCurrency([$dspDebits, $dsp->acc_depn_imp]);
        $impDpn = Common::numberFormat($dsp->acc_depn_imp, thousandsSep: ',');

        self::addJournalRow(
            $data,
            $post,
            $account,
            '',
            $impDpn,
            "$ifrsCode Depreciation De-Recognition on Disposal"
        );
    }

    private static function printImpDpnLineAsset(&$data, $post, $class, $account, $ifrsCode)
    {
        $dspData = self::getDisposalData($class, $ifrsCode, true);

        foreach ($dspData as $dsp) {
            $impDpn = Common::numberFormat($dsp->acc_depn_imp, thousandsSep: ',');
            $accCode = $dsp->account_code;

            self::addJournalRow(
                $data,
                $post,
                $account,
                $accCode,
                $impDpn,
                "$ifrsCode Depreciation De-Recognition on Disposal"
            );
        }
    }

    private static function printRRLine(&$data, $post, $class, $account, $ifrsCode)
    {
        $rrData = self::getDisposalData($class, $ifrsCode, useRR: true);
        $rr = Common::numberFormat(Math::negateCurrency($rrData->rr), thousandsSep: ',');

        self::addJournalRow(
            $data,
            $post,
            $account,
            '',
            $rr,
            "RR Disposal"
        );

        self::addJournalRow(
            $data,
            GC::CR,
            self::GL_RR_CR,
            '',
            $rr,
            "RR Disposal"
        );
    }

    private static function printContraLine(&$data, $post, $account, $costCentre, $amount)
    {
        $contra = Common::numberFormat($amount, thousandsSep: ',');

        self::addJournalRow(
            $data,
            $post,
            $account,
            $costCentre,
            $contra,
            "Contra Disposal"
        );
    }
}
