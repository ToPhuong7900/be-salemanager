<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\GloucestershireJournals\JournalData;

use Tfcloud\Lib\Common;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\GloucestershireJournals\GloucestershireConstant as GC;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\GloucestershireJournals\JournalData\ContraData;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\GloucestershireJournals\Traits\JournalOutputTrait;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\GloucestershireJournals\Traits\JournalDataTrait;

class DepreciationLines
{
    use JournalOutputTrait;
    use JournalDataTrait;

    private const GL_OLB = '90016';
    private const GL_VPE = '90116';
    private const GL_FE = '90216';
    private const GL_INF = '90315';
    private const GL_INT = '91015';
    private const GL_EFW_OLB = '90071';
    private const GL_EFW_VPE = '90071';
    private const GL_PFI = '90081';
    private const GL_CIE = '70010';
    private const CONTRA_CLS = '74201';
    private const CONTRA_CLS_INT = '74214';
    private const CONTRA_ACC = '802512';
    private const CONTRA_CLS_DR = '97100';

    public static function addLines(&$data)
    {
        $contra = new ContraData();

        self::addInYrLines($data, $contra);
        self::addInYrIandELines($data);
        self::addContraCrLines($data, $contra);
        self::addContraDrLines($data, $contra);
    }

    private static function addInYrIandELines(&$data)
    {
        // In year depreciation - application to CI&E.
        self::printCIELine(
            $data,
            GC::DR,
            GC::CLASS_OLB,
            self::GL_CIE,
            GC::IFRS_OLB
        );
        self::printCIELine(
            $data,
            GC::DR,
            GC::CLASS_VPE,
            self::GL_CIE,
            GC::IFRS_VPE
        );
        self::printCIELine(
            $data,
            GC::DR,
            GC::CLASS_FE,
            self::GL_CIE,
            GC::IFRS_FE
        );
        self::printCIELine(
            $data,
            GC::DR,
            GC::CLASS_INF,
            self::GL_CIE,
            GC::IFRS_INF
        );
        self::printCIELine(
            $data,
            GC::DR,
            GC::CLASS_INT,
            self::GL_CIE,
            GC::IFRS_INT
        );
        self::printCIELine(
            $data,
            GC::DR,
            GC::CLASS_EFW_OLB,
            self::GL_CIE,
            GC::IFRS_EFWOLB
        );
        self::printCIELine(
            $data,
            GC::DR,
            GC::CLASS_EFW_VPE,
            self::GL_CIE,
            GC::IFRS_EFWVPE
        );
        self::printCIELine(
            $data,
            GC::DR,
            GC::CLASS_PFI,
            self::GL_CIE,
            GC::IFRS_PFI
        );
    }

    private static function addInYrLines(&$data, &$contra)
    {
        // In year depreciation - application to fixed asset registers.
        self::printLine(
            $data,
            $contra->totalDepnOLB,
            GC::CR,
            GC::CLASS_OLB,
            self::GL_OLB,
            GC::IFRS_OLB
        );
        self::printLine(
            $data,
            $contra->totalDepnVPE,
            GC::CR,
            GC::CLASS_VPE,
            self::GL_VPE,
            GC::IFRS_VPE
        );
        self::printLine(
            $data,
            $contra->totalDepnFE,
            GC::CR,
            GC::CLASS_FE,
            self::GL_FE,
            GC::IFRS_FE
        );
        self::printLine(
            $data,
            $contra->totalDepnINF,
            GC::CR,
            GC::CLASS_INF,
            self::GL_INF,
            GC::IFRS_INF
        );
        self::printLine(
            $data,
            $contra->totalDepnINT,
            GC::CR,
            GC::CLASS_INT,
            self::GL_INT,
            GC::IFRS_INT
        );
        self::printLine(
            $data,
            $contra->totalDepnEFWOLB,
            GC::CR,
            GC::CLASS_EFW_OLB,
            self::GL_EFW_OLB,
            GC::IFRS_EFWOLB
        );
        self::printLine(
            $data,
            $contra->totalDepnEFWVPE,
            GC::CR,
            GC::CLASS_EFW_VPE,
            self::GL_EFW_VPE,
            GC::IFRS_EFWVPE
        );
        self::printLine(
            $data,
            $contra->totalDepnPFI,
            GC::CR,
            GC::CLASS_PFI,
            self::GL_PFI,
            GC::IFRS_PFI
        );
    }

    private static function addContraCrLines(&$data, $contra)
    {
        // Contra Credits.
        self::printContraLine(
            $data,
            $contra->totalDepnOLB,
            GC::CR,
            self::CONTRA_CLS,
            self::CONTRA_ACC,
            GC::IFRS_OLB
        );
        self::printContraLine(
            $data,
            $contra->totalDepnVPE,
            GC::CR,
            self::CONTRA_CLS,
            self::CONTRA_ACC,
            GC::IFRS_VPE
        );
        self::printContraLine(
            $data,
            $contra->totalDepnFE,
            GC::CR,
            self::CONTRA_CLS,
            self::CONTRA_ACC,
            GC::IFRS_FE
        );
        self::printContraLine(
            $data,
            $contra->totalDepnINF,
            GC::CR,
            self::CONTRA_CLS,
            self::CONTRA_ACC,
            GC::IFRS_INF
        );
        self::printContraLine(
            $data,
            $contra->totalDepnINT,
            GC::CR,
            self::CONTRA_CLS_INT,
            self::CONTRA_ACC,
            GC::IFRS_INT
        );
        self::printContraLine(
            $data,
            $contra->totalDepnEFWOLB,
            GC::CR,
            self::CONTRA_CLS,
            self::CONTRA_ACC,
            GC::IFRS_EFWOLB
        );
        self::printContraLine(
            $data,
            $contra->totalDepnEFWVPE,
            GC::CR,
            self::CONTRA_CLS,
            self::CONTRA_ACC,
            GC::IFRS_EFWVPE
        );
        self::printContraLine(
            $data,
            $contra->totalDepnPFI,
            GC::CR,
            self::CONTRA_CLS,
            self::CONTRA_ACC,
            GC::IFRS_PFI
        );
    }

    private static function addContraDrLines(&$data, $contra)
    {
        // Contra Debits.
        self::printContraLine(
            $data,
            $contra->totalDepnOLB,
            GC::DR,
            self::CONTRA_CLS_DR,
            '',
            GC::IFRS_OLB
        );
        self::printContraLine(
            $data,
            $contra->totalDepnVPE,
            GC::DR,
            self::CONTRA_CLS_DR,
            '',
            GC::IFRS_VPE
        );
        self::printContraLine(
            $data,
            $contra->totalDepnFE,
            GC::DR,
            self::CONTRA_CLS_DR,
            '',
            GC::IFRS_FE
        );
        self::printContraLine(
            $data,
            $contra->totalDepnINF,
            GC::DR,
            self::CONTRA_CLS_DR,
            '',
            GC::IFRS_INF
        );
        self::printContraLine(
            $data,
            $contra->totalDepnINT,
            GC::DR,
            self::CONTRA_CLS_DR,
            '',
            GC::IFRS_INT
        );
        self::printContraLine(
            $data,
            $contra->totalDepnEFWOLB,
            GC::DR,
            self::CONTRA_CLS_DR,
            '',
            GC::IFRS_EFWOLB
        );
        self::printContraLine(
            $data,
            $contra->totalDepnEFWVPE,
            GC::DR,
            self::CONTRA_CLS_DR,
            '',
            GC::IFRS_EFWVPE
        );
        self::printContraLine(
            $data,
            $contra->totalDepnPFI,
            GC::DR,
            self::CONTRA_CLS_DR,
            '',
            GC::IFRS_PFI
        );
    }

    private static function printLine(&$data, &$cyDepn, $post, $class, $account, $ifrsCode)
    {
        $depn = self::getDepreciationData($class, $ifrsCode);
        $cyDepn = Common::numberFormat($depn->cy_depn, thousandsSep: ',');

        self::addJournalRow(
            $data,
            $post,
            $account,
            '',
            $cyDepn,
            "$ifrsCode In Year Depreciation"
        );
    }

    private static function printCIELine(&$data, $post, $class, $account, $ifrsCode)
    {
        $accDepn = self::getDepreciationData($class, $ifrsCode, true);

        foreach ($accDepn as $depn) {
            $cyDepn = Common::numberFormat($depn->cy_depn, thousandsSep: ',');
            $accCode = $depn->account_code;

            self::addJournalRow(
                $data,
                $post,
                $account,
                $accCode,
                $cyDepn,
                "$ifrsCode In Year Depreciation"
            );
        }
    }

    private static function printContraLine(&$data, $depn, $post, $account, $cost, $ifrsCode)
    {
        self::addJournalRow(
            $data,
            $post,
            $account,
            $cost,
            $depn,
            "Contra $ifrsCode In Year Depreciation"
        );
    }
}
