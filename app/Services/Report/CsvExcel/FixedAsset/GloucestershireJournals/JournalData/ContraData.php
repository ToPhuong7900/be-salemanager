<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\GloucestershireJournals\JournalData;

class ContraData
{
    public $totalDepnOLB = 0;
    public $totalDepnVPE = 0;
    public $totalDepnFE = 0;
    public $totalDepnINF = 0;
    public $totalDepnINT = 0;
    public $totalDepnEFWOLB = 0;
    public $totalDepnEFWVPE = 0;
    public $totalDepnPFI = 0;
}
