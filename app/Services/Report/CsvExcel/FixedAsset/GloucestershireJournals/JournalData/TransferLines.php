<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\GloucestershireJournals\JournalData;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Math;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\GloucestershireJournals\GloucestershireConstant as GC;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\GloucestershireJournals\Traits\JournalOutputTrait;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\GloucestershireJournals\Traits\JournalDataTrait;

class TransferLines
{
    use JournalOutputTrait;
    use JournalDataTrait;

    private const GL_OLB_GBV = '90015';
    private const GL_VPE_GBV = '90115';
    private const GL_FE_GBV = '90216';
    private const GL_INF_GBV = '90320';
    private const GL_SUR_GBV = '90615';
    private const GL_AUC_GBV = '90500';
    private const GL_EFW_OLB_GBV = '90070';
    private const GL_EFW_VPE_GBV = '90070';
    private const GL_PFI_GBV = '90080';

    private const GL_OLB_DPN = '90016';
    private const GL_INF_DPN = '90315';
    private const GL_VPE_DPN = '90116';
    private const GL_FE_DPN = '90216';
    private const GL_EFW_OLB_DPN = '90071';
    private const GL_EFW_VPE_DPN = '90071';
    private const GL_PFI_DPN = '90081';

    public static function addLines(&$data)
    {
        self::addMovementFromLines($data);
        self::addDepnFromLines($data);
        self::addMovementIntoLines($data);
        self::addDepnIntoLines($data);
    }

    private static function addMovementFromLines(&$data)
    {
       // Transfer From Movements.
        self::printGBVLine(
            $data,
            GC::CR,
            self::GL_OLB_GBV,
            GC::IFRS_OLB
        );
        self::printGBVLine(
            $data,
            GC::CR,
            self::GL_INF_GBV,
            GC::IFRS_INF
        );
        self::printGBVLine(
            $data,
            GC::CR,
            self::GL_SUR_GBV,
            GC::IFRS_SUR
        );
        self::printGBVLine(
            $data,
            GC::CR,
            self::GL_VPE_GBV,
            GC::IFRS_VPE
        );
        self::printGBVLine(
            $data,
            GC::CR,
            self::GL_AUC_GBV,
            GC::IFRS_AUC
        );
        self::printGBVLine(
            $data,
            GC::CR,
            self::GL_FE_GBV,
            GC::IFRS_FE
        );
        self::printGBVLine(
            $data,
            GC::CR,
            self::GL_PFI_GBV,
            GC::IFRS_PFI
        );
        self::printGBVLine(
            $data,
            GC::CR,
            self::GL_EFW_OLB_GBV,
            GC::IFRS_EFWOLB
        );
        self::printGBVLine(
            $data,
            GC::CR,
            self::GL_EFW_VPE_GBV,
            GC::IFRS_EFWVPE
        );
    }

    private static function addDepnFromLines(&$data)
    {
        // Transfer Depreciation from movement.
        self::printDPNLine(
            $data,
            GC::DR,
            self::GL_OLB_DPN,
            GC::IFRS_OLB
        );
        self::printDPNLine(
            $data,
            GC::DR,
            self::GL_INF_DPN,
            GC::IFRS_INF
        );
        self::printDPNLine(
            $data,
            GC::DR,
            self::GL_VPE_DPN,
            GC::IFRS_VPE
        );
        self::printDPNLine(
            $data,
            GC::DR,
            self::GL_FE_DPN,
            GC::IFRS_FE
        );
        self::printDPNLine(
            $data,
            GC::DR,
            self::GL_PFI_DPN,
            GC::IFRS_PFI
        );
        self::printDPNLine(
            $data,
            GC::DR,
            self::GL_EFW_OLB_DPN,
            GC::IFRS_EFWOLB
        );
        self::printDPNLine(
            $data,
            GC::DR,
            self::GL_EFW_VPE_DPN,
            GC::IFRS_EFWVPE
        );
    }

    private static function addMovementIntoLines(&$data)
    {
        // Transfer Into Movements.
        self::printGBVLine(
            $data,
            GC::DR,
            self::GL_OLB_GBV,
            GC::IFRS_OLB,
            false
        );
        self::printGBVLine(
            $data,
            GC::DR,
            self::GL_INF_GBV,
            GC::IFRS_INF,
            false
        );
        self::printGBVLine(
            $data,
            GC::DR,
            self::GL_SUR_GBV,
            GC::IFRS_SUR,
            false
        );
        self::printGBVLine(
            $data,
            GC::DR,
            self::GL_VPE_GBV,
            GC::IFRS_VPE,
            false
        );
        self::printGBVLine(
            $data,
            GC::DR,
            self::GL_AUC_GBV,
            GC::IFRS_AUC,
            false
        );
        self::printGBVLine(
            $data,
            GC::DR,
            self::GL_FE_GBV,
            GC::IFRS_FE,
            false
        );
        self::printGBVLine(
            $data,
            GC::DR,
            self::GL_PFI_GBV,
            GC::IFRS_PFI,
            false
        );
        self::printGBVLine(
            $data,
            GC::DR,
            self::GL_EFW_OLB_GBV,
            GC::IFRS_EFWOLB,
            false
        );
        self::printGBVLine(
            $data,
            GC::DR,
            self::GL_EFW_VPE_GBV,
            GC::IFRS_EFWVPE,
            false
        );
    }

    private static function addDepnIntoLines(&$data)
    {
        // Transfer Depreciation Into Movements.
        self::printDPNLine(
            $data,
            GC::CR,
            self::GL_OLB_DPN,
            GC::IFRS_OLB,
            false
        );
        self::printDPNLine(
            $data,
            GC::CR,
            self::GL_INF_DPN,
            GC::IFRS_INF,
            false
        );
        self::printDPNLine(
            $data,
            GC::CR,
            self::GL_VPE_DPN,
            GC::IFRS_VPE,
            false
        );
        self::printDPNLine(
            $data,
            GC::CR,
            self::GL_FE_DPN,
            GC::IFRS_FE,
            false
        );
        self::printDPNLine(
            $data,
            GC::CR,
            self::GL_PFI_DPN,
            GC::IFRS_PFI,
            false
        );
        self::printDPNLine(
            $data,
            GC::CR,
            self::GL_EFW_OLB_DPN,
            GC::IFRS_EFWOLB,
            false
        );
        self::printDPNLine(
            $data,
            GC::CR,
            self::GL_EFW_VPE_DPN,
            GC::IFRS_EFWVPE,
            false
        );
    }

    private static function printGBVLine(&$data, $post, $account, $ifrsCode, $from = true)
    {
        $tfrData = self::getTransferData($ifrsCode, $from);

        if ($from) {
            $gbv = Common::numberFormat(Math::negateCurrency($tfrData->gross_movement), thousandsSep: ',');
            $msg = "Transfer from $ifrsCode";
        } else {
            $gbv = Common::numberFormat($tfrData->gross_movement, thousandsSep: ',');
            $msg = "Transfer into $ifrsCode";
        }

        self::addJournalRow(
            $data,
            $post,
            $account,
            '',
            $gbv,
            $msg
        );
    }

    private static function printDPNLine(&$data, $post, $account, $ifrsCode, $from = true)
    {
        $tfrData = self::getTransferData($ifrsCode, $from);

        if ($from) {
            $gbv = Common::numberFormat(Math::negateCurrency($tfrData->depn_movement), thousandsSep: ',');
            $msg = "Transfer Depreciation from $ifrsCode";
        } else {
            $gbv = Common::numberFormat($tfrData->depn_movement, thousandsSep: ',');
            $msg = "Transfer Depreciation into $ifrsCode";
        }

        self::addJournalRow(
            $data,
            $post,
            $account,
            '',
            $gbv,
            $msg
        );
    }
}
