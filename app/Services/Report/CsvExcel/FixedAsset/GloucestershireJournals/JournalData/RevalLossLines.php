<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\GloucestershireJournals\JournalData;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Math;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\GloucestershireJournals\GloucestershireConstant as GC;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\GloucestershireJournals\Traits\JournalOutputTrait;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\GloucestershireJournals\Traits\JournalDataTrait;

class RevalLossLines
{
    use JournalOutputTrait;
    use JournalDataTrait;

    private const GL_97100 = '97100';

    private const GL_OLB_DR = '90017';
    private const GL_SUR_DR = '90615';
    private const GL_EFW_OLB_DR = '90072';
    private const GL_PFI_DR = '90082';

    private const GL_97110 = '97110';
    private const GL_EFW_OLB_CR = '97114';
    private const GL_PFI_CR = '97115';

    private const GL_SD = '70100';

    private const CONTRA_SD_BY_ASSET = '74202';

    private const GL_DEPN_WO_OLB_DR = '90016';
    private const GL_DEPN_WO_OLB_CR = '97110';

    private const GL_DEPN_WO_EFW_OLB_DR = '90070';
    private const GL_DEPN_WO_EFW_OLB_CR = '97115';

    private const GL_DEPN_WO_PFI_DR = '90080';
    private const GL_DEPN_WO_PFI_CR = '97114';


    public static function addLines(&$data)
    {
        self::addAssetRegLines($data);
        self::addRRLines($data);
        self::addImpRevLines($data);
        self::addDepnWoLines($data);
    }

    private static function addAssetRegLines(&$data)
    {
        // Revaluation gain - application to fixed asset registers.
        self::printRRLine(
            $data,
            GC::CR,
            GC::CLASS_OLB,
            self::GL_OLB_DR,
            GC::IFRS_OLB,
            self::GL_OLB_DR
        );
        self::printRRLine(
            $data,
            GC::CR,
            GC::CLASS_SUR,
            self::GL_SUR_DR,
            GC::IFRS_SUR,
            self::GL_SUR_DR
        );

        self::printRRLine(
            $data,
            GC::CR,
            GC::CLASS_EFW_OLB,
            self::GL_EFW_OLB_DR,
            GC::IFRS_EFWOLB,
            self::GL_EFW_OLB_DR
        );

        self::printRRLine(
            $data,
            GC::CR,
            GC::CLASS_PFI,
            self::GL_PFI_DR,
            GC::IFRS_PFI,
            self::GL_PFI_DR
        );
    }

    private static function addRRLines(&$data)
    {
        // Revaluation gain - application to revaluation reserve.
        self::printRRLine(
            $data,
            GC::DR,
            GC::CLASS_OLB,
            self::GL_97110,
            GC::IFRS_OLB
        );
        self::printRRLine(
            $data,
            GC::DR,
            GC::CLASS_SUR,
            self::GL_97110,
            GC::IFRS_SUR
        );

        self::printRRLine(
            $data,
            GC::DR,
            GC::CLASS_EFW_OLB,
            self::GL_EFW_OLB_CR,
            GC::IFRS_EFWOLB
        );

        self::printRRLine(
            $data,
            GC::DR,
            GC::CLASS_PFI,
            self::GL_PFI_CR,
            GC::IFRS_PFI
        );
    }

    private static function addImpRevLines(&$data)
    {
        // Revaluation gain - impairment reversal application to revaluation reserve/CI&E/CAA (where asset was
        // previously valued down below historic value).
        // Credit S&D by Asset
        $totalContra = 0;

        self::printSDLine(
            $data,
            $totalContra,
            GC::DR,
            GC::CLASS_OLB,
            self::GL_SD,
            GC::IFRS_OLB
        );

        self::printSDLine(
            $data,
            $totalContra,
            GC::DR,
            GC::CLASS_SUR,
            self::GL_SD,
            GC::IFRS_SUR
        );

        self::printSDLine(
            $data,
            $totalContra,
            GC::DR,
            GC::CLASS_EFW_OLB,
            self::GL_SD,
            GC::IFRS_EFWOLB
        );

        self::printSDLine(
            $data,
            $totalContra,
            GC::DR,
            GC::CLASS_PFI,
            self::GL_SD,
            GC::IFRS_PFI
        );

        self::addJournalRow(
            $data,
            GC::DR,
            self::GL_97100,
            '',
            $totalContra,
            "Contra Reval Decrease to S/D - Loss"
        );

        self::addJournalRow(
            $data,
            GC::CR,
            self::CONTRA_SD_BY_ASSET,
            '802512',
            $totalContra,
            "Contra Reval Decrease to S/D - Loss"
        );
    }

    private static function addDepnWoLines(&$data)
    {
        // W/O Accum Depr on Reval.
        self::printDepnWOLine(
            $data,
            GC::DR,
            GC::CLASS_OLB,
            self::GL_DEPN_WO_OLB_DR,
            GC::IFRS_OLB
        );
        self::printDepnWOLine(
            $data,
            GC::CR,
            GC::CLASS_OLB,
            self::GL_DEPN_WO_OLB_CR,
            GC::IFRS_OLB
        );

        self::printDepnWOLine(
            $data,
            GC::DR,
            GC::CLASS_EFW_OLB,
            self::GL_DEPN_WO_EFW_OLB_DR,
            GC::IFRS_EFWOLB
        );
        self::printDepnWOLine(
            $data,
            GC::CR,
            GC::CLASS_EFW_OLB,
            self::GL_DEPN_WO_EFW_OLB_CR,
            GC::IFRS_EFWOLB
        );

        self::printDepnWOLine(
            $data,
            GC::DR,
            GC::CLASS_PFI,
            self::GL_DEPN_WO_PFI_DR,
            GC::IFRS_PFI
        );
        self::printDepnWOLine(
            $data,
            GC::CR,
            GC::CLASS_PFI,
            self::GL_DEPN_WO_PFI_CR,
            GC::IFRS_PFI
        );
    }

    private static function printRRLine(&$data, $post, $class, $account, $ifrsCode, $sdAccount = '')
    {
        $revUp = self::getRevalData($class, $ifrsCode, up: false);
        $rrGain = Common::numberFormat(Math::negateCurrency($revUp->rr_gain), thousandsSep: ',');
        $sdGain = Common::numberFormat(Math::negateCurrency($revUp->gross_sd), thousandsSep: ',');

        self::addJournalRow(
            $data,
            $post,
            $account,
            '',
            $rrGain,
            "$ifrsCode Revaluation Decrease to RR - Loss"
        );

        if ($post === GC::CR) {
            self::addJournalRow(
                $data,
                $post,
                $sdAccount,
                '',
                $sdGain,
                "$ifrsCode Revaluation Decrease to S/D - Loss"
            );
        }
    }

    private static function printDepnWOLine(&$data, $post, $class, $account, $ifrsCode)
    {
        $revUp = self::getRevalData($class, $ifrsCode, up: false);
        $depnWO = Common::numberFormat($revUp->depn_wo_rr, thousandsSep: ',');

        self::addJournalRow(
            $data,
            $post,
            $account,
            '',
            $depnWO,
            "$ifrsCode Depr W/O to RR"
        );
    }

    private static function printSDLine(&$data, &$totalContra, $post, $class, $account, $ifrsCode)
    {
        $revUp = self::getRevalData($class, $ifrsCode, true, false);

        foreach ($revUp as $rev) {
            $sdGain = Common::numberFormat(Math::negateCurrency($rev->gross_sd), thousandsSep: ',');
            $totalContra = Math::addCurrency([$totalContra, Math::negateCurrency($rev->gross_sd)]);

            $accCode = $rev->account_code;

            self::addJournalRow(
                $data,
                $post,
                $account,
                $accCode,
                $sdGain,
                "$ifrsCode Revaluation Decrease to S/D - Loss"
            );
        }
    }
}
