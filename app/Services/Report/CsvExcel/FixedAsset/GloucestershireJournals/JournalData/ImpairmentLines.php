<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\GloucestershireJournals\JournalData;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Math;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\GloucestershireJournals\GloucestershireConstant as GC;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\GloucestershireJournals\Traits\JournalOutputTrait;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\GloucestershireJournals\Traits\JournalDataTrait;

class ImpairmentLines
{
    use JournalOutputTrait;
    use JournalDataTrait;

    private const GL_90015 = '90015';
    private const GL_90070 = '90070';
    private const GL_90080 = '90080';
    private const GL_97110 = '97110';

    // Asset Reg
    private const GL_OLB_CR = self::GL_90015;
    private const GL_OLB_CR_SD = self::GL_90015;

    private const GL_SUR_CR = '90615';

    private const GL_PFI_CR = self::GL_90080;
    private const GL_PFI_CR_SD = self::GL_90080;

    private const GL_EFW_OLB_CR = self::GL_90070;
    private const GL_EFW_OLB_CR_SD = self::GL_90070;

    // RR
    private const GL_OLB_DR = self::GL_97110;
    private const GL_SUR_DR = self::GL_97110;
    private const GL_EFW_OLB_DR = '97114';
    private const GL_PFI_DR = '97115';


    // CAA
    private const GL_SD = '70100';

    private const CONTRA_SD = '97100';
    private const CONTRA_SD_BY_ASSET = '74202';



    public static function addLines(&$data)
    {
        // Impairment Loss - application to fixed asset registers.
        self::printRRLine(
            $data,
            GC::CR,
            GC::CLASS_OLB,
            self::GL_OLB_CR,
            GC::IFRS_OLB,
            self::GL_OLB_CR_SD
        );
        self::printRRLine(
            $data,
            GC::CR,
            GC::CLASS_SUR,
            self::GL_SUR_CR,
            GC::IFRS_SUR
        );

        self::printRRLine(
            $data,
            GC::CR,
            GC::CLASS_EFW_OLB,
            self::GL_EFW_OLB_CR,
            GC::IFRS_EFWOLB,
            self::GL_EFW_OLB_CR_SD
        );

        self::printRRLine(
            $data,
            GC::CR,
            GC::CLASS_PFI,
            self::GL_PFI_CR,
            GC::IFRS_PFI,
            self::GL_PFI_CR_SD
        );

        // Impairment Loss - application to revaluation reserve.
        self::printRRLine(
            $data,
            GC::DR,
            GC::CLASS_OLB,
            self::GL_OLB_DR,
            GC::IFRS_OLB
        );
        self::printRRLine(
            $data,
            GC::DR,
            GC::CLASS_SUR,
            self::GL_SUR_DR,
            GC::IFRS_SUR
        );

        self::printRRLine(
            $data,
            GC::DR,
            GC::CLASS_EFW_OLB,
            self::GL_EFW_OLB_DR,
            GC::IFRS_EFWOLB
        );

        self::printRRLine(
            $data,
            GC::DR,
            GC::CLASS_PFI,
            self::GL_PFI_DR,
            GC::IFRS_PFI
        );

        // Impairment Loss - impairment reversal application to revaluation reserve/CI&E/CAA (where asset was
        // previously valued down below historic value).
        // Credit S&D by Asset
        $totalContra = 0;

        self::printSDLine(
            $data,
            $totalContra,
            GC::DR,
            GC::CLASS_OLB,
            self::GL_SD,
            GC::IFRS_OLB
        );

        self::printSDLine(
            $data,
            $totalContra,
            GC::DR,
            GC::CLASS_SUR,
            self::GL_SD,
            GC::IFRS_SUR
        );

        self::printSDLine(
            $data,
            $totalContra,
            GC::DR,
            GC::CLASS_EFW_OLB,
            self::GL_SD,
            GC::IFRS_EFWOLB
        );

        self::printSDLine(
            $data,
            $totalContra,
            GC::DR,
            GC::CLASS_PFI,
            self::GL_SD,
            GC::IFRS_PFI
        );


        self::addJournalRow(
            $data,
            GC::DR,
            self::CONTRA_SD,
            '',
            $totalContra,
            "Contra Impairment to S/D - Loss"
        );

        self::addJournalRow(
            $data,
            GC::CR,
            self::CONTRA_SD_BY_ASSET,
            '802512',
            $totalContra,
            "Contra Impairment to S/D - Loss"
        );
    }

    private static function printRRLine(&$data, $post, $class, $account, $ifrsCode, $sdAccount = '')
    {
        $revUp = self::getRevalData($class, $ifrsCode, up: false);
        $rrLoss = Common::numberFormat(Math::negateCurrency($revUp->imp_wo_rr), thousandsSep: ',');
        $sdLoss = Common::numberFormat(Math::negateCurrency($revUp->imp_wo_sd), thousandsSep: ',');

        self::addJournalRow(
            $data,
            $post,
            $account,
            '',
            $rrLoss,
            "$ifrsCode Impairment to RR - Loss"
        );

        if ($post === GC::CR && $class !== GC::CLASS_SUR) {
            self::addJournalRow(
                $data,
                $post,
                $sdAccount,
                '',
                $sdLoss,
                "$ifrsCode Impairment to S/D - Loss"
            );
        }
    }

    private static function printSDLine(&$data, &$totalContra, $post, $class, $account, $ifrsCode)
    {
        $revUp = self::getRevalData($class, $ifrsCode, true, false);

        foreach ($revUp as $rev) {
            $sdLoss = Common::numberFormat(Math::negateCurrency($rev->imp_wo_sd), thousandsSep: ',');
            $totalContra = Math::addCurrency([$totalContra, Math::negateCurrency($rev->imp_wo_sd)]);

            $accCode = $rev->account_code;

            self::addJournalRow(
                $data,
                $post,
                $account,
                $accCode,
                $sdLoss,
                "$ifrsCode Impairment to S/D - Loss"
            );
        }
    }
}
