<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\GloucestershireJournals;

class GloucestershireConstant
{
    public const CR = 'Credit';
    public const DR = 'Debit';

    public const CLASS_OLB = ['1000', '2000'];
    public const CLASS_VPE = ['4000'];
    public const CLASS_FE = ['5000', '6000'];
    public const CLASS_INF = ['7000', '8000', '9000', '10000'];
    public const CLASS_INT = ['18000'];
    public const CLASS_SUR = ['14000', '15000'];
    public const CLASS_AUC = ['12000'];
    public const CLASS_EFW_OLB = ['EFWOLB'];
    public const CLASS_EFW_VPE = ['EFWVPE'];
    public const CLASS_PFI = ['PFIOLB'];

    public const IFRS_OLB = 'OLB';
    public const IFRS_VPE = 'VPE';
    public const IFRS_FE = 'F&E';
    public const IFRS_INF = 'INF';
    public const IFRS_INT = 'INT';
    public const IFRS_SUR = 'SUR';
    public const IFRS_AUC = 'AUC';
    public const IFRS_EFWOLB = 'EFWOLB';
    public const IFRS_EFWVPE = 'EFWVPE';
    public const IFRS_PFI = 'PFIOLB';
}
