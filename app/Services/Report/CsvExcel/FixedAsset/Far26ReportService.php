<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset;

use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Math;
use Tfcloud\Models\Report;
use Tfcloud\Models\SiteGroup;
use Tfcloud\Models\Views\VwFar06;
use Tfcloud\Services\PermissionService;

class Far26ReportService extends JournalPostingService
{
    protected $entity = false;

    private $reportBoxFilterLoader = null;

    public function __construct(PermissionService $permissionService, $filterData, Report $report = null)
    {
        parent::__construct($permissionService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;

        $this->setFilterProperties($filterData);
    }

    private function setFilterProperties($filterData)
    {
        $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($filterData);
        $entity = $reportFilterQuery->getFirstValueFilterField('ca_entity_id');

        // get filter values
        $this->entity = (!$entity) ? false : $entity;
    }

    public function getReportData(&$filterText, &$sOrderText)
    {
        // If no admin ca_posting_account record available, exit.
        if (!$this->postingAcc) {
            return [];
        }

        $far06 = $this->getData();

        $outputData = [];
        $curTotalCAAReversalGF = 0;
        $curTotalMIRSReversal = 0;
        $curTotalHRAMIRSReversal = 0;
        $curTotalCAAReversalHRA = 0;
        $curRRUpwardsTotal = 0;
        $curRRUpwardsTotalHRA = 0;
        $curRRDownwardsTotal = 0;
        $curRRDownwardsTotalHRA = 0;

        foreach ($far06 as $row) {
            $outputRow = [];

            $commonGood = $row->common_good === 'Y';
            $hraAsset = $row->hra_asset === 'Y';

            /**
             * Gross Value change - move to Fixed Asset Balance Sheet
             * Balance sheet code comes from IFRS Category
             */
            $curGross = Math::addCurrency([$row->gross_rr, $row->gross_sd]);
            if (Math::notEqualCurrency($curGross, 0)) {
                if ($hraAsset) {
                    $this->writeCodeSection(
                        $outputRow,
                        $this->postingAcc->acc_balance_sheet,
                        $row->account_code_1,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_hra_code
                    );
                } elseif ($commonGood) {
                    $this->writeCodeSection(
                        $outputRow,
                        $row->account_code,
                        $row->account_code_1,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_blank
                    );
                } else {
                    $this->writeCodeSection(
                        $outputRow,
                        $this->postingAcc->acc_balance_sheet,
                        $row->account_code_1,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_gf_code
                    );
                }

                $text = "{$row->fixed_asset_code} Revaluation ({$row->ca_ifrs_category_code})";
                if (Math::greaterThanEqualCurrency($curGross, 0)) {
                    $this->writeDebit($outputRow, $curGross, $text);
                } else {
                    $this->writeCredit($outputRow, Math::negateCurrency($curGross), $text);
                }
                $this->newRptLine($outputData, $outputRow);
            }

            // Accumulated Depn Write Off
            $curDepnWo = Math::addCurrency([$row->depn_woff_rr, $row->depn_woff_sd]);
            if (Math::notEqualCurrency($curDepnWo, 0)) {
                if ($hraAsset) {
                    $this->writeCodeSection(
                        $outputRow,
                        $this->postingAcc->acc_balance_sheet,
                        $row->account_code_2,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_hra_code
                    );
                } elseif ($commonGood) {
                    $this->writeCodeSection(
                        $outputRow,
                        $row->account_code,
                        $row->account_code_2,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_blank
                    );
                } else {
                    $this->writeCodeSection(
                        $outputRow,
                        $this->postingAcc->acc_balance_sheet,
                        $row->account_code_2,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_gf_code
                    );
                }

                $text = "{$row->fixed_asset_code} Depreciation w.off on valuation";
                if (Math::greaterThanEqualCurrency($curDepnWo, 0)) {
                    $this->writeDebit($outputRow, $curDepnWo, $text);
                } else {
                    $this->writeCredit($outputRow, $curDepnWo, $text);
                }
                $this->newRptLine($outputData, $outputRow);
            }

            // Accumulated Impairment Write Off - currently to gross balance sheet code for Fife
            $curImpWo = Math::addCurrency([$row->imp_woff_rr, $row->imp_woff_sd]);
            if (Math::notEqualCurrency($curImpWo, 0)) {
                $this->writeCodeSection(
                    $outputRow,
                    $this->postingAcc->acc_balance_sheet,
                    $row->account_code_3,
                    $this->postingAcc->prj_blank_project,
                    $this->postingAcc->fund_gf_code
                );

                $text = "{$row->fixed_asset_code} Impairment w.off";
                if (Math::greaterThanEqualCurrency($curImpWo, 0)) {
                    $this->writeDebit($outputRow, $curImpWo, $text);
                } else {
                    $this->writeCredit($outputRow, $curImpWo, $text);
                }
                $this->newRptLine($outputData, $outputRow);
            }

            // Net valuation change to Revaluation Reserve
            $curRR = $row->reval_reserve;
            if (Math::notEqualCurrency($curRR, 0)) {
                if ($hraAsset) {
                    $this->writeCodeSection(
                        $outputRow,
                        $this->postingAcc->acc_balance_sheet,
                        $this->postingAcc->sub_rr,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_hra_code
                    );
                } elseif ($commonGood) {
                    $this->writeCodeSection(
                        $outputRow,
                        $row->account_code,
                        $this->postingAcc->sub_rr,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_blank
                    );
                } else {
                    $this->writeCodeSection(
                        $outputRow,
                        $this->postingAcc->acc_balance_sheet,
                        $this->postingAcc->sub_rr,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_gf_code
                    );
                }

                $text = "{$row->fixed_asset_code} RR Valuation";
                if (Math::greaterThanCurrency($curRR, 0)) {
                    // Gain to RR
                    $this->writeCredit($outputRow, $curRR, "$text Gain", "RR");
                    if (!$commonGood) {
                        if ($hraAsset) {
                            $curRRUpwardsTotalHRA = Math::addCurrency([$curRRUpwardsTotalHRA, $curRR]);
                        } else {
                            $curRRUpwardsTotal = Math::addCurrency([$curRRUpwardsTotal, $curRR]);
                        }
                    }
                } else {
                    // Loss to RR
                    $this->writeDebit($outputRow, Math::negateCurrency($curRR), "$text Loss", "RR");
                    if (!$commonGood) {
                        if ($hraAsset) {
                            $curRRDownwardsTotalHRA =
                                Math::addCurrency([$curRRDownwardsTotalHRA, Math::negateCurrency($curRR)]);
                        } else {
                            $curRRDownwardsTotal =
                                Math::addCurrency([$curRRDownwardsTotal, Math::negateCurrency($curRR)]);
                        }
                    }
                }
                $this->newRptLine($outputData, $outputRow);

                // Common good RR gains and losses
                if ($commonGood) {
                  /**
                   * ////////////////////////////////////////////////////////
                   * // Write out total RR Upwards/downwards and total (HRA)
                   * ////////////////////////////////////////////////////////
                   */
                    if (Math::greaterThanCurrency($curRR, 0)) {
                        $this->writeCodeSection(
                            $outputRow,
                            $row->account_code,
                            $this->postingAcc->sub_revaluation_rr_upward,
                            $this->postingAcc->prj_blank_project,
                            $this->postingAcc->fund_blank
                        );
                        $this->writeCredit($outputRow, $curRR, "Upward Revaluation of Asset to RR (CG)");
                    } else {
                        $this->writeCodeSection(
                            $outputRow,
                            $row->account_code,
                            $this->postingAcc->sub_revaluation_rr_downward,
                            $this->postingAcc->prj_blank_project,
                            $this->postingAcc->fund_blank
                        );
                        $text = "Downward Revaluation of Asset to RR (HRA)";
                        $this->writeDebit($outputRow, Math::negateCurrency($curRR), $text);
                    }
                    $this->newRptLine($outputData, $outputRow);

                    // Credit / Debit difference
                    $this->writeCodeSection(
                        $outputRow,
                        $row->account_code,
                        $this->postingAcc->sub_revaluation_rr,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_blank
                    );

                    $text = "Revaluation of Assets to RR (CG)";
                    if (Math::greaterThanCurrency($curRR, 0)) {
                        $this->writeDebit($outputRow, $curRR, $text);
                    } else {
                        $this->writeCredit($outputRow, Math::negateCurrency($curRR), $text);
                    }
                    $this->newRptLine($outputData, $outputRow);
                }
            }

            // Net to Charge or gain to I&E / Surplus Deficit
            $curSD = $row->surplus_deficit;
            if (Math::notEqualCurrency($curSD, 0)) {
                if ($hraAsset) {
                    $this->writeCodeSection(
                        $outputRow,
                        $row->account_code,
                        $this->postingAcc->sub_revaluation_loss,
                        $this->postingAcc->prj_hra_mirs,
                        $this->postingAcc->fund_hra_ie_sd
                    );
                } else {
                    $this->writeCodeSection(
                        $outputRow,
                        $row->account_code,
                        $this->postingAcc->sub_revaluation_loss,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_blank
                    );
                }
                $text = "{$row->fixed_asset_code} Surplus/Deficit";
                if (Math::greaterThanEqualCurrency($curSD, 0)) {
                    // Gain to I&E
                    $this->writeCredit($outputRow, $curSD, "$text Gain", "I&E");
                } else {
                    // Loss to I&E
                    $this->writeDebit($outputRow, Math::negateCurrency($curSD), "$text Loss", "I&E");
                }
                $this->newRptLine($outputData, $outputRow);

                /**
                 * MIRS and CAA for common good
                 * Loss is Cr MIRS Dr CAA
                 */
                if (Math::notEqualCurrency($curSD, 0) && $commonGood) {
                    /**
                     * ////////////////////////////////////////////////////////
                     * // Write out total reversal to MIRS (Common Good)
                     * ////////////////////////////////////////////////////////
                     */

                    // Cr MIRS Dr CAA
                    $this->writeCodeSection(
                        $outputRow,
                        $row->account_code,
                        $this->postingAcc->sub_revaluation_loss_reversal,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_blank
                    );
                    // Cr MIRS Dr CAA
                    $text = "I&E Gains and Losses (Common Good)";
                    if (Math::greaterThanCurrency($curSD, 0)) {
                        $this->writeDebit($outputRow, $curSD, $text, "MIRS");
                    } else {
                        $this->writeCredit($outputRow, Math::negateCurrency($curSD), $text, "MIRS");
                    }
                    $this->newRptLine($outputData, $outputRow);

                    // Dr CAA line (for Common Good)
                    $this->writeCodeSection(
                        $outputRow,
                        $row->account_code,
                        $this->postingAcc->sub_capital_adjustment_acc,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_blank
                    );
                    $text = "Asset Revaluation Losses (Common Good)";
                    if (Math::greaterThanCurrency($curSD, 0)) {
                        $this->writeCredit($outputRow, $curSD, $text, "CAA");
                    } else {
                        $this->writeDebit($outputRow, Math::negateCurrency($curSD), $text, "CAA");
                    }
                    $this->newRptLine($outputData, $outputRow);
                } else {
                    /**
                     * If not common good add to totals for a general reversal later
                     * Reversal to MIRS (Cr) Opposite to CAA (Dr)
                     */
                    if ($hraAsset) {
                        $curTotalHRAMIRSReversal = Math::addCurrency([$curTotalHRAMIRSReversal, $curSD]);
                        $curTotalCAAReversalHRA = Math::addCurrency([$curTotalCAAReversalHRA, $curSD]);
                    } else {
                        $curTotalMIRSReversal = Math::addCurrency([$curTotalMIRSReversal, $curSD]);
                        $curTotalCAAReversalGF = Math::addCurrency([$curTotalCAAReversalGF, $curSD]);
                    }
                }
            }
        }

        /**
         * ////////////////////////////////////////////////////////
         * // Write out total RR Upwards/downwards and total
         * ////////////////////////////////////////////////////////
         */
        $this->writeCodeSection(
            $outputRow,
            $this->postingAcc->acc_mirs,
            $this->postingAcc->sub_revaluation_rr_upward,
            $this->postingAcc->prj_blank_project,
            $this->postingAcc->fund_blank
        );
        $this->writeCredit($outputRow, $curRRUpwardsTotal, "Upward Revaluation of Asset to RR");
        $this->newRptLine($outputData, $outputRow);

        $this->writeCodeSection(
            $outputRow,
            $this->postingAcc->acc_mirs,
            $this->postingAcc->sub_revaluation_rr_downward,
            $this->postingAcc->prj_blank_project,
            $this->postingAcc->fund_blank
        );
        $this->writeDebit($outputRow, $curRRDownwardsTotal, "Downward Revaluation of Asset to RR");
        $this->newRptLine($outputData, $outputRow);

        // Credit / Debit difference
        $this->writeCodeSection(
            $outputRow,
            $this->postingAcc->acc_mirs,
            $this->postingAcc->sub_revaluation_rr,
            $this->postingAcc->prj_blank_project,
            $this->postingAcc->fund_blank
        );
        $diffRRTotal = Math::subCurrency([$curRRUpwardsTotal, $curRRDownwardsTotal]);
        if (Math::greaterThanEqualCurrency($diffRRTotal, 0)) {
            $this->writeDebit($outputRow, $diffRRTotal, "Revaluation of Asset to RR");
        } else {
            $this->writeCredit($outputRow, Math::negateCurrency($diffRRTotal), "Revaluation of Asset to RR");
        }
        $this->newRptLine($outputData, $outputRow);


        /**
         * ////////////////////////////////////////////////////////
         * // Write out total RR Upwards/downwards and total (HRA)
         * ////////////////////////////////////////////////////////
         */
        $this->writeCodeSection(
            $outputRow,
            $this->postingAcc->acc_hra_mirs,
            $this->postingAcc->sub_revaluation_rr_upward,
            $this->postingAcc->prj_hra_mirs,
            $this->postingAcc->fund_hra_mirs
        );
        $this->writeCredit($outputRow, $curRRUpwardsTotalHRA, "Upward Revaluation of Asset to RR (HRA)");
        $this->newRptLine($outputData, $outputRow);

        $this->writeCodeSection(
            $outputRow,
            $this->postingAcc->acc_hra_mirs,
            $this->postingAcc->sub_revaluation_rr_downward,
            $this->postingAcc->prj_hra_mirs,
            $this->postingAcc->fund_hra_mirs
        );
        $this->writeDebit($outputRow, $curRRDownwardsTotalHRA, "Downward Revaluation of Asset to RR (HRA)");
        $this->newRptLine($outputData, $outputRow);

        // Credit / Debit difference
        $this->writeCodeSection(
            $outputRow,
            $this->postingAcc->acc_hra_mirs,
            $this->postingAcc->sub_revaluation_rr,
            $this->postingAcc->prj_hra_mirs,
            $this->postingAcc->fund_hra_rr
        );
        $diffHraRRTotal = Math::subCurrency([$curRRUpwardsTotalHRA, $curRRDownwardsTotalHRA]);
        if (Math::greaterThanEqualCurrency($diffHraRRTotal, 0)) {
            $this->writeDebit($outputRow, $diffHraRRTotal, "Revaluation of Asset to RR (HRA)");
        } else {
            $this->writeCredit($outputRow, Math::negateCurrency($diffHraRRTotal), "Revaluation of Asset to RR (HRA)");
        }
        $this->newRptLine($outputData, $outputRow);


        /**
         * ////////////////////////////////////////////////////////
         * // Write out total reversal to MIRS (GF)
         * ////////////////////////////////////////////////////////
         */
        $this->writeCodeSection(
            $outputRow,
            $this->postingAcc->acc_mirs,
            $this->postingAcc->sub_revaluation_loss_reversal,
            $this->postingAcc->prj_blank_project,
            $this->postingAcc->fund_blank
        );
        $text = "I&E Gains and Losses (GF)";
        if (Math::greaterThanEqualCurrency($curTotalMIRSReversal, 0)) {
            $this->writeDebit($outputRow, $curTotalMIRSReversal, $text, "MIRS");
        } else {
            $this->writeCredit($outputRow, Math::negateCurrency($curTotalMIRSReversal), $text, "MIRS");
        }
        $this->newRptLine($outputData, $outputRow);


        /**
         * ////////////////////////////////////////////////////////
         * // Write out total reversal to MIRS HRA to CAA
         * ////////////////////////////////////////////////////////
         */
        $this->writeCodeSection(
            $outputRow,
            $this->postingAcc->acc_hra_mirs,
            $this->postingAcc->sub_revaluation_loss_reversal,
            $this->postingAcc->prj_hra_mirs,
            $this->postingAcc->fund_hra_revaluation
        );
        $text = "I&E Gains and Losses (HRA)";
        if (Math::greaterThanEqualCurrency($curTotalHRAMIRSReversal, 0)) {
            $this->writeDebit($outputRow, $curTotalHRAMIRSReversal, $text, "MIRS");
        } else {
            $this->writeCredit($outputRow, Math::negateCurrency($curTotalHRAMIRSReversal), $text, "MIRS");
        }
        $this->newRptLine($outputData, $outputRow);


        /**
         * ////////////////////////////////////////////////////////
         * // Write out total CAA line (for HRA)
         * ////////////////////////////////////////////////////////
         */
        $this->writeCodeSection(
            $outputRow,
            $this->postingAcc->acc_balance_sheet,
            $this->postingAcc->sub_capital_adjustment_acc,
            $this->postingAcc->prj_blank_project,
            $this->postingAcc->fund_hra_code
        );
        $text = "CAA Asset Revaluation Losses (HRA)";
        if (Math::greaterThanEqualCurrency($curTotalCAAReversalGF, 0)) {
            $this->writeCredit($outputRow, $curTotalHRAMIRSReversal, $text, "CAA");
        } else {
            $this->writeDebit($outputRow, Math::negateCurrency($curTotalHRAMIRSReversal), $text, "CAA");
        }
        $this->newRptLine($outputData, $outputRow);


        /**
         * ////////////////////////////////////////////////////////
         * // Write out total CAA line (GF)
         * ////////////////////////////////////////////////////////
         */
        $this->writeCodeSection(
            $outputRow,
            $this->postingAcc->acc_balance_sheet,
            $this->postingAcc->sub_capital_adjustment_acc,
            $this->postingAcc->prj_blank_project,
            $this->postingAcc->fund_gf_code
        );
        $text = "CAA Asset Revaluation Losses (GF)";
        if (Math::greaterThanEqualCurrency($curTotalCAAReversalGF, 0)) {
            $this->writeCredit($outputRow, $curTotalCAAReversalGF, $text, "CAA");
        } else {
            $this->writeDebit($outputRow, Math::negateCurrency($curTotalCAAReversalGF), $text, "CAA");
        }
        $this->newRptLine($outputData, $outputRow);

        return $outputData;
    }

    private function getData()
    {
        $query = VwFar06::join(
            'ca_ifrs_category',
            'ca_ifrs_category.ca_ifrs_category_id',
            '=',
            'vw_far06.ca_ifrs_category_id'
        )
        ->where('vw_far06.site_group_id', SiteGroup::get()->site_group_id)
        ->select(
            [
                'ca_ifrs_category.ca_ifrs_category_code',
                'ca_ifrs_category.common_good',
                'vw_far06.hra_asset',
                \DB::raw('vw_far06.`Asset Code` as fixed_asset_code'),
                \DB::raw('vw_far06.`Account Code` as account_code'),
                'ca_ifrs_category.account_code_1',
                'ca_ifrs_category.account_code_2',
                'ca_ifrs_category.account_code_3',
                \DB::raw('vw_far06.`Gross RR` as gross_rr'),
                \DB::raw('vw_far06.`Gross SD` as gross_sd'),
                \DB::raw('vw_far06.`Depn WOff RR` as depn_woff_rr'),
                \DB::raw('vw_far06.`Depn WOff SD` as depn_woff_sd'),
                \DB::raw('vw_far06.`Imp WOff RR` as imp_woff_rr'),
                \DB::raw('vw_far06.`Imp WOff SD` as imp_woff_sd'),
                \DB::raw('vw_far06.`Reval Reserve` as reval_reserve'),
                \DB::raw('vw_far06.`Surplus/Deficit` as surplus_deficit'),
            ]
        )
        ->orderBy(\DB::raw('vw_far06.`IFRS Code`'))
        ->orderBy(\DB::raw('vw_far06.`Asset Code`'));

        if ($this->entity) {
            $query->where('vw_far06.ca_entity_id', $this->entity);
        }
        return $query->get();
    }
}
