<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\RugbyJournals;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\FixedAsset\CaFinYearManager;
use Tfcloud\Lib\Math;
use Tfcloud\Models\CaBalance;
use Tfcloud\Models\Views\VwFar07;
use Tfcloud\Services\PermissionService;

class FarCliRBY01ReportService extends RugbyJournalPostingService
{
    protected $yearString = "";
    private $journalData = [];

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->addJournalHeader($this->journalData);
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        // Asset by Asset Depreciation.

        $this->prepareDepreciationData();

        return $this->journalData;
    }

    private function prepareDepreciationData()
    {
        $sRevenueCode = '';
        $sDepnCode = '';

        $sCostCentre = '';
        $sAccountCode1 = '';
        $sAccountCode2 = '';
        $sAccountCode3 = '';
        $sAccountCode4 = '';

        $sIFRSSubType = '';
        $sIFRSCode = '';

        $cyDepn = 0;
        $cyHcDepn = 0;

        $curTotalAmount = 0;          // total depreciation
        $curTotalHRAAmount = 0;       // total for housing
        $curTotalAmortAmount = 0;     // total for amortisation
        $curTotalSurplusAmount = 0;   // total for surplus assets 0900 NDC00

        $curGFAmortisation = 0;       // Amortisation on GF assets
        $curHRAAmortisation = 0;      // Amortisation on HRA assets

        $bDemin = false;              // Rugby's own flag for 'demin' written out in 1 year

        // Just used to implement grouping
        $sGroup = '';
        $bGroupDemin = false;
        $sCurrentRecordGroup = '';

        $sGroupCostCentre = '';
        $curSubTotalDepn = 0;
        $curSubTotalRRDepn = 0;

        $lCount = 0;

        $bHRA = false;

        $assetData = $this->getAllAssetData();

        foreach ($assetData as $asset) {
            $lCount++;

            if ($asset->bf_ca_ifrs_category_desc) {
                $sIFRSSubType = $asset->bf_ca_ifrs_category_desc;
                $sIFRSCode = $asset->bf_ca_ifrs_category_code;
            } else {
                $sIFRSSubType = $asset->ca_ifrs_category_desc;
                $sIFRSCode = $asset->ca_ifrs_category_code;
            }

            if ($sIFRSSubType != $asset->ca_ifrs_category_desc) {
                if (!Common::valueYorNtoBoolean(Auth::user()->siteGroup->ca_cal_mode_year_end)) {
                    $sIFRSSubType = $asset->ca_ifrs_category_desc;
                    $sIFRSCode = $asset->ca_ifrs_category_code;
                }
            }

            // 2nd part of group is cost centre
            $sCostCentre = $asset->account_code;

            // Once live data has been loaded this field will be used instead.
            //$bDemin = $this->isDeminimis($asset->deminimis_asset);
            $bDemin = $this->isDeminimis($asset->rbc_deminimis);

            // Starting Group.
            if ($lCount == 1) {
                $sGroup = "$sCostCentre/$sIFRSSubType";

                if ($bDemin) {
                    $sGroup .= ' d ';
                    $bGroupDemin = true;
                } else {
                    $bGroupDemin = false;
                }

                $sGroupCostCentre = $sCostCentre;
                $sCurrentRecordGroup = $sGroup;
            } else {
                $sCurrentRecordGroup = "$sCostCentre/$sIFRSSubType";

                if ($bDemin) {
                    $sCurrentRecordGroup .= ' d ';
                }
            }

            // Start of a new group?
            if ($sGroup != $sCurrentRecordGroup) {
                // Write out the last (previous) group then start new one

                // No point if both zero
                if (Math::notEqualCurrency($curSubTotalDepn, 0) || Math::notEqualCurrency($curSubTotalRRDepn, 0)) {
                    // Depreciation to Dr revenue (I&E Charge) and Cr accum depn.
                    if (Math::greaterThanCurrency($curSubTotalDepn, 0)) {
                        // Add depn. to grand total.
                        $curTotalAmount = Math::addCurrency([$curTotalAmount, $curSubTotalDepn]);

                        // Default to basic code - overwritten for Surplus/Intangibles
                        $sRevenueCode = self::DEPN_CHARGE_CODE; //0900.
                        $sDepnCode = self::DEPRECIATION_CODE;   //IFADEP.

                        $bHRA = $this->isHRA($sIFRSSubType, $sGroupCostCentre);

                        if ($bHRA) {
                            $curTotalHRAAmount = Math::addCurrency([$curTotalHRAAmount, $curSubTotalDepn]);
                        }

                        // Change Charge code for Surplus
                        if ($this->textContains($sGroup, 'Surplus Assets')) {
                            $sRevenueCode = self::DEPN_CHARGE_CODE;
                            $curTotalSurplusAmount = Math::addCurrency([$curTotalSurplusAmount, $curSubTotalDepn]);
                        }

                        // RBC demin assets written out in 1 year.
                        // Could be Amort as well below which takes precidence.
                        if ($bGroupDemin) {
                            $sRevenueCode = '0905';
                        }

                        // Add to total Amortisation if Intangible (can override choices above).
                        if ($this->textContains($sGroup, 'Intangible Assets')) {
                            $sRevenueCode = '0904';
                            $sDepnCode = self::AMORTISATION_CODE;
                            $curTotalAmortAmount = Math::addCurrency([$curTotalAmortAmount, $curSubTotalDepn]);

                            if ($bHRA) {
                                $curHRAAmortisation = Math::addCurrency([$curHRAAmortisation, $curSubTotalDepn]);
                            } else {
                                $curGFAmortisation = Math::addCurrency([$curGFAmortisation, $curSubTotalDepn]);
                            }
                        }

                        // Charge line for depreciation
                        $this->addJournalRow(
                            $this->journalData,
                            self::DR,
                            $sRevenueCode,
                            $sGroupCostCentre,
                            '',
                            $curSubTotalDepn,
                            "$sGroup depn. $this->yearString"
                        );

                        // Credit to Accumulated depreciation
                        // If HRA swap the cat_4 to IDEPSERV not IFADEP
                        if ($sAccountCode2 == '9050') {
                            if ($this->textContains($sGroup, 'Intangible Assets')) {
                                $this->addJournalRow(
                                    $this->journalData,
                                    self::CR,
                                    '9011',
                                    self::BALANCE_SHEET_ACCOUNT,
                                    'IAMORT',
                                    $curSubTotalDepn,
                                    "$sGroup depn. $this->yearString"
                                );
                            } else {
                                $this->addJournalRow(
                                    $this->journalData,
                                    self::CR,
                                    $sAccountCode2,
                                    self::BALANCE_SHEET_ACCOUNT,
                                    'IDEPSERV',
                                    $curSubTotalDepn,
                                    "$sGroup depn. $this->yearString"
                                );
                            }
                        } else {
                            $this->addJournalRow(
                                $this->journalData,
                                self::CR,
                                $sAccountCode2,
                                self::BALANCE_SHEET_ACCOUNT,
                                $sDepnCode,
                                $curSubTotalDepn,
                                "$sGroup depn. $this->yearString"
                            );
                        }
                    }

                    // If there is a difference in depreciation then depreciate RR
                    if (Math::greaterThanCurrency($curSubTotalRRDepn, 0)) {
                        $this->addJournalRow(
                            $this->journalData,
                            'Dr',
                            $this->revalReserveAccount,
                            self::BALANCE_SHEET_ACCOUNT,
                            'EREV',
                            $curSubTotalRRDepn,
                            "$sGroup RR depn. $this->yearString"
                        );

                        if ($bHRA) {
                            $this->addJournalRow(
                                $this->journalData,
                                'Cr',
                                $this->capitalAdjustmentAccount,
                                self::BALANCE_SHEET_ACCOUNT,
                                'IHRAREV',
                                $curSubTotalRRDepn,
                                "$sGroup RR depn. $this->yearString"
                            );
                        } else {
                            $this->addJournalRow(
                                $this->journalData,
                                'Cr',
                                $this->capitalAdjustmentAccount,
                                self::BALANCE_SHEET_ACCOUNT,
                                'IGFREV',
                                $curSubTotalRRDepn,
                                "$sGroup RR depn. $this->yearString"
                            );
                        }
                    }
                }

                /////// Start new group - record this groups signature ///////
                $sGroup = "$sCostCentre/$sIFRSSubType";

                if ($bDemin) {
                    $sGroup .= ' d ';
                }

                $sGroupCostCentre = $sCostCentre;

                // Reset totals for next group.
                $curSubTotalDepn = 0;
                $curSubTotalRRDepn = 0;

                // Is the next group deminimis.
                $bGroupDemin = $bDemin;
            }

            // Get sub-type accounts.
            $this->getIFRSAccount(
                $sIFRSCode,
                $sAccountCode1,
                $sAccountCode2,
                $sAccountCode3,
                $sAccountCode4
            );

            // Add on values to group totals
            $hcBalance = $this->retrieveBalance(
                $asset->ca_fixed_asset_id,
                CaFinYearManager::currentFinYearId(),
                CaBalance::TYPE_HISTORIC_VALUE
            );
            $cyDepn = $asset->amount;
            $cyHcDepn = $hcBalance->cy_depreciation;

            $curSubTotalDepn = Math::addCurrency([$curSubTotalDepn, $cyDepn]);
            $curSubTotalRRDepn = Math::addCurrency([$curSubTotalRRDepn, Math::subCurrency([$cyDepn, $cyHcDepn])]);
        }   //foreach asset.

        ///////////////////////////////////////////////
        // Write out the last group at EOF
        ///////////////////////////////////////////////

        // No point if both zero
        if (Math::notEqualCurrency($curSubTotalDepn, 0) || Math::notEqualCurrency($curSubTotalRRDepn, 0)) {
            // Add depn. to grand total.
            $curTotalAmount = Math::addCurrency([$curTotalAmount, $curSubTotalDepn]);

            if ($bHRA) {
                $curTotalHRAAmount = Math::addCurrency([$curTotalHRAAmount, $curSubTotalDepn]);
            }

            if (Math::notEqualCurrency($curSubTotalDepn, 0)) {
                if ($this->textContains($sGroup, 'Surplus Assets')) {
                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        '0822',
                        $sCostCentre,
                        '',
                        $curSubTotalDepn,
                        "$sGroup depn. $this->yearString"
                    );
                } else {
                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        self::DEPN_CHARGE_CODE,
                        $sCostCentre,
                        '',
                        $curSubTotalDepn,
                        "$sGroup depn. $this->yearString"
                    );
                }

                if ($this->textContains($sGroup, 'Intangible Assets')) {
                    $this->addJournalRow(
                        $this->journalData,
                        'Cr',
                        $sAccountCode2,
                        self::BALANCE_SHEET_ACCOUNT,
                        self::AMORTISATION_CODE,
                        $curSubTotalDepn,
                        "$sGroup depn. $this->yearString"
                    );
                    $curTotalAmortAmount = Math::addCurrency([$curTotalAmortAmount, $curSubTotalDepn]);
                } else {
                    $this->addJournalRow(
                        $this->journalData,
                        'Cr',
                        $sAccountCode2,
                        self::BALANCE_SHEET_ACCOUNT,
                        self::DEPRECIATION_CODE,
                        $curSubTotalDepn,
                        "$sGroup depn. $this->yearString"
                    );
                }
            }

            // If there is a difference in depreciation then depreciate RR.
            if (Math::notEqualCurrency($curSubTotalRRDepn, 0)) {
                $this->addJournalRow(
                    $this->journalData,
                    'Dr',
                    $this->revalReserveAccount,
                    $sCostCentre,
                    '',
                    $curSubTotalRRDepn,
                    "$sIFRSSubType RR depn. $this->yearString"
                );

                $this->addJournalRow(
                    $this->journalData,
                    'Cr',
                    $this->capitalAdjustmentAccount,
                    $sCostCentre,
                    '',
                    $curSubTotalRRDepn,
                    "$sIFRSSubType RR depn. $this->yearString"
                );
            }
        }

        ///////////////////////////////////////////////
        // Write out reversals of the totals
        ///////////////////////////////////////////////

        /////// Amortisation reversal ///////
        // GF Amortisation  Dr to CAA
        // Reverse          Cr to MIRS
        $this->addJournalRow(
            $this->journalData,
            'Dr',
            $this->capitalAdjustmentAccount,
            self::BALANCE_SHEET_ACCOUNT,
            'EINTANG',
            $curGFAmortisation,
            "Amortisation reversal $this->yearString"
        );

        $this->addJournalRow(
            $this->journalData,
            'Cr',
            '6004',
            self::SMR_COST_CENTRE_4,
            '',
            $curGFAmortisation,
            "Amortisation reversal $this->yearString"
        );

        // GF Amortisation reversal 3107 to 9831.
        $this->addJournalRow(
            $this->journalData,
            'Dr',
            '3105',
            self::SMR_COST_CENTRE_4,
            '',
            $curGFAmortisation,
            "GF Amortisation reversal $this->yearString"
        );

        $this->addJournalRow(
            $this->journalData,
            'Cr',
            '9831',
            self::BALANCE_SHEET_ACCOUNT,
            'EXP',
            $curGFAmortisation,
            "GF Amortisation reversal $this->yearString"
        );

        // HRA Amortisation reversal.
        $this->addJournalRow(
            $this->journalData,
            'Dr',
            '3108',
            'HRA00',
            '',
            $curHRAAmortisation,
            "HRA Amortisation reversal $this->yearString"
        );

        $this->addJournalRow(
            $this->journalData,
            'Cr',
            '9832',
            self::BALANCE_SHEET_ACCOUNT,
            'C10',
            $curHRAAmortisation,
            "HRA Amortisation reversal $this->yearString"
        );

        $this->addJournalRow(
            $this->journalData,
            'Dr',
            '9810',
            self::BALANCE_SHEET_ACCOUNT,
            'EINTANG',
            $curHRAAmortisation,
            "HRA Amortisation reversal $this->yearString"
        );

        $this->addJournalRow(
            $this->journalData,
            'Cr',
            '6004',
            self::SMR_COST_CENTRE_4,
            '',
            $curHRAAmortisation,
            "HRA Amortisation reversal $this->yearString"
        );

        // Deduct to find remainder to reverse.
        $curTotalAmount = Math::subCurrency([$curTotalAmount, $curTotalAmortAmount]);

        /////// HRA reversal ///////
        // Total   Debit   to CAA
        // Reverse Credit  HRA "6000 HRA04"
        // Remove HRA Amortisation first
        $this->addJournalRow(
            $this->journalData,
            'Dr',
            $this->capitalAdjustmentAccount,
            self::BALANCE_SHEET_ACCOUNT,
            'EDEPHRA',
            Math::subCurrency([$curTotalHRAAmount, $curHRAAmortisation]),
            "HRA Depreciation $this->yearString"
        );

        $this->addJournalRow(
            $this->journalData,
            'Cr',
            self::SMR_ACCOUNT_HRA,
            self::SMR_COST_CENTRE_HRA_4,
            '',
            Math::subCurrency([$curTotalHRAAmount, $curHRAAmortisation]),
            "HRA Depreciation $this->yearString"
        );

        // HRA depreciation reversal.
        $this->addJournalRow(
            $this->journalData,
            'Dr',
            '9832',
            self::BALANCE_SHEET_ACCOUNT,
            'DB',
            Math::subCurrency([$curTotalHRAAmount, $curHRAAmortisation]),
            "HRA closedown"
        );

        $this->addJournalRow(
            $this->journalData,
            'Cr',
            '9830',
            self::BALANCE_SHEET_ACCOUNT,
            'INCC',
            Math::subCurrency([$curTotalHRAAmount, $curHRAAmortisation]),
            "Depn chg for HRA reversal"
        );

        // Deduct to find remainder to reverse
        $curTotalAmount = Math::subCurrency(
            [$curTotalAmount, Math::subCurrency([$curTotalHRAAmount, $curHRAAmortisation])]
        );

        ///////// GF reversal ///////////
        // Total Debit to CAA
        // Reverse through SMR
        $this->addJournalRow(
            $this->journalData,
            'Dr',
            $this->capitalAdjustmentAccount,
            self::BALANCE_SHEET_ACCOUNT,
            'EDEP',
            $curTotalAmount,
            "GF Depreciation $this->yearString"
        );

        $this->addJournalRow(
            $this->journalData,
            'Cr',
            '6000',
            self::SMR_COST_CENTRE_4,
            '',
            $curTotalAmount,
            "GF Depreciation $this->yearString"
        );

        // Total Debit to ?
        // Reverse through MIRS
        $this->addJournalRow(
            $this->journalData,
            'Dr',
            '3105',
            self::SMR_COST_CENTRE_4,
            '',
            $curTotalAmount,
            "GF depn recognition in the MIRS $this->yearString"
        );

        $this->addJournalRow(
            $this->journalData,
            'Cr',
            '9831',
            self::BALANCE_SHEET_ACCOUNT,
            'EXP',
            $curTotalAmount,
            "GF Depreciation $this->yearString"
        );
    }

    private function getAllAssetData()
    {
        return VwFar07::userSiteGroup()
        ->leftJoin(
            'ca_ifrs_category as bfwd_ca_ifrs_category',
            'bfwd_ca_ifrs_category.ca_ifrs_category_id',
            '=',
            'vw_far07.bfwd_ca_ifrs_category_id'
        )
        ->join('ca_fixed_asset', 'ca_fixed_asset.ca_fixed_asset_id', '=', 'vw_far07.ca_fixed_asset_id')
        ->join(
            'gen_userdef_group',
            'gen_userdef_group.gen_userdef_group_id',
            '=',
            'ca_fixed_asset.gen_userdef_group_id'
        )
        ->where('vw_far07.ca_balance_historic', 'N')
        ->select(
            [
                DB::raw('bfwd_ca_ifrs_category.ca_ifrs_category_desc AS bfwd_ca_ifrs_category_desc'),
                'vw_far07.hra_asset',
                'vw_far07.ca_fixed_asset_id',
                'vw_far07.deminimis_asset',
                DB::raw('gen_userdef_group.user_check2 AS rbc_deminimis'),
                DB::raw('vw_far07.`IFRS Code` as ca_ifrs_category_code'),
                DB::raw('vw_far07.`IFRS Description` as ca_ifrs_category_desc'),
                DB::raw('vw_far07.`Previous IFRS Code` as bf_ca_ifrs_category_code'),
                DB::raw('vw_far07.`Previous IFRS Description` as bf_ca_ifrs_category_desc'),
                DB::raw('vw_far07.`Asset Code` as fixed_asset_code'),
                DB::raw('vw_far07.`Asset Description` as fixed_asset_desc'),
                DB::raw('vw_far07.`Account Code` as account_code'),
                DB::raw('vw_far07.`CY Depreciation` * -1 as amount'),
                'bfwd_ca_ifrs_category.amortisation as bfwd_amortisation',
            ]
        )
        ->orderByRaw('gen_userdef_group.user_check2 DESC')
        ->orderByRaw('bfwd_ca_ifrs_category.ca_ifrs_category_desc')
        ->orderByRaw('vw_far07.`IFRS Code`')
        ->orderByRaw('vw_far07.`Account Code`')
        ->get();
    }
}
