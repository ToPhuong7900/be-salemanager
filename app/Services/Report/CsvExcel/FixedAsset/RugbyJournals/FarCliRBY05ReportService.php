<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\RugbyJournals;

use Illuminate\Support\Facades\DB;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Math;
use Tfcloud\Lib\FixedAsset\CaFinYearManager;
use Tfcloud\Models\CaTransactionType;
use Tfcloud\Models\Views\VwFar07;
use Tfcloud\Services\PermissionService;

class FarCliRBY05ReportService extends RugbyJournalPostingService
{
    protected $yearString = "";
    private $journalData = [];

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->addJournalHeader($this->journalData);
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        // Valuation Transaction Journal.

        $this->prepareRevaluationData();

        return $this->journalData;
    }

    private function prepareRevaluationData()
    {
        $sCostCentre = '';
        $sAccountCode1 = '';
        $sAccountCode2 = '';
        $sAccountCode3 = '';
        $sAccountCode4 = '';

        $bNoHistoricCost = false;

        $sIFRSSubType = '';
        $sIFRSCode = '';

        $curRevAmount = 0;
        $curRevAmountAsset = 0;
        $curRevAmountLand = 0;
        $curRevAmountHCAsset = 0;
        $curRevAmountHCLand = 0;

        $curBfwdDepn = 0;
        $curBfwdImp = 0;

        $curRRGain = 0;
        $curRRGainAsset = 0;
        $curRRGainLand = 0;

        $curIandECharge = 0;
        $curBalanceFixedAsset = 0;

        $curReverseDepnAdj = 0;
        $curROIReverseDepnAdj = 0;

        $bGroupHousingLines = true;

        $sMsgLine = '';
        $bHRAAsset = false;
        $lCount = 0;

        $caFixedAssetId = 0;

        $bHeritageAsset = false;

        $bHasDepnAdj = $this->hasCVDepnAdj();

        // Get additions data from database
        $assetData = $this->getAllAssetData();

        foreach ($assetData as $asset) {
            $lCount++;

            $bHasYearEndReval = false;   // Clear flags just to be safe

            $caFixedAssetId = $asset->ca_fixed_asset_id;

            $historicAssetData = $this->getHistoricAssetData($caFixedAssetId);

            $curRevAmountAsset = $asset->CREVAV;
            $curRevAmountLand = $asset->CREVLV;
            $curRevAmountHCAsset = $historicAssetData->HREVAV;
            $curRevAmountHCLand = $historicAssetData->HREVLV;

            // If we have a transfer is it year end?
            if ($asset->bf_ca_ifrs_category_desc != $asset->ca_ifrs_category_desc) {
                if (!common::valueYorNtoBoolean(\Auth::user()->siteGroup->ca_cal_mode_year_end)) {
                    $sIFRSSubType = $asset->ca_ifrs_category_desc;
                    $sIFRSCode = $asset->ca_ifrs_category_code;
                } else {
                    // Transferred after year start so reval in old category
                    $sIFRSSubType = $asset->bf_ca_ifrs_category_desc;
                    $sIFRSCode = $asset->bf_ca_ifrs_category_code;
                }
            } else {
                $sIFRSSubType = $asset->ca_ifrs_category_desc;
                $sIFRSCode = $asset->ca_ifrs_category_code;
            }

            // Get sub-type accounts.
            $this->getIFRSAccount(
                $sIFRSCode,
                $sAccountCode1,
                $sAccountCode2,
                $sAccountCode3,
                $sAccountCode4,
                $bNoHistoricCost
            );

            // Get cost centre for asset/valuation record
            $sCostCentre = $asset->account_code;

            // Keep track of balance to post to fixed asset
            $curBalanceFixedAsset = 0;

            $bHRAAsset = $this->isHRA($sIFRSSubType, $sCostCentre);
            $bHeritageAsset = $this->isHeritage($sIFRSSubType);

            /////// Accumulated Depreciation W/Off ///////

            // Get BFwd Accum. Depn.
            if ($bHasYearEndReval) {
                $curBfwdDepn = Math::addCurrency([$asset->COpenDPN, $asset->CCYDPN]);
            } else {
                $curBfwdDepn = $asset->COpenDPN;
            }

            if (Math::notEqualCurrency($curBfwdDepn, 0)) {
                // Write off Accum. Depn.
                if ($bHRAAsset && $bGroupHousingLines) {
                    $sMsgLine = "Write off Depreciation $sCostCentre";
                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        $sAccountCode2,
                        self::BALANCE_SHEET_ACCOUNT,
                        'EREV',
                        $curBfwdDepn,
                        $sMsgLine
                    );
                } else {
                    $sMsgLine = "Write off Depreciation $asset->ca_fixed_asset_code";
                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        $sAccountCode2,
                        self::BALANCE_SHEET_ACCOUNT,
                        'EREV',
                        $curBfwdDepn,
                        $sMsgLine
                    );
                }

                // Add to balance to post.
                $curBalanceFixedAsset = Math::addCurrency([$curBalanceFixedAsset, $curBfwdDepn]);
            }

            /////// Accumulated Impairment W/Off ///////
            // Get BFwd Accum. Impairment.
            if ($bHasYearEndReval) {
                $curBfwdImp = Math::addCurrency(
                    [
                        $asset->COpenIMPA,
                        $asset->COpenIMPL,
                        $asset->CIMPAV,
                        $asset->CIMPLV,
                        $asset->CROIAV,
                        $asset->CROILV
                    ]
                );
            } else {
                $curBfwdImp = Math::addCurrency(
                    [
                        $asset->COpenIMPA,
                        $asset->COpenIMPL
                    ]
                );
            }

            // If we have a Year-End revaluation then write of current year impairment as well
            // and if we have ROI we have to include that as well
            // Note negative signs on impairments!

            $curReverseDepnAdj = 0;

            if ($bHasYearEndReval) {
                if (
                    Math::notEqualCurrency($asset->CROILV, 0) ||
                    Math::notEqualCurrency($asset->CROILA, 0) ||
                    Math::notEqualCurrency($asset->CROLLV, 0) ||
                    Math::notEqualCurrency($asset->CROLLV, 0)
                ) {
                    if ($bHasDepnAdj) {
                        $curReverseDepnAdj = $this->getCVDepnAdj(
                            $caFixedAssetId,
                            [CaTransactionType::REVERSAL_OF_IMPAIRMENT, CaTransactionType::REVERSAL_OF_LOSS]
                        );
                    }
                }

                // Only impairments.
                if (
                    Math::notEqualCurrency($asset->CROILV, 0) ||
                    Math::notEqualCurrency($asset->CROILA, 0)
                ) {
                    if ($bHasDepnAdj) {
                        $curROIReverseDepnAdj = $this->getCVDepnAdj(
                            $caFixedAssetId,
                            [CaTransactionType::REVERSAL_OF_IMPAIRMENT]
                        );
                    }

                    // The impairment reversal values above are net of depn. so addd back on depn.
                    $curBfwdImp = Math::addCurrency([$curBfwdImp, $curROIReverseDepnAdj]);
                }

                // If we have a depn. adjustment in year then add that in.
                if (Math::notEqualCurrency($curReverseDepnAdj, 0)) {
                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        $sAccountCode2,
                        self::BALANCE_SHEET_ACCOUNT,
                        'EREV',
                        $curReverseDepnAdj,
                        "W/off depn. from RO+I/ROL $asset->ca_fixed_asset_code"
                    );

                    // As we write off more depreciation this impacts the other half of journal.
                    $curBalanceFixedAsset = Math::addCurrency([$curBalanceFixedAsset, $curReverseDepnAdj]);
                }
            }

            // Write off any Accum. Imp.
            if (Math::notEqualCurrency($curBfwdImp, 0)) {
                // Rugby put impairment on FA account (hence account1 not 3).
                if ($bHRAAsset && $bGroupHousingLines && $sAccountCode1 == '9000') {
                    $sMsgLine = "Write off Impairment $sCostCentre";
                } else {
                    $sMsgLine = "Write off Impairment $asset->ca_fixed_asset_code";
                }

                if ($bHRAAsset) {
                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        $sAccountCode1,
                        self::BALANCE_SHEET_ACCOUNT,
                        'EHRAREV',
                        $curBfwdImp,
                        $sMsgLine
                    );
                } else {
                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        $sAccountCode1,
                        self::BALANCE_SHEET_ACCOUNT,
                        '',
                        $curBfwdImp,
                        $sMsgLine
                    );
                }
                $curBalanceFixedAsset = Math::addCurrency([$curBalanceFixedAsset, $curBfwdImp]);
            }

            /////// Revaluation Reserve ///////

            // If Investment then no HC so no RR.
            if ($bNoHistoricCost) {
                $curRRGainAsset = 0;
                $curRRGainLand = 0;
            } else {
                // Work out any gain in RR.
                $curRRGainAsset = Math::subCurrency([$curRevAmountAsset, $curRevAmountHCAsset]);
                $curRRGainLand = Math::subCurrency([$curRevAmountLand, $curRevAmountHCLand]);
            }

            // DR any movement in RR.
            $curRRGain = Math::addCurrency([$curRRGainAsset, $curRRGainLand]);

            if (Math::notEqualCurrency($curRRGain, 0)) {
                if (Math::greaterThanCurrency($curRRGain, 0)) {
                    if (($bHRAAsset && $bGroupHousingLines) || $bHeritageAsset) {
                        $sMsgLine = "Gain in RR for $sCostCentre";
                    } else {
                        $sMsgLine = "Gain in RR for asset $asset->ca_fixed_asset_code";
                    }

                    if ($bHRAAsset) {
                        $this->addJournalRow(
                            $this->journalData,
                            'Cr',
                            $this->revalReserveAccount,
                            self::BALANCE_SHEET_ACCOUNT,
                            'IHRAREV',
                            $curRRGain,
                            $sMsgLine
                        );
                    } else {
                        $this->addJournalRow(
                            $this->journalData,
                            'Cr',
                            $this->revalReserveAccount,
                            self::BALANCE_SHEET_ACCOUNT,
                            'IGFREV',
                            $curRRGain,
                            $sMsgLine
                        );
                    }
                } else {
                    if (($bHRAAsset && $bGroupHousingLines) || $bHeritageAsset) {
                        $sMsgLine = "Loss in RR for $sCostCentre";
                        $this->addJournalRow(
                            $this->journalData,
                            'Dr',
                            $this->revalReserveAccount,
                            self::BALANCE_SHEET_ACCOUNT,
                            'IHRAREV',
                            Math::negateCurrency($curRRGain),
                            $sMsgLine
                        );
                    } else {
                        $sMsgLine = "Loss in RR for asset $asset->ca_fixed_asset_code";
                        $this->addJournalRow(
                            $this->journalData,
                            'Dr',
                            $this->revalReserveAccount,
                            self::BALANCE_SHEET_ACCOUNT,
                            'IGFREV',
                            Math::negateCurrency($curRRGain),
                            $sMsgLine
                        );
                    }
                }
                $curBalanceFixedAsset = Math::subCurrency([$curBalanceFixedAsset, $curRRGain]);
            }

            /////// Charge to I&E ///////

            // Investment class Assets gain/loss always to I&E
            if ($bNoHistoricCost) {
                $curIandECharge = Math::addCurrency([$curRevAmountAsset, $curRevAmountLand]);

                if (Math::notEqualCurrency($curIandECharge, 0)) {
                    if (Math::greaterThanCurrency($curIandECharge, 0)) {
                        $sMsgLine = "Gain to I&E on $asset->ca_fixed_assset_code";
                        $this->addJournalRow(
                            $this->journalData,
                            'Cr',
                            self::REVAL_LOSS_CODE_INVESTMENT,
                            self::SMR_COST_CENTRE_2,
                            '',
                            $curIandECharge,
                            $sMsgLine
                        );

                        $curBalanceFixedAsset = Math::subCurrency([$curBalanceFixedAsset, $curIandECharge]);

                        $this->addJournalRow(
                            $this->journalData,
                            'Cr',
                            $this->capitalAdjustmentAccount,
                            self::BALANCE_SHEET_ACCOUNT,
                            'IINVREV',
                            $curIandECharge,
                            $sMsgLine
                        );
                        $this->addJournalRow(
                            $this->journalData,
                            'Dr',
                            self::SMR_ACCOUNT_INVESTMENT,
                            self::SMR_COST_CENTRE_4,
                            '',
                            $curIandECharge,
                            $sMsgLine
                        );
                    } else {
                        $curIandECharge = Math::negateCurrency($curIandECharge);

                        $sMsgLine = "Loss to I&E on $asset->ca_fixed_assset_code";
                        $this->addJournalRow(
                            $this->journalData,
                            'Dr',
                            self::REVAL_LOSS_CODE_INVESTMENT,
                            self::SMR_COST_CENTRE_2,
                            '',
                            $curIandECharge,
                            $sMsgLine
                        );

                        $curBalanceFixedAsset = Math::addCurrency([$curBalanceFixedAsset, $curIandECharge]);

                        $this->addJournalRow(
                            $this->journalData,
                            'Dr',
                            $this->capitalAdjustmentAccount,
                            self::BALANCE_SHEET_ACCOUNT,
                            'EINVREV',
                            $curIandECharge,
                            $sMsgLine
                        );
                        $this->addJournalRow(
                            $this->journalData,
                            'Cr',
                            self::SMR_ACCOUNT_INVESTMENT,
                            self::SMR_COST_CENTRE_4,
                            '',
                            $curIandECharge,
                            $sMsgLine
                        );
                    }
                }
            } else {    // Non-investment assets //
                // DR any HC loss charge to I&E.
                $curIandECharge = Math::negateCurrency(Math::addCurrency([$curRevAmountHCAsset, $curRevAmountHCLand]));

                if (Math::notEqualCurrency($curIandECharge, 0)) {
                    if (($bHRAAsset && $bGroupHousingLines)) {
                        $sMsgLine = "Reval. loss charge I&E $sCostCentre";
                    } else {
                        $sMsgLine = "Reval. loss charge I&E $asset->ca_fixed_asset_code";
                    }

                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        self::REVAL_LOSS_CODE,
                        $sCostCentre,
                        '',
                        $curIandECharge,
                        $sMsgLine
                    );

                    $curBalanceFixedAsset = Math::addCurrency([$curBalanceFixedAsset, $curIandECharge]);

                    // Reverse through CAA to SMR.
                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        $this->capitalAdjustmentAccount,
                        self::BALANCE_SHEET_ACCOUNT,
                        'EIMP',
                        $curIandECharge,
                        $sMsgLine
                    );

                    // If HRA Council Dwellings use alternative coding.
                    if ($bHRAAsset) {
                        $this->addJournalRow(
                            $this->journalData,
                            'Cr',
                            '6002',
                            self::SMR_COST_CENTRE_HRA_4,
                            '',
                            $curIandECharge,
                            $sMsgLine
                        );
                        $this->addJournalRow(
                            $this->journalData,
                            'Dr',
                            '3108',
                            'HRA00',
                            '',
                            $curIandECharge,
                            $sMsgLine
                        );
                        $this->addJournalRow(
                            $this->journalData,
                            'Cr',
                            '9832',
                            self::BALANCE_SHEET_ACCOUNT,
                            'C10',
                            $curIandECharge,
                            $sMsgLine
                        );
                    } else {
                        $this->addJournalRow(
                            $this->journalData,
                            'Cr',
                            '6002',
                            self::SMR_COST_CENTRE_4,
                            '',
                            $curIandECharge,
                            $sMsgLine
                        );
                        $this->addJournalRow(
                            $this->journalData,
                            'Dr',
                            '3105',
                            self::SMR_COST_CENTRE_4,
                            '',
                            $curIandECharge,
                            $sMsgLine
                        );
                        $this->addJournalRow(
                            $this->journalData,
                            'Cr',
                            '9831',
                            self::BALANCE_SHEET_ACCOUNT,
                            'EXP',
                            $curIandECharge,
                            $sMsgLine
                        );
                    }
                }
            }

            /////// Post Balance to Fixed Asset ///////

            // Make grouping of council dwellings possible
            if (($bHRAAsset && $bGroupHousingLines && $sAccountCode1 == '9000')) {
                $sMsgLine = "Change to FA for $sCostCentre";
            } elseif ($bHeritageAsset) {
                $sMsgLine = "Change to FA for $sCostCentre";
            } else {
                $sMsgLine = "Change to FA for $asset->ca_fixed_asset_code";
            }

            // = ADepn + AImp + I&E - RR
            if (Math::lessThanCurrency($curBalanceFixedAsset, 0)) {
                $curBalanceFixedAsset = Math::negateCurrency($curBalanceFixedAsset);

                if ($bHRAAsset) {
                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        $sAccountCode1,
                        self::BALANCE_SHEET_ACCOUNT,
                        'EHRAREV',
                        $curBalanceFixedAsset,
                        $sMsgLine
                    );
                } else {
                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        $sAccountCode1,
                        self::BALANCE_SHEET_ACCOUNT,
                        'EREV',
                        $curBalanceFixedAsset,
                        $sMsgLine
                    );
                }
            } else {
                if ($bHRAAsset) {
                    $this->addJournalRow(
                        $this->journalData,
                        'Cr',
                        $sAccountCode1,
                        self::BALANCE_SHEET_ACCOUNT,
                        'EHRAREV',
                        $curBalanceFixedAsset,
                        $sMsgLine
                    );
                } else {
                    $this->addJournalRow(
                        $this->journalData,
                        'Cr',
                        $sAccountCode1,
                        self::BALANCE_SHEET_ACCOUNT,
                        'IGFREV',
                        $curBalanceFixedAsset,
                        $sMsgLine
                    );
                }
            }
        }   //foreach asset.

        // Group the council housing rows.
        $this->postGroupLikeJournalLines(
            $this->journalData,
            'Dr',
            '9000',
            'EHRAREV',
            self::BALANCE_SHEET_ACCOUNT,
            'Change to FA for COC00',
            22
        );
        $this->postGroupLikeJournalLines(
            $this->journalData,
            'Cr',
            '9000',
            'EHRAREV',
            self::BALANCE_SHEET_ACCOUNT,
            'Change to FA for COC00',
            22
        );
        $this->postGroupLikeJournalLines(
            $this->journalData,
            'Dr',
            $sAccountCode2,
            'EREV',
            self::BALANCE_SHEET_ACCOUNT,
            'Write off Depreciation COC00',
            28
        );
        $this->postGroupLikeJournalLines(
            $this->journalData,
            'Dr',
            '9000',
            'EHRAREV',
            self::BALANCE_SHEET_ACCOUNT,
            'Write off Impairment COC00',
            26
        );
        $this->postGroupLikeJournalLines(
            $this->journalData,
            'Cr',
            $this->revalReserveAccount,
            'IHRAREV',
            self::BALANCE_SHEET_ACCOUNT,
            'Gain in RR for asset COC00',
            26
        );

        if ($bGroupHousingLines) {
            $this->postGroupLikeJournalLines(
                $this->journalData,
                'Dr',
                '9800',
                'IHRAREV',
                self::BALANCE_SHEET_ACCOUNT,
                'Loss in RR for COC00',
                20
            );
            $this->postGroupLikeJournalLines(
                $this->journalData,
                'Cr',
                '3000',
                '',
                self::SMR_COST_CENTRE_HRA,
                'Reval. loss charge I&E COC00',
                28
            );
            $this->postGroupLikeJournalLines(
                $this->journalData,
                'Dr',
                '9810',
                'EIMP',
                self::BALANCE_SHEET_ACCOUNT,
                'Reval. loss charge I&E COC00',
                28
            );
            $this->postGroupLikeJournalLines(
                $this->journalData,
                'Dr',
                self::REVAL_LOSS_CODE,
                '',
                self::SMR_COST_CENTRE_HRA,
                'Reval. loss charge I&E COC00',
                28
            );
        }

        $this->postGroupLikeJournalLines(
            $this->journalData,
            'Cr',
            '9800',
            'IGFREV',
            self::BALANCE_SHEET_ACCOUNT,
            'Gain in RR for asset AGM00',
            26
        );
        $this->postGroupLikeJournalLines(
            $this->journalData,
            'Dr',
            '9800',
            'IGFREV',
            self::BALANCE_SHEET_ACCOUNT,
            'Loss in RR for AGM00',
            20
        );

        $this->postGroupLikeJournalLines(
            $this->journalData,
            'Dr',
            '9016',
            'EREV',
            self::BALANCE_SHEET_ACCOUNT,
            'Change to FA for AGM00',
            22
        );
        $this->postGroupLikeJournalLines(
            $this->journalData,
            'Dr',
            '9016',
            'IGFREV',
            self::BALANCE_SHEET_ACCOUNT,
            'Change to FA for AGM00',
            22
        );
    }

    private function getAllAssetData()
    {
        return VwFar07::userSiteGroup()
            ->leftJoin(
                'ca_ifrs_category as bfwd_ca_ifrs_category',
                'bfwd_ca_ifrs_category.ca_ifrs_category_id',
                '=',
                'vw_far07.bfwd_ca_ifrs_category_id'
            )
            ->where('vw_far07.ca_balance_historic', 'N')
            //->where('vw_far07.ca_fixed_asset_id', 41174)
            ->whereRaw('ca_fixed_asset_id IN (SELECT DISTINCT MYCAT.ca_fixed_asset_id '
                . 'FROM ca_transaction MYCAT WHERE MYCAT.ca_transaction_type_id = '
                . CaTransactionType::REVALUATION . ' AND MYCAT.ca_fin_year_id = '
                . CaFinYearManager::currentFinYearId() . ')')
            ->select(
                [
                    DB::raw('bfwd_ca_ifrs_category.ca_ifrs_category_desc AS bfwd_ca_ifrs_category_desc'),
                    'vw_far07.hra_asset',
                    'vw_far07.ca_fixed_asset_id',
                    'vw_far07.deminimis_asset',
                    DB::raw('vw_far07.`REV Asset Value` as CREVAV'),
                    DB::raw('vw_far07.`REV Land Value` as CREVLV'),
                    DB::raw('vw_far07.`Accumulated Depreciation` as COpenDPN'),
                    DB::raw('vw_far07.`CY Depreciation` as CDPN'),
                    DB::raw('vw_far07.`CFwd Accumulated Depreciation` as CCFwdDPN'),
                    DB::raw('vw_far07.`Accumulated Impairment Asset` as COpenIMPA'),
                    DB::raw('vw_far07.`Accumulated Impairment Land` as COpenIMPL'),
                    DB::raw('vw_far07.`IMP Land Value` as CIMPLV'),
                    DB::raw('vw_far07.`IMP Asset Value` as CIMPAV'),
                    DB::raw('vw_far07.`ROI Land Value` as CROILV'),
                    DB::raw('vw_far07.`ROI Asset Value` as CROIAV'),
                    DB::raw('vw_far07.`ROL Land Value` as CROLLV'),
                    DB::raw('vw_far07.`ROL Asset Value` as CROLAV'),
                    DB::raw('vw_far07.`IFRS Code` as ca_ifrs_category_code'),
                    DB::raw('vw_far07.`IFRS Description` as ca_ifrs_category_desc'),
                    DB::raw('vw_far07.`Previous IFRS Code` as bf_ca_ifrs_category_code'),
                    DB::raw('vw_far07.`Previous IFRS Description` as bf_ca_ifrs_category_desc'),
                    DB::raw('vw_far07.`Asset Code` as ca_fixed_asset_code'),
                    DB::raw('vw_far07.`Asset Description` as ca_fixed_asset_desc'),
                    DB::raw('vw_far07.`Account Code` as account_code'),
                    DB::raw('vw_far07.`CY Depreciation` as amount'),
                    'bfwd_ca_ifrs_category.amortisation as bfwd_amortisation',
                ]
            )
            ->orderBy(DB::raw('bfwd_ca_ifrs_category.ca_ifrs_category_desc'))
            ->orderBy(DB::raw('vw_far07.`Account Code`'))
            ->get();
    }
}
