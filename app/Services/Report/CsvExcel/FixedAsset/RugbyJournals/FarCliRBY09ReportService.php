<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\RugbyJournals;

use DateTime;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Math;
use Tfcloud\Models\CaTransactionType;
use Tfcloud\Services\PermissionService;

class FarCliRBY09ReportService extends RugbyJournalPostingService
{
    protected $yearString = "";
    private $journalData = [];

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->addJournalHeader($this->journalData);
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        // Asset by Asset Disposals

        $this->prepareDspData();

        return $this->journalData;
    }

    private function prepareDspData()
    {
        $sCostCentre = '';
        $sAccountCode1 = '';
        $sAccountCode2 = '';
        $sAccountCode3 = '';
        $sAccountCode4 = '';

        $sIFRSSubType = '';

        $curAssetValue = 0;
        $curLandValue = 0;
        $curHCAssetValue = 0;
        $curHCLandValue = 0;
        $curValue = 0;
        $curHCValue = 0;
        $curRRValue = 0;

        $curGrossValue = 0;
        $curBFwdGBV = 0;
        $curProceeds = 0;
        $curCosts = 0;

        $curBFwdImp = 0;
        $curBFwdDepn = 0;
        $curBFwdRR = 0;

        $curCYDepn = 0;
        $curCYHCDepn = 0;

        $sMsg = '';
        $bHraAsset = false;
        $lCount = 0;
        $caFixedAssetId = 0;
        $sClass = '';

        // Get Disposals data from database
        $assetData = $this->retrieveTransactionList(CaTransactionType::DISPOSAL)
            ->orderBy('ca_fixed_asset_id')
            ->get();

        foreach ($assetData as $asset) {
            $lCount++;

            // Store ValuationKey for this transaction
            $caFixedAssetId = $asset->ca_fixed_asset_id;

            $sIFRSSubType = $asset->ca_ifrs_category_desc;

            $sCostCentre = $asset->ca_account_code;

            // Get sub-type accounts.
            $this->getIFRSAccount(
                $asset->ca_ifrs_category_code,
                $sAccountCode1,
                $sAccountCode2,
                $sAccountCode3,
                $sAccountCode4
            );

            $bHraAsset = $this->isHRA($sIFRSSubType, $sCostCentre);

            // Get values and reverse -ves to +ves for posting.
            $curAssetValue = Math::negateCurrency($asset->cv_asset);
            $curLandValue = Math::negateCurrency($asset->cv_land);
            $curValue = Math::addCurrency([$curAssetValue, $curLandValue]);

            $curHCAssetValue = Math::negateCurrency($asset->hc_asset);
            $curHCLandValue = Math::negateCurrency($asset->hc_land);
            $curHCValue = Math::addCurrency([$curHCAssetValue, $curHCLandValue]);

            $curBFwdDepn = 0;
            $curBFwdImp = 0;
            $curCYDepn = 0;
            $curCYHCDepn = 0;

            $sClass = "";

            $curProceeds = Math::subCurrency([$asset->disposal_proceeds, $asset->disposal_costs]);
            $curCosts = $asset->disposal_costs;

            /////// Put in token lines for cash received ///////
            // Sales Proceeds;
            // Dr Cash/Debtors
            // Cr I&E OOE
            if (Math::notEqualCurrency($curProceeds, 0)) {
                $sMsg = "Cash/Debtors disposal proceeds $asset->ca_fixed_asset_code";
                $this->addJournalRow(
                    $this->journalData,
                    'Dr',
                    '9820',
                    self::BALANCE_SHEET_ACCOUNT,
                    $sClass,
                    $curProceeds,
                    $sMsg
                );

                $sMsg = "Cr I&E (OOE) disposal $asset->ca_fixed_asset_code";
                if ($bHraAsset) {
                    $this->addJournalRow(
                        $this->journalData,
                        'Cr',
                        self::DISPOSALS_SALES_LOSS_CODE,
                        'HRA01',
                        '',
                        $curProceeds,
                        $sMsg
                    );
                } else {
                    $this->addJournalRow(
                        $this->journalData,
                        'Cr',
                        self::DISPOSALS_SALES_LOSS_CODE,
                        self::SMR_COST_CENTRE_1,
                        '',
                        $curProceeds,
                        $sMsg
                    );
                }

                $sMsg = "Disposal proceeds reversal I&E $asset->ca_fixed_asset_code";
                if ($bHraAsset) {
                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        '6200',
                        self::SMR_COST_CENTRE_HRA_4,
                        '',
                        $curProceeds,
                        $sMsg
                    );
                } else {
                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        '6200',
                        self::SMR_COST_CENTRE_4,
                        '',
                        $curProceeds,
                        $sMsg
                    );
                }

                $this->addJournalRow(
                    $this->journalData,
                    'Cr',
                    '9820',
                    self::BALANCE_SHEET_ACCOUNT,
                    '',
                    $curProceeds,
                    $sMsg
                );

                // Added sales proceeds to CRR 12-FEB-2018.
                $this->addJournalRow(
                    $this->journalData,
                    'Dr',
                    '3109',
                    'CRR00',
                    '',
                    $curProceeds,
                    $sMsg
                );
                $this->addJournalRow(
                    $this->journalData,
                    'Cr',
                    '6200',
                    'CRR00',
                    '',
                    $curProceeds,
                    $sMsg
                );
            }

            /////// Put in token lines for cost of disposal ///////
            // Disposal Costs;
            // Cr Cash/Creditors
            // Dr C I&E - OOE
            if (Math::notEqualCurrency($curCosts, 0)) {
                $sMsg = "Cash/Creditors disposal costs $asset->ca_fixed_asset_code";
                $this->addJournalRow(
                    $this->journalData,
                    'Cr',
                    '9820',
                    self::BALANCE_SHEET_ACCOUNT,
                    $sClass,
                    $curCosts,
                    $sMsg
                );

                $sMsg = "Dr I&E disposal costs $asset->ca_fixed_asset_code";
                if ($bHraAsset) {
                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        self::DISPOSALS_SALES_LOSS_CODE,
                        'HRA01',
                        '',
                        $curCosts,
                        $sMsg
                    );
                } else {
                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        self::DISPOSALS_SALES_LOSS_CODE,
                        self::SMR_COST_CENTRE_1,
                        '',
                        $curCosts,
                        $sMsg
                    );
                }
            }

            /////// Write out the carrying amount of the Fixed Asset  ///////
            $this->getBFwdAccumulatedBalances(
                $caFixedAssetId,
                $curBFwdGBV,
                $curBFwdDepn,
                $curBFwdImp,
                $curCYDepn,
                $curCYHCDepn,
                $curBFwdRR
            );

            // Start current value
            $curGrossValue = $curValue;

            $curCYDepn = Math::negateCurrency($curCYDepn);
            if (Math::notEqualCurrency($curCYDepn, 0)) {
                $sMsg = "Disposal CY Depn. W/O $asset->ca_fixed_asset_code";
                $this->addJournalRow(
                    $this->journalData,
                    'Dr',
                    $sAccountCode2,
                    self::BALANCE_SHEET_ACCOUNT,
                    'EDEPN',
                    $curCYDepn,
                    $sMsg
                );
                $curGrossValue = Math::addCurrency([$curGrossValue, $curCYDepn]);
            }
            //}

            // If asset has reval in year then that will clear bfwd imp/depn
            // otherwise we need to clear balances here.
            // Write out depreciation balances.
            if (!$this->hasReval($asset->ca_fixed_asset_id)) {
                if (Math::notEqualCurrency($curBFwdDepn, 0)) {
                    $sMsg = "Disposal Accum. Depn. W/O $asset->ca_fixed_asset_code";
                    if ($sAccountCode2 == '9011') {
                        $this->addJournalRow(
                            $this->journalData,
                            'Dr',
                            $sAccountCode2,
                            self::BALANCE_SHEET_ACCOUNT,
                            'EAMORT',
                            $curBFwdDepn,
                            $sMsg
                        );
                    } else {
                        $this->addJournalRow(
                            $this->journalData,
                            'Dr',
                            $sAccountCode2,
                            self::BALANCE_SHEET_ACCOUNT,
                            'EDEPN',
                            $curBFwdDepn,
                            $sMsg
                        );
                    }
                }

                if (Math::notEqualCurrency($curBFwdImp, 0)) {
                    $sMsg = "Disposal Accum. Imp. W/O $asset->ca_fixed_asset_code";
                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        $sAccountCode1,
                        self::BALANCE_SHEET_ACCOUNT,
                        'IALIEN',
                        $curBFwdImp,
                        $sMsg
                    );
                }

                $curGrossValue = Math::addCurrency([$curGrossValue, $curBFwdDepn, $curBFwdImp]);
            }

            // Dr Fixed Carrying Amount (amount of loss reversed Gross)
            // Add in amount of depn and impairment to get back to gross amount
            // Cr carrying amount.
            $sMsg = "Disposal Fixed Asset $asset->ca_fixed_asset_code";
            $this->addJournalRow(
                $this->journalData,
                'Cr',
                $sAccountCode1,
                self::BALANCE_SHEET_ACCOUNT,
                'IALIEN',
                $curGrossValue,
                $sMsg
            );

            // DR I&E
            $sMsg = "$sCostCentre/$sIFRSSubType Disposal (I&E) $this->yearString";
            if ($bHraAsset) {
                $this->addJournalRow(
                    $this->journalData,
                    'Dr',
                    self::DISPOSALS_SALES_LOSS_CODE,
                    'HRA01',
                    '',
                    $curValue,
                    $sMsg
                );
            } else {
                $this->addJournalRow(
                    $this->journalData,
                    'Dr',
                    self::DISPOSALS_SALES_LOSS_CODE,
                    self::SMR_COST_CENTRE_1,
                    '',
                    $curValue,
                    $sMsg
                );
            }

            // Reverse charge to I&E out from GF to CAA
            $sMsg = "Reversal of disposal NBV I&E $asset->ca_fixed_asset_code";
            if ($bHraAsset) {
                $this->addJournalRow(
                    $this->journalData,
                    'Cr',
                    '6012',
                    self::SMR_COST_CENTRE_HRA_4,
                    '',
                    $curValue,
                    $sMsg
                );
                $this->addJournalRow(
                    $this->journalData,
                    'Dr',
                    $this->capitalAdjustmentAccount,
                    self::BALANCE_SHEET_ACCOUNT,
                    'EHRAD',
                    $curValue,
                    $sMsg
                );
            } else {
                $this->addJournalRow(
                    $this->journalData,
                    'Cr',
                    '6012',
                    self::SMR_COST_CENTRE_HRA_4,
                    '',
                    $curValue,
                    $sMsg
                );
                $this->addJournalRow(
                    $this->journalData,
                    'Dr',
                    $this->capitalAdjustmentAccount,
                    self::BALANCE_SHEET_ACCOUNT,
                    'EGFD',
                    $curValue,
                    $sMsg
                );
            }

            /////// Clear out Revaluation Reserve ///////
            // Dr RR
            // Cr CAA
            $curRRValue = Math::subCurrency([$curValue, $curHCValue]);

            if (Math::notEqualCurrency($curRRValue, 0)) {
                // Clear RR.
                $sMsg = "$sCostCentre/$sIFRSSubType Disposal (RR) $this->yearString";
                if ($bHraAsset) {
                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        $this->revalReserveAccount,
                        self::BALANCE_SHEET_ACCOUNT,
                        'EHRAVA',
                        $curRRValue,
                        $sMsg
                    );
                } else {
                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        $this->revalReserveAccount,
                        self::BALANCE_SHEET_ACCOUNT,
                        'EGFVA',
                        $curRRValue,
                        $sMsg
                    );
                }
            }

            // Clear RR.
            $sMsg = "$sCostCentre/$sIFRSSubType Disposal (RR) $this->yearString";
            $this->addJournalRow(
                $this->journalData,
                'Cr',
                $this->capitalAdjustmentAccount,
                self::BALANCE_SHEET_ACCOUNT,
                'EADJ',
                $curRRValue,
                $sMsg
            );
        }   //foreach asset.
    }
}
