<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\RugbyJournals;

use Tfcloud\Lib\Math;
use Tfcloud\Models\CaTransactionType;
use Tfcloud\Services\PermissionService;

class FarCliRBY04ReportService extends RugbyJournalPostingService
{
    protected $yearString = "";
    private $journalData = [];

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->addJournalHeader($this->journalData);
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        // Impairments for all assets.

        $this->prepareImpairmentData();

        return $this->journalData;
    }

    private function prepareImpairmentData()
    {
        $sCostCentre = '';
        $sAccountCode1 = '';
        $sAccountCode2 = '';
        $sAccountCode3 = '';
        $sAccountCode4 = '';

        $sIFRSSubType = '';

        $curAssetValue = 0;
        $curLandValue = 0;
        $curHCAssetValue = 0;
        $curHCLandValue = 0;
        $curValue = 0;
        $curHCValue = 0;

        $sMsg = " Impairment $this->yearString";
        $lCount = 0;

        // Get additions data from database
        $assetData = $this->retrieveTransactionList(CaTransactionType::IMPAIRMENT)->get();

        foreach ($assetData as $asset) {
            $lCount++;

            $sIFRSSubType = $asset->ca_ifrs_category_desc;

            $sCostCentre = $asset->ca_account_code;

            // Get sub-type accounts.
            $this->getIFRSAccount(
                $asset->ca_ifrs_category_code,
                $sAccountCode1,
                $sAccountCode2,
                $sAccountCode3,
                $sAccountCode4
            );

            // Get values (already positive no need to negate).
            $curAssetValue = $asset->cv_asset;
            $curLandValue = $asset->cv_land;
            $curHCAssetValue = $asset->hc_asset;
            $curHCLandValue = $asset->hc_land;

            $curValue = Math::addCurrency([$curAssetValue, $curLandValue]);
            $curHCValue = Math::addCurrency([$curHCAssetValue, $curHCLandValue]);

            // Is this an HRA asset?
            $bHRA_Asset = $this->isHRA($sIFRSSubType, $sCostCentre);

            // Credit Accumlated Impairment.
            $this->addJournalRow(
                $this->journalData,
                'Cr',
                $sAccountCode1,
                self::BALANCE_SHEET_ACCOUNT,
                'IIMPSERV',
                $curValue,
                "$sCostCentre/$sIFRSSubType$sMsg"
            );

            // RR value = CV - HC values
            $curRRValue = Math::subCurrency([$curValue, $curHCValue]);

            // Debit Reval Reserve (if any debited)
            if (Math::greaterThanCurrency($curRRValue, 0)) {
                if ($bHRA_Asset) {
                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        $this->revalReserveAccount,
                        self::BALANCE_SHEET_ACCOUNT,
                        'IHRAREV',
                        $curRRValue,
                        "$sCostCentre/$sIFRSSubType$sMsg"
                    );
                } else {
                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        $this->revalReserveAccount,
                        self::BALANCE_SHEET_ACCOUNT,
                        'EGFVA',
                        $curRRValue,
                        "$sCostCentre/$sIFRSSubType$sMsg"
                    );
                }
            }


            // Debit I&E if more than RR.
            if (Math::greaterThanCurrency($curHCValue, 0)) {
                $this->addJournalRow(
                    $this->journalData,
                    'Dr',
                    self::REVAL_LOSS_CODE,
                    $sCostCentre,
                    '',
                    $curHCValue,
                    "$sCostCentre/$sIFRSSubType$sMsg"
                );

                // Debit CAA.
                $this->addJournalRow(
                    $this->journalData,
                    'Dr',
                    $this->capitalAdjustmentAccount,
                    self::BALANCE_SHEET_ACCOUNT,
                    'EIMP',
                    $curHCValue,
                    "$sCostCentre/$sIFRSSubType$sMsg"
                );

                if ($bHRA_Asset) {
                    // Credit HRA
                    $this->addJournalRow(
                        $this->journalData,
                        'Cr',
                        '3000', //self::SMR_ACCOUNT_HRA,
                        'HRA99', //self::SMR_COST_CENTRE_HRA,
                        '',
                        $curHCValue,
                        "$sCostCentre/$sIFRSSubType$sMsg"
                    );
                } else {
                    // Credit SMR
                    $this->addJournalRow(
                        $this->journalData,
                        'Cr',
                        self::SMR,
                        self::SMR_COST_CENTRE,
                        '',
                        $curHCValue,
                        "$sCostCentre/$sIFRSSubType$sMsg"
                    );
                }
            }

            // Reversals for HRA.
            if (Math::greaterThanCurrency($curHCValue, 0)) {
                if ($bHRA_Asset) {
                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        '3100',
                        self::SMR_COST_CENTRE_HRA_0,
                        '',
                        $curHCValue,
                        "CIES exceptional item"
                    );
                    $this->addJournalRow(
                        $this->journalData,
                        'Cr',
                        '3108',
                        self::SMR_COST_CENTRE_HRA_0,
                        '',
                        $curHCValue,
                        "CIES exceptional item"
                    );
                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        '9832',
                        self::BALANCE_SHEET_ACCOUNT,
                        'D8R',
                        $curHCValue,
                        "tfr imp via MIRS"
                    );
                    $this->addJournalRow(
                        $this->journalData,
                        'Cr',
                        '6001',
                        self::SMR_COST_CENTRE_HRA_4,
                        '',
                        $curHCValue,
                        "tfr imp via MIRS"
                    );
                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        '3105',
                        self::SMR_COST_CENTRE_HRA_4,
                        '',
                        $curHCValue,
                        "imp reversal to 9832 via HRA04"
                    );
                    $this->addJournalRow(
                        $this->journalData,
                        'Cr',
                        '3105',
                        self::SMR_COST_CENTRE_HRA_0,
                        '',
                        $curHCValue,
                        "imp reversal to 9832 via HRA04"
                    );
                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        '3108',
                        self::SMR_COST_CENTRE_HRA_0,
                        '',
                        $curHCValue,
                        "imp reversal to 9832 via HRA04"
                    );
                    $this->addJournalRow(
                        $this->journalData,
                        'Cr',
                        '9832',
                        self::BALANCE_SHEET_ACCOUNT,
                        'C10',
                        $curHCValue,
                        "imp reversal to 9832 via HRA04"
                    );
                }
            }
        }   //foreach asset.

        // Group all the 9060 lines for housing.
        $this->postGroupLikeJournalLines(
            $this->journalData,
            'Cr',
            '9000',
            'IIMPSERV',
            self::BALANCE_SHEET_ACCOUNT,
            'COC00/Council Dwellings Impairment 20',
            34
        );

        // Group all the 0902 lines for housing.
        $this->postGroupLikeJournalLines(
            $this->journalData,
            'Dr',
            self::REVAL_LOSS_CODE,
            '',
            self::SMR_COST_CENTRE_HRA,
            'COC00/Council Dwellings Impairment 20',
            34
        );

        $this->postGroupLikeJournalLines(
            $this->journalData,
            'Dr',
            self::REVAL_LOSS_CODE,
            '',
            self::SMR_COST_CENTRE_HRA_1,
            "COC01/Council Dwellings Impairment {$this->yearString}",
            34
        );

        // Group all the CAA 9810 lines for housing.
        $this->postGroupLikeJournalLines(
            $this->journalData,
            'Dr',
            '9810',
            'EIMP',
            self::BALANCE_SHEET_ACCOUNT,
            'COC00/Council Dwellings Impairment 20',
            34
        );

        $this->postGroupLikeJournalLines(
            $this->journalData,
            'Dr',
            '9810',
            'EIMP',
            self::BALANCE_SHEET_ACCOUNT,
            'COC01/Council Dwellings Impairment 20',
            34
        );
        // Group all the SMR HRA 3000 lines for housing
        $this->postGroupLikeJournalLines(
            $this->journalData,
            'Cr',
            '3000', //self::SMR_ACCOUNT_HRA,
            '',
            'HRA99', //self::SMR_COST_CENTRE_HRA,
            'COC00/Council Dwellings Impairment 20',
            34
        );

        $this->postGroupLikeJournalLines(
            $this->journalData,
            'Cr',
            self::SMR_ACCOUNT_HRA,
            '',
            self::SMR_COST_CENTRE_HRA_1,
            'COC01/Council Dwellings Impairment 20',
            34
        );
    }
}
