<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\RugbyJournals;

use DateTime;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Math;
use Tfcloud\Models\CaTransactionType;
use Tfcloud\Services\PermissionService;

class FarCliRBY10ReportService extends RugbyJournalPostingService
{
    protected $yearString = "";
    private $journalData = [];

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->addJournalHeader($this->journalData);
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        // Asset by Asset Derecognitions.

        $this->prepareDrcData();

        return $this->journalData;
    }

    private function prepareDrcData()
    {
        $sCostCentre = '';
        $sAccountCode1 = '';
        $sAccountCode2 = '';
        $sAccountCode3 = '';
        $sAccountCode4 = '';

        $sIFRSSubType = '';

        $curAssetValue = 0;
        $curLandValue = 0;
        $curHCAssetValue = 0;
        $curHCLandValue = 0;
        $curValue = 0;
        $curHCValue = 0;
        $curRRValue = 0;

        $curGrossValue = 0;
        $sMsg = '';
        $bHraAsset = false;
        $lCount = 0;

        // Get Disposals data from database
        $assetData = $this->retrieveTransactionList(CaTransactionType::DERECOGNITION)
            ->get();

        foreach ($assetData as $asset) {
            $lCount++;

            $sIFRSSubType = $asset->ca_ifrs_category_desc;

            $sCostCentre = $asset->ca_account_code;

            // Get sub-type accounts.
            $this->getIFRSAccount(
                $asset->ca_ifrs_category_code,
                $sAccountCode1,
                $sAccountCode2,
                $sAccountCode3,
                $sAccountCode4
            );

            $bHraAsset = $this->isHRA($sIFRSSubType, $sCostCentre);

            // Get values and reverse -ves to +ves for posting.
            $curAssetValue = Math::negateCurrency($asset->cv_asset);
            $curLandValue = Math::negateCurrency($asset->cv_land);
            $curValue = Math::addCurrency([$curAssetValue, $curLandValue]);

            $curHCAssetValue = Math::negateCurrency($asset->hc_asset);
            $curHCLandValue = Math::negateCurrency($asset->hc_land);
            $curHCValue = Math::addCurrency([$curHCAssetValue, $curHCLandValue]);

            // Start current value
            $curGrossValue = $curValue;

            // Dr Fixed Carrying Amount (amount of loss reversed Gross)
            // Add in amount of depn and impairment to get back to gross amount
            // Cr carrying amount
            // Use cost centre on housing assets so they can be grouped later if needed

            if ($bHraAsset) {
                $sMsg = "Derecognition Fixed Asset $sCostCentre";
                $this->addJournalRow(
                    $this->journalData,
                    'Cr',
                    $sAccountCode1,
                    self::BALANCE_SHEET_ACCOUNT,
                    'IHRAREV',
                    $curGrossValue,
                    $sMsg
                );

                // DR I&E.
                $sMsg = "$sCostCentre/$sIFRSSubType Derec (I&E) $this->yearString";
                $this->addJournalRow(
                    $this->journalData,
                    'Dr',
                    self::DISPOSALS_SALES_LOSS_CODE,
                    'HRA01',
                    '',
                    $curGrossValue,
                    $sMsg
                );
            } else {
                $sMsg = "Derecognition Fixed Asset $asset->ca_fixed_asset_code";
                $this->addJournalRow(
                    $this->journalData,
                    'Cr',
                    $sAccountCode1,
                    self::BALANCE_SHEET_ACCOUNT,
                    'IALIEN',
                    $curGrossValue,
                    $sMsg
                );

                // DR I&E.
                $sMsg = "Derecognition Fixed Asset $asset->ca_fixed_asset_code";
                $this->addJournalRow(
                    $this->journalData,
                    'Dr',
                    self::DISPOSALS_SALES_LOSS_CODE,
                    self::SMR_COST_CENTRE,
                    '',
                    $curGrossValue,
                    $sMsg
                );
            }

            // Reverse charge to I&E out from GF to CAA
            if ($bHraAsset) {
                $sMsg = "Reversal of derecognition NBV I&E $sCostCentre";
                $this->addJournalRow(
                    $this->journalData,
                    'Cr',
                    '6012',
                    'HRA04',
                    '',
                    $curGrossValue,
                    $sMsg
                );

                $this->addJournalRow(
                    $this->journalData,
                    'Dr',
                    $this->capitalAdjustmentAccount,
                    self::BALANCE_SHEET_ACCOUNT,
                    'EHRAD',
                    $curGrossValue,
                    $sMsg
                );
            } else {
                $sMsg = "Reversal of derecognition NBV I&E $asset->ca_fixed_asset_code";
                $this->addJournalRow(
                    $this->journalData,
                    'Cr',
                    '6012',
                    self::SMR_COST_CENTRE,
                    '',
                    $curGrossValue,
                    $sMsg
                );

                $this->addJournalRow(
                    $this->journalData,
                    'Dr',
                    $this->capitalAdjustmentAccount,
                    self::BALANCE_SHEET_ACCOUNT,
                    'EGFD',
                    $curGrossValue,
                    $sMsg
                );
            }

            /////// Clear out Revaluation Reserve ///////
            // Dr RR
            // Cr CAA
            $curRRValue = Math::subCurrency([$curValue, $curHCValue]);

            if (Math::notEqualCurrency($curRRValue, 0)) {
                // Clear RR.
                $sMsg = "$sCostCentre/$sIFRSSubType Disposal (RR) $this->yearString";
                $this->addJournalRow(
                    $this->journalData,
                    'Dr',
                    $this->revalReserveAccount,
                    self::BALANCE_SHEET_ACCOUNT,
                    'EGFVA',
                    $curRRValue,
                    $sMsg
                );
                $sMsg = "$sCostCentre/$sIFRSSubType Disposal (RR) $this->yearString";
                $this->addJournalRow(
                    $this->journalData,
                    'Cr',
                    $this->capitalAdjustmentAccount,
                    self::BALANCE_SHEET_ACCOUNT,
                    'EADJ',
                    $curRRValue,
                    $sMsg
                );
            }
        }   //foreach asset.

        $this->PostGroupLikeJournalLines(
            $this->journalData,
            "Cr",
            "9000",
            "IHRAREV",
            self::BALANCE_SHEET_ACCOUNT,
            "Derecognition Fixed Asset COC00",
            31
        );
        $this->PostGroupLikeJournalLines(
            $this->journalData,
            "Dr",
            "2003",
            "",
            "HRA01",
            "COC00/Council Dwellings Derec (I&E) $this->yearString",
            45
        );
        $this->PostGroupLikeJournalLines(
            $this->journalData,
            "Cr",
            "6012",
            "",
            "HRA04",
            "Reversal of derecognition NBV I&E " . self::SMR_COST_CENTRE_HRA,
            39
        );
        $this->PostGroupLikeJournalLines(
            $this->journalData,
            "Dr",
            "9810",
            "EHRAD",
            self::BALANCE_SHEET_ACCOUNT,
            "Reversal of derecognition NBV I&E " . self::SMR_COST_CENTRE_HRA,
            39
        );
    }
}
