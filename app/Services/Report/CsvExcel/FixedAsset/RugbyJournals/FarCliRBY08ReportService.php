<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\RugbyJournals;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Math;
use Tfcloud\Models\CaTransactionType;
use Tfcloud\Services\PermissionService;

class FarCliRBY08ReportService extends RugbyJournalPostingService
{
    protected $yearString = "";
    private $journalData = [];

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->addJournalHeader($this->journalData);
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        // Asset by Asset Reversal of Impairment.

        $this->prepareROIData();

        return $this->journalData;
    }

    private function prepareROIData()
    {
        $sCostCentre = '';
        $sAccountCode1 = '';
        $sAccountCode2 = '';
        $sAccountCode3 = '';
        $sAccountCode4 = '';

        $sIFRSSubType = '';

        $curAssetValue = 0;
        $curLandValue = 0;
        $curHCAssetValue = 0;
        $curHCLandValue = 0;
        $curValue = 0;
        $curHCValue = 0;
        $curRRValue = 0;

        $curIandECharge = 0;
        $curCAAValue = 0;

        $curDepnAdj = 0;

        $bHraAsset = false;
        $lCount = 0;

        $bCheckHCDepnAdj = $this->hasHCDepnAdj();

        // Get ROIs data from database
        $assetData = $this->retrieveTransactionList(CaTransactionType::REVERSAL_OF_IMPAIRMENT)
            ->get();

        foreach ($assetData as $asset) {
            $lCount++;

            // Store ValuationKey for this transaction
            $caFixedAssetId = $asset->ca_fixed_asset_id;

            $sIFRSSubType = $asset->ca_ifrs_category_desc;

            $sCostCentre = $asset->ca_account_code;

            // Get sub-type accounts.
            $this->getIFRSAccount(
                $asset->ca_ifrs_category_code,
                $sAccountCode1,
                $sAccountCode2,
                $sAccountCode3,
                $sAccountCode4
            );

            $bHraAsset = $this->isHRA($sIFRSSubType, $sCostCentre);

            // Get values and reverse -ves to +ves for posting.
            $curAssetValue = $asset->cv_asset;
            $curLandValue = $asset->cv_land;
            $curValue = Math::addCurrency([$curAssetValue, $curLandValue]);
            $curHCAssetValue = $asset->hc_asset;
            $curHCLandValue = $asset->hc_land;
            $curHCValue = Math::addCurrency([$curHCAssetValue, $curHCLandValue]);

            // The depreciation reversed.
            if ($bCheckHCDepnAdj) {
                $curDepnAdj = $asset->hc_depn_adjustment;
            }

            /////// Reverse out the Impairment - Accum Imp to I&E ///////


            // Dr Fixed Carrying Amount (amount of loss reversed Gross).
            if (Math::notEqualCurrency($curValue, 0)) {
                if ($bHraAsset) {
                    $sMsg = "RoI (Accum. Imp.) $sCostCentre $this->yearString";
                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        $sAccountCode1,
                        self::BALANCE_SHEET_ACCOUNT,
                        'EHRAREV',
                        $curValue,
                        $sMsg
                    );
                } else {
                    $sMsg = "RoI (Accum. Imp.) $asset->ca_fixed_asset_code $this->yearString";
                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        $sAccountCode1,
                        self::BALANCE_SHEET_ACCOUNT,
                        'EIMPREV',
                        $curValue,
                        $sMsg
                    );
                }
            }

            // Cr I&E with value of total gain (less depn. done below).
            $curIandECharge = $curHCValue;
            if (Math::notEqualCurrency($curIandECharge, 0)) {
                if ($bHraAsset) {
                    $sMsg = "RoI (I&E) $sCostCentre $this->yearString";
                } else {
                    $sMsg = "RoI (I&E) $asset->ca_fixed_asset_id $this->yearString";
                }

                $this->addJournalRow(
                    $this->journalData,
                    'Cr',
                    self::REVAL_LOSS_CODE,
                    $sCostCentre,
                    '',
                    $curIandECharge,
                    $sMsg
                );
            }

            /////// Reverse out total via RR to change revaluation transaction errors which picks up total ///////
            $curRRValue = Math::subCurrency([$curValue, $curHCValue]);


            // RR with value of reversal.
            if (Math::notEqualCurrency($curRRValue, 0)) {
                // Cr Accumulated Depn. for Category.
                $sMsg = "RoI (RR Adj.) $asset->ca_fixed_asset_code $this->yearString";

                $this->addJournalRow(
                    $this->journalData,
                    'Cr',
                    $this->revalReserveAccount,
                    self::BALANCE_SHEET_ACCOUNT,
                    '',
                    $curRRValue,
                    $sMsg
                );
            }

            /////// Depreciation Adjustment ///////

            if (Math::notEqualCurrency($curDepnAdj, 0)) {
                // Cr Accumulated Depn.
                $sMsg = "$sCostCentre/$sIFRSSubType RoI Depn. Adj. $this->yearString";

                $this->addJournalRow(
                    $this->journalData,
                    'Cr',
                    $sAccountCode2,
                    self::BALANCE_SHEET_ACCOUNT,
                    'EREV',
                    $curDepnAdj,
                    $sMsg
                );

                // Dr I&E DEPRECIATION with value of depn. adj.
                $sMsg = "$sCostCentre/$sIFRSSubType RoI Depn. (I&E) $this->yearString";

                $this->addJournalRow(
                    $this->journalData,
                    'Dr',
                    self::DEPRECIATION_CODE,
                    $sCostCentre,
                    '',
                    $curDepnAdj,
                    $sMsg
                );
            }

            /////// Reverse out I&E to the CAA/SMR ///////
            $curCAAValue = Math::subCurrency([$curIandECharge, $curDepnAdj]);


            if (Math::notEqualCurrency($curCAAValue, 0)) {
                // Credit CAA
                $sMsg = "$sCostCentre/$sIFRSSubType RoI - CAA $this->yearString";

                $this->addJournalRow(
                    $this->journalData,
                    'Cr',
                    $this->capitalAdjustmentAccount,
                    self::BALANCE_SHEET_ACCOUNT,
                    'EIMP',
                    $curCAAValue,
                    $sMsg
                );

                // Debit SMR (reverse of CAA above)
                $sMsg = "$sCostCentre/$sIFRSSubType RoI - SMR $this->yearString";


                if ($bHraAsset) {
                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        '6001',
                        self::SMR_COST_CENTRE_HRA_4,
                        '',
                        $curCAAValue,
                        $sMsg
                    );
                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        '9832',
                        self::BALANCE_SHEET_ACCOUNT,
                        'C10',
                        $curCAAValue,
                        $sMsg
                    );
                    $this->addJournalRow(
                        $this->journalData,
                        'Cr',
                        '3108',
                        'HRA00',
                        '',
                        $curCAAValue,
                        $sMsg
                    );
                } else {
                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        $this->sMRAccount,
                        self::SMR_COST_CENTRE,
                        '',
                        $curCAAValue,
                        $sMsg
                    );
                }
            }
        }   //foreach asset.

        // Group the council housing rows for CAA/SMR reversal
        $sMsg = "RoI (Accum. Imp.) COC00 $this->yearString";
        $this->postGroupLikeJournalLines(
            $this->journalData,
            'Dr',
            '9000',
            'EHRAREV',
            self::BALANCE_SHEET_ACCOUNT,
            $sMsg,
            strlen($sMsg)
        );

        $sMsg = "RoI (I&E) COC00 $this->yearString";
        $this->postGroupLikeJournalLines(
            $this->journalData,
            'Cr',
            self::REVAL_LOSS_CODE,
            '',
            self::SMR_COST_CENTRE_HRA,
            $sMsg,
            strlen($sMsg)
        );
        $this->postGroupLikeJournalLines(
            $this->journalData,
            'Cr',
            self::REVAL_LOSS_CODE,
            '',
            self::SMR_COST_CENTRE_HRA_1,
            $sMsg,
            strlen($sMsg)
        );

        $this->postGroupLikeJournalLines(
            $this->journalData,
            'Cr',
            '9810',
            'EIMP',
            self::BALANCE_SHEET_ACCOUNT,
            "COC00/Council Dwellings RoI - CAA",
            33
        );
        $this->postGroupLikeJournalLines(
            $this->journalData,
            'Dr',
            '6001',
            '',
            'HRA04',
            'COC00/Council Dwellings RoI - SMR',
            33
        );

        $this->postGroupLikeJournalLines(
            $this->journalData,
            'Dr',
            '9832',
            'C10',
            self::BALANCE_SHEET_ACCOUNT,
            "COC00/Council Dwellings RoI",
            27
        );
        $this->postGroupLikeJournalLines(
            $this->journalData,
            'Cr',
            '3108',
            '',
            'HRA00',
            'COC00/Council Dwellings RoI',
            27
        );
    }
}
