<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\RugbyJournals;

use Tfcloud\Lib\Math;
use Tfcloud\Models\CaTransactionType;
use Tfcloud\Services\PermissionService;

class FarCliRBY03ReportService extends RugbyJournalPostingService
{
    protected $yearString = "";
    private $journalData = [];

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->addJournalHeader($this->journalData);
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        // Asset by Asset Acquisitions.

        $this->prepareAcquisitionData();

        return $this->journalData;
    }

    private function prepareAcquisitionData()
    {
        $sCostCentre = '';
        $sAccountCode1 = '';
        $sAccountCode2 = '';
        $sAccountCode3 = '';
        $sAccountCode4 = '';

        $sIFRSSubType = '';

        $curAssetValue = 0;
        $curLandValue = 0;

        $curValue = 0;

        $lCount = 0;

        // Get additions data from database
        $assetData = $this->retrieveTransactionList(CaTransactionType::ACQUISITION)->get();

        foreach ($assetData as $asset) {
            $lCount++;

            $sIFRSSubType = $asset->ca_ifrs_category_desc;

            $sCostCentre = $asset->ca_account_code;

            // Get sub-type accounts.
            $this->getIFRSAccount(
                $asset->ca_ifrs_category_code,
                $sAccountCode1,
                $sAccountCode2,
                $sAccountCode3,
                $sAccountCode4
            );

            $curAssetValue = $asset->cv_asset;
            $curLandValue = $asset->cv_land;

            $curValue = Math::addCurrency([$curAssetValue, $curLandValue]);

            // If a credit (-ve) then swap sign and swap Cr/Dr indicators.
            if (Math::lessThanCurrency($curValue, 0)) {
                $this->addJournalRow(
                    $this->journalData,
                    'Cr',
                    $sAccountCode1,
                    self::BALANCE_SHEET_ACCOUNT,
                    'EXP',
                    Math::negateCurrency($curValue),
                    "Acquisition to $sIFRSSubType $this->yearString"
                );

                $this->addJournalRow(
                    $this->journalData,
                    'Dr',
                    self::CASH_CREDITOR,
                    self::SMR_COST_CENTRE,
                    '',
                    Math::negateCurrency($curValue),
                    "Acquisition to $sIFRSSubType $this->yearString"
                );
            } else {
                // Normal positive addition postings.
                $this->addJournalRow(
                    $this->journalData,
                    'Dr',
                    $sAccountCode1,
                    self::BALANCE_SHEET_ACCOUNT,
                    'EXP',
                    $curValue,
                    "Acquisition to $sIFRSSubType $this->yearString"
                );

                $this->addJournalRow(
                    $this->journalData,
                    'Cr',
                    self::CASH_CREDITOR,
                    self::SMR_COST_CENTRE,
                    '',
                    $curValue,
                    "Acquisition to $sIFRSSubType $this->yearString"
                );
            }
        }   //foreach asset.
    }
}
