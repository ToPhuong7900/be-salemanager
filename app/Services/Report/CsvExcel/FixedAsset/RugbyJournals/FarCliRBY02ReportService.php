<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\RugbyJournals;

use Tfcloud\Lib\Math;
use Tfcloud\Models\CaBalance;
use Tfcloud\Models\CaTransactionType;
use Tfcloud\Models\Views\VwFar07;
use Tfcloud\Services\PermissionService;

class FarCliRBY02ReportService extends RugbyJournalPostingService
{
    protected $yearString = "";
    private $journalData = [];

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->addJournalHeader($this->journalData);
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        // Asset by Asset Additions.

        $this->prepareAdditionData();

        return $this->journalData;
    }

    private function prepareAdditionData()
    {
        $sCostCentre = '';
        $sAccountCode1 = '';
        $sAccountCode2 = '';
        $sAccountCode3 = '';
        $sAccountCode4 = '';

        $sIFRSSubType = '';

        $curAssetValue = 0;
        $curLandValue = 0;

        $curValue = 0;

        $lCount = 0;

        // Get additions data from database
        $assetData = $this->retrieveTransactionList(CaTransactionType::ADDITION)->get();

        foreach ($assetData as $asset) {
            $lCount++;

            $sIFRSSubType = $asset->ca_ifrs_category_desc;

            $sCostCentre = $asset->ca_account_code;

            // Get sub-type accounts.
            $this->getIFRSAccount(
                $asset->ca_ifrs_category_code,
                $sAccountCode1,
                $sAccountCode2,
                $sAccountCode3,
                $sAccountCode4
            );

            $curAssetValue = $asset->cv_asset;
            $curLandValue = $asset->cv_land;

            $curValue = Math::addCurrency([$curAssetValue, $curLandValue]);

            // If a credit (-ve) then swap sign and swap Cr/Dr indicators.
            if (Math::lessThanCurrency($curValue, 0)) {
                $this->addJournalRow(
                    $this->journalData,
                    'Cr',
                    $sAccountCode1,
                    self::BALANCE_SHEET_ACCOUNT,
                    'EXP',
                    Math::negateCurrency($curValue),
                    "Additions to $sIFRSSubType $this->yearString"
                );

                $this->addJournalRow(
                    $this->journalData,
                    'Dr',
                    self::CASH_CREDITOR,
                    self::SMR_COST_CENTRE,
                    '',
                    Math::negateCurrency($curValue),
                    "Additions to $sIFRSSubType $this->yearString"
                );
            } else {
                // Normal positive addition postings.
                $this->addJournalRow(
                    $this->journalData,
                    'Dr',
                    $sAccountCode1,
                    self::BALANCE_SHEET_ACCOUNT,
                    'EXP',
                    $curValue,
                    "Additions to $sIFRSSubType $this->yearString"
                );

                $this->addJournalRow(
                    $this->journalData,
                    'Cr',
                    self::CASH_CREDITOR,
                    self::SMR_COST_CENTRE,
                    '',
                    $curValue,
                    "Additions to $sIFRSSubType $this->yearString"
                );
            }
        }   //foreach asset.

        // Group the council housing additions into one liners
        $this->postGroupLikeJournalLines(
            $this->journalData,
            'Dr',
            '9000',
            'EXP',
            self::BALANCE_SHEET_ACCOUNT,
            'Additions to Council Dwellings',
            30
        );
        $this->postGroupLikeJournalLines(
            $this->journalData,
            'Cr',
            self::CASH_CREDITOR,
            '',
            self::SMR_COST_CENTRE,
            'Additions to Council Dwellings',
            30
        );
    }
}
