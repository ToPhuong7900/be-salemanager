<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\RugbyJournals;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Math;
use Tfcloud\Models\CaTransactionType;
use Tfcloud\Services\PermissionService;

class FarCliRBY06ReportService extends RugbyJournalPostingService
{
    protected $yearString = "";
    private $journalData = [];

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->addJournalHeader($this->journalData);
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        // Asset by Asset Transfers.

        $this->prepareTransferData();

        return $this->journalData;
    }

    private function prepareTransferData()
    {
        $sCostCentre = '';
        $sAccountCode1 = '';
        $sAccountCode2 = '';
        $sAccountCode3 = '';
        $sAccountCode4 = '';


        $sIFRSSubType = '';

        $curAssetValue = 0;
        $curLandValue = 0;
        $curValue = 0;

        // BFwd Accum Balances
        $curBfwdGBV = 0;
        $curBFwdDepn = 0;
        $curBFwdIMP = 0;
        $curBFwdRR = 0;

        // Current year depreciation
        $curCYDepn = 0;
        $curHCCYDepn = 0;


        $sMsg = '';
        $lCount = 0;
        $caFixedAssetId = 0;

        $bIsYearEnd = Common::valueYorNtoBoolean(\Auth::user()->siteGroup->ca_cal_mode_year_end);

        // Get additions data from database
        $assetData = $this->retrieveTransactionList(CaTransactionType::CATEGORY_TRANSFER)
            ->orderByRaw('ca_transaction.ca_ifrs_cat_id, ca_transaction_code')
            ->get();

        foreach ($assetData as $asset) {
            $lCount++;

            // Store ValuationKey for this transaction
            $caFixedAssetId = $asset->ca_fixed_asset_id;

            $sIFRSSubType = $asset->ca_ifrs_category_desc;

            $sCostCentre = $asset->ca_account_code;

            // Get sub-type accounts.
            $this->getIFRSAccount(
                $asset->ca_ifrs_category_code,
                $sAccountCode1,
                $sAccountCode2,
                $sAccountCode3,
                $sAccountCode4
            );

            // Get values and reverse -ves to +ves for posting.
            $curAssetValue = $asset->cv_asset;
            $curLandValue = $asset->cv_land;
            $curValue = Math::addCurrency([$curAssetValue, $curLandValue]);

            $sMsg = "$sIFRSSubType $this->yearString : $asset->ca_fixed_asset_code";

            // Get the BFwd GBV, BFwd Accum Impairment and Depreciation - so we can move the gross balances.
            $this->getBFwdAccumulatedBalances(
                $caFixedAssetId,
                $curBfwdGBV,
                $curBFwdDepn,
                $curBFwdIMP,
                $curCYDepn,
                $curHCCYDepn,
                $curBFwdRR
            );

            if ($bIsYearEnd) {
                $curBFwdDepn = Math::addCurrency([$curBFwdDepn, $curCYDepn]);
            }

            if (Math::greaterThanCurrency($curValue, 0)) {
                $sMsg = "Tfr in to $sMsg";
                $this->addJournalRow(
                    $this->journalData,
                    'Dr',
                    $sAccountCode1,
                    self::BALANCE_SHEET_ACCOUNT,
                    'ETFR',
                    $curBfwdGBV,
                    $sMsg
                );

                if (Math::notEqualCurrency($curBFwdDepn, 0)) {
                    $this->addJournalRow(
                        $this->journalData,
                        'Cr',
                        $sAccountCode2,
                        self::BALANCE_SHEET_ACCOUNT,
                        'ITFR',
                        $curBFwdDepn,
                        $sMsg
                    );
                }
                if (Math::notEqualCurrency($curBFwdIMP, 0)) {
                    $this->addJournalRow(
                        $this->journalData,
                        'Cr',
                        $sAccountCode1,
                        self::BALANCE_SHEET_ACCOUNT,
                        'ITFR',
                        $curBFwdIMP,
                        $sMsg
                    );
                }
            } else {
                $sMsg = "Tfr out to $sMsg";
                $this->addJournalRow(
                    $this->journalData,
                    'Cr',
                    $sAccountCode1,
                    self::BALANCE_SHEET_ACCOUNT,
                    'ITFR',
                    $curBfwdGBV,
                    $sMsg
                );

                if (Math::notEqualCurrency($curBFwdDepn, 0)) {
                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        $sAccountCode2,
                        self::BALANCE_SHEET_ACCOUNT,
                        'ETFR',
                        $curBFwdDepn,
                        $sMsg
                    );
                }
                if (Math::notEqualCurrency($curBFwdIMP, 0)) {
                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        $sAccountCode1,
                        self::BALANCE_SHEET_ACCOUNT,
                        'ETFR',
                        $curBFwdIMP,
                        $sMsg
                    );
                }
            }
        }   //foreach asset.
    }
}
