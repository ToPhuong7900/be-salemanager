<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\RugbyJournals;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Math;
use Tfcloud\Models\CaTransactionType;
use Tfcloud\Services\PermissionService;

class FarCliRBY07ReportService extends RugbyJournalPostingService
{
    protected $yearString = "";
    private $journalData = [];

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->addJournalHeader($this->journalData);
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        // Asset by Asset Reversal of Loss.

        $this->prepareROLData();

        return $this->journalData;
    }

    private function prepareROLData()
    {
        $sCostCentre = '';
        $sAccountCode1 = '';
        $sAccountCode2 = '';
        $sAccountCode3 = '';
        $sAccountCode4 = '';

        $sIFRSSubType = '';

        $curAssetValue = 0;
        $curLandValue = 0;
        $curHCAssetValue = 0;
        $curHCLandValue = 0;
        $curValue = 0;
        $curHCValue = 0;
        $curRRValue = 0;

        $curIandECharge = 0;
        $curCAAValue = 0;

        $curDepnAdj = 0;
        $curHCDepnAdj = 0;
        $curRRDepnAdj = 0;

        $sMsg = '';
        $bHRA_Asset = false;
        $lCount = 0;
        $caFixedAssetId = 0;

        $bIsYearEnd = Common::valueYorNtoBoolean(\Auth::user()->siteGroup->ca_cal_mode_year_end);

        // Get ROLS data from database
        $assetData = $this->retrieveTransactionList(CaTransactionType::REVERSAL_OF_LOSS)
            ->get();

        foreach ($assetData as $asset) {
            $lCount++;

            // Store ValuationKey for this transaction
            $caFixedAssetId = $asset->ca_fixed_asset_id;

            $sIFRSSubType = $asset->ca_ifrs_category_desc;

            $sCostCentre = $asset->ca_account_code;

            // Get sub-type accounts.
            $this->getIFRSAccount(
                $asset->ca_ifrs_category_code,
                $sAccountCode1,
                $sAccountCode2,
                $sAccountCode3,
                $sAccountCode4
            );

            $bHRA_Asset = $this->isHRA($sIFRSSubType, $sCostCentre);

            // Get values and reverse -ves to +ves for posting.
            $curAssetValue = $asset->cv_asset;
            $curLandValue = $asset->cv_land;
            $curValue = Math::addCurrency([$curAssetValue, $curLandValue]);
            $curHCAssetValue = $asset->hc_asset;
            $curHCLandValue = $asset->hc_land;
            $curHCValue = Math::addCurrency([$curHCAssetValue, $curHCLandValue]);

            // The depreciation reversed.
            $curHCDepnAdj = $asset->hc_depn_adjustment;

            if (Math::equalCurrency($curHCDepnAdj, 0)) {
                $curDepnAdj = 0;
                $curRRDepnAdj = 0;
            } else {
                $curDepnAdj = $asset->cv_depn_adjustment;
                $curRRDepnAdj = Math::subCurrency([$curDepnAdj, $curHCDepnAdj]);
            }

            /////// Reverse out the Loss - FA to I&E ///////
            $curIandECharge = 0;

            $sMsg = "RoL Fixed Assset $asset->ca_fixed_asset_code";

            // Dr Fixed Carrying Amount (amount of loss reversed Gross).
            if (Math::notEqualCurrency($curValue, 0)) {
                if ($bHRA_Asset) {
                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        $sAccountCode1,
                        self::BALANCE_SHEET_ACCOUNT,
                        'EHRAREV',
                        $curValue,
                        $sMsg
                    );
                } else {
                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        $sAccountCode1,
                        self::BALANCE_SHEET_ACCOUNT,
                        'EIMPREV',
                        $curValue,
                        $sMsg
                    );
                }

                // Cr I&E with value of total gain (less depn. done below).
                $curIandECharge = $curHCValue;
                if (Math::notEqualCurrency($curIandECharge, 0)) {
                    $sMsg = "$sCostCentre/$sIFRSSubType RoL (I&E) $this->yearString";

                    if ($bHRA_Asset) {
                        $this->addJournalRow(
                            $this->journalData,
                            'Cr',
                            '0902',
                            self::SMR_COST_CENTRE_HRA,
                            '',
                            $curIandECharge,
                            $sMsg
                        );
                    } else {
                        $this->addJournalRow(
                            $this->journalData,
                            'Cr',
                            self::REVAL_LOSS_CODE,
                            $sCostCentre,
                            '',
                            $curIandECharge,
                            $sMsg
                        );
                    }

                    // Cr Reverse out total via RR if from RR.
                    $curRRValue = Math::subCurrency([$curValue, $curHCValue]);
                    if (Math::notEqualCurrency($curRRValue, 0)) {
                        $sMsg = "RoL (RR) $asset->ca_fixed_asset_code";
                        $this->addJournalRow(
                            $this->journalData,
                            'Cr',
                            $this->revalReserveAccount,
                            self::SMR_COST_CENTRE,
                            '',
                            $curRRValue,
                            $sMsg
                        );
                    }
                }
            }

            /////// Depreciation Adjustment ///////

            // Restore any Depreciation charged to I&E.
            if (Math::notEqualCurrency($curDepnAdj, 0)) {
                // Cr Accumulated Depn. for Category.
                $sMsg = "$sCostCentre/$sIFRSSubType RoL Depn. Adj. $this->yearString";

                $this->addJournalRow(
                    $this->journalData,
                    'Cr',
                    $sAccountCode2,
                    self::BALANCE_SHEET_ACCOUNT,
                    'EREV',
                    $curDepnAdj,
                    $sMsg
                );

                // Dr Service I&E DEPRECIATION with value of depn. adj.
                $sMsg = "$sCostCentre/$sIFRSSubType RoL Depn. (I&E) $this->yearString";

                $this->addJournalRow(
                    $this->journalData,
                    'Dr',
                    self::REVAL_LOSS_CODE,
                    $sCostCentre,
                    '',
                    $curDepnAdj,
                    $sMsg
                );
            }

            // Reverse out Depn. Adj. via RR if from RR.
            if (Math::notEqualCurrency($curRRDepnAdj, 0)) {
                // Cr Accumulated Depn. for Category.
                $sMsg = "$sCostCentre/$sIFRSSubType RoL Depn. Adj. $this->yearString";

                $this->addJournalRow(
                    $this->journalData,
                    'Cr',
                    $this->capitalAdjustmentAccount,
                    self::BALANCE_SHEET_ACCOUNT,
                    'IIMP',
                    $curRRDepnAdj,
                    $sMsg
                );

                // Adjust for depreciation in prior years - RR.
                $sMsg = "$sCostCentre/$sIFRSSubType RR Depn. (I&E) $this->yearString";

                $this->addJournalRow(
                    $this->journalData,
                    'Dr',
                    $this->revalReserveAccount,
                    $sCostCentre,
                    '',
                    $curRRDepnAdj,
                    $sMsg
                );
            }

            /////// Reverse out any I&E charge to the CAA/SMR ///////
            $curCAAValue = Math::subCurrency([$curIandECharge, $curDepnAdj]);
            $sMsg = " RoL Asset $asset->ca_fixed_asset_code";

            if (Math::greaterThanCurrency($curCAAValue, 0)) {
                if ($bHRA_Asset) {
                    // Credit CAA (9810) - gain - depn. adj.
                    $this->addJournalRow(
                        $this->journalData,
                        'Cr',
                        $this->capitalAdjustmentAccount,
                        self::BALANCE_SHEET_ACCOUNT,
                        'EIMP',
                        $curCAAValue,
                        "$sCostCentre/$sIFRSSubType$sMsg"
                    );
                } else {
                    // Credit CAA - gain - depn. adj.
                    $this->addJournalRow(
                        $this->journalData,
                        'Cr',
                        $this->capitalAdjustmentAccount,
                        self::BALANCE_SHEET_ACCOUNT,
                        'IIMP',
                        $curCAAValue,
                        "$sCostCentre/$sIFRSSubType$sMsg"
                    );
                }

                // Debit SMR (reverse of CAA above).
                if ($bHRA_Asset) {
                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        '6002',
                        self::SMR_COST_CENTRE_HRA_4,
                        '',
                        $curCAAValue,
                        "$sCostCentre/$sIFRSSubType$sMsg"
                    );
                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        '9832',
                        self::BALANCE_SHEET_ACCOUNT,
                        'C10',
                        $curCAAValue,
                        "$sCostCentre/$sIFRSSubType$sMsg"
                    );
                    $this->addJournalRow(
                        $this->journalData,
                        'Cr',
                        '3108',
                        'HRA00',
                        '',
                        $curCAAValue,
                        "$sCostCentre/$sIFRSSubType$sMsg"
                    );
                } else {
                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        '6002',
                        self::SMR_COST_CENTRE_4,
                        '',
                        $curCAAValue,
                        "$sCostCentre/$sIFRSSubType$sMsg"
                    );
                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        '9831',
                        self::BALANCE_SHEET_ACCOUNT,
                        'EXP',
                        $curCAAValue,
                        "$sCostCentre/$sIFRSSubType$sMsg"
                    );
                    $this->addJournalRow(
                        $this->journalData,
                        'Cr',
                        '3105',
                        self::SMR_COST_CENTRE_4,
                        '',
                        $curCAAValue,
                        "$sCostCentre/$sIFRSSubType$sMsg"
                    );
                }
            }

            if (Math::lessThanCurrency($curCAAValue, 0)) {
                $curCAAValue = Math::negateCurrency($curCAAValue);

                // Debit CAA - gain - depn. adj.
                if ($bHRA_Asset) {
                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        $this->capitalAdjustmentAccount,
                        self::BALANCE_SHEET_ACCOUNT,
                        'EIMP',
                        $curCAAValue,
                        "$sCostCentre/$sIFRSSubType$sMsg"
                    );
                } else {
                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        $this->capitalAdjustmentAccount,
                        self::BALANCE_SHEET_ACCOUNT,
                        'IIMP',
                        $curCAAValue,
                        "$sCostCentre/$sIFRSSubType$sMsg"
                    );
                }

                // Credit SMR (reverse of CAA above)
                if ($bHRA_Asset) {
                    $this->addJournalRow(
                        $this->journalData,
                        'Cr',
                        self::SMR_COST_CENTRE_HRA,
                        self::SMR_COST_CENTRE_HRA,
                        '',
                        $curCAAValue,
                        "$sCostCentre/$sIFRSSubType$sMsg"
                    );
                    $this->addJournalRow(
                        $this->journalData,
                        'Cr',
                        '9832',
                        self::BALANCE_SHEET_ACCOUNT,
                        'C10',
                        $curCAAValue,
                        "$sCostCentre/$sIFRSSubType$sMsg"
                    );
                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        '3108',
                        self::SMR_COST_CENTRE_HRA,
                        '',
                        $curCAAValue,
                        "$sCostCentre/$sIFRSSubType$sMsg"
                    );
                } else {
                    $this->addJournalRow(
                        $this->journalData,
                        'Cr',
                        $this->sMRAccount,
                        self::SMR_COST_CENTRE_HRA_4,
                        '',
                        $curCAAValue,
                        "$sCostCentre/$sIFRSSubType$sMsg"
                    );
                    $this->addJournalRow(
                        $this->journalData,
                        'Cr',
                        '9831',
                        self::BALANCE_SHEET_ACCOUNT,
                        'EXP',
                        $curCAAValue,
                        "$sCostCentre/$sIFRSSubType$sMsg"
                    );
                    $this->addJournalRow(
                        $this->journalData,
                        'Dr',
                        '3105',
                        self::SMR_COST_CENTRE_4,
                        '',
                        $curCAAValue,
                        "$sCostCentre/$sIFRSSubType$sMsg"
                    );
                }
            }
        }   //foreach asset.

        return;
    }
}
