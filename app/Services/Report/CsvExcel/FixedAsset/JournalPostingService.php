<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Math;
use Tfcloud\Models\CaPostingAccount;
use Tfcloud\Models\SiteGroup;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\CsvExcel\CsvExcelReportBaseService;

abstract class JournalPostingService extends CsvExcelReportBaseService
{
    protected const ENTITY_CODE = "01";

    protected $caPostingAccount = null;

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->postingAcc = $this->getPostingAccount();
    }

    protected function newRptLine(&$outputData, &$outputRow)
    {
        $outputData[] = $outputRow;
        $outputRow = [];
    }

    protected function getPostingAccount()
    {
        return CaPostingAccount::where('site_group_id', SiteGroup::get()->site_group_id)->first();
    }

    protected function writeCodeSection(&$outputRow, $costCentre, $subjective, $project, $fund)
    {
        $outputRow['Entity'] = self::ENTITY_CODE;
        $outputRow['Cost Centre'] = $costCentre;
        $outputRow['Subjective'] =  $subjective;
        $outputRow['Project'] = $project;
        $outputRow['Fund'] = $fund;
    }

    protected function writeDebitBySign(&$outputRow, $curGross, $text, $comments = "")
    {
        if ($curGross < 0) {
            $this->writeCredit($outputRow, Math::negateCurrency($curGross), $text, $comments);
        } else {
            $this->writeDebit($outputRow, $curGross, $text, $comments);
        }
    }

    protected function writeCreditBySign(&$outputRow, $curGross, $text, $comments = "")
    {
        if ($curGross < 0) {
            $this->writeDebit($outputRow, Math::negateCurrency($curGross), $text, $comments);
        } else {
            $this->writeCredit($outputRow, $curGross, $text, $comments);
        }
    }

    protected function writeDebit(&$outputRow, $curGross, $text, $comments = "")
    {
        $outputRow['Debit'] = Common::numberFormat($curGross, false, '');
        $outputRow['Credit'] = '';
        $outputRow['Line Description'] = $text;
        $outputRow['Comments'] = $comments;
    }

    protected function writeCredit(&$outputRow, $curGross, $text, $comments = "")
    {
        $outputRow['Debit'] = '';
        $outputRow['Credit'] = Common::numberFormat($curGross, false, '');
        $outputRow['Line Description'] = $text;
        $outputRow['Comments'] = $comments;
    }
}
