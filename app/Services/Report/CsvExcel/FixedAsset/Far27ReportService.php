<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset;

use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Math;
use Tfcloud\Models\Report;
use Tfcloud\Models\SiteGroup;
use Tfcloud\Models\Views\VwFar10;
use Tfcloud\Services\PermissionService;

class Far27ReportService extends JournalPostingService
{
    protected $entity = false;

    private $reportBoxFilterLoader = null;

    public function __construct(PermissionService $permissionService, $filterData, Report $report = null)
    {
        parent::__construct($permissionService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;

        $this->setFilterProperties($filterData);
    }

    private function setFilterProperties($filterData)
    {
        $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($filterData);
        $entity = $reportFilterQuery->getFirstValueFilterField('ca_entity_id');

        // get filter values
        $this->entity = (!$entity) ? false : $entity;
    }

    public function getReportData(&$filterText, &$sOrderText)
    {
        // If no admin ca_posting_account record available, exit.
        if (!$this->postingAcc) {
            return [];
        }

        $far10 = $this->getData();

        $outputData = [];

        $curTotalCAAReversal = 0;
        $curTotalMIRSReversal = 0;
        $curTotalHRAMIRSReversal = 0;

        foreach ($far10 as $row) {
            $outputRow = [];

            $commonGood = $row->common_good === 'Y';
            $hraAsset = $row->hra_asset === 'Y';

            // Gross Move to Fixed Asset Balance Sheet
            $curGross = Math::addCurrency([$row->asset_loss_reversal, $row->land_loss_reversal]);
            if (Math::notEqualCurrency($curGross, 0)) {
                if ($commonGood) {
                    $this->writeCodeSection(
                        $outputRow,
                        $row->account_code,
                        $row->account_code_1,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_blank
                    );
                } elseif ($hraAsset) {
                    $this->writeCodeSection(
                        $outputRow,
                        $this->postingAcc->acc_balance_sheet,
                        $row->account_code_1,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_hra_code
                    );
                } else {
                    $this->writeCodeSection(
                        $outputRow,
                        $this->postingAcc->acc_balance_sheet,
                        $row->account_code_1,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_gf_code
                    );
                }
                $text = "{$row->fixed_asset_code} Reversal of Loss ({$row->ca_ifrs_category_code})";
                $this->writeDebit($outputRow, $curGross, $text);
                $this->newRptLine($outputData, $outputRow);
            }

            // Cr to Charge I&E
            $curSD = $curGross;
            if (Math::greaterThanCurrency($curSD, 0)) {
                // Write out the charge
                if ($hraAsset) {
                    $this->writeCodeSection(
                        $outputRow,
                        $row->account_code,
                        $this->postingAcc->sub_revaluation_loss,
                        $this->postingAcc->prj_hra_mirs,
                        $this->postingAcc->fund_hra_ie_sd
                    );
                } else {
                    $this->writeCodeSection(
                        $outputRow,
                        $row->account_code,
                        $this->postingAcc->sub_revaluation_loss,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_blank
                    );
                }
                $text = "{$row->fixed_asset_code} Surplus/Deficit Gain";
                $this->writeCredit($outputRow, $curSD, $text);
                $this->newRptLine($outputData, $outputRow);

                if ($commonGood) {
                    /**
                     * ////////////////////////////////////////////////////////
                     * // Write out total reversal to MIRS (Common Good)
                     * ////////////////////////////////////////////////////////
                     */

                    // Cr MIRS (for Common Good)
                    $this->writeCodeSection(
                        $outputRow,
                        $row->account_code,
                        $this->postingAcc->sub_revaluation_loss_reversal,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_blank
                    );
                    $this->writeDebit($outputRow, $curSD, "I&E Gains and Losses (Common Good)", "MIRS");
                    $this->newRptLine($outputData, $outputRow);

                    // Dr CAA line (for Common Good)
                    $this->writeCodeSection(
                        $outputRow,
                        $row->account_code,
                        $this->postingAcc->sub_capital_adjustment_acc,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_blank
                    );
                    $this->writeCredit($outputRow, $curSD, "Asset Revaluation Losses (Common Good)", "CAA");
                    $this->newRptLine($outputData, $outputRow);
                } else {
                    // Reversal to MIRS (Cr)
                    if ($hraAsset) {
                        $curTotalHRAMIRSReversal = Math::addCurrency([$curTotalHRAMIRSReversal, $curSD]);
                    } else {
                        $curTotalMIRSReversal = Math::addCurrency([$curTotalMIRSReversal, $curSD]);
                    }

                    // Opposite to CAA (Dr)
                    $curTotalCAAReversal = Math::addCurrency([$curTotalCAAReversal, $curSD]);
                }
            }
        }

        // Write out total reversal to Dr MIRS
        $this->writeCodeSection(
            $outputRow,
            $this->postingAcc->acc_mirs,
            $this->postingAcc->sub_revaluation_loss_reversal,
            $this->postingAcc->prj_blank_project,
            $this->postingAcc->fund_blank
        );
        $this->writeDebit($outputRow, $curTotalMIRSReversal, "Reverse asset impairment to CAA", "MIRS");
        $this->newRptLine($outputData, $outputRow);

        // Write out total reversal to Dr MIRS (HRA)
        $this->writeCodeSection(
            $outputRow,
            $this->postingAcc->acc_hra_mirs,
            $this->postingAcc->sub_revaluation_loss_reversal,
            $this->postingAcc->prj_hra_mirs,
            $this->postingAcc->fund_hra_revaluation
        );
        $this->writeDebit($outputRow, $curTotalHRAMIRSReversal, "Reverse asset impairment to CAA (HRA)", "MIRS (HRA)");
        $this->newRptLine($outputData, $outputRow);

        // Write out total Cr CAA line (for GF and HRA)
        $this->writeCodeSection(
            $outputRow,
            $this->postingAcc->acc_balance_sheet,
            $this->postingAcc->sub_capital_adjustment_acc,
            $this->postingAcc->prj_blank_project,
            $this->postingAcc->fund_gf_code
        );
        $this->writeCredit($outputRow, $curTotalMIRSReversal, "CAA asset loss reversal", "CAA");
        $this->newRptLine($outputData, $outputRow);
        $this->writeCodeSection(
            $outputRow,
            $this->postingAcc->acc_balance_sheet,
            $this->postingAcc->sub_capital_adjustment_acc,
            $this->postingAcc->prj_blank_project,
            $this->postingAcc->fund_hra_code
        );
        $this->writeCredit($outputRow, $curTotalHRAMIRSReversal, "CAA asset loss reversal (HRA)", "CAA (HRA)");
        $this->newRptLine($outputData, $outputRow);

        return $outputData;
    }

    private function getData()
    {
        $query = VwFar10::join(
            'ca_ifrs_category',
            'ca_ifrs_category.ca_ifrs_category_id',
            '=',
            'vw_far10.ca_ifrs_category_id'
        )
        ->where('vw_far10.site_group_id', SiteGroup::get()->site_group_id)
        ->select(
            [
                'ca_ifrs_category.ca_ifrs_category_code',
                'ca_ifrs_category.common_good',
                'vw_far10.hra_asset',
                \DB::raw('vw_far10.`Asset Code` as fixed_asset_code'),
                \DB::raw('vw_far10.`Account Code` as account_code'),
                'ca_ifrs_category.account_code_1',
                \DB::raw('vw_far10.`Asset Loss Reversal` as asset_loss_reversal'),
                \DB::raw('vw_far10.`Land Loss Reversal` as land_loss_reversal'),
            ]
        )
        ->orderBy(\DB::raw('vw_far10.`IFRS Code`'))
        ->orderBy(\DB::raw('vw_far10.`Asset Code`'));

        if ($this->entity) {
            $query->where('vw_far10.ca_entity_id', $this->entity);
        }
        return $query->get();
    }
}
