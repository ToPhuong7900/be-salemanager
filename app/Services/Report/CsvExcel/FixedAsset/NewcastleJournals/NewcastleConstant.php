<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\NewcastleJournals;

class NewcastleConstant
{
    public const HRA = '1HZ';
    public const HRA_A = '1HZA';
    public const HRA_ACC = 'X001';

    public const GRP_TEXT = "COC00/Council Dwellings Impairment";

    public const BS_ACCOUNT = "4RX01";
    public const BS_ACCOUNT_DSP = "1RB38";
    public const BS_ACCOUNT_HRA = "4HHB1";
    public const BS_ACCOUNT_HRA_DSP = "1HZA5";

    public const CASH_CREDIT = "CashCred";

    public const RR_FOR_DEPN = "X978";
    public const RR_FOR_REVAL_GAIN = "X974";
    public const RR_FOR_REVAL_LOSS = "X977";

    public const CAA = "X935";
    public const SMR = "X8K0";
    public const RR = "X974";
    public const RR_DSP = "X979";

    public const SMR_ACC_HRA = "X8N3";
    public const CAA_FOR_DEPN = "X935";
    public const HRA_FOR_DEPN = "X935";
    public const CAA_FOR_DISP = "X945";
    public const CAA_FOR_RR_DEPN = "X947";
    public const CAA_FOR_REVAL = "X966";

    public const CAA_FOR_IMP = "X936";
    public const RR_FOR_IMP = "X976";



    // Charge Codes
    public const DEPN_CHG_CODE = "J111";

    public const IMP_LOSS_CODE = "J115";

    public const REV_LOSS_CODE = "J119";
    public const REV_LOSS_CODE_SURPLUS = "J119";
    public const REV_LOSS_CODE_INV = "J118";
    public const REV_LOSS_CODE_DSP = "D741";

    // Sub-codes for CAT4
    public const DEPN_CODE = "";
    public const AMORT_CODE  = "";

    public const CR = 'Cr';
    public const DR = 'Dr';
}
