<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\NewcastleJournals\Reports;

use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\NewcastleJournals\NewcastleJournalBaseService;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\NewcastleJournals\JournalData\ReversalOfImpairmentLines
;

class FarCliNEWC07ReportService extends NewcastleJournalBaseService
{
    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->addJournalHeader($this->journalData);
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        // Valuation Journal

        $this->prepareData();

        return $this->journalData;
    }

    protected function prepareData()
    {
        ReversalOfImpairmentLines::addLines($this->journalData, $this->yearString);
    }
}
