<?php

namespace  Tfcloud\Services\Report\CsvExcel\FixedAsset\NewcastleJournals\Traits;

use Tfcloud\Services\Report\CsvExcel\FixedAsset\NewcastleJournals\NewcastleConstant;

trait JournalOutputTrait
{
    protected function addJournalHeader(&$data)
    {
        $data[] = $this->newJournalRow(
            [
                'col_A' => 'COA Advanced Journal Upload',
            ]
        );

        $data[] = $this->newJournalRow();

        $data[] = $this->newJournalRow(
            [
                'col_A' => 'Company:',
                'col_B' => 'NE - Newcastle City Council (Live E5.1)'
            ]
        );

        $data[] = $this->newJournalRow(
            [
                'col_A' => 'Batch Type:',
                'col_B' => 'STD',
                'col_C' => 'Batch Ref:'
            ]
        );

        $data[] = $this->newJournalRow(
            [
                'col_A' => 'Description:'
            ]
        );

        $data[] = $this->newJournalRow(
            [
                'col_A' => 'Batch Date:',
                'col_C' => 'Period:'
            ]
        );

        $data[] = $this->newJournalRow(
            [
                'col_A' => 'Year:',
                'col_C' => 'No of Periods:'
            ]
        );

        $data[] = $this->newJournalRow(
            [
                'col_A' => 'BTZ Element:',
                'col_E' => 'No Trans:'
            ]
        );

        $data[] = $this->newJournalRow(
            [
                'col_F' => 'Debits',
                'col_G' => 'Credits'
            ]
        );

        $data[] = $this->newJournalRow(
            [
                'col_A' => 'Currency Code:',
                'col_C' => 'Rate Type:',
                'col_E' => 'Base:'
            ]
        );

        $data[] = $this->newJournalRow(
            [
                'col_A' => 'Effective Date:',
                'col_E' => 'Foreign:'
            ]
        );

        $data[] = $this->newJournalRow(
            [
                'col_A' => 'Value Date:',
                'col_C' => 'Accrual Reversal:'
            ]
        );

        $data[] = $this->newJournalRow(
            [
                'col_A' => 'Security Group:',
                'col_C' => 'Accrual Period:'
            ]
        );

        $data[] = $this->newJournalRow(
            [
                'col_A' => 'Balance Class:',
                'col_C' => 'Accrual Period:'
            ]
        );


        $data[] = $this->newJournalRow(
            [
                'col_A' => '1',
                'col_B' => '2',
                'col_C' => '3',
                'col_D' => '4',
                'col_E' => '5'
            ]
        );

        $data[] = $this->newJournalRow(
            [
                'col_A' => 'POST',
                'col_B' => 'COST CENTRE (5)',
                'col_C' => 'SUBJECTIVE (4)',
                'col_D' => 'Financial Value',
                'col_E' => 'Description (40)'
            ]
        );

        $data[] = $this->newJournalRow();

        $data[] = $this->newJournalRow(
            [
                'col_D' => '0.00'
            ]
        );
    }

    public static function addJournalRow(
        &$data,
        $post,
        $account,
        $costCentre,
        $amount,
        $text
    ) {
        $data[] = self::newJournalRow(
            [
                'col_A' => $post,
                'col_B' => $costCentre,
                'col_C' => $account,
                'col_D' => $amount,
                'col_E' => substr($text, 0, 49)
            ]
        );
    }


    private static function newJournalRow($row = [])
    {
        return [
            'col_A'   => array_get($row, 'col_A', ''),
            'col_B'   => array_get($row, 'col_B', ''),
            'col_C'   => array_get($row, 'col_C', ''),
            'col_D'   => array_get($row, 'col_D', ''),
            'col_E'   => array_get($row, 'col_E', ''),
            'col_F'   => array_get($row, 'col_F', ''),
            'col_G'   => array_get($row, 'col_G', ''),
            'col_H'   => array_get($row, 'col_H', ''),
            'col_I'   => array_get($row, 'col_I', ''),
            'col_J'   => array_get($row, 'col_J', ''),
            'col_K'   => array_get($row, 'col_K', ''),
            'col_L'   => array_get($row, 'col_L', ''),
            'col_M'   => array_get($row, 'col_M', ''),
            'col_N'   => array_get($row, 'col_N', ''),
            'col_O'   => array_get($row, 'col_O', ''),
            'col_P'   => array_get($row, 'col_P', ''),
            'col_Q'   => array_get($row, 'col_Q', ''),
            'col_R'   => array_get($row, 'col_R', ''),
            'col_S'   => array_get($row, 'col_S', ''),
            'col_T'   => array_get($row, 'col_T', ''),
            'col_U'   => array_get($row, 'col_U', ''),
            'col_V'   => array_get($row, 'col_V', ''),
            'col_W'   => array_get($row, 'col_W', ''),
            'col_X'   => array_get($row, 'col_X', ''),
            'col_Y'   => array_get($row, 'col_Y', '')
        ];
    }
}
