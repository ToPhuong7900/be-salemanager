<?php

namespace  Tfcloud\Services\Report\CsvExcel\FixedAsset\NewcastleJournals\Traits;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\FixedAsset\CaFinYearManager;
use Tfcloud\Models\CaIfrsCategory;
use Tfcloud\Models\SiteGroup;
use Tfcloud\Models\CaTransaction;
use Tfcloud\Models\CaTransactionType;
use Tfcloud\Models\Views\VwFar06;
use Tfcloud\Models\Views\VwFar07;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\NewcastleJournals\NewcastleConstant as NC;

trait JournalDataTrait
{
    protected static function isHRA($costCentre)
    {
        if ($costCentre) {
            return substr($costCentre, 0, 3) == NC::HRA ||
            substr($costCentre, 0, 4) == NC::HRA_A;
        }
    }

    protected static function isYearEnd()
    {
        return Common::valueYorNtoBoolean(Auth::user()->siteGroup->ca_cal_mode_year_end);
    }

    protected static function getIFRSAccount(
        $sIFRSSubType,
        &$accountCode1,
        &$accountCode2,
        &$accountCode3,
        &$accountCode4,
        &$noHC = false,
        &$shortCode = ''
    ) {
        $ifrsCat = CaIfrsCategory::where('ca_ifrs_category_code', $sIFRSSubType)
            ->where('site_group_id', SiteGroup::get()->site_group_id)
            ->first();

        $accountCode1 = $ifrsCat->account_code_1;
        $accountCode2 = $ifrsCat->account_code_2;
        $accountCode3 = $ifrsCat->account_code_3;
        $accountCode4 = $ifrsCat->account_code_4;
        $noHC = Common::valueYorNtoBoolean($ifrsCat->investment);
        $shortCode = $ifrsCat->ca_ifrs_category_code;
    }

    protected static function getDepreciationData()
    {
        $query = VwFar07::userSiteGroup()
            ->leftJoin(
                'ca_ifrs_category as bfwd_ca_ifrs_category',
                'bfwd_ca_ifrs_category.ca_ifrs_category_id',
                '=',
                'vw_far07.bfwd_ca_ifrs_category_id'
            )
            ->where('vw_far07.ca_balance_historic', CommonConstant::DATABASE_VALUE_NO)
            ->where('vw_far07.deminimis_asset', CommonConstant::DATABASE_VALUE_NO)
            ->whereRaw('vw_far07.`CY Depreciation` <> 0')
            ->select(
                [
                    DB::raw('bfwd_ca_ifrs_category.ca_ifrs_category_desc AS bfwd_ca_ifrs_category_desc'),
                    'vw_far07.hra_asset',
                    'vw_far07.ca_fixed_asset_id',
                    'vw_far07.deminimis_asset',
                    DB::raw('vw_far07.user_sel1_code AS sub_ifrs_code'),
                    DB::raw('vw_far07.`Asset Type Code` as asset_type_code'),
                    DB::raw('vw_far07.`REV Asset Value` as CREVAV'),
                    DB::raw('vw_far07.`REV Land Value` as CREVLV'),
                    DB::raw('vw_far07.`IFRS Code` as ca_ifrs_category_code'),
                    DB::raw('vw_far07.`IFRS Description` as ca_ifrs_category_desc'),
                    DB::raw('vw_far07.`Previous IFRS Code` as bf_ca_ifrs_category_code'),
                    DB::raw('vw_far07.`Previous IFRS Description` as bf_ca_ifrs_category_desc'),
                    DB::raw('vw_far07.`Asset Code` as fixed_asset_code'),
                    DB::raw('vw_far07.`Asset Description` as fixed_asset_desc'),
                    DB::raw('vw_far07.`Account Code` as account_code'),
                    DB::raw('vw_far07.`Accumulated Depreciation` as COpenDPN'),
                    DB::raw('vw_far07.`CY Depreciation` as CDPN'),
                    DB::raw('vw_far07.`CFwd Accumulated Depreciation` as CCFwdDPN'),
                    DB::raw('vw_far07.`Accumulated Impairment Asset` as COpenIMPA'),
                    DB::raw('vw_far07.`Accumulated Impairment Land` as COpenIMPL'),
                    DB::raw('vw_far07.`BFwd GBV` as COpenGBV'),
                    DB::raw('vw_far07.`BFwd RR Asset` as BFAssetRR'),
                    DB::raw('vw_far07.`BFwd RR Land` as BFLandRR'),
                    'bfwd_ca_ifrs_category.amortisation as bfwd_amortisation',
                ]
            )
            ->orderBy(DB::raw('bfwd_ca_ifrs_category.ca_ifrs_category_desc'))
            ->orderBy(DB::raw('vw_far07.`IFRS Description`'))
            ->orderBy(DB::raw('vw_far07.`Account Code`'));

        return $query->get();
    }

    protected static function getBfwdData($caFixedAssetId, $historic)
    {
        $query = VwFar07::userSiteGroup()
            ->leftJoin(
                'ca_ifrs_category as bfwd_ca_ifrs_category',
                'bfwd_ca_ifrs_category.ca_ifrs_category_id',
                '=',
                'vw_far07.bfwd_ca_ifrs_category_id'
            )
            ->where('vw_far07.ca_balance_historic', $historic)
            ->where('vw_far07.deminimis_asset', CommonConstant::DATABASE_VALUE_NO)
            ->where('vw_far07.ca_fixed_asset_id', $caFixedAssetId)
            ->select(
                [
                    DB::raw('bfwd_ca_ifrs_category.ca_ifrs_category_desc AS bfwd_ca_ifrs_category_desc'),
                    'vw_far07.hra_asset',
                    'vw_far07.ca_fixed_asset_id',
                    'vw_far07.deminimis_asset',
                    DB::raw('vw_far07.user_sel1_code AS sub_ifrs_code'),
                    DB::raw('vw_far07.`Asset Type Code` as asset_type_code'),
                    DB::raw('vw_far07.`REV Asset Value` as CREVAV'),
                    DB::raw('vw_far07.`REV Land Value` as CREVLV'),
                    DB::raw('vw_far07.`IFRS Code` as ca_ifrs_category_code'),
                    DB::raw('vw_far07.`IFRS Description` as ca_ifrs_category_desc'),
                    DB::raw('vw_far07.`Previous IFRS Code` as bf_ca_ifrs_category_code'),
                    DB::raw('vw_far07.`Previous IFRS Description` as bf_ca_ifrs_category_desc'),
                    DB::raw('vw_far07.`Asset Code` as fixed_asset_code'),
                    DB::raw('vw_far07.`Asset Description` as fixed_asset_desc'),
                    DB::raw('vw_far07.`Account Code` as account_code'),
                    DB::raw('vw_far07.`Accumulated Depreciation` as COpenDPN'),
                    DB::raw('vw_far07.`CY Depreciation` as CYDPN'),
                    DB::raw('vw_far07.`CFwd Accumulated Depreciation` as CCFwdDPN'),
                    DB::raw('vw_far07.`Accumulated Impairment Asset` as COpenIMPA'),
                    DB::raw('vw_far07.`Accumulated Impairment Land` as COpenIMPL'),
                    DB::raw('vw_far07.`BFwd GBV` as COpenGBV'),
                    DB::raw('vw_far07.`BFwd RR Asset` as BFAssetRR'),
                    DB::raw('vw_far07.`BFwd RR Land` as BFLandRR'),
                    'bfwd_ca_ifrs_category.amortisation as bfwd_amortisation',
                ]
            )
            ->orderBy(DB::raw('bfwd_ca_ifrs_category.ca_ifrs_category_desc'))
            ->orderBy(DB::raw('vw_far07.`IFRS Description`'))
            ->orderBy(DB::raw('vw_far07.`Account Code`'));

        return $query->first();
    }

    protected static function hasReval($caFixedAssetId)
    {
        $query = CaTransaction::where('ca_transaction_type_id', CaTransactionType::REVALUATION)
            ->where('ca_fixed_asset_id', $caFixedAssetId)
            ->where('site_group_id', SiteGroup::get()->site_group_id)
            ->where('ca_fin_year_id', CaFinYearManager::currentFinYearId());

        return $query->count() > 0;
    }

    protected static function getTransactionList($transType)
    {
        $query = CaTransaction::join(
            'ca_ifrs_category',
            'ca_ifrs_category.ca_ifrs_category_id',
            '=',
            'ca_transaction.ca_ifrs_cat_id'
        )
        ->join(
            'ca_fixed_asset',
            'ca_fixed_asset.ca_fixed_asset_id',
            '=',
            'ca_transaction.ca_fixed_asset_id'
        )
        ->join(
            'ca_account',
            'ca_account.ca_account_id',
            '=',
            'ca_fixed_asset.ca_account_code_id'
        )
        ->where('ca_transaction_type_id', $transType)
        ->where('ca_fixed_asset.site_group_id', SiteGroup::get()->site_group_id)
        ->where('ca_fixed_asset.active', CommonConstant::DATABASE_VALUE_YES)
        ->where('ca_fixed_asset.deminimis_asset', CommonConstant::DATABASE_VALUE_NO)
        ->where('ca_transaction.ca_fin_year_id', CaFinYearManager::currentFinYearId())
        ->select(
            [
                'ca_transaction.ca_fixed_asset_id',
                'ca_ifrs_category.ca_ifrs_category_code',
                'ca_ifrs_category.ca_ifrs_category_desc',
                'ca_account.ca_account_code',
                'ca_transaction.cv_asset',
                'ca_transaction.cv_land',
                'ca_transaction.hc_asset',
                'ca_transaction.hc_land',
                'ca_transaction.cv_depn_adjustment',
                'ca_transaction.hc_depn_adjustment',
                'ca_fixed_asset.ca_fixed_asset_code',
                'ca_fixed_asset.ca_fixed_asset_id',
                'ca_transaction.disposal_proceeds',
                'ca_transaction.disposal_costs',
                'ca_transaction.effective_date',
                'ca_transaction.effective_date_override'
            ]
        );

        if ($transType == CaTransactionType::CATEGORY_TRANSFER) {
            $query->orderBy('ca_transaction.ca_fixed_asset_id')
                ->orderBy('ca_transaction.ca_transaction_code');
        }

        return $query->get();
    }

    protected static function getRevalData()
    {
        $query = VwFar06::userSiteGroup()
            ->join(
                'ca_fixed_asset',
                'ca_fixed_asset.ca_fixed_asset_id',
                '=',
                'vw_far06.ca_fixed_asset_id'
            )
            ->join(
                'ca_fixed_asset_type',
                'ca_fixed_asset_type.ca_fixed_asset_type_id',
                '=',
                'ca_fixed_asset.ca_fixed_asset_type_id'
            )
            ->select(
                [
                    DB::raw('vw_far06.`Asset Code` AS ca_asset_code'),
                    DB::raw('vw_far06.`IFRS Code` AS ifrs_code'),
                    DB::raw('vw_far06.`IFRS Description` AS ifrs_desc'),
                    DB::raw('vw_far06.`Account Code` AS account_code'),
                    DB::raw('vw_far06.`Reval Asset Value` AS reval_asset_value'),
                    DB::raw('vw_far06.`Reval Land Value` AS reval_land_value'),
                    DB::raw('vw_far06.`Gross RR` AS gross_rr'),
                    DB::raw('vw_far06.`Gross RR` AS rr_gain'),
                    DB::raw('vw_far06.`Gross SD` AS gross_sd'),
                    DB::raw('vw_far06.`Depn WOff RR` AS depn_wo_rr'),
                    DB::raw('vw_far06.`Depn WOff SD` AS depn_wo_sd'),
                    DB::raw('vw_far06.`Imp WOff RR` AS imp_wo_rr'),
                    DB::raw('vw_far06.`Imp WOff SD` AS imp_wo_sd'),
                    DB::raw('vw_far06.`Asset Loss Reversal` AS asset_loss_reversal'),
                    DB::raw('vw_far06.`Land Loss Reversal` AS land_loss_reversal'),
                    DB::raw('vw_far06.`Loss Depreciation Reversal` AS loss_depn_reversal'),
                    DB::raw('vw_far06.`Asset Impairment Reversal` AS asset_imp_reversal'),
                    DB::raw('vw_far06.`Land Impairment Reversal` AS land_imp_reversal'),
                    DB::raw('vw_far06.`Impairment Depreciation Reversal` AS imp_depn_reversal'),
                    DB::raw('vw_far06.`Reval Reserve` AS reval_reserve'),
                ]
            )
            ->orderByRaw('`IFRS Code`')
            ->orderByRaw('`Account Code`')
            ;

        return $query->get();
    }
}
