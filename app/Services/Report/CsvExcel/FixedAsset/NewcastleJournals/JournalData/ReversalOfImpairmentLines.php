<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\NewcastleJournals\JournalData;

use DateTime;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Math;
use Tfcloud\Models\CaTransactionType;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\NewcastleJournals\NewcastleConstant as NC;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\NewcastleJournals\Traits\JournalOutputTrait;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\NewcastleJournals\Traits\JournalDataTrait;

class ReversalOfImpairmentLines
{
    use JournalOutputTrait;
    use JournalDataTrait;

    protected const FROM_YMD = '!Y-m-d';

    public static function addLines(&$data, $yearString)
    {
        self::addROILines($data, $yearString);
    }

    private static function addROILines(&$data, $yearString)
    {
        $rolData = self::getTransactionList(CaTransactionType::REVERSAL_OF_IMPAIRMENT);

        foreach ($rolData as $reversal) {
            $codes = self::getCodes($reversal, $yearString);

            $cvAssetValue = $reversal->cv_asset;
            $cvLandValue = $reversal->cv_land;
            $cvValue = Math::addCurrency([$cvAssetValue, $cvLandValue]);

            $hcAssetValue = $reversal->hc_asset;
            $hcLandValue = $reversal->hc_land;
            $hcValue = Math::addCurrency([$hcAssetValue, $hcLandValue]);

            $rrValue = Math::subCurrency([$cvValue, $hcValue]);

            $hcDepnAdj = $reversal->hc_depn_adjustment;

            // Reverse out the Impairment - FA to I&E

            // Debit the Fixed Carrying amount (Gross Reversal).
            if (Math::notEqualCurrency($cvValue, 0)) {
                $msg = "RoI (Accum. Imp.) $reversal->ca_fixed_asset_code $yearString";
                self::printLine(
                    $data,
                    NC::DR,
                    $codes['accountCode1'],
                    $codes['bsAccount'],
                    $cvValue,
                    $msg
                );
            }

            // Credit I&E
            if (Math::notEqualCurrency($hcValue, 0)) {
                $msg = "RoI I&E $reversal->ca_fixed_asset_code $yearString";
                self::printLine(
                    $data,
                    NC::CR,
                    NC::IMP_LOSS_CODE,
                    $codes['costCentre'],
                    $hcValue,
                    $msg
                );
            }

            // Credit and Reverse out via RR
            if (Math::notEqualCurrency($rrValue, 0)) {
                $msg = "RoI (RR Adj.) $reversal->ca_fixed_asset_code $yearString";
                self::printLine(
                    $data,
                    NC::CR,
                    NC::RR,
                    $codes['bsAccount'],
                    $rrValue,
                    $msg
                );
            }

            // Historic Depreciation Adjustment

            // Reverse out Depn Adj from RR
            if (Math::notEqualCurrency($hcDepnAdj, 0)) {
                $msg = "{$codes['costCentre']}}/{$codes['ifrsSubTypeCode']} %s {$codes['yearString']}";
                // Credit Acc Depn. for Category
                self::printLine(
                    $data,
                    NC::CR,
                    $codes['accountCode2'],
                    $codes['bsAccount'],
                    $hcDepnAdj,
                    sprintf($msg, 'RoI Depn. Adj.')
                );

                // Debit &E Depn with value of depn adj.
                self::printLine(
                    $data,
                    NC::DR,
                    NC::IMP_LOSS_CODE,
                    $codes['costCentre'],
                    $hcDepnAdj,
                    sprintf($msg, 'RoI Depn. (I&E)')
                );
            }

            // Reverse out and I&E charge to CAA/SMR
            $caaValue = Math::subCurrency([$hcAssetValue, $hcDepnAdj]);
            self::addCaaLines($data, $caaValue, $codes);
        }
    }

    private static function addCaaLines(&$data, $caaValue, $codes)
    {
        $msg = "{$codes['costCentre']}}/{$codes['ifrsSubTypeCode']} %s {$codes['yearString']}";
        if (Math::notEqualCurrency($caaValue, 0)) {
            // Credit CAA
            self::printLine(
                $data,
                NC::CR,
                NC::CAA_FOR_IMP,
                $codes['bsAccount'],
                $caaValue,
                sprintf($msg, 'RoI (CAA)')
            );

            // Debit SMR.
            if ($codes['hra']) {
                self::printLine(
                    $data,
                    NC::DR,
                    NC::SMR_ACC_HRA,
                    $codes['bsAccount'],
                    $caaValue,
                    sprintf($msg, 'RoI (HRA)')
                );
            } else {
                self::printLine(
                    $data,
                    NC::DR,
                    NC::SMR,
                    $codes['bsAccount'],
                    $caaValue,
                    sprintf($msg, 'RoI (SMR)')
                );
            }
        }
    }

    private static function getCodes($record, $yearString)
    {
        $caAssetCode = $record->ca_asset_code;
        $ifrsSubTypeCode = self::getIFRSSubTypeCode($record);
        self::getIFRSAccount($ifrsSubTypeCode, $accountCode1, $accountCode2, $accountCode3, $accountCode4, $noHC);

        // Second part of the group is the cost centre.
        $costCentre = self::getAccountCode($record);
        $hra = self::isHRA($costCentre);

        if ($hra) {
            $bsAccount = NC::BS_ACCOUNT_HRA;
            $smrAccount = NC::SMR_ACC_HRA;
        } else {
            $bsAccount = NC::BS_ACCOUNT;
            $smrAccount = NC::SMR;
        }

        return [
            'caAssetCode'     => $caAssetCode,
            'ifrsSubTypeCode' => $ifrsSubTypeCode,
            'hra'             => $hra,
            'costCentre'      => $costCentre,
            'accountCode1'    => $accountCode1,
            'accountCode2'    => $accountCode2,
            'accountCode3'    => $accountCode3,
            'accountCode4'    => $accountCode4,
            'noHC'            => $noHC,
            'bsAccount'       => $bsAccount,
            'smrAccount'      => $smrAccount,
            'yearString'      => $yearString
        ];
    }

    private static function getIFRSSubTypeCode($depn)
    {
        if ($depn) {
            return $depn->ca_ifrs_category_code;
        }
    }

    private static function getAccountCode($depn)
    {
        if ($depn) {
            return $depn->ca_account_code;
        }
    }

    private static function printLine(&$data, $post, $account, $costCentre, $amount, $text)
    {
        if ($post == NC::DR) {
            $amount = Common::numberFormat(Math::negateCurrency($amount), thousandsSep: ',');
        } else {
            $amount = Common::numberFormat($amount, thousandsSep: ',');
        }

        self::addJournalRow(
            $data,
            $post,
            $account,
            $costCentre,
            $amount,
            $text
        );
    }
}
