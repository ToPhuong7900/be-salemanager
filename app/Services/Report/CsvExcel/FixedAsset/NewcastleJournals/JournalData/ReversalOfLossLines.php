<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\NewcastleJournals\JournalData;

use DateTime;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Math;
use Tfcloud\Models\CaTransactionType;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\NewcastleJournals\NewcastleConstant as NC;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\NewcastleJournals\Traits\JournalOutputTrait;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\NewcastleJournals\Traits\JournalDataTrait;

class ReversalOfLossLines
{
    use JournalOutputTrait;
    use JournalDataTrait;

    protected const FROM_YMD = '!Y-m-d';

    public static function addLines(&$data, $yearString)
    {
        self::addROLLines($data, $yearString);
    }

    private static function addROLLines(&$data, $yearString)
    {
        $rolData = self::getTransactionList(CaTransactionType::REVERSAL_OF_LOSS);

        foreach ($rolData as $reversal) {
            $codes = self::getCodes($reversal, $yearString);

            $cvAssetValue = $reversal->cv_asset;
            $cvLandValue = $reversal->cv_land;
            $cvValue = Math::addCurrency([$cvAssetValue, $cvLandValue]);

            $hcAssetValue = $reversal->hc_asset;
            $hcLandValue = $reversal->hc_land;
            $hcValue = Math::addCurrency([$hcAssetValue, $hcLandValue]);

            $rrValue = Math::subCurrency([$cvValue, $hcValue]);

            $cvDepnAdj = $reversal->cv_depn_adjustment;
            $hcDepnAdj = $reversal->hc_depn_adjustment;
            $rrDepnAdj = Math::subCurrency([$cvDepnAdj, $hcDepnAdj]);

            // Reverse out the Loss - FA to I&E

            // Debit the Fixed Carrying amount (Gross Reversal).
            if (Math::notEqualCurrency($cvValue, 0)) {
                $msg = "RoL {$codes['caAssetCode']}";
                self::printLine(
                    $data,
                    NC::DR,
                    $codes['accountCode1'],
                    $codes['bSAccount'],
                    $cvValue,
                    $msg
                );
            }

            // Credit I&E
            if (Math::notEqualCurrency($hcValue, 0)) {
                $msg = "{$codes['costCentre']}/{$codes['ifrsSubTypeCode']} RoL I&E {$codes['yearString']}";
                self::printLine(
                    $data,
                    NC::CR,
                    NC::REV_LOSS_CODE,
                    $codes['bSAccount'],
                    $hcValue,
                    $msg
                );
            }

            // Credit and Revers out via RR
            if (Math::notEqualCurrency($rrValue, 0)) {
                $msg = "{$codes['costCentre']}/{$codes['ifrsSubTypeCode']} RoL I&E {$codes['yearString']}";
                self::printLine(
                    $data,
                    NC::CR,
                    NC::RR,
                    $codes['bSAccount'],
                    $rrValue,
                    $msg
                );
            }


            // Depreciation Adjustment

            // Restore any depreciation to I&E
            if (Math::notEqualCurrency($cvDepnAdj, 0)) {
                // Credit Acc Depn. for Category
                $msg = "{$codes['costCentre']}/{$codes['ifrsSubTypeCode']} RoL Depn. Adj. {$codes['yearString']}";
                self::printLine(
                    $data,
                    NC::CR,
                    $codes['accountCode2'],
                    $codes['bSAccount'],
                    $cvDepnAdj,
                    $msg
                );

                // Debit Service I&E Depn with value of depn adj.
                $msg = "{$codes['costCentre']}/{$codes['ifrsSubTypeCode']} RoL Depn I&E {$codes['yearString']}";
                self::printLine(
                    $data,
                    NC::DR,
                    NC::REV_LOSS_CODE,
                    $codes['costCentre'],
                    $cvDepnAdj,
                    $msg
                );
            }


            // Reverse out Depn Adj from RR
            if (Math::notEqualCurrency($rrDepnAdj, 0)) {
                // Credit Acc Depn. for Category
                $msg = "{$codes['costCentre']}/{$codes['ifrsSubTypeCode']} RoL Depn Adj {$codes['yearString']}";
                self::printLine(
                    $data,
                    NC::CR,
                    NC::CAA,
                    $codes['bSAccount'],
                    $rrDepnAdj,
                    $msg
                );

                // Debit Service I&E Depn with value of depn adj.
                self::printLine(
                    $data,
                    NC::DR,
                    NC::RR,
                    $codes['costCentre'],
                    $rrDepnAdj,
                    $msg
                );
            }

            // // Reverse out and I&E charge to CAA/SMR
            $caaValue = Math::subCurrency([$hcAssetValue, $cvDepnAdj]);
            self::addCaaSmrLines($data, $caaValue, $codes);
        }
    }

    private static function addCaaSmrLines(&$data, $caaValue, $codes)
    {
        // Reverse out and I&E charge to CAA/SMR

        if (Math::greaterThanCurrency($caaValue, 0)) {
            // Credit CAA
            $msg = "{$codes['costCentre']}/{$codes['ifrsSubTypeCode']} RoL (CAA) {$codes['yearString']}";
            self::printLine(
                $data,
                NC::CR,
                NC::CAA_FOR_REVAL,
                $codes['bSAccount'],
                $caaValue,
                $msg
            );

            // Debit SMR.
            if ($codes['hra']) {
                $msg = "{$codes['costCentre']}/{$codes['ifrsSubTypeCode']} RoL (HRA) {$codes['yearString']}";
                self::printLine(
                    $data,
                    NC::DR,
                    NC::SMR_ACC_HRA,
                    $codes['bSAccount'],
                    $caaValue,
                    $msg
                );
            } else {
                $msg = "{$codes['costCentre']}/{$codes['ifrsSubTypeCode']} RoL (GF) {$codes['yearString']}";
                self::printLine(
                    $data,
                    NC::DR,
                    NC::SMR,
                    $codes['bSAccount'],
                    $caaValue,
                    $msg
                );
            }
        }

        if (Math::lessThanCurrency($caaValue, 0)) {
            $caaValue = Math::negateCurrency($caaValue);

            // Debit CAA
            $msg = "{$codes['costCentre']}/{$codes['ifrsSubTypeCode']} RoL (CAA) {$codes['yearString']}";
            self::printLine(
                $data,
                NC::DR,
                NC::CAA_FOR_REVAL,
                $codes['bSAccount'],
                $caaValue,
                $msg
            );

            // Credit SMR.
            if ($codes['hra']) {
                $msg = "{$codes['costCentre']}/{$codes['ifrsSubTypeCode']} RoL (HRA) {$codes['yearString']}";
                self::printLine(
                    $data,
                    NC::CR,
                    NC::SMR_ACC_HRA,
                    $codes['bSAccount'],
                    $caaValue,
                    $msg
                );
            } else {
                $msg = "{$codes['costCentre']}/{$codes['ifrsSubTypeCode']} RoL (GF) {$codes['yearString']}";
                self::printLine(
                    $data,
                    NC::CR,
                    NC::SMR,
                    $codes['bSAccount'],
                    $caaValue,
                    $msg
                );
            }
        }
    }

    private static function getCodes($record, $yearString)
    {
        $caAssetCode = $record->ca_asset_code;
        $ifrsSubTypeCode = self::getIFRSSubTypeCode($record);
        self::getIFRSAccount($ifrsSubTypeCode, $accountCode1, $accountCode2, $accountCode3, $accountCode4, $noHC);

        // Second part of the group is the cost centre.
        $costCentre = self::getAccountCode($record);
        $hra = self::isHRA($costCentre);

        if ($hra) {
            $bsAccount = NC::BS_ACCOUNT_HRA;
            $smrAccount = NC::SMR_ACC_HRA;
        } else {
            $bsAccount = NC::BS_ACCOUNT;
            $smrAccount = NC::SMR;
        }

        return [
            'caAssetCode'     => $caAssetCode,
            'ifrsSubTypeCode' => $ifrsSubTypeCode,
            'hra'             => $hra,
            'costCentre'      => $costCentre,
            'accountCode1'    => $accountCode1,
            'accountCode2'    => $accountCode2,
            'accountCode3'    => $accountCode3,
            'accountCode4'    => $accountCode4,
            'noHC'            => $noHC,
            'bsAccount'       => $bsAccount,
            'smrAccount'      => $smrAccount,
            '$yearString'     => $yearString
        ];
    }


    private static function getIFRSSubTypeCode($depn)
    {
        if ($depn) {
            return $depn->ca_ifrs_category_code;
        }
    }

    private static function getAccountCode($depn)
    {
        if ($depn) {
            return $depn->ca_account_code;
        }
    }

    private static function printLine(&$data, $post, $account, $costCentre, $amount, $text)
    {
        if ($post == NC::DR) {
            $amount = Common::numberFormat(Math::negateCurrency($amount), thousandsSep: ',');
        } else {
            $amount = Common::numberFormat($amount, thousandsSep: ',');
        }

        self::addJournalRow(
            $data,
            $post,
            $account,
            $costCentre,
            $amount,
            $text
        );
    }
}
