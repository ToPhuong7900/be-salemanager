<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\NewcastleJournals\JournalData;

use DateTime;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Math;
use Tfcloud\Models\CaTransactionType;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\NewcastleJournals\NewcastleConstant as NC;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\NewcastleJournals\Traits\JournalOutputTrait;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\NewcastleJournals\Traits\JournalDataTrait;

class DisposalLines
{
    use JournalOutputTrait;
    use JournalDataTrait;

    protected const FROM_YMD = '!Y-m-d';

    public static function addLines(&$data, $yearString)
    {
        self::addDisposalLines($data, $yearString);
    }

    private static function addDisposalLines(&$data, $yearString)
    {
        $dspData = self::getTransactionList(CaTransactionType::DISPOSAL);

        foreach ($dspData as $disposal) {
            $codes = self::getCodes($disposal, $yearString);

            $cvAssetValue = $disposal->cv_asset;
            $cvLandValue = $disposal->cv_land;
            $cvValue = Math::addCurrency([$cvAssetValue, $cvLandValue]);

            $hcAssetValue = $disposal->hc_asset;
            $hcLandValue = $disposal->hc_land;
            $hcValue = Math::addCurrency([$hcAssetValue, $hcLandValue]);

            $rrValue = Math::subCurrency([$cvValue, $hcValue]);

            $bfwdData = self::getBfwdData(
                $disposal->ca_fixed_asset_id,
                CommonConstant::DATABASE_VALUE_NO
            );

            $bfwdDepn = $bfwdData->COpenDPN;
            $cyDepn = $bfwdData->CYDPN;
            $bfwdImpA = $bfwdData->COpenIMPA;
            $bfwdImpL = $bfwdData->COpenIMPL;
            $bfwdImp = Math::addCurrency([$bfwdImpA, $bfwdImpL]);

            $grossValue = $cvValue;

            // If Year end write out CY Depn
            if ($codes['yeDisposal'] && Math::notEqualCurrency($cyDepn, 0)) {
                $msg = "Disposal CY Depn. W/O {$codes['caAssetCode']}";
                self::printLine(
                    $data,
                    NC::DR,
                    $codes['accountCode2'],
                    $codes['bsAccount'],
                    $cyDepn,
                    $msg
                );
                $grossValue = Math::addCurrency([$grossValue, $cyDepn]);
            }

            // If Revalued need to include written off Depn and Imp.
            if (self::hasReval($disposal->ca_fixed_asset_id)) {
                if (Math::notEqualCurrency($bfwdDepn, 0)) {
                    $msg = "Disposal Acc.Depn. W/O {$codes['caAssetCode']}";
                    self::printLine(
                        $data,
                        NC::DR,
                        $codes['accountCode2'],
                        $codes['bsAccount'],
                        $bfwdDepn,
                        $msg
                    );

                    $grossValue = Math::addCurrency([$grossValue, $bfwdDepn]);
                }

                if (Math::notEqualCurrency($bfwdImp, 0)) {
                    $msg = "Disposal Acc.Imp. W/O {$codes['caAssetCode']}";
                    self::printLine(
                        $data,
                        NC::DR,
                        $codes['accountCode1'],
                        $codes['bsAccount'],
                        $bfwdImp,
                        $msg
                    );

                    $grossValue = Math::addCurrency([$grossValue, $bfwdImp]);
                }
            }

            // Credit carrying amount.
            $msg = "Disposal Fixed Asset {$codes['caAssetCode']}";
            self::printLine(
                $data,
                NC::CR,
                $codes['accountCode1'],
                $codes['bsAccount'],
                $grossValue,
                $msg
            );

            // Debit I&E.
            $msg = "{$codes['costCentre']}/{$codes['ifrsSubTypeCode']} Disposal I&E {$codes['yearString']}";
            $bsAcc = $codes['bsAccount'] == NC::BS_ACCOUNT ? NC::BS_ACCOUNT_DSP : NC::BS_ACCOUNT_HRA_DSP;
            self::printLine(
                $data,
                NC::DR,
                NC::REV_LOSS_CODE_DSP,
                $bsAcc,
                $cvValue,
                $msg
            );

            self::addRRLines($data, $rrValue, $codes);
        }
    }

    private static function addRRLines(&$data, $rrValue, $codes)
    {
        // Clear out RR.
        if (Math::notEqualCurrency($rrValue, 0)) {
            $msg = "{$codes['costCentre']}/{$codes['ifrsSubTypeCode']} %s {$codes['yearString']}";

            // Clear RR.
            self::printLine(
                $data,
                NC::DR,
                NC::RR_DSP,
                $codes['bsAccount'],
                $rrValue,
                sprintf($msg, 'Disposal RR')
            );

            // Credit to CAA.
            self::printLine(
                $data,
                NC::CR,
                NC::CAA_FOR_DISP,
                $codes['bsAccount'],
                $rrValue,
                sprintf($msg, 'Disposal (RR-CAA)')
            );
        }
    }

    private static function getCodes($record, $yearString)
    {
        $caAssetCode = $record->ca_asset_code;
        $ifrsSubTypeCode = self::getIFRSSubTypeCode($record);
        self::getIFRSAccount($ifrsSubTypeCode, $accountCode1, $accountCode2, $accountCode3, $accountCode4, $noHC);

        // Second part of the group is the cost centre.
        $costCentre = self::getAccountCode($record);
        $hra = self::isHRA($costCentre);

        if ($hra) {
            $bsAccount = NC::BS_ACCOUNT_HRA;
            $smrAccount = NC::SMR_ACC_HRA;
        } else {
            $bsAccount = NC::BS_ACCOUNT;
            $smrAccount = NC::SMR;
        }

        // Has the effective date been overridden to the year end date.
        if ($record->effective_date) {
            $effectiveDate = DateTime::createFromFormat(self::FROM_YMD, $record->effective_date);
            $yeDisposal = $effectiveDate->format('d/m') == '31/03';
        } else {
            $yeDisposal = false;
        }

        return [
            'caAssetCode'     => $caAssetCode,
            'ifrsSubTypeCode' => $ifrsSubTypeCode,
            'hra'             => $hra,
            'costCentre'      => $costCentre,
            'accountCode1'    => $accountCode1,
            'accountCode2'    => $accountCode2,
            'accountCode3'    => $accountCode3,
            'accountCode4'    => $accountCode4,
            'noHC'            => $noHC,
            'bsAccount'       => $bsAccount,
            'smrAccount'      => $smrAccount,
            'yearString'      => $yearString,
            'yeDisposal'      => $yeDisposal
        ];
    }

    private static function getIFRSSubTypeCode($depn)
    {
        if ($depn) {
            return $depn->ca_ifrs_category_code;
        }
    }

    private static function getAccountCode($depn)
    {
        if ($depn) {
            return $depn->ca_account_code;
        }
    }

    private static function printLine(&$data, $post, $account, $costCentre, $amount, $text)
    {
        if ($post == NC::DR) {
            $amount = Common::numberFormat(Math::negateCurrency($amount), thousandsSep: ',');
        } else {
            $amount = Common::numberFormat($amount, thousandsSep: ',');
        }

        self::addJournalRow(
            $data,
            $post,
            $account,
            $costCentre,
            $amount,
            $text
        );
    }
}
