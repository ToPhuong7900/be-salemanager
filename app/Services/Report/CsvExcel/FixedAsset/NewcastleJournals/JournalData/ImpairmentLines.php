<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\NewcastleJournals\JournalData;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Math;
use Tfcloud\Models\CaTransactionType;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\NewcastleJournals\NewcastleConstant as NC;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\NewcastleJournals\Traits\JournalOutputTrait;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\NewcastleJournals\Traits\JournalDataTrait;

class ImpairmentLines
{
    use JournalOutputTrait;
    use JournalDataTrait;

    public static function addLines(&$data, $yearString)
    {
        self::addImpairmentLines($data, $yearString);
    }

    private static function addImpairmentLines(&$data, $yearString)
    {
        $impData = self::getTransactionList(CaTransactionType::IMPAIRMENT);

        $count = 0;

        $accountCode1 = '';
        $accountCode2 = '';
        $accountCode3 = '';
        $accountCode4 = '';

        $msg = "Impairment $yearString";

        foreach ($impData as $impairment) {
            $count++;

            $ifrsSubTypeCode = self::getIFRSSubTypeCode($impairment);
            self::getIFRSAccount($ifrsSubTypeCode, $accountCode1, $accountCode2, $accountCode3, $accountCode4);

            // Second part of the group is the cost centre.
            $costCentre = self::getAccountCode($impairment);
            $hra = self::isHRA($costCentre);

            $cvAssetValue = Math::negateCurrency($impairment->cv_asset);
            $cvLandValue = Math::negateCurrency($impairment->cv_land);
            $cvValue = Math::addCurrency([$cvAssetValue, $cvLandValue]);

            $hcAssetValue = Math::negateCurrency($impairment->hc_asset);
            $hcLandValue = Math::negateCurrency($impairment->hc_land);
            $hcValue = Math::addCurrency([$hcAssetValue, $hcLandValue]);

            $rrValue = Math::subCurrency([$cvValue, $hcValue]);

            $bSAccount = $hra ? NC::BS_ACCOUNT_HRA : NC::BS_ACCOUNT;

            // Credit Accumulated Impairment
            self::printLine(
                $data,
                NC::CR,
                $accountCode1,
                $bSAccount,
                $cvValue,
                "$costCentre/$ifrsSubTypeCode $msg"
            );

            // Debit RR if necessary.
            if (Math::greaterThanCurrency($rrValue, 0)) {
                self::printLine(
                    $data,
                    NC::DR,
                    NC::RR_FOR_IMP,
                    $bSAccount,
                    $rrValue,
                    "$costCentre/$ifrsSubTypeCode $msg"
                );
            }

            // Debit I&E if more than RR.
            if (Math::greaterThanCurrency($hcValue, 0)) {
                self::printLine(
                    $data,
                    NC::DR,
                    NC::IMP_LOSS_CODE,
                    $costCentre,
                    $hcValue,
                    "$costCentre/$ifrsSubTypeCode $msg"
                );

                // Debit CAA.
                self::printLine(
                    $data,
                    NC::DR,
                    NC::CAA_FOR_IMP,
                    $bSAccount,
                    $hcValue,
                    "$costCentre/$ifrsSubTypeCode $msg"
                );

                if ($hra) {
                    // Credit HRA.
                    self::printLine(
                        $data,
                        NC::CR,
                        NC::SMR_ACC_HRA,
                        $bSAccount,
                        $hcValue,
                        "$costCentre/$ifrsSubTypeCode $msg"
                    );
                } else {
                    // Credit SMR.
                    self::printLine(
                        $data,
                        NC::CR,
                        NC::SMR,
                        $bSAccount,
                        $hcValue,
                        "$costCentre/$ifrsSubTypeCode $msg"
                    );
                }
            }
        }
    }

    private static function getIFRSSubTypeCode($depn)
    {
        if ($depn) {
            return $depn->ca_ifrs_category_code;
        }
    }

    private static function getAccountCode($depn)
    {
        if ($depn) {
            return $depn->ca_account_code;
        }
    }

    private static function printLine(&$data, $post, $account, $costCentre, $amount, $text)
    {
        if ($post == NC::DR) {
            $amount = Common::numberFormat(Math::negateCurrency($amount), thousandsSep: ',');
        } else {
            $amount = Common::numberFormat($amount, thousandsSep: ',');
        }


        self::addJournalRow(
            $data,
            $post,
            $account,
            $costCentre,
            $amount,
            $text
        );
    }
}
