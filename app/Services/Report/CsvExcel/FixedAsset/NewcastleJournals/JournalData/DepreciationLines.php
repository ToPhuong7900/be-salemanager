<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\NewcastleJournals\JournalData;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Math;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\NewcastleJournals\NewcastleConstant as NC;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\NewcastleJournals\Traits\JournalOutputTrait;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\NewcastleJournals\Traits\JournalDataTrait;

class DepreciationLines
{
    use JournalOutputTrait;
    use JournalDataTrait;

    public static function addLines(&$data, $yearString)
    {
        self::addGroupedDepnLines($data, $yearString);
    }

    private static function addGroupedDepnLines(&$data, $yearString)
    {
        $depnData = self::getDepreciationData();

        $subTotalDepn = 0;
        $subTotalRRDepn = 0;
        $totalAmount = 0;
        $totalHRAAmount = 0;

        $count = 0;

        $accountCode1 = '';
        $accountCode2 = '';
        $accountCode3 = '';
        $accountCode4 = '';

        foreach ($depnData as $depn) {
            $count++;

            $bfIfrsSubType = self::getBFIFRSSubType($depn);
            $ifrsSubType = self::getIFRSSubType($depn);
            $bfIfrsSubTypeCode = self::getBFIFRSSubTypeCode($depn);
            $ifrsSubTypeCode = self::getIFRSSubTypeCode($depn);

            self::setYECodes($bfIfrsSubType, $ifrsSubType, $bfIfrsSubTypeCode, $ifrsSubTypeCode);

            $ifrsSubTypeCode = self::getIFRSSubTypeCode($depn);
            // Second part of the group is the cost centre.
            $costCentre = self::getAccountCode($depn);

            // Starting Group.
            if ($count == 1) {
                self::getIFRSAccount($ifrsSubTypeCode, $accountCode1, $accountCode2, $accountCode3, $accountCode4);
                $groupDepnCode = $accountCode2;

                $group = "$costCentre/$ifrsSubTypeCode";
                $groupCostCentre = $costCentre;
                $currentRecordGroup = $group;
            } else {
                $currentRecordGroup = "$costCentre/$ifrsSubTypeCode";
            }

            // Start of new group.
            if ($group != $currentRecordGroup) {
                // Write out the last (previous) group then start new one
                $codes = [
                    'groupCostCentre' => $groupCostCentre,
                    'group'           => $group,
                    'yearString'      => $yearString,
                    'groupDepnCode'   => $groupDepnCode
                ];

                self::addGroup(
                    $data,
                    $subTotalDepn,
                    $subTotalRRDepn,
                    $totalAmount,
                    $totalHRAAmount,
                    $codes,
                );

                // Get sub type accounts
                self::getIFRSAccount($ifrsSubTypeCode, $accountCode1, $accountCode2, $accountCode3, $accountCode4);
                $groupDepnCode = $accountCode2;

                // Start new group - record this group's signature
                $group = "$costCentre/$ifrsSubTypeCode";
                $groupCostCentre = $costCentre;

                // Reset Totals for next group
                $subTotalDepn = 0;
                $subTotalRRDepn = 0;
            }

            $histDepn = self::getBfwdData($depn->ca_fixed_asset_id, CommonConstant::DATABASE_VALUE_YES);

            $cyDepn = $depn->CDPN;
            $cyHcDepn = $histDepn ? $histDepn->CYDPN : 0;
            $rrDepn = Math::subCurrency([$cyDepn, $cyHcDepn]);
            $subTotalDepn = Math::addCurrency([$subTotalDepn, $cyDepn]);
            $subTotalRRDepn = Math::addCurrency([$subTotalRRDepn, $rrDepn]);
        }


        $codes = [
            'costCentre'    => $costCentre,
            'group'         => $group,
            'yearString'    => $yearString,
            'groupDepnCode' => $groupDepnCode,
            'ifrsSubType'   => $ifrsSubType
        ];

        self::addLastGroup(
            $data,
            $subTotalDepn,
            $subTotalRRDepn,
            $totalAmount,
            $totalHRAAmount,
            $codes,
        );

        self::addTotals(
            $data,
            $totalAmount,
            $totalHRAAmount,
            $codes,
        );
    }

    private static function setYECodes($bfIfrsSubType, &$ifrsSubType, $bfIfrsSubTypeCode, &$ifrsSubTypeCode)
    {
        if ($bfIfrsSubType != $ifrsSubType) {
            $ifrsSubType = self::isYearEnd() ? $bfIfrsSubType : $ifrsSubType;
            $ifrsSubTypeCode = self::isYearEnd() ? $bfIfrsSubTypeCode : $ifrsSubTypeCode;
        }
    }

    private static function addGroup(
        &$data,
        $subTotalDepn,
        $subTotalRRDepn,
        &$totalAmount,
        &$totalHRAAmount,
        $codes
    ) {
        // Write out the last group.
        $groupCostCentre = array_get($codes, 'groupCostCentre');

        // No point if both zero
        if (Math::notEqualCurrency($subTotalDepn, 0) || Math::notEqualCurrency($subTotalRRDepn, 0)) {
            $hra = self::isHRA($groupCostCentre);
            $codes['hra'] = $hra;

            self:: addIandELines($data, $subTotalDepn, $totalAmount, $totalHRAAmount, $codes);
            self:: addRRLines($data, $subTotalRRDepn, $codes);
        }
    }

    private static function addRRLines(&$data, $subTotalRRDepn, $codes)
    {
        // If there is a difference in depreciation the depreciate the RR.
        if (Math::notEqualCurrency($subTotalRRDepn, 0)) {
            if ($codes['hra']) {
                self::printLine(
                    $data,
                    NC::DR,
                    NC::RR_FOR_DEPN,
                    NC::BS_ACCOUNT_HRA,
                    $subTotalRRDepn,
                    "{$codes['group']} RR depn. {$codes['yearString']}"
                );
                self::printLine(
                    $data,
                    NC::CR,
                    NC::CAA_FOR_RR_DEPN,
                    NC::BS_ACCOUNT_HRA,
                    $subTotalRRDepn,
                    "{$codes['group']} RR depn. {$codes['yearString']}"
                );
            } else {
                self::printLine(
                    $data,
                    NC::DR,
                    NC::RR_FOR_DEPN,
                    NC::BS_ACCOUNT,
                    $subTotalRRDepn,
                    "{$codes['group']} RR depn. {$codes['yearString']}"
                );
                self::printLine(
                    $data,
                    NC::CR,
                    NC::CAA_FOR_RR_DEPN,
                    NC::BS_ACCOUNT,
                    $subTotalRRDepn,
                    "{$codes['group']} RR depn. {$codes['yearString']}"
                );
            }
        }
    }


    private static function addIandELines(&$data, $subTotalDepn, &$totalAmount, &$totalHRAAmount, $codes)
    {
        if (Math::notEqualCurrency($subTotalDepn, 0)) {
            $totalAmount = Math::addCurrency([$totalAmount, $subTotalDepn]);

            // Add total to HRA if housing.
            if ($codes['hra']) {
                $totalHRAAmount = Math::addCurrency([$totalHRAAmount, $subTotalDepn]);
            }

            //Charge Line for Depn.
            self::printLine(
                $data,
                NC::DR,
                NC::DEPN_CHG_CODE,
                $codes['groupCostCentre'],
                $subTotalDepn,
                "{$codes['group']} depn. {$codes['yearString']}"
            );

            // Credit Accumulated Depn.
            if ($codes['hra']) {
                self::printLine(
                    $data,
                    NC::CR,
                    $codes['groupCostCentre'],
                    NC::BS_ACCOUNT_HRA,
                    $subTotalDepn,
                    "{$codes['group']} depn. {$codes['yearString']}"
                );
            } else {
                self::printLine(
                    $data,
                    NC::CR,
                    $codes['groupCostCentre'],
                    NC::BS_ACCOUNT,
                    $subTotalDepn,
                    "{$codes['group']} depn. {$codes['yearString']}"
                );
            }
        }
    }

    private static function addTotals(
        &$data,
        $totalAmount,
        $totalHRAAmount,
        $codes
    ) {
        // Write out the reversals of the totals.
        $yearString = array_get($codes, 'yearString');

        // HRA Reversal.

        // Total debit to CAA.
        self::printLine(
            $data,
            NC::DR,
            NC::HRA_FOR_DEPN,
            NC::BS_ACCOUNT_HRA,
            $totalHRAAmount,
            "HRA Depreciation $yearString"
        );

        // Reverse through HRA.
        self::printLine(
            $data,
            NC::CR,
            NC::SMR_ACC_HRA,
            NC::BS_ACCOUNT_HRA,
            $totalHRAAmount,
            "HRA Depreciation $yearString"
        );

        // Deduct to find the rmainder to reverse.
        $totalAmount = Math::subCurrency([$totalAmount, $totalHRAAmount]);

        // GF Reversal.
        // Total debit to CAA.
        self::printLine(
            $data,
            NC::DR,
            NC::CAA_FOR_DEPN,
            NC::BS_ACCOUNT,
            $totalAmount,
            "Depreciation $yearString"
        );

        // Reverse through SMR.
        self::printLine(
            $data,
            NC::CR,
            NC::SMR,
            NC::BS_ACCOUNT,
            $totalAmount,
            "Depreciation $yearString"
        );
    }


    private static function addLastGroup(
        &$data,
        $subTotalDepn,
        $subTotalRRDepn,
        &$totalAmount,
        &$totalHRAAmount,
        $codes
    ) {
        // Write out the last group.
        $costCentre = array_get($codes, 'costCentre');
        $group = array_get($codes, 'group');
        $yearString = array_get($codes, 'yearString');
        $groupDepnCode = array_get($codes, 'groupDepnCode');
        $ifrsSubType = array_get($codes, 'ifrsSubType');

        // No point if both zero
        if (Math::notEqualCurrency($subTotalDepn, 0) || Math::notEqualCurrency($subTotalRRDepn, 0)) {
            $hra = self::isHRA($costCentre);

            $totalAmount = Math::addCurrency([$totalAmount, $subTotalDepn]);

            // Add total to HRA if housing.
            if ($hra) {
                $totalHRAAmount = Math::addCurrency([$totalHRAAmount, $subTotalDepn]);
            }

            if (Math::notEqualCurrency($subTotalDepn, 0)) {
                //Charge Line for Depn.
                self::printLine(
                    $data,
                    NC::DR,
                    NC::DEPN_CHG_CODE,
                    $costCentre,
                    $subTotalDepn,
                    "$group depn. $yearString"
                );

                // Credit Accumulated Depn.
                if ($hra) {
                    self::printLine(
                        $data,
                        NC::CR,
                        $groupDepnCode,
                        NC::BS_ACCOUNT_HRA,
                        $subTotalDepn,
                        "$group depn. $yearString"
                    );
                } else {
                    self::printLine(
                        $data,
                        NC::CR,
                        $groupDepnCode,
                        NC::BS_ACCOUNT,
                        $subTotalDepn,
                        "$group depn. $yearString"
                    );
                }
            }

            // If there is a difference in dpereciation the depreciate the RR.
            if (Math::notEqualCurrency($subTotalRRDepn, 0)) {
                self::printLine(
                    $data,
                    NC::DR,
                    NC::RR_FOR_DEPN,
                    NC::BS_ACCOUNT,
                    $subTotalRRDepn,
                    "$costCentre/$ifrsSubType RR depn. $yearString"
                );
                self::printLine(
                    $data,
                    NC::CR,
                    NC::CAA_FOR_RR_DEPN,
                    NC::BS_ACCOUNT,
                    $subTotalRRDepn,
                    "$costCentre/$ifrsSubType RR depn. $yearString"
                );
            }
        }
    }

    private static function getBFIFRSSubType($depn)
    {
        if ($depn) {
            return $depn->bf_ca_ifrs_category_desc;
        }
    }

    private static function getBFIFRSSubTypeCode($depn)
    {
        if ($depn) {
            return $depn->bf_ca_ifrs_category_code;
        }
    }

    private static function getIFRSSubType($depn)
    {
        if ($depn) {
            return $depn->ca_ifrs_category_desc;
        }
    }

    private static function getIFRSSubTypeCode($depn)
    {
        if ($depn) {
            return $depn->ca_ifrs_category_code;
        }
    }

    private static function getAccountCode($depn)
    {
        if ($depn) {
            return $depn->account_code;
        }
    }

    private static function printLine(&$data, $post, $account, $costCentre, $amount, $text)
    {
        if ($post == NC::DR) {
            $amount = Common::numberFormat(Math::negateCurrency($amount), thousandsSep: ',');
        } else {
            $amount = Common::numberFormat($amount, thousandsSep: ',');
        }


        self::addJournalRow(
            $data,
            $post,
            $account,
            $costCentre,
            $amount,
            $text
        );
    }
}
