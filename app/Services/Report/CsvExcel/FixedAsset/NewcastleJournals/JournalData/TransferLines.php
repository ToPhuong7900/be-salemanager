<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\NewcastleJournals\JournalData;

use DateTime;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Math;
use Tfcloud\Models\CaTransactionType;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\NewcastleJournals\NewcastleConstant as NC;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\NewcastleJournals\Traits\JournalOutputTrait;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\NewcastleJournals\Traits\JournalDataTrait;

class TransferLines
{
    use JournalOutputTrait;
    use JournalDataTrait;

    protected const FROM_YMD = '!Y-m-d';

    public static function addLines(&$data, $yearString)
    {
        self::addTransferLines($data, $yearString);
    }

    private static function addTransferLines(&$data, $yearString)
    {
        $tfrData = self::getTransactionList(CaTransactionType::CATEGORY_TRANSFER);

        foreach ($tfrData as $transfer) {
            $codes = self::getCodes($transfer, $yearString);

            $cvAssetValue = Math::negateCurrency($transfer->cv_asset);
            $cvLandValue = Math::negateCurrency($transfer->cv_land);
            $cvValue = Math::addCurrency([$cvAssetValue, $cvLandValue]);

            if (Math::notEqualCurrency($cvValue, 0)) {
                self::addTFRLines($data, $transfer, $codes);
            }
        }
    }

    private static function addTFRLines(&$data, $transfer, $codes)
    {
        $cvAssetValue = Math::negateCurrency($transfer->cv_asset);
        $cvLandValue = Math::negateCurrency($transfer->cv_land);
        $cvValue = Math::addCurrency([$cvAssetValue, $cvLandValue]);

        $msg = "{$codes['ifrsSubTypeCode']} {$codes['yearString']} , $transfer->ca_fixed_asset_code";

        $bfwdDepnData = self::getBfwdData(
            $transfer->ca_fixed_asset_id,
            CommonConstant::DATABASE_VALUE_NO
        );


        $bfwdDepn = $codes['yeTransfer'] ?
            Math::addCurrency([$bfwdDepnData->COpenDPN, $bfwdDepnData->CYDPN]) : $bfwdDepnData->COpenDPN;

        $bfwdGBV = $bfwdDepnData->COpenGBV;
        $bfwdImpA = $bfwdDepnData->COpenIMPA;
        $bfwdImpL = $bfwdDepnData->COpenIMPL;
        $bfwdImp = Math::addCurrency([$bfwdImpA, $bfwdImpL]);

        if (Math::greaterThanCurrency($cvValue, 0)) {
            $msg = "TFR into $msg";

            self::printLine(
                $data,
                NC::DR,
                $codes['accountCode1'],
                $codes['bsAccount'],
                $bfwdGBV,
                $msg
            );

            if (Math::notEqualCurrency($bfwdDepn, 0)) {
                self::printLine(
                    $data,
                    NC::CR,
                    $codes['accountCode2'],
                    $codes['bsAccount'],
                    $bfwdDepn,
                    $msg
                );
            }

            if (Math::notEqualCurrency($bfwdImp, 0)) {
                self::printLine(
                    $data,
                    NC::CR,
                    $codes['accountCode1'],
                    $codes['bsAccount'],
                    $bfwdImp,
                    $msg
                );
            }
        } else {
            $msg = "TFR out of $msg";

            self::printLine(
                $data,
                NC::CR,
                $codes['accountCode1'],
                $codes['bsAccount'],
                $bfwdGBV,
                $msg
            );

            if (Math::notEqualCurrency($bfwdDepn, 0)) {
                self::printLine(
                    $data,
                    NC::DR,
                    $codes['accountCode2'],
                    $codes['bsAccount'],
                    $bfwdDepn,
                    $msg
                );
            }

            if (Math::notEqualCurrency($bfwdImp, 0)) {
                self::printLine(
                    $data,
                    NC::DR,
                    $codes['accountCode1'],
                    $codes['bsAccount'],
                    $bfwdImp,
                    $msg
                );
            }
        }
    }

    private static function getCodes($record, $yearString)
    {
        $caAssetCode = $record->ca_asset_code;
        $ifrsSubTypeCode = self::getIFRSSubTypeCode($record);
        self::getIFRSAccount($ifrsSubTypeCode, $accountCode1, $accountCode2, $accountCode3, $accountCode4, $noHC);

        // Second part of the group is the cost centre.
        $costCentre = self::getAccountCode($record);
        $hra = self::isAcc1Hra($accountCode1);

        if ($hra) {
            $bsAccount = NC::BS_ACCOUNT_HRA;
            $smrAccount = NC::SMR_ACC_HRA;
        } else {
            $bsAccount = NC::BS_ACCOUNT;
            $smrAccount = NC::SMR;
        }

        if ($record->effective_date) {
            $effectiveDate = DateTime::createFromFormat(self::FROM_YMD, $record->effective_date);
            $yeTransfer = $effectiveDate->format('d/m') == '31/03';
        } else {
            $yeTransfer = false;
        }


        return [
            'caAssetCode'     => $caAssetCode,
            'ifrsSubTypeCode' => $ifrsSubTypeCode,
            'hra'             => $hra,
            'costCentre'      => $costCentre,
            'accountCode1'    => $accountCode1,
            'accountCode2'    => $accountCode2,
            'accountCode3'    => $accountCode3,
            'accountCode4'    => $accountCode4,
            'noHC'            => $noHC,
            'bsAccount'       => $bsAccount,
            'smrAccount'      => $smrAccount,
            'yeTransfer'      => $yeTransfer,
            'yearString'      => $yearString
        ];
    }

    private static function isAcc1Hra($accountCode1)
    {
        return $accountCode1 == 'X001';
    }

    private static function getIFRSSubTypeCode($depn)
    {
        if ($depn) {
            return $depn->ca_ifrs_category_code;
        }
    }

    private static function getAccountCode($depn)
    {
        if ($depn) {
            return $depn->ca_account_code;
        }
    }

    private static function printLine(&$data, $post, $account, $costCentre, $amount, $text)
    {
        if ($post == NC::DR) {
            $amount = Common::numberFormat(Math::negateCurrency($amount), thousandsSep: ',');
        } else {
            $amount = Common::numberFormat($amount, thousandsSep: ',');
        }


        self::addJournalRow(
            $data,
            $post,
            $account,
            $costCentre,
            $amount,
            $text
        );
    }
}
