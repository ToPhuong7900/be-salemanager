<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\NewcastleJournals\JournalData;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Math;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\NewcastleJournals\NewcastleConstant as NC;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\NewcastleJournals\Traits\JournalOutputTrait;
use Tfcloud\Services\Report\CsvExcel\FixedAsset\NewcastleJournals\Traits\JournalDataTrait;

class ValuationLines
{
    use JournalOutputTrait;
    use JournalDataTrait;

    public static function addLines(&$data)
    {
        self::addValuationLines($data);
    }

    private static function addValuationLines(&$data)
    {
        $revalData = self::getRevalData();

        foreach ($revalData as $reval) {
            $codes = self::getCodes($reval);

            $balanceFixedAsset = 0;

            self::addWOLines($data, $reval, $codes, $balanceFixedAsset);
            self::addIandELines($data, $reval, $codes, $balanceFixedAsset);

            // Revaluation reserve.
            if ($codes['noHC']) {
                $rrGain = 0;
            } else {
                $rrGain = $reval->reval_reserve;
            }

            if (Math::notEqualCurrency($rrGain, 0)) {
                if (Math::greaterThanCurrency($rrGain, 0)) {
                    self::printLine(
                        $data,
                        NC::CR,
                        NC::RR_FOR_REVAL_GAIN,
                        $codes['bsAccount'],
                        $rrGain,
                        "Gain in RR  {$codes['caAssetCode']}"
                    );
                } else {
                    self::printLine(
                        $data,
                        NC::DR,
                        NC::RR_FOR_REVAL_LOSS,
                        $codes['bsAccount'],
                        $rrGain,
                        "Loss in RR  {$codes['caAssetCode']}"
                    );
                }
                $balanceFixedAsset = Math::subCurrency([$balanceFixedAsset, $rrGain]);
            }

            // Post balance to Fixed Asset.
            // AcDepn + AcImp + I&E - RR.
            $msg = "Change to FA {$codes['caAssetCode']}";
            if (Math::lessThanCurrency($balanceFixedAsset, 0)) {
                $balanceFixedAsset = Math::negateCurrency($balanceFixedAsset);

                self::printLine(
                    $data,
                    NC::DR,
                    $codes['accountCode1'],
                    $codes['bsAccount'],
                    $balanceFixedAsset,
                    $msg
                );
            } elseif (Math::notEqualCurrency($balanceFixedAsset, 0)) {
                self::printLine(
                    $data,
                    NC::CR,
                    $codes['accountCode1'],
                    $codes['bsAccount'],
                    $balanceFixedAsset,
                    $msg
                );
            }
        }
    }

    private static function addWOLines(&$data, $reval, $codes, &$balanceFixedAsset)
    {
        // Get WO Depn.
        $depnWORR = $reval->depn_wo_rr;
        $depnWOSD = $reval->depn_wo_sd;
        $depnWO = Math::addCurrency([$depnWORR, $depnWOSD]);

        // Write Off Accum Depn
        if (Math::greaterThanCurrency($depnWO, 0)) {
            self::printLine(
                $data,
                NC::DR,
                $codes['accountCode2'],
                $codes['bsAccount'],
                $depnWO,
                "W/off Depn. {$codes['caAssetCode']}"
            );

            $balanceFixedAsset = Math::addCurrency([$balanceFixedAsset, $depnWO]);
        }

        // Accumulated Impairment.
        $impWOAsset = $reval->imp_wo_rr;
        $impWOSD = $reval->imp_wo_sd;
        $impWO = Math::addCurrency([$impWOAsset, $impWOSD]);

        // Total Reversed Depn (ROI + ROL).
        $roiDepnReversal = $reval->imp_depn_reversal;
        $rolDepnReversal = $reval->loss_depn_reversal;
        $totalDepnReversal = Math::addCurrency([$roiDepnReversal, $rolDepnReversal]);

        // Add ROI Depn Reversal to Written off Depn
        if (Math::notEqualCurrency($roiDepnReversal, 0)) {
            $impWO = Math::addCurrency([$impWO, $roiDepnReversal]);
        }

        // If we have a depn adjustment add that in.
        if (Math::greaterThanCurrency($totalDepnReversal, 0)) {
            self::printLine(
                $data,
                NC::DR,
                $codes['accountCode2'],
                $codes['bsAccount'],
                $totalDepnReversal,
                "W/off Depn from ROI/ROL. {$codes['caAssetCode']}"
            );

            $balanceFixedAsset = Math::addCurrency([$balanceFixedAsset, $totalDepnReversal]);
        }

        // Write off Accum Imp.
        if (Math::notEqualCurrency($impWO, 0)) {
            self::printLine(
                $data,
                NC::DR,
                $codes['accountCode1'],
                $codes['bsAccount'],
                $impWO,
                "W/off impairment  {$codes['caAssetCode']}"
            );

            $balanceFixedAsset = Math::addCurrency([$balanceFixedAsset, $impWO]);
        }
    }

    private static function addIandELines(&$data, $reval, $codes, &$balanceFixedAsset)
    {
        // Charge to I and E
        // Investment asset always go to I and E.
        $iAndEAsset = $reval->reval_asset_value;
        $iAndLand = $reval->reval_land_value;
        if ($codes['noHC']) {
            $iAndECharge = Math::addCurrency([$iAndEAsset, $iAndLand]);

            if (Math::notEqualCurrency($iAndECharge, 0)) {
                if (Math::greaterThanCurrency($iAndECharge, 0)) {
                    $msg = "Gain I&E {$codes['caAssetCode']}";
                    self::printLine(
                        $data,
                        NC::CR,
                        NC::REV_LOSS_CODE_INV,
                        $codes['costCentre'],
                        $iAndECharge,
                        $msg
                    );

                    $balanceFixedAsset = Math::subCurrency([$balanceFixedAsset, $iAndECharge]);

                    // Credit CAA.
                    self::printLine(
                        $data,
                        NC::CR,
                        NC::CAA_FOR_REVAL,
                        $codes['bsAccount'],
                        $iAndECharge,
                        $msg
                    );

                    // Debit SMR
                    self::printLine(
                        $data,
                        NC::DR,
                        $codes['smrAccount'],
                        $codes['bsAccount'],
                        $iAndECharge,
                        $msg
                    );
                } else {
                    $iAndECharge = Math::negateCurrency($iAndECharge);
                    $msg = "Loss to I&E {$codes['caAssetCode']}";
                    self::printLine(
                        $data,
                        NC::DR,
                        NC::REV_LOSS_CODE_INV,
                        $codes['costCentre'],
                        $iAndECharge,
                        $msg
                    );

                    $balanceFixedAsset = Math::addCurrency([$balanceFixedAsset, $iAndECharge]);

                    // Credit CAA.
                    self::printLine(
                        $data,
                        NC::DR,
                        NC::CAA_FOR_REVAL,
                        $codes['bsAccount'],
                        $iAndECharge,
                        $msg
                    );

                    // Debit SMR
                    self::printLine(
                        $data,
                        NC::CR,
                        $codes['smrAccount'],
                        $codes['bsAccount'],
                        $iAndECharge,
                        $msg
                    );
                }
            }
        } else {
            // Non Investment Assets.
            // Debit any HC Loss charge to I&E.
            $iAndECharge = Math::negateCurrency(Math::addCurrency([$iAndEAsset, $iAndLand]));

            if (Math::notEqualCurrency($iAndECharge, 0)) {
                $msg = "Reval. loss to I&E {$codes['caAssetCode']}";

                if ($codes['ifrsSubType'] == 'Surplus Assets') {
                    self::printLine(
                        $data,
                        NC::DR,
                        NC::REV_LOSS_CODE_SURPLUS,
                        $codes['costCentre'],
                        $iAndECharge,
                        $msg
                    );
                } else {
                    self::printLine(
                        $data,
                        NC::DR,
                        NC::REV_LOSS_CODE,
                        $codes['costCentre'],
                        $iAndECharge,
                        $msg
                    );
                }

                $balanceFixedAsset = Math::addCurrency([$balanceFixedAsset, $iAndECharge]);

                // Credit CAA.
                self::printLine(
                    $data,
                    NC::DR,
                    NC::CAA_FOR_REVAL,
                    $codes['bsAccount'],
                    $iAndECharge,
                    $msg
                );

                // Debit SMR
                self::printLine(
                    $data,
                    NC::CR,
                    $codes['smrAccount'],
                    $codes['bsAccount'],
                    $iAndECharge,
                    $msg
                );
            }
        }
    }

    private static function getCodes($record)
    {
        $caAssetCode = $record->ca_asset_code;
        $ifrsSubType = self::getIFRSSubType($record);
        $ifrsSubTypeCode = self::getIFRSSubTypeCode($record);
        self::getIFRSAccount($ifrsSubTypeCode, $accountCode1, $accountCode2, $accountCode3, $accountCode4, $noHC);

        // Second part of the group is the cost centre.
        $costCentre = self::getAccountCode($record);
        $hra = self::isHRA($costCentre);

        if ($hra) {
            $bsAccount = NC::BS_ACCOUNT_HRA;
            $smrAccount = NC::SMR_ACC_HRA;
        } else {
            $bsAccount = NC::BS_ACCOUNT;
            $smrAccount = NC::SMR;
        }

        return [
            'caAssetCode'     => $caAssetCode,
            'ifrsSubType'     => $ifrsSubType,
            'ifrsSubTypeCode' => $ifrsSubTypeCode,
            'hra'             => $hra,
            'costCentre'      => $costCentre,
            'accountCode1'    => $accountCode1,
            'accountCode2'    => $accountCode2,
            'accountCode3'    => $accountCode3,
            'accountCode4'    => $accountCode4,
            'noHC'            => $noHC,
            'bsAccount'       => $bsAccount,
            'smrAccount'      => $smrAccount
        ];
    }

    private static function getIFRSSubType($depn)
    {
        if ($depn) {
            return $depn->ifrs_desc;
        }
    }

    private static function getIFRSSubTypeCode($depn)
    {
        if ($depn) {
            return $depn->ifrs_code;
        }
    }

    private static function getAccountCode($depn)
    {
        if ($depn) {
            return $depn->account_code;
        }
    }

    private static function printLine(&$data, $post, $account, $costCentre, $amount, $text)
    {
        if ($post == NC::DR) {
            $amount = Common::numberFormat(Math::negateCurrency($amount), thousandsSep: ',');
        } else {
            $amount = Common::numberFormat($amount, thousandsSep: ',');
        }


        self::addJournalRow(
            $data,
            $post,
            $account,
            $costCentre,
            $amount,
            $text
        );
    }
}
