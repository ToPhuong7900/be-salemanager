<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\NMNIJournals;

use Tfcloud\Lib\Math;
use Tfcloud\Models\SiteGroup;
use Tfcloud\Models\Views\VwFar09;
use Tfcloud\Services\PermissionService;

class FarCliNMNI02ReportService extends NMNIJournalPostingService
{
    protected $yearString = "";
    private $journalData = [];
    private $caTransDesc = 'disposal';
    private $rrOBDesc = 'Balance Sheet Reval Res Op ba';

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);

        $headerText = "Fixed Assets revaluation $this->yearString";

        $this->addJournalHeader($this->journalData, $headerText);
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        /**
         * /////////////////////////////////////////////////
         * // Revaluation Reserve  //
         * /////////////////////////////////////////////////
         */

        $this->prepareData();

        return $this->journalData;
    }

    private function prepareData()
    {
        $lCount = 0;

        // Get additions data from database
        $assetData = $this->getData();

        foreach ($assetData as $asset) {
            $lCount++;

            $faTypeCode = $asset->ca_fixed_asset_type_code;

            $accDepnCode = $this->getGLAccDepnCode($faTypeCode);
            $additionCode = $this->getGLAdditionsCode($faTypeCode);

            $codeDescDepn = "Balance Sheet $faTypeCode - Acc Depn";
            $codeDescAdd = "Balance Sheet $faTypeCode - Additions";

            $rrOB = $asset->rr_opening_balance;
            $rrMovement = $asset->rr_movement;
            $cfRR = $asset->cf_rr;

            $narrative = "$asset->ca_fixed_asset_type_desc Dep Rev $this->yearString";

            // Opening Balance
            $this->addJournalRow(
                $this->journalData,
                self::RR_OB,
                $this->rrOBDesc,
                0,
                $rrOB,
                $narrative
            );

            // Carried Forwards
            $this->addJournalRow(
                $this->journalData,
                $accDepnCode,
                $codeDescDepn,
                $cfRR,
                0,
                $narrative
            );

            // Movement
            $this->addJournalRow(
                $this->journalData,
                $additionCode,
                $codeDescAdd,
                0,
                $rrMovement,
                $narrative
            );
        }   //foreach asset.

        return;
    }

    private function getData()
    {
        return VwFar09::join(
            'ca_fixed_asset',
            'ca_fixed_asset.ca_fixed_asset_id',
            '=',
            'vw_far09.ca_fixed_asset_id'
        )
        ->join(
            'ca_fixed_asset_type',
            'ca_fixed_asset_type.ca_fixed_asset_type_id',
            '=',
            'ca_fixed_asset.ca_fixed_asset_type_id'
        )
        ->where('vw_far09.site_group_id', SiteGroup::get()->site_group_id)
        ->select(
            [
                'ca_fixed_asset_type.ca_fixed_asset_type_code',
                'ca_fixed_asset_type.ca_fixed_asset_type_desc',
                \DB::raw("CONCAT_WS('-', ca_fixed_asset_type_code, ca_fixed_asset_type_desc) AS faAssetType"),
                \DB::raw('SUM(vw_far09.`BFwd RR`) as rr_opening_balance'),
                \DB::raw('SUM(vw_far09.`CY Transaction Asset` + vw_far09.`CY Transaction Land`) as rr_movement'),
                \DB::raw(
                    'SUM(vw_far09.`BFwd RR` + (vw_far09.`CY Transaction Asset` + '
                    . 'vw_far09.`CY Transaction Land`)) as cf_rr'
                )
            ]
        )
        ->groupBy('ca_fixed_asset_type.ca_fixed_asset_type_id')
        ->orderBy('ca_fixed_asset_type.ca_fixed_asset_type_code')
        ->get();
    }
}
