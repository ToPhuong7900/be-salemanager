<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\NMNIJournals;

use Tfcloud\Lib\Math;
use Tfcloud\Models\Views\VwFar07;
use Tfcloud\Services\PermissionService;

class FarCliNMNI01ReportService extends NMNIJournalPostingService
{
    protected $yearString = "";
    private $journalData = [];

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);

        $headerText = "Fixed asset depreciation charge $this->yearString";

        $this->addJournalHeader($this->journalData, $headerText);
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        /**
         * /////////////////////////////////////////////////
         * // Depreciation and Impairments  //
         * /////////////////////////////////////////////////
         */

        $this->prepareData();

        return $this->journalData;
    }

    private function prepareData()
    {
        $lCount = 0;

        $assetData = $this->getData()->get();

        $totals = $this->getData(true)->first();
        $totalDepn = $totals->cyDepn;
        $totalImp = $totals->cyImp;

        $intangibles = $this->getData(true, true)->first();
        $totalAmort = $intangibles->cyDepn;


        // Depreciation.
        if (Math::greaterThanCurrency($totalDepn, 0)) {
            $this->addJournalRow(
                $this->journalData,
                self::P_AND_L_DEPN,
                'UMMusServHOD DeprCharge',
                $totalDepn,
                0,
                "FA depn charge $this->yearString"
            );
        }

        foreach ($assetData as $asset) {
            $lCount++;

            $faTypeCode = $asset->ca_fixed_asset_type_code;

            $accDepnCode = $this->getGLAccDepnCode($faTypeCode);

            $codeDesc = "Balance Sheet $faTypeCode - $asset->ca_fixed_asset_type_desc Acc Depn";

            $depn = $asset->cyDepn;

            $narrative = "$faTypeCode Depn charge $this->yearString";

            if (Math::greaterThanCurrency($depn, 0)) {
                $this->addJournalRow(
                    $this->journalData,
                    $accDepnCode,
                    $codeDesc,
                    0,
                    $depn,
                    $narrative
                );
            }
        }   //foreach asset.

        // Impairment.
        if (Math::greaterThanCurrency($totalDepn, 0)) {
            $this->addJournalRow(
                $this->journalData,
                self::P_AND_L_IMP,
                'UMMusServHOD ImpCharge',
                $totalImp,
                0,
                "FA depn charge $this->yearString"
            );
        }

        foreach ($assetData as $asset) {
            $lCount++;

            $faTypeCode = $asset->ca_fixed_asset_type_code;

            $accDepnCode = $this->getGLAccDepnCode($faTypeCode);

            $codeDesc = "Balance Sheet $faTypeCode - $asset->ca_fixed_asset_type_desc Acc Imp";

            $imp = $asset->cyImp;

            $narrative = "$faTypeCode Depn charge $this->yearString";

            if (Math::greaterThanCurrency($depn, 0)) {
                $this->addJournalRow(
                    $this->journalData,
                    $accDepnCode,
                    $codeDesc,
                    0,
                    $imp,
                    $narrative
                );
            }
        }   //foreach asset.

        // Intangible Asset Amortisation.
        if (Math::greaterThanCurrency($totalAmort, 0)) {
            $this->addJournalRow(
                $this->journalData,
                self::P_AND_L_DEPN,
                'UMMusServHOD DeprCharge',
                $totalAmort,
                0,
                "Intangible amortisation $this->yearString"
            );

            $this->addJournalRow(
                $this->journalData,
                self::P_AND_L_DEPN,
                'Balance Sheet Intangible Assets - Amortisation',
                0,
                $totalAmort,
                "Intangible amortisation $this->yearString"
            );
        }

        return;
    }

    private function getData($grandTotal = false, $intangible = false)
    {
        $query = VwFar07::userSiteGroup()
            ->leftJoin(
                'ca_ifrs_category as bfwd_ca_ifrs_category',
                'bfwd_ca_ifrs_category.ca_ifrs_category_id',
                '=',
                'vw_far07.bfwd_ca_ifrs_category_id'
            )
            ->join(
                'ca_fixed_asset',
                'ca_fixed_asset.ca_fixed_asset_id',
                '=',
                'vw_far07.ca_fixed_asset_id'
            )
            ->join(
                'ca_fixed_asset_type',
                'ca_fixed_asset_type.ca_fixed_asset_type_id',
                '=',
                'ca_fixed_asset.ca_fixed_asset_type_id'
            )
            ->where('vw_far07.ca_balance_historic', 'N')
            ->select(
                [
                    'ca_fixed_asset_type.ca_fixed_asset_type_code',
                    'ca_fixed_asset_type.ca_fixed_asset_type_desc',
                    \DB::raw("CONCAT_WS('-', ca_fixed_asset_type_code, ca_fixed_asset_type_desc) AS faAssetType"),
                    \DB::raw('SUM(vw_far07.`CY Depreciation` * -1) as cyDepn'),
                    \DB::raw('SUM(vw_far07.`IMP Asset Value` * -1 + vw_far07.`IMP Land Value` * -1) as cyImp'),
                ]
            );

        if ($intangible) {
            $query->where('ca_fixed_asset_type_code', 'IA');
        } else {
            $query->where('ca_fixed_asset_type_code', '<>', 'IA');
        }

        if (!$grandTotal) {
            $query->groupBy('ca_fixed_asset_type.ca_fixed_asset_type_id')
                ->orderBy('ca_fixed_asset_type.ca_fixed_asset_type_code');
        }

        return $query;
    }
}
