<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\NMNIJournals;

use Tfcloud\Services\PermissionService;

class FarCliNMNITrnBaseReportService extends NMNIJournalPostingService
{
    protected $yearString = "";
    private $journalData = [];

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);

        $headerText = "To reanalyse $this->yearString $this->caTransDesc from project codes 5**** "
            . "to balance sheet code 99999****";

        $this->addJournalHeader($this->journalData, $headerText);
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        $this->prepareData();

        return $this->journalData;
    }

    private function prepareData()
    {
        $lCount = 0;

        // Get additions data from database
        $assetData = $this->retrieveGroupedTransactionList($this->caTransType)->get();

        foreach ($assetData as $asset) {
            $lCount++;

            $faTypeCode = $asset->ca_fixed_asset_type_code;

            $code = $this->getGLAdditionsCode($faTypeCode);

            $codeDesc = "Balance Sheet $faTypeCode $this->caTransDesc";

            $dr = $asset->transValue;

            $narrative = "$faTypeCode $this->caTransDesc $this->yearString";

            $this->addJournalRow(
                $this->journalData,
                $code,
                $codeDesc,
                $dr,
                0,
                $narrative
            );
        }   //foreach asset.

        return;
    }
}
