<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\NMNIJournals;

use Tfcloud\Lib\Math;
use Tfcloud\Models\SiteGroup;
use Tfcloud\Models\Views\VwFar14;
use Tfcloud\Services\PermissionService;

class FarCliNMNI03ReportService extends NMNIJournalPostingService
{
    protected $yearString = "";
    private $journalData = [];
    private $caTransDesc = 'disposal';
    private $stockProv = 'UFTM Income stockprov';

    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);

        $headerText = "To reanalyse disposals to Profit/Loss on disposals a/c";

        $this->addJournalHeader($this->journalData, $headerText);
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        /**
         * /////////////////////////////////////////////////
         * // Disposals  //
         * /////////////////////////////////////////////////
         */

        $this->prepareData();

        return $this->journalData;
    }

    private function prepareData()
    {
        $lCount = 0;

        // Get additions data from database
        $assetData = $this->getData();

        foreach ($assetData as $asset) {
            $lCount++;

            $faTypeCode = $asset->ca_fixed_asset_type_code;

            $accDepnCode = $this->getGLAccDepnCode($faTypeCode);
            $additionCode = $this->getGLAdditionsCode($faTypeCode);

            $codeDesc = "Balance Sheet $faTypeCode - $asset->ca_fixed_asset_type_desc";

            $acumDepn = $asset->accumulated_depreciation;
            $costs = $asset->costs;

            $narrative = "$faTypeCode accum depn on $this->caTransDesc $this->yearString";

            if (Math::greaterThanCurrency($acumDepn, 0)) {
                $this->addJournalRow(
                    $this->journalData,
                    $accDepnCode,
                    $codeDesc,
                    $acumDepn,
                    0,
                    $narrative
                );

                $this->addJournalRow(
                    $this->journalData,
                    self::STOCK_PROV,
                    $this->stockProv,
                    0,
                    $acumDepn,
                    $narrative
                );
            }

            if (Math::greaterThanCurrency($costs, 0)) {
                $narrative = "$faTypeCode cost on $this->caTransDesc $this->yearString";
                $codeDesc = "Balance Sheet $faTypeCode - Additions";
                $this->addJournalRow(
                    $this->journalData,
                    self::STOCK_PROV,
                    $this->stockProv,
                    $costs,
                    0,
                    $narrative
                );
                $this->addJournalRow(
                    $this->journalData,
                    $additionCode,
                    $codeDesc,
                    0,
                    $costs,
                    $narrative
                );
            }
        }   //foreach asset.

        return;
    }

    private function getData()
    {
        return VwFar14::join(
            'ca_ifrs_category',
            'ca_ifrs_category.ca_ifrs_category_id',
            '=',
            'vw_far14.ca_ifrs_category_id'
        )
        ->join(
            'ca_fixed_asset',
            'ca_fixed_asset.ca_fixed_asset_id',
            '=',
            'vw_far14.ca_fixed_asset_id'
        )
        ->join(
            'ca_fixed_asset_type',
            'ca_fixed_asset_type.ca_fixed_asset_type_id',
            '=',
            'ca_fixed_asset.ca_fixed_asset_type_id'
        )
        ->where('vw_far14.site_group_id', SiteGroup::get()->site_group_id)
        ->select(
            [
                'ca_fixed_asset_type.ca_fixed_asset_type_code',
                'ca_fixed_asset_type.ca_fixed_asset_type_desc',
                \DB::raw("CONCAT_WS('-', ca_fixed_asset_type_code, ca_fixed_asset_type_desc) AS faAssetType"),
                \DB::raw('SUM(vw_far14.`Accumulated Depreciation`) as accumulated_depreciation'),
                \DB::raw('SUM(vw_far14.`Accumulated Impairment`) as accumulated_impairment'),
                \DB::raw('SUM(vw_far14.`Net Consideration`) as net_consideration'),
                \DB::raw('SUM(vw_far14.`Costs`) as costs')
            ]
        )
        ->groupBy('ca_fixed_asset_type.ca_fixed_asset_type_id')
        ->orderBy('ca_fixed_asset_type.ca_fixed_asset_type_code')
        ->get();
    }
}
