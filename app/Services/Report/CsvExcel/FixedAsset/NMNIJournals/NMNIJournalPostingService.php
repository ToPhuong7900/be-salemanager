<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\NMNIJournals;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\FixedAsset\CaFinYearManager;
use Tfcloud\Lib\Math;
use Tfcloud\Models\CaPostingAccount;
use Tfcloud\Models\CaIfrsCategory;
use Tfcloud\Models\CaTransaction;
use Tfcloud\Models\CaTransactionType;
use Tfcloud\Models\SiteGroup;
use Tfcloud\Models\Views\VwFar06;
use Tfcloud\Models\Views\VwFar07;
use Tfcloud\Models\Views\VwCaCurrentFinYear;
use Tfcloud\Services\FixedAsset\BalanceService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\CsvExcel\CsvExcelReportBaseService;

abstract class NMNIJournalPostingService extends CsvExcelReportBaseService
{
    // Capital Codes.
    protected const P_AND_L_DEPN = 1010128701;
    protected const P_AND_L_IMP = 1010128702;

    protected const BS_LB_ADD = 9999960102;
    protected const BS_LB_AC_DEPN = 9999960190;

    protected const BS_PE_ADD = 9999960202;
    protected const BS_PE_AC_DEPN = 9999960290;

    protected const BS_FFE_ADD = 9999960402;
    protected const BS_FFE_AC_DEPN = 9999960490;

    protected const BS_PM_ADD = 9999960702;
    protected const BS_PM_AC_DEPN = 9999960790;

    protected const BS_ITH_ADD = 9999960902;
    protected const BS_ITH_AC_DEPN = 9999960990;

    protected const BS_ITS_ADD = 9999961002;
    protected const BS_ITS_AC_DEPN = 9999961090;

    protected const BS_MV_ADD = 9999961102;
    protected const BS_MV_AC_DEPN = 9999961190;

    protected const BS_HA_ADD = 9999961501;
    protected const BS_HA_AC_DEPN = 99999;

    protected const BS_IA_ADD = 9999961602;
    protected const BS_IA_AMO = 9999961603;

    protected const BS_RR_OP_BAL = 9999981001;

    protected const STOCK_PROV = 2052022924;

    protected const RR_OB = 9999981001;

    // Debit or Credit
    protected const DR = 'DR';
    protected const CR = 'CR';


    protected const COL_GL_ACCOUNT = 1;
    protected const COL_DESC = 2;
    protected const COL_DR = 3;
    protected const COL_CR = 4;
    protected const COL_NARR = 5;

    protected $yearString = null;
    protected $permissionService = null;

    protected $accountCode1 = '';
    protected $accountCode2 = '';
    protected $accountCode3 = '';
    protected $accountCode4 = '';
    protected $accountCode5 = '';
    protected $accountCode6 = '';
    protected $accountCode7 = '';


    public function __construct(PermissionService $permissionService)
    {
        parent::__construct($permissionService);
        $this->permissionService = $permissionService;
        $this->setYearString();
    }

    protected function textContains($toCheck, $toFind)
    {
        return strpos($toCheck, $toFind) > 0;
    }

    protected function addJournalHeader(&$data, $headerText)
    {
        $data[] = $this->newJournalRow(
            'Journal',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        );

        $data[] = $this->newJournalRow(
            $headerText,
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        );

        $data[] = $this->newJournalRow(
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        );

        $data[] = $this->newJournalRow(
            '',
            '',
            self::DR,
            self::CR,
            '',
            '',
            '',
            '',
            ''
        );

        $data[] = $this->newJournalRow(
            'Code',
            'Code Description',
            '£',
            '£',
            'Narrative',
            '',
            '',
            '',
            ''
        );

        return;
    }

    protected function addJournalRow(
        &$data,
        $code,
        $codeDesc,
        $dr,
        $cr,
        $narrative
    ) {

        $drFormatted = Common::numberFormat($dr, false, ',', 2);
        $crFormatted = Common::numberFormat($cr, false, ',', 2);

        $data[] = $this->newJournalRow(
            $code,
            $codeDesc,
            $drFormatted,
            $crFormatted,
            $narrative,
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        );

        return;
    }

    protected function getGLAdditionsCode($faTypeCode)
    {
        switch ($faTypeCode) {
            case 'DHA':
            case 'PHA':
                return self::BS_HA_ADD;
            case 'FFE':
                return self::BS_FFE_ADD;
            case 'IA':
                return self::BS_IA_ADD;
            case 'IT-H':
                return self::BS_ITH_ADD;
            case 'IT-S':
                return self::BS_ITS_ADD;
            case 'LB':
                return self::BS_LB_ADD;
            case 'MV':
                return self::BS_MV_ADD;
            case 'PE':
                return self::BS_PE_ADD;
            case 'PM':
                return self::BS_PM_ADD;
        }
    }

    protected function getGLAccDepnCode($faTypeCode)
    {
        switch ($faTypeCode) {
            case 'DHA':
            case 'PHA':
                return self::BS_HA_AC_DEPN;
            case 'FFE':
                return self::BS_FFE_AC_DEPN;
            case 'IA':
                return self::BS_IA_AMO;
            case 'IT-H':
                return self::BS_ITH_AC_DEPN;
            case 'IT-S':
                return self::BS_ITS_AC_DEPN;
            case 'LB':
                return self::BS_LB_AC_DEPN;
            case 'MV':
                return self::BS_MV_AC_DEPN;
            case 'PE':
                return self::BS_PE_AC_DEPN;
            case 'PM':
                return self::BS_PM_AC_DEPN;
        }
    }

//    protected function getIFRSAccount(
//        $caIFRSCatId,
//        &$accountCode1,
//        &$accountCode2,
//        &$accountCode3,
//        &$accountCode4,
//        &$accountCode5,
//        &$accountCode6,
//        &$accountCode7,
//        &$noHC = false,
//        &$ahs = false,
//        &$shortCode = ''
//    ) {
//        $ifrsCat = CaIfrsCategory::where('ca_ifrs_category_id', $caIFRSCatId)
//            ->where('site_group_id', SiteGroup::get()->site_group_id)
//            ->first();
//
//        $accountCode1 = $ifrsCat->account_code_1;
//        $accountCode2 = $ifrsCat->account_code_2;
//        $accountCode3 = $ifrsCat->account_code_3;
//        $accountCode4 = $ifrsCat->account_code_4;
//
//        // Account code 5 containd the codes for accum imp~in year imp.
//        if ($ifrsCat->account_code_5) {
//            $imp = explode('~', $ifrsCat->account_code_5);
//            if (count($imp) > 1) {
//                $accountCode5 = $imp[1];
//                $accountCode7 = $imp[0];
//            } else {
//                $accountCode5 = $imp[0];
//            }
//        } else {
//            $accountCode5 = $ifrsCat->account_code_5;
//        }
//        $accountCode6 = $ifrsCat->account_code_6;
//        $noHC = Common::valueYorNtoBoolean($ifrsCat->investment);
//        $ahs = Common::valueYorNtoBoolean($ifrsCat->asset_held_for_sale);
//        $shortCode = $ifrsCat->ca_ifrs_category_code;
//    }
//
//    protected function retrieveTransactionList($transType, $transTypeTwo = null)
//    {
//        $query = CaTransaction::join(
//            'ca_ifrs_category',
//            'ca_ifrs_category.ca_ifrs_category_id',
//            '=',
//            'ca_transaction.ca_ifrs_cat_id'
//        )
//        ->join(
//            'ca_fixed_asset',
//            'ca_fixed_asset.ca_fixed_asset_id',
//            '=',
//            'ca_transaction.ca_fixed_asset_id'
//        )
//        ->join(
//            'ca_account',
//            'ca_account.ca_account_id',
//            '=',
//            'ca_fixed_asset.ca_account_code_id'
//        )
//        ->where('ca_fixed_asset.site_group_id', SiteGroup::get()->site_group_id)
//        ->where('ca_fixed_asset.active', CommonConstant::DATABASE_VALUE_YES)
//        ->where('ca_transaction.ca_fin_year_id', CaFinYearManager::currentFinYearId())
//        ->select(
//            [
//                'ca_ifrs_category.ca_ifrs_category_id',
//                'ca_ifrs_category.ca_ifrs_category_code',
//                'ca_ifrs_category.ca_ifrs_category_desc',
//                \DB::raw("CONCAT_WS('-', ca_ifrs_category_code, ca_ifrs_category_desc) AS ifrsCat"),
//                'ca_fixed_asset.ca_fixed_asset_code',
//                'ca_account.ca_account_code',
//                'ca_transaction.ca_transaction_type_id',
//                'ca_transaction.cv_asset',
//                'ca_transaction.cv_land',
//                'ca_transaction.hc_asset',
//                'ca_transaction.hc_land',
//                'ca_transaction.depn_wo',
//                'ca_transaction.cv_depn_adjustment',
//                'ca_transaction.hc_depn_adjustment',
//                'ca_fixed_asset.ca_fixed_asset_code',
//                'ca_fixed_asset.ca_fixed_asset_id',
//                'ca_transaction.disposal_proceeds',
//                'ca_transaction.disposal_costs',
//                'ca_transaction.effective_date',
//                'ca_transaction.effective_date_override',
//                'ca_transaction.disposal_mode_full'
//            ]
//        );
//
//        if ($transTypeTwo) {
//            $query->whereIn('ca_transaction_type_id', [$transType, $transTypeTwo]);
//        } else {
//            $query->where('ca_transaction_type_id', $transType);
//        }
//
//        if ($transType == CaTransactionType::DISPOSAL) {
//            //$query->where('disposal_mode_full', CommonConstant::DATABASE_VALUE_YES);
//        }
//
//        return $query;
//    }


    protected function retrieveGroupedTransactionList($transType, $transTypeTwo = null)
    {
        $query = CaTransaction::join(
            'ca_ifrs_category',
            'ca_ifrs_category.ca_ifrs_category_id',
            '=',
            'ca_transaction.ca_ifrs_cat_id'
        )
        ->join(
            'ca_fixed_asset',
            'ca_fixed_asset.ca_fixed_asset_id',
            '=',
            'ca_transaction.ca_fixed_asset_id'
        )
        ->join(
            'ca_fixed_asset_type',
            'ca_fixed_asset_type.ca_fixed_asset_type_id',
            '=',
            'ca_fixed_asset.ca_fixed_asset_type_id'
        )
        ->leftJoin(
            'ca_account',
            'ca_account.ca_account_id',
            '=',
            'ca_fixed_asset.ca_account_code_id'
        )
        ->where('ca_fixed_asset.site_group_id', SiteGroup::get()->site_group_id)
        ->where('ca_fixed_asset.active', CommonConstant::DATABASE_VALUE_YES)
        ->where('ca_transaction.ca_fin_year_id', CaFinYearManager::currentFinYearId())
        ->select(
            [
                'ca_fixed_asset_type.ca_fixed_asset_type_id',
                'ca_fixed_asset_type.ca_fixed_asset_type_code',
                'ca_fixed_asset_type.ca_fixed_asset_type_desc',
                \DB::raw("CONCAT_WS('-', ca_fixed_asset_type_code, ca_fixed_asset_type_desc) AS caFixedAssetType"),
                \DB::raw("SUM(ca_transaction.cv_asset + ca_transaction.cv_land) AS transValue"),
            ]
        );

        if ($transTypeTwo) {
            $query->whereIn('ca_transaction_type_id', [$transType, $transTypeTwo]);
        } else {
            $query->where('ca_transaction_type_id', $transType);
        }

        $query->groupBy('ca_fixed_asset_type.ca_fixed_asset_type_id')
            ->orderBy('ca_fixed_asset_type.ca_fixed_asset_type_code');

        return $query;
    }



//    protected function hasHCDepnAdj()
//    {
//        $query = CaTransaction::where('hc_depn_adjustment', '<>', 0)
//            ->whereNotNull('hc_depn_adjustment')
//            ->where('site_group_id', SiteGroup::get()->site_group_id)
//            ->where('ca_fin_year_id', CaFinYearManager::currentFinYearId());
//
//        return $query->count() > 0;
//    }
//
//    protected function hasCVDepnAdj()
//    {
//        $query = CaTransaction::where('cv_depn_adjustment', '<>', 0)
//            ->whereNotNull('cv_depn_adjustment')
//            ->where('site_group_id', SiteGroup::get()->site_group_id)
//            ->where('ca_fin_year_id', CaFinYearManager::currentFinYearId());
//
//        return $query->count() > 0;
//    }
//
//    protected function hasReval($caFixedAssetId)
//    {
//        $query = CaTransaction::where('ca_transaction_type_id', CaTransactionType::REVALUATION)
//            ->where('ca_fixed_asset_id', $caFixedAssetId)
//            ->where('site_group_id', SiteGroup::get()->site_group_id)
//            ->where('ca_fin_year_id', CaFinYearManager::currentFinYearId());
//
//        return $query->count() > 0;
//    }
//
//
//    protected function getCVDepnAdj($caFixedAssetId, $reversalTypes)
//    {
//        $query = CaTransaction::where('ca_fixed_asset_id', $caFixedAssetId)
//            ->whereIn(ca_transaction_type_id, $reversalTypes)
//            ->where('site_group_id', SiteGroup::get()->site_group_id)
//            ->where('ca_fin_year_id', CaFinYearManager::currentFinYearId());
//
//        return $query->get()->sum('cv_depn_adjustment');
//    }


    protected function retrieveBalance($fixedAssetId, $finYearId, $balanceType)
    {
        $balanceService = new BalanceService($this->permissionService);

        return $balanceService->retrieveByYear($fixedAssetId, $finYearId, $balanceType);
    }

    protected function getBFwdAccumulatedBalances(
        $caFixedAssetId,
        &$curBfwdGBV,
        &$curBFwdDepn,
        &$curBFwdImp,
        &$curCYDepn,
        &$curHCCYDepn,
        &$curBFwdRR
    ) {
        $cv = $this->getCurrentAssetData($caFixedAssetId);
        $hc = $this->getHistoricAssetData($caFixedAssetId);

        $curBfwdGBV = $cv->COpenGBV;
        $curBFwdDepn = $cv->COpenDPN;
        $curBFwdImp = Math::negateCurrency(Math::addCurrency([$cv->COpenIMPA, $cv->COpenIMPL]));
        $curCYDepn = $cv->CDPN;
        $curHCCYDepn = $hc->amount;
        $curBFwdRR = Math::addCurrency([$cv->BFAssetRR, $cv->BFLandRR]);
    }

    protected function getRevalutionData($historic = false, $revalUp = true, $fixedAssetId = null)
    {
        $query = VwFar06::join(
            'ca_ifrs_category',
            'ca_ifrs_category.ca_ifrs_category_id',
            '=',
            'vw_far06.ca_ifrs_category_id'
        )
        ->join(
            'ca_transaction',
            'ca_transaction.ca_transaction_id',
            '=',
            'vw_far06.ca_transaction_id'
        )
        ->where('vw_far06.site_group_id', SiteGroup::get()->site_group_id)
        ->select(
            [
                'vw_far06.ca_fixed_asset_id',
                'ca_ifrs_category.ca_ifrs_category_code',
                'ca_ifrs_category.ca_ifrs_category_desc',
                'ca_ifrs_category.common_good',
                'vw_far06.hra_asset',
                \DB::raw('vw_far06.ca_ifrs_category_id'),
                \DB::raw('vw_far06.`Asset Code` as fixed_asset_code'),
                \DB::raw('vw_far06.`Account Code` as account_code'),
                'ca_ifrs_category.account_code_1',
                'ca_ifrs_category.account_code_2',
                'ca_ifrs_category.account_code_3',
                \DB::raw('vw_far06.`Gross RR` as gross_rr'),
                \DB::raw('vw_far06.`Gross SD` as gross_sd'),
                \DB::raw('vw_far06.`Depn WOff RR` as depn_woff_rr'),
                \DB::raw('vw_far06.`Depn WOff SD` as depn_woff_sd'),
                \DB::raw('vw_far06.`Imp WOff RR` as imp_woff_rr'),
                \DB::raw('vw_far06.`Imp WOff SD` as imp_woff_sd'),
                \DB::raw('vw_far06.`Reval Reserve` as reval_reserve'),
                \DB::raw('vw_far06.`Surplus/Deficit` as surplus_deficit'),
                \DB::raw('vw_far06.`Asset Loss Reversal` + vw_far06.`Land Loss Reversal` as loss_reversal'),
                \DB::raw(
                    'vw_far06.`Asset Impairment Reversal` + vw_far06.`Land Impairment Reversal` as impairment_reversal'
                ),
            ]
        )
        ->orderBy(\DB::raw('vw_far06.`IFRS Code`'))
        ->orderBy(\DB::raw('vw_far06.`Asset Code`'))
        ;

        if ($revalUp) {
            //$query->whereRaw('cfwd_gbv >= `BFwd Total GBV`');
            $query->whereRaw('gbv_change_asset + gbv_change_land >= 0');
        } else {
            //$query->whereRaw('cfwd_gbv < `BFwd Total GBV`');
            $query->whereRaw('gbv_change_asset + gbv_change_land < 0');
        }

        return $query;
    }

    // Is special processing required with a transfer.
    protected function checkIfrsCategory($caIfrsCategoryId)
    {
        $ifrsCat = CaIfrsCategory::where('ca_ifrs_category_id', $caIfrsCategoryId)->first();

        if (
            $ifrsCat->investment == CommonConstant::DATABASE_VALUE_YES
            || $ifrsCat->asset_held_for_sale == CommonConstant::DATABASE_VALUE_YES
            || $ifrsCat->ca_ifrs_category_code == 'CDwel'
        ) {
            return true;
        } else {
            return false;
        }
    }

    protected function getHistoricAssetData($caFixedAssetId)
    {
        return VwFar07::userSiteGroup()
            ->leftJoin(
                'ca_ifrs_category as bfwd_ca_ifrs_category',
                'bfwd_ca_ifrs_category.ca_ifrs_category_id',
                '=',
                'vw_far07.bfwd_ca_ifrs_category_id'
            )
            ->where('vw_far07.ca_balance_historic', 'Y')
            ->where('ca_fixed_asset_id', $caFixedAssetId)
            ->select(
                [
                    \DB::raw('bfwd_ca_ifrs_category.ca_ifrs_category_desc AS bfwd_ca_ifrs_category_desc'),
                    'vw_far07.hra_asset',
                    'vw_far07.ca_fixed_asset_id',
                    'vw_far07.deminimis_asset',
                    \DB::raw('vw_far07.`REV Asset Value` as HREVAV'),
                    \DB::raw('vw_far07.`REV Land Value` as HREVLV'),
                    \DB::raw('vw_far07.`IFRS Code` as ca_ifrs_category_code'),
                    \DB::raw('vw_far07.`IFRS Description` as ca_ifrs_category_desc'),
                    \DB::raw('vw_far07.`Previous IFRS Code` as bf_ca_ifrs_category_code'),
                    \DB::raw('vw_far07.`Previous IFRS Description` as bf_ca_ifrs_category_desc'),
                    \DB::raw('vw_far07.`Asset Code` as fixed_asset_code'),
                    \DB::raw('vw_far07.`Asset Description` as fixed_asset_desc'),
                    \DB::raw('vw_far07.`Account Code` as account_code'),
                    \DB::raw('vw_far07.`CY Depreciation` as amount'),
                    \DB::raw('vw_far07.`BFwd GBV` as HOpenGBV'),
                    'bfwd_ca_ifrs_category.amortisation as bfwd_amortisation',
                ]
            )
            ->orderBy(\DB::raw('bfwd_ca_ifrs_category.ca_ifrs_category_desc'))
            ->orderBy(\DB::raw('vw_far07.`Account Code`'))
            ->first();
    }

    protected function hasTransfer($caFixedAssetId, &$bfwdIfrsCatId, &$ifrsCat)
    {
        $asset = VwFar07::userSiteGroup()
            ->leftJoin(
                'ca_ifrs_category as bfwd_ca_ifrs_category',
                'bfwd_ca_ifrs_category.ca_ifrs_category_id',
                '=',
                'vw_far07.bfwd_ca_ifrs_category_id'
            )
            ->where('vw_far07.ca_balance_historic', CommonConstant::DATABASE_VALUE_NO)
            ->where('ca_fixed_asset_id', $caFixedAssetId)
            ->select(
                [
                    'vw_far07.ca_ifrs_category_id',
                    'vw_far07.bfwd_ca_ifrs_category_id',
                    'bfwd_ca_ifrs_category.ca_ifrs_category_code',
                    'bfwd_ca_ifrs_category.ca_ifrs_category_desc',
                ]
            )
            ->first();

        $bfwdIfrsCatId = $asset->bfwd_ca_ifrs_category_id;
        $ifrsCat = Common::concatFields([$asset->ca_ifrs_category_code, $asset->ca_ifrs_category_desc]);

        return $asset->ca_ifrs_category_id != $asset->bfwd_ca_ifrs_category_id;
    }



    protected function getCurrentAssetData($caFixedAssetId)
    {
        return VwFar07::userSiteGroup()
            ->leftJoin(
                'ca_ifrs_category as bfwd_ca_ifrs_category',
                'bfwd_ca_ifrs_category.ca_ifrs_category_id',
                '=',
                'vw_far07.bfwd_ca_ifrs_category_id'
            )
            ->where('vw_far07.ca_balance_historic', 'N')
            ->where('ca_fixed_asset_id', $caFixedAssetId)
            ->select(
                [
                    \DB::raw('bfwd_ca_ifrs_category.ca_ifrs_category_desc AS bfwd_ca_ifrs_category_desc'),
                    'vw_far07.hra_asset',
                    'vw_far07.ca_fixed_asset_id',
                    'vw_far07.deminimis_asset',
                    \DB::raw('vw_far07.`REV Asset Value` as CREVAV'),
                    \DB::raw('vw_far07.`REV Land Value` as CREVLV'),
                    \DB::raw('vw_far07.`IFRS Code` as ca_ifrs_category_code'),
                    \DB::raw('vw_far07.`IFRS Description` as ca_ifrs_category_desc'),
                    \DB::raw('vw_far07.`Previous IFRS Code` as bf_ca_ifrs_category_code'),
                    \DB::raw('vw_far07.`Previous IFRS Description` as bf_ca_ifrs_category_desc'),
                    \DB::raw('vw_far07.`Asset Code` as fixed_asset_code'),
                    \DB::raw('vw_far07.`Asset Description` as fixed_asset_desc'),
                    \DB::raw('vw_far07.`Account Code` as account_code'),
                    \DB::raw('vw_far07.`Accumulated Depreciation` as COpenDPN'),
                    \DB::raw('vw_far07.`CY Depreciation` as CDPN'),
                    \DB::raw('vw_far07.`CFwd Accumulated Depreciation` as CCFwdDPN'),
                    \DB::raw('vw_far07.`Accumulated Impairment Asset` as COpenIMPA'),
                    \DB::raw('vw_far07.`Accumulated Impairment Land` as COpenIMPL'),
                    \DB::raw('vw_far07.`BFwd GBV` as COpenGBV'),
                    \DB::raw('vw_far07.`BFwd RR Asset` as BFAssetRR'),
                    \DB::raw('vw_far07.`BFwd RR Land` as BFLandRR'),
                    'bfwd_ca_ifrs_category.amortisation as bfwd_amortisation',
                ]
            )
            ->orderBy(\DB::raw('bfwd_ca_ifrs_category.ca_ifrs_category_desc'))
            ->orderBy(\DB::raw('vw_far07.`Account Code`'))
            ->first();
    }


//    protected function isHRA($sIFRSType, $costCentre)
//    {
//        // Is this an HRA asset?
//        if (
//            $sIFRSType == 'Council Dwellings' ||
//            substr($costCentre, 0, 3) == 'COC' ||
//            substr($costCentre, 0, 3) == 'UMR'
//        ) {
//            return true;
//        } else {
//            return false;
//        }
//    }
//
//    protected function isHeritage($sIFRSType)
//    {
//        // Is this an Heritage Asset?
//        if ($sIFRSType == 'Heritage Assets') {
//            return true;
//        } else {
//            return false;
//        }
//    }
//
//    protected function isDeminimis($deminimis)
//    {
//        // Is this an Deminimis Asset?
//        return Common::valueYorNtoBoolean($deminimis);
//    }
//
//
//    protected function getPostingAccount()
//    {
//        return CaPostingAccount::where('site_group_id', SiteGroup::get()->site_group_id)->first();
//    }
//
//    protected function postGroupLikeJournalLines(
//        &$journalData,
//        $DrCr,
//        $sAccountCode,
//        $sSubAccountCode,
//        $sCostCentre,
//        $sText,
//        $iTextLen
//    ) {
//        $curAmount = 0;
//        $curTotalAmount  = 0;
//        $replaceCount = 0;
//        $count = 0;
//
//        foreach ($journalData as $key => $journal) {
//            $count++;
//            // First 9 rows are headings.
//            if ($count > 9) {
//                $journalDrCr = Math::lessThanCurrency(array_get($journal, 'col_S'), 0) ? 'Cr' : 'Dr';
//
//                // Is this one of the target rows - check all the key fields.
//             if (array_get($journal, 'col_G') == $sAccountCode && array_get($journal, 'col_K') == $sSubAccountCode) {
//                    if (array_get($journal, 'col_H') == $sCostCentre && $journalDrCr == $DrCr) {
//                       // If we match then clear out the record and add its value to the total to put back at the end.
//                        if (substr(array_get($journal, 'col_Y'), 0, $iTextLen) == substr($sText, 0, $iTextLen)) {
//                            $curAmount = array_get($journal, 'col_S');
//                            $curTotalAmount = Math::addCurrency([$curTotalAmount, $curAmount]);
//                            //unset($journalData[$key]);
//                            array_forget($journalData, $key);
//                            $replaceCount++;
//                        }
//                    }
//                }
//            }
//        }
//
//        if ($replaceCount > 0) {
//            $this->addJournalRow(
//                $journalData,
//                $DrCr,
//                $sAccountCode,
//                $sCostCentre,
//                $sSubAccountCode,
//                $curTotalAmount,
//                $sText,
//                true
//            );
//        }
//
//        return;
//    }

    private function getDate()
    {
        $dateTime = new \DateTime('now');
        return "{$dateTime->format('d.m.Y')}";
    }

    private function setYearString()
    {
        $openCaFinYear = VwCaCurrentFinYear::userSiteGroup()
            ->select('ca_fin_year_code')
            ->first();

        if ($openCaFinYear) {
            $this->yearString = $openCaFinYear->ca_fin_year_code;
        }
    }

    private function setControlAccounts()
    {
        $this->capitalAdjustmentAccount = self::CAA;
        $this->revalReserveAccount = self::RR;
        $this->sMRAccount = self::SMR;
    }

    private function newJournalRow(
        $col_A = '',
        $col_B = '',
        $col_C = '',
        $col_D = '',
        $col_E = '',
        $col_F = '',
        $col_G = '',
        $col_H = '',
        $col_I = '',
        $col_J = '',
        $col_K = '',
        $col_L = '',
        $col_M = '',
        $col_N = '',
        $col_O = '',
        $col_P = '',
        $col_Q = '',
        $col_R = '',
        $col_S = '',
        $col_T = '',
        $col_U = '',
        $col_V = '',
        $col_W = '',
        $col_X = '',
        $col_Y = ''
    ) {
        return [
            'col_A'   => $col_A,
            'col_B'   => $col_B,
            'col_C'   => $col_C,
            'col_D'   => $col_D,
            'col_E'   => $col_E,
            'col_F'   => $col_F,
            'col_G'   => $col_G,
            'col_H'   => $col_H,
            'col_I'   => $col_I,
            'col_J'   => $col_J,
            'col_K'   => $col_K,
            'col_L'   => $col_L,
            'col_M'   => $col_M,
            'col_N'   => $col_N,
            'col_O'   => $col_O,
            'col_P'   => $col_P,
            'col_Q'   => $col_Q,
            'col_R'   => $col_R,
            'col_S'   => $col_S,
            'col_T'   => $col_T,
            'col_U'   => $col_U,
            'col_V'   => $col_V,
            'col_W'   => $col_W,
            'col_X'   => $col_X,
            'col_Y'   => $col_Y
        ];
    }
}
