<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\NMNIJournals;

use Tfcloud\Models\CaTransactionType;
use Tfcloud\Services\PermissionService;

class FarCliNMNI05ReportService extends FarCliNMNITrnBaseReportService
{
    protected $caTransType = CaTransactionType::ACQUISITION;
    protected $caTransDesc = 'Acquisitions';

    public function __construct(PermissionService $permissionService)
    {
        /**
         * /////////////////////////////////////////////////
         * // Acquisitions  //
         * /////////////////////////////////////////////////
         */
        parent::__construct($permissionService);
    }
}
