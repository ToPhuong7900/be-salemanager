<?php

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset\NMNIJournals;

use Tfcloud\Models\CaTransactionType;
use Tfcloud\Services\PermissionService;

class FarCliNMNI04ReportService extends FarCliNMNITrnBaseReportService
{
    protected $caTransType = CaTransactionType::ADDITION;
    protected $caTransDesc = 'Additions';

    public function __construct(PermissionService $permissionService)
    {
        /**
         * /////////////////////////////////////////////////
         * // Additions  //
         * /////////////////////////////////////////////////
         */
        parent::__construct($permissionService);
    }
}
