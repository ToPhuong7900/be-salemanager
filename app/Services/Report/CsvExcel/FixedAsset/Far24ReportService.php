<?php

/**
 * Transfers
 */

namespace Tfcloud\Services\Report\CsvExcel\FixedAsset;

use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Math;
use Tfcloud\Models\Report;
use Tfcloud\Models\SiteGroup;
use Tfcloud\Models\Views\VwFar18;
use Tfcloud\Services\PermissionService;

class Far24ReportService extends JournalPostingService
{
    protected $entity = false;

    private $reportBoxFilterLoader = null;

    public function __construct(PermissionService $permissionService, $filterData, Report $report = null)
    {
        parent::__construct($permissionService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;

        $this->setFilterProperties($filterData);
    }

    private function setFilterProperties($filterData)
    {
        $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($filterData);
        $entity = $reportFilterQuery->getFirstValueFilterField('ca_entity_id');

        // get filter values
        $this->entity = (!$entity) ? false : $entity;
    }

    public function getReportData(&$filterText, &$sOrderText)
    {
        // If no admin ca_posting_account record available, exit.
        if (!$this->postingAcc) {
            return [];
        }

        $far18 = $this->getData();

        $outputData = [];

        foreach ($far18 as $row) {
            $outputRow = [];

            $rowTextFrom = "{$row->fixed_asset_code} Transfer from ({$row->ca_ifrs_category_code})";
            $rowTextInto = "{$row->fixed_asset_code} Transfer into ({$row->ca_ifrs_category_code})";

            $curGross = $row->gross_movement;
            if (Math::notEqualCurrency($curGross, 0)) {
                $commonGood = $row->common_good === 'Y';
                $hraAsset = $row->hra_asset === 'Y';

                if ($hraAsset) {
                    $this->writeCodeSection(
                        $outputRow,
                        $this->postingAcc->acc_balance_sheet,
                        $row->account_code_1,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_hra_code
                    );
                } elseif ($commonGood) {
                    $this->writeCodeSection(
                        $outputRow,
                        $row->account_code,
                        $row->account_code_1,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_blank
                    );
                } else {
                    $this->writeCodeSection(
                        $outputRow,
                        $this->postingAcc->acc_balance_sheet,
                        $row->account_code_1,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_gf_code
                    );
                }

                if (Math::lessThanCurrency($curGross, 0)) {
                    $this->writeCredit(
                        $outputRow,
                        Math::negateCurrency($curGross),
                        $rowTextFrom,
                        $row->ca_ifrs_category_code
                    );
                } else {
                    $this->writeDebit($outputRow, $curGross, $rowTextInto, $row->ca_ifrs_category_code);
                }

                $this->newRptLine($outputData, $outputRow);
            }

            $curDepn = $row->depreciation_movement;
            if (Math::notEqualCurrency($curDepn, 0)) {
                if ($hraAsset) {
                    $this->writeCodeSection(
                        $outputRow,
                        $this->postingAcc->acc_balance_sheet,
                        $row->account_code_2,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_hra_code
                    );
                } else {
                    $this->writeCodeSection(
                        $outputRow,
                        $this->postingAcc->acc_balance_sheet,
                        $row->account_code_2,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_gf_code
                    );
                }

                if (Math::lessThanCurrency($curDepn, 0)) {
                    $this->writeDebit(
                        $outputRow,
                        Math::negateCurrency($curDepn),
                        $rowTextFrom,
                        $row->ca_ifrs_category_code
                    );
                } else {
                    $this->writeCredit($outputRow, $curDepn, $rowTextInto, $row->ca_ifrs_category_code);
                }
                $this->newRptLine($outputData, $outputRow);
            }


            $curImpairment = $row->impairment_movement;
            if (Math::notEqualCurrency($curImpairment, 0)) {
                if ($hraAsset) {
                    $this->writeCodeSection(
                        $outputRow,
                        $this->postingAcc->acc_balance_sheet,
                        $row->account_code_2,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_hra_code
                    );
                } else {
                    $this->writeCodeSection(
                        $outputRow,
                        $this->postingAcc->acc_balance_sheet,
                        $row->account_code_2,
                        $this->postingAcc->prj_blank_project,
                        $this->postingAcc->fund_gf_code
                    );
                }

                if (Math::lessThanCurrency($curImpairment, 0)) {
                    $this->writeDebit(
                        $outputRow,
                        Math::negateCurrency($curImpairment),
                        $rowTextFrom,
                        $row->ca_ifrs_category_code
                    );
                } else {
                    $this->writeCredit($outputRow, $curImpairment, $rowTextInto, $row->ca_ifrs_category_code);
                }
                $this->newRptLine($outputData, $outputRow);
            }
        }

        return $outputData;
    }

    private function getData()
    {
        $query = VwFar18::join(
            'ca_ifrs_category',
            'ca_ifrs_category.ca_ifrs_category_id',
            '=',
            'vw_far18.ca_ifrs_category_id'
        )
        ->where('vw_far18.site_group_id', SiteGroup::get()->site_group_id)
        ->select(
            [
                'ca_ifrs_category.common_good',
                'vw_far18.hra_asset',
                'ca_ifrs_category.ca_ifrs_category_code',
                \DB::raw('vw_far18.`Asset Code` as fixed_asset_code'),
                \DB::raw('vw_far18.`Gross Movement` as gross_movement'),
                \DB::raw('vw_far18.`Depreciation Movement` as depreciation_movement'),
                \DB::raw('vw_far18.`Impairment Movement` as impairment_movement'),
                \DB::raw('vw_far18.`Account Code` as account_code'),
                'ca_ifrs_category.account_code_1',
                'ca_ifrs_category.account_code_2',
            ]
        )
        ->orderBy(\DB::raw('vw_far18.`IFRS Code`'))
        ->orderBy(\DB::raw('vw_far18.`Asset Code`'));

        if ($this->entity) {
            $query->where('vw_far18.ca_entity_id', $this->entity);
        }
        return $query->get();
    }
}
