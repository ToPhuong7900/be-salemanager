<?php

namespace Tfcloud\Services\Report\CsvExcel\Inspection;

use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\CsvExcel\CsvExcelReportBaseService;
use App\SBoxFilter\Inspection\Report\INS11FilterQuery;

class INS11InspectionGapService extends CsvExcelReportBaseService
{
    protected $reportBoxFilterLoader = null;

    public function __construct(PermissionService $permissionService, $report = null)
    {
        parent::__construct($permissionService);
        $this->reportBoxFilterLoader = new ClassMapReportLoader($report);
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
        /**
         * @var $reportFilterQuery INS11FilterQuery
         */
        $records = $reportFilterQuery->getAll(0);
        $filterText = $reportFilterQuery->getFilterDetailText();
        $sOrderText = "Site Code (ascending)";

        return $this->createOutputData($records);
    }

    protected function createOutputData($records)
    {
        $result = [];

        foreach ($records as $record) {
            $line['Establishment Code'] = $record->establishment_code;
            $line['Establishment Desc'] = $record->establishment_desc;
            $line['Site Code'] = $record->site_code;
            $line['Site Desc'] = $record->site_desc;
            $line['Site Aliases'] = $record->site_alias;
            $line['Site Active'] = $record->active;
            $line['Site Type Code'] = $record->site_type_code;
            $line['Site Type Desc'] = $record->site_type_desc;
            $line['Site Designation Code'] = $record->site_designation_code;
            $line['Site Designation Desc'] = $record->site_designation_desc;
            $line['Site Classification Code'] = $record->site_classification_code;
            $line['Site Classification Desc'] = $record->site_classification_desc;
            $line['UPRN'] = $record->uprn;
            $line['Contact'] = $record->contact_name;
            $line['Contact Organisation'] = $record->organisation;
            $line['Owner'] = $record->display_name;
            $line['Site Email Address'] = $record->site_email_addr;
            $line['Sub Dwelling'] = $record->second_addr_obj;
            $line['Number/Name'] = $record->first_addr_obj;
            $line['Street'] = $record->street;
            $line['Locality'] = $record->locality;
            $line['Town/City'] = $record->town;
            $line['County/Region'] = $record->country;
            $line['Post Code'] = $record->postcode;
            $result[] = $line;
        }

        return $result;
    }
}
