<?php

namespace Tfcloud\Services\Report\CsvExcel\Inspection;

use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Traits\EmailTrait;
use Tfcloud\Services\Inspection\InspectionForecastService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\CsvExcel\CsvExcelReportBaseService;
use App\SBoxFilter\Inspection\Report\INS08FilterQuery;

class INS08InspectionForecastService extends CsvExcelReportBaseService
{
    use EmailTrait;

    /*
    * Tfcloud\Services\Inspection\InspectionService
    */
    protected $inspectionService;
    protected $inspectionForecastService;
    protected $permissionService;

    private $reportBoxFilterLoader = null;

    public function __construct(
        PermissionService $permissionService,
        $report = null
    ) {
        $this->permissionService = $permissionService;
        $this->inspectionService = \Illuminate\Support\Facades\App::make(
            'Tfcloud\Services\Inspection\InspectionService'
        );
        $this->inspectionForecastService = new InspectionForecastService(
            $this->permissionService,
            $this->inspectionService
        );
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            /**
             * @var $reportFilterQuery INS08FilterQuery
             */
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $data = $reportFilterQuery->getAll(0);
            $filterText = $reportFilterQuery->getFilterDetailText();
        } else {
            if (!array_get($inputs, 'month_commencing')) {
                $inputs['month_commencing'] = (new \DateTime())->format('d/m/Y');
            }
            $headers = $this->inspectionForecastService->getTableHeaders($inputs);
            $data = $this->inspectionForecastService->formatInspectionForecast(
                $this->inspectionForecastService->getInspectionForecast($inputs),
                $headers
            );
            $whereCodes = [];               // The codes which have been used in the filter
            $whereTexts = [];               // Plain text about filtering applied
            $orCodes = [];                  // csv string of OR codes
            $bFilterDetail = false;

            $this->generateFilterData($inputs, $whereCodes, $whereTexts, $orCodes);
            $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $filterText);
        }

        return $data;
    }

    private function generateFilterData($filterData, &$whereCodes, &$whereTexts, &$orCodes)
    {
        if ($val = Common::iset($filterData['month_commencing'])) {
            array_push($whereTexts, "Month Commencing: {$val}");
        }

        if ($siteId = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $siteId, "Site Code"]);

            if ($buildingId = Common::iset($filterData['building_id'])) {
                array_push(
                    $whereCodes,
                    ['building', 'building_id', 'building_code', $buildingId, "Building Code"]
                );

                if ($roomId = Common::iset($filterData['room_id'])) {
                    array_push(
                        $whereCodes,
                        ['room', 'room_id', 'room_number', $roomId, "Room Number"]
                    );
                }
            }
        }

        if ($val = Common::iset($filterData['owner'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, "Owner"]);
        }

        if ($val = Common::iset($filterData['inspection_group_id'])) {
            array_push(
                $whereCodes,
                ['inspection_group', 'inspection_group_id', 'inspection_group_code', $val, "Inspection Group"]
            );
        }

        if ($val = Common::iset($filterData['inspection_type_id'])) {
            array_push(
                $whereCodes,
                ['inspection_type', 'inspection_type_id', 'inspection_type_code', $val, "Inspection Type"]
            );
        }

        if ($val = Common::iset($filterData['plant_code'])) {
            array_push($whereTexts, "Plant Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['plant_group_id'])) {
            array_push(
                $whereCodes,
                ['plant_group', 'plant_group_id', 'plant_group_code', $val, "Plant Group"]
            );
        }

        if ($val = Common::iset($filterData['plant_subgroup_id'])) {
            array_push(
                $whereCodes,
                ['plant_subgroup', 'plant_subgroup_id', 'plant_subgroup_code', $val, "Plant Sub Group"]
            );
        }

        if ($val = Common::iset($filterData['plant_type_id'])) {
            array_push(
                $whereCodes,
                ['plant_type', 'plant_type_id', 'plant_type_code', $val, "Plant Type"]
            );
        }

        if ($val = Common::iset($filterData['plant_system_id'])) {
            array_push(
                $whereCodes,
                ['plant_system', 'plant_system_id', 'plant_system_code', $val, "Plant System Type"]
            );
        }

        if ($val = Common::iset($filterData['plantOwner'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, "plantOwner"]);
        }

        if ($val = Common::iset($filterData['prop_zone_id'])) {
            array_push($whereCodes, ['prop_zone', 'prop_zone_id', 'prop_zone_code', $val, 'Zone']);

            if ($val = Common::iset($filterData['prop_external_id'])) {
                array_push(
                    $whereCodes,
                    ['prop_external', 'prop_external_id', 'prop_external_code', $val, 'External Area']
                );
            }
        }

        $statutory = array_get($filterData, 'statutory');
        if (
            in_array(
                $statutory,
                [CommonConstant::DATABASE_VALUE_NUMBER_YES, CommonConstant::DATABASE_VALUE_NUMBER_NO]
            )
        ) {
            $statutory = Common::valueBooleanNumericToYesNo($statutory);
        }

        if ($statutory) {
            switch ($statutory) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.statutory_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.statutory_no'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.statutory_all'));
                    break;
            }
        }
    }
}
