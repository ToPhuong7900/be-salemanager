<?php

namespace Tfcloud\Services\Report\CsvExcel\Inspection;

use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Models\InspectionSFG20\InspSFG20Frequency;
use Tfcloud\Models\InspectionSFG20\InspSFG20InspectionStatusType;
use Tfcloud\Models\InspectionSFG20\InspSFG20ScheduledInspection;
use Tfcloud\Models\InspectionSFG20\InspSFG20Type;
use Tfcloud\Models\Plant;
use Tfcloud\Models\Report;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\CsvExcel\CsvExcelReportBaseService;
use Tfcloud\Services\Report\FilterQuery\PlantFilterQueryService;
use App\SBoxFilter\InspectionSFG20\Reports\INSCLIRBWM01BoxFilter;
use App\SBoxFilter\InspectionSFG20\Reports\INSCLIRBWM01FilterQuery;

class INSCLIRBWM01PlantInspectionComplianceService extends CsvExcelReportBaseService
{
    protected $permissionService;
    private $plantFilterQueryService;
    private $reportBoxFilterLoader = null;

    // Inspection status filter
    private $draft;
    private $open;
    private $issued;
    private $complete;
    private $cancelled;
    private $onHold;
    private $closed;
    private $none;
    // Inspection supplier filter
    private $supplier;
    // Inspection date Filter
    private $targetFrom;
    private $targetTo;
    private $completedFrom;
    private $completedTo;

    public function __construct(
        PermissionService $permissionService,
        Report $report = null
    ) {
        parent::__construct($permissionService);
        $this->plantFilterQueryService = new PlantFilterQueryService($permissionService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        $whereCodes = [];
        $whereTexts = [];
        $orCodes = [];
        $bFilterDetail = false;

        $this->formatInput($inputs);

        $this->setFilterProperites($inputs);
        $filterByStatus = $this->filterByStatus();

        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            /**
             * @var $reportFilterQuery INSCLIRBWM01FilterQuery
             */
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $plantData = $reportFilterQuery->getAll(0);

            $filterText = $reportFilterQuery->getFilterDetailText();

            if ($this->none) {
                $filterText .=  ", Includes records where No Scheduled Inspection Available";
            }

            if (
                is_null($this->draft) &&
                is_null($this->open) &&
                is_null($this->issued) &&
                is_null($this->complete) &&
                is_null($this->cancelled) &&
                is_null($this->onHold) &&
                is_null($this->closed)
            ) {
                $filterText = str_replace('Scheduled Inspection Status Type Includes,', '', $filterText);
            }
        } else {
            $plantData = $this->getPlantData($inputs, $whereCodes, $orCodes, $whereTexts);

            $this->addCustomFiltering($inputs, $whereCodes, $orCodes, $whereTexts);

            $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $filterText);
        }

        $outputData = [];

        foreach ($plantData as $plantRow) {
            $line = [];
            $line['Site Code'] = $plantRow['site_code'];
            $line['Site Description'] = $plantRow['site_desc'];
            $line['Site Type Code'] = $plantRow['site_type_code'];
            $line['Plant Group Description'] = $plantRow['plant_group_desc'];
            $line['Plant Code'] = $plantRow['plant_code'];
            $line['Manufacturer'] = $plantRow['manufacturer'];
            $line['Model'] = $plantRow['model'];
            $line['Serial Number'] = $plantRow['serial_num'];

            $plantId = $plantRow['plant_id'];
            $inspectionTypeId = $plantRow['insp_sfg20_type_id'];
            $inspectionTypeVersionId = $plantRow['insp_sfg20_type_version_id'];
            $inspectionFrequencyId = $plantRow['insp_sfg20_frequency_id'];

            $status = "";
            $dueDate = "";
            $completedDate = "";
            $supplierName = "";
            $inspection = null;

            $inspectionType = InspSFG20Type::find($inspectionTypeId);
            $inspectionFrequency = InspSFG20Frequency::find($inspectionFrequencyId);

            if (!$inspectionType) {
                $status = 'No Scheduled Inspection Available';
            } else {
                $date = new \DateTime();
                // Add float days to todays date if non statutatory
                if (
                    $inspectionFrequency->statutory == CommonConstant::DATABASE_VALUE_NO &&
                    is_numeric($inspectionFrequency->inspectionTypeVersion->float_days_before)
                ) {
                    $date->add(new \DateInterval('P' . $inspectionFrequency->inspectionTypeVersion
                            ->float_days_before . 'D'));
                }
                $dateStr = $date->format('Y-m-d');

                $compInspection = $this->getLatestCompletedClosedInspection(
                    $plantId,
                    $inspectionTypeVersionId,
                    $dateStr
                );
                if ($compInspection) {
                    $nextDueInspection =
                        $this->getNextDueInspection(
                            $plantId,
                            $inspectionTypeVersionId,
                            $compInspection->inspection_due_date
                        );
                    if ($nextDueInspection) {
                        $inspection = $nextDueInspection;
                    } else {
                        $inspection = $compInspection;
                    }
                } else {
                    $prevInspection = $this->getPreviousInspection($plantId, $inspectionTypeVersionId, $dateStr);
                    if ($prevInspection) {
                        $status = $prevInspection->inspectionStatus;
                        if ($status) {
                            if (
                                in_array(
                                    $status->inspection_status_type_id,
                                    [InspSFG20InspectionStatusType::OPEN, InspSFG20InspectionStatusType::ISSUED]
                                )
                            ) {
                                // Inspection Overdue
                                $inspection = $prevInspection;
                            } else {
                                // Inspection completed/closed
                                $nextDueInspection = $this->getNextDueInspection(
                                    $plantId,
                                    $inspectionTypeId,
                                    $prevInspection->inspection_due_date
                                );
                                if ($nextDueInspection) {
                                    $inspection = $nextDueInspection;
                                } else {
                                    $inspection = $prevInspection;
                                }
                            }
                        }
                    } else {
                        $status = 'No Scheduled Inspection Available';
                    }
                }

                if ($inspection) {
                    $status = $inspection->inspectionStatus->description;
                    $dueDate = $inspection->inspection_due_date;
                    $completedDate = $inspection->completed_date;
                    $supplier = $inspection->supplier;
                    if ($supplier) {
                        $supplierName = $supplier->contact_name;
                    }
                }
            }
            $line['Status'] = $status;
            $dueDateTime = \DateTime::createFromFormat('Y-m-d', $dueDate);
            if ($dueDateTime) {
                $line['Due Date'] = $dueDateTime->format('Y/m/d');
            } else {
                $line['Due Date'] = $dueDate;
            }
            $line['Supplier Name'] = $supplierName;

            if ($filterByStatus) {
                // Filter results by status
                if (!$this->none && !$inspection) {
                    // Don't include plant records without an inspection
                    continue;
                }
                if (!$this->draft && $inspection) {
                    // Exclude plant records where the inspection has a status type of draft.
                    if ($this->statusTypeCheck($inspection, InspSFG20InspectionStatusType::DRAFT)) {
                        continue;
                    }
                }
                if (!$this->open && $inspection) {
                    // Exclude plant records where the inspection has a status type of open.
                    if ($this->statusTypeCheck($inspection, InspSFG20InspectionStatusType::OPEN)) {
                        continue;
                    }
                }
                if (!$this->issued && $inspection) {
                    // Exclude plant records where the inspection has a status type of issued.
                    if ($this->statusTypeCheck($inspection, InspSFG20InspectionStatusType::ISSUED)) {
                        continue;
                    }
                }
                if (!$this->complete && $inspection) {
                    // Exclude plant records where the inspection has a status type of complete.
                    if ($this->statusTypeCheck($inspection, InspSFG20InspectionStatusType::COMPLETE)) {
                        continue;
                    }
                }
                if (!$this->cancelled && $inspection) {
                    // Exclude plant records where the inspection has a status type of cancelled.
                    if ($this->statusTypeCheck($inspection, InspSFG20InspectionStatusType::CANCELLED)) {
                        continue;
                    }
                }
                if (!$this->onHold && $inspection) {
                    // Exclude plant records where the inspection has a status type of on hold.
                    if ($this->statusTypeCheck($inspection, InspSFG20InspectionStatusType::ONHOLD)) {
                        continue;
                    }
                }
                if (!$this->closed && $inspection) {
                    // Exclude plant records where the inspection has a status type of closed.
                    if ($this->statusTypeCheck($inspection, InspSFG20InspectionStatusType::CLOSED)) {
                        continue;
                    }
                }
            }

            if ($this->filterBySupplier()) {
                if (!$inspection) {
                    // No inspection so can't be linked to the supplier so exclude.
                    continue;
                }

                // Check if using box filter
                if (is_array($this->supplier)) {
                    if (!in_array($inspection->supplier_id, $this->supplier)) {
                        continue;
                    }
                } else {
                    if ($inspection->supplier_id != $this->supplier) {
                        continue;
                    }
                }
            }

            if ($this->targetFrom) {
                $targetFromInsp = \DateTime::createFromFormat('Y-m-d', $dueDate);
                if (!$targetFromInsp) {
                    // No target date so exclude since filtering by target date.
                    continue;
                }

                $targetFromFilter = \DateTime::createFromFormat('d/m/Y', $this->targetFrom);
                if ($targetFromInsp < $targetFromFilter) {
                    continue;
                }
            }

            if ($this->targetTo) {
                $targetToInsp = \DateTime::createFromFormat('Y-m-d', $dueDate);
                if (!$targetToInsp) {
                    // No target date so exclude since filtering by target date.
                    continue;
                }

                $targetToFilter = \DateTime::createFromFormat('d/m/Y', $this->targetTo);
                if ($targetToInsp > $targetToFilter) {
                    continue;
                }
            }

            if ($this->completedFrom) {
                $completedFromInsp = \DateTime::createFromFormat('Y-m-d', $completedDate);
                if (!$completedFromInsp) {
                    // No completed date so exclude since filtering by completed date.
                    continue;
                }

                $completedFromFilter = \DateTime::createFromFormat('d/m/Y', $this->completedFrom);
                if ($completedFromInsp < $completedFromFilter) {
                    continue;
                }
            }

            if ($this->completedTo) {
                $completedToInsp = \DateTime::createFromFormat('Y-m-d', $completedDate);
                if (!$completedToInsp) {
                    // No completed date so exclude since filtering by completed date.
                    continue;
                }

                $completedToFilter = \DateTime::createFromFormat('d/m/Y', $this->completedTo);
                if ($completedToInsp > $completedToFilter) {
                    continue;
                }
            }

            $outputData[] = $line;
        }

        return $outputData;
    }

    private function getPlantData(array $inputs, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $plantQuery = Plant::userSiteGroup()
            ->join('site', 'site.site_id', '=', 'plant.site_id')
            ->leftJoin('site_type', 'site_type.site_type_id', '=', 'site.site_type_id')
            ->leftJoin('plant_group', 'plant_group.plant_group_id', '=', 'plant.plant_group_id')
            ->leftJoin('plant_subgroup', 'plant_subgroup.plant_subgroup_id', '=', 'plant.plant_subgroup_id')
            ->leftjoin('insp_sfg20_inspection', 'insp_sfg20_inspection.plant_id', '=', 'plant.plant_id')
            ->leftjoin(
                'insp_sfg20_inspection_version',
                'insp_sfg20_inspection_version.insp_sfg20_inspection_id',
                '=',
                'insp_sfg20_inspection.insp_sfg20_inspection_id'
            )
            ->leftjoin(
                'insp_sfg20_type_version',
                'insp_sfg20_inspection_version.insp_sfg20_type_version_id',
                '=',
                'insp_sfg20_type_version.insp_sfg20_type_version_id'
            )
            ->leftjoin(
                'insp_sfg20_group_level_1',
                'insp_sfg20_group_level_1.insp_sfg20_group_level_1_id',
                '=',
                'insp_sfg20_type_version.insp_sfg20_group_level_1_id'
            )
            ->leftjoin(
                'insp_sfg20_group_level_2',
                'insp_sfg20_group_level_2.insp_sfg20_group_level_2_id',
                '=',
                'insp_sfg20_type_version.insp_sfg20_group_level_2_id'
            )
            ->leftjoin(
                'insp_sfg20_group_level_3',
                'insp_sfg20_group_level_3.insp_sfg20_group_level_3_id',
                '=',
                'insp_sfg20_type_version.insp_sfg20_group_level_3_id'
            )
            ->leftjoin(
                'insp_sfg20_group_level_4',
                'insp_sfg20_group_level_4.insp_sfg20_group_level_4_id',
                '=',
                'insp_sfg20_type_version.insp_sfg20_group_level_4_id'
            )
            ->leftjoin(
                'insp_sfg20_group_level_5',
                'insp_sfg20_group_level_5.insp_sfg20_group_level_5_id',
                '=',
                'insp_sfg20_type_version.insp_sfg20_group_level_5_id'
            )
            ->select([
                'site.site_code',
                'site.site_desc',
                'site_type.site_type_code',
                'plant_group.plant_group_desc',
                'plant_subgroup.plant_subgroup_desc',
                'plant.plant_id',
                'plant.plant_code',
                'plant.manufacturer',
                'plant.model',
                'plant.serial_num',
                'insp_sfg20_group_level_1.insp_sfg20_group_level_1_code',
                'insp_sfg20_group_level_2.insp_sfg20_group_level_2_code',
                'insp_sfg20_group_level_3.insp_sfg20_group_level_3_code',
                'insp_sfg20_group_level_4.insp_sfg20_group_level_4_code',
                'insp_sfg20_group_level_5.insp_sfg20_group_level_5_code',
                'insp_sfg20_type_version.insp_sfg20_type_desc',
                'insp_sfg20_type_version.insp_sfg20_type_id'
            ]);

        return $this->plantFilterQueryService
            ->reAddPlantQuery($inputs, false, $plantQuery, $whereCodes, $orCodes, $whereTexts)
            ->get();
    }

    /*
     * Retrieve the latest completed/closed inspection for the supplied plant/type.
     */
    private function getLatestCompletedClosedInspection($plantId, $inspectionTypeVersionId, $dateStr)
    {
        return InspSFG20ScheduledInspection::userSiteGroup()
            ->join(
                'insp_sfg20_inspection_version',
                'insp_sfg20_inspection_version.insp_sfg20_inspection_version_id',
                '=',
                'insp_sfg20_scheduled_inspection.insp_sfg20_inspection_version_id'
            )
            ->join(
                'insp_sfg20_inspection',
                'insp_sfg20_inspection.insp_sfg20_inspection_id',
                '=',
                'insp_sfg20_inspection_version.insp_sfg20_inspection_id'
            )
            ->join(
                'insp_sfg20_type_version',
                'insp_sfg20_type_version.insp_sfg20_type_version_id',
                '=',
                'insp_sfg20_inspection_version.insp_sfg20_type_version_id'
            )
            ->join(
                'inspection_status',
                'inspection_status.inspection_status_id',
                '=',
                'insp_sfg20_scheduled_inspection.inspection_status_id'
            )
            ->where('insp_sfg20_inspection.plant_id', $plantId)
            ->where('insp_sfg20_type_version.insp_sfg20_type_version_id', $inspectionTypeVersionId)
            ->whereIn(
                'inspection_status.inspection_status_type_id',
                [InspSFG20InspectionStatusType::COMPLETE, InspSFG20InspectionStatusType::CLOSED]
            )
            ->where('insp_sfg20_scheduled_inspection.inspection_due_date', '>=', $dateStr)
            ->orderBy('insp_sfg20_scheduled_inspection.inspection_due_date', 'DESC')
            ->first();
    }

    /*
     * Retrieve the latest open/issued/completed/closed inspection before the supplied date..
     */
    private function getpreviousInspection($plantId, $inspectionTypeVersionId, $dateStr)
    {
        return InspSFG20ScheduledInspection::userSiteGroup()
            ->join(
                'insp_sfg20_inspection_version',
                'insp_sfg20_inspection_version.insp_sfg20_inspection_version_id',
                '=',
                'insp_sfg20_scheduled_inspection.insp_sfg20_inspection_version_id'
            )
            ->join(
                'insp_sfg20_inspection',
                'insp_sfg20_inspection.insp_sfg20_inspection_id',
                '=',
                'insp_sfg20_inspection_version.insp_sfg20_inspection_id'
            )
            ->join(
                'insp_sfg20_type_version',
                'insp_sfg20_type_version.insp_sfg20_type_version_id',
                '=',
                'insp_sfg20_inspection_version.insp_sfg20_type_version_id'
            )
            ->join(
                'inspection_status',
                'inspection_status.inspection_status_id',
                '=',
                'insp_sfg20_scheduled_inspection.inspection_status_id'
            )
            ->where('insp_sfg20_inspection.plant_id', $plantId)
            ->where('insp_sfg20_type_version.insp_sfg20_type_version_id', $inspectionTypeVersionId)
            ->whereIn(
                'inspection_status.inspection_status_type_id',
                [
                    InspSFG20InspectionStatusType::OPEN,
                    InspSFG20InspectionStatusType::ISSUED,
                    InspSFG20InspectionStatusType::COMPLETE,
                    InspSFG20InspectionStatusType::CLOSED
                ]
            )
            ->where('insp_sfg20_scheduled_inspection.inspection_due_date', '<', $dateStr)
            ->orderBy('insp_sfg20_scheduled_inspection.inspection_due_date', 'DESC')
            ->first();
    }

    /*
     * Retrieve the next due inspection for the supplied plant/type.
     */
    private function getNextDueInspection($plantId, $inspectionTypeVersionId, $dateStr)
    {
        return InspSFG20ScheduledInspection::userSiteGroup()
            ->join(
                'insp_sfg20_inspection_version',
                'insp_sfg20_inspection_version.insp_sfg20_inspection_version_id',
                '=',
                'insp_sfg20_scheduled_inspection.insp_sfg20_inspection_version_id'
            )
            ->join(
                'insp_sfg20_inspection',
                'insp_sfg20_inspection.insp_sfg20_inspection_id',
                '=',
                'insp_sfg20_inspection_version.insp_sfg20_inspection_id'
            )
            ->join(
                'insp_sfg20_type_version',
                'insp_sfg20_type_version.insp_sfg20_type_version_id',
                '=',
                'insp_sfg20_inspection_version.insp_sfg20_type_version_id'
            )
            ->join(
                'inspection_status',
                'inspection_status.inspection_status_id',
                '=',
                'insp_sfg20_scheduled_inspection.inspection_status_id'
            )
            ->where('insp_sfg20_inspection.plant_id', $plantId)
            ->where('insp_sfg20_type_version.insp_sfg20_type_version_id', $inspectionTypeVersionId)
            ->whereIn(
                'inspection_status.inspection_status_type_id',
                [
                    InspSFG20InspectionStatusType::OPEN,
                    InspSFG20InspectionStatusType::ISSUED,
                ]
            )
            ->where('insp_sfg20_scheduled_inspection.inspection_due_date', '>=', $dateStr)
            ->orderBy('insp_sfg20_scheduled_inspection.inspection_due_date', 'ASC')
            ->first();
    }

    private function addCustomFiltering(array $inputs, &$whereCodes, &$orCodes, &$whereTexts)
    {
        if ($val = array_get($inputs, 'site_id')) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);

            if ($val = array_get($inputs, 'building_id')) {
                array_push($whereCodes, ['building', 'building_id', 'building_code', $val, "Building Code"]);

                if ($val = array_get($inputs, 'room_id')) {
                    array_push($whereCodes, ['room', 'room_id', 'room_number', $val, "Room Number"]);
                }
            }
        }

        if ($this->supplier) {
            array_push(
                $whereCodes,
                ['contact', 'contact_id', 'contact_name', $this->supplier, \Lang::get('text.supplier')]
            );
        }

        $statusType = [];
        if ($this->draft) {
            $statusType[] = InspSFG20InspectionStatusType::DRAFT;
        }
        if ($this->open) {
            $statusType[] = InspSFG20InspectionStatusType::OPEN;
        }
        if ($this->issued) {
            $statusType[] = InspSFG20InspectionStatusType::ISSUED;
        }
        if ($this->complete) {
            $statusType[] = InspSFG20InspectionStatusType::COMPLETE;
        }
        if ($this->cancelled) {
            $statusType[] = InspSFG20InspectionStatusType::CANCELLED;
        }
        if ($this->onHold) {
            $statusType[] = InspSFG20InspectionStatusType::ONHOLD;
        }
        if ($this->closed) {
            $statusType[] = InspSFG20InspectionStatusType::CLOSED;
        }
        if (count($statusType)) {
            array_push($orCodes, [
                'inspection_status_type',
                'inspection_status_type_id',
                'inspection_status_type_code',
                implode(',', $statusType),
                \Lang::get('text.status_type')
            ]);
        }

        if ($this->none) {
            array_push($whereTexts, "Includes records where No Inspection Available");
        }

        if ($this->targetFrom) {
            array_push($whereTexts, 'Due Date From ' . "{$this->targetFrom}" . "'");
        }

        if ($this->targetTo) {
            array_push($whereTexts, 'Due Date To ' . "{$this->targetTo}" . "'");
        }

        if ($this->completedFrom) {
            array_push($whereTexts, 'Completed Date From ' . "{$this->completedFrom}" . "'");
        }

        if ($this->completedTo) {
            array_push($whereTexts, 'Completed Date To ' . "{$this->completedTo}" . "'");
        }
    }

    private function setFilterProperites(array &$inputs)
    {
        $this->draft = array_get($inputs, 'draft');
        $this->open = array_get($inputs, 'open');
        $this->issued = array_get($inputs, 'issued');
        $this->complete = array_get($inputs, 'complete');
        $this->cancelled = array_get($inputs, 'cancelled');
        $this->onHold = array_get($inputs, 'onHold');
        $this->closed = array_get($inputs, 'closed');
        $this->none = array_get($inputs, 'none');

        $this->supplier = array_get($inputs, 'supplier');

        $this->targetFrom = array_get($inputs, 'targetFrom');
        $this->targetTo = array_get($inputs, 'targetTo');

        $this->completedFrom = array_get($inputs, 'completedFrom');
        $this->completedTo = array_get($inputs, 'completedTo');
    }

    /*
     * Returns true if one or more status filter options selected.
     */
    private function filterByStatus()
    {
        return !is_null($this->draft) ||
            !is_null($this->open) ||
            !is_null($this->issued) ||
            !is_null($this->complete) ||
            !is_null($this->cancelled) ||
            !is_null($this->onHold) ||
            !is_null($this->closed) ||
            !is_null($this->none);
    }

    private function statusTypeCheck(InspSFG20ScheduledInspection $inspection, $inspectionStatusTypeId)
    {
        $status = $inspection->inspectionStatus;
        if ($status) {
            return $status->inspection_status_type_id == $inspectionStatusTypeId;
        }
        return false;
    }

    private function filterBySupplier()
    {
        return !is_null($this->supplier);
    }

    private function formatInput(&$inputs)
    {
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);

            $dueDateRange = $reportFilterQuery->getValueFilterField('inspection_due_date');
            $completedDateRange = $reportFilterQuery->getValueFilterField('completed_date');
            $supplierId = $reportFilterQuery->getValueFilterField('supplier_id');
            $inspectionStatusTypes = $reportFilterQuery->getValueFilterField('inspection_status_type_id');

            if (!empty($dueDateRange)) {
                list($dueDateFrom, $dueDateTo) =  $dueDateRange[0];

                $inputs['targetFrom'] = Common::dateFieldDisplayFormat($dueDateFrom);
                $inputs['targetTo'] = Common::dateFieldDisplayFormat($dueDateTo);
            }

            if (!empty($completedDateRange)) {
                list($completedDateFrom, $completedDateTo) =  $completedDateRange[0];

                $inputs['completedFrom'] = Common::dateFieldDisplayFormat($completedDateFrom);
                $inputs['completedTo'] = Common::dateFieldDisplayFormat($completedDateTo);
            }

            if (!empty($supplierId)) {
                $inputs['supplier'] = $supplierId;
            }

            if (!empty($inspectionStatusTypes)) {
                if (in_array(InspSFG20InspectionStatusType::DRAFT, $inspectionStatusTypes)) {
                    $inputs['draft'] = 1;
                }
                if (in_array(InspSFG20InspectionStatusType::OPEN, $inspectionStatusTypes)) {
                    $inputs['open'] = 1;
                }
                if (in_array(InspSFG20InspectionStatusType::ISSUED, $inspectionStatusTypes)) {
                    $inputs['issued'] = 1;
                }
                if (in_array(InspSFG20InspectionStatusType::ONHOLD, $inspectionStatusTypes)) {
                    $inputs['onHold'] = 1;
                }
                if (in_array(InspSFG20InspectionStatusType::COMPLETE, $inspectionStatusTypes)) {
                    $inputs['complete'] = 1;
                }
                if (in_array(InspSFG20InspectionStatusType::CLOSED, $inspectionStatusTypes)) {
                    $inputs['closed'] = 1;
                }
                if (in_array(InspSFG20InspectionStatusType::CANCELLED, $inspectionStatusTypes)) {
                    $inputs['cancelled'] = 1;
                }
                if (in_array(INSCLIRBWM01BoxFilter::INSPECTION_STATUS_TYPE_NONE, $inspectionStatusTypes)) {
                    $inputs['none'] = 1;
                }
            }
        }

        return $inputs;
    }
}
