<?php

namespace Tfcloud\Services\Report\CsvExcel;

use Tfcloud\Services\BaseService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\ReportGenerateService;

class CsvExcelReportBaseService extends BaseService
{
    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    public function getFiltering($whereCodes, $orCodes, $whereTexts, &$bFilterDetail, &$filterText)
    {
        ReportGenerateService::reGetFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $filterText);
    }
}
