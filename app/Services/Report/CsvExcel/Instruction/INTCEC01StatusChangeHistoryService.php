<?php

namespace Tfcloud\Services\Report\CsvExcel\Instruction;

use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\CsvExcel\CsvExcelReportBaseService;
use Tfcloud\Models\Instruction;
use Tfcloud\Models\RecordStatus;

class INTCEC01StatusChangeHistoryService extends CsvExcelReportBaseService
{
    protected $permissionService;

    public function __construct(
        PermissionService $permissionService
    ) {
        parent::__construct($permissionService);
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        $whereCodes = [];
        $whereTexts = [];
        $orCodes = [];
        $bFilterDetail = false;

        $instructions  = Instruction::userSiteGroup()
            ->leftJoin('instruction_status as instructionCurrentStatus', function ($join) {
                $join->on(
                    'instructionCurrentStatus.instruction_status_id',
                    '=',
                    'instruction.instruction_status_id'
                );
                $join->on(
                    'instructionCurrentStatus.site_group_id',
                    '=',
                    'instruction.site_group_id'
                );
            })

            ->leftJoin(\DB::raw("(SELECT 
                record_status.site_group_id, record_status.record_id, max(audit_date) as curr_date 
                FROM record_status
                WHERE gen_table_id = 15 
                GROUP BY record_status.record_id
                ) AS currentStatusTable"), function ($join) {
                  $join->on("instruction.instruction_id", "=", "currentStatusTable.record_id");
            })

            ->select([
                'instruction_id',
                'instruction_code as Instruction Code',
                //'instruction.created_on',
                \DB::raw("DATE_FORMAT(instruction.created_on, '%Y-%m-%d %H:%i') as created_on"),

                'instructionCurrentStatus.instruction_status_code as Current Status',
//                \DB::raw('(CASE WHEN currentStatusTable.curr_date is not null'
//                . ' THEN currentStatusTable.curr_date ELSE instruction.created_on END) AS curr_date'),
                \DB::raw("(CASE WHEN currentStatusTable.curr_date is not null"
                . " THEN DATE_FORMAT(currentStatusTable.curr_date, '%Y-%m-%d %H:%i')"
                . " ELSE DATE_FORMAT(instruction.created_on, '%Y-%m-%d %H:%i') END) AS curr_date"),
                'currentStatusTable.record_id'
             ])

            ->get();

        $outputData = [];

        foreach ($instructions as $instruction) {
            $line = [];

            $currentStatus = RecordStatus::userSiteGroup()
            ->where('gen_table_id', 15)
            ->where('record_id', $instruction->instruction_id)
            ->orderBy('record_id')
            ->orderBy('audit_date', 'DESC')
            ->limit(1)
            ->select([
                'comments as current_status_comments'
            ])
            ->first();

            $line['Instruction Code'] = $instruction->{'Instruction Code'};
            $line['Current Status'] = $instruction->{'Current Status'};
            $line['Current Date'] = $instruction->{'curr_date'};
            $line['Current Comments'] = $currentStatus ? $currentStatus->{'current_status_comments'} : '';

            $prevInstructionStatuses = RecordStatus::userSiteGroup()
                ->leftJoin('instruction_status as instructionPrevStatus', function ($join) {
                    $join->on('instructionPrevStatus.instruction_status_id', '=', 'record_status.status_id');
                    $join->on('instructionPrevStatus.site_group_id', '=', 'record_status.site_group_id');
                })
                ->leftJoin('instruction', 'instruction.instruction_id', '=', 'record_status.record_id')

            ->where('gen_table_id', 15)
            ->where('record_id', $instruction->instruction_id)

            //As there are weird cases where the record_status table contains records at dates before
            //the instruction was even created. Therefore only retrieve previous statuses which were
            //added after or the same time as the Instruction was created.
            ->where(
                \DB::raw("DATE_FORMAT(record_status.audit_date, '%Y-%m-%d %H:%i')"),
                '>=',
                $instruction->created_on
            )

            ->select([
                'instruction_status_code',
                \DB::raw("DATE_FORMAT(record_status.audit_date, '%Y-%m-%d %H:%i') as audit_date"),
                'comments as prev_comments'
            ])

            ->orderBy('record_status_id', 'DESC')
            ->get();

            $counter = 0;
            foreach ($prevInstructionStatuses as $prevStatus) {
                //If you come in the loop a third time then break out & go to next instruction
                if ($counter > 1) {
                    break;
                }

                if ($counter == 0) {
                    $line['Previous Status'] = 'GEN';
                    $line['Previous Status Date'] = $instruction->{'created_on'};
                    $line['Previous Status Comments'] = "";
                } elseif ($counter == 1) {
                    $line['Previous Status'] = $prevStatus->{'instruction_status_code'};
                    $line['Previous Status Date'] = $prevStatus->{'audit_date'};
                    $line['Previous Status Comments'] = $prevStatus->{'prev_comments'};
                }

                $counter++;
            }

            $outputData[] = $line;
        }

        return $outputData;
    }
}
