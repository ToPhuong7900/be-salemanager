<?php

namespace Tfcloud\Services\Report\CsvExcel;

use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\BoxFilterQuery\FilterQueryBootstrap;
use Tfcloud\Models\GenTable;
use Tfcloud\Models\Report;
use Tfcloud\Models\Views\VwDoc01;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\ReportGenerateService;
use App\SBoxFilter\Document\DocumentFilterQuery;
use App\SBoxFilter\General\Reports\DOC01FilterQuery;

class DOC01WithUserDefinedService extends CsvExcelReportBaseService
{
    private $reportBoxFilterLoader;

    public function __construct(PermissionService $permissionService, Report $report)
    {
        $this->permissionService = $permissionService;
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function getReportData($inputs, &$filterText, $sOrderText)
    {
        $outputData = [];

        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            /**
             * @var $reportFilterQuery DOC01FilterQuery
            */
            $reportFilterQuery  = $this->reportBoxFilterLoader->getFilterQuery($inputs);

            /**
             * @var $filterQuery DocumentFilterQuery
             */
            $filterQuery = FilterQueryBootstrap::getFilterQuery($inputs['filterNameId'], $inputs);

            $documentIds = array_map(function ($item) {
                return (int)$item;
            }, $filterQuery->getAll()->pluck('document_id')->toArray());

            $documents = VwDoc01::userSiteGroup()->whereIn('document_id', $documentIds)->get();

            // Use $reportFilterQuery just to get the filter detail text for audit
            $filterText = $reportFilterQuery->getFilterDetailText();

            $userDefineLabels = ReportGenerateService::genUserdefInitialise(GenTable::DOCUMENT);

            foreach ($documents as $document) {
                $line = [];
                $line['Date Loaded'] = $document->{"Date Loaded"};
                $line['Time Loaded'] = $document->{"Time Loaded"};
                $line['Record Type'] = $document->{"Record Type"};
                $line['Document Name'] = $document->{"Document Name"};
                $line['Document URL'] = $document->{"Document URL"};
                $line['Document Code'] = $document->{"Document Code"};
                $line['Document Description'] = $document->{"Document Description"};
                $line['Group Code'] = $document->{"Group Code"};
                $line['Group Description'] = $document->{"Group Description"};
                $line['Sub Group Code'] = $document->{"Sub Group Code"};
                $line['Sub Group Description'] = $document->{"Sub Group Description"};
                $line['Loaded By'] = $document->{"Loaded By"};
                $line['Record Code'] = $document->{"Record Code"};
                $line['Site Code'] = $document->{"Site Code"};
                $line['Site Description'] = $document->{"Site Description"};
                $line['Building Code'] = $document->{"Building Code"};
                $line['Building Description'] = $document->{"Building Description"};
                $line['Comment'] = $document->{"Comment"};
                $line['Archived'] = $document->{"Archived"};
                $line['Private'] = $document->{"Private"};
                $line['Read-only'] = $document->{"Read-only"};
                $line['Document Date'] = $document->{"Document Date"};
                $line['Keep Until Date'] = $document->{"Keep Until Date"};
                $line['Next Review Date'] = $document->{"Next Review Date"};
                $line['Next Review Owner'] = $document->{"Next Review Owner"};

                foreach ($document->toArray() as $key => $item) {
                    foreach ($userDefineLabels as $userDefineKey => $userDefineLabel) {
                        if ($userDefineKey == $key . '_label') {
                            $line[$userDefineLabel] = $item;
                        }
                    }
                }

                $outputData[] = $line;
            }
        }

        return $outputData;
    }
}
