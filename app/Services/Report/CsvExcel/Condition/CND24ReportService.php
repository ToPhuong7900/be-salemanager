<?php

namespace Tfcloud\Services\Report\CsvExcel\Condition;

use Tfcloud\Lib\BoxFilter\Helper\UserDefineBoxFilter;
use Tfcloud\Models\GenTable;

class CND24ReportService extends CNDSiteAndBuildingIdworkCostsReportService
{
    protected function createOutputData($records)
    {
        $result = [];

        $userDefine = new UserDefineBoxFilter(GenTable::SITE);
        $dataHeaderUD = $userDefine->getElementsAndHeaders(['isMemo' => true])['headers'];

        foreach ($records as $record) {
            $line = $this->buildLine($record);
            $line['Site Area']                 = $record->area;
            $line['Site Area Units']           = $record->unit_of_area_code;

            foreach ($dataHeaderUD as $itemHeaderUD) {
                $line[$itemHeaderUD->getTitle()] = !empty($record[$itemHeaderUD->getFieldName()]) ?
                    $record[$itemHeaderUD->getFieldName()] : '';
            }

            $line['Total IW Cost']             = $record->total_cost;

            $result[] = $line;
        }

        return $result;
    }
}
