<?php

namespace Tfcloud\Services\Report\CsvExcel\Condition;

use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\CsvExcel\CsvExcelReportBaseService;

abstract class CNDSiteAndBuildingIdworkCostsReportService extends CsvExcelReportBaseService
{
    protected $reportBoxFilterLoader;

    public function __construct(PermissionService $permissionService, $report)
    {
        parent::__construct($permissionService);
        $this->reportBoxFilterLoader = new ClassMapReportLoader($report);
    }

    public function getReportData($inputs, &$filterText)
    {
        $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
        $records = $reportFilterQuery->getAll(0);
        $filterText = $reportFilterQuery->getFilterDetailText();

        return $this->createOutputData($records);
    }

    protected function buildLine($record)
    {
        $line = [];
        $line['Establishment Code']        = $record->establishment_code;
        $line['Establishment Desc']        = $record->establishment_desc;
        $line['Site Code']                 = $record->site_code;
        $line['Site Desc']                 = $record->site_desc;
        $line['Site Status']               = $record->active;
        $line['Site Type Code']            = $record->site_type_code;
        $line['Site Type Desc']            = $record->site_type_desc;
        $line['Site Classification Code']  = $record->site_classification_code;
        $line['Site Classification Desc']  = $record->site_classification_desc;
        $line['Site Designation Code']     = $record->site_designation_code;
        $line['Site Designation Desc']     = $record->site_designation_desc;
        $line['Committee Code']            = $record->committee_code;
        $line['Committee Desc']            = $record->committee_desc;
        $line['Site Usage Code']           = $record->site_usage_code;
        $line['Site Usage Desc']           = $record->site_usage_desc;
        $line['Site Tenure Code']          = $record->prop_tenure_code;
        $line['Site Tenure Desc']          = $record->prop_tenure_desc;
        $line['Sub dwelling']              = $record->second_addr_obj;
        $line['number/name']               = $record->first_addr_obj;
        $line['Town']                      = $record->town;
        $line['Region']                    = $record->region;
        $line['Post Code']                 = $record->postcode;

        return $line;
    }

    abstract protected function createOutputData($records);
}
