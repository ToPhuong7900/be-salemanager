<?php

namespace Tfcloud\Services\Report\CsvExcel\Condition;

class CND25ReportService extends CNDSiteAndBuildingIdworkCostsReportService
{
    protected function createOutputData($records)
    {
        $result = [];
        foreach ($records as $record) {
            $line = $this->buildLine($record);
            $line['Building Code']             = $record->building_code;
            $line['Building Desc']             = $record->building_desc;
            $line['Building Status']           = $record->building_active;
            $line['Building GIA']              = $record->building_gia;
            $line['Building NIA']              = $record->building_nia;

            $line['Total IW Cost']             = $record->total_cost;

            $result[] = $line;
        }

        return $result;
    }
}
