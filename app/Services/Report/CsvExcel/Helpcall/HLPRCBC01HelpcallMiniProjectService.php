<?php

namespace Tfcloud\Services\Report\CsvExcel\Helpcall;

use Illuminate\Support\Arr;
use Tfcloud\Lib\Math;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\CsvExcel\CsvExcelReportBaseService;
use Tfcloud\Models\InvoiceStatusType;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Common;
use Tfcloud\Models\Report;

class HLPRCBC01HelpcallMiniProjectService extends CsvExcelReportBaseService
{
    protected $permissionService;

    private $reportBoxFilterLoader = null;

    public $filterText = '';

    public function __construct(
        PermissionService $permissionService,
        Report $report = null
    ) {
        parent::__construct($permissionService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    private function formatInputs(&$inputs)
    {
        $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
        $completeDateValue = $reportFilterQuery->getFirstValueFilterField('completed_date');
        $loggedDateValue = $reportFilterQuery->getFirstValueFilterField('logged_date');
        $netTotalValue = $reportFilterQuery->getFirstValueFilterField('net_total');
        $grandTotalValue = $reportFilterQuery->getFirstValueFilterField('grand_total');
        $helpCallCodeValue = $reportFilterQuery->getFirstValueFilterField("helpcall_code");
        $siteTypeValue = $reportFilterQuery->getFirstValueFilterField("site_type_id");
        $statusValue = $reportFilterQuery->getFirstValueFilterField("helpcall_status_id");
        $operativeValue = $reportFilterQuery->getFirstValueFilterField("operative_user_id");
        $categoryValue = $reportFilterQuery->getFirstValueFilterField("category_id");
        $supplierContactValue = $reportFilterQuery->getFirstValueFilterField("supplier_contact_id");
        $propTenureValue = $reportFilterQuery->getFirstValueFilterField("prop_tenure_id");
        $buildingIdValue = $reportFilterQuery->getFirstValueFilterField("building_id");
        $siteIdValue = $reportFilterQuery->getFirstValueFilterField("site_id");
        $roomIdValue = $reportFilterQuery->getFirstValueFilterField("room_id");

        $inputs['code'] = $helpCallCodeValue;
        $inputs['siteType'] = $siteTypeValue;
        $inputs['status'] = $statusValue;
        $inputs['operative_user_id'] =  $operativeValue;
        $inputs['category'] =  $categoryValue;
        $inputs['supplier'] =  $supplierContactValue;
        $inputs['propTenure'] =  $propTenureValue;
        $inputs['building_id'] = $buildingIdValue;
        $inputs['site_id'] = $siteIdValue;
        $inputs['room_id'] = $roomIdValue;
        $inputs['loggedFrom'] = Common::dateFieldDisplayFormat(Arr::first($loggedDateValue));
        $inputs['loggedTo'] = Common::dateFieldDisplayFormat(Arr::last($loggedDateValue));
        $inputs['completedFrom'] = Common::dateFieldDisplayFormat(Arr::first($completeDateValue));
        $inputs['completedTo'] = Common::dateFieldDisplayFormat(Arr::last($completeDateValue));
        $inputs['netTotalFrom'] = !empty(Arr::first($netTotalValue)) ? Arr::first($netTotalValue) : null;
        $inputs['netTotalTo'] = !empty(Arr::last($netTotalValue)) ? Arr::last($netTotalValue) : null;
        $inputs['grandTotalFrom'] = !empty(Arr::first($grandTotalValue)) ? Arr::first($grandTotalValue) : null;
        $inputs['grandTotalTo'] = !empty(Arr::last($grandTotalValue)) ? Arr::last($grandTotalValue) : null;

        // dd($inputs['netTotalFrom']);
        return $inputs;
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        $whereCodes = [];
        $whereTexts = [];
        $orCodes = [];
        $bFilterDetail = false;

        $this->formatInputs($inputs);

        $helpcallQuery = \DB::table(null)
                ->select('*')
                ->from(\DB::raw("(" . $this->getRCB01HelpcallDetails() . ') AS tmp'))
                ->orderBy('Helpcall Code');

        $filter = new \Tfcloud\Lib\Filters\HelpcallFilter($inputs);

        $this->basicFilterAll($helpcallQuery, $filter);

        $helpcalls = $helpcallQuery->get();

        $filteredHelpcalls = [-1 => true];

        //Supplier,Operative and Net Total filtering.
        //E.g. If supplier match return all associated rows with the helpcall.
        foreach ($helpcalls as $hlp) {
            $addHelpcallId = true;

            if (!is_null($filter->supplier)) {
                if ($hlp->contact_id == $filter->supplier) {
                } else {
                    $addHelpcallId = false;
                }
            }
            if (!is_null($filter->operative_user_id)) {
                if ($hlp->user_id == $filter->operative_user_id) {
                } else {
                    $addHelpcallId = false;
                }
            }

            if (!is_null($filter->netTotalFrom)) {
                if ($hlp->{'Net Total'} >= $filter->netTotalFrom) {
                } else {
                    $addHelpcallId = false;
                }
            }

            if (!is_null($filter->netTotalTo)) {
                if ($hlp->{'Net Total'} <= $filter->netTotalTo) {
                } else {
                    $addHelpcallId = false;
                }
            }
            if ($addHelpcallId) {
                $filteredHelpcalls[$hlp->helpcall_id] = true;
            }
        }

        $helpcalls = $helpcallQuery->whereIn('helpcall_id', array_keys($filteredHelpcalls))->get();
        $this->addCustomFiltering($inputs, $whereCodes, $orCodes, $whereTexts);
        $this->getFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $filterText);

        $outputData = [];
        $helpcallGrandTotal = 0;
        $helpcallId = null;
        $firstLine = true;

        $previousLine = null;

        foreach ($helpcalls as $helpcall) {
            $line = [];

            if ($helpcallId == $helpcall->helpcall_id) {
                //$helpcallGrandTotal += $helpcall->{'Net Total'}; //Use common library.
                $helpcallGrandTotal = Math::addCurrency(
                    [$helpcall->{'Net Total'}, $helpcallGrandTotal]
                );
            } else {
                if (!$firstLine) {
                    //New Helpcall Group
                    $totalLine['Helpcall Code'] =  $previousLine['Helpcall Code'];
                    $totalLine['Helpcall Description'] =  $previousLine['Helpcall Description'];
                    $totalLine['Site Code'] =  $previousLine['Site Code'];
                    $totalLine['Site Description'] =  $previousLine['Site Description'];
                    $totalLine['Building Code'] =  $previousLine['Building Code'];
                    $totalLine['Building Description'] =  $previousLine['Building Description'];
                    $totalLine['Room No'] = $previousLine['Room No'];
                    $totalLine['Room Description'] = $previousLine['Room Description'];
                    $totalLine['DLO Job Code'] = "";
                    $totalLine['DLO Job Description'] = "";
                    $totalLine['Instruction Code'] = "";
                    $totalLine['Instruction Description'] = "";
                    $totalLine['Sub Contractor Code'] = "";
                    $totalLine['Sub Contractor Job Description'] = "";
                    $totalLine['Material Order Code'] = "";
                    $totalLine['Material Order Description'] = "";
                    $totalLine['Material Code'] = "";
                    $totalLine['Material Item'] = "";
                    $totalLine['DLO Operative'] = "";
                    $totalLine['Supplier'] = "";
                    $totalLine['DLO Hrs'] = "";
                    $totalLine['Estimate'] = "";
                    $totalLine['Material Order Cost'] = "";
                    $totalLine['Material Cost'] = "";
                    $totalLine['DLO Labour Cost'] = "";
                    $totalLine['Instruction Cost'] = "";
                    $totalLine['DLO Subcontractor Cost'] = "";
                    $totalLine['Net Total'] = '';
                    $totalLine['Invoice Posted Date'] = "";
                    $totalLine['Complete Date'] = $previousLine['Complete Date'];

                    $totalLine['Grand Total'] = $helpcallGrandTotal;

                    $outputData[] = $totalLine;
                }
                $helpcallGrandTotal = $helpcall->{'Net Total'};
            }
            //Track current helpcall_id
            $helpcallId = $helpcall->helpcall_id;

            $line['Helpcall Code'] = $helpcall->{'Helpcall Code'};
            $line['Helpcall Description'] = $helpcall->{'Helpcall Description'};
            $line['Site Code'] = $helpcall->{'Site Code'};
            $line['Site Description'] = $helpcall->{'Site Description'};
            $line['Building Code'] = $helpcall->{'Building Code'};
            $line['Building Description'] = $helpcall->{'Building Description'};
            $line['Room No'] = $helpcall->{'Room No'};
            $line['Room Description'] = $helpcall->{'Room Description'};
            $line['DLO Job Code'] = $helpcall->{'DLO Job Code'};
            $line['DLO Job Description'] = $helpcall->{'DLO Job Description'};
            $line['Instruction Code'] = $helpcall->{'Instruction Code'};
            $line['Instruction Description'] = $helpcall->{'Instruction Description'};
            $line['Sub Contractor Code'] = $helpcall->{'Sub Contractor Code'};
            $line['Sub Contractor Job Description'] = $helpcall->{'Sub Contractor Job Description'};
            $line['Material Order Code'] = $helpcall->{'Material Order Code'};
            $line['Material Order Description'] = $helpcall->{'Material Order Description'};
            $line['Material Code'] = $helpcall->{'Material Code'};
            $line['Material Item'] = $helpcall->{'Material Item'};
            $line['DLO Operative'] = $helpcall->{'DLO Operative'};
            $line['Supplier'] = $helpcall->{'Supplier'};
            $line['DLO Hrs'] = $helpcall->{'DLO Hrs'};
            $line['Estimate'] = $helpcall->{'Estimate'};
            $line['Material Order Cost'] = $helpcall->{'Material Order Cost'};
            $line['Material Cost'] = $helpcall->{'Material Cost'};
            $line['DLO Labour Cost'] = $helpcall->{'DLO Labour Cost'};
            $line['Instruction Cost'] = $helpcall->{'Instruction Cost'};
            $line['DLO Subcontractor Cost'] = $helpcall->{'DLO Subcontractor Cost'};
            $line['Net Total'] = $helpcall->{'Net Total'};
            $line['Invoice Posted Date'] = $helpcall->{'Invoice Posted Date'};
            $line['Complete Date'] = $helpcall->{'Completed Date'};

            $line['Grand Total'] = "";
            $previousLine = $line;

            $outputData[] = $line;
            $firstLine = false;
        }

        if (!$firstLine) {
            $totalLine['Helpcall Code'] =  $previousLine['Helpcall Code'];
            $totalLine['Helpcall Description'] =  $previousLine['Helpcall Description'];
            $totalLine['Site Code'] =  $previousLine['Site Code'];
            $totalLine['Site Description']  =  $previousLine['Site Description'];
            $totalLine['Building Code'] =   $previousLine['Building Code'];
            $totalLine['Building Description'] =  $previousLine['Building Description'];
            $totalLine['Room No'] = $previousLine['Room No'];
            $totalLine['Room Description'] = $previousLine['Room Description'];
            $totalLine['DLO Job Code'] = "";
            $totalLine['DLO Job Description'] = "";
            $totalLine['Instruction Code'] = "";
            $totalLine['Instruction Description'] = "";
            $totalLine['Sub Contractor Code'] = "";
            $totalLine['Sub Contractor Job Description'] = "";
            $totalLine['Material Order Code'] = "";
            $totalLine['Material Order Description'] = "";
            $totalLine['Material Code'] = "";
            $totalLine['Material Item'] = "";
            $totalLine['DLO Operative'] = "";
            $totalLine['Supplier'] = "";
            $totalLine['DLO Hrs'] = "";
            $totalLine['Estimate'] = "";
            $totalLine['Material Order Cost'] = "";
            $totalLine['Material Cost'] = "";
            $totalLine['DLO Labour Cost'] = "";
            $totalLine['Instruction Cost'] = "";
            $totalLine['DLO Subcontractor Cost'] = "";
            $totalLine['Invoice Posted Date'] = "";
            $totalLine['Complete Date'] = $previousLine['Complete Date'];

            $totalLine['Net Total'] = '';
            $totalLine['Grand Total'] = $helpcallGrandTotal;

            $outputData[] = $totalLine;
        }

        //Grand Total Filter
        if (!is_null($filter->grandTotalFrom) || !is_null($filter->grandTotalTo)) {
            $helpcallCodes = [""];

            foreach ($outputData as $row) {
                $addHelpcallCode = true;
                if (!is_numeric($row['Grand Total'])) {
                    continue;
                }

                if (!is_null($filter->grandTotalFrom)) {
                    if ($row['Grand Total'] >= $filter->grandTotalFrom) {
                    } else {
                        $addHelpcallCode = false;
                    }
                }

                if (!is_null($filter->grandTotalTo)) {
                    if ($row['Grand Total'] <= $filter->grandTotalTo) {
                    } else {
                        $addHelpcallCode = false;
                    }
                }

                if ($addHelpcallCode) {
                    $helpcallCodes[] = $row['Helpcall Code'];
                }
            }

            $helpcallCodes = array_unique($helpcallCodes);

            foreach ($outputData as $key => $row) {
                if (! in_array($row['Helpcall Code'], $helpcallCodes)) {
                    unset($outputData[$key]);
                }
            }
        }

        return $outputData;
    }

    private function basicFilterAll($query, $filter)
    {
        if (!is_null($filter->code)) {
            $query->where('helpcall_code', 'like', '%' . $filter->code . '%');
        }

        if (!is_null($filter->site_id)) {
            $query->where('site_id', $filter->site_id);

            if (!is_null($filter->building_id)) {
                $query->where('building_id', $filter->building_id);

                if (!is_null($filter->room_id)) {
                    $query->where('room_id', $filter->room_id);
                }
            };
        }

        if (!is_null($filter->completedFrom)) {
            $completedFrom = \DateTime::createFromFormat('d/m/Y', $filter->completedFrom);

            if ($completedFrom) {
                $query->where(
                    \DB::raw("DATE_FORMAT(completed_date, '%Y-%m-%d')"),
                    '>=',
                    $completedFrom->format('Y-m-d')
                );
            }
        }

        if (!is_null($filter->completedTo)) {
            $completedTo = \DateTime::createFromFormat('d/m/Y', $filter->completedTo);

            if ($completedTo) {
                $query->where(
                    \DB::raw("DATE_FORMAT(completed_date, '%Y-%m-%d')"),
                    '<=',
                    $completedTo->format('Y-m-d')
                );
            }
        }

        if (!is_null($filter->loggedFrom)) {
            $loggedFrom = \DateTime::createFromFormat('d/m/Y', $filter->loggedFrom);

            if ($loggedFrom) {
                $query->where(
                    \DB::raw("DATE_FORMAT(logged_date, '%Y-%m-%d')"),
                    '>=',
                    $loggedFrom->format('Y-m-d')
                );
            }
        }

        if (!is_null($filter->loggedTo)) {
            $loggedTo = \DateTime::createFromFormat('d/m/Y', $filter->loggedTo);

            if ($loggedTo) {
                $query->where(
                    \DB::raw("DATE_FORMAT(logged_date, '%Y-%m-%d')"),
                    '<=',
                    $loggedTo->format('Y-m-d')
                );
            }
        }

        if (!is_null($filter->status)) {
            $query->where('helpcall_status_id', $filter->status);
        }

        if (!is_null($filter->category)) {
            $query->where('category_id', $filter->category);
        }

        if (!is_null($filter->siteType)) {
            $query->where('site_type_id', $filter->siteType);
        }

        if (!is_null($filter->propTenure)) {
            $query->where('prop_tenure_id', $filter->propTenure);
        }
    }

    private function addCustomFiltering(array $inputs, &$whereCodes, &$orCodes, &$whereTexts)
    {
        if ($val = array_get($inputs, 'code')) {
            array_push($whereTexts, "Code contains '" . $val . "'");
        }

        if ($val = array_get($inputs, 'site_id')) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);

            if ($val = array_get($inputs, 'building_id')) {
                array_push($whereCodes, ['building', 'building_id', 'building_code', $val, "Building Code"]);

                if ($val = array_get($inputs, 'room_id')) {
                    array_push($whereCodes, ['room', 'room_id', 'room_number', $val, "Room Number"]);
                }
            }
        }

        if ($val = array_get($inputs, 'completedFrom')) {
            array_push($whereTexts, "Completed Date from {$val}");
        }

        if ($val = array_get($inputs, 'completedTo')) {
            array_push($whereTexts, "Completed Date to {$val}");
        }

        if ($val = array_get($inputs, 'loggedFrom')) {
            array_push($whereTexts, "Logged Date from {$val}");
        }

        if ($val = array_get($inputs, 'loggedTo')) {
            array_push($whereTexts, "Logged Date to {$val}");
        }

        if ($val = array_get($inputs, 'status')) {
            array_push(
                $whereCodes,
                ['helpcall_status', 'helpcall_status_id', 'code', $val, 'Status']
            );
        }

        if ($val = array_get($inputs, 'category')) {
            array_push(
                $whereCodes,
                ['category', 'category_id', 'category_name', $val, 'Category']
            );
        }

        if ($val = array_get($inputs, 'siteType')) {
            array_push($whereCodes, ['site_type', 'site_type_id', 'site_type_code', $val, 'Site Type']);
        }

        if ($val = array_get($inputs, 'propTenure')) {
            array_push($whereCodes, ['prop_tenure', 'prop_tenure_id', 'prop_tenure_code', $val, 'Tenure']);
        }

        if ($val = array_get($inputs, 'operative_user_id')) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Operative']);
        }

        if ($val = array_get($inputs, 'netTotalFrom')) {
            array_push($whereTexts, "Net Total from {$val}");
        }

        if ($val = array_get($inputs, 'netTotalTo')) {
            array_push($whereTexts, "Net Total to {$val}");
        }

        if ($val = array_get($inputs, 'supplier')) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Supplier']);
        }

        if ($val = array_get($inputs, 'grandTotalFrom')) {
            array_push($whereTexts, "Grand Total from {$val}");
        }

        if ($val = array_get($inputs, 'grandTotalTo')) {
            array_push($whereTexts, "Grand Total to {$val}");
        }
    }

    private function getRCB01HelpcallDetails(
        $grandTotalUnion = false,
        $includeWhere = false,
        $helpcallId = null,
        $grandTotal = 0,
        $orderBy = false
    ) {
        //DLO Material

        $query = " SELECT * FROM(";

        $query .= " SELECT";

        $query .= "   helpcall.helpcall_id,";
        $query .= "   helpcall.helpcall_code,";
        $query .= "   helpcall.site_id,";
        $query .= "   helpcall.building_id,";
        $query .= "   helpcall.room_id,";
        $query .= "   '' AS 'dlo_job_id',";
        $query .= "   '' AS 'contact_id',";
        $query .= "   '' AS 'user_id',";
        $query .= "   date_format(`helpcall`.`completed_date`,'%Y-%m-%d') AS `completed_date`,";
        $query .= "   date_format(`helpcall`.`logged_date`,'%Y-%m-%d') AS `logged_date`,";
        $query .= "   helpcall.category_id,";
        $query .= "   helpcall.helpcall_status_id,";
        $query .= "   site.site_type_id,";
        $query .= "   site.prop_tenure_id,";

        $query .= "   1 AS 'Union Order',";
        $query .= "   helpcall.helpcall_code AS 'Helpcall Code',";
        $query .= "   helpcall.helpcall_desc AS 'Helpcall Description',";
        $query .= "   site.site_code AS 'Site Code',";
        $query .= "   site.site_desc AS 'Site Description',";
        $query .= "   building.building_code AS 'Building Code',";
        $query .= "   building.building_desc AS 'Building Description',";
        $query .= "   room.room_number AS 'Room No',";
        $query .= "   room.room_desc AS 'Room Description',";

        $query .= "   dlo_job.dlo_job_code AS 'DLO Job Code',";
        $query .= "   dlo_job.dlo_job_desc AS 'DLO Job Description',";
        $query .= "   '' AS 'Instruction Code',";
        $query .= "   '' AS 'Instruction Description',";
        $query .= "   '' AS 'Sub Contractor Code',";
        $query .= "   '' AS 'Sub Contractor Job Description',";
        $query .= "   '' AS 'Material Order Code',";
        $query .= "   '' AS 'Material Order Description',";
        $query .= "   dlo_job_material.dlo_job_material_code AS 'Material Code',";
        $query .= "   dlo_job_material.dlo_job_material_desc AS 'Material Item',";
        $query .= "   '' AS 'DLO Operative',";
        $query .= "   '' AS 'Supplier',";
        $query .= "   '' AS 'DLO Hrs',";
        $query .= "   '' AS 'Estimate',";
        $query .= "   '' AS 'Material Order Cost',";
        $query .= "   dlo_job_material.cost AS 'Material Cost',";
        $query .= "   '' AS 'DLO Labour Cost',";
        $query .= "   '' AS 'Instruction Cost',";
        $query .= "   '' AS 'DLO Subcontractor Cost',";
        $query .= "   dlo_job_material.cost AS 'Net Total',";
        $query .= "   '' AS 'Invoice Posted Date',";
        $query .= " date_format(`helpcall`.`completed_date`,'%d/%m/%Y') AS `Completed Date`,";
        $query .= "   $grandTotal AS 'Grand Total'";

        $query .= " FROM helpcall";
        $query .= "	inner join site on site.site_id = helpcall.site_id ";
        $query .= "	left join building on building.building_id = helpcall.building_id ";
        $query .= "	left join room on room.room_id = helpcall.room_id ";
        $query .= "	inner join dlo_job ON dlo_job.parent_id = helpcall.helpcall_id ";
        $query .= "     AND dlo_parent_type_id = 1";
        $query .= "	inner join dlo_job_material on dlo_job_material.dlo_job_id = dlo_job.dlo_job_id ";
        $query .= " WHERE helpcall.site_group_id = " . \Auth::user()->site_group_id;
        //DLO Labour
        $query .= " UNION ALL";
        $query .= " SELECT";
        $query .= "   helpcall.helpcall_id,";
        $query .= "   helpcall.helpcall_code,";
        $query .= "   helpcall.site_id,";
        $query .= "   helpcall.building_id,";
        $query .= "   helpcall.room_id,";
        $query .= "   dlo_job.dlo_job_id AS 'dlo_job_id',";
        $query .= "   '' AS 'contact_id',";
        $query .= "   dlo_job_operative.user_id AS 'user_id',";
        $query .= "   date_format(`helpcall`.`completed_date`,'%Y-%m-%d') AS `completed_date`,";
        $query .= "   date_format(`helpcall`.`logged_date`,'%Y-%m-%d') AS `logged_date`,";
        $query .= "   helpcall.category_id,";
        $query .= "   helpcall.helpcall_status_id,";
        $query .= "   site.site_type_id,";
        $query .= "   site.prop_tenure_id,";

        $query .= "   2 AS 'Union Order',";
        $query .= "   helpcall.helpcall_code AS 'Helpcall Code',";
        $query .= "   helpcall.helpcall_desc AS 'Helpcall Description',";
        $query .= "   site.site_code AS 'Site Code',";
        $query .= "   site.site_desc AS 'Site Description',";
        $query .= "   building.building_code AS 'Building Code',";
        $query .= "   building.building_desc AS 'Building Description',";
        $query .= "   room.room_number AS 'Room No',";
        $query .= "   room.room_desc AS 'Room Description',";
        $query .= "   dlo_job.dlo_job_code AS 'Dlo Job Code',";
        $query .= "   dlo_job.dlo_job_desc AS 'Dlo Job Description',";
        $query .= "   '' AS 'Instruction Code',";
        $query .= "   '' AS 'Instruction Description',";
        $query .= "   '' AS 'Sub Contractor Code',";
        $query .= "   '' AS 'Sub Contractor Job Description',";
        $query .= "   '' AS 'Material Order Code',";
        $query .= "   '' AS 'Material Order Description',";
        $query .= "   '' AS 'Material Code',";
        $query .= "   '' AS 'Material Item',";
        $query .= "   user.display_name AS 'DLO Operative',";
        $query .= "   '' AS 'Supplier',";
        $query .= "   DloLabour.hours AS 'DLO Hrs',";
        $query .= "   '' AS 'Estimate',";
        $query .= "   '' AS 'Material Order Cost',";
        $query .= "   '' AS 'Material Cost',";
        $query .= "   DloLabour.labour_total AS 'DLO Labour Cost',";
        $query .= "   '' AS 'Instruction Cost',";
        $query .= "   '' AS 'DLO Subcontractor Cost',";
        $query .= "   DloLabour.labour_total AS 'Net Total',";
        $query .= "   '' AS 'Invoice Posted Date',";
        $query .= "     date_format(`helpcall`.`completed_date`,'%d/%m/%Y') AS `Completed Date`,";
        $query .= "   $grandTotal AS 'Grand Total'";

        $query .= " FROM helpcall";
        $query .= "	inner join site on site.site_id = helpcall.site_id ";
        $query .= "	left join building on building.building_id = helpcall.building_id ";
        $query .= "	left join room on room.room_id = helpcall.room_id ";
        $query .= "	inner join dlo_job ON dlo_job.parent_id = helpcall.helpcall_id ";
        $query .= "     AND dlo_parent_type_id = 1";
        $query .= "	inner join dlo_job_operative on dlo_job_operative.dlo_job_id = dlo_job.dlo_job_id ";
        $query .= "	inner join user on dlo_job_operative.user_id = user.id ";
        $query .= " LEFT JOIN (
                                select  dlo_job_id, dlo_job_operative_id,
                                sum(labour + labour_1_3 + labour_1_5 + labour_2) as 'hours',
                                sum(labour_total) as 'labour_total'
                                from dlo_job_labour
                                GROUP BY dlo_job_id, dlo_job_operative_id
                    )  AS DloLabour
                    on dlo_job.dlo_job_id = DloLabour.dlo_job_id
                    and dlo_job_operative.dlo_job_operative_id = DloLabour.dlo_job_operative_id";
        $query .= " WHERE helpcall.site_group_id = " . \Auth::user()->site_group_id;

        //Helpcall Instructions
        $query .= " UNION ALL";
        $query .= " SELECT";
        $query .= "   helpcall.helpcall_id,";
        $query .= "   helpcall.helpcall_code,";
        $query .= "   helpcall.site_id,";
        $query .= "   helpcall.building_id,";
        $query .= "   helpcall.room_id,";
        $query .= "   '' AS 'dlo_job_id',";
        $query .= "   contact.contact_id AS 'contact_id',";
        $query .= "   '' AS 'user_id',";
        $query .= "   date_format(`helpcall`.`completed_date`,'%Y-%m-%d') AS `completed_date`,";
        $query .= "   date_format(`helpcall`.`logged_date`,'%Y-%m-%d') AS `logged_date`,";
        $query .= "   helpcall.category_id,";
        $query .= "   helpcall.helpcall_status_id,";
        $query .= "   site.site_type_id,";
        $query .= "   site.prop_tenure_id,";

        $query .= "   3 AS 'Union Order',";
        $query .= "   helpcall.helpcall_code AS 'Helpcall Code',";
        $query .= "   helpcall.helpcall_desc AS 'Helpcall Description',";
        $query .= "   site.site_code AS 'Site Code',";
        $query .= "   site.site_desc AS 'Site Description',";
        $query .= "   building.building_code AS 'Building Code',";
        $query .= "   building.building_desc AS 'Building Description',";
        $query .= "   room.room_number AS 'Room No',";
        $query .= "   room.room_desc AS 'Room Description',";
        $query .= "   '' AS 'DLO Job Code',";
        $query .= "   '' AS 'DLO Job Description',";
        $query .= "   instruction.instruction_code AS 'Instruction Code',";
        $query .= "   instruction.instruction_desc AS 'Instruction Description',";
        $query .= "   '' AS 'Sub Contractor Code',";
        $query .= "   '' AS 'Sub Contractor Job Description',";
        $query .= "   '' AS 'Material Order Code',";
        $query .= "   '' AS 'Material Order Description',";
        $query .= "   '' AS 'Material Code',";
        $query .= "   '' AS 'Material Item',";
        $query .= "   '' AS 'DLO Operative',";
        $query .= "   contact.contact_name AS 'Supplier',";
        $query .= "   '' AS 'DLO Hrs',";
        $query .= "   instruction.est_net_total AS 'Estimate',";
        $query .= "   '' AS 'Material Order Cost',";
        $query .= "   '' AS 'Material Cost',";
        $query .= "   '' AS 'DLO Labour Cost',";
        $query .= "   invoice_fin_account.amount AS 'Instruction Cost',";
        $query .= "   '' AS 'DLO Subcontractor Cost',";
        $query .= "   invoice_fin_account.amount AS 'Net Total',";
        $query .= "     date_format(`invoice`.`last_changed_to_posted_date`,'%d/%m/%Y') AS `Invoice Posted Date`,";
        $query .= "     date_format(`helpcall`.`completed_date`,'%d/%m/%Y') AS `Completed Date`,";
        $query .= "   $grandTotal AS 'Grand Total'";

        $query .= " FROM helpcall";
        $query .= "	inner join site on site.site_id = helpcall.site_id ";
        $query .= "	left join building on building.building_id = helpcall.building_id ";
        $query .= "	left join room on room.room_id = helpcall.room_id ";
        $query .= "	  inner join instruction ON instruction.parent_id = helpcall.helpcall_id ";
        $query .= "     AND parent_type_id = 1";
        $query .= "	inner join contact on contact.contact_id = instruction.supplier_contact_id ";
        $query .= "	left join invoice_fin_account "
                . "         ON instruction.instruction_id = invoice_fin_account.instruction_id ";
        $query .= "	left join invoice on invoice.invoice_id = invoice_fin_account.invoice_id ";
        $query .= "	left join invoice_status "
                . "         ON invoice_status.invoice_status_id = invoice.invoice_status_id ";

        $query .= " WHERE (invoice_status.invoice_status_type_id IN ("
                    . InvoiceStatusType::APPROVED . "," . InvoiceStatusType::POSTED . ") "
                . "or invoice_status.invoice_status_type_id is null)";
        $query .= " AND helpcall.site_group_id = " . \Auth::user()->site_group_id;
        //DLO Sub Contract
        $query .= " UNION ALL";
        $query .= " SELECT";
        $query .= "   helpcall.helpcall_id,";
        $query .= "   helpcall.helpcall_code,";
        $query .= "   helpcall.site_id,";
        $query .= "   helpcall.building_id,";
        $query .= "   helpcall.room_id,";
        $query .= "   '' AS 'dlo_job_id',";
        $query .= "   contact.contact_id AS 'contact_id',";
        $query .= "   '' AS 'user_id',";
        $query .= "   date_format(`helpcall`.`completed_date`,'%Y-%m-%d') AS `completed_date`,";
        $query .= "   date_format(`helpcall`.`logged_date`,'%Y-%m-%d') AS `logged_date`,";
        $query .= "   helpcall.category_id,";
        $query .= "   helpcall.helpcall_status_id,";
        $query .= "   site.site_type_id,";
        $query .= "   site.prop_tenure_id,";

        $query .= "   4 AS 'Union Order',";
        $query .= "   helpcall.helpcall_code AS 'Helpcall Code',";
        $query .= "   helpcall.helpcall_desc AS 'Helpcall Description',";
        $query .= "   site.site_code AS 'Site Code',";
        $query .= "   site.site_desc AS 'Site Description',";
        $query .= "   building.building_code AS 'Building Code',";
        $query .= "   building.building_desc AS 'Building Description',";
        $query .= "   room.room_number AS 'Room No',";
        $query .= "   room.room_desc AS 'Room Description',";
        $query .= "   dlo_job.dlo_job_code AS 'DLO Job Code',";
        $query .= "   '' AS 'DLO Job Description',";
        $query .= "   '' AS 'Instruction Code',";
        $query .= "   '' AS 'Instruction Description',";
        $query .= "   instruction.instruction_code AS 'Sub Contractor Code',";
        $query .= "   instruction.instruction_desc AS 'Sub Contractor Job Description',";
        $query .= "   '' AS 'Material Order Code',";
        $query .= "   '' AS 'Material Order Description',";
        $query .= "   '' AS 'Material Code',";
        $query .= "   '' AS 'Material Item',";
        $query .= "   '' AS 'DLO Operative',";
        $query .= "   contact.contact_name AS 'Supplier',";
        $query .= "   '' AS 'DLO Hrs',";
        $query .= "   instruction.est_net_total AS 'Estimate',";
        $query .= "   '' AS 'Material Order Cost',";
        $query .= "   '' AS 'Material Cost',";
        $query .= "   '' AS 'DLO Labour Cost',";
        $query .= "   '' AS 'Instruction Cost',";
        $query .= "   invoice_fin_account.amount AS 'DLO Subcontractor Cost',";
        $query .= "   invoice_fin_account.amount AS 'Net Total',";
        $query .= "     date_format(`invoice`.`last_changed_to_posted_date`,'%d/%m/%Y') AS `Invoice Posted Date`,";
        $query .= "     date_format(`helpcall`.`completed_date`,'%d/%m/%Y') AS `Completed Date`,";
        $query .= "   $grandTotal AS 'Grand Total'";

        $query .= " FROM helpcall";
        $query .= "	inner join site on site.site_id = helpcall.site_id ";
        $query .= "	left join building on building.building_id = helpcall.building_id ";
        $query .= "	left join room on room.room_id = helpcall.room_id ";
        $query .= "	  inner join dlo_job ON dlo_job.parent_id = helpcall.helpcall_id ";
        $query .= "     AND dlo_parent_type_id = 1";
        $query .= "	inner join instruction on instruction.parent_id = dlo_job.dlo_job_id ";
        $query .= "     AND instruction.parent_type_id = 2 and instruction_po_type_id is null";
        $query .= "	inner join contact on contact.contact_id = instruction.supplier_contact_id ";
        $query .= "	left join invoice_fin_account "
                . "         ON instruction.instruction_id = invoice_fin_account.instruction_id ";
        $query .= "	left join invoice on invoice.invoice_id = invoice_fin_account.invoice_id ";
        $query .= "	left join invoice_status "
                . "         ON invoice_status.invoice_status_id = invoice.invoice_status_id ";

        $query .= " WHERE (invoice_status.invoice_status_type_id IN ("
                    . InvoiceStatusType::APPROVED . "," . InvoiceStatusType::POSTED . ") "
                . "or invoice_status.invoice_status_type_id is null)";
        $query .= " AND helpcall.site_group_id = " . \Auth::user()->site_group_id;
        //DLO Material Order (Purchase Order Instruction)
        $query .= " UNION ALL";
        $query .= " SELECT";
        $query .= "   helpcall.helpcall_id,";
        $query .= "   helpcall.helpcall_code,";
        $query .= "   helpcall.site_id,";
        $query .= "   helpcall.building_id,";
        $query .= "   helpcall.room_id,";
        $query .= "   '' AS 'dlo_job_id',";
        $query .= "   contact.contact_id AS 'contact_id',";
        $query .= "   '' AS 'user_id',";
        $query .= "   date_format(`helpcall`.`completed_date`,'%Y-%m-%d') AS `completed_date`,";
        $query .= "   date_format(`helpcall`.`logged_date`,'%Y-%m-%d') AS `logged_date`,";
        $query .= "   helpcall.category_id,";
        $query .= "   helpcall.helpcall_status_id,";
        $query .= "   site.site_type_id,";
        $query .= "   site.prop_tenure_id,";

        $query .= "   5 AS 'Union Order',";
        $query .= "   helpcall.helpcall_code AS 'Helpcall Code',";
        $query .= "   helpcall.helpcall_desc AS 'Helpcall Description',";
        $query .= "   site.site_code AS 'Site Code',";
        $query .= "   site.site_desc AS 'Site Description',";
        $query .= "   building.building_code AS 'Building Code',";
        $query .= "   building.building_desc AS 'Building Description',";
        $query .= "   room.room_number AS 'Room No',";
        $query .= "   room.room_desc AS 'Room Description',";
        $query .= "   dlo_job.dlo_job_code AS 'DLO Job Code',";
        $query .= "   '' AS 'DLO Job Description',";
        $query .= "   '' AS 'Instruction Code',";
        $query .= "   '' AS 'Instruction Description',";
        $query .= "   '' AS 'Sub Contractor Code',";
        $query .= "   '' AS 'Sub Contractor Job Description',";
        $query .= "   instruction.instruction_code AS 'Material Order Code',";
        $query .= "   instruction.instruction_desc AS 'Material Order Description',";
        $query .= "   '' AS 'Material Code',";
        $query .= "   '' AS 'Material Item',";
        $query .= "   '' AS 'DLO Operative',";
        $query .= "   contact.contact_name AS 'Supplier',";
        $query .= "   '' AS 'DLO Hrs',";
        $query .= "   instruction.est_net_total AS 'Estimate',";
        $query .= "   invoice_fin_account.amount AS 'Material Order Cost',";
        $query .= "   '' AS 'Material Cost',";
        $query .= "   '' AS 'DLO Labour Cost',";
        $query .= "   '' AS 'Instruction Cost',";
        $query .= "   '' AS 'DLO Subcontractor Cost',";
        $query .= "   invoice_fin_account.amount AS 'Net Total',";
        $query .= "   date_format(`invoice`.`last_changed_to_posted_date`,'%d/%m/%Y') AS `Invoice Posted Date`,";
        $query .= "   date_format(`helpcall`.`completed_date`,'%d/%m/%Y') AS `Completed Date`,";
        $query .= "   $grandTotal AS 'Grand Total'";

        $query .= " FROM helpcall";
        $query .= "	inner join site on site.site_id = helpcall.site_id ";
        $query .= "	left join building on building.building_id = helpcall.building_id ";
        $query .= "	left join room on room.room_id = helpcall.room_id ";
        $query .= " inner join dlo_job ON dlo_job.parent_id = helpcall.helpcall_id ";
        $query .= "     AND dlo_parent_type_id = 1";
        $query .= "	inner join instruction on instruction.parent_id = dlo_job.dlo_job_id ";
        $query .= "     AND instruction.parent_type_id = 2 and instruction_po_type_id = 1";
        $query .= "	inner join contact on contact.contact_id = instruction.supplier_contact_id ";
        $query .= "	left join invoice_fin_account "
                . "         ON instruction.instruction_id = invoice_fin_account.instruction_id ";
        $query .= "	left join invoice on invoice.invoice_id = invoice_fin_account.invoice_id ";
        $query .= "	left join invoice_status "
                . "         ON invoice_status.invoice_status_id = invoice.invoice_status_id ";

        $query .= " WHERE (invoice_status.invoice_status_type_id IN ("
                    . InvoiceStatusType::APPROVED . "," . InvoiceStatusType::POSTED . ") "
                . "or invoice_status.invoice_status_type_id is null)";
        $query .= " AND helpcall.site_group_id = " . \Auth::user()->site_group_id;
            //Grand Total Union
//        if ($grandTotalUnion) {
//            $query .= " UNION ALL";
//            $query .= " SELECT";
//            $query .= "   helpcall.helpcall_id,";
//            $query .= "   helpcall.site_id,";
//            $query .= "   helpcall.building_id,";
//            $query .= "   helpcall.room_id,";
//            $query .= "   '' AS 'dlo_job_id',";
//            $query .= "  '' AS 'contact_id',";
//
//            $query .= "   6 AS 'Union Order',";
//            $query .= "   helpcall.helpcall_code AS 'Helpcall Code',";
//            $query .= "   helpcall.helpcall_desc AS 'Helpcall Description',";
//            $query .= "   site.site_code AS 'Site Code',";
//            $query .= "   site.site_desc AS 'Site Description',";
//            $query .= "   building.building_code AS 'Building Code',";
//            $query .= "   building.building_desc AS 'Building Description',";
//            $query .= "   room.room_number AS 'Room No',";
//            $query .= "   room.room_desc AS 'Room Description',";
//            $query .= "   '' AS 'DLO Job Code',";
//            $query .= "   '' AS 'DLO Job Description',";
//            $query .= "   '' AS 'Instruction Code',";
//            $query .= "   '' AS 'Instruction Description',";
//            $query .= "   '' AS 'Sub Contractor Code',";
//            $query .= "   '' AS 'Sub Contractor Job Description',";
//            $query .= "   '' AS 'Material Order Code',";
//            $query .= "   '' AS 'Material Order Description',";
//            $query .= "   '' AS 'Material Code',";
//            $query .= "   '' AS 'Material Item',";
//            $query .= "   '' AS 'DLO Operative',";
//            $query .= "   '' AS 'Supplier',";
//            $query .= "   '' AS 'DLO Hrs',";
//            $query .= "   '' AS 'Estimate',";
//            $query .= "   '' AS 'Material Order Cost',";
//            $query .= "   '' AS 'Material Cost',";
//            $query .= "   '' AS 'DLO Labour Cost',";
//            $query .= "   '' AS 'Instruction Cost',";
//            $query .= "   '' AS 'DLO Subcontractor Cost',";
//            $query .= "   '' AS 'Net Total',";
//            $query .= "   '' AS 'Invoice Posted Date',";
//            $query .= "   '' AS `Completed Date`,";
//            $query .= "   $grandTotal AS 'Grand Total'";
//
//            $query .= " FROM helpcall";
//            $query .= "   inner join site on site.site_id = helpcall.site_id ";
//            $query .= "   left join building on building.building_id = helpcall.building_id ";
//            $query .= "   left join room on room.room_id = helpcall.room_id ";
//        }

        $query .= ") AS temp";

        if ($includeWhere) {
            $query .= "	WHERE temp.helpcall_id = $helpcallId";
        }

//        $query .= " order by `Helpcall Code` ";

        return $query;
    }
}
