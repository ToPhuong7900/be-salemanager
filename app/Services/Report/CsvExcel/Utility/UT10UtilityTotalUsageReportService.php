<?php

namespace Tfcloud\Services\Report\CsvExcel\Utility;

use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Math;
use Tfcloud\Models\Report;
use Tfcloud\Services\PermissionService;
use Tfcloud\Models\Views\VwUt10;
use Tfcloud\Services\Report\CsvExcel\CsvExcelReportBaseService;

class UT10UtilityTotalUsageReportService extends CsvExcelReportBaseService
{
    protected $startDate;
    protected $endDate;
    protected $finYearIds;

    protected $permissionService = null;

    public function __construct(
        PermissionService $permissionService,
        Report $report
    ) {
        parent::__construct($permissionService);
        $this->reportBoxFilterLoader = new ClassMapReportLoader($report);
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        $this->setFilterProperties($inputs);

        $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);

        $reportFilterQuery->getOperatorParser()->setPrefixTables($this->getPrefixTables());

        $filterText = $reportFilterQuery->getFilterDetailText();

        return $this->createOutputData();
    }

    protected function setFilterProperties($inputs)
    {
        $this->startDate = null;
        $this->endDate = null;

        $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);

        $date = $reportFilterQuery->getValueFilterField('invoice_date');
        if (!empty($date)) {
            list($this->startDate, $this->endDate) = $date[0];
        }

        $siteId = $reportFilterQuery->getValueFilterField('site_id');
        if (!empty($siteId)) {
            $this->siteId = $siteId;
        }

        $buildingId = $reportFilterQuery->getValueFilterField('building_id');
        if (!empty($buildingId)) {
            $this->buildingId = $buildingId;
        }
    }

    protected function createOutputData()
    {
        $query = VwUt10::userSiteGroup()
            ->where('invoice_date', '>=', $this->startDate)
            ->where('invoice_date', '<=', $this->endDate);

        if (isset($this->siteId)) {
            $query->where(
                'site_id',
                '=',
                $this->siteId
            );
        }

        if (isset($this->buildingId)) {
            $query->where(
                'building_id',
                '=',
                $this->buildingId
            );
        }

            $query->select([
                'ut_utility_code',
                'ut_utility_desc',
                'site_code',
                'site_desc',
                'building_code',
                'building_desc',
                'prop_zone_code',
                'prop_zone_desc',
                'prop_external_code',
                'prop_external_desc',
                'ut_type_code',
                'account_number',
                'meter_number',
                'mpan_mprn',
                'organisation',
                'contact_name',
                'fin_account_code',
                'active',
                'user_text1',
                'user_text2',
                'user_text3',
                'user_text4',
                'user_text5',
                'user_text6',
                'user_check1',
                'user_check2',
                'user_check3',
                'user_check4',
                'user_contact1',
                'user_contact2',
                'user_contact3',
                'user_contact4',
                'user_contact5',
                'user_memo1',
                'user_memo2',
                \DB::raw('ifnull(sum(unit_usage), 0) as total_usage'),
            ])->groupBy('ut_utility_code');

        $data = $query->get();

        $result = [];

        foreach ($data as $record) {
            $line['Utility Code'] = $record->ut_utility_code;
            $line['Utility Description'] = $record->ut_utility_desc;
            $line['Site Code'] = $record->site_code;
            $line['Site Description'] = $record->site_desc;
            $line['Building Code'] = $record->building_code;
            $line['Building Description'] = $record->building_desc;
            $line['Zone Code'] = $record->prop_zone_code;
            $line['Zone Description'] = $record->prop_zone_desc;
            $line['External Area Code'] = $record->prop_external_code;
            $line['External Area Description'] = $record->prop_external_desc;
            $line['Utility Type'] = $record->ut_type_code;
            $line['Utility Account No.'] = $record->account_number;
            $line['Meter No.'] = $record->meter_number;
            $line['MPAN/MPRN'] = $record->mpan_mprn;
            $line['Supplier Organisation'] = $record->organisation;
            $line['Supplier Name'] = $record->contact_name;
            $line['Account Code'] = $record->fin_account_code;
            $line['Active'] = $record->active;
            $line['Total Usage'] = $record->total_usage;
            $line['User Text 1'] = $record->user_text1;
            $line['User Text 2'] = $record->user_text2;
            $line['User Text 3'] = $record->user_text3;
            $line['User Text 4'] = $record->user_text4;
            $line['User Text 5'] = $record->user_text5;
            $line['User Text 6'] = $record->user_text6;
            $line['User Check 1'] = $record->user_check1;
            $line['User Check 2'] = $record->user_check2;
            $line['User Check 3'] = $record->user_check3;
            $line['User Check 4'] = $record->user_check4;
            $line['User Contact 1'] = $record->user_contact1;
            $line['User Contact 2'] = $record->user_contact2;
            $line['User Contact 3'] = $record->user_contact3;
            $line['User Contact 4'] = $record->user_contact4;
            $line['User Contact 5'] = $record->user_contact5;
            $line['User Memo 1'] = $record->user_memo1;
            $line['User Memo 2'] = $record->user_memo2;

            $result[] = $line;
        }

        return $result;
    }

    protected function getPrefixTables()
    {
        return [
            'vw_ut10' => '*'
        ];
    }
}
