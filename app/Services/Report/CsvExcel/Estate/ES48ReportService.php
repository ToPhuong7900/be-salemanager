<?php

namespace Tfcloud\Services\Report\CsvExcel\Estate;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Models\Personnel\PersDayOff;
use Tfcloud\Models\Personnel\PersPublicHoliday;
use Tfcloud\Models\Personnel\PersReasonType;
use Tfcloud\Models\Personnel\PersWorkPatternDetail;
use Tfcloud\Models\Report;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\CsvExcel\CsvExcelReportBaseService;
use App\SBoxFilter\DLO\Report\DLO16FilterQuery;

class ES48ReportService extends CsvExcelReportBaseService
{
    private $reportBoxFilterLoader;

    public function __construct(PermissionService $permissionService, Report $report)
    {
        $this->permissionService = $permissionService;
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function getReportData($inputs, &$filterText)
    {
        $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
        $records = $reportFilterQuery->getAll(0);
        $filterText = $reportFilterQuery->getFilterDetailText();
        return $this->createOutputData($records);
    }

    public function createOutputData($records)
    {
        $result = [];
        foreach ($records as $record) {
            $line['Code']                   = $record->record_code;
            $line['Record Type']            = $record->record_type;
            $line['Description']            = $record->record_desc;
            $line['Site']                   = $record->site_code;
            $line['Site Description']       = $record->site_desc;
            $line['Building']               = $record->building_code;
            $line['Building Description']   = $record->building_desc;
            $line['Room']                   = $record->room_number;
            $line['Location Comment']       = $record->comment;

            $result[] = $line;
        }

        return $result;
    }
}
