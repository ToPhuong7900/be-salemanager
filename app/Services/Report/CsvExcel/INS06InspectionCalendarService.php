<?php

namespace Tfcloud\Services\Report\CsvExcel;

use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Traits\EmailTrait;
use Tfcloud\Models\Report;
use Tfcloud\Services\PermissionService;

class INS06InspectionCalendarService extends CsvExcelReportBaseService
{
    use EmailTrait;

    private $reportBoxFilterLoader;

    public function __construct(
        PermissionService $permissionService,
        Report $report = null
    ) {
        $this->permissionService = $permissionService;

        $this->reportBoxFilterLoader  = new ClassMapReportLoader($report);
    }

    public function getReportData($inputs, &$filterText, &$sOrderText)
    {
        $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
        $reportData = $reportFilterQuery->getAll(0);
        $inspections = $reportData['data'];
        $filterText = $reportFilterQuery->getFilterDetailText();

        $outputData = [];

        foreach ($inspections as $data) {
            $bAdd = false;
            $code = $data['code'];
            unset($data['code']);

            $row['Inspection Type'] = array_get($code, 'name');

            //Report PL05
            if (!empty($data['plant'])) {
                $row['Plant'] = array_get($data['plant'], 'name');
                $row['Active'] = array_get($data['plant'], 'plant_active');
                unset($data['plant']);
            }

            foreach ($data as $key => $value) {
                $count = array_get($value, 'totalCount', 0);

                if ($count) {
                    $bAdd = true;
                    $row[$key] = $count;
                } else {
                    $row[$key] = '';
                }
            }

            if ($bAdd) {
                $outputData[] = $row;
            }
        }

        // Do not have special order
        $sOrderText = "Inspection Types (ascending)";

        return $outputData;
    }
}
