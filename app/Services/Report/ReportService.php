<?php

namespace Tfcloud\Services\Report;

use Carbon\Carbon;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Constant\ReportConstant;
use Tfcloud\Lib\Email\Email;
use Tfcloud\Lib\Exceptions\InvalidRecordException;
use Tfcloud\Lib\Exceptions\PermissionException;
use Tfcloud\Lib\Filters\Admin\ModuleInterface\InterfaceLogFilter;
use Tfcloud\lib\Filters\Admin\System\Dlo\UserTradeLevelFilter;
use Tfcloud\Lib\Filters\Admin\System\ResourceBooking\CateringItemFilter;
use Tfcloud\Lib\Filters\Audit\AuditFilter;
use Tfcloud\Lib\Filters\BillingUpToDate;
use Tfcloud\Lib\Filters\Budget\BudgetFilter;
use Tfcloud\Lib\Filters\Budget\BudgetMasterFilter;
use Tfcloud\Lib\Filters\CaseManagement\CaseManagementStageFilter;
use Tfcloud\Lib\Filters\CaseManagement\CaseRecordFilter;
use Tfcloud\Lib\Filters\Common\CommonFilter;
use Tfcloud\Lib\Filters\CompletionDate;
use Tfcloud\Lib\Filters\ConditionFilter;
use Tfcloud\Lib\Filters\ContactFilter;
use Tfcloud\Lib\Filters\Contract\ContractCreditFilter;
use Tfcloud\Lib\Filters\Contract\ContractFilter;
use Tfcloud\Lib\Filters\Contract\ContractInstructionFilter;
use Tfcloud\Lib\Filters\Contract\ContractInspectionFilter;
use Tfcloud\Lib\Filters\Contract\ContractInspectionReportFilter;
use Tfcloud\Lib\Filters\Contract\ContractInvoiceCreditFilter;
use Tfcloud\Lib\Filters\Contract\ContractInvoiceFilter;
use Tfcloud\Lib\Filters\Contract\AFPFilter;
use Tfcloud\Lib\Filters\Contract\CertificateFilter;
use Tfcloud\Lib\Filters\CostPlus\CpContractFilter;
use Tfcloud\Lib\Filters\CostPlus\CpSalesCreditFilter;
use Tfcloud\Lib\Filters\CostPlus\CpSalesInvoiceFilter;
use Tfcloud\Lib\Filters\InstructionNEL01Filter;
use Tfcloud\Lib\Filters\InstructionNBS01Filter;
use Tfcloud\Lib\Filters\InspectionNBS02Filter;
use Tfcloud\Lib\Filters\Datafeed\DatafeedResultsFilter;
use Tfcloud\Lib\Filters\Dlo\DloJobFilter;
use Tfcloud\Lib\Filters\Dlo\DloJobLabourFilter;
use Tfcloud\Lib\Filters\Dlo\DloStockMaterialItemFilter;
use Tfcloud\Lib\Filters\DloJobMaterialItemFilter;
use Tfcloud\Lib\Filters\DloJobOperativeFilter;
use Tfcloud\Lib\Filters\DocumentFilter;
use Tfcloud\Lib\Filters\Education\CapacityFilter;
use Tfcloud\Lib\Filters\Education\SufficiencyFilter;
use Tfcloud\Lib\Filters\Education\SuitabilityFilter;
use Tfcloud\Lib\Filters\Education\SuitCFSurveyFilter;
use Tfcloud\Lib\Filters\Email\EmailFailureFilter;
use Tfcloud\Lib\Filters\Estate\AcquisitionFilter;
use Tfcloud\Lib\Filters\Estate\DeedPacketFilter;
use Tfcloud\Lib\Filters\Estate\DisposalsFilter;
use Tfcloud\Lib\Filters\Estate\EstateSalesInvoiceFilter;
use Tfcloud\Lib\Filters\Estate\EstBreakDateFilter;
use Tfcloud\Lib\Filters\Estate\EstChargeFilter;
use Tfcloud\Lib\Filters\Estate\EstRecordFilter;
use Tfcloud\lib\Filters\Estate\EstRentalPaymentFilter;
use Tfcloud\Lib\Filters\Estate\EstReviewFilter;
use Tfcloud\Lib\Filters\Estate\EstateUnitFilter;
use Tfcloud\Lib\Filters\Estate\FreeholdFilter;
use Tfcloud\Lib\Filters\Estate\GenChecklistItemFilter;
use Tfcloud\Lib\Filters\Estate\InvoiceGeneratePlanFilter;
use Tfcloud\Lib\Filters\Estate\LeaseInFilter;
use Tfcloud\Lib\Filters\Estate\LeaseOutAlienationFilter;
use Tfcloud\Lib\Filters\Estate\LeaseOutFilter;
use Tfcloud\Lib\Filters\Estate\LettableUnitFilter;
use Tfcloud\Lib\Filters\Estate\LettableUnitPropertyFilter;
use Tfcloud\Lib\Filters\Estate\OccupancyUnitPropertyFilter;
use Tfcloud\Lib\Filters\Estate\LettingPacketFilter;
use Tfcloud\Lib\Filters\Estate\RentalPaymentGeneratePlanFilter;
use Tfcloud\Lib\Filters\Estate\ValuationsFilter;
use Tfcloud\Lib\Filters\FinanceAccountCodeFilter;
use Tfcloud\Lib\Filters\FixedAsset\FixedAssetFilter;
use Tfcloud\Lib\Filters\FixedAsset\TransactionFilter;
use Tfcloud\Lib\Filters\GroundsMaintenance\GmContractExternalAreaTaskFilter;
use Tfcloud\Lib\Filters\GroundsMaintenance\GMContractFilter;
use Tfcloud\Lib\Filters\GroundsMaintenance\GmContractJobFilter;
use Tfcloud\Lib\Filters\HelpcallFilter;
use Tfcloud\Lib\Filters\IdworkFilter;
use Tfcloud\Lib\Filters\InspectionFilter;
use Tfcloud\Lib\Filters\InstructionFilter;
use Tfcloud\Lib\Filters\Invoice\InvoiceFilter;
use Tfcloud\Lib\Filters\NoteFilter;
use Tfcloud\Lib\Filters\PermitToWork\PermitToWorkFilter;
use Tfcloud\Lib\Filters\Personnel\TeamViewFilter;
use Tfcloud\Lib\Filters\Plant\PlantFilter;
use Tfcloud\Lib\Filters\Plant\PlantOutOfServiceHistoryFilter;
use Tfcloud\Lib\Filters\Project\ProjectActionFilter;
use Tfcloud\Lib\Filters\Project\ProjectBillOfQtyFilter;
use Tfcloud\Lib\Filters\Project\ProjectFilter;
use Tfcloud\Lib\Filters\Project\ProjectIssueFilter;
use Tfcloud\Lib\Filters\Project\ProjectProgressFilter;
use Tfcloud\Lib\Filters\Project\ProjectRiskFilter;
use Tfcloud\Lib\Filters\Project\ProjectTaskFilter;
use Tfcloud\Lib\Filters\ProjectCertificatedPaymentFilter;
use Tfcloud\Lib\Filters\ProjectChangeRequestFilter;
use Tfcloud\Lib\Filters\Properties\BlockFilter;
use Tfcloud\Lib\Filters\Properties\BuildingFilter;
use Tfcloud\Lib\Filters\Properties\PR21ComplianceFilter;
use Tfcloud\Lib\Filters\Properties\PropExternalFilter;
use Tfcloud\Lib\Filters\Properties\PropReviewFilter;
use Tfcloud\Lib\Filters\Properties\RoomCapacityFilter;
use Tfcloud\Lib\Filters\Properties\RoomFilter;
use Tfcloud\lib\Filters\Properties\RunningCostFilter;
use Tfcloud\Lib\Filters\Properties\SiteFilter;
use Tfcloud\Lib\Filters\QstFilter;
use Tfcloud\Lib\Filters\Questionnaire\FiledQstActionFilter;
use Tfcloud\Lib\Filters\Questionnaire\FiledQuestionnaireFilter;
use Tfcloud\Lib\Filters\ReportFilter;
use Tfcloud\Lib\Filters\ResourceBooking\RbBookableResourceFilter;
use Tfcloud\Lib\Filters\ResourceBooking\RbBookableResourceSearchFilter;
use Tfcloud\Lib\Filters\ResourceBooking\RbCateringBookingFilter;
use Tfcloud\Lib\Filters\ResourceBooking\RbResourceBookingFilter;
use Tfcloud\Lib\Filters\SorFilter;
use Tfcloud\Lib\Filters\Stock\StockInventoryFilter;
use Tfcloud\Lib\Filters\TimeSheetFilter;
use Tfcloud\Lib\Filters\Tree\TreeSurveyFilter;
use Tfcloud\Lib\Filters\Tree\TreeSurveyActionFilter;
use Tfcloud\Lib\Filters\Utility\UtilityBillFilter;
use Tfcloud\Lib\Filters\Utility\UtilityFilter;
use Tfcloud\Lib\Permissions\EstatePermission;
use Tfcloud\Jobs\SendReportEmail;
use Tfcloud\Models\AuditAction;
use Tfcloud\Models\CrudAccessLevel;
use Tfcloud\Models\DloJobLabour;
use Tfcloud\Models\DloSalesInvoiceStatus;
use Tfcloud\Models\FinAccountType;
use Tfcloud\Models\FinYear;
use Tfcloud\Models\GenTable;
use Tfcloud\Models\InstructionParentType;
use Tfcloud\Models\InterfacePermission;
use Tfcloud\Models\InvoiceStatus;
use Tfcloud\Models\Module;
use Tfcloud\Models\ModuleCode;
use Tfcloud\Models\QstType;
use Tfcloud\Models\RbCateringItem;
use Tfcloud\Models\Report;
use Tfcloud\Models\ReportFavourite;
use Tfcloud\Models\ReportField;
use Tfcloud\Models\SystemReport;
use Tfcloud\Models\User;
use Tfcloud\Models\Usergroup;
use Tfcloud\Models\UsergroupType;
use Tfcloud\Models\UserReport;
use Tfcloud\Models\UserReportPdfDesign;
use Tfcloud\Models\UserReportType;
use Tfcloud\Models\UserSiteAccess;
use Tfcloud\Services\Admin\General\ContactService;
use Tfcloud\Services\Admin\General\UserGroupsService;
use Tfcloud\Services\Admin\General\UserTradeLevelService;
use Tfcloud\Services\Admin\Interfaces\InterfaceLogService;
use Tfcloud\Services\Admin\System\Finance\AccountCodeService;
use Tfcloud\Services\Admin\System\Finance\SorService;
use Tfcloud\Services\Admin\System\Questionnaire\QuestionnaireService;
use Tfcloud\Services\AuditService;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\Budget\ExpenditureBudgetListService;
use Tfcloud\Services\Budget\IncomingBudgetListService;
use Tfcloud\services\CaseManagement\CaseMgmt\CaseManagementStageService;
use Tfcloud\Services\CaseManagement\CaseMgmt\CaseRecordService;
use Tfcloud\Services\Condition\ConditionService;
use Tfcloud\Services\Condition\IdworkService;
use Tfcloud\Services\Contract\ContractService;
use Tfcloud\Services\Contract\CreditService;
use Tfcloud\Services\Contract\InstructionService as InstructionService2;
use Tfcloud\Services\Contract\InvoiceCreditService;
use Tfcloud\Services\Contract\InvoiceService as InvoiceService2;
use Tfcloud\Services\CostPlus\CpContractService;
use Tfcloud\Services\Dlo\JobLabourService;
use Tfcloud\Services\Dlo\JobService;
use Tfcloud\Services\DocumentsService;
use Tfcloud\Services\Education\CapacityService;
use Tfcloud\Services\Education\SufficiencyService;
use Tfcloud\Services\Education\SuitabilityService;
use Tfcloud\Services\EmailReportService;
use Tfcloud\Services\Estate\AcquisitionService;
use Tfcloud\Services\Estate\AgreementService;
use Tfcloud\Services\Estate\EstBreakDateService;
use Tfcloud\Services\Estate\DisposalService;
use Tfcloud\Services\Estate\EstRecordService;
use Tfcloud\Services\Estate\EstateSalesInvoiceService;
use Tfcloud\Services\Estate\EstChargeService;
use Tfcloud\Services\Estate\EstReviewService;
use Tfcloud\Services\Estate\EstateUnitService;
use Tfcloud\Services\Estate\FeeReceiptService;
use Tfcloud\Services\Estate\FreeholdService;
use Tfcloud\Services\Estate\LeaseInService;
use Tfcloud\Services\Estate\LeaseOutAlienationService;
use Tfcloud\Services\Estate\LeaseOutService;
use Tfcloud\Services\Estate\LeaseOutLetuService;
use Tfcloud\Services\Estate\LettableUnitService;
use Tfcloud\Services\Estate\OccupancyUnitService;
use Tfcloud\Services\Estate\ScServiceCharge\ScActualService;
use Tfcloud\Services\Estate\ScServiceCharge\ScApportionmentService;
use Tfcloud\Services\Estate\ScServiceCharge\ScLetuService;
use Tfcloud\Services\Estate\ScServiceCharge\ScScheduleService;
use Tfcloud\Services\Estate\ScServiceCharge\ScServiceChargePermissionService;
use Tfcloud\Services\Estate\ScServiceCharge\ScServiceChargeService;
use Tfcloud\Services\Estate\ValuationService;
use Tfcloud\Services\FixedAsset\FixedAssetListService;
use Tfcloud\Services\FixedAsset\TransactionService;
use Tfcloud\Services\GroundsMaintenance\GmContractExternalAreaTaskService;
use Tfcloud\Services\GroundsMaintenance\GmContractJobService;
use Tfcloud\Services\Helpcall\HelpcallService;
use Tfcloud\Services\Inspection\InspectionForecastService;
use Tfcloud\Services\Inspection\InspectionService;
use Tfcloud\Services\Instruction\InstructionService;
use Tfcloud\Services\Invoice\InvoiceService;
use Tfcloud\Services\NotesService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\PermitToWork\PermitToWorkService;
use Tfcloud\Services\Personnel\TeamViewService;
use Tfcloud\Services\Project\ProjectActionService;
use Tfcloud\Services\Project\ProjectCertificatedPaymentService;
use Tfcloud\Services\Project\ProjectChangeRequestService;
use Tfcloud\Services\Project\ProjectIssueService;
use Tfcloud\Services\Project\ProjectProgressService;
use Tfcloud\Services\Project\ProjectRiskService;
use Tfcloud\Services\Project\ProjectService;
use Tfcloud\Services\Project\ProjectTaskService;
use Tfcloud\Services\Project\TimesheetService;
use Tfcloud\Services\Property\BlockService;
use Tfcloud\Services\Property\BuildingService;
use Tfcloud\Services\Property\PlantOutOfServiceHistoryService;
use Tfcloud\Services\Property\PlantService;
use Tfcloud\Services\Property\PropExternalService;
use Tfcloud\Services\Property\PropReviewService;
use Tfcloud\Services\Property\RoomCapacityService;
use Tfcloud\Services\Property\RoomService;
use Tfcloud\Services\Property\SiteService;
use Tfcloud\Services\Admin\System\Plant\PlantPartService;
use Tfcloud\Services\PupilPlacePlanning\PlacementService;
use Tfcloud\Services\PupilPlacePlanning\PupilsService;
use Tfcloud\Services\Questionnaire\FiledQstActionService;
use Tfcloud\Services\Questionnaire\FiledQstService;
use Tfcloud\Services\ResourceBooking\BookableResourceService;
use Tfcloud\Services\ResourceBooking\RbResourceBookingService;
use Tfcloud\Services\Stock\StockInventoryService;
use Tfcloud\Services\Stock\StockItemService;
use Tfcloud\Services\Stock\StockReturnService;
use Tfcloud\Services\Stock\StockStoreService;
use Tfcloud\Services\Stock\StockStocktakeService;
use Tfcloud\Services\Stock\Action\StockTransactionService;
use Tfcloud\Services\Tree\TreeSurveyService;
use Tfcloud\Services\Tree\TreeSurveyActionService;
use Tfcloud\Services\UserDefinedService;
use Tfcloud\Services\Utility\UtilityService;
use Tfcloud\services\WaterManagement\PlantTempResultService;
use Tfcloud\Services\IncidentManagement\IncidentService;
use Tfcloud\Lib\Filters\IncidentManagement\IncidentFilter;
use Tfcloud\Services\IncidentManagement\IncidentActionService;
use Tfcloud\Lib\Filters\IncidentManagement\IncidentActionFilter;
use Tfcloud\Services\Property\AgreementService as PropAgreementService;
use Tfcloud\Lib\Filters\Properties\AgreementsFilter as PropAgreementFilter;

class ReportService extends BaseService
{
    public const REP_TYPE_ALL = 0;
    public const REP_TYPE_SYSTEM = 1;
    public const REP_TYPE_USERDEFINED = 2;
    public const REP_TYPE_SCHEDULED = 3;
    public const SELF_SERVICE = 5;


    public const REP_TYPE_FILTER_ALL = 0;
    public const REP_TYPE_FILTER_SYSTEM = 1;
    public const REP_TYPE_FILTER_USERDEFINED_ALL = 2;
    public const REP_TYPE_FILTER_USERDEFINED_STD = 3;
    public const REP_TYPE_FILTER_USERDEFINED_PDF = 4;
    public const REP_TYPE_FILTER_USERDEFINED_QRY = 5;


    public const REP_FAV_OPT_YES = 1;
    public const REP_FAV_OPT_NO = 2;
    public const REP_FAV_OPT_ALL = 3;

    protected $permissionService;
    protected $userGroupService;

    /*
     * Tfcloud\Models\UserReport
     */
    protected $userReport;

    private $estRestrictedModeRpts = ['ES03', 'ES04', 'ES10', 'ES13', 'ES16', 'ES25', 'ES47',
                                      'ES57', 'RE04', 'ES_CLI_WAV01', 'ES_CLI_WAV02'];

    public function __construct(PermissionService $permissionService, UserGroupsService $userGroupService)
    {
        $this->permissionService = $permissionService;
        $this->userGroupService = $userGroupService;
    }

    /**
     * Get the report details
     * @param string $repType report type (system report | user report)
     * @param integer $id the id of report, it can be systemReport Id or userReport Id
     * @return boolean
     * @throws PermissionException
     */
    public function get($repType, $id, $allowDebug = false)
    {
        \Auth::user()->setSourceController(User::SOURCE_REPORT);
        $inputs = \Input::all();
        if (
            !$this->permissionService->hasModuleAccess(
                Module::MODULE_REPORTS,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
            )
        ) {
            $msg = "Not met minimum readonly access for report module";
            throw new PermissionException($msg);
        }

        $res = null;

        if ($repType == Report::USER_REPORT) {
            $res = UserReport::userSiteGroup()->where('user_report_id', $id)->first();
        } elseif ($repType == Report::SYSTEM_REPORT) {
            $res = SystemReport::findOrFail($id);
        }

        if (is_null($res)) {
            \Log::error("No report found");
            throw new InvalidRecordException("No Report found");
        }

        $moduleId = ($repType == Report::USER_REPORT) ? $res->systemReport->module_id : $res->module_id;
        $moduleId2 = ($repType == Report::USER_REPORT) ? $res->systemReport->req_2_module_id : $res->req_2_module_id;

        $canAccess = $this->permissionService->hasModuleAccess(
            $moduleId,
            CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
        );
        if ($moduleId2) {
            $canAccess = $canAccess && $this->permissionService->hasModuleAccess(
                $moduleId2,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
            );
        }
        if (!$canAccess) {
            $msg = "Not met minimum readonly access for report module";
            throw new PermissionException($msg);
        }

        if ($repType == Report::SYSTEM_REPORT && $moduleId == Module::MODULE_ESTATES) {
            if (EstatePermission::moduleAccessMode(EstatePermission::RESTRICTIVE_MODULE_ACCESS)) {
                if (!in_array($res->system_report_code, $this->estRestrictedModeRpts)) {
                    throw new PermissionException('Permission denied in restricted estates mode.');
                }
            }
        }

        $internalOnly = ($repType == Report::USER_REPORT) ? $res->systemReport->internal_only : $res->internal_only;

        if (\Auth::user()->isContractor() && $internalOnly === 'Y') {
            throw new PermissionException('Permission denied for contractor to access this report.');
        }

        if (
            \Auth::user()->isContractor() && (\Auth::user()->siteGroup->asb_inc_contractor_reports) === 'N'
            && $moduleId == Module::MODULE_ASBESTOS
        ) {
            throw new PermissionException('Permission denied for contractor to access this report.');
        }

        $bAllSites = false;
        $siteAccess = \Auth::User()->site_access;
        if ($siteAccess == UserSiteAccess::USER_SITE_ACCESS_ALL) {
            $bAllSites = true;
        }

        $reportDesc = '';
        $reportView = '';
        $repModuleId = null;
        $userReportUseQuery = CommonConstant::DATABASE_VALUE_NO;
        $userReportWpdf = CommonConstant::DATABASE_VALUE_NO;
        $userReportWpdfDefaultDesign = null;
        $userReportWpdfDefaultGroupBy = null;
        $userReportQuery = '';

        if ($res instanceof UserReport) {
            $reportCode         = $res->user_report_code;
            if ($res->user_report_type_id == UserReportType::USER_REPORT_TYPE_QUERY) {
                $userReportUseQuery = CommonConstant::DATABASE_VALUE_YES;
            } else {
                $userReportUseQuery = CommonConstant::DATABASE_VALUE_NO;
            }
            if ($res->user_report_type_id == UserReportType::USER_REPORT_TYPE_PDF) {
                $userReportWpdf = CommonConstant::DATABASE_VALUE_YES;
                $userReportWpdfDefaultDesign = $res->wpdf_default_design;
                $userReportWpdfDefaultGroupBy = $res->wpdf_default_group_by;
            }
            $userReportQuery  = $res->query;
            $reportDesc       = $res->user_report_desc;
            $reportView       = $res->systemReport->view_name;
            $systemReportId   = $res->systemReport->system_report_id;
            $systemReportCode = $res->systemReport->system_report_code;
            $repModuleId      = $res->systemReport->module_id;
            $wpdfTextCanGrow  = $res->wpdf_text_can_grow;
            $available        = $res->available;
        } else {
            $reportCode       = $res->system_report_code;
            $reportDesc       = $res->system_report_desc;
            $reportView       = $res->view_name;
            $systemReportId   = $res->system_report_id;
            $systemReportCode = $reportCode;
            $repModuleId      = $res->module_id;
            $wpdfTextCanGrow  = $res->wpdf_text_can_grow;
            $available        = CommonConstant::DATABASE_VALUE_YES;
        }

        if (\Auth::user()->isSelfServiceUser()) {
            switch ($systemReportCode) {
                case ReportConstant::SYSTEM_REPORT_NOT01:
                case ReportConstant::SYSTEM_REPORT_HLP12:
                case ReportConstant::SYSTEM_REPORT_CM03:
                case ReportConstant::SYSTEM_REPORT_CM2_03:
                case ReportConstant::SYSTEM_REPORT_CM3_03:
                case ReportConstant::SYSTEM_REPORT_CM4_03:
                case ReportConstant::SYSTEM_REPORT_CM5_03:
                    throw new PermissionException(
                        "Report [$systemReportCode] not available to self service user groups."
                    );
                case ReportConstant::SYSTEM_REPORT_HLP10:
                    if (
                        \Auth::User()->isSelfServiceUser() &&
                        !Common::valueYorNtoBoolean(\Auth::user()->siteGroup->hlp_selfserve_show_quotes)
                    ) {
                        throw new PermissionException(
                            "Report [$systemReportCode] not available to self service user groups."
                        );
                    }
                    break;
                default:
                    break;
            }
        }
        if (\Auth::user()->isContractor()) {
            switch ($systemReportCode) {
                case ReportConstant::SYSTEM_REPORT_CM03:
                case ReportConstant::SYSTEM_REPORT_CM2_03:
                case ReportConstant::SYSTEM_REPORT_CM3_03:
                case ReportConstant::SYSTEM_REPORT_CM4_03:
                case ReportConstant::SYSTEM_REPORT_CM5_03:
                    throw new PermissionException(
                        "Report [$systemReportCode] not available to contractor user groups."
                    );
                case ReportConstant::SYSTEM_REPORT_PL03:
                case ReportConstant::SYSTEM_REPORT_PL04:
                case ReportConstant::SYSTEM_REPORT_PL05:
                case ReportConstant::SYSTEM_REPORT_PL06:
                    if (
                        \Auth::user()->siteGroup->plant_restrict_contractor_access ===
                        CommonConstant::DATABASE_VALUE_YES
                    ) {
                        throw new PermissionException("Contractor does not have access.");
                    }
                    break;
                default:
                    break;
            }
        }

        if ($reportView == "vw_adm16") {
            $reportView = "vw_adm16_" . \Auth::User()->site_group_id;
        }

        if (
            !\Auth::user()->siteGroup->turnOnProjectSpecificFields()
            && $systemReportCode ==  ReportConstant::SYSTEM_REPORT_PRJ19
        ) {
            $msg = "Not met minimum access for project specific data report";
            throw new PermissionException($msg);
        }

        if (
            !\Auth::user()->siteGroup->turnOnSiteSpecificFields()
            && $systemReportCode == ReportConstant::SYSTEM_REPORT_PR32
        ) {
            $msg = "Not met minimum access for site specific data report";
            throw new PermissionException($msg);
        }

        if (
            !\Auth::user()->siteGroup->turnOnPropBuildingSpecificFields()
            && $systemReportCode == ReportConstant::SYSTEM_REPORT_PR42
        ) {
            $msg = "Not met minimum access for building specific data report";
            throw new PermissionException($msg);
        }

        if (
            !\Auth::user()->siteGroup->enableEstateRentalPayment() && (
                $systemReportCode ==  ReportConstant::SYSTEM_REPORT_ES53 ||
                $systemReportCode ==  ReportConstant::SYSTEM_REPORT_ES54 ||
                $systemReportCode ==  ReportConstant::SYSTEM_REPORT_ES55 ||
                $systemReportCode ==  ReportConstant::SYSTEM_REPORT_ES56 ||
                $systemReportCode ==  ReportConstant::SYSTEM_REPORT_BUD04 ||
                $systemReportCode ==  ReportConstant::SYSTEM_REPORT_BUD05
            )
        ) {
            $msg = "Not met minimum access for rental payment data report";
            throw new PermissionException($msg);
        }

        if (!$reportCode) {
            throw new InvalidRecordException("No Report found");
        }

        if (!$bAllSites && $userReportUseQuery == CommonConstant::DATABASE_VALUE_YES) {
            throw new PermissionException(
                'Permission denied for user with selected site access to access this report.'
            );
        }

        $this->checkPropertyStrategyPermission($systemReportCode);

        $report = new Report();

        $report->setRepReportId($id);
        $report->setRepCode($reportCode);
        $report->setRepType($repType);
        $report->setRepModuleId($repModuleId);

        $report->setRepDesc($reportDesc);
        $report->setRepTitle("$reportCode: $reportDesc");

        $report->setRepViewName($reportView);
        $report->setRepUserReportUseQuery($userReportUseQuery);
        $report->setRepUserReportWpdf($userReportWpdf);
        $report->setRepUserReportWpdfDefaultDesign($userReportWpdfDefaultDesign);
        $report->setRepUserReportWpdfDefaultGroupBy($userReportWpdfDefaultGroupBy);
        $report->setRepUserReportQuery($userReportQuery);
        $report->setSystemReportId($systemReportId);
        $report->setSystemReportCode($systemReportCode);
        $report->setAvailable($available);

        // wpdf
        $report->setRepWpdfTextCanGrow($wpdfTextCanGrow);
        $report->setRepWpdf2($res->wpdf2);

        $this->setDefaultFilterValues($report);
        $this->reGetExtraReportDetails($report);

        return $report;
    }

    public function getAll($inputs)
    {
        if (
            !$this->permissionService->hasModuleAccess(
                Module::MODULE_REPORTS,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
            )
        ) {
            $msg = "Not met minimum readonly access for report module in class" .
                " (" . __CLASS__ . ") method (" . __METHOD__ . "). " .
                " See line (" . __LINE__ . ")";
            throw new PermissionException($msg);
        }

        $bSystemReports = false;
        $bUserReports = false;
        $iFavouriteReports = 0;
        $this->reGetReportTypes($bSystemReports, $bUserReports, $iFavouriteReports, $inputs);

        $filter = new ReportFilter($inputs);

        // Has the user got View Confidential Message permission set
        $filter->confidential = \Auth::user()->canViewConfidentialMessages();

        $filter->stage_package_task_gantt = \Auth::user()->siteGroup->projEnableStagePackageTaskGantt();
        $filter->finance = \Auth::user()->siteGroup->projEnableFinance();

        if ($bSystemReports) {
            $systemReportQuery = $this->commonReportQuery($this->getSystemReportQuery($filter, $iFavouriteReports));
            $query = $systemReportQuery;
            $this->addReportQuery($query);
        }

        if ($bUserReports) {
            $userReportQuery = $this->commonReportQuery($this->getUserReportQuery($filter, $iFavouriteReports));
            $query = $bSystemReports ? $systemReportQuery->union($userReportQuery)->distinct() : $userReportQuery;
        }

        $orderBy = 'report_code';
        if ($filter->sort) {
            $orderBy = $filter->sort . ' ' . $filter->sortOrder;
        }
        $secondOrderBy = $filter->sort == 'title' ? '' : ', report_desc';

        $query = \DB::table(\DB::raw(
            "(" . $query->toSql() . ") as a" . ' ORDER BY ' . $orderBy . $secondOrderBy
        ))->mergeBindings($query);
        unset($userReportQuery);
        unset($systemReportQuery);
        unset($orderBy);

        return $query->get();
    }

    public function addReportQuery($query)
    {
        if (\Auth::user()->isContractor() || \Auth::user()->isSelfServiceUser()) {
            $query->whereNotIn('system_report.system_report_code', [
                ReportConstant::SYSTEM_REPORT_CM03,
                ReportConstant::SYSTEM_REPORT_CM2_03,
                ReportConstant::SYSTEM_REPORT_CM3_03,
                ReportConstant::SYSTEM_REPORT_CM4_03,
                ReportConstant::SYSTEM_REPORT_CM5_03,
            ]);
        }

        if (!\Auth::user()->strategyAccessible()) {
            $query->where('system_report.system_report_code', '<>', ReportConstant::SYSTEM_REPORT_PR28);
        }
    }

    public function getFilterElement($report, &$filterLookups = [])
    {
        if (!$report instanceof Report) {
            return false;
        }

        $inputs = \Input::all();
        $filterElement = [];

        $userDefinedService = new UserDefinedService();
        if ($report->getRepFilterSite() || $report->getRepFilterSiteContact() || $report->getRepFilterSFRS01()) {
            $udfStatus = [];
            if ($report->getRepFilterSiteContact()) {
                $udfStatus = [
                    'isText' => false,
                    'isCheck' => false,
                    'isDate' => false,
                    'isSection' => false,
                    'isContact' => true
                ];
            }

            if ($report->getRepFilterSFRS01()) {
                $udfStatus = [
                    'isText' => false,
                    'isCheck' => false,
                    'isDate' => false,
                    'isSection' => false,
                    'isContact' => false
                ];
            }

            $extraContactFilterReports = [
                ReportConstant::SYSTEM_REPORT_PR01,
                ReportConstant::SYSTEM_REPORT_PR32,
                ReportConstant::SYSTEM_REPORT_PR42,
                ReportConstant::SYSTEM_REPORT_PR11
            ];
            if (!in_array($report->getSystemReportCode(), $extraContactFilterReports)) {
                $udfStatus['isExtraContact'] = false;
            }

            //Reports are using SiteFilter::setFilterElements:
            //PR01
            //PR13
            //PR-CLI-BRA01
            //PR-CLI-SAYR-01
            //PR26
            //PR-CLI-SFRS01
            //PR11
            $siteService = new SiteService($this->permissionService);
            $site = $siteService->newSite(false);
            $filterLookups = $siteService->filterLookups();
            $filterLookups['lookups'] = $siteService->lookups($site);
            $filterElement = SiteFilter::setFilterElements(
                $filterLookups,
                ['udfStatus' => $udfStatus, 'repMode' => true, 'repCode' => $report->getSystemReportCode()]
            );
        } elseif ($report->getRepAllNotes()) {
            $noteService = new NotesService($this->permissionService);
            $filterLookups = $noteService->filterLookups();
            $filterElement = NoteFilter::setHomeFilterElements($filterLookups);
        /**
         * Cost Plus Module
         */
        } elseif ($report->getRepFilterCostPlusContract()) {
            /*** - Cost Plus Contracts - CPC01/2*/
            $cpContractService = new CpContractService($this->permissionService);
            $filterLookups = $cpContractService->filterLookups();
            $filterElement = CpContractFilter::setFilterElements([], $filterLookups);
        } elseif ($report->getRepFilterCostPlusSalesInvoice()) {
            /*** - Cost Plus Contract Sales Invoices - CPC03*/
            $filterElement = CpSalesInvoiceFilter::setFilterElements();
        } elseif ($report->getRepFilterCostPlusSalesCredit()) {
            /*** - Cost Plus Contract Sales Credit - CPC10*/
            $filterElement = CpSalesCreditFilter::setFilterElements();
        /**
         * Estate Module
         */
        } elseif ($report->getRepFilterEstatesFreehold()) {
            // ES01: Freehold
            // ES14: Freehold Notes
            // ES19: Freehold Property
            $filterRepCode = $report->getRepCode();
            $filterLookups = FreeholdService::lookups();
            $filterElement = FreeholdFilter::setFilterElements($filterLookups);

            //ES33:Account statement report

            /** @todo - Check for the usage of this code and remove if possible */
            switch ($filterRepCode) {
                case ReportConstant::SYSTEM_REPORT_ES33:
                    $filterLookups = (new EstateSalesInvoiceService($this->permissionService))->filterLookups();
                    $filterElement = EstateSalesInvoiceFilter::setFilterElements($filterLookups);
                    break;
                case ReportConstant::SYSTEM_REPORT_ES14:
                    $filterElement = FreeholdFilter::setFilterElements($filterLookups, false);
                    break;
            }
        } elseif ($report->getRepFilterEstatesFreeholdAgreement()) {
            // ES22: Freehold Agreement
            $filterLookupsFreehold = FreeholdService::lookups();
            $filterLookupsAgreement = (new AgreementService($this->permissionService))->filterLookupsAgreement();
            $filterLookups = array_merge_recursive($filterLookupsFreehold, $filterLookupsAgreement);
            $filterElement = FreeholdFilter::setReportES22FilterElements($filterLookups);
        } elseif ($report->getRepFilterEstatesLeaseIn()) {
            // ES02: Leasehold
            $filterLookups = LeaseInService::filterLookups();
            $filterElement = LeaseInFilter::setFilterElements($filterLookups);
        } elseif ($report->getRepFilterEstatesLeaseInAgreement()) {
            // ES21: Lease In Agreement
            $filterLookupsLeaseIn = LeaseInService::filterLookups();
            $filterLookupsAgreement = (new AgreementService($this->permissionService))->filterLookupsAgreement();
            $filterLookups = array_merge_recursive($filterLookupsLeaseIn, $filterLookupsAgreement);
            $filterElement = LeaseInFilter::setReportES21FilterElements($filterLookups);
        } elseif ($report->getRepFilterEstatesLettableUnit()) {
            // ES03: Lettable Unit
            // ES13: Lease Out by Lettable Unit
            $filterLookups = LettableUnitService::lookups();
            $filterElement = LettableUnitFilter::setFilterElements($filterLookups);
        } elseif ($report->getRepFilterEstatesLettableUnitCurrentRent()) {
            // ES70: Lettable Unit Current Rent
            $filterLookups = LettableUnitService::lookups();
            $filterElement = LettableUnitFilter::setFilterElementsCurrentRent($filterLookups);
        } elseif ($report->getRepFilterEstatesOccupancyUnit()) {
            // OCC01: Occupancy Unit
            $filterLookups = LettableUnitService::lookups();
            $filterElement = LettableUnitFilter::setFilterElements($filterLookups);
        } elseif ($report->getRepFilterEstatesDeedPacket()) {
            // ES05: Deed Packet
            $filterElement = DeedPacketFilter::setFilterElements(true);
        } elseif ($report->getRepFilterEstatesDisposal()) {
            // ES06 - Disposal
            $filterLookups = DisposalService::filterLookups();
            $filterElement = DisposalsFilter::setFilterElements($filterLookups);
        } elseif ($report->getRepFilterEstatesReview()) {
            // ES07 - Review
            $filterLookups = (new EstReviewService($this->permissionService))->filterLookups(null);
            $filterElement = EstReviewFilter::setFilterElements($filterLookups);
        } elseif ($report->getRepFilterEstatesCharge()) {
            // ES08 - Titles Charges/Rent
            // ES09 - Lettings Charges/Rent
            $filterLookups = (new EstChargeService($this->permissionService))->filterLookups(null);
            $tenant = $report->getRepFilterEstatesChargeTenant();
            $filterElement = EstChargeFilter::setFilterElements($filterLookups, ['tenant' => $tenant]);
        } elseif ($repCode = $report->getRepFilterEstatesValuation()) {
            // ES12: Estates Valuations
            switch ($repCode) {
                case ReportConstant::SYSTEM_REPORT_ES12:
                    $filterLookups = (new ValuationService($this->permissionService))->filterLookups();
                    $filterElement = ValuationsFilter::setFilterElements(
                        $filterLookups,
                        $report = true,
                        ['repCode' => ReportConstant::SYSTEM_REPORT_ES12]
                    );
                    break;
                case ReportConstant::SYSTEM_REPORT_ES_CLI_SEF01:
                    $filterLookups = (new ValuationService($this->permissionService))->filterLookups();
                    $filterElement = ValuationsFilter::setFilterElements(
                        $filterLookups,
                        $report = true,
                        ['repCode' => ReportConstant::SYSTEM_REPORT_ES_CLI_SEF01]
                    );
                    break;
            }
        } elseif ($report->getRepFilterEstatesLettableUnitProperty()) {
            // ES10: Lettable Unit Property
            $filterLookups = LettableUnitService::lookups();
            $filterElement = LettableUnitPropertyFilter::setFilterElements($filterLookups, true);
        } elseif ($report->getRepFilterEstatesOccupancyUnitProperty()) {
            // ES10: Occupancy Unit Property
            $filterLookups = OccupancyUnitService::lookups();
            $filterElement = OccupancyUnitPropertyFilter::setFilterElements($filterLookups, true);
        } elseif ($report->getRepFilterEstatesDeedPacketProperty()) {
            // ES11: Deed Packet Property
            $filterElement = DeedPacketFilter::setFilterElements(true);
        } elseif ($report->getRepFilterEstatesAcquisition()) {
            // ES18: Acquisitions
            $filterLookups = (new AcquisitionService($this->permissionService))->filterLookups();
            $filterElement = AcquisitionFilter::setFilterElements($filterLookups);
        } elseif ($report->getRepFilterEstatesLettingPacket()) {
            // ES23: Letting Packet Report
            $filterLookups = [];
            $filterElement = LettingPacketFilter::setFilterElements($filterLookups);
        } elseif ($report->getRepFilterEstatesLeaseInAudit()) {
            // ES24: Lease In Audit
            $auditService = new AuditService();
            $filterLookups = (new LeaseInService($this->permissionService))->filterLookups();
            $lookups = array_merge($auditService->filterLookups(), $filterLookups);
            $filterElement = AuditFilter::setLeaseInAuditFilterElements($lookups);
        } elseif ($report->getRepFilterEstatesLeaseOutAudit()) {
            // ES25: Lease Out Audit
            $auditService = new AuditService();
            $filterLeaseOutLookups = (new LeaseOutService($this->permissionService))->filterLookups();
            $lookups = array_merge($auditService->filterLookups(), $filterLeaseOutLookups);
            $filterElement = AuditFilter::setFilterLeaseOutReportElements($lookups);
        } elseif ($report->getRepFilterAud01Audit()) {
            // AUD01:  Audit
            $auditService = new AuditService();
            $filterLookups = $auditService->filterLookups();
            $filterElement = \Tfcloud\Models\AuditFilter::setAUD01ReportFilterElements($filterLookups);
        } elseif ($report->getRepFilterEstatesAlienation()) {
            // ES28: Lease Out Alienations
            $filterLookups = (new LeaseOutAlienationService($this->permissionService))->getFilterLookups();
            $filterLeaseOutLookups = (new LeaseOutService($this->permissionService))->filterLookups();
            $lookups = array_merge($filterLookups, $filterLeaseOutLookups);
            $filterElement = LeaseOutAlienationFilter::setFilterReportElements($lookups);
        } elseif ($report->getRepFilterEstatesLeaseOutAgreement()) {
            // ES20: Lease Out Agreement
            $filterLookups = (new AgreementService($this->permissionService))->filterLookupsAgreement();
            $filterLeaseOutLookups = LeaseOutService::filterLookups();
            $lookups = array_merge($filterLookups, $filterLeaseOutLookups);
            $filterElement = LeaseOutFilter::setReportES20FilterElements($lookups);
        } elseif ($report->getRepFilterEstatesInvoiceGenerationPlan()) {
            // ES29: Invoice Generation Plan
            $filterElement = InvoiceGeneratePlanFilter::setFilterElements();
        } elseif ($report->getRepFilterEstatesRentalPaymentGenerationPlan()) {
            // ES53: Rental Payment Generation Plan
            $filterElement = RentalPaymentGeneratePlanFilter::setFilterElements();
        } elseif ($report->getRepFilterSalesInvoice()) {
            /** Easiest way to alter the specific report filter */
            if ($report->getRepCode() === ReportConstant::SYSTEM_REPORT_ES33) {
                $filterLookups = LeaseOutService::filterLookups();
                $filterElement = EstateSalesInvoiceFilter::setEs33ReportFilterElements($filterLookups);
            } elseif ($report->getRepCode() === ReportConstant::SYSTEM_REPORT_ES34) {
                $filterElement = EstateSalesInvoiceFilter::setES34ReportFilterElements();
            } elseif ($report->getRepCode() === ReportConstant::SYSTEM_REPORT_ES_CLI_HRT01) {
                $filterElement = EstateSalesInvoiceFilter::setHrt01ReportFilterElements();
            } else {
                // ES61: Sales Invoice Lines
                $filterElement = EstateSalesInvoiceFilter::setReportFilterElements();
            }
        } elseif ($report->getRepFilterRentalPayment()) {
            // ES56: Rental Payment
            $filterElement = EstRentalPaymentFilter::setFilterElements();
        } elseif ($report->getRepFilterEstatesDisposalAgreement()) {
            // ES50: Disposal Agreement
            $filterLookups = (new AgreementService($this->permissionService))->filterLookupsAgreement();
            $filterElement = \Tfcloud\Lib\Filters\Estate\AgreementsFilter::setFilterElements($filterLookups);
        } elseif ($report->getRepFilterEstatesTitlesAgreement()) {
            // ES51: Titles Agreement
            $filterLookups = (new AgreementService($this->permissionService))->filterLookupsAgreement();
            $filterElement = \Tfcloud\Lib\Filters\Estate\AgreementsFilter::setFilterElements($filterLookups);
        } elseif ($report->getRepFilterEstatesCliScotb01()) {
            // ES_CLI_SCOTB01: Scottish Borders
            $filterLookups = (new AgreementService($this->permissionService))->filterLookupsAgreement();
            $filterElement = EstateSalesInvoiceFilter::setCliScotB01ReportFilterElements($filterLookups);
        } elseif ($report->getRepFilterGenChecklist()) {
            $filterElement = GenChecklistItemFilter::getElementFilter();
        } elseif ($report->getRepFilterServiceCharge()) {
            $scPermissionService = new ScServiceChargePermissionService();
            $filterLookups = (new ScServiceChargeService($scPermissionService))->filterLookups();
            $filterElement = \Tfcloud\Lib\Filters\Estate\ScServiceChargeFilter::setFilterElements($filterLookups);
        } elseif ($report->getRepFilterServiceChargeActual()) {
            $scPermissionService = new ScServiceChargePermissionService();
            $filterLookups = (new ScActualService($scPermissionService))->filterLookups();
            $filterElement = \Tfcloud\Lib\Filters\Estate\ScActualFilter::setFilterElements($filterLookups);
        } elseif ($report->getRepFilterServiceChargeLetu()) {
            $scPermissionService = new ScServiceChargePermissionService();
            $permissionService = new PermissionService();
            $userDefinedService = new UserDefinedService();
            $leaseOutLetuService = new LeaseOutLetuService($permissionService, $userDefinedService);
            $lettableUnitService = new LettableUnitService($permissionService, $leaseOutLetuService);
            $scServiceChargeService = new ScServiceChargeService($scPermissionService, $userDefinedService);
            $scApportionmentService = new ScApportionmentService($scPermissionService, $scServiceChargeService);

            $scLetuService = new ScLetuService(
                $scPermissionService,
                $scApportionmentService,
                $scServiceChargeService,
                $lettableUnitService
            );

            $filterLookups = $scLetuService->filterLookups();
            $filterElement = \Tfcloud\Lib\Filters\Estate\ScLetuFilter::setFilterElements($filterLookups);
        } elseif ($report->getRepFilterServiceChargeSchedule()) {
            $scPermissionService = new ScServiceChargePermissionService();
            $userDefinedService = new UserDefinedService();
            $scServiceChargeService = new ScServiceChargeService($scPermissionService, $userDefinedService);
            $scApportionmentService = new ScApportionmentService($scPermissionService, $scServiceChargeService);
            $scScheduleService = new ScScheduleService($scPermissionService, $scApportionmentService);

            $filterLookups = $scScheduleService->filterLookups();
            $filterElement = \Tfcloud\Lib\Filters\Estate\ScScheduleFilter::setFilterElements($filterLookups);
        } elseif ($report->getRepFilterLeaseOutBreakDate()) {
            $estBreakDateService = new EstBreakDateService(
                $this->permissionService,
                new LeaseOutService($this->permissionService),
                new LeaseInService($this->permissionService)
            );

            $filterLookups = $estBreakDateService->getFilterLookups();
            $filterElement = EstBreakDateFilter::setFilterOutReportElements($filterLookups);
        } elseif ($report->getRepFilterLeaseInBreakDate()) {
            $estBreakDateService = new EstBreakDateService(
                $this->permissionService,
                new LeaseOutService($this->permissionService),
                new LeaseInService($this->permissionService)
            );

            $filterLookups = $estBreakDateService->getFilterLookups();
            $filterElement = EstBreakDateFilter::setFilterInReportElements($filterLookups);
        } elseif ($report->getRepFilterLettableUnitEnergyRating()) {
            $lettableUnitService = new LettableUnitService(
                $this->permissionService,
                new LeaseOutLetuService($this->permissionService),
            );

            $filterLookups = $lettableUnitService->lookups();
            $filterElement = LettableUnitFilter::setFilterLettableUnitEnergyRatingReportElements($filterLookups);
        } elseif ($report->getRepFilterLettableUnitEnergyRatingWorkRequired()) {
            $lettableUnitService = new LettableUnitService(
                $this->permissionService,
                new LeaseOutLetuService($this->permissionService),
            );

            $filterLookups = $lettableUnitService->lookups();
            $filterElement = LettableUnitFilter::setFilterLettableUnitEnergyRatingReportElements(
                $filterLookups,
                ['total_cost' => true]
            );

        /**
         * Fixed Asset
         */
        } elseif ($report->getRepFilterCapitalAccounting()) {
            // FAR03: Balance Sheet Breakdown
            // FAR04: Current Value vs. Historic Cost Comparision
            // FAR06: Revaluation
            // FAR10: Reversal of Loss
            // FAR12: Re-life
            // FAR13: Derecognition
            // FAR14: Disposal
            // FAR15: Impairment
            // FAR16: Addition
            // FAR17: Acquisition
            $filterLookups = (new FixedAssetListService($this->permissionService))->filterLookups();
            $filterElement = FixedAssetFilter::setFilterReportCapitalAccounting($filterLookups);
        } elseif ($filterRepCode = $report->getRepFilterCapitalAccountingFull()) {
            switch ($filterRepCode) {
                case ReportConstant::SYSTEM_REPORT_FAR07:
                    // FAR07: Asset Register Report
                    $filterLookups = (new FixedAssetListService($this->permissionService))->filterLookups();
                    $filterElement = FixedAssetFilter::setFilterReportCapitalAccountingFull(
                        $filterLookups,
                        ['include_user_define' => true]
                    );
                    break;
                case ReportConstant::SYSTEM_REPORT_FAR34:
                    // FAR34: Fixed Asset List View Report
                    $filterLookups = (new FixedAssetListService($this->permissionService))->filterLookups();
                    $filterElement = FixedAssetFilter::setFilterElements(
                        $filterLookups,
                        [
                            'include_user_define' => true,
                            'include_cv_hc'       => false
                        ]
                    );
                    break;
                case ReportConstant::SYSTEM_REPORT_FAR36:
                    // FAR36: Fixed Asset Fair Value Level Report
                    $filterLookups = (new FixedAssetListService($this->permissionService))->filterLookups();
                    $filterElement = FixedAssetFilter::setFilterElements(
                        $filterLookups,
                        [
                            'include_user_define' => false,
                            'include_cv_hc'       => false
                        ]
                    );
                    break;
                case ReportConstant::SYSTEM_REPORT_FAR22:
                    $filterLookups = (new FixedAssetListService($this->permissionService))->filterLookups();
                    $filterElement = FixedAssetFilter::setFilterElements($filterLookups);
                    break;
                default:
                    break;
            }
        } elseif ($filterRepCode = $report->getRepFilterCapitalAccountingTransaction()) {
            if ($filterRepCode == ReportConstant::SYSTEM_REPORT_FAR35) {
                // FAR35: Fixed Asset Transaction List View Report
                $filterLookups = (new TransactionService($this->permissionService))->filterLookups();
                $filterElement = TransactionFilter::setFilterElements(
                    $filterLookups,
                    [
                        'include_user_define' => false,
                        'include_cv_hc'       => false
                    ]
                );
            }
        } elseif ($filterRepCode = $report->getRepFilterCapitalAccountingAlt1()) {
            // FAR01: Movements On Balances
            // FAR08: Revaluation Reserve Statement
            // FAR31: Movements On Balances (Detailed)
            $filterLookups = (new FixedAssetListService($this->permissionService))->filterLookups();
            $filterElement = FixedAssetFilter::setFilterReportCapitalAccountingAlt1($filterLookups, $filterRepCode);
        } elseif ($report->getRepFilterCapitalAccountingAlt2()) {
            // FAR37: Balance Sheet Movements
            $filterLookups = (new FixedAssetListService($this->permissionService))->filterLookups();
            $filterElement = FixedAssetFilter::setFilterReportCapitalAccountingAlt2($filterLookups);
        } elseif ($filterRepCode = $report->getRepFilterStock()) {
        /**
         * Stock
         */

            switch ($filterRepCode) {
                case ReportConstant::SYSTEM_REPORT_STK01:
                    $filterLookups = (new StockInventoryService($this->permissionService))->filterLookups();
                    $filterElement = StockInventoryFilter::setFilterElementsReportInventory($filterLookups);
                    break;
            }
        } elseif ($filterRepCode = $report->getRepFilterTreeSurvey()) {
        /**
         * Tree Module
         */
            switch ($filterRepCode) {
                case ReportConstant::SYSTEM_REPORT_TREE02:
                case ReportConstant::SYSTEM_REPORT_TREE03:
                    $filterLookups = (new TreeSurveyService($this->permissionService, $userDefinedService))
                        ->filterLookups();
                    $filterElement = TreeSurveyFilter::setFilterElements($filterLookups);
                    break;
            }
        /**
         * Invoice Module
         */
        } elseif ($report->getRepFilterInvoiceSOR()) {
            // ADM18: Invoice SOR
            $filterLookups = (new SorService($this->permissionService))->lookups();
            $filterElement = SorFilter::setFilterElements($filterLookups);
        } elseif ($filterRepCode = $report->getRepFilterInvoices()) {
            switch ($filterRepCode) {
                case ReportConstant::SYSTEM_REPORT_INV01:
                case ReportConstant::SYSTEM_REPORT_INV02:
                case ReportConstant::SYSTEM_REPORT_INV03:
                case ReportConstant::SYSTEM_REPORT_INV04:
                    $filterLookups = (new InvoiceService($this->permissionService, new UserDefinedService()))
                        ->getLookups();
                    $filterElement = InvoiceFilter::setFilterElementsInv02(
                        $filterLookups,
                        [],
                        $filterRepCode
                    );
                    break;
                case ReportConstant::SYSTEM_REPORT_INV_CONT01:
                    $filterElement = InvoiceFilter::setFilterElementsReportInvCont01();
                    break;

                case ReportConstant::SYSTEM_REPORT_INV_CONT02:
                case ReportConstant::SYSTEM_REPORT_INV_CONT03:
                    $filterElement = InvoiceFilter::setFilterElementsReportInvCont02();
                    break;

                case ReportConstant::SYSTEM_REPORT_INV_CONT_CLI_REDB01:
                    $filterElement = InvoiceFilter::setFilterElementsReportInvContCliRedb01();
                    break;

                case ReportConstant::SYSTEM_REPORT_CONT09:
                    $lookups  = (new InvoiceService2($this->permissionService))->lookups();
                    $filterElement = ContractInvoiceFilter::setFilterElements($lookups);
                    break;
                default:
                    break;
            }
        } elseif ($report->getRepFilterDocument()) {
            /**
             * Document report
             */
            $reportCode = $report->getSystemReportCode();
            switch ($reportCode) {
                case ReportConstant::SYSTEM_REPORT_DOC01:
                case ReportConstant::SYSTEM_REPORT_DOC02:
                    $documentService = new DocumentsService($this->permissionService);
                    $filterLookups = $documentService->filterLookups($this->getModule());
                    $filterElement = DocumentFilter::setHomeFilterElements($filterLookups, ['inc-read-only' => false]);
                    break;

                case ReportConstant::SYSTEM_REPORT_INS_CLI_WAK01:
                    $documentService = new DocumentsService($this->permissionService);
                    $filterLookups = $documentService->filterLookups($this->getModule());
                    $filterElement = DocumentFilter::setHomeFilterElements(
                        $filterLookups,
                        ['inc-read-only' => false, 'repCode' => ReportConstant::SYSTEM_REPORT_INS_CLI_WAK01]
                    );
                    break;
                default:
                    break;
            }
        } elseif ($report->getRepFilterDloLabourExceptions()) {
            // DLO01: DLO Job Labour Exceptions
            $filterElement = DloJobLabourFilter::setFilterElementsForDlo01();
        } elseif ($report->getRepFilterDLOJob()) {
            // DLO04: DLO Jobs
            // DLO14: DLO Outstanding Charges
            // DLO19: Risk Assessment Answers
            // DLO21
            // DLO24
            $type = $report->getType();
            $reportCode = $report->getSystemReportCode();
            $filterLookups = (new JobService(
                $this->permissionService,
                $userDefinedService,
                new PlantService($this->permissionService)
            ))->getFilterLookups($type);
            $filterElement = DloJobFilter::setFilterElements($filterLookups, true, [
                'include_user_define' => true,
                'reportCode' => $reportCode
            ]);
        } elseif ($report->getRepFilterDLOJobMK()) {
            // DLO_CLI_MK01: DLO Jobs Milton Keynes
            $type = $report->getType();
            $filterLookups = (new JobService(
                $this->permissionService,
                $userDefinedService,
                new PlantService($this->permissionService)
            ))->getFilterLookups($type);

            $statusList = (new DloSalesInvoiceStatus())->lookupList(['incInactive' => true, 'orders' => [[
                'sortField' => 'dlo_sales_invoice_status_code'
            ]]]);

            $filterLookups['dloSalesInvStatus'] = $statusList;

            $filterElement = DloJobFilter::setFilterElements(
                $filterLookups,
                true,
                ['viewName' => 'vw_int_cli_mk01', 'include_user_define' => true]
            );
        } elseif ($report->getRepFilterDLOMaterialItem()) {
            // DLO05: DLO Material List
            $filterLookups = (new \Tfcloud\Services\Dlo\JobMaterialItemService(
                $this->permissionService
            ))->getFormLookups(new \Tfcloud\Models\DloJob());
            $filterElement = DloJobMaterialItemFilter::setFilterElements($filterLookups);
        } elseif ($report->getRepFilterDLOStockMaterialItem()) {
            // DLO018/DLO20: DLO Stock Material List
            $filterLookups = (new \Tfcloud\Services\Dlo\JobStockItemService(
                $this->permissionService
            ))->getFormLookups(new \Tfcloud\Models\DloJob(), new \Tfcloud\Models\DloStockMaterial());
            $options = [];
            if ($report->getSystemReportCode() == ReportConstant::SYSTEM_REPORT_DLO20) {
                $options['show-from-stock-opt'] = false;
            }
            $filterElement = DloStockMaterialItemFilter::setFilterElements($filterLookups, $options);
        } elseif ($report->getRepFilterDloHoursWorked()) {
            // DLO06: DLO Hours Worked
            $type = $report->getType();
            $filterLookups = (new JobService(
                $this->permissionService,
                $userDefinedService,
                new PlantService($this->permissionService)
            ))->getFilterLookups($type);
            $labourLookups = (new JobLabourService($this->permissionService))->getDLOHoursReportLookups();
            $filterElement = DloJobLabourFilter::setFilterReportElements(array_merge($filterLookups, $labourLookups));
        } elseif ($report->getRepFilterDloOperative()) {
            // DLO07: DLO Operative
            $type = $report->getType();
            $filterLookups = (new \Tfcloud\Services\Dlo\JobOperativeService(
                $this->permissionService,
                new JobService(
                    $this->permissionService,
                    $userDefinedService,
                    new PlantService($this->permissionService)
                )
            ))->getFilterLookups($type);

            $filterElement = DloJobOperativeFilter::setFilterElements($filterLookups, true);
        } elseif ($report->getRepFilterDloLabourHours()) {
            // DLO08: DLO Job Labour Hours
            //$type = $report->getType();
            $filterRepCode = $report->getSystemReportCode();
            switch ($filterRepCode) {
                case ReportConstant::SYSTEM_REPORT_DLO15:
                    $filterLookups = (new JobLabourService($this->permissionService))
                        ->getFormLookups((new DloJobLabour()), 0, 1);
                    $filterElement = DloJobLabourFilter::setFilterDLO15Elements($filterLookups, true);
                    break;
                case ReportConstant::SYSTEM_REPORT_DLO08:
                    $filterLookups = (new JobLabourService($this->permissionService))
                        ->getDLOHoursReportLookups();
                    $filterElement = DloJobLabourFilter::setFilterElements($filterLookups, true);
                    break;
                default:
                    $filterLookups = (new JobLabourService($this->permissionService))
                        ->getFormLookups((new DloJobLabour()), 0, 1);
                    $filterElement = DloJobLabourFilter::setFilterElements($filterLookups, true);
                    break;
            }
        } elseif ($report->getRepFilterDloOperativeTradeLevel()) {
            $userTradeLevelService = new UserTradeLevelService($this->permissionService);
            $filterLookups = $userTradeLevelService->getFilterLookups();
            $filterElement = UserTradeLevelFilter::getUserTradeLevelFilterElements($filterLookups);


        /**
         * Personnel Module
         */
        } elseif ($report->getRepFilterPers03()) {
            $filterLookups = (new TeamViewService($this->permissionService))->getFilterReportLookups();
            $filterElement = TeamViewFilter::setFilterElements($filterLookups);

        /**
         * Budgets.
         */
        } elseif ($report->getRepFilterExpenditureBudgets()) {
            // BUD01: Expenditure Budgets.
            $filterLookups = (new ExpenditureBudgetListService($this->permissionService))->getLookups();
            $filterElement = BudgetFilter::setFilterElements($filterLookups, $inputs);
        } elseif ($report->getRepFilterEstatesBudgets()) {
            // BUD02: Estates Income Budgets.
            $filterLookups = (new IncomingBudgetListService($this->permissionService))->getLookups();
            $filterElement = BudgetFilter::setIncomingFilterElements($filterLookups, $inputs);
        } elseif ($report->getRepFilterSalesInvoiceBudgets()) {
            // BUD03: Estates Income Budgets.
            $filterLookups = (new IncomingBudgetListService($this->permissionService))->getLookups();
            $filterElement = BudgetFilter::setIncomingFilterElements($filterLookups, $inputs);
        } elseif ($report->getRepFilterEstatesExpenditureBudgets()) {
            // BUD04: Expenditure Budgets.
            $filterElement = BudgetFilter::setExpenditureFilterElements();
        } elseif ($report->getRepFilterRentalPaymentBudgets()) {
            // BUD05: Rental Payment Breakdown.
            $filterElement = BudgetFilter::setExpenditureFilterElements();
        } elseif ($report->getRepFilterDloOperativeHours()) {
            // DLO16: DLO Operative Hours
            $filterLookups = (new JobLabourService($this->permissionService))->filterLookups();
            $filterElement = DloJobLabourFilter::setFilterElementsForDlo16($filterLookups, true);
        } elseif ($report->getRepFilterEstatesMultipleRecordType()) {
            // ES48: Estate multiple record types
            $filterLookups = (new EstRecordService($this->permissionService))->lookups();
            $filterElement = EstRecordFilter::setFilterElements();
        } elseif ($report->getRepFilterEstateUnitMultipleRecordType()) {
            // ES59: Estate Unit multiple record types
            $estateUnitService = new EstateUnitService(
                $this->permissionService,
                new LeaseOutLetuService($this->permissionService, new UserDefinedService())
            );
            $filterLookups = $estateUnitService->lookups();
            $filterElement = EstateUnitFilter::setFilterElements($filterLookups);
            //$filterElement = EstRecordFilter::setFilterElements();
        } elseif ($report->getRepFilterFiledQst()) {
            // QSTx01: Filed Questionnaires
            // QSTx04: Filed Questionnaire Details
            $type = $report->getType();
            $filterLookups = (new FiledQstService($this->permissionService))->getFilterLookups($type);
            $filterElement = FiledQuestionnaireFilter::setFilterElements(
                $filterLookups,
                ['repCode' => $report->getRepCode(), 'repMode' => true, 'qstType' => $type]
            );
        } elseif ($report->getRepFilterQst()) {
            // QSTx02: Questionnaire Structure
            // QSTx03: Questionnaires
            $type = $report->getType();
            if ($type) {
                $inputs['separatedType'] = true;
            }
            $filterLookups = (new QuestionnaireService($this->permissionService))->lookups();
            $filterElement = QstFilter::setFilterElements($filterLookups, $inputs);
        } elseif ($report->getRepFilterFiledQstAction()) {
            // QSTx05: Completed Questionnaire Actions
            $filterLookups = (new FiledQstActionService(
                $this->permissionService,
                new FiledQstService($this->permissionService)
            ))->filterLookups($report->getType());

            $showType = false;
            if (!$report->getType()) {
                $showType = true;
            }

            $filterElement =
                FiledQstActionFilter::setFilterElements(
                    $filterLookups,
                    null,
                    ['repCode' => $report->getType(), 'show_qst_type' => $showType]
                );
        } elseif ($report->getRepFilterFiledSCF01()) {
            $filterElement = SuitCFSurveyFilter::setFilterElementsForSCF01();
        /**
         * Project Module
         */
        } elseif ($repCode = $report->getRepFilterProject()) {
            // Project
            switch ($repCode) {
                case ReportConstant::SYSTEM_REPORT_PRJ01:
                case ReportConstant::SYSTEM_REPORT_PRJ17:
                    $filterLookups = (new ProjectService($this->permissionService))->prjLookups();
                    $filterElement = ProjectFilter::setFilterElements($filterLookups, ['include_user_define' => true]);
                    break;

                case ReportConstant::SYSTEM_REPORT_PRJ15:
                    $filterLookups = (new ProjectService($this->permissionService))->prjLookups();
                    $filterElement = ProjectFilter::setFilterElementsForPRJ15($filterLookups);
                    break;
                case ReportConstant::SYSTEM_REPORT_PRJ02:
                    // PRJ02: Project Note
                    $filterLookups = (new NotesService($this->permissionService))->filterLookups();
                    $filterElement = NoteFilter::setFilterElements($filterLookups);
                    break;
                case ReportConstant::SYSTEM_REPORT_PRJ03:
                    // PRJ03: Project Issue
                    $filterLookups = (new ProjectIssueService($this->permissionService))->filterLookups($inputs);
                    $filterElement = ProjectIssueFilter::setFilterElements('all', $filterLookups, [
                        'include_user_define' => true
                    ]);
                    break;
                case ReportConstant::SYSTEM_REPORT_PRJ04:
                    // PRJ04: Project Progress
                    $filterLookups = (new ProjectProgressService($this->permissionService))->filterLookups();
                    $filterElement = ProjectProgressFilter::setFilterElements(false, $filterLookups);
                    break;
                case ReportConstant::SYSTEM_REPORT_PRJ05:
                    // PRJ05: Project Risk
                    $filterLookups = (new ProjectRiskService($this->permissionService))->filterLookups();
                    $filterElement = ProjectRiskFilter::setFilterElements('all', $filterLookups);
                    break;
                case ReportConstant::SYSTEM_REPORT_PRJ06:
                    // PRJ06: Project Action
                    $projectActionService = new ProjectActionService($this->permissionService);
                    $lookups = $projectActionService->lookups();
                    $filterElement = ProjectActionFilter::setFilterElements('all', $lookups);
                    break;
                case ReportConstant::SYSTEM_REPORT_PRJ07:
                    // PRJ07: Project Timesheets
                    $timesheetService = new TimesheetService($this->permissionService);
                    $filterLookups = $timesheetService->lookups(
                        $inputs,
                        ['report_code' => ReportConstant::SYSTEM_REPORT_PRJ07]
                    );
                    $filterElement = TimeSheetFilter::setFilterElements(
                        $filterLookups,
                        ['report_code' => ReportConstant::SYSTEM_REPORT_PRJ07]
                    );
                    break;
                case ReportConstant::SYSTEM_REPORT_PRJ08:
                    // PRJ08: Project Sales Invoices
                    $filterLookups = (new ProjectService($this->permissionService))->prjLookups();
                    $filterElement = ProjectFilter::setFilterElements($filterLookups);
                    break;
                case ReportConstant::SYSTEM_REPORT_PRJ09:
                    // PRJ09: Project Change Request
                    $filterLookups = (new ProjectChangeRequestService($this->permissionService))->filterLookups();
                    $filterElement = ProjectChangeRequestFilter::setFilterElements($filterLookups);
                    break;
                case ReportConstant::SYSTEM_REPORT_PRJ10:
                    // PRJ10: Project Cash Flow
                    $filterLookups = (new ProjectService($this->permissionService))->lookupsForReportPRJ10();
                    $filterElement = ProjectFilter::setReportPRJ10FilterElements($filterLookups);
                    break;
                case ReportConstant::SYSTEM_REPORT_PRJ16:
                    // PRJ16: All Project Cash Flow financial year
                    $filterLookups = (new ProjectService($this->permissionService))->lookups();
                    $filterElement = ProjectFilter::setReportPRJ16FilterElements($filterLookups);
                    break;
                case ReportConstant::SYSTEM_REPORT_PPRJ02:
                    // PRJ11: Project Actions with Notes
                    $projectActionService = new ProjectActionService($this->permissionService);
                    $lookups = $projectActionService->lookups();
                    $filterElement = ProjectActionFilter::setFilterElements(
                        'all',
                        $lookups,
                        ['include_note_filter' => true]
                    );
                    break;
                case ReportConstant::SYSTEM_REPORT_PRJ12:
                    //PRJ12: Project issues with notes
                    $projectIssueService = new ProjectIssueService($this->permissionService);
                    $lookups = $projectIssueService->lookups();
                    $filterElement = ProjectIssueFilter::setFilterElements('all', $lookups);
                    break;
                case ReportConstant::SYSTEM_REPORT_PL02:
                    $filterLookups = (new PlantService($this->permissionService))->filterLookups();
                    $filterElement = PlantFilter::setFilterElements($filterLookups);
                    break;
                case ReportConstant::SYSTEM_REPORT_PRJ14:
                    // PRJ14: Project Tasks
                    $filterLookups = (new ProjectTaskService($this->permissionService))->filterLookups();
                    $filterElement = ProjectTaskFilter::setFilterElements($filterLookups);
                    break;
                case ReportConstant::SYSTEM_REPORT_PRJ18:
                    // PRJ18: Project Bill of Qty
                    $filterElement = ProjectBillOfQtyFilter::setFilterElements();
                    break;
                case ReportConstant::SYSTEM_REPORT_PRJ19:
                    $filterLookups = (new ProjectService($this->permissionService))->prjLookups();
                    $filterElement = ProjectFilter::setFilterElements($filterLookups, ['include_user_define' => true]);
                    break;

                case ReportConstant::SYSTEM_REPORT_PRJ20:
                    $filterLookups = (new ProjectCertificatedPaymentService($this->permissionService))
                        ->filterLookups();
                    $filterElement = ProjectCertificatedPaymentFilter::setFilterElements($filterLookups);
                    break;

                case ReportConstant::SYSTEM_REPORT_PRJ_CLI_SUT01:
                    $filter = new BillingUpToDate();
                    $filter->setTargetDate();
                    $filterElement = $filter->getFields();
                    break;
                default:
                    break;
            }
        /**
         * Instructions Module
         */
        } elseif ($repCode = $report->getRepFilterInstructions()) {
            switch ($repCode) {
                case ReportConstant::SYSTEM_REPORT_ADM32:
                    // ADM32: Account Code
                    $filterLookups = (new AccountCodeService($this->permissionService))->lookups();
                    $filterElement = FinanceAccountCodeFilter::setFilterElements($filterLookups);
                    break;

                case ReportConstant::SYSTEM_REPORT_INT01:
                case ReportConstant::SYSTEM_REPORT_INT02:
                case ReportConstant::SYSTEM_REPORT_INT05:
                case ReportConstant::SYSTEM_REPORT_INT06:
                case ReportConstant::SYSTEM_REPORT_INT08:
                case ReportConstant::SYSTEM_REPORT_INT09:
                case ReportConstant::SYSTEM_REPORT_INT10:
                case ReportConstant::SYSTEM_REPORT_INT_CLI_OSS01:
                case ReportConstant::SYSTEM_REPORT_INT12:
                case ReportConstant::SYSTEM_REPORT_INT13:
                case ReportConstant::SYSTEM_REPORT_INT14:
                case ReportConstant::SYSTEM_REPORT_INT15:
                case ReportConstant::SYSTEM_REPORT_INT16:
                case ReportConstant::SYSTEM_REPORT_INT17:
                case ReportConstant::SYSTEM_REPORT_INT18:
                case ReportConstant::SYSTEM_REPORT_INT19:
                case ReportConstant::SYSTEM_REPORT_INT20:
                case ReportConstant::SYSTEM_REPORT_INT21:
                case ReportConstant::SYSTEM_REPORT_INT25:
                    // INT01, INT02, INT04, INT05, INT06, INT10, INT12, INT13
                    $filterLookups = (new InstructionService($this->permissionService, $userDefinedService))
                            ->getLookups();
                    $filterElement = InstructionFilter::getFilterFields($filterLookups, false, null, $repCode);
                    break;

                case ReportConstant::SYSTEM_REPORT_INT04:
                case ReportConstant::SYSTEM_REPORT_INT11:
                    //need to limit parent type in hepcall and all. As comment of Pippa in CLD-3015
                    $filterLookups = (new InstructionService($this->permissionService, $userDefinedService))
                            ->getLookups();
                    $filterLookups['parentType'] = [];
                    if ($this->permissionService->moduleLicensed(ModuleCode::HELP_CALL)) {
                        $filterLookups['parentType'] = [['id' => InstructionParentType::HELPCALL,
                            'name' => InstructionParentType::find(InstructionParentType::HELPCALL)
                                        ->instruction_parent_type_name,
                            'desc' => '']];
                    }

                    $filterElement = InstructionFilter::getFilterFields($filterLookups, false, null, $repCode);
                    break;

                case ReportConstant::SYSTEM_REPORT_INT_CLI_MK01:
                    //need to limit parent type in hepcall and all. As comment of Pippa in CLD-3015
                    $filterLookups = (new InstructionService($this->permissionService, $userDefinedService))
                            ->getLookups();
                    $filterLookups['parentType'] = [];
                    if ($this->permissionService->moduleLicensed(ModuleCode::HELP_CALL)) {
                        $filterLookups['parentType'] = [['id' => InstructionParentType::HELPCALL,
                            'name' => InstructionParentType::find(InstructionParentType::HELPCALL)
                                        ->instruction_parent_type_name,
                            'desc' => '']];
                    }

                    $statusList = (new InvoiceStatus())->lookupList(['incInactive' => true, 'orders' => [[
                        'sortField' => 'invoice_status_code'
                    ]]]);

                    $filterLookups['invStatus'] = $statusList;

                    $filterElement = InstructionFilter::getFilterFields($filterLookups, false, null, $repCode);
                    break;

                case ReportConstant::SYSTEM_REPORT_INT07:
                    $lookups = (new ExpenditureBudgetListService($this->permissionService))->getLookups();
                    $filter = new BudgetMasterFilter();
                    $filter->setFinancialYearBudget($inputs, $lookups['finYear']);
                    $filter->setAccountCodeBudget(FinAccountType::ALL);
                    $filter->setStatus();
                    $filterElement = $filter->getFields();
                    break;

                case ReportConstant::SYSTEM_REPORT_INT_CLI_PP01:
                    $filter = new CompletionDate();
                    $filter->setTargetDate();
                    $filterElement = $filter->getFields();
                    break;

                case ReportConstant::SYSTEM_REPORT_INT_CLI_NEL01:
                    $filterLookups = (new InstructionService($this->permissionService, $userDefinedService))
                            ->getLookups();
                    $filter = new InstructionNEL01Filter();
                    $filterElement = $filter->getFilterFields($filterLookups);
                    break;

                case ReportConstant::SYSTEM_REPORT_INT_CLI_NBS01:
                    $filterLookups = (new InstructionService($this->permissionService, $userDefinedService))
                            ->getLookups();
                    $filter = new InstructionNBS01Filter();
                    $filterElement = $filter->getFilterFields($filterLookups);
                    break;

                default:
                    break;
            }

        /**
         * Interfaces Module
         */
        } elseif ($report->getRepFilterInterfaces()) {
            if ($report->getRepFilterInterfaces() == ReportConstant::SYSTEM_REPORT_INF01) {
                // INF01: Interface Execution Log Report
                $filterLookups = (new InterfaceLogService($this->permissionService))->getFilterLookups();
                $filterElement = InterfaceLogFilter::setFilterElements($filterLookups);
            }

        /**
         * Condition Module
         */
        } elseif ($report->getRepFilterCondition()) {
            // CND01: Condition Survey
            // CND02: Condition Survey Report
            // CND04: Identified Work by Condition Survey
            // CND07: Identified Work with Specific Remedies

            //CND08: Identified work for PDF format
            $filterRepCode = $report->getSystemReportCode();
            switch ($filterRepCode) {
                case ReportConstant::SYSTEM_REPORT_CND08:
                    $filterLookups = (new IdworkService($this->permissionService))->filterLookups();
                    $filterElement = IdworkFilter::setFilterElements($filterLookups);
                    break;
                case ReportConstant::SYSTEM_REPORT_CND01:
                case ReportConstant::SYSTEM_REPORT_CND02:
                case ReportConstant::SYSTEM_REPORT_CND04:
                case ReportConstant::SYSTEM_REPORT_CND05:
                case ReportConstant::SYSTEM_REPORT_CND07:
                    $filterLookups = (new ConditionService($this->permissionService))->filterLookups();
                    break;
                case ReportConstant::SYSTEM_REPORT_CND09:
                    $filterElement = ConditionFilter::setCND09FilterElements();
                    break;
                case ReportConstant::SYSTEM_REPORT_CND10:
                case ReportConstant::SYSTEM_REPORT_CND13:
                    $filterElement = ConditionFilter::setCnd10FilterElements();
                    break;
                default:
                    break;
            }
        } elseif ($report->getRepFilterConditionIdentifiedWork()) {
            // CND03: Identified Work
            $filterLookups = (new IdworkService($this->permissionService))->filterLookups();
            $options['includeZoneFilter'] = true;
            $options['repMode'] = true;
            $options['repCode'] = ReportConstant::SYSTEM_REPORT_CND03;
            $options['userDefines'] = true;
            $filterElement = IdworkFilter::setFilterElements($filterLookups, true, $options);
        } elseif ($report->getRepFilterConditionItemsBySurvey()) {
            // CND06: Condition Items by Condition Survey
            $filterLookups = (new IdworkService($this->permissionService))->filterLookups();
            $filterElement = IdworkFilter::setCND06FilterElements($filterLookups);
        } elseif ($report->getRepFilterIdworkTotalCostByProperty()) {
            // CND11: Identified Work Total Cost by Property
            $filterLookups = (new ConditionService($this->permissionService))->filterLookups();
            $filterElement = IdworkFilter::setCND11FilterElements($filterLookups);
        /**
         * Resource Booking Module
         */
        } elseif ($report->getRepFilterResourceBooking()) {
            // RB01: Resource Bookings
            $filterLookups = (new RbResourceBookingService(
                $this->permissionService,
                $userDefinedService
            ))->filterLookups();
            $filterElement = RbResourceBookingFilter::setFilterElements($filterLookups, $inputs);
        } elseif ($report->getRepFilterCateringBooking()) {
            // RB02: Catering Bookings
            $filterLookups = ['cateringItems' => (new RbCateringItem())->lookupList()];
            $userDefinedService = new UserDefinedService();
            $lookup = (new RbResourceBookingService($this->permissionService, $userDefinedService))->filterLookups();
            $filterElement = RbCateringBookingFilter::setFilterElements(array_merge($filterLookups, $lookup));
        } elseif ($report->getRepFilterBookableResource()) {
            // RB03: Bookable Resources
            $filterLookups = (new BookableResourceService($this->permissionService))->lookups();
            $filterElement = RbBookableResourceFilter::setFilterElementsRB03($filterLookups);
        } elseif ($report->getRepFilterCateringItem()) {
            // RB04: Resource Booking Catering Item
            $filterElement = CateringItemFilter::setFilterElements();
        } elseif ($report->getRepFilterBookableResourceAvailability()) {
            // RB05: Bookable Resource Availability
            $filterLookups = (new BookableResourceService($this->permissionService))->lookups();
            $filterElement = RbBookableResourceFilter::setFilterElements($filterLookups);


        /**
         * Utility Module
         */
        } elseif ($report->getRepFilterUtilityRecord()) {
            //UT04: Utility Record
            $filterLookups = (new UtilityService($this->permissionService, $userDefinedService))->lookups();
            $filterElement = UtilityFilter::setReportFilterElements($filterLookups);
        } elseif ($report->getRepFilterUtilityBill()) {
            //UT05: Utility Bill
            $filterLookups = (new UtilityService($this->permissionService, $userDefinedService))->lookups();
            $filterElement = UtilityBillFilter::setFilterElements($filterLookups);
        } elseif ($report->getRepFilterSufficiency()) {
            /*
            * Sufficiency
            */
            $filterRepCode = $report->getRepCode();
            if ($filterRepCode == ReportConstant::SYSTEM_REPORT_SUFF04) {
                $lookups = SufficiencyService::lookups();
                $filterElement = SufficiencyFilter::setSUFF04FilterElements($lookups);
            } else {
                $lookups = SufficiencyService::lookups();
                $filterElement = SufficiencyFilter::setFilterElements($lookups);
            }
        /*
         * Suitability
         */
        } elseif ($report->getRepFilterSuitability()) {
            $lookups = SuitabilityService::lookups();
            $filterElement = SuitabilityFilter::setFilterElements($lookups);

        /*
         * Plant & TREE01
         */
        } elseif ($report->getRepFilterPlantGrp()) {
            $filterRepCode = $report->getRepCode();

            $lookupOptions = [];
            $filterOptions = [];
            switch ($filterRepCode) {
                case ReportConstant::SYSTEM_REPORT_TREE01:
                    $showTreeFilter = false;
                    break;
                case ReportConstant::SYSTEM_REPORT_INS_CLI_RBWM01:
                    $showTreeFilter = true;
                    $lookupOptions['include-site-type'] = true;
                    $filterOptions['show-site-type'] = true;
                    $filterOptions['show-insp-status-type'] = true;
                    $filterOptions['show-insp-due_date'] = true;
                    $filterOptions['show-insp-completed_date'] = true;
                    break;
                default:
                    $showTreeFilter = true;
                    break;
            }

            $filterLookups = (new PlantService($this->permissionService))->filterLookups(null, $lookupOptions);
            $filterElement = PlantFilter::setFilterElements(
                $filterLookups,
                [],
                array_merge(
                    ['report_code' => $filterRepCode, 'show-tree-filter' => $showTreeFilter, 'repMode' => true],
                    $filterOptions
                )
            );
        /*
         * Contract Module
         */
        } elseif ($report->getRepFilterPl03()) {
            $filterLookups = (new PlantOutOfServiceHistoryService($this->permissionService))
                ->filterPl03Lookups();
            $filterElement = PlantOutOfServiceHistoryFilter::setFilterElementsPl03($filterLookups);
        } elseif ($report->getRepFilterContract()) {
            //CONT01
            $filterLookups = (new ContractService($this->permissionService))->getFilterLookups();
            $filterElement = ContractFilter::setFilterElements($inputs, false, $filterLookups);
        } elseif ($report->getRepFilterContractInstructions()) {
            //CONT02
            $filterLookups = (new InstructionService2($this->permissionService))->getFilterLookups();
            $filterElement = ContractInstructionFilter::setFilterElements($filterLookups);
        } elseif ($report->getRepFilterContractInspectionReport()) {
            //CONT_CLI_BRA01
            $filterLookups = (new InstructionService2($this->permissionService))->getFilterLookups();
            $filterElement = ContractInspectionReportFilter::setFilterElements($filterLookups);
        } elseif ($report->getRepFilterContractInvoice()) {
            //CONT03
            $filterLookups = (new InvoiceService2($this->permissionService))->lookups();
            $filterElement = ContractInvoiceFilter::setFilterElements($filterLookups);
        } elseif ($report->getRepFilterContractInvoiceInspection()) {
            //CONT06
            $filterLookups = (new InvoiceService2($this->permissionService))->lookups();
            $filterElement = ContractInvoiceFilter::setFilterElements(
                $filterLookups,
                [
                    #'cert-code-visible' => false,
                    #'type-editable' => false,
                    #'type-value' => \Tfcloud\Models\ContractInvoiceType::PLANNED_INSPECTION
                ]
            );
        } elseif ($report->getRepFilterContractCredit()) {
            // CONT04
            $filterLookups = (new CreditService($this->permissionService, new UserDefinedService()))->lookups();
            $filterElement = ContractCreditFilter::setFilterElements($filterLookups);
        } elseif ($report->getRepFilterContractCreditInspection()) {
            //CONT05
            $filterLookups = (new CreditService($this->permissionService, new UserDefinedService()))->lookups();
            $filterElement = ContractCreditFilter::setFilterElements(
                $filterLookups,
                [
                    'afp-code-visible' => true,
                    #'type-editable' => false,
                    #'type-value' => \Tfcloud\Models\ContractCreditType::STANDARD
                ]
            );
        } elseif ($report->getRepFilterContractInvoiceCredit()) {
            // CONT07 and CONT19
            $filterLookups = (new InvoiceCreditService($this->permissionService))->lookups();
            $filterElement = ContractInvoiceCreditFilter::setFilterElements(
                $filterLookups,
                [
                    'insp-code-visible' => true,
                    'loc-visible'       => true,
                    'acc-code-visible'  => true,
                ]
            );
        } elseif ($report->getRepFilterContractInvoiceCreditInspection()) {
            //CONT08
            $filterLookups = (new InvoiceCreditService($this->permissionService))->lookups();
            $filterElement = ContractInvoiceCreditFilter::setFilterElements($filterLookups);
        } elseif ($report->getRepFilterContractInspection()) {
            //CONT11
            $filterLookups = array_merge(
                (new InstructionService2($this->permissionService))->getFilterLookups(),
                InspectionService::filterLookups($inputs)
            );
            $filterElement = ContractInspectionFilter::setFilterElements($filterLookups);
        } elseif ($report->getRepFilterContractAFP()) {
            //CONT12, CONT18
            $filterLookups = array_merge(
                (new InstructionService2($this->permissionService))->getFilterLookups()
            );
            $filterElement = AFPFilter::setFilterElements(
                $filterLookups,
                ['show-certificate-code' => true, 'repCode' => $report->getRepCode()]
            );
        } elseif ($report->getRepFilterContractCertificate()) {
            //CONT13
            $filterLookups = array_merge(
                (new InstructionService2($this->permissionService))->getFilterLookups()
            );
            $filterElement = CertificateFilter::setFilterElements($filterLookups);
        } elseif ($report->getRepFilterContractCreditApplication()) {
            //CONT14
            $filterLookups = (new CreditService($this->permissionService, new UserDefinedService()))->lookups();
            $filterElement = ContractCreditFilter::setFilterElements(
                $filterLookups,
                [
                    'insp-code-visible' => false,
                    'type-editable' => false,
                    'type-value' => \Tfcloud\Models\ContractCreditType::CERTIFICATE
                ]
            );
        } elseif ($report->getRepFilterContractInvoiceApplication()) {
            //CONT15
            $filterLookups = (new InvoiceService2($this->permissionService))->lookups();
            $filterElement = ContractInvoiceFilter::setFilterElements(
                $filterLookups,
                [
                    'cert-code-visible' => false,
                    'type-editable' => false,
                    'type-value' => \Tfcloud\Models\ContractInvoiceType::CERTIFICATE
                ]
            );
        } elseif ($report->getRepFilterContractInvoiceCreditApplication()) {
            //CONT16
            $filterLookups = (new InvoiceCreditService($this->permissionService))->lookups();
            $filterElement = ContractInvoiceCreditFilter::setFilterElements(
                $filterLookups,
                [
                    'afp-code-visible' => true,
                    'loc-visible' => true,
                    'acc-code-visible' => true,
                ]
            );
        } elseif ($repCode = $report->getRepFilterInspection()) {
            // Inspection
            switch ($repCode) {
                case ReportConstant::SYSTEM_REPORT_INS06:
                    $filterLookups = InspectionService::filterLookups($inputs, true);
                    $filterElement = InspectionFilter::setInspectionReportINS06FilterElements($filterLookups);
                    break;
                case ReportConstant::SYSTEM_REPORT_INS08:
                    $filterLookups = InspectionService::filterLookups($inputs, true);
                    InspectionForecastService::formatInputData($inputs);
                    $filterElement  = InspectionFilter::setInspectionRagFilterElements(
                        $filterLookups,
                        ['month_commencing' => true]
                    );
                    break;
                case ReportConstant::SYSTEM_REPORT_INS_CLI_LANC02:
                    $filterLookups = InspectionService::filterLookups($inputs, true);
                    InspectionForecastService::formatInputData($inputs);
                    $filterElement  = InspectionFilter::setFilterElements(
                        $filterLookups,
                        [
                            'month_commencing' => true,
                            'repCode'          => ReportConstant::SYSTEM_REPORT_INS_CLI_LANC02
                        ]
                    );
                    break;
                case ReportConstant::SYSTEM_REPORT_INT_CLI_NBS02:
                    $filterLookups = (new InspectionService($this->permissionService))
                            ->filterLookups($inputs);
                    $filter = new InspectionNBS02Filter();
                    $filterElement = $filter->getFilterFields($filterLookups);
                    break;
                default:
                    $options = [];
                    if (
                        in_array($repCode, [
                        ReportConstant::SYSTEM_REPORT_INS01,
                        ReportConstant::SYSTEM_REPORT_INS03,
                        ReportConstant::SYSTEM_REPORT_INS04,
                        ReportConstant::SYSTEM_REPORT_INS05,
                        ReportConstant::SYSTEM_REPORT_INS10,
                        ReportConstant::SYSTEM_REPORT_INS_CLI_LAN
                        ])
                    ) {
                        $options = ['include_user_define' => true];
                    }
                    $options['repMode'] = true;
                    $options['repCode'] = $repCode;
                    $filterLookups = InspectionService::filterLookups($inputs);
                    $filterElement = InspectionFilter::setFilterElements($filterLookups, $options);
                    break;
            }
        } elseif ($repCode = $report->getRepFilterPtw()) {
            // Permit to work
            switch ($repCode) {
                case ReportConstant::SYSTEM_REPORT_PTW01:
                case ReportConstant::SYSTEM_REPORT_PTW02:
                    $filterLookups = PermitToWorkService::filterLookups($inputs);
                    $filterElement = PermitToWorkFilter::setFilterElements($filterLookups);
                    break;
                default:
                    break;
            }
        } elseif ($repCode = $report->getRepFilterGroundsMaintenance()) {
            /**
             * Grounds Maintenance
             */
            switch ($repCode) {
                case ReportConstant::SYSTEM_REPORT_GM01:
                    $filterElement = GMContractFilter::setFilterElements(['active' => 'all']);
                    break;
                case ReportConstant::SYSTEM_REPORT_GM02:
                    $filterLookups = GmContractJobService::filterReportLookups();
                    $filterElement = GmContractJobFilter::setReportFilterElements($filterLookups);
                    break;
                case ReportConstant::SYSTEM_REPORT_GM03:
                    $filterLookups = GmContractExternalAreaTaskService::filterReportLookups();
                    $filterElement = GmContractExternalAreaTaskFilter::setReportFilterElements($filterLookups);
                    break;
                default:
                    break;
            }
        } elseif ($repCode = $report->getRepFilterDatafeed()) {
            /**
             * Datafeed
             */
            if ($repCode == ReportConstant::SYSTEM_REPORT_DF01) {
                $filterElement = DatafeedResultsFilter::setReportFilterElements();
            }
        } elseif ($repCode = $report->getRepFilterCaseManagement()) {
            /**
             * Case Management
             */
            switch ($repCode) {
                case ReportConstant::SYSTEM_REPORT_CM01:
                case ReportConstant::SYTEM_REPORT_CM_CLI_SAYR_01:
                    $filterLookups = CaseRecordService::filterReportLookups();
                    $filterElement = CaseRecordFilter::setFilterElementsCM01($filterLookups);
                    break;
                case ReportConstant::SYSTEM_REPORT_CM02:
                    $caseFilterLookups = CaseRecordService::filterReportLookups();
                    $stageFilterLookups = CaseManagementStageService::filterReportLookups();
                    $filterLookups = array_merge($caseFilterLookups, $stageFilterLookups);
                    $filterElement = CaseManagementStageFilter::setFilterElements($filterLookups);
                    break;
                case ReportConstant::SYSTEM_REPORT_CM03:
                    $filterLookups = CaseRecordService::filterReportLookups();
                    $filterElement = CaseRecordFilter::setFilterElementsCM03($filterLookups);
                    break;
                default:
                    break;
            }
        /**
         * Helpcall RCBC01
         */
        } elseif ($repCode = $report->getRepFilterHlpRcbc01()) {
            if ($repCode == ReportConstant::SYSTEM_REPORT_HLP_CLI_RCBC01) {
                $helpcallService = new HelpcallService($this->permissionService);
                $filterLookups = $helpcallService->filterLookups([]);
                $filterElement = HelpcallFilter::setFilterElementsForCLIRCBC01($filterLookups);
            }
        /**
         * Property module
         */
        } elseif ($report->getRepFilterBuild()) {
            $repCode = $report->getSystemReportCode();
            //PR02: Building report
            //PR12: Building Contact
            //PR18: Building Summary
            $buildService = new BuildingService($this->permissionService);
            $filterLookups = $buildService->filterLookups();
            $filterElement = BuildingFilter::setFilterElements(
                $filterLookups,
                ['repMode' => true, 'repCode' => $repCode]
            );
        } elseif ($report->getRepFilterRoom() || $report->getRepFilterEducationRoom()) {
            //PR03: Room report
            $roomService = new RoomService();
            $filterLookups = $roomService->filterLookups();
            $options = $report->getRepFilterEducationRoom() ?
                ['reportCode' => ReportConstant::SYSTEM_REPORT_PR20] : ['reportCode' => $report->getSystemReportCode()];
            $filterElement = RoomFilter::setReportFilterElements($filterLookups, $options);
        } elseif ($report->getRepFilterBuildingBlock()) {
            //PR16: Building Block Report
            $blockService = new BlockService($this->permissionService);
            $filterLookups  = $blockService->filterLookups();
            $filterElement = BlockFilter::setFilterElements($filterLookups, ['PR16' => true]);
        } elseif ($report->getRepFilterPropReview()) {
            $propReviewService = new PropReviewService($this->permissionService);
            $filterLookups = $propReviewService->filterLookups();
            $filterElement = PropReviewFilter::setFilterElements($filterLookups);
        } elseif ($report->getRepFilterPropAgreements()) {
            $propReviewService = new PropAgreementService($this->permissionService);
            $filterLookups = $propReviewService->filterLookupsAgreement();
            $filterElement = PropAgreementFilter::setFilterElements($filterLookups);
        } elseif ($report->getRepFilterLoc()) {
            $repCode = $report->getSystemReportCode();
            switch ($repCode) {
                case ReportConstant::SYSTEM_REPORT_PR08:
                case ReportConstant::SYSTEM_REPORT_PR09:
                    $filterData = ['maxLevel' => 2];
                    $filterElement = RunningCostFilter::setFilterElements($filterData);
                    break;
                case ReportConstant::SYSTEM_REPORT_PR10:
                    $filterData = ['maxLevel' => 2];
                    $filterElement = CommonFilter::setLocationFilterElements($filterData);
                    break;
                case ReportConstant::SYSTEM_REPORT_PR25:
                case ReportConstant::SYSTEM_REPORT_PR33:
                    $filterData = ['maxLevel' => 1];
                    $filterElement = RunningCostFilter::setFilterElements($filterData);
                    break;
                case ReportConstant::SYSTEM_REPORT_PR34:
                    $filterData = ['maxLevel' => 1];
                    $filterElement = CommonFilter::setLocationFilterElements($filterData);
                    break;
                default:
                    break;
            }
        } elseif ($report->getRepFilterFiledPR21()) {
            //PR21:
            $siteService = new SiteService($this->permissionService);
            $site = $siteService->newSite(false);
            $filterLookups = $siteService->filterLookups();
            $filterLookups['lookups'] = $siteService->lookups($site);

            $filterElement = PR21ComplianceFilter::setFilterElements($filterLookups);
        } elseif ($report->getRepFilterFiledRB07()) {
            //RB07:
            $today = Carbon::today();
            \Input::merge(
                [
                    'targetFrom' => $today->format('d/m/Y'),
                    'targetTo' => $today->addDay(7)->format('d/m/Y'),
                    'days' => ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
                ]
            );
            $filterLookups = (new BookableResourceService($this->permissionService))->filterLookups();
            $filterElement = RbBookableResourceSearchFilter::setRB07FilterElements($filterLookups);
        } elseif ($report->getRepFilterCodeDescActive()) {
            //for some case report with Code,Description,Active
            $filterElement = CommonFilter::setCodeDescActiveFilterElements();
        } elseif ($report->getRepFilterExternalArea()) {
            //PR19: External Areas
            $filterLookups = PropExternalService::filterLookups();
            $filterElement = PropExternalFilter::setFilterElements($filterLookups);

        // General
        } elseif ($report->getRepFilterContact()) {
            $filterLookups = (new ContactService($this->permissionService))->lookups();
            $filterElement = ContactFilter::setFilterElements($filterLookups, ['userDefines' => true]);

        // Capacity
        } elseif ($report->getRepFilterCapacity()) {
            $filterLookups = (new CapacityService($this->permissionService))->filerLookups();
            $filterElement = CapacityFilter::setFilterElements($filterLookups);

        // CAP03
        } elseif ($report->getRepFilterRoomCapacity()) {
            $roomService = new RoomService();
            $roomCapacityService = new RoomCapacityService($this->permissionService, $roomService);
            $filterLookups = $roomCapacityService->getFilterLookup();
            $filterElement = RoomCapacityFilter::setReportFilterElements($filterLookups);
        } elseif ($repCode = $report->getRepFilterWkhtmltopdf()) {
        /**
         * wkthtmltopdf Report
         */
            switch ($repCode) {
                case ReportConstant::SYSTEM_REPORT_QST_CLI_SUN02:
                case ReportConstant::SYSTEM_REPORT_QST_CLI_SUN03:
                case ReportConstant::SYSTEM_REPORT_QST_CLI_SUN04:
                    $filterLookups = (new FiledQstService($this->permissionService))
                        ->getFilterLookups(QstType::GENERAL);
                    $filterElement = FiledQuestionnaireFilter::setFilterElements(
                        $filterLookups,
                        [
                            'maxLocationLevel'  => 1,
                            'showCurrent'       => false,
                            'addAllOpt'         => false,
                            'defaultNull'       => false,
                            'showQuestionnaire' => false
                        ]
                    );
                    break;
                case ReportConstant::SYSTEM_REPORT_CND_CLI_SUN01:
                    $filterLookups = (new ConditionService($this->permissionService))->filterLookups();
                    $filterElement = ConditionFilter::setFilterElements($filterLookups);
                    break;
                case ReportConstant::SYSTEM_REPORT_CND_CLI_TORB02:
                    $filterLookups = (new ConditionService($this->permissionService))->filterLookups();
                    $filterElement = ConditionFilter::setCndCliTorb02FilterElements($filterLookups);
                    break;

                case ReportConstant::SYSTEM_REPORT_CND_CLI_MK01:
                    //add filter here
                    $filterLookups = (new ConditionFilter());
                    $filterElement = ConditionFilter::setCndCliFif01FilterElements(true);
                    break;


                case ReportConstant::SYSTEM_REPORT_CND_CLI_NEL01:
                    $filterLookups = (new ConditionFilter());
                    $filterElement = ConditionFilter::setCndCliNel01FilterElements();
                    break;
                case ReportConstant::SYSTEM_REPORT_CND09:
                    //add filter here
                    $filterLookups = (new ConditionFilter());
                    $filterElement = ConditionFilter::setNoValCND09FilterElements();
                    break;
                case ReportConstant::SYSTEM_REPORT_CND10:
                case ReportConstant::SYSTEM_REPORT_CND13:
                    //add filter here
                    $filterLookups = (new ConditionFilter());
                    $filterElement = ConditionFilter::setCnd10FilterElements();
                    break;
                case ReportConstant::SYSTEM_REPORT_ES33:
                    $filterLookups = (new EstateSalesInvoiceFilter($this->permissionService))->lookups();
                    $filterElement = EstateSalesInvoiceFilter::setFilterElements($filterLookups);
                    break;
                case ReportConstant::SYSTEM_REPORT_ES34:
                    $filterLookups = (new FeeReceiptService($this->permissionService))->lookups();
                    break;
                case ReportConstant::SYSTEM_REPORT_ES35:
                    $filterLookups = (new EstChargeService($this->permissionService))->lookups();
                    $filterElement = EstChargeFilter::setFilterElements($filterLookups);
                    break;
                case ReportConstant::SYSTEM_REPORT_CND08:
                    $filterLookups = (new IdworkService($this->permissionService))->lookups();
                    $filterElement = IdworkFilter::setFilterElements($filterLookups);
                    break;
                case ReportConstant::SYSTEM_REPORT_DLO15:
                    $filterLookups = (new JobLabourService($this->permissionService))
                        ->getFormLookups((new DloJobLabour()), 0, 1);
                    $filterElement = DloJobLabourFilter::setFilterElements($filterLookups, true);
                    break;
                case ReportConstant::SYSTEM_REPORT_PL02:
                    $filterLookups = (new PlantService($this->permissionService))->lookups();
                    $filterElement = PlantFilter::setFilterElements('all', $filterLookups);
                    break;
                case ReportConstant::SYSTEM_REPORT_PRJ11:
                    $filterLookups = (new ProjectService($this->permissionService))->prjLookups();
                    $filterElement = ProjectFilter::setFilterElements($filterLookups);
                    break;
                case ReportConstant::SYSTEM_REPORT_PRJ12:
                    $filterLookups = (new ProjectIssueService($this->permissionService))->filterLookups($inputs);
                    $filterElement = ProjectIssueFilter::setFilterElements(
                        'all',
                        $filterLookups,
                        ['include_note_filter' => true]
                    );
                    break;

                case ReportConstant::SYSTEM_REPORT_CPC04:
                case ReportConstant::SYSTEM_REPORT_CPC05:
                    $options = ['showContractSelector' => true];
                    $filterElement = CpSalesInvoiceFilter::setCPC04ReportFilterElements($options);
                    break;
                case ReportConstant::SYSTEM_REPORT_PRJ13:
                    $options = ['showProjectSelector' => true];
                    $filterElement = ProjectFilter::setPrintReportFilterElements($options);
                    break;
                case ReportConstant::SYSTEM_REPORT_CND_CLI_WOK01:
                    $filterLookups = (new ConditionService($this->permissionService))->filterLookups();
                    $filterElement = ConditionFilter::setCndCliWok01FilterElements($filterLookups);
                    break;
                case ReportConstant::SYSTEM_REPORT_CND15:
                case ReportConstant::SYSTEM_REPORT_CND16:
                case ReportConstant::SYSTEM_REPORT_CND17:
                case ReportConstant::SYSTEM_REPORT_CND18:
                case ReportConstant::SYSTEM_REPORT_CND19:
                case ReportConstant::SYSTEM_REPORT_CND20:
                case ReportConstant::SYSTEM_REPORT_CND21:
                case ReportConstant::SYSTEM_REPORT_CND22:
                case ReportConstant::SYSTEM_REPORT_CND23:
                    $filterLookups = (new IdworkService($this->permissionService))->lookupIdWorksFMR();
                    $filterElement = IdworkFilter::setIWFMRFilterElements($filterLookups);
                    break;
                case ReportConstant::SYSTEM_REPORT_PERS01:
                    $filterLookups = (new TeamViewService($this->permissionService))->getFilterReportLookups([
                        TeamViewService::INC_NON_WORKING_TYPE_OPTION => true
                    ]);
                    $filterElement = TeamViewFilter::setPers01ReportFilterElements($filterLookups);
                    break;
                case ReportConstant::SYSTEM_REPORT_PERS02:
                    $filterLookups = (new TeamViewService($this->permissionService))->getFilterReportLookups([
                        TeamViewService::INC_NON_WORKING_TYPE_OPTION => true
                    ]);
                    $filterElement = TeamViewFilter::setPers02ReportFilterElements($filterLookups);
                    break;
            }
        } elseif ($report->getRepFilterExcel()) {
            $filterElement = null;
        } elseif ($report->getRepFilterEmail()) {
            $repCode = $report->getSystemReportCode();
            $filterElement = $this->getFilterElementForEmail($repCode);
        } elseif ($report->getRepFilterIncidentRecords()) {
            $filterLookups = (new IncidentService($this->permissionService))->filterLookups($inputs);
            $filterElement = IncidentFilter::setFilterElements($filterLookups);
        } elseif ($report->getRepFilterIncidentAction()) {
            $filterLookups = (new IncidentActionService($this->permissionService))->filterLookups();
            $filterElement = IncidentActionFilter::setFilterElements($filterLookups);
        } elseif ($report->getRepFilterTreeSurveyAction()) {
            $filterLookups = (new TreeSurveyActionService($this->permissionService))->filterLookups();
            $filterElement = TreeSurveyActionFilter::setFilterElements($filterLookups);
        }

        return $filterElement;
    }

    private function getFilterElementForEmail($repCode)
    {
        $filterElement = null;
        switch ($repCode) {
            case ReportConstant::SYSTEM_REPORT_ADM36:
                $filterLookups = (new EmailReportService($this->permissionService))->filterLookups();
                $filterElement = EmailFailureFilter::setFilterElementsEmailFailure($filterLookups);
                break;
            case ReportConstant::SYSTEM_REPORT_ADM38:
                $filterLookups = (new EmailReportService($this->permissionService))->filterLookups();
                $filterElement = EmailFailureFilter::setFilterElementsEmail($filterLookups);
                break;
            default:
                break;
        }

        return $filterElement;
    }

    public function getSecondFilterElement($report)
    {
        if (!$report instanceof Report) {
            return false;
        }

        return [];
    }
    /*
     * Get data for the filter of report, e.g: lookup, checkbox values,...
     */
    public function getFilterData($report)
    {
        if (!$report instanceof Report) {
            return false;
        }

        $filterData = [];

        if ($report->getRepFilterSite() || $report->getRepFilterSFRS01()) {
            $siteService = new SiteService($this->permissionService);
            $site = $siteService->newSite(false);
            $filterLookups = $siteService->filterLookups();
            $filterLookups['lookups'] = $siteService->lookups($site);
            $filterData['filterLookups'] = $filterLookups;
        } elseif ($report->getRepFilterDocument()) {
            $documentService = new DocumentsService($this->permissionService);
            $filterLookups = $documentService->filterLookups();
            $filterData['filterLookups'] = $filterLookups;
        } elseif ($report->getRepFilterDLOPoWo()) {
            /*** - DLO09 */
            $userDefinedService = new UserDefinedService();
            $instructionService = new InstructionService($this->permissionService, $userDefinedService);
            $filterLookups = $instructionService->getLookups();
            $filterData['filterLookups'] = $filterLookups;
        } elseif ($report->getRepFilterDLOSalesInvoice()) {
            /*** - DLO10/DLO11 */
            $dloSalesInvoiceService = new \Tfcloud\Services\Dlo\DloSalesInvoiceService($this->permissionService);
            $filterLookups = $dloSalesInvoiceService->getFilterLookups();
            $filterData['filterLookups'] = $filterLookups;
        } elseif ($report->getRepFilterPlantPartAdm()) {
            /*** - ADM39 - Plant Part*/
            $plantPartService = new PlantPartService($this->permissionService);
            $filterLookups = $plantPartService->getFormLookups();
            $filterData['lookups'] = $filterLookups;
        } elseif ($report->getRepFilterHelpcallBrief()) {
            $helpcallService = new HelpcallService($this->permissionService);
            $helpcall = $helpcallService->newHelpcall();
            $filterLookups = $helpcallService->filterLookups($helpcall);
            $filterData['filterLookups'] = $filterLookups;

            $userDefinedService = new UserDefinedService();
            $filterData['userDefine'] = $userDefinedService->get(GenTable::HELPCALL);
        } elseif (
            $report->getRepFilterCondition() ||
            $report->getRepFilterConditionItemsBySurvey() ||
            $report->getRepFilterConditionIdentifiedWork()
        ) {
            $idworkService = new IdworkService($this->permissionService);
            $filterLookups = (new ConditionService($this->permissionService))->filterLookups();
            $idworkLookups = $idworkService->filterLookups();
            $filterData['conditionLookups'] = $idworkLookups;
            $filterData['filterLookups'] = $filterLookups;
            $userDefinedService = new UserDefinedService();
            $filterData['userDefine'] = $userDefinedService->get(GenTable::CONDITON);
        } elseif ($report->getRepFilterUser()) {
            $usergroup = Usergroup::userSiteGroup()
                ->get(['usergroup_id', 'usergroup_code', 'usergroup_desc']);

            $usergroupType = UsergroupType::get(['usergroup_type_id', 'usergroup_type_code']);

            $filterData['repFilterUse'] = ['usergroup' => $usergroup, 'usergroupType' => $usergroupType];
        } elseif ($report->getRepFilterUserLogin()) {
            $usergroup = Usergroup::userSiteGroup()
                ->get(['usergroup_id', 'usergroup_code', 'usergroup_desc']);

            $usergroupType = UsergroupType::get(['usergroup_type_id', 'usergroup_type_code']);

            $filterData['repFilterUseL'] = ['usergroup' => $usergroup, 'usergroupType' => $usergroupType];
        } elseif ($report->getRepFilterFiledQst()) {
            $filterData['current'] = CommonConstant::DATABASE_VALUE_ONE;
        } elseif ($report->getRepFilterQst()) {
            $filterData['active'] = CommonConstant::DATABASE_VALUE_ONE;
        } elseif ($reportSystemCode = $report->getRepFilterProject()) {
            // CLD-3815: set the current year as default
            if ($reportSystemCode == ReportConstant::SYSTEM_REPORT_PRJ10) {
                $filterData['finYear'] = FinYear::current() ?
                    FinYear::current()->fin_year_id : null;
            }
        }

        return $filterData;
    }

    public function wpdfLookups($report)
    {
        $pdfDesign = UserReportPdfDesign::where('user_report_id', $report->getRepReportId())
            ->orderBy('pdf_design_name', 'ASC')
            ->get();
        $pdfGroupBy = ReportField::where('system_report_id', $report->getSystemReportId())
            ->get(['report_field_id', 'report_field_code']);

        return [
            'pdfDesign' => $pdfDesign,
            'pdfGroupBy' => $pdfGroupBy
        ];
    }

    public function filterLookups()
    {
        $modules = $this->userGroupService->getModulesByUserGroupId(\Auth::user()->usergroup_id);

        $filterLookups['types'] = [
            ['type_id' => self::REP_TYPE_FILTER_SYSTEM, 'type_desc' => 'System'],
            ['type_id' => self::REP_TYPE_FILTER_USERDEFINED_ALL, 'type_desc' => 'User Defined - All'],
            ['type_id' => self::REP_TYPE_FILTER_USERDEFINED_STD, 'type_desc' => 'User Defined - Standard'],
            ['type_id' => self::REP_TYPE_FILTER_USERDEFINED_PDF, 'type_desc' => 'User Defined - PDF']
        ];

        if ((\Auth::user()->site_access === UserSiteAccess::USER_SITE_ACCESS_ALL)) {
            array_push(
                $filterLookups['types'],
                ['type_id' => self::REP_TYPE_FILTER_USERDEFINED_QRY, 'type_desc' => 'User Defined - Query']
            );
        }

        $modulesSidebar = [
            ["module_id" => Module::MODULE_ASBESTOS,
                "module_sidebar" => "Asbestos", "module_description" => "Asbestos"],
            ["module_id" => Module::MODULE_CAD,
                "module_sidebar" => "CAD", "module_description" => "CAD"],
            ["module_id" => Module::MODULE_CAPACITY,
                "module_sidebar" => "Capacity", "module_description" => "Capacity"],
            [
                "module_id" => Module::MODULE_CASE_MANAGEMENT,
                "module_sidebar" => Module::getModuleDescription(Module::MODULE_CASE_MANAGEMENT),
                "module_description" => Module::getModuleDescription(Module::MODULE_CASE_MANAGEMENT)
            ],
            [
                "module_id" => Module::MODULE_CASE_MANAGEMENT_2,
                "module_sidebar" => Module::getModuleDescription(Module::MODULE_CASE_MANAGEMENT_2),
                "module_description" => Module::getModuleDescription(Module::MODULE_CASE_MANAGEMENT_2)
            ],
            [
                "module_id" => Module::MODULE_CASE_MANAGEMENT_3,
                "module_sidebar" => Module::getModuleDescription(Module::MODULE_CASE_MANAGEMENT_3),
                "module_description" => Module::getModuleDescription(Module::MODULE_CASE_MANAGEMENT_3)
            ],
            [
                "module_id" => Module::MODULE_CASE_MANAGEMENT_4,
                "module_sidebar" => Module::getModuleDescription(Module::MODULE_CASE_MANAGEMENT_4),
                "module_description" => Module::getModuleDescription(Module::MODULE_CASE_MANAGEMENT_4)
            ],
            [
                "module_id" => Module::MODULE_CASE_MANAGEMENT_5,
                "module_sidebar" => Module::getModuleDescription(Module::MODULE_CASE_MANAGEMENT_5),
                "module_description" => Module::getModuleDescription(Module::MODULE_CASE_MANAGEMENT_5)
            ],
            ["module_id" => Module::MODULE_CONDITION,
                "module_sidebar" => "Condition", "module_description" => "Condition"],
            ["module_id" => Module::MODULE_CLEANING,
                "module_sidebar" => "Cleaning", "module_description" => "Cleaning"],
            ["module_id" => Module::MODULE_CONTRACT,
                "module_sidebar" => "Contract", "module_description" => "Contract"],
            ["module_id" => Module::MODULE_COST_PLUS,
                "module_sidebar" => "Cost Plus", "module_description" => "Cost Plus"],
            ["module_id" => Module::MODULE_DATA_FEEDS,
                "module_sidebar" => "Data Feeds", "module_description" => "Data Feeds"],
            ["module_id" => Module::MODULE_DLO, "module_sidebar" => "DLO", "module_description" => "DLO"],
            ["module_id" => Module::MODULE_CAPITAL_ACCOUNTING,
                "module_sidebar" => "Fixed Assets", "module_description" => "Fixed Asset Register"],
            ["module_id" => Module::MODULE_GENERAL, "module_sidebar" => "General", "module_description" => "General"],
            ["module_id" => Module::MODULE_ESTATES, "module_sidebar" => "Estates", "module_description" => "Estates"],
            ["module_id" => Module::MODULE_GROUNDS_MAINTENANCE,
                "module_sidebar" => "Grounds Maintenance",
                "module_description" => "Grounds Maintenance"],
            ["module_id" => Module::MODULE_HELP,
                "module_sidebar" => "Help Calls", "module_description" => "Help Calls"],
            ["module_id" => Module::MODULE_INCIDENT_MANAGEMENT,
                "module_sidebar" => "Incident Management", "module_description" => "Incident Management"],
            ["module_id" => Module::MODULE_INSPECTION,
                "module_sidebar" => "Inspection", "module_description" => "Inspection"],
            ["module_id" => Module::MODULE_INSTRUCTIONS,
                "module_sidebar" => "Instructions", "module_description" => "Instructions"],
            ["module_id" => Module::MODULE_INVOICING,
                "module_sidebar" => "Invoices", "module_description" => "Invoicing"],
            ["module_id" => Module::MODULE_INTERFACE,
                "module_sidebar" => "Interface", "module_description" => "Interface"],
            ["module_id" => Module::MODULE_NHS_CLEANING,
                "module_sidebar" => "NHS Cleaning", "module_description" => "NHS Cleaning"],
            ["module_id" => Module::MODULE_OCCUPANCY,
                "module_sidebar" => "Occupancy", "module_description" => "Occupancy"],
            ["module_id" => Module::MODULE_PERMIT_TO_WORK,
                "module_sidebar" => "Permit To Work", "module_description" => "Permit To Work"],
            ["module_id" => Module::MODULE_PERSONNEL,
                "module_sidebar" => "Personnel", "module_description" => "Personnel"],
            ["module_id" => Module::MODULE_PLANT, "module_sidebar" => "Plant", "module_description" => "Plant"],
            ["module_id" => Module::MODULE_PROJECT,
                "module_sidebar" => "Project", "module_description" => "Project Management"],
            ["module_id" => Module::MODULE_PROPERTY,
                "module_sidebar" => "Property", "module_description" => "Property"],
            ["module_id" => Module::MODULE_PPP,
                "module_sidebar" => "Pupil Place Planning", "module_description" => "Pupil Place Planning"],
            ["module_id" => Module::MODULE_QST,
                "module_sidebar" => "Questionnaire", "module_description" => "Questionnaire"],
            ["module_id" => Module::MODULE_QST_CORE_FACTS,
                "module_sidebar" => "Questionnaire Core Facts", "module_description" => "Questionnaire Core Facts"],
            ["module_id" => Module::MODULE_QST_COVID_19,
                "module_sidebar" => "Questionnaire Covid-19", "module_description" => "Questionnaire Covid-19"],
            ["module_id" => Module::MODULE_QST_DAA,
                "module_sidebar" => "Questionnaire DAA", "module_description" => "Questionnaire DAA"],
            ["module_id" => Module::MODULE_QST_FIRE,
                "module_sidebar" => "Questionnaire Fire", "module_description" => "Questionnaire Fire"],
            ["module_id" => Module::MODULE_QST_GENERAL,
                "module_sidebar" => "Questionnaire General", "module_description" => "Questionnaire General"],
            ["module_id" => Module::MODULE_QST_LEGIONELLA,
                "module_sidebar" => "Questionnaire Legionella", "module_description" => "Questionnaire Legionella"],
            ["module_id" => Module::MODULE_RESOURCE_BOOKING,
                "module_sidebar" => "Resource Booking", "module_description" => "Resource Booking"],
            ["module_id" => Module::MODULE_RISK_ASSESSMENT,
                "module_sidebar" => "Risk Assessment", "module_description" => "Risk Assessment"],
            ["module_id" => Module::MODULE_RURAL_ESTATES,
                "module_sidebar" => "Rural Estate", "module_description" => "Rural Estate"],
            ["module_id" => Module::MODULE_SUFFICIENCY,
                "module_sidebar" => "Sufficiency", "module_description" => "Sufficiency"],
            ["module_id" => Module::MODULE_SUITABILITY,
                "module_sidebar" => "Suitability", "module_description" => "Suitability"],
            ["module_id" => Module::MODULE_STOCK,
                "module_sidebar" => "Stock", "module_description" => "Stock"],
            ["module_id" => Module::MODULE_TPY,
                "module_sidebar" => "Transparency", "module_description" => "Transparency"],
            ["module_id" => Module::MODULE_TREE,
                "module_sidebar" => "Tree", "module_description" => "Tree"],
            ["module_id" => Module::MODULE_UTILITY,
                "module_sidebar" => "Utility", "module_description" => "Utility"],
            ["module_id" => Module::MODULE_WATER_MANAGEMENT,
                "module_sidebar" => "Water Management", "module_description" => "Water Management"],
        ];
        $modulesAccess = [];
        if (count($modules) > 0) {
            foreach ($modules as $module) {
                $modulesAccess[] = $module->module_id;
            }
            foreach ($modulesSidebar as $key => $moduleSidebar) {
                $readonly = $this->permissionService->hasModuleAccess(
                    Module::MODULE_REPORTS,
                    CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
                ) && $this->permissionService->hasModuleAccess(
                    $moduleSidebar["module_id"],
                    CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
                );
                if (!in_array($moduleSidebar["module_id"], $modulesAccess) || !$readonly) {
                    unset($modulesSidebar[$key]);
                }
                // #404835: Self Service users with read on reports, shouldn't see general reports.
                if (\Auth::user()->isSelfServiceUser() && $moduleSidebar["module_id"] == Module::MODULE_GENERAL) {
                    unset($modulesSidebar[$key]);
                }
            }
        }
        uasort($modulesSidebar, function ($a, $b) {
            return strcasecmp($a['module_description'], $b['module_description']);
        });
        $filterLookups['modules'] = $modulesSidebar;
        return $filterLookups;
    }

    public function intIsInterfaceEnabled($interfaceId)
    {
        $interfacePermission = InterfacePermission::userSiteGroup()->select('interface_permission_id')
            ->where('interface_id', $interfaceId)->get()->toArray();

            return !empty($interfacePermission);
    }

    public function sendEmail($data, $repFile = false)
    {
        $repId      = array_get($data, 'repId');
        $repType    = array_get($data, 'repType');
        $repCode    = array_get($data, 'repCode');
        $repTitle   = array_get($data, 'repTitle');
        $repFormat  = array_get($data, 'repFormat');
        $repFileName = array_get($data, 'repFileName');
        $repMessage = array_get($data, 'repMessage');
        $emailReply = array_get($data, 'emailReply');
        $bcc        = array_get($data, 'bcc');
        $disableAudit = array_get($data, 'disableAudit', false);

        $sErrMsg = '';

        // Need to remove the extension as we will add it further down.
        if (substr($repFileName, -4) == '.pdf') {
            $repFileName = substr($repFileName, 0, strlen($repFileName) - 4);
        }

        if (!$repCode || !$repTitle) {
            $sErrMsg = 'Cannot get the report data';
            return $sErrMsg;
        }
        if (!$repFile) {
            $reportDir = storage_path() . '/reports/' . \Auth::User()->site_group_id . '/' . \Auth::User()->id;
            if (substr($repFileName, -4) == '.pdf') {
                $repFile = $reportDir . '/' . $repFileName;
            } else {
                $repFile = $reportDir . '/' . $repCode;
            }

            switch ($repFormat) {
                case ReportConstant::REPORT_OUTPUT_PDF_FORMAT:
                case ReportConstant::REPORT_OUTPUT_XLS_FORMAT:
                case ReportConstant::REPORT_OUTPUT_XLSX_FORMAT:
                case ReportConstant::REPORT_OUTPUT_HTML_FORMAT:
                    $repFormat = '.' . $repFormat;
                    break;
                default:
                    $repFormat = '.' . ReportConstant::REPORT_OUTPUT_CSV_FORMAT;
                    break;
            }

            $repFile .= $repFormat;
        }

        if (realpath($repFile)) {
            $user = \Auth::User();
            $emailFromAddress = $user->siteGroup->email_from;
            $displayName = $emailReply ? '' : $user->display_name;
            $emailReplyTo = $emailReply ?: $user->email_reply_to;
            $emailBcc = $bcc;

            if ($emailFromAddress == '') {
                $sErrMsg = "'Default From' has not been defined in Email Settings.";
                $sErrMsg .= " Please contact your system administrator.";
                return $sErrMsg;
            }

            if ($emailReplyTo == '') {
                $sErrMsg = "'Email Reply To' has not been defined in your user details.";
                $sErrMsg .= " Please edit your user details to set your email address.";
                return $sErrMsg;
            }

            $subject = "Report $repCode";

            if (array_key_exists('repSubject', $data)) {
                $emailSubject = array_get($data, 'repSubject');
                if (! is_null($emailSubject)) {
                    $subject = $emailSubject;
                }
            }

            $emailData = [
                'subject'       => $subject,
                'from'          => $emailFromAddress,
                'to'            => $emailReplyTo,
                'toDisplayName' => $displayName,
                'attachment'    => $repFile,
                'bcc'           => $emailBcc
            ];

            $email = new Email($emailData);
            $replyData = [
                'repTitle'   => $repTitle,
                'repMessage' => $repMessage,
            ];
            $parent = $this->getReportModel($repType, $repId);

            SendReportEmail::dispatch($email, \Auth::User(), $replyData, ['mergeTo' => true], $parent);

            $repInPdfFormat = self::getPdfReportList();
            if (in_array($repCode, $repInPdfFormat)) {
                $repPdfFile = $repFile . '.pdf';
                if (realpath($repPdfFile)) {
                    $email->attachment = $repPdfFile;
                    SendReportEmail::dispatch($email, \Auth::User(), $replyData, ['mergeTo' => true], $parent);
                }
            }

            $sErrMsg = "The email of $repTitle has been queued for sending to {$email->to}.";

            if (!$disableAudit) {
                // add entry to audit log
                $report = $this->get($repType, $repId);
                $auditService = new AuditService();
                $auditService->auditText = sprintf(
                    'Email queued from %s to %s, subject %s',
                    $email->from,
                    $email->to,
                    $email->subject
                );
                $auditService->addAuditEntry($report, AuditAction::ACT_EMAIL);
            }
        } else {
            $sErrMsg = "Cannot send email of the report $repTitle.";
        }

        return $sErrMsg;
    }

    public function existFav($reportType, $id)
    {
        $count = ReportFavourite::where('user_id', \Auth::user()->getKey());

        if ($reportType == Report::USER_REPORT) {
            $count = $count->where('user_report_id', $id)->count();
        } else {
            $count = $count->where('system_report_id', $id)->count();
        }

        return $count > 0;
    }

    public function addFavouriteReport($report, $reportType, $id)
    {
        \DB::transaction(function () use ($report, $reportType, $id) {
            $auditText = "";
            $favReport          = new ReportFavourite();
            $favReport->user_id = \Auth::user()->getKey();
            if ($reportType == Report::USER_REPORT) {
                $favReport->user_report_id = $id;
                $auditText = "User Defined Report " . $report->getAuditCode() . " added to the favourites list.";
            } else {
                $favReport->system_report_id = $id;
                $auditText = "System Report " . $report->getAuditCode() . " added to the favourites list.";
            }
            $favReport->save();

            $audit = new AuditService();
            $audit->auditText = $auditText;
            $audit->addAuditEntry($favReport, AuditAction::ACT_INSERT, $report);
        });
    }

    public function enableDisableReport($id, $enable)
    {
        \DB::transaction(function () use ($id, $enable) {
            $auditText = "";
            $report = new SystemReport();
            $sysReport = $report::findOrFail($id);
            $auditReport = $this->get(Report::SYSTEM_REPORT, $id);
            if ($enable) {
                $sysReport->superuser_only = CommonConstant::DATABASE_VALUE_NO;
                $auditText = "Report " . $report->getAuditCode() . " enabled for use.";
            } else {
                $sysReport->superuser_only = CommonConstant::DATABASE_VALUE_YES;
                $auditText = "Report " . $report->getAuditCode() . " removed from general use.";
            }

            $sysReport->save();

            $audit = new AuditService();
            $audit->auditText = $auditText;
            $audit->addAuditEntry($auditReport, AuditAction::ACT_REPORT);
        });
    }

    public function removeFavouriteReport($report, $reportType, $id)
    {
        \DB::transaction(function () use ($report, $reportType, $id) {
            $auditText = "";
            $favReport          = ReportFavourite::where('user_id', \Auth::user()->getKey());

            if ($reportType == Report::USER_REPORT) {
                $favReport->where('user_report_id', $id);
                $auditText = "User Defined Report " . $report->getAuditCode() . " removed from the favourites list.";
            } else {
                $favReport->where('system_report_id', $id);
                $auditText = "System Report " . $report->getAuditCode() . " removed from the favourites list.";
            }
            $favReport = $favReport->first();

            $audit = new AuditService();
            $audit->auditText = $auditText;
            $audit->addAuditEntry($favReport, AuditAction::ACT_DELETE, $report);

            $favReport->delete();
        });
    }

    public static function getPdfReportList()
    {
        return [
            ReportConstant::REPORT_COND_SURVEY,
            ReportConstant::REPORT_ASB_SURVEY
        ];
    }

    public static function getWkHtmlToPdfWhiteList()
    {
        return [
            ReportConstant::SYSTEM_REPORT_ASB_CLI_BRA01,
            ReportConstant::SYSTEM_REPORT_ASB_CLI_CEC01,
            ReportConstant::SYSTEM_REPORT_ASB_CLI_FIF01,
            ReportConstant::SYSTEM_REPORT_ASB_CLI_GWN01,
            ReportConstant::SYSTEM_REPORT_ASB_CLI_SEF01,
            ReportConstant::SYSTEM_REPORT_ASB_CLI_SEF02,
            ReportConstant::SYSTEM_REPORT_ASB_CLI_HRT01,
            ReportConstant::SYSTEM_REPORT_ASB_CLI_NEL01,
            ReportConstant::SYSTEM_REPORT_ASB_CLI_CB01,

            ReportConstant::SYSTEM_REPORT_CND08,
            ReportConstant::SYSTEM_REPORT_CND09,
            ReportConstant::SYSTEM_REPORT_CND10,
            ReportConstant::SYSTEM_REPORT_CND13,

            ReportConstant::SYSTEM_REPORT_CND_CLI_SB01,
            ReportConstant::SYSTEM_REPORT_CND_CLI_MK01,
            ReportConstant::SYSTEM_REPORT_CND_CLI_FIF01,
            ReportConstant::SYSTEM_REPORT_CND_CLI_NEL01,
            ReportConstant::SYSTEM_REPORT_CND_CLI_SUN01,
            ReportConstant::SYSTEM_REPORT_CND_CLI_WOK01,
            ReportConstant::SYSTEM_REPORT_CND_CLI_TORB02,
            ReportConstant::SYSTEM_REPORT_CPC04,
            ReportConstant::SYSTEM_REPORT_CPC05,
            ReportConstant::SYSTEM_REPORT_ES33,
            ReportConstant::SYSTEM_REPORT_ES34,
            ReportConstant::SYSTEM_REPORT_ES_CLI_HRT01,
            ReportConstant::SYSTEM_REPORT_ES35,
            ReportConstant::SYSTEM_REPORT_ES36,
            ReportConstant::SYSTEM_REPORT_ES37,
            ReportConstant::SYSTEM_REPORT_ES39,
            ReportConstant::SYSTEM_REPORT_ES40,
            ReportConstant::SYSTEM_REPORT_ES67,
            ReportConstant::SYSTEM_REPORT_RE03,
            ReportConstant::SYSTEM_REPORT_PL02,
            ReportConstant::SYSTEM_REPORT_PPRJ02,
            ReportConstant::SYSTEM_REPORT_PRJ11,
            ReportConstant::SYSTEM_REPORT_PRJ12,
            ReportConstant::SYSTEM_REPORT_PRJ13,
            ReportConstant::SYSTEM_REPORT_PR_CLI_SAYR_01,
            ReportConstant::SYSTEM_REPORT_DLO15,
            ReportConstant::SYSTEM_REPORT_QST_CLI_SUN02,
            ReportConstant::SYSTEM_REPORT_QST_CLI_SUN03,
            ReportConstant::SYSTEM_REPORT_QST_CLI_SUN04,

            ReportConstant::SYSTEM_REPORT_CND15,
            ReportConstant::SYSTEM_REPORT_CND16,
            ReportConstant::SYSTEM_REPORT_CND17,
            ReportConstant::SYSTEM_REPORT_CND18,
            ReportConstant::SYSTEM_REPORT_CND19,
            ReportConstant::SYSTEM_REPORT_CND20,
            ReportConstant::SYSTEM_REPORT_CND21,
            ReportConstant::SYSTEM_REPORT_CND22,
            ReportConstant::SYSTEM_REPORT_CND23,

            ReportConstant::SYSTEM_REPORT_PTW02,

            ReportConstant::SYSTEM_REPORT_CM03,
            ReportConstant::SYSTEM_REPORT_CM2_03,
            ReportConstant::SYSTEM_REPORT_CM3_03,
            ReportConstant::SYSTEM_REPORT_CM4_03,
            ReportConstant::SYSTEM_REPORT_CM5_03,
            ReportConstant::SYSTEM_REPORT_PRJ17,

            ReportConstant::SYSTEM_REPORT_PERS01,
            ReportConstant::SYSTEM_REPORT_PERS02,

            ReportConstant::SYSTEM_REPORT_RSK03,

            ReportConstant::SYSTEM_REPORT_HLP13,
            ReportConstant::SYSTEM_REPORT_PR_CLI_TUN01,
            ReportConstant::SYSTEM_REPORT_PR_CLI_TUN02,
            ReportConstant::SYSTEM_REPORT_PR_CLI_CEC01,
            ReportConstant::SYSTEM_REPORT_ES_CLI_SEF01,
            ReportConstant::SYSTEM_REPORT_QST_CLI_LBHFH01
        ];
    }

    public static function getExcelWhiteList()
    {
        return [
            ReportConstant::SYSTEM_REPORT_SCF01,
            ReportConstant::SYSTEM_REPORT_PR21,
            ReportConstant::SYSTEM_REPORT_INS_CLI_WAK02,
            ReportConstant::SYSTEM_REPORT_CND_CLI_CFF01,
            ReportConstant::SYSTEM_REPORT_ES_CLI_SUN01,
            // RB 06/01/21 Looks to have been added in error whils TC was working on CLD-13084.
            // Commented out so the report will run.
            //ReportConstant::SYSTEM_REPORT_INT_CLI_MK01,
            ReportConstant::SYSTEM_REPORT_RB07,
            ReportConstant::SYSTEM_REPORT_PRJ19,
            ReportConstant::SYSTEM_REPORT_PR32,
            ReportConstant::SYSTEM_REPORT_PR42,
            ReportConstant::SYSTEM_REPORT_DLO21,
            ReportConstant::SYSTEM_REPORT_ES39,
            ReportConstant::SYSTEM_REPORT_RE03
        ];
    }

    public static function getCustomizeCsvWhiteList()
    {
        return [
            ReportConstant::SYSTEM_REPORT_INS06,
            ReportConstant::SYSTEM_REPORT_INS08,
            ReportConstant::SYSTEM_REPORT_INS_CLI_RBWM01,
            ReportConstant::SYSTEM_REPORT_HLP_CLI_RCBC01,
            ReportConstant::SYSTEM_REPORT_INT_CLI_CEC01,
            ReportConstant::SYSTEM_REPORT_FAR23,
            ReportConstant::SYSTEM_REPORT_FAR24,
            ReportConstant::SYSTEM_REPORT_FAR25,
            ReportConstant::SYSTEM_REPORT_FAR26,
            ReportConstant::SYSTEM_REPORT_FAR27,
            ReportConstant::SYSTEM_REPORT_FAR28,
            ReportConstant::SYSTEM_REPORT_FAR29,
            ReportConstant::SYSTEM_REPORT_FAR30,
            ReportConstant::SYSTEM_REPORT_FAR33,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_RBY01,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_RBY02,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_RBY03,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_RBY04,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_RBY05,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_RBY06,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_RBY07,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_RBY08,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_RBY09,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_RBY10,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_MK01,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_MK02,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_MK03,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_MK04,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_MK05,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_MK06,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_MK07,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_MK08,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_MK09,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_MK10,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_CFF01,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_CFF02,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_CFF03,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_CFF04,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_CFF05,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_CFF06,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_CFF07,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_CFF08,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_CFF09,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_NMNI01,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_NMNI02,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_NMNI03,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_NMNI04,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_NMNI05,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_SAND01,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_GCC01,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_GCC02,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_GCC03,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_GCC04,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_GCC05,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_GCC06,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_NEWC01,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_NEWC02,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_NEWC03,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_NEWC04,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_NEWC05,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_NEWC06,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_NEWC07,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_NEWC08,
            ReportConstant::SYSTEM_REPORT_FAR_CLI_NEWC09,
            ReportConstant::SYSTEM_REPORT_PR_CLI_STFC01,
            ReportConstant::SYSTEM_REPORT_PR_CLI_STFC02,
            ReportConstant::SYSTEM_REPORT_PL05,
            ReportConstant::SYSTEM_REPORT_CND24,
            ReportConstant::SYSTEM_REPORT_CND25,
            ReportConstant::SYSTEM_REPORT_INS11,
            ReportConstant::SYSTEM_REPORT_DLO16,
            ReportConstant::SYSTEM_REPORT_ES48,
            ReportConstant::SYSTEM_REPORT_UT10,
        ];
    }

    public function getNonViewQueryReportWhiteList()
    {
        return [
            ReportConstant::SYSTEM_REPORT_AUD01,
        ];
    }

    private function setDefaultFilterValues($report)
    {
        if ($report->repType == Report::SYSTEM_REPORT) {
            // Only set defaults for system reports. The userdefined reports have their own filtering
            $reportCode = $report->getSystemReportCode();
            switch ($reportCode) {
                case ReportConstant::SYSTEM_REPORT_INT01:
                case ReportConstant::SYSTEM_REPORT_INT02:
                case ReportConstant::SYSTEM_REPORT_INT04:
                case ReportConstant::SYSTEM_REPORT_INT_CLI_MK01:
                case ReportConstant::SYSTEM_REPORT_INT_CLI_NEL01:
                case ReportConstant::SYSTEM_REPORT_CONT_CLI_BRA01:
                case ReportConstant::SYSTEM_REPORT_INT05:
                case ReportConstant::SYSTEM_REPORT_INT06:
                case ReportConstant::SYSTEM_REPORT_INT08:
                case ReportConstant::SYSTEM_REPORT_INT09:
                case ReportConstant::SYSTEM_REPORT_INT10:
                case ReportConstant::SYSTEM_REPORT_INT11:
                case ReportConstant::SYSTEM_REPORT_INT12:
                case ReportConstant::SYSTEM_REPORT_INT13:
                case ReportConstant::SYSTEM_REPORT_INT15:
                case ReportConstant::SYSTEM_REPORT_INT16:
                case ReportConstant::SYSTEM_REPORT_INT17:
                case ReportConstant::SYSTEM_REPORT_INT18:
                case ReportConstant::SYSTEM_REPORT_INT19:
                case ReportConstant::SYSTEM_REPORT_INT20:
                case ReportConstant::SYSTEM_REPORT_INT21:
                case ReportConstant::SYSTEM_REPORT_INT25:
                    \Input::merge(
                        ['generated' => true, 'printed' => true]
                    );
                    break;
                case ReportConstant::SYSTEM_REPORT_PRJ12:
                case ReportConstant::SYSTEM_REPORT_PRJ03:
                    \Input::merge(
                        ['issueStatusFilterOpen' => true]
                    );
                    break;
                case ReportConstant::SYSTEM_REPORT_CND08:
                    \Input::merge(
                        ['draft' => true, 'open' => true, 'qa' => true]
                    );
                    break;
                case ReportConstant::SYSTEM_REPORT_PL02:
                    break;
                default:
                    break;
            }
        }
    }

    private function reGetExtraReportDetails($report)
    {
        if (!$report instanceof Report) {
            return false;
        }

        $reportType = $report->getRepType();
        if ($reportType === Report::USER_REPORT) {
            $repUserdefData = ReportGenerateService::reGetUserDefLabels($report->getRepReportId(), false);
        } elseif ($reportType === Report::SYSTEM_REPORT) {
            $repUserdefData = ReportGenerateService::reGetUserDefLabels($report->getRepReportId(), true);
        }

        $report->setRepUserdefData($repUserdefData);
        $reportCode = $report->getSystemReportCode();

        switch ($reportCode) {
            /*
             * General Module
             */
            // DOC01: Document
            case ReportConstant::SYSTEM_REPORT_DOC01:
            case ReportConstant::SYSTEM_REPORT_DOC02:
            case ReportConstant::SYSTEM_REPORT_INS_CLI_WAK01:
                $report->setRepFilterable(true);
                $report->setRepLocMax(1);
                $report->setRepFilterDocument(true);
                break;

            /*
             * Condition Module
             */
            case ReportConstant::SYSTEM_REPORT_CND01:
                $report->setRepFilterable(true);
                $report->setRepLocMax(2);
                $report->setIgnoreSiteId(false);
                $report->setRepFilterCondition(ReportConstant::SYSTEM_REPORT_CND01);
                break;
            case ReportConstant::SYSTEM_REPORT_CND02:
            case ReportConstant::SYSTEM_REPORT_CND04:
            case ReportConstant::SYSTEM_REPORT_CND05:
            case ReportConstant::SYSTEM_REPORT_CND07:
            case ReportConstant::SYSTEM_REPORT_CND08:
            case ReportConstant::SYSTEM_REPORT_CND09:
            case ReportConstant::SYSTEM_REPORT_CND10:
            case ReportConstant::SYSTEM_REPORT_CND12:
            case ReportConstant::SYSTEM_REPORT_CND13:
                $report->setRepFilterable(true);
                $report->setRepLocMax(2);
                $report->setIgnoreSiteId(false);
                $report->setRepFilterCondition(true);
                break;
            case ReportConstant::SYSTEM_REPORT_CND03:
                $report->setRepFilterable(true);
                $report->setRepFilterConditionIdentifiedWork(ReportConstant::SYSTEM_REPORT_CND03);
                $report->setRepLocMax(3);
                $report->setIgnoreSiteId(false);
                break;
            case ReportConstant::SYSTEM_REPORT_ADM01:
            case ReportConstant::SYSTEM_REPORT_ADM17:
            case ReportConstant::SYSTEM_REPORT_ADM20:
            case ReportConstant::SYSTEM_REPORT_ADM21:
                break;
            case ReportConstant::SYSTEM_REPORT_CND06:
                $report->setRepFilterable(true);
                $report->setRepLocMax(3);
                $report->setIgnoreSiteId(false);
                $report->setRepFilterConditionItemsBySurvey(true);
                break;

            case ReportConstant::SYSTEM_REPORT_CND11:
                $report->setRepFilterable(true);
                $report->setRepLocMax(3);
                $report->setIgnoreSiteId(false);
                $report->setRepFilterIdworkTotalCostByProperty(true);
                break;

            /*
             * Resource Booking
             */
            case ReportConstant::SYSTEM_REPORT_RB01:
            case ReportConstant::SYSTEM_REPORT_RB06:
                $report->setRepFilterable(true);
                $report->setRepFilterResourceBooking(true);
                break;
            case ReportConstant::SYSTEM_REPORT_RB02:
                $report->setRepFilterable(true);
                $report->setRepFilterCateringBooking(true);
                break;
            case ReportConstant::SYSTEM_REPORT_RB03:
                $report->setRepFilterable(true);
                $report->setRepFilterBookableResource(true);
                $report->setRepLocMax(3);
                break;
            case ReportConstant::SYSTEM_REPORT_RB04:
                $report->setRepFilterable(true);
                $report->setRepFilterCateringItem(true);
                break;
            case ReportConstant::SYSTEM_REPORT_RB05:
                $report->setRepFilterable(true);
                $report->setRepLocMax(3);
                $report->setRepFilterBookableResourceAvailability(true);
                break;

            case "INF02":
                $report->setRepFilterable(true);
                $report->setRepLocMax(0);
                $report->setIgnoreSiteId(false);
                $report->setRepFilterInterfaceLog(true);
                break;

            /*
             * Instruction Module
             */
            case ReportConstant::SYSTEM_REPORT_INT01:
                $report->setRepFilterable(true);
                $report->setRepLocMax(3);
                $report->setIgnoreSiteId(false);
                $report->setRepFilterInstructions(ReportConstant::SYSTEM_REPORT_INT01);
                break;
            case ReportConstant::SYSTEM_REPORT_INT02:
                $report->setRepFilterable(true);
                $report->setRepLocMax(3);
                $report->setIgnoreSiteId(false);
                $report->setRepFilterInstructions(ReportConstant::SYSTEM_REPORT_INT02);
                break;
            case ReportConstant::SYSTEM_REPORT_INT04:
                $report->setRepFilterable(true);
                $report->setRepLocMax(3);
                $report->setIgnoreSiteId(false);
                $report->setRepFilterInstructions(ReportConstant::SYSTEM_REPORT_INT04);
                break;
            case ReportConstant::SYSTEM_REPORT_INT_CLI_MK01:
                $report->setRepFilterable(true);
                $report->setRepLocMax(3);
                $report->setIgnoreSiteId(false);
                $report->setRepFilterInstructions(ReportConstant::SYSTEM_REPORT_INT_CLI_MK01);
                break;
            case ReportConstant::SYSTEM_REPORT_INT_CLI_NEL01:
                $report->setRepFilterable(true);
                $report->setRepLocMax(3);
                $report->setIgnoreSiteId(false);
                $report->setRepFilterInstructions(ReportConstant::SYSTEM_REPORT_INT_CLI_NEL01);
                break;
            case ReportConstant::SYSTEM_REPORT_CONT_CLI_BRA01:
                $report->setRepFilterable(true);
                $report->setRepLocMax(3);
                $report->setIgnoreSiteId(false);
                $report->setRepFilterContractInspectionReport(true);
                break;
            case ReportConstant::SYSTEM_REPORT_INT05:
                $report->setRepFilterable(true);
                $report->setRepLocMax(3);
                $report->setIgnoreSiteId(false);
                $report->setRepFilterInstructions(ReportConstant::SYSTEM_REPORT_INT05);
                break;
            case ReportConstant::SYSTEM_REPORT_INT06:
                $report->setRepFilterable(true);
                $report->setRepLocMax(3);
                $report->setIgnoreSiteId(false);
                $report->setRepFilterInstructions(ReportConstant::SYSTEM_REPORT_INT06);
                break;
            case ReportConstant::SYSTEM_REPORT_INT07:
                $report->setRepFilterable(true);
                $report->setRepLocMax(3);
                $report->setIgnoreSiteId(true);
                $report->setRepFilterInstructions(ReportConstant::SYSTEM_REPORT_INT07);
                break;
            case ReportConstant::SYSTEM_REPORT_INT08:
                $report->setRepFilterable(true);
                $report->setRepLocMax(3);
                $report->setIgnoreSiteId(false);
                $report->setRepFilterInstructions(ReportConstant::SYSTEM_REPORT_INT08);
                break;
            case ReportConstant::SYSTEM_REPORT_INT09:
                $report->setRepFilterable(true);
                $report->setRepLocMax(3);
                $report->setIgnoreSiteId(false);
                $report->setRepFilterInstructions(ReportConstant::SYSTEM_REPORT_INT09);
                break;
            case ReportConstant::SYSTEM_REPORT_INT10:
                $report->setRepFilterable(true);
                $report->setRepLocMax(3);
                $report->setIgnoreSiteId(false);
                $report->setRepFilterInstructions(ReportConstant::SYSTEM_REPORT_INT10);
                break;
            case ReportConstant::SYSTEM_REPORT_INT11:
                $report->setRepFilterable(true);
                $report->setRepLocMax(3);
                $report->setIgnoreSiteId(false);
                $report->setRepFilterInstructions(ReportConstant::SYSTEM_REPORT_INT11);
                break;
            case ReportConstant::SYSTEM_REPORT_INT12:
                $report->setRepFilterable(true);
                $report->setRepLocMax(3);
                $report->setIgnoreSiteId(false);
                $report->setRepFilterInstructions(ReportConstant::SYSTEM_REPORT_INT12);
                break;
            case ReportConstant::SYSTEM_REPORT_INT13:
                $report->setRepFilterable(true);
                $report->setRepLocMax(3);
                $report->setIgnoreSiteId(false);
                $report->setRepFilterInstructions(ReportConstant::SYSTEM_REPORT_INT13);
                break;
            case ReportConstant::SYSTEM_REPORT_INT14:
                $report->setRepFilterable(true);
                $report->setRepLocMax(3);
                $report->setIgnoreSiteId(false);
                $report->setRepFilterInstructions(ReportConstant::SYSTEM_REPORT_INT14);
                break;
            case ReportConstant::SYSTEM_REPORT_INT15:
                $report->setRepFilterable(true);
                $report->setRepLocMax(3);
                $report->setIgnoreSiteId(false);
                $report->setRepFilterInstructions(ReportConstant::SYSTEM_REPORT_INT15);
                break;
            case ReportConstant::SYSTEM_REPORT_INT16:
                $report->setRepFilterable(true);
                $report->setRepLocMax(3);
                $report->setIgnoreSiteId(false);
                $report->setRepFilterInstructions(ReportConstant::SYSTEM_REPORT_INT16);
                break;
            case ReportConstant::SYSTEM_REPORT_INT17:
                $report->setRepFilterable(true);
                $report->setRepLocMax(3);
                $report->setIgnoreSiteId(false);
                $report->setRepFilterInstructions(ReportConstant::SYSTEM_REPORT_INT17);
                break;
            case ReportConstant::SYSTEM_REPORT_INT18:
                $report->setRepFilterable(true);
                $report->setRepLocMax(3);
                $report->setIgnoreSiteId(false);
                $report->setRepFilterInstructions(ReportConstant::SYSTEM_REPORT_INT18);
                break;
            case ReportConstant::SYSTEM_REPORT_INT19:
                $report->setRepFilterable(true);
                $report->setRepLocMax(3);
                $report->setIgnoreSiteId(false);
                $report->setRepFilterInstructions(ReportConstant::SYSTEM_REPORT_INT19);
                break;
            case ReportConstant::SYSTEM_REPORT_INT20:
                $report->setRepFilterable(true);
                $report->setRepLocMax(3);
                $report->setIgnoreSiteId(false);
                $report->setRepFilterInstructions(ReportConstant::SYSTEM_REPORT_INT20);
                break;
            case ReportConstant::SYSTEM_REPORT_INT21:
                $report->setRepFilterable(true);
                $report->setRepLocMax(3);
                $report->setIgnoreSiteId(false);
                $report->setRepFilterInstructions(ReportConstant::SYSTEM_REPORT_INT21);
                break;
            case ReportConstant::SYSTEM_REPORT_INT22:
                $report->setRepFilterable(true);
                $report->setRepLocMax(3);
                $report->setIgnoreSiteId(false);
                $report->setRepFilterInstructions(ReportConstant::SYSTEM_REPORT_INT22);
                break;
            case ReportConstant::SYSTEM_REPORT_INT25:
                $report->setRepFilterable(true);
                $report->setRepLocMax(3);
                $report->setIgnoreSiteId(false);
                $report->setRepFilterInstructions(ReportConstant::SYSTEM_REPORT_INT25);
                break;
            case ReportConstant::SYSTEM_REPORT_INT23:
            case ReportConstant::SYSTEM_REPORT_PR38:
            case ReportConstant::SYSTEM_REPORT_PR39:
            case ReportConstant::SYSTEM_REPORT_PR40:
            case ReportConstant::SYSTEM_REPORT_PR41:
            case ReportConstant::SYSTEM_REPORT_PR45:
            case ReportConstant::SYSTEM_REPORT_INT_CLI_CEC02:
                $report->setRepFilterable(true);
                break;
            case ReportConstant::SYSTEM_REPORT_INT_CLI_OSS01:
                $report->setRepFilterable(true);
                $report->setRepLocMax(3);
                $report->setIgnoreSiteId(false);
                $report->setRepFilterInstructions(ReportConstant::SYSTEM_REPORT_INT_CLI_OSS01);
                break;
            case ReportConstant::SYSTEM_REPORT_INT_CLI_PP01:
                $report->setRepFilterable(true);
                $report->setRepLocMax(3);
                $report->setIgnoreSiteId(false);
                $report->setRepFilterInstructions(ReportConstant::SYSTEM_REPORT_INT_CLI_PP01);
                break;
            case ReportConstant::SYSTEM_REPORT_INT_CLI_NBS01:
                $report->setRepFilterable(true);
                $report->setRepLocMax(1);
                $report->setIgnoreSiteId(false);
                $report->setRepFilterInstructions(ReportConstant::SYSTEM_REPORT_INT_CLI_NBS01);
                break;
            case ReportConstant::SYSTEM_REPORT_INT_CLI_NBS02:
                $report->setRepFilterable(true);
                $report->setRepLocMax(1);
                $report->setIgnoreSiteId(false);
                $report->setRepFilterInspection(ReportConstant::SYSTEM_REPORT_INT_CLI_NBS02);
                break;
            case ReportConstant::SYSTEM_REPORT_INF01:
                $report->setRepFilterable(true);
                $report->setRepFilterInterfaces(ReportConstant::SYSTEM_REPORT_INF01);
                break;
            case ReportConstant::SYSTEM_REPORT_ADM18:
                $report->setRepFilterable(true);
                $report->setRepFilterInvoiceSOR(true);
                break;
            case ReportConstant::SYSTEM_REPORT_STK01:
                $report->setRepFilterable(true);
                $report->setRepFilterStock(ReportConstant::SYSTEM_REPORT_STK01);
                break;
            case ReportConstant::SYSTEM_REPORT_INV01:
                $report->setRepFilterable(true);
                $report->setRepFilterInvoices(ReportConstant::SYSTEM_REPORT_INV01);
                break;
            case ReportConstant::SYSTEM_REPORT_INV02:
                $report->setRepFilterable(true);
                $report->setRepFilterInvoices(ReportConstant::SYSTEM_REPORT_INV02);
                break;
            case ReportConstant::SYSTEM_REPORT_INV03:
                $report->setRepFilterable(true);
                $report->setRepFilterInvoices(ReportConstant::SYSTEM_REPORT_INV03);
                break;
            case ReportConstant::SYSTEM_REPORT_INV04:
                $report->setRepFilterable(true);
                $report->setRepFilterInvoices(ReportConstant::SYSTEM_REPORT_INV04);
                break;
            case ReportConstant::SYSTEM_REPORT_INV_CONT01:
                $report->setRepFilterable(true);
                $report->setRepFilterInvoices(ReportConstant::SYSTEM_REPORT_INV_CONT01);
                break;
            case ReportConstant::SYSTEM_REPORT_INV_CONT02:
                $report->setRepFilterable(true);
                $report->setRepFilterInvoices(ReportConstant::SYSTEM_REPORT_INV_CONT02);
                break;
            case ReportConstant::SYSTEM_REPORT_INV_CONT03:
                $report->setRepFilterable(true);
                $report->setRepFilterInvoices(ReportConstant::SYSTEM_REPORT_INV_CONT03);
                break;
            case ReportConstant::SYSTEM_REPORT_INV_CONT_CLI_REDB01:
                $report->setRepFilterable(true);
                $report->setRepFilterInvoices(ReportConstant::SYSTEM_REPORT_INV_CONT_CLI_REDB01);
                break;
            case ReportConstant::SYSTEM_REPORT_CONT09:
                $report->setRepFilterable(true);
                $report->setRepFilterInvoices(ReportConstant::SYSTEM_REPORT_CONT09);
                break;
            case ReportConstant::SYSTEM_REPORT_SUIT01:
            case ReportConstant::SYSTEM_REPORT_SUIT02:
            case ReportConstant::SYSTEM_REPORT_SUIT03:
                $report->setRepLocMax(false);
                $report->setRepFilterable(true);
                $report->setRepFilterSuitability(true);
                break;

            // SUFF01, SUFF02, SUFF03 is the same filter, same filterAll (SufficiencyService::filterAll)
            case ReportConstant::SYSTEM_REPORT_SUFF01:
            case ReportConstant::SYSTEM_REPORT_SUFF02:
            case ReportConstant::SYSTEM_REPORT_SUFF03:
            case ReportConstant::SYSTEM_REPORT_SUFF04:
                $report->setRepLocMax(false);
                $report->setRepFilterable(true);
                $report->setRepFilterSufficiency(true);
                break;

            case ReportConstant::SYSTEM_REPORT_CAP01:
            case ReportConstant::SYSTEM_REPORT_CAP02:
                $report->setRepLocMax(false);
                $report->setRepFilterable(true);
                $report->setRepFilterCapacity(true);
                break;
            case ReportConstant::SYSTEM_REPORT_CAP03:
                $report->setRepFilterable(true);
                $report->setRepFilterRoomCapacity(true);
                break;

            case "SUIT01":
            case "SUIT02":
            case "SUIT03":
                $report->setRepLocMax(1);
                break;

            /*
             * Fixed Asset Module
             */
            case ReportConstant::SYSTEM_REPORT_FAR03:
            case ReportConstant::SYSTEM_REPORT_FAR04:
            case ReportConstant::SYSTEM_REPORT_FAR06:
            case ReportConstant::SYSTEM_REPORT_FAR10:
            case ReportConstant::SYSTEM_REPORT_FAR12:
            case ReportConstant::SYSTEM_REPORT_FAR13:
            case ReportConstant::SYSTEM_REPORT_FAR14:
            case ReportConstant::SYSTEM_REPORT_FAR15:
            case ReportConstant::SYSTEM_REPORT_FAR16:
            case ReportConstant::SYSTEM_REPORT_FAR17:
                $report->setRepFilterable(true);
                $report->setRepLocMax(3);
                $report->setRepFilterCapitalAccounting(true);
                break;
            case ReportConstant::SYSTEM_REPORT_FAR07:
                $report->setRepFilterable(true);
                $report->setRepFilterCapitalAccountingFull(ReportConstant::SYSTEM_REPORT_FAR07);
                break;
            case ReportConstant::SYSTEM_REPORT_FAR34:
                $report->setRepFilterable(true);
                $report->setRepFilterCapitalAccountingFull(ReportConstant::SYSTEM_REPORT_FAR34);
                break;
            case ReportConstant::SYSTEM_REPORT_FAR35:
                $report->setRepFilterable(true);
                $report->setRepFilterCapitalAccountingTransaction(ReportConstant::SYSTEM_REPORT_FAR35);
                break;
            case ReportConstant::SYSTEM_REPORT_FAR36:
                $report->setRepFilterable(true);
                $report->setRepFilterCapitalAccountingFull(ReportConstant::SYSTEM_REPORT_FAR36);
                break;
            case ReportConstant::SYSTEM_REPORT_FAR22:
                $report->setRepFilterable(true);
                $report->setRepLocMax(3);
                $report->setRepFilterCapitalAccountingFull(ReportConstant::SYSTEM_REPORT_FAR22);
                break;
            case ReportConstant::SYSTEM_REPORT_FAR01:
            case ReportConstant::SYSTEM_REPORT_FAR08:
            case ReportConstant::SYSTEM_REPORT_FAR31:
            case ReportConstant::SYSTEM_REPORT_FAR32:
                $report->setRepFilterable(true);
                $report->setRepFilterCapitalAccountingAlt1($reportCode);
                break;
            case ReportConstant::SYSTEM_REPORT_FAR37:
                $report->setRepFilterable(true);
                $report->setRepFilterCapitalAccountingAlt2(true);
                break;

            /**
             * PPP Module
             */
            case ReportConstant::SYSTEM_REPORT_PPP01:
            case ReportConstant::SYSTEM_REPORT_PPP02:
            case ReportConstant::SYSTEM_REPORT_PPP03:
            case ReportConstant::SYSTEM_REPORT_PPP04:
                // PPP Pupils
                $report->setRepFilterable(true);
                $report->setRepFilterPpp(true);
                break;

            case ReportConstant::SYSTEM_REPORT_PPP05:
                // PPP Pupils
                $report->setRepFilterable(true);
                $report->setRepFilterPppPupils(ReportConstant::SYSTEM_REPORT_PPP05);
                break;

            case ReportConstant::SYSTEM_REPORT_PPP06:
                // Placements
                $report->setRepFilterable(true);
                $report->setRepFilterPlacement(ReportConstant::SYSTEM_REPORT_PPP06);
                break;

            case ReportConstant::SYSTEM_REPORT_PPP07:
                // Placement Sites
                $report->setRepFilterable(true);
                $report->setRepFilterPlacementSite(ReportConstant::SYSTEM_REPORT_PPP07);
                break;
            /**
             * Dlo Module
             */
            case ReportConstant::SYSTEM_REPORT_DLO01:
                $report->setRepFilterable(true);
                $report->setRepFilterDLOLabourExceptions(ReportConstant::SYSTEM_REPORT_DLO01);
                break;

            case ReportConstant::SYSTEM_REPORT_DLO04:
            case ReportConstant::SYSTEM_REPORT_DLO13:
            case ReportConstant::SYSTEM_REPORT_DLO14:
            case ReportConstant::SYSTEM_REPORT_DLO17:
            case ReportConstant::SYSTEM_REPORT_DLO19:
            case ReportConstant::SYSTEM_REPORT_DLO21:
            case ReportConstant::SYSTEM_REPORT_DLO24:
            case ReportConstant::SYSTEM_REPORT_DLO_CLI_WAK01:
            case ReportConstant::SYSTEM_REPORT_DLO_CLI_STFC01:
            case ReportConstant::SYSTEM_REPORT_DLO23:
            case ReportConstant::SYSTEM_REPORT_DLO_CLI_REDCAR01:
                // DLO Jobs
                $report->setRepFilterable(true);
                $report->setRepFilterDLOJob(true);
                break;

            case ReportConstant::SYSTEM_REPORT_DLO_CLI_MK01:
                // DLO Jobs
                $report->setRepFilterable(true);
                $report->setRepFilterDLOJobMK(true);
                break;

            case ReportConstant::SYSTEM_REPORT_DLO05:
                $report->setRepFilterable(true);
                $report->setRepFilterDLOMaterialItem(true);
                break;

            case ReportConstant::SYSTEM_REPORT_DLO06:
                $report->setRepFilterable(true);
                $report->setRepFilterDloHoursWorked(true);
                break;

            case ReportConstant::SYSTEM_REPORT_DLO07:
                $report->setRepFilterable(true);
                $report->setRepFilterDloOperative(true);
                break;

            case ReportConstant::SYSTEM_REPORT_DLO08:
            case ReportConstant::SYSTEM_REPORT_DLO15:
                $report->setRepFilterable(true);
                $report->setRepFilterDloLabourHours(true);
                break;

            case ReportConstant::SYSTEM_REPORT_DLO09:
                $report->setRepFilterable(true);
                break;

            case ReportConstant::SYSTEM_REPORT_DLO10:
            case ReportConstant::SYSTEM_REPORT_DLO11:
                $report->setRepFilterable(true);
                $report->setRepFilterDLOSalesInvoice(true);
                break;

            case ReportConstant::SYSTEM_REPORT_DLO16:
                $report->setRepFilterable(true);
                $report->setRepFilterDloOperativeHours(true);
                break;

            case ReportConstant::SYSTEM_REPORT_DLO18:
                $report->setRepFilterable(true);
                $report->setRepFilterDLOStockMaterialItem(true);
                break;

            case ReportConstant::SYSTEM_REPORT_DLO20:
                $report->setRepFilterable(true);
                $report->setRepFilterDLOStockMaterialItem(true);
                break;
            case ReportConstant::SYSTEM_REPORT_DLO22:
                $report->setRepFilterable(true);
                $report->setRepFilterDloOperativeTradeLevel(true);
                $report->setIgnoreSiteId(true);
                break;
            case ReportConstant::SYSTEM_REPORT_INT24:
                $report->setRepFilterable(true);
                break;
            case ReportConstant::SYSTEM_REPORT_BUD01:
                $report->setRepFilterable(true);
                $report->setRepFilterExpenditureBudgets(true);
                break;

            case ReportConstant::SYSTEM_REPORT_BUD02:
                $report->setRepFilterable(true);
                $report->setRepFilterEstatesBudgets(true);
                break;

            case ReportConstant::SYSTEM_REPORT_BUD03:
                $report->setRepFilterable(true);
                $report->setRepFilterSalesInvoiceBudgets(true);
                break;

            case ReportConstant::SYSTEM_REPORT_BUD04:
                $report->setRepFilterable(true);
                $report->setRepFilterEstatesExpenditureBudgets(true);
                break;

            case ReportConstant::SYSTEM_REPORT_BUD05:
                $report->setRepFilterable(true);
                $report->setRepFilterRentalPaymentBudgets(true);
                break;

            /*
             * Plant Module
             */
            case ReportConstant::SYSTEM_REPORT_PL01:
            case ReportConstant::SYSTEM_REPORT_PL06:
                $report->setRepFilterable(true);
                $report->setRepLocMax(3);
                $report->setRepFilterPlantGrp(true);
                break;

            case ReportConstant::SYSTEM_REPORT_PL03:
                $report->setRepFilterable(true);
                $report->setRepLocMax(3);
                $report->setRepFilterPl03(true);
                break;

            case ReportConstant::SYSTEM_REPORT_PL04:
                $report->setRepFilterable(true);
                $report->setRepLocMax(3);
                $report->setRepFilterPlantGrp(true);
                break;

            /*
             * Tree Module
             */
            case ReportConstant::SYSTEM_REPORT_TREE01:
                $report->setRepFilterable(true);
                $report->setRepLocMax(3);
                $report->setRepFilterPlantGrp(true);
                break;
            case ReportConstant::SYSTEM_REPORT_TREE02:
            case ReportConstant::SYSTEM_REPORT_TREE03:
                $report->setRepFilterable(true);
                $report->setRepFilterTreeSurvey(true);
                break;
            case ReportConstant::SYSTEM_REPORT_TREE04:
                $report->setRepFilterable(true);
                $report->setRepFilterTreeSurveyAction(true);
                break;
            case ReportConstant::SYSTEM_REPORT_PTW01:
                $report->setRepFilterable(true);
                $report->setRepFilterPtw(ReportConstant::SYSTEM_REPORT_PTW01);
                break;
            case ReportConstant::SYSTEM_REPORT_PTW02:
                $report->setRepFilterable(true);
                $report->setRepFilterPtw(ReportConstant::SYSTEM_REPORT_PTW02);
                break;

            /*
             * Property Module
             */
            case ReportConstant::SYSTEM_REPORT_PR01:
            case ReportConstant::SYSTEM_REPORT_PR13:
            case ReportConstant::SYSTEM_REPORT_PR_CLI_BRA01:
            case ReportConstant::SYSTEM_REPORT_PR_CLI_SAYR_01:
            case ReportConstant::SYSTEM_REPORT_PR26:
            case ReportConstant::SYSTEM_REPORT_RE01:
            case ReportConstant::SYSTEM_REPORT_RE02:
            case ReportConstant::SYSTEM_REPORT_PR28:
            case ReportConstant::SYSTEM_REPORT_PR29:
            case ReportConstant::SYSTEM_REPORT_PR32:
            case ReportConstant::SYSTEM_REPORT_PR42:
            case ReportConstant::SYSTEM_REPORT_PR35:
                $report->setRepFilterable(true);
                $report->setRepFilterSite(true);
                break;
            case ReportConstant::SYSTEM_REPORT_PR_CLI_SFRS01:
                $report->setRepFilterable(true);
                $report->setRepFilterSFRS01(true);
                break;
            case ReportConstant::SYSTEM_REPORT_PR11:
                $report->setRepFilterable(true);
                $report->setRepFilterSiteContact(true);
                break;
            case ReportConstant::SYSTEM_REPORT_PR12:
                $report->setRepFilterable(true);
                $report->setRepFilterBuild(true);
                $report->setRepFilterLoc(true);
                $report->setRepLocMax(2);
                break;
            case ReportConstant::SYSTEM_REPORT_PR02:
            case ReportConstant::SYSTEM_REPORT_PR18:
            case ReportConstant::SYSTEM_REPORT_PR27:
            case ReportConstant::SYSTEM_REPORT_PR30:
            case ReportConstant::SYSTEM_REPORT_PR36:
                $report->setRepFilterable(true);
                $report->setRepFilterBuild(true);
                $report->setRepFilterLoc(true);
                $report->setRepLocMax(2);
                break;
            case ReportConstant::SYSTEM_REPORT_PR03:
            case ReportConstant::SYSTEM_REPORT_PR31:
            case ReportConstant::SYSTEM_REPORT_PR37:
                $report->setRepFilterable(true);
                $report->setRepFilterRoom(true);
                $report->setRepLocMax(3);
                break;
            case ReportConstant::SYSTEM_REPORT_PR08:
            case ReportConstant::SYSTEM_REPORT_PR09:
                $report->setRepFilterable(true);
                $report->setRepFilterLoc(true);
                $report->setRepLocMax(2);
                $report->setRepFilterPropertyRunningCost(true);
                break;
            case ReportConstant::SYSTEM_REPORT_PR10:
                $report->setRepFilterable(true);
                $report->setRepFilterLoc(true);
                $report->setRepLocMax(2);
                break;
            case ReportConstant::SYSTEM_REPORT_PR14:
                $report->setRepFilterable(true);
                break;
            case ReportConstant::SYSTEM_REPORT_PR16:
                $report->setRepFilterable(true);
                $report->setRepFilterBuildingBlock(true);
                $report->setRepLocMax(2);
                break;
            case ReportConstant::SYSTEM_REPORT_PR19:
                $report->setRepFilterable(true);
                $report->setRepFilterExternalArea(true);
                break;
            case ReportConstant::SYSTEM_REPORT_PR20:
                $report->setRepFilterable(true);
                $report->setRepLocMax(3);
                $report->setRepFilterEducationRoom(true);
                break;
            case ReportConstant::SYSTEM_REPORT_PR21:
                $report->setRepFilterFiledPR21(true);
                $report->setRepLocMax(2);
                break;
            case ReportConstant::SYSTEM_REPORT_RB07:
                $report->setRepFilterFiledRB07(true);
                break;
            case ReportConstant::SYSTEM_REPORT_PR22:
                $report->setRepFilterable(true);
                $report->setRepLocMax(3);
                $report->setRepFilterPropReview(true);
                break;
            case ReportConstant::SYSTEM_REPORT_PR23:
                $report->setRepFilterable(true);
                break;
            case ReportConstant::SYSTEM_REPORT_PR24:
                $report->setRepFilterable(true);
                $report->setRepLocMax(2);
                $report->setRepFilterPropAgreements(true);
                break;
            case ReportConstant::SYSTEM_REPORT_PR25:
            case ReportConstant::SYSTEM_REPORT_PR33:
                $report->setRepFilterable(true);
                $report->setRepFilterLoc(true);
                $report->setRepLocMax(1);
                $report->setRepFilterPropertyRunningCost(true);
                break;
            case ReportConstant::SYSTEM_REPORT_PR34:
                $report->setRepFilterable(true);
                $report->setRepFilterLoc(true);
                $report->setRepLocMax(1);
                break;
            case ReportConstant::SYSTEM_REPORT_CLN01:
                $report->setRepFilterable(true);
                $report->setRepFilterCleaning(ReportConstant::SYSTEM_REPORT_CLN01);
                break;
            case ReportConstant::SYSTEM_REPORT_CLN02:
                $report->setRepFilterable(true);
                $report->setRepFilterCleaning(ReportConstant::SYSTEM_REPORT_CLN02);
                break;
            case ReportConstant::SYSTEM_REPORT_CLN03:
                $report->setRepFilterable(true);
                $report->setRepFilterCleaning(ReportConstant::SYSTEM_REPORT_CLN03);
                break;
            case ReportConstant::SYSTEM_REPORT_CLN04:
                $report->setRepFilterable(true);
                $report->setRepFilterCleaning(ReportConstant::SYSTEM_REPORT_CLN04);
                break;
            case ReportConstant::SYSTEM_REPORT_CLN05:
                $report->setRepFilterable(true);
                $report->setRepFilterCleaning(ReportConstant::SYSTEM_REPORT_CLN05);
                break;
            case ReportConstant::SYSTEM_REPORT_CLN06:
                $report->setRepFilterable(true);
                $report->setRepFilterCleaning(ReportConstant::SYSTEM_REPORT_CLN06);
                break;
            case ReportConstant::SYSTEM_REPORT_CLN07:
                $report->setRepFilterable(true);
                $report->setRepFilterCleaning(ReportConstant::SYSTEM_REPORT_CLN07);
                break;
            case ReportConstant::SYSTEM_REPORT_NHSCLN01:
                $report->setRepFilterable(true);
                $report->setRepFilterNhsCleaning(ReportConstant::SYSTEM_REPORT_NHSCLN01);
                break;
            case ReportConstant::SYSTEM_REPORT_NHSCLN02:
                $report->setRepFilterable(true);
                $report->setRepFilterNhsCleaning(ReportConstant::SYSTEM_REPORT_NHSCLN02);
                break;
            case ReportConstant::SYSTEM_REPORT_NHSCLN03:
                $report->setRepFilterable(true);
                $report->setRepFilterNhsCleaning(ReportConstant::SYSTEM_REPORT_NHSCLN03);
                break;
            case ReportConstant::SYSTEM_REPORT_NHSCLN04:
                $report->setRepFilterable(true);
                $report->setRepFilterNhsCleaning(ReportConstant::SYSTEM_REPORT_NHSCLN04);
                break;
            case ReportConstant::SYSTEM_REPORT_NHSCLN05:
                $report->setRepFilterable(true);
                $report->setRepFilterNhsCleaning(ReportConstant::SYSTEM_REPORT_NHSCLN05);
                break;
            case ReportConstant::SYSTEM_REPORT_NHSCLN06:
                $report->setRepFilterable(true);
                $report->setRepFilterNhsCleaning(ReportConstant::SYSTEM_REPORT_NHSCLN06);
                break;

            /**
             * Estates Module
             */
            case ReportConstant::SYSTEM_REPORT_ES01:
            case ReportConstant::SYSTEM_REPORT_ES14:
            case ReportConstant::SYSTEM_REPORT_ES19:
                // ES01: Freehold
                // ES14: Freehold Notes
                // ES19: Freehold Property
                $report->setRepFilterable(true);
                $report->setRepFilterEstatesFreehold(true);
                $report->setRepLocMax(2);
                break;

            case ReportConstant::SYSTEM_REPORT_ES02:
            case ReportConstant::SYSTEM_REPORT_ES15:
            case ReportConstant::SYSTEM_REPORT_ES36:
            case ReportConstant::SYSTEM_REPORT_ES_CLI_DON02:
                // ES02: Lease In
                // ES15: Lease In Note
                $report->setRepFilterable(true);
                $report->setRepFilterEstatesLeaseIn(true);
                $report->setRepLocMax(2);
                break;

            case ReportConstant::SYSTEM_REPORT_ES20:
                $report->setRepFilterable(true);
                $report->setRepLocMax(1);
                $report->setRepFilterEstatesLeaseOutAgreement(true);
                break;

            case ReportConstant::SYSTEM_REPORT_ES03:
            case ReportConstant::SYSTEM_REPORT_ES13:
                // ES03: Lettable Unit
                // ES13: Lease Out by Lettable Unit
                $report->setRepFilterable(true);
                $report->setRepFilterEstatesLettableUnit(true);
                break;
            case ReportConstant::SYSTEM_REPORT_ES70:
                // ES70: Lettable Unit Current Rent
                $report->setRepFilterable(true);
                $report->setRepFilterEstatesLettableUnitCurrentRent(true);
                break;
            case ReportConstant::SYSTEM_REPORT_OCC01:
                // OCC01: Occupancy Unit
                $report->setRepFilterable(true);
                $report->setRepFilterEstatesOccupancyUnit(true);
                break;

            case ReportConstant::SYSTEM_REPORT_ES04:
            case ReportConstant::SYSTEM_REPORT_RE04:
            case ReportConstant::SYSTEM_REPORT_ES_CLI_WAV01:
                // ES37: Lease Out Summary
                $report->setRepFilterable(true);
                $report->setRepFilterEstatesLeaseOut(true);
                break;

            case ReportConstant::SYSTEM_REPORT_ES05:
                // ES05: Deed Packet
                $report->setRepFilterable(true);
                $report->setRepFilterEstatesDeedPacket(true);
                break;

            case ReportConstant::SYSTEM_REPORT_ES06:
            case ReportConstant::SYSTEM_REPORT_ES17:
                // ES06: Disposal
                // ES17: Disposal Notes
                $report->setRepFilterable(true);
                $report->setRepFilterEstatesDisposal(true);
                $report->setRepLocMax(2);
                break;

            case ReportConstant::SYSTEM_REPORT_ES07:
            case ReportConstant::SYSTEM_REPORT_ES26:
            case ReportConstant::SYSTEM_REPORT_ES27:
                // ES07: Reviews
                // ES26: Lease In Reviews
                // ES27: Lease Out Reviews
                $report->setRepFilterable(true);
                $report->setRepFilterEstatesReview(true);
                break;

            case ReportConstant::SYSTEM_REPORT_ES08:
                // ES08: Titles Charges/Rents
                $report->setRepFilterable(true);
                $report->setRepFilterEstatesCharge(true);
                $report->setRepFilterEstatesChargeTenant(false);
                $report->setRepLocMax(1);
                break;

            case ReportConstant::SYSTEM_REPORT_ES09:
                // ES09: Lettings Charges/Rents
                $report->setRepFilterable(true);
                $report->setRepFilterEstatesCharge(true);
                $report->setRepFilterEstatesChargeTenant(true);
                break;

            case ReportConstant::SYSTEM_REPORT_ES10:
                // ES10: Lettable Unit Property
                $report->setRepFilterable(true);
                $report->setRepFilterEstatesLettableUnitProperty(true);
                break;

            case ReportConstant::SYSTEM_REPORT_OCC02:
                // ES10: Occupancy Unit Property
                $report->setRepFilterable(true);
                $report->setRepFilterEstatesOccupancyUnitProperty(true);
                break;

            case ReportConstant::SYSTEM_REPORT_ES11:
                // ES11: Deed Packet Property
                $report->setRepFilterable(true);
                $report->setRepFilterEstatesDeedPacketProperty(true);
                break;

            case ReportConstant::SYSTEM_REPORT_ES12:
            case ReportConstant::SYSTEM_REPORT_ES_CLI_SEF01:
                // ES12: Estates Valuations
                $report->setRepFilterable(true);
                $report->setRepFilterEstatesValuation(true);
                break;

            case ReportConstant::SYSTEM_REPORT_ES18:
            case ReportConstant::SYSTEM_REPORT_ES41:
                // ES18: Acquisitions
                // ES41: Acquisition Note
                $report->setRepFilterable(true);
                $report->setRepFilterEstatesAcquisition(true);
                break;

            case ReportConstant::SYSTEM_REPORT_ES21:
                // ES21: Lease In Agreement
                $report->setRepFilterable(true);
                $report->setRepFilterEstatesLeaseInAgreement(true);
                break;

            case ReportConstant::SYSTEM_REPORT_ES22:
                // ES22: Freehold Agreement
                $report->setRepFilterable(true);
                $report->setRepFilterEstatesFreeholdAgreement(true);
                $report->setRepLocMax(2);
                break;

            case ReportConstant::SYSTEM_REPORT_ES23:
                // ES23: Letting Packet
                $report->setRepFilterable(true);
                $report->setRepFilterEstatesLettingPacket(true);
                break;

            case ReportConstant::SYSTEM_REPORT_ES24:
                // ES24: Lease In Audit
                $report->setRepFilterable(true);
                $report->setRepFilterEstatesLeaseInAudit(true);
                break;

            case ReportConstant::SYSTEM_REPORT_ES25:
                // ES25: Lease Out Audit
                $report->setRepFilterable(true);
                $report->setRepFilterEstatesLeaseOutAudit(true);
                break;

            case ReportConstant::SYSTEM_REPORT_AUD01:
                // AUD01 Audit
                $report->setRepFilterable(true);
                $report->setRepFilterAud01Audit(true);
                break;

            case ReportConstant::SYSTEM_REPORT_NOT01:
                // NOT01 All Notes
                $report->setRepFilterable(true);
                $report->setRepAllNotes(true);
                break;

            case ReportConstant::SYSTEM_REPORT_ES28:
                // ES28: Lease Out Alienations
                $report->setRepFilterable(true);
                $report->setRepFilterEstatesAlienation(true);
                break;

            case ReportConstant::SYSTEM_REPORT_ES29:
                // ES29: Invoice Generation Plan
                $report->setRepFilterable(true);
                $report->setRepFilterEstatesInvoiceGenerationPlan(true);
                break;

            case ReportConstant::SYSTEM_REPORT_ES30:
                // ES30: Fee Receipts
                $report->setRepFilterable(true);
                $report->setRepFilterEstatesFeeReceipts(true);
                break;
            case ReportConstant::SYSTEM_REPORT_ES53:
                // ES53: Rental Payment Generation Plan
                $report->setRepFilterable(true);
                $report->setRepFilterEstatesRentalPaymentGenerationPlan(true);
                break;
            case ReportConstant::SYSTEM_REPORT_ES61:
                // ES61: Sales Invoice Lines
                $report->setRepFilterable(true);
                $report->setRepFilterSalesInvoice(true);
                break;
            case ReportConstant::SYSTEM_REPORT_ES33:
                $report->setRepFilterable(true);
                $report->setRepLocMax(2);
                $report->setRepFilterSalesInvoice(true);
                break;
            case ReportConstant::SYSTEM_REPORT_ES34:
                $report->setRepFilterable(true);
                $report->setRepLocMax(2);
                $report->setRepFilterSalesInvoice(true);
                break;
            case ReportConstant::SYSTEM_REPORT_ES_CLI_HRT01:
                $report->setRepFilterable(true);
                $report->setRepFilterSalesInvoice(true);
                break;
            case ReportConstant::SYSTEM_REPORT_ES35:
                $report->setRepFilterable(true);
                $report->setRepLocMax(2);
                $report->setRepFilterEstatesCharge(true);
                $report->setRepFilterEstatesChargeTenant(true);
                break;
            case ReportConstant::SYSTEM_REPORT_ES38:
                // ES38: Titles Charges/Rent Groups
                $report->setRepFilterable(true);
                $report->setRepFilterEstatesCharge(true);
                $report->setRepFilterEstatesChargeTenant(true);
                $report->setRepLocMax(1);
                break;
            case ReportConstant::SYSTEM_REPORT_ES42:
            case ReportConstant::SYSTEM_REPORT_ES43:
            case ReportConstant::SYSTEM_REPORT_ES44:
            case ReportConstant::SYSTEM_REPORT_ES45:
            case ReportConstant::SYSTEM_REPORT_ES46:
            case ReportConstant::SYSTEM_REPORT_ES52:
                $report->setRepFilterable(true);
                $report->setRepFilterGenChecklist(true);
                $report->setRepLocMax(1);
                break;
            case ReportConstant::SYSTEM_REPORT_ES48:
                // ES48: Multiple Estate Record Types
                $report->setRepFilterable(true);
                $report->setRepFilterEstatesMultipleRecordType(true);
                $report->setRepLocMax(2);
                break;
            case ReportConstant::SYSTEM_REPORT_ES59:
                // ES59: All lettable and occupancy units.
                $report->setRepFilterable(true);
                $report->setRepFilterEstateUnitMultipleRecordType(true);
                $report->setRepLocMax(2);
                break;
            case ReportConstant::SYSTEM_REPORT_ES49:
                $report->setRepFilterable(true);
                $report->setRepFilterEstatesLettableUnit(true);
                break;
            case ReportConstant::SYSTEM_REPORT_ES50:
                $report->setRepFilterable(true);
                //$report->setRepLocMax(1);
                $report->setRepFilterEstatesDisposalAgreement(true);
                break;
            case ReportConstant::SYSTEM_REPORT_ES51:
                $report->setRepFilterable(true);
                $report->setRepFilterEstatesTitlesAgreement(true);
                break;
            case ReportConstant::SYSTEM_REPORT_ES_CLI_SCOTB01:
                $report->setRepFilterable(true);
                //$report->setRepLocMax(1);
                $report->setRepFilterEstatesCliScotB01(true);
                break;
            case ReportConstant::SYSTEM_REPORT_ES54:
                // ES54: Notification Of Payments
                $report->setRepFilterable(true);
                $report->setRepFilterEstatesNotificationOfPayments(true);
                break;
            case ReportConstant::SYSTEM_REPORT_ES56:
                // ES56: Rental Payment
                $report->setRepFilterable(true);
                $report->setRepFilterRentalPayment(true);
                break;
            case ReportConstant::SYSTEM_REPORT_ES58:
                // ES58: Sales Invoice by Charge
                $report->setRepFilterable(true);
                $report->setRepFilterSalesInvoice(true);
                break;
            case ReportConstant::SYSTEM_REPORT_ES60:
                // ES60: Service Charge
                $report->setRepFilterable(true);
                $report->setRepFilterServiceCharge(true);
                break;
            case ReportConstant::SYSTEM_REPORT_ES62:
                // ES62: Service Charge Actual
                $report->setRepFilterable(true);
                $report->setRepFilterServiceChargeActual(true);
                break;
            case ReportConstant::SYSTEM_REPORT_ES63:
                // ES63: Service Charge Letu
                $report->setRepFilterable(true);
                $report->setRepFilterServiceChargeLetu(true);
                break;
            case ReportConstant::SYSTEM_REPORT_ES64:
                // ES64: Service Charge Schedule
                $report->setRepFilterable(true);
                $report->setRepFilterServiceChargeSchedule(true);
                break;
            case ReportConstant::SYSTEM_REPORT_ES65:
                // ES65: Lease Out Break Dates.
                $report->setRepFilterable(true);
                $report->setRepFilterLeaseOutBreakDate(true);
                break;
            case ReportConstant::SYSTEM_REPORT_ES66:
                // ES66: Lease In Break Dates
                $report->setRepFilterable(true);
                $report->setRepFilterLeaseInBreakDate(true);
                break;
            case ReportConstant::SYSTEM_REPORT_ES68:
                // ES68: Lettable Unit Energy Rating
                $report->setRepFilterable(true);
                $report->setRepFilterLettableUnitEnergyRating(true);
                break;
            case ReportConstant::SYSTEM_REPORT_ES69:
                // ES69: Lettable Unit Energy Rating Work Required
                $report->setRepFilterable(true);
                $report->setRepFilterLettableUnitEnergyRatingWorkRequired(true);
                break;

            /**
             * Project Module
             */
            case ReportConstant::SYSTEM_REPORT_PRJ01:
                // PRJ01: Project
                $report->setRepFilterable(true);
                $report->setRepLocMax(2);
                $report->setRepFilterProject(ReportConstant::SYSTEM_REPORT_PRJ01);
                break;

            case ReportConstant::SYSTEM_REPORT_PRJ15:
                // PRJ01: Project
                $report->setRepFilterable(true);
                $report->setRepLocMax(2);
                $report->setRepFilterProject(ReportConstant::SYSTEM_REPORT_PRJ15);
                break;

            case ReportConstant::SYSTEM_REPORT_PRJ17:
                // PRJ17: Project Risk Register Report
                $report->setRepFilterable(true);
                $report->setRepLocMax(2);
                $report->setRepFilterProject(ReportConstant::SYSTEM_REPORT_PRJ17);
                break;

            case ReportConstant::SYSTEM_REPORT_PRJ02:
                // PRJ02: Project Note
                $report->setRepFilterable(true);
                $report->setRepFilterProject(ReportConstant::SYSTEM_REPORT_PRJ02);
                break;

            case ReportConstant::SYSTEM_REPORT_PRJ03:
                // Project Issue
                $report->setRepFilterable(true);
                $report->setRepLocMax(2);
                $report->setRepFilterProject(ReportConstant::SYSTEM_REPORT_PRJ03);
                break;

            case ReportConstant::SYSTEM_REPORT_PRJ04:
                // Project Progress
                $report->setRepFilterable(true);
                $report->setRepLocMax(2);
                $report->setRepFilterProject(ReportConstant::SYSTEM_REPORT_PRJ04);
                break;

            case ReportConstant::SYSTEM_REPORT_PRJ05:
                // Project Risk
                $report->setRepFilterable(true);
                $report->setRepLocMax(2);
                $report->setRepFilterProject(ReportConstant::SYSTEM_REPORT_PRJ05);
                break;
            case ReportConstant::SYSTEM_REPORT_PRJ06:
                // Project Action
                $report->setRepFilterable(true);
                $report->setRepLocMax(2);
                $report->setRepFilterProject(ReportConstant::SYSTEM_REPORT_PRJ06);
                break;
            case ReportConstant::SYSTEM_REPORT_PRJ07:
                // Project Timesheets
                $report->setRepFilterable(true);
                $report->setRepFilterProject(ReportConstant::SYSTEM_REPORT_PRJ07);
                break;
            case ReportConstant::SYSTEM_REPORT_PRJ08:
                // PRJ08: Project Sales Invoices
                $report->setRepFilterable(true);
                $report->setRepLocMax(2);
                $report->setRepFilterProject(ReportConstant::SYSTEM_REPORT_PRJ08);
                break;
            case ReportConstant::SYSTEM_REPORT_PRJ09:
                // Project Change Requests
                $report->setRepFilterable(true);
                $report->setRepFilterProject(ReportConstant::SYSTEM_REPORT_PRJ09);
                break;

            case ReportConstant::SYSTEM_REPORT_PRJ10:
                // PRJ10: Cash Flow
                $report->setRepFilterable(true);
                $report->setRepLocMax(2);
                $report->setRepFilterProject(ReportConstant::SYSTEM_REPORT_PRJ10);
                break;
            case ReportConstant::SYSTEM_REPORT_PRJ16:
                // PRJ16: Cash Flow all financial year
                $report->setRepFilterable(true);
                $report->setRepLocMax(2);
                $report->setRepFilterProject(ReportConstant::SYSTEM_REPORT_PRJ16);
                break;
            case ReportConstant::SYSTEM_REPORT_PRJ14:
                // PRJ14: Project Tasks
                $report->setRepFilterable(true);
                $report->setRepLocMax(2);
                $report->setRepFilterProject(ReportConstant::SYSTEM_REPORT_PRJ14);
                break;
            case ReportConstant::SYSTEM_REPORT_PRJ18:
                // PRJ18: Project Bill Of Qty
                $report->setRepFilterable(true);
                $report->setRepFilterProject(ReportConstant::SYSTEM_REPORT_PRJ18);
                break;
            case ReportConstant::SYSTEM_REPORT_PRJ19:
                // PRJ01: Project
                $report->setRepFilterable(true);
                $report->setRepLocMax(2);
                $report->setRepFilterProject(ReportConstant::SYSTEM_REPORT_PRJ19);
                break;
            case ReportConstant::SYSTEM_REPORT_PRJ20:
                // PRJ20: Project Certificated Payments
                $report->setRepFilterable(true);
                $report->setRepLocMax(2);
                $report->setRepFilterProject(ReportConstant::SYSTEM_REPORT_PRJ20);
                break;
            case ReportConstant::SYSTEM_REPORT_PRJ21:
                // PRJ21: Project Funding Sources
                $report->setRepFilterable(true);
                $report->setRepFilterProject(ReportConstant::SYSTEM_REPORT_PRJ21);
                break;
            case ReportConstant::SYSTEM_REPORT_PRJ23:
            case ReportConstant::SYSTEM_REPORT_PRJ22:
            case ReportConstant::SYSTEM_REPORT_PRJ24:
            case ReportConstant::SYSTEM_REPORT_PRJ25:
                // PRJ23: Project Packages
                $report->setRepFilterable(true);
                break;
            case ReportConstant::SYSTEM_REPORT_PRJ_CLI_SUT01:
                //PRJ_CLI_SUT01: Sutton Billing Charges
                $report->setRepFilterable(true);
                $report->setRepFilterProject(ReportConstant::SYSTEM_REPORT_PRJ_CLI_SUT01);
                break;

            /**
             * Instruction Module
             */
            case ReportConstant::SYSTEM_REPORT_ADM32:
                // ADM32: Account Code
                $report->setRepFilterable(true);
                $report->setRepFilterInstructions(true);
                break;

            /**
             * Transparency
             */
            case ReportConstant::SYSTEM_REPORT_TPY01:
            case ReportConstant::SYSTEM_REPORT_TPY02:
            case ReportConstant::SYSTEM_REPORT_TPY03:
            case ReportConstant::SYSTEM_REPORT_TPY04:
                $report->setRepFilterable(true);
                $report->setRepFilterTransparency(true);
                $report->setRepLocMax(2);
                break;

            case ReportConstant::SYSTEM_REPORT_INS01:           // Inspection
            case ReportConstant::SYSTEM_REPORT_INS02:           // Contractor Inspection
            case ReportConstant::SYSTEM_REPORT_INS09:       // Contractor Inspection by Plant Item
            case ReportConstant::SYSTEM_REPORT_INS03:           // Inspections By Site
            case ReportConstant::SYSTEM_REPORT_INS04:           // Inspections By Building
            case ReportConstant::SYSTEM_REPORT_INS05:           // Inspection With Tasks
            case ReportConstant::SYSTEM_REPORT_INS_CLI_LAN:
            case ReportConstant::SYSTEM_REPORT_INS_CLI_LANC02:
            case ReportConstant::SYSTEM_REPORT_INS06:           // Inspection Calendar
            case ReportConstant::SYSTEM_REPORT_INS07:           // Inspection Note
            case ReportConstant::SYSTEM_REPORT_INS08:
            case ReportConstant::SYSTEM_REPORT_INS10:           // Inspection
                $report->setRepFilterable(true);
                $report->setRepFilterInspection($reportCode);
                $report->setRepLocMax(3);
                break;
            case ReportConstant::SYSTEM_REPORT_INS_CLI_RBWM01:
                $report->setRepFilterable(true);
                $report->setRepFilterPlantGrp($reportCode);
                $report->setRepLocMax(3);
                break;
            case ReportConstant::SYSTEM_REPORT_HLP_CLI_RCBC01:
                $report->setRepFilterable(true);
                $report->setRepFilterHlpRcbc01($reportCode);
                $report->setRepLocMax(3);
                break;

            case ReportConstant::SYSTEM_REPORT_ADM02:
            case ReportConstant::SYSTEM_REPORT_ADM35:
                $report->setIgnoreSiteId(true);
                $report->setRepFilterUser(true);
                $report->setRepFilterable(true);
                break;
            case ReportConstant::SYSTEM_REPORT_ADM36:
            case ReportConstant::SYSTEM_REPORT_ADM38:
            case ReportConstant::SYSTEM_REPORT_ADM38_ALT:
                $report->setRepFilterable(true);
                $report->setRepFilterEmail(true);
                break;
            case ReportConstant::SYSTEM_REPORT_ADM03:
            case ReportConstant::SYSTEM_REPORT_ADM33:
            case ReportConstant::SYSTEM_REPORT_ADM34:
                $report->setRepFilterable(true);
                $report->setRepFilterContact(true);
                $report->setIgnoreSiteId(true);
                break;

            case ReportConstant::SYSTEM_REPORT_ADM04:
                $report->setIgnoreSiteId(true);
                break;

            case ReportConstant::SYSTEM_REPORT_ADM05:
                $report->setIgnoreSiteId(true);
                break;

            case ReportConstant::SYSTEM_REPORT_ADM06:
                $report->setIgnoreSiteId(true);
                $report->setUseSiteGroupId(false);
                break;

            case "ADM07":
                $report->setIgnoreSiteId(true);
                break;

            case "ADM08":
                $report->setIgnoreSiteId(true);
                break;

            case "ADM09":
                $report->setIgnoreSiteId(true);
                break;

            case "ADM10":
                $report->setIgnoreSiteId(true);
                break;

            case "ADM11":
                $report->setIgnoreSiteId(true);
                break;

            case "ADM12":
                $report->setIgnoreSiteId(true);
                break;

            case "ADM13":
                $report->setIgnoreSiteId(true);
                $report->setRepFilterUserLogin(true);
                $report->setRepFilterable(true);
                break;

            case "ADM14":
                $report->setUseSiteGroupId(false);
                $report->setIgnoreSiteId(true);
                break;

            case "ADM15":
                $report->setUseSiteGroupId(false);
                $report->setIgnoreSiteId(true);
                break;

            case "ADM19":
                break;

            case ReportConstant::SYSTEM_REPORT_ADM39:
                $report->setRepFilterable(true);
                $report->setRepFilterPlantPartAdm(ReportConstant::SYSTEM_REPORT_ADM39);
                break;
            /**
             * Questionnaire Module
             */
            case ReportConstant::SYSTEM_REPORT_QST01:
                $report->setRepFilterable(true);
                $report->setRepLocMax(2);
                $report->setRepFilterFiledQst(true);
                $report->setFilterFunction("reAddFiledQstQuery");
                $report->setType(null);
                break;

            case ReportConstant::SYSTEM_REPORT_QST02:
                $report->setRepFilterable(true);
                $report->setRepLocMax(1);
                $report->setRepFilterQst(true);
                $report->setFilterFunction("reAddQstQuery");
                $report->setType(null);
                break;

            case ReportConstant::SYSTEM_REPORT_QST03:
                $report->setRepFilterable(true);
                $report->setRepLocMax(1);
                $report->setRepFilterQst(true);
                $report->setFilterFunction("reAddQstQuery");
                $report->setType(null);
                break;

            case ReportConstant::SYSTEM_REPORT_QST04:
                $report->setRepFilterable(true);
                $report->setRepLocMax(2);
                $report->setRepFilterFiledQst(true);
                $report->setFilterFunction("reAddFiledQstQuery");
                $report->setType(null);
                break;

            case ReportConstant::SYSTEM_REPORT_QST05:
                $report->setRepFilterable(true);
                $report->setRepFilterFiledQstAction(true);
                $report->setRepLocMax(3);
                break;

            /**
             * Questionnaire Core Facts Module
             */
            case ReportConstant::SYSTEM_REPORT_QSTC01:
                $report->setRepFilterable(true);
                $report->setRepLocMax(2);
                $report->setRepFilterFiledQst(true);
                $report->setFilterFunction("reAddFiledQstQuery");
                $report->setType(QstType::CORE_FACTS);
                break;

            case ReportConstant::SYSTEM_REPORT_QSTC02:
                $report->setRepFilterable(true);
                $report->setRepLocMax(1);
                $report->setRepFilterQst(true);
                $report->setFilterFunction("reAddQstQuery");
                $report->setType(QstType::CORE_FACTS);
                $report->setIgnoreSiteId(true);
                break;

            case ReportConstant::SYSTEM_REPORT_QSTC03:
                $report->setRepFilterable(true);
                $report->setRepLocMax(1);
                $report->setRepFilterQst(true);
                $report->setFilterFunction("reAddQstQuery");
                $report->setType(QstType::CORE_FACTS);
                $report->setIgnoreSiteId(true);
                break;

            case ReportConstant::SYSTEM_REPORT_QSTC04:
                $report->setRepFilterable(true);
                $report->setRepLocMax(2);
                $report->setRepFilterFiledQst(true);
                $report->setFilterFunction("reAddFiledQstQuery");
                $report->setType(QstType::CORE_FACTS);
                break;

            case ReportConstant::SYSTEM_REPORT_QSTC05:
                $report->setRepFilterable(true);
                $report->setRepFilterFiledQstAction(true);
                $report->setRepLocMax(3);
                $report->setType(QstType::CORE_FACTS);
                break;
            case ReportConstant::SYSTEM_REPORT_SCF01:
                $report->setRepFilterFiledSCF01(true);
                break;

            /**
             * Questionnaire DAA Module
             */
            case ReportConstant::SYSTEM_REPORT_QSTD01:
                $report->setRepFilterable(true);
                $report->setRepLocMax(2);
                $report->setRepFilterFiledQst(true);
                $report->setFilterFunction("reAddFiledQstQuery");
                $report->setType(QstType::DAA);
                break;

            case ReportConstant::SYSTEM_REPORT_QSTD02:
                $report->setRepFilterable(true);
                $report->setRepLocMax(1);
                $report->setRepFilterQst(true);
                $report->setFilterFunction("reAddQstQuery");
                $report->setType(QstType::DAA);
                $report->setIgnoreSiteId(true);
                break;

            case ReportConstant::SYSTEM_REPORT_QSTD03:
                $report->setRepFilterable(true);
                $report->setRepLocMax(1);
                $report->setRepFilterQst(true);
                $report->setFilterFunction("reAddQstQuery");
                $report->setType(QstType::DAA);
                $report->setIgnoreSiteId(true);
                break;

            case ReportConstant::SYSTEM_REPORT_QSTD04:
                $report->setRepFilterable(true);
                $report->setRepLocMax(2);
                $report->setRepFilterFiledQst(true);
                $report->setFilterFunction("reAddFiledQstQuery");
                $report->setType(QstType::DAA);
                break;

            case ReportConstant::SYSTEM_REPORT_QSTD05:
                $report->setRepFilterable(true);
                $report->setRepFilterFiledQstAction(true);
                $report->setRepLocMax(3);
                $report->setType(QstType::DAA);
                break;

            /**
             * Questionnaire FIRE Module
             */
            case ReportConstant::SYSTEM_REPORT_QSTF01:
            case ReportConstant::SYSTEM_REPORT_QST_CLI_LBHFH01:
                $report->setRepFilterable(true);
                $report->setRepLocMax(2);
                $report->setRepFilterFiledQst(true);
                $report->setFilterFunction("reAddFiledQstQuery");
                $report->setType(QstType::FIRE);
                break;

            case ReportConstant::SYSTEM_REPORT_QSTF02:
                $report->setRepFilterable(true);
                $report->setRepLocMax(1);
                $report->setRepFilterQst(true);
                $report->setFilterFunction("reAddQstQuery");
                $report->setType(QstType::FIRE);
                $report->setIgnoreSiteId(true);
                break;

            case ReportConstant::SYSTEM_REPORT_QSTF03:
                $report->setRepFilterable(true);
                $report->setRepLocMax(1);
                $report->setRepFilterQst(true);
                $report->setFilterFunction("reAddQstQuery");
                $report->setType(QstType::FIRE);
                $report->setIgnoreSiteId(true);
                break;

            case ReportConstant::SYSTEM_REPORT_QSTF04:
                $report->setRepFilterable(true);
                $report->setRepLocMax(2);
                $report->setRepFilterFiledQst(true);
                $report->setFilterFunction("reAddFiledQstQuery");
                $report->setType(QstType::FIRE);
                break;

            case ReportConstant::SYSTEM_REPORT_QSTF05:
                $report->setRepFilterable(true);
                $report->setRepFilterFiledQstAction(true);
                $report->setRepLocMax(3);
                $report->setType(QstType::FIRE);
                break;

            /**
             * Questionnaire Genaral Module
             */
            case ReportConstant::SYSTEM_REPORT_QSTG01:
                $report->setRepFilterable(true);
                $report->setRepLocMax(2);
                $report->setRepFilterFiledQst(true);
                $report->setFilterFunction("reAddFiledQstQuery");
                $report->setType(QstType::GENERAL);
                break;

            case ReportConstant::SYSTEM_REPORT_QSTG02:
                $report->setRepFilterable(true);
                $report->setRepLocMax(1);
                $report->setRepFilterQst(true);
                $report->setFilterFunction("reAddQstQuery");
                $report->setType(QstType::GENERAL);
                $report->setIgnoreSiteId(true);
                break;

            case ReportConstant::SYSTEM_REPORT_QSTG03:
                $report->setRepFilterable(true);
                $report->setRepLocMax(1);
                $report->setRepFilterQst(true);
                $report->setFilterFunction("reAddQstQuery");
                $report->setType(QstType::GENERAL);
                $report->setIgnoreSiteId(true);
                break;

            case ReportConstant::SYSTEM_REPORT_QSTG04:
                $report->setRepFilterable(true);
                $report->setRepLocMax(2);
                $report->setRepFilterFiledQst(true);
                $report->setFilterFunction("reAddFiledQstQuery");
                $report->setType(QstType::GENERAL);
                break;

            case ReportConstant::SYSTEM_REPORT_QSTG05:
                $report->setRepFilterable(true);
                $report->setRepFilterFiledQstAction(true);
                $report->setRepLocMax(3);
                $report->setType(QstType::GENERAL);
                break;

            /**
             * Questionnaire Legionella Module
             */
            case ReportConstant::SYSTEM_REPORT_QSTL01:
                $report->setRepFilterable(true);
                $report->setRepLocMax(2);
                $report->setRepFilterFiledQst(true);
                $report->setFilterFunction("reAddFiledQstQuery");
                $report->setType(QstType::LEGIONELLA);
                break;

            case ReportConstant::SYSTEM_REPORT_QSTL02:
                $report->setRepFilterable(true);
                $report->setRepLocMax(1);
                $report->setRepFilterQst(true);
                $report->setFilterFunction("reAddQstQuery");
                $report->setType(QstType::LEGIONELLA);
                break;

            case ReportConstant::SYSTEM_REPORT_QSTL03:
                $report->setRepFilterable(true);
                $report->setRepLocMax(1);
                $report->setRepFilterQst(true);
                $report->setFilterFunction("reAddQstQuery");
                $report->setType(QstType::LEGIONELLA);
                break;

            case ReportConstant::SYSTEM_REPORT_QSTL04:
                $report->setRepFilterable(true);
                $report->setRepLocMax(2);
                $report->setRepFilterFiledQst(true);
                $report->setFilterFunction("reAddFiledQstQuery");
                $report->setType(QstType::LEGIONELLA);
                break;

            case ReportConstant::SYSTEM_REPORT_QSTL05:
                $report->setRepFilterable(true);
                $report->setRepFilterFiledQstAction(true);
                $report->setRepLocMax(3);
                $report->setType(QstType::LEGIONELLA);
                break;
            /**
             * Questionnaire Covid-19 Module
             */
            case ReportConstant::SYSTEM_REPORT_QSTCO01:
                $report->setRepFilterable(true);
                $report->setRepLocMax(2);
                $report->setRepFilterFiledQst(true);
                $report->setFilterFunction("reAddFiledQstQuery");
                $report->setType(QstType::COVID_19);
                break;
            case ReportConstant::SYSTEM_REPORT_QSTCO02:
                $report->setRepFilterable(true);
                $report->setRepLocMax(1);
                $report->setRepFilterQst(true);
                $report->setFilterFunction("reAddQstQuery");
                $report->setType(QstType::COVID_19);
                $report->setIgnoreSiteId(true);
                break;
            case ReportConstant::SYSTEM_REPORT_QSTCO03:
                $report->setRepFilterable(true);
                $report->setRepLocMax(1);
                $report->setRepFilterQst(true);
                $report->setFilterFunction("reAddQstQuery");
                $report->setType(QstType::COVID_19);
                $report->setIgnoreSiteId(true);
                break;
            case ReportConstant::SYSTEM_REPORT_QSTCO04:
                $report->setRepFilterable(true);
                $report->setRepLocMax(2);
                $report->setRepFilterFiledQst(true);
                $report->setFilterFunction("reAddFiledQstQuery");
                $report->setType(QstType::COVID_19);
                break;
            case ReportConstant::SYSTEM_REPORT_QSTCO05:
                $report->setRepFilterable(true);
                $report->setRepFilterFiledQstAction(true);
                $report->setRepLocMax(3);
                $report->setType(QstType::COVID_19);
                break;
            case ReportConstant::SYSTEM_REPORT_QST_DESIGN_QUESTIONNAIRE:
            case ReportConstant::SYSTEM_REPORT_QST_DESIGN_SECTION:
            case ReportConstant::SYSTEM_REPORT_QST_DESIGN_QUESTION:
            case ReportConstant::SYSTEM_REPORT_QST_DESIGN_QUESTION_DAA:
            case ReportConstant::SYSTEM_REPORT_QST_DESIGN_PROBLEM:
            case ReportConstant::SYSTEM_REPORT_QST_DESIGN_SOLUTION:
                $report->setRepFilterable(true);
                $report->setReportQstDesign(true);
                break;
                // Grounds Maintenance
                break;
            case ReportConstant::SYSTEM_REPORT_GM01:
                $report->setRepFilterable(true);
                $report->setRepFilterGroundsMaintenance($reportCode);
                break;
            case ReportConstant::SYSTEM_REPORT_GM02:
            case ReportConstant::SYSTEM_REPORT_GM03:
                // GM03: Grounds Maintenance Contract Tasks
                $report->setRepFilterable(true);
                $report->setRepLocMax(1);
                $report->setRepFilterGroundsMaintenance($reportCode);
                break;
            // Case Management
            case ReportConstant::SYSTEM_REPORT_CM01:
            case ReportConstant::SYSTEM_REPORT_CM02:
            case ReportConstant::SYSTEM_REPORT_CM03:
            case ReportConstant::SYSTEM_REPORT_CM2_01:
            case ReportConstant::SYSTEM_REPORT_CM2_02:
            case ReportConstant::SYSTEM_REPORT_CM2_03:
            case ReportConstant::SYSTEM_REPORT_CM3_01:
            case ReportConstant::SYSTEM_REPORT_CM3_02:
            case ReportConstant::SYSTEM_REPORT_CM3_03:
            case ReportConstant::SYSTEM_REPORT_CM4_01:
            case ReportConstant::SYSTEM_REPORT_CM4_02:
            case ReportConstant::SYSTEM_REPORT_CM4_03:
            case ReportConstant::SYSTEM_REPORT_CM5_01:
            case ReportConstant::SYSTEM_REPORT_CM5_02:
            case ReportConstant::SYSTEM_REPORT_CM5_03:
            case ReportConstant::SYTEM_REPORT_CM_CLI_SAYR_01:
                $report->setRepFilterable(true);
                $report->setRepLocMax(3);
                $report->setRepFilterCaseManagement($reportCode);
                break;

            case "SUP01":
                if (\Auth::user()->isSuperuser()) {
                    $report->setIgnoreSiteId(true);
                }
                $report->setRepFilterable(true);
                $report->setRepFilterSup01Audit(true);
                break;
            case "SUP02":
                if (\Auth::user()->isSuperuser()) {
                    $report->setIgnoreSiteId(true);
                }
                $report->setRepFilterable(true);
                $report->setRepFilterSup02Audit(true);
                break;

            case "SUP03":
                if (\Auth::user()->isSuperuser()) {
                    $report->setIgnoreSiteId(true);
                }
                $report->setRepFilterable(true);
                $report->setRepFilterSup03Audit(true);
                break;

            case "SUP04":
                if (\Auth::user()->isSuperuser()) {
                    $report->setIgnoreSiteId(true);
                }
                $report->setRepFilterable(true);
                $report->setRepFilterSup04Audit(true);
                break;

            case ReportConstant::SYSTEM_REPORT_CONT01:
                $report->setRepFilterable(true);
                $report->setRepFilterContract(true);
                break;

            case ReportConstant::SYSTEM_REPORT_CONT02:
                $report->setRepFilterable(true);
                $report->setRepFilterContractInstructions(true);
                break;

            case ReportConstant::SYSTEM_REPORT_CONT10:
                $report->setRepFilterable(true);
                $report->setRepFilterContractInstructions(true);
                $report->setIgnoreSiteId(true);
                break;

            case ReportConstant::SYSTEM_REPORT_CONT03:
                $report->setRepFilterable(true);
                $report->setRepFilterContractInvoice(true);
                break;

            case ReportConstant::SYSTEM_REPORT_CONT06:
                $report->setRepFilterable(true);
                $report->setRepFilterContractInvoiceInspection(true);
                break;

            case ReportConstant::SYSTEM_REPORT_CONT04:
                $report->setRepFilterable(true);
                $report->setRepFilterContractCredit(true);
                break;

            case ReportConstant::SYSTEM_REPORT_CONT05:
                $report->setRepFilterable(true);
                $report->setRepFilterContractCreditInspection(true);
                break;

            case ReportConstant::SYSTEM_REPORT_CONT07:
            case ReportConstant::SYSTEM_REPORT_CONT19:
                $report->setRepFilterable(true);
                $report->setRepFilterContractInvoiceCredit(true);
                break;

            case ReportConstant::SYSTEM_REPORT_CONT08:
                $report->setRepFilterable(true);
                $report->setRepFilterContractInvoiceCreditInspection(true);
                break;

            case ReportConstant::SYSTEM_REPORT_CONT11:
            case ReportConstant::SYSTEM_REPORT_CONT_CLI_NBS01:
            case ReportConstant::SYSTEM_REPORT_CONT20:
                $report->setRepFilterable(true);
                $report->setRepFilterContractInspection(true);
                break;

            case ReportConstant::SYSTEM_REPORT_CONT12:
            case ReportConstant::SYSTEM_REPORT_CONT18:
                $report->setRepFilterable(true);
                $report->setRepFilterContractAFP(true);
                break;

            case ReportConstant::SYSTEM_REPORT_CONT13:
                $report->setRepFilterable(true);
                $report->setRepFilterContractCertificate(true);
                break;

            case ReportConstant::SYSTEM_REPORT_CONT14:
                $report->setRepFilterable(true);
                $report->setRepFilterContractCreditApplication(true);
                break;

            case ReportConstant::SYSTEM_REPORT_CONT15:
                $report->setRepFilterable(true);
                $report->setRepFilterContractInvoiceApplication(true);
                break;

            case ReportConstant::SYSTEM_REPORT_CONT16:
                $report->setRepFilterable(true);
                $report->setRepFilterContractInvoiceCreditApplication(true);
                break;
            /**
             * Utility Module
             */
            case ReportConstant::SYSTEM_REPORT_UT01:
            case ReportConstant::SYSTEM_REPORT_UT02:
                $report->setRepFilterable(true);
                $report->setRepFilterCodeDescActive(true);
                break;

            case ReportConstant::SYSTEM_REPORT_UT04:
                $report->setRepFilterable(true);
                $report->setRepFilterUtilityRecord(true);
                break;

            case ReportConstant::SYSTEM_REPORT_UT05:
            case ReportConstant::SYSTEM_REPORT_UT06:
                $report->setRepFilterable(true);
                $report->setRepFilterUtilityBill(true);
                break;
            case ReportConstant::SYSTEM_REPORT_UT09:
            case ReportConstant::SYSTEM_REPORT_UT10:
                $report->setRepFilterable(true);
                break;

            /**
             * Cost Plus
             */

            case ReportConstant::SYSTEM_REPORT_CPC01:
                $report->setRepFilterable(true);
                $report->setIgnoreSiteId(true);
                $report->setRepFilterCostPlusContract(true);
                break;
            case ReportConstant::SYSTEM_REPORT_CPC02:
            case ReportConstant::SYSTEM_REPORT_CPC06:
            case ReportConstant::SYSTEM_REPORT_CPC07:
            case ReportConstant::SYSTEM_REPORT_CPC08:
            case ReportConstant::SYSTEM_REPORT_CPC09:
                $report->setRepFilterable(true);
                $report->setRepFilterCostPlusContract(true);
                break;
            case ReportConstant::SYSTEM_REPORT_CPC03:
                $report->setRepFilterable(true);
                $report->setRepFilterCostPlusSalesInvoice(true);
                break;

            case ReportConstant::SYSTEM_REPORT_CPC04:
                $report->setRepFilterWkhtmltopdf(ReportConstant::SYSTEM_REPORT_CPC04);
                break;

            case ReportConstant::SYSTEM_REPORT_CPC05:
                $report->setRepFilterWkhtmltopdf(ReportConstant::SYSTEM_REPORT_CPC05);
                break;
            case ReportConstant::SYSTEM_REPORT_CPC10:
                $report->setRepFilterable(true);
                $report->setRepFilterCostPlusSalesCredit(true);
                break;

            /**
             * Wkhtmltopdf Report
             */
            // Questionnaire reports for Sunderland.
            case ReportConstant::SYSTEM_REPORT_QST_CLI_SUN02:
                $report->setRepFilterWkhtmltopdf(ReportConstant::SYSTEM_REPORT_QST_CLI_SUN02);
                break;
            case ReportConstant::SYSTEM_REPORT_QST_CLI_SUN03:
                $report->setRepFilterWkhtmltopdf(ReportConstant::SYSTEM_REPORT_QST_CLI_SUN03);
                break;
            case ReportConstant::SYSTEM_REPORT_QST_CLI_SUN04:
                $report->setRepFilterWkhtmltopdf(ReportConstant::SYSTEM_REPORT_QST_CLI_SUN04);
                break;

            // Property reports for Sunderland.
            case ReportConstant::SYSTEM_REPORT_CND_CLI_SUN01:
                $report->setRepFilterWkhtmltopdf(ReportConstant::SYSTEM_REPORT_CND_CLI_SUN01);
                break;

            case ReportConstant::SYSTEM_REPORT_CND_CLI_TORB02:
                $report->setRepFilterWkhtmltopdf(ReportConstant::SYSTEM_REPORT_CND_CLI_TORB02);
                break;

            //Condition Report for Fife
            case ReportConstant::SYSTEM_REPORT_CND_CLI_FIF01:
                $report->setRepFilterWkhtmltopdf(ReportConstant::SYSTEM_REPORT_CND_CLI_FIF01);
                break;

            //Condition report for Scottish Borders
            case ReportConstant::SYSTEM_REPORT_CND_CLI_SB01:
                $report->setRepFilterWkhtmltopdf(ReportConstant::SYSTEM_REPORT_CND_CLI_SB01);
                break;

            //Condition report for Milton Keynes
            case ReportConstant::SYSTEM_REPORT_CND_CLI_MK01:
                $report->setRepFilterWkhtmltopdf(ReportConstant::SYSTEM_REPORT_CND_CLI_MK01);
                break;

            //Condition Report for NE Lincs
            case ReportConstant::SYSTEM_REPORT_CND_CLI_NEL01:
                $report->setRepFilterWkhtmltopdf(ReportConstant::SYSTEM_REPORT_CND_CLI_NEL01);
                break;

            case ReportConstant::SYSTEM_REPORT_CND_CLI_WOK01:
                $report->setRepFilterable(true);
                $report->setRepLocMax(1);
                $report->setRepFilterWkhtmltopdf(ReportConstant::SYSTEM_REPORT_CND_CLI_WOK01);
                break;

            case ReportConstant::SYSTEM_REPORT_PL02:
                $report->setRepFilterable(true);
                $report->setRepLocMax(1);
                $report->setRepFilterProject(ReportConstant::SYSTEM_REPORT_PL02);
                break;
            case ReportConstant::SYSTEM_REPORT_PPRJ02:
                // Project Actions with notes
                $report->setRepFilterable(true);
                $report->setRepLocMax(1);
                $report->setRepFilterProject(ReportConstant::SYSTEM_REPORT_PPRJ02);
                break;
            //Project Monitoring
            case ReportConstant::SYSTEM_REPORT_PRJ11:
                $report->setRepFilterable(true);
                $report->setRepLocMax(1);
                $report->setRepFilterWkhtmltopdf(ReportConstant::SYSTEM_REPORT_PRJ11);
                break;
            case ReportConstant::SYSTEM_REPORT_PRJ12:
                $report->setRepFilterable(true);
                $report->setRepLocMax(1);
                $report->setRepFilterWkhtmltopdf(ReportConstant::SYSTEM_REPORT_PRJ12);
                break;
            case ReportConstant::SYSTEM_REPORT_PRJ13:
                $report->setRepFilterWkhtmltopdf(ReportConstant::SYSTEM_REPORT_PRJ13);
                break;
            case ReportConstant::SYSTEM_REPORT_ES_CLI_SUN01:
            case ReportConstant::SYSTEM_REPORT_ES_CLI_SUN02:
                $report->setRepFilterable(true);
                $report->setRepFilterExcel(true);
                break;
            case ReportConstant::SYSTEM_REPORT_INC01:
                // INC01: Incident Records
                $report->setRepFilterable(true);
                $report->setRepFilterIncidentRecords(true);
                break;
            case ReportConstant::SYSTEM_REPORT_INC02:
                // INC02: Incident Action
                $report->setRepFilterable(true);
                $report->setRepFilterIncidentAction(true);
                break;
            case ReportConstant::SYSTEM_REPORT_CND15:
            case ReportConstant::SYSTEM_REPORT_CND16:
            case ReportConstant::SYSTEM_REPORT_CND17:
            case ReportConstant::SYSTEM_REPORT_CND18:
            case ReportConstant::SYSTEM_REPORT_CND19:
            case ReportConstant::SYSTEM_REPORT_CND20:
            case ReportConstant::SYSTEM_REPORT_CND21:
            case ReportConstant::SYSTEM_REPORT_CND22:
            case ReportConstant::SYSTEM_REPORT_CND23:
                $report->setRepFilterable(true);
                $report->setRepLocMax(1);
                $report->setRepFilterWkhtmltopdf($reportCode);
                break;

            case ReportConstant::SYSTEM_REPORT_DF01:
                // DF01: Datafeed
                $report->setRepFilterable(true);
                $report->setUseSiteGroupId(false);
                $report->setRepFilterDataFeed(true);
                break;
            case ReportConstant::SYSTEM_REPORT_PERS03:
                $report->setRepFilterable(true);
                $report->setRepFilterPers03(true);
                $report->setIgnoreSiteId(true);
                break;
            case ReportConstant::SYSTEM_REPORT_PERS01:
                $report->setRepFilterWkhtmltopdf(ReportConstant::SYSTEM_REPORT_PERS01);
                break;
            case ReportConstant::SYSTEM_REPORT_PERS02:
                $report->setRepFilterWkhtmltopdf(ReportConstant::SYSTEM_REPORT_PERS02);
                break;
            default:
                break;
        }

        $this->report = $report;
    }

    private function getSystemReportQuery($filter, $iFavouriteReports)
    {
        $systemReportQuery = $this->getBaseSystemReportQuery();

        switch ($iFavouriteReports) {
            case self::REP_FAV_OPT_ALL:
                $systemReportQuery->leftjoin('report_favourite', function ($join) {
                    $join->on('report_favourite.system_report_id', '=', 'system_report.system_report_id')
                        ->on('report_favourite.user_id', '=', \DB::raw(\Auth::user()->getKey()));
                });
                break;
            case self::REP_FAV_OPT_YES:
                $systemReportQuery->join(
                    'report_favourite',
                    'report_favourite.system_report_id',
                    '=',
                    'system_report.system_report_id'
                )->where('report_favourite.user_id', '=', \DB::raw(\Auth::user()->getKey()));
                break;
            case self::REP_FAV_OPT_NO:
                $systemReportQuery->leftjoin('report_favourite', function ($join) {
                    $join->on('report_favourite.system_report_id', '=', 'system_report.system_report_id')
                        ->on('report_favourite.user_id', '=', \DB::raw(\Auth::user()->getKey()));
                })->whereNull('report_favourite.report_favourite_id');
                break;
            default:
                break;
        }

        if (!is_null($filter->title)) {
            $systemReportQuery->where('system_report.system_report_desc', 'like', '%' . $filter->title . '%');
        }

        if (!\Auth::user()->isAdministrator()) {
            $systemReportQuery->where('system_report.admin_only', '=', CommonConstant::DATABASE_VALUE_NO);
        }

        if (!is_null($filter->module)) {
            $systemReportQuery->where(function ($subQuery) use ($filter) {
                $subQuery->where('module.module_id', $filter->module)
                    ->orWhere('module_2.module_id', $filter->module);
            });
        }

        if (!is_null($filter->confidential)) {
            if (!$filter->confidential) {
                $systemReportQuery->whereNotIn('system_report.system_report_code', ['PR35','PR36','PR37']);
            }
        }

        if (!is_null($filter->stage_package_task_gantt)) {
            if (!$filter->stage_package_task_gantt) {
                $systemReportQuery->whereNotIn(
                    'system_report.system_report_code',
                    ['PRJ07', 'PRJ14', 'PRJ22', 'PRJ23']
                );
            }
        }

        if (!is_null($filter->finance)) {
            if (!$filter->finance) {
                $systemReportQuery->whereNotIn(
                    'system_report.system_report_code',
                    ['PRJ08', 'PRJ09', 'PRJ10', 'PRJ11', 'PRJ16', 'PRJ18', 'PRJ20', 'PRJ21', 'PRJ24']
                );
            }
        }

        if (!is_null($filter->code)) {
            $systemReportQuery->where('system_report.system_report_code', 'like', '%' . $filter->code . '%');
        }

        if (\Auth::user()->siteGroup->dlo_labour_hours != CommonConstant::DATABASE_VALUE_YES) {
            $systemReportQuery->whereNotIn(
                'system_report.system_report_code',
                [ReportConstant::SYSTEM_REPORT_DLO15, ReportConstant::SYSTEM_REPORT_DLO16]
            );
        }

        if (EstatePermission::moduleAccessMode(EstatePermission::RESTRICTIVE_MODULE_ACCESS)) {
            $systemReportQuery->where(function ($query) {
                $query->where('system_report.system_report_code', 'not like', 'ES%')
                    ->orWhereIn('system_report.system_report_code', $this->estRestrictedModeRpts);
            });
        }

        if (!\Auth::user()->siteGroup->turnOnProjectSpecificFields()) {
            $systemReportQuery->whereNotIn(
                'system_report.system_report_code',
                [ReportConstant::SYSTEM_REPORT_PRJ19]
            );
        }

        if (!\Auth::user()->siteGroup->turnOnSiteSpecificFields()) {
            $systemReportQuery->whereNotIn(
                'system_report.system_report_code',
                [ReportConstant::SYSTEM_REPORT_PR32]
            );
        }

        if (!\Auth::user()->siteGroup->turnOnPropBuildingSpecificFields()) {
            $systemReportQuery->whereNotIn(
                'system_report.system_report_code',
                [ReportConstant::SYSTEM_REPORT_PR42]
            );
        }

        if (!\Auth::user()->siteGroup->enableEstateRentalPayment()) {
            $systemReportQuery->whereNotIn(
                'system_report.system_report_code',
                [
                    ReportConstant::SYSTEM_REPORT_ES53,
                    ReportConstant::SYSTEM_REPORT_ES54,
                    ReportConstant::SYSTEM_REPORT_ES55,
                    ReportConstant::SYSTEM_REPORT_ES56,
                    ReportConstant::SYSTEM_REPORT_BUD04,
                    ReportConstant::SYSTEM_REPORT_BUD05,
                ]
            );
        }

        if (!Common::valueYorNtoBoolean(\Auth::user()->siteGroup->dlo_quoted_job)) {
            $systemReportQuery->whereNotIn(
                'system_report.system_report_code',
                [ReportConstant::SYSTEM_REPORT_DLO24]
            );
        }

        // These reports are accessible from the Fixed Asset page only.
        $systemReportQuery->whereNotIn(
            'system_report.system_report_code',
            [
                ReportConstant::SYSTEM_REPORT_FAR38,
                ReportConstant::SYSTEM_REPORT_FAR39
            ]
        );


        return $systemReportQuery;
    }

    private function getUserReportQuery($filter, $iFavouriteReports)
    {
        $userReportQuery = $this->getBaseUserReportQuery();

        switch ($iFavouriteReports) {
            case self::REP_FAV_OPT_ALL:
                $userReportQuery->leftjoin('report_favourite', function ($join) {
                    $join->on('report_favourite.user_report_id', '=', 'user_report.user_report_id')
                        ->on('report_favourite.user_id', '=', \DB::raw(\Auth::user()->getKey()));
                });
                break;
            case self::REP_FAV_OPT_YES:
                $userReportQuery->join(
                    'report_favourite',
                    'report_favourite.user_report_id',
                    '=',
                    'user_report.user_report_id'
                )->where('report_favourite.user_id', '=', \DB::raw(\Auth::user()->getKey()));
                break;
            case self::REP_FAV_OPT_NO:
                $userReportQuery->leftjoin('report_favourite', function ($join) {
                    $join->on('report_favourite.user_report_id', '=', 'user_report.user_report_id')
                        ->on('report_favourite.user_id', '=', \DB::raw(\Auth::user()->getKey()));
                })->whereNull('report_favourite.report_favourite_id');
                break;
            default:
                break;
        }

        if (!(\Auth::user()->site_access === UserSiteAccess::USER_SITE_ACCESS_ALL)) {
            $userReportQuery->where("user_report.user_report_type_id", "<>", UserReportType::USER_REPORT_TYPE_QUERY);
        } else {
            if (!is_null($filter->type)) {
                switch ($filter->type) {
                    case self::REP_TYPE_FILTER_USERDEFINED_STD:
                        $userReportQuery->where(
                            "user_report.user_report_type_id",
                            UserReportType::USER_REPORT_TYPE_STANDARD
                        );
                        break;
                    case self::REP_TYPE_FILTER_USERDEFINED_PDF:
                        $userReportQuery->where(
                            "user_report.user_report_type_id",
                            UserReportType::USER_REPORT_TYPE_PDF
                        );
                        break;
                    case self::REP_TYPE_FILTER_USERDEFINED_QRY:
                        $userReportQuery->where(
                            "user_report.user_report_type_id",
                            UserReportType::USER_REPORT_TYPE_QUERY
                        );
                        break;
                }
            }
        }

        if (!is_null($filter->creator)) {
            $userReportQuery->where('user_report.creator_user_id', $filter->creator);
        }


        if (!is_null($filter->title)) {
            $userReportQuery->where('user_report.user_report_desc', 'like', '%' . $filter->title . '%');
        }

        if (!is_null($filter->module)) {
            $userReportQuery->where(function ($subQuery) use ($filter) {
                $subQuery->where('module.module_id', $filter->module)
                    ->orWhere('module_2.module_id', $filter->module);
            });
        }

        if (!is_null($filter->code)) {
            $userReportQuery->where('user_report.user_report_code', 'like', '%' . $filter->code . '%');
        }

        return $userReportQuery;
    }

    private function reGetReportTypes(&$bSystemReports, &$bUserReports, &$iFavOpt, $inputs)
    {
        $repType = isset($inputs['type']) ? $inputs['type'] : '';
        switch ($repType) {
            case self::REP_TYPE_FILTER_SYSTEM:
                $bSystemReports = true;
                break;

            case self::REP_TYPE_FILTER_USERDEFINED_ALL:
            case self::REP_TYPE_FILTER_USERDEFINED_STD:
            case self::REP_TYPE_FILTER_USERDEFINED_PDF:
            case self::REP_TYPE_FILTER_USERDEFINED_QRY:
                $bUserReports = true;
                break;

            case self::REP_TYPE_ALL:
            case "":
            default:
                $bSystemReports = true;
                $bUserReports = true;
                break;
        }

        // CLD-13685 If filtering by Creator then don't show any system reports.
        $byCreator = array_get($inputs, 'creator', false);

        if ($byCreator) {
            $bSystemReports = false;
        }

        $favOpt = isset($inputs['fav']) ? $inputs['fav'] : '';
        switch ($favOpt) {
            case CommonConstant::DATABASE_VALUE_YES:
                $iFavOpt = self::REP_FAV_OPT_YES;
                break;
            case CommonConstant::DATABASE_VALUE_NO:
                $iFavOpt = self::REP_FAV_OPT_NO;
                break;
            default:
                $iFavOpt = self::REP_FAV_OPT_ALL;
                break;
        }
    }

    /**
     * Is a System Report a CSV Report?
     * @param type $id
     * @return boolean
     */
    public static function isCsvReport($systemReportId)
    {
        $systemReportCode = self::getSystemReportCode($systemReportId);

        if (in_array($systemReportCode, self::getWkHtmlToPdfWhiteList())) {
            // PDF Reports
            return false;
        }

        if (in_array($systemReportCode, self::getExcelWhiteList())) {
            // Non-CSV Reports
            return false;
        }

        // Assumes it is a CSV Report at this point
        return true;
    }

    public static function getSystemReportCode($systemReportId)
    {
        $report = SystemReport::where(
            'system_report_id',
            '=',
            $systemReportId
        )
        ->first();

        return $report->system_report_code;
    }

    public static function getSystemReportByCode($systemReportCode)
    {
        $report = SystemReport::where(
            'system_report_code',
            '=',
            $systemReportCode
        );

        return $report->first();
    }

    private function getReportModel($repType, $id)
    {
        if ($repType == Report::USER_REPORT) {
            $model = UserReport::findOrFail($id);
        } elseif ($repType == Report::SYSTEM_REPORT) {
            $model = SystemReport::findOrFail($id);
        }

        return $model ?? null;
    }

    private function checkPropertyStrategyPermission($systemReportCode)
    {
        if (!\Auth::user()->strategyAccessible() && $systemReportCode == ReportConstant::SYSTEM_REPORT_PR28) {
            throw new PermissionException(
                'Report PR28 not available to this user.'
            );
        }
    }

    public function getBaseSystemReportQuery()
    {
        $systemReportQuery = \DB::table('system_report')->select([
            "system_report.system_report_id AS system_report_id",
            \DB::raw("'' AS user_report_id"),
            "system_report.module_id AS module_id",
            "system_report.req_2_module_id AS module_id_2",
            \DB::raw(
                "CONCAT_WS(' - ', module.module_description, IFNULL(module_2.module_description, NULL))"
                . " AS module_description"
            ),
            "system_report.system_report_code AS report_code",
            "system_report.system_report_desc AS report_desc",
            "system_report.further_desc AS further_desc",
            \DB::raw("'Y' AS available"),
            \DB::raw("'' AS creator_userid"),
            \DB::raw("'' AS creator_username"),
            \DB::raw("'Standard' AS running_type"),
            "report_favourite.report_favourite_id AS report_favourite_id",
            "system_report.customizable AS customizable",
            "system_report.superuser_only AS superuser_only",
            \DB::raw("'N' AS use_query"),
            \DB::raw("'' AS query")
        ]);

        if (\Auth::user()->siteGroup->dlo_labour_hours != CommonConstant::DATABASE_VALUE_YES) {
            $systemReportQuery->whereNotIn(
                'system_report.system_report_code',
                [ReportConstant::SYSTEM_REPORT_DLO15, ReportConstant::SYSTEM_REPORT_DLO16]
            );
        }

        if (EstatePermission::moduleAccessMode(EstatePermission::RESTRICTIVE_MODULE_ACCESS)) {
            $systemReportQuery->where(function ($query) {
                $query->where('system_report.system_report_code', 'not like', 'ES%')
                    ->orWhereIn('system_report.system_report_code', $this->estRestrictedModeRpts);
            });
        }

        if (!\Auth::user()->siteGroup->turnOnProjectSpecificFields()) {
            $systemReportQuery->whereNotIn(
                'system_report.system_report_code',
                [ReportConstant::SYSTEM_REPORT_PRJ19]
            );
        }

        if (!\Auth::user()->siteGroup->turnOnSiteSpecificFields()) {
            $systemReportQuery->whereNotIn(
                'system_report.system_report_code',
                [ReportConstant::SYSTEM_REPORT_PR32, ReportConstant::SYSTEM_REPORT_PR42]
            );
        }

        if (!\Auth::user()->siteGroup->enableEstateRentalPayment()) {
            $systemReportQuery->whereNotIn(
                'system_report.system_report_code',
                [
                    ReportConstant::SYSTEM_REPORT_ES53,
                    ReportConstant::SYSTEM_REPORT_ES54,
                    ReportConstant::SYSTEM_REPORT_ES55,
                    ReportConstant::SYSTEM_REPORT_ES56,
                    ReportConstant::SYSTEM_REPORT_BUD04,
                    ReportConstant::SYSTEM_REPORT_BUD05,
                ]
            );
        }

        if (!Common::valueYorNtoBoolean(\Auth::user()->siteGroup->dlo_quoted_job)) {
            $systemReportQuery->whereNotIn(
                'system_report.system_report_code',
                [ReportConstant::SYSTEM_REPORT_DLO24]
            );
        }

        // These reports are accessible from the Fixed Asset page only.
        $systemReportQuery->whereNotIn(
            'system_report.system_report_code',
            [
                ReportConstant::SYSTEM_REPORT_FAR38,
                ReportConstant::SYSTEM_REPORT_FAR39
            ]
        );

        // #404835: Self Service users with read on reports, shouldn't see general reports.
        if (\Auth::user()->isSelfServiceUser()) {
            $systemReportQuery->whereNotIn(
                'system_report.module_id',
                [
                    Module::MODULE_GENERAL
                ]
            );
        }

        return $systemReportQuery;
    }

    public function getBaseUserReportQuery()
    {
        $userReportQuery = \DB::table('user_report')->select([
             "user_report.system_report_id AS system_report_id",
             "user_report.user_report_id AS user_report_id",
             "system_report.module_id",
             "system_report.req_2_module_id AS module_id_2",
            \DB::raw(
                "CONCAT_WS(' - ', module.module_description, IFNULL(module_2.module_description, NULL))"
                . " AS module_description"
            ),
            "user_report.user_report_code AS report_code",
            "user_report.user_report_desc AS report_desc",
            "user_report.comment AS further_desc",
            "available AS available",
            "user_report.creator_user_id AS creator_userid",
            "report_user.display_name AS creator_username",
            "user_report_type.user_report_type_code AS running_type",
            "report_favourite.report_favourite_id AS report_favourite_id",
            "system_report.customizable AS customizable",
            "system_report.superuser_only AS superuser_only",
            \DB::raw("case when user_report.user_report_type_id = " . UserReportType::USER_REPORT_TYPE_QUERY
                . " then 'Y' else 'N' end as use_query"),
            "user_report.query AS query"
        ])
            ->join("system_report", "user_report.system_report_id", "=", "system_report.system_report_id")
            ->join("user AS report_user", "user_report.creator_user_id", "=", "report_user.id")
            ->join(
                "user_report_type AS user_report_type",
                "user_report_type.user_report_type_id",
                "=",
                "user_report.user_report_type_id"
            );


        if (
            !$this->permissionService->hasModuleAccess(
                Module::MODULE_REPORTS,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_SUPERVISOR
            )
        ) {
            $userReportQuery->where(function ($subQuery) {
                $subQuery->where('user_report.available', '=', 'Y')
                    ->orWhere('user_report.creator_user_id', '=', \DB::raw(\Auth::user()->getKey()));
            });
        }

        $userReportQuery->where('user_report.site_group_id', '=', \Auth::user()->site_group_id);

        // #404835: Self Service users with read on reports, shouldn't see general reports.
        if (\Auth::user()->isSelfServiceUser()) {
            $userReportQuery->whereNotIn(
                'system_report.module_id',
                [
                    Module::MODULE_GENERAL
                ]
            );
        }

        return $userReportQuery;
    }
}
