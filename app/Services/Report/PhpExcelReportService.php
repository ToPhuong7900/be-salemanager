<?php

namespace Tfcloud\Services\Report;

use Tfcloud\Lib\Constant\ReportConstant;
use Tfcloud\Lib\Exceptions\PermissionException;
use Tfcloud\Models\AuditAction;
use Tfcloud\Models\CrudAccessLevel;
use Tfcloud\Models\Module;
use Tfcloud\Models\Report;
use Tfcloud\Services\AuditService;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\Education\SuitCFSurveyService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\PhpExcel\ESCLISUN01EstatesReviewService;
use Tfcloud\services\Report\PhpExcel\OperativeAvailableReportService;
use Tfcloud\Services\Report\PhpExcel\PRJ19ProjectSpecificCustomDataReportService;
use Tfcloud\Services\Report\PhpExcel\ProjectRiskRegisterReportService;
use Tfcloud\Services\Report\PhpExcel\Property\PR42BuildingSpecificCustomDataReportService;
use Tfcloud\Services\Report\PhpExcel\RB07ReportService;
use Tfcloud\Services\Report\PhpExcel\Property\PR32SiteSpecificCustomDataReportService;

class PhpExcelReportService extends BaseService
{
    public function __construct(PermissionService $permissionService, ReportService $reportService)
    {
        $this->permissionService = $permissionService;
        $this->reportService = $reportService;
    }

    public function reOutputPhpExcelData($inputs)
    {
        if (
            !$this->permissionService->hasModuleAccess(
                Module::MODULE_REPORTS,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
            )
        ) {
            $msg = "Not met minimum readonly access for report module";
            throw new PermissionException($msg);
        }


        $repId          = array_get($inputs, 'repId', null);
        $repType        = array_get($inputs, 'repType', null);

        $report = $this->reportService->get($repType, $repId);
        $returnData = [
            'result' => false,
            'repFile' => '',
            'repCode' => $report->getRepCode(),
            'repTitle' => $report->getRepTitle(),
            'repFormat' => ReportConstant::REPORT_OUTPUT_XLSX_FORMAT,
            'repFileName' => '',
            'repFilePath' => '',
        ];
        if (!$report instanceof Report) {
            return $returnData;
        }

        if ($repDownloadPath = array_get($inputs, 'repDownloadPath')) {
            $reportDir = $repDownloadPath;
            if (!is_dir($reportDir)) {
                return $returnData;
            }
        } else {
            // Create a reports directory for the site group
            $reportDir = storage_path()
                . DIRECTORY_SEPARATOR . 'reports'
                . DIRECTORY_SEPARATOR . \Auth::User()->site_group_id;
            if (!is_dir($reportDir)) {
                mkdir($reportDir, 0755);
            }
            // Create a reports directory for the userid
            $reportDir .= DIRECTORY_SEPARATOR . \Auth::User()->id;
            if (!is_dir($reportDir)) {
                mkdir($reportDir, 0755);
            }
        }


        $reportCode = $report->getRepCode();
        // Default format
        $reportFileName = $reportCode . '.xlsx';
        $repExcelFile = $reportDir . DIRECTORY_SEPARATOR . $reportFileName;

        $returnData['repFile'] = url("report/reports/{$reportCode}/download");
        //$returnData['repFile'] = \Route('reports.download', ['repCode' => $reportCode]);

        set_time_limit(1200);

        switch ($report->getRepCode()) {
            // Suit Core Facts
            case ReportConstant::SYSTEM_REPORT_SCF01:
                $suitCFSurveyService = new SuitCFSurveyService($this->permissionService);
                $reportService = new PhpExcel\SCF01SuitCoreFactsSurveyService(
                    $this->permissionService,
                    $suitCFSurveyService
                );
                $reportService->runReportWithValidate($repExcelFile, $inputs, $repId, $returnData);
                break;
            // Compliance
            case ReportConstant::SYSTEM_REPORT_PR21:
                $reportService = new PhpExcel\PR21ComplianceService($this->permissionService, $report);

                if (
                    array_get($inputs, 'exportHTML', false) ||
                    ((array_get($inputs, 'fileExtension', false)) && $inputs['fileExtension'] === 'html') ||
                    ((array_key_exists('exportAs', $inputs)) && array_get($inputs['exportAs'], 'label') === 'HTML')
                ) {
                    $reportFileName = $reportCode . '.html';
                    $returnData['repFormat'] = 'html';
                } else {
                    $reportFileName = $reportCode . '.xlsx';
                }

                $repExcelFile = $reportDir . DIRECTORY_SEPARATOR . $reportFileName;

                \Log::error('rep directory is:');
                \Log::error($repExcelFile);

                $reportService->runReportWithValidate($repExcelFile, $inputs, $repId, $returnData);
                break;
            case ReportConstant::SYSTEM_REPORT_INS_CLI_WAK02:
                $reportService = new PhpExcel\INSCLIWAK02ComplianceService($this->permissionService, $report);

                if (
                    array_get($inputs, 'exportHTML', false) ||
                    ((array_get($inputs, 'fileExtension', false)) && $inputs['fileExtension'] === 'html') ||
                    ((array_key_exists('exportAs', $inputs)) && array_get($inputs['exportAs'], 'label') === 'HTML')
                ) {
                    $reportFileName = $reportCode . '.html';
                    $returnData['repFormat'] = 'html';
                } else {
                    $reportFileName = $reportCode . '.xlsx';
                }

                $repExcelFile = $reportDir . DIRECTORY_SEPARATOR . $reportFileName;

                \Log::error('rep directory is:');
                \Log::error($repExcelFile);

                $reportService->runReportWithValidate($repExcelFile, $inputs, $repId, $returnData);
                break;
            case ReportConstant::SYSTEM_REPORT_CND_CLI_CFF01:
                $reportService = new PhpExcel\CNDCLICFF01ElementService($this->permissionService, $report);

                if (
                    array_get($inputs, 'exportHTML', false) ||
                    ((array_get($inputs, 'fileExtension', false)) && $inputs['fileExtension'] === 'html') ||
                    ((array_key_exists('exportAs', $inputs)) && array_get($inputs['exportAs'], 'label') === 'HTML')
                ) {
                    $reportFileName = $reportCode . '.html';
                    $returnData['repFormat'] = 'html';
                } else {
                    $reportFileName = $reportCode . '.xlsx';
                }

                $repExcelFile = $reportDir . DIRECTORY_SEPARATOR . $reportFileName;

                $reportService->runReportWithValidate($repExcelFile, $inputs, $repId, $returnData);
                break;

            case ReportConstant::SYSTEM_REPORT_RB07:
                $reportService = new RB07ReportService($this->permissionService);
                $reportFileName = $reportCode . '.' . ReportConstant::REPORT_OUTPUT_XLSX_FORMAT;
                $repExcelFile = $reportDir . DIRECTORY_SEPARATOR . $reportFileName;
                $reportService->runReportWithValidate($repExcelFile, $inputs, $repId, $returnData);
                break;
            case ReportConstant::SYSTEM_REPORT_PRJ17:
                $reportService = new ProjectRiskRegisterReportService(
                    $this->permissionService
                );
                $reportService->runReportWithValidate($repExcelFile, $inputs, $repId, $returnData);
                break;
            case ReportConstant::SYSTEM_REPORT_DLO21:
                $reportService = new OperativeAvailableReportService(
                    $this->permissionService,
                    $report
                );
                $reportService->runReportWithValidate($repExcelFile, $inputs, $repId, $returnData);
                break;
            case ReportConstant::SYSTEM_REPORT_PRJ19:
                $reportService = new PRJ19ProjectSpecificCustomDataReportService($this->permissionService, $report);
                $reportService->runReportWithValidate($repExcelFile, $inputs, $repId, $returnData);
                break;
            case ReportConstant::SYSTEM_REPORT_PR32:
                $reportService = new PR32SiteSpecificCustomDataReportService($this->permissionService, $report);
                $reportService->runReportWithValidate($repExcelFile, $inputs, $repId, $returnData);
                break;
            case ReportConstant::SYSTEM_REPORT_PR42:
                $reportService = new PR42BuildingSpecificCustomDataReportService($this->permissionService, $report);
                $reportService->runReportWithValidate($repExcelFile, $inputs, $repId, $returnData);
                break;
            case ReportConstant::SYSTEM_REPORT_ES_CLI_SUN01:
                $reportService = new PhpExcel\ESCLISUN01EstatesReviewService(
                    $this->permissionService,
                    $report
                );
                $reportService->runReportWithValidate($repExcelFile, $inputs, $repId, $returnData);
                break;
            default:
                \Log::error("Unknown Report");
                break;
        }

        if ($returnData['result']) {
            ReportGenerateService::setRecentReport($repType, $repId);
            $returnData['repFileName'] = $reportFileName;
            $returnData['repFilePath'] = $repExcelFile;

            // add entry to audit log
            $auditService = new AuditService();
            if ($returnData['filterText']) {
                $auditService->auditText = "Filtering details: " . $returnData['filterText'];
            }
            $auditService->addAuditEntry($report, AuditAction::ACT_REPORT);
        }


        return $returnData;
    }
}
