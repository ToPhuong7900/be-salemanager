<?php

namespace Tfcloud\Services\Report;

use Auth;
use DateTime;
use DB;
use Illuminate\Support\MessageBag;
use Input;
use Tfcloud\Lib\AuditFields;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Constant\ReportConstant;
use Tfcloud\Lib\Exceptions\InvalidRecordException;
use Tfcloud\Lib\Exceptions\PermissionException;
use Tfcloud\Lib\Exceptions\StateException;
use Tfcloud\Lib\Filters\UserReportFilterFilter;
use Tfcloud\Models\AuditAction;
use Tfcloud\Models\CrudAccessLevel;
use Tfcloud\Models\GenUserdefLabel;
use Tfcloud\Models\IdControl;
use Tfcloud\Models\Module;
use Tfcloud\Models\ReportDateOption;
use Tfcloud\Models\ReportField;
use Tfcloud\Models\ReportFieldType;
use Tfcloud\Models\ReportOperatorType;
use Tfcloud\Models\SystemReport;
use Tfcloud\Models\UserReport;
use Tfcloud\Models\UserReportField;
use Tfcloud\Models\UserReportFilter;
use Tfcloud\Models\UserReportPdfDesign;
use Tfcloud\Models\UserReportType;
use Tfcloud\Services\AuditService;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\PermissionService;
use Validator;

class UserReportService extends BaseService
{
    /*
     * Tfcloud\Models\UserReport
     */
    protected $userReport = null;
    protected $userReportField = null;

    /*
     * Tfcloud\Models\SystemReport
     */
    protected $systemReport = null;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
        $this->userReportPdfDesignService = new UserReportPdfDesignService($this->permissionService, $this);

        \Validator::extend('validate_existed_ordering', function ($field, $value, $params, $validator) {
            if (!count($params)) {
                throw new \InvalidArgumentException(
                    'The service validation rule expects at least one parameter, 0 given.'
                );
            }

            $inputs = $validator->getData();
            return $this->checkExistedOrdering($value, $params, $inputs);
        });
    }

    public function newUserReport($systemReportId, $userReport = null)
    {
        $this->userReport = new UserReport();
        $this->systemReport = SystemReport::find($systemReportId);
        $this->initDefaultData($systemReportId, $userReport);

        return $this->userReport;
    }

    public function getAll($inputs)
    {
        if (
            !$this->permissionService->hasModuleAccess(
                Module::MODULE_REPORTS,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
            )
        ) {
            $msg = "Not met minimum readonly access for report module in class" .
                " (" . __CLASS__ . ") method (" . __METHOD__ . "). " .
                " See line (" . __LINE__ . ")";
            throw new PermissionException($msg);
        }

        $query = $this->all();
        $this->filterAll($query, $inputs);

        return $query;
    }

    public static function filterAll(&$query, $inputs)
    {
        return true;
    }

    public function get($id, $audit = true)
    {
        if (
            !$this->permissionService->hasModuleAccess(
                Module::MODULE_REPORTS,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
            )
        ) {
            $msg = "Not met minimum readonly access for report module";
            throw new PermissionException($msg);
        }

        $this->userReport = UserReport::userSiteGroup()
            ->where('user_report_id', $id)
            ->first();

        if (is_null($this->userReport)) {
            throw new InvalidRecordException("No User Report found");
        }

        if ($audit) {
            $auditService = new AuditService();
            $auditService->addAuditEntry($this->userReport, AuditAction::ACT_VIEW);
        }

        return $this->userReport;
    }

    public function addUserReport(&$id, $inputs, $userReport = null)
    {
        if (
            !$this->permissionService->hasModuleAccess(
                Module::MODULE_REPORTS,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_CREATE
            )
        ) {
            $msg = "Not met minimum create new access for report module";
            throw new PermissionException($msg);
        }

        $systemReportId = isset($inputs['system_report_id']) ? $inputs['system_report_id'] : null;
        $options = [];

        $this->newUserReport($systemReportId, $userReport);
        $this->setModelFields($inputs);

        $validator = $this->getUserReportValidator();

        if ($validator->passes()) {
            DB::transaction(function () use ($inputs, $userReport, &$validator, &$options) {
                // Generate user report code.
                if (
                    !$this->userReport->user_report_code
                    || $this->userReport->user_report_code == CommonConstant::CREATE_NEW_CODE
                ) {
                    $this->userReport->user_report_code = IdControl::getNextCode(Module::MODULE_REPORTS, 'user_report');
                }

                $now = new DateTime();
                $this->userReport->create_date = $now->format('Y-m-d H:i:s');

                $this->userReport->save();

                $validator->success = true;

                if (array_key_exists('new_version', $inputs)) {
                    $this->reudInsertUserReportFilters($this->userReport->user_report_id, $inputs['base_on_report_id']);
                    $validator = $this->cloneUserReportPdfDesign(
                        $this->userReport->getKey(),
                        $this->userReport->user_report_code,
                        $userReport
                    );
                    $options['new_version'] = true;
                    $options['base_on_report_id'] = $inputs['base_on_report_id'];
                }
                $audit = new AuditService();
                $audit->addAuditEntry($this->userReport, AuditAction::ACT_INSERT);
            });

            $id = $this->userReport->getKey();


            $this->addUserReportField($id, $this->userReport->system_report_id, 0, $options);
        }

        return $validator;
    }

    public function updateUserReport($id, $inputs)
    {
        $this->userReport = $this->get($id, false);

        $isOwner = $this->userReport->creator_user_id == \Auth::user()->id;
        $isSupervisor = $this->permissionService->hasModuleAccess(
            Module::MODULE_REPORTS,
            CrudAccessLevel::CRUD_ACCESS_LEVEL_SUPERVISOR
        );

        $canUpdate = $this->permissionService->hasModuleAccess(
            Module::MODULE_REPORTS,
            CrudAccessLevel::CRUD_ACCESS_LEVEL_UPDATE
        ) && $this->permissionService->hasModuleAccess(
            $this->userReport->systemReport->module_id,
            CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
        ) && ($isOwner || $isSupervisor);

        if ($this->userReport->systemReport->req_2_module_id) {
            $canUpdate = $canUpdate && $this->permissionService->hasModuleAccess(
                $this->userReport->systemReport->req_2_module_id,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
            );
        }

        if (!$canUpdate) {
            $msg = "Not met minimum edit access for report module";
            throw new PermissionException($msg);
        }
        $this->setModelFields($inputs);

        $validator = $this->getUserReportValidator();

        if ($validator->passes()) {
            DB::transaction(function () {
                $auditFields = $this->setupAuditFields();

                $this->userReport->save();

                $audit = new AuditService();
                $audit->auditFields($auditFields);
                $audit->addAuditEntry($this->userReport, AuditAction::ACT_UPDATE);
            });

            $validator->success = true;
        }

        return $validator;
    }

    public function deleteUserReport($id)
    {
        $this->userReport = $this->get($id, false);

        $isOwner = $this->userReport->creator_user_id == \Auth::user()->id;
        $isSupervisor = $this->permissionService->hasModuleAccess(
            Module::MODULE_REPORTS,
            CrudAccessLevel::CRUD_ACCESS_LEVEL_SUPERVISOR
        );

        $canDelete = $this->permissionService->hasModuleAccess(
            Module::MODULE_REPORTS,
            CrudAccessLevel::CRUD_ACCESS_LEVEL_DELETE
        ) && $this->permissionService->hasModuleAccess(
            $this->userReport->systemReport->module_id,
            CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
        ) && ($isOwner || $isSupervisor);

        if ($this->userReport->systemReport->req_2_module_id) {
            $canDelete = $canDelete && $this->permissionService->hasModuleAccess(
                $this->userReport->systemReport->req_2_module_id,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
            );
        }

        if (!$canDelete) {
            $msg = "Not met minimum delete access for report module";
            throw new PermissionException($msg);
        }

        $messages = $this->warningDeleteMessage($this->userReport);

        DB::transaction(function () {
            $this->userReport->delete();

            // Delete any user report filters
            $userReportFilters = UserReportFilter::where('user_report_id', $this->userReport->user_report_id)->get();
            foreach ($userReportFilters as $userReportFilter) {
                $userReportFilter = $this->getUserReportFilterApplied($userReportFilter->user_report_filter_id);
                $this->removeUserReportFilter($userReportFilter);
            }

            // Audit any changes
            $audit = new AuditService();
            $audit->addAuditEntry($this->userReport, AuditAction::ACT_DELETE);
        });

        return $messages;
    }

    public function warningDeleteMessage(UserReport $userReport)
    {
        $messages = new MessageBag();

        if ($userReport->reportSchedules()->count()) {
            $messages->add('Report Schedules', 'One or more report schedules exist for this report.');
        }

        return $messages;
    }


    public function checkUserDefinedQuery($query)
    {
        $resultText = "Query is valid";
        $total = 0;
        $first = 0;
        $maxFirst = ReportConstant::MAX_FIRST;
        $header = null;
        $data = null;

        if ($query) {
            try {
                $results = \DB::connection('mysqlReadView')->select(\DB::raw($query));
                if (is_array($results) && count($results)) {
                    $total = count($results);
                    while ($first < $maxFirst && $first < $total) {
                        $data[] = (array) $results[$first++];
                    }

                    $header = array_keys($data[0]);
                }
            } catch (\Exception $e) {
                $resultText = $e->getMessage();
            }
        } else {
            $resultText = "No Query defined";
        }

        return [
            'resultText' => $resultText,
            'first' => $first,
            'total' => $total,
            'header' => $header,
            'results' => $data
        ];
    }

    public function removeUserReportField($userReportId, $id)
    {
        $this->userReportField = $this->getUserReportFieldByReporFieldIdApi($userReportId, $id);
        if ($this->userReportField) {
            DB::transaction(function () {
                $this->userReportField->delete();
            });
        }

        return true;
    }

    public function lookups($editMode = false)
    {
        $pdfGroupBy = [];
        if ($editMode) {
            $userReportType = UserReportType::find($this->userReport->user_report_type_id);
            $pdfGroupBy = ReportField::where('system_report_id', $this->userReport->system_report_id)
                ->get(['report_field_id', 'report_field_code']);
        } else {
            $userReportType = UserReportType::where('user_report_type_id', UserReportType::USER_REPORT_TYPE_STANDARD);

            if (Common::valueYorNtoBoolean(\Auth::user()->report_queries_allowed)) {
                $userReportType->orWhere('user_report_type_id', UserReportType::USER_REPORT_TYPE_QUERY);
            }
            if (Common::valueYorNtoBoolean(\Auth::user()->wpdf_report_allowed)) {
                $userReportType->orWhere('user_report_type_id', UserReportType::USER_REPORT_TYPE_PDF);
            }

            $userReportType = $userReportType->get();
        }

        return [
            'userReportType' => $userReportType,
            'pdfGroupBy' => $pdfGroupBy
        ];
    }

    public function filterLookups($userReport)
    {

        $reportFields = ReportField::select('report_field.report_field_id', 'report_field.report_field_code')
            ->where('system_report_id', $userReport->system_report_id)
            ->orderBy('report_field.report_field_code', 'ASC')
            ->get();

        return [
            'reportFields' => $reportFields,
        ];
    }

    public function getUserReportFields($userRepId)
    {
        $return['userReportSelectedFields'] = UserReportField::orderby('user_report_field.output_position')
            ->where('user_report_field.user_report_id', $userRepId)
            ->leftjoin('report_field', 'user_report_field.report_field_id', "=", 'report_field.report_field_id')
            ->leftjoin(
                'report_field_type',
                'report_field.report_field_type_id',
                "=",
                'report_field_type.report_field_type_id'
            )
            ->get()
        ;
        $userRep = $this->get($userRepId, false);
        $return['userReportAvailableFields'] = ReportField::orderby('report_field.output_order')
            ->leftjoin(
                'report_field_type',
                'report_field.report_field_type_id',
                '=',
                'report_field_type.report_field_type_id'
            )
            ->where('report_field.system_report_id', $userRep->system_report_id)
            ->whereNotExists(function ($query) use ($userRepId) {
                $query->select()
                    ->from("user_report_field")
                    ->whereRaw("user_report_field.user_report_id = $userRepId")
                    ->whereRaw("user_report_field.report_field_id = report_field.report_field_id");
            })
            ->get()
        ;
        return $return;
    }

    public function getUserReportFiltersApplied($userRepId)
    {
        if (
            !$this->permissionService->hasModuleAccess(
                Module::MODULE_REPORTS,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
            )
        ) {
            $msg = "Not met minimum readonly access for report module in class" .
                " (" . __CLASS__ . ") method (" . __METHOD__ . "). " .
                " See line (" . __LINE__ . ")";
            throw new PermissionException($msg);
        }

        $query = UserReportFilter::select([
                "user_report_filter.user_report_filter_id",
                "report_field.report_field_code",
                "report_field_type.report_field_type_id",
                "report_field_type.report_field_type_code",
                "user_report_filter.user_report_id",
                "user_report_filter.filter_value",
                "report_operator_type.report_operator_type_code",
                "report_date_option.report_date_option_id",
                "report_date_option.report_date_option_code",
            ])
            ->where('user_report_filter.user_report_id', $userRepId)
            ->leftjoin('report_field', 'user_report_filter.report_field_id', "=", 'report_field.report_field_id')
            ->leftjoin(
                'report_field_type',
                'report_field_type.report_field_type_id',
                "=",
                'report_field.report_field_type_id'
            )
            ->leftjoin(
                'report_operator_type',
                'user_report_filter.report_operator_type_id',
                "=",
                'report_operator_type.report_operator_type_id'
            )
            ->leftjoin(
                'report_date_option',
                'user_report_filter.report_date_option_id',
                "=",
                'report_date_option.report_date_option_id'
            )
        ;
        $filter = new UserReportFilterFilter(Input::all());

        $query->orderBy($filter->sort, $filter->sortOrder);
        if ($filter->sort != "code") {
            $query->orderBy('report_field_code', 'ASC');
        }
        return $query;
    }

    public function getUserReportFilterApplied($userReportFilterId)
    {
        $this->userReportFilter = UserReportFilter::find($userReportFilterId);
        return $this->userReportFilter;
    }

    public function sortUserReportField($items)
    {
        $userReportFieldData = $this->buildUserReportFieldSortingData($items);
        DB::transaction(function () use ($userReportFieldData) {
//            UserReportField::where("user_report_id", $userReportId)->delete();
            foreach ($userReportFieldData as $item) {
                $item->save();
            }
        });
        return true;
    }

    public function addUserReportField($userReportId, $systemReportId, $reportFieldId = 0, $options = [])
    {
        if ($reportFieldId) {
            $exited = UserReportField::where('report_field_id', $reportFieldId)
                ->where('user_report_id', $userReportId)
                ->first()
            ;
            if ($exited) {
                return false;
            }
        }
        if (array_get($options, 'new_version')) {
            $baseReportFields = $this->getUserReportFields($options['base_on_report_id']);
            $result = $baseReportFields['userReportSelectedFields'];
        } else {
            $query = ReportField::select(['report_field_id','report_field_code'])
                ->where('report_field.system_report_id', $systemReportId)
                ->orderBy('output_order');

            if ($reportFieldId) {
                $query->where('report_field_id', $reportFieldId);
            }
            $result = $query->get();
        }

        $moveOutArr = [];
        if (empty($reportFieldId)) {
            $moveOutArr = $this->inactiveUserDefine($systemReportId);
        }

        DB::transaction(function () use ($result, $userReportId, $reportFieldId, $moveOutArr) {
            $outputPos = $this->generateOutputPos($userReportId, $reportFieldId);

            foreach ($result as $record) {
                if (!in_array($record->report_field_code, $moveOutArr)) {
                    $this->userReportField = new UserReportField();
                    $this->userReportField->user_report_id = $userReportId;
                    $this->userReportField->report_field_id = $record->report_field_id;
                    $this->userReportField->output_position = $outputPos++;

                    $this->userReportField->save();
                }
            }
        });
        if ($reportFieldId) {
            return $this->userReportField->user_report_field_id;
        }
    }

    public function addUserReportFilter(
        $userReportId,
        $reportFieldId,
        $rpOperatorTypeId,
        $date,
        $filterValue,
        $fieldType
    ) {
        $reportType = Input::get("report_type");
        DB::transaction(function () use (
            $userReportId,
            $reportFieldId,
            $rpOperatorTypeId,
            $date,
            $reportType,
            $filterValue,
            $fieldType
        ) {
            $this->userReportFilter = new UserReportFilter();
            $this->userReportFilter->user_report_id  = $userReportId;
            $this->userReportFilter->report_field_id = $reportFieldId;
            $this->userReportFilter->report_operator_type_id = $rpOperatorTypeId;
            $this->userReportFilter->report_date_option_id = $date;
            $this->userReportFilter->filter_value = $filterValue;
            if ($reportType == ReportFieldType::REP_FIELD_TYPE_DATE) {
                $this->setUserReportFilterDateData($fieldType, $this->userReportFilter);
            }
            $this->userReportFilter->save();
        });
        return $this->userReportFilter->user_report_filter_id;
    }

    public function updateUserReportFilter(
        $userReportId,
        $userReportFilterId,
        $rpOperatorTypeId,
        $date,
        $filterValue,
        $fieldType
    ) {
        $reportType = Input::get("report_type");
        DB::transaction(function () use (
            $userReportId,
            $userReportFilterId,
            $rpOperatorTypeId,
            $date,
            $reportType,
            $filterValue,
            $fieldType
        ) {
            $this->userReportFilter = UserReportFilter::find($userReportFilterId);
            $this->userReportFilter->user_report_id  = $userReportId;
            $this->userReportFilter->user_report_filter_id = $userReportFilterId;
            $this->userReportFilter->report_operator_type_id = $rpOperatorTypeId;
            $this->userReportFilter->report_date_option_id = $date;
            $this->userReportFilter->filter_value = $filterValue;
            if ($reportType == ReportFieldType::REP_FIELD_TYPE_DATE) {
                $this->setUserReportFilterDateData($fieldType, $this->userReportFilter);
            }

            $this->userReportFilter->save();
        });
        return $this->userReportFilter->user_report_filter_id;
    }

    private function setUserReportFilterDateData($fieldType, $userReportFilter)
    {
        $userReportFilter->filter_value_as_field_id = CommonConstant::DATABASE_VALUE_NO;
        $rpOperatorTypeId = $userReportFilter->report_operator_type_id;
        if (
            $rpOperatorTypeId == ReportOperatorType::REP_OPERATOR_TYPE_IS_EMPTY ||
            $rpOperatorTypeId == ReportOperatorType::REP_OPERATOR_TYPE_IS_NOT_EMPTY
        ) {
            $userReportFilter->report_date_option_id = null;
            $userReportFilter->filter_value = null;
        } else {
            if ($fieldType == ReportConstant::USER_RP_DATE_TYPE_RP_FIELD) {
                $userReportFilter->filter_value_as_field_id = CommonConstant::DATABASE_VALUE_YES;
                $userReportFilter->filter_value = $userReportFilter->report_date_option_id;
                $userReportFilter->report_date_option_id = null;
            } elseif ($userReportFilter->report_date_option_id != ReportDateOption::REP_DATE_OPTION_SPECIFIC_DATE) {
                $userReportFilter->filter_value = null;
            }
        }
    }

    public function removeUserReportFilter($userReportFilter)
    {
        DB::transaction(function () use ($userReportFilter) {
            $userReportFilter->delete();
        });
        return true;
    }

    public function reGetUserRepFiltering($userReportId)
    {
        $filterText = "";
        $sep = "";

        $userRepFilters = $this->getQueryUserRepFiltering($userReportId)->get();

        if (count($userRepFilters) > 0) {
            foreach ($userRepFilters as $userRepFilter) {
                $reportFieldCode = $userRepFilter->report_field_code;

                if (
                    ($userRepFilter->report_operator_type_id == ReportConstant::REP_FILTER_TYPE_IS_EMPTY)
                    || ($userRepFilter->report_operator_type_id == ReportConstant::REP_FILTER_TYPE_IS_NOT_EMPTY)
                ) {
                    $filterText .= "$sep" . $reportFieldCode . " " . $userRepFilter->report_operator_type_code;
                } else {
                    if ($userRepFilter->report_field_type_id == ReportConstant::REP_FIELD_TYPE_DATE) {
                        if ($userRepFilter->report_date_option_id == ReportConstant::REPORT_DATE_OPT_SPECIFIC) {
                            $filterText .= "$sep" . $reportFieldCode . " " . $userRepFilter->report_operator_type_code
                                . " (" . Common::dateFieldDisplayFormat($userRepFilter->filter_value) . ")";
                        } else {
                            $filterText .= "$sep" . $reportFieldCode . " " . $userRepFilter->report_operator_type_code
                                . " (" . $userRepFilter->report_date_option_code . ")";
                        }
                    } else {
                        $filterText .= "$sep" . $reportFieldCode . " " . $userRepFilter->report_operator_type_code
                            . " (" . Common::dateFieldDisplayFormat($userRepFilter->filter_value) . ")";
                    }
                }
                $sep = ", ";
            }
        }

        return $filterText;
    }

    private function buildUserReportFieldSortingData($items)
    {
        $userReportFieldData = array();
        if (count($items)) {
            foreach ($items as $key => $item) {
                $userReportField = UserReportField::find($item);
                if ($userReportField) {
                    Common::assignField($userReportField, 'output_position', ['output_position' => $key]);
                    $userReportFieldData[] = $userReportField;
                }
            }
            return $userReportFieldData;
        }
        return array();
    }

    private function getUserReportFieldByOutputPositionApi($userReportId, $output)
    {
        $this->userReportField = UserReportField::where('output_position', $output)
            ->where("user_report_id", $userReportId)
            ->first()
        ;
        if (is_null($this->userReportField)) {
            return null;
        }
        return $this->userReportField;
    }

    private function getUserReportFieldByReporFieldIdApi($userReportId, $id)
    {
        $this->userReportField = UserReportField::where('report_field_id', $id)
            ->where("user_report_id", $userReportId)
            ->first()
        ;
        if (is_null($this->userReportField)) {
            return null;
        }
        return $this->userReportField;
    }

    private function initDefaultData($systemReportId, $userReport = null)
    {
        $this->userReport->user_report_code = CommonConstant::CREATE_NEW_CODE;
        $this->userReport->site_group_id = Auth::user()->site_group_id;
        $this->userReport->system_report_id = $systemReportId;
        $this->userReport->user_report_desc = $this->systemReport->system_report_desc;
        $this->userReport->available = CommonConstant::DATABASE_VALUE_YES;
        $this->userReport->creator_user_id = Auth::user()->id;
        $this->userReport->wpdf2 = CommonConstant::DATABASE_VALUE_YES;

        if (! is_null($userReport)) {
            $this->userReport->user_report_type_id  = $userReport->user_report_type_id;
            $this->userReport->order_by_field1_id   = $userReport->order_by_field1_id;
            $this->userReport->order_by_field1_asc  = $userReport->order_by_field1_asc;
            $this->userReport->order_by_field2_id   = $userReport->order_by_field2_id;
            $this->userReport->order_by_field2_asc  = $userReport->order_by_field2_asc;
            $this->userReport->order_by_field3_id   = $userReport->order_by_field3_id;
            $this->userReport->order_by_field3_asc  = $userReport->order_by_field3_asc;
            $this->userReport->wpdf_default_group_by = $userReport->wpdf_default_group_by;
            $this->userReport->wpdf2                = $userReport->wpdf2;
            $this->userReport->wpdf_text_can_grow   = $userReport->wpdf_text_can_grow;
        } else {
            $this->userReport->order_by_field1_id   = $this->systemReport->order_by_field1_id;
            $this->userReport->order_by_field1_asc  = $this->systemReport->order_by_field1_asc;
            $this->userReport->order_by_field2_id   = $this->systemReport->order_by_field2_id;
            $this->userReport->order_by_field2_asc  = $this->systemReport->order_by_field2_asc;
            $this->userReport->order_by_field3_id   = $this->systemReport->order_by_field3_id;
            $this->userReport->order_by_field3_asc  = $this->systemReport->order_by_field3_asc;
        }
    }

    private function all()
    {
        $query = UserReport::userSiteGroup();

        return $query->select();
    }

    private function setModelFields($inputs)
    {
        Common::assignField($this->userReport, 'user_report_code', $inputs);
        Common::assignField($this->userReport, 'user_report_desc', $inputs);
        Common::assignField($this->userReport, 'comment', $inputs);
        Common::assignField($this->userReport, 'available', $inputs);
        Common::assignField($this->userReport, 'user_report_type_id', $inputs);
        Common::assignField($this->userReport, 'creator_user_id', $inputs);

        Common::assignField($this->userReport, 'order_by_field1_id', $inputs);
        Common::assignField($this->userReport, 'order_by_field1_asc', $inputs);
        Common::assignField($this->userReport, 'order_by_field2_id', $inputs);
        Common::assignField($this->userReport, 'order_by_field2_asc', $inputs);
        Common::assignField($this->userReport, 'order_by_field3_id', $inputs);
        Common::assignField($this->userReport, 'order_by_field3_asc', $inputs);

        if (
            $this->userReport->user_report_type_id == UserReportType::USER_REPORT_TYPE_QUERY
            && \Auth::user()->report_queries_allowed == CommonConstant::DATABASE_VALUE_YES
        ) {
            Common::assignField($this->userReport, 'query', $inputs);
        }
        if ($this->userReport->user_report_type_id == UserReportType::USER_REPORT_TYPE_PDF) {
            Common::assignField($this->userReport, 'wpdf_default_design', $inputs);
            Common::assignField($this->userReport, 'wpdf_default_group_by', $inputs);
            Common::assignField($this->userReport, 'wpdf_text_can_grow', $inputs);
            Common::assignField($this->userReport, 'wpdf2', $inputs);
        } else {
            $this->userReport->wpdf2 = CommonConstant::DATABASE_VALUE_NO;
            $this->userReport->wpdf_text_can_grow = CommonConstant::DATABASE_VALUE_NO;
        }
    }

    private function getUserReportValidator($isEdit = false)
    {
        $attributes = $this->userReport->getAttributes();

        $validationRules = [
            'user_report_desc' => ['max:255'],
            'creator_user_id' => ['required', 'user_id_exists'],
            'comment' => ['max:2048'],
            'query' => ['max:30000'],
            'order_by_field3_id' => ["validate_existed_ordering:3"],
            'order_by_field2_id' => ["validate_existed_ordering:2"]
        ];

        $validationMessages = [
            'validate_existed_ordering' => 'Cannot order by the same field more than once.',
        ];

        if (!config('cloud.reporting.allow_views_in_sql_report')) {
            $this->validateQueryViewReferenced();
            $validationRules['query'][] = 'query_view_referenced';
            $validationMessages['query_view_referenced']
                = 'Entered query must not reference a view, except for SHOW COLUMNS FROM [view_name].';
        }

        $validator = Validator::make($attributes, $validationRules, $validationMessages);
        $validator->success = false;

        return $validator;
    }

    private function validateQueryViewReferenced()
    {
        // Check the query string doesn't contain a view.
        \Validator::extend('query_view_referenced', function ($attribute, $value, $parameters) {
            // CLD-16048: Allow views under one condition.  That the query begins with 'show' and does not contain
            // the select statement.  i.e. SHOW COLUMNS FROM vw_pr01.
            $showIsFirstCommand = stripos($this->userReport->query, 'show') === 0;
            $selectCommandNotUsed = stripos($this->userReport->query, 'select') === false;

            return ($showIsFirstCommand && $selectCommandNotUsed)
                || stripos($this->userReport->query, 'vw_') === false;
        });
    }

    private function generateOutputPos($userReportId = 0, $reportFieldId = 0)
    {
        if ($reportFieldId) {
            $reportPos = 0;
            $userReportFields = $this->getUserReportFields($userReportId)['userReportSelectedFields'];
            if (count($userReportFields)) {
                foreach ($userReportFields as $field) {
                    $reportPos = $reportPos > $field->output_position ? $reportPos : $field->output_position;
                }
            }
            return ++$reportPos;
        }
        return 1;
    }

    private function getQueryUserRepFiltering($userReportId)
    {
        $query = UserReportFilter::select(
            'user_report_filter.user_report_filter_id',
            'report_field.report_field_code',
            'report_field_type.report_field_type_id',
            'report_field_type.report_field_type_code',
            'user_report_filter.user_report_id',
            'user_report_filter.filter_value',
            'report_date_option.report_date_option_id',
            'report_date_option.report_date_option_code',
            'report_operator_type.report_operator_type_id',
            'report_operator_type.report_operator_type_code'
        )->leftJoin('report_field', 'user_report_filter.report_field_id', '=', 'report_field.report_field_id')
            ->leftJoin(
                'report_field_type',
                'report_field.report_field_type_id',
                '=',
                'report_field_type.report_field_type_id'
            )
            ->leftJoin(
                'report_operator_type',
                'user_report_filter.report_operator_type_id',
                '=',
                'report_operator_type.report_operator_type_id'
            )
            ->leftJoin(
                'report_date_option',
                'user_report_filter.report_date_option_id',
                '=',
                'report_date_option.report_date_option_id'
            )
            ->where('user_report_filter.user_report_id', $userReportId);

        return $query;
    }

    private function reudInsertUserReportFilters($userReportId, $basedOnUserReportId)
    {
        $userReportFilters = $this->getUserReportFilters($userReportId, $basedOnUserReportId);
        if (is_array($userReportFilters) && count($userReportFilters)) {
            \DB::transaction(function () use ($userReportFilters) {
                \DB::table('user_report_filter')->insert($userReportFilters);
            });
        }
    }

    private function getUserReportFilters($userReportId, $basedOnUserReportId)
    {
        $query = UserReportFilter::select(
            \DB::raw("$userReportId AS user_report_id"),
            'report_field_id',
            'report_operator_type_id',
            'report_date_option_id',
            'filter_value',
            'filter_value_as_field_id'
        )->where('user_report_id', $basedOnUserReportId)
            ->get();

        $userReportFilters = $query->toArray();
        return $userReportFilters;
    }

    private function checkExistedOrdering($value, $params, $inputs)
    {
        $matchOrdering3 = [
            $inputs['order_by_field1_id'],
            $inputs['order_by_field2_id']
        ];
        $matchOrdering2 = [
            $inputs['order_by_field1_id']
        ];

        $ordering2 = $inputs['order_by_field2_id'];
        $ordering3 = $inputs['order_by_field3_id'];

        if (is_array($params)) {
            foreach ($params as $param) {
                switch ($param) {
                    case '3':
                        if (!is_null($ordering3) && $ordering3 > 0 && in_array($ordering3, $matchOrdering3)) {
                            return false;
                        }
                        break;

                    case '2':
                        if (!is_null($ordering2) && $ordering2 > 0 && in_array($ordering2, $matchOrdering2)) {
                            return false;
                        }
                        break;

                    default:
                        return true;
                }
            }
        }

        return true;
    }

    private function setServices($inputs)
    {
        $this->matchService5 = [
            $inputs['gov_effective_svc_function_1_id'],
            $inputs['gov_effective_svc_function_2_id'],
            $inputs['gov_effective_svc_function_3_id'],
            $inputs['gov_effective_svc_function_4_id']
        ];
        $this->matchService4 = [
            $inputs['gov_effective_svc_function_1_id'],
            $inputs['gov_effective_svc_function_2_id'],
            $inputs['gov_effective_svc_function_3_id']
        ];
        $this->matchService3 = [
            $inputs['gov_effective_svc_function_1_id'],
            $inputs['gov_effective_svc_function_2_id']
        ];
        $this->matchService2 = [
            $inputs['gov_effective_svc_function_1_id']
        ];

        $this->service5 = $inputs['gov_effective_svc_function_5_id'];
        $this->service4 = $inputs['gov_effective_svc_function_4_id'];
        $this->service3 = $inputs['gov_effective_svc_function_3_id'];
        $this->service2 = $inputs['gov_effective_svc_function_2_id'];
    }

    private function cloneUserReportPdfDesign($newUserReportId, $newUserReportCode, UserReport $oldUserReport)
    {
        $validator = \Validator::make([], []);
        $validator->success = true;
        $oldUserReportDesign = UserReportPdfDesign::where('user_report_id', '=', $oldUserReport->getKey())->first();

        if (!is_null($oldUserReportDesign)) {
            $newUserReportDesign = $this->userReportPdfDesignService->newUserReportPdfDesign();
            $newUserReportDesign->wpdfUserReportId = $newUserReportId;
            $newUserReportDesign->wpdfSaveName = $newUserReportCode;
            $newUserReportDesign->wpdfUserReportId = $newUserReportId;

            $oldPdfDesignContent = json_decode($oldUserReportDesign->pdf_design_content);
            // skip for wpdf2
            if ($oldUserReport->wpdf2 !== CommonConstant::DATABASE_VALUE_YES) {
                $oldPdfDesignContent->design->name = $newUserReportCode;
                $oldPdfDesignContent->design->userreportid = $newUserReportId;
            }

            $newUserReportDesign->wpdfJSONData = json_encode($oldPdfDesignContent);
            $newUserReportDesignId = null;
            $validator = $this->userReportPdfDesignService->addUserReportPdfDesign(
                $newUserReportDesignId,
                $newUserReportDesign->toArray()
            );
        }

        return $validator;
    }

    private function setupAuditFields()
    {
        $pdfDefault = UserReportPdfDesign::find($this->userReport->getOriginal('wpdf_default_design'));
        $pdfDefaultCode = $pdfDefault ? $pdfDefault->pdf_design_name : '';

        $pdfGroupByDefault = ReportField::find($this->userReport->getOriginal('wpdf_default_group_by'));
        $pdfGroupByDefaultCode = $pdfGroupByDefault ? $pdfGroupByDefault->report_field_code : '';

        $orderBy1 = ReportField::find($this->userReport->getOriginal('order_by_field1_id'));
        $orderBy1Code = $orderBy1 ? $orderBy1->report_field_code : '';

        $orderBy1Acs = $this->userReport->getOriginal('order_by_field1_asc');
        $orderBy1AcsText = $orderBy1Acs == CommonConstant::DATABASE_VALUE_YES ? 'Ascending' :
                ($orderBy1Acs == CommonConstant::DATABASE_VALUE_NO ? 'Descending' : '');

        $orderBy2 = ReportField::find($this->userReport->getOriginal('order_by_field2_id'));
        $orderBy2Code = $orderBy2 ? $orderBy2->report_field_code : '';

        $orderBy2Acs = $this->userReport->getOriginal('order_by_field2_asc');
        $orderBy2AcsText = $orderBy2Acs == CommonConstant::DATABASE_VALUE_YES ? 'Ascending' :
                ($orderBy2Acs == CommonConstant::DATABASE_VALUE_NO ? 'Descending' : '');

        $orderBy3 = ReportField::find($this->userReport->getOriginal('order_by_field3_id'));
        $orderBy3Code = $orderBy3 ? $orderBy3->report_field_code : '';

        $orderBy3Acs = $this->userReport->getOriginal('order_by_field3_asc');
        $orderBy3AcsText = $orderBy3Acs == CommonConstant::DATABASE_VALUE_YES ? 'Ascending' :
                ($orderBy3Acs == CommonConstant::DATABASE_VALUE_NO ? 'Descending' : '');

        $creator            = \Tfcloud\Models\User::find($this->userReport->getOriginal('creator_user_id'));
        $creatorAuditText   = empty($creator) ? '' : $creator->display_name;

        $auditFields = new AuditFields($this->userReport);

        return $auditFields
            ->addField('Description', 'user_report_desc')
            ->addField('Created By', 'creator_user_id', ['originalText' => $creatorAuditText])
            ->addField('Comment', 'comment')
            ->addField('Available', 'available')
            ->addField('Default PDF Design', 'wpdf_default_design', ['originalText' => $pdfDefaultCode])
            ->addField('Group By', 'wpdf_default_group_by', ['originalText' => $pdfGroupByDefaultCode])
            ->addField('Order By Field (1)', 'order_by_field1_id', ['originalText' => $orderBy1Code])
            ->addField('Order By (1)', 'order_by_field1_asc', ['originalText' => $orderBy1AcsText])
            ->addField('Order By Field (2)', 'order_by_field2_id', ['originalText' => $orderBy2Code])
            ->addField('Order By (2)', 'order_by_field2_asc', ['originalText' => $orderBy2AcsText])
            ->addField('Order By Field (3)', 'order_by_field3_id', ['originalText' => $orderBy3Code])
            ->addField('Order By (3)', 'order_by_field3_asc', ['originalText' => $orderBy3AcsText])
            ->getFields();
    }

    private function inactiveUserDefine($systemReportId)
    {
        $inactiveUserDefArr = [];
        $systemReportObj = SystemReport::find($systemReportId);

        if ($systemReportObj instanceof SystemReport) {
            if (is_null($systemReportObj->gen_table_id)) {
                return $inactiveUserDefArr;
            }
            $genUserDefData = GenUserdefLabel::where('site_group_id', Auth::user()->site_group_id)
                ->where('gen_table_id', $systemReportObj->gen_table_id)
                ->select([
                    'user_text1_label AS user_text1',
                    'user_text2_label AS user_text2',
                    'user_text3_label AS user_text3',
                    'user_text4_label AS user_text4',
                    'user_text5_label AS user_text5',
                    'user_text6_label AS user_text6',
                    'user_check1_label AS user_check1',
                    'user_check2_label AS user_check2',
                    'user_check3_label AS user_check3',
                    'user_check4_label AS user_check4',
                    'user_date1_label AS user_date1',
                    'user_date2_label AS user_date2',
                    'user_date3_label AS user_date3',
                    'user_date4_label AS user_date4',
                    'user_selection1_label AS user_sel1_code',
                    'user_selection2_label AS user_sel2_code',
                    'user_selection3_label AS user_sel3_code',
                    'user_selection4_label AS user_sel4_code',
                    'user_selection1_label AS user_sel1_desc',
                    'user_selection2_label AS user_sel2_desc',
                    'user_selection3_label AS user_sel3_desc',
                    'user_selection4_label AS user_sel4_desc',
                    'user_contact1_label AS user_contact1',
                    'user_contact2_label AS user_contact2',
                    'user_contact3_label AS user_contact3',
                    'user_contact4_label AS user_contact4',
                    'user_contact5_label AS user_contact5',
                    'user_memo1_label AS user_memo1',
                    'user_memo2_label AS user_memo2',
                ])
                ->first()->toArray();

            foreach ($genUserDefData as $key => $value) {
                if (is_null($value) || $value == '') {
                    $inactiveUserDefArr[] = $key;
                    if (strpos($key, '_contact') !== false) {
                        $inactiveUserDefArr[] = str_replace('_contact', '_contact_org', $key);
                    }
                }
            }
        }
        return $inactiveUserDefArr;
    }
}
