<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Models\GenTable;
use Tfcloud\Models\Datafeed;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Datafeed\DatafeedService;

class DatafeedFilterQueryService extends BaseService
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    public function reAddDatafeedQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $datafeedService = new DatafeedService($this->permissionService);
        $datafeedQuery = $datafeedService->filterAll($query, $filterData, $viewName);

        $readingFrom = array_get($filterData, 'readingFrom');
        $readingTo = array_get($filterData, 'readingTo');
        if ($readingFrom && $readingTo) {
            array_push($whereTexts, "Reading Date from $readingFrom to $readingTo");
        } else {
            if ($readingFrom) {
                array_push($whereTexts, "Reading Date from $readingFrom");
            } elseif ($readingTo) {
                array_push($whereTexts, "Reading Date to $readingTo");
            }
        }

        return $datafeedQuery;
    }
}
