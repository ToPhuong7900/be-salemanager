<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Filters\Contract\ContractInspectionReportFilter;
use Tfcloud\Models\InspectionSFG20\InspSFG20InspectionStatus;
use Tfcloud\Models\InspectionSFG20\InspSFG20InspectionStatusType;
use Tfcloud\Models\Report;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\Contract\InstructionService;
use Tfcloud\Services\PermissionService;

class ContractInspectionFilterQueryService extends BaseService
{
    protected $permissionService;
    protected $instructionService;
    private $reportBoxFilterLoader = '';

    public function __construct(PermissionService $permissionService, Report $report = null)
    {
        $this->permissionService = $permissionService;
        $this->instructionService = new InstructionService($this->permissionService);
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function reAddContractScheduledInspectionQuery($filterData, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $filterData = $this->formatInputs($filterData);
        return $this->filterAllScheduledInspections($filterData);
    }

    private function filterAllScheduledInspections($filterData)
    {
        $rawData = $this->allScheduledInspections($filterData);
        return $rawData;
    }

    private function allScheduledInspections($filterData)
    {
        $query  = "";
        $query .= " ";
        $query .= "         CASE ";
        $query .= "                  WHEN row_num_contact = 1 ";
        $query .= "                  THEN contact_name ";
        $query .= "                  ELSE '' ";
        $query .= "         END AS 'Resource Name', ";
        $query .= "         CASE ";
        $query .= "                  WHEN row_num_instruction = 1 ";
        $query .= "                  THEN contract_instruction_code ";
        $query .= "                  ELSE '' ";
        $query .= "         END AS 'Instruction ID', ";
        $query .= "         CASE ";
        $query .= "                  WHEN rec_type = 0 ";
        $query .= "                  THEN instruction_desc ";
        $query .= "                  ELSE '' ";
        $query .= "         END AS 'Instruction Description', ";
        $query .= "         CASE ";
        $query .= "                  WHEN row_num_instruction = 1 ";
        $query .= "                  THEN site_desc ";
        $query .= "                  ELSE '' ";
        $query .= "         END AS 'Site Description', ";
        $query .= "         CASE ";
        $query .= "                  WHEN row_num_instruction = 1 ";
        $query .= "                  THEN second_addr_obj ";
        $query .= "                  ELSE '' ";
        $query .= "         END AS 'Sub Dwelling', ";
        $query .= "         CASE ";
        $query .= "                  WHEN row_num_instruction = 1 ";
        $query .= "                  THEN first_addr_obj ";
        $query .= "                  ELSE '' ";
        $query .= "         END AS 'Number/Name', ";
        $query .= "         CASE ";
        $query .= "                  WHEN row_num_instruction = 1 ";
        $query .= "                  THEN street ";
        $query .= "                  ELSE '' ";
        $query .= "         END AS 'Street', ";
        $query .= "         CASE ";
        $query .= "                  WHEN row_num_instruction = 1 ";
        $query .= "                  THEN locality ";
        $query .= "                  ELSE '' ";
        $query .= "         END AS 'Locality', ";
        $query .= "         CASE ";
        $query .= "                  WHEN row_num_instruction = 1 ";
        $query .= "                  THEN town ";
        $query .= "                  ELSE '' ";
        $query .= "         END AS 'Town', ";
        $query .= "         CASE ";
        $query .= "                  WHEN row_num_instruction = 1 ";
        $query .= "                  THEN region ";
        $query .= "                  ELSE '' ";
        $query .= "         END AS 'Region', ";
        $query .= "         CASE ";
        $query .= "                  WHEN row_num_instruction = 1 ";
        $query .= "                  THEN postcode ";
        $query .= "                  ELSE '' ";
        $query .= "         END AS 'Post Code', ";
        $query .= "         CASE ";
        $query .= "                  WHEN row_num_instruction = 1 ";
        $query .= "                  THEN site_email_addr ";
        $query .= "                  ELSE '' ";
        $query .= "         END AS 'Site Email', ";
        $query .= "         CASE ";
        $query .= "                  WHEN row_num_instruction = 1 ";
        $query .= "                  THEN phone_main ";
        $query .= "                  ELSE '' ";
        $query .= "         END AS 'Main Phone' ";
        $query .= "FROM     ( SELECT  ROW_NUMBER() OVER ( ORDER BY T.contact_name, ";
        $query .= "                  T.contract_instruction_code, T.insp_sfg20_scheduled_inspection_code, ";
        $query .= "                  rec_type DESC ) row_num  , ";
        $query .= "                  T.contract_instruction_id, ";
        $query .= "                  T.contact_id             , ";
        $query .= "                  T.site_id                , ";
        $query .= "                  ROW_NUMBER() OVER ( PARTITION BY ";
        $query .= "                  T.contract_instruction_code ORDER BY ";
        $query .= "                  T.contact_name, T.contract_instruction_code, ";
        $query .= "                  T.insp_sfg20_scheduled_inspection_code, rec_type ) ";
        $query .= "                  row_num_instruction        , ";
        $query .= "                  T.contract_instruction_code, ";
        $query .= "                  ROW_NUMBER() OVER ( PARTITION BY T.contact_name ";
        $query .= "                  ORDER BY T.contact_name, ";
        $query .= "                  T.contract_instruction_code, T.insp_sfg20_scheduled_inspection_code, ";
        $query .= "                  rec_type ) row_num_contact, ";
        $query .= "                  CONCAT(insp_sfg20_group_level_1.insp_sfg20_group_level_1_code, ' - ', "
            . "insp_sfg20_type_version.insp_sfg20_type_desc, '\n', IF (I.plant_id IS NULL, '', CONCAT('Plant: ', "
            . "plant_code, ' - ', plant_desc, '\n', IF (serial_num IS NULL, '', CONCAT('S/N: ', serial_num, '\n')), "
            . "IF (gen_userdef_group.user_text2 IS NULL, '', "
            . "CONCAT('Location: ', gen_userdef_group.user_text2, '\n'))) ), IF (SI.inspection_due_date IS NULL, '', "
            . "CONCAT('Scheduled Date: ', DATE_FORMAT(SI.inspection_due_date, \"%d %b %Y\"), "
            . "' (', SI.insp_sfg20_scheduled_inspection_code, ')', '\n')), "
            . "IF (SI.comments IS NULL, '', CONCAT('Job Comment: "
            . "', REPLACE(SI.comments, '\n', ' '), '\n')) ) AS instruction_desc, ";
        $query .= "                  second_addr_obj     , ";
        $query .= "                  first_addr_obj      , ";
        $query .= "                  street              , ";
        $query .= "                  locality            , ";
        $query .= "                  town                , ";
        $query .= "                  region              , ";
        $query .= "                  address.postcode    , ";
        $query .= "                  address.phone_main  , ";
        $query .= "                  site.site_email_addr, ";
        $query .= "                  site.site_desc      , ";
        $query .= "                  T.contact_name      , ";
        $query .= "                  T.insp_sfg20_scheduled_inspection_code   , ";
        $query .= "                  T.insp_sfg20_scheduled_inspection_id     , ";
        $query .= "                  rec_type ";
        $query .= "         FROM     ( SELECT ";
        $query .= "                          contract_instruction.contract_instruction_id ";
        $query .= "                          , ";
        $query .= "                          contact.contact_id        , ";
        $query .= "                          insp_sfg20_inspection.site_id        , ";
        $query .= "                          contract_instruction_code , ";
        $query .= "                          contact.contact_name      , ";
        $query .= "                          insp_sfg20_scheduled_inspection.insp_sfg20_scheduled_inspection_code, ";
        $query .= "                          insp_sfg20_scheduled_inspection.insp_sfg20_scheduled_inspection_id  , ";
        $query .= "                          0 AS rec_type ";
        $query .= "                  FROM    contract_instruction ";
        $query .= "                          INNER JOIN contract ";
        $query .= "                          ON      contract.contract_id = ";
        $query .= "                                  contract_instruction.contract_id ";
        $query .= "                          INNER JOIN contact ";
        $query .= "                          ON      contact.contact_id = ";
        $query .= "                                  contract.supplier_contact_id ";
        $query .= "                          INNER JOIN insp_sfg20_scheduled_inspection ";
        $query .= "                          ON ";
        $query .= "                                  insp_sfg20_scheduled_inspection.contract_instruction_id ";
        $query .= "                                  = ";
        $query .= "                                  contract_instruction.contract_instruction_id ";
        $query .= "                          LEFT JOIN insp_sfg20_inspection_version ";
        $query .= "                          ON ";
        $query .= "                                  insp_sfg20_inspection_version.insp_sfg20_inspection_version_id ";
        $query .= "                                  = ";
        $query .= "                                  insp_sfg20_scheduled_inspection.insp_sfg20_inspection_version_id ";
        $query .= "                          LEFT JOIN insp_sfg20_inspection ";
        $query .= "                          ON ";
        $query .= "                                  insp_sfg20_inspection.insp_sfg20_inspection_id ";
        $query .= "                                  = ";
        $query .= "                                  insp_sfg20_inspection_version.insp_sfg20_inspection_id ";
        $query .= "                   ";
        $query .= "                  UNION ALL ";
        $query .= "                   ";
        $query .= "                  SELECT ";
        $query .= "                         contract_instruction.contract_instruction_id ";
        $query .= "                         , ";
        $query .= "                         contact.contact_id        , ";
        $query .= "                         insp_sfg20_inspection.site_id        , ";
        $query .= "                         contract_instruction_code , ";
        $query .= "                         contact.contact_name      , ";
        $query .= "                         insp_sfg20_scheduled_inspection.insp_sfg20_scheduled_inspection_code, ";
        $query .= "                         insp_sfg20_scheduled_inspection.insp_sfg20_scheduled_inspection_id  , ";
        $query .= "                         1 AS rec_type ";
        $query .= "                  FROM   contract_instruction ";
        $query .= "                         INNER JOIN contract ";
        $query .= "                         ON     contract.contract_id = ";
        $query .= "                                contract_instruction.contract_id ";
        $query .= "                         INNER JOIN contact ";
        $query .= "                         ON     contact.contact_id = ";
        $query .= "                                contract.supplier_contact_id ";
        $query .= "                         INNER JOIN insp_sfg20_scheduled_inspection ";
        $query .= "                         ON ";
        $query .= "                                 insp_sfg20_scheduled_inspection.contract_instruction_id ";
        $query .= "                                 = ";
        $query .= "                                 contract_instruction.contract_instruction_id ";
        $query .= "                         LEFT JOIN insp_sfg20_inspection_version ";
        $query .= "                         ON ";
        $query .= "                                 insp_sfg20_inspection_version.insp_sfg20_inspection_version_id ";
        $query .= "                                 = ";
        $query .= "                                 insp_sfg20_scheduled_inspection.insp_sfg20_inspection_version_id ";
        $query .= "                         LEFT JOIN insp_sfg20_inspection ";
        $query .= "                         ON ";
        $query .= "                                 insp_sfg20_inspection.insp_sfg20_inspection_id ";
        $query .= "                                 = ";
        $query .= "                                 insp_sfg20_inspection_version.insp_sfg20_inspection_id ";
        $query .= "                  ) ";
        $query .= "                  T ";
        $query .= "                  INNER JOIN contract_instruction CI ";
        $query .= "                  ON       T.contract_instruction_id = ";
        $query .= "                           CI.contract_instruction_id ";
        $query .= "                  INNER JOIN contract C ";
        $query .= "                  ON       C.contract_id = CI.contract_id ";
        $query .= "                  INNER JOIN insp_sfg20_scheduled_inspection SI ";
        $query .= "                  ON       T.insp_sfg20_scheduled_inspection_id = "
                                    . "SI.insp_sfg20_scheduled_inspection_id ";
        $query .= "                  LEFT JOIN insp_sfg20_inspection_version ";
        $query .= "                  ON ";
        $query .= "                            insp_sfg20_inspection_version.insp_sfg20_inspection_version_id ";
        $query .= "                            = ";
        $query .= "                            SI.insp_sfg20_inspection_version_id ";
        $query .= "                  INNER JOIN insp_sfg20_type_version ";
        $query .= "                  ON ";
        $query .= "                            insp_sfg20_type_version.insp_sfg20_type_version_id ";
        $query .= "                            = ";
        $query .= "                            insp_sfg20_inspection_version.insp_sfg20_type_version_id ";
        $query .= "                  LEFT JOIN insp_sfg20_inspection I";
        $query .= "                  ON ";
        $query .= "                            I.insp_sfg20_inspection_id ";
        $query .= "                            = ";
        $query .= "                            insp_sfg20_inspection_version.insp_sfg20_inspection_id ";
        $query .= "                  LEFT JOIN insp_sfg20_group_level_1 ";
        $query .= "                  ON ";
        $query .= "                            insp_sfg20_group_level_1.insp_sfg20_group_level_1_id ";
        $query .= "                            = ";
        $query .= "                            insp_sfg20_type_version.insp_sfg20_group_level_1_id ";
        $query .= "                  INNER JOIN inspection_status ";
        $query .= "                  ON       inspection_status.inspection_status_id ";
        $query .= "                           = SI.inspection_status_id ";
        $query .= "                  LEFT JOIN plant ";
        $query .= "                  ON       plant.plant_id = I.plant_id ";
        $query .= "                  LEFT JOIN gen_userdef_group ";
        $query .= "                  ON       gen_userdef_group.gen_userdef_group_id ";
        $query .= "                           = plant.gen_userdef_group_id ";
        $query .= "                  INNER JOIN site ";
        $query .= "                  ON       site.site_id = I.site_id ";
        $query .= "                  INNER JOIN address ";
        $query .= "                  ON       address.address_id = site.address_id ";
        $query .= "                  INNER JOIN address_street ";
        $query .= "                  ON       address_street.address_street_id = ";
        $query .= "                           address.address_street_id ";

        // Add where clause here
        $this->applyFilter($filterData, $query);

        $query .= "         ) ";
        $query .= "         TT ";

        $query .= "ORDER BY row_num";

        return \DB::table(null)->select(\DB::raw($query));
    }

    private function applyFilter($filterData, &$query)
    {
        $filter = new ContractInspectionReportFilter($filterData);

        $query .= " WHERE C.site_group_id = " . Auth::user()->site_group_id;

        if (!is_null($filter->code)) {
            $query .= " AND C.contract_code like '%" . trim($filter->code) . "%'";
        }

        if (!is_null($filter->name)) {
            $query .= " AND C.contract_name like '%" . trim($filter->name) . "%'";
        }

        if (!is_null($filter->supplier)) {
            $query .= " AND C.supplier_contact_id = " . $filter->supplier;
        }
        if (!empty($filter->targetFrom)) {
            $query .= " AND DATE_FORMAT(SI.inspection_due_date, '%Y-%m-%d') >= '$filter->targetFrom'";
        }

        if (!empty($filter->targetTo)) {
            $query .= " AND DATE_FORMAT(SI.inspection_due_date, '%Y-%m-%d') <= '$filter->targetTo'";
        }

        if (
            !is_null($filter->draft) ||
            !is_null($filter->open) ||
            !is_null($filter->issued) ||
            !is_null($filter->complete) ||
            !is_null($filter->cancelled) ||
            !is_null($filter->onHold) ||
            !is_null($filter->closed)
        ) {
            $options = [];

            if (!is_null($filter->draft)) {
                array_push($options, InspSFG20InspectionStatusType::DRAFT);
            }

            if (!is_null($filter->open)) {
                array_push($options, InspSFG20InspectionStatusType::OPEN);
            }

            if (!is_null($filter->issued)) {
                array_push($options, InspSFG20InspectionStatusType::ISSUED);
            }

            if (!is_null($filter->complete)) {
                array_push($options, InspSFG20InspectionStatusType::COMPLETE);
            }

            if (!is_null($filter->cancelled)) {
                array_push($options, InspSFG20InspectionStatusType::CANCELLED);
            }

            if (!is_null($filter->onHold)) {
                array_push($options, InspSFG20InspectionStatusType::ONHOLD);
            }

            if (!is_null($filter->closed)) {
                array_push($options, InspSFG20InspectionStatusType::CLOSED);
            }

            $inspectionStatus = InspSFG20InspectionStatus::whereIn('inspection_status_type_id', $options)->get();

            $statusIds = '';
            foreach ($inspectionStatus as $status) {
                if ($statusIds != '') {
                    $statusIds .= ', ';
                }
                $statusIds .= $status->inspection_status_id;
            }

            $query .= " AND inspection_status.inspection_status_id IN (" . $statusIds . ")";
        }
    }

    private function formatInputs($inputs)
    {
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($inputs);
            $inputs['code'] = Arr::first($reportFilterQuery->getValueFilterField('contract_code'));
            $inputs['name'] = Arr::first($reportFilterQuery->getValueFilterField('contract_name'));
            $inputs['supplier'] = Arr::first($reportFilterQuery->getValueFilterField('supplier_contact_id'));
            $inspectionDueDate = Arr::first($reportFilterQuery->getValueFilterField('inspection_due_date'));
            if (!empty($inspectionDueDate)) {
                $inputs['targetFrom'] = $inspectionDueDate[0];
                $inputs['targetTo'] = $inspectionDueDate[1];
            }
            $statusType = $reportFilterQuery->getValueFilterField('inspection_status_type_id');
            if (!empty($statusType)) {
                if (in_array(InspSFG20InspectionStatusType::DRAFT, $statusType)) {
                    $inputs['draft'] = 1;
                }
                if (in_array(InspSFG20InspectionStatusType::OPEN, $statusType)) {
                    $inputs['open'] = 1;
                }
                if (in_array(InspSFG20InspectionStatusType::COMPLETE, $statusType)) {
                    $inputs['complete'] = 1;
                }
                if (in_array(InspSFG20InspectionStatusType::CANCELLED, $statusType)) {
                    $inputs['cancelled'] = 1;
                }
                if (in_array(InspSFG20InspectionStatusType::ONHOLD, $statusType)) {
                    $inputs['onHold'] = 1;
                }
                if (in_array(InspSFG20InspectionStatusType::CLOSED, $statusType)) {
                    $inputs['closed'] = 1;
                }
            }
        }

        return $inputs;
    }
}
