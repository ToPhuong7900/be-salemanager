<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\Filters\BaseFilter;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\PupilPlacePlanning\PupilsService;
use Tfcloud\Services\BaseService;

class PppFilterQueryService extends BaseService
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    public function reAddPppQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $pupil = new PupilsService(
            $this->permissionService
        );
        $query = $pupil->pppFilter($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'academic_year', null)) {
            array_push($whereCodes, [
                'academic_year',
                'academic_year_id',
                'academic_year_code',
                $val,
                \Form::fieldLang('academicYear')
            ]);
        }

        if ($val = array_get($filterData, 'la_code', null)) {
            array_push($whereTexts, \Lang::get('text.la_code') . " contains '$val'");
        }

        if ($val = array_get($filterData, 'dfe_number', null)) {
            array_push($whereTexts, \Lang::get('text.dfe_number') . " contains '$val'");
        }

        if ($val = array_get($filterData, 'location', null)) {
            array_push($whereCodes, [
                'site',
                'site_id',
                'site_code',
                $val,
                \Form::fieldLang('location')
            ]);
        }

        if ($val = array_get($filterData, 'suffType', null)) {
            array_push($whereCodes, [
                'suff_type',
                'suff_type_id',
                'suff_type_code',
                $val,
                \Form::fieldLang('suffType')
            ]);
        }

        \Log::error('got to the end' . $query->toSql());
        return $query;
    }
}
