<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Services\Estate\GenChecklistItemService;
use Tfcloud\Services\PermissionService;

class GenChecklistFilterQueryService
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    public function reAddGenChecklistQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $service = new GenChecklistItemService($this->permissionService);
        $query = $service->filterAll($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'owner')) {
            array_push(
                $whereCodes,
                ['user', 'id', 'display_name', $val, "Owner"]
            );
        }

        if ($val = Common::iset($filterData['complete'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, 'Complete = Yes');
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, 'Complete = No');
                    break;
                default:
                    array_push($whereTexts, 'Complete = All');
                    break;
            }
        }

        if ($val = Common::iset($filterData['dueDateFrom'])) {
            array_push($whereTexts, "Due Date from {$val}");
        }

        if ($val = Common::iset($filterData['dueDateTo'])) {
            array_push($whereTexts, "Due Date to {$val}");
        }

        return $query;
    }
}
