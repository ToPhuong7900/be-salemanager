<?php

namespace Tfcloud\Services\Report\FilterQuery;

use DateTime;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Filters\Contract\ContractInspectionFilter;
use Tfcloud\Models\CrudAccessLevel;
use Tfcloud\Models\FinAuthStatus;
use Tfcloud\Models\GenTable;
use Tfcloud\Models\InspectionSFG20\InspSFG20InspectionStatus;
use Tfcloud\Models\InspectionSFG20\InspSFG20InspectionStatusType;
use Tfcloud\Models\InvoiceStatusType;
use Tfcloud\Models\Module;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\Contract\AFPService;
use Tfcloud\Services\Contract\CertificateService;
use Tfcloud\Services\Contract\ContractService;
use Tfcloud\Services\Contract\CreditService;
use Tfcloud\Services\Contract\InstructionService;
use Tfcloud\Services\Contract\InvoiceCreditService;
use Tfcloud\Services\Contract\InvoiceService;
use Tfcloud\Services\Inspection\InspectionService;
use Tfcloud\Services\PermissionService;

class ContractFilterQueryService extends BaseService
{
    protected $permissionService;
    protected $instructionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
        $this->instructionService = new InstructionService($this->permissionService);
        $this->inspectionService = new InspectionService($this->permissionService);
    }

    public function reAddContractQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $contractQuery = ContractService::filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Contract Code contains '{$val}'");
        }

        if ($val = Common::iset($filterData['contractType'])) {
            array_push($whereCodes, [
                'contract_type',
                'contract_type_id',
                'contract_type_name',
                $val, \Lang::get('text.contract_type')
            ]);
        }

        if ($val = Common::iset($filterData['name'])) {
            array_push($whereTexts, "Contract Name contains '{$val}'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Description contains '{$val}'");
        }

        if ($val = Common::iset($filterData['owner'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Owner']);
        }

        if ($val = Common::iset($filterData['supplier'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Supplier']);
        }

        if ($val = array_get($filterData, 'site_id')) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);

            if ($val = array_get($filterData, 'building_id')) {
                array_push($whereCodes, ['building', 'building_id', 'building_code', $val, "Building Code"]);

                if ($val = array_get($filterData, 'room_id')) {
                    array_push($whereCodes, ['room', 'room_id', 'room_number', $val, "Room No."]);
                }
            }
        }

        if ($val = Common::iset($filterData['active'])) {
            switch ($val) {
                case 'archived':
                    array_push($whereTexts, "Archive = 'Y'");
                    break;
                case 'all':
                    break;
                case 'active':
                default:
                    array_push($whereTexts, "Archive = 'N'");
                    break;
            }
        }

        return $contractQuery;
    }

    public function reAddContractAFPQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $contractQuery = AFPService::filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['afpCode'])) {
            array_push($whereTexts, "AFP Code contains '{$val}'");
        }

        if ($val = Common::iset($filterData['afpDesc'])) {
            array_push($whereTexts, "AFP Description contains '{$val}'");
        }

        if ($val = Common::iset($filterData['contCode'])) {
            array_push($whereTexts, "Contract Code contains '{$val}'");
        }

        if ($val = Common::iset($filterData['contInstCode'])) {
            array_push($whereTexts, "Contract Instruction Code contains '{$val}'");
        }

        if ($val = Common::iset($filterData['contractCertificateId'])) {
            array_push(
                $whereCodes,
                ['contract_certificate', 'contract_certificate_id', 'contract_certificate_code', $val, 'Certificate']
            );
        }

        if ($val = Common::iset($filterData['certificateCode'])) {
            array_push($whereTexts, "Certificate Code contains '{$val}'");
        }

        if ($val = Common::iset($filterData['parentCode'])) {
            array_push($whereTexts, "Parent Code contains '{$val}'");
        }

        if ($val = Common::iset($filterData['afpFinal'])) {
            array_push($whereTexts, "Final set to '{$val}'");
        }

        if ($val = Common::iset($filterData['supplier'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Supplier']);
        }

        if ($val = array_get($filterData, 'netTotalFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.net_total_from'), $val));
        }

        if ($val = array_get($filterData, 'netTotalTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.net_total_to'), $val));
        }

        if ($val = Common::iset($filterData['status'])) {
            array_push(
                $orCodes,
                [
                    'afp_status',
                    'afp_status_id',
                    'afp_status_desc',
                    implode(',', $val),
                    "Status"
                ]
            );
        }

        if ($val = Common::iset($filterData['createdFrom'])) {
            array_push($whereTexts, "Created From '{$val}'");
        }

        if ($val = Common::iset($filterData['createdTo'])) {
            array_push($whereTexts, "Created To '{$val}'");
        }

        if ($val = Common::iset($filterData['approvedFrom'])) {
            array_push($whereTexts, "Approved From '{$val}'");
        }

        if ($val = Common::iset($filterData['approvedTo'])) {
            array_push($whereTexts, "Approved To '{$val}'");
        }


        // Spot check
        if ($val = Common::iset($filterData['auditor'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Spot Check Auditor']);
        }

        if ($val = Common::iset($filterData['spotCheckRequired'])) {
            switch ($val) {
                case 'Y':
                    array_push($whereTexts, "Spot Check Required = 'Yes'");
                    break;
                case 'N':
                    array_push($whereTexts, "Spot Check Required = 'No'");
                    break;
                case 'all':
                    break;
            }
        }

        $spotCheckStatus = [];
        if ($val = array_get($filterData, 'spotCheckForApproval')) {
            $spotCheckStatus[] = FinAuthStatus::APPROVAL;
        }
        if ($val = array_get($filterData, 'spotCheckApproved')) {
            $spotCheckStatus[] = FinAuthStatus::APPROVED;
        }
        if ($val = array_get($filterData, 'spotCheckRejected')) {
            $spotCheckStatus[] = FinAuthStatus::REJECTED;
        }
        if (count($spotCheckStatus)) {
            array_push($orCodes, [
                'fin_auth_status',
                'fin_auth_status_id',
                'name',
                implode(',', $spotCheckStatus),
                "Spot Check Audit"
            ]);
        }

        //CONT18 Applications notes report.
        if ($viewName == 'vw_cont18') {
            if ($val = Common::iset($filterData['noteType'])) {
                array_push($whereCodes, [
                    'note_type',
                    'note_type_id',
                    'note_type_code',
                    $val, 'Note Type'
                ]);
            }

            if ($val = Common::iset($filterData['notePrivate'])) {
                switch ($val) {
                    case 'Y':
                        array_push($whereTexts, "Private Note = 'Yes'");
                        break;
                    case 'N':
                        array_push($whereTexts, "Private Note = 'No'");
                        break;
                    case 'all':
                        break;
                }
            }
        }

        return $contractQuery;
    }

    public function reAddContractCertificateQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $contractQuery = CertificateService::filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['certCode'])) {
            array_push($whereTexts, "Certificate Code contains '{$val}'");
        }

        if ($val = Common::iset($filterData['certDesc'])) {
            array_push($whereTexts, "Certificate Description contains '{$val}'");
        }

        if ($val = Common::iset($filterData['contCode'])) {
            array_push($whereTexts, "Contract Code contains '{$val}'");
        }

        if ($val = Common::iset($filterData['contInstCode'])) {
            array_push($whereTexts, "Contract Instruction Code contains '{$val}'");
        }

        if ($val = Common::iset($filterData['supplier'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Supplier']);
        }

        return $contractQuery;
    }

    public function reAddContractInstructionQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $contractQuery = $this->instructionService->filterAll($query, $filterData, false, $viewName);

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Contract Instructions Code contains '{$val}'");
        }

        if ($val = Common::iset($filterData['instructionType'])) {
            array_push($whereCodes, [
                'contract_instruction_type',
                'contract_instruction_type_id',
                'contract_instruction_type_name',
                $val, \Lang::get('text.contract_instruction_type')
            ]);
        }

        if ($val = Common::iset($filterData['financialYear'])) {
            array_push(
                $whereCodes,
                ['fin_year', 'fin_year_id', 'fin_year_code', $val, 'Financial Year']
            );
        }

        if ($val = Common::iset($filterData['supplier'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Supplier']);
        }

        if ($val = array_get($filterData, 'netTotalFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.net_total_from'), $val));
        }

        if ($val = array_get($filterData, 'netTotalTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.net_total_to'), $val));
        }

        // CB 2021-02-15: These don't appear to be used.
        // if ($val = array_get($filterData, 'completedFrom', null)) {
        //     array_push($whereTexts, 'Actual Complete Date From ' . " $val" . "'");
        // }

        // if ($val = array_get($filterData, 'completedTo', null)) {
        //     array_push($whereTexts, "Actual Complete Date To '" . " $val" . "'");
        // }

        if ($val = array_get($filterData, 'actualFrom', null)) {
            array_push($whereTexts, 'Actual Complete Date From ' . " $val" . "'");
        }

        if ($val = array_get($filterData, 'actualTo', null)) {
            array_push($whereTexts, "Actual Complete Date To '" . " $val" . "'");
        }

        if ($val = Common::iset($filterData['statusType'])) {
            array_push(
                $orCodes,
                [
                    'instruction_status_type',
                    'instruction_status_type_id',
                    'instruction_status_type_desc',
                    implode(',', $val),
                    "Status Type"
                ]
            );
        }

        if ($val = Common::iset($filterData['account'])) {
            array_push($whereCodes, [
                'fin_account',
                'fin_account_id',
                'fin_account_code',
                $val, \Lang::get('text.account')
            ]);
        }

        $this->reAddUserDefinesQuery(GenTable::CONTRACT_INSTR, $filterData, $whereCodes, $whereTexts);

        return $contractQuery;
    }

    public function reAddContractInvoiceQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $contractQuery = InvoiceService::filterAll($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'invoiceNo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.invoice_no'), $val));
        }

        if ($val = Common::iset($filterData['invoiceType'])) {
            array_push($whereCodes, [
                'contract_invoice_type',
                'contract_invoice_type_id',
                'contract_invoice_type_name',
                $val, \Lang::get('text.contract_invoice_type')
            ]);
        }

        if ($val = array_get($filterData, 'invoiceDesc', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.invoice_desc'), $val));
        }

        if ($val = Common::iset($filterData['certCode'])) {
            array_push($whereTexts, "Certificate Code contains '{$val}'");
        }

        // Special location
        if ($val = array_get($filterData, 'site_id')) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);

            if ($val = array_get($filterData, 'building_id')) {
                array_push($whereCodes, ['building', 'building_id', 'building_code', $val, "Building Code"]);
            }
        }

        if ($val = Common::iset($filterData['supplier'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, \Lang::get('text.supplier')]);
        }

        if ($val = Common::iset($filterData['owner'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, \Lang::get('text.owner')]);
        }

        if ($val = Common::iset($filterData['authoriser'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, \Lang::get('text.authoriser')]);
        }

        if ($val = Common::iset($filterData['account'])) {
            array_push($whereCodes, [
                'fin_account',
                'fin_account_id',
                'fin_account_code',
                $val, \Lang::get('text.account')
            ]);
        }

        if ($val = Common::iset($filterData['financialYear'])) {
            array_push($whereCodes, [
                'fin_year',
                'fin_year_id',
                'fin_year_code',
                $val, \Lang::get('text.financial_year')
            ]);
        }

        if ($val = array_get($filterData, 'netTotalFrom', 0)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.net_total_from'), $val));
        }

        if ($val = array_get($filterData, 'netTotalTo', 0)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.net_total_to'), $val));
        }

        if ($val = array_get($filterData, 'instruction_code', array_get($filterData, 'instructionCode', null))) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.linked_instruction_code'), $val));
        }

        if ($val = array_get($filterData, 'instruction_code', array_get($filterData, 'instructionCode', null))) {
            array_push($whereTexts, \Form::fieldLang('instruction_code_full') . " contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'received_from', array_get($filterData, 'receivedFrom', null))) {
            array_push($whereTexts, \Form::fieldLang('received_date_from') . " $val");
        }

        if ($val = array_get($filterData, 'received_to', array_get($filterData, 'receivedTo', null))) {
            array_push($whereTexts, \Form::fieldLang('to') . " $val");
        }

        if ($val = array_get($filterData, 'tax_from', array_get($filterData, 'taxFrom', null))) {
            array_push($whereTexts, \Form::fieldLang('tax_date_from') . " $val");
        }

        if ($val = array_get($filterData, 'tax_to', array_get($filterData, 'taxTo', null))) {
            array_push($whereTexts, \Form::fieldLang('to') . " $val");
        }

        $statusType = [];
        if (Common::iset($filterData['draft'])) {
            $statusType[] = InvoiceStatusType::DRAFT;
        }
        if (Common::iset($filterData['registered'])) {
            $statusType[] = InvoiceStatusType::REGISTERED;
        }
        if (Common::iset($filterData['approved'])) {
            $statusType[] = InvoiceStatusType::APPROVED;
        }
        if (Common::iset($filterData['posted'])) {
            $statusType[] = InvoiceStatusType::POSTED;
        }
        if (Common::iset($filterData['rejected'])) {
            $statusType[] = InvoiceStatusType::REJECTED;
        }
        if (count($statusType)) {
            array_push($orCodes, [
                'invoice_status_type',
                'invoice_status_type_id',
                'invoice_status_type_desc',
                implode(',', $statusType),
                \Lang::get('text.status_type')
            ]);
        }

        if ($val = Common::iset($filterData['status'])) {
            array_push($whereCodes, [
                'contract_invoice_status',
                'contract_invoice_status_id',
                'contract_invoice_status_desc',
                $val,
                'Status'
            ]);
        }

        if ($val = Common::iset($filterData['auditor'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Spot Check Auditor']);
        }

        if ($val = Common::iset($filterData['spotCheckRequired'])) {
            switch ($val) {
                case 'Y':
                    array_push($whereTexts, "Spot Check Required = 'Yes'");
                    break;
                case 'N':
                    array_push($whereTexts, "Spot Check Required = 'No'");
                    break;
                case 'all':
                    break;
            }
        }

        $spotCheckStatus = [];
        if ($val = array_get($filterData, 'spotCheckForApproval')) {
            $spotCheckStatus[] = FinAuthStatus::APPROVAL;
        }
        if ($val = array_get($filterData, 'spotCheckApproved')) {
            $spotCheckStatus[] = FinAuthStatus::APPROVED;
        }
        if ($val = array_get($filterData, 'spotCheckRejected')) {
            $spotCheckStatus[] = FinAuthStatus::REJECTED;
        }
        if (count($spotCheckStatus)) {
            array_push($orCodes, [
                'fin_auth_status',
                'fin_auth_status_id',
                'name',
                implode(',', $spotCheckStatus),
                "Spot Check Audit"
            ]);
        }

        $authorisationType = [];
        if (Common::iset($filterData['authOpen'])) {
            $authorisationType[] = FinAuthStatus::OPEN;
        }
        if (Common::iset($filterData['authApproval'])) {
            $authorisationType[] = FinAuthStatus::APPROVAL;
        }
        if (Common::iset($filterData['authApproved'])) {
            $authorisationType[] = FinAuthStatus::APPROVED;
        }
        if (Common::iset($filterData['authRejected'])) {
            $authorisationType[] = FinAuthStatus::REJECTED;
        }
        if (count($authorisationType)) {
            array_push($orCodes, [
                'fin_auth_status',
                'fin_auth_status_id',
                'name',
                implode(',', $authorisationType),
                'Authorisation'
            ]);
        }

        return $contractQuery;
    }

    public function reAddContractCreditQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $creditQuery = CreditService::filterAll($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'creditNo', null)) {
            array_push(
                $whereTexts,
                sprintf(\Lang::get('validation.attributes.contract_credit_no') . " contains '%s'", $val)
            );
        }

        if ($val = Common::iset($filterData['creditType'])) {
            array_push($whereCodes, [
                'contract_credit_type',
                'contract_credit_type_id',
                'contract_credit_type_name',
                $val, \Lang::get('text.contract_credit_type')
            ]);
        }

        if ($val = array_get($filterData, 'creditDesc', null)) {
            array_push($whereTexts, sprintf(\Lang::get('validation.attributes.invoice_desc') . " contains '%s'", $val));
        }

        if ($val = array_get($filterData, 'invoiceNo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.invoice_no'), $val));
        }

        if ($val = array_get($filterData, 'instructionCode', null)) {
            array_push($whereTexts, sprintf("Instruction Code contains '%s'", $val));
        }

        if ($val = array_get($filterData, 'contractCode', null)) {
            array_push($whereTexts, sprintf("Contract Code contains '%s'", $val));
        }

        if ($val = array_get($filterData, 'inspectionCode', null)) {
            array_push($whereTexts, sprintf("Inspection Code contains '%s'", $val));
        }

        if ($val = array_get($filterData, 'afpCode', null)) {
            array_push($whereTexts, sprintf("Application Code contains '%s'", $val));
        }

        if ($val = Common::iset($filterData['owner'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, \Lang::get('text.owner')]);
        }

        if ($val = array_get($filterData, 'site_id')) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);

            if ($val = array_get($filterData, 'building_id')) {
                array_push($whereCodes, ['building', 'building_id', 'building_code', $val, "Building Code"]);
            }
        }

        if ($val = Common::iset($filterData['supplier'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, \Lang::get('text.supplier')]);
        }

        if ($val = Common::iset($filterData['authoriser'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, \Lang::get('text.authoriser')]);
        }

        if ($val = Common::iset($filterData['account'])) {
            array_push($whereCodes, [
                'fin_account',
                'fin_account_id',
                'fin_account_code',
                $val, \Lang::get('text.account')
            ]);
        }

        if ($val = Common::iset($filterData['financialYear'])) {
            array_push($whereCodes, [
                'fin_year',
                'fin_year_id',
                'fin_year_code',
                $val, \Lang::get('text.financial_year')
            ]);
        }

        if ($val = array_get($filterData, 'netTotalFrom', 0)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.net_total_from'), $val));
        }

        if ($val = array_get($filterData, 'netTotalTo', 0)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.net_total_to'), $val));
        }

        if ($val = array_get($filterData, 'received_from', array_get($filterData, 'receivedFrom', null))) {
            array_push($whereTexts, \Form::fieldLang('received_date_from') . " $val");
        }

        if ($val = array_get($filterData, 'received_to', array_get($filterData, 'receivedTo', null))) {
            array_push($whereTexts, \Form::fieldLang('to') . " $val");
        }

        if ($val = array_get($filterData, 'tax_from', array_get($filterData, 'taxFrom', null))) {
            array_push($whereTexts, \Form::fieldLang('tax_date_from') . " $val");
        }

        if ($val = array_get($filterData, 'tax_to', array_get($filterData, 'taxTo', null))) {
            array_push($whereTexts, \Form::fieldLang('to') . " $val");
        }

        $statusType = [];
        if (Common::iset($filterData['draft'])) {
            $statusType[] = InvoiceStatusType::DRAFT;
        }
        if (Common::iset($filterData['registered'])) {
            $statusType[] = InvoiceStatusType::REGISTERED;
        }
        if (Common::iset($filterData['approved'])) {
            $statusType[] = InvoiceStatusType::APPROVED;
        }
        if (Common::iset($filterData['posted'])) {
            $statusType[] = InvoiceStatusType::POSTED;
        }
        if (Common::iset($filterData['rejected'])) {
            $statusType[] = InvoiceStatusType::REJECTED;
        }
        if (count($statusType)) {
            array_push($orCodes, [
                'invoice_status_type',
                'invoice_status_type_id',
                'invoice_status_type_desc',
                implode(',', $statusType),
                \Lang::get('text.status_type')
            ]);
        }

        if ($val = Common::iset($filterData['status'])) {
            array_push($whereCodes, [
                'contract_credit_status',
                'contract_credit_status_id',
                'contract_credit_status_desc',
                $val,
                'Status'
            ]);
        }

        $authorisationType = [];
        if (Common::iset($filterData['authOpen'])) {
            $authorisationType[] = FinAuthStatus::OPEN;
        }
        if (Common::iset($filterData['authApproval'])) {
            $authorisationType[] = FinAuthStatus::APPROVAL;
        }
        if (Common::iset($filterData['authApproved'])) {
            $authorisationType[] = FinAuthStatus::APPROVED;
        }
        if (Common::iset($filterData['authRejected'])) {
            $authorisationType[] = FinAuthStatus::REJECTED;
        }
        if (count($authorisationType)) {
            array_push($orCodes, [
                'fin_auth_status',
                'fin_auth_status_id',
                'name',
                implode(',', $authorisationType),
                'Authorisation'
            ]);
        }

        return $creditQuery;
    }

    public function reAddContractInvoiceCreditQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $invCreditQuery = InvoiceCreditService::filterAll($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'creditNo', null)) {
            array_push(
                $whereTexts,
                sprintf(\Lang::get('validation.attributes.contract_credit_no') . " contains '%s'", $val)
            );
        }

        if ($val = array_get($filterData, 'creditDesc', null)) {
            array_push($whereTexts, sprintf("Credit Description contains '%s'", $val));
        }

        if ($val = array_get($filterData, 'invoiceNo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.invoice_no'), $val));
        }

        if ($val = array_get($filterData, 'invoiceDesc', null)) {
            array_push($whereTexts, sprintf("Invoice Description contains '%s'", $val));
        }

        if ($val = array_get($filterData, 'instructionCode', null)) {
            array_push($whereTexts, sprintf("Instruction Code contains '%s'", $val));
        }

        if ($val = array_get($filterData, 'contractCode', null)) {
            array_push($whereTexts, sprintf("Contract Code contains '%s'", $val));
        }

        if ($val = array_get($filterData, 'inspectionCode', null)) {
            array_push($whereTexts, sprintf("Inspection Code contains '%s'", $val));
        }

        if ($val = array_get($filterData, 'afpCode', null)) {
            array_push($whereTexts, sprintf("Application Code contains '%s'", $val));
        }

        if ($val = Common::iset($filterData['owner'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, \Lang::get('text.owner')]);
        }

        if ($val = Common::iset($filterData['authoriser'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, \Lang::get('text.authoriser')]);
        }

        if ($val = array_get($filterData, 'site_id')) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);

            if ($val = array_get($filterData, 'building_id')) {
                array_push($whereCodes, ['building', 'building_id', 'building_code', $val, "Building Code"]);
            }
        }

        if ($val = Common::iset($filterData['supplier'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, \Lang::get('text.supplier')]);
        }

        if ($val = Common::iset($filterData['account'])) {
            array_push($whereCodes, [
                'fin_account',
                'fin_account_id',
                'fin_account_code',
                $val, \Lang::get('text.account')
            ]);
        }

        if ($val = Common::iset($filterData['financialYear'])) {
            array_push($whereCodes, [
                'fin_year',
                'fin_year_id',
                'fin_year_code',
                $val, \Lang::get('text.financial_year')
            ]);
        }

        if ($val = array_get($filterData, 'netTotalFrom', 0)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.net_total_from'), $val));
        }

        if ($val = array_get($filterData, 'netTotalTo', 0)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.net_total_to'), $val));
        }

        if ($val = array_get($filterData, 'received_from', array_get($filterData, 'receivedFrom', null))) {
            array_push($whereTexts, \Form::fieldLang('received_date_from') . " $val");
        }

        if ($val = array_get($filterData, 'received_to', array_get($filterData, 'receivedTo', null))) {
            array_push($whereTexts, \Form::fieldLang('to') . " $val");
        }

        if ($val = array_get($filterData, 'tax_from', array_get($filterData, 'taxFrom', null))) {
            array_push($whereTexts, \Form::fieldLang('tax_date_from') . " $val");
        }

        if ($val = array_get($filterData, 'tax_to', array_get($filterData, 'taxTo', null))) {
            array_push($whereTexts, \Form::fieldLang('to') . " $val");
        }

        $statusType = [];
        if (Common::iset($filterData['draft'])) {
            $statusType[] = InvoiceStatusType::DRAFT;
        }
        if (Common::iset($filterData['registered'])) {
            $statusType[] = InvoiceStatusType::REGISTERED;
        }
        if (Common::iset($filterData['approved'])) {
            $statusType[] = InvoiceStatusType::APPROVED;
        }
        if (Common::iset($filterData['posted'])) {
            $statusType[] = InvoiceStatusType::POSTED;
        }
        if (Common::iset($filterData['rejected'])) {
            $statusType[] = InvoiceStatusType::REJECTED;
        }
        if (count($statusType)) {
            array_push($orCodes, [
                'invoice_status_type',
                'invoice_status_type_id',
                'invoice_status_type_desc',
                implode(',', $statusType),
                \Lang::get('text.status_type')
            ]);
        }

        $authorisationType = [];
        if (Common::iset($filterData['authOpen'])) {
            $authorisationType[] = FinAuthStatus::OPEN;
        }
        if (Common::iset($filterData['authApproval'])) {
            $authorisationType[] = FinAuthStatus::APPROVAL;
        }
        if (Common::iset($filterData['authApproved'])) {
            $authorisationType[] = FinAuthStatus::APPROVED;
        }
        if (Common::iset($filterData['authRejected'])) {
            $authorisationType[] = FinAuthStatus::REJECTED;
        }
        if (count($authorisationType)) {
            array_push($orCodes, [
                'fin_auth_status',
                'fin_auth_status_id',
                'name',
                implode(',', $authorisationType),
                'Authorisation'
            ]);
        }

        return $invCreditQuery;
    }

    public function reAddContractInspectionQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        // default filtering done by ContractService::filterAll
        $query->where($viewName . '.archive', '=', CommonConstant::DATABASE_VALUE_NO);

        if (\Auth::user()->isContractor()) {
            $query->where($viewName . '.supplier_contact_id', \Auth::user()->contractor->getKey());
        }

        // no default filtering is done by [Contract] InstructionService->filterAll

        // default filtering done by InspectionService->filterAll
        /** If a user doesn't have plant module permissions we need to prevent them from seeing inspections that have
         *  plant data associated to them  */
        if (
            !$this->permissionService->hasModuleAccess(
                Module::MODULE_PLANT,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
            )
        ) {
            $query->where($viewName . '.plant_id', null)
                ->where($viewName . '.site_plant_system_id', null)
                ->where($viewName . '.plant_group_id', null)
                ->where($viewName . '.plant_subgroup_id', null)
                ->where($viewName . '.plant_type_id', null);
        }

        if (
            \Auth::user()->usergroup->usergroupType->usergroup_type_id ==
            PermissionService::USERGROUP_TYPE_ID_SELF_SERVICE
        ) {
            $query->where("{$viewName}.inspection_status_type_id", '<>', InspSFG20InspectionStatusType::DRAFT);
        }
        /** Contractor should only be able to see inspections assigned to them (this is done above)
            and that are issued or complete */
        if (\Auth::user()->isContractor()) {
            $query->whereIn(
                "{$viewName}.inspection_status_type_id",
                [InspSFG20InspectionStatusType::ISSUED, InspSFG20InspectionStatusType::COMPLETE]
            );
        }

        if (\Auth::user()->isSelfServiceUser()) {
            $query->where("{$viewName}.visible_to_self_service", CommonConstant::DATABASE_VALUE_YES);
        }

        // Bespoke filtering is required
        $filter = new ContractInspectionFilter($filterData);

        if (!is_null($filter->contractCode)) {
            $query->where($viewName . '.contract_code', 'like', '%' . trim($filter->contractCode) . '%');
        }

        if (!is_null($filter->financialYear)) {
            $query->where($viewName . '.fin_year_id', '=', $filter->financialYear);
        }

        if (!is_null($filter->supplier)) {
            $query->where($viewName . '.supplier_contact_id', '=', $filter->supplier);
        }

        if (!is_null($filter->instructionStatusType)) {
            $instructionStatusTypes = json_decode(json_encode($filter->instructionStatusType), true);
            $query->whereIn($viewName . '.instruction_status_type_id', $instructionStatusTypes);
        }

        if (!is_null($filter->account)) {
            $query->where($viewName . '.fin_account_id', '=', $filter->account);
        }

        if (!is_null($filter->instructionCode)) {
            $query->where(
                $viewName . '.contract_instruction_code',
                'like',
                '%' . trim($filter->instructionCode) . '%'
            );
        }

        if (!is_null($filter->targetFrom) && $targetFrom = DateTime::createFromFormat('d/m/Y', $filter->targetFrom)) {
            $targetFrom  = $targetFrom->format('Y-m-d');
            if (Common::valueYorNtoBoolean($filter->includeDateFloat)) {
                $query->where(function ($sub) use ($viewName, $targetFrom) {
                    $sub->orWhere(function ($sub1) use ($viewName, $targetFrom) {
                        $sub1->where(
                            \DB::raw("DATEDIFF('{$targetFrom}', {$viewName}.inspection_due_date)"),
                            '<=',
                            '0'
                        );
                        $sub1->where("{$viewName}.statutory", CommonConstant::DATABASE_VALUE_ONE);
                    });
                    $sub->orWhere(function ($sub2) use ($viewName, $targetFrom) {
                        $sub2->where(
                            \DB::raw("DATEDIFF('{$targetFrom}', {$viewName}.inspection_due_date)"),
                            '<=',
                            \DB::raw("{$viewName}.float_days")
                        );
                        $sub2->where("{$viewName}.statutory", CommonConstant::DATABASE_VALUE_ZERO);
                    });
                });
            } else {
                $query->where(
                    "{$viewName}.inspection_due_date",
                    '>=',
                    $targetFrom
                );
            }
        }

        if (!is_null($filter->targetTo) && $targetTo = DateTime::createFromFormat('d/m/Y', $filter->targetTo)) {
            $targetTo = $targetTo->format('Y-m-d');
            $query->where(
                "{$viewName}.inspection_due_date",
                '<=',
                $targetTo
            );
        }

        if (!is_null($filter->inspection_status_id)) {
            $query->where("{$viewName}.inspection_status_id", $filter->inspection_status_id);
        }

        if (
            !is_null($filter->draft) ||
            !is_null($filter->open) ||
            !is_null($filter->issued) ||
            !is_null($filter->complete) ||
            !is_null($filter->cancelled) ||
            !is_null($filter->onHold) ||
            !is_null($filter->closed)
        ) {
            $options = [];

            if (
                !is_null($filter->draft) && \Auth::user()->usergroup->usergroupType->usergroup_type_id !=
                PermissionService::USERGROUP_TYPE_ID_SELF_SERVICE
            ) {
                array_push($options, InspSFG20InspectionStatusType::DRAFT);
            }

            if (!is_null($filter->open)) {
                array_push($options, InspSFG20InspectionStatusType::OPEN);
            }

            if (!is_null($filter->issued)) {
                array_push($options, InspSFG20InspectionStatusType::ISSUED);
            }

            if (!is_null($filter->complete)) {
                array_push($options, InspSFG20InspectionStatusType::COMPLETE);
            }

            if (!is_null($filter->cancelled)) {
                array_push($options, InspSFG20InspectionStatusType::CANCELLED);
            }

            if (!is_null($filter->onHold)) {
                array_push($options, InspSFG20InspectionStatusType::ONHOLD);
            }

            if (!is_null($filter->closed)) {
                array_push($options, InspSFG20InspectionStatusType::CLOSED);
            }

            $inspectionStatus = InspSFG20InspectionStatus::whereIn('inspection_status_type_id', $options)->get();

            $statusIds = [];

            foreach ($inspectionStatus as $status) {
                array_push($statusIds, $status->inspection_status_id);
            }

            $query->whereIn("{$viewName}.inspection_status_type_id", $options);
        }

        if (!is_null($filter->inspection)) {
            $query->where("{$viewName}.inspection_code", 'like', '%' . $filter->inspection . '%');
        }

        if (!is_null($filter->inspection_group_id)) {
            $query->where("{$viewName}.inspection_group_id", $filter->inspection_group_id);
        }

        if (!is_null($filter->inspection_type_id)) {
            $query->where("{$viewName}.inspection_type_id", $filter->inspection_type_id);
        }

        if (!is_null($filter->statutory) && $filter->statutory != 'all') {
            $query->where("{$viewName}.statutory", $filter->statutory);
        }

        if (!is_null($filter->owner)) {
            $query->where("{$viewName}.owner_user_id", $filter->owner);
        }

        if (!is_null($filter->site_id)) {
            $query->where("{$viewName}.site_id", $filter->site_id);
        }

        if (!is_null($filter->building_id)) {
            $query->where("{$viewName}.building_id", $filter->building_id);
        }

        if (!is_null($filter->room_id)) {
            $query->where("{$viewName}.room_id", $filter->room_id);
        }

        // hidden contract_instruction_id can come from the Contract Instruction Inspections list page filter
        if (isset($filter->contract_instruction_id) && !is_null($filter->contract_instruction_id)) {
            $query->where("{$viewName}.contract_instruction_id", $filter->contract_instruction_id);
        }

        // series_list can come from Contract Instruction Inspections list page filter
        if (isset($filter->series_list) && !is_null($filter->series_list) && $filter->series_list != 'N') {
            $query->whereNull("next_inspection_id");
        }

        // Where text
        if ($val = Common::iset($filterData['contractCode'])) {
            array_push($whereTexts, "Contract Code contains '{$val}'");
        }

        if ($val = Common::iset($filterData['financialYear'])) {
            array_push(
                $whereCodes,
                ['fin_year', 'fin_year_id', 'fin_year_code', $val, 'Financial Year']
            );
        }

        if ($val = Common::iset($filterData['supplier'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Supplier']);
        }

        if ($val = Common::iset($filterData['instructionStatusType'])) {
            array_push(
                $orCodes,
                [
                    'instruction_status_type',
                    'instruction_status_type_id',
                    'instruction_status_type_desc',
                    implode(',', $val),
                    "Instruction Status Type"
                ]
            );
        }

        if ($val = Common::iset($filterData['account'])) {
            array_push($whereCodes, [
                'fin_account',
                'fin_account_id',
                'fin_account_code',
                $val, \Lang::get('text.account')
            ]);
        }

        if ($val = Common::iset($filterData['instructionCode'])) {
            array_push($whereTexts, "Instruction Code contains '{$val}'");
        }

        if ($val = Common::iset($filterData['targetFrom'])) {
            array_push($whereTexts, "Inspection Due Date From '{$val}'");
        }

        if ($val = Common::iset($filterData['targetTo'])) {
            array_push($whereTexts, "Inspection Due Date To '{$val}'");
        }

        if (Common::iset($filterData['includeDateFloat'])) {
            if (Common::valueYorNtoBoolean($filterData['includeDateFloat']) && Common::iset($filterData['targetTo'])) {
                array_push($whereTexts, "Include Date Float = Y");
            }
        }

        if ($val = Common::iset($filterData['contract_instruction_id'])) {
            array_push(
                $whereCodes,
                [
                    'contract_instruction',
                    'contract_instruction_id',
                    'contract_instruction_code',
                    $val,
                    'Contract Instruction'
                ]
            );
        }

        if ($val = Common::iset($filterData['inspection_status_id'])) {
            array_push(
                $whereCodes,
                array('inspection_status', 'inspection_status_id', 'code', $val, 'Inspection Status')
            );
        }

        $statusType = [];
        if (Common::iset($filterData['draft'])) {
            array_push($statusType, InspSFG20InspectionStatusType::DRAFT);
        }
        if (Common::iset($filterData['open'])) {
            array_push($statusType, InspSFG20InspectionStatusType::OPEN);
        }
        if (Common::iset($filterData['issued'])) {
            array_push($statusType, InspSFG20InspectionStatusType::ISSUED);
        }
        if (Common::iset($filterData['complete'])) {
            array_push($statusType, InspSFG20InspectionStatusType::COMPLETE);
        }
        if (Common::iset($filterData['cancelled'])) {
            array_push($statusType, InspSFG20InspectionStatusType::CANCELLED);
        }
        if (Common::iset($filterData['onHold'])) {
            array_push($statusType, InspSFG20InspectionStatusType::ONHOLD);
        }
        if (Common::iset($filterData['closed'])) {
            array_push($statusType, InspSFG20InspectionStatusType::CLOSED);
        }
        if (is_array($statusType) && count($statusType)) {
            array_push(
                $orCodes,
                array(
                    'inspection_status_type',
                    'inspection_status_type_id',
                    'inspection_status_type_code',
                    $statusType,
                    'Inspection Status Type'
                )
            );
        }

        if ($val = Common::iset($filterData['inspection'])) {
            array_push($whereTexts, "Inspection  Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['inspection_group_id'])) {
            array_push(
                $whereCodes,
                array('inspection_group', 'inspection_group_id', 'inspection_group_code', $val, 'Inspection Group')
            );
        }

        if ($val = Common::iset($filterData['inspection_type_id'])) {
            array_push(
                $whereCodes,
                array('inspection_type', 'inspection_type_id', 'inspection_type_code', $val, 'Inspection Type')
            );
        }

        $val = array_get($filterData, 'statutory', null);
        if (!is_null($val)) {
            if ($val == '1') {
                array_push($whereTexts, "Statutory = Yes");
            } elseif ($val == '0') {
                array_push($whereTexts, "Statutory = No");
            }
        }

        if ($val = Common::iset($filterData['owner'])) {
            array_push(
                $whereCodes,
                array('user', 'id', 'display_name', $val, 'Owner')
            );
        }

        if ($val = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);
        }

        if ($val = Common::iset($filterData['building_id'])) {
            array_push(
                $whereCodes,
                ['building', 'building_id', 'building_code', $val, "Building Code"]
            );
        }

        if ($val = Common::iset($filterData['room_id'])) {
            array_push($whereCodes, ['room', 'room_id', 'room_number', $val, "Room Number"]);
        }

        $val = array_get($filterData, 'series_list', null);
        if (!is_null($val)) {
            if ($val == 'Y') {
                array_push($whereTexts, "Series List = Series");
            } elseif ($val == 'N') {
                array_push($whereTexts, "Series List = All");
            }
        }

        return $query;
    }
}
