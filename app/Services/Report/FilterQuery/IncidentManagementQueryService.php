<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\models\IncidentManagement\IncidentStatusType;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\UserDefinedService;
use Tfcloud\Services\IncidentManagement\IncidentService;
use Tfcloud\Services\IncidentManagement\IncidentActionService;

class IncidentManagementQueryService extends BaseService
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
        $this->userDefinedService = new UserDefinedService();
    }

    public function reAddIncidentRecordsQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $query = (new IncidentService($this->permissionService))->filterAll($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'incidentCode')) {
            array_push($whereTexts, "Code contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'incidentDesc')) {
            array_push($whereTexts, "Description contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'dateFrom', null)) {
            array_push($whereTexts, "Date From '" . " $val" . "'");
        }

        if ($val = array_get($filterData, 'dateTo', null)) {
            array_push($whereTexts, "Date To '" . " $val" . "'");
        }

        // Special location
        if ($val = array_get($filterData, 'site_id')) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);

            if ($val = array_get($filterData, 'building_id')) {
                array_push($whereCodes, ['building', 'building_id', 'building_code', $val, "Building Code"]);

                if ($val = array_get($filterData, 'room_id')) {
                    array_push($whereCodes, ['room', 'room_id', 'room_number', $val, "Room Number"]);
                }
            }
        }


        if ($val = array_get($filterData, 'incidentStatus', null)) {
            array_push($orCodes, [
                'inc_incident_status_type',
                'inc_incident_status_type_id',
                'inc_incident_status_type_code',
                $val,
                'Status'
            ]);
        }

        if ($val = array_get($filterData, 'incidentType', null)) {
            array_push($whereCodes, [
                'inc_incident_type',
                'inc_incident_type_id',
                'inc_incident_type_code',
                $val,
                'Type of Incident'
            ]);
        }

        if ($val = array_get($filterData, 'incidentSeverity', null)) {
            array_push($whereCodes, [
                'inc_incident_severity',
                'inc_incident_severity_id',
                'inc_incident_severity_code',
                $val,
                'Severity of Incident'
            ]);
        }

        if ($val = Common::iset($filterData['reportedBy'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Reported By']);
        }

        if ($val = Common::iset($filterData['riddorSubmitted'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.riddor_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.riddor_no'));
                    break;
                case CommonConstant::DATABASE_VALUE_NA:
                    array_push($whereTexts, \Lang::get('text.report_texts.riddor_na'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.riddor_all'));
                    break;
            }
        }

        if ($val = array_get($filterData, 'injuredPeronName')) {
            array_push($whereTexts, "Injured Person Name '" . $val . "'");
        }

        if ($val = array_get($filterData, 'plant_type_id', null)) {
            array_push($whereCodes, [
                'plant_type',
                'plant_type_id',
                'plant_type_code',
                $val,
                'Plant Type'
            ]);
        }

        if ($val = array_get($filterData, 'plant_id', null)) {
            array_push($whereCodes, ['plant', 'plant_id', 'plant_code', $val, 'Plant']);
        }

        if ($val = array_get($filterData, 'plant_group_id', null)) {
            array_push($whereCodes, [
                'plant_group',
                'plant_group_id',
                'plant_group_code',
                $val,
                'Plant Group'
            ]);
        }

        if ($val = array_get($filterData, 'plant_subgroup_id', null)) {
            array_push($whereCodes, [
                'plant_subgroup',
                'plant_subgroup_id',
                'plant_subgroup_code',
                $val,
                'Plant Sub Group'
            ]);
        }

        return $query;
    }

    public function reAddIncidentActionsQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $query = (new IncidentActionService($this->permissionService))->filterAll($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'targetFrom', null)) {
            array_push($whereTexts, "Target From '" . " $val" . "'");
        }

        if ($val = array_get($filterData, 'targetTo', null)) {
            array_push($whereTexts, "Target To '" . " $val" . "'");
        }

        if ($val = array_get($filterData, 'actionType', null)) {
            array_push($whereCodes, [
                'inc_incident_action_type',
                'inc_incident_action_type_id',
                'inc_incident_action_type_code',
                $val,
                'Action Type'
            ]);
        }

        if ($val = array_get($filterData, 'actionPriority', null)) {
            array_push($whereCodes, [
                'inc_incident_action_priority',
                'inc_incident_action_priority_id',
                'inc_incident_action_priority_code',
                $val,
                'Action Priority'
            ]);
        }

        if ($val = array_get($filterData, 'priceBand', null)) {
            array_push($whereCodes, [
                'inc_incident_action_price_band',
                'inc_incident_action_price_band_id',
                'inc_incident_action_price_band_code',
                $val,
                'Price Band'
            ]);
        }

        if ($val = Common::iset($filterData['owner'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Owner']);
        }

        // Special location
        if ($val = array_get($filterData, 'site_id')) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);

            if ($val = array_get($filterData, 'building_id')) {
                array_push($whereCodes, ['building', 'building_id', 'building_code', $val, "Building Code"]);

                if ($val = array_get($filterData, 'room_id')) {
                    array_push($whereCodes, ['room', 'room_id', 'room_number', $val, "Room Number"]);
                }
            }
        }

        if ($val = array_get($filterData, 'code')) {
            array_push($whereTexts, "Code contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'responsibleParty')) {
            array_push($whereTexts, "Responsible Party contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'comments')) {
            array_push($whereTexts, "Comments contains '" . $val . "'");
        }

        return $query;
    }
}
