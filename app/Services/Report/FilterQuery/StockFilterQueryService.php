<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Models\StockGRNStatus;
use Tfcloud\Models\StockIssueStatus;
use Tfcloud\Models\StockJobReturnStatus;
use Tfcloud\Models\StockReturnStatus;
use Tfcloud\Models\StockStocktakeStatus;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\Stock\Action\StockIssueService;
use Tfcloud\Services\Stock\Action\StockJobReturnService;
use Tfcloud\Services\Stock\StockGRNService;
use Tfcloud\Services\Stock\StockInventoryService;
use Tfcloud\Services\Stock\StockItemService;
use Tfcloud\Services\Stock\StockReturnService;
use Tfcloud\Services\Stock\StockStoreService;
use Tfcloud\Services\Stock\StockStocktakeService;
use Tfcloud\Services\Stock\Action\StockTransactionService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\UserDefinedService;

class StockFilterQueryService extends BaseService
{
    protected $permissionService;
    protected $userDefinedService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
        $this->userDefinedService = new UserDefinedService();
    }

    public function reAddInventoryQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $inventoryService = new StockInventoryService($this->permissionService);
        $inventoryQuery = $inventoryService->filterAll($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'stock_inventory_code', array_get($filterData, 'stock_inventory_code'))) {
            array_push($whereTexts, sprintf("Inventory Code contains '%s'", $val));
        }

        if ($val = array_get($filterData, 'stock_item_id')) {
            array_push($whereCodes, ['stock_item', 'stock_item_id', 'stock_item_code', $val, 'Stock Item']);
        }

        if ($val = array_get($filterData, 'stock_store_id')) {
            array_push($whereCodes, ['stock_store', 'stock_store_id', 'stock_store_code', $val, 'Store']);
        }

        if ($val = array_get($filterData, 'stock_category_id')) {
            array_push($whereCodes, ['stock_category', 'stock_category_id', 'stock_category_code', $val, 'Category']);
        }

        if ($val = array_get($filterData, 'stock_sub_category_id')) {
            array_push(
                $whereCodes,
                ['stock_sub_category', 'stock_sub_category_id', 'stock_sub_category_code', $val, 'Sub Category']
            );
        }


        if ($val = array_get($filterData, 'stock_level', 0)) {
            if ($val == 1) {
                array_push($whereTexts, "Qty In Stock = 'Above Reorder Level'");
            } elseif ($val == 2) {
                array_push($whereTexts, "Qty In Stock = 'On Or Below Reorder Level'");
            }
        }

        return $inventoryQuery;
    }
}
