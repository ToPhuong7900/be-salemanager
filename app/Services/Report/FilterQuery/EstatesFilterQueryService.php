<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Filters\Audit\AuditFilter;
use Tfcloud\Models\Audit;
use Tfcloud\Models\EstRecordType;
use Tfcloud\Models\GenTable;
use Tfcloud\Models\LeaseOutStatusType;
use Tfcloud\Models\Estate\EstScServiceChargeStatus;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\Estate\AcquisitionService;
use Tfcloud\Services\Estate\AgreementService;
use Tfcloud\Services\Estate\DeedPacketService;
use Tfcloud\Services\Estate\DisposalService;
use Tfcloud\Services\Estate\EstBreakDateService;
use Tfcloud\Services\Estate\EstateSalesInvoiceService;
use Tfcloud\Services\Estate\EstateUnitService;
use Tfcloud\Services\Estate\EstChargeService;
use Tfcloud\Services\Estate\EstReviewService;
use Tfcloud\Services\Estate\FeeReceiptService;
use Tfcloud\Services\Estate\FreeholdService;
use Tfcloud\Services\Estate\InvoiceGenerationPlanService;
use Tfcloud\Services\Estate\LeaseInService;
use Tfcloud\Services\Estate\LeaseOutAlienationService;
use Tfcloud\Services\Estate\LeaseOutLetuService;
use Tfcloud\Services\Estate\LettingPacketService;
use Tfcloud\Services\Estate\LeaseOutService;
use Tfcloud\Services\Estate\LettableUnitService;
use Tfcloud\Services\Estate\NotificationOfPaymentService;
use Tfcloud\Services\Estate\RentalPayment\EstRentalPaymentService;
use Tfcloud\Services\Estate\RentalPayment\RentalPaymentGenPlanService;
use Tfcloud\Services\Estate\ScServiceCharge\ScActualService;
use Tfcloud\Services\Estate\ScServiceCharge\ScApportionmentService;
use Tfcloud\Services\Estate\ScServiceCharge\ScLetuService;
use Tfcloud\Services\Estate\ScServiceCharge\ScServiceChargePermissionService;
use Tfcloud\Services\Estate\ScServiceCharge\ScServiceChargeService;
use Tfcloud\Services\Estate\ScServiceCharge\ScScheduleService;
use Tfcloud\Services\Estate\ValuationService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\UserDefinedService;

class EstatesFilterQueryService extends BaseService
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    public function reAddEstatesFreeholdQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $freeholdService = new FreeholdService($this->permissionService);
        $freeholdQuery = $freeholdService->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Freehold Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Freehold Description contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['contact'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Landlord']);
        }

        if ($val = Common::iset($filterData['freeholdStatus'])) {
            array_push(
                $whereCodes,
                ['freehold_status', 'freehold_status_id', 'freehold_status_code', $val, 'Status']
            );
        }

        if ($val = Common::iset($filterData['deedPacket'])) {
            array_push(
                $whereCodes,
                ['deed_packet', 'deed_packet_id', 'deed_packet_code', $val, 'Deed Packet']
            );
        }

        if ($val = Common::iset($filterData['owner'])) {
            array_push(
                $whereCodes,
                ['user', 'id', 'display_name', $val, 'Owner User']
            );
        }

        if ($val = Common::iset($filterData['landCertificate'])) {
            array_push($whereTexts, "Land Certificate contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'deedFrom', null)) {
            array_push($whereTexts, "Deed contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'deedTo', null)) {
            array_push($whereTexts, "Deed to contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['areaFrom'])) {
            array_push($whereTexts, "Area contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['areaTo'])) {
            array_push($whereTexts, "Area to contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['unit_of_area_id'])) {
            array_push(
                $whereCodes,
                [
                    'unit_of_area',
                    'unit_of_area_id',
                    'unit_of_area_code',
                    $val,
                    "Unit of Area"
                ]
            );
        }

        if ($val = Common::iset($filterData['address'])) {
            array_push($whereTexts, "Address contains '" . $val . "'");
        }
        $this->getAddressSub($filterData, $whereTexts);

        $this->reAddUserDefinesQuery(GenTable::FREEHOLD, $filterData, $whereCodes, $whereTexts);

        return $freeholdQuery;
    }

    public function reAddEstatesLeaseInQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $leaseInService = new LeaseInService($this->permissionService);
        $leaseInQuery = $leaseInService->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Lease In Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Lease In Description contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['leaseInStatus'])) {
            array_push(
                $whereCodes,
                ['lease_in_status', 'lease_in_status_id', 'lease_in_status_code', $val, 'Status']
            );
        }

        if ($val = Common::iset($filterData['leaseType'])) {
            array_push($whereCodes, ['lease_type', 'lease_type_id', 'lease_type_code', $val, 'Lease Type']);
        }

        if ($val = Common::iset($filterData['leaseSubType'])) {
            array_push(
                $whereCodes,
                ['est_lease_sub_type', 'est_lease_sub_type_id', 'est_lease_sub_type_code', $val, 'Lease Sub Type']
            );
        }

        if ($val = Common::iset($filterData['contact'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Landlord']);
        }

        if ($val = Common::iset($filterData['owner'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Owner']);
        }

        if ($val = Common::iset($filterData['holdingOver'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.holding_over_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.holding_over_no'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.holding_over_all'));
                    break;
            }
        }

        if ($val = array_get($filterData, 'holdingOverFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.holding_over_from'), $val));
        }

        if ($val = array_get($filterData, 'holdingOverTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.holding_over_to'), $val));
        }

        if ($val = array_get($filterData, 'agreementDateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.agreement_date_from'), $val));
        }

        if ($val = array_get($filterData, 'agreementDateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.agreement_date_to'), $val));
        }

        if ($val = array_get($filterData, 'authorisedDateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.authorised_date_from'), $val));
        }

        if ($val = array_get($filterData, 'authorisedDateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.authorised_date_to'), $val));
        }

        if ($val = array_get($filterData, 'terminationDateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.termination_date_from'), $val));
        }

        if ($val = array_get($filterData, 'terminationDateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.termination_date_to'), $val));
        }

        if ($val = array_get($filterData, 'startDateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.start_date_from'), $val));
        }

        if ($val = array_get($filterData, 'startDateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.start_date_to'), $val));
        }

        if ($val = array_get($filterData, 'endDateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.end_date_from'), $val));
        }

        if ($val = array_get($filterData, 'endDateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.end_date_to'), $val));
        }

        if ($val = Common::iset($filterData['rates_liability_id'])) {
            array_push($whereCodes, ['rates_liability', 'rates_liability_id', 'rates_liability_code', $val, "Rates"]);
        }

        if ($val = Common::iset($filterData['insce_liability_id'])) {
            array_push(
                $whereCodes,
                [
                    'insce_liability',
                    'insce_liability_id',
                    'insce_liability_code',
                    $val,
                    "Insurance"
                ]
            );
        }

        if ($val = Common::iset($filterData['estate_maint_id'])) {
            array_push($whereCodes, ['estate_maint', 'estate_maint_id', 'estate_maint_code', $val, "Maintenance"]);
        }

        if ($val = Common::iset($filterData['address'])) {
            array_push($whereTexts, "Address contains '" . $val . "'");
        }
        $this->getAddressSub($filterData, $whereTexts);

        $this->reAddLandlordTenantActQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        $this->reAddRollingBreakDateAplicableQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);

        if ($val = Common::iset($filterData['vatIncluded'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.vat_included_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.vat_included_no'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.vat_included_all'));
                    break;
            }
        }

        if ($val = Common::iset($filterData['areaFrom'])) {
            array_push($whereTexts, "Area contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['areaTo'])) {
            array_push($whereTexts, "Area to contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['unit_of_area_id'])) {
            array_push(
                $whereCodes,
                [
                    'unit_of_area',
                    'unit_of_area_id',
                    'unit_of_area_code',
                    $val,
                    "Unit of Area"
                ]
            );
        }

        $this->reAddUserDefinesQuery(GenTable::LEASE_IN, $filterData, $whereCodes, $whereTexts);

        return $leaseInQuery;
    }

    public function reAddEstatesLettableUnitQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $leaseOutLetuService = new LeaseOutLetuService($this->permissionService);
        $lettableUnitService = new LettableUnitService($this->permissionService, $leaseOutLetuService);
        $lettableUnitQuery = $lettableUnitService->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Lettable Unit Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Lettable Unit Description contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['contact'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Landlord']);
        }

        if ($val = Common::iset($filterData['tenant'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Tenant']);
        }

        if ($val = Common::iset($filterData['owner'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, \Lang::get('validation.attributes.owner')]);
        }

        if ($val = Common::iset($filterData['available'])) {
            array_push($whereTexts, "Available = $val");
        }

        if ($val = Common::iset($filterData['vacant'])) {
            array_push($whereTexts, "Vacant = $val");
        }

        if ($val = Common::iset($filterData['status'])) {
            array_push(
                $whereCodes,
                ['lettable_unit_status', 'lettable_unit_status_id', 'lettable_unit_status_code', $val, 'Status']
            );
        }

        // Special location
        if ($val = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);

            if ($val = Common::iset($filterData['building_id'])) {
                array_push($whereCodes, ['building', 'building_id', 'building_code', $val, "Building Code"]);

                if ($val = Common::iset($filterData['room_id'])) {
                    array_push($whereCodes, ['room', 'room_id', 'room_number', $val, "Room Number"]);
                }
            }
        }

        if ($val = Common::iset($filterData['address'])) {
            array_push($whereTexts, "Address contains '" . $val . "'");
        }
        $this->getAddressSub($filterData, $whereTexts);

        $dateFields = [
            ["parkingSpacesFrom", "Parking Spaces", "greater than or equal to"],
            ["parkingSpacesTo", "Parking Spaces", "less than or equal to"],
            ["giaFrom", "GIA", "greater than or equal to"],
            ["giaTo", "GIA", "less than or equal to"],
            ["niaFrom", "NIA", "greater than or equal to"],
            ["niaTo", "NIA", "less than or equal to"],
            ["annualRentFrom", "Annual Rent ", "greater than or equal to"],
            ["annualRentTo", "Annual Rent ", "less than or equal to"],
            ["marketRentFrom", "Market Rent ", "greater than or equal to"],
            ["marketRentTo", "Market Rent ", "less than or equal to"],
            ["currentRentDate", "Current Rent Date ", "equal to"],
        ];

        foreach ($dateFields as $dateField) {
            $dateArg = $dateField[0];

            if ($val = Common::iset($filterData[$dateArg])) {
                $dateFieldDesc = $dateField[1];
                $dateFieldCompareText = $dateField[2];

                array_push($whereTexts, "$dateFieldDesc $dateFieldCompareText '" . $val . "'");
            }
        }

        $this->reAddUserDefinesQuery(GenTable::LETTABLE_UNIT, $filterData, $whereCodes, $whereTexts);

        return $lettableUnitQuery;
    }

    public function reAddEstatesLeaseOutQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $leaseOutService = new LeaseOutService($this->permissionService);
        $leaseInQuery = $leaseOutService->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Lease Out Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Lease Out Description contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['landlord'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Landlord']);
        }

        if ($val = Common::iset($filterData['leaseOutStatus'])) {
            array_push(
                $whereCodes,
                ['lease_out_status', 'lease_out_status_id', 'lease_out_status_code', $val, 'Status']
            );
        }

        $leaseOutStatusTypes = [];
        if (Common::iset($filterData['lostActive'])) {
            $leaseOutStatusTypes[] = LeaseOutStatusType::ACTIVE;
        }
        if (Common::iset($filterData['lostInactive'])) {
            $leaseOutStatusTypes[] = LeaseOutStatusType::INACTIVE;
        }
        if (Common::iset($filterData['lostArchive'])) {
            $leaseOutStatusTypes[] = LeaseOutStatusType::ARCHIVE;
        }
        if (Common::iset($filterData['lostTerminated'])) {
            $leaseOutStatusTypes[] = LeaseOutStatusType::TERMINATED;
        }
        if (count($leaseOutStatusTypes)) {
            array_push($orCodes, [
                'lease_out_status_type',
                'lease_out_status_type_id',
                'lease_out_status_type_code',
                implode(',', $leaseOutStatusTypes),
                "Lease Out Status Type"
            ]);
        }

        if ($val = Common::iset($filterData['leaseType'])) {
            array_push($whereCodes, ['lease_type', 'lease_type_id', 'lease_type_code', $val, 'Lease Type']);
        }

        if ($val = Common::iset($filterData['leaseSubType'])) {
            array_push(
                $whereCodes,
                ['est_lease_sub_type', 'est_lease_sub_type_id', 'est_lease_sub_type_code', $val, 'Lease Sub Type']
            );
        }

        if ($val = Common::iset($filterData['contact'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Tenant']);
        }

        if ($val = Common::iset($filterData['owner'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Owner']);
        }

        if ($val = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);
        }

        if ($val = Common::iset($filterData['holdingOver'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.holding_over_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.holding_over_no'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.holding_over_all'));
                    break;
            }
        }

        if ($val = array_get($filterData, 'holdingOverFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.holding_over_from'), $val));
        }

        if ($val = array_get($filterData, 'holdingOverTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.holding_over_to'), $val));
        }

        if ($val = array_get($filterData, 'agreementDateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.agreement_date_from'), $val));
        }

        if ($val = array_get($filterData, 'agreementDateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.agreement_date_to'), $val));
        }

        if ($val = array_get($filterData, 'authorisedDateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.authorised_date_from'), $val));
        }

        if ($val = array_get($filterData, 'authorisedDateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.authorised_date_to'), $val));
        }

        if ($val = array_get($filterData, 'terminationDateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.termination_date_from'), $val));
        }

        if ($val = array_get($filterData, 'terminationDateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.termination_date_to'), $val));
        }

        if ($val = array_get($filterData, 'startDateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.start_date_from'), $val));
        }

        if ($val = array_get($filterData, 'startDateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.start_date_to'), $val));
        }

        if ($val = array_get($filterData, 'endDateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.end_date_from'), $val));
        }

        if ($val = array_get($filterData, 'endDateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.end_date_to'), $val));
        }

        if ($val = Common::iset($filterData['rates_liability_id'])) {
            array_push($whereCodes, ['rates_liability', 'rates_liability_id', 'rates_liability_code', $val, "Rates"]);
        }

        if ($val = Common::iset($filterData['insce_liability_id'])) {
            array_push(
                $whereCodes,
                [
                    'insce_liability',
                    'insce_liability_id',
                    'insce_liability_code',
                    $val,
                    "Insurance"
                ]
            );
        }

        if ($val = Common::iset($filterData['estate_maint_id'])) {
            array_push($whereCodes, ['estate_maint', 'estate_maint_id', 'estate_maint_code', $val, "Maintenance"]);
        }

        if ($val = Common::iset($filterData['ruralLeases'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.rural_leases_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.rural_leases_no'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.rural_leases_all'));
                    break;
            }
        }

        if ($val = array_get($filterData, 'areaFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.area_from'), $val));
        }

        if ($val = array_get($filterData, 'areaTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.area_to'), $val));
        }

        if ($val = Common::iset($filterData['areaUnits'])) {
            array_push($whereCodes, ['unit_of_area', 'unit_of_area_id', 'unit_of_area_code', $val, "Area Units"]);
        }

        if ($val = Common::iset($filterData['site_designation'])) {
            array_push(
                $whereCodes,
                ['site_designation', 'site_designation_id', 'site_designation_code', $val, "Site Designation"]
            );
        }

        if ($val = Common::iset($filterData['vatIncluded'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.vat_included_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.vat_included_no'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.vat_included_all'));
                    break;
            }
        }

        $this->reAddLandlordTenantActQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        $this->reAddRollingBreakDateAplicableQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);

        if ($val = Common::iset($filterData['address'])) {
            array_push($whereTexts, "Address contains '" . $val . "'");
        }
        $this->getAddressSub($filterData, $whereTexts);

        $this->reAddUserDefinesQuery(GenTable::LEASE_OUT, $filterData, $whereCodes, $whereTexts);

        return $leaseInQuery;
    }

    public function reAddLandlordTenantActQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        if ($val = Common::iset($filterData['landlordTenantAct'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.landlord_tenant_act_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.landlord_tenant_act_no'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.landlord_tenant_act_all'));
                    break;
            }
        }
    }

    public function reAddRollingBreakDateAplicableQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        if ($val = Common::iset($filterData['applicable'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.rolling_break_applicable_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.rolling_break_applicable_no'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.rolling_break_applicable_all'));
                    break;
            }
        }
    }


    public function reAddEstatesDeedPacketQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $deedPacketService = new DeedPacketService($this->permissionService);

        if ($viewName == 'vw_es05') {
            // ES05: Deed Packet
            $deedPacketQuery = $deedPacketService->filterAll($query, $filterData, $viewName);
        } elseif ($viewName == 'vw_es11') {
            // ES11: Deed Packet Property
            $deedPacketQuery = $deedPacketService->getAllDeedPacketPropertyFilter($query, $filterData, $viewName);
        }

        if ($val = Common::iset($filterData['active'])) {
            Common::getStatusReport($val, $whereTexts);
        }

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Deed Packet Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Deed Packet Description contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['contact'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Landlord']);
        }

        if ($val = Common::iset($filterData['packet_holder'])) {
            array_push($whereTexts, "Deed Packet Holder contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['local_id'])) {
            array_push($whereTexts, "Deed Packet Id contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);

            if ($val = Common::iset($filterData['building_id'])) {
                array_push($whereCodes, ['building', 'building_id', 'building_code', $val, "Building Code"]);
            }
        }

        return $deedPacketQuery;
    }

    public function reAddEstatesDisposalQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {

        $disposalService = new DisposalService($this->permissionService);
        $disposalQuery = $disposalService->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Disposal Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Disposal Description contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['type'])) {
            array_push(
                $whereCodes,
                ['est_disposal_type', 'est_disposal_type_id', 'est_disposal_type_code', $val, 'Type']
            );
        }

        if ($val = Common::iset($filterData['status'])) {
            array_push(
                $whereCodes,
                ['est_disposal_status', 'est_disposal_status_id', 'est_disposal_status_code', $val, 'Status']
            );
        }

        if ($val = Common::iset($filterData['landlord'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Landlord']);
        }

        if ($val = Common::iset($filterData['deedPacket'])) {
            array_push(
                $whereCodes,
                ['deed_packet', 'deed_packet_id', 'deed_packet_code', $val, 'Deed Packet']
            );
        }

        if ($val = Common::iset($filterData['deedPacketCode'])) {
            array_push(
                $whereCodes,
                ['deed_packet', 'deed_packet_code', 'deed_packet_code', $val, 'Deed Packet Code']
            );
        }

        if ($val = Common::iset($filterData['considerationFrom'])) {
            array_push($whereTexts, "Consideration from '" . $val . "'");
        }

        if ($val = Common::iset($filterData['considerationTo'])) {
            array_push($whereTexts, "Consideration to '" . $val . "'");
        }

        if ($val = Common::iset($filterData['actualPaidFrom'])) {
            array_push($whereTexts, "Actual Paid from '" . $val . "'");
        }

        if ($val = Common::iset($filterData['actualPaidTo'])) {
            array_push($whereTexts, "Actual Paid to '" . $val . "'");
        }

        if ($val = Common::iset($filterData['legalCostsFrom'])) {
            array_push($whereTexts, "Legal Costs from '" . $val . "'");
        }

        if ($val = Common::iset($filterData['legalCostsTo'])) {
            array_push($whereTexts, "Legal Costs to '" . $val . "'");
        }

        if ($val = Common::iset($filterData['salesCostsFrom'])) {
            array_push($whereTexts, "Sales Costs from '" . $val . "'");
        }

        if ($val = Common::iset($filterData['salesCostsTo'])) {
            array_push($whereTexts, "Sales Costs to '" . $val . "'");
        }

        if ($val = Common::iset($filterData['otherCostsFrom'])) {
            array_push($whereTexts, "Other Costs from '" . $val . "'");
        }

        if ($val = Common::iset($filterData['otherCostsTo'])) {
            array_push($whereTexts, "Other Costs to '" . $val . "'");
        }

        if ($val = Common::iset($filterData['totalCostsFrom'])) {
            array_push($whereTexts, "Total Costs from '" . $val . "'");
        }

        if ($val = Common::iset($filterData['totalCostsTo'])) {
            array_push($whereTexts, "Total Costs to '" . $val . "'");
        }

        if ($val = Common::iset($filterData['netProceedsFrom'])) {
            array_push($whereTexts, "Net Proceeds from '" . $val . "'");
        }

        if ($val = Common::iset($filterData['netProceedsTo'])) {
            array_push($whereTexts, "Net Proceeds to '" . $val . "'");
        }

        if ($val = Common::iset($filterData['disposalRef'])) {
            array_push($whereTexts, "Disposal Ref. contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['minuteNumber'])) {
            array_push($whereTexts, "Minute Number contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['legalRef'])) {
            array_push($whereTexts, "Legal Ref. contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['legalNotices'])) {
            array_push($whereTexts, "Legal Notices contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['landCertificate'])) {
            array_push($whereTexts, "Land Certificate contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['surveyorRef'])) {
            array_push($whereTexts, "Surveyor Ref. contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['capitalReceiptCode'])) {
            array_push($whereTexts, "Capital Receipt Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['owner'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Owner']);
        }

        if ($val = Common::iset($filterData['authorisedDateFrom'])) {
            array_push($whereTexts, "Authorised Date from '" . $val . "'");
        }

        if ($val = Common::iset($filterData['authorisedDateTo'])) {
            array_push($whereTexts, "Authorised Date to '" . $val . "'");
        }

        if ($val = Common::iset($filterData['advertisedDateFrom'])) {
            array_push($whereTexts, "Advertised Date from '" . $val . "'");
        }

        if ($val = Common::iset($filterData['advertisedDateTo'])) {
            array_push($whereTexts, "Advertised Date to '" . $val . "'");
        }

        if ($val = Common::iset($filterData['exchangeDateFrom'])) {
            array_push($whereTexts, "Exchange Date from '" . $val . "'");
        }

        if ($val = Common::iset($filterData['exchangeDateTo'])) {
            array_push($whereTexts, "Exchange Date to '" . $val . "'");
        }

        if ($val = Common::iset($filterData['completionDateFrom'])) {
            array_push($whereTexts, "Completion Date from '" . $val . "'");
        }

        if ($val = Common::iset($filterData['completionDateTo'])) {
            array_push($whereTexts, "Completion Date to '" . $val . "'");
        }

        if ($val = Common::iset($filterData['demolishedDateFrom'])) {
            array_push($whereTexts, "Demolished Date from '" . $val . "'");
        }

        if ($val = Common::iset($filterData['demolishedDateTo'])) {
            array_push($whereTexts, "Demolished Date to '" . $val . "'");
        }

        if ($val = Common::iset($filterData['areaFrom'])) {
            array_push($whereTexts, "Area from '" . $val . "'");
        }

        if ($val = Common::iset($filterData['areaTo'])) {
            array_push($whereTexts, "Area to '" . $val . "'");
        }

        if ($val = Common::iset($filterData['areaUnits'])) {
            array_push($whereCodes, ['unit_of_area', 'unit_of_area_id', 'unit_of_area_code', $val, "Area Units"]);
        }

        if ($val = Common::iset($filterData['vatIncluded'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.vat_included_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.vat_included_no'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.vat_included_all'));
                    break;
            }
        }

        if ($val = Common::iset($filterData[''])) {
            array_push($whereTexts, " contains '" . $val . "'");
        }

        $this->reAddUserDefinesQuery(GenTable::EST_DISPOSAL, $filterData, $whereCodes, $whereTexts);

        return $disposalQuery;
    }

    public function reAddEstatesReviewQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $parentType = array_get($filterData, 'parentType', '');
        $reviewService = new EstReviewService($this->permissionService);
        $reviewQuery = $reviewService->getAllReviewsFilter($query, $filterData, $parentType, $viewName);

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Review Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['reviewType'])) {
            array_push(
                $orCodes,
                ['est_review_type', 'est_review_type_id', 'est_review_type_code', implode(',', $val), 'Type']
            );
        }

        if ($val = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);
        }

        if ($val = Common::iset($filterData['owner'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Owner']);
        }

        if ($val = Common::iset($filterData['landlord'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Landlord']);
        }

        if ($val = Common::iset($filterData['parentStatus'])) {
            array_push(
                $orCodes,
                [
                    'lease_in_status',
                    'lease_in_status_type_id',
                    'lease_in_status_code',
                    implode(',', $val),
                    "Parent Status"
                ]
            );
        }

        $dateFields = [
            ["nextNotifyFrom", "Notify Date", "Greater than or equal to"],
            ["nextNotifyTo", "Notify Date", "Less than or equal to"],
            ["reviewFrom", "Review Date", "Greater than or equal to"],
            ["reviewTo", "Review Date", "Less than or equal to"],
        ];

        foreach ($dateFields as $dateField) {
            $dateArg = $dateField[0];

            if ($val = Common::iset($filterData[$dateArg])) {
                $dateFieldDesc = $dateField[1];
                $dateFieldCompareText = $dateField[2];

                array_push($whereTexts, "$dateFieldDesc $dateFieldCompareText '" . $val . "'");
            }
        }

        if ($val = Common::iset($filterData['reviewStatus'])) {
            array_push(
                $orCodes,
                [
                    'est_review_status',
                    'est_review_status_id',
                    'est_review_status_code',
                    implode(',', $val),
                    "Status"
                ]
            );
        }

        if ($val = Common::iset($filterData['estReviewStatusType'])) {
            array_push(
                $orCodes,
                [
                    'est_review_status_type',
                    'est_review_status_type_id',
                    'est_review_status_type_code',
                    implode(',', $val),
                    "Status Type"
                ]
            );
        }

        return $reviewQuery;
    }

    public function reAddEstatesChargesRentQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $chargesRentService = new EstChargeService($this->permissionService);
        $chargesRentQuery = $chargesRentService->getListAllChargesFilter($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['status'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, "Status = 'Inactive'");
                    break;
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, "Status = 'Active'");
                    break;
                default:
                    array_push($whereTexts, "Status = 'All'");
                    break;
            }
        }

        if ($val = Common::iset($filterData['landlord'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Landlord']);
        }

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Charges/Rent Code contains '" . $val . "'");
        }

        // Charge
        if ($val = Common::iset($filterData['chargeType'])) {
            array_push($whereTexts, "Rent is '" . $val . "'");

            // Charge Type
            if ($val == CommonConstant::DATABASE_VALUE_NO) {
                if ($val = Common::iset($filterData['estChargeTypeId'])) {
                    array_push(
                        $whereCodes,
                        ['est_charge_type', 'est_charge_type_id', 'est_charge_type_code', $val, "Charge Type"]
                    );
                }
            }
        } else {
            array_push($whereTexts, "Charge/Rent = 'All'");
        }

        if ($val = Common::iset($filterData['nextReviewFrom'])) {
            array_push($whereTexts, "Review Date from {$val}");
        }

        if ($val = Common::iset($filterData['creditNote'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, "Credit Note = 'No'");
                    break;
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, "Credit Note = 'Yes'");
                    break;
                default:
                    array_push($whereTexts, "Credit Note = 'All'");
                    break;
            }
        }

        if ($val = Common::iset($filterData['nextReviewTo'])) {
            array_push($whereTexts, "Review Date to {$val}");
        }

        if ($val = Common::iset($filterData['peppercornRent'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, "Peppercorn Rent = 'No'");
                    break;
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, "Peppercorn Rent = 'Yes'");
                    break;
            }
        }

        if ($val = Common::iset($filterData['leaseOwner'])) {
            array_push($whereCodes, ['user', 'id', 'username', $val, 'Lease Owner']);
        }

        if ($val = Common::iset($filterData['rentChargeOwner'])) {
            array_push($whereCodes, ['user', 'id', 'username', $val, 'Rent Charge Owner']);
        }

        if ($val = Common::iset($filterData['paymentMethodId'])) {
            array_push(
                $whereCodes,
                ['payment_method', 'payment_method_id', 'payment_method_desc', $val, 'Payment Method']
            );
        }

        if ($val = Common::iset($filterData['account'])) {
            array_push(
                $whereCodes,
                ['fin_account', 'fin_account_id', 'fin_account_code', $val, 'Account Code']
            );
        }

        if ($val = Common::iset($filterData['tenant'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Tenant']);
        }

        if ($val = Common::iset($filterData['chargeStartDateFrom'])) {
            array_push($whereTexts, "Charge Start Date from {$val}");
        }

        if ($val = Common::iset($filterData['chargeStartDateTo'])) {
            array_push($whereTexts, "Charge Start Date to {$val}");
        }

        if ($val = Common::iset($filterData['chargeEndDateFrom'])) {
            array_push($whereTexts, "Charge End Date from {$val}");
        }

        if ($val = Common::iset($filterData['chargeEndDateTo'])) {
            array_push($whereTexts, "Charge End Date to {$val}");
        }

        if ($val = Common::iset($filterData['paymentStartDateFrom'])) {
            array_push($whereTexts, "Payment Start Date from {$val}");
        }

        if ($val = Common::iset($filterData['paymentStartDateTo'])) {
            array_push($whereTexts, "Payment Start Date to {$val}");
        }

        if ($val = Common::iset($filterData['paymentEndDateFrom'])) {
            array_push($whereTexts, "Payment End Date from {$val}");
        }

        if ($val = Common::iset($filterData['paymentEndDateTo'])) {
            array_push($whereTexts, "Payment End Date to {$val}");
        }

        if ($val = Common::iset($filterData['annualValueFrom'])) {
            array_push($whereTexts, "Annual Value from {$val}");
        }

        if ($val = Common::iset($filterData['annualValueTo'])) {
            array_push($whereTexts, "Annual Value to {$val}");
        }

        $this->reAddUserDefinesQuery(GenTable::EST_CHARGE, $filterData, $whereCodes, $whereTexts);

        return $chargesRentQuery;
    }

    public function reAddEstatesAcquisitionQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $acquisitionService = new AcquisitionService($this->permissionService);
        $acquisitionQuery = $acquisitionService->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Acquisition Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Acquisition Description contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['contact'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Landlord']);
        }

        if ($val = Common::iset($filterData['purpose'])) {
            array_push($whereTexts, "Acquisition Purpose contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['comments'])) {
            array_push($whereTexts, "Acquisition Comments contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);
        }

        if ($val = Common::iset($filterData['status'])) {
            array_push(
                $whereCodes,
                ['est_aquisition_status', 'est_aquisition_status_id', 'est_aquisition_status_code', $val, "Status"]
            );
        }

        if ($val = Common::iset($filterData['archived'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, "Archived = 'Yes'");
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                default:
                    array_push($whereTexts, "Archived = 'No'");
                    break;
            }
        }

        if ($val = Common::iset($filterData['conveyanceDateFrom'])) {
            array_push($whereTexts, "Conveyance Date from '" . $val . "'");
        }

        if ($val = Common::iset($filterData['conveyanceDateTo'])) {
            array_push($whereTexts, "Conveyance Date to '" . $val . "'");
        }

        if ($val = Common::iset($filterData['acquisitionDateFrom'])) {
            array_push($whereTexts, "Acquisition Date from '" . $val . "'");
        }

        if ($val = Common::iset($filterData['acquisitionDateTo'])) {
            array_push($whereTexts, "Acquisition Date to '" . $val . "'");
        }

        if ($val = Common::iset($filterData['recordingDateFrom'])) {
            array_push($whereTexts, "Recording Date from '" . $val . "'");
        }

        if ($val = Common::iset($filterData['recordingDateTo'])) {
            array_push($whereTexts, "Recording Date to '" . $val . "'");
        }

        if ($val = Common::iset($filterData['authorisationDateFrom'])) {
            array_push($whereTexts, "Authorisation Date from '" . $val . "'");
        }

        if ($val = Common::iset($filterData['authorisationDateTo'])) {
            array_push($whereTexts, "Authorisation Date to '" . $val . "'");
        }

        if ($val = Common::iset($filterData['considerationFrom'])) {
            array_push($whereTexts, "Consideration from '" . $val . "'");
        }

        if ($val = Common::iset($filterData['considerationTo'])) {
            array_push($whereTexts, "Consideration to '" . $val . "'");
        }

        if ($val = Common::iset($filterData['actualPaidFrom'])) {
            array_push($whereTexts, "Actual Paid from '" . $val . "'");
        }

        if ($val = Common::iset($filterData['actualPaidTo'])) {
            array_push($whereTexts, "Actual Paid to '" . $val . "'");
        }

        if ($val = Common::iset($filterData['legalCostsFrom'])) {
            array_push($whereTexts, "Legal Costs from '" . $val . "'");
        }

        if ($val = Common::iset($filterData['legalCostsTo'])) {
            array_push($whereTexts, "Legal Costs to '" . $val . "'");
        }

        if ($val = Common::iset($filterData['purchaseCostsFrom'])) {
            array_push($whereTexts, "Purchase Costs from '" . $val . "'");
        }

        if ($val = Common::iset($filterData['purchaseCostsTo'])) {
            array_push($whereTexts, "Purchase Costs to '" . $val . "'");
        }

        if ($val = Common::iset($filterData['otherCostsFrom'])) {
            array_push($whereTexts, "Other Costs from '" . $val . "'");
        }

        if ($val = Common::iset($filterData['otherCostsTo'])) {
            array_push($whereTexts, "Other Costs to '" . $val . "'");
        }

        if ($val = Common::iset($filterData['totalCostsFrom'])) {
            array_push($whereTexts, "Total Costs from '" . $val . "'");
        }

        if ($val = Common::iset($filterData['totalCostsTo'])) {
            array_push($whereTexts, "Total Costs to '" . $val . "'");
        }

        if ($val = Common::iset($filterData['grossCostsFrom'])) {
            array_push($whereTexts, "Gross Costs from '" . $val . "'");
        }

        if ($val = Common::iset($filterData['grossCostsTo'])) {
            array_push($whereTexts, "Gross Costs to '" . $val . "'");
        }

        return $acquisitionQuery;
    }

    public function reAddEstatesLettableUnitPropertyQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $leaseOutLetuService       = new LeaseOutLetuService($this->permissionService);
        $lettableUnitService       = new LettableUnitService($this->permissionService, $leaseOutLetuService);
        $lettableUnitPropertyQuery = $lettableUnitService->filterAllProperties($query, $filterData, 0, $viewName);

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Lettable Unit Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Lettable Unit Description contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['available'])) {
            array_push($whereTexts, "Available = $val");
        }

        if ($val = Common::iset($filterData['vacant'])) {
            array_push($whereTexts, "Vacant = $val");
        }

        if ($val = Common::iset($filterData['status'])) {
            array_push(
                $whereCodes,
                ['lettable_unit_status', 'lettable_unit_status_id', 'lettable_unit_status_code', $val, 'Status']
            );
        }

        if ($val = Common::iset($filterData['contact'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Landlord']);
        }

        if ($val = Common::iset($filterData['owner'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, \Lang::get('validation.attributes.owner')]);
        }

        // Special location
        if ($val = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);

            if ($val = Common::iset($filterData['building_id'])) {
                array_push($whereCodes, ['building', 'building_id', 'building_code', $val, "Building Code"]);

                if ($val = Common::iset($filterData['room_id'])) {
                    array_push($whereCodes, ['room', 'room_id', 'room_number', $val, "Room Number"]);
                }
            }
        }

        if ($val = Common::iset($filterData['prop_zone_id'])) {
            array_push($whereCodes, ['prop_zone', 'prop_zone_id', 'prop_zone_code', $val, 'Zone']);

            if ($val = Common::iset($filterData['prop_external_id'])) {
                array_push(
                    $whereCodes,
                    ['prop_external', 'prop_external_id', 'prop_external_code', $val, 'External Area']
                );
            }
        }

        return $lettableUnitPropertyQuery;
    }

    public function reAddEstatesOccupancyUnitPropertyQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $leaseOutLetuService       = new LeaseOutLetuService($this->permissionService);
        $lettableUnitService       = new LettableUnitService($this->permissionService, $leaseOutLetuService);
        $lettableUnitPropertyQuery = $lettableUnitService->filterAllProperties($query, $filterData, 0, $viewName);

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Lettable Unit Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Lettable Unit Description contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['available'])) {
            array_push($whereTexts, "Available = $val");
        }

        if ($val = Common::iset($filterData['vacant'])) {
            array_push($whereTexts, "Vacant = $val");
        }

        if ($val = Common::iset($filterData['status'])) {
            array_push(
                $whereCodes,
                ['lettable_unit_status', 'lettable_unit_status_id', 'lettable_unit_status_code', $val, 'Status']
            );
        }


        if ($val = Common::iset($filterData['occupant'])) {
            array_push(
                $whereCodes,
                ['occupancy_occupant', 'occupancy_occupant_id', 'occupancy_occupant_desc', $val, 'Occupant']
            );
        }

        if ($val = Common::iset($filterData['owner'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, \Lang::get('validation.attributes.owner')]);
        }

        // Special location
        if ($val = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);

            if ($val = Common::iset($filterData['building_id'])) {
                array_push($whereCodes, ['building', 'building_id', 'building_code', $val, "Building Code"]);

                if ($val = Common::iset($filterData['room_id'])) {
                    array_push($whereCodes, ['room', 'room_id', 'room_number', $val, "Room Number"]);
                }
            }
        }


        return $lettableUnitPropertyQuery;
    }


    public function reAddEstatesValuationQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $valuationService = new ValuationService($this->permissionService);
        $valuationQuery = $valuationService->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Valuation Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Valuation Description contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['status'])) {
            array_push(
                $whereCodes,
                ['est_valuation_status', 'est_valuation_status_id', 'est_valuation_status_code', $val, 'Status']
            );
        }

        if ($val = Common::iset($filterData['basis'])) {
            array_push(
                $whereCodes,
                ['valuation_basis', 'valuation_basis_id', 'valuation_basis_code', $val, 'Valuation Basis']
            );
        }

        if ($val = Common::iset($filterData['basis'])) {
            array_push(
                $whereCodes,
                ['valuation_type', 'valuation_type_id', 'valuation_type_code', $val, 'Valuation Type']
            );
        }


        if ($val = Common::iset($filterData['valuationMode'])) {
            array_push(
                $whereCodes,
                ['est_valuation_mode', 'est_valuation_mode_id', 'est_valuation_mode_code', $val, 'Valuation Mode']
            );
        }

        if ($val = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);

            if ($val = Common::iset($filterData['building_id'])) {
                array_push($whereCodes, ['building', 'building_id', 'building_code', $val, "Building Code"]);
            }
        }

        if ($val = Common::iset($filterData['contact'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Valuer']);
        }

        if ($val = Common::iset($filterData['landlord'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Landlord']);
        }

        if ($val = Common::iset($filterData['current'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, "Current = 'Yes'");
                    break;
                case 'all':
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                default:
                    array_push($whereTexts, "Current = 'No'");
                    break;
            }
        }

        if ($val = Common::iset($filterData['active'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, "Site active = 'Yes'");
                    break;
                case 'all':
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                default:
                    array_push($whereTexts, "Site active = 'No'");
                    break;
            }
        }

        if ($val = Common::iset($filterData['costFrom'])) {
            array_push($whereTexts, "Cost contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['costTo'])) {
            array_push($whereTexts, "Cost to contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['marriageValueFrom'])) {
            array_push($whereTexts, "Marriage value contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['marriageValueTo'])) {
            array_push($whereTexts, "Marriage value to contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['locationValueFrom'])) {
            array_push($whereTexts, "Location value contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['locationValueTo'])) {
            array_push($whereTexts, "Location value to contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['contentsValueFrom'])) {
            array_push($whereTexts, "Contents value contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['contentsValueTo'])) {
            array_push($whereTexts, "Contents value to contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['landValueFrom'])) {
            array_push($whereTexts, "Land value contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['landValueTo'])) {
            array_push($whereTexts, "Land value to contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['totalValueFrom'])) {
            array_push($whereTexts, "Total value contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['totalValueTo'])) {
            array_push($whereTexts, "Total value to contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['netReplacementSayFrom'])) {
            array_push($whereTexts, "Net Replacement Say contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['netReplacementSayTo'])) {
            array_push($whereTexts, "Net Replacement Say to contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['capitalValueLandSayFrom'])) {
            array_push($whereTexts, "Capital Value of Land Say contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['capitalValueLandSayTo'])) {
            array_push($whereTexts, "Capital Value of Land Say to contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['buildingValueSayFrom'])) {
            array_push($whereTexts, "Building Value Say contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['buildingValueSayTo'])) {
            array_push($whereTexts, "Building Value Say to contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['reinstatementOnlySayFrom'])) {
            array_push($whereTexts, "Reinstatement Only Say contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['reinstatementOnlySayTo'])) {
            array_push($whereTexts, "Reinstatement Only Say to contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['rentValueSayFrom'])) {
            array_push($whereTexts, "Rent Value Say contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['rentValueSayTo'])) {
            array_push($whereTexts, "Rent Value Say to contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['totalDevtValueSayFrom'])) {
            array_push($whereTexts, "Total DEVT Say contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['totalDevtValueSayTo'])) {
            array_push($whereTexts, "Total DEVT Say to contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['totalCostsSayFrom'])) {
            array_push($whereTexts, "Total Costs Say contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['totalCostsSayTo'])) {
            array_push($whereTexts, "Total Costs Say to contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['termTotalSayFrom'])) {
            array_push($whereTexts, "Term Total Say contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['termTotalSayTo'])) {
            array_push($whereTexts, "Term Total Say to contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['reversionTotalSayFrom'])) {
            array_push($whereTexts, "Reversion Total Say contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['reversionTotalSayTo'])) {
            array_push($whereTexts, "Reversion Total Say to contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['sayValueFrom'])) {
            array_push($whereTexts, "Say contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['sayValueTo'])) {
            array_push($whereTexts, "Say to contains '" . $val . "'");
        }



        if ($val = Common::iset($filterData['owner'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Owner']);
        }

        if ($val = array_get($filterData, 'valuationDateFrom', null)) {
            array_push($whereTexts, "Valuation Date contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'valuationDateFrom', null)) {
            array_push($whereTexts, "Valuation Date to contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'nextValuationDateFrom', null)) {
            array_push($whereTexts, "Next Valuation Date contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'nextValuationDateTo', null)) {
            array_push($whereTexts, "Next Valuation Date to contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'valuationEffectiveDateFrom', null)) {
            array_push($whereTexts, "Effective Valuation Date contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'valuationEffectiveDateTo', null)) {
            array_push($whereTexts, "Effective Valuation Date to contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['ifrsSubCategory'])) {
            array_push(
                $whereCodes,
                ['asset_subcategory', 'asset_subcategory_id', 'asset_subcategory_code', $val, 'IFRS SubCategory']
            );
        }

        if ($val = Common::iset($filterData['address'])) {
            array_push($whereTexts, "Address contains '" . $val . "'");
        }
        $this->getAddressSub($filterData, $whereTexts);

        return $valuationQuery;
    }

    public function reAddLeaseOutAuditQuery()
    {
        $auditTableName = (new Audit())->getTable();

        $query = Audit::join('user', 'user.id', '=', $auditTableName . '.user_id')
            ->join('audit_action', $auditTableName . '.audit_action_id', '=', 'audit_action.audit_action_id')
            ->join('audit_table', $auditTableName . '.table_name', '=', 'audit_table.table_dbname')
            ->leftjoin('lease_out', $auditTableName . '.record_id', '=', 'lease_out.lease_out_id')
            ->leftjoin(
                'lease_out AS lease_out_parent',
                $auditTableName . '.parent_record_id',
                '=',
                'lease_out_parent.lease_out_id'
            )
            ->leftjoin(
                'est_charge',
                $auditTableName . '.record_id',
                '=',
                'est_charge.est_charge_id'
            )
            ->leftjoin(
                'est_review',
                $auditTableName . '.record_id',
                '=',
                'est_review.est_review_id'
            )
            ->leftjoin(
                'est_review_type',
                'est_review.est_review_type_id',
                '=',
                'est_review_type.est_review_type_id'
            )
            ->leftjoin(
                'lettable_unit',
                $auditTableName . '.record_id',
                '=',
                'lettable_unit.lettable_unit_id'
            )
            ->leftJoin('lease_out_status', 'lease_out.lease_out_status_id', '=', 'lease_out_status.lease_out_status_id')
            ->leftJoin('lease_type', 'lease_out.lease_type_id', '=', 'lease_type.lease_type_id')
            ->leftJoin(
                'est_lease_sub_type',
                'lease_out.est_lease_sub_type_id',
                '=',
                'est_lease_sub_type.est_lease_sub_type_id'
            )
            ->leftJoin('contact', 'lease_out.tenant_contact_id', '=', 'contact.contact_id')
            ->where(function ($subQuery) {
                $subQuery->where('parent_table_name', 'lease_out')->orWhere('table_name', 'lease_out');
            })
        ;

        $query->select([
            \DB::raw("CASE table_name WHEN 'lease_out' THEN record_code ELSE lease_out_parent.lease_out_code
                        END AS `Lease Out Code`"),
            \DB::raw("CASE table_name WHEN 'lease_out' THEN lease_out.lease_out_desc
                        ELSE lease_out_parent.lease_out_desc
                        END AS `Lease Out Description`"),
            \DB::raw("table_display_name AS `Record Type`"),
            \DB::raw("CASE table_name WHEN 'lease_out' THEN '' ELSE record_code
                        END AS `Record Code`"),
            \DB::raw("CASE table_name
                        WHEN 'est_charge' THEN est_charge.est_charge_desc
                        WHEN 'est_review' THEN est_review_type.est_review_type_desc
                        WHEN 'lettable_unit' THEN lettable_unit.lettable_unit_desc
                        ELSE ''
                        END AS `Record Description`"),
            \DB::raw("audit_action.audit_action_code AS Action"),
            \DB::raw("audit_text AS `Audit Description`"),
            \DB::raw("DATE_FORMAT($auditTableName.timestamp, '%d/%m/%Y') AS 'Audit Date'"),
            \DB::raw("TIME_FORMAT($auditTableName.timestamp, '%H:%i:%s') AS 'Audit Time'"),
            \DB::raw("user.display_name AS User"),
            \DB::raw("$auditTableName.audit_id AS `Audit Id`")
        ])->distinct();

        return $query;
    }

    public function reGetLeaseInAuditQuery()
    {
        $auditTable = (new Audit())->getTable();

        $query = Audit::join('user', 'user.id', '=', "{$auditTable}.user_id")
            ->join('audit_action', "{$auditTable}.audit_action_id", '=', 'audit_action.audit_action_id')
            ->leftjoin('audit_table', "{$auditTable}.table_name", '=', 'audit_table.table_dbname')
            ->leftjoin('lease_in', "{$auditTable}.record_id", '=', 'lease_in.lease_in_id')
            ->leftjoin(
                'lease_in AS lease_in_parent',
                "{$auditTable}.parent_record_id",
                '=',
                'lease_in_parent.lease_in_id'
            )
            ->leftjoin('est_charge', "{$auditTable}.record_id", '=', 'est_charge.est_charge_id')
            ->leftjoin('est_review', "{$auditTable}.record_id", '=', 'est_review.est_review_id')
            ->leftjoin('est_review_type', 'est_review.est_review_type_id', '=', 'est_review_type.est_review_type_id')
            ->where(function ($subQuery) {
                $subQuery->where('parent_table_name', 'lease_in')->orWhere('table_name', 'lease_in');
            })
            ->select([
                \DB::raw("CASE table_name WHEN 'lease_in' THEN record_code ELSE lease_in_parent.lease_in_code
                            END AS `Lease In Code`"),
                \DB::raw("CASE table_name WHEN 'lease_in' THEN lease_in.lease_in_desc
                            ELSE lease_in_parent.lease_in_desc
                            END AS `Lease In Description`"),
                \DB::raw("table_display_name AS `Record Type`"),
                \DB::raw("CASE table_name WHEN 'lease_in' THEN '' ELSE record_code
                            END AS `Record Code`"),
                \DB::raw("CASE table_name
                            WHEN 'est_charge' THEN est_charge.est_charge_desc
                            WHEN 'est_review' THEN est_review_type.est_review_type_desc
                            ELSE ''
                            END AS `Record Description`"),
                \DB::raw("audit_action.audit_action_code AS Action"),
                \DB::raw("audit_text AS `Audit Description`"),
                \DB::raw("DATE_FORMAT($auditTable.timestamp, '%d/%m/%Y') AS 'Audit Date'"),
                \DB::raw("TIME_FORMAT($auditTable.timestamp, '%H:%i:%s') AS 'Audit Time'"),
                \DB::raw("user.display_name AS User"),
                \DB::raw("$auditTable.audit_id AS `Audit Id`")
            ]);

        return $query;
    }

    public function reAddEstatesLettingPacketQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $lettingPacketService = new LettingPacketService($this->permissionService);
        $query = $lettingPacketService->filterAll($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'code')) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.code_contains'), $val));
        }
        if ($val = array_get($filterData, 'description')) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.description_contains'), $val));
        }
        if ($val = Common::iset($filterData['contact'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Landlord']);
        }
        if ($val = array_get($filterData, 'local_id')) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.estate.local_id_contains'), $val));
        }
        if ($val = array_get($filterData, 'packet_holder')) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.estate.packet_holder_contains'), $val));
        }

        // Special location
        if ($val = array_get($filterData, 'site_id')) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);

            if ($val = array_get($filterData, 'building_id')) {
                array_push($whereCodes, ['building', 'building_id', 'building_code', $val, "Building Code"]);

                if ($val = array_get($filterData, 'room_id')) {
                    array_push($whereCodes, ['room', 'room_id', 'room_number', $val, "Room Number"]);
                }
            }
        }

        return $query;
    }

    public function reAddEstatesAlienationQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $invGenPlanService = new LeaseOutAlienationService($this->permissionService);
        $invGenPlanQuery   = $invGenPlanService->filterAll($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'code')) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.estate.lease_out_code_contains'), $val));
        }

        if ($val = array_get($filterData, 'description')) {
            array_push($whereTexts, sprintf(
                \Lang::get('text.report_texts.estate.lease_out_description_contains'),
                $val
            ));
        }

        if ($val = array_get($filterData, 'location')) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.location_contains'), $val));
        }

        if ($val = array_get($filterData, 'leaseOutStatus')) {
            array_push($whereCodes, [
                'lease_out_status',
                'lease_out_status_id',
                'lease_out_status_code',
                $val,
                \Lang::get('text.estate_module.lease_out_status')
            ]);
        }

        if ($val = array_get($filterData, 'leaseType')) {
            array_push($whereCodes, [
                'lease_type',
                'lease_type_id',
                'lease_type_code',
                $val,
                \Lang::get('text.estate_module.lease_type')
            ]);
        }

        if ($val = array_get($filterData, 'leaseSubType')) {
            array_push($whereCodes, [
                'est_lease_sub_type',
                'est_lease_sub_type_id',
                'est_lease_sub_type_code',
                $val,
                \Lang::get('text.estate_module.lease_sub_type')
            ]);
        }

        if ($val = array_get($filterData, 'contact')) {
            array_push($whereCodes, [
                'contact',
                'contact_id',
                'contact_name',
                $val,
                \Lang::get('text.contact_name')
            ]);
        }

        if ($val = array_get($filterData, 'landlord')) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Landlord']);
        }

        if ($val = array_get($filterData, 'leaseOutAlienationType')) {
            array_push($whereCodes, [
                'lease_out_alienation_type',
                'lease_out_alienation_type_id',
                'lease_out_alienation_type_name',
                $val,
                \Lang::get('text.report_texts.estate.alienation_type')
            ]);
        }

        // Special location
        if ($val = array_get($filterData, 'site_id')) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);
        }

        return $invGenPlanQuery;
    }

    public function reAddEstatesAgreementQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $agreementService = new AgreementService($this->permissionService);

        if ($viewName == 'vw_es50') {
            $agreementQuery   = $agreementService->filterAll(
                $query,
                $filterData,
                $viewName,
                \Tfcloud\Models\EstAgreement::TYPE_DISPOSAL
            );
        } else {
            $agreementQuery   = $agreementService->filterAll($query, $filterData, $viewName);
        }

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.estate.agreement_code_contains'), $val));
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push(
                $whereTexts,
                sprintf(\Lang::get('text.report_texts.estate.agreement_out_description_contains'), $val)
            );
        }

        if ($val = Common::iset($filterData['type'])) {
            array_push($whereCodes, [
                'est_agr_type',
                'est_agr_type_id',
                'est_agr_type_code',
                $val,
                \Lang::get('text.report_texts.estate.agreement_type')
            ]);
        }

        if ($val = Common::iset($filterData['onus'])) {
            array_push($whereCodes, [
                'est_agr_onus',
                'est_agr_onus_id',
                'est_agr_onus_code',
                $val,
                \Lang::get('text.report_texts.estate.agreement_onus')
            ]);
        }

        return $agreementQuery;
    }

    public function reAddEstatesInvoiceGenerationPlanQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $invGenPlanService = new InvoiceGenerationPlanService($this->permissionService, ['checkPermission' => false]);
        $invGenPlanService->getInvoiceGenerationPlanBySiteGroup();
        $invGenPlanQuery   = $invGenPlanService->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['leaseOutCode'])) {
            array_push($whereTexts, "Lease Out Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['leaseOutDesc'])) {
            array_push($whereTexts, "Lease Out Description contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['dueDateFrom'])) {
            array_push($whereTexts, "Due Date from {$val}");
        }

        if ($val = Common::iset($filterData['dueDateTo'])) {
            array_push($whereTexts, "Due Date to {$val}");
        }

        if ($val = Common::iset($filterData['onHold'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, "On Hold = 'Yes'");
                    break;
                case 'all':
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                default:
                    array_push($whereTexts, "On Hold = 'No'");
                    break;
            }
        }

        return $invGenPlanQuery;
    }

    public function reAddEstatesRentalPaymentGenerationPlanQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $rentalPaymentGenPlanService =
            new RentalPaymentGenPlanService($this->permissionService, ['checkPermission' => false]);
        $rentalPaymentGenPlanService->getRentalPaymentGenerationPlanBySiteGroup();
        $rentalPaymentGenPlanQuery   = $rentalPaymentGenPlanService->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['leaseInCode'])) {
            array_push($whereTexts, "Lease In Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['leaseInDesc'])) {
            array_push($whereTexts, "Lease In Description contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['dueDateFrom'])) {
            array_push($whereTexts, "Due Date from {$val}");
        }

        if ($val = Common::iset($filterData['dueDateTo'])) {
            array_push($whereTexts, "Due Date to {$val}");
        }

        if ($val = Common::iset($filterData['onHold'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, "On Hold = 'Yes'");
                    break;
                case 'all':
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                default:
                    array_push($whereTexts, "On Hold = 'No'");
                    break;
            }
        }

        return $rentalPaymentGenPlanQuery;
    }

    public function reAddEstatesLeaseOutAuditQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $query = $this->leaseOutAuditFilterQuery($query, $filterData);

        if ($val = array_get($filterData, 'code')) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.estate.lease_out_code_contains'), $val));
        }

        if ($val = array_get($filterData, 'description')) {
            array_push($whereTexts, sprintf(
                \Lang::get('text.report_texts.estate.lease_out_description_contains'),
                $val
            ));
        }

        if ($val = array_get($filterData, 'leaseOutStatus')) {
            array_push($whereCodes, [
                'lease_out_status',
                'lease_out_status_id',
                'lease_out_status_code',
                $val,
                \Lang::get('text.estate_module.lease_out_status')
            ]);
        }

        if ($val = array_get($filterData, 'leaseType')) {
            array_push($whereCodes, [
                'lease_type',
                'lease_type_id',
                'lease_type_code',
                $val,
                \Lang::get('text.estate_module.lease_type')
            ]);
        }

        if ($val = array_get($filterData, 'leaseSubType')) {
            array_push($whereCodes, [
                'est_lease_sub_type',
                'est_lease_sub_type_id',
                'est_lease_sub_type_code',
                $val,
                \Lang::get('text.estate_module.lease_sub_type')
            ]);
        }

        if ($val = array_get($filterData, 'contact')) {
            array_push($whereCodes, [
                'contact',
                'contact_id',
                'contact_name',
                $val,
                \Lang::get('text.contact_name')
            ]);
        }

        if ($val = array_get($filterData, 'landlord')) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Landlord']);
        }

        if ($val = array_get($filterData, 'user')) {
            array_push($whereCodes, [
                'user',
                'id',
                'display_name',
                $val,
                \Lang::get('text.user')
            ]);
        }

        if ($val = array_get($filterData, 'action')) {
            array_push($whereCodes, [
                'audit_action',
                'audit_action_id',
                'audit_action_code',
                $val,
                \Lang::get('text.action')
            ]);
        }

        if ($val = array_get($filterData, 'from')) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.audit_from'), $val));
        }

        if ($val = array_get($filterData, 'to')) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.audit_to'), $val));
        }

        // Special location
        if ($val = array_get($filterData, 'site_id')) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);
        }

        return $query;
    }

    private function leaseOutAuditFilterQuery($query, $inputs)
    {
        $auditTableName = (new Audit())->getTable();
        $filter = new AuditFilter($inputs);
        if (!is_null($filter->code)) {
            $query->where(function ($query) use ($filter) {
                $query->where('lease_out_parent.lease_out_code', 'like', '%' . $filter->code . '%');
                $query->orWhere('record_code', 'like', '%' . $filter->code . '%');
            });
        }
        if (!is_null($filter->description)) {
            $query->where(function ($query) use ($filter) {
                $query->where('lease_out_parent.lease_out_desc', 'like', '%' . $filter->description . '%');
                $query->orWhere('lease_out.lease_out_desc', 'like', '%' . $filter->description . '%');
            });
        }
        if (!is_null($filter->site_id)) {
            $query->leftJoin('vw_lease_out_all_site_id', function ($join) {
                $join->on('lease_out.lease_out_id', '=', 'vw_lease_out_all_site_id.lease_out_id')
                ->orOn('lease_out_parent.lease_out_id', '=', 'vw_lease_out_all_site_id.lease_out_id');
            });
            $query->where('vw_lease_out_all_site_id.site_id', $filter->site_id);
        } else {
            $query->leftJoin('vw_lease_out_site', function ($join) {
                $join->on('lease_out.lease_out_id', '=', 'vw_lease_out_site.lease_out_id')
                ->orOn('lease_out_parent.lease_out_id', '=', 'vw_lease_out_site.lease_out_id');
            });
        }
        if (!is_null($filter->leaseOutStatus)) {
            $query->where(function ($query) use ($filter) {
                $query->where('lease_out_parent.lease_out_status_id', $filter->leaseOutStatus);
                $query->orWhere('lease_out.lease_out_status_id', $filter->leaseOutStatus);
            });
        }
        if (!is_null($filter->contact)) {
            $query->where(function ($query) use ($filter) {
                $query->where('lease_out_parent.tenant_contact_id', $filter->contact);
                $query->orWhere('lease_out.tenant_contact_id', $filter->contact);
            });
        }
        if (!is_null($filter->landlord)) {
            $query->where(function ($query) use ($filter) {
                $query->where('lease_out_parent.landlord_contact_id', $filter->landlord);
                $query->orWhere('lease_out.landlord_contact_id', $filter->landlord);
            });
        }
        if (!is_null($filter->leaseType)) {
            $query->where(function ($query) use ($filter) {
                $query->where('lease_out_parent.lease_type_id', $filter->leaseType);
                $query->orWhere('lease_out.lease_type_id', $filter->leaseType);
            });
        }
        if (!is_null($filter->leaseSubType)) {
            $query->where(function ($query) use ($filter) {
                $query->where('lease_out_parent.est_lease_sub_type_id', $filter->leaseSubType);
                $query->orWhere('lease_out.est_lease_sub_type_id', $filter->leaseSubType);
            });
        }
        if (!is_null($filter->user)) {
            $query->where('user_id', $filter->user);
        }
        if (!is_null($filter->action)) {
            $query->where('audit_action.audit_action_id', $filter->action);
        }
        if (!is_null($filter->from)) {
            $from = \DateTime::createFromFormat('d/m/Y', $filter->from);
            if ($from) {
                $query->where(
                    \DB::raw("DATE_FORMAT({$auditTableName}.timestamp, '%Y-%m-%d')"),
                    '>=',
                    $from->format('Y-m-d')
                );
            }
        }
        if (!is_null($filter->to)) {
            $to = \DateTime::createFromFormat('d/m/Y', $filter->to);
            if ($to) {
                $query->where(
                    \DB::raw("DATE_FORMAT({$auditTableName}.timestamp, '%Y-%m-%d')"),
                    '<=',
                    $to->format('Y-m-d')
                );
            }
        }

        return $query;
    }

    public function reAddEstatesLeaseInAuditQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $query = $this->leaseInAuditFilterQuery($query, $filterData);

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Lease In Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Lease In Description contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['leaseInStatus'])) {
            array_push(
                $whereCodes,
                ['lease_in_status', 'lease_in_status_id', 'lease_in_status_code', $val, 'Status']
            );
        }

        if ($val = Common::iset($filterData['leaseType'])) {
            array_push($whereCodes, ['lease_type', 'lease_type_id', 'lease_type_code', $val, 'Lease Type']);
        }

        if ($val = Common::iset($filterData['leaseSubType'])) {
            array_push(
                $whereCodes,
                ['est_lease_sub_type', 'est_lease_sub_type_id', 'est_lease_sub_type_code', $val, 'Lease Sub Type']
            );
        }

        if ($val = Common::iset($filterData['contact'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Landlord']);
        }

        if ($val = Common::iset($filterData['owner'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Owner']);
        }

        if ($val = array_get($filterData, 'user')) {
            array_push($whereCodes, [
                'user',
                'id',
                'display_name',
                $val,
                \Lang::get('text.user')
            ]);
        }

        if ($val = array_get($filterData, 'action')) {
            array_push($whereCodes, [
                'audit_action',
                'audit_action_id',
                'audit_action_code',
                $val,
                \Lang::get('text.action')
            ]);
        }

        if ($val = array_get($filterData, 'from')) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.audit_from'), $val));
        }

        if ($val = array_get($filterData, 'to')) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.audit_to'), $val));
        }

        if ($val = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);

            if ($val = Common::iset($filterData['building_id'])) {
                array_push($whereCodes, ['building', 'building_id', 'building_code', $val, "Building Code"]);
            }
        }

        return $query;
    }

    private function leaseInAuditFilterQuery($query, $inputs)
    {
        $auditTableName = (new Audit())->getTable();
        $filter = new AuditFilter($inputs);
        if (!is_null($filter->code)) {
            $query->where(function ($query) use ($filter) {
                $query->where('lease_in_parent.lease_in_code', 'like', '%' . $filter->code . '%');
                $query->orWhere('lease_in.lease_in_code', 'like', '%' . $filter->code . '%');
            });
        }
        if (!is_null($filter->description)) {
            $query->where(function ($query) use ($filter) {
                $query->where('lease_in_parent.lease_in_desc', 'like', '%' . $filter->description . '%');
                $query->orWhere('lease_in.lease_in_desc', 'like', '%' . $filter->description . '%');
            });
        }
        if (!is_null($filter->leaseInStatus)) {
            $query->where(function ($query) use ($filter) {
                $query->where('lease_in_parent.lease_in_status_id', $filter->leaseInStatus);
                $query->orWhere('lease_in.lease_in_status_id', $filter->leaseInStatus);
            });
        }
        if (!is_null($filter->leaseType)) {
            $query->where(function ($query) use ($filter) {
                $query->where('lease_in_parent.lease_type_id', $filter->leaseType);
                $query->orWhere('lease_in.lease_type_id', $filter->leaseType);
            });
        }
        if (!is_null($filter->leaseSubType)) {
            $query->where(function ($query) use ($filter) {
                $query->where('lease_in_parent.est_lease_sub_type_id', $filter->leaseSubType);
                $query->orWhere('lease_in.est_lease_sub_type_id', $filter->leaseSubType);
            });
        }
        if (!is_null($filter->contact)) {
            $query->where(function ($query) use ($filter) {
                $query->where('lease_in_parent.landlord_contact_id', $filter->contact);
                $query->orWhere('lease_in.landlord_contact_id', $filter->contact);
            });
        }
        if (!is_null($filter->owner)) {
            $query->where(function ($query) use ($filter) {
                $query->where('lease_in_parent.owner_user_id', $filter->owner);
                $query->orWhere('lease_in.owner_user_id', $filter->owner);
            });
        }
        if (!is_null($filter->site_id)) {
            $query->leftJoin('lease_in_property', function ($join) {
                $join->on('lease_in.lease_in_id', '=', 'lease_in_property.lease_in_id')
                    ->orOn('lease_in_parent.lease_in_id', '=', 'lease_in_property.lease_in_id');
            });
            $query->where('lease_in_property.site_id', $filter->site_id);
            if (!is_null($filter->building_id)) {
                $query->where('lease_in_property.building_id', $filter->building_id);
            }
        }
        if (!is_null($filter->action)) {
            $query->where('audit_action.audit_action_id', $filter->action);
        }
        if (!is_null($filter->user)) {
            $query->where($auditTableName . '.user_id', $filter->user);
        }
        if (!is_null($filter->from)) {
            $from = \DateTime::createFromFormat('d/m/Y', $filter->from);
            if ($from) {
                $query->where(
                    \DB::raw("DATE_FORMAT({$auditTableName}.timestamp, '%Y-%m-%d')"),
                    '>=',
                    $from->format('Y-m-d')
                );
            }
        }
        if (!is_null($filter->to)) {
            $to = \DateTime::createFromFormat('d/m/Y', $filter->to);
            if ($to) {
                $query->where(
                    \DB::raw("DATE_FORMAT({$auditTableName}.timestamp, '%Y-%m-%d')"),
                    '<=',
                    $to->format('Y-m-d')
                );
            }
        }

        return $query;
    }

    public function reAddEstatesFeeReceiptsQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $feeReceiptService = new FeeReceiptService($this->permissionService);
        $feeReceiptQuery   = $feeReceiptService->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['targetFrom'])) {
            array_push($whereTexts, "Paid Date From = {$val}");
        }

        if ($val = Common::iset($filterData['targetTo'])) {
            array_push($whereTexts, "Paid Date To = {$val}");
        }

        if ($val = Common::iset($filterData['amount_from'])) {
            array_push($whereTexts, "Amount From = {$val}");
        }

        if ($val = Common::iset($filterData['amount_to'])) {
            array_push($whereTexts, "Amount to = {$val}");
        }

        if ($val = Common::iset($filterData['est_fee_receipt_code'])) {
            array_push($whereTexts, "Receipt No. contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['fin_account_code'])) {
            array_push($whereTexts, "Account ID contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['reference_number'])) {
            array_push($whereTexts, "Reference contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['tenant_contact_id'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Contact']);
        }

        if ($val = Common::iset($filterData['fully_allocated'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, "Full Allocated = 'Fully Allocated Only'");
                    break;
                case 'all':
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                default:
                    array_push($whereTexts, "Full Allocated = 'Not Fully Allocated'");
                    break;
            }
        }

        if ($val = Common::iset($filterData['adjustment'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, "Adjustment = 'Show Adjustments Only'");
                    break;
                case 'all':
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                default:
                    array_push($whereTexts, "Adjustment = 'Show Fee Receipt Only'");
                    break;
            }
        }

        if ($val = Common::iset($filterData['rural'])) {
            $include = "Include = ";
            switch ($val) {
                case 'all':
                    $include .= 'All';
                    break;
                case 'Y':
                    $include .= 'Rural Lettings Only';
                    break;
                case 'N':
                    $include .= 'Standard Lettings Only';
                    break;
                default:
                    $include .= $val;
            }

            array_push($whereTexts, $include);
        }

        return $feeReceiptQuery;
    }

    public function reAddEstatesNotificationOfPaymentsQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $service = new NotificationOfPaymentService($this->permissionService);
        $query = $service->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['targetFrom'])) {
            array_push($whereTexts, "Paid Date From = {$val}");
        }

        if ($val = Common::iset($filterData['targetTo'])) {
            array_push($whereTexts, "Paid Date To = {$val}");
        }

        if ($val = Common::iset($filterData['amount_from'])) {
            array_push($whereTexts, "Amount From = {$val}");
        }

        if ($val = Common::iset($filterData['amount_to'])) {
            array_push($whereTexts, "Amount to = {$val}");
        }

        if ($val = Common::iset($filterData['est_notification_of_payment_code'])) {
            array_push($whereTexts, "Receipt No. contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['fin_account_code'])) {
            array_push($whereTexts, "Account ID contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['reference_number'])) {
            array_push($whereTexts, "Reference contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['landlord_contact_id'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Contact']);
        }

        if ($val = Common::iset($filterData['payee_contact_id'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Payee']);
        }

        if ($val = Common::iset($filterData['fully_allocated'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, "Full Allocated = 'Fully Allocated Only'");
                    break;
                case 'all':
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                default:
                    array_push($whereTexts, "Full Allocated = 'Not Fully Allocated'");
                    break;
            }
        }

        if ($val = Common::iset($filterData['adjustment'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, "Adjustment = 'Show Adjustments Only'");
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, "Adjustment = 'Exclude Adjustments'");
                    break;
                default:
                    array_push($whereTexts, "Adjustment = 'Include Adjustments'");
                    break;
            }
        }

        return $query;
    }

    public function reAddEstatesSalesInvoiceQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $salesInvoiceService = new EstateSalesInvoiceService($this->permissionService);
        $salesInvoiceQuery   = $salesInvoiceService->filterAll($query, $filterData, $viewName);

        if ($viewName == 'vw_est_cli_scotb01') {
            $salesInvoiceQuery->orderBy('Lease Ref')
                ->orderBy('tax_date', 'DESC');
        }

        if ($val = Common::iset($filterData['invoiceNo'])) {
            array_push($whereTexts, "Invoice No. contains '{$val}'");
        }

        if ($val = Common::iset($filterData['accountId'])) {
            array_push($whereTexts, "Account ID contains '{$val}'");
        }

        if ($val = Common::iset($filterData['leaseOutCode'])) {
            array_push($whereTexts, "Lease Out Code contains '{$val}'");
        }

        if ($val = Common::iset($filterData['reference'])) {
            array_push($whereTexts, "Reference contains '{$val}'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Description contains '{$val}'");
        }

        if ($val = Common::iset($filterData['contact'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Contact']);
        }

        if ($val = Common::iset($filterData['landlord'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Landlord']);
        }

        $status = [];
        if ($val = Common::iset($filterData['printed'])) {
            array_push($status, 'Printed');
        }
        if ($val = Common::iset($filterData['generated'])) {
            array_push($status, 'Generated');
        }
        if ($val = Common::iset($filterData['posted'])) {
            array_push($status, 'Posted');
        }
        if ($val = Common::iset($filterData['paid'])) {
            array_push($status, 'Paid');
        }
        if ($val = Common::iset($filterData['cancelled'])) {
            array_push($status, 'Cancelled');
        }

        if ($status != []) {
            array_push($whereTexts, 'Status = ' . implode(', ', $status));
        }

        if ($val = Common::iset($filterData['taxDateFrom'])) {
            array_push($whereTexts, "Tax Date From = {$val}");
        }

        if ($val = Common::iset($filterData['taxDateTo'])) {
            array_push($whereTexts, "Tax Date To = {$val}");
        }

        if ($val = Common::iset($filterData['lastPaidDateFrom'])) {
            array_push($whereTexts, "Last Paid Date From = {$val}");
        }

        if ($val = Common::iset($filterData['lastPaidDateTo'])) {
            array_push($whereTexts, "Last Paid Date To = {$val}");
        }

        if ($val = Common::iset($filterData['netFrom'])) {
            array_push($whereTexts, "Net From = {$val}");
        }

        if ($val = Common::iset($filterData['netTo'])) {
            array_push($whereTexts, "Net To = {$val}");
        }

        if ($val = Common::iset($filterData['taxFrom'])) {
            array_push($whereTexts, "Tax From = {$val}");
        }

        if ($val = Common::iset($filterData['taxTo'])) {
            array_push($whereTexts, "Tax To = {$val}");
        }

        if ($val = Common::iset($filterData['grossFrom'])) {
            array_push($whereTexts, "Gross From = {$val}");
        }

        if ($val = Common::iset($filterData['grossTo'])) {
            array_push($whereTexts, "Gross To = {$val}");
        }

        if ($val = Common::iset($filterData['rural'])) {
            $include = "Include = ";
            switch ($val) {
                case 'all':
                    $include .= 'All';
                    break;
                case 'Y':
                    $include .= 'Rural Lettings Only';
                    break;
                case 'N':
                    $include .= 'Standard Lettings Only';
                    break;
                default:
                    $include .= $val;
            }

            array_push($whereTexts, $include);
        }

        return $salesInvoiceQuery;
    }

    public function reAddEstatesRentalPaymentQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $service = new EstRentalPaymentService($this->permissionService);
        $query   = $service->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['est_rental_payment_code'])) {
            array_push($whereTexts, "Rental Payment No contains '{$val}'");
        }

        if ($val = Common::iset($filterData['accountId'])) {
            array_push($whereTexts, "Account ID contains '{$val}'");
        }

        if ($val = Common::iset($filterData['leaseInCode'])) {
            array_push($whereTexts, "Lease In Code contains '{$val}'");
        }

        if ($val = Common::iset($filterData['reference'])) {
            array_push($whereTexts, "Reference contains '{$val}'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Description contains '{$val}'");
        }

        if ($val = Common::iset($filterData['contact'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Landlord']);
        }

        if ($val = Common::iset($filterData['payee'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Payee']);
        }

        if ($val = Common::iset($filterData['taxDateFrom'])) {
            array_push($whereTexts, "Tax Date From = {$val}");
        }

        if ($val = Common::iset($filterData['taxDateTo'])) {
            array_push($whereTexts, "Tax Date To = {$val}");
        }

        if ($val = Common::iset($filterData['lastPaidDateFrom'])) {
            array_push($whereTexts, "Last Paid Date From = {$val}");
        }

        if ($val = Common::iset($filterData['lastPaidDateTo'])) {
            array_push($whereTexts, "Last Paid Date To = {$val}");
        }

        if ($val = Common::iset($filterData['netFrom'])) {
            array_push($whereTexts, "Net From = {$val}");
        }

        if ($val = Common::iset($filterData['netTo'])) {
            array_push($whereTexts, "Net To = {$val}");
        }

        if ($val = Common::iset($filterData['taxFrom'])) {
            array_push($whereTexts, "Tax From = {$val}");
        }

        if ($val = Common::iset($filterData['taxTo'])) {
            array_push($whereTexts, "Tax To = {$val}");
        }

        if ($val = Common::iset($filterData['grossFrom'])) {
            array_push($whereTexts, "Gross From = {$val}");
        }

        if ($val = Common::iset($filterData['grossTo'])) {
            array_push($whereTexts, "Gross To = {$val}");
        }

        $status = [];
        if ($val = Common::iset($filterData['printed'])) {
            array_push($status, 'Printed');
        }
        if ($val = Common::iset($filterData['generated'])) {
            array_push($status, 'Generated');
        }
        if ($val = Common::iset($filterData['posted'])) {
            array_push($status, 'Posted');
        }
        if ($val = Common::iset($filterData['paid'])) {
            array_push($status, 'Paid');
        }
        if ($val = Common::iset($filterData['cancelled'])) {
            array_push($status, 'Cancelled');
        }

        if ($status != []) {
            array_push($whereTexts, 'Status = ' . implode(', ', $status));
        }

        return $query;
    }

    public function reAddEstatesMultipleRecordTypesQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {

        $estRecordService = new \Tfcloud\Services\Estate\EstRecordService($this->permissionService);
        //$estRecordQuery  = $estRecordService->filterAll($query, $filterData);
        $estRecordQuery  = $estRecordService->filterAll($query, $filterData, true);

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Code contains '{$val}'");
        }

        if ($val = Common::iset($filterData['$description'])) {
            array_push($whereTexts, "Description contains '{$val}'");
        }

        if ($val = Common::iset($filterData['active'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, "Status = 'Inactive'");
                    break;
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, "Status = 'Active'");
                    break;
                default:
                    array_push($whereTexts, "Status = 'All'");
                    break;
            }
        }

        if ($val = Common::iset($filterData['recordType'])) {
            $this->estRecordType($val, $whereTexts);
        }

        return $estRecordQuery;
    }

    public function reAddEstateUnitMultipleRecordTypesQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $estUnitRecordService = new EstateUnitService(
            $this->permissionService,
            new LeaseOutLetuService($this->permissionService, new UserDefinedService())
        );

        $estRecordQuery  = $estUnitRecordService->filterAll($query, $filterData, false, true);

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Lettable Unit Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Lettable Unit Description contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['contact'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Landlord']);
        }

        if ($val = Common::iset($filterData['tenant'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Tenant']);
        }

        if ($val = Common::iset($filterData['owner'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, \Lang::get('validation.attributes.owner')]);
        }

        if ($val = Common::iset($filterData['available'])) {
            array_push($whereTexts, "Available = $val");
        }

        if ($val = Common::iset($filterData['vacant'])) {
            array_push($whereTexts, "Vacant = $val");
        }

        if ($val = Common::iset($filterData['status'])) {
            array_push(
                $whereCodes,
                ['lettable_unit_status', 'lettable_unit_status_id', 'lettable_unit_status_code', $val, 'Status']
            );
        }

        // Special location
        if ($val = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);

            if ($val = Common::iset($filterData['building_id'])) {
                array_push($whereCodes, ['building', 'building_id', 'building_code', $val, "Building Code"]);

                if ($val = Common::iset($filterData['room_id'])) {
                    array_push($whereCodes, ['room', 'room_id', 'room_number', $val, "Room Number"]);
                }
            }
        }

        if ($val = Common::iset($filterData['address'])) {
            array_push($whereTexts, "Address contains '" . $val . "'");
        }
        $this->getAddressSub($filterData, $whereTexts);

        $dateFields = [
            ["parkingSpacesFrom", "Parking Spaces", "greater than or equal to"],
            ["parkingSpacesTo", "Parking Spaces", "less than or equal to"],
            ["giaFrom", "GIA", "greater than or equal to"],
            ["giaTo", "GIA", "less than or equal to"],
            ["niaFrom", "NIA", "greater than or equal to"],
            ["niaTo", "NIA", "less than or equal to"],
            ["annualRentFrom", "Annual Rent ", "greater than or equal to"],
            ["annualRentTo", "Annual Rent ", "less than or equal to"],
            ["marketRentFrom", "Market Rent ", "greater than or equal to"],
            ["marketRentTo", "Market Rent ", "less than or equal to"],
        ];

        foreach ($dateFields as $dateField) {
            $dateArg = $dateField[0];

            if ($val = Common::iset($filterData[$dateArg])) {
                $dateFieldDesc = $dateField[1];
                $dateFieldCompareText = $dateField[2];

                array_push($whereTexts, "$dateFieldDesc $dateFieldCompareText '" . $val . "'");
            }
        }

        return $estRecordQuery;
    }

    public function reAddEstateLeaseOutBreakDateQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $estBreakDateService = new EstBreakDateService(
            $this->permissionService,
            new LeaseOutService($this->permissionService),
            new LeaseInService($this->permissionService)
        );

        $estRecordQuery  = $estBreakDateService->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Lease Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['estBreakDateType'])) {
            array_push($whereCodes, [
                'est_break_date_type',
                'est_break_date_type_id',
                'est_break_date_type_desc',
                $val,
                'Break Date Served By'
            ]);
        }

        if ($val = Common::iset($filterData['leaseOutStatus'])) {
            array_push($whereCodes, [
                'lease_out_status',
                'lease_out_status_id',
                'lease_out_status_code',
                $val,
                \Lang::get('text.estate_module.lease_out_status')
            ]);
        }

        $dateFields = [
            ["breakDateFrom", "Break Date", "greater than or equal to"],
            ["breakDateTo", "Break Date", "less than or equal to"],
            ["actionedDateFrom", "Actioned Date", "greater than or equal to"],
            ["actionDateTo", "Actioned Date", "less than or equal to"]
        ];

        foreach ($dateFields as $dateField) {
            $dateArg = $dateField[0];

            if ($val = Common::iset($filterData[$dateArg])) {
                $dateFieldDesc = $dateField[1];
                $dateFieldCompareText = $dateField[2];

                array_push($whereTexts, "$dateFieldDesc $dateFieldCompareText '" . $val . "'");
            }
        }

        return $estRecordQuery;
    }

    public function reAddEstateLeaseInBreakDateQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $estBreakDateService = new EstBreakDateService(
            $this->permissionService,
            new LeaseOutService($this->permissionService),
            new LeaseInService($this->permissionService)
        );

        $estRecordQuery  = $estBreakDateService->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Lease Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['estBreakDateType'])) {
            array_push($whereCodes, [
                'est_break_date_type',
                'est_break_date_type_id',
                'est_break_date_type_desc',
                $val,
                'Break Date Served By'
            ]);
        }

        if ($val = Common::iset($filterData['leaseInStatus'])) {
            array_push($whereCodes, [
                'lease_in_status',
                'lease_in_status_id',
                'lease_in_status_code',
                $val,
                \Lang::get('text.estate_module.lease_in_status')
            ]);
        }

        $dateFields = [
            ["breakDateFrom", "Break Date", "greater than or equal to"],
            ["breakDateTo", "Break Date", "less than or equal to"],
            ["actionedDateFrom", "Actioned Date", "greater than or equal to"],
            ["actionDateTo", "Actioned Date", "less than or equal to"]
        ];

        foreach ($dateFields as $dateField) {
            $dateArg = $dateField[0];

            if ($val = Common::iset($filterData[$dateArg])) {
                $dateFieldDesc = $dateField[1];
                $dateFieldCompareText = $dateField[2];

                array_push($whereTexts, "$dateFieldDesc $dateFieldCompareText '" . $val . "'");
            }
        }

        return $estRecordQuery;
    }


    public function reAddEstatesServiceChargeQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $scPermissionService = new ScServiceChargePermissionService();
        $serviceChargeService = new ScServiceChargeService($scPermissionService);
        $scQuery  = $serviceChargeService->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Service Charge Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['title'])) {
            array_push($whereTexts, "Title contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['owner'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, \Lang::get('validation.attributes.owner')]);
        }

        if ($val = array_get($filterData, 'startDateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.start_date_from'), $val));
        }

        if ($val = array_get($filterData, 'startDateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.start_date_to'), $val));
        }

        if ($val = array_get($filterData, 'endDateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.end_date_from'), $val));
        }

        if ($val = array_get($filterData, 'endDateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.end_date_to'), $val));
        }

        if ($val = Common::iset($filterData['establishment'])) {
            array_push($whereCodes, ['establishment', 'establishment_id', 'establishment_code', $val, 'Establishment']);
        }

        // Location
        if ($val = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);

            if ($val = Common::iset($filterData['building_id'])) {
                array_push($whereCodes, ['building', 'building_id', 'building_code', $val, "Building Code"]);
            }
        }

        $status = [];
        if ($val = Common::iset($filterData['draft'])) {
            array_push($status, EstScServiceChargeStatus::getDescription(EstScServiceChargeStatus::DRAFT));
        }
        if ($val = Common::iset($filterData['open'])) {
            array_push($status, EstScServiceChargeStatus::getDescription(EstScServiceChargeStatus::OPEN));
        }
        if ($val = Common::iset($filterData['issued'])) {
            array_push($status, EstScServiceChargeStatus::getDescription(EstScServiceChargeStatus::ISSUED));
        }
        if ($val = Common::iset($filterData['complete'])) {
            array_push($status, EstScServiceChargeStatus::getDescription(EstScServiceChargeStatus::COMPLETE));
        }
        if ($val = Common::iset($filterData['close'])) {
            array_push($status, EstScServiceChargeStatus::getDescription(EstScServiceChargeStatus::CLOSE));
        }

        if ($status != []) {
            array_push($whereTexts, 'Status = ' . implode(', ', $status));
        }

        $this->reAddUserDefinesQuery(GenTable::EST_SERVICE_CHARGE, $filterData, $whereCodes, $whereTexts);

        return $scQuery;
    }

    public function reAddEstatesServiceChargeActualQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $scPermissionService = new ScServiceChargePermissionService();
        $scActualService = new ScActualService($scPermissionService);
        $scQuery  = $scActualService->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['est_sc_service_charge_id'])) {
            array_push(
                $whereCodes,
                [
                    'est_sc_service_charge',
                    'est_sc_service_charge_id',
                    'est_sc_service_charge_code',
                    $val,
                    'Service Charge'
                ]
            );
        }

        if ($val = Common::iset($filterData['est_sc_service_charge_code'])) {
            array_push($whereTexts, "Service Charge Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['title'])) {
            array_push($whereTexts, "Title contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['est_sc_actual_code'])) {
            array_push($whereTexts, "Actual Code contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'actualDateFrom', null)) {
            array_push($whereTexts, 'Actual Date From', $val);
        }

        if ($val = array_get($filterData, 'actualDateTo', null)) {
            array_push($whereTexts, 'Actual Date To', $val);
        }

        return $scQuery;
    }

    public function reAddEstatesServiceChargeLetuQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $scPermissionService = new ScServiceChargePermissionService();
        $permissionService = new PermissionService();
        $userDefinedService = new UserDefinedService();
        $leaseOutLetuService = new LeaseOutLetuService($permissionService, $userDefinedService);
        $lettableUnitService = new LettableUnitService($permissionService, $leaseOutLetuService);
        $scServiceChargeService = new ScServiceChargeService($scPermissionService, $userDefinedService);
        $scApportionmentService = new ScApportionmentService($scPermissionService, $scServiceChargeService);

        $scLetuService = new ScLetuService(
            $scPermissionService,
            $scApportionmentService,
            $scServiceChargeService,
            $lettableUnitService
        );

        $scQuery  = $scLetuService->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['est_sc_service_charge_id'])) {
            array_push(
                $whereCodes,
                [
                    'est_sc_service_charge',
                    'est_sc_service_charge_id',
                    'est_sc_service_charge_code',
                    $val,
                    'Service Charge'
                ]
            );
        }

        if ($val = Common::iset($filterData['est_sc_service_charge_code'])) {
            array_push($whereTexts, "Service Charge Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Lettable Unit Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Lettable Unit  Description contains '" . $val . "'");
        }

        return $scQuery;
    }

    public function reAddEstatesServiceChargeScheduleQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $scPermissionService = new ScServiceChargePermissionService();
        $userDefinedService = new UserDefinedService();
        $scServiceChargeService = new ScServiceChargeService($scPermissionService, $userDefinedService);
        $scApportionmentService = new ScApportionmentService($scPermissionService, $scServiceChargeService);
        $scScheduleService = new ScScheduleService($scPermissionService, $scApportionmentService);

        $scQuery  = $scScheduleService->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['est_sc_service_charge_id'])) {
            array_push(
                $whereCodes,
                [
                    'est_sc_service_charge',
                    'est_sc_service_charge_id',
                    'est_sc_service_charge_code',
                    $val,
                    'Service Charge'
                ]
            );
        }

        if ($val = Common::iset($filterData['est_sc_service_charge_code'])) {
            array_push($whereTexts, "Service Charge Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['schedule_code'])) {
            array_push($whereTexts, "Schedule Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['schedule_title'])) {
            array_push($whereTexts, "Schedule Title contains '" . $val . "'");
        }

        return $scQuery;
    }

    private function estRecordType($val, &$whereTexts)
    {
        $recordType = 'Record Type(s) = ';

        foreach ($val as $v) {
            if ($v == EstRecordType::ACQUISITION) {
                $recordType .= 'Acquisition,';
            }

            if ($v == EstRecordType::DEED_PACKET) {
                $recordType .= 'Deed Packet,';
            }

            if ($v == EstRecordType::FREEHOLD) {
                $recordType .= 'Freehold,';
            }

            if ($v == EstRecordType::LEASE_IN) {
                $recordType .= 'Lease In,';
            }

            if ($v == EstRecordType::LETTABLE_UNIT) {
                $recordType .= 'Lettable Unit,';
            }

            if ($v == EstRecordType::LETTING_PACKET) {
                $recordType .= 'Letting Packet,';
            }

            if ($v == EstRecordType::LEASE_OUT) {
                $recordType .= 'Lease Out,';
            }

            if ($v == EstRecordType::DISPOSAL) {
                $recordType .= 'Disposal,';
            }

            if ($v == EstRecordType::VALUATION) {
                $recordType .= 'Valuation,';
            }
            if ($v == EstRecordType::OCCUPANCY_UNIT) {
                $recordType .= 'Occupancy Unit,';
            }
        }
        $recordType = rtrim($recordType, ",");
        array_push($whereTexts, "'$recordType'");

        return;
    }

    public function reAddEstateLettableUnitEnergyRatingQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $leaseOutLetuService = new LeaseOutLetuService($this->permissionService);
        $lettableUnitService = new LettableUnitService($this->permissionService, $leaseOutLetuService);

        $query  = $lettableUnitService->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['energy_current_epc_rating_id'])) {
            $whereCodes[] = [
                'energy_epc_rating',
                'energy_epc_rating_id',
                'energy_epc_rating_code',
                $val,
                'Current EPC Rating'
            ];
        }

        if ($val = Common::iset($filterData['code'])) {
            $whereTexts[] = "Lettable Unit Code contains '" . $val . "'";
        }

        if ($val = Common::iset($filterData['description'])) {
            $whereTexts[] = "Lettable Unit Description contains '" . $val . "'";
        }

        if ($val = Common::iset($filterData['address'])) {
            $whereTexts[] = "Address contains '" . $val . "'";
        }
        $this->getAddressSub($filterData, $whereTexts);

        if ($val = Common::iset($filterData['site_id'])) {
            $whereCodes[] = ['site', 'site_id', 'site_code', $val, "Site Code"];

            if ($val = Common::iset($filterData['building_id'])) {
                $whereCodes[] = ['building', 'building_id', 'building_code', $val, "Building Code"];

                if ($val = Common::iset($filterData['room_id'])) {
                    $whereCodes[] = ['room', 'room_id', 'room_number', $val, "Room Number"];
                }
            }
        }
        if ($val = Common::iset($filterData['owner'])) {
            $whereCodes[] = ['user', 'id', 'display_name', $val, 'Owner User'];
        }
        if ($val = Common::iset($filterData['contact'])) {
            $whereCodes[] = ['contact', 'contact_id', 'contact_name', $val, 'Landlord'];
        }
        if ($val = Common::iset($filterData['vacant'])) {
            $whereTexts[] = "Vacant = $val";
        }

        if ($val = Common::iset($filterData['status'])) {
            $whereCodes[] = [
                'lettable_unit_status',
                'lettable_unit_status_id',
                'lettable_unit_status_code',
                $val,
                'Status'
            ];
        }

        if ($val = array_get($filterData, 'epcExpiryDateFrom', null)) {
            $whereTexts[] = sprintf(\Lang::get('text.report_texts.epc_expiry_date_from'), $val);
        }
        if ($val = array_get($filterData, 'epcExpiryDateFrom', null)) {
            $whereTexts[] = sprintf(\Lang::get('text.report_texts.epc_expiry_date_to'), $val);
        }
        if ($val = array_get($filterData, 'dateOfInspectionFrom', null)) {
            $whereTexts[] = sprintf(\Lang::get('text.report_texts.date_of_inspection_from'), $val);
        }
        if ($val = array_get($filterData, 'dateOfInspectionTo', null)) {
            $whereTexts[] = sprintf(\Lang::get('text.report_texts.date_of_inspection_to'), $val);
        }

        if ($val = Common::iset($filterData['totalCostFrom'])) {
            $whereTexts[] = "Total Cost from '" . $val . "'";
        }

        if ($val = Common::iset($filterData['totalCostTo'])) {
            $whereTexts[] = "Total Cost to '" . $val . "'";
        }
        return $query;
    }
}
