<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Filters\IdworkFilter;
use Tfcloud\Models\CondsurveyStatus;
use Tfcloud\Models\GenTable;
use Tfcloud\Models\IdworkStatusType;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\Condition\ConditionService;
use Tfcloud\Services\Condition\IdworkService;
use Tfcloud\Services\PermissionService;

class ConditionsFilterQueryService extends BaseService
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    public function reAddCondQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $conditionService = new ConditionService($this->permissionService);
        $condQuery = $conditionService->filterAll($query, $filterData, $viewName);

        // Buiding Block
        if ($val = Common::iset($filterData['block_id'])) {
            array_push($whereCodes, [
                'building_block',
                'building_block_id',
                'building_block_code',
                $val,
                "Building Block Code"
            ]);
        }

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Description contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['surveyor'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Surveyor']);
        }

        if ($val = Common::iset($filterData['second_surveyor_id'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Second Surveyor']);
        }

        if ($val = Common::iset($filterData['electrical_surveyor_id'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Electrical Surveyor']);
        }

        if ($val = Common::iset($filterData['mechanical_surveyor_id'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Mechanical Surveyor']);
        }

        if ($val = Common::iset($filterData['reference'])) {
            array_push($whereTexts, "Reference contains '" . $val . "'");
        }

        $status = [];
        if (Common::iset($filterData['open'])) {
            $status[] = CondsurveyStatus::OPEN;
        }
        if (Common::iset($filterData['qa'])) {
            $status[] = CondsurveyStatus::QA;
        }
        if (Common::iset($filterData['complete'])) {
            $status[] = CondsurveyStatus::COMPLETE;
        }
        if (Common::iset($filterData['historic'])) {
            $status[] = CondsurveyStatus::HISTORIC;
        }
        if (count($status)) {
            array_push($orCodes, [
                'condsurvey_status',
                'condsurvey_status_id',
                'condsurvey_status_code',
                implode(',', $status),
                "Survey Status"
            ]);
        }

        if ($val = Common::iset($filterData['siteStatus'])) {
            switch ($val) {
                case 'Y':
                    array_push($whereTexts, "Site Status = 'Active'");
                    break;
                case 'N':
                    array_push($whereTexts, "Site Status = 'Inactive'");
                    break;
                case 'all':
                    break;
            }
        }

        if ($val = Common::iset($filterData['prop_zone_id'])) {
            array_push($whereCodes, ['prop_zone', 'prop_zone_id', 'prop_zone_code', $val, 'Zone']);

            if ($val = Common::iset($filterData['prop_external_id'])) {
                array_push(
                    $whereCodes,
                    ['prop_external', 'prop_external_id', 'prop_external_code', $val, 'External Area']
                );
            }
        }

        if ($val = Common::iset($filterData['owner'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, \Lang::get('text.owner')]);
        }

        $this->reAddUserDefinesQuery(GenTable::CONDITON, $filterData, $whereCodes, $whereTexts);

        return $condQuery;
    }

    public function reAddCondIWQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $idworkService = new IdworkService($this->permissionService);
        $condIWQuery = $idworkService->filterAll($query, $filterData, $viewName);

        // Buiding Block
        if ($val = Common::iset($filterData['block_id'])) {
            array_push($whereCodes, [
                'building_block',
                'building_block_id',
                'building_block_code',
                $val,
                "Building Block Code"
            ]);
        }

        if ($val = Common::iset($filterData['filter_iw'])) {
            array_push($whereTexts, "Code, Remedy or Defect contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['remainingLifeFrom'])) {
            array_push($whereTexts, "Remaining Life >= '" . $val . "'");
        }

        if ($val = Common::iset($filterData['remainingLifeTo'])) {
            array_push($whereTexts, "Remaining Life <= '" . $val . "'");
        }

        if ($val = Common::iset($filterData['costFrom'])) {
            array_push($whereTexts, "Cost >= '" . $val . "'");
        }

        if ($val = Common::iset($filterData['costTo'])) {
            array_push($whereTexts, "to <= '" . $val . "'");
        }

        if ($val = Common::iset($filterData['element_id'])) {
            array_push($whereCodes, [
                'idwork_element',
                'idwork_element_id',
                'idwork_element_code',
                $val,
                "Element Code"
            ]);

            if ($val = Common::iset($filterData['sub_element_id'])) {
                array_push($whereCodes, [
                    'idwork_subelement',
                    'idwork_subelement_id',
                    'idwork_subelement_code',
                    $val,
                    "Sub Element Code"
                ]);

                if ($val = Common::iset($filterData['item_id'])) {
                    array_push($whereCodes, [
                        'idwork_item',
                        'idwork_item_id',
                        'idwork_item_code',
                        $val,
                        "Item Code"
                    ]);
                }
            }
        }

        if ($val = Common::iset($filterData['category_id'])) {
            array_push($whereCodes, [
                'idwork_category',
                'idwork_category_id',
                'idwork_category_code',
                $val,
                "Category Code"
            ]);
        }

        if ($val = Common::iset($filterData['cyclical_maintenance'])) {
            switch ($val) {
                case CommonConstant::YES:
                    array_push($whereTexts, "Cyclical Maintenance = 'Yes'");
                    break;

                case CommonConstant::NO:
                    array_push($whereTexts, "Cyclical Maintenance = 'No'");
                    break;

                case CommonConstant::ALL:
                default:
                    break;
            }
        }

        if (($val = Common::iset($filterData['idwork_priority_id'])) && count($val)) {
            array_push($orCodes, [
                'idwork_priority',
                'idwork_priority_id',
                'idwork_priority_code',
                implode(',', $val),
                "Priority Code"
            ]);
        }

        if (($val = Common::iset($filterData['idwork_condition_id'])) && count($val)) {
            array_push($orCodes, [
                'idwork_condition',
                'idwork_condition_id',
                'idwork_condition_code',
                implode(',', $val),
                "Condition Code"
            ]);
        }

        if ($val = Common::iset($filterData['idworkStatus'])) {
            array_push(
                $whereCodes,
                ['idwork_status', 'idwork_status_id', 'idwork_status_code', $val, 'Status']
            );
        }

        $statusType = [];
        if (Common::iset($filterData['draft'])) {
            $statusType[] = IdworkStatusType::DRAFT;
        }
        if (Common::iset($filterData['plan'])) {
            $statusType[] = IdworkStatusType::PLAN;
        }
        if (Common::iset($filterData['tender'])) {
            $statusType[] = IdworkStatusType::TENDER;
        }
        if (Common::iset($filterData['wip'])) {
            $statusType[] = IdworkStatusType::WIP;
        }
        if (Common::iset($filterData['complete'])) {
            $statusType[] = IdworkStatusType::COMPLETE;
        }
        if (Common::iset($filterData['superseded'])) {
            $statusType[] = IdworkStatusType::SUPERSEDED;
        }
        if (count($statusType)) {
            array_push($orCodes, [
                'idwork_status_type',
                'idwork_status_type_id',
                'idwork_status_type_code',
                implode(',', $statusType),
                "Identified Work Status Type"
            ]);
        }

        // Condition Survey Code
        if ($val = Common::iset($filterData['condition_survey_code'])) {
            array_push($whereTexts, "Condition Survey Code contains '" . $val . "'");
        }
        if ($val = Common::iset($filterData['establishment'])) {
            array_push(
                $whereCodes,
                array('establishment', 'establishment_id', 'establishment_code', $val, 'Establishment')
            );
        }
        if ($val = Common::iset($filterData['prop_zone_id'])) {
            array_push($whereCodes, ['prop_zone', 'prop_zone_id', 'prop_zone_code', $val, 'Zone']);

            if ($val = Common::iset($filterData['prop_external_id'])) {
                array_push(
                    $whereCodes,
                    ['prop_external', 'prop_external_id', 'prop_external_code', $val, 'External Area']
                );
            }
        }
        $this->reAddUserDefinesQuery(GenTable::IDWORK, $filterData, $whereCodes, $whereTexts);

        return $condIWQuery;
    }

    public function reAddCondItemsQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $condItemQuery = $this->filterAllConditionItems($query, $filterData);

        if ($val = Common::iset($filterData['filter_report'])) {
            array_push($whereTexts, "Element, Sub Element or Item contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['element_id'])) {
            array_push($whereCodes, [
                'idwork_element',
                'idwork_element_id',
                'idwork_element_code',
                $val,
                "Element Code"
            ]);

            if ($val = Common::iset($filterData['sub_element_id'])) {
                array_push($whereCodes, [
                    'idwork_subelement',
                    'idwork_subelement_id',
                    'idwork_subelement_code',
                    $val,
                    "Sub Element Code"
                ]);

                if ($val = Common::iset($filterData['item_id'])) {
                    array_push($whereCodes, [
                        'idwork_item',
                        'idwork_item_id',
                        'idwork_item_code',
                        $val,
                        "Item Code"
                    ]);
                }
            }
        }

        if ($val = Common::iset($filterData['show'])) {
            switch ($val) {
                case 'showBuildings':
                    array_push($whereTexts, "Show Type = 'Buildings'");
                    break;

                case 'showRooms':
                    array_push($whereTexts, "Show Type = ' Rooms'");
                    break;

                case CommonConstant::ALL:
                default:
                    break;
            }
        }

        return $condItemQuery;
    }

    public function bindFilterParametersPCND01($parameters, $inputs)
    {
        if (array_get($inputs, 'code', false)) {
            $parameters = $parameters . "&filter_condsurvey_code=" . $inputs['code'];
        } else {
            $parameters = $parameters . "&filter_condsurvey_code=";
        }

        return $parameters;
    }

    private function filterAllConditionItems($query, $inputs)
    {
        $filter = new IdworkFilter($inputs);

        if (\Auth::User()->isSelfServiceUser()) {
            $query->where('condsurvey_status_id', '<>', CondsurveyStatus::OPEN)
                ->where('condsurvey_status_id', '<>', CondsurveyStatus::QA);
        }

        if (!is_null($filter->filter_report)) {
            $query->where(function ($query) use ($filter) {
                $query->where('comments', 'like', "%{$filter->filter_report}%")
                    ->orwhere('idwork_element_code', 'like', "%{$filter->filter_report}%")
                    ->orwhere('idwork_element_desc', 'like', "%{$filter->filter_report}%")
                    ->orwhere('idwork_subelement_code', 'like', "%{$filter->filter_report}%")
                    ->orwhere('idwork_subelement_desc', 'like', "%{$filter->filter_report}%")
                    ->orwhere('idwork_item_code', 'like', "%{$filter->filter_report}%")
                    ->orwhere('idworkitem_desc', 'like', "%{$filter->filter_report}%");
            });
        }

        if (!is_null($filter->element_id)) {
            $query->where('idwork_element_id', $filter->element_id);

            if (!is_null($filter->sub_element_id)) {
                $query->where('idwork_subelement_id', $filter->sub_element_id);

                if (!is_null($filter->item_id)) {
                    $query->where('idwork_item_id', $filter->item_id);
                }
            }
        }

        if (!is_null($filter->show)) {
            if ($filter->show == 'showBuildings') {
                $query->whereNull('room_id');
            } elseif ($filter->show == 'showRooms') {
                $query->whereNotNull('room_id');
            }
        }

        return $query;
    }
}
