<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Services\PermissionService;
use Tfcloud\Services\CostPlus\CpContractService;
use Tfcloud\Services\BaseService;

class CostPlusContractFilterQueryService extends BaseService
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    public function reAddCpContractQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $costPlusContractService = new CpContractService(
            $this->permissionService
        );
        $query = $costPlusContractService->filterAll($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'code', null)) {
            array_push($whereTexts, " Code contains '$val'");
        }

        if ($val = array_get($filterData, 'name', null)) {
            array_push($whereTexts, "  Name contains '$val'");
        }

        if ($val = array_get($filterData, 'description', null)) {
            array_push($whereTexts, "  Description contains '$val'");
        }

        if ($val = array_get($filterData, 'Owner', null)) {
            array_push($whereCodes, [
                'user',
                'id',
                'display_name',
                $val,
                'Owner'
                //\Form::fieldLang('location')
            ]);
        }

        if ($val = array_get($filterData, 'clientRef', null)) {
            array_push($whereTexts, "  Client Ref contains '$val'");
        }

        if ($val = array_get($filterData, 'customer', null)) {
            array_push($whereCodes, [
                'contact',
                'contact_id',
                'contact_name',
                $val,
                'Customer'
                //\Form::fieldLang('location')
            ]);
        }

        $val = array_get($filterData, 'active', null);
        if (!is_null($val)) {
            switch ($val) {
                case 'active':
                    array_push($whereTexts, " Active = 'Active'");
                    break;
                case 'archived':
                    array_push($whereTexts, " Active = 'Archived'");
                    break;
                default:
                    array_push($whereTexts, " Active = 'All'");
                    break;
            }
        }

        if ($val = array_get($filterData, 'site_id', null)) {
            array_push(
                $whereCodes,
                [
                    'site',
                    'site_id',
                    'site_code',
                    $val,
                    "Site Code"
                ]
            );
        }

        if ($val = array_get($filterData, 'establishment', null)) {
            array_push(
                $whereCodes,
                [
                    'establishment',
                    'establishment_id',
                    'establishment_code',
                    $val,
                    "Establishment"
                ]
            );
        }

        return $query;
    }
}
