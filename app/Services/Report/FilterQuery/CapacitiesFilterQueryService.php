<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\Education\CapacityService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Property\RoomCapacityService;

class CapacitiesFilterQueryService extends BaseService
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    public function reAddCapacityQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $suffQuery = CapacityService::filterAll($query, $filterData, $viewName, false);

        if ($val = array_get($filterData, 'code')) {
            array_push($whereTexts, "Code contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'description')) {
            array_push($whereTexts, "Description contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'type')) {
            array_push(
                $whereCodes,
                ['cap_type', 'cap_type_id', 'cap_type_code', $val, 'Capacity Type']
            );
        }

        if ($val = array_get($filterData, 'status')) {
            array_push(
                $orCodes,
                ['cap_status', 'cap_status_id', 'cap_status_desc', $val, 'Capacity Status']
            );
        }

        if ($val = array_get($filterData, 'assessmentDateFrom')) {
            array_push($whereTexts, "Assessment Date from '{$val}'");
        }

        if ($val = array_get($filterData, 'assessmentDateTo')) {
            array_push($whereTexts, "Assessment Date to '{$val}'");
        }

        if ($val = array_get($filterData, 'site_id')) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);
        }

        if ($val = array_get($filterData, 'assessor')) {
            array_push(
                $whereCodes,
                ['contact', 'contact_id', 'contact_name', $val, 'Assessor']
            );
        }

        return $suffQuery;
    }

    public function reAddRoomCapQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $roomCapQuery = RoomCapacityService::filterAll($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'room_id')) {
            array_push($whereCodes, ['room', 'room_id', 'room_number', $val, "Room Number"]);
        } elseif ($val = array_get($filterData, 'building_id')) {
            array_push($whereCodes, ['building', 'building_id', 'building_code', $val, "Building Code"]);
        } elseif ($val = array_get($filterData, 'site_id')) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);
        }

        if ($val = array_get($filterData, 'capRoomType')) {
            array_push(
                $whereCodes,
                ['cap_room_type', 'cap_room_type_id', 'cap_room_type_code', $val, 'Capacity Room Type']
            );
        } else {
            array_push($whereTexts, "Capacity Room Type = '" . CommonConstant::ALL . "'");
        }

        if ($val = array_get($filterData, 'exclude')) {
            array_push($whereTexts, "Excluded = '{$val}'");
        } else {
            array_push($whereTexts, "Excluded = '" . CommonConstant::ALL . "'");
        }

        return $roomCapQuery;
    }
}
