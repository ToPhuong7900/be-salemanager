<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Models\TreeSurveyStatus;
use Tfcloud\Models\StockStocktakeStatus;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\Personnel\PersonnelBookingsService;
use Tfcloud\Services\Tree\TreeSurveyActionService;
use Tfcloud\Services\Tree\TreeSurveyService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\UserDefinedService;

class PersonnelFilterQueryService extends BaseService
{
    protected $permissionService;
    protected $userDefinedService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    public function reAddPersonnelBookingsQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $personnelBookingService = new PersonnelBookingsService($this->permissionService);
        $personnelBookingQuery = $personnelBookingService->filterAll($query, $filterData, $viewName);

        $this->setFilteringDetails($filterData, $whereCodes, $orCodes, $whereTexts);

        return $personnelBookingQuery;
    }

    private function setFilteringDetails($filterData, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $reasonTypeIds = array_get($filterData, 'reasonTypeIds');

        if ($reasonTypeIds && is_array($reasonTypeIds)) {
            array_push($orCodes, [
                    'pers_reason_type',
                    'pers_reason_type_id',
                    'pers_reason_type_code',
                    implode(',', $reasonTypeIds),
                    'Reason Type'
            ]);
        }

        if ($val = array_get($filterData, 'startDate')) {
            array_push($whereTexts, "Date from $val");
        }

        if ($val = array_get($filterData, 'endDate')) {
            array_push($whereTexts, "Date to $val");
        }

        if ($val = array_get($filterData, 'lineManagerId')) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Line Manager']);
        }

        if ($val = array_get($filterData, 'userGroupId')) {
            array_push($whereCodes, ['usergroup', 'usergroup_id', 'usergroup_code', $val, 'User Group']);
        }

        if ($val = array_get($filterData, 'dloTeamId')) {
            array_push($whereCodes, ['team', 'team_id', 'team_code', $val, 'Dlo Team']);
        }

        if ($val = array_get($filterData, 'userId')) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'User']);
        }

        if ($val = array_get($filterData, 'reasonId')) {
            array_push($whereCodes, ['pers_reason', 'pers_reason_id', 'pers_reason_code', $val, 'Reason']);
        }
    }
}
