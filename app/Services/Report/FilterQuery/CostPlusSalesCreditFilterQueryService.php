<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\Common;
use Tfcloud\Models\CpSalesCreditStatus;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\CostPlus\CpSalesCreditService;
use Tfcloud\Services\BaseService;

class CostPlusSalesCreditFilterQueryService extends BaseService
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    public function reAddCpSalesCreditQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $costPlusSalesCreditService = new CpSalesCreditService(
            $this->permissionService
        );
        $query = $costPlusSalesCreditService->filterAll($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'code', null)) {
            array_push($whereTexts, " Code contains '$val'");
        }

        if ($val = array_get($filterData, 'desc', null)) {
            array_push($whereTexts, " Description contains '$val'");
        }

        if ($val = array_get($filterData, 'invCode', null)) {
            array_push($whereTexts, " Invoice Code contains '$val'");
        }

        if ($val = array_get($filterData, 'taxDateFrom', null)) {
            array_push($whereTexts, 'Tax Date From ' . " $val" . "'");
        }

        if ($val = array_get($filterData, 'taxDateTo', null)) {
            array_push($whereTexts, 'Tax Date To ' . " $val" . "'");
        }

        if ($val = array_get($filterData, 'netTotalFrom', null)) {
            array_push($whereTexts, 'Net Total From ' . " $val" . "'");
        }

        if ($val = array_get($filterData, 'netTotalTo', null)) {
            array_push($whereTexts, 'Net Total To ' . " $val" . "'");
        }

        $statusType = [];

        if (Common::iset($filterData['draft'])) {
            $statusType[] = CpSalesCreditStatus::DRAFT;
        }
        if (Common::iset($filterData['approved'])) {
            $statusType[] = CpSalesCreditStatus::APPROVED;
        }
        if (Common::iset($filterData['paid'])) {
            $statusType[] = CpSalesCreditStatus::PAID;
        }
        if (Common::iset($filterData['cancelled'])) {
            $statusType[] = CpSalesCreditStatus::CANCELLED;
        }
        if (count($statusType)) {
            array_push($orCodes, [
                    'cp_sales_credit_status',
                    'cp_sales_credit_status_id',
                    'cp_sales_credit_status_desc',
                    implode(',', $statusType),
                    \Lang::get('text.status_type')
            ]);
        }

        return $query;
    }
}
