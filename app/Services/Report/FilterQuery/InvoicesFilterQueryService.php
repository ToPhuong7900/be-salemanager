<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Filters\Invoice\InvoiceFilter;
use Tfcloud\Models\FinAuthStatus;
use Tfcloud\Models\FinCertStatus;
use Tfcloud\Models\InstructionParentType;
use Tfcloud\Models\InvoiceStatus;
use Tfcloud\Models\SiteType;
use Tfcloud\Models\InvoiceStatusType;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\Invoice\InvoiceListService;
use Tfcloud\Services\PermissionService;

class InvoicesFilterQueryService extends BaseService
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    public function reAddQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $invoiceService = new InvoiceListService($this->permissionService);
        $invoiceQuery = $invoiceService->filterAll($query, $filterData, $viewName, $viewName == 'vw_inv03');

        if ($val = array_get($filterData, 'invoice_no', array_get($filterData, 'invoiceNo', null))) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.invoice_no'), $val));
        }

        if ($val = array_get($filterData, 'invoice_desc', array_get($filterData, 'invoiceDesc', null))) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.invoice_desc'), $val));
        }
        if ($val = array_get($filterData, 'owner')) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, \Lang::get('text.owner')]);
        }

        // Special location
        if ($val = array_get($filterData, 'site_id')) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);

            if ($val = array_get($filterData, 'building_id')) {
                array_push($whereCodes, ['building', 'building_id', 'building_code', $val, "Building Code"]);

                if ($val = array_get($filterData, 'room_id')) {
                    array_push($whereCodes, ['room', 'room_id', 'room_number', $val, "Room Number"]);
                }
            }
        }

        if ($val = Common::iset($filterData['supplier_contact_id'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, \Lang::get('text.supplier')]);
        }

        if ($val = Common::iset($filterData['certifier'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, \Lang::get('text.certifier')]);
        }

        if ($val = Common::iset($filterData['authoriser'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, \Lang::get('text.authoriser')]);
        }

        if ($val = Common::iset($filterData['account'])) {
            array_push($whereCodes, [
                'fin_account',
                'fin_account_id',
                'fin_account_code',
                $val, \Lang::get('text.account')
            ]);
        }

        if ($val = Common::iset($filterData['fin_year_id'])) {
            array_push($whereCodes, [
                'fin_year',
                'fin_year_id',
                'fin_year_code',
                $val, \Lang::get('text.financial_year')
            ]);
        }

        if ($val = array_get($filterData, 'netTotalFrom', 0)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.net_total_from'), $val));
        }

        if ($val = array_get($filterData, 'netTotalTo', 0)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.net_total_to'), $val));
        }

        if ($val = array_get($filterData, 'instruction_code', array_get($filterData, 'instructionCode', null))) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.linked_instruction_code'), $val));
        }

        if ($val = array_get($filterData, 'instruction_code', array_get($filterData, 'instructionCode', null))) {
            array_push($whereTexts, \Form::fieldLang('instruction_code_full') . " contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'parentCode', array_get($filterData, 'parentCode', null))) {
            array_push($whereTexts, 'Parent Code' . " contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'received_from', array_get($filterData, 'receivedFrom', null))) {
            array_push($whereTexts, \Form::fieldLang('received_date_from') . " $val");
        }

        if ($val = array_get($filterData, 'received_to', array_get($filterData, 'receivedTo', null))) {
            array_push($whereTexts, \Form::fieldLang('to') . " $val");
        }

        if ($val = array_get($filterData, 'approved_from', array_get($filterData, 'approvedFrom', null))) {
            array_push($whereTexts, \Form::fieldLang('approval_date_from') . " $val");
        }

        if ($val = array_get($filterData, 'approved_to', array_get($filterData, 'approvedTo', null))) {
            array_push($whereTexts, \Form::fieldLang('to') . " $val");
        }

        if ($val = array_get($filterData, 'tax_from', array_get($filterData, 'taxFrom', null))) {
            array_push($whereTexts, \Form::fieldLang('tax_date_from') . " $val");
        }

        if ($val = array_get($filterData, 'tax_to', array_get($filterData, 'taxTo', null))) {
            array_push($whereTexts, \Form::fieldLang('to') . " $val");
        }

        if ($val = array_get($filterData, 'audit_date_from', array_get($filterData, 'auditDateFrom', null))) {
            array_push($whereTexts, \Form::fieldLang('audit_date_from') . " $val");
        }

        if ($val = array_get($filterData, 'audit_date_to', array_get($filterData, 'auditDateTo', null))) {
            array_push($whereTexts, \Form::fieldLang('to') . " $val");
        }

        $statusType = [];
        if (Common::iset($filterData['registered'])) {
            $statusType[] = InvoiceStatusType::REGISTERED;
        }
        if (Common::iset($filterData['approved'])) {
            $statusType[] = InvoiceStatusType::APPROVED;
        }
        if (Common::iset($filterData['posted'])) {
            $statusType[] = InvoiceStatusType::POSTED;
        }
        if (Common::iset($filterData['rejected'])) {
            $statusType[] = InvoiceStatusType::REJECTED;
        }
        if (count($statusType)) {
            array_push($orCodes, [
                'invoice_status_type',
                'invoice_status_type_id',
                'invoice_status_type_code',
                implode(',', $statusType),
                \Lang::get('text.status_type')
            ]);
        }

        if (Common::iset($filterData['sourceType'])) {
            $result = InstructionParentType::select(
                ['instruction_parent_type_id', 'instruction_parent_type_name']
            )->where('instruction_parent_type_id', $filterData['sourceType'])->first();

            $code = $result->instruction_parent_type_name;
            $whereTexts[] = 'Source Type = ' . $code;
        }

        // Status
        $statusId = array_get($filterData, 'status');
        if ($statusId) {
            $status = InvoiceStatus::find($statusId);
            $whereTexts[] = 'Status Code = ' . $status->invoice_status_code;
        }

        // Site Type
        $siteTypeId = array_get($filterData, 'siteType');
        if ($siteTypeId) {
            $siteType = SiteType::find($siteTypeId);
            $whereTexts[] = 'Site Type = ' . $siteType->site_type_code;
        }

        $cert = [];
        if (Common::iset($filterData['certOpen'])) {
            $cert[] = FinCertStatus::OPEN;
        }
        if (Common::iset($filterData['certForCertification'])) {
            $cert[] = FinCertStatus::FOR_CERTIFICATION;
        }
        if (Common::iset($filterData['certCertified'])) {
            $cert[] = FinCertStatus::CERTIFIED;
        }
        if (Common::iset($filterData['certRejected'])) {
            $cert[] = FinCertStatus::REJECTED;
        }
        if (count($cert)) {
            array_push($orCodes, [
                    'fin_cert_status',
                    'fin_cert_status_id',
                    'name',
                    implode(',', $cert),
                    \Lang::get('text.certification')
            ]);
        }

        $authorisations = [];
        if ($val = array_get($filterData, 'authOpen')) {
            $authorisations[] = FinAuthStatus::OPEN;
        }
        if ($val = array_get($filterData, 'authApproval')) {
            $authorisations[] = FinAuthStatus::APPROVAL;
        }
        if ($val = array_get($filterData, 'authApproved')) {
            $authorisations[] = FinAuthStatus::APPROVED;
        }
        if ($val = array_get($filterData, 'authRejected')) {
            $authorisations[] = FinAuthStatus::REJECTED;
        }
        if (count($authorisations)) {
            array_push($orCodes, [
                    'fin_auth_status',
                    'fin_auth_status_id',
                    'name',
                    implode(',', $authorisations),
                    \Lang::get('text.authorisation')
            ]);
        }

        if ($val = Common::iset($filterData['auditor'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Spot Check Auditor']);
        }

        if ($val = Common::iset($filterData['spotCheckRequired'])) {
            switch ($val) {
                case 'Y':
                    array_push($whereTexts, "Spot Check Required = 'Yes'");
                    break;
                case 'N':
                    array_push($whereTexts, "Spot Check Required = 'No'");
                    break;
                case 'all':
                    break;
            }
        }

        $spotCheckStatus = [];
        if ($val = array_get($filterData, 'spotCheckForApproval')) {
            $spotCheckStatus[] = FinAuthStatus::APPROVAL;
        }
        if ($val = array_get($filterData, 'spotCheckApproved')) {
            $spotCheckStatus[] = FinAuthStatus::APPROVED;
        }
        if ($val = array_get($filterData, 'spotCheckRejected')) {
            $spotCheckStatus[] = FinAuthStatus::REJECTED;
        }
        if (count($spotCheckStatus)) {
            array_push($orCodes, [
                'fin_auth_status',
                'fin_auth_status_id',
                'name',
                implode(',', $spotCheckStatus),
                "Spot Check Audit"
            ]);
        }

        return $invoiceQuery;
    }

    public function reAddInvContQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $invoiceService = new InvoiceListService($this->permissionService);
        $invoiceQuery = $invoiceService->filterAllForInvContView($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'recordNo')) {
            array_push($whereTexts, "Record No contains '{$val}'");
        }

        if ($val = array_get($filterData, 'instructionCode')) {
            array_push($whereTexts, "Instruction Code contains '{$val}'");
        }

        if ($val = array_get($filterData, 'inspectionCode')) {
            array_push($whereTexts, "Inspection Code contains '{$val}'");
        }

        if ($val = array_get($filterData, 'afpCode')) {
            array_push($whereTexts, "Application Code contains '{$val}'");
        }

        // Special location
        if ($val = array_get($filterData, 'site_id')) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);

            if ($val = array_get($filterData, 'building_id')) {
                array_push($whereCodes, ['building', 'building_id', 'building_code', $val, "Building Code"]);

                if ($val = array_get($filterData, 'room_id')) {
                    array_push($whereCodes, ['room', 'room_id', 'room_number', $val, "Room Number"]);
                }
            }
        }

        $statusType = [];
        if (Common::iset($filterData['registered'])) {
            $statusType[] = InvoiceStatusType::REGISTERED;
        }
        if (Common::iset($filterData['approved'])) {
            $statusType[] = InvoiceStatusType::APPROVED;
        }
        if (Common::iset($filterData['posted'])) {
            $statusType[] = InvoiceStatusType::POSTED;
        }
        if (Common::iset($filterData['rejected'])) {
            $statusType[] = InvoiceStatusType::REJECTED;
        }
        if (count($statusType)) {
            array_push($orCodes, [
                'invoice_status_type',
                'invoice_status_type_id',
                'invoice_status_type_code',
                implode(',', $statusType),
                "Status Type"
            ]);
        }

        $authorisations = [];
        if ($val = array_get($filterData, 'authOpen')) {
            $authorisations[] = FinAuthStatus::OPEN;
        }
        if ($val = array_get($filterData, 'authApproval')) {
            $authorisations[] = FinAuthStatus::APPROVAL;
        }
        if ($val = array_get($filterData, 'authApproved')) {
            $authorisations[] = FinAuthStatus::APPROVED;
        }
        if ($val = array_get($filterData, 'authRejected')) {
            $authorisations[] = FinAuthStatus::REJECTED;
        }
        if (count($authorisations)) {
            array_push($orCodes, [
                    'fin_auth_status',
                    'fin_auth_status_id',
                    'name',
                    implode(',', $authorisations),
                    "Authorisation"
            ]);
        }

        if ($val = Common::iset($filterData['auditor'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Spot Check Auditor']);
        }

        if ($val = Common::iset($filterData['spotCheckRequired'])) {
            switch ($val) {
                case 'Y':
                    array_push($whereTexts, "Spot Check Required = 'Yes'");
                    break;
                case 'N':
                    array_push($whereTexts, "Spot Check Required = 'No'");
                    break;
                case 'all':
                    break;
            }
        }

        $spotCheckStatus = [];
        if ($val = array_get($filterData, 'spotCheckForApproval')) {
            $spotCheckStatus[] = FinAuthStatus::APPROVAL;
        }
        if ($val = array_get($filterData, 'spotCheckApproved')) {
            $spotCheckStatus[] = FinAuthStatus::APPROVED;
        }
        if ($val = array_get($filterData, 'spotCheckRejected')) {
            $spotCheckStatus[] = FinAuthStatus::REJECTED;
        }
        if (count($spotCheckStatus)) {
            array_push($orCodes, [
                'fin_auth_status',
                'fin_auth_status_id',
                'name',
                implode(',', $spotCheckStatus),
                "Spot Check Audit"
            ]);
        }

        return $invoiceQuery;
    }

    public function reAddContInvDatesQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $invoiceService = new \Tfcloud\Services\Contract\InvoiceService($this->permissionService);
        $invoiceQuery = $invoiceService->filterAll($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'invoiceNo')) {
            array_push($whereTexts, "Invoice No contains '{$val}'");
        }

        if ($val = array_get($filterData, 'invoiceDesc')) {
            array_push($whereTexts, "Description contains '{$val}'");
        }

        if ($val = Common::iset($filterData['invoiceType'])) {
            array_push($whereCodes, [
                'contract_invoice_type',
                'contract_invoice_type_id',
                'contract_invoice_type_name',
                $val, \Lang::get('text.contract_invoice_type')
            ]);
        }

        if ($val = array_get($filterData, 'certCode')) {
            array_push($whereTexts, "Certificate Code contains '{$val}'");
        }

        if ($val = array_get($filterData, 'owner')) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, \Lang::get('text.owner')]);
        }

        // Special location
        if ($val = array_get($filterData, 'site_id')) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);

            if ($val = array_get($filterData, 'building_id')) {
                array_push($whereCodes, ['building', 'building_id', 'building_code', $val, "Building Code"]);

                if ($val = array_get($filterData, 'room_id')) {
                    array_push($whereCodes, ['room', 'room_id', 'room_number', $val, "Room Number"]);
                }
            }
        }

        if ($val = Common::iset($filterData['supplier'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, \Lang::get('text.supplier')]);
        }

        if ($val = Common::iset($filterData['authoriser'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, \Lang::get('text.authoriser')]);
        }

        if ($val = Common::iset($filterData['account'])) {
            array_push($whereCodes, [
                'fin_account',
                'fin_account_id',
                'fin_account_code',
                $val, \Lang::get('text.account')
            ]);
        }

        if ($val = Common::iset($filterData['financialYear'])) {
            array_push($whereCodes, [
                'fin_year',
                'fin_year_id',
                'fin_year_code',
                $val, \Lang::get('text.financial_year')
            ]);
        }

        if ($val = array_get($filterData, 'netTotalFrom', 0)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.net_total_from'), $val));
        }

        if ($val = array_get($filterData, 'netTotalTo', 0)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.net_total_to'), $val));
        }

        if ($val = array_get($filterData, 'instructionCode')) {
            array_push($whereTexts, "Instruction Code contains '{$val}'");
        }

        if ($val = array_get($filterData, 'received_from', array_get($filterData, 'receivedFrom', null))) {
            array_push($whereTexts, \Form::fieldLang('received_date_from') . " $val");
        }

        if ($val = array_get($filterData, 'received_to', array_get($filterData, 'receivedTo', null))) {
            array_push($whereTexts, \Form::fieldLang('to') . " $val");
        }

        if ($val = array_get($filterData, 'tax_from', array_get($filterData, 'taxFrom', null))) {
            array_push($whereTexts, \Form::fieldLang('tax_date_from') . " $val");
        }

        if ($val = array_get($filterData, 'tax_to', array_get($filterData, 'taxTo', null))) {
            array_push($whereTexts, \Form::fieldLang('to') . " $val");
        }

        $statusType = [];
        if (Common::iset($filterData['draft'])) {
            $statusType[] = InvoiceStatusType::DRAFT;
        }
        if (Common::iset($filterData['registered'])) {
            $statusType[] = InvoiceStatusType::REGISTERED;
        }
        if (Common::iset($filterData['approved'])) {
            $statusType[] = InvoiceStatusType::APPROVED;
        }
        if (Common::iset($filterData['posted'])) {
            $statusType[] = InvoiceStatusType::POSTED;
        }
        if (Common::iset($filterData['rejected'])) {
            $statusType[] = InvoiceStatusType::REJECTED;
        }
        if (count($statusType)) {
            array_push($orCodes, [
                'invoice_status_type',
                'invoice_status_type_id',
                'invoice_status_type_code',
                implode(',', $statusType),
                "Status Type"
            ]);
        }

        // Status
        $statusId = array_get($filterData, 'status');
        if ($statusId) {
            $status = \Tfcloud\Models\ContractInvoiceStatus::find($statusId);
            $whereTexts[] = 'Status Code = ' . $status->contract_invoice_status_code;
        }

        $authorisations = [];
        if ($val = array_get($filterData, 'authOpen')) {
            $authorisations[] = FinAuthStatus::OPEN;
        }
        if ($val = array_get($filterData, 'authApproval')) {
            $authorisations[] = FinAuthStatus::APPROVAL;
        }
        if ($val = array_get($filterData, 'authApproved')) {
            $authorisations[] = FinAuthStatus::APPROVED;
        }
        if ($val = array_get($filterData, 'authRejected')) {
            $authorisations[] = FinAuthStatus::REJECTED;
        }
        if (count($authorisations)) {
            array_push($orCodes, [
                    'fin_auth_status',
                    'fin_auth_status_id',
                    'name',
                    implode(',', $authorisations),
                    \Lang::get('text.authorisation')
            ]);
        }

        if ($val = Common::iset($filterData['auditor'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Spot Check Auditor']);
        }

        if ($val = Common::iset($filterData['spotCheckRequired'])) {
            switch ($val) {
                case 'Y':
                    array_push($whereTexts, "Spot Check Required = 'Yes'");
                    break;
                case 'N':
                    array_push($whereTexts, "Spot Check Required = 'No'");
                    break;
                case 'all':
                    break;
            }
        }

        $spotCheckStatus = [];
        if ($val = array_get($filterData, 'spotCheckForApproval')) {
            $spotCheckStatus[] = FinAuthStatus::APPROVAL;
        }
        if ($val = array_get($filterData, 'spotCheckApproved')) {
            $spotCheckStatus[] = FinAuthStatus::APPROVED;
        }
        if ($val = array_get($filterData, 'spotCheckRejected')) {
            $spotCheckStatus[] = FinAuthStatus::REJECTED;
        }
        if (count($spotCheckStatus)) {
            array_push($orCodes, [
                'fin_auth_status',
                'fin_auth_status_id',
                'name',
                implode(',', $spotCheckStatus),
                "Spot Check Audit"
            ]);
        }

        return $invoiceQuery;
    }

    public function reAddInvContCliRedb01Query(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $filter = new InvoiceFilter($filterData);

        if (!is_null($filter->site_id)) {
            $query->where('site_id', '=', $filter->site_id);
        }

        if (!is_null($filter->account)) {
            $query->where('fin_account_id', '=', $filter->account);
        }

        if (!is_null($filter->auditDateFrom)) {
            $auditDateFrom = \DateTime::createFromFormat('d/m/Y', $filter->auditDateFrom);

            if ($auditDateFrom) {
                $query->where(
                    \DB::raw("DATE_FORMAT(first_changed_to_posted_date, '%Y-%m-%d')"),
                    '>=',
                    $auditDateFrom->format('Y-m-d')
                );
            }
        }

        if (!is_null($filter->auditDateTo)) {
            $auditDateTo = \DateTime::createFromFormat('d/m/Y', $filter->auditDateTo);

            if ($auditDateTo) {
                $query->where(
                    \DB::raw("DATE_FORMAT(first_changed_to_posted_date, '%Y-%m-%d')"),
                    '<=',
                    $auditDateTo->format('Y-m-d')
                );
            }
        }

        if ($val = array_get($filterData, 'site_id')) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);
        }

        if ($val = Common::iset($filterData['account'])) {
            array_push($whereCodes, [
                'fin_account',
                'fin_account_id',
                'fin_account_code',
                $val, \Lang::get('text.account')
            ]);
        }

        if ($val = array_get($filterData, 'audit_date_from', array_get($filterData, 'auditDateFrom', null))) {
            array_push($whereTexts, \Form::fieldLang('audit_date_from') . " $val");
        }

        if ($val = array_get($filterData, 'audit_date_to', array_get($filterData, 'auditDateTo', null))) {
            array_push($whereTexts, \Form::fieldLang('to') . " $val");
        }

        return $query;
    }
}
