<?php

/**
 * FAR32
 *
 * IMPORTANT - READ THIS!!!
 *
 * This report is a copy of FAR01 (FixedAssetsMovementsOnBalancesQueryService), but with the values broken down
 * by asset and land.  When making changes to this report, consideration should be given to whether FAR01 should
 * also be updated.
 *
 */

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\FixedAsset\CaSystemManager;
use Tfcloud\Models\CaIfrsCategory;
use Tfcloud\Models\CaTransactionType;
use Tfcloud\Models\Report;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\PermissionService;

class FixedAssetsMovementsOnBalancesAssetLandQueryService extends BaseService
{
    protected $permissionService;

    protected $siteGroupId;
    protected $bIncludeRRTrans = false;
    protected $rrTrans = 'N';
    protected $bSumNegative = false;

    // filters
    protected $commonGood = false;
    protected $investment = false;
    protected $deminimis = false;
    protected $entity = false;

    private $reportBoxFilterLoader = null;

    public function __construct(PermissionService $permissionService, $filterData, $repCode, Report $report = null)
    {
        $this->permissionService = $permissionService;
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;

        $this->setFilterProperties($filterData);
    }

    private function setFilterProperties($filterData)
    {
        $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($filterData);
        $commonGood = $reportFilterQuery->getFirstValueFilterField('common_good');
        $investment = $reportFilterQuery->getFirstValueFilterField('investment');
        $deminimis = $reportFilterQuery->getFirstValueFilterField('deminimis_asset');
        $entity = $reportFilterQuery->getFirstValueFilterField('ca_entity_id');

        // get filter values
        $this->commonGood = (!$commonGood) ? false : $commonGood;
        $this->investment = (!$investment) ? false : $investment;
        $this->deminimis = (!$deminimis) ? false : $deminimis;
        $this->entity = (!$entity) ? false : $entity;
    }


    public function movementsOnBalancesQuery()
    {
        // Get all IFRS categories the report should include.
        $arrIFRSCat = $this->getIFRSCategories();

        // Get IFRS categories code and description for use in report header.
        $arrIFRSCatDesc = $this->getIFRSCategories(true);

        $bYearEnd = CaSystemManager::isInYearEndMode();
        $yearStartDate = CaSystemManager::getYearStartDate(CommonConstant::DATE_FORMAT_MONTH);
        $yearEndDate = CaSystemManager::getYearEndDate(CommonConstant::DATE_FORMAT_MONTH);
        $this->siteGroupId = \Auth::user()->site_group_id;

        // *****************************************************************
        // REPORT HEADER
        // *****************************************************************
        // Build report header
        $queryRpt = $this->reportHeader($arrIFRSCatDesc);

        // *****************************************************************
        // PART 1 - COST OR VALUATION SECTION
        // *****************************************************************

        // Add Cost or Valuation blank format row
        $rowDesc = "Cost or Valuation";
        $queryRpt->unionAll($this->buildLabelRow($rowDesc, $arrIFRSCat));

        // Add First detail row (values at start of year)
        $arrAssetColumn = array("ca_balance.bfwd_gbv_asset");
        $arrLandColumn = array("ca_balance.bfwd_gbv_land");
        $rowDesc = "At " . $yearStartDate;
        $queryRpt->unionAll($this->buildBalanceQuery($rowDesc, $arrIFRSCat, $arrAssetColumn, $arrLandColumn, true));

        // Add row - Acquisitions
        $arrAssetColumn = array("ca_transaction.gbv_change_asset");
        $arrLandColumn = array("ca_transaction.gbv_change_land");
        $arrTranType = array(CaTransactionType::ACQUISITION);
        $this->setSumNegative(false);
        $this->setIncludeRRTrans(false);
        $queryRpt->unionAll(
            $this->buildTranQuery("Acquisitions", $arrIFRSCat, $arrAssetColumn, $arrLandColumn, $arrTranType)
        );

        // Add row - Additions.
        $arrAssetColumn = array("ca_transaction.gbv_change_asset");
        $arrLandColumn = array("ca_transaction.gbv_change_land");
        $arrTranType = array(CaTransactionType::ADDITION);
        $this->setSumNegative(false);
        $this->setIncludeRRTrans(false);
        $queryRpt->unionAll(
            $this->buildTranQuery("Additions", $arrIFRSCat, $arrAssetColumn, $arrLandColumn, $arrTranType)
        );

        // Add row - Revaluation Increases/Decreases to RR
        $queryRpt->unionAll($this->buildRevaluationToRR(
            'Revaluation Increases/Decreases to RR',
            $arrIFRSCat
        ));

        // Add row - Revaluation Increases/Decreases to Surplus/Deficit
        $queryRpt->unionAll($this->buildRevaluationAndReversalOfLossToSurplusDeficit(
            'Revaluation Increases/Decreases to Surplus/Deficit',
            $arrIFRSCat
        ));

        // Add row - Derecognition - Disposals
        $arrAssetColumn = array("ca_transaction.gbv_change_asset");
        $arrLandColumn = array("ca_transaction.gbv_change_land");
        $arrTranType = array(CaTransactionType::DISPOSAL);
        $this->setSumNegative(false);
        $this->setIncludeRRTrans(false);
        $queryRpt->unionAll(
            $this->buildTranQuery(
                "Derecognition - Disposals",
                $arrIFRSCat,
                $arrAssetColumn,
                $arrLandColumn,
                $arrTranType
            )
        );

        // Add row - Derecognition - Other
        $arrAssetColumn = array("ca_transaction.gbv_change_asset");
        $arrLandColumn = array("ca_transaction.gbv_change_land");
        $arrTranType = array(CaTransactionType::DERECOGNITION);
        $this->setSumNegative(false);
        $this->setIncludeRRTrans(false);
        $queryRpt->unionAll(
            $this->buildTranQuery("Derecognition - Other", $arrIFRSCat, $arrAssetColumn, $arrLandColumn, $arrTranType)
        );

        // Add row - Reclassification and Transfers
        $arrAssetColumn = array("ca_transaction.gbv_change_asset");
        $arrLandColumn = array("ca_transaction.gbv_change_land");
        $arrTranType = array(CaTransactionType::CATEGORY_TRANSFER);
        $this->setSumNegative(false);
        $this->setIncludeRRTrans(false);
        $queryRpt->unionAll(
            $this->buildTranQuery(
                "Reclassification and Transfers",
                $arrIFRSCat,
                $arrAssetColumn,
                $arrLandColumn,
                $arrTranType
            )
        );

        // Add row - Balance as at 31 March XXXX (Column totals)
        $arrAssetColumn = array("ca_balance.cfwd_gbv_asset");
        $arrLandColumn = array("ca_balance.cfwd_gbv_land");
        $rowDesc = "Balance as at " . $yearEndDate;
        $queryRpt->unionAll($this->buildBalanceQuery($rowDesc, $arrIFRSCat, $arrAssetColumn, $arrLandColumn));


        // *****************************************************************
        // PART 2 - DEPRECIATION AND IMPAIRMENT SECTION
        // *****************************************************************

        // Add Depreciation and Impairment blank format row.
        $queryRpt->unionAll($this->addBlankRow($arrIFRSCat));

        // Add section heading row.
        $rowDesc = "Depreciation and Impairment";
        $queryRpt->unionAll($this->buildLabelRow($rowDesc, $arrIFRSCat));

        // Add detail line (bfwd_accum_imp at start of year).
        $arrAssetColumn = array("ca_balance.bfwd_accum_imp_asset");
        $arrLandColumn = array("ca_balance.bfwd_accum_imp_land");
        $rowDesc = "Impairment at " . $yearStartDate;
        $queryRpt->unionAll($this->buildBalanceQuery($rowDesc, $arrIFRSCat, $arrAssetColumn, $arrLandColumn, true));

        // Add detail line (bfwd_accum_dep at start of year).
        $arrAssetColumn = array("ca_balance.bfwd_accum_depn");
        $arrLandColumn = array();
        $rowDesc = "Depreciation at " . $yearStartDate;
        $queryRpt->unionAll($this->buildBalanceQuery($rowDesc, $arrIFRSCat, $arrAssetColumn, $arrLandColumn, true));

        // Add detail line (cy_depreciation).
        $arrAssetColumn = array("ca_balance.cy_depreciation");
        $arrLandColumn = array();
        $rowDesc = "Depreciation Charge";
        $queryRpt->unionAll(
            $this->buildBalanceQuery($rowDesc, $arrIFRSCat, $arrAssetColumn, $arrLandColumn, $bYearEnd)
        );

        // Add line. Depreciation written out to the RR
        $arrAssetColumn = array("ca_transaction.depn_wo");
        $arrLandColumn = array();
        $arrTranType = array(CaTransactionType::REVALUATION);
        $this->setSumNegative(true);
        $this->setIncludeRRTrans(true);
        $this->setRRTrans("Y");
        $queryRpt->unionAll(
            $this->buildTranQuery(
                "Depreciation written out to the RR",
                $arrIFRSCat,
                $arrAssetColumn,
                $arrLandColumn,
                $arrTranType
            )
        );

        // Add line. Depreciation written out to Surplus/Deficit on the Provision of Services
        $arrAssetColumn = array("ca_transaction.depn_wo");
        $arrLandColumn = array();
        $arrTranType = array(CaTransactionType::REVALUATION);
        $this->setSumNegative(true);
        $this->setIncludeRRTrans(true);
        $this->setRRTrans("N");
        $queryRpt->unionAll(
            $this->buildTranQuery(
                "Depreciation written out to Surplus/Deficit",
                $arrIFRSCat,
                $arrAssetColumn,
                $arrLandColumn,
                $arrTranType
            )
        );

        // Add line. Depreciation adjustment from reversal
        $arrAssetColumn = array("ca_transaction.cv_depn_adjustment");
        $arrLandColumn = array();
        $arrTranType = array(CaTransactionType::REVERSAL_OF_IMPAIRMENT, CaTransactionType::REVERSAL_OF_LOSS);
        $this->setSumNegative(false);
        $this->setIncludeRRTrans(false);
        $queryRpt->unionAll(
            $this->buildTranQuery(
                "Depreciation adjustment from reversal",
                $arrIFRSCat,
                $arrAssetColumn,
                $arrLandColumn,
                $arrTranType
            )
        );

        // Add line. Impairment losses recognised in the RR
        $arrAssetColumn = array("(ca_transaction.cv_asset - ca_transaction.hc_asset)");
        $arrLandColumn = array("(ca_transaction.cv_land - ca_transaction.hc_land)");
        $arrTranType = array(CaTransactionType::IMPAIRMENT);
        $this->setSumNegative(false);
        $this->setIncludeRRTrans(true);
        $this->setRRTrans("Y");
        $queryRpt->unionAll(
            $this->buildTranQuery(
                "Impairment losses recognised in the RR",
                $arrIFRSCat,
                $arrAssetColumn,
                $arrLandColumn,
                $arrTranType
            )
        );

        // Add line. Impairments written out to the RR
        $arrAssetColumn = array("ca_transaction.imp_wo_asset");
        $arrLandColumn = array("ca_transaction.imp_wo_land");
        $arrTranType = array(CaTransactionType::REVALUATION);
        $this->setSumNegative(true);
        $this->setIncludeRRTrans(true);
        $this->setRRTrans("Y");
        $queryRpt->unionAll(
            $this->buildTranQuery(
                "Impairments written out to the RR",
                $arrIFRSCat,
                $arrAssetColumn,
                $arrLandColumn,
                $arrTranType
            )
        );

        // Add line. Impairments recognised in the Surplus/Deficit
        $arrAssetColumn = array("ca_transaction.hc_asset");
        $arrLandColumn = array("ca_transaction.hc_land");
        $arrTranType = array(CaTransactionType::IMPAIRMENT);
        $this->setSumNegative(false);
        $this->setIncludeRRTrans(false);
        //$this->setRRTrans("N");
        $queryRpt->unionAll(
            $this->buildTranQuery(
                "Impairments recognised in Surplus/Deficit",
                $arrIFRSCat,
                $arrAssetColumn,
                $arrLandColumn,
                $arrTranType
            )
        );

        // Add line. Impairments written out to Surplus/Deficit
        $arrAssetColumn = array("ca_transaction.imp_wo_asset");
        $arrLandColumn = array("ca_transaction.imp_wo_land");
        $arrTranType = array(CaTransactionType::REVALUATION);
        $this->setSumNegative(true);
        $this->setIncludeRRTrans(true);
        $this->setRRTrans("N");
        $queryRpt->unionAll(
            $this->buildTranQuery(
                "Impairments written out to Surplus/Deficit",
                $arrIFRSCat,
                $arrAssetColumn,
                $arrLandColumn,
                $arrTranType
            )
        );

        $arrAssetColumn = array("(ca_transaction.cv_asset - ca_transaction.gbv_change_asset)");
        $arrLandColumn = array("(ca_transaction.cv_land - ca_transaction.gbv_change_land)");
        $arrTranType = array(CaTransactionType::REVERSAL_OF_IMPAIRMENT);
        $this->setSumNegative(true);
        $this->setIncludeRRTrans(false);
        $queryRpt->unionAll(
            $this->buildTranQuery(
                "Reversals of Impairment to Surplus/Deficit",
                $arrIFRSCat,
                $arrAssetColumn,
                $arrLandColumn,
                $arrTranType
            )
        );

        // Add line. Derecognition - Disposal
        $arrAssetColumn = array("ca_transaction.depn_wo", "ca_transaction.imp_wo_asset");
        $arrLandColumn = array("ca_transaction.imp_wo_land");
        $arrTranType = array(CaTransactionType::DISPOSAL);
        $this->setSumNegative(true);
        $this->setIncludeRRTrans(false);
        $queryRpt->unionAll(
            $this->buildTranQuery(
                "Derecognition - Disposals",
                $arrIFRSCat,
                $arrAssetColumn,
                $arrLandColumn,
                $arrTranType
            )
        );

        // Add line. Derecognition - Other
        $arrAssetColumn = array("ca_transaction.depn_wo", "ca_transaction.imp_wo_asset");
        $arrLandColumn = array("ca_transaction.imp_wo_land");
        $arrTranType = array(CaTransactionType::DERECOGNITION);
        $this->setSumNegative(true);
        $this->setIncludeRRTrans(false);
        $queryRpt->unionAll(
            $this->buildTranQuery("Derecognition - other", $arrIFRSCat, $arrAssetColumn, $arrLandColumn, $arrTranType)
        );

        // Add line. Reclassification and Transfers
        $arrAssetColumn = array("ca_transaction.depn_wo", "ca_transaction.imp_wo_asset");
        $arrLandColumn = array("ca_transaction.imp_wo_land");
        $arrTranType = array(CaTransactionType::CATEGORY_TRANSFER);
        $this->setSumNegative(false);
        $this->setIncludeRRTrans(false);
        $queryRpt->unionAll(
            $this->buildTranQuery(
                "Reclassification and Transfers",
                $arrIFRSCat,
                $arrAssetColumn,
                $arrLandColumn,
                $arrTranType
            )
        );

        // Add section 2 total line (build column totals ).
        $arrAssetColumn = array("ca_balance.cfwd_accum_depn", "ca_balance.cfwd_accum_imp_asset");
        $arrLandColumn = array("ca_balance.cfwd_accum_imp_land");
        $rowDesc = "Balance as at " . $yearEndDate;
        $queryRpt->unionAll($this->buildBalanceQuery($rowDesc, $arrIFRSCat, $arrAssetColumn, $arrLandColumn));

        // *******************************************
        // SECTION 3 - NET BOOK VALUE
        // *******************************************

        // Add Net Book Value blank format row.
        $queryRpt->unionAll($this->addBlankRow($arrIFRSCat));

        // Add Net Book Value title row.
        $rowDesc = "Net Book Value";
        $queryRpt->unionAll($this->buildLabelRow($rowDesc, $arrIFRSCat));

        // Line - 'Balance as at 31 March 2011.
        $arrAssetColumn = array("ca_balance.bfwd_nbv_asset");
        $arrLandColumn = array("ca_balance.bfwd_nbv_land");
        $rowDesc = "Balance as at " . $yearStartDate;
        $queryRpt->unionAll($this->buildBalanceQuery($rowDesc, $arrIFRSCat, $arrAssetColumn, $arrLandColumn, true));

        // Line - 'Balance as at 31 March 2012.
        $arrAssetColumn = array("ca_balance.cfwd_nbv_asset");
        $arrLandColumn = array("ca_balance.cfwd_nbv_land");
        $rowDesc = "Balance as at " . $yearEndDate;
        $queryRpt->unionAll($this->buildBalanceQuery($rowDesc, $arrIFRSCat, $arrAssetColumn, $arrLandColumn));

        return \DB::table(null)->select('*')->from(\DB::raw("(" . $queryRpt->toSql() . ') AS report'));
    }

    private function setIncludeRRTrans($bValue)
    {
        $this->bIncludeRRTrans = $bValue;
    }

    private function setRRTrans($value)
    {
        $this->rrTrans = $value;
    }

    private function setSumNegative($bValue)
    {
        $this->bSumNegative = $bValue;
    }

    /**
     * Returns the sql to add a blank row into a csv report.
     * @param array $arrIFRSCat
     * @return string
     */
    protected function addBlankRow(array $arrIFRSCat)
    {
        // First column is the row description label.
        $query =  "' ' `Element`";

        // Add columns for each IFRS category
        foreach ($arrIFRSCat as $key => $value) {
            $query .= " , ' ', ' ' ";
        }

        // Calculate row total.
        $query .= " , '' `total` ";

        return \DB::table(null)->select(\DB::raw($query));
    }

    private function getIFRSCategories($bIncludeDesc = false)
    {
        $arrIFRSCat = array();   // Holds all IFRS categories the report should include.

        // Get available ifrs categories to allow the report query to be built dynamically
        $query = CaIfrsCategory::userSiteGroup()
        ->select(
            [
                'ca_ifrs_category_code',
                'ca_ifrs_category_desc',
                'ca_ifrs_category_id'
            ]
        )
        ->where(
            'ca_ifrs_category.active',
            '=',
            CommonConstant::DATABASE_VALUE_YES
        );

        if ($this->commonGood) {
            $query->where('common_good', '=', $this->commonGood);
        }

        if ($this->investment) {
            $query->where('investment', '=', $this->investment);
        }

        $query->orderBy('investment');
        $query->orderBy('depreciable', 'desc');

        // RB 21/11/2022 This can cause the headings to be out of sync with te data Ordering should be the same.
//        if ($bIncludeDesc) {
//            $query->orderBy(\DB::raw('CONCAT(ca_ifrs_category_code, ca_ifrs_category_desc)'));
//        } else {
            $query->orderBy('ca_ifrs_category_code');
//        }

        $queryIFRS = $query->get();

        // Loop through all ifrs categories and store in array.
        foreach ($queryIFRS as $row) {
            $bDesc = '';
            if ($bIncludeDesc) {
                $bDesc = Common::concatFields([$row['ca_ifrs_category_code'], $row['ca_ifrs_category_desc']]);
            } else {
                $bDesc = $row['ca_ifrs_category_code'];
            }
            $arrIFRSCat[$row['ca_ifrs_category_id']] = $bDesc;
        }

        return $arrIFRSCat;
    }

    private function reportHeader($arrIFRSCatDesc)
    {
        $query = " '' `Element`";
        foreach ($arrIFRSCatDesc as $key => $codeDesc) {
            $query .= " , '' `$codeDesc (Asset)` , '' `$codeDesc (Land)`";
        }
        $query .= ", '' Totals ";

        return \DB::table(null)->select(\DB::raw($query));
    }

    private function buildLabelRow($rowDesc, array $arrIFRSCat)
    {
        // First column is the row description label.
        $query =  "'$rowDesc' `Element`";

        foreach ($arrIFRSCat as $key => $code) {
            $query .= " , '' `$code (Asset)`, '' `$code (Land)`";
        }
        $query .= ", '' Totals ";

        return \DB::table(null)->select(\DB::raw($query));
    }

    private function buildBalanceQuery(
        $rowDesc,
        array $arrIFRSCat,
        array $arrAssetColumn,
        array $arrLandColumn,
        $bUseBfwdIFRS = false
    ) {
        // Get columns as a  string.
        $assetColumns = implode(" + ", $arrAssetColumn);
        $landColumns = implode(" + ", $arrLandColumn);

        $allColumns = $assetColumns;
        if ($landColumns) {
            if ($allColumns) {
                $allColumns .= ' + ';
            }
            $allColumns .= $landColumns;
        }

        // First column is the row description label.
        $query =  "'$rowDesc' `Element`";

        // Add columns for each IFRS category
        foreach ($arrIFRSCat as $key => $value) {
            // Asset
            $query .= " , ";
            $query .= " (SELECT IFNULL(";
            $query .= " (SELECT ROUND(IFNULL(SUM($assetColumns), 0), 0) ";
            $query .= "  FROM ca_ifrs_category ";
            if ($bUseBfwdIFRS) {
                $query .= " INNER JOIN ca_fixed_asset ON
                    ca_fixed_asset.bfwd_ifrs_cat_id = ca_ifrs_category.ca_ifrs_category_id ";
            } else {
                $query .= " INNER JOIN ca_fixed_asset ON
                    ca_fixed_asset.ca_ifrs_cat_id = ca_ifrs_category.ca_ifrs_category_id ";
            }
            $query .= " INNER JOIN ca_balance ON ca_balance.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
            $query .= " LEFT JOIN vw_far_disposed ON
                   vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
            $query .= " INNER JOIN vw_ca_current_fin_year ON
                vw_ca_current_fin_year.ca_fin_year_id = ca_balance.ca_fin_year_id ";
            $query .= "  WHERE ca_ifrs_category.site_group_id = '{$this->siteGroupId}' ";
            $query .= " AND ca_ifrs_category.active = 'Y' ";
            $query .= " AND ca_balance.historic = 'N' ";
            //$query .= " AND ca_fixed_asset.deminimis_asset = 'N'";
            if ($this->deminimis) {
                $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
            }
            if ($this->entity) {
                $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
            }
            $query .= " AND ca_fixed_asset.active = 'Y'";
            $query .= " AND ca_ifrs_category.ca_ifrs_category_id = " . $key;
            $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
            $query .= ") , 0)) ";

            // Land
            if ($landColumns) {
                $query .= " , ";
                $query .= " (SELECT IFNULL(";
                $query .= " (SELECT ROUND(IFNULL(SUM($landColumns), 0), 0) ";
                $query .= "  FROM ca_ifrs_category ";
                if ($bUseBfwdIFRS) {
                    $query .= " INNER JOIN ca_fixed_asset ON
                        ca_fixed_asset.bfwd_ifrs_cat_id = ca_ifrs_category.ca_ifrs_category_id ";
                } else {
                    $query .= " INNER JOIN ca_fixed_asset ON
                        ca_fixed_asset.ca_ifrs_cat_id = ca_ifrs_category.ca_ifrs_category_id ";
                }
                $query .= " INNER JOIN ca_balance ON ca_balance.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
                $query .= " LEFT JOIN vw_far_disposed ON
                       vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
                $query .= " INNER JOIN vw_ca_current_fin_year ON
                    vw_ca_current_fin_year.ca_fin_year_id = ca_balance.ca_fin_year_id ";
                $query .= "  WHERE ca_ifrs_category.site_group_id = '{$this->siteGroupId}' ";
                $query .= " AND ca_ifrs_category.active = 'Y' ";
                $query .= " AND ca_balance.historic = 'N' ";
                //$query .= " AND ca_fixed_asset.deminimis_asset = 'N'";
                if ($this->deminimis) {
                    $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
                }
                if ($this->entity) {
                    $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
                }
                $query .= " AND ca_fixed_asset.active = 'Y'";
                $query .= " AND ca_ifrs_category.ca_ifrs_category_id = " . $key;
                $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
                $query .= ") , 0)) ";
            } else {
                $query .= ", 0";
            }
        }

        // Calculate row total.
        $query .= " , ";
        $query .= " (SELECT IFNULL(";
        $query .= " (SELECT ROUND(IFNULL(SUM($allColumns), 0), 0) ";
        $query .= "  FROM ca_ifrs_category ";
        if ($bUseBfwdIFRS) {
            $query .= " INNER JOIN ca_fixed_asset ON
                ca_fixed_asset.bfwd_ifrs_cat_id = ca_ifrs_category.ca_ifrs_category_id ";
        } else {
            $query .= " INNER JOIN ca_fixed_asset ON
                ca_fixed_asset.ca_ifrs_cat_id = ca_ifrs_category.ca_ifrs_category_id ";
        }
        $query .= " INNER JOIN ca_balance ON ca_balance.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
        $query .= " INNER JOIN vw_ca_current_fin_year ON
            vw_ca_current_fin_year.ca_fin_year_id = ca_balance.ca_fin_year_id ";
        $query .= " LEFT JOIN vw_far_disposed ON
               vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
        $query .= " WHERE ca_ifrs_category.site_group_id = '{$this->siteGroupId}' ";
        $query .= "     AND ca_ifrs_category.active = 'Y' ";
        //$query .= "     AND ca_fixed_asset.deminimis_asset = 'N'";
        if ($this->deminimis) {
            $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
        }
        if ($this->entity) {
            $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
        }
        $query .= "     AND ca_fixed_asset.active = 'Y'";
        if ($this->commonGood) {
            $query .= " AND ca_ifrs_category.common_good = '{$this->commonGood}'";
        }
        if ($this->investment) {
            $query .= " AND ca_ifrs_category.investment = '{$this->investment}'";
        }
        $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
        $query .= "     AND ca_balance.historic = 'N' ),0)) `total` ";

        return \DB::table(null)->select(\DB::raw($query));
    }

    private function buildTranQuery(
        $rowDesc,
        array $arrIFRSCat,
        array $arrAssetColumn,
        array $arrLandColumn,
        array $arrTranTypes
    ) {
        // Get columns and transactions types as a comma separated string.
        $assetColumn = implode(" + ", $arrAssetColumn);
        $landColumn = implode(" + ", $arrLandColumn);
        $tranTypes = implode(",", $arrTranTypes);

        $allColumns = $assetColumn;
        if ($landColumn) {
            if ($allColumns) {
                $allColumns .= ' + ';
            }
            $allColumns .= $landColumn;
        }

        // First column is the row description label.
        $query =  "'$rowDesc' `Element`";

        // Add columns for each IFRS category
        foreach ($arrIFRSCat as $key => $value) {
            // Asset
            $query .= ", ";
            $query .= " ROUND(IFNULL((SELECT SUM($assetColumn) " . ($this->bSumNegative ? " * -1 " : "");
            $query .= " FROM ca_transaction ";
            $query .= " INNER JOIN vw_ca_current_fin_year ON
                vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
            $query .= " INNER JOIN ca_fixed_asset ON
                ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
            $query .= " LEFT JOIN vw_far_disposed ON
                   vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
            $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId'";
            $query .= " AND ca_transaction_type_id IN ($tranTypes)";
            $query .= " AND ca_transaction.ca_ifrs_cat_id = $key";
            //$query .= " AND ca_fixed_asset.deminimis_asset = 'N'";
            if ($this->deminimis) {
                $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
            }
            if ($this->entity) {
                $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
            }
            $query .= " AND ca_fixed_asset.active = 'Y'";
            if ($this->bIncludeRRTrans) {
                $query .= " AND ca_transaction.rr_trans = '{$this->rrTrans}'";
            }
            $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
            $query .= "),0),0)";
            $query .= " `$value asset`";

            // Land
            if ($landColumn) {
                $query .= ", ";
                $query .= " ROUND(IFNULL((SELECT SUM($landColumn) " . ($this->bSumNegative ? " * -1 " : "");
                $query .= " FROM ca_transaction ";
                $query .= " INNER JOIN vw_ca_current_fin_year ON
                    vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
                $query .= " INNER JOIN ca_fixed_asset ON
                    ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
                $query .= " LEFT JOIN vw_far_disposed ON
                       vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
                $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId'";
                $query .= " AND ca_transaction_type_id IN ($tranTypes)";
                $query .= " AND ca_transaction.ca_ifrs_cat_id = $key";
                //$query .= " AND ca_fixed_asset.deminimis_asset = 'N'";
                if ($this->deminimis) {
                    $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
                }
                if ($this->entity) {
                    $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
                }
                $query .= " AND ca_fixed_asset.active = 'Y'";
                if ($this->bIncludeRRTrans) {
                    $query .= " AND ca_transaction.rr_trans = '{$this->rrTrans}'";
                }
                $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
                $query .= "),0),0)";
                $query .= " `$value land`";
            } else {
                $query .= ", 0 `$value land`";
            }
        }

        // Add row total column
        $query .= " , ";
        $query .= " ROUND(IFNULL((SELECT SUM($allColumns) " . ($this->bSumNegative ? " * -1 " : "");
        $query .= " FROM ca_transaction ";
        $query .= " INNER JOIN ca_ifrs_category ON
            ca_ifrs_category.ca_ifrs_category_id = ca_transaction.ca_ifrs_cat_id ";
        $query .= " INNER JOIN vw_ca_current_fin_year ON
            vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
        $query .= " INNER JOIN ca_fixed_asset ON ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
        $query .= " LEFT JOIN vw_far_disposed ON
               vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
        $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
        $query .= " AND ca_ifrs_category.active = 'Y'";
        $query .= " AND ca_transaction_type_id IN ($tranTypes)";
        //$query .= " AND ca_fixed_asset.deminimis_asset = 'N'";
        if ($this->deminimis) {
            $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
        }
        if ($this->entity) {
            $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
        }
        $query .= " AND ca_fixed_asset.active = 'Y'";
        if ($this->bIncludeRRTrans) {
            $query .= " AND ca_transaction.rr_trans = '{$this->rrTrans}'";
        }
        if ($this->commonGood) {
            $query .= " AND ca_ifrs_category.common_good = '{$this->commonGood}'";
        }
        if ($this->investment) {
            $query .= " AND ca_ifrs_category.investment = '{$this->investment}'";
        }
        $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
        $query .= "),0),0)";
        $query .= " `Total`";

        return \DB::table(null)->select(\DB::raw($query));
    }

    private function buildRevaluationToRR($rowDesc, array $arrIFRSCat)
    {
        $query =  "'$rowDesc' `Element`";

        foreach ($arrIFRSCat as $id => $code) {
            // Asset
            $query .= ", ";
            $query .= " ROUND(";
            $query .= " (SELECT IFNULL(SUM( ";

            $query .= " if(`ca_transaction`.`rr_trans` = 'Y',";
            $query .= "    if (ca_transaction.hc_asset = 0 && ca_transaction.hc_land = 0";
            $query .= "       , ca_transaction.gbv_change_asset ";
            $query .= "       ,(`ca_transaction`.`cv_asset` - `ca_transaction`.`hc_asset`) -
                `ca_transaction`.`depn_wo` - `ca_transaction`.`imp_wo_asset`)";
            $query .= "  ,0)";
            $query .= " ), 0)";

            $query .= " FROM ca_transaction ";
            $query .= " INNER JOIN vw_ca_current_fin_year ON
                vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
            $query .= " INNER JOIN ca_fixed_asset ON
                ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
            $query .= " LEFT JOIN vw_far_disposed ON
                   vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
            $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
            $query .= " AND ca_transaction_type_id = " . CaTransactionType::REVALUATION;
            $query .= " AND ca_transaction.ca_ifrs_cat_id = " . $id;
            //$query .= " AND ca_fixed_asset.deminimis_asset = 'N'";
            if ($this->deminimis) {
                $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
            }
            if ($this->entity) {
                $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
            }
            $query .= " AND ca_fixed_asset.active = 'Y'";
            $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
            $query .= " )";

            $query .= ", 0)";
            $query .= " `" . $code . " asset`";

            // Land
            $query .= ", ";
            $query .= " ROUND(";
            $query .= " (SELECT IFNULL(SUM( ";

            $query .= " if(`ca_transaction`.`rr_trans` = 'Y',";
            $query .= "    if (ca_transaction.hc_asset = 0 && ca_transaction.hc_land = 0";
            $query .= "       , ca_transaction.gbv_change_land";
            $query .= "       ,(`ca_transaction`.`cv_land` - `ca_transaction`.`hc_land`) ";
            $query .= "         - `ca_transaction`.`imp_wo_land`)";
            $query .= "  ,0)";
            $query .= " ), 0)";

            $query .= " FROM ca_transaction ";
            $query .= " INNER JOIN vw_ca_current_fin_year ON
                vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
            $query .= " INNER JOIN ca_fixed_asset ON
                ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
            $query .= " LEFT JOIN vw_far_disposed ON
                   vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
            $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
            $query .= " AND ca_transaction_type_id = " . CaTransactionType::REVALUATION;
            $query .= " AND ca_transaction.ca_ifrs_cat_id = " . $id;
            //$query .= " AND ca_fixed_asset.deminimis_asset = 'N'";
            if ($this->deminimis) {
                $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
            }
            if ($this->entity) {
                $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
            }
            $query .= " AND ca_fixed_asset.active = 'Y'";
            $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
            $query .= " )";

            $query .= ", 0)";
            $query .= " `" . $code . " land`";
        }

        // Add row total column
        $query .= " , ";
        $query .= " ROUND(";
        $query .= " (SELECT IFNULL(SUM( ";

        $query .= " if(`ca_transaction`.`rr_trans` = 'Y',";
        $query .= "    if (ca_transaction.hc_asset = 0 && ca_transaction.hc_land = 0";
        $query .= "       , ca_transaction.gbv_change_asset + ca_transaction.gbv_change_land";
        $query .= "       ,((`ca_transaction`.`cv_asset` + `ca_transaction`.`cv_land`) -
            (`ca_transaction`.`hc_asset` + `ca_transaction`.`hc_land`)) -
            `ca_transaction`.`depn_wo` - `ca_transaction`.`imp_wo_asset` - `ca_transaction`.`imp_wo_land`)";
        $query .= "  ,0)";

        $query .= " ), 0)";
        $query .= " FROM ca_transaction ";
        $query .= " INNER JOIN vw_ca_current_fin_year ON
            vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
        $query .= " INNER JOIN ca_fixed_asset ON
            ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
        $query .= " LEFT JOIN vw_far_disposed ON
               vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
        $query .= " INNER JOIN ca_ifrs_category ON
            ca_ifrs_category.ca_ifrs_category_id = ca_transaction.ca_ifrs_cat_id ";
        $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
        $query .= "    AND ca_transaction_type_id = " . CaTransactionType::REVALUATION;
        //$query .= "    AND ca_fixed_asset.deminimis_asset = 'N'";
        if ($this->deminimis) {
            $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
        }
        if ($this->entity) {
            $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
        }
        $query .= "    AND ca_fixed_asset.active = 'Y'";
        if ($this->commonGood) {
            $query .= " AND ca_ifrs_category.common_good = '{$this->commonGood}'";
        }
        if ($this->investment) {
            $query .= " AND ca_ifrs_category.investment = '{$this->investment}'";
        }
        $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
        $query .= ")";
        $query .= ", 0)";
        $query .= " `Total`";

        return \DB::table(null)->select(\DB::raw($query));
    }

    private function buildRevaluationAndReversalOfLossToSurplusDeficit($rowDesc, array $arrIFRSCat)
    {
        $query =  "'$rowDesc' `Element`";

        foreach ($arrIFRSCat as $id => $code) {
            // Asset
            $query .= ", ";
            $query .= " ROUND(";
            $query .= " (SELECT IFNULL(SUM( ";

            $query .= "  if(`ca_transaction`.`rr_trans` = 'Y', ";
            $query .= "     if(ca_transaction.hc_asset = 0 && ca_transaction.hc_land = 0 ";
            $query .= "        , 0 ";
            $query .= "        , ca_transaction.hc_asset)";
            $query .= "     ,`ca_transaction`.`gbv_change_asset`)";

            $query .= " ), 0)";

            $query .= " FROM ca_transaction ";
            $query .= "    INNER JOIN vw_ca_current_fin_year ON
                vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
            $query .= "    INNER JOIN ca_fixed_asset ON
                ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
            $query .= " LEFT JOIN vw_far_disposed ON
                   vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
            $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
            $query .= "    AND ca_transaction_type_id = " . CaTransactionType::REVALUATION;
            $query .= "    AND ca_transaction.ca_ifrs_cat_id = " . $id;
            //$query .= "    AND ca_fixed_asset.deminimis_asset = 'N'";
            if ($this->deminimis) {
                $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
            }
            if ($this->entity) {
                $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
            }
            $query .= "    AND ca_fixed_asset.active = 'Y'";
            $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
            $query .= " )";
            $query .= " + ";
            $query .= " (SELECT IFNULL(SUM(ca_transaction.gbv_change_asset), 0) ";
            $query .= " FROM ca_transaction ";
            $query .= "    INNER JOIN vw_ca_current_fin_year ON
                vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
            $query .= "    INNER JOIN ca_fixed_asset ON
                ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
            $query .= " LEFT JOIN vw_far_disposed ON
                   vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
            $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
            $query .= "    AND ca_transaction_type_id IN
                (" . CaTransactionType::REVERSAL_OF_IMPAIRMENT . "," . CaTransactionType::REVERSAL_OF_LOSS . ")";
            $query .= "    AND ca_transaction.ca_ifrs_cat_id = " . $id;
            //$query .= "    AND ca_fixed_asset.deminimis_asset = 'N'";
            if ($this->deminimis) {
                $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
            }
            if ($this->entity) {
                $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
            }
            $query .= "    AND ca_fixed_asset.active = 'Y'";
            $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
            $query .= ")";

            $query .= ", 0)";
            $query .= " `" . $code . " asset`";

            // Land
            $query .= ", ";
            $query .= " ROUND(";
            $query .= " (SELECT IFNULL(SUM( ";

            $query .= "  if(`ca_transaction`.`rr_trans` = 'Y', ";
            $query .= "     if(ca_transaction.hc_asset = 0 && ca_transaction.hc_land = 0 ";
            $query .= "        , 0 ";
            $query .= "        , ca_transaction.hc_land)";
            $query .= "     ,`ca_transaction`.`gbv_change_land`)";
            $query .= " ), 0)";

            $query .= " FROM ca_transaction ";
            $query .= "    INNER JOIN vw_ca_current_fin_year ON
                vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
            $query .= "    INNER JOIN ca_fixed_asset ON
                ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
            $query .= " LEFT JOIN vw_far_disposed ON
                   vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
            $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
            $query .= "    AND ca_transaction_type_id = " . CaTransactionType::REVALUATION;
            $query .= "    AND ca_transaction.ca_ifrs_cat_id = " . $id;
            //$query .= "    AND ca_fixed_asset.deminimis_asset = 'N'";
            if ($this->deminimis) {
                $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
            }
            if ($this->entity) {
                $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
            }
            $query .= "    AND ca_fixed_asset.active = 'Y'";
            $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
            $query .= " )";
            $query .= " + ";
            $query .= " (SELECT IFNULL(SUM(ca_transaction.gbv_change_land), 0) ";
            $query .= " FROM ca_transaction ";
            $query .= "    INNER JOIN vw_ca_current_fin_year ON
                vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
            $query .= "    INNER JOIN ca_fixed_asset ON
                ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
            $query .= " LEFT JOIN vw_far_disposed ON
                   vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
            $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
            $query .= "    AND ca_transaction_type_id IN
                (" . CaTransactionType::REVERSAL_OF_IMPAIRMENT . "," . CaTransactionType::REVERSAL_OF_LOSS . ")";
            $query .= "    AND ca_transaction.ca_ifrs_cat_id = " . $id;
           // $query .= "    AND ca_fixed_asset.deminimis_asset = 'N'";
            if ($this->deminimis) {
                $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
            }
            if ($this->entity) {
                $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
            }
            $query .= "    AND ca_fixed_asset.active = 'Y'";
            $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
            $query .= ")";

            $query .= ", 0)";
            $query .= " `" . $code . " land`";
        }
        // Add row total
        $query .= " , ";
        $query .= " ROUND(";
        $query .= " (SELECT IFNULL(SUM( ";

        $query .= "  if(`ca_transaction`.`rr_trans` = 'Y', ";
        $query .= "     if(ca_transaction.hc_asset = 0 && ca_transaction.hc_land = 0 ";
        $query .= "        , 0 ";
        $query .= "        , ca_transaction.hc_asset + ca_transaction.hc_land)";
        $query .= "     ,(`ca_transaction`.`gbv_change_asset` + `ca_transaction`.`gbv_change_land`))";

        $query .= " ), 0)";
        $query .= " FROM ca_transaction ";
        $query .= "    INNER JOIN ca_ifrs_category ON
            ca_ifrs_category.ca_ifrs_category_id = ca_transaction.ca_ifrs_cat_id ";
        $query .= "    INNER JOIN vw_ca_current_fin_year ON
            vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
        $query .= "    INNER JOIN ca_fixed_asset ON
            ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
        $query .= " LEFT JOIN vw_far_disposed ON
               vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
        $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
        $query .= "    AND ca_transaction_type_id = " . CaTransactionType::REVALUATION;
        $query .= "    AND ca_ifrs_category.active = 'Y'";
        //$query .= "    AND ca_fixed_asset.deminimis_asset = 'N'";
        if ($this->deminimis) {
            $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
        }
        if ($this->entity) {
            $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
        }
        $query .= "    AND ca_fixed_asset.active = 'Y'";
        if ($this->commonGood) {
            $query .= " AND ca_ifrs_category.common_good = '{$this->commonGood}'";
        }
        if ($this->investment) {
            $query .= " AND ca_ifrs_category.investment = '{$this->investment}'";
        }
        $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
        $query .= " )";
        $query .= " + ";
        $query .= " (SELECT IFNULL(SUM(ca_transaction.gbv_change_asset + ca_transaction.gbv_change_land), 0) ";
        $query .= " FROM ca_transaction ";
        $query .= "    INNER JOIN ca_ifrs_category ON
            ca_ifrs_category.ca_ifrs_category_id = ca_transaction.ca_ifrs_cat_id ";
        $query .= "    INNER JOIN vw_ca_current_fin_year ON
            vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
        $query .= "    INNER JOIN ca_fixed_asset ON
            ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
        $query .= " LEFT JOIN vw_far_disposed ON
               vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
        $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
        $query .= "    AND ca_ifrs_category.active = 'Y'";
        //$query .= "    AND ca_fixed_asset.deminimis_asset = 'N'";
        if ($this->deminimis) {
            $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
        }
        if ($this->entity) {
            $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
        }
        $query .= "    AND ca_fixed_asset.active = 'Y'";
        $query .= "    AND ca_transaction_type_id IN
            (" . CaTransactionType::REVERSAL_OF_IMPAIRMENT . "," . CaTransactionType::REVERSAL_OF_LOSS . ")";
        if ($this->commonGood) {
            $query .= " AND ca_ifrs_category.common_good = '{$this->commonGood}'";
        }
        if ($this->investment) {
            $query .= " AND ca_ifrs_category.investment = '{$this->investment}'";
        }
        $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
        $query .= ")";
        $query .= ", 0)";
        $query .= " `Total`";

        return \DB::table(null)->select(\DB::raw($query));
    }

    private function buildDepreciationRR($rowDesc, array $arrIFRSCat, $gains)
    {
        // First column is the row description label.
        $query =  "'$rowDesc' `Element`";

        // set the compare operator to select for gains (positive) or losses (negative)
        $op = ($gains) ? "<" : ">";

        // Add columns for each IFRS category
        foreach ($arrIFRSCat as $key => $value) {
            $query .= ", ";
            $query .= " ROUND(IFNULL((SELECT SUM(if(ca_transaction.depn_wo " . $op . " 0, ca_transaction.depn_wo, 0))";
            $query .= "      * -1 ";
            $query .= " FROM ca_transaction ";
            $query .= " INNER JOIN vw_ca_current_fin_year ON
                vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
            $query .= " INNER JOIN ca_fixed_asset ON
                ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
            $query .= " LEFT JOIN vw_far_disposed ON
                   vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
            $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId'";
            $query .= " AND ca_transaction_type_id = " . CaTransactionType::REVALUATION;
            $query .= " AND ca_transaction.ca_ifrs_cat_id = $key";
            //$query .= " AND ca_fixed_asset.deminimis_asset = 'N'";
            if ($this->deminimis) {
                $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
            }
            if ($this->entity) {
                $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
            }
            $query .= " AND ca_fixed_asset.active = 'Y'";
            $query .= " AND ca_transaction.rr_trans = 'Y'";
            $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
            $query .= "),0),0)";
            $query .= " `$value`";
        }

        // Add row total column
        $query .= " , ";
        $query .= " ROUND(IFNULL((SELECT SUM(if(ca_transaction.depn_wo " . $op . " 0, ca_transaction.depn_wo, 0))";
        $query .= "      * -1 ";
        $query .= " FROM ca_transaction ";
        $query .= " INNER JOIN ca_ifrs_category ON
            ca_ifrs_category.ca_ifrs_category_id = ca_transaction.ca_ifrs_cat_id ";
        $query .= " INNER JOIN vw_ca_current_fin_year ON
            vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
        $query .= " INNER JOIN ca_fixed_asset ON ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
        $query .= " LEFT JOIN vw_far_disposed ON
               vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
        $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
        $query .= " AND ca_ifrs_category.active = 'Y'";
        $query .= " AND ca_transaction_type_id = " . CaTransactionType::REVALUATION;
        // $query .= " AND ca_fixed_asset.deminimis_asset = 'N'";
        if ($this->deminimis) {
            $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
        }
        if ($this->entity) {
            $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
        }
        $query .= " AND ca_fixed_asset.active = 'Y'";
        $query .= " AND ca_transaction.rr_trans = 'Y'";
        if ($this->commonGood) {
            $query .= " AND ca_ifrs_category.common_good = '{$this->commonGood}'";
        }
        if ($this->investment) {
            $query .= " AND ca_ifrs_category.investment = '{$this->investment}'";
        }
        $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
        $query .= "),0),0)";
        $query .= " `Total`";

        return \DB::table(null)->select(\DB::raw($query));
    }
}
