<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\Common;
use Tfcloud\Models\User;
use Tfcloud\Services\BaseService;
use Tfcloud\Lib\Filters\UserFilter;
use Tfcloud\Lib\Filters\UserLoginFilter;
use Tfcloud\Services\PermissionService;

class UsersFilterQueryService extends BaseService
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    public function reAddUserQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $filter = new UserFilter($filterData);

        if (!is_null($filter->username)) {
            $query->where(
                $viewName . ".username",
                "like",
                "%{$filter->username}%"
            );
        }
        if (!is_null($filter->active_directory_user)) {
            $query->where($viewName . ".active_directory_user", $filter->active_directory_user);
        }
        if (!empty($filter->active_user)) {
            $query->where($viewName . ".active_user", $filter->active_user);
        }
        if (!is_null($filter->display_name)) {
            $query->where(
                $viewName . ".display_name",
                "like",
                "%{$filter->display_name}%"
            );
        }
        if (!is_null($filter->usergroup_type_id)) {
            $query->where(
                $viewName . ".usergroup_type_id",
                $filter->usergroup_type_id
            );
        }

        if (!is_null($filter->usergroup_id)) {
            $query->where(
                $viewName . ".usergroup_id",
                $filter->usergroup_id
            );
        }

        if (!is_null($filter->site_id)) {
            $query->where(function ($subQuery) use ($viewName, $filter) {
                $subQuery->where($viewName . ".site_access", User::SITE_ACCESS_MODE_ALL_EDIT);
                $subQuery->orwhere($viewName . ".site_access", User::SITE_ACCESS_MODE_ALL_READ);
                $subQuery->orwhere(function ($minQuery) use ($viewName, $filter) {
                    $minQuery->where($viewName . ".site_access", User::SITE_ACCESS_MODE_SELECTED);
                    $minQuery->where($viewName . ".site_id", $filter->site_id);
                });
            });
        }

        if ($val = array_get($filterData, 'username', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.username') . " contains '" . $val . "'"));
        }

        if ($val = Common::iset($filterData['active_directory_user'])) {
            array_push(
                $whereTexts,
                sprintf(\Lang::get('text.report_texts.active_directory_user') . " = '" . $val . "'")
            );
        }
        if ($val = Common::iset($filterData['active_user'])) {
            array_push($whereTexts, sprintf("Account Active = '" . $val . "'"));
        }

        if ($val = array_get($filterData, 'display_name', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.display_name') . " contains '" . $val . "'"));
        }

        if ($val = array_get($filterData, 'usergroup_type_id', null)) {
            array_push($whereCodes, [
                'usergroup_type',
                'usergroup_type_id',
                'usergroup_type_code',
                $val,
                \Form::fieldLang('usergroup_type_id')
            ]);
        }

        if ($val = array_get($filterData, 'usergroup_id', null)) {
            array_push($whereCodes, [
                'usergroup',
                'usergroup_id',
                'usergroup_code',
                $val,
                \Form::fieldLang('usergroup_id')
            ]);
        }

        if ($val = array_get($filterData, 'site_id', null)) {
            array_push($whereCodes, [
                'site',
                'site_id',
                'site_code',
                $val,
                'Site Code'
            ]);
        }

        return $query;
    }

    public function reAddUserLoginQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $filter = new UserLoginFilter($filterData);

        if (!is_null($filter->username)) {
            $query->where(
                $viewName . ".username",
                "like",
                "%{$filter->username}%"
            );
        }
        if (!is_null($filter->display_name)) {
            $query->where(
                $viewName . ".display_name",
                "like",
                "%{$filter->display_name}%"
            );
        }

        if (!is_null($filter->description)) {
            $query->where(
                $viewName . ".Description",
                "like",
                "%{$filter->description}%"
            );
        }

        if (!is_null($filter->job_title)) {
            $query->where(
                $viewName . ".job_title",
                "like",
                "%{$filter->job_title}%"
            );
        }

        if (!is_null($filter->usergroup_id)) {
            $query->where(
                $viewName . ".usergroup_id",
                $filter->usergroup_id
            );
        }

        if (!is_null($filter->loggedFrom)) {
            $loggedFrom = \DateTime::createFromFormat('d/m/Y', $filter->loggedFrom);

            if ($loggedFrom) {
                $query->where(
                    \DB::raw("DATE_FORMAT($viewName.login_datetime, '%Y-%m-%d')"),
                    '>=',
                    $loggedFrom->format('Y-m-d')
                );
            }
        }

        if (!is_null($filter->loggedTo)) {
            $loggedTo = \DateTime::createFromFormat('d/m/Y', $filter->loggedTo);

            if ($loggedTo) {
                $query->where(
                    \DB::raw("DATE_FORMAT($viewName.login_datetime, '%Y-%m-%d')"),
                    '<=',
                    $loggedTo->format('Y-m-d')
                );
            }
        }


        // Report feedback.
        if ($val = array_get($filterData, 'username', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.username') . " contains '" . $val . "'"));
        }

        if ($val = array_get($filterData, 'display_name', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.display_name') . " contains '" . $val . "'"));
        }

        if ($val = array_get($filterData, 'description', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.description'), $val));
        }

        if ($val = array_get($filterData, 'job_title', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.job_title'), $val . "'"));
        }

        if ($val = array_get($filterData, 'usergroup_id', null)) {
            array_push($whereCodes, [
                'usergroup',
                'usergroup_id',
                'usergroup_code',
                $val,
                \Form::fieldLang('usergroup_id')
            ]);
        }

        if ($val = array_get($filterData, 'loggedFrom', null)) {
            array_push($whereTexts, sprintf("Loged In Date From" . " contains '" . $val . "'"));
        }

        if ($val = array_get($filterData, 'loggedTo', null)) {
            array_push($whereTexts, sprintf("Loged In Date To" . " contains '" . $val . "'"));
        }

        return $query;
    }


    public function reAddDloJobLabourQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $service = new JobLabourService($this->permissionService);
        $query = $service->filterAll($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'dlo_job_code', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.dlo.job_code_contains'), $val));
        }

        if ($val = array_get($filterData, 'dlo_job_labour_code', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.dlo.labour_code_contains'), $val));
        }

        if ($val = array_get($filterData, 'dlo_job_desc', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.dlo.job_description_contains'), $val));
        }

        if ($val = array_get($filterData, 'dateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.date_from'), $val));
        }

        if ($val = array_get($filterData, 'dateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.date_to'), $val));
        }

        if ($val = array_get($filterData, 'startDateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.start_date_from'), $val));
        }

        if ($val = array_get($filterData, 'startDateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.start_date_to'), $val));
        }

        if ($val = array_get($filterData, 'finishDateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.finish_date_from'), $val));
        }

        if ($val = array_get($filterData, 'finishDateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.finish_date_to'), $val));
        }

        if ($val = array_get($filterData, 'dlo_job_type', null)) {
            array_push($whereCodes, [
                'dlo_job_type',
                'dlo_job_type_id',
                'dlo_job_type_code',
                $val,
                \Form::fieldLang('dlo_job_type_id')
            ]);
        }

        if ($val = array_get($filterData, 'plant_group_id', null)) {
            array_push($whereCodes, [
                'plant_group',
                'plant_group_id',
                'plant_group_code',
                $val,
                \Form::fieldLang('plant_group_id')
            ]);
        }

        if ($val = array_get($filterData, 'plant_subgroup_id', null)) {
            array_push($whereCodes, [
                'plant_subgroup',
                'plant_subgroup_id',
                'plant_subgroup_code',
                $val,
                \Form::fieldLang('plant_subgroup_id')
            ]);
        }

        if ($val = array_get($filterData, 'plant_type_id', null)) {
            array_push($whereCodes, [
                'plant_type',
                'plant_type_id',
                'plant_type_code',
                $val,
                \Form::fieldLang('plant_type_id')
            ]);
        }

        if ($val = array_get($filterData, 'site_plant_system_id', null)) {
            array_push($whereCodes, [
                'site_plant_system',
                'site_plant_system_id',
                'site_plant_system_code',
                $val,
                \Form::fieldLang('site_plant_system_id')
            ]);
        }

        if ($val = array_get($filterData, 'plant_id', null)) {
            array_push($whereCodes, [
                'plant',
                'plant_id',
                'plant_code',
                $val,
                \Form::fieldLang('plant_id')
            ]);
        }

        if ($val = array_get($filterData, 'manager_user_id', null)) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, \Form::fieldLang('manager_user_id')]);
        }
        if ($val = array_get($filterData, 'dlo_job_operative_id', null)) {
            $dloJobOperative = DloJobOperative::find($val);
            if ($dloJobOperative) {
                array_push(
                    $whereCodes,
                    ['user', 'id', 'display_name', $dloJobOperative->user_id, \Form::fieldLang('dlo_job_operative_id')]
                );
            }
        }

        if ($val = Common::iset($filterData['surveyor_user_id'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, \Form::fieldLang('surveyor_user_id')]);
        }

        if ($val = Common::iset($filterData['owner_user_id'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, \Form::fieldLang('owner_user_id')]);
        }

        if ($val = Common::iset($filterData['trade_code'])) {
            array_push($whereCodes, [
                'trade_code',
                'trade_code_id',
                'trade_code_code',
                $val,
                \Form::fieldLang('trade_code_id')
            ]);
        }

        if ($val = array_get($filterData, 'dlo_job_priority_id', null)) {
            array_push($whereCodes, [
                'dlo_job_priority',
                'dlo_job_priority_id',
                'dlo_job_priority_code',
                $val,
                \Form::fieldLang('dlo_job_priority_id')
            ]);
        }

        if ($val = array_get($filterData, 'dlo_job_status_id', null)) {
            array_push($whereCodes, [
                'dlo_job_status',
                'dlo_job_status_id',
                'dlo_job_status_desc',
                $val,
                \Form::fieldLang('dlo_job_status_id')
            ]);
        }

        $statusType = [];
        if (array_get($filterData, 'statusTypePlan', null)) {
            $statusType[] = DloJobStatusType::IN_PLANNING;
        }
        if (array_get($filterData, 'statusTypeOpen', null)) {
            $statusType[] = DloJobStatusType::OPEN;
        }
        if (array_get($filterData, 'statusTypeInProgress', null)) {
            $statusType[] = DloJobStatusType::IN_PROGRESS;
        }
        if (array_get($filterData, 'statusTypeWorksComp', null)) {
            $statusType[] = DloJobStatusType::WORKS_COMP;
        }
        if (array_get($filterData, 'statusTypeWorksExpComp', null)) {
            $statusType[] = DloJobStatusType::WORKS_EXP_COMP;
        }
        if (array_get($filterData, 'statusTypeCancelled', null)) {
            $statusType[] = DloJobStatusType::CANCELLED;
        }
        if (array_get($filterData, 'statusTypePosted', null)) {
            $statusType[] = DloJobStatusType::POSTED;
        }
        if (count($statusType)) {
            array_push($orCodes, [
                'dlo_job_status_type',
                'dlo_job_status_type_id',
                'dlo_job_status_type_desc',
                implode(',', $statusType),
                \Lang::get('text.status_type')
            ]);
        }

        // Special location
        if ($val = array_get($filterData, 'site_id')) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);

            if ($val = array_get($filterData, 'building_id')) {
                array_push($whereCodes, ['building', 'building_id', 'building_code', $val, "Building Code"]);

                if ($val = array_get($filterData, 'room_id')) {
                    array_push($whereCodes, ['room', 'room_id', 'room_number', $val, "Room Number"]);
                }
            }
        }

        return $query;
    }

    public function reAddDloJobOperativeQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $jobService = new JobService(
            $this->permissionService,
            new UserDefinedService(),
            new PlantService($this->permissionService)
        );
        $service = new JobOperativeService($this->permissionService, $jobService);
        $query = $service->filterAll($filterData, $viewName, $query);

        //if ($val = array_get($filterData, 'dlo_job_id', null)) {
        //    array_push($whereTexts, sprintf(\Lang::get('text.report_texts.dlo.job_code_contains'), $val));
        //}


        return $query;
    }

    public function reAddDloJobLabourHoursQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $service = new JobLabourService($this->permissionService);
        $query = $service->filterAll($query, $filterData, $viewName);

        //if ($val = array_get($filterData, 'dlo_job_id', null)) {
        //    array_push($whereTexts, sprintf(\Lang::get('text.report_texts.dlo.job_code_contains'), $val));
        //}

        return $query;
    }

    public function reAddDloJobPoWoQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $service = new InstructionListService($this->permissionService);
        $query = $service->filterForReportDLO09($query, $filterData);

        //if ($val = array_get($filterData, 'dlo_job_id', null)) {
        //    array_push($whereTexts, sprintf(\Lang::get('text.report_texts.dlo.job_code_contains'), $val));
        //}

        return $query;
    }

    public function reAddDloSalesInvoiceQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $service = new DloSalesInvoiceService($this->permissionService);
        $query = $service->filterDloSalesInvoiceReports($query, $filterData, $viewName);

        //if ($val = array_get($filterData, 'dlo_job_id', null)) {
        //    array_push($whereTexts, sprintf(\Lang::get('text.report_texts.dlo.job_code_contains'), $val));
        //}

        return $query;
    }

    public function reAddDloMaterialItemQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $service = new JobMaterialItemService($this->permissionService);
        $query = $service->filterAll($filterData, $query, $viewName);

        //if ($val = array_get($filterData, 'dlo_job_id', null)) {
        //    array_push($whereTexts, sprintf(\Lang::get('text.report_texts.dlo.job_code_contains'), $val));
        //}

        return $query;
    }
}
