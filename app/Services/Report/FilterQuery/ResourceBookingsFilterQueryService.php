<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Models\RbBookableResourceType;
use Tfcloud\Models\RbResourceBookingStatusType;
use Tfcloud\Services\Admin\System\ResourceBooking\CateringItemService;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\ResourceBooking\BookableResourceService;
use Tfcloud\Services\ResourceBooking\EventService;
use Tfcloud\Services\ResourceBooking\RbCateringBookingService;
use Tfcloud\Services\ResourceBooking\RbResourceBookingService;
use Tfcloud\Services\UserDefinedService;

class ResourceBookingsFilterQueryService extends BaseService
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    public function reAddBookableResourceQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $bookableResourceService = new BookableResourceService($this->permissionService);
        $bookableResourceQuery = $bookableResourceService->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Description contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['roomCapacityFrom'])) {
            array_push($whereTexts, "Capacity is greater than or equal to '" . $val . "'");
        }

        if ($val = Common::iset($filterData['roomCapacityTo'])) {
            array_push($whereTexts, "Capacity is less than or equal to '" . $val . "'");
        }

        if ($val = Common::iset($filterData['resourceType'])) {
            array_push(
                $whereCodes,
                [
                    'rb_bookable_resource_type',
                    'rb_bookable_resource_type_id',
                    'rb_bookable_resource_type_code',
                    $val,
                    "Resource Type"
                ]
            );
        }

        if ($val = Common::iset($filterData['plant_id'])) {
            array_push($whereCodes, ['plant', 'plant_id', 'plant_code', $val, 'Plant']);
        }

        return $bookableResourceQuery;
    }

    public function reAddBookableResourceAvailabilityQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $bookableResourceService = new BookableResourceService($this->permissionService);
        $bookableResourceQuery = $bookableResourceService->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Description contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['roomCapacityFrom'])) {
            array_push($whereTexts, "Capacity is greater than or equal to '" . $val . "'");
        }

        if ($val = Common::iset($filterData['roomCapacityTo'])) {
            array_push($whereTexts, "Capacity is less than or equal to '" . $val . "'");
        }

        if ($val = Common::iset($filterData['resourceType'])) {
            array_push(
                $whereCodes,
                [
                    'rb_bookable_resource_type',
                    'rb_bookable_resource_type_id',
                    'rb_bookable_resource_type_code',
                    $val,
                    "Resource Type"
                ]
            );
        }

        if ($val = Common::iset($filterData['plant_id'])) {
            array_push($whereCodes, ['plant', 'plant_id', 'plant_code', $val, 'Plant']);
        }

        if ($val = Common::iset($filterData['fromTime'])) {
            array_push($whereTexts, "Available from time contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['toTime'])) {
            array_push($whereTexts, "Available to time contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['bookable_on'])) {
            array_push($whereTexts, "Available On contains '" . $val . "'");
        }

        return $bookableResourceQuery;
    }

    public function reAddResourceBookingQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $resourceBookingService = new RbResourceBookingService($this->permissionService, new UserDefinedService());
        $rsQuery = $resourceBookingService->filterAll($query, $filterData, $viewName);

        $this->logResourceBookingFilter($filterData, $whereCodes, $orCodes, $whereTexts);

        return $rsQuery;
    }

    public function reAddCateringBookingQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $userDefinedService = new UserDefinedService();
        $resourceBookingService = new RbResourceBookingService($this->permissionService, $userDefinedService);
        $eventService = new EventService($this->permissionService);
        $cateringService = new RbCateringBookingService(
            $this->permissionService,
            $resourceBookingService,
            $eventService
        );

        if ($val = Common::iset($filterData['cateringCode'])) {
            array_push($whereTexts, "Catering Booking Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['cateringDesc'])) {
            array_push($whereTexts, "Catering Booking Description contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['item'])) {
            $trackingQuery = [
                'rb_catering_item',
                'rb_catering_item_id',
                'rb_catering_item_code',
                $val,
                'Catering Item'
            ];
            array_push($whereCodes, $trackingQuery);
        }

        if ($val = Common::iset($filterData['serviceDateFrom'])) {
            array_push($whereTexts, "Service Date From = '" . $val . "'");
        }

        if ($val = Common::iset($filterData['serviceDateTo'])) {
            array_push($whereTexts, "Service Date To = '" . $val . "'");
        }

        if ($val = Common::iset($filterData['quantityMin'])) {
            array_push($whereTexts, "Quantity Min = " . $val);
        }

        if ($val = Common::iset($filterData['quantityMax'])) {
            array_push($whereTexts, "Quantity Max = " . $val);
        }

        $this->logResourceBookingFilter($filterData, $whereCodes, $orCodes, $whereTexts);

        $filterQuery = $cateringService->filterAll($query, $filterData, $viewName);

        return $filterQuery;
    }

    public function reAddCateringItemQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $cateringItemService = new CateringItemService($this->permissionService);
        $cateringItemQuery = $cateringItemService->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Description contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['comments'])) {
            array_push($whereTexts, "Comment contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['status'])) {
            array_push($whereTexts, "Active contains '" . $val . "'");
        }

        return $cateringItemQuery;
    }

    private function logResourceBookingFilter($filterData, &$whereCodes, &$orCodes, &$whereTexts)
    {
        if ($val = array_get($filterData, 'bookingFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.date_from'), $val));
        }

        if ($val = array_get($filterData, 'bookingTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.date_to'), $val));
        }

        if ($val = Common::iset($filterData['plant_id'])) {
            array_push($whereCodes, ['plant', 'plant_id', 'plant_code', $val, \Form::fieldLang('plant_id')]);
        }

        if ($val = Common::iset($filterData['rb_bookable_resource_id'])) {
            array_push($whereCodes, [
                'rb_bookable_resource',
                'rb_bookable_resource_id',
                'rb_bookable_resource_code',
                $val,
                \Form::fieldLang('rb_bookable_resource_id')
            ]);
        }

        if ($val = Common::iset($filterData['rb_bookable_resource_layout_id'])) {
            if ($val == 'All') {
                array_push($whereTexts, "Layout = 'All'");
            } else {
                array_push($whereCodes, [
                    'rb_bookable_resource_layout',
                    'rb_bookable_resource_layout_id',
                    'rb_bookable_resource_layout_code',
                    $val,
                    'Layout'
                ]);
            }
        }

        if ($val = array_get($filterData, 'bookingCode', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.booking_code'), $val));
        }

        if ($val = array_get($filterData, 'bookingDesc', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.booking_desc'), $val));
        }

        if ($val = Common::iset($filterData['event'])) {
            array_push($whereCodes, [
                'rb_event',
                'rb_event_id',
                'rb_event_code',
                $val,
                \Form::fieldLang('rb_event_id')
            ]);
        }

        if ($val = Common::iset($filterData['booked_for_contact'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Booked For Contact']);
        }

        if ($val = Common::iset($filterData['booked_for_user'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Booked For User']);
        }

        if ($val = Common::iset($filterData['bookFor'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, \Form::fieldLang('booked_for_user_id')]);
        }

        if ($val = Common::iset($filterData['owner'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, \Form::fieldLang('created_by')]);
        }

        $statusType = [];
        if (Common::iset($filterData['tentative'])) {
            $statusType[] = RbResourceBookingStatusType::TENTATIVE;
        }
        if (Common::iset($filterData['forApproval'])) {
            $statusType[] = RbResourceBookingStatusType::FOR_APPROVAL;
        }
        if (Common::iset($filterData['confirmed'])) {
            $statusType[] = RbResourceBookingStatusType::CONFIRMED;
        }
        if (Common::iset($filterData['cancelled'])) {
            $statusType[] = RbResourceBookingStatusType::CANCELLED;
        }
        if (Common::iset($filterData['noShow'])) {
            $statusType[] = RbResourceBookingStatusType::NO_SHOW;
        }
        if (count($statusType)) {
            array_push($orCodes, [
                'rb_resource_booking_status_type',
                'rb_resource_booking_status_type_id',
                'rb_resource_booking_status_type_code',
                implode(',', $statusType),
                \Lang::get('text.status_type')
            ]);
        }

        if (Common::iset($filterData['cateringStatusType'])) {
            $cateringBookingStatusType = array_get($filterData, 'cateringStatusType', []);
            if (count($cateringBookingStatusType)) {
                array_push($orCodes, [
                    'rb_catering_booking_status_type',
                    'rb_catering_booking_status_type_id',
                    'rb_catering_booking_status_type_code',
                    implode(',', $cateringBookingStatusType),
                    \Lang::get('text.report_texts.catering_status_type')
                ]);
            }
        }

        if ($val = Common::iset($filterData['roomCapacityFrom'])) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.capacity_from'), $val));
        }

        if ($val = Common::iset($filterData['roomCapacityTo'])) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.capacity_to'), $val));
        }

        if ($val = Common::iset($filterData['resourceAttributes'])) {
            if ($val == CommonConstant::YES) {
                $attributes = array_get($filterData, 'attributes');
                if (count($attributes)) {
                    array_push($orCodes, [
                        'rb_bookable_resource_att',
                        'rb_bookable_resource_att_id',
                        'rb_bookable_resource_att_code',
                        implode(',', $attributes),
                        \Form::fieldLang('resource_attributes')
                    ]);
                }
            } else {
                array_push($whereTexts, \Form::fieldLang('resource_attributes') . ' ' . \Lang::get('text.all'));
            }
        }

        if ($val = Common::iset($filterData['resourceType'])) {
            array_push(
                $whereCodes,
                [
                    'rb_bookable_resource_type',
                    'rb_bookable_resource_type_id',
                    'rb_bookable_resource_type_code',
                    $val,
                    "Resource Type"
                ]
            );
        }

        if ($val = array_get($filterData, 'rbResourceDesc', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.resource_desc'), $val));
        }

        $this->logBookingTypeFilter($filterData, $whereCodes, $orCodes, $whereTexts);
    }

    private function logBookingTypeFilter($filterData, &$whereCodes, &$orCodes, &$whereTexts)
    {
        if ($val = Common::iset($filterData['rbBookingType'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push(
                        $whereTexts,
                        sprintf(\Lang::get('text.report_texts.external_booking'), CommonConstant::YES)
                    );
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push(
                        $whereTexts,
                        sprintf(\Lang::get('text.report_texts.external_booking'), CommonConstant::NO)
                    );
                    break;
                default:
                    array_push(
                        $whereTexts,
                        sprintf(\Lang::get('text.report_texts.external_booking'), CommonConstant::ALL)
                    );
                    break;
            }
        }
    }
}
