<?php

/**
 * FAR01
 *
 * IMPORTANT - READ THIS!!!
 *
 * This report has been duplicated as FAR32 (FixedAssetsMovementsOnBalancesAssetLandQueryService).
 * This report combines the asset and land values whereas FAR32 has the values broken down.
 * When making changes to this report, consideration should be given to whether FAR32 should
 * also be updated.
 *
 */

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\FixedAsset\CaSystemManager;
use Tfcloud\Models\CaIfrsCategory;
use Tfcloud\Models\CaTransactionType;
use Tfcloud\Models\Report;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\PermissionService;

class FixedAssetsMovementsOnBalancesQueryService extends BaseService
{
    protected $permissionService;

    protected $siteGroupId;
    protected $bIncludeRRTrans = false;
    protected $rrTrans = 'N';
    protected $bSumNegative = false;

    // filters
    protected $commonGood = false;
    protected $investment = false;
    protected $deminimis = false;
    protected $entity = false;
    protected $filterData = [];

    // detailed report
    protected $detailed = true;

    private $reportBoxFilterLoader = null;

    public function __construct(PermissionService $permissionService, $filterData, $repCode, Report $report = null)
    {
        $this->permissionService = $permissionService;
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;

        $this->setFilterProperties($filterData, $repCode);
    }

    private function setFilterProperties($filterData, $repCode)
    {
        $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($filterData);
        $commonGood = $reportFilterQuery->getFirstValueFilterField('common_good');
        $investment = $reportFilterQuery->getFirstValueFilterField('investment');
        $deminimis = $reportFilterQuery->getFirstValueFilterField('deminimis_asset');
        $entity = $reportFilterQuery->getFirstValueFilterField('ca_entity_id');

        // get filter values
        $this->filterData = $filterData;
        $this->commonGood = (!$commonGood) ? false : $commonGood;
        $this->investment = (!$investment) ? false : $investment;
        $this->deminimis = (!$deminimis) ? false : $deminimis;
        $this->entity = (!$entity) ? false : $entity;
        $this->detailed = ($repCode == 'FAR31') ? true : false;
    }

    public function getNumericCols()
    {
        $arrIFRSCat = $this->getIFRSCategories();
        $numCols = [];

        $col = 0;

        foreach ($arrIFRSCat as $key => $category) {
            $col++;
            $numCols[] = $col;
        }

        $numCols[] = $col + 1;

        return $numCols;
    }

    public function movementsOnBalancesQuery(
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        // Get all IFRS categories the report should include.
        $arrIFRSCat = $this->getIFRSCategories();

        // Get IFRS categories code and description for use in report header.
        $arrIFRSCatDesc = $this->getIFRSCategories(true);

        $bYearEnd = CaSystemManager::isInYearEndMode();
        $yearStartDate = CaSystemManager::getYearStartDate(CommonConstant::DATE_FORMAT_MONTH);
        $yearEndDate = CaSystemManager::getYearEndDate(CommonConstant::DATE_FORMAT_MONTH);
        $this->siteGroupId = \Auth::user()->site_group_id;

        // *****************************************************************
        // REPORT HEADER
        // *****************************************************************
        // Build report header
        $queryRpt = $this->reportHeader($arrIFRSCatDesc);

        // *****************************************************************
        // PART 1 - COST OR VALUATION SECTION
        // *****************************************************************

        // Add Cost or Valuation blank format row
        $rowDesc = "Cost or Valuation";
        $queryRpt->unionAll($this->buildLabelRow($rowDesc, $arrIFRSCat));

        // Add First detail row (values at start of year)
        $arrColumn = array("ca_balance.bfwd_gbv");
        $rowDesc = "At " . $yearStartDate;
        $queryRpt->unionAll($this->buildBalanceQuery($rowDesc, $arrIFRSCat, $arrColumn, true));

        // Add row - Acquisitions
        $arrColumn = array("ca_transaction.gbv_change_asset", "ca_transaction.gbv_change_land");
        $arrTranType = array(CaTransactionType::ACQUISITION);
        $this->setSumNegative(false);
        $this->setIncludeRRTrans(false);
        $queryRpt->unionAll($this->buildTranQuery("Acquisitions", $arrIFRSCat, $arrColumn, $arrTranType));

        // Add row - Additions.
        $arrColumn = array("ca_transaction.gbv_change_asset", "ca_transaction.gbv_change_land");
        $arrTranType = array(CaTransactionType::ADDITION);
        $this->setSumNegative(false);
        $this->setIncludeRRTrans(false);
        $queryRpt->unionAll($this->buildTranQuery("Additions", $arrIFRSCat, $arrColumn, $arrTranType));

        if ($this->detailed) {
            // Add row - Revaluation Gains to RR
            $queryRpt->unionAll($this->buildRevaluationGainsToRR(
                'Revaluation Gains to RR',
                $arrIFRSCat
            ));

            // Add row - Revaluation Losses to RR
            $queryRpt->unionAll($this->buildRevaluationLossesToRR(
                'Revaluation Losses to RR',
                $arrIFRSCat
            ));
        } else {
            // Add row - Revaluation Increases/Decreases to RR
            $queryRpt->unionAll($this->buildRevaluationToRR(
                'Revaluation Increases/Decreases to RR',
                $arrIFRSCat
            ));
        }

        if ($this->detailed) {
            // Add row - Revaluation Increases/Decreases to Surplus/Deficit
            $queryRpt->unionAll($this->buildRevaluationToSurplusDeficit(
                'Revaluation Increases/Decreases to Surplus/Deficit',
                $arrIFRSCat
            ));

            // Add row - Reversal of Loss to Surplus/Deficit
            $queryRpt->unionAll($this->buildReversalOfLossToSurplusDeficit(
                'Reversal of Loss to Surplus/Deficit',
                $arrIFRSCat
            ));
        } else {
            // Add row - Revaluation Increases/Decreases to Surplus/Deficit
            $queryRpt->unionAll($this->buildRevaluationAndReversalOfLossToSurplusDeficit(
                'Revaluation Increases/Decreases to Surplus/Deficit',
                $arrIFRSCat
            ));
        }

        // Add row - Derecognition - Disposals
        $arrColumn = array("ca_transaction.gbv_change_asset", "ca_transaction.gbv_change_land");
        $arrTranType = array(CaTransactionType::DISPOSAL);
        $this->setSumNegative(false);
        $this->setIncludeRRTrans(false);
        $queryRpt->unionAll($this->buildTranQuery("Derecognition - Disposals", $arrIFRSCat, $arrColumn, $arrTranType));

        // Add row - Derecognition - Other
        $arrColumn = array("ca_transaction.gbv_change_asset", "ca_transaction.gbv_change_land");
        $arrTranType = array(CaTransactionType::DERECOGNITION);
        $this->setSumNegative(false);
        $this->setIncludeRRTrans(false);
        $queryRpt->unionAll($this->buildTranQuery("Derecognition - Other", $arrIFRSCat, $arrColumn, $arrTranType));

        // Add row - Reclassification and Transfers
        $arrColumn = array("ca_transaction.gbv_change_asset", "ca_transaction.gbv_change_land");
        $arrTranType = array(CaTransactionType::CATEGORY_TRANSFER);
        $this->setSumNegative(false);
        $this->setIncludeRRTrans(false);
        $queryRpt->unionAll(
            $this->buildTranQuery("Reclassification and Transfers", $arrIFRSCat, $arrColumn, $arrTranType)
        );

        // Add row - Balance as at 31 March XXXX (Column totals)
        $arrColumn = array("ca_balance.cfwd_gbv");
        $rowDesc = "Balance as at " . $yearEndDate;
        $queryRpt->unionAll($this->buildBalanceQuery($rowDesc, $arrIFRSCat, $arrColumn));

        // *****************************************************************
        // PART 2 - DEPRECIATION AND IMPAIRMENT SECTION
        // *****************************************************************

        // Add Depreciation and Impairment blank format row.
        $queryRpt->unionAll($this->addBlankRow($arrIFRSCat));

        // Add section heading row.
        $rowDesc = "Depreciation and Impairment";
        $queryRpt->unionAll($this->buildLabelRow($rowDesc, $arrIFRSCat));

        // Add detail line (bfwd_accum_imp at start of year).
        $arrColumn = array("ca_balance.bfwd_accum_imp_asset", "ca_balance.bfwd_accum_imp_land");
        $rowDesc = "Impairment at " . $yearStartDate;
        $queryRpt->unionAll($this->buildBalanceQuery($rowDesc, $arrIFRSCat, $arrColumn, true));

        // Add detail line (bfwd_accum_dep at start of year).
        $arrColumn = array("ca_balance.bfwd_accum_depn");
        $rowDesc = "Depreciation at " . $yearStartDate;
        $queryRpt->unionAll($this->buildBalanceQuery($rowDesc, $arrIFRSCat, $arrColumn, true));

        // Add detail line (cy_depreciation).
        $arrColumn = array("ca_balance.cy_depreciation");
        $rowDesc = "Depreciation Charge";
        $queryRpt->unionAll($this->buildBalanceQuery($rowDesc, $arrIFRSCat, $arrColumn, $bYearEnd));

        if ($this->detailed) {
            // Add line. Depreciation written out to the RR on gains
            $queryRpt->unionAll(
                $this->buildDepreciationRR("Depreciation written out to the RR on gains", $arrIFRSCat, true)
            );

            // Add line. Depreciation written out to the RR on losses
            $queryRpt->unionAll(
                $this->buildDepreciationRR("Depreciation written out to the RR on losses", $arrIFRSCat, false)
            );
        } else {
            // Add line. Depreciation written out to the RR
            $arrColumn = array("ca_transaction.depn_wo");
            $arrTranType = array(CaTransactionType::REVALUATION);
            $this->setSumNegative(true);
            $this->setIncludeRRTrans(true);
            $this->setRRTrans("Y");
            $queryRpt->unionAll(
                $this->buildTranQuery("Depreciation written out to the RR", $arrIFRSCat, $arrColumn, $arrTranType)
            );
        }

        // Add line. Depreciation written out to Surplus/Deficit on the Provision of Services
        $arrColumn = array("ca_transaction.depn_wo");
        $arrTranType = array(CaTransactionType::REVALUATION);
        $this->setSumNegative(true);
        $this->setIncludeRRTrans(true);
        $this->setRRTrans("N");
        $queryRpt->unionAll(
            $this->buildTranQuery("Depreciation written out to Surplus/Deficit", $arrIFRSCat, $arrColumn, $arrTranType)
        );

        // Add line. Depreciation adjustment from reversal
        $arrColumn = array("ca_transaction.cv_depn_adjustment");
        $arrTranType = array(CaTransactionType::REVERSAL_OF_IMPAIRMENT, CaTransactionType::REVERSAL_OF_LOSS);
        $this->setSumNegative(false);
        $this->setIncludeRRTrans(false);
        $queryRpt->unionAll(
            $this->buildTranQuery("Depreciation adjustment from reversal", $arrIFRSCat, $arrColumn, $arrTranType)
        );

        // Add line. Impairment losses recognised in the RR
        $arrColumn = array(
            "(ca_transaction.cv_asset - ca_transaction.hc_asset)", "(ca_transaction.cv_land - ca_transaction.hc_land)"
        );
        $arrTranType = array(CaTransactionType::IMPAIRMENT);
        $this->setSumNegative(false);
        $this->setIncludeRRTrans(true);
        $this->setRRTrans("Y");
        $queryRpt->unionAll(
            $this->buildTranQuery("Impairment losses recognised in the RR", $arrIFRSCat, $arrColumn, $arrTranType)
        );

        // Add line. Impairments written out to the RR
        $arrColumn = array("ca_transaction.imp_wo_asset", "ca_transaction.imp_wo_land");
        $arrTranType = array(CaTransactionType::REVALUATION);
        $this->setSumNegative(true);
        $this->setIncludeRRTrans(true);
        $this->setRRTrans("Y");
        $queryRpt->unionAll(
            $this->buildTranQuery("Impairments written out to the RR", $arrIFRSCat, $arrColumn, $arrTranType)
        );

        // Add line. Impairments recognised in the Surplus/Deficit
        $arrColumn = array("ca_transaction.hc_asset", "ca_transaction.hc_land");
        $arrTranType = array(CaTransactionType::IMPAIRMENT);
        $this->setSumNegative(false);
        $this->setIncludeRRTrans(false);
        //$this->setRRTrans("N");
        $queryRpt->unionAll(
            $this->buildTranQuery("Impairments recognised in Surplus/Deficit", $arrIFRSCat, $arrColumn, $arrTranType)
        );

        // Add line. Impairments written out to Surplus/Deficit
        $arrColumn = array("ca_transaction.imp_wo_asset", "ca_transaction.imp_wo_land");
        $arrTranType = array(CaTransactionType::REVALUATION);
        $this->setSumNegative(true);
        $this->setIncludeRRTrans(true);
        $this->setRRTrans("N");
        $queryRpt->unionAll(
            $this->buildTranQuery("Impairments written out to Surplus/Deficit", $arrIFRSCat, $arrColumn, $arrTranType)
        );

        // Add line. Reversal of impairment to Surplus/Deficit
        // Modified to include ROI that are fully apportioned to S&D to be applied once agreed with MAW.
//        $arrColumn = array(
//            "(IF(ca_transaction.cv_asset = ca_transaction.gbv_change_asset, "
//            . "ca_transaction.cv_asset, ca_transaction.cv_asset - ca_transaction.gbv_change_asset))",
//            "(IF(ca_transaction.cv_land = ca_transaction.gbv_change_land, "
//            . "ca_transaction.cv_land, ca_transaction.cv_land - ca_transaction.gbv_change_land))"
//        );
        $arrColumn = array(
            "(ca_transaction.cv_asset - ca_transaction.gbv_change_asset)",
            "(ca_transaction.cv_land - ca_transaction.gbv_change_land)"
        );

        $arrTranType = array(CaTransactionType::REVERSAL_OF_IMPAIRMENT);
        $this->setSumNegative(true);
        $this->setIncludeRRTrans(false);
        $queryRpt->unionAll(
            $this->buildTranQuery("Reversals of Impairment to Surplus/Deficit", $arrIFRSCat, $arrColumn, $arrTranType)
        );

        // Add line. Derecognition - Disposal
        $arrColumn = array("ca_transaction.depn_wo", "ca_transaction.imp_wo_asset", "ca_transaction.imp_wo_land");
        $arrTranType = array(CaTransactionType::DISPOSAL);
        $this->setSumNegative(true);
        $this->setIncludeRRTrans(false);
        $queryRpt->unionAll($this->buildTranQuery("Derecognition - Disposals", $arrIFRSCat, $arrColumn, $arrTranType));

        // Add line. Derecognition - Other
        $arrColumn = array("ca_transaction.depn_wo", "ca_transaction.imp_wo_asset", "ca_transaction.imp_wo_land");
        $arrTranType = array(CaTransactionType::DERECOGNITION);
        $this->setSumNegative(true);
        $this->setIncludeRRTrans(false);
        $queryRpt->unionAll($this->buildTranQuery("Derecognition - other", $arrIFRSCat, $arrColumn, $arrTranType));

        // Add line. Reclassification and Transfers
        $arrColumn = array("ca_transaction.depn_wo", "ca_transaction.imp_wo_asset", "ca_transaction.imp_wo_land");
        $arrTranType = array(CaTransactionType::CATEGORY_TRANSFER);
        $this->setSumNegative(false);
        $this->setIncludeRRTrans(false);
        $queryRpt->unionAll(
            $this->buildTranQuery("Reclassification and Transfers", $arrIFRSCat, $arrColumn, $arrTranType)
        );

        // Add section 2 total line (build column totals ).
        $arrColumn = array(
            "ca_balance.cfwd_accum_depn",
            "ca_balance.cfwd_accum_imp_asset",
            "ca_balance.cfwd_accum_imp_land"
        );
        $rowDesc = "Balance as at " . $yearEndDate;
        $queryRpt->unionAll($this->buildBalanceQuery($rowDesc, $arrIFRSCat, $arrColumn));

        // *******************************************
        // SECTION 3 - NET BOOK VALUE
        // *******************************************

        // Add Net Book Value blank format row.
        $queryRpt->unionAll($this->addBlankRow($arrIFRSCat));

        // Add Net Book Value title row.
        $rowDesc = "Net Book Value";
        $queryRpt->unionAll($this->buildLabelRow($rowDesc, $arrIFRSCat));

        // Line - 'Balance as at 31 March 2011.
        $arrColumn = array("ca_balance.bfwd_nbv");
        $rowDesc = "Balance as at " . $yearStartDate;
        $queryRpt->unionAll($this->buildBalanceQuery($rowDesc, $arrIFRSCat, $arrColumn, true));

        // Line - 'Balance as at 31 March 2012.
        $arrColumn = array("ca_balance.cfwd_nbv");
        $rowDesc = "Balance as at " . $yearEndDate;
        $queryRpt->unionAll($this->buildBalanceQuery($rowDesc, $arrIFRSCat, $arrColumn));

        $this->setFilterTexts($whereCodes, $orCodes, $whereTexts);

        return \DB::table(null)->select('*')->from(\DB::raw("(" . $queryRpt->toSql() . ') AS report'));
    }

    private function setFilterTexts(
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        if ($val = Common::iset($this->filterData['deminimis_asset'])) {
            switch ($val) {
                case CommonConstant::ALL:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.deminimis_asset_all'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.deminimis_asset_no'));
                    break;
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.deminimis_asset_yes'));
                    break;
            }
        }

        if ($val = Common::iset($this->filterData['common_good'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.common_good_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.common_good_no'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.common_good_all'));
                    break;
            }
        }


        if ($val = Common::iset($this->filterData['investment'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.investment_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.investment_no'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.investment_all'));
                    break;
            }
        }
    }

    private function setIncludeRRTrans($bValue)
    {
        $this->bIncludeRRTrans = $bValue;
    }

    private function setRRTrans($value)
    {
        $this->rrTrans = $value;
    }

    private function setSumNegative($bValue)
    {
        $this->bSumNegative = $bValue;
    }

    /**
     * Returns the sql to add a blank row into a csv report.
     * @param array $arrIFRSCat
     * @return string
     */
    protected function addBlankRow(array $arrIFRSCat)
    {
        // First column is the row description label.
        $query =  "' ' `Element`";

        // Add columns for each IFRS category
        foreach ($arrIFRSCat as $key => $value) {
            $query .= " , ' ' ";
        }

        // Calculate row total.
        $query .= " , '' `total` ";

        return \DB::table(null)->select(\DB::raw($query));
    }

    private function getIFRSCategories($bIncludeDesc = false)
    {
        $arrIFRSCat = array();   // Holds all IFRS categories the report should include.

        // Get available ifrs categories to allow the report query to be built dynamically
        $query = CaIfrsCategory::userSiteGroup()
        ->select(
            [
                'ca_ifrs_category_code',
                'ca_ifrs_category_desc',
                'ca_ifrs_category_id'
            ]
        )
        ->where(
            'ca_ifrs_category.active',
            '=',
            CommonConstant::DATABASE_VALUE_YES
        );

        if ($this->commonGood) {
            $query->where('common_good', '=', $this->commonGood);
        }

        if ($this->investment) {
            $query->where('investment', '=', $this->investment);
        }

        $query->orderBy('investment');
        $query->orderBy('depreciable', 'desc');

        // RB 04/11/2019 This can cause the headings to be out of sync with te data Ordering should be the same.
        //if ($bIncludeDesc) {
        //    $query->orderBy(\DB::raw('CONCAT(ca_ifrs_category_code, ca_ifrs_category_desc)'));
        //} else {
        $query->orderBy('ca_ifrs_category_code');
        //}

        $queryIFRS = $query->get();

        // Loop through all ifrs categories and store in array.
        foreach ($queryIFRS as $row) {
            $bDesc = '';
            if ($bIncludeDesc) {
                $bDesc = Common::concatFields([$row['ca_ifrs_category_code'], $row['ca_ifrs_category_desc']]);
            } else {
                $bDesc = $row['ca_ifrs_category_code'];
            }
            $arrIFRSCat[$row['ca_ifrs_category_id']] = $bDesc;
        }

        return $arrIFRSCat;
    }

    private function reportHeader($arrIFRSCatDesc)
    {
        $query = " '' `Element`";
        foreach ($arrIFRSCatDesc as $key => $codeDesc) {
            $query .= " , '' `$codeDesc`";
        }
        $query .= ", '' Totals ";

        return \DB::table(null)->select(\DB::raw($query));
    }

    private function buildLabelRow($rowDesc, array $arrIFRSCat)
    {
        // First column is the row description label.
        $query =  "'$rowDesc' `Element`";

        foreach ($arrIFRSCat as $key => $code) {
            $query .= " , '' `$code`";
        }
        $query .= ", '' Totals ";

        return \DB::table(null)->select(\DB::raw($query));
    }

    private function buildBalanceQuery($rowDesc, array $arrIFRSCat, array $arrColumn, $bUseBfwdIFRS = false)
    {
        // Get columns as a  string.
        $columns = implode(" + ", $arrColumn);

        // First column is the row description label.
        $query =  "'$rowDesc' `Element`";

        // Add columns for each IFRS category
        foreach ($arrIFRSCat as $key => $value) {
            $query .= " , ";
            $query .= " (SELECT IFNULL(";
            $query .= " (SELECT ROUND(IFNULL(SUM($columns), 0), 0) ";
            $query .= "  FROM ca_ifrs_category ";
            if ($bUseBfwdIFRS) {
                $query .= " INNER JOIN ca_fixed_asset ON
                    ca_fixed_asset.bfwd_ifrs_cat_id = ca_ifrs_category.ca_ifrs_category_id ";
            } else {
                $query .= " INNER JOIN ca_fixed_asset ON
                    ca_fixed_asset.ca_ifrs_cat_id = ca_ifrs_category.ca_ifrs_category_id ";
            }
            $query .= " INNER JOIN ca_balance ON ca_balance.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
            $query .= " LEFT JOIN vw_far_disposed ON
                   vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
            $query .= " INNER JOIN vw_ca_current_fin_year ON
                vw_ca_current_fin_year.ca_fin_year_id = ca_balance.ca_fin_year_id ";
            $query .= "  WHERE ca_ifrs_category.site_group_id = '{$this->siteGroupId}' ";
            $query .= " AND ca_ifrs_category.active = 'Y' ";
            $query .= " AND ca_balance.historic = 'N' ";
            if ($this->deminimis) {
                $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
            }
            if ($this->entity) {
                $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
            }
            $query .= " AND ca_fixed_asset.active = 'Y'";
            $query .= " AND ca_ifrs_category.ca_ifrs_category_id = " . $key;
            $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
            $query .= ") , 0)) ";
        }

        // Calculate row total.
        $query .= " , ";
        $query .= " (SELECT IFNULL(";
        $query .= " (SELECT ROUND(IFNULL(SUM($columns), 0), 0) ";
        $query .= "  FROM ca_ifrs_category ";
        if ($bUseBfwdIFRS) {
            $query .= " INNER JOIN ca_fixed_asset ON
                ca_fixed_asset.bfwd_ifrs_cat_id = ca_ifrs_category.ca_ifrs_category_id ";
        } else {
            $query .= " INNER JOIN ca_fixed_asset ON
                ca_fixed_asset.ca_ifrs_cat_id = ca_ifrs_category.ca_ifrs_category_id ";
        }
        $query .= " INNER JOIN ca_balance ON ca_balance.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
        $query .= " INNER JOIN vw_ca_current_fin_year ON
            vw_ca_current_fin_year.ca_fin_year_id = ca_balance.ca_fin_year_id ";
        $query .= " LEFT JOIN vw_far_disposed ON
               vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
        $query .= " WHERE ca_ifrs_category.site_group_id = '{$this->siteGroupId}' ";
        $query .= "     AND ca_ifrs_category.active = 'Y' ";
        if ($this->deminimis) {
            $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
        }
        if ($this->entity) {
            $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
        }
        $query .= "     AND ca_fixed_asset.active = 'Y'";
        if ($this->commonGood) {
            $query .= " AND ca_ifrs_category.common_good = '{$this->commonGood}'";
        }
        if ($this->investment) {
            $query .= " AND ca_ifrs_category.investment = '{$this->investment}'";
        }
        $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
        $query .= "     AND ca_balance.historic = 'N' ),0)) `total` ";

        return \DB::table(null)->select(\DB::raw($query));
    }

    private function buildTranQuery($rowDesc, array $arrIFRSCat, array $arrColumn, array $arrTranTypes)
    {
        // Get columns and transactions types as a comma separated string.
        $columns = implode(" + ", $arrColumn);
        $tranTypes = implode(",", $arrTranTypes);

        // First column is the row description label.
        $query =  "'$rowDesc' `Element`";

        // Add columns for each IFRS category
        foreach ($arrIFRSCat as $key => $value) {
            $query .= ", ";
            $query .= " ROUND(IFNULL((SELECT SUM($columns) " . ($this->bSumNegative ? " * -1 " : "");
            $query .= " FROM ca_transaction ";
            $query .= " INNER JOIN vw_ca_current_fin_year ON
                vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
            $query .= " INNER JOIN ca_fixed_asset ON
                ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
            $query .= " LEFT JOIN vw_far_disposed ON
                   vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
            $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId'";
            $query .= " AND ca_transaction_type_id IN ($tranTypes)";
            $query .= " AND ca_transaction.ca_ifrs_cat_id = $key";
            if ($this->deminimis) {
                $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
            }
            if ($this->entity) {
                $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
            }
            $query .= " AND ca_fixed_asset.active = 'Y'";
            if ($this->bIncludeRRTrans) {
                $query .= " AND ca_transaction.rr_trans = '{$this->rrTrans}'";
            }
            $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
            $query .= "),0),0)";
            $query .= " `$value`";
        }

        // Add row total column
        $query .= " , ";
        $query .= " ROUND(IFNULL((SELECT SUM($columns) " . ($this->bSumNegative ? " * -1 " : "");
        $query .= " FROM ca_transaction ";
        $query .= " INNER JOIN ca_ifrs_category ON
            ca_ifrs_category.ca_ifrs_category_id = ca_transaction.ca_ifrs_cat_id ";
        $query .= " INNER JOIN vw_ca_current_fin_year ON
            vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
        $query .= " INNER JOIN ca_fixed_asset ON ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
        $query .= " LEFT JOIN vw_far_disposed ON
               vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
        $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
        $query .= " AND ca_ifrs_category.active = 'Y'";
        $query .= " AND ca_transaction_type_id IN ($tranTypes)";
        if ($this->deminimis) {
            $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
        }
        if ($this->entity) {
            $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
        }
        $query .= " AND ca_fixed_asset.active = 'Y'";
        if ($this->bIncludeRRTrans) {
            $query .= " AND ca_transaction.rr_trans = '{$this->rrTrans}'";
        }
        if ($this->commonGood) {
            $query .= " AND ca_ifrs_category.common_good = '{$this->commonGood}'";
        }
        if ($this->investment) {
            $query .= " AND ca_ifrs_category.investment = '{$this->investment}'";
        }
        $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
        $query .= "),0),0)";
        $query .= " `Total`";

        return \DB::table(null)->select(\DB::raw($query));
    }

    private function buildRevaluationToRR($rowDesc, array $arrIFRSCat)
    {
        $query =  "'$rowDesc' `Element`";

        foreach ($arrIFRSCat as $id => $code) {
            $query .= ", ";
            $query .= " ROUND(";
            $query .= " (SELECT IFNULL(SUM( ";

            $query .= " if(`ca_transaction`.`rr_trans` = 'Y',";
            $query .= "    if (ca_transaction.hc_asset = 0 && ca_transaction.hc_land = 0";
            $query .= "       , ca_transaction.gbv_change_asset + ca_transaction.gbv_change_land";
            $query .= "       ,((`ca_transaction`.`cv_asset` + `ca_transaction`.`cv_land`) -
                (`ca_transaction`.`hc_asset` + `ca_transaction`.`hc_land`)) -
                `ca_transaction`.`depn_wo` - `ca_transaction`.`imp_wo_asset` - `ca_transaction`.`imp_wo_land`)";
            $query .= "  ,0)";
            $query .= " ), 0)";

            $query .= " FROM ca_transaction ";
            $query .= " INNER JOIN vw_ca_current_fin_year ON
                vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
            $query .= " INNER JOIN ca_fixed_asset ON
                ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
            $query .= " LEFT JOIN vw_far_disposed ON
                   vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
            $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
            $query .= " AND ca_transaction_type_id = " . CaTransactionType::REVALUATION;
            $query .= " AND ca_transaction.ca_ifrs_cat_id = " . $id;
            if ($this->deminimis) {
                $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
            }
            if ($this->entity) {
                $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
            }
            $query .= " AND ca_fixed_asset.active = 'Y'";
            $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
            $query .= " )";

            $query .= ", 0)";
            $query .= " `" . $code . "`";
        }

        // Add row total column
        $query .= " , ";
        $query .= " ROUND(";
        $query .= " (SELECT IFNULL(SUM( ";

        $query .= " if(`ca_transaction`.`rr_trans` = 'Y',";
        $query .= "    if (ca_transaction.hc_asset = 0 && ca_transaction.hc_land = 0";
        $query .= "       , ca_transaction.gbv_change_asset + ca_transaction.gbv_change_land";
        $query .= "       ,((`ca_transaction`.`cv_asset` + `ca_transaction`.`cv_land`) -
            (`ca_transaction`.`hc_asset` + `ca_transaction`.`hc_land`)) -
            `ca_transaction`.`depn_wo` - `ca_transaction`.`imp_wo_asset` - `ca_transaction`.`imp_wo_land`)";
        $query .= "  ,0)";

        $query .= " ), 0)";
        $query .= " FROM ca_transaction ";
        $query .= " INNER JOIN vw_ca_current_fin_year ON
            vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
        $query .= " INNER JOIN ca_fixed_asset ON
            ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
        $query .= " LEFT JOIN vw_far_disposed ON
               vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
        $query .= " INNER JOIN ca_ifrs_category ON
            ca_ifrs_category.ca_ifrs_category_id = ca_transaction.ca_ifrs_cat_id ";
        $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
        $query .= "    AND ca_transaction_type_id = " . CaTransactionType::REVALUATION;
        if ($this->deminimis) {
            $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
        }
        if ($this->entity) {
            $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
        }
        $query .= "    AND ca_fixed_asset.active = 'Y'";
        if ($this->commonGood) {
            $query .= " AND ca_ifrs_category.common_good = '{$this->commonGood}'";
        }
        if ($this->investment) {
            $query .= " AND ca_ifrs_category.investment = '{$this->investment}'";
        }
        $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
        $query .= ")";
        $query .= ", 0)";
        $query .= " `Total`";

        return \DB::table(null)->select(\DB::raw($query));
    }

    private function buildRevaluationGainsToRR($rowDesc, array $arrIFRSCat)
    {
        $query =  "'$rowDesc' `Element`";

        foreach ($arrIFRSCat as $id => $code) {
            $query .= ", ";
            $query .= " ROUND(";
            $query .= " (SELECT IFNULL(SUM( ";

            $query .= " if(`ca_transaction`.`rr_trans` = 'Y',";
            $query .= "    if (ca_transaction.hc_asset = 0 && ca_transaction.hc_land = 0";
            $query .= "       , if(ca_transaction.gbv_change_asset + ca_transaction.gbv_change_land > 0, ";
            $query .= "         ca_transaction.gbv_change_asset + ca_transaction.gbv_change_land, 0)";
            $query .= "       , if(((`ca_transaction`.`cv_asset` + `ca_transaction`.`cv_land`) - ";
            $query .= "         (`ca_transaction`.`hc_asset` + `ca_transaction`.`hc_land`)) - ";
            $query .= "         `ca_transaction`.`depn_wo` - `ca_transaction`.`imp_wo_asset` -  ";
            $query .= "         `ca_transaction`.`imp_wo_land` > 0, ((`ca_transaction`.`cv_asset` + ";
            $query .= "         `ca_transaction`.`cv_land`) - (`ca_transaction`.`hc_asset` + ";
            $query .= "         `ca_transaction`.`hc_land`)) - `ca_transaction`.`depn_wo` - ";
            $query .= "         `ca_transaction`.`imp_wo_asset` -  `ca_transaction`.`imp_wo_land`, 0))";
            $query .= "  ,0)";
            $query .= " ), 0)";

            $query .= " FROM ca_transaction ";
            $query .= " INNER JOIN vw_ca_current_fin_year ON
                vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
            $query .= " INNER JOIN ca_fixed_asset ON
                ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
            $query .= " LEFT JOIN vw_far_disposed ON
                   vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
            $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
            $query .= " AND ca_transaction_type_id = " . CaTransactionType::REVALUATION;
            $query .= " AND ca_transaction.ca_ifrs_cat_id = " . $id;
            if ($this->deminimis) {
                $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
            }
            if ($this->entity) {
                $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
            }
            $query .= " AND ca_fixed_asset.active = 'Y'";
            $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
            $query .= " )";

            $query .= ", 0)";
            $query .= " `" . $code . "`";
        }

        // Add row total column
        $query .= " , ";
        $query .= " ROUND(";
        $query .= " (SELECT IFNULL(SUM( ";

        $query .= " if(`ca_transaction`.`rr_trans` = 'Y',";
        $query .= "    if (ca_transaction.hc_asset = 0 && ca_transaction.hc_land = 0";
        $query .= "       , if(ca_transaction.gbv_change_asset + ca_transaction.gbv_change_land > 0, ";
        $query .= "         ca_transaction.gbv_change_asset + ca_transaction.gbv_change_land, 0)";
        $query .= "       , if(((`ca_transaction`.`cv_asset` + `ca_transaction`.`cv_land`) - ";
        $query .= "         (`ca_transaction`.`hc_asset` + `ca_transaction`.`hc_land`)) - ";
        $query .= "         `ca_transaction`.`depn_wo` - `ca_transaction`.`imp_wo_asset` -  ";
        $query .= "         `ca_transaction`.`imp_wo_land` > 0, ((`ca_transaction`.`cv_asset` + ";
        $query .= "         `ca_transaction`.`cv_land`) - (`ca_transaction`.`hc_asset` + ";
        $query .= "         `ca_transaction`.`hc_land`)) - `ca_transaction`.`depn_wo` - ";
        $query .= "         `ca_transaction`.`imp_wo_asset` -  `ca_transaction`.`imp_wo_land`, 0))";
        $query .= "  ,0)";
        $query .= " ), 0)";
        $query .= " FROM ca_transaction ";
        $query .= " INNER JOIN vw_ca_current_fin_year ON
            vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
        $query .= " INNER JOIN ca_fixed_asset ON
            ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
        $query .= " LEFT JOIN vw_far_disposed ON
               vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
        $query .= " INNER JOIN ca_ifrs_category ON
            ca_ifrs_category.ca_ifrs_category_id = ca_transaction.ca_ifrs_cat_id ";
        $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
        $query .= "    AND ca_transaction_type_id = " . CaTransactionType::REVALUATION;
        if ($this->deminimis) {
            $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
        }
        if ($this->entity) {
            $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
        }
        $query .= "    AND ca_fixed_asset.active = 'Y'";
        if ($this->commonGood) {
            $query .= " AND ca_ifrs_category.common_good = '{$this->commonGood}'";
        }
        if ($this->investment) {
            $query .= " AND ca_ifrs_category.investment = '{$this->investment}'";
        }
        $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
        $query .= ")";
        $query .= ", 0)";
        $query .= " `Total`";

        return \DB::table(null)->select(\DB::raw($query));
    }

    private function buildRevaluationLossesToRR($rowDesc, array $arrIFRSCat)
    {
        $query =  "'$rowDesc' `Element`";

        foreach ($arrIFRSCat as $id => $code) {
            $query .= ", ";
            $query .= " ROUND(";
            $query .= " (SELECT IFNULL(SUM( ";

            $query .= " if(`ca_transaction`.`rr_trans` = 'Y',";
            $query .= "    if (ca_transaction.hc_asset = 0 && ca_transaction.hc_land = 0";
            $query .= "       , if(ca_transaction.gbv_change_asset + ca_transaction.gbv_change_land < 0, ";
            $query .= "         ca_transaction.gbv_change_asset + ca_transaction.gbv_change_land, 0)";
            $query .= "       , if(((`ca_transaction`.`cv_asset` + `ca_transaction`.`cv_land`) - ";
            $query .= "         (`ca_transaction`.`hc_asset` + `ca_transaction`.`hc_land`)) - ";
            $query .= "         `ca_transaction`.`depn_wo` - `ca_transaction`.`imp_wo_asset` -  ";
            $query .= "         `ca_transaction`.`imp_wo_land` < 0, ((`ca_transaction`.`cv_asset` + ";
            $query .= "         `ca_transaction`.`cv_land`) - (`ca_transaction`.`hc_asset` + ";
            $query .= "         `ca_transaction`.`hc_land`)) - `ca_transaction`.`depn_wo` - ";
            $query .= "         `ca_transaction`.`imp_wo_asset` -  `ca_transaction`.`imp_wo_land`, 0))";
            $query .= "  ,0)";
            $query .= " ), 0)";

            $query .= " FROM ca_transaction ";
            $query .= " INNER JOIN vw_ca_current_fin_year ON
                vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
            $query .= " INNER JOIN ca_fixed_asset ON
                ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
            $query .= " LEFT JOIN vw_far_disposed ON
                   vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
            $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
            $query .= " AND ca_transaction_type_id = " . CaTransactionType::REVALUATION;
            $query .= " AND ca_transaction.ca_ifrs_cat_id = " . $id;
            if ($this->deminimis) {
                $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
            }
            if ($this->entity) {
                $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
            }
            $query .= " AND ca_fixed_asset.active = 'Y'";
            $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
            $query .= " )";

            $query .= ", 0)";
            $query .= " `" . $code . "`";
        }

        // Add row total column
        $query .= " , ";
        $query .= " ROUND(";
        $query .= " (SELECT IFNULL(SUM( ";

        $query .= " if(`ca_transaction`.`rr_trans` = 'Y',";
        $query .= "    if (ca_transaction.hc_asset = 0 && ca_transaction.hc_land = 0";
        $query .= "       , if(ca_transaction.gbv_change_asset + ca_transaction.gbv_change_land < 0, ";
        $query .= "         ca_transaction.gbv_change_asset + ca_transaction.gbv_change_land, 0)";
        $query .= "       , if(((`ca_transaction`.`cv_asset` + `ca_transaction`.`cv_land`) - ";
        $query .= "         (`ca_transaction`.`hc_asset` + `ca_transaction`.`hc_land`)) - ";
        $query .= "         `ca_transaction`.`depn_wo` - `ca_transaction`.`imp_wo_asset` -  ";
        $query .= "         `ca_transaction`.`imp_wo_land` < 0, ((`ca_transaction`.`cv_asset` + ";
        $query .= "         `ca_transaction`.`cv_land`) - (`ca_transaction`.`hc_asset` + ";
        $query .= "         `ca_transaction`.`hc_land`)) - `ca_transaction`.`depn_wo` - ";
        $query .= "         `ca_transaction`.`imp_wo_asset` -  `ca_transaction`.`imp_wo_land`, 0))";
        $query .= "  ,0)";
        $query .= " ), 0)";
        $query .= " FROM ca_transaction ";
        $query .= " INNER JOIN vw_ca_current_fin_year ON
            vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
        $query .= " INNER JOIN ca_fixed_asset ON
            ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
        $query .= " LEFT JOIN vw_far_disposed ON
               vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
        $query .= " INNER JOIN ca_ifrs_category ON
            ca_ifrs_category.ca_ifrs_category_id = ca_transaction.ca_ifrs_cat_id ";
        $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
        $query .= "    AND ca_transaction_type_id = " . CaTransactionType::REVALUATION;
        if ($this->deminimis) {
            $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
        }
        if ($this->entity) {
            $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
        }
        $query .= "    AND ca_fixed_asset.active = 'Y'";
        if ($this->commonGood) {
            $query .= " AND ca_ifrs_category.common_good = '{$this->commonGood}'";
        }
        if ($this->investment) {
            $query .= " AND ca_ifrs_category.investment = '{$this->investment}'";
        }
        $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
        $query .= ")";
        $query .= ", 0)";
        $query .= " `Total`";

        return \DB::table(null)->select(\DB::raw($query));
    }

    private function buildRevaluationAndReversalOfLossToSurplusDeficit($rowDesc, array $arrIFRSCat)
    {
        $query =  "'$rowDesc' `Element`";

        foreach ($arrIFRSCat as $id => $code) {
            $query .= ", ";
            $query .= " ROUND(";
            $query .= " (SELECT IFNULL(SUM( ";

            $query .= "  if(`ca_transaction`.`rr_trans` = 'Y', ";
            $query .= "     if(ca_transaction.hc_asset = 0 && ca_transaction.hc_land = 0 ";
            $query .= "        , 0 ";
            $query .= "        , ca_transaction.hc_asset + ca_transaction.hc_land)";
            $query .= "     ,(`ca_transaction`.`gbv_change_asset` + `ca_transaction`.`gbv_change_land`))";

            $query .= " ), 0)";

            $query .= " FROM ca_transaction ";
            $query .= "    INNER JOIN vw_ca_current_fin_year ON
                vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
            $query .= "    INNER JOIN ca_fixed_asset ON
                ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
            $query .= " LEFT JOIN vw_far_disposed ON
                   vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
            $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
            $query .= "    AND ca_transaction_type_id = " . CaTransactionType::REVALUATION;
            $query .= "    AND ca_transaction.ca_ifrs_cat_id = " . $id;
            if ($this->deminimis) {
                $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
            }
            if ($this->entity) {
                $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
            }
            $query .= "    AND ca_fixed_asset.active = 'Y'";
            $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
            $query .= " )";
            $query .= " + ";
            $query .= " (SELECT IFNULL(SUM(ca_transaction.gbv_change_asset + ca_transaction.gbv_change_land), 0) ";
            $query .= " FROM ca_transaction ";
            $query .= "    INNER JOIN vw_ca_current_fin_year ON
                vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
            $query .= "    INNER JOIN ca_fixed_asset ON
                ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
            $query .= " LEFT JOIN vw_far_disposed ON
                   vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
            $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
            $query .= "    AND ca_transaction_type_id IN
                (" . CaTransactionType::REVERSAL_OF_IMPAIRMENT . "," . CaTransactionType::REVERSAL_OF_LOSS . ")";
            $query .= "    AND ca_transaction.ca_ifrs_cat_id = " . $id;
            if ($this->deminimis) {
                $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
            }
            if ($this->entity) {
                $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
            }
            $query .= "    AND ca_fixed_asset.active = 'Y'";
            $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
            $query .= ")";

            $query .= ", 0)";
            $query .= " `" . $code . "`";
        }
        // Add row total
        $query .= " , ";
        $query .= " ROUND(";
        $query .= " (SELECT IFNULL(SUM( ";

        $query .= "  if(`ca_transaction`.`rr_trans` = 'Y', ";
        $query .= "     if(ca_transaction.hc_asset = 0 && ca_transaction.hc_land = 0 ";
        $query .= "        , 0 ";
        $query .= "        , ca_transaction.hc_asset + ca_transaction.hc_land)";
        $query .= "     ,(`ca_transaction`.`gbv_change_asset` + `ca_transaction`.`gbv_change_land`))";

        $query .= " ), 0)";
        $query .= " FROM ca_transaction ";
        $query .= "    INNER JOIN ca_ifrs_category ON
            ca_ifrs_category.ca_ifrs_category_id = ca_transaction.ca_ifrs_cat_id ";
        $query .= "    INNER JOIN vw_ca_current_fin_year ON
            vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
        $query .= "    INNER JOIN ca_fixed_asset ON
            ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
        $query .= " LEFT JOIN vw_far_disposed ON
               vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
        $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
        $query .= "    AND ca_transaction_type_id = " . CaTransactionType::REVALUATION;
        $query .= "    AND ca_ifrs_category.active = 'Y'";
        if ($this->deminimis) {
            $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
        }
        if ($this->entity) {
            $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
        }
        $query .= "    AND ca_fixed_asset.active = 'Y'";
        if ($this->commonGood) {
            $query .= " AND ca_ifrs_category.common_good = '{$this->commonGood}'";
        }
        if ($this->investment) {
            $query .= " AND ca_ifrs_category.investment = '{$this->investment}'";
        }
        $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
        $query .= " )";
        $query .= " + ";
        $query .= " (SELECT IFNULL(SUM(ca_transaction.gbv_change_asset + ca_transaction.gbv_change_land), 0) ";
        $query .= " FROM ca_transaction ";
        $query .= "    INNER JOIN ca_ifrs_category ON
            ca_ifrs_category.ca_ifrs_category_id = ca_transaction.ca_ifrs_cat_id ";
        $query .= "    INNER JOIN vw_ca_current_fin_year ON
            vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
        $query .= "    INNER JOIN ca_fixed_asset ON
            ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
        $query .= " LEFT JOIN vw_far_disposed ON
               vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
        $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
        $query .= "    AND ca_ifrs_category.active = 'Y'";
        if ($this->deminimis) {
            $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
        }
        if ($this->entity) {
            $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
        }
        $query .= "    AND ca_fixed_asset.active = 'Y'";
        $query .= "    AND ca_transaction_type_id IN
            (" . CaTransactionType::REVERSAL_OF_IMPAIRMENT . "," . CaTransactionType::REVERSAL_OF_LOSS . ")";
        if ($this->commonGood) {
            $query .= " AND ca_ifrs_category.common_good = '{$this->commonGood}'";
        }
        if ($this->investment) {
            $query .= " AND ca_ifrs_category.investment = '{$this->investment}'";
        }
        $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
        $query .= ")";
        $query .= ", 0)";
        $query .= " `Total`";

        return \DB::table(null)->select(\DB::raw($query));
    }

    private function buildRevaluationToSurplusDeficit($rowDesc, array $arrIFRSCat)
    {
        $query =  "'$rowDesc' `Element`";

        foreach ($arrIFRSCat as $id => $code) {
            $query .= ", ";
            $query .= " ROUND(";
            $query .= " (SELECT IFNULL(SUM( ";

            $query .= "  if(`ca_transaction`.`rr_trans` = 'Y', ";
            $query .= "     if(ca_transaction.hc_asset = 0 && ca_transaction.hc_land = 0 ";
            $query .= "        , 0 ";
            $query .= "        , ca_transaction.hc_asset + ca_transaction.hc_land)";
            $query .= "     ,(`ca_transaction`.`gbv_change_asset` + `ca_transaction`.`gbv_change_land`))";

            $query .= " ), 0)";

            $query .= " FROM ca_transaction ";
            $query .= "    INNER JOIN vw_ca_current_fin_year ON
                vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
            $query .= "    INNER JOIN ca_fixed_asset ON
                ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
            $query .= " LEFT JOIN vw_far_disposed ON
                   vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
            $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
            $query .= "    AND ca_transaction_type_id = " . CaTransactionType::REVALUATION;
            $query .= "    AND ca_transaction.ca_ifrs_cat_id = " . $id;
            if ($this->deminimis) {
                $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
            }
            if ($this->entity) {
                $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
            }
            $query .= "    AND ca_fixed_asset.active = 'Y'";
            $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
            $query .= " )";
            $query .= ", 0)";
            $query .= " `" . $code . "`";
        }
        // Add row total
        $query .= " , ";
        $query .= " ROUND(";
        $query .= " (SELECT IFNULL(SUM( ";

        $query .= "  if(`ca_transaction`.`rr_trans` = 'Y', ";
        $query .= "     if(ca_transaction.hc_asset = 0 && ca_transaction.hc_land = 0 ";
        $query .= "        , 0 ";
        $query .= "        , ca_transaction.hc_asset + ca_transaction.hc_land)";
        $query .= "     ,(`ca_transaction`.`gbv_change_asset` + `ca_transaction`.`gbv_change_land`))";

        $query .= " ), 0)";
        $query .= " FROM ca_transaction ";
        $query .= "    INNER JOIN ca_ifrs_category ON
            ca_ifrs_category.ca_ifrs_category_id = ca_transaction.ca_ifrs_cat_id ";
        $query .= "    INNER JOIN vw_ca_current_fin_year ON
            vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
        $query .= "    INNER JOIN ca_fixed_asset ON
            ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
        $query .= " LEFT JOIN vw_far_disposed ON
               vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
        $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
        $query .= "    AND ca_transaction_type_id = " . CaTransactionType::REVALUATION;
        $query .= "    AND ca_ifrs_category.active = 'Y'";
        if ($this->deminimis) {
            $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
        }
        if ($this->entity) {
            $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
        }
        $query .= "    AND ca_fixed_asset.active = 'Y'";
        if ($this->commonGood) {
            $query .= " AND ca_ifrs_category.common_good = '{$this->commonGood}'";
        }
        if ($this->investment) {
            $query .= " AND ca_ifrs_category.investment = '{$this->investment}'";
        }
        $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
        $query .= " )";
        $query .= ", 0)";
        $query .= " `Total`";

        return \DB::table(null)->select(\DB::raw($query));
    }

    private function buildReversalOfLossToSurplusDeficit($rowDesc, array $arrIFRSCat)
    {
        $query =  "'$rowDesc' `Element`";

        foreach ($arrIFRSCat as $id => $code) {
            $query .= ", ";
            $query .= " ROUND(";
            $query .= " (SELECT IFNULL(SUM(ca_transaction.gbv_change_asset + ca_transaction.gbv_change_land), 0) ";
            $query .= " FROM ca_transaction ";
            $query .= "    INNER JOIN vw_ca_current_fin_year ON
                vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
            $query .= "    INNER JOIN ca_fixed_asset ON
                ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
            $query .= " LEFT JOIN vw_far_disposed ON
                   vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
            $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
            $query .= "    AND ca_transaction_type_id IN
                (" . CaTransactionType::REVERSAL_OF_IMPAIRMENT . "," . CaTransactionType::REVERSAL_OF_LOSS . ")";
            $query .= "    AND ca_transaction.ca_ifrs_cat_id = " . $id;
            if ($this->deminimis) {
                $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
            }
            if ($this->entity) {
                $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
            }
            $query .= "    AND ca_fixed_asset.active = 'Y'";
            $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
            $query .= ")";
            $query .= ", 0)";
            $query .= " `" . $code . "`";
        }
        // Add row total
        $query .= " , ";
        $query .= " ROUND(";
        $query .= " (SELECT IFNULL(SUM(ca_transaction.gbv_change_asset + ca_transaction.gbv_change_land), 0) ";
        $query .= " FROM ca_transaction ";
        $query .= "    INNER JOIN ca_ifrs_category ON
            ca_ifrs_category.ca_ifrs_category_id = ca_transaction.ca_ifrs_cat_id ";
        $query .= "    INNER JOIN vw_ca_current_fin_year ON
            vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
        $query .= "    INNER JOIN ca_fixed_asset ON
            ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
        $query .= " LEFT JOIN vw_far_disposed ON
               vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
        $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
        $query .= "    AND ca_ifrs_category.active = 'Y'";
        if ($this->deminimis) {
            $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
        }
        if ($this->entity) {
            $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
        }
        $query .= "    AND ca_fixed_asset.active = 'Y'";
        $query .= "    AND ca_transaction_type_id IN
            (" . CaTransactionType::REVERSAL_OF_IMPAIRMENT . "," . CaTransactionType::REVERSAL_OF_LOSS . ")";
        if ($this->commonGood) {
            $query .= " AND ca_ifrs_category.common_good = '{$this->commonGood}'";
        }
        if ($this->investment) {
            $query .= " AND ca_ifrs_category.investment = '{$this->investment}'";
        }
        $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
        $query .= ")";
        $query .= ", 0)";
        $query .= " `Total`";

        return \DB::table(null)->select(\DB::raw($query));
    }

    private function buildDepreciationRR($rowDesc, array $arrIFRSCat, $gains)
    {
        // First column is the row description label.
        $query =  "'$rowDesc' `Element`";

        // set the compare operator to select for gains (positive) or losses (negative)
        $op = ($gains) ? "<" : ">";

        // Add columns for each IFRS category
        foreach ($arrIFRSCat as $key => $value) {
            $query .= ", ";
            $query .= " ROUND(IFNULL((SELECT SUM(if(ca_transaction.depn_wo " . $op . " 0, ca_transaction.depn_wo, 0))";
            $query .= "      * -1 ";
            $query .= " FROM ca_transaction ";
            $query .= " INNER JOIN vw_ca_current_fin_year ON
                vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
            $query .= " INNER JOIN ca_fixed_asset ON
                ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
            $query .= " LEFT JOIN vw_far_disposed ON
                   vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
            $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId'";
            $query .= " AND ca_transaction_type_id = " . CaTransactionType::REVALUATION;
            $query .= " AND ca_transaction.ca_ifrs_cat_id = $key";
            if ($this->deminimis) {
                $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
            }
            if ($this->entity) {
                $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
            }
            $query .= " AND ca_fixed_asset.active = 'Y'";
            $query .= " AND ca_transaction.rr_trans = 'Y'";
            $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
            $query .= "),0),0)";
            $query .= " `$value`";
        }

        // Add row total column
        $query .= " , ";
        $query .= " ROUND(IFNULL((SELECT SUM(if(ca_transaction.depn_wo " . $op . " 0, ca_transaction.depn_wo, 0))";
        $query .= "      * -1 ";
        $query .= " FROM ca_transaction ";
        $query .= " INNER JOIN ca_ifrs_category ON
            ca_ifrs_category.ca_ifrs_category_id = ca_transaction.ca_ifrs_cat_id ";
        $query .= " INNER JOIN vw_ca_current_fin_year ON
            vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
        $query .= " INNER JOIN ca_fixed_asset ON ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
        $query .= " LEFT JOIN vw_far_disposed ON
               vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
        $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
        $query .= " AND ca_ifrs_category.active = 'Y'";
        $query .= " AND ca_transaction_type_id = " . CaTransactionType::REVALUATION;
        if ($this->deminimis) {
            $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
        }
        if ($this->entity) {
            $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
        }
        $query .= " AND ca_fixed_asset.active = 'Y'";
        $query .= " AND ca_transaction.rr_trans = 'Y'";
        if ($this->commonGood) {
            $query .= " AND ca_ifrs_category.common_good = '{$this->commonGood}'";
        }
        if ($this->investment) {
            $query .= " AND ca_ifrs_category.investment = '{$this->investment}'";
        }
        $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
        $query .= "),0),0)";
        $query .= " `Total`";

        return \DB::table(null)->select(\DB::raw($query));
    }
}
