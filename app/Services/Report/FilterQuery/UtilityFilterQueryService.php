<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\UserDefinedService;
use Tfcloud\Services\Utility\UtilityBillService;
use Tfcloud\Services\Utility\UtilityService;

class UtilityFilterQueryService extends BaseService
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService, UserDefinedService $userDefinedService)
    {
        $this->permissionService = $permissionService;
        $this->userDefinedService = $userDefinedService;
    }

    public function reAddUtilityRecordQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $utilityService = new UtilityService($this->permissionService, $this->userDefinedService);
        $query = $utilityService->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['account'])) {
            array_push($whereCodes, ['fin_account', 'fin_account_id', 'fin_account_code', $val, 'Account']);
        }

        if ($val = Common::iset($filterData['meterNo'])) {
            array_push($whereTexts, "Meter No. contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['mpan_mprn'])) {
            array_push($whereTexts, "MPAN / MPRN contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['accountNo'])) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.utilities.account_number'), $val));
        }

        if ($val = Common::iset($filterData['amr'])) {
            array_push($whereTexts, "AMR contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['type'])) {
            array_push($whereCodes, ['ut_type', 'ut_type_id', 'ut_type_code', $val, 'Type']);
        }

        if ($val = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);

            if ($val = Common::iset($filterData['building_id'])) {
                array_push($whereCodes, ['building', 'building_id', 'building_code', $val, "Building Code"]);
            }
        }

        if ($val = Common::iset($filterData['prop_zone_id'])) {
            array_push($whereCodes, ['prop_zone', 'prop_zone_id', 'prop_zone_code', $val, 'Zone']);

            if ($val = Common::iset($filterData['prop_external_id'])) {
                array_push(
                    $whereCodes,
                    ['prop_external', 'prop_external_id', 'prop_external_code', $val, 'External Area']
                );
            }
        }

        if ($val = Common::iset($filterData['supplier'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Supplier']);
        }

        if ($val = Common::iset($filterData['paymentMethod'])) {
            array_push(
                $whereCodes,
                ['payment_method', 'payment_method_id', 'payment_method_code', $val, 'Payment Type']
            );
        }

        if ($val = Common::iset($filterData['financialYear'])) {
            array_push($whereCodes, ['fin_year', 'fin_year_id', 'fin_year_code', $val, 'Financial Year']);
        }

        if ($val = Common::iset($filterData['active'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, "Active = 'Active'");
                    break;
                case 'all':
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                default:
                    array_push($whereTexts, "Active = 'Inactive'");
                    break;
            }
        }

        return $query;
    }

    public function reAddUtilityBillQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $utilityBillService = new UtilityBillService(
            $this->permissionService,
            new UtilityService($this->permissionService, $this->userDefinedService),
            $this->userDefinedService
        );
        $query = $utilityBillService->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['invoiceNumber'])) {
            array_push($whereTexts, "Invoice Number contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['account'])) {
            array_push($whereCodes, ['fin_account', 'fin_account_id', 'fin_account_code', $val, 'Account']);
        }

        if ($val = Common::iset($filterData['utilityType'])) {
            array_push($whereCodes, ['ut_type', 'ut_type_id', 'ut_type_code', $val, 'Utility Type']);
        }

        if ($val = Common::iset($filterData['invoiceDateFrom'])) {
            array_push($whereTexts, "Invoice Date From '" . $val . "'");
        }

        if ($val = Common::iset($filterData['invoiceDateTo'])) {
            array_push($whereTexts, "Invoice Date To '" . $val . "'");
        }

        if ($val = Common::iset($filterData['readingDateFrom'])) {
            array_push($whereTexts, "Reading Date From '" . $val . "'");
        }

        if ($val = Common::iset($filterData['readingDateTo'])) {
            array_push($whereTexts, "Reading Date To '" . $val . "'");
        }

        if ($val = Common::iset($filterData['periodStartDate'])) {
            array_push($whereTexts, "Billing Period From '" . $val . "'");
        }

        if ($val = Common::iset($filterData['periodEndDate'])) {
            array_push($whereTexts, "Billing Period To '" . $val . "'");
        }

        if ($val = Common::iset($filterData['financialYear'])) {
            array_push($whereCodes, ['fin_year', 'fin_year_id', 'fin_year_code', $val, 'Financial Year']);
        }

        return $query;
    }
}
