<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Services\Property\TransparencyService;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Property\BuildingService;
use Tfcloud\Services\Property\SiteService;

class TransparencyFilterQueryService extends BaseService
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    public function reAddLocalAuthorityLand(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $siteService = new SiteService($this->permissionService);
        $buildingService = new BuildingService($this->permissionService);
        $service = new TransparencyService($this->permissionService, $siteService, $buildingService);
        $query = $service->filterAll($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'status')) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, "Status = Active");
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, "Status = Inactive");
                    break;
                default:
                    array_push($whereTexts, "Status = All");
                    break;
            }
        }

        return $query;
    }
}
