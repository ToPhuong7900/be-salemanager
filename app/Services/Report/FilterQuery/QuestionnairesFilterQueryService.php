<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Filters\Questionnaire\FiledQstActionFilter;
use Tfcloud\Services\Admin\System\Questionnaire\QuestionnaireService;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Questionnaire\FiledQstActionService;
use Tfcloud\Services\Questionnaire\FiledQstService;

class QuestionnairesFilterQueryService extends BaseService
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    public function reAddFiledQstQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $filedQstService = new FiledQstService($this->permissionService);
        $filedQstQuery = $filedQstService->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Filed Questionnaire Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['qst'])) {
            array_push(
                $whereCodes,
                ['qst', 'qst_id', 'qst_code', $val, 'Questionnaire']
            );
        }

        if ($val = Common::iset($filterData['dateFrom'])) {
            array_push($whereTexts, "Date from {$val}");
        }

        if ($val = Common::iset($filterData['dateTo'])) {
            array_push($whereTexts, "Date to {$val}");
        }

        if ($val = Common::iset($filterData['owner'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Owner']);
        }

        $val = Common::iset($filterData['current']);
        if (is_numeric($val)) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_ONE:
                    array_push($whereTexts, "Current = 'Yes'");
                    break;

                case CommonConstant::DATABASE_VALUE_ZERO:
                default:
                    array_push($whereTexts, "Current = 'No'");
                    break;
            }
        } else {
            array_push($whereTexts, "Current = 'All'");
        }

        if ($val = Common::iset($filterData['status_type_id'])) {
            array_push(
                $orCodes,
                [
                    'qst_status_type',
                    'status_type_id',
                    'status_type_code',
                    implode(',', $val),
                    "Status Type"
                ]
            );
        }

        return $filedQstQuery;
    }

    public function reAddQstQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $qstService = new QuestionnaireService($this->permissionService);
        $qstQuery = $qstService->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Questionnaire Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Questionnaire Description contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['type'])) {
            array_push(
                $whereCodes,
                ['qst_type', 'qst_type_id', 'qst_type_code', $val, 'Questionnaire Type']
            );
        }

        $val = Common::iset($filterData['active']);
        if (is_numeric($val)) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_ONE:
                    array_push($whereTexts, "Active = 'Yes'");
                    break;

                case CommonConstant::DATABASE_VALUE_ZERO:
                default:
                    array_push($whereTexts, "Active = 'No'");
                    break;
            }
        } else {
            array_push($whereTexts, "Active = 'All'");
        }

        $val = Common::iset($filterData['inUse']);
        if (is_numeric($val)) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_ONE:
                    array_push($whereTexts, "In Use = 'Yes'");
                    break;

                case CommonConstant::DATABASE_VALUE_ZERO:
                default:
                    array_push($whereTexts, "In Use = 'No'");
                    break;
            }
        } else {
            array_push($whereTexts, "In Use = 'All'");
        }

        return $qstQuery;
    }

    public function reAddFiledQstActionQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $service = new FiledQstActionService($this->permissionService, new FiledQstService($this->permissionService));
        $filter = new FiledQstActionFilter($filterData);

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Filed Questionnaire Code contains '" . $val . "'");
        }

        if ($filter->priority) {
            array_push(
                $whereCodes,
                ['qst_action_priority', 'action_priority_id', 'action_priority_code', $filter->priority, 'Priority']
            );
        }

        if ($filter->category) {
            array_push(
                $whereCodes,
                ['qst_action_category', 'action_category_id', 'action_category_code', $filter->category, 'Category']
            );
        }

        if (is_numeric($filter->owner)) {
            array_push($whereCodes, ['user', 'id', 'display_name', $filter->owner, 'Owner']);
        }

        if ($filter->dateFrom) {
            array_push($whereTexts, "Due Date from {$filter->dateFrom}");
        }

        if ($filter->dateTo) {
            array_push($whereTexts, "Due Date to {$filter->dateTo}");
        }

        if ($filter->dateCompletedFrom) {
            array_push($whereTexts, "Completed Date from {$filter->dateCompletedFrom}");
        }

        if ($filter->dateCompletedTo) {
            array_push($whereTexts, "Completed Date to {$filter->dateCompletedTo}");
        }

        if ($filter->completed) {
            switch ($filter->completed) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, "Completed = Yes");
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, "Completed = No");
                    break;
                default:
                    array_push($whereTexts, "Completed = All");
                    break;
            }
        }

        if ($filter->type) {
            array_push(
                $whereCodes,
                ['qst_type', 'qst_type_id', 'qst_type_code', $filter->type, 'Type']
            );
        }

        return $service->filterAll($query, $filterData, $viewName);
    }

    public function reAddFiledQstDesignQuery($filterData, $viewName, $query)
    {
        $qstId = array_get($filterData, 'qstId', null);
        $query->where("$viewName.qst_id", $qstId)
        ;

        return $query;
    }
}
