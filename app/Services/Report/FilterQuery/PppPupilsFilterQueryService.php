<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\Filters\BaseFilter;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\PupilPlacePlanning\PupilsService;
use Tfcloud\Services\BaseService;

class PppPupilsFilterQueryService extends BaseService
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    public function reAddPppPupilQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $pupil = new PupilsService(
            $this->permissionService
        );
        $query = $pupil->filterAll($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'academic_year', null)) {
            array_push($whereCodes, [
                'academic_year',
                'academic_year_id',
                'academic_year_code',
                $val,
                \Form::fieldLang('academicYear')
            ]);
        }

        if ($val = array_get($filterData, 'upn', null)) {
            array_push($whereTexts, \Lang::get('text.upn') . " contains '$val'");
        }

        if ($val = array_get($filterData, 'postcode', null)) {
            array_push($whereTexts, \Lang::get('text.postcode') . " contains '$val'");
        }

        if ($val = array_get($filterData, 'from', null)) {
            array_push($whereTexts, 'Date of Birth ' . \Lang::get('text.from') . " " . $val);
        }

        if ($val = array_get($filterData, 'to', null)) {
            array_push($whereTexts, 'Date of Birth ' . \Lang::get('text.to') . " " . $val);
        }

        if ($val = array_get($filterData, 'location', null)) {
            array_push($whereCodes, [
                'site',
                'site_id',
                'site_code',
                $val,
                \Form::fieldLang('location')
            ]);
        }

        if ($val = array_get($filterData, 'la_code', null)) {
            array_push($whereTexts, \Lang::get('text.la_code') . " contains '$val'");
        }

        if ($val = array_get($filterData, 'suffType', null)) {
            array_push($whereCodes, [
                'suff_type',
                'suff_type_id',
                'suff_type_code',
                $val,
                \Form::fieldLang('suffType')
            ]);
        }
//
        if ($val = array_get($filterData, 'schoolYear', null)) {
            array_push($whereCodes, [
                'school_year',
                'school_year_id',
                'school_year_code',
                $val,
                \Form::fieldLang('schoolYear')
            ]);
        }

        $val = array_get($filterData, 'catholic', null);
        if (!is_null($val)) {
            if ($val == 'Y') {
                array_push($whereTexts, \Lang::get('text.catholic') . " contains Catholic");
            } elseif ($val == 'N') {
                array_push($whereTexts, \Lang::get('text.catholic') . " contains Other");
            }
        }

        return $query;
    }
}
