<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\Common;
use Tfcloud\Models\CpSalesInvoiceStatus;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\CostPlus\CpSalesInvoiceService;
use Tfcloud\Services\BaseService;

class CostPlusSalesInvoiceFilterQueryService extends BaseService
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    public function reAddCpSalesInvoiceQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $costPlusSalesInvoiceService = new CpSalesInvoiceService(
            $this->permissionService
        );
        $query = $costPlusSalesInvoiceService->filterAll($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'code', null)) {
            array_push($whereTexts, " Code contains '$val'");
        }

        if ($val = array_get($filterData, 'description', null)) {
            array_push($whereTexts, " Description contains '$val'");
        }

        if ($val = array_get($filterData, 'taxDateFrom', null)) {
            array_push($whereTexts, 'Tax Date From ' . " $val" . "'");
        }

        if ($val = array_get($filterData, 'taxDateTo', null)) {
            array_push($whereTexts, 'Tax Date To ' . " $val" . "'");
        }

        if ($val = array_get($filterData, 'netTotalFrom', null)) {
            array_push($whereTexts, 'Net Total From ' . " $val" . "'");
        }

        if ($val = array_get($filterData, 'netTotalTo', null)) {
            array_push($whereTexts, 'Net Total To ' . " $val" . "'");
        }

        $statusType = [];

        if (Common::iset($filterData['draft'])) {
            $statusType[] = CpSalesInvoiceStatus::DRAFT;
        }
        if (Common::iset($filterData['approved'])) {
            $statusType[] = CpSalesInvoiceStatus::APPROVED;
        }
        if (Common::iset($filterData['generated'])) {
            $statusType[] = CpSalesInvoiceStatus::GENERATED;
        }
        if (Common::iset($filterData['posted'])) {
            $statusType[] = CpSalesInvoiceStatus::POSTED;
        }
        if (Common::iset($filterData['paid'])) {
            $statusType[] = CpSalesInvoiceStatus::PAID;
        }
        if (Common::iset($filterData['cancelled'])) {
            $statusType[] = CpSalesInvoiceStatus::CANCELLED;
        }
        if (count($statusType)) {
            array_push($orCodes, [
                    'cp_sales_invoice_status',
                    'cp_sales_invoice_status_id',
                    'cp_sales_invoice_status_desc',
                    implode(',', $statusType),
                    \Lang::get('text.status_type')
            ]);
        }

        return $query;
    }
}
