<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\Common;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\NotesService;
use Tfcloud\Services\PermissionService;

class AllNotesFilterQueryService extends BaseService
{
    protected $permissionService;
    protected $notesService;

    public function __construct()
    {
        $this->permissionService = new PermissionService();
        $this->notesService = new NotesService($this->permissionService);
    }

    public function allNotesQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $query = $this->notesService->applyFilter(
            $this->notesService->allNotesReport($query),
            $filterData,
            $viewName,
            ['referrer_module_id' => true, 'is_report' => true]
        );

        if ($val = Common::iset($filterData['module'])) {
            array_push(
                $whereCodes,
                ['module', 'module_id', 'module_description', $val, 'Module']
            );
        }

        if ($val = array_get($filterData, 'from')) {
            array_push($whereTexts, "Notes Form = '" . $val . "'");
        }

        if ($val = array_get($filterData, 'to')) {
            array_push($whereTexts, "Notes To = '" . $val . "'");
        }

        if ($val = array_get($filterData, 'fromTime')) {
            array_push($whereTexts, "Time From = '" . $val . "'");
        }

        if ($val = array_get($filterData, 'dueDateFrom')) {
            array_push($whereTexts, "Due Date From = '" . $val . "'");
        }

        if ($val = array_get($filterData, 'dueDateTo')) {
            array_push($whereTexts, "Due Date To = '" . $val . "'");
        }

        if ($val = array_get($filterData, 'completedDateFrom')) {
            array_push($whereTexts, "Completed Date From = '" . $val . "'");
        }

        if ($val = array_get($filterData, 'completedDateTo')) {
            array_push($whereTexts, "Completed Date To = '" . $val . "'");
        }

        if ($val = array_get($filterData, 'search')) {
            array_push($whereTexts, "Detail contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'noteType')) {
            array_push($whereCodes, [
                'note_type',
                'note_type_id',
                'note_type_code',
                $val,
                'Type'
            ]);
        }

        if ($val = array_get($filterData, 'authorUserId')) {
            array_push($whereCodes, [
                'user',
                'id',
                'display_name',
                $val,
                'Author'
            ]);
        }

        if ($val = array_get($filterData, 'owner')) {
            array_push($whereCodes, [
                'user',
                'id',
                'display_name',
                $val,
                'Owner'
            ]);
        }

        if ($val = array_get($filterData, 'noteSubject')) {
            array_push($whereCodes, [
                'note_subject',
                'note_subject_id',
                'note_subject_code',
                $val,
                'Subject'
            ]);
        }

        if ($val = array_get($filterData, 'privacy')) {
            array_push($whereTexts, "Private Notes = '" . $val . "'");
        }

        if ($val = array_get($filterData, 'rural')) {
            array_push($whereTexts, "Rural Notes = '" . $val . "'");
        }
        return $query;
    }
}
