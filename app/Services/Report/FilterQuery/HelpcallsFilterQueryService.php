<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Filters\Admin\System\Helpcall\HelpcallSurveyQstFilter;
use Tfcloud\Models\EstUnitType;
use Tfcloud\Models\FinAuthStatus;
use Tfcloud\Models\GenTable;
use Tfcloud\Models\HelpcallOnHold;
use Tfcloud\Models\HelpcallStatusType;
use Tfcloud\Models\HelpcallSurveyQst;
use Tfcloud\Models\HelpcallSurveyQstAnswerType;
use Tfcloud\Models\InstructionOnHold;
use Tfcloud\Models\InstructionParentType;
use Tfcloud\Models\InstructionStatus;
use Tfcloud\Models\InstructionStatusType;
use Tfcloud\Models\User;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\Helpcall\HelpcallService;
use Tfcloud\Services\PermissionService;

class HelpcallsFilterQueryService extends BaseService
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    public function hlp12ResultsQuery($filterData, $report, &$whereTexts)
    {
        $fromDate = '1971-01-01 00:00:00';
        $toDate = Carbon::now()->format('Y-m-d H:i:s');
        $reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
        if ($reportBoxFilterLoader && $reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $reportBoxFilterLoader->getFilterQuery($filterData);
            $values = $reportFilterQuery->getValueFilterField('date');
            if ($values && isset($values[0])) {
                if (isset($values[0][0]) && $values[0][0]) {
                    $fromDate = Carbon::createFromFormat('Y-m-d', $values[0][0]);
                    array_push($whereTexts, "Date from {$fromDate->format('d/m/Y')}");
                    $fromDate = $fromDate->format('Y-m-d 00:00:00');
                }
                if (isset($values[0][1]) && $values[0][1]) {
                    $toDate = Carbon::createFromFormat('Y-m-d', $values[0][1]);
                    array_push($whereTexts, "Date from {$toDate->format('d/m/Y')}");
                    $toDate = $toDate->format('Y-m-d 23:59:59');
                }
            }
            $report->setRepFilterable(false);
        } else {
            if (isset($filterData['Date_From'])) {
                $fromDate = Carbon::createFromFormat('d/m/Y', $filterData['Date_From'])->format('Y-m-d 00:00:00');
            }

            if (isset($filterData['Date_To'])) {
                $toDate = Carbon::createFromFormat('d/m/Y', $filterData['Date_To'])->format('Y-m-d H:i:s');
            }
        }

        $query = \DB::table('helpcall_survey_qst_answer_type_option')->select([
            'helpcall_survey_qst.site_group_id AS `site_group_id`',
            'helpcall_survey_qst.helpcall_survey_qst_code AS `Helpcall Question Code`',
            'helpcall_survey_qst.question AS `Question`',
            'helpcall_survey_qst_answer_type_option.helpcall_survey_qst_answer_type_option_code AS `Answer`',
            \DB::raw('COALESCE((select count(helpcall_survey_qst_id) from helpcall_survey_detail
            inner join helpcall_survey on helpcall_survey.helpcall_survey_id = helpcall_survey_detail.helpcall_survey_id
            where helpcall_survey_detail.helpcall_survey_qst_id = helpcall_survey_qst.helpcall_survey_qst_id
            AND helpcall_survey.completion_date >= "' .  $fromDate .
            '" AND helpcall_survey.completion_date <= "' .  $toDate .
            '" group by helpcall_survey_qst_id), 0) AS `Question Count`'),
            \DB::raw('COALESCE((select count(answer) from helpcall_survey_detail
            inner join helpcall_survey on helpcall_survey.helpcall_survey_id = helpcall_survey_detail.helpcall_survey_id
            where helpcall_survey_detail.helpcall_survey_qst_id = helpcall_survey_qst.helpcall_survey_qst_id
            and helpcall_survey_detail.answer =
            helpcall_survey_qst_answer_type_option.helpcall_survey_qst_answer_type_option_id
            AND helpcall_survey.completion_date >= "' .  $fromDate .
            '" AND helpcall_survey.completion_date <= "' .  $toDate .
            '" group by helpcall_survey_qst_id, answer), 0) AS `Answer Count`'),
            \DB::raw('COALESCE(ROUND(((select count(answer) from helpcall_survey_detail
            inner join helpcall_survey on helpcall_survey.helpcall_survey_id = helpcall_survey_detail.helpcall_survey_id
            where helpcall_survey_detail.helpcall_survey_qst_id = helpcall_survey_qst.helpcall_survey_qst_id
            and helpcall_survey_detail.answer =
            helpcall_survey_qst_answer_type_option.helpcall_survey_qst_answer_type_option_id
            AND helpcall_survey.completion_date >= "' .  $fromDate .
            '" AND helpcall_survey.completion_date <= "' .  $toDate .
            '" group by helpcall_survey_qst_id, answer) /
            (select count(helpcall_survey_qst_id) from helpcall_survey_detail
            inner join helpcall_survey on helpcall_survey.helpcall_survey_id = helpcall_survey_detail.helpcall_survey_id
            where helpcall_survey_detail.helpcall_survey_qst_id = helpcall_survey_qst.helpcall_survey_qst_id
            AND helpcall_survey.completion_date >= "' .  $fromDate .
            '" AND helpcall_survey.completion_date <= "' .  $toDate .
            '" group by helpcall_survey_qst_id)) * 100, 2), 0) AS `Percentage`'),
            \DB::raw('COALESCE(helpcall_survey_qst_answer_type_option.score * (select count(answer)
            from helpcall_survey_detail
            inner join helpcall_survey on helpcall_survey.helpcall_survey_id = helpcall_survey_detail.helpcall_survey_id
            where helpcall_survey_detail.helpcall_survey_qst_id = helpcall_survey_qst.helpcall_survey_qst_id
            and helpcall_survey_detail.answer =
            helpcall_survey_qst_answer_type_option.helpcall_survey_qst_answer_type_option_id
            AND helpcall_survey.completion_date >= "' .  $fromDate .
            '" AND helpcall_survey.completion_date <= "' .  $toDate .
            '" group by helpcall_survey_qst_id, answer), 0) AS `Total Score`'),
        ])
        ->join(
            'helpcall_survey_qst',
            'helpcall_survey_qst.helpcall_survey_qst_id',
            '=',
            'helpcall_survey_qst_answer_type_option.helpcall_survey_qst_id'
        )
        ->orderBy(
            'helpcall_survey_qst.helpcall_survey_qst_id'
        );

        return $query;
    }

    public function hlp15ResultsQuery($filterData, $report, &$whereTexts)
    {
        $recordStatusHelpCall = $this->getStatusHelpCall();

        $hlpOnHold = $this->getHlpOnHold();

        $recordStatusHelpCall->unionAll($hlpOnHold);

        $recordStatusInstruction = $this->getStatusInstruction();

        $recordStatusInitialInstruction = $this->getStatusInitialInstruction();

        $allRecordStatusInstruction = $this->getAllRecordStatusInstruction(
            $recordStatusInstruction,
            $recordStatusInitialInstruction
        );

        $insOnHold = $this->getInsOnHold();

        $allRecordStatusInstruction->unionAll($insOnHold);

        $query = DB::query()->fromSub($recordStatusHelpCall->unionAll($allRecordStatusInstruction), 'data');
        $query->select(
            [
                DB::raw("`helpcall`.`helpcall_code` AS `Help Call Code`"),
                DB::raw("`helpcall`.`client_ref` AS `Client ref`"),
                DB::raw("`establishment`.`establishment_code` AS `Establishment Code`"),
                DB::raw("`establishment`.`establishment_desc` AS `Establishment Description`"),
                DB::raw("`site`.`site_code` AS `Site Code`"),
                DB::raw("`site`.`site_desc` AS `Site Description`"),
                DB::raw("`building`.`building_code` AS `Building Code`"),
                DB::raw("`building`.`building_desc` AS `Building Description`"),
                DB::raw("`room`.`room_number` AS `Room Number`"),
                DB::raw("`room`.`room_desc` AS `Room Description`"),
                DB::raw("`plant`.`plant_code` AS `Plant Code`"),
                DB::raw("`plant`.`plant_desc` AS `Plant Description`"),
                DB::raw("`helpcall`.`contact_name` AS `Name`"),
                DB::raw("`helpcall`.`contact_email` AS `Email`"),
                DB::raw("`helpcall`.`contact_telephone` AS `Telephone`"),
                DB::raw("`helpcall`.`contact_organisation` AS `Organisation`"),
                DB::raw("`helpcall_std_problem`.`helpcall_std_problem_code` AS `Standard Problem Code`"),
                DB::raw("`helpcall_std_problem`.`helpcall_std_problem_desc` AS `Standard Problem Description`"),
                DB::raw("`helpcall`.`helpcall_short_desc` AS `Short Description`"),
                DB::raw("`helpcall`.`helpcall_desc` AS `Description`"),
                DB::raw("`category`.`category_name` AS `Category Code`"),
                DB::raw("`category`.`category_desc` AS `Category Description`"),
                DB::raw("`helpcall_subcategory`.`helpcall_subcategory_code` AS `Sub Category Code`"),
                DB::raw("`helpcall_subcategory`.`helpcall_subcategory_desc` AS `Sub Category Description`"),
                DB::raw("`priority`.`priority_name` AS `Priority Code`"),
                DB::raw("`priority`.`priority_desc` AS `Priority Description`"),
                DB::raw("`data`.`Instruction Code`"),
                DB::raw("`data`.`Instruction Ref`"),
                DB::raw("`data`.`Instruction Supplier`"),
                DB::raw("date_format(`data`.`status_date`,'%d/%m/%Y') AS `Event Date`"),
                DB::raw("`data`.`status_time` AS `Event Time`"),
                DB::raw("`data`.`Status Type Code` AS `Status Type Code`"),
                DB::raw("`data`.`Status Type Description` AS `Status Type Description`"),
                DB::raw("`data`.`Status Code` AS `Status Code`"),
                DB::raw("`data`.`Status Description` AS `Status Description`"),
                DB::raw("IFNULL(`data`.`on_hold`,`helpcall`.`on_hold`) AS `On Hold`"),
                DB::raw("`data`.`comments` AS `Comments`"),
            ]
        )
            ->leftJoin(
                'helpcall',
                'helpcall.helpcall_id',
                '=',
                'data.helpcall_id'
            )
            ->leftJoin(
                'site',
                'site.site_id',
                '=',
                'helpcall.site_id'
            )
            ->leftJoin(
                'establishment',
                'establishment.establishment_id',
                '=',
                'site.establishment_id'
            )
            ->leftJoin(
                'building',
                'building.building_id',
                '=',
                'helpcall.building_id'
            )
            ->leftJoin(
                'room',
                'room.room_id',
                '=',
                'helpcall.room_id'
            )
            ->leftJoin(
                'plant',
                'plant.plant_id',
                '=',
                'helpcall.plant_id'
            )
            ->leftJoin(
                'plant_group',
                'plant_group.plant_group_id',
                '=',
                'plant.plant_group_id'
            )
            ->leftJoin(
                'plant_subgroup',
                'plant_subgroup.plant_subgroup_id',
                '=',
                'plant.plant_subgroup_id'
            )
            ->leftJoin(
                'plant_type',
                'plant_type.plant_type_id',
                '=',
                'plant.plant_type_id'
            )
            ->leftJoin(
                'helpcall_std_problem',
                'helpcall_std_problem.helpcall_std_problem_id',
                '=',
                'helpcall.helpcall_std_problem_id'
            )
            ->leftJoin(
                'category',
                'category.category_id',
                '=',
                'helpcall.category_id'
            )
            ->leftJoin(
                'priority',
                'priority.priority_id',
                '=',
                'helpcall.priority_id'
            )
            ->leftJoin(
                'helpcall_subcategory',
                'helpcall_subcategory.helpcall_subcategory_id',
                '=',
                'helpcall.helpcall_subcategory_id'
            )
            ->leftJoin(
                'gen_userdef_group',
                'gen_userdef_group.gen_userdef_group_id',
                '=',
                'helpcall.gen_userdef_group_id'
            )
            ->leftJoin(
                'spot_check',
                'spot_check.helpcall_id',
                '=',
                'helpcall.helpcall_id'
            )
            ->leftJoin(
                'lettable_unit',
                'lettable_unit.lettable_unit_id',
                '=',
                'helpcall.lettable_unit_id'
            )
            ->orderBy('helpcall.helpcall_code', 'asc')
            ->orderBy('data.audit_date', 'desc')
            ;

        $siteAccess = \Auth::user()->site_access;
        if ($siteAccess == User::SITE_ACCESS_MODE_SELECTED) {
            $sites = \Auth::user()->sites->map(
                function ($site) {
                    return $site->site_id;
                }
            );
            if (!$sites->isEmpty()) {
                $query = $query->whereIn('helpcall.site_id', $sites->toArray());
            } else {
                $query = $query->where(\DB::raw('false'));
            }
        }
        return $query;
    }

    private function getStatusHelpCall()
    {
        return DB::table('record_status AS rs1')
        ->select([
            'rs1.audit_date',
            'rs1.status_date',
            'rs1.status_time',
            'rs1.comments',
            'rs1.record_id AS helpcall_id',
            DB::raw("NULL AS 'Instruction Code'"),
            DB::raw("NULL AS 'Instruction Ref'"),
            DB::raw("NULL AS 'Instruction Supplier'"),
            DB::raw("NULL AS 'on_hold'"),
            'helpcall_status.code AS Status Code',
            'helpcall_status.description AS Status Description',
            'helpcall_status_type.helpcall_status_type_code AS Status Type Code',
            'helpcall_status_type.helpcall_status_type_desc AS Status Type Description'
        ])
        ->where('rs1.gen_table_id', GenTable::HELPCALL)
        ->where("rs1.site_group_id", "=", \Auth::user()->site_group_id)
        ->leftJoin(
            'helpcall_status',
            'helpcall_status.helpcall_status_id',
            '=',
            'rs1.status_id'
        )
        ->leftJoin(
            'helpcall_status_type',
            'helpcall_status_type.helpcall_status_type_id',
            '=',
            'helpcall_status.helpcall_status_type_id'
        );
    }

    private function getHlpOnHold()
    {
        $hlpPutOnHold = HelpcallOnHold::userSiteGroup()
            ->select([
                DB::raw("CONCAT(DATE_FORMAT(`start_date`, '%Y-%m-%d'),
                ' ',
                TIME_FORMAT(`start_time`, '%H:%i:%s')) AS `audit_date`"),
                'start_date AS status_date',
                'start_time AS status_time',
                DB::raw("'Y' AS 'on_hold'"),
                DB::raw("'Put on hold' AS 'comments'"),
                'helpcall_id AS helpcall_id',
            ])
            ->whereNotNull('start_date')
            ->whereNotNull('start_time');
        $hlpTakeOffHold = HelpcallOnHold::userSiteGroup()
            ->select([
                DB::raw("CONCAT(DATE_FORMAT(`end_date`, '%Y-%m-%d'),
                ' ',
                TIME_FORMAT(`end_time`, '%H:%i:%s')) AS `audit_date`"),
                'end_date AS status_date',
                'end_time AS status_time',
                DB::raw("'N' AS 'on_hold'"),
                DB::raw("'Taken off hold' AS 'comments'"),
                'helpcall_id AS helpcall_id',
            ])
            ->whereNotNull('end_date')
            ->whereNotNull('end_time');
        $hlpOnHold = DB::query()->fromSub(
            $hlpPutOnHold->unionAll($hlpTakeOffHold),
            'hlpOnHold'
        );
        $hlpOnHold->select([
            'hlpOnHold.audit_date',
            'hlpOnHold.status_date',
            'hlpOnHold.status_time',
            'hlpOnHold.comments',
            'hlpOnHold.helpcall_id',
            DB::raw("NULL AS 'Instruction Code'"),
            DB::raw("NULL AS 'Instruction Ref'"),
            DB::raw("NULL AS 'Instruction Supplier'"),
            'hlpOnHold.on_hold',
            'helpcall_status.code AS Status Code',
            'helpcall_status.description AS Status Description',
            'helpcall_status_type.helpcall_status_type_code AS Status Type Code',
            'helpcall_status_type.helpcall_status_type_desc AS Status Type Description'
        ])
            ->join(
                'helpcall',
                'helpcall.helpcall_id',
                '=',
                'hlpOnHold.helpcall_id'
            )
            ->leftJoin(
                'helpcall_status',
                'helpcall_status.helpcall_status_id',
                '=',
                'helpcall.helpcall_status_id'
            )
            ->leftJoin(
                'helpcall_status_type',
                'helpcall_status_type.helpcall_status_type_id',
                '=',
                'helpcall_status.helpcall_status_type_id'
            );
        return $hlpOnHold;
    }

    private function getStatusInstruction()
    {
        return \DB::table('record_status AS rs2')
        ->select([
            'rs2.gen_table_id',
            'rs2.record_id',
            'rs2.audit_date',
            'rs2.status_date',
            'rs2.status_time',
            'rs2.comments',
            'instruction.parent_id AS parent_id',
            'instruction.instruction_code AS instruction_code',
            'instruction.reference AS reference',
            'instruction.on_hold AS on_hold',
            'instruction.supplier_contact_id AS supplier_contact_id',
            'rs2.status_id',
        ])
        ->join('instruction', function ($q) {
            $q->on('instruction.instruction_id', '=', 'rs2.record_id')
                ->where('rs2.gen_table_id', GenTable::INSTRUCTION)
                ->whereNotNull('instruction.parent_id')
                ->whereIn('instruction.parent_id', function ($query) {
                    $query->select('record_id')->from('record_status')->where('gen_table_id', GenTable::HELPCALL);
                });
        })
        ->where("rs2.site_group_id", "=", \Auth::user()->site_group_id);
    }

    private function getStatusInitialInstruction()
    {
        $recordStatusInitialInstruction = \DB::table('instruction')
            ->select([
                DB::raw(GenTable::INSTRUCTION . " AS 'gen_table_id'"),
                'instruction.instruction_id AS record_id',
                'instruction.created_on AS audit_date',
                DB::raw("date_format(`instruction`.`created_on`,'%d/%m/%Y') AS `status_date`"),
                DB::raw("date_format(`instruction`.`created_on`,'%H:%i') AS `status_time`"),
                DB::raw("NULL AS 'comments'"),
                'instruction.parent_id AS parent_id',
                'instruction.instruction_code AS instruction_code',
                'instruction.reference AS reference',
                'instruction.on_hold AS on_hold',
                'instruction.supplier_contact_id AS supplier_contact_id',
            ])
            ->where("instruction.site_group_id", "=", \Auth::user()->site_group_id)
            ->where("instruction.parent_type_id", InstructionParentType::HELPCALL);
        $recordStatusInitialInstruction->addSelect(['status_id' =>
            InstructionStatus::userSiteGroup()->select('instruction_status_id')
                ->where('instruction_status_type_id', '=', InstructionStatusType::GENERATED)
                ->where('instruction_status.active', 'Y')
                ->orderBy('display_order')
                ->limit(1)
        ]);
        return $recordStatusInitialInstruction;
    }
    private function getAllRecordStatusInstruction(
        $recordStatusInstruction,
        $recordStatusInitialInstruction
    ) {
        $query = DB::query()->fromSub(
            $recordStatusInstruction->unionAll($recordStatusInitialInstruction),
            'insData'
        );
        $query
            ->select([
                'insData.audit_date',
                'insData.status_date',
                'insData.status_time',
                'insData.comments',
                'insData.parent_id AS helpcall_id',
                'insData.instruction_code AS Instruction Code',
                'insData.reference AS Instruction Ref',
                'contact.contact_name AS Instruction Supplier',
                'insData.on_hold AS on_hold',
                'instruction_status.instruction_status_code AS Status Code',
                'instruction_status.instruction_status_desc AS Status Description',
                'instruction_status_type.instruction_status_type_code AS Status Type Code',
                'instruction_status_type.instruction_status_type_desc AS Status Type Description',
            ])
            ->leftJoin(
                'instruction_status',
                'instruction_status.instruction_status_id',
                '=',
                'insData.status_id'
            )
            ->leftJoin(
                'instruction_status_type',
                'instruction_status_type.instruction_status_type_id',
                '=',
                'instruction_status.instruction_status_type_id'
            )
            ->leftJoin(
                'contact',
                'contact.contact_id',
                '=',
                'insData.supplier_contact_id'
            );
        return $query;
    }

    private function getInsOnHold()
    {
        $insPutOnHold = InstructionOnHold::userSiteGroup()
            ->select([
                DB::raw("CONCAT(DATE_FORMAT(`start_date`, '%Y-%m-%d'),
                ' ',
                TIME_FORMAT(`start_time`, '%H:%i:%s')) AS `audit_date`"),
                'start_date AS status_date',
                'start_time AS status_time',
                DB::raw("'Y' AS 'on_hold'"),
                DB::raw("'Put on hold' AS 'comments'"),
                'instruction_id AS instruction_id',
            ])
            ->whereNotNull('start_date')
            ->whereNotNull('start_time');
        $insTakeOffHold = InstructionOnHold::userSiteGroup()
            ->select([
                DB::raw("CONCAT(DATE_FORMAT(`end_date`, '%Y-%m-%d'),
                ' ',
                TIME_FORMAT(`end_time`, '%H:%i:%s')) AS `audit_date`"),
                'end_date AS status_date',
                'end_time AS status_time',
                DB::raw("'N' AS 'on_hold'"),
                DB::raw("'Taken off hold' AS 'comments'"),
                'instruction_id AS instruction_id',
            ])
            ->whereNotNull('end_date')
            ->whereNotNull('end_time');
        $insOnHold = DB::query()->fromSub(
            $insPutOnHold->unionAll($insTakeOffHold),
            'insOnHold'
        );

        $insOnHold->select([
            'insOnHold.audit_date',
            'insOnHold.status_date',
            'insOnHold.status_time',
            'insOnHold.comments',
            'instruction.parent_id AS helpcall_id',
            'instruction.instruction_code AS Instruction Code',
            'instruction.reference AS Instruction Ref',
            'contact.contact_name AS Instruction Supplier',
            'insOnHold.on_hold AS on_hold',
            'instruction_status.instruction_status_code AS Status Code',
            'instruction_status.instruction_status_desc AS Status Description',
            'instruction_status_type.instruction_status_type_code AS Status Type Code',
            'instruction_status_type.instruction_status_type_desc AS Status Type Description',
        ])
            ->join(
                'instruction',
                'instruction.instruction_id',
                '=',
                'insOnHold.instruction_id'
            )
            ->leftJoin(
                'instruction_status',
                'instruction_status.instruction_status_id',
                '=',
                'instruction.instruction_status_id'
            )
            ->leftJoin(
                'instruction_status_type',
                'instruction_status_type.instruction_status_type_id',
                '=',
                'instruction_status.instruction_status_type_id'
            )
            ->leftJoin(
                'contact',
                'contact.contact_id',
                '=',
                'instruction.supplier_contact_id'
            );
        return $insOnHold;
    }
}
