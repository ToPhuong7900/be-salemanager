<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Services\Admin\System\Utility\LimitService;
use Tfcloud\Services\Admin\System\Utility\UseService;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\PermissionService;

class AdminCodeFilterQueryService extends BaseService
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    // UT01: Utility Limit
    public function reAddUtilityLimitQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $service = new LimitService($this->permissionService);
        $reQuery = $service->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Description contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['active'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.active_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.active_no'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.active_all'));
                    break;
            }
        }

        return $reQuery;
    }

    // UT02: Utility Use
    public function reAddUtilityUseQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $service = new UseService($this->permissionService);
        $reQuery = $service->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Description contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['active'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.active_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.active_no'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.active_all'));
                    break;
            }
        }

        return $reQuery;
    }
}
