<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Models\CrudAccessLevel;
use Tfcloud\Models\GenRagStatus;
use Tfcloud\Models\GenTable;
use Tfcloud\Models\Module;
use Tfcloud\Models\ProjectPriority;
use Tfcloud\Models\ProjectProgramme;
use Tfcloud\Models\ProjectRecordType;
use Tfcloud\Models\ProjectStatus;
use Tfcloud\Models\ProjectStatusType;
use tfcloud\Models\ProjectChangeReqStatus;
use tfcloud\Models\ProjectChangeReqStatusType;
use Tfcloud\Models\ProjectTaskStatus;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\NotesService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Project\FundingSource\FundingSourceService;
use Tfcloud\services\Project\ProjectAccessService;
use Tfcloud\Services\Project\ProjectActionService;
use Tfcloud\Services\Project\ProjectBillOfQtyService;
use Tfcloud\Services\Project\ProjectCashFlowService;
use Tfcloud\Services\Project\ProjectCertificatedPaymentService;
use Tfcloud\Services\Project\ProjectChangeRequestService;
use Tfcloud\Services\Project\ProjectIssueService;
use Tfcloud\Services\Project\ProjectProgressService;
use Tfcloud\Services\Project\ProjectRiskService;
use Tfcloud\Services\Project\ProjectTaskService;
use Tfcloud\Services\Project\ProjectService;
use Tfcloud\Services\Project\TimesheetService;

class ProjectsFilterQueryService extends BaseService
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    public function reAddProjectQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $projectService = new ProjectService($this->permissionService);
        $projectQuery = $projectService->filterAll($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'code')) {
            array_push($whereTexts, "Code contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'title')) {
            array_push($whereTexts, "Title contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'description')) {
            array_push($whereTexts, "Description contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'clientRef')) {
            array_push($whereTexts, "Client ref contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'owner')) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Owner']);
        }

        if ($val = array_get($filterData, 'projectPortfolio')) {
            array_push(
                $whereCodes,
                ['project_portfolio', 'project_portfolio_id', 'project_portfolio_code', $val, 'Project Portfolio']
            );
        }

        if ($val = array_get($filterData, 'projectPriority')) {
            array_push(
                $whereCodes,
                ['project_priority', 'project_priority_id', 'project_priority_code', $val, 'Project Priority']
            );
        }

        if ($val = array_get($filterData, 'projectProgramme')) {
            array_push(
                $whereCodes,
                ['project_programme', 'project_programme_id', 'project_programme_code', $val, 'Project Programme']
            );
        }

        if ($val = array_get($filterData, 'project_status_id')) {
            array_push(
                $whereCodes,
                ['project_status', 'project_status_id', 'project_status_code', $val, 'Project Status']
            );
        }

        if ($val = array_get($filterData, 'establishment')) {
            array_push(
                $whereCodes,
                ['establishment', 'establishment_id', 'establishment_code', $val, 'Establishment']
            );
        }

        if ($val = array_get($filterData, 'project')) {
            array_push(
                $whereCodes,
                ['project', 'project_id', 'project_code', $val, 'Project']
            );
        }

        $projectStatusTypes = [];
        if (Common::iset($filterData['draft'])) {
            $projectStatusTypes[] = ProjectStatusType::DRAFT;
        }
        if (Common::iset($filterData['active'])) {
            $projectStatusTypes[] = ProjectStatusType::ACTIVE;
        }
        if (Common::iset($filterData['complete'])) {
            $projectStatusTypes[] = ProjectStatusType::COMPLETE;
        }
        if (Common::iset($filterData['rejected'])) {
            $projectStatusTypes[] = ProjectStatusType::REJECTED;
        }
        if (count($projectStatusTypes)) {
            array_push($orCodes, [
                'project_status_type',
                'project_status_type_id',
                'project_status_type_code',
                implode(',', $projectStatusTypes),
                "Project Status Type"
            ]);
        }

        $ragStatus = [];
        if (Common::iset($filterData['overallRagRed'])) {
            $ragStatus[] = GenRagStatus::RED;
        }
        if (Common::iset($filterData['overallRagAmber'])) {
            $ragStatus[] = GenRagStatus::AMBER;
        }
        if (Common::iset($filterData['overallRagGreen'])) {
            $ragStatus[] = GenRagStatus::GREEN;
        }
        if (count($ragStatus)) {
            array_push($orCodes, [
                'gen_rag_status',
                'gen_rag_status_id',
                'gen_rag_status_code',
                implode(',', $ragStatus),
                "Overall RAG Status"
            ]);
        }

        $ragStatus = [];
        if (Common::iset($filterData['qualityRagRed'])) {
            $ragStatus[] = GenRagStatus::RED;
        }
        if (Common::iset($filterData['qualityRagAmber'])) {
            $ragStatus[] = GenRagStatus::AMBER;
        }
        if (Common::iset($filterData['qualityRagGreen'])) {
            $ragStatus[] = GenRagStatus::GREEN;
        }
        if (count($ragStatus)) {
            array_push($orCodes, [
                'gen_rag_status',
                'gen_rag_status_id',
                'gen_rag_status_code',
                implode(',', $ragStatus),
                "Quality RAG Status"
            ]);
        }

        $ragStatus = [];
        if (Common::iset($filterData['costRagRed'])) {
            $ragStatus[] = GenRagStatus::RED;
        }
        if (Common::iset($filterData['costRagAmber'])) {
            $ragStatus[] = GenRagStatus::AMBER;
        }
        if (Common::iset($filterData['costRagGreen'])) {
            $ragStatus[] = GenRagStatus::GREEN;
        }
        if (count($ragStatus)) {
            array_push($orCodes, [
                'gen_rag_status',
                'gen_rag_status_id',
                'gen_rag_status_code',
                implode(',', $ragStatus),
                "Cost RAG Status"
            ]);
        }

        $ragStatus = [];
        if (Common::iset($filterData['timeRagRed'])) {
            $ragStatus[] = GenRagStatus::RED;
        }
        if (Common::iset($filterData['timeRagAmber'])) {
            $ragStatus[] = GenRagStatus::AMBER;
        }
        if (Common::iset($filterData['timeRagGreen'])) {
            $ragStatus[] = GenRagStatus::GREEN;
        }
        if (count($ragStatus)) {
            array_push($orCodes, [
                'gen_rag_status',
                'gen_rag_status_id',
                'gen_rag_status_code',
                implode(',', $ragStatus),
                "Time RAG Status"
            ]);
        }

        $ragStatus = [];
        if (Common::iset($filterData['safetyRagRed'])) {
            $ragStatus[] = GenRagStatus::RED;
        }
        if (Common::iset($filterData['safetyRagAmber'])) {
            $ragStatus[] = GenRagStatus::AMBER;
        }
        if (Common::iset($filterData['safetyRagGreen'])) {
            $ragStatus[] = GenRagStatus::GREEN;
        }
        if (count($ragStatus)) {
            array_push($orCodes, [
                'gen_rag_status',
                'gen_rag_status_id',
                'gen_rag_status_code',
                implode(',', $ragStatus),
                "Safety RAG Status"
            ]);
        }

        $ragStatus = [];
        if (Common::iset($filterData['relationshipRagRed'])) {
            $ragStatus[] = GenRagStatus::RED;
        }
        if (Common::iset($filterData['relationshipRagAmber'])) {
            $ragStatus[] = GenRagStatus::AMBER;
        }
        if (Common::iset($filterData['relationshipRagGreen'])) {
            $ragStatus[] = GenRagStatus::GREEN;
        }
        if (count($ragStatus)) {
            array_push($orCodes, [
                'gen_rag_status',
                'gen_rag_status_id',
                'gen_rag_status_code',
                implode(',', $ragStatus),
                "Relationship RAG Status"
            ]);
        }

        $this->reAddUserDefinesQuery(GenTable::PROJECT, $filterData, $whereCodes, $whereTexts);

        return $projectQuery;
    }

    public function reAddProjectNoteQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $noteService = new NotesService($this->permissionService);
        $projectNoteQuery = $noteService->applyFilter($query, $filterData, $viewName);
        if (
            !\Auth::user()->isSuperuser() && !$this->permissionService->hasModuleAccess(
                Module::MODULE_PROJECT,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_SUPERVISOR
            )
        ) {
            $projectNoteQuery = $projectNoteQuery
                ->where(function ($where) use ($viewName) {
                    $where->where($viewName . '.all_users_have_access', CommonConstant::DATABASE_VALUE_YES)
                        ->orWhere("$viewName.owner_user_id", \Auth::user()->getKey())
                        ->orWhere(function ($sub) use ($viewName) {
                            $sub->where("$viewName.project_record_type", 0)
                            ->whereExists(function ($select) use ($viewName) {
                                $select->select(\DB::raw(1))
                                    ->from('project_user')
                                    ->where('project_user.user_id', \Auth::user()->getKey())
                                    ->whereRaw('project_user.project_id = ' . $viewName . '.project_id')
                                ;
                            });
                        })
                        ->orWhereExists(function ($select) use ($viewName) {
                            $select->select(\DB::raw(1))
                                ->from('project_user_access')
                                ->where('project_user_access.user_id', \Auth::user()->getKey())
                                ->whereRaw("{$viewName}.record_id = project_user_access.record_id")
                                ->whereRaw("{$viewName}.project_record_type
                                = project_user_access.project_record_type_id")
                            ;
                        });
                });
        }

        if ($date = array_get($filterData, 'from')) {
            $text = "Note Date from {$date}";

            if ($time = array_get($filterData, 'fromTime')) {
                $text .= " {$time}";
            }

            array_push($whereTexts, $text);
        }

        if ($val = array_get($filterData, 'to')) {
            array_push($whereTexts, "Note Date to {$val}");
        }


        if ($val = array_get($filterData, 'search')) {
            array_push($whereTexts, "Detail contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'noteType')) {
            array_push($whereCodes, ['note_type', 'note_type_id', 'note_type_code', $val, 'Type']);
        }

        if ($val = array_get($filterData, 'owner')) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Owner']);
        }

        if ($val = array_get($filterData, 'privacy')) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, "Private Notes = 'Yes'");
                    break;

                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, "Private Notes = 'No'");
                    break;

                case CommonConstant::ALL:
                default:
                    break;
            }
        }

        if ($val = array_get($filterData, 'projectId')) {
            array_push($whereCodes, ['project', 'project_id', 'project_code', $val, "Project"]);
        }

        if ($val = array_get($filterData, 'project')) {
            array_push(
                $whereCodes,
                ['project', 'project_id', 'project_code', $val, 'Project']
            );
        }

        return $projectNoteQuery;
    }

    public function reAddProjectProgressQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $projectProgressServcie = new ProjectProgressService($this->permissionService);
        $projectProgressQuery = $projectProgressServcie->filterAll($query, $filterData, $viewName);

        if ($code = array_get($filterData, 'code')) {
            $text = "Project Code or Project Progress Code contains '{$code}'";
            array_push($whereTexts, $text);
        }

        if ($description = array_get($filterData, 'description')) {
            $text = "Project Description or Project Progress Description contains '{$description}'";
            array_push($whereTexts, $text);
        }

        if ($val = array_get($filterData, 'progressDateFrom')) {
            array_push($whereTexts, "Project Progress Date from '{$val}'");
        }

        if ($val = array_get($filterData, 'progressDateTo')) {
            array_push($whereTexts, "Project Progress Date to '{$val}'");
        }

        if ($val = array_get($filterData, 'logged_by')) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Logged By']);
        }

        if ($val = array_get($filterData, 'project_status_id')) {
            array_push(
                $whereCodes,
                ['project_status', 'project_status_id', 'project_status_code', $val, 'Project Status']
            );
        }

        if ($val = array_get($filterData, 'projectPriority')) {
            array_push(
                $whereCodes,
                ['project_priority', 'project_priority_id', 'project_priority_code', $val, 'Project Priority']
            );
        }

        if ($val = array_get($filterData, 'projectProgramme')) {
            array_push(
                $whereCodes,
                ['project_programme', 'project_programme_id', 'project_programme_code', $val, 'Project Programme']
            );
        }

        if ($val = array_get($filterData, 'establishment')) {
            array_push(
                $whereCodes,
                ['establishment', 'establishment_id', 'establishment_code', $val, 'Establishment']
            );
        }

        if ($val = array_get($filterData, 'project')) {
            array_push(
                $whereCodes,
                ['project', 'project_id', 'project_code', $val, 'Project']
            );
        }

        $projectStatusTypes = [];
        if (Common::iset($filterData['draft'])) {
            $projectStatusTypes[] = ProjectStatusType::DRAFT;
        }
        if (Common::iset($filterData['active'])) {
            $projectStatusTypes[] = ProjectStatusType::ACTIVE;
        }
        if (Common::iset($filterData['complete'])) {
            $projectStatusTypes[] = ProjectStatusType::COMPLETE;
        }
        if (Common::iset($filterData['rejected'])) {
            $projectStatusTypes[] = ProjectStatusType::REJECTED;
        }
        if (count($projectStatusTypes)) {
            array_push($orCodes, [
                'project_status_type',
                'project_status_type_id',
                'project_status_type_code',
                implode(',', $projectStatusTypes),
                "Project Status Type"
            ]);
        }

        if ($val = array_get($filterData, 'projectPortfolio')) {
            array_push(
                $whereCodes,
                ['project_portfolio', 'project_portfolio_id', 'project_portfolio_code', $val, 'Project Portfolio']
            );
        }

        if ($val = array_get($filterData, 'projectType')) {
            array_push(
                $whereCodes,
                ['project_type', 'project_type_id', 'project_type_code', $val, 'Type']
            );
        }

        return $projectProgressQuery;
    }

    public function reAddProjectItemsQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts,
        $projectRecordType = ProjectRecordType::TYPE_ISSUE
    ) {
        switch ($projectRecordType) {
            case ProjectRecordType::TYPE_ISSUE:
                $projectService = new ProjectIssueService($this->permissionService);
                break;
            case ProjectRecordType::TYPE_RISK:
                $projectService = new ProjectRiskService($this->permissionService);
                break;
            case ProjectRecordType::TYPE_ACTION:
                $projectService = new ProjectActionService($this->permissionService);
                break;

            default:
                break;
        }

        $query = $this->getQuery(
            $projectService->filterAll($query, $filterData, $viewName),
            $viewName,
            $projectRecordType
        );

        if ($val = array_get($filterData, 'nextReviewFrom', null)) {
            array_push($whereTexts, "Next Review From '" . " $val" . "'");
        }

        if ($val = array_get($filterData, 'nextReviewTo', null)) {
            array_push($whereTexts, "Next Review To '" . " $val" . "'");
        }

        if ($val = array_get($filterData, 'establishment')) {
            array_push(
                $whereCodes,
                ['establishment', 'establishment_id', 'establishment_code', $val, 'Establishment']
            );
        }

        // filter action
        if ($val = array_get($filterData, 'completed', null)) {
            $text = '';
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_NO:
                    $text = \Lang::get('text.no');
                    break;
                case CommonConstant::DATABASE_VALUE_YES:
                    $text = \Lang::get('text.yes');
                    break;
                default:
                    break;
            }
            if (! empty($text)) {
                array_push($whereTexts, "Completed? = '" . $text . "'");
            }
        }

        if ($val = array_get($filterData, 'project_status_id')) {
            array_push(
                $whereCodes,
                ['project_status', 'project_status_id', 'project_status_code', $val, 'Project Status']
            );
        }

        if ($val = array_get($filterData, 'project')) {
            array_push(
                $whereCodes,
                ['project', 'project_id', 'project_code', $val, 'Project']
            );
        }

        $projectStatusTypes = [];
        if (Common::iset($filterData['draft'])) {
            $projectStatusTypes[] = ProjectStatusType::DRAFT;
        }
        if (Common::iset($filterData['active'])) {
            $projectStatusTypes[] = ProjectStatusType::ACTIVE;
        }
        if (Common::iset($filterData['complete'])) {
            $projectStatusTypes[] = ProjectStatusType::COMPLETE;
        }
        if (Common::iset($filterData['rejected'])) {
            $projectStatusTypes[] = ProjectStatusType::REJECTED;
        }
        if (count($projectStatusTypes)) {
            array_push($orCodes, [
                'project_status_type',
                'project_status_type_id',
                'project_status_type_code',
                implode(',', $projectStatusTypes),
                "Project Status Type"
            ]);
        }

        if ($val = Common::iset($filterData['owner'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, \Lang::get('text.owner')]);
        }

        //action group in project action filter
        if ($val = Common::iset($filterData['group'])) {
            array_push(
                $whereCodes,
                [
                    'project_action_group',
                    'project_action_group_id',
                    'project_action_group_code',
                    $val,
                    \Form::fieldLang('group_action')
                ]
            );
        }

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Code contains '{$filterData['code']}'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Description contains '{$filterData['description']}'");
        }

        // show issue status (issue filter)
        $issueStatus = [];
        $val = array_get($filterData, 'issueStatusFilterOpen', null);
        if ($val == 1) {
            $issueStatus[] = 'OPEN';
        }

        $val = array_get($filterData, 'issueStatusFilterOnHold', null);
        if ($val == 1) {
            $issueStatus[] = 'ON HOLD';
        }

        $val = array_get($filterData, 'issueStatusFilterClosed', null);
        if ($val == 1) {
            $issueStatus[] = 'CLOSED';
        }

        if (! empty($issueStatus)) {
            array_push($whereTexts, "Project issue status = '" . implode(', ', $issueStatus) . "'");
        }

        // Filter issue category
        if ($val = Common::iset($filterData['category'])) {
            array_push(
                $whereCodes,
                [
                    'project_issue_category',
                    'project_issue_category_id',
                    'project_issue_category_code',
                    $val,
                    \Lang::get('text.issue_category')
                ]
            );
        }

        // Filter issue priority
        if ($val = Common::iset($filterData['priority'])) {
            array_push(
                $whereCodes,
                [
                    'project_issue_priority',
                    'project_issue_priority_id',
                    'project_issue_priority_code',
                    $val,
                    \Lang::get('text.issue_priority')
                ]
            );
        }

        // filter issue
        if ($val = array_get($filterData, 'resolved', null)) {
            $text = '';
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_NO:
                    $text = \Lang::get('text.no');
                    break;
                case CommonConstant::DATABASE_VALUE_YES:
                    $text = \Lang::get('text.yes');
                    break;
                default:
                    break;
            }
            if (!empty($text)) {
                array_push($whereTexts, "Resolved = '" . $text . "'");
            }
        }

        if ($val = array_get($filterData, 'projectPortfolio')) {
            array_push(
                $whereCodes,
                ['project_portfolio', 'project_portfolio_id', 'project_portfolio_code', $val, 'Project Portfolio']
            );
        }

        if ($val = array_get($filterData, 'projectPriority')) {
            array_push(
                $whereCodes,
                ['project_priority', 'project_priority_id', 'project_priority_code', $val, 'Project Priority']
            );
        }

        if ($val = array_get($filterData, 'projectProgramme')) {
            array_push(
                $whereCodes,
                ['project_programme', 'project_programme_id', 'project_programme_code', $val, 'Project Programme']
            );
        }

        if ($val = array_get($filterData, 'projectType')) {
            array_push(
                $whereCodes,
                ['project_type', 'project_type_id', 'project_type_code', $val, 'Type']
            );
        }

        if ($val = array_get($filterData, 'createdDateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.created_date_from'), $val));
        }

        if ($val = array_get($filterData, 'createdDateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.created_date_to'), $val));
        }

        if ($projectRecordType == ProjectRecordType::TYPE_ISSUE) {
            $this->reAddUserDefinesQuery(GenTable::PROJECT_ISSUE, $filterData, $whereCodes, $whereTexts);
        }

        return $query;
    }

    public function reAddTimesheetQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $timesheetService = new TimesheetService($this->permissionService);
        $timesheetQuery = $timesheetService->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['timesheetNo'])) {
            array_push($whereTexts, "Timesheet No contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['owner'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Resource']);
        }

        if ($val = Common::iset($filterData['project'])) {
            array_push($whereCodes, ['project', 'project_id', 'project_code', $val, "Project"]);

            if ($val = Common::iset($filterData['projectPackage'])) {
                array_push(
                    $whereCodes,
                    ['project_package', 'project_package_id', 'project_package_code', $val, "Package"]
                );

                if ($val = Common::iset($filterData['projectPackageTask'])) {
                    array_push(
                        $whereCodes,
                        [
                            'project_package_task',
                            'project_package_task_id',
                            'project_package_task_code',
                            $val,
                            "Activity"
                        ]
                    );
                }
            }
        }

        if ($val = Common::iset($filterData['timesheetStatus'])) {
            array_push(
                $whereCodes,
                [
                    'project_timesheet_status',
                    'project_timesheet_status_id',
                    'project_timesheet_status_code',
                    $val,
                    "Status"
                ]
            );
        }

        if ($val = Common::iset($filterData['weekCommencingFrom'])) {
            array_push($whereTexts, "Week Commencing from '" . $val . "'");
        }

        if ($val = Common::iset($filterData['weekCommencingTo'])) {
            array_push($whereTexts, "Week Commencing to '" . $val . "'");
        }

        if ($val = Common::iset($filterData['usergroup_id'])) {
            array_push(
                $whereCodes,
                [
                    'usergroup',
                    'usergroup_id',
                    'usergroup_code',
                    $val,
                    "User Group"
                ]
            );
        }

        return $timesheetQuery;
    }

    public function reAddProjectChangeRequestQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $projectChangeRequestService = new ProjectChangeRequestService($this->permissionService);
        $projectChangeRequestQuery = $projectChangeRequestService->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Project Change Request Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Project Change Request Description contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['prjChangeReqType'])) {
            array_push(
                $whereCodes,
                ['project_change_req_type', 'project_change_req_type_id', 'project_change_req_type_code', $val, 'Type']
            );
        }

        if ($val = Common::iset($filterData['impact'])) {
            array_push(
                $whereCodes,
                [
                    'project_change_req_impact',
                    'project_change_req_impact_id',
                    'project_change_req_impact_code',
                    $val,
                    'Impact'
                ]
            );
        }

        if ($val = Common::iset($filterData['priority'])) {
            array_push(
                $whereCodes,
                [
                    'project_change_req_priority',
                    'project_change_req_priority_id',
                    'project_change_req_priority_code',
                    $val,
                    'Priority'
                ]
            );
        }

        if ($val = array_get($filterData, 'project')) {
            array_push(
                $whereCodes,
                ['project', 'project_id', 'project_code', $val, 'Project']
            );
        }

        if ($val = Common::iset($filterData['prj_change_req_status_id'])) {
            array_push(
                $whereCodes,
                [
                    'project_change_req_status',
                    'project_change_req_status_id',
                    'project_change_req_status_code',
                    $val,
                    'Project Change Request Status'
                ]
            );
        }

        $prjChangeReqStatusTypes = [];
        if (Common::iset($filterData['draft'])) {
            $prjChangeReqStatusTypes[] = ProjectChangeReqStatusType::DRAFT;
        }
        if (Common::iset($filterData['approved'])) {
            $prjChangeReqStatusTypes[] = ProjectChangeReqStatusType::APPROVED;
        }
        if (Common::iset($filterData['rejected'])) {
            $prjChangeReqStatusTypes[] = ProjectChangereqStatusType::REJECTED;
        }
        if (Common::iset($filterData['cancelled'])) {
            $prjChangeReqStatusTypes[] = ProjectChangeReqStatusType::CANCELLED;
        }
        if (count($prjChangeReqStatusTypes)) {
            array_push($orCodes, [
                'project_change_req_status_type',
                'project_change_req_status_type_id',
                'project_change_req_status_type_code',
                implode(',', $prjChangeReqStatusTypes),
                "Project Change Request Status Type"
            ]);
        }

        return $projectChangeRequestQuery;
    }

    public function reAddProjectCertificatedPaymentsQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $certService = new ProjectCertificatedPaymentService($this->permissionService);
        $query = $certService->filterAllReport($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'project')) {
            array_push($whereCodes, ['project', 'project_id', 'project_code', $val, "Project"]);
        }

        if ($val = array_get($filterData, 'establishment')) {
            array_push(
                $whereCodes,
                ['establishment', 'establishment_id', 'establishment_code', $val, 'Establishment']
            );
        }

        if ($val = array_get($filterData, 'projectPortfolio')) {
            array_push(
                $whereCodes,
                ['project_portfolio', 'project_portfolio_id', 'project_portfolio_code', $val, 'Project Portfolio']
            );
        }

        if ($val = array_get($filterData, 'projectPriority')) {
            array_push(
                $whereCodes,
                ['project_priority', 'project_priority_id', 'project_priority_code', $val, 'Project Priority']
            );
        }

        if ($val = array_get($filterData, 'projectProgramme')) {
            array_push(
                $whereCodes,
                ['project_programme', 'project_programme_id', 'project_programme_code', $val, 'Project Programme']
            );
        }

        if ($val = array_get($filterData, 'projectType')) {
            array_push(
                $whereCodes,
                ['project_type', 'project_type_id', 'project_type_code', $val, 'Type']
            );
        }

        if ($val = array_get($filterData, 'project_status_id')) {
            array_push(
                $whereCodes,
                ['project_status', 'project_status_id', 'project_status_code', $val, 'Project Status']
            );
        }

        $projectStatusTypes = [];
        if (Common::iset($filterData['draft'])) {
            $projectStatusTypes[] = ProjectStatusType::DRAFT;
        }
        if (Common::iset($filterData['active'])) {
            $projectStatusTypes[] = ProjectStatusType::ACTIVE;
        }
        if (Common::iset($filterData['complete'])) {
            $projectStatusTypes[] = ProjectStatusType::COMPLETE;
        }
        if (Common::iset($filterData['rejected'])) {
            $projectStatusTypes[] = ProjectStatusType::REJECTED;
        }
        if (count($projectStatusTypes)) {
            array_push($orCodes, [
                'project_status_type',
                'project_status_type_id',
                'project_status_type_code',
                implode(',', $projectStatusTypes),
                "Project Status Type"
            ]);
        }

        if ($val = array_get($filterData, 'certCode')) {
            array_push($whereTexts, sprintf("Certificated Payment Code contains '%s'", $val));
        }

        if ($val = array_get($filterData, 'certDateFrom')) {
            array_push($whereTexts, "Certificated Payment Date from '{$val}'");
        }

        if ($val = array_get($filterData, 'certDateTo')) {
            array_push($whereTexts, "Certificated Payment Date to '{$val}'");
        }

        return $query;
    }

    public function reAddProjectBillOfQtyQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $prjService = new ProjectService($this->permissionService);
        $prjBillOfQtyService = new ProjectBillOfQtyService($this->permissionService, $prjService);
        $prjBillOfQtyQuery = $prjBillOfQtyService->filterAll($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'project')) {
            array_push(
                $whereCodes,
                ['project', 'project_id', 'project_code', $val, 'Project']
            );
        }

        return $prjBillOfQtyQuery;
    }

    public function reAddProjectCashFlowQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $projectCashFlowService = new ProjectCashFlowService($this->permissionService);
        $projectCashFlowQuery = $projectCashFlowService->filterForReportPRJ10($query, $filterData);

        if ($val = array_get($filterData, 'finYear')) {
            array_push(
                $whereCodes,
                ['fin_year', 'fin_year_id', 'fin_year_code', $val, 'Financial Year']
            );
        }

        if ($val = Common::iset($filterData['account'])) {
            array_push($whereCodes, [
                'fin_account',
                'fin_account_id',
                'fin_account_code',
                $val, \Lang::get('text.account')
            ]);
        }

        return $projectCashFlowQuery;
    }

    public function projectAdditionalContacts()
    {
        // Get Project Contact Labels for use in report header.
        $projectContactLabels = $this->getProjectContactLabels();
        $projectContactLabelString = $this->getProjectContactLabelStringOrArray($projectContactLabels);

        // *****************************************************************
        // REPORT HEADER
        // Build report header
        // *****************************************************************
        $queryRpt = $this->reportHeader($projectContactLabelString);

        $this->siteGroupId = \Auth::user()->site_group_id;

        $query = $this->getProjects($projectContactLabels);
        $queryRpt->unionAll(\DB::table(null)->select('*')->from(\DB::raw("(" . $query . ') AS tmp')));

        $projectContactLabelArray = $this->getProjectContactLabelStringOrArray($projectContactLabels, true);

        return \DB::table(null)
                ->select(array_merge(['Project Code',
                    'Project Title',
                    'Project Description',
                    'Project Owner',
                    'Portfolio Code',
                    'Portfolio Description'
                    ], $projectContactLabelArray))
                ->from(\DB::raw("(" . $queryRpt->toSql() . ') AS report'))
                ->where('Project Code', '<>', "")
                ->orderBy('Project Code');
    }

    public function reAddProjAdditionalContactsQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {

        $query = $this->projectAdditionalContactFilterAll($query, $filterData);

        if ($val = array_get($filterData, 'code')) {
            array_push($whereTexts, "Code contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'title')) {
            array_push($whereTexts, "Title contains '" . $val . "'");
        }


        if ($val = array_get($filterData, 'description')) {
            array_push($whereTexts, "Description contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'owner')) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Owner']);
        }

        if ($val = array_get($filterData, 'projectPortfolio')) {
                array_push(
                    $whereCodes,
                    ['project_portfolio', 'project_portfolio_id', 'project_portfolio_code', $val, 'Project Portfolio']
                );
        }

        return $query;
    }

    public function reAddProjectTaskQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $projectTaskService = new ProjectTaskService($this->permissionService);
        $projectQuery = $projectTaskService->filterAll($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'taskCode')) {
            array_push($whereTexts, "Task Code contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'taskTitle')) {
            array_push($whereTexts, "Task Title contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'taskAllocated')) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Task Allocated To']);
        }

        if ($val = array_get($filterData, 'taskStartDateFrom')) {
            array_push($whereTexts, "Task Start Date from $val");
        }

        if ($val = array_get($filterData, 'taskStartDateTo')) {
            array_push($whereTexts, "Task Start Date to $val");
        }

        if ($val = array_get($filterData, 'taskEndDateFrom')) {
            array_push($whereTexts, "Task End Date from $val");
        }

        if ($val = array_get($filterData, 'taskEndDateTo')) {
            array_push($whereTexts, "Task End Date to $val");
        }

        if ($val = array_get($filterData, 'taskType')) {
            array_push(
                $whereCodes,
                ['project_task_type', 'project_task_type_id', 'project_task_type_code', $val, 'Task Type']
            );
        }

        $taskStatuses = [];
        if (Common::iset($filterData['taskDraft'])) {
            $taskStatuses[] = ProjectTaskStatus::DRAFT;
        }
        if (Common::iset($filterData['taskInProgress'])) {
            $taskStatuses[] = ProjectTaskStatus::IN_PROGRESS;
        }
        if (Common::iset($filterData['taskComplete'])) {
            $taskStatuses[] = ProjectTaskStatus::COMPLETE;
        }
        if (Common::iset($filterData['taskClosed'])) {
            $taskStatuses[] = ProjectTaskStatus::CLOSED;
        }
        if (count($taskStatuses)) {
            array_push($orCodes, [
                'project_task_status',
                'project_task_status_id',
                'project_task_status_code',
                implode(',', $taskStatuses),
                "Task Status"
            ]);
        }

        if ($val = array_get($filterData, 'taskDiscipline')) {
            array_push(
                $whereCodes,
                [
                    'project_task_discipline',
                    'project_task_discipline_id',
                    'project_task_discipline_code',
                    $val,
                    'Task Discipline'
                ]
            );
        }

        if ($val = array_get($filterData, 'project')) {
            array_push($whereCodes, ['project', 'project_id', 'project_code', $val, 'Project']);
        }

        if ($val = array_get($filterData, 'project_status_id')) {
            array_push(
                $whereCodes,
                ['project_status', 'project_status_id', 'project_status_code', $val, 'Project Status']
            );
        }

        if ($val = array_get($filterData, 'projectPortfolio')) {
            array_push(
                $whereCodes,
                ['project_portfolio', 'project_portfolio_id', 'project_portfolio_code', $val, 'Project Portfolio']
            );
        }

        if ($val = array_get($filterData, 'projectOwner')) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Project Owner']);
        }

        if ($val = array_get($filterData, 'establishment')) {
            array_push(
                $whereCodes,
                ['establishment', 'establishment_id', 'establishment_code', $val, 'Establishment']
            );
        }

        if ($val = array_get($filterData, 'projectPriority')) {
            array_push(
                $whereCodes,
                ['project_priority', 'project_priority_id', 'project_priority_code', $val, 'Project Priority']
            );
        }

        if ($val = array_get($filterData, 'projectProgramme')) {
            array_push(
                $whereCodes,
                ['project_programme', 'project_programme_id', 'project_programme_code', $val, 'Project Programme']
            );
        }

        if ($val = array_get($filterData, 'projectType')) {
            array_push(
                $whereCodes,
                ['project_type', 'project_type_id', 'project_type_code', $val, 'Type']
            );
        }

        return $projectQuery;
    }

    //only for issue, action and
    private function getQuery($projectQuery, $viewName, $recordType)
    {
        $projectQuery->distinct();

        // CLD-12175: I don't think this is used.  Joining onto a view is slow so remove.
        //$projectQuery->leftJoin('project_user as pu', 'pu.project_id', '=', $viewName . '.project_id');


        /*  Interrim solution for CLD-5720
        $projectQuery->leftJoin(
            'project_user_access as pua',
            function ($join) use ($viewName, $recordType) {
                $join->on('pua.project_id', '=', \DB::raw($viewName .'.project_id'))
                ->on('pua.project_record_type_id', '=', \DB::raw($recordType))
                ->on('pua.record_id', '=', "$viewName.record_id");
            }
        );
        $projectQuery->where(function ($query) use ($viewName) {
            $query->where(function ($subQuery) {
                $subQuery->where('all_users_have_access', CommonConstant::DATABASE_VALUE_YES);
            });
            $query->orWhere(function ($subQuery) use ($viewName) {
                $subQuery->where('all_users_have_access', CommonConstant::DATABASE_VALUE_NO);
                $subQuery->where("$viewName.Record Access Restricted", CommonConstant::DATABASE_VALUE_YES);
                $subQuery->where('pua.user_id', \Auth::user()->id);
            });
        });
        */

        return $projectQuery;
    }

    private function reportHeader($projectContactLabelString)
    {
        $query = " '' as `Project Code`";
        $query .= ", '' as `Project Title`";
        $query .= ", '' as `Project Description`";
        $query .= ", '' as `owner_user_id`";
        $query .= ", '' as `Project Owner`";
        $query .= ", '' as `project_portfolio_id`";
        $query .= ", '' as `Portfolio Code`";
        $query .= ", '' as `Portfolio Description`";

//        foreach ($arrProjectContactLabel as $key => $label) {
//            $query .= ", '' as `$label Name`, '' as `$label Organisation`";
//        }

        $query .= "$projectContactLabelString";

        return \DB::table(null)->select(\DB::raw($query));
    }

    private function addContactSelectSubQuery($labelId, $label)
    {
        $subQuery = "(";
        $subQuery .= " SELECT contact.contact_name "
                . "from project_contact "
                . "inner join contact "
                . "on contact.contact_id = project_contact.contact_id "
                . "WHERE project_contact.project_id = project.project_id "
                . "AND project_contact.project_contact_label_id = $labelId";
        $subQuery .= ") as '{$label}_name'";
        $subQuery .= ", (";
        $subQuery .= " SELECT contact.organisation"
                . " from project_contact"
                . " inner join contact on contact.contact_id = project_contact.contact_id"
                . " WHERE project_contact.project_id = project.project_id "
                . "AND project_contact.project_contact_label_id = $labelId";
        $subQuery .= ") as '{$label}_organisation'";
        return $subQuery;
    }

    private function getProjects($projectContactLabels)
    {
        $query =  "SELECT `project`.`project_code` AS `Project Code`"
                . ",`project`.`project_title` AS `Project Title`"
                . ",`project`.`project_desc` AS `Project Description` "
                . ",`project`.`owner_user_id` AS `owner_user_id` "
                . ",`user_project_owner`.`display_name` AS `Project Owner` "
                . ",`project`.`project_portfolio_id` AS `project_portfolio_id` "
                . ",`project_portfolio`.`project_portfolio_code` AS `Portfolio Code` "
                . ",`project_portfolio`.`project_portfolio_desc` AS `Portfolio Description` ";

        foreach ($projectContactLabels as $labelId => $label) {
            $query .= "," . $this->addContactSelectSubQuery($labelId, $label);
        }

        $query .= "FROM project";
        $query .= " LEFT JOIN `user` AS `user_project_owner` on project.owner_user_id = user_project_owner.id";
        $query .= " LEFT JOIN `project_portfolio`  ON  `project`.`project_portfolio_id` = "
            . "`project_portfolio`.`project_portfolio_id`";
        if (
            !\Auth::user()->isSuperuser() && !$this->permissionService->hasModuleAccess(
                Module::MODULE_PROJECT,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_SUPERVISOR
            )
        ) {
            $query .= "WHERE (`project`.`all_users_have_access` = 'Y' OR `project`.`owner_user_id` = "
                . \Auth::user()->getKey() . " OR `project`.`project_id` IN"
                . " (SELECT `project_id` FROM `project_user` WHERE `user_id` = "
                . \Auth::user()->getKey() . "))";
        }

        return $query;
    }

    private function getProjectContactLabels()
    {
        $arrProjContLabels = array();   // Holds array of project contact labels.

        // Get available project contact labels from project_contact_label to build dynamic contact columns
        $query = \Tfcloud\Models\ProjectContactLabel::userSiteGroup()
        ->select(
            [
                'project_contact_label_id',
                'label',
            ]
        )
        ->where(
            'active',
            '=',
            CommonConstant::DATABASE_VALUE_YES
        );
        $query->orderBy('label');

        $queryProjContLabels = $query->get();

        // Loop through all contact labels and store in array.
        foreach ($queryProjContLabels as $row) {
            $bLabel = '';
            $bLabel = $row['label'];
            $arrProjContLabels[$row['project_contact_label_id']] = $bLabel;
        }

        return $arrProjContLabels;
    }

    private function getProjectContactLabelStringOrArray($arrProjectContactLabel, $returnLabelArray = false)
    {
        $string = "";

        $projectLabelArray = [];

        foreach ($arrProjectContactLabel as $key => $label) {
            if ($returnLabelArray) {
                $projectLabelArray[] = "$label Name";
                $projectLabelArray[] = "$label Organisation";
            } else {
                $string .= ", '' as `$label Name`, '' as `$label Organisation`";
            }
        }

        if ($returnLabelArray) {
            return $projectLabelArray;
        }

        return $string;
    }

    private function projectAdditionalContactFilterAll($query, $inputs)
    {
        $filter = new \Tfcloud\Lib\Filters\Project\ProjectFilter($inputs);

        if (!is_null($filter->code)) {
            $query->where('Project Code', $filter->code);
        }

        if (!is_null($filter->title)) {
            $query->where('Project Title', 'like', '%' . $filter->title . '%');
        }

        if (!is_null($filter->description)) {
            $query->where('Project Description', 'like', '%' . $filter->description . '%');
        }

        if (!is_null($filter->owner)) {
            $query->where('owner_user_id', $filter->owner);
        }

        if (!is_null($filter->projectPortfolio)) {
            $query->where('project_portfolio_id', $filter->projectPortfolio);
        }

        return $query;
    }

    /**
     * @deprecated switch to use new filter
     */
    public function reAddProjectFundingSourcesQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $fundingSourceService = new FundingSourceService($this->permissionService);
        $query = $fundingSourceService->filterAll($query, $filterData, $viewName);

        if ($description = array_get($filterData, 'code')) {
            $text = "Code contains '{$description}'";
            array_push($whereTexts, $text);
        }

        if ($description = array_get($filterData, 'description')) {
            $text = "Description contains '{$description}'";
            array_push($whereTexts, $text);
        }

        if ($val = array_get($filterData, 'startDateFrom')) {
            array_push($whereTexts, "Start Date from $val");
        }

        if ($val = array_get($filterData, 'startDateTo')) {
            array_push($whereTexts, "Start Date to $val");
        }

        if ($val = array_get($filterData, 'endDateFrom')) {
            array_push($whereTexts, "End Date from $val");
        }

        if ($val = array_get($filterData, 'endDateTo')) {
            array_push($whereTexts, "End Date to $val");
        }

        return $query;
    }
}
