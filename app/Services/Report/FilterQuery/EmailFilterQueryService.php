<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\Common;
use Tfcloud\Models\Module;
use Tfcloud\Models\UserSiteAccess;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\EmailReportService;
use Tfcloud\Services\PermissionService;

class EmailFilterQueryService extends BaseService
{
    protected $permissionService;
    protected $emailReportService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
        $this->emailReportService = new EmailReportService($this->permissionService);
    }

    public function reAddADM36Query(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$whereTexts
    ) {

        $query = $this->emailReportService->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['module'])) {
            array_push(
                $whereCodes,
                ['module', 'module_id', 'module_description', $val, 'Module']
            );
        }

        if ($val = Common::iset($filterData['user'])) {
            array_push(
                $whereCodes,
                ['user', 'id', 'display_name', $val, 'User']
            );
        }

        if ($val = array_get($filterData, 'emailDateFrom', null)) {
            array_push($whereTexts, "Email Date From '" . " $val" . "'");
        }

        if ($val = array_get($filterData, 'emailDateTo', null)) {
            array_push($whereTexts, "Email Date To '" . " $val" . "'");
        }


        return $query;
    }

    public function reAddADM38Query(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$whereTexts
    ) {
        $user = \Auth::user();
        // Check module access

        $query = $query->leftjoin(
            'usergroup_module',
            'usergroup_module.module_id',
            '=',
            "{$viewName}.module_id"
        )
            ->where('usergroup_module.usergroup_id', $user->usergroup_id)
            ->where(\DB::raw("IFNULL(usergroup_module.crud_access_level_id, 0)"), '>', Module::MODULE_ACCESS_NONE);

        // check site access
        $siteAccess = $user->site_access;

        if ($siteAccess == UserSiteAccess::USER_SITE_ACCESS_ALL) {
            // avoid duplicate record.
            $query->groupBy("{$viewName}.email_id");
        } elseif ($siteAccess == UserSiteAccess::USER_SITE_ACCESS_SELECTED) {
            // don't show estate record if site access is selected
            $query->where("{$viewName}.module_id", '!=', Module::MODULE_ESTATES);

            $query->where(function ($where) use ($user, $viewName) {
                // show all contract record if site access is selected
                $where->where(
                    "{$viewName}.module_id",
                    Module::MODULE_CONTRACT
                );

                $siteAccessLists = $user->userAccesses->pluck(['site_id'])->toArray();
                // also select record without site_id
                $where->orWhere(function ($where) use ($siteAccessLists, $viewName) {
                    $where->where("{$viewName}.module_id", '<>', Module::MODULE_INVOICING);
                    $where->where(function ($where) use ($siteAccessLists, $viewName) {
                        $where->whereIn("{$viewName}.site_id", $siteAccessLists);
                        $where->orWhereNull("{$viewName}.site_id");
                    });
                });

                // don't select record without site_id
                $where->orWhere(function ($where) use ($siteAccessLists, $user, $viewName) {
                    $where->where("{$viewName}.module_id", Module::MODULE_INVOICING);
                    $where->whereIn("{$viewName}.site_id", $siteAccessLists);
                });
            });
        } elseif ($siteAccess == UserSiteAccess::USER_SITE_ACCESS_NONE) {
            $query->whereRaw('1 = 0');
        }

        $query = $this->emailReportService->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['module'])) {
            array_push(
                $whereCodes,
                ['module', 'module_id', 'module_description', $val, 'Module']
            );
        }

        if ($val = Common::iset($filterData['user'])) {
            array_push(
                $whereCodes,
                ['user', 'id', 'display_name', $val, 'User']
            );
        }

        if ($val = Common::iset($filterData['recordCode'])) {
            array_push($whereTexts, "Record Code contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'emailDateFrom', null)) {
            array_push($whereTexts, "Email Date From = '" . " $val" . "'");
        }

        if ($val = array_get($filterData, 'emailDateTo', null)) {
            array_push($whereTexts, "Email Date To = '" . " $val" . "'");
        }

        if ($val = array_get($filterData, 'email_from', null)) {
            array_push($whereTexts, "Email From = '" . " $val" . "'");
        }

        if ($val = array_get($filterData, 'email_to', null)) {
            array_push($whereTexts, "Email To = '" . " $val" . "'");
        }

        if ($val = array_get($filterData, 'email_message', null)) {
            array_push($whereTexts, "Email Body = '" . " $val" . "'");
        }

        if ($val = array_get($filterData, 'email_subject', null)) {
            array_push($whereTexts, "Email Subject ='" . " $val" . "'");
        }

        if ($val = array_get($filterData, 'success', null)) {
            array_push($whereTexts, "Success = '" . " $val" . "'");
        }

        if ($val = array_get($filterData, 'incoming', null)) {
            array_push($whereTexts, "Incoming =  '" . " $val" . "'");
        }

        return $query;
    }
}
