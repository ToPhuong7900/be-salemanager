<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\Filters\BaseFilter;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\PupilPlacePlanning\PlacementService;
use Tfcloud\Services\BaseService;

class PlacementsFilterQueryService extends BaseService
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    public function reAddPlacementQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $placement = new PlacementService(
            $this->permissionService
        );
        $query = $placement->filterAllPlacements($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'postcode', null)) {
            array_push($whereTexts, \Lang::get('text.postcode') . " contains '$val'");
        }

        if ($val = array_get($filterData, 'academic_year', null)) {
            array_push($whereCodes, [
                'academic_year',
                'academic_year_id',
                'academic_year_code',
                $val,
                \Form::fieldLang('academicYear')
            ]);
        }

        if ($val = array_get($filterData, 'from', null)) {
            array_push($whereTexts, 'Date of Birth ' . \Lang::get('text.from') . " " . $val);
        }

        if ($val = array_get($filterData, 'to', null)) {
            array_push($whereTexts, 'Date of Birth ' . \Lang::get('text.to') . " " . $val);
        }

        if ($val = array_get($filterData, 'location', null)) {
            array_push($whereCodes, [
                'site',
                'site_id',
                'site_code',
                $val,
                \Form::fieldLang('location')
            ]);
        }

        $val = array_get($filterData, 'catholic', null);
        if (!is_null($val)) {
            if ($val == '1') {
                array_push($whereTexts, \Lang::get('text.catholic') . " contains Catholic");
            } elseif ($val == '0') {
                array_push($whereTexts, \Lang::get('text.catholic') . " contains Other");
            }
        }

        $val = array_get($filterData, 'applied', null);
        if (!is_null($val)) {
            if ($val == '1') {
                array_push($whereTexts, \Lang::get('text.applied') . " = Yes");
            } elseif ($val == '0') {
                array_push($whereTexts, \Lang::get('text.applied') . " = No");
            }
        }

        $val = array_get($filterData, 'allocated', null);
        if (!is_null($val)) {
            if ($val == '1') {
                array_push($whereTexts, \Lang::get('text.allocated') . " = Yes");
            } elseif ($val == '0') {
                array_push($whereTexts, \Lang::get('text.allocated') . " = No");
            }
        }

        return $query;
    }
}
