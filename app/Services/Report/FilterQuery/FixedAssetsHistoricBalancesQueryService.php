<?php

/**
 * FAR38
 *
 *
 */

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\PermissionService;

class FixedAssetsHistoricBalancesQueryService extends BaseService
{
    protected $permissionService;

    protected $siteGroupId;
    protected $bIncludeRRTrans = false;
    protected $rrTrans = 'N';
    protected $bSumNegative = false;

    // filters
    protected $commonGood = false;
    protected $investment = false;
    protected $deminimis = false;
    protected $filterData = [];

    // detailed report
    protected $detailed = true;

    public function __construct(PermissionService $permissionService, $filterData, $repCode)
    {
        $this->permissionService = $permissionService;

        // get filter values
        $commonGood = $filterData['common_good'] ?? false;
        $investment = $filterData['investment'] ?? false;
        $deminimis = $filterData['deminimis_asset'] ?? CommonConstant::DATABASE_VALUE_NO;

        // set properties
        $this->filterData = $filterData;
        $this->commonGood = ($commonGood == 'All') ? false : $commonGood;
        $this->investment = ($investment == 'All') ? false : $investment;
        $this->deminimis = ($deminimis == 'All') ? false : $deminimis;
        $this->detailed = ($repCode == 'FAR31') ? true : false;
    }

    public function getNumericCols()
    {
        $numCols = [1, 2, 3, 4, 5 ,6, 7, 8, 9, 10];

        return $numCols;
    }

    public function historicBalancesQuery(
        $inputs,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $this->siteGroupId = \Auth::user()->site_group_id;

        // *****************************************************************
        // REPORT HEADER
        // *****************************************************************
        // Build report header
        $queryRpt = $this->reportHeader(false);

        $caFixedAssetId = array_get($inputs, 'ca_fixed_asset_id', false);

        // Current Values
        $queryRpt->unionAll($this->buildBalanceQuery(false, $caFixedAssetId));

        // Historic Costs
        $queryRpt->unionAll($this->sectionHeader());
        $queryRpt->unionAll($this->balanceHeader(true));
        $queryRpt->unionAll($this->sectionHeader());
        $queryRpt->unionAll($this->buildBalanceQuery(true, $caFixedAssetId));

        // Revaluation Reserve
        $queryRpt->unionAll($this->sectionHeader());
        $queryRpt->unionAll($this->balanceRRHeader());
        $queryRpt->unionAll($this->sectionHeader());
        $queryRpt->unionAll($this->buildBalanceRRQuery($caFixedAssetId));

        $this->setFilterTexts($whereCodes, $orCodes, $whereTexts);

        return \DB::table(null)->select('*')->from(\DB::raw("(" . $queryRpt->toSql() . ') AS report'));
    }

    private function setFilterTexts(
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        if ($val = Common::iset($this->filterData['deminimis_asset'])) {
            switch ($val) {
                case CommonConstant::ALL:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.deminimis_asset_all'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.deminimis_asset_no'));
                    break;
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.deminimis_asset_yes'));
                    break;
            }
        }

        if ($val = Common::iset($this->filterData['common_good'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.common_good_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.common_good_no'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.common_good_all'));
                    break;
            }
        }


        if ($val = Common::iset($this->filterData['investment'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.investment_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.investment_no'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.investment_all'));
                    break;
            }
        }
    }

    private function reportHeader($historic)
    {
        $query = $historic ? " '' AS 'Historic Cost'" :  " '' AS 'Current Value'";
        $query .= ",'' AS 'Year' ";
        $query .= ",'' AS  'BFwd GBV' ";
        $query .= ",'' AS  'Accum Depn' ";
        $query .= ",'' AS  'Accum Imp' ";
        $query .= $historic ? ",'' AS  'Accum Loss' " : ", '' AS  'Accum Loss' ";
        $query .= ",'' AS  'BFwd NBV' ";
        $query .= ",'' AS  'CY Depn' ";
        $query .= ",'' AS  'CFwd GBV' ";
        $query .= ",'' AS  'CFwd Asset' ";
        $query .= ",'' AS  'CFwd Land' ";
        $query .= ",'' AS  'CFwd NBV' ";
        $query .= ",'' AS  'Residual Value' ";

        return \DB::table(null)->select(\DB::raw($query));
    }


    private function balanceHeader($historic)
    {
        $query = $historic ? " 'Historic Cost'" :  " 'CurrentValue'";
        $query .= ", 'Year' ";
        $query .= ", 'BFwd GBV' ";
        $query .= ", 'Accum Depn' ";
        $query .= ", 'Accum Imp' ";
        $query .= $historic ? ",  'Accum Loss' " : ",  '_' ";
        $query .= ", 'BFwd NBV' ";
        $query .= ", 'CY Depn' ";
        $query .= ", 'CFwd GBV' ";
        $query .= ", 'CFwd Asset' ";
        $query .= ", 'CFwd Land' ";
        $query .= ", 'CFwd NBV' ";
        $query .= ", 'Residual Value' ";

        return \DB::table(null)->select(\DB::raw($query));
    }

    private function balanceRRHeader()
    {
        $query =  " 'Revaluation Reserve'";
        $query .= ", 'Year' ";
        $query .= ", 'Asset' ";
        $query .= ", 'Land' ";
        $query .= ", 'BFwd RR' ";
        $query .= ", 'CY Depn' ";
        $query .= ", 'CFwd Asset' ";
        $query .= ", 'CFwd Land' ";
        $query .= ", 'CFwd RR' ";
        $query .= ", '' ";
        $query .= ", '' ";
        $query .= ", '' ";
        $query .= ", '' ";

        return \DB::table(null)->select(\DB::raw($query));
    }

    private function sectionHeader()
    {
        $query = " '' AS Col1";
        $query .= ", '' AS Col2";
        $query .= ", '' AS Col3";
        $query .= ", '' AS Col4";
        $query .= ", '' AS Col5";
        $query .= ", '' AS Col6";
        $query .= ", '' AS Col7";
        $query .= ", '' AS Col8";
        $query .= ", '' AS Col9";
        $query .= ", '' AS Col10";
        $query .= ", '' AS Col11";
        $query .= ", '' AS Col12";
        $query .= ", '' AS Col13";

        return \DB::table(null)->select(\DB::raw($query));
    }



    private function buildBalanceQuery($bHistoric, $caFixedAssetId)
    {
        $query = "   ''                                 AS Col1                                        , ";
        $query .= "   `fin_year_code`                                                                         , ";
        $query .= "         FORMAT((ca_historic_balance.bfwd_gbv), 2)                            AS bfwd_gbv       , ";
        $query .= "         FORMAT((ca_historic_balance.accum_depreciation), 2)                                        "
            . "AS accum_depn     , ";
        $query .= "         FORMAT(SUM(ca_historic_balance.accum_imp_asset  + ca_historic_balance.accum_imp_land), 2) "
            . "AS accum_imp      , ";
        if ($bHistoric) {
            $query .= "    FORMAT(SUM(ca_historic_balance.accum_loss_asset + ca_historic_balance.accum_loss_land), 2) "
                . "AS accum_loss     , ";
        } else {
            $query .= " 'N/A' AS accum_loss, ";
        }
        $query .= "         FORMAT((ca_historic_balance.bfwd_nbv), 2)                               bfwd_nbv       , ";
        $query .= "         FORMAT((ca_historic_balance.depreciation), 2)                           cy_depreciation, ";
        $query .= "         FORMAT((ca_historic_balance.cfwd_gbv), 2)                               cfwd_gbv       , ";
        $query .= "         FORMAT((ca_historic_balance.cfwd_asset), 2)                             cfwd_gbv_asset , ";
        $query .= "         FORMAT((ca_historic_balance.cfwd_land), 2)                              cfwd_gbv_land  , ";
        $query .= "         FORMAT((ca_historic_balance.cfwd_nbv), 2)                               cfwd_nbv       , ";
        $query .= "         FORMAT((ca_historic_balance.residual_value), 2)                         residual_value ";
        $query .= "FROM     `ca_historic_balance` ";
        $query .= "         INNER JOIN `ca_fixed_asset` ";
        $query .= "         ON       `ca_fixed_asset`.`ca_fixed_asset_id` = `ca_historic_balance`.`ca_fixed_asset_id` ";
        $query .= "WHERE    `ca_historic_balance`.`site_group_id`         = $this->siteGroupId ";
        $query .= "AND      `ca_historic_balance`.`ca_fixed_asset_id`     = $caFixedAssetId ";
        if ($bHistoric) {
            $query .= "AND      `ca_historic_balance`.`historic`              = 'Y' ";
        } else {
            $query .= "AND      `ca_historic_balance`.`historic`              = 'N' ";
        }

        $query .= "GROUP BY `ca_historic_balance`.`fin_year_code` ";
        $query .= "ORDER BY `ca_historic_balance`.`year_start_date` DESC";


        return \DB::table(null)->select(\DB::raw($query));
    }


    private function buildBalanceRRQuery($caFixedAssetId)
    {
        $query = " '' AS Col1,   `fin_year_code`                                                        , ";
        $query .= "         FORMAT((ca_historic_balance_rr.bfwd_asset_value), 2) bfwd_rr_asset     , ";
        $query .= "         FORMAT((ca_historic_balance_rr.bfwd_land_value), 2)  bfwd_rr_land      , ";
        $query .= "         FORMAT((ca_historic_balance_rr.bfwd_rr), 2)          bfwd_rr           , ";
        $query .= "         FORMAT((ca_historic_balance_rr.depreciation), 2)     cy_rr_depreciation, ";
        $query .= "         FORMAT((ca_historic_balance_rr.cfwd_asset_value), 2) cfwd_rr_asset     , ";
        $query .= "         FORMAT((ca_historic_balance_rr.cfwd_land_value), 2)  cfwd_rr_land      , ";
        $query .= "         FORMAT((ca_historic_balance_rr.cfwd_rr), 2)          cfwd_rr, ";
        $query .= "         '' AS Col10, ";
        $query .= "         '' AS Col11, ";
        $query .= "         '' AS Col12, ";
        $query .= "         '' AS Col13 ";
        $query .= "FROM     `ca_historic_balance_rr` ";
        $query .= "         INNER JOIN `ca_fixed_asset` ";
        $query .= "         ON   `ca_fixed_asset`.`ca_fixed_asset_id` = `ca_historic_balance_rr`.`ca_fixed_asset_id` ";
        $query .= "WHERE    `ca_historic_balance_rr`.`site_group_id`      = $this->siteGroupId ";
        $query .= "AND      `ca_historic_balance_rr`.`ca_fixed_asset_id`  = $caFixedAssetId ";
        $query .= "GROUP BY `ca_historic_balance_rr`.`fin_year_code` ";
        $query .= "ORDER BY `ca_historic_balance_rr`.`year_start_date` DESC";


        return \DB::table(null)->select(\DB::raw($query));
    }
}
