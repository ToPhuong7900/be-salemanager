<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Constant\GmConstant;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\GroundsMaintenance\GmContractExternalAreaTaskService;
use Tfcloud\Services\GroundsMaintenance\GmContractJobService;
use Tfcloud\Services\GroundsMaintenance\GMContractService;
use Tfcloud\Services\PermissionService;

class GroundsMaintenanceFilterQueryService extends BaseService
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    public function reAddGroundsMaintenanceContractQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $gmExtTaskQuery = (new GMContractService($this->permissionService))->filterAll($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'code')) {
            array_push($whereTexts, "Code contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'name')) {
            array_push($whereTexts, "Name contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'description')) {
            array_push($whereTexts, "Description contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['owner'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Owner']);
        }

        if ($val = Common::iset($filterData['supplier'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, \Lang::get('text.supplier')]);
        }

        if ($val = array_get($filterData, 'active')) {
            if ($val != 'all') {
                array_push($whereTexts, "Active = '" . ucwords($val) . "'");
            }
        }


        return $gmExtTaskQuery;
    }

    public function reAddGroundsMaintenanceExtTaskQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $gmExtTaskQuery = GmContractExternalAreaTaskService::filterAll(null, $filterData, $query, $viewName);

        if ($val = array_get($filterData, 'gm_task_code')) {
            array_push($whereTexts, "Task Code contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'gm_task_desc')) {
            array_push($whereTexts, "Task Description contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'job_start_date_from', null)) {
            array_push($whereTexts, sprintf("Job start date from '%s'", $val));
        }

        if ($val = array_get($filterData, 'job_start_date_to', null)) {
            array_push($whereTexts, sprintf("Job start date to '%s'", $val));
        }

        if ($val = array_get($filterData, 'plant_id', null)) {
            array_push($whereCodes, ['plant', 'plant_id', 'plant_code', $val, 'Plant']);
        }

        if ($val = array_get($filterData, 'gm_task_group_id', null)) {
            array_push($whereCodes, [
                'gm_task_group',
                'gm_task_group_id',
                'gm_task_group_code',
                $val,
                'Task Group'
            ]);
        }
        if ($val = array_get($filterData, 'plantTaskCheck', null)) {
            if ($val == CommonConstant::DATABASE_VALUE_YES) {
                array_push($whereTexts, " Plant Tasks is Yes");
            } elseif ($val == CommonConstant::DATABASE_VALUE_NO) {
                array_push($whereTexts, " Plant Tasks is No");
            }
        }
        if ($val = array_get($filterData, 'prop_external_id')) {
            array_push(
                $whereCodes,
                ['prop_external', 'prop_external_id', 'prop_external_code', $val, "External Area Code"]
            );
        }

        if ($val = array_get($filterData, 'prop_zone_id')) {
            array_push(
                $whereCodes,
                ['prop_zone', 'prop_zone_id', 'prop_zone_code', $val, "Zone Code"]
            );
        }
        if ($val = array_get($filterData, 'task_type', null)) {
            if ($val == GmConstant::EXT_AREA_TYPE) {
                array_push($whereTexts, " Task Type is External Area");
            } else {
                array_push($whereTexts, ' Task Type is ' . ucfirst($val));
            }
        }

        return $gmExtTaskQuery;
    }

    public function reAddGroundsMaintenanceJobQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $gmJobQuery = GmContractJobService::filterAll($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'prop_zone_id')) {
            array_push(
                $whereCodes,
                ['prop_zone', 'prop_zone_id', 'prop_zone_code', $val, "Zone Code"]
            );
        }

        if ($val = array_get($filterData, 'prop_external_id')) {
            array_push(
                $whereCodes,
                ['prop_external', 'prop_external_id', 'prop_external_code', $val, "External Area Code"]
            );
        }

        if ($val = array_get($filterData, 'plant_id', null)) {
            array_push($whereCodes, ['plant', 'plant_id', 'plant_code', $val, 'Plant']);
        }

        if ($val = array_get($filterData, 'code')) {
            array_push($whereTexts, "Code contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'gm_contract_job_due_date_from', null)) {
            array_push($whereTexts, sprintf("Job due date  from '%s'", $val));
        }

        if ($val = array_get($filterData, 'gm_contract_job_due_date_to', null)) {
            array_push($whereTexts, sprintf("Job due date  to '%s'", $val));
        }

        if ($val = array_get($filterData, 'gm_task_group_id', null)) {
            array_push($whereCodes, [
                'gm_task_group',
                'gm_task_group_id',
                'gm_task_group_code',
                $val,
                'Task Group'
            ]);
        }

        if ($val = array_get($filterData, 'gm_task_id', null)) {
            array_push($whereCodes, [
                'gm_task',
                'gm_task_id',
                'gm_task_code',
                $val,
                'Task'
            ]);
        }

        if (!empty($filterData['status'])) {
            array_push($orCodes, [
                'gm_contract_job_status',
                'gm_contract_job_status_id',
                'gm_contract_job_status_code',
                implode(',', $filterData['status']),
                "Status Code"
            ]);
        }


        return $gmJobQuery;
    }
}
