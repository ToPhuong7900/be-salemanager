<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Filters\Audit\AuditFilter;
use Tfcloud\Models\Audit;
use Tfcloud\Models\CrudAccessLevel;
use Tfcloud\Models\Module;
use Tfcloud\Models\Project;
use Tfcloud\Models\UsergroupModule;
use Tfcloud\Models\Views\VwAdm15;
use Tfcloud\Services\Admin\General\ContactService;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\DocumentsService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\AuditService;
use Tfcloud\Models\DocRecordType;

class GeneralsFilterQueryService extends BaseService
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
        $this->docService = new DocumentsService($permissionService);
    }

    public function reAddDocumentQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $documentService = new DocumentsService($this->permissionService);
        $query = $this->docService->limitProjectDocuments($query, $viewName);
        $query = $this->docService->limitCaseDocument($query, $viewName);
        $query = $this->docService->limitEstablishmentDocuments($query, $viewName);
        $query = $this->docService->limitStrategyDocuments($query, $viewName);
        $query = $this->docService->documentSitesAccess($query, $viewName);
        $query = $this->docService->limitIncidentDocument($query, $viewName);
        $query = $this->docService->limitQstDocument($query, $viewName);

        if (\Auth::user()->isSelfServiceAndShowOwnHelpcalls()) {
                // Helpcall documents only showing for helpcalls which the self service user logged or owner of.
                $query->leftJoin('helpcall', 'helpcall.helpcall_id', "$viewName.record_id")
                ->where(function ($subQuery1) use ($viewName) {
                    $subQuery1->where("$viewName.doc_record_type_id", '<>', DocRecordType::HELPCALL)
                        ->orWhere(function ($subQuery2) use ($viewName) {
                            $subQuery2->where("$viewName.doc_record_type_id", '=', DocRecordType::HELPCALL)
                                ->where('helpcall.logged_by', '=', \Auth::User()->getKey())
                                ->orWhere("helpcall.owner_user_id", \Auth::User()->getKey());
                        });
                });

                $query->leftJoin('helpcall_quote', 'helpcall_quote.helpcall_quote_id', "$viewName.record_id")
                        ->leftJoin('helpcall as hlp', 'hlp.helpcall_id', 'helpcall_quote.helpcall_id')
                ->where(function ($subQuery3) use ($viewName) {
                    $subQuery3->where("$viewName.doc_record_type_id", '<>', DocRecordType::HELPCALL_QUOTE)
                        ->orWhere(function ($subQuery4) use ($viewName) {
                            $subQuery4->where("$viewName.doc_record_type_id", '=', DocRecordType::HELPCALL_QUOTE)
                                ->where('hlp.logged_by', '=', \Auth::User()->getKey())
                                ->orWhere("hlp.owner_user_id", \Auth::User()->getKey());
                        });
                });
        }

        if (!$this->permissionService->isAdministratorOrSuperuser()) {
            // For user documents, they must have loaded the document.
            $query->where(function ($subQuery1) use ($viewName) {
                $subQuery1->where("$viewName.doc_record_type_id", '<>', DocRecordType::USER)
                ->orWhere(function ($subQuery2) use ($viewName) {
                    $subQuery2->where("$viewName.doc_record_type_id", '=', DocRecordType::USER)
                        ->where("$viewName.record_id", '=', \Auth::User()->getKey());
                });
            });
        }

        $documentQuery = $documentService->applyFilter($query, $filterData, $viewName);

        $moduleAllowList = UsergroupModule::where('usergroup_id', \Auth::user()->usergroup_id)
            ->where('crud_access_level_id', '>', Module::MODULE_ACCESS_NONE)
            ->pluck('module_id')
            ->toArray();

        $documentQuery = $documentQuery->whereIn('module_id', $moduleAllowList);

        if ($val = Common::iset($filterData['name'])) {
            array_push($whereTexts, "Document name contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['record'])) {
            array_push($whereTexts, "Record contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Document description contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['module'])) {
            array_push(
                $whereCodes,
                ['module', 'module_id', 'module_description', $val, 'Module']
            );
        }

        if ($val = Common::iset($filterData['group'])) {
            array_push(
                $whereCodes,
                ['doc_group', 'doc_group_id', 'doc_group_code', $val, 'Group']
            );
        }
        if ($val = Common::iset($filterData['docSubGroup'])) {
            array_push(
                $whereCodes,
                ['doc_sub_group', 'doc_sub_group_id', 'doc_sub_group_code', $val, 'Sub Group']
            );
        }
        if ($val = array_get($filterData, 'documentDateFrom')) {
            array_push($whereTexts, "Document Date From $val");
        }
        if ($val = array_get($filterData, 'documentDateTo')) {
            array_push($whereTexts, "Document Date To $val");
        }

        if ($val = array_get($filterData, 'nextReviewDateFrom')) {
            array_push($whereTexts, "Next Review From $val");
        }
        if ($val = array_get($filterData, 'nextReviewDateTo')) {
            array_push($whereTexts, "Next Review To $val");
        }

        if ($val = Common::iset($filterData['comment'])) {
            array_push($whereTexts, "Document comment contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'nextReviewOwnerUserId')) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Review Owner']);
        }

        if ($val = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, 'Site Code']);
        }

        if ($val = array_get($filterData, 'readOnly')) {
            array_push($whereTexts, "Read-only is $val");
        }
        if ($val = array_get($filterData, 'archived')) {
            array_push($whereTexts, "Archived is $val");
        }
        if ($val = array_get($filterData, 'overdue')) {
            array_push($whereTexts, "Overdue is $val");
        }

        return $documentQuery;
    }

    public function reAddContactQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {

        $service = new ContactService($this->permissionService);
        $query = $service->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['contactName'])) {
            array_push($whereTexts, "Contact name contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['supplierAcctRef'])) {
            array_push($whereTexts, "Supplier acct ref contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['salesAcctRef'])) {
            array_push($whereTexts, "Sales acct ref contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['active'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.active_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.active_no'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.active_all'));
                    break;
            }
        }

        if ($val = Common::iset($filterData['surveyor'])) {
            array_push($whereTexts, "Surveyor = 'Yes'");
        }

        if ($val = Common::iset($filterData['supplier'])) {
            array_push($whereTexts, "Supplier = 'Yes'");
        }

        if ($val = Common::iset($filterData['landlord'])) {
            array_push($whereTexts, "Landlord = 'Yes'");
        }

        if ($val = Common::iset($filterData['tenant'])) {
            array_push($whereTexts, "Tenant = 'Yes'");
        }

        if ($val = Common::iset($filterData['organisation'])) {
            array_push($whereTexts, "Organisation contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Description contains '" . $val . "'");
        }


        if ($val = array_get($filterData, 'category', null)) {
            array_push($whereCodes, [
                'category',
                'category_id',
                'category_name',
                $val,
                'Category'
            ]);
        }

        if ($val = array_get($filterData, 'site_id', null)) {
            array_push($whereCodes, [
                'site',
                'site_id',
                'site_code',
                $val,
                'Site Code'
            ]);
        }

        return $query;
    }

    public function reAddTemplateTypeQuery()
    {
        $siteGroupId = \Auth::user()->site_group_id;
        $templateTypeModulesQuery = \DB::table('template_type_modules as ttm')->select([
            \DB::raw('COUNT(ttm.required_module_id) AS Matches'),
            'tt.template_type_description',
            'tt.module_id',
            'ttm.template_type_id'
        ])
            ->join('template_type AS tt', 'tt.template_type_id', '=', 'ttm.template_type_id')
            ->join('module_site_group AS msg', function ($join) use ($siteGroupId) {
                $join->on('msg.module_id', '=', 'ttm.required_module_id');
                $join->on('msg.site_group_id', '=', \DB::raw($siteGroupId));
            });

        $firstTemplateTypeModulesQuery = (clone $templateTypeModulesQuery)->groupBy('tt.template_type_description')
            ->groupBy('ttm.template_type_id');
        $secondTemplateTypeModulesQuery = (clone $templateTypeModulesQuery)->select([
            \DB::raw('COUNT(ttm.required_module_id) AS Matches'),
            'tt.template_type_description',
            'tt.template_type_id'
        ])
            ->whereRaw(
                "tt.template_type_id NOT IN (
                    SELECT ttm.template_type_id
                    FROM template_type_modules AS ttm
                    LEFT JOIN module_site_group AS msg ON msg.module_id = ttm.required_module_id
                        AND msg.site_group_id = {$siteGroupId}
                    WHERE msg.module_id IS NULL
                )"
            )
            ->whereRaw(
                "tt.template_type_id IN (
                    SELECT ttm.template_type_id
                    FROM template_type_modules AS ttm
                    INNER JOIN module_site_group AS msg ON msg.module_id = ttm.required_module_id
                        AND msg.site_group_id = {$siteGroupId}
                )"
            )
            ->groupBy('tt.template_type_description')
            ->groupBy('tt.template_type_id');
        $innerJoin = Common::getSql(
            \DB::table(\DB::raw("(" . Common::getSql($secondTemplateTypeModulesQuery) . ") AS t"))
                ->select([
                    'template_type_description',
                    \DB::raw('MAX(Matches) AS Max'),
                ])
                ->groupBy('template_type_description')
        );
        $query = \DB::table(\DB::raw("(" . Common::getSql($firstTemplateTypeModulesQuery) . ") AS t"))
            ->leftJoin('template_type_default', function ($join) use ($siteGroupId) {
                $join->on('template_type_default.template_type_id', '=', 't.template_type_id');
                $join->on('template_type_default.site_group_id', '=', \DB::raw($siteGroupId));
            })
            ->leftJoin(
                'print_template',
                'print_template.print_template_id',
                '=',
                'template_type_default.default_print_template_id'
            )
            ->leftJoin(
                'email_template',
                'email_template.email_template_id',
                '=',
                'template_type_default.default_email_template_id'
            )
            ->join('module', 'module.module_id', '=', 't.module_id')
            ->join(\DB::raw("({$innerJoin}) AS u"), 'u.template_type_description', '=', 't.template_type_description')
            ->where('u.Max', 't.Matches')
            ->whereRaw(
                "t.template_type_id NOT IN (
                    SELECT ttm.template_type_id
                    FROM template_type_modules AS ttm
                    LEFT JOIN module_site_group AS msg ON msg.module_id = ttm.required_module_id
                        AND msg.site_group_id = {$siteGroupId}
                    WHERE msg.module_id IS NULL
                )"
            )
            ->select('t.template_type_id')
            ->distinct()
        ;

        return $query;
    }

    public function reGetSupAuditQuery()
    {
        $auditTable = (new Audit())->getTable();

        $query = Audit::join('user', 'user.id', '=', "{$auditTable}.user_id")
            ->join('audit_action', "{$auditTable}.audit_action_id", '=', 'audit_action.audit_action_id')
            ->leftjoin('audit_table AS record_table', "{$auditTable}.table_name", '=', 'record_table.table_dbname')
            ->leftjoin(
                'audit_table AS parent_table',
                "{$auditTable}.parent_table_name",
                '=',
                'parent_table.table_dbname'
            )
            ->select([
                \DB::raw("$auditTable.audit_id AS `Audit Id`"),
                \DB::raw("$auditTable.audit_v2 AS `Version`"),
                \DB::raw("$auditTable.audit_v2 AS `Source`"),
                \DB::raw(
                    "CASE $auditTable.source_id
                        WHEN '1' THEN 'Mobile'
                        WHEN '2' THEN 'Dataload'
                    ELSE ''
                    END AS `Source`"
                ),
                \DB::raw("record_table.table_display_name AS `Record Type`"),
                \DB::raw("record_id AS `Record Id`"),
                \DB::raw("record_code AS `Record Code`"),
                \DB::raw("parent_table.table_display_name AS `Parent Record Type`"),
                \DB::raw("parent_record_id AS `Parent Record Id`"),
                \DB::raw("DATE_FORMAT($auditTable.timestamp, '%d/%m/%Y') AS 'Audit Date'"),
                \DB::raw("TIME_FORMAT($auditTable.timestamp, '%H:%i:%s') AS 'Audit Time'"),
                \DB::raw("user.display_name AS User"),
                \DB::raw("audit_action.audit_action_code AS Action"),
                \DB::raw("audit_text AS `Audit Description`")
            ]);

        return $query;
    }

    public function reAddSup01AuditQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        return $this->reAddSupAuditQuery(null, $filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
    }

    public function reAddSup02AuditQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        return $this->reAddSupAuditQuery(1, $filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
    }

    public function reAddSup03AuditQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        return $this->reAddSupAuditQuery(2, $filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
    }

    public function reAddSup04AuditQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        return $this->reAddSupAuditQuery(3, $filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
    }

    private function reAddSupAuditQuery(
        $auditActionId,
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $query = $this->supAuditFilterQuery($auditActionId, $query, $filterData);

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Code contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'user')) {
            array_push($whereCodes, [
                'user',
                'id',
                'display_name',
                $val,
                \Lang::get('text.user')
            ]);
        }

        if ($val = array_get($filterData, 'action')) {
            array_push($whereCodes, [
                'audit_action',
                'audit_action_id',
                'audit_action_code',
                $val,
                \Lang::get('text.action')
            ]);
        }

        if ($val = array_get($filterData, 'from')) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.audit_from'), $val));
        }

        if ($val = array_get($filterData, 'to')) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.audit_to'), $val));
        }

        return $query;
    }

    private function supAuditFilterQuery($auditActionId, $query, $inputs)
    {
        $auditTableName = (new Audit())->getTable();
        $filter = new AuditFilter($inputs);
        if (!is_null($filter->code)) {
            $query->where(function ($query) use ($filter) {
                $query->where('record_code', 'like', '%' . $filter->code . '%');
                $query->orWhere('parent_record_code', 'like', '%' . $filter->code . '%');
            });
        }
        if (!is_null($filter->action)) {
            $query->where('audit_action.audit_action_id', $filter->action);
        } elseif ($auditActionId) {
            $query->where('audit_action.audit_action_id', $auditActionId);
        }
        if (!is_null($filter->user)) {
            $query->where($auditTableName . '.user_id', $filter->user);
        }
        if (!is_null($filter->from)) {
            $from = \DateTime::createFromFormat('d/m/Y', $filter->from);
            if ($from) {
                $query->where(
                    \DB::raw("DATE_FORMAT({$auditTableName}.timestamp, '%Y-%m-%d')"),
                    '>=',
                    $from->format('Y-m-d')
                );
            }
        }
        if (!is_null($filter->to)) {
            $to = \DateTime::createFromFormat('d/m/Y', $filter->to);
            if ($to) {
                $query->where(
                    \DB::raw("DATE_FORMAT({$auditTableName}.timestamp, '%Y-%m-%d')"),
                    '<=',
                    $to->format('Y-m-d')
                );
            }
        }

        return $query;
    }

    public function reAddAud01AuditQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {

        $auditService = new AuditService();
        $filterData['propertyStrategyAccessible'] = true;
        $query = $auditService->filter($query, $filterData);


        if ($val = array_get($filterData, 'module', null)) {
            array_push($whereCodes, [
                'module',
                'module_id',
                'module_description',
                $val,
                'Module'
            ]);
        }

        if ($val = array_get($filterData, 'user', null)) {
            array_push($whereCodes, [
                'user',
                'id',
                'username',
                $val,
                'User'
            ]);
        }

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Description contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'table', null)) {
            array_push($whereCodes, [
                'audit_table',
                'audit_table_id',
                'table_display_name',
                $val,
                'Table'
            ]);
        }

        if ($val = array_get($filterData, 'action', null)) {
            array_push($whereCodes, [
                'audit_action',
                'audit_action_id',
                'audit_action_code',
                $val,
                'Action'
            ]);
        }

        if ($val = array_get($filterData, 'from')) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.audit_from'), $val));
        }

        if ($val = array_get($filterData, 'to')) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.audit_to'), $val));
        }

        return $query;
    }


    public function reAddInsCliWak01DocumentQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $documentService = new DocumentsService($this->permissionService);
        $query = $this->docService->documentSitesAccess($query, $viewName);

        if (!$this->permissionService->isAdministratorOrSuperuser()) {
            // For user documents, they must have loaded the document.
            $query->where(function ($subQuery1) use ($viewName) {
                $subQuery1->where("$viewName.doc_record_type_id", '<>', DocRecordType::USER)
                ->orWhere(function ($subQuery2) use ($viewName) {
                    $subQuery2->where("$viewName.doc_record_type_id", '=', DocRecordType::USER)
                        ->where("$viewName.record_id", '=', \Auth::User()->getKey());
                });
            });
        }
        $documentQuery = $documentService->applyFilter($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['name'])) {
            array_push($whereTexts, "Document name contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['record'])) {
            array_push($whereTexts, "Inspection Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Document description contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['group'])) {
            array_push(
                $whereCodes,
                ['doc_group', 'doc_group_id', 'doc_group_code', $val, 'Group']
            );
        }
        if ($val = Common::iset($filterData['docSubGroup'])) {
            array_push(
                $whereCodes,
                ['doc_sub_group', 'doc_sub_group_id', 'doc_sub_group_code', $val, 'Sub Group']
            );
        }
        if ($val = array_get($filterData, 'documentDateFrom')) {
            array_push($whereTexts, "Document Date From $val");
        }
        if ($val = array_get($filterData, 'documentDateTo')) {
            array_push($whereTexts, "Document Date To $val");
        }

        if ($val = array_get($filterData, 'nextReviewDateFrom')) {
            array_push($whereTexts, "Next Review From $val");
        }
        if ($val = array_get($filterData, 'nextReviewDateTo')) {
            array_push($whereTexts, "Next Review To $val");
        }

        if ($val = Common::iset($filterData['comment'])) {
            array_push($whereTexts, "Document comment contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'completedDateFrom')) {
            array_push($whereTexts, "Completed Date From $val");
        }
        if ($val = array_get($filterData, 'completedDateTo')) {
            array_push($whereTexts, "Completed Date To $val");
        }

        if ($val = array_get($filterData, 'differenceFrom')) {
            array_push($whereTexts, "Difference From $val");
        }

        if ($val = array_get($filterData, 'differenceTo')) {
            array_push($whereTexts, "Difference To $val");
        }

        if ($val = array_get($filterData, 'nextReviewOwnerUserId')) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Review Owner']);
        }

        if ($val = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, 'Site Code']);
        }

        if ($val = array_get($filterData, 'readOnly')) {
            array_push($whereTexts, "Read-only is $val");
        }
        if ($val = array_get($filterData, 'archived')) {
            array_push($whereTexts, "Archived is $val");
        }
        if ($val = array_get($filterData, 'overdue')) {
            array_push($whereTexts, "Overdue is $val");
        }

        return $documentQuery;
    }
}
