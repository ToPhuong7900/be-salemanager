<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Constant\GmConstant;
use Tfcloud\Models\CaseManagement\CmCaseStatusType;
use Tfcloud\Models\CaseManagement\CmStageStatusType;
use Tfcloud\Services\BaseService;
use Tfcloud\services\CaseManagement\CaseMgmt\CaseManagementStageService;
use Tfcloud\Services\CaseManagement\CaseMgmt\CaseRecordService;
use Tfcloud\Services\GroundsMaintenance\GmContractExternalAreaTaskService;
use Tfcloud\Services\GroundsMaintenance\GmContractJobService;
use Tfcloud\Services\GroundsMaintenance\GMContractService;
use Tfcloud\Services\PermissionService;

class CaseManagementFilterQueryService extends BaseService
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    public function reAddCaseManagementCaseQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $cmCaseQuery = (new CaseRecordService($this->permissionService))->filter($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'code')) {
            array_push($whereTexts, "Code contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'description')) {
            array_push($whereTexts, "Description contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['owner'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Owner']);
        }

        if ($val = Common::iset($filterData['contact'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Contact']);
        }
        if ($val = array_get($filterData, 'plant_id', null)) {
            array_push($whereCodes, ['plant', 'plant_id', 'plant_code', $val, 'Plant']);
        }

        if ($val = array_get($filterData, 'type', null)) {
            array_push($whereCodes, [
                'cm_case_type',
                'cm_case_type_id',
                'cm_case_type_code',
                $val,
                'Type'
            ]);
        }
        $statusType = [];
        if (Common::iset($filterData['status_draft'])) {
            $statusType[] = CmCaseStatusType::DRAFT;
        }
        if (Common::iset($filterData['status_open'])) {
            $statusType[] = CmCaseStatusType::OPEN;
        }
        if (Common::iset($filterData['status_complete'])) {
            $statusType[] = CmCaseStatusType::COMPLETE;
        }
        if (count($statusType)) {
            array_push(
                $orCodes,
                [
                    'cm_case_status_type',
                    'cm_case_status_type_id',
                    'cm_case_status_type_code',
                    implode(',', $statusType),
                    "Status"
                ]
            );
        }
        if ($val = array_get($filterData, 'nextReviewFrom', null)) {
            array_push($whereTexts, "Next Review From '" . " $val" . "'");
        }

        if ($val = array_get($filterData, 'nextReviewTo', null)) {
            array_push($whereTexts, "Next Review To '" . " $val" . "'");
        }

        if ($val = array_get($filterData, 'team', null)) {
            array_push($whereCodes, [
                'cm_team',
                'cm_team_id',
                'cm_team_code',
                $val,
                'Team'
            ]);
        }

        if ($val = array_get($filterData, 'priority', null)) {
            array_push($whereCodes, [
                'cm_priority',
                'cm_priority_id',
                'cm_priority_code',
                $val,
                'Priority'
            ]);
        }

        return $cmCaseQuery;
    }

    public function reAddCaseManagementStageQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $cmStageQuery = (new CaseManagementStageService($this->permissionService))->filterAll(
            $query,
            $filterData,
            $viewName
        );

        if ($val = array_get($filterData, 'code')) {
            array_push($whereTexts, "Case Code contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'description')) {
            array_push($whereTexts, "Case Description contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['caseOwner'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Case Owner']);
        }

        if ($val = Common::iset($filterData['stageOwner'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Stage Owner']);
        }

        if ($val = Common::iset($filterData['contact'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Contact']);
        }
        if ($val = array_get($filterData, 'plant_id', null)) {
            array_push($whereCodes, ['plant', 'plant_id', 'plant_code', $val, 'Plant']);
        }

        if ($val = array_get($filterData, 'type', null)) {
            array_push($whereCodes, [
                'cm_case_type',
                'cm_case_type_id',
                'cm_case_type_code',
                $val,
                'Type'
            ]);
        }
        $statusType = [];
        if (Common::iset($filterData['status_draft'])) {
            $statusType[] = CmCaseStatusType::DRAFT;
        }
        if (Common::iset($filterData['status_open'])) {
            $statusType[] = CmCaseStatusType::OPEN;
        }
        if (Common::iset($filterData['status_complete'])) {
            $statusType[] = CmCaseStatusType::COMPLETE;
        }
        if (count($statusType)) {
            array_push(
                $orCodes,
                [
                    'cm_case_status_type',
                    'cm_case_status_type_id',
                    'cm_case_status_type_code',
                    implode(',', $statusType),
                    "Status"
                ]
            );
        }
        $stageStatusType = [];
        if (Common::iset($filterData['stageStatusAwaitingResponse']) && $filterData['stageStatusAwaitingResponse']) {
            $stageStatusType[] = CmStageStatusType::AWAITING_RESPONSE;
        }
        if (Common::iset($filterData['stageStatusComplete']) && $filterData['stageStatusComplete']) {
            $stageStatusType[] = CmStageStatusType::COMPLETE;
        }
        if (Common::iset($filterData['stageStatusIncomplete']) && $filterData['stageStatusIncomplete']) {
            $stageStatusType[] = CmStageStatusType::INCOMPLETE;
        }
        if (Common::iset($filterData['stageStatusLocked']) && $filterData['stageStatusLocked']) {
            $stageStatusType[] = CmStageStatusType::LOCKED;
        }
        if (count($stageStatusType)) {
            array_push(
                $orCodes,
                [
                    'cm_stage_status_type',
                    'cm_stage_status_type_id',
                    'cm_stage_status_type_code',
                    implode(',', $stageStatusType),
                    "Stage Status"
                ]
            );
        }

        if ($val = array_get($filterData, 'nextReviewFrom', null)) {
            array_push($whereTexts, "Next Review From '" . " $val" . "'");
        }

        if ($val = array_get($filterData, 'nextReviewTo', null)) {
            array_push($whereTexts, "Next Review To '" . " $val" . "'");
        }

        if ($val = array_get($filterData, 'stageDueDateFrom', null)) {
            array_push($whereTexts, "Stage Due Date From '" . " $val" . "'");
        }

        if ($val = array_get($filterData, 'stageDueDateTo', null)) {
            array_push($whereTexts, "Stage Due Date To '" . " $val" . "'");
        }

        if ($val = array_get($filterData, 'responseTypeId', null)) {
            array_push($whereCodes, [
                'cm_response_type',
                'cm_response_type_id',
                'cm_response_type_code',
                $val,
                'Response Type'
            ]);
        }

        if ($val = array_get($filterData, 'responseTypeItemId', null)) {
            array_push($whereCodes, [
                'cm_response_type_item',
                'cm_response_type_item_id',
                'cm_response_type_item_code',
                $val,
                'Stage Response'
            ]);
        }

        return $cmStageQuery;
    }
}
