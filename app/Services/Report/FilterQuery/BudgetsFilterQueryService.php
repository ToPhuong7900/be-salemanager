<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Services\BaseService;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\ReportConstant;
use Tfcloud\Models\FinAccount;
use Tfcloud\Models\FinAccountType;
use Tfcloud\Models\FinYear;
use Tfcloud\Services\Budget\EstExpenditureBudgetListService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Budget\ExpenditureBudgetListService;
use Tfcloud\Services\Budget\IncomingBudgetListService;
use Tfcloud\Services\Budget\SpendBySiteBudgetListService;

class BudgetsFilterQueryService extends BaseService
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    public function reAddSpendBySiteExpenditureBudgetQuery($filterData, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $spendBySiteBudgetListService = new SpendBySiteBudgetListService($this->permissionService);
        $query = $spendBySiteBudgetListService->getAll($filterData, false, true);

        return $query;
    }

    public function reAddExpenditureBudgetQuery($filterData, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $expenditureBudgetListService = new ExpenditureBudgetListService($this->permissionService);
        $query = $expenditureBudgetListService->getAll($filterData, false, true);

        if ($val = array_get($filterData, 'code', null)) {
            array_push($whereTexts, " Code contains '$val'");
        }

        if ($val = array_get($filterData, 'description', null)) {
            array_push($whereTexts, "  Description contains '$val'");
        }

        if ($val = array_get($filterData, 'finYear', null)) {
            $finYear = FinYear::find($val);
            array_push($whereTexts, "  Financial Year contains '{$finYear->fin_year_code}'");
        }

        if ($val = array_get($filterData, 'account', null)) {
            $finAccount = FinAccount::find($val);
            array_push($whereTexts, "  Account contains '{$finAccount->fin_account_code}'");
        }

        $val = array_get($filterData, 'status', null);
        if (!is_null($val)) {
            if ($val == 'Y') {
                array_push($whereTexts, " Active records only");
            } elseif ($val == 'N') {
                array_push($whereTexts, " Inactive records only");
            }
        }

        return $query;
    }

    public function reAddEstatesBudgetQuery($filterData, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $estatesBudgetListService = new IncomingBudgetListService($this->permissionService);
        $query = $estatesBudgetListService->getAll($filterData, true);

        if ($val = array_get($filterData, 'code', null)) {
            array_push($whereTexts, " Code contains '$val'");
        }

        if ($val = array_get($filterData, 'description', null)) {
            array_push($whereTexts, "  Description contains '$val'");
        }

        if ($val = array_get($filterData, 'finYear', null)) {
            $finYear = FinYear::find($val);
            array_push($whereTexts, "  Financial Year contains '{$finYear->fin_year_code}'");
        }

        if ($val = array_get($filterData, 'account', null)) {
            $finAccount = FinAccount::find($val);
            array_push($whereTexts, "  Account contains '{$finAccount->fin_account_code}'");
        }

        $val = array_get($filterData, 'status', null);
        if (!is_null($val)) {
            if ($val == 'Y') {
                array_push($whereTexts, " Active records only");
            } elseif ($val == 'N') {
                array_push($whereTexts, " Inactive records only");
            }
        }

        return $query;
    }

    public function reAddEstatesExpenditureBudgetQuery($filterData, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $estExpenditureBudgetListService = new EstExpenditureBudgetListService($this->permissionService);
        $query = $estExpenditureBudgetListService->getAll($filterData, true);

        if ($val = array_get($filterData, 'accountCode', null)) {
            array_push($whereTexts, " Account Code contains '$val'");
        }

        if ($val = array_get($filterData, 'account', null)) {
            $finAccount = FinAccount::find($val);
            array_push($whereTexts, "  Account contains '{$finAccount->fin_account_code}'");
        }

        $val = array_get($filterData, 'status', null);
        if (!is_null($val)) {
            if ($val == 'Y') {
                array_push($whereTexts, " Active records only");
            } elseif ($val == 'N') {
                array_push($whereTexts, " Inactive records only");
            }
        }

        return $query;
    }

    public function reAddEstatesRentalPaymentBreakdownQuery($filterData, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $estExpenditureBudgetListService = new EstExpenditureBudgetListService($this->permissionService);
        $query = $estExpenditureBudgetListService->getAllRentalPaymentLines(0, $filterData, true);

        if ($val = array_get($filterData, 'accountCode', null)) {
            array_push($whereTexts, " Account Code contains '$val'");
        }

        if ($val = array_get($filterData, 'account', null)) {
            $finAccount = FinAccount::find($val);
            array_push($whereTexts, "  Account contains '{$finAccount->fin_account_code}'");
        }

        $val = array_get($filterData, 'status', null);
        if (!is_null($val)) {
            if ($val == 'Y') {
                array_push($whereTexts, " Active records only");
            } elseif ($val == 'N') {
                array_push($whereTexts, " Inactive records only");
            }
        }

        return $query;
    }

    public function reAddSalesInvoiceQuery($filterData, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $estatesBudgetListService = new IncomingBudgetListService($this->permissionService);
        $query = $estatesBudgetListService->getAllSaleInvoieLines(0, $filterData, true);

        if ($val = array_get($filterData, 'code', null)) {
            array_push($whereTexts, " Code contains '$val'");
        }

        if ($val = array_get($filterData, 'description', null)) {
            array_push($whereTexts, "  Description contains '$val'");
        }

        if ($val = array_get($filterData, 'finYear', null)) {
            $finYear = FinYear::find($val);
            array_push($whereTexts, "  Financial Year contains '{$finYear->fin_year_code}'");
        }

        if ($val = array_get($filterData, 'account', null)) {
            $finAccount = FinAccount::find($val);
            array_push($whereTexts, "  Account contains '{$finAccount->fin_account_code}'");
        }

        $val = array_get($filterData, 'status', null);
        if (!is_null($val)) {
            if ($val == 'Y') {
                array_push($whereTexts, " Active records only");
            } elseif ($val == 'N') {
                array_push($whereTexts, " Inactive records only");
            }
        }

        return $query;
    }

    public function reportField()
    {
        $reportFields = \Tfcloud\Models\ReportField::
        where('system_report_id', '=', Common::getSystemReportId(ReportConstant::SYSTEM_REPORT_INT07))
        ->select('report_field_code')->get();

        $field = [];
        foreach ($reportFields as $reportField) {
            $field[] = $reportField->report_field_code;
        }

        $selectFields = implode(',', $field);

        return $selectFields;
    }

    public function getAllBudgets()
    {
        $expenditureBudgetListService = new ExpenditureBudgetListService($this->permissionService);
        $query = $expenditureBudgetListService->all(true)
            ->where('fin_account.fin_account_type_id', FinAccountType::EXPENDITURE);

        return $query;
    }
}
