<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Models\GenTable;
use Tfcloud\Models\Plant;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Property\PlantService;
use Tfcloud\Services\Admin\System\Plant\PlantPartService;

class PlantFilterQueryService extends BaseService
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    public function reAddPlantQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $plantService = new PlantService($this->permissionService);
        $plantQuery = $plantService->filterAll($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'siteType')) {
            array_push($whereCodes, [
                'site_type',
                'site_type_id',
                'site_type_code',
                $val, \Lang::get('text.report_texts.property.site_type_code')
            ]);
        }

        if ($val = array_get($filterData, 'plant_group_id')) {
            array_push($whereCodes, ['plant_group', 'plant_group_id', 'plant_group_code', $val, "Plant Group"]);

            if ($val = array_get($filterData, 'plant_subgroup_id')) {
                array_push(
                    $whereCodes,
                    ['plant_subgroup', 'plant_subgroup_id', 'plant_subgroup_code', $val, "Plant Sub Group"]
                );

                if ($val = array_get($filterData, 'plant_type_id')) {
                    array_push(
                        $whereCodes,
                        ['plant_type', 'plant_type_id', 'plant_type_code', $val, "Plant Type"]
                    );
                }
            }
        }

        if ($val = array_get($filterData, 'code')) {
            array_push($whereTexts, "Code contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'description')) {
            array_push($whereTexts, "Description contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'plant_criticality_id')) {
            array_push(
                $whereCodes,
                ['plant_criticality', 'plant_criticality_id', 'plant_criticality_code', $val, "Plant Criticality"]
            );
        }

        if ($val = array_get($filterData, 'plant_condition_id')) {
            array_push(
                $whereCodes,
                ['plant_condition', 'plant_condition_id', 'plant_condition_code', $val, "Plant Condition"]
            );
        }

        if ($val = array_get($filterData, 'status')) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, "Active = 'Active'");
                    break;
                case 'all':
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                default:
                    array_push($whereTexts, "Active = 'Inactive'");
                    break;
            }
        }

        if ($val = array_get($filterData, 'plantKind')) {
            switch ($val) {
                case Plant::KIND_GENERAL:
                    array_push($whereTexts, "Plant Kind = 'General'");
                    break;
                case Plant::KIND_GROUNDS_MAINTENANCE:
                    array_push($whereTexts, "Plant Kind = 'Grounds Maintenance'");
                    break;
                default:
                    break;
            }
        }

        if ($val = array_get($filterData, 'plantInclude')) {
            switch ($val) {
                case 1:
                    array_push($whereTexts, "Include = 'Tree'");
                    break;
                case 2:
                    array_push($whereTexts, "Include = 'Standard'");
                    break;
            }
        }

        if ($val = array_get($filterData, 'tpoInclude')) {
            switch ($val) {
                case 1:
                    array_push($whereTexts, "TPO = 'Yes'");
                    break;
                case 2:
                    array_push($whereTexts, "TPO = 'No'");
                    break;
            }
        }

        if ($val = array_get($filterData, 'contact')) {
            array_push(
                $whereCodes,
                ['contact', 'contact_id', 'contact_name', $val, "Maint. Contact"]
            );
        }

        if ($val = array_get($filterData, 'owner')) {
            array_push(
                $whereCodes,
                ['user', 'id', 'display_name', $val, "Owner"]
            );
        }

        if ($val = array_get($filterData, 'replacementYear')) {
            array_push($whereTexts, "Replacement Year = $val");
        }

        $ageRagFrom = array_get($filterData, 'ageRagFrom');
        $ageRagTo = array_get($filterData, 'ageRagTo');
        if ($ageRagFrom && $ageRagTo) {
            array_push($whereTexts, "Age RAG (%) from $ageRagFrom to $ageRagTo");
        } else {
            if ($ageRagFrom) {
                array_push($whereTexts, "Age RAG (%) From $ageRagFrom");
            } elseif ($ageRagTo) {
                array_push($whereTexts, "Age RAG (%) To $ageRagTo");
            }
        }

        if ($val = array_get($filterData, 'prop_external_id')) {
            array_push(
                $whereCodes,
                ['prop_external', 'prop_external_id', 'prop_external_code', $val, "External Area Code"]
            );
        }

        if ($val = array_get($filterData, 'tree_species_id')) {
            array_push(
                $whereCodes,
                ['tree_species', 'tree_species_id', 'common_name', $val, "Tree Species"]
            );
        }

        if ($val = array_get($filterData, 'tree_grading_id')) {
            array_push(
                $whereCodes,
                ['tree_grading', 'tree_grading_id', 'tree_grading_code', $val, "Tree Grading"]
            );
        }

        if ($val = array_get($filterData, 'tree_sub_grading_id')) {
            array_push(
                $whereCodes,
                ['tree_sub_grading', 'tree_sub_grading_id', 'tree_sub_grading_code', $val, "Tree Sub Grading"]
            );
        }

        $this->reAddUserDefinesQuery(GenTable::PLANT, $filterData, $whereCodes, $whereTexts);

        return $plantQuery;
    }

    public function reAddPlantAdmQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $plantPartService = new PlantPartService($this->permissionService);
        $plantQuery = $plantPartService->filterAll($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'plant_part_code')) {
            array_push($whereTexts, "Code contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'plant_part_desc')) {
            array_push($whereTexts, "Description contains '" . $val . "'");
        }


        if ($val = array_get($filterData, 'plant_part_type_id')) {
            array_push(
                $whereCodes,
                ['plant_part_type', 'plant_part_type_id', 'plant_part_type_code', $val, "Plant Part Type"]
            );
        }

        if ($val = Common::iset($filterData['supplier_contact_id'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Supplier']);
        }

        if ($val = array_get($filterData, 'active')) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.active_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.active_no'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.active_all'));
                    break;
            }
        }

        return $plantQuery;
    }
}
