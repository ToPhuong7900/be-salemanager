<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\FixedAsset\CaSystemManager;
use Tfcloud\Models\CaIfrsCategory;
use Tfcloud\Models\CaTransactionType;
use Tfcloud\Models\Report;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\PermissionService;

class FixedAssetsFAR02QueryService extends BaseService
{
    protected $permissionService;

    protected $siteGroupId;
    protected $bSumNegative = false;
    protected $entity = false;

    public function __construct(PermissionService $permissionService, $filterData, Report $report = null)
    {
        $this->permissionService = $permissionService;
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;

        $this->setFilterProperties($filterData);
    }

    private function setFilterProperties($filterData)
    {
        $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($filterData);
        $entity = $reportFilterQuery->getFirstValueFilterField('ca_entity_id');

        // get filter values
        $this->filterData = $filterData;
        $this->entity = (!$entity) ? false : $entity;
    }

    public function getNumericCols()
    {
        $arrIFRSCat = $this->getIFRSCategories();
        $numCols = [];

        $col = 0;

        foreach ($arrIFRSCat as $key => $category) {
            $col++;
            $numCols[] = $col;
        }

        $numCols[] = $col + 1;

        return $numCols;
    }

    public function movementsInNetBookValueQuery()
    {
        // Get all IFRS categories the report should include.
        $arrIFRSCat = $this->getIFRSCategories();

        // Get IFRS categories code and description for use in report header.
        $arrIFRSCatDesc = $this->getIFRSCategories(true);

        $bYearEnd = CaSystemManager::isInYearEndMode();
        $this->siteGroupId = \Auth::user()->site_group_id;

        // *****************************************************************
        // REPORT HEADER
        // *****************************************************************
        // Build report header
        $queryRpt = $this->reportHeader($arrIFRSCatDesc);

        // *****************************************************************
        // REPORT DETAIL
        // *****************************************************************
        // Add Gross Book Value line
        $arrColumn = array("ca_balance.bfwd_gbv");
        $this->setSumNegative(false);
        $queryRpt->unionAll($this->buildBalanceQuery("Gross Book Value", $arrIFRSCat, $arrColumn, true));

        // Add Accumulated Depreciation line
        $arrColumn = array("ca_balance.bfwd_accum_depn");
        $this->setSumNegative(true);
        $queryRpt->unionAll($this->buildBalanceQuery("Accumulated Depreciation", $arrIFRSCat, $arrColumn, true));

        // Add Accumulated Impairment line
        $arrColumn = array("ca_balance.bfwd_accum_imp_asset", "ca_balance.bfwd_accum_imp_land");
        $this->setSumNegative(true);
        $queryRpt->unionAll($this->buildBalanceQuery("Accumulated Impairment", $arrIFRSCat, $arrColumn, true));

        // Add Net Book Value
        $arrColumn = array("ca_balance.bfwd_nbv");
        $this->setSumNegative(false);
        $queryRpt->unionAll($this->buildBalanceQuery("Net Book Value", $arrIFRSCat, $arrColumn, true));

        // Add formatting blank row
        $queryRpt->unionAll($this->addBlankRow($arrIFRSCat));

        // Add Acquisition Value
        $this->setSumNegative(false);
        $queryRpt->unionAll($this->buildTranQuery("Acquisition", $arrIFRSCat, CaTransactionType::ACQUISITION));

        // Add Addition Value
        $this->setSumNegative(false);
        $queryRpt->unionAll($this->buildTranQuery("Addition", $arrIFRSCat, CaTransactionType::ADDITION));

        // Add Derecognition Value
        $this->setSumNegative(false);
        $queryRpt->unionAll($this->buildTranQuery("Derecognition", $arrIFRSCat, CaTransactionType::DERECOGNITION));

        // Add Disposal Value
        $this->setSumNegative(false);
        $queryRpt->unionAll($this->buildTranQuery("Disposal", $arrIFRSCat, CaTransactionType::DISPOSAL));

        // Add Transfer Value
        $this->setSumNegative(false);
        $queryRpt->unionAll($this->buildTranQuery("Transfer", $arrIFRSCat, CaTransactionType::CATEGORY_TRANSFER));

        // Add Impairment Value
        $this->setSumNegative(true);
        $queryRpt->unionAll($this->buildTranQuery("Impairment", $arrIFRSCat, CaTransactionType::IMPAIRMENT));

        // Add Reversal of Impairment Value
        $this->setSumNegative(false);
        $queryRpt->unionAll(
            $this->buildTranQuery("Reversal of Impairment", $arrIFRSCat, CaTransactionType::REVERSAL_OF_IMPAIRMENT)
        );

        // Add Revaluation Surplus Value
        $this->setSumNegative(false);
        $sWhere = " AND ca_transaction.cv_asset + ca_transaction.cv_land >=0 ";
        $queryRpt->unionAll(
            $this->buildTranQuery("Revaluation Surplus", $arrIFRSCat, CaTransactionType::REVALUATION, $sWhere)
        );

        // Add Revaluation Loss of Value
        $this->setSumNegative(false);
        $sWhere = " AND ca_transaction.cv_asset + ca_transaction.cv_land <0 ";
        $queryRpt->unionAll(
            $this->buildTranQuery("Revaluation Loss of Value", $arrIFRSCat, CaTransactionType::REVALUATION, $sWhere)
        );

        // Add Reversal of Loss Value
        $this->setSumNegative(false);
        $queryRpt->unionAll(
            $this->buildTranQuery("Reversal of Loss", $arrIFRSCat, CaTransactionType::REVERSAL_OF_LOSS)
        );

        // Add formatting blank row
        $queryRpt->unionAll($this->addBlankRow($arrIFRSCat));

        // Add In Year Depreciation
        $arrColumn = array("ca_balance.cy_depreciation");
        $this->setSumNegative(true);
        $queryRpt->unionAll($this->buildBalanceQuery("In Year Depreciation", $arrIFRSCat, $arrColumn, $bYearEnd));

        // Add Net Book Value
        $arrColumn = array("ca_balance.cfwd_nbv");
        $this->setSumNegative(false);
        $queryRpt->unionAll($this->buildBalanceQuery("Net Book Value", $arrIFRSCat, $arrColumn));

        return \DB::table(null)->select('*')->from(\DB::raw("(" . $queryRpt->toSql() . ') AS report'));
    }

    private function setSumNegative($bValue)
    {
        $this->bSumNegative = $bValue;
    }

    /**
     * Returns the sql to add a blank row into a csv report.
     * @param array $arrIFRSCat
     * @return string
     */
    protected function addBlankRow(array $arrIFRSCat)
    {
        // First column is the row description label.
        $query =  "' ' `Element`";

        // Add columns for each IFRS category
        foreach ($arrIFRSCat as $key => $value) {
            $query .= " , ' ' ";
        }

        // Calculate row total.
        $query .= " , '' `total` ";

        return \DB::table(null)->select(\DB::raw($query));
    }

    private function getIFRSCategories($bIncludeDesc = false)
    {
        $arrIFRSCat = array();   // Holds all IFRS categories the report should include.

        // Get available ifrs categories to allow the report query to be built dynamically
        $query = CaIfrsCategory::userSiteGroup()
            ->select(
                ['ca_ifrs_category_code', 'ca_ifrs_category_desc', 'ca_ifrs_category_id']
            )
            ->where('ca_ifrs_category.active', '=', CommonConstant::DATABASE_VALUE_YES);

        $query->orderBy('investment');
        $query->orderBy('depreciable', 'desc');

        // if ($bIncludeDesc) {
        //     $query->orderBy(\DB::raw('CONCAT(ca_ifrs_category_code, ca_ifrs_category_desc)'));
        // } else {
            $query->orderBy('ca_ifrs_category_code');
        // }

        $queryIFRS = $query->get();

        // Loop through all ifrs categories and store in array.
        foreach ($queryIFRS as $row) {
            $bDesc = '';
            if ($bIncludeDesc) {
                $bDesc = Common::concatFields([$row['ca_ifrs_category_code'], $row['ca_ifrs_category_desc']]);
            } else {
                $bDesc = $row['ca_ifrs_category_code'];
            }
            $arrIFRSCat[$row['ca_ifrs_category_id']] = $bDesc;
        }

        return $arrIFRSCat;
    }

    private function reportHeader($arrIFRSCatDesc)
    {
        $query = " '' `Element`";
        foreach ($arrIFRSCatDesc as $key => $codeDesc) {
            $query .= " , '' `$codeDesc`";
        }
        $query .= ", '' Totals ";

        return \DB::table(null)->select(\DB::raw($query));
    }

    private function buildBalanceQuery($rowDesc, array $arrIFRSCat, array $arrColumn, $bUseBfwdIFRS = false)
    {
        // Get columns as a  string.
        $columns = implode(" + ", $arrColumn);

        // First column is the row description label.
        $query =  "'$rowDesc' `Element`";

        // Add columns for each IFRS category
        foreach ($arrIFRSCat as $key => $value) {
            $query .= " , ";
            $query .= " (SELECT IFNULL(";
            $query .= " (SELECT ROUND(SUM($columns)" . ($this->bSumNegative ? " * -1 " : "") . ", 0) ";
            $query .= "  FROM ca_ifrs_category ";
            if ($bUseBfwdIFRS) {
                $query .= " INNER JOIN ca_fixed_asset ON
                    ca_fixed_asset.bfwd_ifrs_cat_id = ca_ifrs_category.ca_ifrs_category_id ";
            } else {
                $query .= " INNER JOIN ca_fixed_asset ON
                    ca_fixed_asset.ca_ifrs_cat_id = ca_ifrs_category.ca_ifrs_category_id ";
            }
            $query .= " INNER JOIN ca_balance ON ca_balance.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
            $query .= " INNER JOIN vw_ca_current_fin_year ON
                vw_ca_current_fin_year.ca_fin_year_id = ca_balance.ca_fin_year_id ";
            $query .= "  WHERE ca_ifrs_category.site_group_id = '{$this->siteGroupId}' ";
            $query .= "     AND ca_ifrs_category.active = 'Y' ";
            $query .= "     AND ca_balance.historic = 'N' ";
            $query .= "     AND ca_fixed_asset.deminimis_asset = 'N'";
            if ($this->entity) {
                $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
            }
            $query .= "     AND ca_fixed_asset.active = 'Y'";
            $query .= "     AND ca_ifrs_category.ca_ifrs_category_id = " . $key;
            $query .= ") , 0)) ";
        }

        // Calculate row total.
        $query .= " , ";
        $query .= " (SELECT IFNULL(";
        $query .= " (SELECT ROUND(SUM($columns) " . ($this->bSumNegative ? " * -1 " : "") . ", 0) ";
        $query .= "  FROM ca_ifrs_category ";
        if ($bUseBfwdIFRS) {
            $query .= " INNER JOIN ca_fixed_asset ON
                ca_fixed_asset.bfwd_ifrs_cat_id = ca_ifrs_category.ca_ifrs_category_id ";
        } else {
            $query .= " INNER JOIN ca_fixed_asset ON
                ca_fixed_asset.ca_ifrs_cat_id = ca_ifrs_category.ca_ifrs_category_id ";
        }
        $query .= " INNER JOIN ca_balance ON ca_balance.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
        $query .= " INNER JOIN vw_ca_current_fin_year ON
            vw_ca_current_fin_year.ca_fin_year_id = ca_balance.ca_fin_year_id ";
        $query .= "  WHERE ca_ifrs_category.site_group_id = '{$this->siteGroupId}' ";
        $query .= "     AND ca_ifrs_category.active = 'Y' ";
        $query .= "     AND ca_fixed_asset.deminimis_asset = 'N'";
        if ($this->entity) {
            $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
        }
        $query .= "     AND ca_fixed_asset.active = 'Y'";
        $query .= "     AND ca_balance.historic = 'N' ),0)) `total` ";

        return \DB::table(null)->select(\DB::raw($query));
    }

    private function buildTranQuery($rowDesc, array $arrIFRSCat, $transactionTypeId, $bWhere = "")
    {
        // First column is the row description label.
        $query =  "'$rowDesc' `Element`";

        // Add columns for each IFRS category
        foreach ($arrIFRSCat as $key => $value) {
            $query .= " , ";
            $query .= " (SELECT IFNULL(";
            $query .= " (SELECT ROUND(SUM(
                ca_transaction.cv_asset - ca_transaction.cv_depn_adjustment + ca_transaction.cv_land)" .
                ($this->bSumNegative ? " * -1 " : "") . ", 0) ";
            $query .= " FROM ca_ifrs_category ";
            $query .= " INNER JOIN ca_transaction ON
                ca_transaction.ca_ifrs_cat_id = ca_ifrs_category.ca_ifrs_category_id ";
            $query .= " INNER JOIN vw_ca_current_fin_year ON
                vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
            $query .= " INNER JOIN ca_fixed_asset ON
                ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
            $query .= "  WHERE ca_ifrs_category.site_group_id = '{$this->siteGroupId}' ";
            $query .= "  AND ca_ifrs_category.active = 'Y' ";
            $query .= "  AND ca_ifrs_category.ca_ifrs_category_id = " . $key;
            $query .= "  AND ca_fixed_asset.deminimis_asset = 'N'";
            if ($this->entity) {
                $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
            }
            $query .= "  AND ca_fixed_asset.active = 'Y'";
            $query .= "  AND ca_transaction.ca_transaction_type_id = $transactionTypeId";
            if (!empty($bWhere)) {
                $query .= " " . $bWhere;
            }
            $query .= ") , 0)) ";
        }

        // Calculate row total.
        $query .= " , ";
        $query .= " (SELECT IFNULL(";
        $query .= " (SELECT ROUND(SUM(
            ca_transaction.cv_asset - ca_transaction.cv_depn_adjustment + ca_transaction.cv_land) " .
            ($this->bSumNegative ? " * -1 " : "") . ", 0) ";
        $query .= " FROM ca_ifrs_category ";
        $query .= " INNER JOIN ca_transaction ON ca_transaction.ca_ifrs_cat_id = ca_ifrs_category.ca_ifrs_category_id ";
        $query .= " INNER JOIN vw_ca_current_fin_year ON
            vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
        $query .= " INNER JOIN ca_fixed_asset ON ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
        $query .= " WHERE ca_ifrs_category.site_group_id = '{$this->siteGroupId}' ";
        $query .= " AND ca_ifrs_category.active = 'Y' ";
        $query .= " AND ca_fixed_asset.deminimis_asset = 'N'";
        if ($this->entity) {
            $query .= "     AND ca_fixed_asset.ca_entity_id = $this->entity";
        }
        $query .= " AND ca_fixed_asset.active = 'Y'";
        $query .= " AND ca_transaction.ca_transaction_type_id = $transactionTypeId";
        if (!empty($bWhere)) {
            $query .= " " . $bWhere;
        }
        $query .= "), 0)) `total` ";

        return \DB::table(null)->select(\DB::raw($query));
    }
}
