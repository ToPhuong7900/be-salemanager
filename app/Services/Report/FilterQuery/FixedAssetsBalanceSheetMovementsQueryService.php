<?php

/**
 * FAR37
 *
 * Equivalent of CA00 in tf facility.
 *
 *
 */

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\FixedAsset\CaSystemManager;
use Tfcloud\Models\CaIfrsCategory;
use Tfcloud\Models\CaTransactionType;
use Tfcloud\Models\Report;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\PermissionService;

class FixedAssetsBalanceSheetMovementsQueryService extends BaseService
{
    protected $permissionService;

    protected $siteGroupId;
    protected $bIncludeRRTrans = false;
    protected $rrTrans = 'N';
    protected $bSumNegative = false;

    // filters
    protected $commonGood = false;
    protected $investment = false;
    protected $deminimis = false;
    protected $entity = false;
    protected $historicValue = false;
    protected $currentValue = true;
    protected $filterData = [];

    // detailed report
    protected $detailed = true;

    private $reportBoxFilterLoader = null;

    public function __construct(PermissionService $permissionService, $filterData, $repCode, Report $report = null)
    {
        $this->permissionService = $permissionService;

        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;

        $this->setFilterProperties($filterData, $repCode);
//        // get filter values
//        $commonGood = $filterData['common_good'] ?? false;
//        $investment = $filterData['investment'] ?? false;
//        $deminimis = $filterData['deminimis_asset'] ?? CommonConstant::DATABASE_VALUE_NO;
//        $this->historicValue = $filterData['ca_balance_historic'] ?? CommonConstant::DATABASE_VALUE_NO;
//
//        // set properties
//        $this->filterData = $filterData;
//        $this->commonGood = ($commonGood == 'All') ? false : $commonGood;
//        $this->investment = ($investment == 'All') ? false : $investment;
//        $this->deminimis = ($deminimis == 'All') ? false : $deminimis;
//        $this->detailed = ($repCode == 'FAR31') ? true : false;
    }

    private function setFilterProperties($filterData)
    {
        $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($filterData);
        $commonGood = $reportFilterQuery->getFirstValueFilterField('common_good');
        $investment = $reportFilterQuery->getFirstValueFilterField('investment');
        $deminimis = $reportFilterQuery->getFirstValueFilterField('deminimis_asset');
        $entity = $reportFilterQuery->getFirstValueFilterField('ca_entity_id');
        $balanceHistoric = $reportFilterQuery->getFirstValueFilterField('ca_balance_historic');

        // get filter values
        $this->filterData = $filterData;
        $this->commonGood = (!$commonGood) ? false : $commonGood;
        $this->investment = (!$investment) ? false : $investment;
        $this->deminimis = (!$deminimis) ? false : $deminimis;
        $this->entity = (!$entity) ? false : $entity;
        $this->historicValue = (!$balanceHistoric) ? false : $balanceHistoric;
    }


    public function getNumericCols()
    {
        $arrIFRSCat = $this->getIFRSCategories();
        $numCols = [];

        $col = 0;

        foreach ($arrIFRSCat as $key => $category) {
            $col++;
            $numCols[] = $col;
        }

        $numCols[] = $col + 1;

        return $numCols;
    }

    public function balanceSheetMovementsQuery(
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        // Get all IFRS categories the report should include.
        $arrIFRSCat = $this->getIFRSCategories();

        // Get IFRS categories code and description for use in report header.
        $arrIFRSCatDesc = $this->getIFRSCategories(true);

        $bYearEnd = CaSystemManager::isInYearEndMode();
        $yearStartDate = CaSystemManager::getYearStartDate(CommonConstant::DATE_FORMAT_MONTH);
        $yearEndDate = CaSystemManager::getYearEndDate(CommonConstant::DATE_FORMAT_MONTH);
        $this->siteGroupId = \Auth::user()->site_group_id;

        // *****************************************************************
        // REPORT HEADER
        // *****************************************************************
        // Build report header
        $queryRpt = $this->reportHeader($arrIFRSCatDesc);

        // *****************************************************************
        // PART 1 - BFWD Values
        // *****************************************************************

        // Add Cost or Valuation blank format row
        $rowDesc = "Cost or Valuation";
        $queryRpt->unionAll($this->buildLabelRow($rowDesc, $arrIFRSCat));

        // Add First detail row (values at start of year)
        $arrColumn = array("ca_balance.bfwd_gbv");
        $rowDesc = "Gross Book Value " . $yearStartDate;
        $queryRpt->unionAll($this->buildBalanceQuery($rowDesc, $arrIFRSCat, $arrColumn, true));

        // Add detail line (bfwd_accum_dep at start of year).
        $arrColumn = array("ca_balance.bfwd_accum_depn");
        $rowDesc = "Accumulated Depreciation";
        $this->setSumNegative(true);
        $queryRpt->unionAll($this->buildBalanceQuery($rowDesc, $arrIFRSCat, $arrColumn, true));

        // Add detail line (bfwd_accum_imp at start of year).
        $arrColumn = array("ca_balance.bfwd_accum_imp_asset", "ca_balance.bfwd_accum_imp_land");
        $rowDesc = "Accumulated Impairment";
        $this->setSumNegative(true);
        $queryRpt->unionAll($this->buildBalanceQuery($rowDesc, $arrIFRSCat, $arrColumn, true));

        if ($this->historicValue == CommonConstant::DATABASE_VALUE_YES) {
            // Add detail line (bfwd_accum_loss at start of year).
            $arrColumn = array("ca_balance.bfwd_accum_rev_loss_asset", "ca_balance.bfwd_accum_rev_loss_land");
            $rowDesc = "Accumulated Loss";
            $this->setSumNegative(true);
            $queryRpt->unionAll($this->buildBalanceQuery($rowDesc, $arrIFRSCat, $arrColumn, true));
        }

        $queryRpt->unionAll($this->addBlankRow($arrIFRSCat));

        // Add row - Balance as at 31 March XXXX (Column totals)
        $arrColumn = array("ca_balance.bfwd_nbv");
        $rowDesc = "Balance as at " . $yearStartDate;
        $this->setSumNegative(false);
        $queryRpt->unionAll($this->buildBalanceQuery($rowDesc, $arrIFRSCat, $arrColumn, true));


        $queryRpt->unionAll($this->addBlankRow($arrIFRSCat));

        $rowDesc = "Movements in Year:";
        $queryRpt->unionAll($this->buildLabelRow($rowDesc, $arrIFRSCat));

        if ($this->historicValue == CommonConstant::DATABASE_VALUE_NO) {
            $cvHc = 'cv';
        } else {
            $cvHc = 'hc';
        }
        // Add row - IAT
        $arrColumn = array("ca_transaction.{$cvHc}_asset", "ca_transaction.{$cvHc}_land");
        $arrTranType = array(CaTransactionType::INTER_ASSET_TRANSFER);
        $this->setSumNegative(false);
        $queryRpt->unionAll($this->buildTranQuery("Inter Asset Transfers", $arrIFRSCat, $arrColumn, $arrTranType));

        // Add row - Acquisitions
        $arrColumn = array("ca_transaction.{$cvHc}_asset", "ca_transaction.{$cvHc}_land");
        $arrTranType = array(CaTransactionType::ACQUISITION);
        $this->setSumNegative(false);
        $queryRpt->unionAll($this->buildTranQuery("Acquisitions", $arrIFRSCat, $arrColumn, $arrTranType));

        // Add row - Additions.
        $arrColumn = array("ca_transaction.{$cvHc}_asset", "ca_transaction.{$cvHc}_land");
        $arrTranType = array(CaTransactionType::ADDITION);
        $this->setSumNegative(false);
        $queryRpt->unionAll($this->buildTranQuery("Additions", $arrIFRSCat, $arrColumn, $arrTranType));

        // Add row - Derecognition - Other
        $arrColumn = array("ca_transaction.{$cvHc}_asset", "ca_transaction.{$cvHc}_land");
        $arrTranType = array(CaTransactionType::DERECOGNITION);
        $this->setSumNegative(false);
        $queryRpt->unionAll($this->buildTranQuery("Derecognition", $arrIFRSCat, $arrColumn, $arrTranType));

        // Add row - Derecognition - Disposals
        $arrColumn = array("ca_transaction.{$cvHc}_asset", "ca_transaction.{$cvHc}_land");
        $arrTranType = array(CaTransactionType::DISPOSAL);
        $this->setSumNegative(false);
        $queryRpt->unionAll($this->buildTranQuery("Disposals", $arrIFRSCat, $arrColumn, $arrTranType));

        // Add row - Reclassification and Transfers
        $arrColumn = array("ca_transaction.{$cvHc}_asset", "ca_transaction.{$cvHc}_land");
        $arrTranType = array(CaTransactionType::CATEGORY_TRANSFER);
        $this->setSumNegative(false);
        $queryRpt->unionAll($this->buildTranQuery("Transfers", $arrIFRSCat, $arrColumn, $arrTranType));


        // Add row - Impairment.
        $arrColumn = array("ca_transaction.{$cvHc}_asset", "ca_transaction.{$cvHc}_land");
        $arrTranType = array(CaTransactionType::IMPAIRMENT);
        $this->setSumNegative(true);
        $queryRpt->unionAll($this->buildTranQuery("Impairment", $arrIFRSCat, $arrColumn, $arrTranType));

        // Add row - Reversal of Impairment.
        $arrColumn = array("ca_transaction.{$cvHc}_asset", "ca_transaction.{$cvHc}_land");
        $arrTranType = array(CaTransactionType::REVERSAL_OF_IMPAIRMENT);
        $this->setSumNegative(false);
        $queryRpt->unionAll(
            $this->buildTranQuery("Reversal of Impairment", $arrIFRSCat, $arrColumn, $arrTranType, false, true)
        );

        $rowDesc = "Revaluation:";
        $queryRpt->unionAll($this->buildLabelRow($rowDesc, $arrIFRSCat));


        // Add row - Revaluation Surplus.
        $arrColumn = array("ca_transaction.{$cvHc}_asset", "ca_transaction.{$cvHc}_land");
        $arrTranType = array(CaTransactionType::REVALUATION);
        $this->setSumNegative(false);
        $queryRpt->unionAll($this->buildTranQuery("Surplus", $arrIFRSCat, $arrColumn, $arrTranType));

        // Add row - Revaluation Loss.
        $arrColumn = array("ca_transaction.{$cvHc}_asset", "ca_transaction.{$cvHc}_land");
        $arrTranType = array(CaTransactionType::REVALUATION);
        $this->setSumNegative(false);
        $queryRpt->unionAll($this->buildTranQuery("Loss of Value", $arrIFRSCat, $arrColumn, $arrTranType, true));

        // Add row - Reversal of Loss.
        $arrColumn = array("ca_transaction.{$cvHc}_asset", "ca_transaction.{$cvHc}_land");
        $arrTranType = array(CaTransactionType::REVERSAL_OF_LOSS);
        $this->setSumNegative(false);
        $queryRpt->unionAll(
            $this->buildTranQuery("Reversal of Loss", $arrIFRSCat, $arrColumn, $arrTranType, false, true)
        );

        $rowDesc = "Depreciation:";
        $queryRpt->unionAll($this->buildLabelRow($rowDesc, $arrIFRSCat));

        // Add row - Depreciation as at 31 March XXXX (Column totals)
        $arrColumn = array("ca_balance.cy_depreciation");
        $rowDesc = "In Year";
        $this->setSumNegative(true);
        $queryRpt->unionAll($this->buildBalanceQuery($rowDesc, $arrIFRSCat, $arrColumn));

        $queryRpt->unionAll($this->addBlankRow($arrIFRSCat));

        // Add row - NBV as at 31 March XXXX (Column totals)
        $arrColumn = array("ca_balance.cfwd_nbv");
        $rowDesc = "Net Book Value " . $yearEndDate;
        $this->setSumNegative(false);
        $queryRpt->unionAll($this->buildBalanceQuery($rowDesc, $arrIFRSCat, $arrColumn));


        $this->setFilterTexts($whereCodes, $orCodes, $whereTexts);

        return \DB::table(null)->select('*')->from(\DB::raw("(" . $queryRpt->toSql() . ') AS report'));
    }

    private function setFilterTexts(
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        if ($val = Common::iset($this->filterData['deminimis_asset'])) {
            switch ($val) {
                case CommonConstant::ALL:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.deminimis_asset_all'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.deminimis_asset_no'));
                    break;
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.deminimis_asset_yes'));
                    break;
            }
        }

        if ($val = Common::iset($this->filterData['common_good'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.common_good_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.common_good_no'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.common_good_all'));
                    break;
            }
        }


        if ($val = Common::iset($this->filterData['investment'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.investment_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.investment_no'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.investment_all'));
                    break;
            }
        }
        if ($val = Common::iset($this->filterData['ca_balance_historic'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.ca_balance_historic_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.ca_balance_historic_no'));
                    break;
            }
        }
    }

    private function setIncludeRRTrans($bValue)
    {
        $this->bIncludeRRTrans = $bValue;
    }

    private function setRRTrans($value)
    {
        $this->rrTrans = $value;
    }

    private function setSumNegative($bValue)
    {
        $this->bSumNegative = $bValue;
    }

    /**
     * Returns the sql to add a blank row into a csv report.
     * @param array $arrIFRSCat
     * @return string
     */
    protected function addBlankRow(array $arrIFRSCat)
    {
        // First column is the row description label.
        $query =  "' ' `Element`";

        // Add columns for each IFRS category
        foreach ($arrIFRSCat as $key => $value) {
            $query .= " , ' ' ";
        }

        // Calculate row total.
        $query .= " , '' `total` ";

        return \DB::table(null)->select(\DB::raw($query));
    }

    private function getIFRSCategories($bIncludeDesc = false)
    {
        $arrIFRSCat = array();   // Holds all IFRS categories the report should include.

        // Get available ifrs categories to allow the report query to be built dynamically
        $query = CaIfrsCategory::userSiteGroup()
        ->select(
            [
                'ca_ifrs_category_code',
                'ca_ifrs_category_desc',
                'ca_ifrs_category_id'
            ]
        )
        ->where(
            'ca_ifrs_category.active',
            '=',
            CommonConstant::DATABASE_VALUE_YES
        );

        if ($this->commonGood) {
            $query->where('common_good', '=', $this->commonGood);
        }

        if ($this->investment) {
            $query->where('investment', '=', $this->investment);
        }

        $query->orderBy('investment');
        $query->orderBy('depreciable', 'desc');

        // RB 04/11/2019 This can cause the headings to be out of sync with te data Ordering should be the same.
        //if ($bIncludeDesc) {
        //    $query->orderBy(\DB::raw('CONCAT(ca_ifrs_category_code, ca_ifrs_category_desc)'));
        //} else {
        $query->orderBy('ca_ifrs_category_code');
        //}

        $queryIFRS = $query->get();

        // Loop through all ifrs categories and store in array.
        foreach ($queryIFRS as $row) {
            $bDesc = '';
            if ($bIncludeDesc) {
                $bDesc = Common::concatFields([$row['ca_ifrs_category_code'], $row['ca_ifrs_category_desc']]);
            } else {
                $bDesc = $row['ca_ifrs_category_code'];
            }
            $arrIFRSCat[$row['ca_ifrs_category_id']] = $bDesc;
        }

        return $arrIFRSCat;
    }

    private function reportHeader($arrIFRSCatDesc)
    {
        $query = " '' `Element`";
        foreach ($arrIFRSCatDesc as $key => $codeDesc) {
            $query .= " , '' `$codeDesc`";
        }
        $query .= ", '' Totals ";

        return \DB::table(null)->select(\DB::raw($query));
    }

    private function buildLabelRow($rowDesc, array $arrIFRSCat)
    {
        // First column is the row description label.
        $query =  "'$rowDesc' `Element`";

        foreach ($arrIFRSCat as $key => $code) {
            $query .= " , '' `$code`";
        }
        $query .= ", '' Totals ";

        return \DB::table(null)->select(\DB::raw($query));
    }

    private function buildBalanceQuery($rowDesc, array $arrIFRSCat, array $arrColumn, $bUseBfwdIFRS = false)
    {
        // Get columns as a  string.
        $columns = implode(" + ", $arrColumn);

        // First column is the row description label.
        $query =  "'$rowDesc' `Element`";

        // Add columns for each IFRS category
        foreach ($arrIFRSCat as $key => $value) {
            $query .= " , ";
            $query .= " (SELECT IFNULL(";
            $query .= " (SELECT ROUND(IFNULL(SUM($columns)" . ($this->bSumNegative ? ' * -1 ' : '') . ", 0), 0) ";
            $query .= "  FROM ca_ifrs_category ";
            if ($bUseBfwdIFRS) {
                $query .= " INNER JOIN ca_fixed_asset ON
                    ca_fixed_asset.bfwd_ifrs_cat_id = ca_ifrs_category.ca_ifrs_category_id ";
            } else {
                $query .= " INNER JOIN ca_fixed_asset ON
                    ca_fixed_asset.ca_ifrs_cat_id = ca_ifrs_category.ca_ifrs_category_id ";
            }
            $query .= " INNER JOIN ca_balance ON ca_balance.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
            $query .= " LEFT JOIN vw_far_disposed ON
                   vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
            $query .= " INNER JOIN vw_ca_current_fin_year ON
                vw_ca_current_fin_year.ca_fin_year_id = ca_balance.ca_fin_year_id ";
            $query .= "  WHERE ca_ifrs_category.site_group_id = '{$this->siteGroupId}' ";
            $query .= " AND ca_ifrs_category.active = 'Y' ";
            if ($this->historicValue == CommonConstant::DATABASE_VALUE_NO) {
                $query .= " AND ca_balance.historic = 'N' ";
            } else {
                $query .= " AND ca_balance.historic = 'Y' ";
            }
            if ($this->deminimis) {
                $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
            }
            if ($this->entity) {
                $query .= "     AND ca_fixed_asset.ca_entity_id = '{$this->entity}'";
            }
            $query .= " AND ca_fixed_asset.active = 'Y'";
            $query .= " AND ca_ifrs_category.ca_ifrs_category_id = " . $key;
            $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
            $query .= ") , 0)) ";
        }

        // Calculate row total.
        $query .= " , ";
        $query .= " (SELECT IFNULL(";
        $query .= " (SELECT ROUND(IFNULL(SUM($columns)" . ($this->bSumNegative ? ' * -1 ' : '') . ", 0), 0) ";
        $query .= "  FROM ca_ifrs_category ";
        if ($bUseBfwdIFRS) {
            $query .= " INNER JOIN ca_fixed_asset ON
                ca_fixed_asset.bfwd_ifrs_cat_id = ca_ifrs_category.ca_ifrs_category_id ";
        } else {
            $query .= " INNER JOIN ca_fixed_asset ON
                ca_fixed_asset.ca_ifrs_cat_id = ca_ifrs_category.ca_ifrs_category_id ";
        }
        $query .= " INNER JOIN ca_balance ON ca_balance.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
        $query .= " INNER JOIN vw_ca_current_fin_year ON
            vw_ca_current_fin_year.ca_fin_year_id = ca_balance.ca_fin_year_id ";
        $query .= " LEFT JOIN vw_far_disposed ON
               vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
        $query .= " WHERE ca_ifrs_category.site_group_id = '{$this->siteGroupId}' ";
        $query .= "     AND ca_ifrs_category.active = 'Y' ";
        if ($this->deminimis) {
            $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
        }
        if ($this->entity) {
            $query .= "     AND ca_fixed_asset.ca_entity_id = '{$this->entity}'";
        }
        $query .= "     AND ca_fixed_asset.active = 'Y'";
        if ($this->commonGood) {
            $query .= " AND ca_ifrs_category.common_good = '{$this->commonGood}'";
        }
        if ($this->investment) {
            $query .= " AND ca_ifrs_category.investment = '{$this->investment}'";
        }
        $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";

        if ($this->historicValue == CommonConstant::DATABASE_VALUE_NO) {
            $query .= " AND ca_balance.historic = 'N' ";
        } else {
            $query .= " AND ca_balance.historic = 'Y' ";
        }

        $query .= " ),0)) `total` ";

        return \DB::table(null)->select(\DB::raw($query));
    }

    private function buildTranQuery(
        $rowDesc,
        array $arrIFRSCat,
        array $arrColumn,
        array $arrTranTypes,
        $revalLoss = false,
        $reversal = false
    ) {
        // Get columns and transactions types as a comma separated string.
        $columns = implode(" + ", $arrColumn);
        if ($reversal) {
            if ($this->historicValue == CommonConstant::DATABASE_VALUE_NO) {
                $columns = "($columns) - cv_depn_adjustment";
            } else {
                $columns = "($columns) - hc_depn_adjustment";
            }
        }
        $tranTypes = implode(",", $arrTranTypes);

        // First column is the row description label.
        $query =  "'$rowDesc' `Element`";

        // Add columns for each IFRS category
        foreach ($arrIFRSCat as $key => $value) {
            $query .= ", ";
            $query .= " ROUND(IFNULL((SELECT SUM($columns) " . ($this->bSumNegative ? " * -1 " : "");
            $query .= " FROM ca_transaction ";
            $query .= " INNER JOIN vw_ca_current_fin_year ON
                vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
            $query .= " INNER JOIN ca_fixed_asset ON
                ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
            $query .= " LEFT JOIN vw_far_disposed ON
                   vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
            $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId'";
            $query .= " AND ca_transaction_type_id IN ($tranTypes)";
            $query .= " AND ca_transaction.ca_ifrs_cat_id = $key";
            if ($tranTypes == CaTransactionType::REVALUATION) {
                if ($revalLoss) {
                    $query .= " AND COALESCE($columns, 0) <  0";
                } else {
                    $query .= " AND COALESCE($columns, 0) >  0";
                }
            }
            if ($this->deminimis) {
                $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
            }
            if ($this->entity) {
                $query .= "     AND ca_fixed_asset.ca_entity_id = '{$this->entity}'";
            }
            $query .= " AND ca_fixed_asset.active = 'Y'";
            if ($this->bIncludeRRTrans) {
                $query .= " AND ca_transaction.rr_trans = '{$this->rrTrans}'";
            }
            $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
            $query .= "),0),0)";
            $query .= " `$value`";
        }

        // Add row total column
        $query .= " , ";
        $query .= " ROUND(IFNULL((SELECT SUM($columns) " . ($this->bSumNegative ? " * -1 " : "");
        $query .= " FROM ca_transaction ";
        $query .= " INNER JOIN ca_ifrs_category ON
            ca_ifrs_category.ca_ifrs_category_id = ca_transaction.ca_ifrs_cat_id ";
        $query .= " INNER JOIN vw_ca_current_fin_year ON
            vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
        $query .= " INNER JOIN ca_fixed_asset ON ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
        $query .= " LEFT JOIN vw_far_disposed ON
            vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
        $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
        $query .= " AND ca_ifrs_category.active = 'Y'";
        $query .= " AND ca_transaction_type_id IN ($tranTypes)";
        if ($tranTypes == CaTransactionType::REVALUATION) {
            if ($revalLoss) {
                $query .= " AND COALESCE($columns, 0) <  0";
            } else {
                $query .= " AND COALESCE($columns, 0) >  0";
            }
        }

        if ($this->deminimis) {
            $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
        }
        if ($this->entity) {
            $query .= "     AND ca_fixed_asset.ca_entity_id = '{$this->entity}'";
        }
        $query .= " AND ca_fixed_asset.active = 'Y'";
        if ($this->bIncludeRRTrans) {
            $query .= " AND ca_transaction.rr_trans = '{$this->rrTrans}'";
        }
        if ($this->commonGood) {
            $query .= " AND ca_ifrs_category.common_good = '{$this->commonGood}'";
        }
        if ($this->investment) {
            $query .= " AND ca_ifrs_category.investment = '{$this->investment}'";
        }
        $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
        $query .= "),0),0)";
        $query .= " `Total`";

        return \DB::table(null)->select(\DB::raw($query));
    }

    private function buildRevaluationToRR($rowDesc, array $arrIFRSCat)
    {
        $query =  "'$rowDesc' `Element`";

        foreach ($arrIFRSCat as $id => $code) {
            $query .= ", ";
            $query .= " ROUND(";
            $query .= " (SELECT IFNULL(SUM( ";

            $query .= " if(`ca_transaction`.`rr_trans` = 'Y',";
            $query .= "    if (ca_transaction.hc_asset = 0 && ca_transaction.hc_land = 0";
            $query .= "       , ca_transaction.gbv_change_asset + ca_transaction.gbv_change_land";
            $query .= "       ,((`ca_transaction`.`cv_asset` + `ca_transaction`.`cv_land`) -
                (`ca_transaction`.`hc_asset` + `ca_transaction`.`hc_land`)) -
                `ca_transaction`.`depn_wo` - `ca_transaction`.`imp_wo_asset` - `ca_transaction`.`imp_wo_land`)";
            $query .= "  ,0)";
            $query .= " ), 0)";

            $query .= " FROM ca_transaction ";
            $query .= " INNER JOIN vw_ca_current_fin_year ON
                vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
            $query .= " INNER JOIN ca_fixed_asset ON
                ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
            $query .= " LEFT JOIN vw_far_disposed ON
                   vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
            $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
            $query .= " AND ca_transaction_type_id = " . CaTransactionType::REVALUATION;
            $query .= " AND ca_transaction.ca_ifrs_cat_id = " . $id;
            if ($this->deminimis) {
                $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
            }
            if ($this->entity) {
                $query .= "     AND ca_fixed_asset.ca_entity_id = '{$this->entity}'";
            }
            $query .= " AND ca_fixed_asset.active = 'Y'";
            $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
            $query .= " )";

            $query .= ", 0)";
            $query .= " `" . $code . "`";
        }

        // Add row total column
        $query .= " , ";
        $query .= " ROUND(";
        $query .= " (SELECT IFNULL(SUM( ";

        $query .= " if(`ca_transaction`.`rr_trans` = 'Y',";
        $query .= "    if (ca_transaction.hc_asset = 0 && ca_transaction.hc_land = 0";
        $query .= "       , ca_transaction.gbv_change_asset + ca_transaction.gbv_change_land";
        $query .= "       ,((`ca_transaction`.`cv_asset` + `ca_transaction`.`cv_land`) -
            (`ca_transaction`.`hc_asset` + `ca_transaction`.`hc_land`)) -
            `ca_transaction`.`depn_wo` - `ca_transaction`.`imp_wo_asset` - `ca_transaction`.`imp_wo_land`)";
        $query .= "  ,0)";

        $query .= " ), 0)";
        $query .= " FROM ca_transaction ";
        $query .= " INNER JOIN vw_ca_current_fin_year ON
            vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
        $query .= " INNER JOIN ca_fixed_asset ON
            ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
        $query .= " LEFT JOIN vw_far_disposed ON
               vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
        $query .= " INNER JOIN ca_ifrs_category ON
            ca_ifrs_category.ca_ifrs_category_id = ca_transaction.ca_ifrs_cat_id ";
        $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
        $query .= "    AND ca_transaction_type_id = " . CaTransactionType::REVALUATION;
        if ($this->deminimis) {
            $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
        }
        if ($this->entity) {
            $query .= "     AND ca_fixed_asset.ca_entity_id = '{$this->entity}'";
        }
        $query .= "    AND ca_fixed_asset.active = 'Y'";
        if ($this->commonGood) {
            $query .= " AND ca_ifrs_category.common_good = '{$this->commonGood}'";
        }
        if ($this->investment) {
            $query .= " AND ca_ifrs_category.investment = '{$this->investment}'";
        }
        $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
        $query .= ")";
        $query .= ", 0)";
        $query .= " `Total`";

        return \DB::table(null)->select(\DB::raw($query));
    }

    private function buildRevaluationGainsToRR($rowDesc, array $arrIFRSCat)
    {
        $query =  "'$rowDesc' `Element`";

        foreach ($arrIFRSCat as $id => $code) {
            $query .= ", ";
            $query .= " ROUND(";
            $query .= " (SELECT IFNULL(SUM( ";

            $query .= " if(`ca_transaction`.`rr_trans` = 'Y',";
            $query .= "    if (ca_transaction.hc_asset = 0 && ca_transaction.hc_land = 0";
            $query .= "       , if(ca_transaction.gbv_change_asset + ca_transaction.gbv_change_land > 0, ";
            $query .= "         ca_transaction.gbv_change_asset + ca_transaction.gbv_change_land, 0)";
            $query .= "       , if(((`ca_transaction`.`cv_asset` + `ca_transaction`.`cv_land`) - ";
            $query .= "         (`ca_transaction`.`hc_asset` + `ca_transaction`.`hc_land`)) - ";
            $query .= "         `ca_transaction`.`depn_wo` - `ca_transaction`.`imp_wo_asset` -  ";
            $query .= "         `ca_transaction`.`imp_wo_land` > 0, ((`ca_transaction`.`cv_asset` + ";
            $query .= "         `ca_transaction`.`cv_land`) - (`ca_transaction`.`hc_asset` + ";
            $query .= "         `ca_transaction`.`hc_land`)) - `ca_transaction`.`depn_wo` - ";
            $query .= "         `ca_transaction`.`imp_wo_asset` -  `ca_transaction`.`imp_wo_land`, 0))";
            $query .= "  ,0)";
            $query .= " ), 0)";

            $query .= " FROM ca_transaction ";
            $query .= " INNER JOIN vw_ca_current_fin_year ON
                vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
            $query .= " INNER JOIN ca_fixed_asset ON
                ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
            $query .= " LEFT JOIN vw_far_disposed ON
                   vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
            $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
            $query .= " AND ca_transaction_type_id = " . CaTransactionType::REVALUATION;
            $query .= " AND ca_transaction.ca_ifrs_cat_id = " . $id;
            if ($this->deminimis) {
                $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
            }
            if ($this->entity) {
                $query .= "     AND ca_fixed_asset.ca_entity_id = '{$this->entity}'";
            }
            $query .= " AND ca_fixed_asset.active = 'Y'";
            $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
            $query .= " )";

            $query .= ", 0)";
            $query .= " `" . $code . "`";
        }

        // Add row total column
        $query .= " , ";
        $query .= " ROUND(";
        $query .= " (SELECT IFNULL(SUM( ";

        $query .= " if(`ca_transaction`.`rr_trans` = 'Y',";
        $query .= "    if (ca_transaction.hc_asset = 0 && ca_transaction.hc_land = 0";
        $query .= "       , if(ca_transaction.gbv_change_asset + ca_transaction.gbv_change_land > 0, ";
        $query .= "         ca_transaction.gbv_change_asset + ca_transaction.gbv_change_land, 0)";
        $query .= "       , if(((`ca_transaction`.`cv_asset` + `ca_transaction`.`cv_land`) - ";
        $query .= "         (`ca_transaction`.`hc_asset` + `ca_transaction`.`hc_land`)) - ";
        $query .= "         `ca_transaction`.`depn_wo` - `ca_transaction`.`imp_wo_asset` -  ";
        $query .= "         `ca_transaction`.`imp_wo_land` > 0, ((`ca_transaction`.`cv_asset` + ";
        $query .= "         `ca_transaction`.`cv_land`) - (`ca_transaction`.`hc_asset` + ";
        $query .= "         `ca_transaction`.`hc_land`)) - `ca_transaction`.`depn_wo` - ";
        $query .= "         `ca_transaction`.`imp_wo_asset` -  `ca_transaction`.`imp_wo_land`, 0))";
        $query .= "  ,0)";
        $query .= " ), 0)";
        $query .= " FROM ca_transaction ";
        $query .= " INNER JOIN vw_ca_current_fin_year ON
            vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
        $query .= " INNER JOIN ca_fixed_asset ON
            ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
        $query .= " LEFT JOIN vw_far_disposed ON
               vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
        $query .= " INNER JOIN ca_ifrs_category ON
            ca_ifrs_category.ca_ifrs_category_id = ca_transaction.ca_ifrs_cat_id ";
        $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
        $query .= "    AND ca_transaction_type_id = " . CaTransactionType::REVALUATION;
        if ($this->deminimis) {
            $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
        }
        if ($this->entity) {
            $query .= "     AND ca_fixed_asset.ca_entity_id = '{$this->entity}'";
        }
        $query .= "    AND ca_fixed_asset.active = 'Y'";
        if ($this->commonGood) {
            $query .= " AND ca_ifrs_category.common_good = '{$this->commonGood}'";
        }
        if ($this->investment) {
            $query .= " AND ca_ifrs_category.investment = '{$this->investment}'";
        }
        $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
        $query .= ")";
        $query .= ", 0)";
        $query .= " `Total`";

        return \DB::table(null)->select(\DB::raw($query));
    }

    private function buildRevaluationLossesToRR($rowDesc, array $arrIFRSCat)
    {
        $query =  "'$rowDesc' `Element`";

        foreach ($arrIFRSCat as $id => $code) {
            $query .= ", ";
            $query .= " ROUND(";
            $query .= " (SELECT IFNULL(SUM( ";

            $query .= " if(`ca_transaction`.`rr_trans` = 'Y',";
            $query .= "    if (ca_transaction.hc_asset = 0 && ca_transaction.hc_land = 0";
            $query .= "       , if(ca_transaction.gbv_change_asset + ca_transaction.gbv_change_land < 0, ";
            $query .= "         ca_transaction.gbv_change_asset + ca_transaction.gbv_change_land, 0)";
            $query .= "       , if(((`ca_transaction`.`cv_asset` + `ca_transaction`.`cv_land`) - ";
            $query .= "         (`ca_transaction`.`hc_asset` + `ca_transaction`.`hc_land`)) - ";
            $query .= "         `ca_transaction`.`depn_wo` - `ca_transaction`.`imp_wo_asset` -  ";
            $query .= "         `ca_transaction`.`imp_wo_land` < 0, ((`ca_transaction`.`cv_asset` + ";
            $query .= "         `ca_transaction`.`cv_land`) - (`ca_transaction`.`hc_asset` + ";
            $query .= "         `ca_transaction`.`hc_land`)) - `ca_transaction`.`depn_wo` - ";
            $query .= "         `ca_transaction`.`imp_wo_asset` -  `ca_transaction`.`imp_wo_land`, 0))";
            $query .= "  ,0)";
            $query .= " ), 0)";

            $query .= " FROM ca_transaction ";
            $query .= " INNER JOIN vw_ca_current_fin_year ON
                vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
            $query .= " INNER JOIN ca_fixed_asset ON
                ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
            $query .= " LEFT JOIN vw_far_disposed ON
                   vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
            $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
            $query .= " AND ca_transaction_type_id = " . CaTransactionType::REVALUATION;
            $query .= " AND ca_transaction.ca_ifrs_cat_id = " . $id;
            if ($this->deminimis) {
                $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
            }
            if ($this->entity) {
                $query .= "     AND ca_fixed_asset.ca_entity_id = '{$this->entity}'";
            }
            $query .= " AND ca_fixed_asset.active = 'Y'";
            $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
            $query .= " )";

            $query .= ", 0)";
            $query .= " `" . $code . "`";
        }

        // Add row total column
        $query .= " , ";
        $query .= " ROUND(";
        $query .= " (SELECT IFNULL(SUM( ";

        $query .= " if(`ca_transaction`.`rr_trans` = 'Y',";
        $query .= "    if (ca_transaction.hc_asset = 0 && ca_transaction.hc_land = 0";
        $query .= "       , if(ca_transaction.gbv_change_asset + ca_transaction.gbv_change_land < 0, ";
        $query .= "         ca_transaction.gbv_change_asset + ca_transaction.gbv_change_land, 0)";
        $query .= "       , if(((`ca_transaction`.`cv_asset` + `ca_transaction`.`cv_land`) - ";
        $query .= "         (`ca_transaction`.`hc_asset` + `ca_transaction`.`hc_land`)) - ";
        $query .= "         `ca_transaction`.`depn_wo` - `ca_transaction`.`imp_wo_asset` -  ";
        $query .= "         `ca_transaction`.`imp_wo_land` < 0, ((`ca_transaction`.`cv_asset` + ";
        $query .= "         `ca_transaction`.`cv_land`) - (`ca_transaction`.`hc_asset` + ";
        $query .= "         `ca_transaction`.`hc_land`)) - `ca_transaction`.`depn_wo` - ";
        $query .= "         `ca_transaction`.`imp_wo_asset` -  `ca_transaction`.`imp_wo_land`, 0))";
        $query .= "  ,0)";
        $query .= " ), 0)";
        $query .= " FROM ca_transaction ";
        $query .= " INNER JOIN vw_ca_current_fin_year ON
            vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
        $query .= " INNER JOIN ca_fixed_asset ON
            ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
        $query .= " LEFT JOIN vw_far_disposed ON
               vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
        $query .= " INNER JOIN ca_ifrs_category ON
            ca_ifrs_category.ca_ifrs_category_id = ca_transaction.ca_ifrs_cat_id ";
        $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
        $query .= "    AND ca_transaction_type_id = " . CaTransactionType::REVALUATION;
        if ($this->deminimis) {
            $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
        }
        if ($this->entity) {
            $query .= "     AND ca_fixed_asset.ca_entity_id = '{$this->entity}'";
        }
        $query .= "    AND ca_fixed_asset.active = 'Y'";
        if ($this->commonGood) {
            $query .= " AND ca_ifrs_category.common_good = '{$this->commonGood}'";
        }
        if ($this->investment) {
            $query .= " AND ca_ifrs_category.investment = '{$this->investment}'";
        }
        $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
        $query .= ")";
        $query .= ", 0)";
        $query .= " `Total`";

        return \DB::table(null)->select(\DB::raw($query));
    }

    private function buildRevaluationAndReversalOfLossToSurplusDeficit($rowDesc, array $arrIFRSCat)
    {
        $query =  "'$rowDesc' `Element`";

        foreach ($arrIFRSCat as $id => $code) {
            $query .= ", ";
            $query .= " ROUND(";
            $query .= " (SELECT IFNULL(SUM( ";

            $query .= "  if(`ca_transaction`.`rr_trans` = 'Y', ";
            $query .= "     if(ca_transaction.hc_asset = 0 && ca_transaction.hc_land = 0 ";
            $query .= "        , 0 ";
            $query .= "        , ca_transaction.hc_asset + ca_transaction.hc_land)";
            $query .= "     ,(`ca_transaction`.`gbv_change_asset` + `ca_transaction`.`gbv_change_land`))";

            $query .= " ), 0)";

            $query .= " FROM ca_transaction ";
            $query .= "    INNER JOIN vw_ca_current_fin_year ON
                vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
            $query .= "    INNER JOIN ca_fixed_asset ON
                ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
            $query .= " LEFT JOIN vw_far_disposed ON
                   vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
            $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
            $query .= "    AND ca_transaction_type_id = " . CaTransactionType::REVALUATION;
            $query .= "    AND ca_transaction.ca_ifrs_cat_id = " . $id;
            if ($this->deminimis) {
                $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
            }
            if ($this->entity) {
                $query .= "     AND ca_fixed_asset.ca_entity_id = '{$this->entity}'";
            }
            $query .= "    AND ca_fixed_asset.active = 'Y'";
            $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
            $query .= " )";
            $query .= " + ";
            $query .= " (SELECT IFNULL(SUM(ca_transaction.gbv_change_asset + ca_transaction.gbv_change_land), 0) ";
            $query .= " FROM ca_transaction ";
            $query .= "    INNER JOIN vw_ca_current_fin_year ON
                vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
            $query .= "    INNER JOIN ca_fixed_asset ON
                ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
            $query .= " LEFT JOIN vw_far_disposed ON
                   vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
            $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
            $query .= "    AND ca_transaction_type_id IN
                (" . CaTransactionType::REVERSAL_OF_IMPAIRMENT . "," . CaTransactionType::REVERSAL_OF_LOSS . ")";
            $query .= "    AND ca_transaction.ca_ifrs_cat_id = " . $id;
            if ($this->deminimis) {
                $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
            }
            if ($this->entity) {
                $query .= "     AND ca_fixed_asset.ca_entity_id = '{$this->entity}'";
            }
            $query .= "    AND ca_fixed_asset.active = 'Y'";
            $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
            $query .= ")";

            $query .= ", 0)";
            $query .= " `" . $code . "`";
        }
        // Add row total
        $query .= " , ";
        $query .= " ROUND(";
        $query .= " (SELECT IFNULL(SUM( ";

        $query .= "  if(`ca_transaction`.`rr_trans` = 'Y', ";
        $query .= "     if(ca_transaction.hc_asset = 0 && ca_transaction.hc_land = 0 ";
        $query .= "        , 0 ";
        $query .= "        , ca_transaction.hc_asset + ca_transaction.hc_land)";
        $query .= "     ,(`ca_transaction`.`gbv_change_asset` + `ca_transaction`.`gbv_change_land`))";

        $query .= " ), 0)";
        $query .= " FROM ca_transaction ";
        $query .= "    INNER JOIN ca_ifrs_category ON
            ca_ifrs_category.ca_ifrs_category_id = ca_transaction.ca_ifrs_cat_id ";
        $query .= "    INNER JOIN vw_ca_current_fin_year ON
            vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
        $query .= "    INNER JOIN ca_fixed_asset ON
            ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
        $query .= " LEFT JOIN vw_far_disposed ON
               vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
        $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
        $query .= "    AND ca_transaction_type_id = " . CaTransactionType::REVALUATION;
        $query .= "    AND ca_ifrs_category.active = 'Y'";
        if ($this->deminimis) {
            $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
        }
        if ($this->entity) {
            $query .= "     AND ca_fixed_asset.ca_entity_id = '{$this->entity}'";
        }
        $query .= "    AND ca_fixed_asset.active = 'Y'";
        if ($this->commonGood) {
            $query .= " AND ca_ifrs_category.common_good = '{$this->commonGood}'";
        }
        if ($this->investment) {
            $query .= " AND ca_ifrs_category.investment = '{$this->investment}'";
        }
        $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
        $query .= " )";
        $query .= " + ";
        $query .= " (SELECT IFNULL(SUM(ca_transaction.gbv_change_asset + ca_transaction.gbv_change_land), 0) ";
        $query .= " FROM ca_transaction ";
        $query .= "    INNER JOIN ca_ifrs_category ON
            ca_ifrs_category.ca_ifrs_category_id = ca_transaction.ca_ifrs_cat_id ";
        $query .= "    INNER JOIN vw_ca_current_fin_year ON
            vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
        $query .= "    INNER JOIN ca_fixed_asset ON
            ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
        $query .= " LEFT JOIN vw_far_disposed ON
               vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
        $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
        $query .= "    AND ca_ifrs_category.active = 'Y'";
        if ($this->deminimis) {
            $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
        }
        if ($this->entity) {
            $query .= "     AND ca_fixed_asset.ca_entity_id = '{$this->entity}'";
        }
        $query .= "    AND ca_fixed_asset.active = 'Y'";
        $query .= "    AND ca_transaction_type_id IN
            (" . CaTransactionType::REVERSAL_OF_IMPAIRMENT . "," . CaTransactionType::REVERSAL_OF_LOSS . ")";
        if ($this->commonGood) {
            $query .= " AND ca_ifrs_category.common_good = '{$this->commonGood}'";
        }
        if ($this->investment) {
            $query .= " AND ca_ifrs_category.investment = '{$this->investment}'";
        }
        $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
        $query .= ")";
        $query .= ", 0)";
        $query .= " `Total`";

        return \DB::table(null)->select(\DB::raw($query));
    }

    private function buildRevaluationToSurplusDeficit($rowDesc, array $arrIFRSCat)
    {
        $query =  "'$rowDesc' `Element`";

        foreach ($arrIFRSCat as $id => $code) {
            $query .= ", ";
            $query .= " ROUND(";
            $query .= " (SELECT IFNULL(SUM( ";

            $query .= "  if(`ca_transaction`.`rr_trans` = 'Y', ";
            $query .= "     if(ca_transaction.hc_asset = 0 && ca_transaction.hc_land = 0 ";
            $query .= "        , 0 ";
            $query .= "        , ca_transaction.hc_asset + ca_transaction.hc_land)";
            $query .= "     ,(`ca_transaction`.`gbv_change_asset` + `ca_transaction`.`gbv_change_land`))";

            $query .= " ), 0)";

            $query .= " FROM ca_transaction ";
            $query .= "    INNER JOIN vw_ca_current_fin_year ON
                vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
            $query .= "    INNER JOIN ca_fixed_asset ON
                ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
            $query .= " LEFT JOIN vw_far_disposed ON
                   vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
            $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
            $query .= "    AND ca_transaction_type_id = " . CaTransactionType::REVALUATION;
            $query .= "    AND ca_transaction.ca_ifrs_cat_id = " . $id;
            if ($this->deminimis) {
                $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
            }
            if ($this->entity) {
                $query .= "     AND ca_fixed_asset.ca_entity_id = '{$this->entity}'";
            }
            $query .= "    AND ca_fixed_asset.active = 'Y'";
            $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
            $query .= " )";
            $query .= ", 0)";
            $query .= " `" . $code . "`";
        }
        // Add row total
        $query .= " , ";
        $query .= " ROUND(";
        $query .= " (SELECT IFNULL(SUM( ";

        $query .= "  if(`ca_transaction`.`rr_trans` = 'Y', ";
        $query .= "     if(ca_transaction.hc_asset = 0 && ca_transaction.hc_land = 0 ";
        $query .= "        , 0 ";
        $query .= "        , ca_transaction.hc_asset + ca_transaction.hc_land)";
        $query .= "     ,(`ca_transaction`.`gbv_change_asset` + `ca_transaction`.`gbv_change_land`))";

        $query .= " ), 0)";
        $query .= " FROM ca_transaction ";
        $query .= "    INNER JOIN ca_ifrs_category ON
            ca_ifrs_category.ca_ifrs_category_id = ca_transaction.ca_ifrs_cat_id ";
        $query .= "    INNER JOIN vw_ca_current_fin_year ON
            vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
        $query .= "    INNER JOIN ca_fixed_asset ON
            ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
        $query .= " LEFT JOIN vw_far_disposed ON
               vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
        $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
        $query .= "    AND ca_transaction_type_id = " . CaTransactionType::REVALUATION;
        $query .= "    AND ca_ifrs_category.active = 'Y'";
        if ($this->deminimis) {
            $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
        }
        if ($this->entity) {
            $query .= "     AND ca_fixed_asset.ca_entity_id = '{$this->entity}'";
        }
        $query .= "    AND ca_fixed_asset.active = 'Y'";
        if ($this->commonGood) {
            $query .= " AND ca_ifrs_category.common_good = '{$this->commonGood}'";
        }
        if ($this->investment) {
            $query .= " AND ca_ifrs_category.investment = '{$this->investment}'";
        }
        $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
        $query .= " )";
        $query .= ", 0)";
        $query .= " `Total`";

        return \DB::table(null)->select(\DB::raw($query));
    }

    private function buildReversalOfLossToSurplusDeficit($rowDesc, array $arrIFRSCat)
    {
        $query =  "'$rowDesc' `Element`";

        foreach ($arrIFRSCat as $id => $code) {
            $query .= ", ";
            $query .= " ROUND(";
            $query .= " (SELECT IFNULL(SUM(ca_transaction.gbv_change_asset + ca_transaction.gbv_change_land), 0) ";
            $query .= " FROM ca_transaction ";
            $query .= "    INNER JOIN vw_ca_current_fin_year ON
                vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
            $query .= "    INNER JOIN ca_fixed_asset ON
                ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
            $query .= " LEFT JOIN vw_far_disposed ON
                   vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
            $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
            $query .= "    AND ca_transaction_type_id IN
                (" . CaTransactionType::REVERSAL_OF_IMPAIRMENT . "," . CaTransactionType::REVERSAL_OF_LOSS . ")";
            $query .= "    AND ca_transaction.ca_ifrs_cat_id = " . $id;
            if ($this->deminimis) {
                $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
            }
            if ($this->entity) {
                $query .= "     AND ca_fixed_asset.ca_entity_id = '{$this->entity}'";
            }
            $query .= "    AND ca_fixed_asset.active = 'Y'";
            $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
            $query .= ")";
            $query .= ", 0)";
            $query .= " `" . $code . "`";
        }
        // Add row total
        $query .= " , ";
        $query .= " ROUND(";
        $query .= " (SELECT IFNULL(SUM(ca_transaction.gbv_change_asset + ca_transaction.gbv_change_land), 0) ";
        $query .= " FROM ca_transaction ";
        $query .= "    INNER JOIN ca_ifrs_category ON
            ca_ifrs_category.ca_ifrs_category_id = ca_transaction.ca_ifrs_cat_id ";
        $query .= "    INNER JOIN vw_ca_current_fin_year ON
            vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
        $query .= "    INNER JOIN ca_fixed_asset ON
            ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
        $query .= " LEFT JOIN vw_far_disposed ON
               vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
        $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
        $query .= "    AND ca_ifrs_category.active = 'Y'";
        if ($this->deminimis) {
            $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
        }
        if ($this->entity) {
            $query .= "     AND ca_fixed_asset.ca_entity_id = '{$this->entity}'";
        }
        $query .= "    AND ca_fixed_asset.active = 'Y'";
        $query .= "    AND ca_transaction_type_id IN
            (" . CaTransactionType::REVERSAL_OF_IMPAIRMENT . "," . CaTransactionType::REVERSAL_OF_LOSS . ")";
        if ($this->commonGood) {
            $query .= " AND ca_ifrs_category.common_good = '{$this->commonGood}'";
        }
        if ($this->investment) {
            $query .= " AND ca_ifrs_category.investment = '{$this->investment}'";
        }
        $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
        $query .= ")";
        $query .= ", 0)";
        $query .= " `Total`";

        return \DB::table(null)->select(\DB::raw($query));
    }

    private function buildDepreciationRR($rowDesc, array $arrIFRSCat, $gains)
    {
        // First column is the row description label.
        $query =  "'$rowDesc' `Element`";

        // set the compare operator to select for gains (positive) or losses (negative)
        $op = ($gains) ? "<" : ">";

        // Add columns for each IFRS category
        foreach ($arrIFRSCat as $key => $value) {
            $query .= ", ";
            $query .= " ROUND(IFNULL((SELECT SUM(if(ca_transaction.depn_wo " . $op . " 0, ca_transaction.depn_wo, 0))";
            $query .= "      * -1 ";
            $query .= " FROM ca_transaction ";
            $query .= " INNER JOIN vw_ca_current_fin_year ON
                vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
            $query .= " INNER JOIN ca_fixed_asset ON
                ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
            $query .= " LEFT JOIN vw_far_disposed ON
                   vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
            $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId'";
            $query .= " AND ca_transaction_type_id = " . CaTransactionType::REVALUATION;
            $query .= " AND ca_transaction.ca_ifrs_cat_id = $key";
            if ($this->deminimis) {
                $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
            }
            if ($this->entity) {
                $query .= "     AND ca_fixed_asset.ca_entity_id = '{$this->entity}'";
            }
            $query .= " AND ca_fixed_asset.active = 'Y'";
            $query .= " AND ca_transaction.rr_trans = 'Y'";
            $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
            $query .= "),0),0)";
            $query .= " `$value`";
        }

        // Add row total column
        $query .= " , ";
        $query .= " ROUND(IFNULL((SELECT SUM(if(ca_transaction.depn_wo " . $op . " 0, ca_transaction.depn_wo, 0))";
        $query .= "      * -1 ";
        $query .= " FROM ca_transaction ";
        $query .= " INNER JOIN ca_ifrs_category ON
            ca_ifrs_category.ca_ifrs_category_id = ca_transaction.ca_ifrs_cat_id ";
        $query .= " INNER JOIN vw_ca_current_fin_year ON
            vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id ";
        $query .= " INNER JOIN ca_fixed_asset ON ca_fixed_asset.ca_fixed_asset_id = ca_transaction.ca_fixed_asset_id ";
        $query .= " LEFT JOIN vw_far_disposed ON
               vw_far_disposed.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id ";
        $query .= " WHERE ca_transaction.site_group_id = '$this->siteGroupId' ";
        $query .= " AND ca_ifrs_category.active = 'Y'";
        $query .= " AND ca_transaction_type_id = " . CaTransactionType::REVALUATION;
        if ($this->deminimis) {
            $query .= "     AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
        }
        if ($this->entity) {
            $query .= "     AND ca_fixed_asset.ca_entity_id = '{$this->entity}'";
        }
        $query .= " AND ca_fixed_asset.active = 'Y'";
        $query .= " AND ca_transaction.rr_trans = 'Y'";
        if ($this->commonGood) {
            $query .= " AND ca_ifrs_category.common_good = '{$this->commonGood}'";
        }
        if ($this->investment) {
            $query .= " AND ca_ifrs_category.investment = '{$this->investment}'";
        }
        $query .= " AND vw_far_disposed.ca_fixed_asset_id IS NULL";
        $query .= "),0),0)";
        $query .= " `Total`";

        return \DB::table(null)->select(\DB::raw($query));
    }
}
