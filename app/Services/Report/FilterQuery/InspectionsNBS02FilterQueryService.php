<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\Common;
use Carbon\Carbon;
use Tfcloud\Models\FinAuthStatus;
use Tfcloud\Models\GenTable;
use Tfcloud\Models\InstructionStatusType;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\Instruction\InstructionListService;
use Tfcloud\Services\PermissionService;

class InspectionsNBS02FilterQueryService extends BaseService
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    public function reAddInspectionQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts,
        $helpcallUserDefinedFields = false
    ) {
        $listQuery = (new InstructionListService($this->permissionService))
            ->filterAll($query, $filterData, $viewName, true, false, $helpcallUserDefinedFields)
        ;

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['desc'])) {
            array_push($whereTexts, "Description contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['reference'])) {
            array_push($whereTexts, "Reference contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['instructionType'])) {
            array_push(
                $whereCodes,
                ['instruction_type', 'instruction_type_id', 'instruction_type_name', $val, 'Instruction Type']
            );
        }

        if ($val = Common::iset($filterData['trade_code'])) {
            array_push(
                $whereCodes,
                ['trade_code', 'trade_code_id', 'trade_code_code', $val, 'Trade Code']
            );
        }

        if ($val = Common::iset($filterData['parentType'])) {
            array_push(
                $whereCodes,
                [
                    'instruction_parent_type',
                    'instruction_parent_type_id',
                    'instruction_parent_type_name',
                    $val,
                    'Parent Type'
                ]
            );
        }

        if ($val = Common::iset($filterData['parentCode'])) {
            array_push($whereTexts, "Parent Code contains '{$filterData['parentCode']}'");
        }

        if ($val = Common::iset($filterData['account'])) {
            array_push($whereCodes, [
                'fin_account',
                'fin_account_id',
                'fin_account_code',
                $val, \Lang::get('text.account')
            ]);
        }

        if ($val = Common::iset($filterData['tradeCode'])) {
            array_push(
                $whereCodes,
                [
                    'trade_code',
                    'trade_code_id',
                    'trade_code_code',
                    $val,
                    'Trade Code'
                ]
            );
        }

        if ($val = Common::iset($filterData['siteType'])) {
            array_push($whereCodes, ['site_type', 'site_type_id', 'site_type_code', $val, 'Site Type']);
        }

        if ($val = Common::iset($filterData['supplier_contact_id'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, \Lang::get('text.supplier')]);
        }

        if ($val = Common::iset($filterData['authoriser'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, \Lang::get('text.authoriser')]);
        }

        if ($val = Common::iset($filterData['owner'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, \Lang::get('text.owner')]);
        }

        if ($val = Common::iset($filterData['finYear'])) {
            array_push(
                $whereCodes,
                ['fin_year', 'fin_year_id', 'fin_year_code', $val, 'Financial Year']
            );
        }

        if ($val = array_get($filterData, 'targetFrom', null)) {
            array_push($whereTexts, 'Target Complete Date From ' . " $val" . "'");
        }

        if ($val = array_get($filterData, 'targetTo', null)) {
            array_push($whereTexts, "Target Complete Date To '" . " $val" . "'");
        }

        if ($val = array_get($filterData, 'actualFrom', null)) {
            array_push($whereTexts, 'Actual Complete Date From ' . " $val" . "'");
        }

        if ($val = array_get($filterData, 'actualTo', null)) {
            array_push($whereTexts, "Actual Complete Date To '" . " $val" . "'");
        }

        if ($val = array_get($filterData, 'targetRespondFrom', null)) {
            array_push($whereTexts, 'Target Respond Date From ' . " $val" . "'");
        }

        if ($val = array_get($filterData, 'targetRespondTo', null)) {
            array_push($whereTexts, "Target Respond Date To '" . " $val" . "'");
        }

        if ($val = array_get($filterData, 'createdFrom', null)) {
            array_push($whereTexts, 'Created From ' . " $val" . "'");
        }

        if ($val = array_get($filterData, 'createdTo', null)) {
            array_push($whereTexts, "Created To '" . " $val" . "'");
        }

        if ($val = Common::iset($filterData['category'])) {
            array_push($whereCodes, [
                'category',
                'category_id',
                'category_desc',
                $val,
                'Category'
            ]);
        }

        $statusType = [];
        if (Common::iset($filterData['generated'])) {
            $statusType[] = InstructionStatusType::GENERATED;
        }
        if (Common::iset($filterData['printed'])) {
            $statusType[] = InstructionStatusType::PRINTED;
        }
        if (Common::iset($filterData['complete'])) {
            $statusType[] = InstructionStatusType::COMPLETE;
        }
        if (Common::iset($filterData['closed'])) {
            $statusType[] = InstructionStatusType::CLOSED;
        }
        if (Common::iset($filterData['cancelled'])) {
            $statusType[] = InstructionStatusType::CANCELLED;
        }
        if (count($statusType)) {
            array_push($orCodes, [
                    'instruction_status_type',
                    'instruction_status_type_id',
                    'instruction_status_type_code',
                    implode(',', $statusType),
                    \Lang::get('text.status_type')
            ]);
        }

        if ($val = Common::iset($filterData['status'])) {
            array_push($whereCodes, [
                'instruction_status',
                'instruction_status_id',
                'instruction_status_code',
                $val,
                'Status'
            ]);
        }

        if ($val = Common::iset($filterData['invStatus'])) {
            array_push($whereCodes, [
                'invoice_status',
                'invoice_status_id',
                'invoice_status_code',
                $val,
                'Invoice Status'
            ]);
        }

        $authorisation = [];
        if (Common::iset($filterData['open'])) {
            $authorisation[] = FinAuthStatus::OPEN;
        }
        if (Common::iset($filterData['approval'])) {
            $authorisation[] = FinAuthStatus::APPROVAL;
        }
        if (Common::iset($filterData['approved'])) {
            $authorisation[] = FinAuthStatus::APPROVED;
        }
        if (Common::iset($filterData['rejected'])) {
            $authorisation[] = FinAuthStatus::REJECTED;
        }

        if (count($authorisation)) {
            array_push($orCodes, [
                    'fin_auth_status',
                    'fin_auth_status_id',
                    'name',
                    implode(',', $authorisation),
                    \Lang::get('text.authorisation')
            ]);
        }

        if ($val = array_get($filterData, 'estimatedAmountFrom')) {
            array_push($whereTexts, "Estimated Amount From '{$val}'");
        }

        if ($val = array_get($filterData, 'estimatedAmountTo')) {
            array_push($whereTexts, "Estimated Amount To '{$val}'");
        }

        if ($val = array_get($filterData, 'actualAmountFrom')) {
            array_push($whereTexts, "Actual Amount From '{$val}'");
        }

        if ($val = array_get($filterData, 'actualAmountTo')) {
            array_push($whereTexts, "Actual Amount To '{$val}'");
        }
        if ($val = array_get($filterData, 'establishment')) {
            array_push(
                $whereCodes,
                array('establishment', 'establishment_id', 'establishment_code', $val, 'Establishment')
            );
        }

        if ($helpcallUserDefinedFields) {
            $this->reAddUserDefinesQuery(GenTable::HELPCALL, $filterData, $whereCodes, $whereTexts);
        } else {
            $this->reAddUserDefinesQuery(GenTable::INSTRUCTION, $filterData, $whereCodes, $whereTexts);
        }

        if ($postedDateFrom = array_get($filterData, 'postedDateFrom', null)) {
            array_push($whereTexts, 'Invoice Posted Date From ' . " $postedDateFrom" . "'");
        }

        if ($postedDateTo = array_get($filterData, 'postedDateTo', null)) {
            array_push($whereTexts, "Invoice Posted Date To '" . " $postedDateTo" . "'");
        }

        if ($postedDateFrom || $postedDateTo) {
            $filterDate = '((';

            if ($postedDateFrom) {
                $temp = \DateTime::createFromFormat('d/m/Y', $postedDateFrom);
                $postedDateFrom = $temp->format('Y-m-d');
                $filterDate .= 'last_changed_to_posted_date >= \'' . $postedDateFrom . '\'';
            }

            if ($postedDateTo) {
                $temp = \DateTime::createFromFormat('d/m/Y', $postedDateTo);
                $postedDateTo = $temp->format('Y-m-d');
                if ($postedDateFrom) {
                    $filterDate .= ' AND ';
                }
                $filterDate .= 'last_changed_to_posted_date <= \'' . $postedDateTo . '\'';
            }

            $filterDate .= ')';
            $filterDate .= ' OR last_changed_to_posted_date IS null)';

            $listQuery->whereRaw($filterDate);
        }

        return $listQuery;
    }
}
