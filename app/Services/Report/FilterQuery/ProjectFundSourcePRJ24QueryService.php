<?php

/**
 * PRJ24
 *
 */

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\Common;
use Tfcloud\Models\FinYear;
use Tfcloud\Models\FinYearPeriod;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\PermissionService;

class ProjectFundSourcePRJ24QueryService extends BaseService
{
    protected $permissionService;

    protected $siteGroupId;

    // filters
    protected $projectCode = false;
    protected $finYearId = array();
    protected $fundSourceId = array();
    protected $filterData = [];

    // detailed report
    protected $detailed = true;

    public function __construct(PermissionService $permissionService, $filterData, $repCode)
    {
        $this->permissionService = $permissionService;

        // set properties
        $this->filterData = $filterData;
        $this->siteGroupId = \Auth::user()->site_group_id;
    }

    public function projectFundSourceQuery(
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        // Prepare the filters.
        $this->prepareFilters();

        // Get the fin year period titles.
        $finYearPeriodHeader = $this->buildFinYearPeriodHeader();

        // Build report header
        $queryRpt = $this->reportHeader($finYearPeriodHeader);

        // Get the funding source data.
        $queryRpt->unionAll($this->fundSourceQuery());

        $this->setFilterTexts($whereCodes, $orCodes, $whereTexts);

        return \DB::table(null)->select('*')->from(\DB::raw("(" . $queryRpt->toSql() . ') AS report'));
    }

    private function prepareFilters()
    {
        if (count($this->filterData)) {
            foreach ($this->filterData as $filter) {
                switch ($filter['f']) {
                    case 'project_code':
                        $this->projectCode = $filter['v'];
                        break;
                    case 'fin_year_id':
                        array_push($this->finYearId, $filter['v']);
                        break;
                    case 'funding_source_id':
                        array_push($this->fundSourceId, $filter['v']);
                        break;
                }
            }
        }
    }

    private function buildFinYearPeriodHeader()
    {
        if (FinYear::has13Periods()) {
            $finYearPeriodMonth = array_map(fn ($value) => "P" . $value, range(1, 13));
        } else {
            $finYearPeriodMonth = [];
            $finYear = FinYear::userSiteGroup()->first();
            if ($finYear) {
                $finYearPeriods = FinYearPeriod::orderBy('period_start_date')
                ->where('fin_year_id', $finYear->fin_year_id)->get();
                foreach ($finYearPeriods as $finYearPeriod) {
                    $startDate = $finYearPeriod->period_start_date;
                    $month = date('M', strtotime($startDate));
                    array_push($finYearPeriodMonth, $month);
                }
                // add a blank header for period 13
                array_push($finYearPeriodMonth, ' ');
            }
        }

        return $finYearPeriodMonth;
    }

    private function setFilterTexts(
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        if ($val = Common::iset($this->filterData['project_id'])) {
            array_push($whereTexts, 'Project');
        }
        if ($val = Common::iset($this->filterData['fin_year_id'])) {
            array_push($whereTexts, 'Finanacial Year');
        }
        if ($val = Common::iset($this->filterData['funding_source_id'])) {
            array_push($whereTexts, 'Funding Source');
        }
        if ($val = Common::iset($this->filterData['project_Code'])) {
            array_push($whereTexts, 'Project Code');
        }
    }


    protected function addBlankRow(array $finYearPeriodHeader)
    {
        // Fixed  columns.
        $query = " '' `Project`, '' `Project Fund Source`, '' `Financial Year`,"
            . " '' `Allocation`, '' `Allocated`, '' `% Allocated`";

        // Add blank columns for each period
        foreach ($finYearPeriodHeader as $value) {
            $query .= " , ' ' ";
        }
        return \DB::table(null)->select(\DB::raw($query));
    }


    private function reportHeader($finYearPeriodHeader)
    {
        $query = " '' `Project`, '' `Project Fund Source`, '' `Financial Year`,"
            . " '' `Allocation`, '' `Allocated`, '' `% Allocated`";

        foreach ($finYearPeriodHeader as $fyp) {
            $query .= " , '' `$fyp`";
        }

        return \DB::table(null)->select(\DB::raw($query));
    }


    private function fundSourceQuery()
    {
        $query  = "";
        $query .= "        CONCAT_WS(' - ', project_code, project_desc)               AS project       , ";
        $query .= "        CONCAT_WS(' - ', funding_source_code, funding_source_desc) AS funding_source, ";
        $query .= "        CONCAT_WS(' - ', fin_year_code, fin_year_desc)             AS fin_year      , ";
        $query .= "        planned_allocation                                         AS Allocation    , ";
        $query .= "        total_allocated                                            AS Allocated     , ";
        $query .= "        percent_of_planned_allocated                                                , ";
        $query .= "        p1_value                                                                    , ";
        $query .= "        p2_value                                                                    , ";
        $query .= "        p3_value                                                                    , ";
        $query .= "        p4_value                                                                    , ";
        $query .= "        p5_value                                                                    , ";
        $query .= "        p6_value                                                                    , ";
        $query .= "        p7_value                                                                    , ";
        $query .= "        p8_value                                                                    , ";
        $query .= "        p9_value                                                                    , ";
        $query .= "        p10_value                                                                   , ";
        $query .= "        p11_value                                                                   , ";
        $query .= "        p12_value                                                                   , ";
        $query .= "        p13_value ";
        $query .= " FROM   project_funding_source ";
        $query .= "        INNER JOIN project ";
        $query .= "        ON     project.project_id = project_funding_source.project_id ";
        $query .= "        INNER JOIN fin_year ";
        $query .= "        ON     fin_year.fin_year_id = project_funding_source.fin_year_id ";
        $query .= "        INNER JOIN funding_source ";
        $query .= "        ON     funding_source.funding_source_id = project_funding_source.funding_source_id";
        $query .= " WHERE project.site_group_id = '{$this->siteGroupId}' ";


        if ($this->projectCode) {
            $query .= "     AND project.project_code LIKE '%{$this->projectCode}%'";
        }

        if (count($this->fundSourceId)) {
            $query .= "     AND project_funding_source.funding_source_id IN (" . implode($this->fundSourceId) . ")";
        }

        if (count($this->finYearId)) {
            $query .= "     AND project_funding_source.fin_year_id IN (" . implode($this->finYearId) . ")";
        }

        return \DB::table(null)->select(\DB::raw($query));
    }
}
