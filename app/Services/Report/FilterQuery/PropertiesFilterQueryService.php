<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Models\GenTable;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Property\AgreementService;
use Tfcloud\Services\Property\BlockService;
use Tfcloud\Services\Property\BuildingService;
use Tfcloud\Services\Property\PropExternalService;
use Tfcloud\Services\Property\PropReviewService;
use Tfcloud\Services\Property\RoomService;
use Tfcloud\Services\Property\RunningCostsService;
use Tfcloud\Services\Property\SiteService;

class PropertiesFilterQueryService extends BaseService
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    public function reAddSiteQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $siteService = new SiteService($this->permissionService);
        $query = $siteService->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['site_type'])) {
            array_push($whereCodes, [
                'site_type',
                'site_type_id',
                'site_type_code',
                $val, \Lang::get('text.report_texts.property.site_type_code')
            ]);
        }

        if ($val = Common::iset($filterData['owner'])) {
            array_push($whereCodes, [
                'user',
                'id',
                'display_name',
                $val,
                \Lang::get('text.report_texts.property.site_owner')
            ]);
        }

        if ($val = Common::iset($filterData['tenure'])) {
            array_push($whereCodes, [
                'prop_tenure',
                'prop_tenure_id',
                'prop_tenure_code',
                $val,
                \Lang::get('text.report_texts.tenure')
            ]);
        }

        if ($val = Common::iset($filterData['site_alias'])) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.property.site_alias_contains'), $val));
        }

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.code_contains'), $val));
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.description_contains'), $val));
        }

        if ($val = Common::iset($filterData['uprn'])) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.property.site_uprn_contains'), $val));
        }

        if ($val = Common::iset($filterData['usage'])) {
            array_push($whereCodes, [
                'site_usage',
                'site_usage_id',
                'site_usage_code',
                $val,
                \Lang::get('text.report_texts.usage_code')
            ]);
        }

        if ($val = Common::iset($filterData['ward'])) {
            array_push($whereCodes, [
                'ward',
                'ward_id',
                'ward_code',
                $val,
                \Lang::get('text.report_texts.ward_code')
            ]);
        }

        if ($val = Common::iset($filterData['parish'])) {
            array_push($whereCodes, [
                'parish',
                'parish_id',
                'parish_code',
                $val,
                \Lang::get('text.report_texts.parish_code')
            ]);
        }

        if ($val = Common::iset($filterData['committee'])) {
            array_push(
                $whereCodes,
                [
                    'committee',
                    'committee_id',
                    'committee_code',
                    $val,
                    \Config::get('cloud.labels.site_committee') . " Code"
                ]
            );
        }

        if ($val = Common::iset($filterData['status'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.status_active'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.status_inactive'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.status_all'));
                    break;
            }
        }
        $vacant = Common::iset($filterData['vacant']);
        if ($vacant) {
            switch ($vacant) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.vacant_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.vacant_no'));
                    break;
            }
        } else {
            array_push($whereTexts, \Lang::get('text.report_texts.vacant_all'));
        }

        if ($val = Common::iset($filterData['rural_site'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.rural_site_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.rural_site_no'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.rural_site_all'));
                    break;
            }
        }

        if ($val = Common::iset($filterData['terrier_status'])) {
            array_push(
                $whereCodes,
                [
                    'terrier_status',
                    'terrier_status_id',
                    'terrier_status_code',
                    $val,
                    "Terrier Status"
                ]
            );
        }

        if ($val = Common::iset($filterData['listed_type'])) {
            array_push(
                $whereCodes,
                [
                    'listed_type',
                    'listed_type_id',
                    'listed_type_code',
                    $val,
                    "Listed"
                ]
            );
        }

        if ($val = Common::iset($filterData['site_designation'])) {
            array_push(
                $whereCodes,
                [
                    'site_designation',
                    'site_designation_id',
                    'site_designation_code',
                    $val,
                    "Site Designation"
                ]
            );
        }

        if ($val = Common::iset($filterData['site_classification'])) {
            array_push(
                $whereCodes,
                [
                    'site_classification',
                    'site_classification_id',
                    'site_classification_code',
                    $val,
                    "Site Classification"
                ]
            );
        }

        if ($val = Common::iset($filterData['establishment'])) {
            array_push(
                $whereCodes,
                [
                    'establishment',
                    'establishment_id',
                    'establishment_code',
                    $val,
                    "Establishment"
                ]
            );
        }

        if ($val = Common::iset($filterData['propFacilityAssetCategory'])) {
            array_push(
                $whereCodes,
                [
                    'prop_facility_asset_category',
                    'prop_facility_asset_category_id',
                    'prop_facility_asset_category_code',
                    $val,
                    "Asset Category"
                ]
            );
        }

        if ($val = Common::iset($filterData['seed_number'])) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.property.seed_number'), $val));
        }

        if ($val = Common::iset($filterData['dfe_county_code'])) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.property.dfe_county_code'), $val));
        }

        if ($val = Common::iset($filterData['dfe_number'])) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.property.dfe_number'), $val));
        }

        if ($val = Common::iset($filterData['address'])) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.address_contains'), $val));
        }

        if ($val = Common::iset($filterData['location_message'])) {
            array_push($whereTexts, "Message contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'startDateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.start_date_from'), $val));
        }

        if ($val = array_get($filterData, 'startDateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.start_date_to'), $val));
        }

        if ($val = array_get($filterData, 'startFromTime', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.start_time_from'), $val));
        }

        if ($val = array_get($filterData, 'startToTime', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.start_time_to'), $val));
        }

        if ($val = array_get($filterData, 'endDateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.end_date_from'), $val));
        }

        if ($val = array_get($filterData, 'endDateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.end_date_to'), $val));
        }

        if ($val = array_get($filterData, 'endFromTime', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.end_time_from'), $val));
        }

        if ($val = array_get($filterData, 'endToTime', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.end_time_to'), $val));
        }

        $this->getAddressSub($filterData, $whereTexts);

        $this->reAddUserDefinesQuery(GenTable::SITE, $filterData, $whereCodes, $whereTexts);

        return $query;
    }

    public function reAddBuildQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {

        $buildingService = new BuildingService($this->permissionService);
        $query = $buildingService->filter($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Description contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['building_alias'])) {
            array_push($whereTexts, "Aliases contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['uprn'])) {
            array_push($whereTexts, "UPRN contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['address'])) {
            array_push($whereTexts, "Address contains '" . $val . "'");
        }
        $this->getAddressSub($filterData, $whereTexts);

        if ($val = Common::iset($filterData['status'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.status_active'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.status_inactive'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.status_all'));
                    break;
            }
        }

        if ($val = Common::iset($filterData['buildingType'])) {
            array_push($whereCodes, ['building_type', 'building_type_id', 'building_type_code', $val, "Building Type"]);
        }

        if ($val = Common::iset($filterData['usage'])) {
            array_push($whereCodes, ['building_usage', 'building_usage_id', 'building_usage_code', $val, "Usage"]);
        }

        if ($val = Common::iset($filterData['occupier'])) {
            array_push($whereCodes, ['occupier', 'occupier_id', 'occupier_code', $val, "Occupier"]);
        }

        if ($val = Common::iset($filterData['committee'])) {
            array_push($whereCodes, ['committee', 'committee_id', 'committee_code', $val, "Building Portfolio"]);
        }

        if ($val = Common::iset($filterData['ward'])) {
            array_push($whereCodes, ['ward', 'ward_id', 'ward_code', $val, "Ward"]);
        }

        if ($val = Common::iset($filterData['parish'])) {
            array_push($whereCodes, ['parish', 'parish_id', 'parish_code', $val, "Parish"]);
        }

        if ($val = Common::iset($filterData['propFacilityAssetCategory'])) {
            array_push(
                $whereCodes,
                [
                    'prop_facility_asset_category',
                    'prop_facility_asset_category_id',
                    'prop_facility_asset_category_code',
                    $val,
                    "Asset Category"
                ]
            );
        }

        if ($val = Common::iset($filterData['location_message'])) {
            array_push($whereTexts, "Message contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'startDateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.start_date_from'), $val));
        }

        if ($val = array_get($filterData, 'startDateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.start_date_to'), $val));
        }

        if ($val = array_get($filterData, 'startFromTime', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.start_time_from'), $val));
        }

        if ($val = array_get($filterData, 'startToTime', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.start_time_to'), $val));
        }

        if ($val = array_get($filterData, 'endDateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.end_date_from'), $val));
        }

        if ($val = array_get($filterData, 'endDateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.end_date_to'), $val));
        }

        if ($val = array_get($filterData, 'endFromTime', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.end_time_from'), $val));
        }

        if ($val = array_get($filterData, 'endToTime', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.end_time_to'), $val));
        }

        $this->reAddUserDefinesQuery(GenTable::BUILDING, $filterData, $whereCodes, $whereTexts);

        return $query;
    }

    public function reAddRoomQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $roomService = new RoomService();
        $query = $roomService->filterAll($query, $filterData, $viewName);
        if ($searchStr = Common::iset($filterData['search'])) {
            array_push($whereTexts, "Room Number or Description '" . $searchStr . "'");
        }

        if ($type = Common::iset($filterData['roomType'])) {
            array_push($whereCodes, ['room_type', 'room_type_id', 'room_type_code', $type, "Type Code"]);
        }

        if ($subType = Common::iset($filterData['roomSubtype'])) {
            array_push(
                $whereCodes,
                ['room_subtype', 'room_subtype_id', 'room_subtype_code', $subType, "Sub Type Code"]
            );
        }

        if ($roomFloor = Common::iset($filterData['roomFloor'])) {
            array_push(
                $whereCodes,
                ['room_floor', 'room_floor_id', 'room_floor_code', $roomFloor, "Floor Code"]
            );
        }

        if ($number = Common::iset($filterData['number'])) {
            array_push($whereTexts, "Room Number contains '" . $number . "'");
        }

        if ($desc = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Room Description contains '" . $desc . "'");
        }

        if ($val = Common::iset($filterData['status'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.status_active'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.status_inactive'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.status_all'));
                    break;
            }
        }
        if ($dfeCountyCode = Common::iset($filterData['dfe_county_code'])) {
            array_push($whereTexts, "LA Code contains '" . $dfeCountyCode . "'");
        }

        if ($dfeNumber = Common::iset($filterData['dfe_number'])) {
            array_push($whereTexts, "DFE Number contains '" . $dfeNumber . "'");
        }

        if ($uprn = Common::iset($filterData['uprn'])) {
            array_push($whereTexts, "UPRN contains '" . $uprn . "'");
        }

        if ($val = Common::iset($filterData['location_message'])) {
            array_push($whereTexts, "Message contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'startDateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.start_date_from'), $val));
        }

        if ($val = array_get($filterData, 'startDateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.start_date_to'), $val));
        }

        if ($val = array_get($filterData, 'startFromTime', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.start_time_from'), $val));
        }

        if ($val = array_get($filterData, 'startToTime', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.start_time_to'), $val));
        }

        if ($val = array_get($filterData, 'endDateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.end_date_from'), $val));
        }

        if ($val = array_get($filterData, 'endDateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.end_date_to'), $val));
        }

        if ($val = array_get($filterData, 'endFromTime', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.end_time_from'), $val));
        }

        if ($val = array_get($filterData, 'endToTime', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.end_time_to'), $val));
        }

        return $query;
    }

    public function reAddBlockQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $blockService = new BlockService($this->permissionService);
        $query = $blockService->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Description contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['building_alias'])) {
            array_push($whereTexts, "Aliases contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['uprn'])) {
            array_push($whereTexts, "UPRN contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['address'])) {
            array_push($whereTexts, "Address contains '" . $val . "'");
        }
        $this->getAddressSub($filterData, $whereTexts);

        if ($val = Common::iset($filterData['status'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, "Building Status = 'Active'");
                    break;
                case 'all':
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                default:
                    array_push($whereTexts, "Building Status = 'Inactive'");
                    break;
            }
        }

        if ($val = Common::iset($filterData['blockStatus'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, "Block Status = 'Active'");
                    break;
                case 'all':
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                default:
                    array_push($whereTexts, "Block Status = 'Inactive'");
                    break;
            }
        }

        if ($val = Common::iset($filterData['blockType'])) {
            array_push(
                $whereCodes,
                [
                    'building_block_type',
                    'building_block_type_id',
                    'building_block_type_code',
                    $val,
                    "Building Block Type"
                ]
            );
        }

        if ($val = Common::iset($filterData['usage'])) {
            array_push($whereCodes, ['building_usage', 'building_usage_id', 'building_usage_code', $val, "Usage"]);
        }

        if ($val = Common::iset($filterData['occupier'])) {
            array_push($whereCodes, ['occupier', 'occupier_id', 'occupier_code', $val, "Occupier"]);
        }

        if ($val = Common::iset($filterData['committee'])) {
            array_push($whereCodes, ['committee', 'committee_id', 'committee_code', $val, "Building Portfolio"]);
        }

        if ($val = Common::iset($filterData['ward'])) {
            array_push($whereCodes, ['ward', 'ward_id', 'ward_code', $val, "Ward"]);
        }

        if ($val = Common::iset($filterData['parish'])) {
            array_push($whereCodes, ['parish', 'parish_id', 'parish_code', $val, "Parish"]);
        }

        $this->reAddUserDefinesQuery(GenTable::BUILDING_BLOCK, $filterData, $whereCodes, $whereTexts);

        return $query;
    }

    public function reAddPropExternalQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $query = PropExternalService::filterAll($query, $filterData, $viewName);

        if (strlen($val = array_get($filterData, 'code'))) {
            array_push($whereTexts, "Code contains '" . $val . "'");
        }

        if (strlen($val = array_get($filterData, 'description'))) {
            array_push($whereTexts, "Description contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'prop_external_type_id')) {
            array_push(
                $whereCodes,
                ['prop_external_type', 'prop_external_type_id', 'prop_external_type_code', $val, 'Type']
            );
        }

        if ($val = array_get($filterData, 'prop_external_subtype_id')) {
            array_push(
                $whereCodes,
                ['prop_external_subtype', 'prop_external_subtype_id', 'prop_external_subtype_code', $val, 'Subtype']
            );
        }

        if ($val = Common::iset($filterData['active'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.active_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.active_no'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.active_all'));
                    break;
            }
        }

        if ($val = array_get($filterData, 'public_access', 'all')) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, "Public Access = Yes");
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, "Public Access = No");
                    break;
                default:
                    array_push($whereTexts, "Public Access = All");
                    break;
            }
        }

        return $query;
    }

    public function reAddPropReviewQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $reviewService = new PropReviewService($this->permissionService);
        $reviewQuery = $reviewService->getReviewsFilter($query, $filterData, $viewName);
        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Review Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['reviewType'])) {
            array_push(
                $whereCodes,
                ['gen_review_type', 'gen_review_type_id', 'gen_review_type_code', $val, 'Type']
            );
        }

        if ($val = Common::iset($filterData['owner'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Owner']);
        }

        $dateFields = [
            ["reviewDateFrom", "Review Date", "Greater than or equal to"],
            ["reviewDateTo", "Review Date", "Less than or equal to"],
        ];

        foreach ($dateFields as $dateField) {
            $dateArg = $dateField[0];

            if ($val = Common::iset($filterData[$dateArg])) {
                $dateFieldDesc = $dateField[1];
                $dateFieldCompareText = $dateField[2];
                array_push($whereTexts, "$dateFieldDesc $dateFieldCompareText '" . $val . "'");
            }
        }

        if ($val = Common::iset($filterData['reviewStatus'])) {
            array_push(
                $orCodes,
                [
                    'gen_review_status',
                    'gen_review_status_id',
                    'gen_review_status_code',
                    implode(',', $val),
                    "Review Status"
                ]
            );
        }
        return $reviewQuery;
    }

    public function reAddPropAgreementQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $service = new AgreementService($this->permissionService);
        $query = $service->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Description contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'establishment')) {
            array_push(
                $whereCodes,
                ['establishment', 'establishment_id', 'establishment_code', $val, "Establishment Code"]
            );
        }

        if ($val = Common::iset($filterData['type'])) {
            array_push(
                $whereCodes,
                ['prop_agr_type', 'prop_agr_type_id', 'prop_agr_type_code', $val, 'Type']
            );
        }

        if ($val = Common::iset($filterData['onus'])) {
            array_push(
                $whereCodes,
                ['prop_agr_onus', 'prop_agr_onus_id', 'prop_agr_onus_code', $val, 'Onus']
            );
        }

        $dateFields = [
            ["startDateFrom", "Start Date", "Greater than or equal to"],
            ["startDateTo", "Start Date", "Less than or equal to"],
            ["endDateFrom", "End Date", "Greater than or equal to"],
            ["endDateTo", "End Date", "Less than or equal to"],
        ];

        foreach ($dateFields as $dateField) {
            $dateArg = $dateField[0];

            if ($val = Common::iset($filterData[$dateArg])) {
                $dateFieldDesc = $dateField[1];
                $dateFieldCompareText = $dateField[2];
                array_push($whereTexts, "$dateFieldDesc $dateFieldCompareText '" . $val . "'");
            }
        }

        $this->reAddUserDefinesQuery(GenTable::PROP_AGREEMENT, $filterData, $whereCodes, $whereTexts);

        return $query;
    }

    public function reAddPropRunningCostQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $service = new RunningCostsService($this->permissionService);
        $query = $service->reportFilterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['active'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.active_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.active_no'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.active_all'));
                    break;
            }
        }

        return $query;
    }
}
