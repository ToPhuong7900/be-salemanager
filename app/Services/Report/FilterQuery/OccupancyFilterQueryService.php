<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\Common;
use Tfcloud\Models\GenTable;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\Estate\LeaseOutLetuService;
use Tfcloud\Services\Estate\OccupancyUnitService;
use Tfcloud\Services\PermissionService;

class OccupancyFilterQueryService extends BaseService
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }


    public function reAddEstatesOccupancyUnitQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $leaseOutLetuService = new LeaseOutLetuService($this->permissionService);
        $occupancyUnitService = new OccupancyUnitService($this->permissionService, $leaseOutLetuService);
        $occupancyUnitQuery = $occupancyUnitService->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Lettable Unit Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Lettable Unit Description contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['contact'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Landlord']);
        }

        if ($val = Common::iset($filterData['tenant'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Tenant']);
        }

        if ($val = Common::iset($filterData['owner'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, \Lang::get('validation.attributes.owner')]);
        }

        if ($val = Common::iset($filterData['available'])) {
            array_push($whereTexts, "Available = $val");
        }

        if ($val = Common::iset($filterData['vacant'])) {
            array_push($whereTexts, "Vacant = $val");
        }

        if ($val = Common::iset($filterData['status'])) {
            array_push(
                $whereCodes,
                ['lettable_unit_status', 'lettable_unit_status_id', 'lettable_unit_status_code', $val, 'Status']
            );
        }

        // Special location
        if ($val = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);

            if ($val = Common::iset($filterData['building_id'])) {
                array_push($whereCodes, ['building', 'building_id', 'building_code', $val, "Building Code"]);

                if ($val = Common::iset($filterData['room_id'])) {
                    array_push($whereCodes, ['room', 'room_id', 'room_number', $val, "Room Number"]);
                }
            }
        }

        if ($val = Common::iset($filterData['address'])) {
            array_push($whereTexts, "Address contains '" . $val . "'");
        }
        $this->getAddressSub($filterData, $whereTexts);

        $dateFields = [
            ["parkingSpacesFrom", "Parking Spaces", "greater than or equal to"],
            ["parkingSpacesTo", "Parking Spaces", "less than or equal to"],
            ["giaFrom", "GIA", "greater than or equal to"],
            ["giaTo", "GIA", "less than or equal to"],
            ["niaFrom", "NIA", "greater than or equal to"],
            ["niaTo", "NIA", "less than or equal to"],
            ["annualRentFrom", "Annual Rent ", "greater than or equal to"],
            ["annualRentTo", "Annual Rent ", "less than or equal to"],
            ["marketRentFrom", "Market Rent ", "greater than or equal to"],
            ["marketRentTo", "Market Rent ", "less than or equal to"],
        ];

        foreach ($dateFields as $dateField) {
            $dateArg = $dateField[0];

            if ($val = Common::iset($filterData[$dateArg])) {
                $dateFieldDesc = $dateField[1];
                $dateFieldCompareText = $dateField[2];

                array_push($whereTexts, "$dateFieldDesc $dateFieldCompareText '" . $val . "'");
            }
        }

        $this->reAddUserDefinesQuery(GenTable::OCCUPANCY_UNIT, $filterData, $whereCodes, $whereTexts);

        return $occupancyUnitQuery;
    }


    public function reAddEstatesOccupancyUnitPropertyQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $leaseOutLetuService       = new LeaseOutLetuService($this->permissionService);
        $occupancyUnitService       = new LettableUnitService($this->permissionService, $leaseOutLetuService);
        $occupancyUnitPropertyQuery = $occupancyUnitService->filterAllProperties($query, $filterData, 0, $viewName);

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Lettable Unit Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Lettable Unit Description contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['available'])) {
            array_push($whereTexts, "Available = $val");
        }

        if ($val = Common::iset($filterData['vacant'])) {
            array_push($whereTexts, "Vacant = $val");
        }

        if ($val = Common::iset($filterData['status'])) {
            array_push(
                $whereCodes,
                ['lettable_unit_status', 'lettable_unit_status_id', 'lettable_unit_status_code', $val, 'Status']
            );
        }

        if ($val = Common::iset($filterData['contact'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Landlord']);
        }

        if ($val = Common::iset($filterData['owner'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, \Lang::get('validation.attributes.owner')]);
        }

        // Special location
        if ($val = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);

            if ($val = Common::iset($filterData['building_id'])) {
                array_push($whereCodes, ['building', 'building_id', 'building_code', $val, "Building Code"]);

                if ($val = Common::iset($filterData['room_id'])) {
                    array_push($whereCodes, ['room', 'room_id', 'room_number', $val, "Room Number"]);
                }
            }
        }

        if ($val = Common::iset($filterData['prop_zone_id'])) {
            array_push($whereCodes, ['prop_zone', 'prop_zone_id', 'prop_zone_code', $val, 'Zone']);

            if ($val = Common::iset($filterData['prop_external_id'])) {
                array_push(
                    $whereCodes,
                    ['prop_external', 'prop_external_id', 'prop_external_code', $val, 'External Area']
                );
            }
        }

        return $occupancyUnitPropertyQuery;
    }
}
