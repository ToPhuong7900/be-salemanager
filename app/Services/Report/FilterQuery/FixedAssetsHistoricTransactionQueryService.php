<?php

/**
 * FAR39
 *
 *
 */

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\PermissionService;

class FixedAssetsHistoricTransactionQueryService extends BaseService
{
    protected $permissionService;

    protected $siteGroupId;
    protected $bIncludeRRTrans = false;
    protected $rrTrans = 'N';
    protected $bSumNegative = false;

    // filters
    protected $commonGood = false;
    protected $investment = false;
    protected $deminimis = false;
    protected $filterData = [];

    // detailed report
    protected $detailed = true;

    public function __construct(PermissionService $permissionService, $filterData, $repCode)
    {
        $this->permissionService = $permissionService;

        // get filter values
        $commonGood = $filterData['common_good'] ?? false;
        $investment = $filterData['investment'] ?? false;
        $deminimis = $filterData['deminimis_asset'] ?? CommonConstant::DATABASE_VALUE_NO;

        // set properties
        $this->filterData = $filterData;
        $this->commonGood = ($commonGood == 'All') ? false : $commonGood;
        $this->investment = ($investment == 'All') ? false : $investment;
        $this->deminimis = ($deminimis == 'All') ? false : $deminimis;
        $this->detailed = ($repCode == 'FAR31') ? true : false;
    }

    public function getNumericCols()
    {
        $numCols = [1, 2, 3, 4, 5 ,6, 7, 8, 9, 10];

        return $numCols;
    }

    public function historicTransactionQuery(
        $inputs,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $this->siteGroupId = \Auth::user()->site_group_id;

        // *****************************************************************
        // REPORT HEADER
        // *****************************************************************
        // Build report header
        $queryRpt = $this->reportHeader();

        $caFixedAssetId = array_get($inputs, 'ca_fixed_asset_id', false);

        // Current Values
        $queryRpt->unionAll($this->buildTransactionQuery($caFixedAssetId));

        $this->setFilterTexts($whereCodes, $orCodes, $whereTexts);

        return \DB::table(null)->select('*')->from(\DB::raw("(" . $queryRpt->toSql() . ') AS report'));
    }

    private function setFilterTexts(
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        if ($val = Common::iset($this->filterData['deminimis_asset'])) {
            switch ($val) {
                case CommonConstant::ALL:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.deminimis_asset_all'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.deminimis_asset_no'));
                    break;
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.deminimis_asset_yes'));
                    break;
            }
        }

        if ($val = Common::iset($this->filterData['common_good'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.common_good_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.common_good_no'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.common_good_all'));
                    break;
            }
        }


        if ($val = Common::iset($this->filterData['investment'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.investment_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.investment_no'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.investment_all'));
                    break;
            }
        }
    }

    private function reportHeader()
    {
        $query =  " '' AS 'Transactions'";
        $query .= ", '' AS 'Transaction Code' ";
        $query .= ", '' AS 'Transaction Type' ";
        $query .= ", '' AS 'Year' ";
        $query .= ", '' AS 'Effective Date' ";
        $query .= ", '' AS 'Actual Date' ";
        $query .= ", '' AS 'Effective Value Asset' ";
        $query .= ", '' AS 'Effective Value Land' ";
        $query .= ", '' AS 'Actual Value Asset' ";
        $query .= ", '' AS 'Actual Value Land' ";
        $query .= ", '' AS 'Depreciation' ";
        $query .= ", '' AS 'Historical Depreciation' ";
        $query .= ", '' AS 'IFRS Category' ";
        $query .= ", '' AS 'User' ";

        return \DB::table(null)->select(\DB::raw($query));
    }


    private function buildTransactionQuery($caFixedAssetId)
    {
        $query = " '' AS Col1,   `transaction_code`                                                                 , ";
        $query .= "         `transaction_type`                                                                 , ";
        $query .= "         `fin_year_code`                                                                    , ";
        $query .= "         `effective_date`                                                                   , ";
        $query .= "         `actual_date`                                                                      , ";
        $query .= "         FORMAT((ca_historic_transaction.effective_value_asset), 2) AS effective_value_asset, ";
        $query .= "         FORMAT((ca_historic_transaction.effective_value_land), 2)  AS effective_value_land , ";
        $query .= "         FORMAT((ca_historic_transaction.actual_value_asset), 2)    AS actual_value_asset   , ";
        $query .= "         FORMAT((ca_historic_transaction.actual_value_land), 2)     AS actual_value_land    , ";
        $query .= "         FORMAT((ca_historic_transaction.depreciation), 2)             depreciation         , ";
        $query .= "         FORMAT((ca_historic_transaction.historic_depreciation), 2)    historic_depreciation, ";
        $query .= "         `ifrs_category`                                                                    , ";
        $query .= "         `user` ";
        $query .= "FROM     `ca_historic_transaction` ";
        $query .= "         INNER JOIN `ca_fixed_asset` ";
        $query .= "         ON  `ca_fixed_asset`.`ca_fixed_asset_id` = `ca_historic_transaction`.`ca_fixed_asset_id` ";
        $query .= "WHERE    `ca_historic_transaction`.`site_group_id`     = $this->siteGroupId ";
        $query .= "AND      `ca_historic_transaction`.`ca_fixed_asset_id` = $caFixedAssetId ";
        $query .= "ORDER BY `ca_historic_transaction`.`transaction_code` DESC";

        return \DB::table(null)->select(\DB::raw($query));
    }
}
