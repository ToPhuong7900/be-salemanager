<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\Filters\BaseFilter;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\PupilPlacePlanning\PlacementService;
use Tfcloud\Services\BaseService;

class PlacementSitesFilterQueryService extends BaseService
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    public function reAddPlacementSiteQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $placementSite = new PlacementService(
            $this->permissionService
        );
        $query = $placementSite->filterAll($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'code', null)) {
            array_push($whereTexts, \Lang::get('text.site_code') . " contains '$val'");
        }

        if ($val = array_get($filterData, 'desc', null)) {
            array_push($whereTexts, \Lang::get('text.site_desc') . " contains '$val'");
        }

        if ($val = array_get($filterData, 'uprn', null)) {
            array_push($whereTexts, \Lang::get('text.report_texts.uprn') . " contains '$val'");
        }

        if ($val = array_get($filterData, 'general_search', null)) {
            array_push($whereTexts, \Lang::get('text.general_search') . " contains '$val'");
        }

        if ($val = array_get($filterData, 'siteType', null)) {
            array_push($whereCodes, [
                'site_type',
                'site_type_id',
                'site_type_code',
                $val,
                \Form::fieldLang('site_type')
            ]);
        }

        if ($val = array_get($filterData, 'siteUsage', null)) {
            array_push($whereCodes, [
                'site_usage',
                'site_usage_id',
                'site_usage_code',
                $val,
                \Form::fieldLang('site_usage_id')
            ]);
        }

        if ($val = array_get($filterData, 'committee', null)) {
            array_push($whereCodes, [
                'committee',
                'committee_id',
                'committee_code',
                $val,
                \Form::fieldLang('committee')
            ]);
        }







        return $query;
    }
}
