<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Models\TreeSurveyStatus;
use Tfcloud\Models\StockStocktakeStatus;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\Tree\TreeSurveyActionService;
use Tfcloud\Services\Tree\TreeSurveyService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\UserDefinedService;

class TreeFilterQueryService extends BaseService
{
    protected $permissionService;
    protected $userDefinedService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
        $this->userDefinedService = new UserDefinedService();
    }

    public function reAddSurveyQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $surveyService = new TreeSurveyService($this->permissionService, $this->userDefinedService);
        $surveyQuery = $surveyService->filterAll($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'plant_id')) {
            $plant = \Tfcloud\Models\Plant::find($val);
            if ($plant) {
                array_push($whereTexts, sprintf("Surveys linked to plant item '%s'", $plant->plant_code));
            }
        }

        if ($val = array_get($filterData, 'tree_survey_id')) {
            $treeSurvey = \Tfcloud\Models\TreeSurvey::find($val);
            if ($treeSurvey) {
                array_push($whereTexts, sprintf("Surveys code equals '%s'", $treeSurvey->tree_survey_code));
            }
        }

        if ($val = array_get($filterData, 'tree_survey_code')) {
            array_push($whereTexts, sprintf("Survey Code contains '%s'", $val));
        }

        if ($val = array_get($filterData, 'tree_survey_desc')) {
            array_push($whereTexts, sprintf("Survey Description contains '%s'", $val));
        }

        if ($val = array_get($filterData, 'reference')) {
            array_push($whereTexts, sprintf("Reference contains '%s'", $val));
        }

        if ($val = array_get($filterData, 'tree_ref_num')) {
            array_push($whereTexts, sprintf("Tree Ref. No. contains '%s'", $val));
        }

        if ($val = array_get($filterData, 'tree_species_id')) {
            array_push(
                $whereCodes,
                ['tree_species', 'tree_species_id', 'common_name', $val, "Tree Species"]
            );
        }

        if ($val = array_get($filterData, 'tree_grading_id')) {
            array_push(
                $whereCodes,
                ['tree_grading', 'tree_grading_id', 'tree_grading_code', $val, "Tree Grading"]
            );
        }

        if ($val = array_get($filterData, 'tree_sub_grading_id')) {
            array_push(
                $whereCodes,
                ['tree_sub_grading', 'tree_sub_grading_id', 'tree_sub_grading_code', $val, "Tree Sub Grading"]
            );
        }

        if ($val = Common::iset($filterData['establishment'])) {
            array_push(
                $whereCodes,
                ['establishment', 'establishment_id', 'establishment_code', $val, 'Establishment Code']
            );
        }

        if ($val = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, 'Site Code']);
        }

        if ($val = array_get($filterData, 'building_id')) {
            array_push($whereCodes, ['building', 'building_id', 'building_code', $val, 'Building Code']);
        }

        if ($val = array_get($filterData, 'room_id')) {
            array_push(
                $whereCodes,
                ['room', 'room_id', 'room_number', $val, 'Room Number']
            );
        }

        if ($val = Common::iset($filterData['prop_zone_id'])) {
            array_push($whereCodes, ['prop_zone', 'prop_zone_id', 'prop_zone_code', $val, 'Zone']);

            if ($val = Common::iset($filterData['prop_external_id'])) {
                array_push(
                    $whereCodes,
                    ['prop_external', 'prop_external_id', 'prop_external_code', $val, 'External Area']
                );
            }
        }

        if ($val = array_get($filterData, 'plant_group_id')) {
            array_push($whereCodes, ['plant_group', 'plant_group_id', 'plant_group_code', $val, "Plant Group"]);

            if ($val = array_get($filterData, 'plant_subgroup_id')) {
                array_push(
                    $whereCodes,
                    ['plant_subgroup', 'plant_subgroup_id', 'plant_subgroup_code', $val, "Plant Sub Group"]
                );

                if ($val = array_get($filterData, 'plant_type_id')) {
                    array_push(
                        $whereCodes,
                        ['plant_type', 'plant_type_id', 'plant_type_code', $val, "Plant Type"]
                    );
                }
            }
        }

        if ($val = array_get($filterData, 'surveyDateFrom')) {
            array_push($whereTexts, "Survey From $val");
        }
        if ($val = array_get($filterData, 'surveyDateTo')) {
            array_push($whereTexts, "Survey To $val");
        }

        if ($val = array_get($filterData, 'surveyor_contact_id')) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Surveyor']);
        }

        if ($val = array_get($filterData, 'owner')) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Owner']);
        }

        if ($val = Common::iset($filterData['tree_survey_status_id'])) {
            array_push($whereCodes, [
                'tree_survey_status',
                'tree_survey_status_id',
                'tree-survey_status_desc',
                $val,
                'Status'
            ]);
        }

        $statusType = [];
        if (array_get($filterData, 'status_open', null)) {
            $statusType[] = TreeSurveyStatus::OPEN;
        }
        if (array_get($filterData, 'status_complete', null)) {
            $statusType[] = TreeSurveyStatus::COMPLETE;
        }
        if (array_get($filterData, 'status_archive', null)) {
            $statusType[] = TreeSurveyStatus::ARCHIVE;
        }
        if (count($statusType)) {
            array_push($orCodes, [
                'tree_survey_status',
                'tree_survey_status_id',
                'tree_survey_status_desc',
                implode(',', $statusType),
                'Survey Status'
            ]);
        }

        return $surveyQuery;
    }

    public function reAddTreeSurveyActionsQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $query = (new TreeSurveyActionService($this->permissionService))->filterAll($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'tree_survey_id')) {
            $treeSurvey = \Tfcloud\Models\TreeSurvey::find($val);
            if ($treeSurvey) {
                array_push($whereTexts, sprintf("Survey code equals '%s'", $treeSurvey->tree_survey_code));
            }
        }

        if ($val = array_get($filterData, 'targetFrom', null)) {
            array_push($whereTexts, "Target From '" . " $val" . "'");
        }

        if ($val = array_get($filterData, 'targetTo', null)) {
            array_push($whereTexts, "Target To '" . " $val" . "'");
        }

        if ($val = array_get($filterData, 'actionType', null)) {
            array_push($whereCodes, [
                'tree_survey_action_type',
                'tree_survey_action_type_id',
                'tree_survey_action_type_code',
                $val,
                'Action Type'
            ]);
        }

        if ($val = array_get($filterData, 'actionPriority', null)) {
            array_push($whereCodes, [
                'tree_survey_action_priority',
                'tree_survey_action_priority_id',
                'tree_survey_action_priority_code',
                $val,
                'Action Priority'
            ]);
        }

        if ($val = array_get($filterData, 'priceBand', null)) {
            array_push($whereCodes, [
                'tree_survey_action_price_band',
                'tree_survey_action_price_band_id',
                'tree_survey_action_price_band_code',
                $val,
                'Price Band'
            ]);
        }

        if ($val = Common::iset($filterData['owner'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Owner']);
        }

        // Special location
        if ($val = array_get($filterData, 'site_id')) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);

            if ($val = array_get($filterData, 'building_id')) {
                array_push($whereCodes, ['building', 'building_id', 'building_code', $val, "Building Code"]);

                if ($val = array_get($filterData, 'room_id')) {
                    array_push($whereCodes, ['room', 'room_id', 'room_number', $val, "Room Number"]);
                }
            }
        }

        if ($val = array_get($filterData, 'code')) {
            array_push($whereTexts, "Code contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'responsibleParty')) {
            array_push($whereTexts, "Responsible Party contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'comments')) {
            array_push($whereTexts, "Comments contains '" . $val . "'");
        }

        return $query;
    }
}
