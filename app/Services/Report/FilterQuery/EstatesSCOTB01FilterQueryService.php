<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Illuminate\Support\Facades\Auth;
use Tfcloud\Lib\Filters\Contract\ContractInspectionReportFilter;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\Contract\InstructionService;
use Tfcloud\Services\PermissionService;

class EstatesSCOTB01FilterQueryService extends BaseService
{
    protected $permissionService;
    protected $instructionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
        $this->instructionService = new InstructionService($this->permissionService);
    }

    public function reAddSCOTB01Query($filterData, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $tenantQuery = $this->filterAllSalesInvoices($filterData);

        return $tenantQuery;
    }




    private function filterAllSalesInvoices($filterData)
    {
        $rawData = $this->allSalesInvoices();

        $filter = new ContractInspectionReportFilter($filterData);

        $rawData->where('repdata.site_group_id', Auth::user()->site_group_id)
            ->orderByRaw('`Lease Ref` DESC');

        return $rawData;
    }


    private function allSalesInvoices()
    {
        $query  = "";
        //$query .= "       repdata.est_sales_invoice_code AS `Invoice No`         , ";
        $query .= "       repdata.sales_acount_ref       AS `Customer ID`        , ";
        $query .= "       repdata.contact_name           AS `Customer Name`      , ";
        $query .= "       repdata.lettable_unit_desc     AS `Unit Description`   , ";
        $query .= "       repdata.term                   AS Terms                , ";
        $query .= "       repdata.advance                AS `Advance/Arrears`    , ";
        //$query .= "       NULL                           AS `Account Code`       , ";
        $query .= "       bw_code                           AS `BW Cost Centre`     , ";
        //$query .= "       NULL                           AS `Sub Centre`         , ";
        $query .= "       repdata.credit_note            AS Credit               , ";
        $query .= "       repdata.lease_out_code         AS `Lease Ref`          , ";
        $query .= "       repdata.est_sales_invoice_desc AS `Invoice Description`, ";
        $query .= "       repdata.est_charge_desc        AS `Charge Description` , ";
        $query .= "       repdata.tax_code_code          AS `Tax Code`           , ";
        $query .= "       repdata.net_total              AS `Rent (Net)`         , ";
        $query .= "       insurance.net_total            AS Insurance            , ";
        $query .= "       service.net_total              AS `Service Charge`      ";
        //$query .= "       Reference                      AS `Order Number` ";
        $query .= "FROM   ( SELECT est_sales_invoice.site_group_id, est_sales_invoice.est_sales_invoice_id, ";
        $query .= "                MAX(invoice_due_date)                  , ";
        $query .= "                est_sales_invoice_code                 , ";
        $query .= "                contact.sales_acount_ref               , ";
        $query .= "                contact_name                           , ";
        $query .= "                lettable_unit.lettable_unit_desc       , ";
        $query .= "                rent.term            , ";
        $query .= "                rent.bw_code            , ";
        $query .= "                rent.advance                           , ";
        $query .= "                est_sales_invoice.credit_note          , ";
        $query .= "                lease_out.lease_out_code               , ";
        $query .= "                est_sales_invoice_desc                 , ";
        $query .= "                rent.est_charge_desc                   , ";
        $query .= "                tax_code.tax_code_code                 , ";
        //$query .= "                rent.net_total                         , ";
        $query .= "                IF(rent.rent = 'Y', rent.net_total, null) as net_total      , ";
        $query .= "                Reference                              , ";
        $query .= "                est_sales_invoice.lease_out_id ";
        $query .= "       FROM     est_sales_invoice ";
        $query .= "                INNER JOIN lease_out ";
        $query .= "                ON       lease_out.lease_out_id = est_sales_invoice.lease_out_id ";
        $query .= "                INNER JOIN lease_out_letu ";
        $query .= "                ON       lease_out_letu.lease_out_id = lease_out.lease_out_id ";
        $query .= "                INNER JOIN lettable_unit ";
        $query .= "                ON       lettable_unit.lettable_unit_id = lease_out_letu.lettable_unit_id "
            . "AND lettable_unit.est_unit_type_id = " . EstUnitType::EST_UT_LU;
        $query .= "                LEFT JOIN contact ";
        $query .= "                ON       contact.contact_id = lease_out.tenant_contact_id ";
        $query .= "                LEFT JOIN ";
        $query .= "                         ( SELECT rent                                      , ";
        $query .= "                                 est_sales_invoice_line.est_sales_invoice_id, ";
        $query .= "                                 est_sales_invoice_line.est_charge_id       , ";
        $query .= "                                 est_sales_invoice_line.tax_code_id         , ";
        $query .= "                                 est_charge.gen_userdef_group_id            , ";
        $query .= "                                 CASE est_charge.payment_in_advance ";
        $query .= "                                         WHEN 'Y' ";
        $query .= "                                         THEN 'In Advance' ";
        $query .= "                                         ELSE 'In Arrears' ";
        $query .= "                                 END AS advance , ";
        $query .= "                                 est_charge_desc, ";
        $query .= "                                 CONCAT(fin_account_code, '-', fin_account_desc) AS bw_code, ";
        $query .= "                           CONCAT(period_freq, ' ', est_time_period.est_time_period_desc) AS term, ";
        $query .= "                                 est_sales_invoice_line.net_total ";
        $query .= "                         FROM    est_sales_invoice_line ";
        $query .= "                                 INNER JOIN est_charge ";
        $query .= "                                ON est_charge.est_charge_id = est_sales_invoice_line.est_charge_id ";
        $query .= "                                 INNER JOIN est_time_period ";
        $query .= "                      ON est_time_period.est_time_period_id = est_charge.repeat_est_time_period_id ";
        $query .= "                                 INNER JOIN fin_account ";
        $query .= "                      ON fin_account.fin_account_id = est_charge.fin_account_id ";
        $query .= "                         ) ";
        $query .= "                         rent ";
        $query .= "                ON       rent.est_sales_invoice_id = est_sales_invoice.est_sales_invoice_id ";
        //$query .= "                AND      rent.rent                 = 'Y' ";
        $query .= "                INNER JOIN tax_code ";
        $query .= "                ON       tax_code.tax_code_id = rent.tax_code_id ";
        $query .= "                INNER JOIN gen_userdef_group ";
        $query .= "                ON       gen_userdef_group.gen_userdef_group_id = rent.gen_userdef_group_id ";
        //$query .= "       GROUP BY lease_out_id ";
        $query .= "       GROUP BY est_sales_invoice_id, lease_out_id ";
        $query .= "       ) ";
        $query .= "       repdata ";
        $query .= "       LEFT JOIN ";
        $query .= "              ( SELECT est_sales_invoice_line.est_sales_invoice_id, ";
        $query .= "                      est_sales_invoice_line.net_total ";
        $query .= "              FROM    est_sales_invoice_line ";
        $query .= "                      INNER JOIN est_charge ";
        $query .= "                      ON      est_charge.est_charge_id = est_sales_invoice_line.est_charge_id ";
        $query .= "                      INNER JOIN est_charge_type ";
        $query .= "                      ON      est_charge_type.est_charge_type_id = est_charge.est_charge_type_id ";
        $query .= "              WHERE   est_charge_type.est_charge_type_code       = 'IN' ";
        $query .= "              ) ";
        $query .= "              insurance ";
        $query .= "       ON     insurance.est_sales_invoice_id = repdata.est_sales_invoice_id ";
        $query .= "       LEFT JOIN ";
        $query .= "              ( SELECT est_sales_invoice_line.est_sales_invoice_id, ";
        $query .= "                      est_sales_invoice_line.net_total ";
        $query .= "              FROM    est_sales_invoice_line ";
        $query .= "                      INNER JOIN est_charge ";
        $query .= "                      ON      est_charge.est_charge_id = est_sales_invoice_line.est_charge_id ";
        $query .= "                      INNER JOIN est_charge_type ";
        $query .= "                      ON      est_charge_type.est_charge_type_id = est_charge.est_charge_type_id ";
        $query .= "              WHERE   est_charge_type.est_charge_type_code       = 'SC' ";
        $query .= "              ) ";
        $query .= "              service ";
        $query .= "       ON     service.est_sales_invoice_id = repdata.est_sales_invoice_id";

        return \DB::table(null)->select(\DB::raw($query));
    }
}
