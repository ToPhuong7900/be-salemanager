<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\Common;
use Tfcloud\Services\Admin\System\Finance\SorService;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\PermissionService;

class InvoiceSORFilterQueryService extends BaseService
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    public function reAddQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $invoiceService = new SorService($this->permissionService);
        $sorQuery = $invoiceService->filterAll($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'validFromDate')) {
            array_push($whereTexts, \Lang::get('text.valid_on') . " $val");
        }

        if ($val = array_get($filterData, 'code')) {
            array_push($whereTexts, \Form::fieldLang('code') . " contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'description')) {
            array_push($whereTexts, \Form::fieldLang('description') . " contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['sorSet'])) {
            array_push($whereCodes, [
                'sor_set',
                'sor_set_id',
                'sor_set_code',
                $val, \Form::fieldLang('sor_set')
            ]);
        }

        return $sorQuery;
    }
}
