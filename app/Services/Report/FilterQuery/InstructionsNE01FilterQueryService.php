<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Common;
use Tfcloud\Models\InstructionParentType;
use Tfcloud\Models\Report;
use Tfcloud\Services\BaseService;
use Tfcloud\Models\Site;
use Tfcloud\Models\Building;
use Tfcloud\Models\Room;
use Tfcloud\Models\FinYear;
use Tfcloud\Models\FinAccount;
use Tfcloud\Services\PermissionService;

class InstructionsNE01FilterQueryService extends BaseService
{
    protected $permissionService;
    private $reportBoxFilterLoader = null;

    public function __construct(PermissionService $permissionService, Report $report = null)
    {
        $this->permissionService = $permissionService;
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;
    }

    public function reAddInstructionQuery(
        $filterData,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $filterData = $this->formatInputData($filterData);
        $query = '';
        $query = $this->buildNEL01INT10Query($query, $filterData);

        $query .= " UNION ALL ";

        $query = $this->buildNEL01CONT07Query($query, $filterData);

        $query .= " UNION ALL ";

        $query = $this->buildNEL01UDR256Query($query, $filterData);

        return \DB::table(null)
                ->select(
                    'Instruction Code',
                    'Reference',
                    'Instruction Description',
                    'Parent Type',
                    'Parent Code',
                    'Help Call Description',
                    'Invoice Status Description',
                    'Invoice No',
                    'Credit Note',
                    'Invoice Tax Date',
                    'Invoice Net Total',
                    'Invoice VAT',
                    'Invoice Gross Total',
                    'Establishment Code',
                    'Establishment Description',
                    'Help Call Contact Name',
                    'Help Call Contact Number',
                    'Created On',
                    'Instruction Owner',
                    'Print Template Code',
                    'Site Code',
                    'Site Description',
                    'Building Code',
                    'Building Description',
                    'Room Number',
                    'Room Description',
                    'Supplier Name',
                    'Year Code',
                    'Period Code',
                    'Account Code',
                    'Account Description'
                )->from(\DB::raw("(" . $query . ') AS report'))->orderBy('RecordType')->orderBy('Instruction Code');
    }

    private function buildNEL01INT10Query($query, $filterData)
    {
        $query .= "SELECT 1                                                     AS RecordType                   , ";
        $query .= "       site.site_id                                          AS site_id                      , ";
        $query .= "       building.building_id                                  AS building_id                  , ";
        $query .= "       room.room_id                                          AS room_id                      , ";
        $query .= "       instruction.site_group_id                                                             , ";
        $query .= "       fin_year.fin_year_id                                                                  , ";
        $query .= "       fin_account.fin_account_id                            AS fin_account_id               , ";
        $query .= "       fin_account.fin_account_code                          AS fin_account_code             , ";
        $query .= "       instruction.instruction_code                                                          , ";
        $query .= "       instruction.instruction_code                          AS `Instruction Code`           , ";
        $query .= "       instruction.reference                                 AS `Reference`                  , ";
        $query .= "       instruction.instruction_desc                          AS `Instruction Description`    , ";
        $query .= "       instruction_parent_type.instruction_parent_type_name  AS `Parent Type`                , ";
        $query .= "       COALESCE(helpcall.helpcall_code, project.project_code,                                  ";
        $query .= "       contract.contract_code, gm_contract.gm_contract_code) AS `Parent Code`                , ";
        $query .= "       helpcall.helpcall_desc                                AS `Help Call Description`      , ";
        $query .= "       invoice_status.invoice_status_desc                    AS `Invoice Status Description` , ";
        $query .= "       invoice.invoice_no                                    AS `Invoice No`                 , ";
        $query .= "       invoice.credit_note                                   AS `Credit Note`                , ";
        $query .= "       DATE_FORMAT(invoice.tax_date, '%d/%m/%Y')             AS `Invoice Tax Date`           , ";
        $query .= "       REPLACE(FORMAT(invoice.act_net_total, 2), ',', '')    AS `Invoice Net Total`          , ";
        $query .= "       REPLACE(FORMAT(invoice.act_vat, 2), ',', '')          AS `Invoice VAT`                , ";
        $query .= "       REPLACE(FORMAT(invoice.act_gross_total, 2), ',', '')  AS `Invoice Gross Total`        , ";
        $query .= "       establishment.establishment_code                      AS `Establishment Code`         , ";
        $query .= "       establishment.establishment_desc                      AS `Establishment Description`  , ";
        $query .= "       helpcall.contact_name                                 AS `Help Call Contact Name`     , ";
        $query .= "       helpcall.contact_telephone                            AS `Help Call Contact Number`   , ";
        $query .= "       DATE_FORMAT(instruction.created_on,'%d/%m/%Y')        AS `Created On`                 , ";
        $query .= "       instruction_user.display_name                         AS `Instruction Owner`          , ";
        $query .= "       print_template.print_template_code                    AS `Print Template Code`        , ";
        $query .= "       site.site_code                                        AS `Site Code`                  , ";
        $query .= "       site.site_desc                                        AS `Site Description`           , ";
        $query .= "       building.building_code                                AS `Building Code`              , ";
        $query .= "       building.building_desc                                AS `Building Description`       , ";
        $query .= "       room.room_number                                      AS `Room Number`                , ";
        $query .= "       room.room_desc                                        AS `Room Description`           , ";
        $query .= "       supplier.contact_name                                 AS `Supplier Name`              , ";
        $query .= "       fin_year.fin_year_code                                AS `Year Code`                  , ";
        $query .= "       fin_year_period.fin_year_period_code                  AS `Period Code`                , ";
        $query .= "       fin_account.fin_account_code                          AS `Account Code`               , ";
        $query .= "       fin_account.fin_account_desc                          AS `Account Description`          ";

        $query .= "FROM   Instruction ";
        $query .= "       LEFT JOIN helpcall ";
        $query .= "       ON     helpcall.helpcall_id       = instruction.parent_id ";
        $query .= "       AND    instruction.parent_type_id = " . InstructionParentType::HELPCALL;
        $query .= "       LEFT JOIN project ";
        $query .= "       ON     project.project_id         = instruction.parent_id ";
        $query .= "       AND    instruction.parent_type_id = " . InstructionParentType::PROJECT;
        $query .= "       LEFT JOIN contract ";
        $query .= "       ON     contract.contract_id       = instruction.parent_id ";
        $query .= "       AND    instruction.parent_type_id = " . InstructionParentType::CONTRACT;
        $query .= "       LEFT JOIN gm_contract ";
        $query .= "       ON     gm_contract.gm_contract_id = instruction.parent_id ";
        $query .= "       AND    instruction.parent_type_id = " . InstructionParentType::GROUNDS_MAINT;
        $query .= "       LEFT JOIN instruction_parent_type ";
        $query .= "       ON     instruction_parent_type.instruction_parent_type_id = ";
        $query .= "              instruction.parent_type_id ";
        $query .= "       LEFT JOIN print_template ";
        $query .= "       ON     print_template.print_template_id = instruction.print_template_id ";
        $query .= "       LEFT JOIN site ";
        $query .= "       ON     site.site_id = instruction.site_id ";
        $query .= "       LEFT JOIN building ";
        $query .= "       ON     building.building_id = instruction.building_id ";
        $query .= "       LEFT JOIN room ";
        $query .= "       ON     room.room_id = instruction.room_id ";
        $query .= "       LEFT JOIN contact supplier ";
        $query .= "       ON     supplier.contact_id = instruction.supplier_contact_id ";
        $query .= "       LEFT JOIN USER instruction_user ";
        $query .= "       ON     `instruction_user`.`id` = instruction.owner_user_id ";
        $query .= "       LEFT JOIN fin_year ";
        $query .= "       ON     fin_year.fin_year_id = instruction.fin_year_id ";
        $query .= "       LEFT JOIN fin_year_period ";
        $query .= "       ON     fin_year_period.fin_year_period_id = instruction.fin_year_period_id ";
        $query .= "       INNER JOIN instruction_bgt_line ";
        $query .= "       ON     instruction_bgt_line.instruction_id = instruction.instruction_id ";
        $query .= "       LEFT JOIN fin_account ";
        $query .= "       ON     instruction_bgt_line.fin_account_id = fin_account.fin_account_id ";
        $query .= "       LEFT JOIN establishment ";
        $query .= "       ON     site.establishment_id = establishment.establishment_id ";
        $query .= "       LEFT JOIN invoice_fin_account ";
        $query .= "       ON     invoice_fin_account.instruction_id = instruction.instruction_id ";
        $query .= "       LEFT JOIN invoice ";
        $query .= "       ON     invoice.invoice_id = invoice_fin_account.invoice_id ";
        $query .= "       LEFT JOIN invoice_status ";
        $query .= "       ON     invoice_status.invoice_status_id = invoice.invoice_status_id ";

        $query .= "WHERE  print_template.print_template_code = 'ENGIE' ";
        $query .= "AND    instruction_parent_type.instruction_parent_type_id != " . InstructionParentType::DLO_JOB;

        // Apply any filters
        $this->filterAll($query, $filterData);

        return $query;
    }

    private function buildNEL01CONT07Query($query, $filterData)
    {
        $query .= "SELECT 2                                                     AS RecordType                       , ";
        $query .= "       site.site_id                                          AS site_id                          , ";
        $query .= "       building.building_id                                  AS building_id                  , ";
        $query .= "       room.room_id                                          AS room_id                      , ";
        $query .= "       contract_instruction.site_group_id                                                        , ";
        $query .= "       fin_year.fin_year_id                                                                      , ";
        $query .= "       fin_account.fin_account_id                            AS fin_account_id                   , ";
        $query .= "       fin_account.fin_account_code                          AS fin_account_code                 , ";
        $query .= "       contract_instruction.contract_instruction_code                                            , ";
        $query .= "       contract_instruction.contract_instruction_code        AS 'Instruction Code'               , ";
        $query .= "       contract_instruction.reference                        AS 'Reference'                      , ";
        $query .= "       contract_instruction.contract_instruction_desc        AS 'Instruction Description'        , ";
        $query .= "       ''                                                    AS 'Parent Type'                    , ";
        $query .= "       ''                                                    AS 'Parent Code'                    , ";
        $query .= "       ''                                                    AS 'Help Call Description'          , ";
        $query .= "       ''                                                    AS 'Invoice Status Description'     , ";
        $query .= "       contract_invoice.contract_invoice_no                  AS 'Invoice No.'                    , ";
        $query .= "       ''                                                    AS 'Credit Note'                    , ";
        $query .= "       DATE_FORMAT(contract_invoice.tax_date, '%d/%m/%Y')    AS 'Tax Date'                       , ";
        $query .= "       inspection.contract_invoice_actual                    AS 'Net Total'                      , ";
        $query .= "       ''                                                    AS 'Invoice VAT'                    , ";
        $query .= "       ''                                                    AS 'Invoice Gross Total'            , ";
        $query .= "       ''                                                    AS 'Establishment Code'             , ";
        $query .= "       ''                                                    AS 'Establishment Description'      , ";
        $query .= "       ''                                                    AS 'Help Call Contact Name'         , ";
        $query .= "       ''                                                    AS 'Help Call Contact Number'       , ";
        $query .= "       DATE_FORMAT(contract_invoice.created_on, '%d/%m/%Y')  AS 'Created On'                     , ";
        $query .= "       ''                                                    AS 'Instruction Owner'              , ";
        $query .= "       print_template.print_template_code                    AS 'Print Template Code'            , ";
        $query .= "       site.site_code                                        AS 'Site Code'                      , ";
        $query .= "       site.site_desc                                        AS 'Site Description'               , ";
        $query .= "       building.building_code                                AS `Building Code`              , ";
        $query .= "       building.building_desc                                AS `Building Description`       , ";
        $query .= "       room.room_number                                      AS `Room Number`                , ";
        $query .= "       room.room_desc                                        AS `Room Description`           , ";
        $query .= "       supplier.contact_name                                 AS 'Supplier Name'                  , ";
        $query .= "       fin_year.fin_year_code                                AS `Year Code`                      , ";
        $query .= "       fin_year_period.fin_year_period_code                  AS `Period Code`                    , ";
        $query .= "       fin_account.fin_account_code                          AS 'Account Code'                   , ";
        $query .= "       fin_account.fin_account_desc                          AS 'Account Description'              ";

        $query .= "FROM   contract_invoice ";
        $query .= "       LEFT JOIN contract_instruction ";
        $query .= "       ON     contract_instruction.contract_instruction_id = " .
            " contract_invoice.contract_instruction_id ";
        $query .= "       LEFT JOIN contract_invoice_status ";
        $query .= "       ON     contract_invoice_status.contract_invoice_status_id = " .
            " contract_invoice.contract_invoice_status_id ";
        $query .= "       LEFT JOIN contract ";
        $query .= "       ON     contract.contract_id = contract_instruction.contract_id ";
        $query .= "       LEFT JOIN print_template ";
        $query .= "       ON     print_template.print_template_id = contract_instruction.print_template_id ";
        $query .= "       LEFT JOIN contact supplier ";
        $query .= "       ON     supplier.contact_id = contract.supplier_contact_id ";
        $query .= "       LEFT JOIN fin_year ";
        $query .= "       ON     contract_invoice.fin_year_id = fin_year.fin_year_id ";
        $query .= "       LEFT JOIN fin_year_period ";
        $query .= "       ON     contract_invoice.fin_year_period_id = fin_year_period.fin_year_period_id ";
        $query .= "       LEFT JOIN inspection ";
        $query .= "       ON     inspection.contract_invoice_id = contract_invoice.contract_invoice_id ";
        $query .= "       LEFT JOIN site ";
        $query .= "       ON     site.site_id = inspection.site_id ";
        $query .= "       LEFT JOIN building ";
        $query .= "       ON     building.building_id = inspection.building_id ";
        $query .= "       LEFT JOIN room ";
        $query .= "       ON     room.room_id = inspection.room_id ";
        $query .= "       LEFT JOIN fin_account ";
        $query .= "       ON     fin_account.fin_account_id  = inspection.fin_account_id ";

        $query .= " WHERE  print_template.print_template_code = 'ENGIE' ";

        // Apply any filters
        $this->filterAll($query, $filterData);

        $query .= "UNION ALL ";

        $query .= "SELECT 2                                                     AS RecordType                   , ";
        $query .= "       site.site_id                                          AS site_id                      , ";
        $query .= "       building.building_id                                  AS building_id                  , ";
        $query .= "       room.room_id                                          AS room_id                      , ";
        $query .= "       contract_instruction.site_group_id                                                    , ";
        $query .= "       fin_year.fin_year_id                                                                  , ";
        $query .= "       fin_account.fin_account_id                            AS fin_account_id               , ";
        $query .= "       fin_account.fin_account_code                          AS fin_account_code             , ";
        $query .= "       contract_instruction.contract_instruction_code                                        , ";
        $query .= "       contract_instruction.contract_instruction_code        AS 'Instruction Code'           , ";
        $query .= "       contract_instruction.reference                        AS 'Reference'                  , ";
        $query .= "       contract_instruction.contract_instruction_desc        AS 'Instruction Description'    , ";
        $query .= "       ''                                                    AS 'Parent Type'                , ";
        $query .= "       ''                                                    AS 'Parent Code'                , ";
        $query .= "       ''                                                    AS 'Help Call Description'      , ";
        $query .= "       ''                                                    AS 'Invoice Status Description' , ";
        $query .= "       contract_invoice.contract_invoice_no                  AS 'Invoice No.'                , ";
        $query .= "       'Y'                                                   AS 'Credit Note'                , ";
        $query .= "       DATE_FORMAT(contract_invoice.tax_date, '%d/%m/%Y')    AS 'Tax Date'                   , ";
        $query .= "       FORMAT(-1*contract_credit.net_total, 2)               AS 'Net Total'                  , ";
        $query .= "       ''                                                    AS 'Invoice VAT'                , ";
        $query .= "       ''                                                    AS 'Invoice Gross Total'        , ";
        $query .= "       ''                                                    AS 'Establishment Code'         , ";
        $query .= "       ''                                                    AS 'Establishment Description'  , ";
        $query .= "       ''                                                    AS 'Help Call Contact Name'     , ";
        $query .= "       ''                                                    AS 'Help Call Contact Number'   , ";
        $query .= "       DATE_FORMAT(contract_invoice.created_on, '%d/%m/%Y')  AS 'Created On'                 , ";
        $query .= "       ''                                                    AS 'Instruction Owner'          , ";
        $query .= "       print_template.print_template_code                    AS 'Print Template Code'        , ";
        $query .= "       site.site_code                                        AS 'Site Code'                  , ";
        $query .= "       site.site_desc                                        AS 'Site Description'           , ";
        $query .= "       building.building_code                                AS `Building Code`              , ";
        $query .= "       building.building_desc                                AS `Building Description`       , ";
        $query .= "       room.room_number                                      AS `Room Number`                , ";
        $query .= "       room.room_desc                                        AS `Room Description`           , ";
        $query .= "       supplier.contact_name                                 AS 'Supplier Name'              , ";
        $query .= "       fin_year.fin_year_code                                AS `Year Code`                  , ";
        $query .= "       fin_year_period.fin_year_period_code                  AS `Period Code`                , ";
        $query .= "       fin_account.fin_account_code                          AS 'Account Code'               , ";
        $query .= "       fin_account.fin_account_desc                          AS 'Account Description'          ";

        $query .= "FROM   contract_credit ";
        $query .= "       LEFT JOIN contract_invoice ";
        $query .= "       ON     contract_invoice.contract_invoice_id = contract_credit.contract_invoice_id ";
        $query .= "       LEFT JOIN contract_instruction ";
        $query .= "       ON     contract_instruction.contract_instruction_id = " .
            " contract_invoice.contract_instruction_id ";
        $query .= "       LEFT JOIN contract_credit_status ";
        $query .= "       ON     contract_credit_status.contract_credit_status_id = " .
            "contract_credit.contract_credit_status_id ";
        $query .= "       LEFT JOIN contract ";
        $query .= "       ON     contract.contract_id = contract_instruction.contract_id ";
        $query .= "       LEFT JOIN print_template ";
        $query .= "       ON     print_template.print_template_id = contract_instruction.print_template_id ";
        $query .= "       LEFT JOIN contact supplier ";
        $query .= "       ON     supplier.contact_id = contract.supplier_contact_id ";
        $query .= "       LEFT JOIN fin_year ";
        $query .= "       ON     contract_credit.fin_year_id = fin_year.fin_year_id ";
        $query .= "       LEFT JOIN fin_year_period ";
        $query .= "       ON     contract_credit.fin_year_period_id = fin_year_period.fin_year_period_id ";
        $query .= "       LEFT JOIN contract_credit_inspection ";
        $query .= "       ON     contract_credit_inspection.contract_credit_id = contract_credit.contract_credit_id ";
        $query .= "       LEFT JOIN inspection ";
        $query .= "       ON     inspection.inspection_id = contract_credit_inspection.inspection_id ";
        $query .= "       LEFT JOIN site ";
        $query .= "       ON     site.site_id = inspection.site_id ";
        $query .= "       LEFT JOIN building ";
        $query .= "       ON     building.building_id = inspection.building_id ";
        $query .= "       LEFT JOIN room ";
        $query .= "       ON     room.room_id = inspection.room_id ";
        $query .= "       LEFT JOIN fin_account ";
        $query .= "       ON     fin_account.fin_account_id  = inspection.fin_account_id ";

        $query .= " WHERE  print_template.print_template_code = 'ENGIE' ";

        // Apply any filters
        $this->filterAll($query, $filterData);

        return $query;
    }

    private function buildNEL01UDR256Query($query, $filterData)
    {
        $query .= "SELECT * FROM ( ";
        $query .= "SELECT 3                                                 AS RecordType                  , ";
        $query .= "       site.site_id                                      AS site_id                     , ";
        $query .= "       building.building_id                                  AS building_id             , ";
        $query .= "       room.room_id                                          AS room_id                 , ";
        $query .= "       dlo_job.site_group_id                                                            , ";
        $query .= "    ( SELECT fin_year.fin_year_id                                                         ";
        $query .= "         FROM    fin_year                                                                 ";
        $query .= "         WHERE   `dlo_job`.`created_on` >= fin_year.year_start_date                       ";
        $query .= "             AND     `dlo_job`.`created_on` <= fin_year.year_end_date                     ";
        $query .= "             AND     fin_year.site_group_id  = `dlo_job`.`site_group_id`                  ";
        $query .= "    )                                                    AS fin_year_id                 , ";
        $query .= "       fin_account.fin_account_id                        AS fin_account_id              , ";
        $query .= "       fin_account.fin_account_code                      AS fin_account_code            , ";
        $query .= "       `dlo_job`.`dlo_job_code`                          AS instruction_code            , ";
        $query .= "       `dlo_job`.`dlo_job_code`                          AS `Instruction Code`          , ";
        $query .= "       ''                                                AS `Reference`                 , ";
        $query .= "       ''                                                AS `Instruction Description`   , ";
        $query .= "       ''                                                AS `Parent Type`               , ";
        $query .= "       'DloJob'                                          AS `Parent Code`               , ";
        $query .= "       `dlo_job`.`dlo_job_desc`                          AS `Help Call Description`     , ";
        $query .= "       ''                                                AS `Invoice Status Description`, ";
        $query .= "       ''                                                AS `Invoice No`                , ";
        $query .= "       ''                                                AS `Credit Note`               , ";
        $query .= "       ''                                                AS `Tax Date`                  , ";
        $query .= "       CASE ";
        $query .= "              WHEN `dlo_sales_invoice`.`quoted_total` > 0 ";
        $query .= "              THEN `dlo_sales_invoice`.`quoted_total` ";
        $query .= "              ELSE `dlo_sales_invoice`.`labour_total`  + `dlo_sales_invoice`.`material_total` + ";
        $query .= "                     `dlo_sales_invoice`.`plant_total` + `dlo_sales_invoice`.`subcontract_total` ";
        $query .= "       END                                               AS `Net Total`                , ";
        $query .= "       ''                                                AS `Invoice VAT`              , ";
        $query .= "       ''                                                AS `Invoice Gross Total`      , ";
        $query .= "       ''                                                AS `Establishment Code`       , ";
        $query .= "       ''                                                AS `Establishment Description`, ";
        $query .= "       ''                                                AS `Help Call Contact Name`   , ";
        $query .= "       ''                                                AS `Help Call Contact Number` , ";
        $query .= "       date_format(`dlo_job`.`created_on`,'%d/%m/%Y')    AS `Created On`               , ";
        $query .= "       ''                                                AS `Instruction Owner`        , ";
        $query .= "       ''                                                AS `Print Template Code`      , ";
        $query .= "       site.site_code                                    AS `Site Code`                , ";
        $query .= "       site.site_desc                                    AS `Site Description`         , ";
        $query .= "       building.building_code                                AS `Building Code`              , ";
        $query .= "       building.building_desc                                AS `Building Description`       , ";
        $query .= "       room.room_number                                      AS `Room Number`                , ";
        $query .= "       room.room_desc                                        AS `Room Description`           , ";
        $query .= "       ''                                                AS `Supplier Name`            , ";
        $query .= "       ''                                                AS `Year Code`                , ";
        $query .= "       ''                                                AS `Period Code`              , ";
        $query .= "       fin_account.fin_account_code                      AS `Account Code`             , ";
        $query .= "       fin_account.fin_account_desc                      AS `Account Description` ";

        $query .= "FROM   `cp_contract_item` ";
        $query .= "       JOIN `dlo_job` ON `dlo_job`.`dlo_job_id` = `cp_contract_item`.`dlo_job_id` ";
        $query .= "       LEFT JOIN `dlo_sales_invoice` ON `dlo_sales_invoice`.`parent_id` = `dlo_job`.`dlo_job_id` ";
        $query .= "       LEFT JOIN `cp_sales_invoice_line` AS cpsl ";
        $query .= "       ON     cpsl.`dlo_sales_invoice_id` = `dlo_sales_invoice`.`dlo_sales_invoice_id` ";
        $query .= "       LEFT JOIN `cp_sales_invoice` AS cps ";
        $query .= "       ON     cps.`cp_sales_invoice_id` = cpsl.`cp_sales_invoice_id` ";
        $query .= "       LEFT JOIN site ";
        $query .= "       ON     site.site_id = dlo_job.site_id ";
        $query .= "       LEFT JOIN building ";
        $query .= "       ON     building.building_id = dlo_job.building_id ";
        $query .= "       LEFT JOIN room ";
        $query .= "       ON     room.room_id = dlo_job.room_id ";
        $query .= "       LEFT JOIN fin_account ON fin_account.fin_account_id = dlo_job.fin_account_id ";
        $query .= ") T";

        $query .= " WHERE 1 = 1";

        // Financial Year
        if (Common::iset($filterData['finYear'])) {
            $query .= " AND T.fin_year_id = " . $filterData['finYear'] . " ";
        }

        // Account
        if (Common::iset($filterData['account'])) {
            $query .= " AND T.fin_account_id = " . $filterData['account'] . " ";
        }

        // Account Code
        if (Common::iset($filterData['accountCode'])) {
            $query .= " AND T.fin_account_code LIKE '%" . $filterData['accountCode'] . "%' ";
        }

        // Site
        if (Common::iset($filterData['site_id'])) {
            $query .= " AND T.site_id = " . $filterData['site_id'] . " ";
        }

        // Building
        if (Common::iset($filterData['building_id'])) {
            $query .= " AND T.building_id = " . $filterData['building_id'] . " ";
        }
        // Room
        if (Common::iset($filterData['room_id'])) {
            $query .= " AND T.room_id = " . $filterData['room_id'] . " ";
        }

        return $query;
    }

    private function filterAll(&$query, $filterData)
    {
        // Financial Year
        if (Common::iset($filterData['finYear'])) {
            $query .= " AND fin_year.fin_year_id = " . $filterData['finYear'] . " ";
        }

        // Account
        if (Common::iset($filterData['account'])) {
            $query .= " AND fin_account.fin_account_id = " . $filterData['account'] . " ";
        }

        // Account Code
        if (Common::iset($filterData['accountCode'])) {
            $query .= " AND fin_account.fin_account_code LIKE '%" . $filterData['accountCode'] . "%' ";
        }

        // Site
        if (Common::iset($filterData['site_id'])) {
            $query .= " AND site.site_id = " . $filterData['site_id'] . " ";
        }

        // Building
        if (Common::iset($filterData['building_id'])) {
            $query .= " AND building.building_id = " . $filterData['building_id'] . " ";
        }

        // Room
        if (Common::iset($filterData['room_id'])) {
            $query .= " AND room.room_id = " . $filterData['room_id'] . " ";
        }
    }
    private function formatInputData($filterData)
    {
        if ($this->reportBoxFilterLoader && $this->reportBoxFilterLoader->hasBoxFilter()) {
            $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($filterData);
            $getValueAccountId = $reportFilterQuery->getValueFilterField('fin_account_id');
            $getValueFinYearId = $reportFilterQuery->getValueFilterField('fin_year_id');
            $getValueFinAccountCode = $reportFilterQuery->getValueFilterField('fin_account_code');
            $getValueSite = $reportFilterQuery->getValueFilterField('site_id');
            $getValueBuilding = $reportFilterQuery->getValueFilterField('building_id');
            $getValueRoom = $reportFilterQuery->getValueFilterField('room_id');

            $filterData['accountCode'] = empty($getValueFinAccountCode) ? null : $getValueFinAccountCode[0];
            $filterData['account'] = empty($getValueAccountId) ? null : $getValueAccountId[0];
            $filterData['finYear'] = empty($getValueFinYearId) ? null : $getValueFinYearId[0];
            $filterData['site_id'] = empty($getValueSite) ? null : $getValueSite[0];
            $filterData['building_id'] = empty($getValueBuilding) ? null : $getValueBuilding[0];
            $filterData['room_id'] = empty($getValueRoom) ? null : $getValueRoom[0];
        }

        return $filterData;
    }
}
