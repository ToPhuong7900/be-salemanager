<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Services\BaseService;
use Tfcloud\Services\Education\SufficiencyService;
use Tfcloud\Services\PermissionService;

class SufficienciesFilterQueryService extends BaseService
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    public function reAddSufficiencyQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $suffQuery = SufficiencyService::filterAll($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'code')) {
            array_push($whereTexts, "Code contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'description')) {
            array_push($whereTexts, "Description contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'type')) {
            array_push(
                $whereCodes,
                ['suff_type', 'suff_type_id', 'suff_type_code', $val, 'Sufficiency Type']
            );
        }

        if ($val = array_get($filterData, 'status')) {
            array_push(
                $orCodes,
                ['suff_status', 'suff_status_id', 'suff_status_desc', $val, 'Sufficiency Status']
            );
        }

        if ($val = array_get($filterData, 'assessment_date')) {
            array_push($whereTexts, "Assessment date contains '{$val}'");
        }

        if ($val = array_get($filterData, 'exclude')) {
            if ($val !== 'all') {
                array_push($whereTexts, "Excluded = '{$val}'");
            }
        }


        if ($val = array_get($filterData, 'site_id')) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);
        }

        return $suffQuery;
    }
}
