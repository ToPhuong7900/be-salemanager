<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\FixedAsset\CaSystemManager;
use Tfcloud\Models\CaIfrsCategory;
use Tfcloud\Models\CaTransactionType;
use Tfcloud\Models\GenTable;
use Tfcloud\Models\Report;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\FixedAsset\FixedAssetListService;
use Tfcloud\Services\FixedAsset\TransactionService;
use Tfcloud\Services\PermissionService;

class FixedAssetsFilterQueryService extends BaseService
{
    protected $permissionService;
    // Filters.
    protected $commonGood = false;
    protected $investment = false;
    protected $deminimis = false;
    protected $entity = false;
    protected $filterData = [];

    private $reportBoxFilterLoader = null;

    public function __construct(PermissionService $permissionService, $filterData = [], Report $report = null)
    {
        $this->permissionService = $permissionService;
        $this->reportBoxFilterLoader = $report ? new ClassMapReportLoader($report) : null;

        $this->setFilterProperties($filterData);
    }

    private function setFilterProperties($filterData)
    {
        $reportFilterQuery = $this->reportBoxFilterLoader->getFilterQuery($filterData);
        $commonGood = $reportFilterQuery->getFirstValueFilterField('common_good');
        $investment = $reportFilterQuery->getFirstValueFilterField('investment');
        $deminimis = $reportFilterQuery->getFirstValueFilterField('deminimis_asset');
        $entity = $reportFilterQuery->getFirstValueFilterField('ca_entity_id');

        // get filter values
        $this->filterData = $filterData;
        $this->commonGood = (!$commonGood) ? false : $commonGood;
        $this->investment = (!$investment) ? false : $investment;
        $this->deminimis = (!$deminimis) ? false : $deminimis;
        $this->entity = (!$entity) ? false : $entity;
    }

    public function reAddFixedAssetsQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $service = new FixedAssetListService($this->permissionService);
        $query = $service->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['code'])) {
            array_push($whereTexts, "Fixed Asset Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['description'])) {
            array_push($whereTexts, "Fixed Asset Description contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'category', null)) {
            array_push($whereCodes, [
                'ca_ifrs_category',
                'ca_ifrs_category_id',
                'ca_ifrs_category_code',
                $val,
                \Form::fieldLang('ca_ifrs_category_id')
            ]);
        }

        if ($val = array_get($filterData, 'ca_depreciation_type_id', null)) {
            array_push($whereCodes, [
                'ca_depreciation_type',
                'ca_depreciation_type_id',
                'ca_depreciation_type_code',
                $val,
                \Form::fieldLang('ca_depreciation_type_id')
            ]);
        }

        if ($val = array_get($filterData, 'ca_account_code_id', null)) {
            array_push($whereCodes, [
                'ca_account',
                'ca_account_id',
                'ca_account_code',
                $val,
                \Form::fieldLang('ca_account_code_id')
            ]);
        }

        if ($val = array_get($filterData, 'committee_id', null)) {
            array_push($whereCodes, [
                'committee',
                'committee_id',
                'committee_code',
                $val,
                \Form::fieldLang('committee_id')
            ]);
        }

        if ($val = array_get($filterData, 'classification', null)) {
            array_push($whereCodes, [
                'ca_fixed_asset_classification',
                'ca_fixed_asset_classification_id',
                'ca_fixed_asset_classification_code',
                $val,
                \Form::fieldLang('ca_fixed_asset_classification_id')
            ]);
        }


        if ($val = Common::iset($filterData['type'])) {
            array_push(
                $whereCodes,
                ['ca_fixed_asset_type', 'ca_fixed_asset_type_id', 'ca_fixed_asset_type_code', $val, 'Asset Type']
            );
        }

        if ($val = Common::iset($filterData['active'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.active_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.active_no'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.active_all'));
                    break;
            }
        }

        if ($val = Common::iset($filterData['master_asset'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.master_asset_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.master_asset_no'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.master_asset_all'));
                    break;
            }
        }

        if ($val = Common::iset($filterData['deminimis_asset'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.deminimis_asset_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.deminimis_asset_no'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.deminimis_asset_all'));
                    break;
            }
        }
        if ($val = Common::iset($filterData['hra_asset'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.hra_asset_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.hra_asset_no'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.hra_asset_all'));
                    break;
            }
        }
        if ($val = Common::iset($filterData['investment'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.investment_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.investment_no'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.investment_all'));
                    break;
            }
        }

        if ($val = Common::iset($filterData['ca_balance_historic'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.ca_balance_historic_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.ca_balance_historic_no'));
                    break;
                default:
                    break;
            }
        }

        if ($viewName == 'vw_far07') {
            $this->reAddUserDefinesQuery(GenTable::CA_FIXED_ASSET, $filterData, $whereCodes, $whereTexts);
        }

        return $query;
    }

    public function reAddFixedAssetTransactionQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $service = new TransactionService($this->permissionService);
        $query = $service->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['fixedAssetCode'])) {
            array_push($whereTexts, "Fixed Asset Code contains '" . $val . "'");
        }


        if ($val = array_get($filterData, 'ca_fixed_asset_id', null)) {
            array_push($whereCodes, [
                'ca_fixed_assset',
                'ca_fixed_asset_id',
                'ca_fixed_asset_code',
                $val,
                \Form::fieldLang('ca_fixed_asset_id')
            ]);
        }

        if ($val = array_get($filterData, 'caFinYear', null)) {
            array_push($whereCodes, [
                'ca_fin_year',
                'ca_fin_year_id',
                'ca_fin_year_code',
                $val,
                \Form::fieldLang('financial_year')
            ]);
        }


        if ($val = array_get($filterData, 'caTransactionType', null)) {
            array_push($whereCodes, [
                'ca_transaction_type',
                'ca_transaction_type_id',
                'ca_transaction_type_code',
                $val,
                \Form::fieldLang('transaction_type')
            ]);
        }

        return $query;
    }


    /**
     * Report FAR08: Revaluation Reserve Statement
     */
    public function reAddRevaluationReserveStatementQuery(
        $filterData,
        &$whereCodes,
        &$orCodes,
        &$whereTexts,
        &$numericCols = []
    ) {
        $numericCols[] = 2;

        // set filter values
       // $commonGood = $filterData['common_good'] ?? false;
       // $investment = $filterData['investment'] ?? false;
      //  $deminimis = $filterData['deminimis_asset'] ?? CommonConstant::DATABASE_VALUE_NO;

      //  $this->commonGood = ($commonGood == 'All') ? false : $commonGood;
      //  $this->investment = ($investment == 'All') ? false : $investment;
      //  $this->deminimis = ($deminimis == 'All') ? false : $deminimis;

        // get IFRS Categories
        $query = CaIfrsCategory::userSiteGroup()->active()->orderBy("ca_ifrs_category_code");
        if ($this->commonGood) {
            $query->where('common_good', '=', $this->commonGood);
        }
        if ($this->investment) {
            $query->where('investment', '=', $this->investment);
        }
        $caIfrsCategories = $query->get();

        $unions = "";
        $first = true;
        foreach ($caIfrsCategories as $cat) {
            if ($first) {
                $unions = $this->titleQuery($cat);
                $first = false;
            } else {
                $unions = $unions->unionAll($this->titleQuery($cat));
            }
            $unions->unionAll($this->openingBalanceQuery($cat->getKey()));
            $unions->unionAll($this->upwardRevaluationsQuery($cat->getKey()));
            $unions->unionAll($this->downwardRevaluationsQuery($cat->getKey()));
            $unions->unionAll($this->depreciationQuery($cat->getKey()));
            $unions->unionAll($this->impairmentsQuery($cat->getKey()));
            $unions->unionAll($this->disposalsDerecognitionQuery($cat->getKey()));
            $unions->unionAll($this->transfersQuery($cat->getKey()));
            $unions->unionAll($this->writeOffToCaaQuery($cat->getKey()));
            $unions->unionAll($this->closingBalanceQuery($cat->getKey()));
        }

        if ($val = Common::iset($filterData['deminimis_asset'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.deminimis_asset_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.deminimis_asset_no'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.deminimis_asset_all'));
                    break;
            }
        }


        if ($val = Common::iset($filterData['common_good'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.common_good_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.common_good_no'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.common_good_all'));
                    break;
            }
        }


        if ($val = Common::iset($filterData['investment'])) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.investment_yes'));
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.investment_no'));
                    break;
                default:
                    array_push($whereTexts, \Lang::get('text.report_texts.fixedAssets.investment_all'));
                    break;
            }
        }


        return \DB::table(null)->select('*')->from(\DB::raw("(" . $unions->toSql() . ') AS report'));
    }

    public static function far09SetInvestment(&$query)
    {
        if (Common::valueYorNtoBoolean(\Auth::user()->siteGroup->ca_hide_rr_on_investment)) {
            $query->where('investment', CommonConstant::DATABASE_VALUE_NO);
        } else {
            //$query->where('investment', CommonConstant::DATABASE_VALUE_YES);
        }
    }


    public static function far07ReplaceColumn($reportColumns)
    {
        if (Common::valueYorNtoBoolean(\Auth::user()->siteGroup->ca_hide_rr_on_investment)) {
            $explodeColumn = explode(',', $reportColumns);

            self::replaceCol('BFwd RR Asset', $explodeColumn);
            self::replaceCol('BFwd RR Land', $explodeColumn);
            self::replaceCol('RR Depreciation', $explodeColumn);
            self::replaceCol('CFwd RR Asset', $explodeColumn);
            self::replaceCol('CFwd RR Land', $explodeColumn);

            $reportColumnsReplace = implode(',', $explodeColumn);
            return $reportColumnsReplace;
        } else {
            return $reportColumns;
        }
    }

    private static function replaceCol($col, &$explodeColumn)
    {
        $newcCol = "IF((`vw_far07`.`investment` = 'N'), `$col`, "
            . "0) AS `$col`";

        $rrField = array_search("vw_far07.`$col`", $explodeColumn);

        $explodeColumn[$rrField] = $newcCol;
    }

    private function titleQuery($cat)
    {
        return \DB::table(null)->select([
            \DB::raw("'{$cat->ca_ifrs_category_code} - {$cat->ca_ifrs_category_desc}' AS `IFRS Category`"),
            \DB::raw("'' AS `Element`"),
            \DB::raw("'' AS `Amount`")
        ]);
    }

    private function deminimisWhere()
    {
        if ($this->deminimis) {
            return " AND ca_fixed_asset.deminimis_asset = '{$this->deminimis}'";
        }
    }

    private function entityWhere()
    {
        if ($this->entity) {
            return " AND ca_fixed_asset.ca_entity_id = '{$this->entity}'";
        }
    }

    private function openingBalanceQuery($caIfrsCategoryId)
    {
        $query = \DB::table(null)->select([
            \DB::raw("'' AS `IFRS Category`"),
            \DB::raw("'Opening Balance' AS `Element`"),
            \DB::raw(
                "ROUND(IFNULL((SELECT SUM(bfwd_rr) FROM ca_fixed_asset"
                . " INNER JOIN ca_rr_balance ON ca_rr_balance.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id"
                . " INNER JOIN vw_ca_current_fin_year ON"
                . " vw_ca_current_fin_year.ca_fin_year_id = ca_rr_balance.ca_fin_year_id"
                . " WHERE ca_fixed_asset.bfwd_ifrs_cat_id = {$caIfrsCategoryId}"
                . $this->deminimisWhere()
                . $this->entityWhere()
                . " AND ca_fixed_asset.active = '" . CommonConstant::DATABASE_VALUE_YES . "'"
                . " ), 0), 0) AS `Amount` "
            )
        ]);
        return $query;
    }
    private function upwardRevaluationsQuery($caIfrsCategoryId)
    {
        $query = \DB::table(null)->select([
            \DB::raw("'' AS `IFRS Category`"),
            \DB::raw("'Upward Revaluations' AS `Element`"),
            \DB::raw(
                "ROUND(IFNULL((SELECT SUM(cv_asset + cv_land) FROM ca_fixed_asset"
                . " INNER JOIN ca_transaction ON ca_transaction.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id"
                . "     AND ca_transaction.ca_ifrs_cat_id = {$caIfrsCategoryId}"
                . " INNER JOIN vw_ca_current_fin_year ON"
                . " vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id"
                . " WHERE rr_trans = '" . CommonConstant::DATABASE_VALUE_YES . "'"
                . " AND ca_transaction_type_id = " . CaTransactionType::REVALUATION
                . " AND cv_asset >= 0"
                . " AND cv_land >= 0"
                . $this->deminimisWhere()
                . $this->entityWhere()
                . " AND ca_fixed_asset.active = '" . CommonConstant::DATABASE_VALUE_YES . "'"
                . " ), 0), 0) AS `Amount` "
            )
        ]);
        return $query;
    }
    private function downwardRevaluationsQuery($caIfrsCategoryId)
    {
        $query = \DB::table(null)->select([
            \DB::raw("'' AS `IFRS Category`"),
            \DB::raw("'Downward Revaluations' AS `Element`"),
            \DB::raw(
                "ROUND(IFNULL((SELECT SUM((cv_asset - hc_asset) + (cv_land - hc_land)) FROM ca_fixed_asset"
                . " INNER JOIN ca_transaction ON ca_transaction.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id"
                . "     AND ca_transaction.ca_ifrs_cat_id = {$caIfrsCategoryId}"
                . " INNER JOIN vw_ca_current_fin_year ON"
                . " vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id"
                . " WHERE rr_trans = '" . CommonConstant::DATABASE_VALUE_YES . "'"
                . " AND ca_transaction_type_id = " . CaTransactionType::REVALUATION
                . " AND (cv_asset < 0 OR cv_land < 0)"
                . $this->deminimisWhere()
                . $this->entityWhere()
                . " AND ca_fixed_asset.active = '" . CommonConstant::DATABASE_VALUE_YES . "'"
                . " ), 0), 0) AS `Amount` "
            )
        ]);
        return $query;
    }
    private function depreciationQuery($caIfrsCategoryId)
    {
        $bYearEnd = CaSystemManager::isInYearEndMode();

        $query = \DB::table(null)->select([
            \DB::raw("'' AS `IFRS Category`"),
            \DB::raw("'Depreciation' AS `Element`"),
            \DB::raw(
                "ROUND(IFNULL((SELECT SUM(cy_rr_depreciation) * -1 FROM ca_fixed_asset"
                . " INNER JOIN ca_rr_balance ON ca_rr_balance.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id"
                . " INNER JOIN vw_ca_current_fin_year ON"
                . " vw_ca_current_fin_year.ca_fin_year_id = ca_rr_balance.ca_fin_year_id"
                . " WHERE ca_fixed_asset." . ($bYearEnd ? "bfwd_ifrs_cat_id" : "ca_ifrs_cat_id")
                . "    = {$caIfrsCategoryId}"
                . $this->deminimisWhere()
                . $this->entityWhere()
                . " AND ca_fixed_asset.active = '" . CommonConstant::DATABASE_VALUE_YES . "'"
                . " ), 0), 0) AS `Amount` "
            )
        ]);
        return $query;
    }
    private function impairmentsQuery($caIfrsCategoryId)
    {
        $query = \DB::table(null)->select([
            \DB::raw("'' AS `IFRS Category`"),
            \DB::raw("'Impairment Losses' AS `Element`"),
            \DB::raw(
                "ROUND(IFNULL((SELECT SUM((cv_asset - hc_asset) + (cv_land - hc_land)) * -1 FROM ca_fixed_asset"
                . " INNER JOIN ca_transaction ON ca_transaction.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id"
                . "     AND ca_transaction.ca_ifrs_cat_id = {$caIfrsCategoryId}"
                . " INNER JOIN vw_ca_current_fin_year ON"
                . " vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id"
                . " WHERE rr_trans = '" . CommonConstant::DATABASE_VALUE_YES . "'"
                . " AND ca_transaction_type_id = " . CaTransactionType::IMPAIRMENT
                . $this->deminimisWhere()
                . $this->entityWhere()
                . " AND ca_fixed_asset.active = '" . CommonConstant::DATABASE_VALUE_YES . "'"
                . " ), 0), 0) AS `Amount` "
            )
        ]);
        return $query;
    }
    private function disposalsDerecognitionQuery($caIfrsCategoryId)
    {
        $query = \DB::table(null)->select([
            \DB::raw("'' AS `IFRS Category`"),
            \DB::raw("'Disposals and Derecognition' AS `Element`"),
            \DB::raw(
                "ROUND(IFNULL((SELECT SUM((cv_asset - hc_asset) + (cv_land - hc_land)) FROM ca_fixed_asset"
                . " INNER JOIN ca_transaction ON ca_transaction.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id"
                . "     AND ca_transaction.ca_ifrs_cat_id = {$caIfrsCategoryId}"
                . " INNER JOIN vw_ca_current_fin_year ON"
                . " vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id"
                . " WHERE rr_trans = '" . CommonConstant::DATABASE_VALUE_YES . "'"
                . " AND ca_transaction_type_id IN"
                . " (" . CaTransactionType::DISPOSAL . "," . CaTransactionType::DERECOGNITION . ")"
                . $this->deminimisWhere()
                . $this->entityWhere()
                . " AND ca_fixed_asset.active = '" . CommonConstant::DATABASE_VALUE_YES . "'"
                . " ), 0), 0) AS `Amount` "
            )
        ]);
        return $query;
    }
    private function transfersQuery($caIfrsCategoryId)
    {
        $query = \DB::table(null)->select([
            \DB::raw("'' AS `IFRS Category`"),
            \DB::raw("'Transfers' AS `Element`"),
            \DB::raw(
                "ROUND(IFNULL((SELECT SUM((cv_asset - hc_asset) + (cv_land - hc_land)) FROM ca_fixed_asset"
                . " INNER JOIN ca_transaction ON ca_transaction.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id"
                . "     AND ca_transaction.ca_ifrs_cat_id = {$caIfrsCategoryId}"
                . " INNER JOIN vw_ca_current_fin_year ON"
                . " vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id"
                . " WHERE ca_transaction.rr_trans = '" . CommonConstant::DATABASE_VALUE_YES . "'"
                . " AND ca_transaction_type_id = " . CaTransactionType::CATEGORY_TRANSFER
                . $this->deminimisWhere()
                . $this->entityWhere()
                . " AND ca_fixed_asset.active = '" . CommonConstant::DATABASE_VALUE_YES . "'"
                . " ), 0), 0) AS `Amount` "
            )
        ]);
        return $query;
    }
    private function writeOffToCaaQuery($caIfrsCategoryId)
    {
        $query = \DB::table(null)->select([
            \DB::raw("'' AS `IFRS Category`"),
            \DB::raw("'Write off to CAA' AS `Element`"),
            \DB::raw(
                "ROUND(IFNULL((SELECT SUM((cv_asset - hc_asset) + (cv_land - hc_land)) FROM ca_fixed_asset"
                . " INNER JOIN ca_transaction ON ca_transaction.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id"
                . "     AND ca_transaction.ca_ifrs_cat_id = {$caIfrsCategoryId}"
                . " INNER JOIN vw_ca_current_fin_year ON"
                . " vw_ca_current_fin_year.ca_fin_year_id = ca_transaction.ca_fin_year_id"
                . " WHERE rr_trans = '" . CommonConstant::DATABASE_VALUE_YES . "'"
                . " AND ca_transaction_type_id = " . CaTransactionType::REVALUATION_RESERVE_WRITE_OFF
                . $this->deminimisWhere()
                . $this->entityWhere()
                . " AND ca_fixed_asset.active = '" . CommonConstant::DATABASE_VALUE_YES . "'"
                . " ), 0), 0) AS `Amount` "
            )
        ]);
        return $query;
    }
    private function closingBalanceQuery($caIfrsCategoryId)
    {
        $query = \DB::table(null)->select([
            \DB::raw("'' AS `IFRS Category`"),
            \DB::raw("'Closing Balance' AS `Element`"),
            \DB::raw(
                "ROUND(IFNULL((SELECT SUM(cfwd_rr) FROM ca_fixed_asset"
                . " INNER JOIN ca_rr_balance ON ca_rr_balance.ca_fixed_asset_id = ca_fixed_asset.ca_fixed_asset_id"
                . " INNER JOIN vw_ca_current_fin_year ON"
                . " vw_ca_current_fin_year.ca_fin_year_id = ca_rr_balance.ca_fin_year_id"
                . " WHERE ca_fixed_asset.ca_ifrs_cat_id = {$caIfrsCategoryId}"
                . $this->deminimisWhere()
                . $this->entityWhere()
                . " AND ca_fixed_asset.active = '" . CommonConstant::DATABASE_VALUE_YES . "'"
                . " ), 0), 0) AS `Amount` "
            )
        ]);
        return $query;
    }
}
