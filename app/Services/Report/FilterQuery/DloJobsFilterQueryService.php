<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Models\DloJobOperative;
use Tfcloud\Models\DloJobStatusType;
use Tfcloud\Models\DloHourType;
use Tfcloud\Models\FinAuthStatus;
use Tfcloud\Models\GenTable;
use Tfcloud\Models\InstructionStatusType;
use Tfcloud\Models\DloSalesInvoiceStatusType;
use Tfcloud\Models\User;
use Tfcloud\Services\Admin\General\UserTradeLevelService;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\Dlo\DloSalesInvoiceService;
use Tfcloud\Services\Dlo\JobService;
use Tfcloud\Services\Dlo\JobLabourService;
use Tfcloud\Services\Dlo\JobMaterialItemService;
use Tfcloud\Services\Dlo\JobOperativeService;
use Tfcloud\Services\Dlo\JobStockItemService;
use Tfcloud\Services\Instruction\InstructionListService;
use Tfcloud\Services\Property\PlantService;
use Tfcloud\Services\UserDefinedService;
use Tfcloud\Services\PermissionService;

class DloJobsFilterQueryService extends BaseService
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    public function reAddDloLabourExceptionsQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $service = new JobLabourService($this->permissionService);
        $query = $service->filterDlo01Report($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['operative_user_id'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, \Form::fieldLang('operative_user_id')]);
        }

        if ($val = array_get($filterData, 'dateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.date_from'), $val));
        }

        if ($val = array_get($filterData, 'dateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.date_to'), $val));
        }

        if ($val = array_get($filterData, 'startDateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.start_date_from'), $val));
        }

        if ($val = array_get($filterData, 'startDateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.start_date_to'), $val));
        }

        if ($val = array_get($filterData, 'finishDateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.finish_date_from'), $val));
        }

        if ($val = array_get($filterData, 'finishDateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.finish_date_to'), $val));
        }

        return $query;
    }


    public function reAddDloJobQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $job = new JobService(
            $this->permissionService,
            new UserDefinedService(),
            new PlantService($this->permissionService)
        );
        $query = $job->filterAll($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'dlo_job_code', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.dlo.job_code_contains'), $val));
        }

        if ($val = array_get($filterData, 'dlo_job_desc', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.description_contains'), $val));
        }

        if ($val = array_get($filterData, 'dlo_job_type', null)) {
            array_push($whereCodes, [
                'dlo_job_type',
                'dlo_job_type_id',
                'dlo_job_type_code',
                $val,
                \Form::fieldLang('dlo_job_type_id')
            ]);
        }

        if ($val = array_get($filterData, 'plant_group_id', null)) {
            array_push($whereCodes, [
                'plant_group',
                'plant_group_id',
                'plant_group_code',
                $val,
                \Form::fieldLang('plant_group_id')
            ]);
        }

        if ($val = array_get($filterData, 'plant_subgroup_id', null)) {
            array_push($whereCodes, [
                'plant_subgroup',
                'plant_subgroup_id',
                'plant_subgroup_code',
                $val,
                \Form::fieldLang('plant_subgroup_id')
            ]);
        }

        if ($val = array_get($filterData, 'plant_type_id', null)) {
            array_push($whereCodes, [
                'plant_type',
                'plant_type_id',
                'plant_type_code',
                $val,
                \Form::fieldLang('plant_type_id')
            ]);
        }

        if ($val = array_get($filterData, 'site_plant_system_id', null)) {
            array_push($whereCodes, [
                'site_plant_system',
                'site_plant_system_id',
                'site_plant_system_code',
                $val,
                \Form::fieldLang('site_plant_system_id')
            ]);
        }

        if ($val = array_get($filterData, 'plant_id', null)) {
            array_push($whereCodes, [
                'plant',
                'plant_id',
                'plant_code',
                $val,
                \Form::fieldLang('plant_id')
            ]);
        }

        if ($val = Common::iset($filterData['manager_user_id'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, \Form::fieldLang('manager_user_id')]);
        }
        if ($val = Common::iset($filterData['operative_user_id'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, \Form::fieldLang('operative_user_id')]);
        }

        if ($val = Common::iset($filterData['surveyor_user_id'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, \Form::fieldLang('surveyor_user_id')]);
        }

        if ($val = Common::iset($filterData['owner_user_id'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, \Form::fieldLang('owner_user_id')]);
        }

        if ($val = array_get($filterData, 'dlo_job_priority_id', null)) {
            array_push($whereCodes, [
                'dlo_job_priority',
                'dlo_job_priority_id',
                'dlo_job_priority_code',
                $val,
                \Form::fieldLang('dlo_job_priority_id')
            ]);
        }

        if ($val = array_get($filterData, 'dlo_job_status_id', null)) {
            array_push($whereCodes, [
                'dlo_job_status',
                'dlo_job_status_id',
                'dlo_job_status_desc',
                $val,
                \Form::fieldLang('dlo_job_status_id')
            ]);
        }

        if ($val = Common::iset($filterData['dloSalesInvStatus'])) {
            array_push($whereCodes, [
                'dlo_sales_invoice_status',
                'dlo_sales_invoice_status_id',
                'dlo_sales_invoice_status_code',
                $val,
                'Sales Invoice Status'
            ]);
        }

        if ($statusTypes = array_get($filterData, 'status_types', [])) {
            if (count($statusTypes)) {
                array_push($orCodes, [
                    'dlo_job_status_type',
                    'dlo_job_status_type_id',
                    'dlo_job_status_type_desc',
                    implode(',', $statusTypes),
                    \Lang::get('text.status_type')
                ]);
            }
        }

        // Special location
        if ($val = array_get($filterData, 'site_id')) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);

            if ($val = array_get($filterData, 'building_id')) {
                array_push($whereCodes, ['building', 'building_id', 'building_code', $val, "Building Code"]);

                if ($val = array_get($filterData, 'room_id')) {
                    array_push($whereCodes, ['room', 'room_id', 'room_number', $val, "Room Number"]);
                }
            }
        }

        if ($val = array_get($filterData, 'dlo_quoted_job')) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, "Quoted='Yes'");
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, "Quoted='No'");
                    break;
                default:
                    break;
            }
        }

        if ($val = array_get($filterData, 'targetDateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.dlo.target_date_from'), $val));
        }

        if ($val = array_get($filterData, 'targetDateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.dlo.target_date_to'), $val));
        }

        if ($val = array_get($filterData, 'createdDateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.dlo.created_date_from'), $val));
        }

        if ($val = array_get($filterData, 'createdDateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.dlo.created_date_to'), $val));
        }

        if ($val = array_get($filterData, 'actualCompleteDateFrom', null)) {
            array_push($whereTexts, 'Actual Complete Date From ' . $val);
        }

        if ($val = array_get($filterData, 'actualCompleteDateTo', null)) {
            array_push($whereTexts, 'Actual Complete Date To ' . $val);
        }

        if ($val = array_get($filterData, 'team_id', null)) {
            array_push($whereCodes, [
                'team',
                'team_id',
                'team_desc',
                $val,
                'Team'
            ]);
        }

        if ($val = array_get($filterData, 'jobCompletedOnTime', null)) {
            $text = 'Jobs Completed on Time=';
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, $text . "'Yes'");
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, $text . "'No'");
                    break;
            }
        }

        if ($val = array_get($filterData, 'jobCompletedOverEstimate', null)) {
            $text = 'Jobs Completed Over Estimate=';
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, $text . "'Yes'");
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, $text . "'No'");
                    break;
            }
        }

        if ($val = array_get($filterData, 'trade_code_id', null)) {
            array_push($whereCodes, [
                'trade_code',
                'trade_code_id',
                'trade_code_code',
                $val,
                \Form::fieldLang('trade_code_id')
            ]);
        }

        if ($val = array_get($filterData, 'trade_level_id', null)) {
            array_push($whereCodes, [
                'trade_level',
                'trade_level_id',
                'trade_level_no',
                $val,
                \Form::fieldLang('trade_level_id')
            ]);
        }


        if ($viewName == 'vw_dlo_cli_mk01') {
            $this->reAddUserDefinesQuery(GenTable::HELPCALL, $filterData, $whereCodes, $whereTexts);
        }

        $this->reAddUserDefinesQuery(GenTable::DLO_JOB, $filterData, $whereCodes, $whereTexts);

        return $query;
    }

    public function reAddDloJobLabourQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $service = new JobLabourService($this->permissionService);
        $query = $service->filterAll($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'dlo_job_code', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.dlo.job_code_contains'), $val));
        }

        if ($val = array_get($filterData, 'dlo_job_labour_code', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.dlo.labour_code_contains'), $val));
        }

        if ($val = array_get($filterData, 'dlo_job_desc', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.dlo.job_description_contains'), $val));
        }

        if ($val = array_get($filterData, 'dateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.date_from'), $val));
        }

        if ($val = array_get($filterData, 'dateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.date_to'), $val));
        }

        if ($val = array_get($filterData, 'startDateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.start_date_from'), $val));
        }

        if ($val = array_get($filterData, 'startDateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.start_date_to'), $val));
        }

        if ($val = array_get($filterData, 'finishDateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.finish_date_from'), $val));
        }

        if ($val = array_get($filterData, 'finishDateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.finish_date_to'), $val));
        }

        if ($val = array_get($filterData, 'dlo_job_type', null)) {
            array_push($whereCodes, [
                'dlo_job_type',
                'dlo_job_type_id',
                'dlo_job_type_code',
                $val,
                \Form::fieldLang('dlo_job_type_id')
            ]);
        }

        if ($val = array_get($filterData, 'plant_group_id', null)) {
            array_push($whereCodes, [
                'plant_group',
                'plant_group_id',
                'plant_group_code',
                $val,
                \Form::fieldLang('plant_group_id')
            ]);
        }

        if ($val = array_get($filterData, 'plant_subgroup_id', null)) {
            array_push($whereCodes, [
                'plant_subgroup',
                'plant_subgroup_id',
                'plant_subgroup_code',
                $val,
                \Form::fieldLang('plant_subgroup_id')
            ]);
        }

        if ($val = array_get($filterData, 'plant_type_id', null)) {
            array_push($whereCodes, [
                'plant_type',
                'plant_type_id',
                'plant_type_code',
                $val,
                \Form::fieldLang('plant_type_id')
            ]);
        }

        if ($val = array_get($filterData, 'site_plant_system_id', null)) {
            array_push($whereCodes, [
                'site_plant_system',
                'site_plant_system_id',
                'site_plant_system_code',
                $val,
                \Form::fieldLang('site_plant_system_id')
            ]);
        }

        if ($val = array_get($filterData, 'plant_id', null)) {
            array_push($whereCodes, [
                'plant',
                'plant_id',
                'plant_code',
                $val,
                \Form::fieldLang('plant_id')
            ]);
        }

        if ($val = array_get($filterData, 'manager_user_id', null)) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, \Form::fieldLang('manager_user_id')]);
        }
        if ($val = array_get($filterData, 'dlo_job_operative_id', null)) {
            $dloJobOperative = DloJobOperative::find($val);
            if ($dloJobOperative) {
                array_push(
                    $whereCodes,
                    ['user', 'id', 'display_name', $dloJobOperative->user_id, \Form::fieldLang('dlo_job_operative_id')]
                );
            }
        }

        if ($val = Common::iset($filterData['surveyor_user_id'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, \Form::fieldLang('surveyor_user_id')]);
        }

        if ($val = Common::iset($filterData['owner_user_id'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, \Form::fieldLang('owner_user_id')]);
        }

        if ($val = Common::iset($filterData['operative_user_id'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, \Form::fieldLang('operative_user_id')]);
        }

        if ($val = Common::iset($filterData['trade_code'])) {
            array_push($whereCodes, [
                'trade_code',
                'trade_code_id',
                'trade_code_code',
                $val,
                \Form::fieldLang('trade_code_id')
            ]);
        }

        if ($val = array_get($filterData, 'dlo_job_priority_id', null)) {
            array_push($whereCodes, [
                'dlo_job_priority',
                'dlo_job_priority_id',
                'dlo_job_priority_code',
                $val,
                \Form::fieldLang('dlo_job_priority_id')
            ]);
        }

        if ($val = array_get($filterData, 'dlo_job_status_id', null)) {
            array_push($whereCodes, [
                'dlo_job_status',
                'dlo_job_status_id',
                'dlo_job_status_desc',
                $val,
                \Form::fieldLang('dlo_job_status_id')
            ]);
        }

        $statusType = [];
        if (array_get($filterData, 'statusTypePlan', null)) {
            $statusType[] = DloJobStatusType::IN_PLANNING;
        }
        if (array_get($filterData, 'statusTypeOpen', null)) {
            $statusType[] = DloJobStatusType::OPEN;
        }
        if (array_get($filterData, 'statusTypeInProgress', null)) {
            $statusType[] = DloJobStatusType::SCHEDULED;
        }
        if (array_get($filterData, 'statusTypeWorksComp', null)) {
            $statusType[] = DloJobStatusType::WORKS_COMP;
        }
        if (array_get($filterData, 'statusTypeWorksExpComp', null)) {
            $statusType[] = DloJobStatusType::WORKS_EXP_COMP;
        }
        if (array_get($filterData, 'statusTypeCancelled', null)) {
            $statusType[] = DloJobStatusType::CANCELLED;
        }
        if (array_get($filterData, 'statusTypePosted', null)) {
            $statusType[] = DloJobStatusType::POSTED;
        }
        if (count($statusType)) {
            array_push($orCodes, [
                'dlo_job_status_type',
                'dlo_job_status_type_id',
                'dlo_job_status_type_desc',
                implode(',', $statusType),
                \Lang::get('text.status_type')
            ]);
        }

        // Special location
        if ($val = array_get($filterData, 'site_id')) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);

            if ($val = array_get($filterData, 'building_id')) {
                array_push($whereCodes, ['building', 'building_id', 'building_code', $val, "Building Code"]);

                if ($val = array_get($filterData, 'room_id')) {
                    array_push($whereCodes, ['room', 'room_id', 'room_number', $val, "Room Number"]);
                }
            }
        }

        return $query;
    }

    public function reAddDloJobOperativeQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $jobService = new JobService(
            $this->permissionService,
            new UserDefinedService(),
            new PlantService($this->permissionService)
        );
        $service = new JobOperativeService($this->permissionService, $jobService);
        $query = $service->filterAll($filterData, $viewName, $query);

        if ($val = array_get($filterData, 'dlo_job_code', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.dlo.job_code_contains'), $val));
        }

        if ($val = array_get($filterData, 'operative_user_id', null)) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, \Form::fieldLang('operative_user_id')]);
        }

        if ($val = array_get($filterData, 'trade_code', null)) {
            array_push($whereCodes, [
                'trade_code',
                'trade_code_id',
                'trade_code_desc',
                $val,
                \Form::fieldLang('trade_code_id')
            ]);
        }

        if ($val = array_get($filterData, 'dlo_job_operative_code', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.dlo.operative_code_contains'), $val));
        }

        if ($val = array_get($filterData, 'startDateFrom', array_get($filterData, 'startDateFrom', null))) {
            array_push($whereTexts, \Form::fieldLang('start_date_from') . " $val");
        }

        if ($val = array_get($filterData, 'startDateTo', array_get($filterData, 'startDateTo', null))) {
            array_push($whereTexts, \Form::fieldLang('start_date_to') . " $val");
        }

        return $query;
    }

    public function reAddDloJobLabourHoursQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $service = new JobLabourService($this->permissionService);
        $query = $service->filterAll($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'dlo_job_labour_code', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.dlo.operative_code_contains'), $val));
        }

        if ($val = Common::iset($filterData['operative_user_id'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, \Form::fieldLang('operative_user_id')]);
        }

        if ($val = array_get($filterData, 'dlo_job_operative_id', null)) {
            $dloJobOperative = DloJobOperative::find($val);
            if ($dloJobOperative) {
                array_push(
                    $whereCodes,
                    ['user', 'id', 'display_name', $dloJobOperative->user_id, \Form::fieldLang('dlo_job_operative_id')]
                );
            }
        }

        if ($val = array_get($filterData, 'trade_code', null)) {
            array_push($whereCodes, [
                'trade_code',
                'trade_code_id',
                'trade_code_desc',
                $val,
                \Form::fieldLang('trade_code_id')
            ]);
        }

        if ($val = array_get($filterData, 'dateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.date_from'), $val));
        }

        if ($val = array_get($filterData, 'dateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.date_to'), $val));
        }

        if ($val = array_get($filterData, 'startDateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.start_date_from'), $val));
        }

        if ($val = array_get($filterData, 'startDateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.start_date_to'), $val));
        }

        if ($val = array_get($filterData, 'finishDateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.finish_date_from'), $val));
        }

        if ($val = array_get($filterData, 'finishDateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.finish_date_to'), $val));
        }

        if ($val = array_get($filterData, 'weekendOnly', array_get($filterData, 'weekendOnly', null))) {
            array_push($whereTexts, 'Weekends Only');
        }

        if ($val = array_get($filterData, 'dlo_job_code', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.dlo.job_code_contains'), $val));
        }

        //if ($val = array_get($filterData, 'dlo_job_id', null)) {
        //    array_push($whereTexts, sprintf(\Lang::get('text.report_texts.dlo.job_code_contains'), $val));
        //}

        return $query;
    }

    public function reAddDloJobPoWoQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $service = new InstructionListService($this->permissionService);
        $query = $service->filterForReportDLO09($query, $filterData);

        if ($val = array_get($filterData, 'code', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.code_contains'), $val));
        }

        if ($val = array_get($filterData, 'desc', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.description_contains'), $val));
        }

        if ($val = Common::iset($filterData['supplier'])) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, \Lang::get('text.supplier')]);
        }

        if ($val = array_get($filterData, 'authoriser', null)) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, \Lang::get('text.authoriser')]);
        }

        if ($val = array_get($filterData, 'finYear', null)) {
            array_push($whereCodes, [
                'fin_year',
                'fin_year_id',
                'fin_year_code',
                $val,
                \Lang::get('text.financialYear')
            ]);
        }

        if ($val = array_get($filterData, 'account', null)) {
            array_push($whereCodes, [
                'fin_account',
                'fin_account_id',
                'fin_account_code',
                $val,
                \Lang::get('text.account')
            ]);
        }

        $statusType = [];
        if (Common::iset($filterData['generated'])) {
            $statusType[] = InstructionStatusType::GENERATED;
        }
        if (Common::iset($filterData['printed'])) {
            $statusType[] = InstructionStatusType::PRINTED;
        }
        if (Common::iset($filterData['complete'])) {
            $statusType[] = InstructionStatusType::COMPLETE;
        }
        if (Common::iset($filterData['closed'])) {
            $statusType[] = InstructionStatusType::CLOSED;
        }
        if (Common::iset($filterData['cancelled'])) {
            $statusType[] = InstructionStatusType::CANCELLED;
        }
        if (count($statusType)) {
            array_push($orCodes, [
                    'instruction_status_type',
                    'instruction_status_type_id',
                    'instruction_status_type_code',
                    implode(',', $statusType),
                    \Lang::get('text.status_type')
            ]);
        }

        if ($val = Common::iset($filterData['status'])) {
            array_push($whereCodes, [
                'instruction_status',
                'instruction_status_id',
                'instruction_status_code',
                $val,
                'Status'
            ]);
        }

        $authorisation = [];
        if (Common::iset($filterData['open'])) {
            $authorisation[] = FinAuthStatus::OPEN;
        }
        if (Common::iset($filterData['approval'])) {
            $authorisation[] = FinAuthStatus::APPROVAL;
        }
        if (Common::iset($filterData['approved'])) {
            $authorisation[] = FinAuthStatus::APPROVED;
        }
        if (Common::iset($filterData['rejected'])) {
            $authorisation[] = FinAuthStatus::REJECTED;
        }

        if (count($authorisation)) {
            array_push($orCodes, [
                    'fin_auth_status',
                    'fin_auth_status_id',
                    'name',
                    implode(',', $authorisation),
                    \Lang::get('text.authorisation')
            ]);
        }

        //if ($val = array_get($filterData, 'dlo_job_id', null)) {
        //    array_push($whereTexts, sprintf(\Lang::get('text.report_texts.dlo.job_code_contains'), $val));
        //}

        return $query;
    }

    public function reAddDloSalesInvoiceQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {

        $service = new DloSalesInvoiceService($this->permissionService);
        $query = $service->filterDloSalesInvoiceReports($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'dlo_job_code', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.dlo.job_code_contains'), $val));
        }

        if ($val = array_get($filterData, 'dlo_sales_invoice_code', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.dlo.sales_inv_code_contains'), $val));
        }

        if ($val = array_get($filterData, 'dlo_sales_invoice_desc', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.description_contains'), $val));
        }

        if ($val = Common::iset($filterData['created_user_id'])) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, \Form::fieldLang('created_by')]);
        }

        if ($val = array_get($filterData, 'final')) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, "Final='Yes'");
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, "Final='No'");
                    break;
                default:
                    break;
            }
        }

        if ($val = array_get($filterData, 'credit', array_get($filterData, 'credit', null))) {
            array_push($whereTexts, 'Credit = Credit');
        }

        $statusType = [];
        if (Common::iset($filterData['statusTypeDraft'])) {
            $statusType[] = DloSalesInvoiceStatusType::DRAFT;
        }
        if (Common::iset($filterData['statusTypeApproved'])) {
            $statusType[] = DloSalesInvoiceStatusType::APPROVED;
        }
        if (Common::iset($filterData['statusTypePosted'])) {
            $statusType[] = DloSalesInvoiceStatusType::POSTED;
        }
        if (Common::iset($filterData['statusTypePaid'])) {
            $statusType[] = DloSalesInvoiceStatusType::PAID;
        }
        if (Common::iset($filterData['statusTypeCancelled'])) {
            $statusType[] = DloSalesInvoiceStatusType::CANCELLED;
        }

        if (count($statusType)) {
            array_push($orCodes, [
                    'dlo_sales_invoice_status_type',
                    'dlo_sales_invoice_status_type_id',
                    'dlo_sales_invoice_status_type_code',
                    implode(',', $statusType),
                    \Lang::get('text.status_type')
            ]);
        }

        if ($val = Common::iset($filterData['dlo_sales_invoice_status_id'])) {
            array_push($whereCodes, [
                'dlo_sales_invoice_status',
                'dlo_sales_invoice_status_id',
                'dlo_sales_invoice_status_desc',
                $val,
                'Status'
            ]);
        }

        if ($val = array_get($filterData, 'dateFrom', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.date_from'), $val));
        }

        if ($val = array_get($filterData, 'dateTo', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.date_to'), $val));
        }

        if ($val = array_get($filterData, 'account', null)) {
            array_push($whereCodes, [
                'fin_account',
                'fin_account_id',
                'fin_account_code',
                $val,
                \Lang::get('text.account')
            ]);
        }

        // Location
        if ($val = array_get($filterData, 'site_id')) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);

            if ($val = array_get($filterData, 'building_id')) {
                array_push($whereCodes, ['building', 'building_id', 'building_code', $val, "Building Code"]);

                if ($val = array_get($filterData, 'room_id')) {
                    array_push($whereCodes, ['room', 'room_id', 'room_number', $val, "Room Number"]);
                }
            }
        }

        //if ($val = array_get($filterData, 'dlo_job_id', null)) {
        //    array_push($whereTexts, sprintf(\Lang::get('text.report_texts.dlo.job_code_contains'), $val));
        //}

        return $query;
    }

    public function reAddDloMaterialItemQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $service = new JobMaterialItemService($this->permissionService);
        $query = $service->filterAll($filterData, $query, $viewName);

        if ($val = array_get($filterData, 'dlo_job_code', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.dlo.job_code_contains'), $val));
        }

        if ($val = array_get($filterData, 'dlo_job_material_code', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.dlo.dlo_job_material_code'), $val));
        }

        if ($val = array_get($filterData, 'dlo_job_material_desc', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.dlo.dlo_job_material_desc'), $val));
        }

        if ($val = Common::iset($filterData['plant_part_id'])) {
            array_push($whereCodes, [
                'plant_part',
                'plant_part_id',
                'plant_part_desc',
                $val,
                'Plant Part'
            ]);
        }

        if ($val = Common::iset($filterData['plant_part_store_id'])) {
            array_push($whereCodes, [
                'plant_part_store',
                'plant_part_store_id',
                'plant_part_store_desc',
                $val,
                'Store'
            ]);
        }

        //if ($val = array_get($filterData, 'dlo_job_id', null)) {
        //    array_push($whereTexts, sprintf(\Lang::get('text.report_texts.dlo.job_code_contains'), $val));
        //}

        return $query;
    }

    public function reAddDloStockMaterialItemQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $service = new JobStockItemService($this->permissionService);
        $query = $service->filterAll($filterData, $query, $viewName);

        if ($val = array_get($filterData, 'dlo_job_id')) {
            array_push($whereCodes, ['dlo_job', 'dlo_job_id', 'dlo_job_code', $val, 'DLO Job']);
        }

        if ($val = array_get($filterData, 'dlo_stock_material_code', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.dlo.dlo_stock_material_code'), $val));
        }

        if ($val = array_get($filterData, 'dlo_stock_material_desc', null)) {
            array_push($whereTexts, sprintf(\Lang::get('text.report_texts.dlo.dlo_stock_material_desc'), $val));
        }

        if ($val = array_get($filterData, 'from_stock')) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, "From Stock='Yes'");
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, "From Stock='No'");
                    break;
                default:
                    break;
            }
        }

//        if ($val = Common::iset($filterData['plant_part_id'])) {
//            array_push($whereCodes, [
//                'plant_part',
//                'plant_part_id',
//                'plant_part_desc',
//                $val,
//                'Plant Part'
//            ]);
//        }
//
//        if ($val = Common::iset($filterData['plant_part_store_id'])) {
//            array_push($whereCodes, [
//                'plant_part_store',
//                'plant_part_store_id',
//                'plant_part_store_desc',
//                $val,
//                'Store'
//            ]);
//        }

        return $query;
    }

    public function reAddDloOperativeTradeLevelQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $service = new UserTradeLevelService($this->permissionService);
        $query = $service->filterAll($filterData, $query, $viewName);

        if ($val = array_get($filterData, 'team_id')) {
            array_push($whereCodes, ['team', 'team_id', 'team_code', $val, 'Team']);
        }

        if ($val = array_get($filterData, 'dlo_operative_user_id')) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Operative']);
        }

        if ($val = array_get($filterData, 'trade_code_id')) {
            array_push($whereCodes, ['trade_code', 'trade_code_id', 'trade_code_code', $val, 'Trade']);
        }

        if ($val = array_get($filterData, 'trade_level_id')) {
            array_push($whereCodes, ['trade_level', 'trade_level_id', 'trade_level_no', $val, 'Level']);
        }

        if ($val = array_get($filterData, 'expiryDateFrom', null)) {
            array_push($whereTexts, "Expiry Date From {$val}");
        }

        if ($val = array_get($filterData, 'expiryDateTo', null)) {
            array_push($whereTexts, "Expiry Date To {$val}");
        }

        if ($val = array_get($filterData, 'trade_active')) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, "Trade Active='Yes'");
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, "Trade Active='No'");
                    break;
                default:
                    break;
            }
        }

        if ($val = array_get($filterData, 'level_active')) {
            switch ($val) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, "Level Active='Yes'");
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, "Level Active='No'");
                    break;
                default:
                    break;
            }
        }

        return $query;
    }
}
