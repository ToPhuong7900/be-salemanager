<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\Common;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\PermitToWork\PermitToWorkService;
use Tfcloud\Services\UserDefinedService;

class PermitToWorkFilterQueryService extends BaseService
{
    protected $permissionService;
    protected $userDefinedService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
        $this->userDefinedService = new UserDefinedService();
    }

    public function reAddPermitToWorkQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $service = new PermitToWorkService($this->permissionService);
        $newQuery = $service->filterAll($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'code')) {
            array_push($whereTexts, sprintf("Code contains '%s'", $val));
        }

        if ($val = array_get($filterData, 'validFromDateFrom')) {
            array_push($whereTexts, "Valid From From $val");
        }
        if ($val = array_get($filterData, 'validFromDateTo')) {
            array_push($whereTexts, "Valid From To $val");
        }

        if ($val = array_get($filterData, 'validToDateFrom')) {
            array_push($whereTexts, "Valid To From $val");
        }
        if ($val = array_get($filterData, 'validToDateTo')) {
            array_push($whereTexts, "Valid To To $val");
        }

        if ($val = array_get($filterData, 'status')) {
            array_push(
                $whereCodes,
                ['ptw_status', 'ptw_status_id', 'ptw_status_desc', $val, "Status"]
            );
        }

        if ($val = array_get($filterData, 'type')) {
            array_push(
                $whereCodes,
                ['ptw_type', 'ptw_type_id', 'ptw_type_desc', $val, "Type"]
            );
        }

        if ($val = array_get($filterData, 'requester')) {
            array_push($whereCodes, ['user', 'id', 'display_name', $val, 'Requester']);
        }

        if ($val = array_get($filterData, 'supplier')) {
            array_push($whereCodes, ['contact', 'contact_id', 'contact_name', $val, 'Supplier']);
        }


        if ($val = Common::iset($filterData['site_id'])) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, 'Site Code']);
        }

        if ($val = array_get($filterData, 'building_id')) {
            array_push($whereCodes, ['building', 'building_id', 'building_code', $val, 'Building Code']);
        }

        if ($val = array_get($filterData, 'room_id')) {
            array_push(
                $whereCodes,
                ['room', 'room_id', 'room_number', $val, 'Room Number']
            );
        }

        if ($val = array_get($filterData, 'plant_group_id')) {
            array_push($whereCodes, ['plant_group', 'plant_group_id', 'plant_group_code', $val, "Plant Group"]);

            if ($val = array_get($filterData, 'plant_subgroup_id')) {
                array_push(
                    $whereCodes,
                    ['plant_subgroup', 'plant_subgroup_id', 'plant_subgroup_code', $val, "Plant Sub Group"]
                );

                if ($val = array_get($filterData, 'plant_type_id')) {
                    array_push(
                        $whereCodes,
                        ['plant_type', 'plant_type_id', 'plant_type_code', $val, "Plant Type"]
                    );
                }
            }
        }

        if ($val = array_get($filterData, 'plant_code')) {
            array_push($whereTexts, sprintf("Plant Code contains '%s'", $val));
        }

        return $newQuery;
    }
}
