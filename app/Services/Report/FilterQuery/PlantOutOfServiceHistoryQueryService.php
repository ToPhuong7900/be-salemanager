<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Models\PlantOutOfServiceHistory;
use Tfcloud\Models\PlantOutOfServiceRecordType;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Property\PlantOutOfServiceHistoryService;
use Tfcloud\Services\UserDefinedService;

class PlantOutOfServiceHistoryQueryService extends BaseService
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
        $this->userDefinedService = new UserDefinedService();
    }

    public function reAddPlantOOSHistoryRecordsQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $query = (new PlantOutOfServiceHistoryService($this->permissionService))
            ->filterAll($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'dateFrom', null)) {
            array_push($whereTexts, "Date From = '" . " $val" . "'");
        }

        if ($val = array_get($filterData, 'dateTo', null)) {
            array_push($whereTexts, "Date To = '" . " $val" . "'");
        }

        if ($val = array_get($filterData, 'plant_group_id')) {
            array_push($whereCodes, ['plant_group', 'plant_group_id', 'plant_group_code', $val, "Plant Group"]);

            if ($val = array_get($filterData, 'plant_subgroup_id')) {
                array_push(
                    $whereCodes,
                    ['plant_subgroup', 'plant_subgroup_id', 'plant_subgroup_code', $val, "Plant Sub Group"]
                );

                if ($val = array_get($filterData, 'plant_type_id')) {
                    array_push(
                        $whereCodes,
                        ['plant_type', 'plant_type_id', 'plant_type_code', $val, "Plant Type"]
                    );
                }
            }
        }

        if ($val = array_get($filterData, 'plant_code')) {
            array_push($whereTexts, "Plant Code contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'owner')) {
            array_push(
                $whereCodes,
                ['user', 'id', 'display_name', $val, "Owner"]
            );
        }

        if ($val = array_get($filterData, 'logged_by_user_id')) {
            array_push(
                $whereCodes,
                ['user', 'id', 'display_name', $val, "Logged By"]
            );
        }

        if ($val = array_get($filterData, 'plant_id')) {
            array_push(
                $whereCodes,
                ['plant', 'plant_id', 'plant_code', $val, "Plant Code"]
            );
        }

        if (array_get($filterData, 'parent_type_id') != '') {
            $val = array_get($filterData, 'parent_type_id');
            if ($val == PlantOutOfServiceRecordType::MANUAL) {
                array_push($whereTexts, "Source = 'Manual'");
            } else {
                array_push(
                    $whereCodes,
                    ['plant_oos_record_type', 'plant_oos_record_type_id', 'plant_oos_record_type_desc', $val, "Source"]
                );
            }
        }

        if ($val = array_get($filterData, 'parent_code')) {
            array_push($whereTexts, "Source Code contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'plant_oos_status')) {
            switch ($val) {
                case PlantOutOfServiceHistory::STATUS_OPEN:
                    array_push($whereTexts, "Status = 'Open'");
                    break;
                case 'All':
                    array_push($whereTexts, "Status = 'All'");
                    break;
                case PlantOutOfServiceHistory::STATUS_CLOSED:
                default:
                    array_push($whereTexts, "Status = 'Closed'");
                    break;
            }
        }

        return $query;
    }

    public static function pl03ReplaceColumn($reportColumns)
    {
        $explodeColumn = explode(',', $reportColumns);
        $reasonKey = array_search('vw_pl03.`Reason`', $explodeColumn);
        $explodeColumn[$reasonKey] = PlantOutOfServiceHistoryQueryService::pl03ReasonBuild();
        $reportColumnsReplace = implode(',', $explodeColumn);

        return $reportColumnsReplace;
    }

    public static function pl03ReasonBuild()
    {
        $parentTypeIds = [
            PlantOutOfServiceRecordType::HELPCALL
        ];

        $userModulesHasReadLevel = \Auth::user()->getModuleAtLeastReadIds();
        $oosRecordTypeIds = PlantOutOfServiceRecordType::all()->pluck('module_id', 'plant_oos_record_type_id');

        $query = "CASE parent_type_id WHEN ";
        foreach ($parentTypeIds as $parentTypeId) {
            $hasPermission = in_array($oosRecordTypeIds[$parentTypeId], $userModulesHasReadLevel) ? '0 = 0' : '1 = 0';
            $query .= "$parentTypeId THEN ";
            switch ($parentTypeId) {
                case PlantOutOfServiceRecordType::HELPCALL:
                    $contractor = \Auth::User()->isContractor() ? '0 = 0' : '1 = 0';
                    $userId = \Auth::User()->getKey();

                    $query .= "IF(!($hasPermission) OR (($contractor) AND parent_logged_by != $userId), " .
                        "CONCAT_WS(' - ', `plant_oos_record_type_desc`, `parent_code`), " .
                        "CONCAT_WS(' - ', `parent_code`, `parent_desc`))";
                    break;
                default:
                    $query .= "IF(!($hasPermission)), " .
                        "CONCAT_WS(' - ', `plant_oos_record_type_desc`, `parent_code`), " .
                        "CONCAT_WS(' - ', `parent_code`, `parent_desc`))";
                    break;
            }
        }
        $query .= " ELSE reason END AS Reason";

        return $query;
    }
}
