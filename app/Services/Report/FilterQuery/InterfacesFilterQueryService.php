<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Lib\Common;
use Tfcloud\Models\FinAuthStatus;
use Tfcloud\Models\InstructionStatusType;
use Tfcloud\Models\InterfaceStatus;
use Tfcloud\Services\Admin\Interfaces\InterfaceLogService;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\PermissionService;

class InterfacesFilterQueryService extends BaseService
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }


    public function reAddInterfacesQuery(
        $filterData,
        $viewName,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        $listQuery = (new InterfaceLogService($this->permissionService))->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['interface_id'])) {
            array_push(
                $whereCodes,
                ['interface', 'interface_id', 'interface_code', $val, 'Interface']
            );
        }

        if ($val = array_get($filterData, 'executionFrom', null)) {
            array_push($whereTexts, 'Execution From ' . " $val" . "'");
        }

        if ($val = array_get($filterData, 'executionTo', null)) {
            array_push($whereTexts, "Execution To '" . " $val" . "'");
        }

        $status = [];
        if (Common::iset($filterData['statusSuccess'])) {
            $status[] = InterfaceStatus::SUCCESS;
        }
        if (Common::iset($filterData['statusPartial'])) {
            $status[] = InterfaceStatus::PARTIAL;
        }
        if (Common::iset($filterData['statusFail'])) {
            $status[] = InterfaceStatus::FAIL;
        }
        if (Common::iset($filterData['statusProcessing'])) {
            $status[] = InterfaceStatus::PROCESSING;
        }
        if (Common::iset($filterData['statusSuccNoData'])) {
            $status[] = InterfaceStatus::SUCC_NO_DATA;
        }
        if (count($status)) {
            array_push($orCodes, [
                    'interface_status',
                    'interface_status_id',
                    'interface_status_code',
                    implode(',', $status),
                    \Lang::get('text.status')
            ]);
        }

        return $listQuery;
    }
}
