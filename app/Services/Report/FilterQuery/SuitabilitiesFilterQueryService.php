<?php

namespace Tfcloud\Services\Report\FilterQuery;

use Tfcloud\Services\BaseService;
use Tfcloud\Services\Education\SuitabilityService;
use Tfcloud\Services\PermissionService;

class SuitabilitiesFilterQueryService extends BaseService
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    public function reAddSuitabilityQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $suitQuery = SuitabilityService::filterAll($query, $filterData, $viewName);

        if ($val = array_get($filterData, 'site_id')) {
            array_push($whereCodes, ['site', 'site_id', 'site_code', $val, "Site Code"]);
        }

        if ($val = array_get($filterData, 'code')) {
            array_push($whereTexts, "Code contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'description')) {
            array_push($whereTexts, "Description contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'status')) {
            array_push(
                $orCodes,
                ['suitability_status', 'suitability_status_id', 'suitability_status_code', $val, 'Suitability Status']
            );
        }

        return $suitQuery;
    }
}
