<?php

namespace Tfcloud\Services\Report;

use Illuminate\Support\Facades\Auth;
use Tfcloud\Lib\Constant\ReportConstant;
use Tfcloud\Lib\Exceptions\PermissionException;
use Tfcloud\Lib\Exceptions\StateException;
use Tfcloud\Models\AuditAction;
use Tfcloud\Models\CrudAccessLevel;
use Tfcloud\Models\Module;
use Tfcloud\Models\Report;
use Tfcloud\Services\AuditService;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Report\WkHtmlToPdf\AsbestosReportForBradfordService;
use Tfcloud\Services\Report\WkHtmlToPdf\AsbestosReportForCBService;
use Tfcloud\Services\Report\WkHtmlToPdf\AsbestosReportForFifeService;
use Tfcloud\Services\Report\WkHtmlToPdf\AsbestosReportForHertsService;
use Tfcloud\Services\Report\WkHtmlToPdf\AsbestosReportForNELService;
use Tfcloud\Services\Report\WkHtmlToPdf\AccountStatementReportService;
use Tfcloud\Services\Report\WkHtmlToPdf\CM03Casemgmt2ProgressService;
use Tfcloud\Services\Report\WkHtmlToPdf\CM03Casemgmt3ProgressService;
use Tfcloud\Services\Report\WkHtmlToPdf\CM03Casemgmt4ProgressService;
use Tfcloud\Services\Report\WkHtmlToPdf\CM03Casemgmt5ProgressService;
use Tfcloud\Services\Report\WkHtmlToPdf\CM03CaseProgressService;
use Tfcloud\Services\Report\WkHtmlToPdf\CNDCLITORB02BuildingConditionSurveyService;
use Tfcloud\Services\Report\WkHtmlToPdf\CNDNELincsSurveyBuildingService;
use Tfcloud\Services\Report\WkHtmlToPdf\ConditionReportForFifeService;
use Tfcloud\Services\Report\WkHtmlToPdf\IdWorkFMRReportService;
use Tfcloud\Services\Report\WkHtmlToPdf\LBHFHousing\QSTCLILBHFHousing01ReportService;
use Tfcloud\Services\Report\WkHtmlToPdf\LeaseOutCharge12MonthForecastReportService;
use Tfcloud\Services\Report\WkHtmlToPdf\LeaseOutCharge5YearForecastReportService;
use Tfcloud\Services\Report\WkHtmlToPdf\LeaseOutSummaryReportService;
use Tfcloud\Services\Report\WkHtmlToPdf\Modules\Personnel\PERS01ReportService;
use Tfcloud\Services\Report\WkHtmlToPdf\Modules\Personnel\PERS02ReportService;
use Tfcloud\Services\Report\WkHtmlToPdf\PL02AssetHistoryReportService;
use Tfcloud\services\Report\WkHtmlToPdf\PRJ17RiskRegisterReportService;
use Tfcloud\Services\Report\WkHtmlToPdf\ProjectActionsWithNotesReportService;
use Tfcloud\Services\Report\WkHtmlToPdf\ProjectHighLightService;
use Tfcloud\Services\Report\WkHtmlToPdf\ProjectIssuesWithNotesReportService;
use Tfcloud\Services\Report\WkHtmlToPdf\ProjectMonitoringReportService;
use Tfcloud\Services\Report\WkHtmlToPdf\PRCLICEC01PropertyReportService;
use Tfcloud\services\Report\WkHtmlToPdf\Ptw02ReportService;
use Tfcloud\Services\Report\WkHtmlToPdf\RSK03RiskAssessmentSummaryReportService;
use Tfcloud\Services\Report\WkHtmlToPdf\ScSummaryReportService;
use Tfcloud\Services\Report\WkHtmlToPdf\SiteAsbestosReportGwyneddService;
use Tfcloud\Services\Report\WkHtmlToPdf\Sefton\ESCLISEF01ReportService;
use Tfcloud\Services\Report\WkHtmlToPdf\Sefton\SiteAsbestosActionReportService;
use Tfcloud\Services\Report\WkHtmlToPdf\Sefton\SiteAsbestosRegisterReportService;
use Tfcloud\Services\Report\WkHtmlToPdf\Wokingham\CNDCLIWOK01CondSurveyReportService;
use Tfcloud\Services\Report\WkHtmlToPdf\ScottishBorders\CNDCLISB01ReportService;
use Tfcloud\Services\Report\WkHtmlToPdf\MiltonKeynes\CNDCLIMK01ReportService;

class WkReportService extends BaseService
{
    private $permissionService;
    private $reportService;

    public function __construct(PermissionService $permissionService, ReportService $reportService)
    {
        $this->permissionService = $permissionService;
        $this->reportService = $reportService;
    }

    public function generateReportData($inputs)
    {
        if (
            !$this->permissionService->hasModuleAccess(
                Module::MODULE_REPORTS,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
            )
        ) {
            $msg = "Not met minimum readonly access for report module";
            throw new PermissionException($msg);
        }

        $repId          = array_get($inputs, 'repId', null);
        $repType        = array_get($inputs, 'repType', null);

        $report = $this->reportService->get($repType, $repId);
        $returnData = [
            'result' => false,
            'repFile' => '',
            'repCode' => $report->getRepCode(),
            'repTitle' => $report->getRepTitle(),
            'repFormat' => ReportConstant::REPORT_OUTPUT_PDF_FORMAT,
            'repFileName' => '',
            'repFilePath' => '',
        ];
        if (!$report instanceof Report) {
            return $returnData;
        }

        if ($repDownloadPath = array_get($inputs, 'repDownloadPath')) {
            $reportDir = $repDownloadPath;
            if (!is_dir($reportDir)) {
                return $returnData;
            }
        } else {
            // Create a reports directory for the site group
            $reportDir = storage_path()
                . DIRECTORY_SEPARATOR . 'reports'
                . DIRECTORY_SEPARATOR . Auth::User()->site_group_id;
            if (!is_dir($reportDir)) {
                mkdir($reportDir, 0755);
            }
            // Create a reports directory for the userid
            $reportDir .= DIRECTORY_SEPARATOR . Auth::User()->id;
            if (!is_dir($reportDir)) {
                mkdir($reportDir, 0755);
            }
        }

        $reportFileName = $this->setCustomFileName($inputs, $report);

        $repPdfFile = $reportDir . DIRECTORY_SEPARATOR . $reportFileName . '.pdf';

        $returnData['repFile'] = url("report/reports/{$reportFileName}/download");
        $returnData['repFileName'] = $reportFileName . '.pdf';
        $returnData['repFilePath'] = $repPdfFile;

        if (!\Tfcloud\Lib\Common::invokedFromCommandLine()) {
            set_time_limit(1200);
        }
        switch ($report->getRepCode()) {
            case ReportConstant::SYSTEM_REPORT_ASB_CLI_BRA01:
                $asbestosReportForBradfordService = new AsbestosReportForBradfordService(
                    $this->permissionService,
                    $report
                );
                $this->runReportWithValidate(
                    $repPdfFile,
                    $inputs,
                    $repId,
                    $asbestosReportForBradfordService,
                    $returnData
                );
                $filterText = $asbestosReportForBradfordService->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_ASB_CLI_FIF01:
                $asbestosReportForFifeService = new AsbestosReportForFifeService($this->permissionService, $report);
                $returnData['result'] = $asbestosReportForFifeService->runReport($repPdfFile, $inputs, $repId);
                $filterText = $asbestosReportForFifeService->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_ASB_CLI_CEC01:
                $reportService = new WkHtmlToPdf\AsbestosReportForEdinburghService($this->permissionService);
                $returnData['result'] = $reportService->runReport($repPdfFile, $inputs, $repId);
                $filterText = $reportService->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_ASB_CLI_HRT01:
                $asbestosReportForHertsService = new AsbestosReportForHertsService($this->permissionService, $report);
                $this->runReportWithValidate($repPdfFile, $inputs, $repId, $asbestosReportForHertsService, $returnData);
                $filterText = $asbestosReportForHertsService->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_ASB_CLI_CB01:
                $asbestosReportForCBService = new AsbestosReportForCBService($this->permissionService, $report);
                $this->runReportWithValidate($repPdfFile, $inputs, $repId, $asbestosReportForCBService, $returnData);
                $filterText = $asbestosReportForCBService->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_ASB_CLI_GWN01:
                $asbestosReportForGwyneddService = new SiteAsbestosReportGwyneddService(
                    $this->permissionService,
                    $report
                );
                $this->runReportWithValidate(
                    $repPdfFile,
                    $inputs,
                    $repId,
                    $asbestosReportForGwyneddService,
                    $returnData
                );
                $filterText = $asbestosReportForGwyneddService->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_ASB_CLI_SEF01:
                $asbestosRegisterReportForSeftonService =
                    new SiteAsbestosRegisterReportService($this->permissionService, $report);
                $this->runReportWithValidate(
                    $repPdfFile,
                    $inputs,
                    $repId,
                    $asbestosRegisterReportForSeftonService,
                    $returnData
                );
                $filterText = $asbestosRegisterReportForSeftonService->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_ASB_CLI_SEF02:
                $asbestosActionReportForSeftonService =
                    new SiteAsbestosActionReportService($this->permissionService, $report);
                $this->runReportWithValidate(
                    $repPdfFile,
                    $inputs,
                    $repId,
                    $asbestosActionReportForSeftonService,
                    $returnData
                );
                $filterText = $asbestosActionReportForSeftonService->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_ASB_CLI_NEL01:
                $asbestosReportForNELService = new AsbestosReportForNELService($this->permissionService, $report);
                $this->runReportWithValidate($repPdfFile, $inputs, $repId, $asbestosReportForNELService, $returnData);
                $filterText = $asbestosReportForNELService->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_ES33:
                $statementOfAccountForLeaseService =
                    new AccountStatementReportService($this->permissionService, $report);
                $returnData['result'] = $statementOfAccountForLeaseService->runReport($repPdfFile, $inputs, $repId);
                $filterText = $statementOfAccountForLeaseService->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_ES34:
                $reportService = new WkHtmlToPdf\ES34AgedDebtReportService($this->permissionService, $report);
                $exportType = array_get($inputs, 'exportType');
                switch ($exportType) {
                    case ReportConstant::REPORT_OUTPUT_CSV_FORMAT:
                        $repPdfFile = $reportDir . DIRECTORY_SEPARATOR . $reportFileName . '.csv';
                        $returnData['repFileName'] = $reportFileName . '.csv';
                        $returnData['repFilePath'] = $repPdfFile;
                        $returnData['repFormat'] = ReportConstant::REPORT_OUTPUT_CSV_FORMAT;
                        break;
                    case ReportConstant::REPORT_OUTPUT_XLSX_FORMAT:
                        $repPdfFile = $reportDir . DIRECTORY_SEPARATOR . $reportFileName . '.xlsx';
                        $returnData['repFileName'] = $reportFileName . '.xlsx';
                        $returnData['repFilePath'] = $repPdfFile;
                        $returnData['repFormat'] = ReportConstant::REPORT_OUTPUT_XLSX_FORMAT;
                        break;
                    default:
                        break;
                }
                $returnData['result'] = $reportService->runReport($exportType, $repPdfFile, $inputs, $repId);
                $filterText = $reportService->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_ES_CLI_HRT01:
                $reportService = new WkHtmlToPdf\Hertfordshire\HRT01EstatesStatementReportService(
                    $this->permissionService,
                    $report
                );
                $this->runReportWithValidate(
                    $repPdfFile,
                    $inputs,
                    $repId,
                    $reportService,
                    $returnData
                );
                $filterText = $reportService->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_ES35:
                $reportService = new WkHtmlToPdf\ES35TenancyRentReportService($this->permissionService, $report);
                $returnData['result'] = $reportService->runReport($repPdfFile, $inputs, $repId);
                $filterText = $reportService->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_ES36:
                $reportService = new WkHtmlToPdf\ES36LeaseInSummaryReportService($this->permissionService, $report);
                $returnData['result'] = $reportService->runReport($repPdfFile, $inputs, $repId);
                $filterText = $reportService->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_CND_CLI_FIF01:
                $reportService = new ConditionReportForFifeService($this->permissionService, $report);
                $returnData['result'] = $reportService->runReport($repId, $repPdfFile, $inputs);
                $filterText = $reportService->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_CND_CLI_NEL01:
                $cndNeLincsSurveyBuildingService = new CNDNELincsSurveyBuildingService(
                    $this->permissionService,
                    $report
                );
                $this->runReportWithValidate(
                    $repPdfFile,
                    $inputs,
                    $repId,
                    $cndNeLincsSurveyBuildingService,
                    $returnData
                );
                $filterText = $cndNeLincsSurveyBuildingService->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_CND08:
                $identifiedWorkReportService =
                    new WkHtmlToPdf\IdentifiedWorkReportService($this->permissionService, $report);
                $returnData['result'] = $identifiedWorkReportService->runReport($repPdfFile, $inputs, $repId);
                $filterText = $identifiedWorkReportService->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_CND09:
                $condElementService = new WkHtmlToPdf\CND09ConditionSurveyElementInformation(
                    $this->permissionService,
                    $report
                );
                $this->runReportWithValidate($repPdfFile, $inputs, $repId, $condElementService, $returnData);
                $filterText = $condElementService->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_CND10:
                $fullCondSurvey = new WkHtmlToPdf\CND10FullConditionSurveyService($this->permissionService, $report);
                $returnData['result'] = $fullCondSurvey->runReport($repPdfFile, $inputs, $repId);
                $filterText = $fullCondSurvey->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_CND13:
                $fullCondSurvey = new WkHtmlToPdf\CND13FullConditionSurveyNoIWCostsService(
                    $this->permissionService,
                    $report
                );
                $returnData['result'] = $fullCondSurvey->runReport($repPdfFile, $inputs, $repId);
                $filterText = $fullCondSurvey->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_CND_CLI_SUN01:
                $reportService = new WkHtmlToPdf\Sunderland\CNDCLISUN01ReportService($this->permissionService, $report);
                $this->runReportWithValidate(
                    $repPdfFile,
                    $inputs,
                    $repId,
                    $reportService,
                    $returnData
                );
                $filterText = $reportService->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_DLO15:
                $dloTimesheet = new WkHtmlToPdf\DLO15WeeklyTimesheet($this->permissionService, $report);
                $this->runReportWithValidate(
                    $repPdfFile,
                    $inputs,
                    $repId,
                    $dloTimesheet,
                    $returnData
                );
                $filterText = $dloTimesheet->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_PL02:
                $plantAssetHistoryService = new PL02AssetHistoryReportService($this->permissionService, $report);
                $returnData['result'] = $plantAssetHistoryService->runReport($repPdfFile, $inputs, $repId);
                $filterText = $plantAssetHistoryService->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_PPRJ02:
                $projectActionsWithNotesReportService =
                    new ProjectActionsWithNotesReportService($this->permissionService, $report);
                $returnData['result'] = $projectActionsWithNotesReportService->runReport($repPdfFile, $inputs, $repId);
                $filterText = $projectActionsWithNotesReportService->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_PRJ11:
                $projectMonitoringReportService =
                    new ProjectMonitoringReportService($this->permissionService, $report);
                $this->runReportWithValidate(
                    $repPdfFile,
                    $inputs,
                    $repId,
                    $projectMonitoringReportService,
                    $returnData
                );
                $filterText = $projectMonitoringReportService->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_PRJ12:
                $projectIssuesWithNotesReportService =
                    new ProjectIssuesWithNotesReportService($this->permissionService, $report);
                $returnData['result'] = $projectIssuesWithNotesReportService->runReport($repPdfFile, $inputs, $repId);
                $filterText = $projectIssuesWithNotesReportService->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_CPC04:
            case ReportConstant::SYSTEM_REPORT_CPC05:
                $costPlusSalesInvoiceStatementService =
                    new WkHtmlToPdf\CPC04CostPlusSalesInvoiceStatementService($this->permissionService, $report);
                $inputs['report_code'] = $report->getRepCode();
                $this->runReportWithValidate(
                    $repPdfFile,
                    $inputs,
                    $repId,
                    $costPlusSalesInvoiceStatementService,
                    $returnData
                );
                $filterText = $costPlusSalesInvoiceStatementService->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_PRJ13:
                $projectHighlightService = new ProjectHighLightService($this->permissionService, $report);
                $this->runReportWithValidate(
                    $repPdfFile,
                    $inputs,
                    $repId,
                    $projectHighlightService,
                    $returnData
                );
                $filterText = $projectHighlightService->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_ES37:
                $leaseOutSummaryReportService =
                    new LeaseOutSummaryReportService($this->permissionService, $report);
                $returnData['result'] = $leaseOutSummaryReportService->runReport($repPdfFile, $inputs, $repId);
                $filterText = $leaseOutSummaryReportService->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_ES39:
            case ReportConstant::SYSTEM_REPORT_RE03:
                $repOutFile = $repPdfFile;
                if ($inputs['exportAs']['label'] === 'XLSX') {
                    $inputs['exportExcel'] = 1;
                }
                if (array_get($inputs, 'exportExcel')) {
                    $repOutFile = $reportDir . DIRECTORY_SEPARATOR . $reportFileName . '.xlsx';
                    $returnData['repFileName'] = $reportFileName . '.xlsx';
                    $returnData['repFilePath'] = $repOutFile;
                    $returnData['repFormat'] = ReportConstant::REPORT_OUTPUT_XLSX_FORMAT;
                }
                $leaseOutCharge12MonthForecastReportService =
                    new LeaseOutCharge12MonthForecastReportService($this->permissionService, $report);
                $returnData['result'] = $leaseOutCharge12MonthForecastReportService->runReport(
                    $repOutFile,
                    $inputs,
                    $repId
                );
                $filterText = $leaseOutCharge12MonthForecastReportService->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_ES40:
                $leaseOutCharge5YearForecastReportService =
                    new LeaseOutCharge5YearForecastReportService($this->permissionService, $report);
                $returnData['result'] = $leaseOutCharge5YearForecastReportService->runReport(
                    $repPdfFile,
                    $inputs,
                    $repId
                );
                $filterText = $leaseOutCharge5YearForecastReportService->filterText;
                break;

            case ReportConstant::SYSTEM_REPORT_ES_CLI_SEF01:
                $esCliSef01Report = new ESCLISEF01ReportService($this->permissionService, $report);
                $returnData['result'] = $esCliSef01Report->runReport($repPdfFile, $inputs, $repId);
                $filterText = $esCliSef01Report->filterText;
                break;

            case ReportConstant::SYSTEM_REPORT_QST_CLI_LBHFH01:
                $qstCliLBHFHousing01Report = new QSTCLILBHFHousing01ReportService($this->permissionService);
                $returnData['result'] = $qstCliLBHFHousing01Report->runReport($repPdfFile, $inputs, $repId);
                $filterText = $qstCliLBHFHousing01Report->filterText;
                break;

            case ReportConstant::SYSTEM_REPORT_QST_CLI_SUN02:
            case ReportConstant::SYSTEM_REPORT_QST_CLI_SUN03:
            case ReportConstant::SYSTEM_REPORT_QST_CLI_SUN04:
                $qstCliSun02 = new WkHtmlToPdf\Sunderland\QSTCLISUN02ReportService($this->permissionService, $report);
                $this->runReportWithValidate(
                    $repPdfFile,
                    $inputs,
                    $repId,
                    $qstCliSun02,
                    $returnData
                );
                $filterText = $qstCliSun02->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_PR_CLI_SAYR_01:
                $assetInfo = new WkHtmlToPdf\SouthAyrshire\PRCLISAYR01AssetInformationService(
                    $this->permissionService,
                    $report
                );
                $returnData['result'] = $assetInfo->runReport($repPdfFile, $inputs, $repId);
                $filterText = $assetInfo->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_CND_CLI_WOK01:
                $cndCliWok01 = new CNDCLIWOK01CondSurveyReportService($this->permissionService);
                $returnData['result'] = $cndCliWok01->runReport($repPdfFile, $inputs, $repId);
                $filterText = $cndCliWok01->filterText;
                break;

            case ReportConstant::SYSTEM_REPORT_CND_CLI_SB01:
                $cndCliSB01 = new CNDCLISB01ReportService($this->permissionService, $report);
                $this->runReportWithValidate(
                    $repPdfFile,
                    $inputs,
                    $repId,
                    $cndCliSB01,
                    $returnData
                );


                $filterText = $cndCliSB01->filterText;
                break;

            case ReportConstant::SYSTEM_REPORT_CND_CLI_MK01:
                $cndCliMk01 = new CNDCLIMK01ReportService($this->permissionService);
                $returnData['result'] = $cndCliMk01->runReport($repPdfFile, $inputs, $repId);
                $filterText = $cndCliMk01->filterText;
                break;

            case ReportConstant::SYSTEM_REPORT_CND_CLI_TORB02:
                $cndCliTorb02 = new CNDCLITORB02BuildingConditionSurveyService($this->permissionService, $report);
                $this->runReportWithValidate(
                    $repPdfFile,
                    $inputs,
                    $repId,
                    $cndCliTorb02,
                    $returnData
                );
                $filterText = $cndCliTorb02->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_CND15:
            case ReportConstant::SYSTEM_REPORT_CND16:
            case ReportConstant::SYSTEM_REPORT_CND17:
            case ReportConstant::SYSTEM_REPORT_CND18:
            case ReportConstant::SYSTEM_REPORT_CND19:
            case ReportConstant::SYSTEM_REPORT_CND20:
            case ReportConstant::SYSTEM_REPORT_CND21:
            case ReportConstant::SYSTEM_REPORT_CND22:
            case ReportConstant::SYSTEM_REPORT_CND23:
                $idWorkFMR = new IdWorkFMRReportService($this->permissionService, $report);
                $inputs['repCode'] = $report->getRepCode();

                $this->runReportWithValidate(
                    $repPdfFile,
                    $inputs,
                    $repId,
                    $idWorkFMR,
                    $returnData
                );

                $filterText = $idWorkFMR->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_PTW02:
                $ptwReport = new Ptw02ReportService($this->permissionService, $report);
                $returnData['result'] = $ptwReport->runReport($repPdfFile, $inputs, $repId);
                $filterText = $ptwReport->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_CM03:
                $cm03 = new CM03CaseProgressService($this->permissionService, $report);
                $returnData['result'] = $cm03->runReport($repPdfFile, $inputs, $repId);
                $filterText = $cm03->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_CM2_03:
                $cm03 = new CM03Casemgmt2ProgressService($this->permissionService, $report);
                $returnData['result'] = $cm03->runReport($repPdfFile, $inputs, $repId);
                $filterText = $cm03->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_CM3_03:
                $cm03 = new CM03Casemgmt3ProgressService($this->permissionService, $report);
                $returnData['result'] = $cm03->runReport($repPdfFile, $inputs, $repId);
                $filterText = $cm03->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_CM4_03:
                $cm03 = new CM03Casemgmt4ProgressService($this->permissionService, $report);
                $returnData['result'] = $cm03->runReport($repPdfFile, $inputs, $repId);
                $filterText = $cm03->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_CM5_03:
                $cm03 = new CM03Casemgmt5ProgressService($this->permissionService, $report);
                $returnData['result'] = $cm03->runReport($repPdfFile, $inputs, $repId);
                $filterText = $cm03->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_PRJ17:
                $prj17 = new PRJ17RiskRegisterReportService($this->permissionService, $report);
                $returnData['result'] = $prj17->runReport($repPdfFile, $inputs, $repId);
                $filterText = $prj17->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_PERS01:
                $reportService = new PERS01ReportService($this->permissionService);
                $this->runReportWithValidate(
                    $repPdfFile,
                    $inputs,
                    $repId,
                    $reportService,
                    $returnData
                );
                $filterText = $reportService->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_PERS02:
                $reportService = new PERS02ReportService($this->permissionService);
                $returnData['result'] = $reportService->runReport($repPdfFile, $inputs, $repId);
                $filterText = $reportService->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_RSK03:
                $rsk03 = new RSK03RiskAssessmentSummaryReportService($this->permissionService, $report);
                $returnData['result'] = $rsk03->runReport($repPdfFile, $inputs, $repId);
                $filterText = $rsk03->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_HLP13:
                $reportService = new WkHtmlToPdf\HLP13FinancialSummaryReportService(
                    $this->permissionService,
                    $report,
                    $inputs
                );
                $returnData['result'] = $reportService->runReport($repPdfFile, $repId);
                $filterText = $reportService->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_PR_CLI_TUN01:
                $reportService = new WkHtmlToPdf\PRCLITUN01PropertyAssetReportService(
                    $this->permissionService,
                    $report,
                    $inputs
                );
                $returnData['result'] = $reportService->runReport($repPdfFile, $repId);
                $filterText = $reportService->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_PR_CLI_TUN02:
                $reportService = new WkHtmlToPdf\PRCLITUN02PropertyAssetReportService(
                    $this->permissionService,
                    $report,
                    $inputs
                );
                $returnData['result'] = $reportService->runReport($repPdfFile, $repId);
                $filterText = $reportService->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_PR_CLI_CEC01:
                $reportService = new PRCLICEC01PropertyReportService($this->permissionService, $report);
                $this->runReportWithValidate(
                    $repPdfFile,
                    $inputs,
                    $repId,
                    $reportService,
                    $returnData
                );
                $filterText = $reportService->filterText;
                break;
            case ReportConstant::SYSTEM_REPORT_ES67:
                $scSummaryReportService =
                    new ScSummaryReportService($this->permissionService);
                $returnData['result'] = $scSummaryReportService->runReport($repPdfFile, $inputs, $repId);
                $filterText = $scSummaryReportService->filterText;
                break;
            default:
                throw new StateException("Unknown wkhtml Report");
        }

        if ($returnData['result']) {
            ReportGenerateService::setRecentReport($repType, $repId);

            // add entry to audit log
            $auditService = new AuditService();
            if ($filterText) {
                $auditService->auditText = "Filtering details: $filterText";
            }
            $auditService->addAuditEntry($report, AuditAction::ACT_REPORT);
        }

        return $returnData;
    }

    private function runReportWithValidate($repPdfFile, $inputs, $repId, $reportService, &$returnData)
    {
        $this->formatInputData($inputs);
        $validator = $reportService->validateFilter($inputs);
        if ($validator->passes()) {
            $returnData['result'] = $reportService->runReport($repPdfFile, $inputs, $repId);
        } else {
            $returnData['result'] = false;
            $returnData['repFile'] = '';
            $returnData['filterValidation'] = $validator->messages()->toArray();
        }
    }

    private function formatInputData(&$inputs)
    {
        $selectorInputs = ['site_id', 'building_id', 'room_id','plant_id', 'project', 'cpContract'];
        foreach ($inputs as $key => $value) {
            if (($value === "") || ((in_array($key, $selectorInputs)) && ($value === "null"))) {
                $inputs[$key] = null;
            }
        }
    }

    private function setCustomFileName($inputs, $report)
    {
        $reportFileName = $report->getRepCode();
        if ($report->getRepCode() == ReportConstant::SYSTEM_REPORT_ASB_CLI_FIF01) {
            $siteId = null;
            $buildingId = null;

            if (array_key_exists('flt', $inputs)) {
                foreach ($inputs['flt'] as $key => $val) {
                    $siteIdFilter = array_search('site_id', $val);
                    $buildingIdFilter = array_search('building_id', $val);

                    if ($siteIdFilter) {
                        $siteId = $val['v'];
                    }

                    if ($buildingIdFilter) {
                        $buildingId = $val['v'];
                    }
                }
            }

            if ($siteId) {
                $siteModel = \Tfcloud\Models\Site::find($siteId);

                if ($siteModel) {
                    $siteCode = $siteModel->site_code;
                    $reportFileName .= ' - ' . $siteCode;
                }
            }

            if ($buildingId) {
                $buildingModel = \Tfcloud\Models\Building::find($buildingId);

                if ($buildingModel) {
                    $buildingCode = $buildingModel->building_code;
                    $reportFileName .= ' - ' . $buildingCode;
                }
            }
        }

        return $reportFileName;
    }
}
