<?php

namespace Tfcloud\Services\Report;

use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Tfcloud\Lib\AuditFields;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\CommonTime;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Exceptions\InvalidRecordException;
use Tfcloud\Lib\Exceptions\PermissionException;
use Tfcloud\Lib\Filters\ScheduledReportFilter;
use Tfcloud\Models\AuditAction;
use Tfcloud\Models\CrudAccessLevel;
use Tfcloud\Models\Module;
use Tfcloud\Models\Report\ReportFieldGroup;
use Tfcloud\Models\Report\ReportFormat;
use Tfcloud\Models\ReportRunDay;
use Tfcloud\Models\ReportSchedule;
use Tfcloud\Models\SiteGroup;
use Tfcloud\Models\SystemReport;
use Tfcloud\Models\UserReport;
use Tfcloud\Models\UserReportType;
use Tfcloud\Services\AuditService;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\PermissionService;

class ScheduledReportService extends BaseService
{
    private const EMAIL_SEPARATE = ',';
    protected $reportSchedule;

    public function __construct(PermissionService $permissionService, ReportService $reportService)
    {
        $this->permissionService = $permissionService;
        $this->reportService          = $reportService;

        Validator::extend('validate_report_type', function ($attribute, $value, $parameters) {
            if (empty($value) && empty($this->reportSchedule->user_report_id)) {
                return false;
            }

            if ($value && $this->reportSchedule->user_report_id) {
                return false;
            }

            return true;
        });

        Validator::extend('exceeded_max', function ($attribute, $value, $parameters) {
            return false;
        });
    }

    public function interfaceEnabled()
    {
        return $this->reportService->intIsInterfaceEnabled(CommonConstant::INTERFACE_EXP_GEN_REPORT);
    }

    public function get($id, $audit = true)
    {
        if (
            !$this->permissionService->hasModuleAccess(
                Module::MODULE_REPORTS,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
            )
        ) {
            $msg = "Not met minimum readonly access for report module in class" .
                " (" . __CLASS__ . ") method (" . __METHOD__ . "). " .
                " See line (" . __LINE__ . ")";
            throw new PermissionException($msg);
        }

        $query = ReportSchedule::userSiteGroup()->where('report_schedule_id', $id);
        /**
         * Admin, Super Users, and report module Supervisors, can all edit any scheduled report entry.
         */
        if (
            !$this->permissionService->isAdministratorOrSuperuser() &&
            !$this->checkPermissionForModule($this->reportSchedule, CrudAccessLevel::CRUD_ACCESS_LEVEL_SUPERVISOR)
        ) {
            $query->where('user_id', \Auth::user()->getKey());
        }

        $this->reportSchedule = $query->first();

        if (
            ! $this->checkPermissionForModule($this->reportSchedule, CrudAccessLevel::CRUD_ACCESS_LEVEL_READ)
            && $this->reportSchedule->user_id != \Auth::user()->getKey()
        ) {
            $msg = "Not met minimum readonly access for report module in class" .
                " (" . __CLASS__ . ") method (" . __METHOD__ . "). " .
                " See line (" . __LINE__ . ")";
            throw new PermissionException($msg);
        }

        if (is_null($this->reportSchedule)) {
            throw new InvalidRecordException(\Lang::get('reminders.scheduled_report_not_found'));
        }

        $this->reportSchedule->days_of_week = [];

        if ($audit) {
            $auditService = new AuditService();
            $auditService->addAuditEntry($this->reportSchedule, AuditAction::ACT_VIEW);
        }

        return $this->reportSchedule;
    }

    public function getAll($inputs)
    {
        if (
            !$this->permissionService->hasModuleAccess(
                Module::MODULE_REPORTS,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
            )
        ) {
            $msg = "Not met minimum readonly access for report module in class" .
                " (" . __CLASS__ . ") method (" . __METHOD__ . "). " .
                " See line (" . __LINE__ . ")";
            throw new PermissionException($msg);
        }

        $reportModuleSupervisor = $this->permissionService->hasModuleAccess(
            Module::MODULE_REPORTS,
            CrudAccessLevel::CRUD_ACCESS_LEVEL_SUPERVISOR
        );

        if ($this->permissionService->isAdministratorOrSuperuser() || $reportModuleSupervisor) {
            // Admin users, superuser or report module supervisors can see all scheduled reports
        } else {
            // Can only see their own scheduled reports.
            $inputs['owner'] = \Auth::user()->getKey();
        }

        $query = $this->filterAll($this->all(), $inputs);

        return $query;
    }

    public static function filterAll($query, $inputs)
    {
        $filter = new ScheduledReportFilter($inputs);

        if (!is_null($filter->owner)) {
            $query->where('report_schedule.user_id', $filter->owner);
        }

        $query->orderBy($filter->sort, $filter->sortOrder);

        return $query;
    }

    public function newScheduleReport()
    {
        $this->reportSchedule = new ReportSchedule();
        $this->initDefaultData();

        return $this->reportSchedule;
    }

    public function lookups()
    {
        $inputs = [
            'sort' => 'code',
            'type' => ReportService::REP_TYPE_SYSTEM
        ];

        $reports = $this->reportService->getAll($inputs);

        foreach ($reports as $key => $report) {
            if (
                ! $this->permissionService->hasModuleAccess(
                    Module::MODULE_REPORTS,
                    CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
                ) || ! $this->permissionService->hasModuleAccess(
                    $report->module_id,
                    CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
                )
            ) {
                unset($reports[$key]);
            } else {
                //reformat running type of System report
                if (in_array($report->report_code, $this->reportService->getWkHtmlToPdfWhiteList())) {
                    $report->running_type = 'PDF';
                } elseif (in_array($report->report_code, $this->reportService->getExcelWhiteList())) {
                    $report->running_type = 'Excel';
                }
            }
        }

        $systemReport   = json_decode(json_encode($reports), true);

        $inputs['type'] = ReportService::REP_TYPE_USERDEFINED;
        $reports = $this->reportService->getAll($inputs);
        foreach ($reports as $key => $report) {
            if (
                ! $this->permissionService->hasModuleAccess(
                    Module::MODULE_REPORTS,
                    CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
                ) || ! $this->permissionService->hasModuleAccess(
                    $report->module_id,
                    CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
                )
            ) {
                unset($reports[$key]);
            }
        }
        $userReport     = json_decode(json_encode($reports), true);
        $reportFormat = (new ReportFormat())->lookupList(
            [
                'wheres' => [
                    'field' => 'report_format_id',
                    'operator' => '!=',
                    'param' => ReportFormat::XLS
                ],
                'ignoreCheckUserGroup' => true,
                'incInactive' => true
            ]
        );

        $reportFieldGroup =  [];
        if ($this->reportSchedule) {
            $systemReportRecord = null;
            $userReportId = $this->reportSchedule->user_report_id ? : \Input::old('user_report_id');
            $systemReportId = $this->reportSchedule->system_report_id ? : \Input::old('system_report_id');

            if ($userReportId) {
                $this->reportSchedule->user_report_id = $userReportId;
                $systemReportRecord = $this->reportSchedule->userReport->systemReport;
            } elseif ($systemReportId) {
                $this->reportSchedule->system_report_id = $systemReportId;
                $systemReportRecord = $this->reportSchedule->systemReport;
            }
            if ($systemReportRecord) {
                $reportFieldGroup = $this->getReportFieldGroupByReportCode($systemReportRecord->system_report_code);
            }
        }
        return [
            'systemReport' => $systemReport,
            'userReport'   => $userReport,
            'reportFormat' => $reportFormat,
            'reportFieldGroup' => $reportFieldGroup,
        ];
    }

    public function getReportFieldGroup($systemReportId, $userReportId = null, $options = [])
    {
        $isCheckPermission = array_get($options, 'check_permission', true);
        if ($isCheckPermission) {
            if (
                !$this->permissionService->hasModuleAccess(
                    Module::MODULE_REPORTS,
                    CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
                )
            ) {
                $msg = "Not met minimum create new access for report module.";
                throw new PermissionException($msg);
            }
        }
        $systemReport = null;
        $reportFieldGroup = null;
        if ($userReportId) {
            $systemReport = SystemReport::join(
                'user_report',
                'user_report.system_report_id',
                '=',
                'system_report.system_report_id'
            )
            ->where('user_report_id', $userReportId)
            ->where('user_report_type_id', '<>', UserReportType::USER_REPORT_TYPE_QUERY)
            ->where('site_group_id', SiteGroup::get()->site_group_id)
            ->select(['system_report.system_report_code'])
            ->first();
        } else {
            $systemReport = SystemReport::where('system_report_id', $systemReportId)
                ->select(['system_report.system_report_code'])
                ->first();
        }

        if ($systemReport) {
            $reportFieldGroup = $this->getReportFieldGroupByReportCode($systemReport->system_report_code);
        }
        return $reportFieldGroup;
    }

    private function getReportFieldGroupByReportCode($systemReportCode)
    {
        $reportFieldGroup = ReportFieldGroup::join(
            'report_field_group_model',
            'report_field_group_model.report_field_group_model_id',
            '=',
            'report_field_group.report_field_group_model_id'
        )->where(
            'system_report_code',
            $systemReportCode
        )->select([
            'report_field_group.report_field_group_id',
            'report_field_group_model.report_field_group_model_desc',
        ])->get();
        return $reportFieldGroup;
    }

    public function filterLookups()
    {
        return $this->reportService->filterLookups();
    }

    public function checkPermissionForModule($reportSchedule, $crudLevel)
    {
        if (is_null($reportSchedule)) {
            return true;
        }
        $bAccess = true;
        if ($reportSchedule->system_report_id) {
            if (empty($reportSchedule->user_report_id)) {
                $bAccess = $this->permissionService->hasModuleAccess(
                    $reportSchedule->systemReport->module_id,
                    $crudLevel
                );
            }
        } elseif ($reportSchedule->user_report_id) {
            if (is_null($reportSchedule->userReport)) {
                $bAccess = false;
            } else {
                $bAccess = $this->permissionService->hasModuleAccess(
                    $reportSchedule->userReport->systemReport->module_id,
                    $crudLevel
                );
            }
        }

        return $bAccess;
    }

    public function addScheduleReport(&$id, $inputs)
    {
        if (
            !$this->permissionService->hasModuleAccess(
                Module::MODULE_REPORTS,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_CREATE
            )
        ) {
            $msg = "Not met minimum create new access for report module in class" .
                " (" . __CLASS__ . ") method (" . __METHOD__ . "). " .
                " See line (" . __LINE__ . ")";
            throw new PermissionException($msg);
        }

        $this->newScheduleReport();
        $this->formatInputData($inputs);
        $this->setModelFields($inputs);

        if (! $this->checkPermissionForModule($this->reportSchedule, CrudAccessLevel::CRUD_ACCESS_LEVEL_CREATE)) {
            $msg = "Not met minimum create new access for report module in class" .
                " (" . __CLASS__ . ") method (" . __METHOD__ . "). " .
                " See line (" . __LINE__ . ")";
            throw new PermissionException($msg);
        }

        $validator = $this->getScheduleReportValidator();

        if ($validator->passes()) {
            \DB::transaction(function () use ($inputs) {

                if ($this->reportSchedule->run_once == CommonConstant::DATABASE_VALUE_NO) {
                    $this->reportSchedule->next_run_date =
                        $this->getNextRunDate($inputs, $this->reportSchedule->next_run_date);
                    $this->reportSchedule->schedule_options = CommonTime::recurringParseToJson($inputs);
                } else {
                    $this->reportSchedule->schedule_options = null;
                }
                $this->reportSchedule->save();

                $audit = new AuditService();
                $audit->addAuditEntry($this->reportSchedule, AuditAction::ACT_INSERT);
            });

            $id = $this->reportSchedule->getKey();

            $validator->success = true;
        }

        return $validator;
    }

    public function updateScheduleReport($id, $inputs)
    {
        if (
            !$this->permissionService->hasModuleAccess(
                Module::MODULE_REPORTS,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_UPDATE
            )
        ) {
            $msg = "Not met minimum update access for report module in class" .
                " (" . __CLASS__ . ") method (" . __METHOD__ . "). " .
                " See line (" . __LINE__ . ")";
            throw new PermissionException($msg);
        }

        $this->reportSchedule = $this->get($id, false);

        if (! $this->checkPermissionForModule($this->reportSchedule, CrudAccessLevel::CRUD_ACCESS_LEVEL_UPDATE)) {
            $msg = "Not met minimum update access for report module in class" .
                " (" . __CLASS__ . ") method (" . __METHOD__ . "). " .
                " See line (" . __LINE__ . ")";
            throw new PermissionException($msg);
        }
        $this->formatInputData($inputs);
        $this->setModelFields($inputs);
        $validator = $this->getScheduleReportValidator(true);

        if ($validator->passes()) {
            \DB::transaction(function () use ($inputs) {
                if ($this->reportSchedule->run_once == CommonConstant::DATABASE_VALUE_NO) {
                    $this->reportSchedule->next_run_date =
                        $this->getNextRunDate($inputs, $this->reportSchedule->next_run_date);
                    $this->reportSchedule->schedule_options = CommonTime::recurringParseToJson($inputs);
                } else {
                    $this->reportSchedule->schedule_options = null;
                }
                $auditFields = $this->setupAuditFields();
                $audit = new AuditService();
                $audit->auditFields($auditFields);
                $audit->addAuditEntry($this->reportSchedule, AuditAction::ACT_UPDATE);
                $this->reportSchedule->save();
            });

            $validator->success = true;
        }

        return $validator;
    }

    public function deleteScheduleReport($id)
    {
        if (
            !$this->permissionService->hasModuleAccess(
                Module::MODULE_REPORTS,
                CrudAccessLevel::CRUD_ACCESS_LEVEL_DELETE
            )
        ) {
            $msg = "Not met minimum delete access for report module in class" .
                " (" . __CLASS__ . ") method (" . __METHOD__ . "). " .
                " See line (" . __LINE__ . ")";
            throw new PermissionException($msg);
        }

        $this->reportSchedule = $this->get($id, false);

        if (! $this->checkPermissionForModule($this->reportSchedule, CrudAccessLevel::CRUD_ACCESS_LEVEL_DELETE)) {
            $msg = "Not met minimum update access for report module in class" .
                " (" . __CLASS__ . ") method (" . __METHOD__ . "). " .
                " See line (" . __LINE__ . ")";
            throw new PermissionException($msg);
        }

        $message = $this->checkRelationShip($this->reportSchedule);

        if (count($message) === 0) {
            \DB::transaction(function () {

                $audit = new AuditService();
                $audit->addAuditEntry($this->reportSchedule, AuditAction::ACT_DELETE);
                $this->reportSchedule->delete();
            });
        }

        return $message;
    }

    private function initDefaultData()
    {
        $this->reportSchedule->site_group_id = SiteGroup::get()->site_group_id;
        $this->reportSchedule->user_id       = \Auth::User()->getKey();
        $this->reportSchedule->run_once      = CommonConstant::DATABASE_VALUE_NO;
        $this->reportSchedule->days_of_week  = [];
        $this->reportSchedule->email_to_addr = \Auth::User()->email_reply_to;
        $this->reportSchedule->include_filter_summary = CommonConstant::DATABASE_VALUE_YES;
        $this->reportSchedule->send_if_empty = CommonConstant::DATABASE_VALUE_YES;
        $this->reportSchedule->group_send = CommonConstant::DATABASE_VALUE_NO;
    }

    public function all()
    {
        $query = ReportSchedule::userSiteGroup()
        ->leftJoin(
            'system_report',
            'report_schedule.system_report_id',
            '=',
            'system_report.system_report_id'
        )->leftJoin(
            'user_report',
            'report_schedule.user_report_id',
            '=',
            'user_report.user_report_id'
        )->join(
            'user',
            'report_schedule.user_id',
            '=',
            'user.id'
        )
            ->select([
                'report_schedule.*',
                'user.username',
                \DB::raw("DATE_FORMAT(report_schedule.last_run_datetime, '%H:%i') AS last_run_time"),
                \DB::raw('IF(report_schedule.system_report_id > 0,system_report_code,user_report_code) AS report_code'),
                \DB::raw('IF(report_schedule.system_report_id > 0,system_report_desc,user_report_desc) AS report_desc')
            ]);

        return $query;
    }

    private function formatInputData(&$inputs)
    {
        $nextRunDate = array_get($inputs, 'next_run_date', null);
        if ($nextRunDate) {
            $inputs['next_run_date'] =
                Common::dateFieldToSqlFormat($nextRunDate, ['includeTime' => false]);
        }
    }

    private function setModelFields($inputs)
    {
        Common::assignField($this->reportSchedule, 'system_report_id', $inputs);
        Common::assignField($this->reportSchedule, 'user_report_id', $inputs);
        Common::assignCheckboxField($this->reportSchedule, 'run_once', $inputs);
        Common::assignField($this->reportSchedule, 'email_to_addr', $inputs);
        Common::assignField($this->reportSchedule, 'include_filter_summary', $inputs);
        Common::assignField($this->reportSchedule, 'send_if_empty', $inputs);

        Common::assignField($this->reportSchedule, 'recurring_option', $inputs);
        Common::assignField($this->reportSchedule, 'recurring_every_day', $inputs);
        Common::assignField($this->reportSchedule, 'recurring_every_weekday', $inputs);
        Common::assignField($this->reportSchedule, 'recurring_every_week', $inputs);
        $this->reportSchedule->days_of_week = array_get($inputs, 'days_of_week', []);
        Common::assignField($this->reportSchedule, 'recurring_every_month', $inputs);
        Common::assignField($this->reportSchedule, 'day_of_month', $inputs);
        Common::assignField($this->reportSchedule, 'recurring_every_condition_month', $inputs);
        Common::assignField($this->reportSchedule, 'monthly_ordinal_option', $inputs);
        Common::assignField($this->reportSchedule, 'monthly_day_option', $inputs);
        Common::assignField($this->reportSchedule, 'next_run_date', $inputs);
        Common::assignField($this->reportSchedule, 'group_send', $inputs);

        if (array_get($inputs, 'max_rows')) {
            Common::assignField($this->reportSchedule, 'max_rows', $inputs);
        }

        Common::assignField($this->reportSchedule, 'email_subject', $inputs);
        Common::assignField($this->reportSchedule, 'email_text', $inputs);

        if (array_get($inputs, 'report_format_id')) {
            Common::assignField($this->reportSchedule, 'report_format_id', $inputs);
        }
        if ($this->reportSchedule->group_send == CommonConstant::DATABASE_VALUE_YES) {
            $this->reportSchedule->email_to_addr = '';
            Common::assignField($this->reportSchedule, 'report_field_group_id', $inputs);
        } else {
            $this->reportSchedule->report_field_group_id = null;
        }
    }

    private function getScheduleReportValidator($isEdit = false)
    {
        $attributes = $this->reportSchedule->getAttributes();
        $today = Carbon::now();
        $validationRules = [
            'system_report_id'  => ['required_without:user_report_id', 'validate_report_type'],
            'email_to_addr'     => ['emailList', 'max:255'],
            'max_rows'          => ['integer', 'min:1', 'max:9999999999'],
            'email_subject'     => ['max:255'],
            'email_text'        => ['max:4000'],
            'report_format_id'  => ['integer', 'nullable'],
            'include_filter_summary' => ['in:' . CommonConstant::YES_NO],
            'send_if_empty' => ['in:' . CommonConstant::YES_NO],
            'next_run_date' => ['required', 'date_format:Y-m-d', 'after:' . $today->format('Y-m-d')],
            'group_send' => ['in:' . CommonConstant::YES_NO],
        ];

        if ($this->reportSchedule->group_send == CommonConstant::DATABASE_VALUE_YES) {
            $reportFieldGroupRules = ['required'];
            $reportFieldGroups = $this->getReportFieldGroup(
                $this->reportSchedule->system_report_id,
                $this->reportSchedule->user_report_id,
                ['check_permission' => false]
            );
            if (!empty($reportFieldGroups) && count($reportFieldGroups)) {
                $reportFieldGroups = $reportFieldGroups->implode('report_field_group_id', ', ');
                $reportFieldGroupRules[] = 'in:' . $reportFieldGroups;
            }
            $validationRules['report_field_group_id'] = $reportFieldGroupRules;
        } else {
            array_push($validationRules['email_to_addr'], 'required');
        }

        if ($this->reportSchedule->run_once == CommonConstant::DATABASE_VALUE_NO) {
            switch ($this->reportSchedule->recurring_option) {
                case CommonConstant::RECUR_EVERY_DAY:
                    $validationRules['recurring_every_day'] = ['required', 'integer', 'min:1', 'max:999'];
                    break;
                case CommonConstant::RECUR_EVERY_WEEKDAY:
                    $validationRules['recurring_every_weekday'] = ['required', 'integer', 'min:1', 'max:999'];
                    break;
                case CommonConstant::RECUR_EVERY_WEEK:
                    $validationRules['recurring_every_week'] = ['required', 'integer', 'min:1', 'max:99'];
                    $validationRules['days_of_week'] = ['required'];
                    break;
                case CommonConstant::RECUR_EVERY_MONTH:
                    $validationRules['recurring_every_month'] = ['required', 'integer', 'min:1', 'max:99'];
                    $validationRules['day_of_month'] = ['required', 'integer', 'min:1', 'max:31'];
                    break;
                case CommonConstant::RECUR_EVERY_MONTH_SPECIAL_CASE:
                    $validationRules['recurring_every_condition_month'] = ['required', 'integer', 'min:1', 'max:99'];
                    $validationRules['monthly_ordinal_option'] = ['required'];
                    $validationRules['monthly_day_option'] = ['required'];
                    break;
            }
        }

        $validationMessages = [
            'system_report_id.required_without'     => 'Either a system report or user report must be selected.',
            'system_report_id.validate_report_type' => 'Either a system report or user report' .
                ' can be selected, not both.',
            'report_format_id.integer'              => 'Format is invalid',
            'next_run_date.after'                   => 'Next Run Date must be a date after ' . $today->format('d/m/Y')
        ];
        $bRunOnceSetToN = false;
        if ($isEdit) {
            $bRunOnceSetToN = (($this->reportSchedule->run_once == CommonConstant::DATABASE_VALUE_NO)
                && ($this->reportSchedule->getOriginal('run_once') == CommonConstant::DATABASE_VALUE_YES));
        }

        if (!$isEdit || $bRunOnceSetToN) {
            $currentTotal = $this->getCurrentTotalSchedReport();
            $maxTotal     = $this->getMaxTotalSchedReport();
            if ($currentTotal >= $maxTotal) {
                $validationRules['run_once'] = ['exceeded_max'];
                $validationMessages['run_once.exceeded_max'] = "Maximum total of $maxTotal scheduled reports"
                    . " must not be exceeded.";
            }
        }

        $validator = Validator::make($attributes, $validationRules, $validationMessages);
        $validator->success = false;

        return $validator;
    }

    public function checkRelationShip($reportSchedule)
    {
        $messages = new MessageBag();

        return $messages;
    }

    private function setupAuditFields()
    {
        $systemReportText = '';
        $systemReportId = $this->reportSchedule->getOriginal('system_report_id');
        if ($systemReportId !== $this->reportSchedule->system_report_id) {
            $systemReport = SystemReport::find($systemReportId);
            if ($systemReport) {
                $systemReportText = Common::concatFields(
                    [
                        $systemReport->system_report_code,
                        $systemReport->system_report_desc
                    ]
                );
            }
        }

        $userReportText = '';
        $userReportId = $this->reportSchedule->getOriginal('user_report_id');
        if ($userReportId !== $this->reportSchedule->user_report_id) {
            $userReport = UserReport::find($userReportId);
            if ($userReport) {
                $userReportText = Common::concatFields(
                    [
                        $userReport->user_report_code,
                        $userReport->user_report_desc
                    ]
                );
            }
        }
        $reportGroupFieldText = '';
        $reportGroupFieldId = $this->reportSchedule->getOriginal('report_field_group_id');
        if ($reportGroupFieldId !== $this->reportSchedule->report_field_group_id) {
            $reportGroupField = ReportFieldGroup::join(
                'report_field_group_model',
                'report_field_group_model.report_field_group_model_id',
                '=',
                'report_field_group.report_field_group_model_id'
            )->where('report_field_group_id', $reportGroupFieldId)->select('report_field_group_model_desc')->first();
            if ($reportGroupField) {
                $reportGroupFieldText = $reportGroupField->report_field_group_model_desc;
            }
        }

        $audiFields = new AuditFields($this->reportSchedule);

        $scheduleOptions = $this->reportSchedule->getOriginal('schedule_options');
        $scheduleOptionsText = '';
        if ($scheduleOptions !== $this->reportSchedule->schedule_options && $scheduleOptions) {
            $scheduleOptionsText = CommonTime::parseRecurringJsonToText($scheduleOptions);
        }

        $nextRunDateText = '';
        list($nextRunDate, $nextRunTime) = explode(' ', $this->reportSchedule->getOriginal('next_run_date'));
        if ($nextRunDate !== $this->reportSchedule->next_run_date) {
            $nextRunDateText = Common::dateFieldDisplayFormat($nextRunDate);
        }

        return $audiFields
            ->addField('System Report', 'system_report_id', ['originalText' => $systemReportText])
            ->addField('User Report', 'user_report_id', ['originalText' => $userReportText])
            ->addField('Send To Email', 'email_to_addr')
            ->addField('Run Once', 'run_once')
            ->addField('Next Run Date', 'next_run_date', ['originalText' => $nextRunDateText])
            ->addField('Schedule Options', 'schedule_options', ['originalText' => $scheduleOptionsText])
            ->addField('Email Subject', 'email_subject')
            ->addField('Email Text', 'email_text')
            ->addField('Include Filter Summary', 'include_filter_summary')
            ->addField('Send If Empty', 'send_if_empty')
            ->addField('Group Send', 'group_send')
            ->addField('Group Field', 'report_field_group_id', ['originalText' => $reportGroupFieldText])
            ->getFields();
    }

    private function getCurrentTotalSchedReport()
    {
        $count = ReportSchedule::userSiteGroup()
            ->whereNull('last_run_datetime')
            ->orWhere('run_once', CommonConstant::DATABASE_VALUE_NO)
            ->count();

        return $count;
    }

    private function getMaxTotalSchedReport()
    {
        return SiteGroup::get()->max_total_sched_report;
    }

    /**
     * Does the scheduled report have any format options to choose between?
     * @return boolean
     */
    public function hasFormatOptions(ReportSchedule $reportSchedule)
    {
        // Assumes user reports are all CSV, so they can have CSV or XLS options
        if ($reportSchedule->user_report_id) {
            return true;
        }

        // If user report and system report are not set then there are no format options to return
        if (!$reportSchedule->system_report_id) {
            return false;
        }

        // If a system report is not CSV then there are no options to return
        if (!$this->reportService::isCsvReport($reportSchedule->system_report_id)) {
            return false;
        }

        // The system report is assumed to be CSV at this point, so it does have format options
        return true;
    }

    private function getNextRunDate($inputs, $nextRunDate)
    {
        // Return next run date based on typed next run date.
        $next = CommonTime::getRecurring($nextRunDate, $inputs);
        return $next[0];
    }
}
