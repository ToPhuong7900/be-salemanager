<?php

namespace Tfcloud\Services\Report;

use Exception;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Constant\ReportConstant;
use Tfcloud\Lib\Exceptions\StateException;
use Tfcloud\Models\Report;
use Tfcloud\Models\Report\ReportQueue;
use Tfcloud\Models\Report\ReportQueueControl;
use Tfcloud\Models\Report\ReportQueueStatus;
use Tfcloud\Services\BaseService;

class ReportGenerateCommandService extends BaseService
{
    /**
     * @var ReportService
     */
    protected $reportService;

    /**
     * @var ReportGenerateService
     */
    protected $reportGenerateService;

    protected $log;

    /**
     * @var ReportQueue
     */
    protected $reportQueue;

    private const DATE_FMT = 'Y-m-d H:i:s';

    public function __construct(
        ReportService $reportService,
        ReportGenerateService $reportGenerateService
    ) {
        $this->log = new Logger(CommonConstant::LOGGER_REPORT_BACKGROUND);
        $file = Common::initialiseReportBackgroundLogFile();
        $this->log->pushHandler(new StreamHandler($file, Logger::INFO));
        $this->reportService = $reportService;
        $this->reportGenerateService = $reportGenerateService;
    }

    /**
     * @param $reportQueueId
     * @return ReportQueue
     */
    public function getReportQueueById($reportQueueId)
    {
        $userIsRunningReport = ReportQueueControl::select('report_for_user_id')->pluck('report_for_user_id')->toArray();

        $reportQueueQuery = ReportQueue::select(['report_queue.*'])
            ->where('report_queue_id', $reportQueueId)
            ->where('report_queue_status_id', ReportQueueStatus::PEN);

        if (count($userIsRunningReport)) {
            $reportQueueQuery->whereNotIn('report_queue.user_id', $userIsRunningReport);
        }
        $this->reportQueue = $reportQueueQuery->first();
        return $this->reportQueue;
    }

    public function updateReportQueueByStatus($statusId, ReportQueue &$reportQueue)
    {
        $now = new \DateTime();
        switch ($statusId) {
            case ReportQueueStatus::RUN:
                $reportQueue->started = $now->format(self::DATE_FMT);
                $reportQueue->report_queue_status_id = ReportQueueStatus::RUN;
                $reportQueue->save();
                break;
            case ReportQueueStatus::FAIL:
                $reportQueue->finished = $now->format(self::DATE_FMT);
                $reportQueue->report_queue_status_id = ReportQueueStatus::FAIL;
                $reportQueue->save();
                break;
            case ReportQueueStatus::COMP:
                $reportQueue->finished = $now->format(self::DATE_FMT);
                $reportQueue->report_queue_status_id = ReportQueueStatus::COMP;
                $reportQueue->save();
                break;
            default:
                throw new StateException('Invalid Report Queue Status');
        }
    }

    public function generateReportInQueue($reportQueueId, $pid)
    {
        if (
            !$this->reportQueue instanceof ReportQueue || ($this->reportQueue instanceof ReportQueue &&
                $this->reportQueue->report_queue_id != $reportQueueId)
        ) {
            $this->reportQueue = $this->getReportQueueById($reportQueueId);
        }
        /*
         * Update report queue status to run and create report queue control
         * */
        $this->updateReportQueueByStatus(ReportQueueStatus::RUN, $this->reportQueue);
        $reportQueueControl = $this->newReportQueueControl(
            $reportQueueId,
            $this->reportQueue->user_id,
            $pid
        );
        try {
            /*
             * Create report storage path
             * */
            $siteGroupId = \Auth::user()->site_group_id;
            $storagePathway = Common::getUploadSectionStoragePathway(
                $siteGroupId,
                ReportConstant::REPORT_UPLOAD_FOLDER,
                $reportQueueId
            );
            if (!file_exists($storagePathway)) {
                \File::makeDirectory($storagePathway, 0775, true);
            }

            if ($this->reportQueue->system_report_id) {
                $repId = $this->reportQueue->system_report_id;
                $repType = Report::SYSTEM_REPORT;
            } else {
                $repId = $this->reportQueue->user_report_id;
                $repType = Report::USER_REPORT;
            }

            $report = $this->reportService->get($repType, $repId);
            $fileExtension = ReportConstant::REPORT_OUTPUT_CSV_FORMAT;
            $reportTech = '';
            if ($reportFormat = $this->reportQueue->reportFormat) {
                $fileExtension = strtolower($reportFormat->report_format_code);
                $reportTech = ReportConstant::GENERAL_TECH;
            }

            if ($report->getRepUserReportWpdf() == CommonConstant::DATABASE_VALUE_YES) {
                $fileExtension = ReportConstant::REPORT_OUTPUT_PDF_FORMAT;
            } elseif (in_array($report->getRepCode(), $this->reportService->getWkHtmlToPdfWhiteList())) {
                $fileExtension = ReportConstant::REPORT_OUTPUT_PDF_FORMAT;
                $reportTech = ReportConstant::WK_TECH;
            } elseif (in_array($report->getRepCode(), $this->reportService->getExcelWhiteList())) {
                $fileExtension = ReportConstant::REPORT_OUTPUT_XLSX_FORMAT;
                $reportTech = ReportConstant::PHP_EXCEL_TECH;
            } elseif (in_array($report->getRepCode(), $this->reportService->getCustomizeCsvWhiteList())) {
                $reportTech = ReportConstant::CUSTOMIZE_CSV_TECH;
            }

            $filterArr = json_decode($this->reportQueue->filter_query, true);
            $inputs = [
                'repId'            => $repId,
                'repType'          => $repType,
                'repDownloadPath'  => $storagePathway,
                'fileExtension'    => $fileExtension,
                'reportTech'       => $reportTech,
                'startRow'         => $this->reportQueue->start_row,
                'maxRows'          => $this->reportQueue->max_row,
                'runInBackground'  => true
            ];

            $returnData = $this->reportGenerateService->generateReport(array_merge($inputs, $filterArr));

            $this->processReport($returnData, $storagePathway, $repId, $repType);
        } catch (Exception $ex) {
            $this->reportQueue->details = ReportConstant::REPORT_ERROR_MSG;
            $this->updateReportQueueByStatus(ReportQueueStatus::FAIL, $this->reportQueue);
            $this->log->error($ex->getMessage(), [$this->reportQueue->report_queue_code]);
        } finally {
            $reportQueueControl->delete();
        }
    }

    private function processReport($returnData, $storagePathway, $repId, $repType)
    {
        if (!empty($returnData) && array_get($returnData, 'result', false)) {
            $repFileName = array_get($returnData, 'repFileName');
            $returnedRows = array_get($returnData, 'returnedRows');
            $rowNumbers = array_get($returnData, 'rowNumbers');
            $detail = '';
            if ($returnedRows) {
                $returnedRows = 'Returned Rows: ' . $returnedRows;
                $detail = $detail . $returnedRows;
            }
            if ($rowNumbers) {
                $rowNumbers = 'Row Numbers: ' . $rowNumbers;
                $detail = $detail . ($returnedRows ? ', ' : '') . $rowNumbers;
            }
            if ($this->reportQueue->email_report == CommonConstant::DATABASE_VALUE_YES) {
                /*Sent mail*/
                $repCode = array_get($returnData, 'repCode');
                $repFile = $storagePathway . $repFileName;
                $mailInput = [
                    'repId'    => $repId,
                    'repType'  => $repType,
                    'repCode'  => $repCode,
                    'repTitle' => $repCode,
                ];
                try {
                    $mailMsg = $this->reportService->sendEmail($mailInput, $repFile);
                    $detail = $mailMsg . ' ' . $detail;
                } catch (Exception $ex) {
                    $mailMsg = "Cannot send email of the report $repCode.";
                    $detail = $mailMsg . ' ' . $detail;
                    $this->log->error($ex->getMessage(), [$this->reportQueue->report_queue_code]);
                }
            }
            /*
             * Update report queue status and delete report queue control after generating
             * */
            $this->reportQueue->details = $detail;
            $this->reportQueue->report_file = $repFileName;
            $this->updateReportQueueByStatus(ReportQueueStatus::COMP, $this->reportQueue);
        } else {
            // format error to list
            $eList = [];
            $filterValidationError = array_get($returnData, 'filterValidation', null);
            $errors = ReportConstant::REPORT_ERROR_MSG;

            if ($filterValidationError) {
                $e = array_values($returnData['filterValidation']);
                foreach ($e as $_e) {
                    $eList = array_merge($eList, array_values($_e));
                }
                $errors = implode(', ', $eList);
            }

            $this->reportQueue->details = $errors;
            $this->updateReportQueueByStatus(ReportQueueStatus::FAIL, $this->reportQueue);
        }
    }

    private function newReportQueueControl($reportQueueId, $userId, $processId)
    {
        $reportQueueControl = new ReportQueueControl();
        $reportQueueControl->report_queue_id = $reportQueueId;
        $reportQueueControl->report_for_user_id = $userId;
        $reportQueueControl->system_process_id = $processId;
        $reportQueueControl->save();
        return $reportQueueControl;
    }
}
