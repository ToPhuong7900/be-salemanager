<?php

namespace Tfcloud\Services\Report;

use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapReportLoader;
use Tfcloud\Lib\BoxFilterQuery\IReportFilterQuery;
use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Lib\Constant\ReportConstant;
use Tfcloud\Lib\Constant\UserDefConstant;
use Tfcloud\Lib\Exceptions\StateException;
use Tfcloud\Lib\FixedAsset\CaFinYearManager;
use Tfcloud\Lib\Interfaces\InterfaceManager;
use Tfcloud\Lib\Permissions\RuralEstatesPermission;
use Tfcloud\lib\Reporting\Spout\IXlsxSpoutWriter;
use Tfcloud\lib\Reporting\Spout\SpoutFactory;
use Tfcloud\Lib\Reporting\wPDF;
use Tfcloud\Lib\Reporting\wpdf2\WPDF2;
use Tfcloud\Lib\TfHttp;
use Tfcloud\Models\AuditAction;
use Tfcloud\Models\CrudAccessLevel;
use Tfcloud\Models\EstReview;
use Tfcloud\Models\FinAccountType;
use Tfcloud\Models\FinAuthStatus;
use Tfcloud\Models\FinYear;
use Tfcloud\Models\FinYearPeriod;
use Tfcloud\Models\GenTable;
use Tfcloud\Models\GenUserdefLabel;
use Tfcloud\Models\HelpcallSurveyQst;
use Tfcloud\Models\InspectionSFG20\InspSFG20InspectionStatusType;
use Tfcloud\Models\InterfaceRunLog;
use Tfcloud\Models\Module;
use Tfcloud\Models\ProjectCashFlow;
use Tfcloud\Models\ProjectCashFlowAccount;
use Tfcloud\Models\ProjectRecordType;
use Tfcloud\Models\Report;
use Tfcloud\Models\ReportDateOption;
use Tfcloud\Models\ReportField;
use Tfcloud\Models\ReportFieldType;
use Tfcloud\Models\ReportOperatorType;
use Tfcloud\Models\SiteGroup;
use Tfcloud\Models\SystemReport;
use Tfcloud\Models\User;
use Tfcloud\Models\UserReport;
use Tfcloud\Models\UserReportField;
use Tfcloud\Models\UserReportFilter;
use Tfcloud\Models\UserReportPdfDesign;
use Tfcloud\Models\UserSiteAccess;
use Tfcloud\Models\VwAdm19;
use Tfcloud\Services\AuditService;
use Tfcloud\Services\BaseService;
use Tfcloud\Services\Dlo\JobLabourService;
use Tfcloud\Services\Estate\EstateUnitService;
use Tfcloud\Services\Estate\LeaseOutLetuService;
use Tfcloud\Services\Estate\LettableUnitService;
use Tfcloud\Services\Inspection\InspectionService;
use Tfcloud\Services\ModuleService;
use Tfcloud\Services\PermissionService;
use Tfcloud\Services\Project\ProjectCashFlowAccService;
use Tfcloud\Services\Report\FilterQuery\AdminCodeFilterQueryService;
use Tfcloud\Services\Report\FilterQuery\AllNotesFilterQueryService;
use Tfcloud\Services\Report\FilterQuery\BudgetsFilterQueryService;
use Tfcloud\Services\Report\FilterQuery\CapacitiesFilterQueryService;
use Tfcloud\Services\Report\FilterQuery\CaseManagementFilterQueryService;
use Tfcloud\Services\Report\FilterQuery\ConditionsFilterQueryService;
use Tfcloud\Services\Report\FilterQuery\ContractFilterQueryService;
use Tfcloud\Services\Report\FilterQuery\ContractInspectionFilterQueryService;
use Tfcloud\Services\Report\FilterQuery\CostPlusContractFilterQueryService;
use Tfcloud\Services\Report\FilterQuery\CostPlusSalesCreditFilterQueryService;
use Tfcloud\Services\Report\FilterQuery\CostPlusSalesInvoiceFilterQueryService;
use Tfcloud\Services\Report\FilterQuery\DatafeedFilterQueryService;
use Tfcloud\Services\Report\FilterQuery\DloJobsFilterQueryService;
use Tfcloud\services\Report\FilterQuery\EmailFilterQueryService;
use Tfcloud\Services\Report\FilterQuery\EstatesFilterQueryService;
use Tfcloud\Services\Report\FilterQuery\FixedAssetsBalanceSheetMovementsQueryService;
use Tfcloud\Services\Report\FilterQuery\FixedAssetsFAR02QueryService;
use Tfcloud\Services\Report\FilterQuery\FixedAssetsFilterQueryService;
use Tfcloud\Services\Report\FilterQuery\FixedAssetsHistoricBalancesQueryService;
use Tfcloud\Services\Report\FilterQuery\FixedAssetsHistoricTransactionQueryService;
use Tfcloud\Services\Report\FilterQuery\FixedAssetsMovementsOnBalancesAssetLandQueryService;
use Tfcloud\Services\Report\FilterQuery\FixedAssetsMovementsOnBalancesQueryService;
use Tfcloud\Services\Report\FilterQuery\GenChecklistFilterQueryService;
use Tfcloud\Services\Report\FilterQuery\GeneralsFilterQueryService;
use Tfcloud\Services\Report\FilterQuery\GroundsMaintenanceFilterQueryService;
use Tfcloud\Services\Report\FilterQuery\HelpcallsFilterQueryService;
use Tfcloud\Services\Report\FilterQuery\IncidentManagementQueryService;
use Tfcloud\Services\Report\FilterQuery\InspectionsNBS02FilterQueryService;
use Tfcloud\Services\Report\FilterQuery\InstructionsFilterQueryService;
use Tfcloud\Services\Report\FilterQuery\InstructionsNBS01FilterQueryService;
use Tfcloud\Services\Report\FilterQuery\InstructionsNE01FilterQueryService;
use Tfcloud\Services\Report\FilterQuery\InterfacesFilterQueryService;
use Tfcloud\Services\Report\FilterQuery\InvoicesFilterQueryService;
use Tfcloud\Services\Report\FilterQuery\InvoiceSORFilterQueryService;
use Tfcloud\Services\Report\FilterQuery\OccupancyFilterQueryService;
use Tfcloud\services\Report\FilterQuery\PermitToWorkFilterQueryService;
use Tfcloud\Services\Report\FilterQuery\PersonnelFilterQueryService;
use Tfcloud\Services\Report\FilterQuery\PlacementsFilterQueryService;
use Tfcloud\Services\Report\FilterQuery\PlacementSitesFilterQueryService;
use Tfcloud\Services\Report\FilterQuery\PlantFilterQueryService;
use Tfcloud\Services\Report\FilterQuery\PlantOutOfServiceHistoryQueryService;
use Tfcloud\Services\Report\FilterQuery\PppFilterQueryService;
use Tfcloud\Services\Report\FilterQuery\PppPupilsFilterQueryService;
use Tfcloud\Services\Report\FilterQuery\ProjectFundSourcePRJ24QueryService;
use Tfcloud\Services\Report\FilterQuery\ProjectsFilterQueryService;
use Tfcloud\Services\Report\FilterQuery\PropertiesFilterQueryService;
use Tfcloud\Services\Report\FilterQuery\QuestionnairesFilterQueryService;
use Tfcloud\Services\Report\FilterQuery\ResourceBookingsFilterQueryService;
use Tfcloud\Services\Report\FilterQuery\StockFilterQueryService;
use Tfcloud\Services\Report\FilterQuery\SufficienciesFilterQueryService;
use Tfcloud\Services\Report\FilterQuery\SuitabilitiesFilterQueryService;
use Tfcloud\Services\Report\FilterQuery\TransparencyFilterQueryService;
use Tfcloud\Services\Report\FilterQuery\TreeFilterQueryService;
use Tfcloud\Services\Report\FilterQuery\UsersFilterQueryService;
use Tfcloud\Services\Report\FilterQuery\UtilityFilterQueryService;
use Tfcloud\Services\Report\Grouping\ReportGroupingService;
use Tfcloud\Services\UserDefinedService;
use App\SBoxFilter\DLO\Report\DLO16FilterQuery;

class ReportGenerateService extends BaseService
{
    private const MAX_ROWS = 100000;
    /*
     * Tfcloud\Services\Report\ReportService
     */
    protected $reportService;
    private $allSiteAccess = true;
    private $csvExcelReportService;
    private $phpExcelReportService;
    private $wkReportService;
    private $reportGroupingService;
    private $userReportPdfDesignService;
    private $permissionService;
    private $interfaceManager;

    public function __construct(
        PermissionService $permissionService,
        ReportService $reportService,
        UserReportPdfDesignService $userReportPdfDesignService = null,
        WkReportService $wkReportService = null,
        PhpExcelReportService $phpExcelReportService = null,
        CsvExcelReportService $csvExcelReportService = null,
        ReportGroupingService $reportGroupingService = null
    ) {
        $this->permissionService = $permissionService;
        $this->reportService = $reportService;

        if ($userReportPdfDesignService) {
            $this->userReportPdfDesignService = $userReportPdfDesignService;
        } else {
            $this->userReportPdfDesignService = \Illuminate\Support\Facades\App::make(
                'Tfcloud\Services\Report\UserReportPdfDesignService'
            );
        }

        if ($wkReportService) {
            $this->wkReportService = $wkReportService;
        } else {
            $this->wkReportService = \Illuminate\Support\Facades\App::make(
                'Tfcloud\Services\Report\WkReportService'
            );
        }

        if ($phpExcelReportService) {
            $this->phpExcelReportService = $phpExcelReportService;
        } else {
            $this->phpExcelReportService = \Illuminate\Support\Facades\App::make(
                'Tfcloud\Services\Report\PhpExcelReportService'
            );
        }

        if ($csvExcelReportService) {
            $this->csvExcelReportService = $csvExcelReportService;
        } else {
            $this->csvExcelReportService = \Illuminate\Support\Facades\App::make(
                'Tfcloud\Services\Report\CsvExcelReportService'
            );
        }

        if ($reportGroupingService) {
            $this->reportGroupingService = $reportGroupingService;
        } else {
            $this->reportGroupingService = \Illuminate\Support\Facades\App::make(
                'Tfcloud\Services\Report\Grouping\ReportGroupingService'
            );
        }
    }

    public function generateReport($inputs)
    {
        \Log::info("generateReport()");
        $repTech = array_get($inputs, 'reportTech');

        $returnData = null;
        \Log::info("reportTech = $repTech");
        switch ($repTech) {
            case ReportConstant::WK_TECH:
                $returnData = $this->wkReportService->generateReportData($inputs);
                break;
            case ReportConstant::PHP_EXCEL_TECH:
                $returnData = $this->phpExcelReportService->reOutputPhpExcelData($inputs);
                break;
            case ReportConstant::CUSTOMIZE_CSV_TECH:
                $returnData = $this->csvExcelReportService->reOutputCsvData($inputs);
                break;
            case ReportConstant::GENERAL_TECH:
            default:
                // When running in background if no specific tech then ensure the Excel file isn't generated
                // to save time.
                $runInBackground = array_get($inputs, 'runInBackground', false);
                if ($runInBackground) {
                    // Handle for user define report which set report type is PDF design or
                    // In case fileExtension already is provided
                    $fileExtension = array_get($inputs, 'fileExtension', 'csv');
                    $inputs['fileExtension'] = $fileExtension;
                }
                $returnData = $this->reOutputData($inputs);
                break;
        }

        return $returnData;
    }

    public function reOutputWkhtmltopdfReport($inputs)
    {
        return $this->wkReportService->generateReportData($inputs);
    }

    public function reOutputWpdfReport($userReportId, $options = [])
    {
        $this->interfaceManager = new InterfaceManager();

        $report = $this->reportService->get(Report::USER_REPORT, $userReportId);

        if (!$report || !($report instanceof Report)) {
            $this->interfaceManager->Log(
                InterfaceRunLog::INTERFACE_RUN_STATUS_ERROR,
                "Invalid PDF default design.",
                false
            );
            throw new StateException("Invalid PDF default design.");
        }

        $groupByColumnId = $report->getRepUserReportWpdfDefaultGroupBy();

        // Get PDF design data from DB
        $pdfDesign = null;
        if ($report->getRepWpdf2() == CommonConstant::DATABASE_VALUE_YES) {
            $userReportPdfDesign = UserReportPdfDesign::where('user_report_id', $userReportId)->first();
            if ($userReportPdfDesign) {
                $pdfDesign = $userReportPdfDesign->pdf_design_content;
                $designId = $userReportPdfDesign->pdf_design_id;
            }
        } else {
            $designId = $report->getRepUserReportWpdfDefaultDesign();
            $pdfDesign = UserReportPdfDesign::select('pdf_design_content')->where('pdf_design_id', $designId)->first();
        }

        if (!$pdfDesign) {
            $this->interfaceManager->Log(
                InterfaceRunLog::INTERFACE_RUN_STATUS_SUCCESS_NO_DATA,
                "Cannot get PDF Design Content.",
                false
            );
            throw new StateException("Cannot get PDF Design Content.");
        }

        $strUrlStartRow = \Input::get('startrow');
        $strUrlMaxRow = \Input::get('maxrow');
        $startRow = 1;
        $maxRows = self::MAX_ROWS;
        if ($strUrlStartRow) {
            $iUrlStartRow = intval($strUrlStartRow);
            if ($iUrlStartRow) {
                $startRow = $iUrlStartRow;
            }
        }
        if ($strUrlMaxRow) {
            $iUrlMaxRow = intval($strUrlMaxRow);
            if ($iUrlMaxRow && $iUrlMaxRow < self::MAX_ROWS) {
                $maxRows = $iUrlMaxRow;
            }
        }

        return $this->reOutputData([
            'repType'               => Report::USER_REPORT,
            'repId'                 => $userReportId,
            'startRow'              => $startRow,
            'maxRows'               => $maxRows,
            'wpdf_default_group_by' => $groupByColumnId,
            'wpdf_default_design'   => $designId,
        ], $errMessages, $options);
    }

    /**
     * Get the report details
     */
    public function reOutputData($inputs, &$errMessages = [], $options = [])
    {
        $debugMode = true;
        // Default values
        $iEndRow = 0;
        $iTotal = 0;
        $iRetRows = 0;
        $reportFilterQuery = null;
        $numericCols = [];

        $iStartRow = isset($inputs['startRow']) ? (int) $inputs['startRow'] : 1;
        $iNumRows = isset($inputs['maxRows']) ? (int) $inputs['maxRows'] : 100000;

        //Todo: Grouping options
        $returnsForGrouping = array_get($options, 'returnsForGrouping', false);
        $grouping = array_get($options, 'grouping', false);
        $disableAudit = array_get($options, 'disableAudit', false);

        if ($iStartRow < 1) {
            $iStartRow = 1;
        }
        if ($iNumRows <= 0) {
            $iNumRows = 100000;
        }

        $repId = array_get($inputs, 'repId', null);
        $repType = array_get($inputs, 'repType', '');
        $repFormat = ReportConstant::REPORT_OUTPUT_CSV_FORMAT;

        $report = $this->reportService->get($repType, $repId);

        if (!$report instanceof Report) {
            return false;
        }

        $filterData = $inputs;
        foreach ($filterData as $key => $value) {
            if ((empty($value) && !is_numeric($value)) || $value == 'null') {
                unset($filterData[$key]);
            }
        }

        $sOrderText = "";               // Any order by text from the user define report
        $whereCodes = [];               // The codes which have been used in the filter
        $whereTexts = [];               // Plain text about filtering applied
        $orCodes = [];                  // csv string of OR codes
        $arrOrder = [];                 // Array of ORDER BY clauses.

        $bUseSiteGroupId = $report->getUseSiteGroupId();
        $bIgnoreSiteId = $report->getIgnoreSiteId();
        $repCode = $report->getRepCode();
        $repSystemReportCode = $report->getSystemReportCode();
        $bSqlReport = $repType == Report::USER_REPORT
            && $report->getRepUserReportUseQuery() == CommonConstant::DATABASE_VALUE_YES;

        $viewTable = $report->getRepViewName($filterData);

        if (!empty($repCode) && empty($viewTable)) {
            $viewTable = "vw_" .  strtolower($repSystemReportCode);
        }

        if (
            ($repCode == 'SUP01') ||
            ($repCode == 'SUP02') ||
            ($repCode == 'SUP03') ||
            ($repCode == 'SUP04') ||
            ($repCode == 'AUD01') ||
            ($repCode == ReportConstant::SYSTEM_REPORT_INS_CLI_RBWM01) ||
            ($repCode == 'FAR23') ||
            ($repCode == 'FAR24') ||
            ($repCode == 'FAR25') ||
            ($repCode == 'FAR26') ||
            ($repCode == 'FAR27') ||
            ($repCode == 'FAR28') ||
            ($repCode == 'FAR29') ||
            ($repCode == 'FAR30') ||
            ($repCode == 'FAR33') ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_RBY01) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_RBY02) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_RBY03) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_RBY04) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_RBY05) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_RBY06) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_RBY07) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_RBY08) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_RBY09) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_RBY10) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_MK01) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_MK02) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_MK03) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_MK04) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_MK05) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_MK06) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_MK07) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_MK08) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_MK09) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_MK10) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_CFF01) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_CFF02) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_CFF03) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_CFF04) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_CFF05) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_CFF06) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_CFF07) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_CFF08) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_CFF09) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_NMNI01) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_NMNI02) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_NMNI03) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_NMNI04) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_NMNI05) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_SAND01) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_GCC01) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_GCC02) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_GCC03) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_GCC04) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_GCC05) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_GCC06) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_NEWC01) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_NEWC02) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_NEWC03) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_NEWC04) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_NEWC05) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_NEWC06) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_NEWC07) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_NEWC08) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_FAR_CLI_NEWC09) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_PR_CLI_STFC01) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_PR_CLI_STFC02) ||
            ($repCode == 'DLO08') || ($repSystemReportCode == 'DLO08') ||
            ($repCode == ReportConstant::SYSTEM_REPORT_INT24) ||
            ($repCode == 'BUD01') ||
            ($repCode == 'BUD02') ||
            ($repCode == ReportConstant::SYSTEM_REPORT_BUD04) ||
            ($repCode == 'BUD03') ||
            ($repCode == ReportConstant::SYSTEM_REPORT_BUD05) ||
            ($repCode == 'PRJ15') ||
            ($repCode == ReportConstant::SYSTEM_REPORT_CONT_CLI_BRA01) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_INT_CLI_NEL01) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_HLP_CLI_RCBC01) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_PRJ24) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_HLP15) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_INT_CLI_CEC01) ||
            ($repCode == ReportConstant::SYSTEM_REPORT_UT10)
        ) {
            // Audit reports
            $repViewModel = null;
        } else {
            $viewModel = $this->viewFormat($viewTable);
            $viewNameModel = "\\Tfcloud\\Models\\Views\\$viewModel";

            if (class_exists($viewNameModel)) {
                $repViewModel = new $viewNameModel();
            } else {
                $sErrMsg = "Class '$viewNameModel' not found.";
                \Log::info("View '$viewTable' not found, check Model exists.");
                if ($debugMode) {
                    echo $sErrMsg;
                }
                return false;
            }
        }

        // Create a reports directory for the site group
        if ($repDownloadPath = array_get($inputs, 'repDownloadPath')) {
            $reportDir = $repDownloadPath;
            if (!is_dir($reportDir)) {
                return false;
            }
        } else {
            $reportDir = storage_path()
                . DIRECTORY_SEPARATOR . 'reports'
                . DIRECTORY_SEPARATOR . Auth::User()->site_group_id;
            if (!is_dir($reportDir)) {
                mkdir($reportDir, 0755);
            }

            $reportDir .= DIRECTORY_SEPARATOR . Auth::User()->id;
            if (!is_dir($reportDir)) {
                mkdir($reportDir, 0755);
            }

            //TODO: grouping > step 2 > make new $reportDir /reports/{siteGroupId}/{userId}/grouping
            if ($grouping) {
                $reportDir .= DIRECTORY_SEPARATOR . 'grouping';
                if (!is_dir($reportDir)) {
                    mkdir($reportDir, 0755);
                }

                $reportScheduleId = array_get($options, 'reportScheduleId', null);
                if ($reportScheduleId) {
                    $reportDir .= DIRECTORY_SEPARATOR . $reportScheduleId;
                    if (!is_dir($reportDir)) {
                        mkdir($reportDir, 0755);
                    }
                }
            }
        }

        // Temporary directory
        $tempDir = storage_path()
            . DIRECTORY_SEPARATOR . 'reports'
            . DIRECTORY_SEPARATOR . Auth::User()->site_group_id;
        if (!is_dir($tempDir)) {
            mkdir($tempDir, 0755);
        }
        $tempDir .= DIRECTORY_SEPARATOR . Auth::User()->id;
        if (!is_dir($tempDir)) {
            mkdir($tempDir, 0755);
        }
        $tempDir .= DIRECTORY_SEPARATOR . 'temp';
        if (!is_dir($tempDir)) {
            mkdir($tempDir, 0755);
        }

        $repCsvFile = $reportDir . DIRECTORY_SEPARATOR . $repCode . '.csv';
        $repXlsFile = $reportDir . DIRECTORY_SEPARATOR . $repCode . '.xlsx';
        $repPdfFile = $reportDir . DIRECTORY_SEPARATOR . $repCode . '.pdf';

        $repFileName = $repCode . '.csv';
        $repFilePath = $repCsvFile;
        $fileExtension = array_get($inputs, 'fileExtension');
        $usePdfTempFile = false;
        if ($repFormat = array_get($inputs, 'repFormat')) {
            switch ($repFormat) {
                case Report\ReportFormat::XLSX:
                case ReportConstant::REPORT_OUTPUT_XLSX_FORMAT:
                    $repFileName = $repCode . '.xlsx';
                    $repFilePath = $repXlsFile;
                    break;
                case Report\ReportFormat::CSV:
                case ReportConstant::REPORT_OUTPUT_CSV_FORMAT:
                default:
                    $repFileName = $repCode . '.csv';
                    $repFilePath = $repCsvFile;
                    break;
            }
        } elseif ($fileExtension) {
            switch ($fileExtension) {
                case ReportConstant::REPORT_OUTPUT_PDF_FORMAT:
                    $repFileName = $repCode . '.pdf';
                    $repFilePath = $tempDir . DIRECTORY_SEPARATOR . $repFileName;
                    $usePdfTempFile = true;
                    break;
                case ReportConstant::REPORT_OUTPUT_XLSX_FORMAT:
                    $repFileName = $repCode . '.xlsx';
                    $repFilePath = $repXlsFile;
                    break;
                case ReportConstant::REPORT_OUTPUT_CSV_FORMAT:
                default:
                    $repFileName = $repCode . '.csv';
                    $repFilePath = $repCsvFile;
                    break;
            }
        } elseif (empty($fileExtension) && array_get($inputs, 'outputOption') == 2) {
            /**
             * There are 2 cases that $fileExtension is null:
             * 1: export report from a list (not a new filter list) => expect CSV file
             * 2: export report from running report page and select XLSX format => expect XLSX file
             * Thus, include $inputs['outputOption'] == 2 to make sure it comes from running report page
             */
            $repFormat = ReportConstant::REPORT_OUTPUT_XLSX_FORMAT;
            $repFileName = $repCode . '.xlsx';
            $repFilePath = $repXlsFile;
        }

        $bAllSites = false;
        $siteAccess = Auth::User()->site_access;
        if ($siteAccess == UserSiteAccess::USER_SITE_ACCESS_ALL) {
            $bAllSites = true;
        }

        // Build the raw SQL query statement
        $sQuery = '';

        $userQueryError = false;
        /**
         * User Defined Report
         **/
        if ($repType == Report::USER_REPORT) {
            // Query Report
            if ($report->getRepUserReportUseQuery() == CommonConstant::DATABASE_VALUE_YES) {
                // The query is supplied by the user
                $sQuery = $report->getRepUserReportQuery();
                if (trim($sQuery)) {
                    try {
                        $userQuery = DB::connection('mysqlReadView')->select(DB::raw($sQuery));
                    } catch (Exception $e) {
                        $userQuery = [];
                        $userQueryError = $e->getMessage();
                    }
                } else {
                    $userQuery = [];
                }
            } else {
                // It is a user defined report, I only want some of the fields and in a particular order.
                $sUserDefRepFields = $this->reGetUserDefRepQuery($report, $viewTable);

                if (empty(trim($sUserDefRepFields))) {
                    $sUserDefRepFields = "'' AS `None Output fields`";
                }

                if ($repSystemReportCode == ReportConstant::SYSTEM_REPORT_INS02) {
                    $sUserDefRepFields = "DISTINCT $sUserDefRepFields";
                } elseif ($repSystemReportCode == ReportConstant::SYSTEM_REPORT_ADM03) {
                    $sUserDefRepFields .= ", contact_id";
                } elseif (
                    $repSystemReportCode == ReportConstant::SYSTEM_REPORT_PRJ10
                    || $repSystemReportCode == ReportConstant::SYSTEM_REPORT_PRJ16
                ) {
                    $sUserDefRepFields .= ", project_cash_flow_id";
                } elseif ($repSystemReportCode == ReportConstant::SYSTEM_REPORT_PL03) {
                    $sUserDefRepFields = PlantOutOfServiceHistoryQueryService::pl03ReplaceColumn($sUserDefRepFields);
                    $sUserDefRepFields .=
                        ", parent_desc, plant_oos_record_type_desc, parent_module_id, parent_logged_by";
                }

                if ($bAllSites || $bIgnoreSiteId) {
                    if ($repSystemReportCode == ReportConstant::SYSTEM_REPORT_ES24) {
                        $estatesFilterQueryService = new EstatesFilterQueryService($this->permissionService);
                        $query = $estatesFilterQueryService->reGetLeaseInAuditQuery();
                    } elseif ($repSystemReportCode == ReportConstant::SYSTEM_REPORT_ES25) {
                        $estatesFilterQueryService = new EstatesFilterQueryService($this->permissionService);
                        $query = $estatesFilterQueryService->reAddLeaseOutAuditQuery();
                    } elseif ($repSystemReportCode == ReportConstant::SYSTEM_REPORT_INT07) {
                        $repViewModel = null;
                        $budgetsFilterQueryService = new BudgetsFilterQueryService($this->permissionService);
                        $reportQuery = $budgetsFilterQueryService->getAllBudgets();
                        $newReportFieldCodes = $this->reGetUserDefRepNoViewArray($report);

                        $filterFields = 'inner join fin_account on (fin_account.fin_account_id = tmp.fin_account_id)';
                        $filterFields .= 'inner join fin_year on (fin_year.fin_year_id = tmp.fin_year_id)';

                        $query = DB::table(
                            DB::raw("({$reportQuery->toSql()}) as tmp $filterFields")
                        )
                            ->select(
                                $newReportFieldCodes
                            );
                        $query->mergeBindings($reportQuery->getQuery());
                    } elseif ($repSystemReportCode == ReportConstant::SYSTEM_REPORT_DLO08) {
                        $repViewModel = null;
                        $jobLabService = new JobLabourService($this->permissionService);
                        $query = $jobLabService->filterDlo08Report($inputs);
                        $sOrderText = "";
                    } else {
                        $query = $repViewModel::select(DB::raw($sUserDefRepFields));
                        if ($bUseSiteGroupId) {
                            $query->where("{$repViewModel->getTable()}.site_group_id", Auth::User()->site_group_id);
                        }
                    }
                } else {
                    // Restrict the number of records shown. This works because
                    // every view has to return site_id as a field if they have one.
                    $query = $this->selectedSiteAccessQuery($repSystemReportCode, $repViewModel, $sUserDefRepFields);

                    if ($bUseSiteGroupId) {
                        $query->where("{$repViewModel->getTable()}.site_group_id", Auth::User()->site_group_id);
                    }

                    if ($repSystemReportCode == ReportConstant::SYSTEM_REPORT_ADM03) {
                        $query->where(function ($subQuery) {
                            $subQuery->whereNull('user_access.user_id');
                            $subQuery->orWhere('user_access.user_id', Auth::User()->id);
                        });
                    } else {
                        if ($this->allSiteAccess) {
                            $query->where('user_access.user_id', Auth::User()->id);
                        }
                    }
                }

                // Fix CLD-5232
                // This piece of code would have to be removed if
                // deed packet or location is added to output report file
                if (
                    $repSystemReportCode == ReportConstant::SYSTEM_REPORT_ES22
                ) {
                    $query->groupBy('freehold_id');
                }
                if ($repSystemReportCode == ReportConstant::SYSTEM_REPORT_ES22) {
                    $query->groupBy('est_agreement_id');
                }

                if ($repSystemReportCode == ReportConstant::SYSTEM_REPORT_ES16) {
                    if (!RuralEstatesPermission::hasModuleAccessRead(false)) {
                        $query->where("{$repViewModel->getTable()}.rural_est_note", 'N');
                    }
                }
                // Filters Applied
                $this->reAddUserDefWhere($repId, $query, $arrOrder, $sOrderText);
            }
        /**
         * System Report
         **/
        } else {
            if ($repCode == ReportConstant::SYSTEM_REPORT_PRJ_CLI_SUT01) {
                $repViewModel = null;
                $queryService = new SuttonReportService($this->permissionService, $report);
                $queryService->runProfitAndLossReport($filterData);
                $query = $queryService->runReport($filterData);
            } elseif ($repCode == ReportConstant::SYSTEM_REPORT_INT07) {
                $repViewModel = null;

                $budgetsFilterQueryService = new BudgetsFilterQueryService($this->permissionService);
                $reportQuery = $budgetsFilterQueryService->getAllBudgets();
                $reportFieldCodes = $budgetsFilterQueryService->reportField();
                $newReportFieldCodes = explode(',', $reportFieldCodes);

                $filterFields = 'inner join fin_account on (fin_account.fin_account_id = tmp.fin_account_id)';
                $filterFields .= 'inner join fin_year on (fin_year.fin_year_id = tmp.fin_year_id)';

                $query = DB::table(
                    DB::raw(
                        "({$reportQuery->toSql()}) as tmp $filterFields"
                    )
                )
                    ->select(
                        $newReportFieldCodes
                    );

                $query->mergeBindings(
                    $reportQuery->getQuery()
                );

                $this->repGetReportOrder(ReportConstant::SYSTEM_REPORT, $repId, $arrOrder, $sOrderText);
            } elseif ($repCode == 'FAR01' || $repCode == 'FAR31') {
                $repViewModel = null;
                $queryService = new FixedAssetsMovementsOnBalancesQueryService(
                    $this->permissionService,
                    $filterData,
                    $repCode,
                    $report
                );

                $query = $queryService->movementsOnBalancesQuery($whereCodes, $orCodes, $whereTexts);
                $numericCols = $queryService->getnumericCols();
                $this->repGetReportOrder(ReportConstant::SYSTEM_REPORT, $repId, $arrOrder, $sOrderText);
            } elseif ($repCode == 'FAR38') {
                $repViewModel = null;
                $queryService = new FixedAssetsHistoricBalancesQueryService(
                    $this->permissionService,
                    $filterData,
                    $repCode
                );
                $query = $queryService->historicBalancesQuery($inputs, $whereCodes, $orCodes, $whereTexts);
                $numericCols = $queryService->getnumericCols();
                $this->repGetReportOrder(ReportConstant::SYSTEM_REPORT, $repId, $arrOrder, $sOrderText);
            } elseif ($repCode == 'FAR39') {
                $repViewModel = null;
                $queryService = new FixedAssetsHistoricTransactionQueryService(
                    $this->permissionService,
                    $filterData,
                    $repCode
                );
                $query = $queryService->historicTransactionQuery($inputs, $whereCodes, $orCodes, $whereTexts);
                $numericCols = $queryService->getnumericCols();
                $this->repGetReportOrder(ReportConstant::SYSTEM_REPORT, $repId, $arrOrder, $sOrderText);
            } elseif ($repCode == ReportConstant::SYSTEM_REPORT_PRJ24) {
                $repViewModel = null;

                $reportBoxFilterLoader = new ClassMapReportLoader($report);
                $reportFilterQuery = $reportBoxFilterLoader->getFilterQuery($filterData);

                $filterParams = [];
                $filterFields = $reportFilterQuery->getFilterSet()->getFilterFields();

                foreach ($filterFields as $filter) {
                    $param = TfHttp::getBoxFilterParams(
                        $filter->getFieldName(),
                        $filter->getFilterOperators()[0]->getOperator(),
                        $filter->getFilterOperators()[0]->getValues(),
                        null
                    );
                    $filterParams = array_merge($filterParams, $param);
                }

                $queryService = new ProjectFundSourcePRJ24QueryService(
                    $this->permissionService,
                    $filterParams,
                    $repCode
                );

                $query = $queryService->projectFundSourceQuery($whereCodes, $orCodes, $whereTexts);
                $this->repGetReportOrder(ReportConstant::SYSTEM_REPORT, $repId, $arrOrder, $sOrderText);
                // This fiddle prevents the box filter query being added to the SQL. Filter being processed
                // by the report class.
                $report->setRepUserReportUseQuery(CommonConstant::DATABASE_VALUE_YES);
            } elseif ($repCode == ReportConstant::SYSTEM_REPORT_FAR37) {
                $repViewModel = null;
                $queryService = new FixedAssetsBalanceSheetMovementsQueryService(
                    $this->permissionService,
                    $filterData,
                    $repCode,
                    $report
                );
                $query = $queryService->balanceSheetMovementsQuery($whereCodes, $orCodes, $whereTexts);
                $numericCols = $queryService->getnumericCols();
                $this->repGetReportOrder(ReportConstant::SYSTEM_REPORT, $repId, $arrOrder, $sOrderText);
            } elseif ($repCode == 'FAR32') {
                $repViewModel = null;
                $queryService = new FixedAssetsMovementsOnBalancesAssetLandQueryService(
                    $this->permissionService,
                    $filterData,
                    $repCode,
                    $report
                );
                $query = $queryService->movementsOnBalancesQuery();
                $this->repGetReportOrder(ReportConstant::SYSTEM_REPORT, $repId, $arrOrder, $sOrderText);
            } elseif ($repCode == 'FAR02') {
                $repViewModel = null;
                $queryService = new FixedAssetsFAR02QueryService($this->permissionService, $filterData, $report);
                $query = $queryService->movementsInNetBookValueQuery();
                $numericCols = $queryService->getnumericCols();
                $this->repGetReportOrder(ReportConstant::SYSTEM_REPORT, $repId, $arrOrder, $sOrderText);
            } elseif ($repCode == ReportConstant::SYSTEM_REPORT_FAR08) {
                // Report FAR08: Revaluation Reserve Statement
                $repViewModel = null;

                $queryService = new FixedAssetsFilterQueryService($this->permissionService, $filterData, $report);
                $query = $queryService->reAddRevaluationReserveStatementQuery(
                    $filterData,
                    $whereCodes,
                    $orCodes,
                    $whereTexts,
                    $numericCols
                );
                $this->repGetReportOrder(ReportConstant::SYSTEM_REPORT, $repId, $arrOrder, $sOrderText);
            } elseif ($repCode == ReportConstant::SYSTEM_REPORT_CONT_CLI_BRA01) {
                $repViewModel = null;
                $queryService = new ContractInspectionFilterQueryService($this->permissionService, $report);
                $query = $queryService->reAddContractScheduledInspectionQuery(
                    $filterData,
                    $whereCodes,
                    $orCodes,
                    $whereTexts
                );
                $this->repGetReportOrder(ReportConstant::SYSTEM_REPORT, $repId, $arrOrder, $sOrderText);
            } elseif (
                ($repCode == 'SUP01') ||
                ($repCode == 'SUP02') ||
                ($repCode == 'SUP03') ||
                ($repCode == 'SUP04')
            ) {
                $repViewModel = null;
                $generalFilterQueryService = new GeneralsFilterQueryService($this->permissionService);
                $query = $generalFilterQueryService->reGetSupAuditQuery();
                $arrOrder[] = [
                    'key' => '`Audit Id`',
                    'dir' => 'DESC'
                ];
                $sOrderText = "Audit Id (descending)";
            } elseif (($repCode == ReportConstant::SYSTEM_REPORT_AUD01)) {
                $repViewModel = null;
                $auditService = new AuditService();
                $query = $auditService->allHistoryList($inputs);
                $sOrderText = "Audit Id (descending)";
            } elseif (($repCode == ReportConstant::SYSTEM_REPORT_DLO08) ||  ($repSystemReportCode == 'DLO08')) {
                $repViewModel = null;
                $jobLabService = new JobLabourService($this->permissionService);
                $query = $jobLabService->filterDlo08Report($inputs);
                $sOrderText = "";
            } elseif (($repCode == ReportConstant::SYSTEM_REPORT_ADM19)) {
                $repViewModel = null;
                $moduleService = new ModuleService($this->permissionService);
                $modules = $moduleService->getAll()->get()->toArray();
                $columns = array_pluck($modules, 'module_description');
                $select = ['User Group Code', 'User Group Description', 'User Group Type'];
                $query = VwAdm19::userSiteGroup()
                    ->select(array_merge($select, $columns))
                    ->orderBy('User Group Code');
                $sOrderText = "";
            } elseif ($repCode == ReportConstant::SYSTEM_REPORT_DLO16) {
                $repViewModel = null;
                $query = (new DLO16FilterQuery())->getOperativeUnionQuery(false);
            } elseif ($repCode == ReportConstant::SYSTEM_REPORT_PRJ15) {
                $repViewModel = null;
                $projectsFilterQueryService = new ProjectsFilterQueryService($this->permissionService);
                $query = $projectsFilterQueryService->projectAdditionalContacts();
            } elseif ($repCode == ReportConstant::SYSTEM_REPORT_ES48) {
                $repViewModel = null;
                $estRecordService = new \Tfcloud\Services\Estate\EstRecordService($this->permissionService);
                $query = $estRecordService->all(true, $filterData);
            } elseif ($repCode == ReportConstant::SYSTEM_REPORT_ES59) {
                $repViewModel = null;
                $estUnitRecordService = new EstateUnitService(
                    $this->permissionService,
                    new LeaseOutLetuService($this->permissionService)
                );
                $query = $estUnitRecordService->getAll($filterData);
                $numericCols = [8];
            } elseif (($repCode == ReportConstant::SYSTEM_REPORT_INT24)) {
                // Spend by Site.
                $budgetsFilterQueryService = new BudgetsFilterQueryService($this->permissionService);
                $inputs['type'] = FinAccountType::EXPENDITURE;
                $query = $budgetsFilterQueryService->reAddSpendBySiteExpenditureBudgetQuery(
                    $inputs,
                    $whereCodes,
                    $orCodes,
                    $whereTexts
                );

                $sOrderText = "";
            } elseif (($repCode == ReportConstant::SYSTEM_REPORT_BUD01)) {
                $budgetsFilterQueryService = new BudgetsFilterQueryService($this->permissionService);
                $inputs['type'] = FinAccountType::EXPENDITURE;
                //fix bug move report to new filter skip check fin_account.active when show_inactive_codes
                $inputs['status'] = 'all';
                $query = $budgetsFilterQueryService->reAddExpenditureBudgetQuery(
                    $inputs,
                    $whereCodes,
                    $orCodes,
                    $whereTexts
                );

                $sOrderText = "";
            } elseif (($repCode == ReportConstant::SYSTEM_REPORT_BUD02)) {
                $budgetsFilterQueryService = new BudgetsFilterQueryService($this->permissionService);
                $inputs['type'] = FinAccountType::INCOME;
                //fix bug move report to new filter skip check fin_account.active when show_inactive_codes
                $inputs['status'] = 'all';
                $query = $budgetsFilterQueryService->reAddEstatesBudgetQuery(
                    $inputs,
                    $whereCodes,
                    $orCodes,
                    $whereTexts
                );

                $sOrderText = "";
            } elseif (($repCode == ReportConstant::SYSTEM_REPORT_BUD04)) {
                $budgetsFilterQueryService = new BudgetsFilterQueryService($this->permissionService);
                //fix bug move report to new filter skip check fin_account.active when show_inactive_codes
                $inputs['status'] = 'all';
                $query = $budgetsFilterQueryService->reAddEstatesExpenditureBudgetQuery(
                    $inputs,
                    $whereCodes,
                    $orCodes,
                    $whereTexts
                );

                $sOrderText = "";
            } elseif (($repCode == ReportConstant::SYSTEM_REPORT_BUD05)) {
                $budgetsFilterQueryService = new BudgetsFilterQueryService($this->permissionService);
                $query = $budgetsFilterQueryService->reAddEstatesRentalPaymentBreakdownQuery(
                    $inputs,
                    $whereCodes,
                    $orCodes,
                    $whereTexts
                );

                $sOrderText = "";
            } elseif (($repCode == ReportConstant::SYSTEM_REPORT_BUD03)) {
                $budgetsFilterQueryService = new BudgetsFilterQueryService($this->permissionService);
                $query = $budgetsFilterQueryService->reAddSalesInvoiceQuery(
                    $inputs,
                    $whereCodes,
                    $orCodes,
                    $whereTexts
                );

                $sOrderText = "";
            } elseif ($repCode == ReportConstant::SYSTEM_REPORT_ES24) {
                $repViewModel = null;
                $estatesFilterQueryService = new EstatesFilterQueryService($this->permissionService);
                $query = $estatesFilterQueryService->reGetLeaseInAuditQuery();
                $this->repGetReportOrder(ReportConstant::SYSTEM_REPORT, $repId, $arrOrder, $sOrderText);
            } elseif ($repCode == ReportConstant::SYSTEM_REPORT_ES25) {
                $repViewModel = null;
                $estatesFilterQueryService = new EstatesFilterQueryService($this->permissionService);
                $query = $estatesFilterQueryService->reAddLeaseOutAuditQuery();
                $this->repGetReportOrder(ReportConstant::SYSTEM_REPORT, $repId, $arrOrder, $sOrderText);
            } elseif ($repCode == ReportConstant::SYSTEM_REPORT_HLP12) {
                $repViewModel = null;
                $queryService = new HelpcallsFilterQueryService($this->permissionService);
                $query = $queryService->hlp12ResultsQuery($filterData, $report, $whereTexts);
            } elseif ($repCode == ReportConstant::SYSTEM_REPORT_INT_CLI_NEL01) {
                $repViewModel = null;
                $queryService = new InstructionsNE01FilterQueryService($this->permissionService, $report);
                $query = $queryService->reAddInstructionQuery(
                    $filterData,
                    $whereCodes,
                    $orCodes,
                    $whereTexts
                );
            } elseif ($repCode == ReportConstant::SYSTEM_REPORT_HLP15) {
                $repViewModel = null;
                $queryService = new HelpcallsFilterQueryService($this->permissionService);
                $query = $queryService->hlp15ResultsQuery($filterData, $report, $whereTexts);
            } else {
                // Fetch columns to select from the view
                $reportColumns = $this->reGetReportColumns($report, $viewTable);

                // Addition selection fields
                // PR14 need site_id to be passed to reGetPropertySummaryData()
                if ($repCode == ReportConstant::SYSTEM_REPORT_PR14) {
                    $reportColumns .= ", site_id, building_id";
                } elseif ($repCode == ReportConstant::SYSTEM_REPORT_ADM03) {
                    $reportColumns .= ", contact_id";
                } elseif ($repCode == ReportConstant::SYSTEM_REPORT_PRJ10) {
                    $reportColumns .= ", project_cash_flow_id, project_cash_flow_acc_id";
                } elseif ($repCode == ReportConstant::SYSTEM_REPORT_PRJ16) {
                    $reportColumns .= ", project_id, fin_account_id, project_budget_line_id";
                } elseif ($repCode == ReportConstant::SYSTEM_REPORT_INS02) {
                    $reportColumns = "DISTINCT $reportColumns";
                } elseif ($repCode == ReportConstant::SYSTEM_REPORT_HLP09) {
                    $additionalColumns = ReportField::select(['report_field_code'])
                            ->leftjoin(
                                'helpcall_survey_qst',
                                'helpcall_survey_qst.helpcall_survey_qst_code',
                                '=',
                                'report_field.report_field_code'
                            )
                            ->where([
                                ['helpcall_survey_qst.site_group_id', '=', Auth::User()->site_group_id],
                                ['report_field.output_by_default', '=', 'N']
                            ]);

                    if (
                        isset($filterData['originalQuestions']) && $filterData['originalQuestions']
                        == CommonConstant::DATABASE_VALUE_YES
                        || isset($filterData['extraFilters']['originalQuestions'])
                        && $filterData['extraFilters']['originalQuestions'] == CommonConstant::DATABASE_VALUE_YES
                    ) {
                        $additionalColumns->where('helpcall_survey_qst.active', '=', 'Y');
                    }

                    $columns = $additionalColumns->get();

                    foreach ($columns as $additionalColumn) {
                        $reportColumns .=  ", vw_hlp09.`" . $additionalColumn->report_field_code . "`";
                    }
                } elseif ($repCode == ReportConstant::SYSTEM_REPORT_ES04) {
                    if (!RuralEstatesPermission::hasModuleLicence(false)) {
                        // vw_es04.`Rural Lease`,
                        $reportColumns = str_replace("vw_es04.`Rural Lease`,", "", $reportColumns);
                    }
                } elseif ($repCode == ReportConstant::SYSTEM_REPORT_PL03) {
                    $reportColumns = PlantOutOfServiceHistoryQueryService::pl03ReplaceColumn($reportColumns);
                    $reportColumns .=
                        ", parent_desc, plant_oos_record_type_desc, parent_module_id, parent_logged_by";
                } elseif ($repCode == ReportConstant::SYSTEM_REPORT_ES70) {
                    $reportColumns .= ", vw_es70.lettable_unit_id";
                } elseif ($repCode == ReportConstant::SYSTEM_REPORT_FAR07) {
                    $reportColumns = FixedAssetsFilterQueryService::far07ReplaceColumn($reportColumns);
                }

                if ($bAllSites || $bIgnoreSiteId) {
                    $query = $repViewModel::select(DB::raw($reportColumns));

                    if ($bUseSiteGroupId) {
                        $query->where("{$repViewModel->getTable()}.site_group_id", Auth::User()->site_group_id);
                    }
                } else {
                    // Restrict the number of records shown. This works because
                    // every view has to return site_id as a field if they have one.

                    $query = $this->selectedSiteAccessQuery($repCode, $repViewModel, $reportColumns);

                    if ($bUseSiteGroupId) {
                        $query->where("{$repViewModel->getTable()}.site_group_id", Auth::User()->site_group_id);
                    }

                    if ($repCode == ReportConstant::SYSTEM_REPORT_ADM03) {
                        $query->where(function ($subQuery) {
                            $subQuery->whereNull('user_access.user_id');
                            $subQuery->orWhere('user_access.user_id', Auth::User()->id);
                        });
                    } else {
                        if ($this->allSiteAccess) {
                            $query->where('user_access.user_id', Auth::User()->id);
                        }
                    }
                }

                if ($repCode == ReportConstant::SYSTEM_REPORT_FAR09) {
                    FixedAssetsFilterQueryService::far09SetInvestment($query);
                }

                if ($repCode == ReportConstant::SYSTEM_REPORT_ADM15) {
                    $generalFilterQueryService = new GeneralsFilterQueryService($this->permissionService);
                    $query->whereRaw(
                        'template_type_id IN (' .
                        Common::getSql($generalFilterQueryService->reAddTemplateTypeQuery()) . ')'
                    );
                }
                if ($repCode == ReportConstant::SYSTEM_REPORT_INT_CLI_PP01) {
                    //Set the from and two dates for the CompletionDate column
                    //This is both instruction.target_date and inspection.inspection_due_date
                    if (isset($filterData['Date_From'])) {
                        $dateFrom = \DateTime::createFromFormat('d/m/Y', $filterData['Date_From']);
                        $fromDate = $dateFrom->format('Y-m-d');
                        $query->whereRaw('DueDate >= ' . "'" . $fromDate . "'");
                    }

                    if (isset($filterData['Date_To'])) {
                        $dateTo = \DateTime::createFromFormat('d/m/Y', $filterData['Date_To']);
                        $toDate = $dateTo->format('Y-m-d');
                        $query->whereRaw("DueDate <= " . "'" . $toDate . "'");
                    }
                }
                if ($repCode == ReportConstant::SYSTEM_REPORT_ES16) {
                    if (!RuralEstatesPermission::hasModuleAccessRead(false)) {
                        $query->where("{$repViewModel->getTable()}.rural_est_note", 'N');
                    }
                }
                // Fix CLD-5232
                // This piece of code would have to be removed if
                // deed packet or location is added to output report file
                if ($repCode == ReportConstant::SYSTEM_REPORT_ES22) {
                    $query->groupBy('freehold_id');
                }
                if ($repSystemReportCode == ReportConstant::SYSTEM_REPORT_ES22) {
                    $query->groupBy('est_agreement_id');
                }

                 // CLD-16352: Applications linked to Certificate export doesnt have a filter
                // so needed to add contract certificate id filter here to
                // ensure that the report only inclued applications for the certificate.
                if ($repCode == ReportConstant::SYSTEM_REPORT_CONT12) {
                    if (isset($filterData['contractCertificateId'])) {
                        $certId = $filterData['contractCertificateId'];
                        $query->where("contract_certificate_id", $certId);
                        array_push($whereCodes, [
                            'contract_certificate',
                            'contract_certificate_id',
                            'contract_certificate_code',
                            $certId,
                            "Certificate Code"
                        ]);
                    }
                }

                // Now sort out any order by stuff that has been set up for the system report
                $this->repGetReportOrder("system_report", $repId, $arrOrder, $sOrderText);
            }
        }

        //echo '<pre>DEBUG START ==> ['. $query->toSql() . '] <== DEBUG END</pre>';exit;
        // Clone query to get total records without filter
        if (
            $repType != Report::USER_REPORT
            || $report->getRepUserReportUseQuery() != CommonConstant::DATABASE_VALUE_YES
        ) {
            $totalQuery = clone $query;
        }

        $reportBoxFilterLoader = new ClassMapReportLoader($report);
        $hasBoxFilter = $reportBoxFilterLoader->hasBoxFilter();

        if (
            ($hasBoxFilter || $report->getRepFilterable()) &&
            $report->getRepUserReportUseQuery() == CommonConstant::DATABASE_VALUE_NO
        ) {
            if ($report->getRepLocMax()) {
                $siteId = isset($filterData['site_id']) ? $filterData['site_id'] : null;

                if ($repCode == ReportConstant::SYSTEM_REPORT_PR02 && isset($filterData['siteId'])) {
                    $siteId = $filterData['siteId'];
                }

                if ($siteId) {
                    if ($repViewModel != null) {
                        $query->where("{$repViewModel->getTable()}.site_id", $siteId);
                    }

                    array_push($whereCodes, ['site', 'site_id', 'site_code', $siteId, "Site Code"]);
                }

                if ($report->getRepLocMax() > 1) {
                    $buildingId = isset($filterData['building_id']) ? $filterData['building_id'] : null;
                    if ($buildingId) {
                        if ($repViewModel != null) {
                            $query->where("{$repViewModel->getTable()}.building_id", $buildingId);
                        }
                        array_push(
                            $whereCodes,
                            ['building', 'building_id', 'building_code', $buildingId, "Building Code"]
                        );
                    }
                }

                if ($report->getRepLocMax() > 2) {
                    $roomId = isset($filterData['room_id']) ? $filterData['room_id'] : null;
                    if ($roomId) {
                        $query->where("{$repViewModel->getTable()}.room_id", $roomId);

                        array_push($whereCodes, ['room', 'room_id', 'room_number', $roomId, "Room Number"]);
                    }
                }
            }

            $reportFilterQuery =  $hasBoxFilter ? $reportBoxFilterLoader->getFilterQuery($filterData) : null;
            if ($reportFilterQuery && $reportFilterQuery->isApplyFilterQuery()) {
                $inputs = $reportFilterQuery->formatReportInputs($inputs);
                $siteIdInputs = $reportFilterQuery->getFilterField('site_id') ? ['site_id' => true] : null;
                $reportFilterQuery->setViewName($report->getRepViewName($siteIdInputs));
                $reportFilterQuery->handleReportMode();
                $query = $reportFilterQuery->filterAll($query);
            } else {
                /* Scope by filter */
                $query =
                    $this->reAddSpecificQuery($report, $filterData, $query, $whereCodes, $orCodes, $whereTexts);
            }
        }

        if (!$bAllSites) {
            $sQuery = str_replace("(site_id =", "($viewTable.site_id =", $sQuery);
        }

        // Now add any extra ordering required
        if ($repCode == ReportConstant::SYSTEM_REPORT_DOC01) {
            // This union needs ordering with the newest records first
            $arrOrder[] = [
                'key' => 'raw_date',
                'dir' => 'DESC'
            ];
        }
        // Now prepare to output the data
        $repTitle = $report->getRepTitle();
        $iStartRowNum = $iStartRow - 1;

        // wPDF
        $jsonData = "";
        $groupByColumn = "";
        $wpdfTotals = [];

        if ($report->getRepUserReportWpdf() == CommonConstant::DATABASE_VALUE_YES) {
            $wpdfDefaultDesign = $report->getRepUserReportWpdfDefaultDesign();
            if ($wpdfDefaultDesign) {
                $pdfDesign = UserReportPdfDesign::where('pdf_design_id', $wpdfDefaultDesign)
                    ->where('user_report_id', $repId)->first();
            } else {
                $pdfDesign = UserReportPdfDesign::where('user_report_id', $repId)->first();
            }

            if (!$pdfDesign) {
                $errMessages = ['PDF_Designer' => "Report Designer not configured."];
                return false;
            }

            $jsonData = $pdfDesign->pdf_design_content;

            $wpdfDefaultGroupBy = array_get($inputs, 'wpdf_default_group_by');
            if ($wpdfDefaultGroupBy) {
                $wpdfDefaultGroupByField = ReportField::where('report_field_id', $wpdfDefaultGroupBy)->first();
                if ($wpdfDefaultGroupByField) {
                    $groupByColumn = $wpdfDefaultGroupByField->report_field_code;

                    $summedNumericFields = [];
                    $numericType = ReportFieldType::REP_FIELD_TYPE_NUMERIC;

                    // Fetch numeric report fields.
                    $numericReportFields = ReportField
                        ::join('user_report', 'user_report.system_report_id', '=', 'report_field.system_report_id')
                        ->where('user_report.user_report_id', $repId)
                        ->where('report_field_type_id', DB::raw($numericType))
                        ->get();

                    foreach ($numericReportFields as $numericReportField) {
                        $reportFieldCode = $numericReportField->report_field_code;

                        $summedNumericFields[] = " SUM(`$reportFieldCode`) AS 'SUM $reportFieldCode' ";
                    }

                    $wpdfQuery = clone $query;

                    $wpdfTotals = $this->getSummedTotals(
                        $wpdfQuery,
                        $groupByColumn,
                        $summedNumericFields,
                        $arrOrder,
                        $iStartRowNum,
                        $iNumRows
                    );
                }
            }
        }

        // Get Result
        $results = [];
        $usingCursor = false;
        if (
            $repType == Report::USER_REPORT
            && $report->getRepUserReportUseQuery() == CommonConstant::DATABASE_VALUE_YES
        ) {
            $iTotal = count($userQuery);
            $results = $userQuery;
        } else {
            // Append all ORDER BY options Unless this is a user defined report which uses queries.
            if ($groupByColumn) {
                $query->orderBy(DB::raw("`$groupByColumn`"), 'ASC');
            }
            // wpdf2
            if (!empty($arrOrder) && $report->wpdf2 != CommonConstant::DATABASE_VALUE_YES) {
                foreach ($arrOrder as $order) {
                    $query->orderBy(DB::raw($order['key']), DB::raw($order['dir']));
                }
            }

            $query->skip($iStartRowNum)->take($iNumRows);

            if ($returnsForGrouping) {
                return [
                    'query' => $query,
                    'repType' => $repType,
                    'options' => $options,
                ];
            }

            if ($grouping) {
                $query = $this->groupingQuery($query, $options);
            }

            $returnQuery = clone $query;

            if ($repViewModel) {
                $iTotal = $totalQuery->count();
            } else {
                if (in_array($repCode, $this->reportService->getNonViewQueryReportWhiteList())) {
                    $iTotal = $totalQuery->count();
                } else {
                    $iTotal = $totalQuery->get()->count();
                }
            }

            // wpdf2
            if ($report->wpdf2 != CommonConstant::DATABASE_VALUE_YES) {
                if ($report->getRepUserReportWpdf() == CommonConstant::DATABASE_VALUE_YES || !$repViewModel) {
                    // get results in order to create wPDF
                    $results = $query->get();
                    if (!is_array($results)) {
                        $results = $results->toArray();
                    }
                } else {
                    // use a cursor instead of getting all the results to prevent memory problems when retrieving large
                    // numbers of rows
                    if ($repViewModel) {
                        $results = $query->cursor();
                        $usingCursor = true;
                    }
                }
            }
        }

        $iTotalNextYear = 0;
        $prj10FinYearPeriodCount = 0;
        $columnHeadingsToReplace = [];
        switch ($repSystemReportCode) {
            case ReportConstant::SYSTEM_REPORT_PRJ10:
                $columnHeadingsToReplace = $this->replaceHeadingPRJ10(
                    $inputs,
                    $iTotalNextYear,
                    $prj10FinYearPeriodCount
                );
                break;
            case ReportConstant::SYSTEM_REPORT_HLP09:
                $columnHeadingsToReplace = $this->replaceHeadingHLP09();
                break;
            default:
                $columnHeadingsToReplace = [];
                break;
        }

        if ($repSystemReportCode == ReportConstant::SYSTEM_REPORT_ES59) {
            $estUnitRecordService = new EstateUnitService(
                $this->permissionService,
                new LeaseOutLetuService($this->permissionService)
            );

            foreach ($results as $estateUnit) {
                $tenant = $estUnitRecordService->getTenant($estateUnit->lettable_unit_id);
                if (is_object($tenant) && $tenant) {
                    $estateUnit->Tenant = Common::concatContactFields(
                        $tenant->contact_name,
                        $tenant->organisation
                    );
                }
                unset($estateUnit->lettable_unit_id);
            }
        }

        // Generate report file
        if ($report->getRepUserReportWpdf() == CommonConstant::DATABASE_VALUE_YES) {
            /* Generate PDF file */
            $iRetRows = $returnQuery->get()->count() - $iStartRowNum;

            $iEndRow = $iRetRows > 0 ? ($iStartRow + $iRetRows - 1) : 0;

            $repFormat = ReportConstant::REPORT_OUTPUT_PDF_FORMAT;
            $textCanGrow = $report->getRepWpdfTextCanGrow() == CommonConstant::DATABASE_VALUE_YES ? true : false;
            // wpdf2
            $columnList = explode(
                ',',
                $this->userReportPdfDesignService->getWpdfTagList($report->getSystemReportId(), false)
            );

            if ($report->wpdf2 == CommonConstant::DATABASE_VALUE_YES) {
                $wpdf = WPDF2::init(
                    $report->getRepReportId(),
                    $report->getSystemReportCode(),
                    json_decode($jsonData),
                    $query,
                    $columnList, // columns
                    $arrOrder
                );
                $wpdf->createReport();
            } else {
                $wpdf = new wPDF(
                    $jsonData,
                    $results,
                    $groupByColumn,
                    $wpdfTotals,
                    $report->getSystemReportCode(),
                    ReportConstant::PDF_FORMAT_FOOTER,
                    $textCanGrow
                );
            }
            $repPdfTempFile = $usePdfTempFile ? $repFilePath : $repPdfFile;
            $wpdf->serveForFile($repPdfTempFile);
            if ($usePdfTempFile) {
                rename($repPdfTempFile, $repPdfFile);
                $repFilePath = $repPdfFile;
            }

            // get filter text for the audit log
            $bFilterDetail = false;
            $filterText = "";
            self::reGetFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $filterText);
            if ($reportFilterQuery instanceof IReportFilterQuery) {
                $filterText = $reportFilterQuery->getFilterDetailText($filterText);
            }
        } else {
            /* Generate CSV or Excel file */
            // Init writer
            $fileWriter = (new SpoutFactory($repFilePath))->getSpoutWriter($report)->openFile();

            if ($fileWriter instanceof IXlsxSpoutWriter && count($numericCols) > 0) {
                // Reports the are not customizable and don't go through the CSV White List require their numeric
                // columns to be set.
                $this->setNumericColumnsForNonCustomizableReports($repCode, $fileWriter, $numericCols);
            }

            // add a UTF-8  BOM
            // JA Apr 2020: commented out as Microsoft Excel is wrapping text for cells with lots of data resulting
            // in large row heights. This doesn't happen when Excel uses it's default encoding.
            // fwrite($repFile, "\xEF\xBB\xBF");

            $bFilterDetail = false;
            $filterText = "";

            if ($repType == Report::USER_REPORT) {
                if ($report->getRepUserReportUseQuery() == CommonConstant::DATABASE_VALUE_NO) {
                    $this->reGetUserRepFiltering($repId, $filterText);
                    if (! empty($filterText)) {
                        $bFilterDetail = true;
                    }
                    self::reGetFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $filterText);
                }
            } else {
                self::reGetFiltering($whereCodes, $orCodes, $whereTexts, $bFilterDetail, $filterText);
            }
            if ($reportFilterQuery instanceof IReportFilterQuery) {
                $filterText = $reportFilterQuery->getFilterDetailText($filterText);
            }


            $bRowFound = false;

            if (!$results) {
                $fileWriter->addRowFromString('No Output fields');
            }

            foreach ($results as $row) {
                // $results can be from a cursor, so convert $row to an array
                if ($usingCursor && $repViewModel) {
                    $row = $row->toArray();
                }
                $bSelectedOutputFields = false;
                $aSelectedOutputFields = [];

                /*
                 * Don't attempt to remove/update columns for a SQL report.
                 */
                if (!$bSqlReport) {
                    switch ($repSystemReportCode) {
                        case ReportConstant::SYSTEM_REPORT_RB01:
                            if (
                                !(Common::valueYorNtoBoolean(Auth::user()->siteGroup->rb_allow_cost_override_by_all))
                                && !((Common::valueYorNtoBoolean(
                                    Auth::user()->siteGroup->rb_allow_cost_override_by_supervisor
                                )) && ($this->permissionService->hasModuleAccess(
                                    Module::MODULE_RESOURCE_BOOKING,
                                    CrudAccessLevel::CRUD_ACCESS_LEVEL_SUPERVISOR
                                ))
                                )
                            ) {
                                unset($row['Standard Bookable Resource Cost']);
                                unset($row['Standard Catering Cost']);
                            }

                            break;
                        case ReportConstant::SYSTEM_REPORT_ES70:
                            // Get the current annual rent for today.
                            $annualRent = 0;
                            $lettableUnitId = array_get($row, 'lettable_unit_id', false);
                            $currentRentDate = array_get($filterData, 'currentRentDate', false);

                            if ($lettableUnitId) {
                                LettableUnitService::calculateAnnualRent(
                                    $lettableUnitId,
                                    $annualRent,
                                    false,
                                    $currentRentDate
                                );
                            }
                            $row['Current Annual Rent'] = $annualRent;

                            unset($row['lettable_unit_id']);
                            break;
                        case ReportConstant::SYSTEM_REPORT_PRJ10:
                            // Generate
                            $projectCashFlowAcc =
                                ProjectCashFlowAccount::find(array_get($row, 'project_cash_flow_acc_id'));
                            if ($projectCashFlowAcc) {
                                // Last year
                                $row['Previous Year'] =
                                    ProjectCashFlowAccService::lastYearCashFlow($projectCashFlowAcc);

                                for ($i = 1; $i <= $iTotalNextYear; $i++) {
                                    $row["Next $i Year"] =
                                        ProjectCashFlowAccService::nextYearCashFlow($projectCashFlowAcc, $i);
                                }
                            }

                            unset($row['project_cash_flow_id']);
                            unset($row['project_cash_flow_acc_id']);

                            if ($prj10FinYearPeriodCount != 13) {
                                unset($row['Period 13']);
                            }

                            break;
                        case ReportConstant::SYSTEM_REPORT_PRJ16:
                            $data = $this->getFinYearData($row, $columnHeadingsToReplace);
                            unset($row['project_id']);
                            unset($row['fin_account_id']);
                            unset($row['project_budget_line_id']);
                            $row = array_merge($row, $data);
                            break;
                        case ReportConstant::SYSTEM_REPORT_INV01:
                        case ReportConstant::SYSTEM_REPORT_INV04:
                        case ReportConstant::SYSTEM_REPORT_INT06:
                        case ReportConstant::SYSTEM_REPORT_INT12:
                            if (SiteGroup::get()->inv_show_voucher_code != CommonConstant::DATABASE_VALUE_YES) {
                                unset($row['Voucher Code']);
                            }

                            break;
                        case ReportConstant::SYSTEM_REPORT_PL03:
                            unset($row['parent_module_id']);
                            unset($row['parent_desc']);
                            unset($row['plant_oos_record_type_desc']);
                            unset($row['parent_logged_by']);
                            unset($row['reason']);

                            break;
                        default:
                            break;
                    }
                }

                if (!$bRowFound) {
                    // Output the headers by using the keys of this first row of data returned.
                    $bFirstCol = true;
                    $data = null;
                    foreach ($row as $key => $value) {
                        if ($bSelectedOutputFields && !in_array($key, $aSelectedOutputFields)) {
                            continue;
                        }

                        $key = $this->getKeyHeaderLabel($key, $repSystemReportCode, $columnHeadingsToReplace);
                        $this->reGetHeader($report->getRepUserdefData(), $key, $bFirstCol, $data);
                    }

                    if (empty($data)) {
                        $fileWriter->addRowFromString('No Output fields');
                    } else {
                        $fileWriter->addHeaderRow($data);
                    }

                    $bRowFound = true;
                }

                // Go through each key/value pair in the array
                $data = "";
                $rowArr = [];
                foreach ($row as $key => $value) {
                    if ($bSelectedOutputFields && !in_array($key, $aSelectedOutputFields)) {
                        continue;
                    }
                    // Put values in double quotes.
                    //$escapeValue = str_replace('"', '""', $value);
                    $avoidExcelExpression = "";
                    if (!empty($value) && is_string($value)) {
                        $firstChar = $value[0];
                        // RB 26/11/2019 Adding a space before the hyphen causes Excel to treat a negative number
                        // as a string. Appears to be ok to let it the value through when it is a number.
                        if ($firstChar == '-' || $firstChar == '=') {
                            $avoidExcelExpression = ' ';
                        }
                    }
                    $rowArr[$key] = $avoidExcelExpression . $value;
                }
                $fileWriter->addRowFromArray($rowArr);
                // Create comma separated string for this row.
                ++$iRetRows;
            }

            $iEndRow = $iStartRow + $iRetRows - 1;
            $includeFilterSummary = array_get($inputs, 'includeFilterSummary', null);
            if (is_null($includeFilterSummary)) {
                $includeFilterSummary = Auth::user()->include_filter_summary;
            }

            if ($includeFilterSummary == CommonConstant::DATABASE_VALUE_YES) {
                // Now write out the title of the report and the details of the filtering applied.
                $repTitleOutput = $this->reGetReportTitle($report);

                //Write report filter summary details
                $fileWriter->addRowFromString(null);
                $fileWriter->addRowFromString("Report: $repTitleOutput");

                if ($filterText) {
                    $fileWriter->addRowFromString("Filtering details:");
                    $fileWriter->addRowFromString($filterText);
                }

                if ($sOrderText != "") {
                    $fileWriter->addRowFromString("Ordering details:");
                    $fileWriter->addRowFromString($sOrderText);
                }
                if ($report->getRepUserReportUseQuery() == CommonConstant::DATABASE_VALUE_YES) {
                    $fileWriter->addRowFromString("Query run:");
                    $fileWriter->addRowFromString($sQuery);
                    if (isset($userQueryError)) {
                        \log::error('SQL Error:' . $userQueryError . "\n");
                    }
                }

                if (!$bRowFound) {
                    $fileWriter->addRowFromString(null);
                    $fileWriter->addRowFromString("No data found.");
                } else {
                    $fileWriter->addRowFromString("Rows returned:");
                    $returnInformation = "Returned $iRetRows rows (from $iStartRow to $iEndRow)";

                    if ($report->getRepUserReportUseQuery() == CommonConstant::DATABASE_VALUE_NO) {
                        $returnInformation .= " of $iTotal rows.";
                    }
                    $fileWriter->addRowFromString($returnInformation);
                }
            }
            $fileWriter->closeFile();
        }

        // Add to Recent Items
        self::setRecentReport($repType, $repId);
        $returnData = [
            'returnedRows' => $iRetRows,
            'rowNumbers' => $iEndRow > 0 ? "{$iStartRow} to {$iEndRow}" : 0,
            'totalRows' => $iTotal,
            'repFormat' => $repFormat,
            'repCode' => $repCode,
            'repTitle' => $repTitle,
            'repFile' => url("report/reports/{$repCode}/download"),
            'repFileName' => $repFileName,
            'repFilePath' => $repFilePath,
            'reportDir' => $reportDir,
            'result' => true,
            'userQueryError' => $userQueryError
        ];

        //Skip Audit from Grouping path: ReportGroupingService@processGroupingToDispatch
        if ($disableAudit) {
            return $returnData;
        }

        // add entry to audit log
        $auditService = new AuditService();
        if ($filterText) {
            $auditService->auditText = "Filtering details: {$filterText}";
        }
        $auditService->addAuditEntry($report, AuditAction::ACT_REPORT);

        return $returnData;
    }

    public static function setRecentReport($repType, $repId)
    {
        $recentReport = Auth::User()->recent_report;

        $maxCookies = CommonConstant::MAX_RECENT_ITEMS;
        $maxLength = CommonConstant::MAX_RECENT_LENTH;

        if ($repType == Report::USER_REPORT) {
            $repType = ReportConstant::REPORT_USER_RUN;
        } else {
            $repType = ReportConstant::REPORT_SYSTEM_RUN;
        }

        $topReport = "$repType:$repId";
        $newReport = $topReport;
        $curRecentReport = $recentReport;

        if ($curRecentReport) {
            $arrCurRecentReport = explode(",", $curRecentReport);

            $arraySize = count($arrCurRecentReport);
            $i = 0;
            $iRecent = 1;
            $len = strlen($newReport);

            while (($i < $arraySize) && ($iRecent < $maxCookies)) {
                if ($arrCurRecentReport[$i] != $topReport) {
                    // It's different so add into string
                    $newLen = $len + strlen($arrCurRecentReport[$i]);
                    if ($newLen < $maxLength) {
                        ++$iRecent;
                        $newReport .= "," . $arrCurRecentReport[$i];
                        $len = $newLen;
                    }
                }
                ++$i;
            }
        }

        $user = User::find(Auth::User()->id);
        $user->recent_report = $newReport;
        $user->save();
    }

    public static function reGetUserDefLabels($reportId, $systemReport = false)
    {
        $labels = [];

        // fetch the right gen_table_id to start with
        $userReportTable = self::reGetUserDefReportTable($reportId, $systemReport);
        $allowExtraContact = false;

        if ($systemReport) {
            $report = SystemReport::find($reportId);

            if (
                $report->system_report_code == ReportConstant::SYSTEM_REPORT_PR01 ||
                $report->system_report_code == ReportConstant::SYSTEM_REPORT_PR02 ||
                $report->system_report_code == ReportConstant::SYSTEM_REPORT_PR16 ||
                $report->system_report_code == ReportConstant::SYSTEM_REPORT_PR32 ||
                $report->system_report_code == ReportConstant::SYSTEM_REPORT_PR42 ||
                $report->system_report_code == ReportConstant::SYSTEM_REPORT_PR11
            ) {
                $allowExtraContact = true;
            }
        } else {
            $userReport = UserReport::find($reportId);
            $report = SystemReport::find($userReport->system_report_id);

            if (
                $report->system_report_code == ReportConstant::SYSTEM_REPORT_PR01 ||
                $report->system_report_code == ReportConstant::SYSTEM_REPORT_PR32 ||
                $report->system_report_code == ReportConstant::SYSTEM_REPORT_PR42 ||
                $report->system_report_code == ReportConstant::SYSTEM_REPORT_PR11
            ) {
                $allowExtraContact = true;
            }
        }

        if ($userReportTable) {
            $labels = self::genUserdefInitialise($userReportTable, $allowExtraContact);
        }

        if (
            $report && in_array($report->system_report_code, [
            ReportConstant::SYSTEM_REPORT_INS01,
            ReportConstant::SYSTEM_REPORT_INS02,
            ReportConstant::SYSTEM_REPORT_INS03,
            ReportConstant::SYSTEM_REPORT_INS04,
            ReportConstant::SYSTEM_REPORT_INS09
            ])
        ) {
            $inspTypeLabels = self::genUserdefInitialise(
                GenTable::INSPECTION_TYPE,
                true,
                'inspection_type_'
            );
            $labels = array_merge($labels, $inspTypeLabels);
        }

        if (
            $report && in_array($report->system_report_code, [
            ReportConstant::SYSTEM_REPORT_DLO10,
            ReportConstant::SYSTEM_REPORT_DLO11,
            ])
        ) {
            $invLabels = self::genUserdefInitialise(
                GenTable::DLO_SALES_INVOICE,
                false,
                'inv_'
            );

            $labels = array_merge($labels, $invLabels);
        }

        return $labels;
    }

    public static function reGetUserDefReportTable($repId, $systemReport = false)
    {
        if ($systemReport) {
            $query = SystemReport::select('gen_table_id')
                ->where('system_report_id', '=', $repId)
                ->first();
        } else {
            $query = UserReport::select('system_report.gen_table_id AS gen_table_id')
                ->join('system_report', 'system_report.system_report_id', '=', 'user_report.system_report_id')
                ->where('user_report_id', $repId)
                ->first();
        }

        return $query->gen_table_id;
    }

    public static function genUserdefInitialise($genTableId, $allowExtraContact = false, $prefix = '')
    {
        $userDefLabel = GenUserdefLabel::where('gen_table_id', $genTableId)
                ->where('site_group_id', '=', Auth::User()->site_group_id)
                ->first();

        $record = [];
        $fields = Common::reportUserDefinedFields($allowExtraContact);
        array_unshift($fields, UserDefConstant::GEN_USERDEF_GROUP_ID);

        foreach ($fields as $field) {
            $record[$prefix . $field] = '';
        }

        $fields = [
            UserDefConstant::GEN_USERDEF_LABEL_USERTXT1,
            UserDefConstant::GEN_USERDEF_LABEL_USERTXT2,
            UserDefConstant::GEN_USERDEF_LABEL_USERTXT3,
            UserDefConstant::GEN_USERDEF_LABEL_USERTXT4,
            UserDefConstant::GEN_USERDEF_LABEL_USERTXT5,
            UserDefConstant::GEN_USERDEF_LABEL_USERTXT6,
            UserDefConstant::GEN_USERDEF_LABEL_USERCHK1,
            UserDefConstant::GEN_USERDEF_LABEL_USERCHK2,
            UserDefConstant::GEN_USERDEF_LABEL_USERCHK3,
            UserDefConstant::GEN_USERDEF_LABEL_USERCHK4,
            UserDefConstant::GEN_USERDEF_LABEL_USERDATE1,
            UserDefConstant::GEN_USERDEF_LABEL_USERDATE2,
            UserDefConstant::GEN_USERDEF_LABEL_USERDATE3,
            UserDefConstant::GEN_USERDEF_LABEL_USERDATE4,
            UserDefConstant::GEN_USERDEF_LABEL_USERCONT1,
            UserDefConstant::GEN_USERDEF_LABEL_USERCONT2,
            UserDefConstant::GEN_USERDEF_LABEL_USERCONT3,
            UserDefConstant::GEN_USERDEF_LABEL_USERCONT4,
            UserDefConstant::GEN_USERDEF_LABEL_USERCONT5

        ];

        if ($allowExtraContact) {
            $fields[] = UserDefConstant::GEN_USERDEF_LABEL_USERCONT6;
            $fields[] = UserDefConstant::GEN_USERDEF_LABEL_USERCONT7;
            $fields[] = UserDefConstant::GEN_USERDEF_LABEL_USERCONT8;
            $fields[] = UserDefConstant::GEN_USERDEF_LABEL_USERCONT9;
            $fields[] = UserDefConstant::GEN_USERDEF_LABEL_USERCONT10;
        }

        $fields[] = UserDefConstant::GEN_USERDEF_LABEL_USERMEMO1;
        $fields[] = UserDefConstant::GEN_USERDEF_LABEL_USERMEMO2;

        foreach ($fields as $field) {
            $record[$prefix . $field] = $userDefLabel->$field;
        }

        $record[$prefix . UserDefConstant::GEN_USERDEF_LABEL_USERCONTORG1] =
            empty($userDefLabel->{UserDefConstant::GEN_USERDEF_LABEL_USERCONT1}) ?
                '' : $userDefLabel->{UserDefConstant::GEN_USERDEF_LABEL_USERCONT1} . ' Organisation';
        $record[$prefix . UserDefConstant::GEN_USERDEF_LABEL_USERCONTORG2] =
            empty($userDefLabel->{UserDefConstant::GEN_USERDEF_LABEL_USERCONT2}) ?
                '' : $userDefLabel->{UserDefConstant::GEN_USERDEF_LABEL_USERCONT2} . ' Organisation';
        $record[$prefix . UserDefConstant::GEN_USERDEF_LABEL_USERCONTORG3] =
            empty($userDefLabel->{UserDefConstant::GEN_USERDEF_LABEL_USERCONT3}) ?
                '' : $userDefLabel->{UserDefConstant::GEN_USERDEF_LABEL_USERCONT3} . ' Organisation';
        $record[$prefix . UserDefConstant::GEN_USERDEF_LABEL_USERCONTORG4] =
            empty($userDefLabel->{UserDefConstant::GEN_USERDEF_LABEL_USERCONT4}) ?
                '' : $userDefLabel->{UserDefConstant::GEN_USERDEF_LABEL_USERCONT4} . ' Organisation';
        $record[$prefix . UserDefConstant::GEN_USERDEF_LABEL_USERCONTORG5] =
            empty($userDefLabel->{UserDefConstant::GEN_USERDEF_LABEL_USERCONT5}) ?
                '' : $userDefLabel->{UserDefConstant::GEN_USERDEF_LABEL_USERCONT5} . ' Organisation';


        if ($allowExtraContact) {
            $record[$prefix . UserDefConstant::GEN_USERDEF_LABEL_USERCONTORG6] =
                empty($userDefLabel->{UserDefConstant::GEN_USERDEF_LABEL_USERCONT6}) ?
                    '' : $userDefLabel->{UserDefConstant::GEN_USERDEF_LABEL_USERCONT6} . ' Organisation';

            $record[$prefix . UserDefConstant::GEN_USERDEF_LABEL_USERCONTORG7] =
                empty($userDefLabel->{UserDefConstant::GEN_USERDEF_LABEL_USERCONT7}) ?
                    '' : $userDefLabel->{UserDefConstant::GEN_USERDEF_LABEL_USERCONT7} . ' Organisation';

            $record[$prefix . UserDefConstant::GEN_USERDEF_LABEL_USERCONTORG8] =
                empty($userDefLabel->{UserDefConstant::GEN_USERDEF_LABEL_USERCONT8}) ?
                    '' : $userDefLabel->{UserDefConstant::GEN_USERDEF_LABEL_USERCONT8} . ' Organisation';

            $record[$prefix . UserDefConstant::GEN_USERDEF_LABEL_USERCONTORG9] =
                empty($userDefLabel->{UserDefConstant::GEN_USERDEF_LABEL_USERCONT9}) ?
                    '' : $userDefLabel->{UserDefConstant::GEN_USERDEF_LABEL_USERCONT9} . ' Organisation';

            $record[$prefix . UserDefConstant::GEN_USERDEF_LABEL_USERCONTORG10] =
                empty($userDefLabel->{UserDefConstant::GEN_USERDEF_LABEL_USERCONT10}) ?
                    '' : $userDefLabel->{UserDefConstant::GEN_USERDEF_LABEL_USERCONT10} . ' Organisation';
        }


        // Now sort out the selection labels if any.
        $fieldDets = [
            [UserDefConstant::GEN_USERDEF_LABEL_USERSEL1_CODE, UserDefConstant::GEN_USERDEF_OPTIONS_USERSEL1_CODE, 1],
            [UserDefConstant::GEN_USERDEF_LABEL_USERSEL1_DESC, UserDefConstant::GEN_USERDEF_OPTIONS_USERSEL1_DESC, 1],
            [UserDefConstant::GEN_USERDEF_LABEL_USERSEL2_CODE, UserDefConstant::GEN_USERDEF_OPTIONS_USERSEL2_CODE, 2],
            [UserDefConstant::GEN_USERDEF_LABEL_USERSEL2_DESC, UserDefConstant::GEN_USERDEF_OPTIONS_USERSEL2_DESC, 2],
            [UserDefConstant::GEN_USERDEF_LABEL_USERSEL3_CODE, UserDefConstant::GEN_USERDEF_OPTIONS_USERSEL3_CODE, 3],
            [UserDefConstant::GEN_USERDEF_LABEL_USERSEL3_DESC, UserDefConstant::GEN_USERDEF_OPTIONS_USERSEL3_DESC, 3],
            [UserDefConstant::GEN_USERDEF_LABEL_USERSEL4_CODE, UserDefConstant::GEN_USERDEF_OPTIONS_USERSEL4_CODE, 4],
            [UserDefConstant::GEN_USERDEF_LABEL_USERSEL4_DESC, UserDefConstant::GEN_USERDEF_OPTIONS_USERSEL4_DESC, 4],
        ];
        // Obtain the label from the csv which represents this
        foreach ($fieldDets as $key => $fieldDet) {
            $labelColumn            = "user_selection{$fieldDet[2]}_label";
            $record[$prefix . $fieldDet[0]]   = '';
            $record[$prefix . $fieldDet[1]]   = '';
            $value                  = $key % 2 == 0 ? 'gen_userdef_sel_code' : 'gen_userdef_sel_desc';
            if ($userDefLabel->$labelColumn) {
                $record[$prefix . $fieldDet[0]] =
                    $userDefLabel->$labelColumn . ($key % 2 == 0 ? ' Code' : ' Description');
                $genUserDefSels = $userDefLabel->genUserDefSels($fieldDet[2])->get()->toArray();
                $record[$prefix . $fieldDet[1]] = implode(",", array_pluck($genUserDefSels, $value));
            }
        }

        return $record;
    }

    /**
     * Get Report Filtering
     */
    public static function reGetFiltering($whereCodes, $orCodes, $whereTexts, &$bFilterDetail, &$filterText)
    {
        if (is_string($whereTexts) && strlen($whereTexts) > 0) {
            $filterText .= $whereTexts;
        } else {
            foreach ($whereCodes as $whereCode) {
                $result = DB::table($whereCode[0])
                    ->where($whereCode[1], $whereCode[3])
                    ->select($whereCode[2] . ' as code')
                    ->first();

                $sCode = "";
                if ($result) {
                    $sCode = $result->code;
                }

                $filterText .= static::reWriteFilterCode($whereCode[4] . " = ", $sCode, $bFilterDetail);
            }

            foreach ($orCodes as $orCode) {
                if (is_array($orCode[3])) {
                    $ids = $orCode[3];
                } else {
                    $ids = explode(',', $orCode[3]);
                }

                $sOrText = $orCode[4] . " = ";

                $numIds = count($ids);
                $i = 1;
                foreach ($ids as $id) {
                    $result = DB::table($orCode[0])
                        ->where($orCode[1], $id)
                        ->select($orCode[2] . ' as code')
                        ->first();

                    $sCode = "";
                    if ($result) {
                        $sCode = $result->code;
                    }

                    $sOrText .= $sCode;
                    if ($numIds > 1) {
                        if ($i != $numIds) {
                            $sOrText .= " or ";
                        }
                    }
                    ++$i;
                }
                $filterText .= static::reWriteFilterCode($sOrText, "", $bFilterDetail);
            }

            if (is_array($whereTexts) && count($whereTexts) > 0) {
                foreach ($whereTexts as $whereText) {
                    $filterText .= static::reWriteFilterCode($whereText, "", $bFilterDetail);
                }
            }
        }

        if ($bFilterDetail) {
            $filterText .= '"';
        }
    }

    private function setNumericColumnsForNonCustomizableReports($repCode, &$fileWriter, $numericCols)
    {
        switch ($repCode) {
            case ReportConstant::SYSTEM_REPORT_FAR01:
            case ReportConstant::SYSTEM_REPORT_FAR02:
            case ReportConstant::SYSTEM_REPORT_FAR08:
            case ReportConstant::SYSTEM_REPORT_FAR31:
            case ReportConstant::SYSTEM_REPORT_FAR37:
            case ReportConstant::SYSTEM_REPORT_ES59:
                $fileWriter->addNumberColKeys($numericCols);
                break;
        }
    }

    /**
     * Get the Report Order
     * @param string $reportTable The report table (system_report | user_report)
     * @param int $reportId The report Id
     * @param array $arrOrder Array of order params
     * @param string $sOrderText order text
     */
    private function repGetReportOrder($reportTable, $reportId, &$arrOrder, &$sOrderText)
    {
        $orderSep = "";
        $idField = $reportTable . "_id";
        $fields = ["field1", "field2", "field3"];

        foreach ($fields as $field) {
            $query = DB::table($reportTable)->leftjoin(
                'report_field',
                $reportTable . '.order_by_' . $field . '_id',
                '=',
                'report_field.report_field_id'
            )->where($reportTable . '.' . $idField, $reportId)
                ->select([
                    'report_field.report_field_code',
                    'report_field.report_field_type_id',
                    $reportTable . '.order_by_' . $field . '_asc AS asc_desc'
                ]);
            $result = $query->get()->toArray();

            if (!is_array($result)) {
                return ;
            }

            foreach ($result as $row) {
                $reportFieldCode = $row->report_field_code;
                $reportFieldTypeId = $row->report_field_type_id;

                if ($reportFieldCode != "") {
                    $reportFieldCode = "`$reportFieldCode`";
                    if ($row->asc_desc == CommonConstant::DATABASE_VALUE_NO) {
                        $dirSql = "DESC";
                        $dirText = "descending";
                    } else {
                        $dirSql = "ASC";
                        $dirText = "ascending";
                    }

                    if ($reportFieldTypeId == ReportFieldType::REP_FIELD_TYPE_DATE) {
                        $reportFieldCodeSql = "STR_TO_DATE($reportFieldCode,'%d/%m/%Y %H:%i:%s')";
                    } elseif ($reportFieldTypeId == ReportFieldType::REP_FIELD_TYPE_NUMERIC) {
                        $reportFieldCodeSql = " CONVERT(REPLACE($reportFieldCode, ',', ''), DECIMAL(13,7)) ";
                    } else {
                        $reportFieldCodeSql = $reportFieldCode;
                    }

                    // For use in an actual SQL statement
                    $arrOrder[] = [
                        'key' => $reportFieldCodeSql,
                        'dir' => $dirSql
                    ];

                    // For display indicating the sort order
                    $sOrderText .= $orderSep . "$reportFieldCode ($dirText)";
                    $orderSep = ", ";
                }
            }
        }
    }

    /**
     * Get User Report Filtering
     * @param int $userReportId
     * @param string $filterText
     */
    private function reGetUserRepFiltering($userReportId, &$filterText)
    {
        $filterText = "";
        $sep = "";
        $query = UserReportFilter::select([
            'user_report_filter.user_report_filter_id'
            , 'report_field.report_field_code'
            , 'report_field_type.report_field_type_id'
            , 'report_field_type.report_field_type_code'
            , 'user_report_filter.user_report_id'
            , 'user_report_filter.filter_value'
            , 'report_date_option.report_date_option_id'
            , 'report_date_option.report_date_option_code'
            , 'report_operator_type.report_operator_type_id'
            , 'report_operator_type.report_operator_type_code'
            , 'rpf.compare_field'
            , 'user_report_filter.filter_value_as_field_id'
        ])
            ->leftJoin('report_field', 'user_report_filter.report_field_id', '=', 'report_field.report_field_id')
            ->leftJoinSub(
                UserReportFilter::select([
                    'report_field.report_field_code as compare_field',
                    'report_field.report_field_id'
                ])->join('report_field', 'report_field.report_field_id', '=', 'user_report_filter.filter_value')
                    ->where('filter_value_as_field_id', CommonConstant::DATABASE_VALUE_YES),
                'rpf',
                function ($join) {
                    $join->on('rpf.report_field_id', '=', 'user_report_filter.filter_value')
                        ->where('user_report_filter.filter_value_as_field_id', CommonConstant::DATABASE_VALUE_YES);
                }
            )
            ->leftJoin(
                'report_field_type',
                'report_field_type.report_field_type_id',
                '=',
                'report_field.report_field_type_id'
            )
            ->leftJoin(
                'report_operator_type',
                'report_operator_type.report_operator_type_id',
                '=',
                'user_report_filter.report_operator_type_id'
            )
            ->leftJoin(
                'report_date_option',
                'report_date_option.report_date_option_id',
                '=',
                'user_report_filter.report_date_option_id'
            )
            ->where('user_report_filter.user_report_id', $userReportId);

        $result = $query->get();

        $userDefLabels = $this->reGetUserDefLabels($userReportId);

        if (!$result) {
            return false;
        }

        foreach ($result as $row) {
            $reportFieldCode = $this->reGetUserDefLabel($row->report_field_code, $userDefLabels);

            if (
                $row->report_operator_type_id == ReportConstant::REP_FILTER_TYPE_IS_EMPTY
                || $row->report_operator_type_id == ReportConstant::REP_FILTER_TYPE_IS_NOT_EMPTY
            ) {
                $filterText .= "$sep" . $reportFieldCode . " " . $row->report_operator_type_code;
            } else {
                if ($row->report_field_type_id == ReportConstant::REP_FIELD_TYPE_DATE) {
                    if ($row->report_date_option_id == ReportDateOption::REP_DATE_OPTION_SPECIFIC_DATE) {
                        $filterText .= "$sep" . $reportFieldCode . " " . $row->report_operator_type_code
                                . " (" . Common::dateFieldDisplayFormat($row->filter_value) . ")";
                    } elseif ($row->filter_value_as_field_id == CommonConstant::DATABASE_VALUE_YES) {
                        $filterText .= "$sep" . $reportFieldCode . " " . $row->report_operator_type_code
                            . " (" . $row->compare_field . ")";
                    } else {
                        $filterText .= "$sep" . $reportFieldCode . " " . $row->report_operator_type_code
                                . " (" . $row->report_date_option_code . ")";
                    }
                } else {
                    $filterText .= "$sep" . $reportFieldCode . " " . $row->report_operator_type_code
                            . " (" . Common::dateFieldDisplayFormat($row->filter_value) . ")";
                }
            }

            $sep = ", ";
        }
    }

    /**
     * Get Report Title
     */
    private function reGetReportTitle(Report $report)
    {
        $reportTitle = $report->getRepTitle();
        $reportSystemCode = $report->getSystemReportCode();

        switch ($reportSystemCode) {
            case ReportConstant::SYSTEM_REPORT_FAR01:
            case ReportConstant::SYSTEM_REPORT_FAR02:
            case ReportConstant::SYSTEM_REPORT_FAR03:
            case ReportConstant::SYSTEM_REPORT_FAR04:
            case ReportConstant::SYSTEM_REPORT_FAR05:
            case ReportConstant::SYSTEM_REPORT_FAR06:
            case ReportConstant::SYSTEM_REPORT_FAR07:
            case ReportConstant::SYSTEM_REPORT_FAR08:
            case ReportConstant::SYSTEM_REPORT_FAR09:
            case ReportConstant::SYSTEM_REPORT_FAR10:
            case ReportConstant::SYSTEM_REPORT_FAR11:
            case ReportConstant::SYSTEM_REPORT_FAR12:
            case ReportConstant::SYSTEM_REPORT_FAR13:
            case ReportConstant::SYSTEM_REPORT_FAR14:
            case ReportConstant::SYSTEM_REPORT_FAR15:
            case ReportConstant::SYSTEM_REPORT_FAR16:
            case ReportConstant::SYSTEM_REPORT_FAR17:
            case ReportConstant::SYSTEM_REPORT_FAR18:
            case ReportConstant::SYSTEM_REPORT_FAR19:
            case ReportConstant::SYSTEM_REPORT_FAR31:
            case ReportConstant::SYSTEM_REPORT_FAR32:
            case ReportConstant::SYSTEM_REPORT_FAR37:
                // For all Capital Accounting reports, postfix the open fin year to the title.
                if ($openYear = CaFinYearManager::retrieveOpenFinYear()) {
                    $reportTitle .= " ({$openYear->ca_fin_year_code})";
                }

                break;
        }

        return $reportTitle;
    }

    private static function reWriteFilterCode($idText, $sCode, &$bFilterDetail)
    {
        if ($bFilterDetail) {
            $text = ", ";
        } else {
            $bFilterDetail = true;
            $text = '"';
        }

        $text .= $idText;
        if ($sCode != "") {
            $text .= " '" . $sCode . "'";
        }

        return $text;
    }

    private function reGetUserDefRepQuery($report, $viewTable)
    {
        if (!$report instanceof Report) {
            return null;
        }

        $repUserdefData = $report->getRepUserdefData();

        $userDefFieldStr = "";
        $csvSep = "";
        $userReportId = $report->getRepReportId();

        $reportFields = UserReportField::select('report_field.report_field_code')
                ->leftJoin('report_field', 'user_report_field.report_field_id', '=', 'report_field.report_field_id')
                ->where('user_report_id', $userReportId)
                ->orderBy('output_position', 'ASC')
                ->get();

        foreach ($reportFields as $reportField) {
            $labelIndex = $this->reGetUserDefLabel($reportField->report_field_code, $repUserdefData);
            if (isset($repUserdefData[$labelIndex])) {
                if (!empty($repUserdefData[$labelIndex])) {
                    $userDefFieldStr .= $csvSep . $viewTable . "." . "`" . $reportField->report_field_code . "`";
                    $csvSep = ",";
                }
            } else {
                $userDefFieldStr .= $csvSep . $viewTable . "." . "`" . $reportField->report_field_code . "`";
                $csvSep = ",";
            }
        }

        return $userDefFieldStr;
    }

    private function reGetUserDefRepNoViewArray($report)
    {
        if (!$report instanceof Report) {
            return null;
        }

        $repUserdefData = $report->getRepUserdefData();
        $userReportId = $report->getRepReportId();

        $userDefFields = [];

        $reportFields = UserReportField::select('report_field.report_field_code')
                ->leftJoin('report_field', 'user_report_field.report_field_id', '=', 'report_field.report_field_id')
                ->where('user_report_id', $userReportId)
                ->orderBy('output_position', 'ASC')
                ->get();

        foreach ($reportFields as $reportField) {
            $labelIndex = $this->reGetUserDefLabel($reportField->report_field_code, $repUserdefData);
            if (isset($repUserdefData[$labelIndex])) {
                if (!empty($repUserdefData[$labelIndex])) {
                    $userDefFields [] = $reportField->report_field_code;
                }
            } else {
                $userDefFields [] = $reportField->report_field_code;
            }
        }

        return $userDefFields;
    }

    private function reAddUserDefWhere($userReportId, &$query, &$arrOrder, &$sOrderText)
    {
        $filterQuery = UserReportFilter::select([
                'report_operator_type_id',
                'filter_value',
                'report_date_option_id',
                'report_field.report_field_code',
                'report_field.report_field_type_id',
                'user_report_filter.filter_value_as_field_id'
            ])
            ->leftJoin('report_field', 'user_report_filter.report_field_id', '=', 'report_field.report_field_id')
            ->where('user_report_filter.user_report_id', $userReportId);

        $result = $filterQuery->get();

        $userReport = UserReport::find($userReportId);
        $modelUserReportFiltersApplied = new UserReportFilterAppliedService($this->permissionService);
        $fieldList = $modelUserReportFiltersApplied->getReportDateFields($userReport);

        foreach ($result as $record) {
            $reportOperatorTypeId = $record->report_operator_type_id;
            $reportFieldCode = "`$record->report_field_code`";  // to avoid spacing, e.g. Audit Id => 'Audit Id'
            $reportFieldTypeId = $record->report_field_type_id;
            $filterValue = trim($record->filter_value);
            $reportDateOptionId = $record->report_date_option_id;

            if ($reportFieldTypeId == ReportFieldType::REP_FIELD_TYPE_DATE) {
                $newFieldCode = "STR_TO_DATE($reportFieldCode, '%d/%m/%Y')";
                $reportFieldCode = $newFieldCode;
                $dbRaw = true;                          // Most of the functions should be left

                if ($record->filter_value_as_field_id == CommonConstant::DATABASE_VALUE_YES) {
                    $compareRecord = $fieldList->where('report_field_id', $filterValue)->first();
                    $filterValue  = "STR_TO_DATE(`$compareRecord->report_field_code`, '%d/%m/%Y')";
                } else {
                    switch ($reportDateOptionId) {
                        case ReportDateOption::REP_DATE_OPTION_TODAY:
                            $filterValue = "CURDATE()";
                            break;
                        case ReportDateOption::REP_DATE_OPTION_TODAY_MINUS_7:
                            $filterValue = "DATE_SUB(CURDATE(),INTERVAL 7 DAY)";
                            break;
                        case ReportDateOption::REP_DATE_OPTION_TODAY_MINUS_14:
                            $filterValue = "DATE_SUB(CURDATE(),INTERVAL 14 DAY)";
                            break;
                        case ReportDateOption::REP_DATE_OPTION_TODAY_MINUS_30:
                            $filterValue = "DATE_SUB(CURDATE(),INTERVAL 30 DAY)";
                            break;
                        case ReportDateOption::REP_DATE_OPTION_TODAY_MINUS_60:
                            $filterValue = "DATE_SUB(CURDATE(),INTERVAL 60 DAY)";
                            break;
                        case ReportDateOption::REP_DATE_OPTION_TODAY_MINUS_120:
                            $filterValue = "DATE_SUB(CURDATE(),INTERVAL 120 DAY)";
                            break;
                        case ReportDateOption::REP_DATE_OPTION_TODAY_MINUS_150:
                            $filterValue = "DATE_SUB(CURDATE(),INTERVAL 150 DAY)";
                            break;
                        case ReportDateOption::REP_DATE_OPTION_TODAY_MINUS_QUARTER:
                            $filterValue = "DATE_SUB(CURDATE(),INTERVAL 1 QUARTER)";
                            break;
                        case ReportDateOption::REP_DATE_OPTION_TODAY_MINUS_YEAR:
                            $filterValue = "DATE_SUB(CURDATE(),INTERVAL 1 YEAR)";
                            break;
                        case ReportDateOption::REP_DATE_OPTION_TODAY_PLUS_7:
                            $filterValue = "DATE_ADD(CURDATE(),INTERVAL 7 DAY)";
                            break;
                        case ReportDateOption::REP_DATE_OPTION_TODAY_PLUS_14:
                            $filterValue = "DATE_ADD(CURDATE(),INTERVAL 14 DAY)";
                            break;
                        case ReportDateOption::REP_DATE_OPTION_TODAY_PLUS_30:
                            $filterValue = "DATE_ADD(CURDATE(),INTERVAL 30 DAY)";
                            break;
                        case ReportDateOption::REP_DATE_OPTION_TODAY_PLUS_60:
                            $filterValue = "DATE_ADD(CURDATE(),INTERVAL 60 DAY)";
                            break;
                        case ReportDateOption::REP_DATE_OPTION_TODAY_PLUS_120:
                            $filterValue = "DATE_ADD(CURDATE(),INTERVAL 120 DAY)";
                            break;
                        case ReportDateOption::REP_DATE_OPTION_TODAY_PLUS_150:
                            $filterValue = "DATE_ADD(CURDATE(),INTERVAL 150 DAY)";
                            break;
                        case ReportDateOption::REP_DATE_OPTION_TODAY_PLUS_QUARTER:
                            $filterValue = "DATE_ADD(CURDATE(),INTERVAL 1 QUARTER)";
                            break;
                        case ReportDateOption::REP_DATE_OPTION_TODAY_PLUS_YEAR:
                            $filterValue = "DATE_ADD(CURDATE(),INTERVAL 1 YEAR)";
                            break;
                        case ReportDateOption::REP_DATE_OPTION_YESTERDAY:
                            $filterValue = "DATE_SUB(CURDATE(),INTERVAL 1 DAY)";
                            break;
                        case ReportDateOption::REP_DATE_OPTION_TOMORROW:
                            $filterValue = "DATE_ADD(CURDATE(),INTERVAL 1 DAY)";
                            break;
                        case ReportDateOption::REP_DATE_OPTION_SPECIFIC_DATE:
                        default:
                            $filterValue = "STR_TO_DATE('{$filterValue}', '%d/%m/%Y')";
                            break;
                    }
                }

                $this->reUserdefCommonReportOption(
                    $reportOperatorTypeId,
                    $dbRaw,
                    $reportFieldCode,
                    $filterValue,
                    $query
                );
            } else {
                $reportFieldCode = DB::raw($reportFieldCode);
                switch ($reportOperatorTypeId) {
                    case ReportOperatorType::REP_OPERATOR_TYPE_INCLUDES:
                        // split filter string on comma, but ignore commas wrapped in double quotes.
                        $listItems = str_getcsv($filterValue, ',', '"');
                        // Iterate array through array
                        // and use trim() function to remove all
                        // whitespaces from every array object.
                        $listItems = array_map('trim', $listItems);
                        $query->whereIn($reportFieldCode, $listItems);
                        break;

                    case ReportOperatorType::REP_OPERATOR_TYPE_EXCLUDES:
                        // split filter string on comma, but ignore commas wrapped in double quotes.
                        $listItems = str_getcsv($filterValue, ',', '"');
                        // Iterate array through array
                        // and use trim() function to remove all
                        // whitespaces from every array object.
                        $listItems = array_map('trim', $listItems);
                        $query->where(function ($query) use ($reportFieldCode, $listItems) {
                            $query->whereNull($reportFieldCode)
                                ->orWhereNotIn($reportFieldCode, $listItems);
                        });
                        break;

                    case ReportOperatorType::REP_OPERATOR_TYPE_NOT_EQUAL_TO:
                        $query->where(function ($query) use ($reportFieldCode, $filterValue) {
                            $query->where($reportFieldCode, '!=', "{$filterValue}")
                                ->orWhereNull($reportFieldCode);
                        });
                        break;

                    case ReportOperatorType::REP_OPERATOR_TYPE_STARTS_WITH:
                        $query->where($reportFieldCode, 'LIKE', "{$filterValue}%");
                        break;

                    case ReportOperatorType::REP_OPERATOR_TYPE_ENDS_WITH:
                        $query->where($reportFieldCode, 'LIKE', "%{$filterValue}");
                        break;

                    case ReportOperatorType::REP_OPERATOR_TYPE_CONTAINS:
                        $query->where($reportFieldCode, 'LIKE', "%{$filterValue}%");
                        break;

                    case ReportOperatorType::REP_OPERATOR_TYPE_DOES_NOT_CONTAIN:
                        $query->where(function ($query) use ($reportFieldCode, $filterValue) {
                            $query->where($reportFieldCode, 'NOT LIKE', "%{$filterValue}%")
                                ->orWhereNull($reportFieldCode);
                        });
                        break;

                    default:
                        $this->reUserdefCommonReportOption(
                            $reportOperatorTypeId,
                            false,
                            $reportFieldCode,
                            $filterValue,
                            $query
                        );
                        break;
                }
            }
        }
        $this->repGetReportOrder('user_report', $userReportId, $arrOrder, $sOrderText);
    }

    private function reUserdefCommonReportOption($reportOperatorTypeId, $dbRaw, $reportFieldCode, $filterValue, &$query)
    {
        $rawReportFieldCode = DB::raw($reportFieldCode);

        if ($reportOperatorTypeId == ReportOperatorType::REP_OPERATOR_TYPE_IS_EMPTY) {
            $query->where(function ($query) use ($rawReportFieldCode) {
                $query->whereNull($rawReportFieldCode)
                    ->orWhere($rawReportFieldCode, '=', '');
            });
        } elseif ($reportOperatorTypeId == ReportOperatorType::REP_OPERATOR_TYPE_IS_NOT_EMPTY) {
            $query->where($rawReportFieldCode, '<>', '');
        } else {
            // A comparison with an actual value
            switch ($reportOperatorTypeId) {
                case ReportOperatorType::REP_OPERATOR_TYPE_EQUAL_TO:
                    $operator = '=';
                    break;

                case ReportOperatorType::REP_OPERATOR_TYPE_NOT_EQUAL_TO:
                    $operator = '<>';
                    break;

                case ReportOperatorType::REP_OPERATOR_TYPE_GREATER_THAN:
                    $operator = '>';
                    break;

                case ReportOperatorType::REP_OPERATOR_TYPE_GREATER_THAN_OR_EQUAL:
                    $operator = '>=';
                    break;

                case ReportOperatorType::REP_OPERATOR_TYPE_LESS_THAN:
                    $operator = '<';
                    break;

                case ReportOperatorType::REP_OPERATOR_TYPE_LESS_THAN_OR_EQUAL:
                    $operator = '<=';
                    break;

                default:
                    break;
            }
            if ($dbRaw) {
                // Using a function like a date addition, should not convert to string
                $query->where($rawReportFieldCode, $operator, DB::raw($filterValue));
            } else {
                $query->where($rawReportFieldCode, $operator, $filterValue);
            }
        }
    }

    private function reGetHeader($repUserDefData, $key, &$bFirstCol, &$data)
    {
        $headerVal = $key;

        if (
            $repUserDefData
            && (strpos($headerVal, ReportConstant::REP_USERDEF_FIELD_PREFIX) === 0
                || strpos($headerVal, ReportConstant::REP_INSP_TYPE_USERDEF_FIELD_PREFIX) === 0
                || strpos($headerVal, ReportConstant::REP_DLO_SALES_INV_TYPE_USERDEF_FIELD_PREFIX) === 0)
        ) {
            // This report does have some links to user defined data
            $headerVal = $this->reGetUserDefLabel($key, $repUserDefData);
            if ($headerVal === $key) {
                $headerVal = "";
            }
        }

        if ($headerVal != "") {
            // As long as the header column hasn't been altered to be blank - a
            // user defined field not in use, output it.
            if ($bFirstCol) {
                $bFirstCol = false;
            }

            $data[$key] = $headerVal;
        }
    }

    private function reGetUserDefLabel($reportFieldCode, $userDefLabels)
    {
        $retReportFieldCode = $reportFieldCode;
        if (!is_array($userDefLabels)) {
            return $retReportFieldCode;
        }

        // Check if row has a user-defined label
        // matches on "user_contact1_label"
        $labelIndex = $reportFieldCode . "_label";

        if (array_key_exists($labelIndex, $userDefLabels)) {
            if (trim($userDefLabels[$labelIndex]) != "") {
                // If it has, use the userdefined label instead
                $retReportFieldCode = $userDefLabels[$labelIndex];
            }
        } else {
            // if no "user_contact1_label" then check for partial match and replace
            // e.g. "user_contact1 Name" would return "Contact 1 Name"
            foreach ($userDefLabels as $key => $label) {
                if (strpos($reportFieldCode, $key) !== false && trim($userDefLabels[$key . "_label"]) !== "") {
                    $retReportFieldCode = str_replace($key, $userDefLabels[$key . "_label"], $reportFieldCode);
                }
            }
        }

        return $retReportFieldCode;
    }

    /**
     * Build query with filter values
     */
    private function reAddSpecificQuery(
        $report,
        $filterData,
        $query,
        &$whereCodes,
        &$orCodes,
        &$whereTexts
    ) {
        if (!$report instanceof Report) {
            return $query;
        }

        $estatesFilterQueryService = new EstatesFilterQueryService($this->permissionService);
        $occupancyFilterQueryService = new OccupancyFilterQueryService($this->permissionService);
        $generalFilterQueryService = new GeneralsFilterQueryService($this->permissionService);
        $viewName = $report->getRepViewName($filterData);

        if ($report->getRepFilterCostPlusContract()) {
            /**
         * Cost Plus Contract Module - Cost Plus Contracts list
         */
            $costPlusContractQueryService = new CostPlusContractFilterQueryService($this->permissionService);
            $query =  $costPlusContractQueryService
                    ->reAddCpContractQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterCostPlusSalesInvoice()) {
            /**
         * Cost Plus Sales Invoices
         */
            $costPlusSalesInvoiceFilterQueryService =
                    new CostPlusSalesInvoiceFilterQueryService($this->permissionService);
            $query =  $costPlusSalesInvoiceFilterQueryService
                    ->reAddCpSalesInvoiceQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterCostPlusSalesCredit()) {
            /**
         * Cost Plus Sales Credit
         */
            $costPlusSalesCreditFilterQueryService =
                    new CostPlusSalesCreditFilterQueryService($this->permissionService);
            $query =  $costPlusSalesCreditFilterQueryService
                    ->reAddCpSalesCreditQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterDocument()) {
        /**
         * General Module
         */
            $reportCode = $report->getSystemReportCode();
            switch ($reportCode) {
                case ReportConstant::SYSTEM_REPORT_DOC01:
                case ReportConstant::SYSTEM_REPORT_DOC02:
                    $generalsFilterQueryService = new GeneralsFilterQueryService($this->permissionService);
                    $query = $generalsFilterQueryService
                        ->reAddDocumentQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
                    break;
                case ReportConstant::SYSTEM_REPORT_INS_CLI_WAK01:
                    $generalsFilterQueryService = new GeneralsFilterQueryService($this->permissionService);
                    $query = $generalsFilterQueryService
                        ->reAddInsCliWak01DocumentQuery(
                            $filterData,
                            $viewName,
                            $query,
                            $whereCodes,
                            $orCodes,
                            $whereTexts
                        );
                    break;
            }
        } elseif ($report->getRepFilterContact()) {
            $service = new GeneralsFilterQueryService($this->permissionService);
            $query = $service->reAddContactQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepAllNotes()) {
            $service = new AllNotesFilterQueryService();
            $service->allNotesQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterPpp()) {
            /**
         * PPP Module -  PPP reports filters */
            $pppFilterQueryService = new PppFilterQueryService($this->permissionService);
            $query =  $pppFilterQueryService
                    ->reAddPppQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterPppPupils()) {
            /**
         * PPP Module - Ppp Pupils
         */
            $pppPupilsFilterQueryService = new PppPupilsFilterQueryService($this->permissionService);
            $query =  $pppPupilsFilterQueryService
                    ->reAddPppPupilQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterPlacement()) {
        /**
         * PPP Module - Placement
        */
            $placementsFilterQueryService = new PlacementsFilterQueryService($this->permissionService);
            $query =  $placementsFilterQueryService
                    ->reAddPlacementQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterPlacementSite()) {
        /**
         * PPP Module - Placement Site
        */
            $placementSitesFilterQueryService = new PlacementSitesFilterQueryService($this->permissionService);
            $query =  $placementSitesFilterQueryService
                    ->reAddPlacementSiteQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        /**
         * Dlo Module
         */
        } elseif ($report->getRepFilterDloLabourExceptions()) {
            $dloJobsFilterQueryService = new DloJobsFilterQueryService($this->permissionService);
            $query = $dloJobsFilterQueryService
                ->reAddDloLabourExceptionsQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterDLOJob() || $report->getRepFilterDLOJobMK()) {
            $dloJobsFilterQueryService = new DloJobsFilterQueryService($this->permissionService);
            $query = $dloJobsFilterQueryService
                ->reAddDloJobQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterDloHoursWorked()) {
            $dloJobsFilterQueryService = new DloJobsFilterQueryService($this->permissionService);
            $query = $dloJobsFilterQueryService
                ->reAddDloJobLabourQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterDLOOperative()) {
            $dloJobsFilterQueryService = new DloJobsFilterQueryService($this->permissionService);
            $query = $dloJobsFilterQueryService
                ->reAddDloJobOperativeQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterDLOLabourHours()) {
            $dloJobsFilterQueryService = new DloJobsFilterQueryService($this->permissionService);
            $query = $dloJobsFilterQueryService
                ->reAddDloJobLabourHoursQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterDLOPoWo()) {
            $dloJobsFilterQueryService = new DloJobsFilterQueryService($this->permissionService);
            $query = $dloJobsFilterQueryService
                ->reAddDloJobPoWoQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterDLOSalesInvoice()) {
            $dloJobsFilterQueryService = new DloJobsFilterQueryService($this->permissionService);
            $query = $dloJobsFilterQueryService
                ->reAddDloSalesInvoiceQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterDLOMaterialItem()) {
            $dloJobsFilterQueryService = new DloJobsFilterQueryService($this->permissionService);
            $query = $dloJobsFilterQueryService
                ->reAddDloMaterialItemQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterDLOStockMaterialItem()) {
            $dloJobsFilterQueryService = new DloJobsFilterQueryService($this->permissionService);
            $query = $dloJobsFilterQueryService
                ->reAddDloStockMaterialItemQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterDloOperativeTradeLevel()) {
            $dloJobsFilterQueryService = new DloJobsFilterQueryService($this->permissionService);
            $query = $dloJobsFilterQueryService
                ->reAddDloOperativeTradeLevelQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);

        /**
         * Property Module
         */
        } elseif ($report->getRepFilterSite() || $report->getRepFilterSiteContact() || $report->getRepFilterSFRS01()) {
            $propertiesFilterQueryService = new PropertiesFilterQueryService($this->permissionService);
            $query = $propertiesFilterQueryService
                ->reAddSiteQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterBuild()) {
            $propertiesFilterQueryService = new PropertiesFilterQueryService($this->permissionService);
            $query = $propertiesFilterQueryService
                ->reAddBuildQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterBuildingBlock()) {
            $propertiesFilterQueryService = new PropertiesFilterQueryService($this->permissionService);
            $query = $propertiesFilterQueryService
                ->reAddBlockQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterRoom() || $report->getRepFilterEducationRoom()) {
            $propertiesFilterQueryService = new PropertiesFilterQueryService($this->permissionService);
            $query = $propertiesFilterQueryService
                ->reAddRoomQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterPropReview()) {
            $propertiesFilterQueryService = new PropertiesFilterQueryService($this->permissionService);
            $query = $propertiesFilterQueryService
                ->reAddPropReviewQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterPropAgreements()) {
            $propertiesFilterQueryService = new PropertiesFilterQueryService($this->permissionService);
            $query = $propertiesFilterQueryService
                ->reAddPropAgreementQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterExternalArea()) {
            $propertiesFilterQueryService = new PropertiesFilterQueryService($this->permissionService);
            $query = $propertiesFilterQueryService
                ->reAddPropExternalQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterPropertyRunningCost()) {
            $propertiesFilterQueryService = new PropertiesFilterQueryService($this->permissionService);
            $query = $propertiesFilterQueryService
                ->reAddPropRunningCostQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);

        /**
         * Plant
         */
        } elseif ($report->getRepFilterPlantPartAdm()) {
            $plantFilterQueryService = new PlantFilterQueryService($this->permissionService);
            $query = $plantFilterQueryService
                ->reAddPlantAdmQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        /**
         * Transparency
         */
        } elseif ($report->getRepFilterTransparency()) {
            $transparencyFilterQueryService = new TransparencyFilterQueryService($this->permissionService);
            $query = $transparencyFilterQueryService
                ->reAddLocalAuthorityLand($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);

        /**
         * Estate Module
         */
        } elseif ($report->getRepFilterEstatesFreehold()) {
            // ES01: Freehold
            // ES14: Freehold Notes
            // ES19: Freehold Property
            $query = $estatesFilterQueryService
                ->reAddEstatesFreeholdQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterEstatesFreeholdAgreement()) {
            // ES22: Freehold Agreement
            // Freehold
            $filterData['code'] = array_get($filterData, 'freeholdCode');
            $filterData['description'] = array_get($filterData, 'freeholdDescription');
            $query = $estatesFilterQueryService
                ->reAddEstatesFreeholdQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
            // Agreement
            $filterData['code'] = array_get($filterData, 'agreementCode');
            $filterData['description'] = array_get($filterData, 'agreementDescription');
            $query = $estatesFilterQueryService
                ->reAddEstatesAgreementQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterEstatesLeaseIn()) {
            // ES02: Lease In
            // ES15: Lease In Note
            $query = $estatesFilterQueryService
                ->reAddEstatesLeaseInQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterEstatesLeaseInAgreement()) {
            // ES21: Lease In Agreement
            // Lease In
            $filterData['code'] = array_get($filterData, 'leaseInCode');
            $filterData['description'] = array_get($filterData, 'leaseInDescription');
            $query = $estatesFilterQueryService
                ->reAddEstatesLeaseInQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
            // Agreement
            $filterData['code'] = array_get($filterData, 'agreementCode');
            $filterData['description'] = array_get($filterData, 'agreementDescription');
            $query = $estatesFilterQueryService
                ->reAddEstatesAgreementQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterEstatesDisposalAgreement()) {
            $query = $estatesFilterQueryService
                ->reAddEstatesAgreementQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterEstatesTitlesAgreement()) {
            // ES51: Titles Agreements
            $query = $estatesFilterQueryService
                ->reAddEstatesAgreementQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterEstatesCliScotb01()) {
            // ES_CLI_SCOB01
            $query = $estatesFilterQueryService
                ->reAddEstatesSalesInvoiceQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterEstatesCliHrt01()) {
            // ES_CLI_Hrt01
            $query = $estatesFilterQueryService
                ->reAddEstatesSalesInvoiceQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterEstatesLettableUnit()) {
            // ES03: Lettable Unit
            // ES13: Lease Out by Lettable Unit
            $query = $estatesFilterQueryService
                ->reAddEstatesLettableUnitQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterEstatesLettableUnitCurrentRent()) {
            // ES70: Lettable Unit Current Rent
            $query = $estatesFilterQueryService
                ->reAddEstatesLettableUnitQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterEstatesOccupancyUnit()) {
            // OCC01: Occupancy Unit
            $query = $occupancyFilterQueryService
                ->reAddEstatesOccupancyUnitQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterEstatesLeaseOut()) {
            // ES04: Lease Out
            // ES16: Lease Out Notes
            // ES47: Lease Out split by Lettable Unit
            // ES57: Lease Out split by Lettable Unit / Site / Building
            $query = $estatesFilterQueryService
                ->reAddEstatesLeaseOutQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterEstatesMultipleRecordType()) {
                    // ES48: Estates Multiple Record Types
                $query = $estatesFilterQueryService
                ->reAddEstatesMultipleRecordTypesQuery(
                    $filterData,
                    $viewName,
                    $query,
                    $whereCodes,
                    $orCodes,
                    $whereTexts
                );
        } elseif ($report->getRepFilterEstateUnitMultipleRecordType()) {
                    // ES59: Estates Unit Multiple Record Types
                $query = $estatesFilterQueryService
                ->reAddEstateUnitMultipleRecordTypesQuery(
                    $filterData,
                    $viewName,
                    $query,
                    $whereCodes,
                    $orCodes,
                    $whereTexts
                );
        } elseif ($report->getRepFilterEstatesDeedPacket()) {
            // ES05: Deed Packet
            $query = $estatesFilterQueryService
                ->reAddEstatesDeedPacketQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterEstatesDisposal()) {
            // ES06: Disposal
            // ES17: Disposal Notes
            $query = $estatesFilterQueryService
                ->reAddEstatesDisposalQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterEstatesReview()) {
            // ES07: Review
            // ES26: Lease In Reviews
            // ES27: Lease Out Reviews
            if ($report->getSystemReportCode() == ReportConstant::SYSTEM_REPORT_ES26) {
                $parentType =  EstReview::LEASE_IN_REVIEW;
            } elseif ($report->getSystemReportCode() == ReportConstant::SYSTEM_REPORT_ES27) {
                $parentType = EstReview::LEASE_OUT_REVIEW;
            } else {
                $parentType  = '';
            }

            $filterData['parentType'] = $parentType;
            $query = $estatesFilterQueryService
                ->reAddEstatesReviewQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterEstatesCharge()) {
            // ES08: Titles Charges/Rent
            // ES09 - Lettings Charges/Rent
            // ES38 - Estates Charge Group
            $query = $estatesFilterQueryService
                ->reAddEstatesChargesRentQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterEstatesDeedPacketProperty()) {
            // ES11: Deed Packet Property
            $query = $estatesFilterQueryService
                ->reAddEstatesDeedPacketQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterEstatesValuation()) {
            // ES12: Estates Valuation
            $query = $estatesFilterQueryService
                ->reAddEstatesValuationQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterEstatesAcquisition()) {
            // ES18: Acquisitions
            // ES41: Acquisition note
            $query = $estatesFilterQueryService
                ->reAddEstatesAcquisitionQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterEstatesLettableUnitProperty()) {
            // ES10: Lettable Unit Property
            $query = $estatesFilterQueryService
                ->reAddEstatesLettableUnitPropertyQuery(
                    $filterData,
                    $viewName,
                    $query,
                    $whereCodes,
                    $orCodes,
                    $whereTexts
                );
        } elseif ($report->getRepFilterEstatesOccupancyUnitProperty()) {
            // OCC02: Occupancy Unit Property
            $query = $estatesFilterQueryService
                ->reAddEstatesOccupancyUnitPropertyQuery(
                    $filterData,
                    $viewName,
                    $query,
                    $whereCodes,
                    $orCodes,
                    $whereTexts
                );
        } elseif ($report->getRepFilterEstatesLettingPacket()) {
            // ES23: Letting Packet
            $query = $estatesFilterQueryService
                ->reAddEstatesLettingPacketQuery(
                    $filterData,
                    $viewName,
                    $query,
                    $whereCodes,
                    $orCodes,
                    $whereTexts
                );
        } elseif ($report->getRepFilterAud01Audit()) {
            $query = $generalFilterQueryService
                ->reAddAud01AuditQuery(
                    $filterData,
                    $viewName,
                    $query,
                    $whereCodes,
                    $orCodes,
                    $whereTexts
                );
        } elseif ($report->getRepFilterSup01Audit()) {
            // SUP01 Audit
            $query = $generalFilterQueryService
                ->reAddSup01AuditQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterSup02Audit()) {
            // SUP02 Audit
            $query = $generalFilterQueryService
                ->reAddSup02AuditQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterSup03Audit()) {
            // SUP03 Audit
            $query = $generalFilterQueryService
                ->reAddSup03AuditQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterSup04Audit()) {
            // SUP04 Audit
            $query = $generalFilterQueryService
                ->reAddSup04AuditQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterEstatesLeaseInAudit()) {
            // ES24: Lease In Audit
            $query = $estatesFilterQueryService
                ->reAddEstatesLeaseInAuditQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterEstatesLeaseOutAudit()) {
            // ES25: Lease Out Audit
            $query = $estatesFilterQueryService
                ->reAddEstatesLeaseOutAuditQuery(
                    $filterData,
                    $viewName,
                    $query,
                    $whereCodes,
                    $orCodes,
                    $whereTexts
                );
        } elseif ($report->getRepFilterEstatesAlienation()) {
            // ES28: Lease Out Alienations
            $query = $estatesFilterQueryService
                ->reAddEstatesAlienationQuery(
                    $filterData,
                    $viewName,
                    $query,
                    $whereCodes,
                    $orCodes,
                    $whereTexts
                );
        } elseif ($report->getRepFilterEstatesLeaseOutAgreement()) {
            // ES20: Lease Out Agreement
            // Lease Out
            $filterData['code'] = array_get($filterData, 'leaseOutCode');
            $filterData['description'] = array_get($filterData, 'leaseOutDescription');
            $query = $estatesFilterQueryService
                ->reAddEstatesLeaseOutQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
            // Agreement
            $filterData['code'] = array_get($filterData, 'agreementCode');
            $filterData['description'] = array_get($filterData, 'agreementDescription');
            $query = $estatesFilterQueryService
                ->reAddEstatesAgreementQuery($filterData, $viewName, $query, $whereCodes, $orCodes, $whereTexts);
        } elseif ($report->getRepFilterEstatesInvoiceGenerationPlan()) {
            // ES29: Invoice Generation Plan
            $query = $estatesFilterQueryService
                ->reAddEstatesInvoiceGenerationPlanQuery(
                    $filterData,
                    $viewName,
                    $query,
                    $whereCodes,
                    $orCodes,
                    $whereTexts
                );
        } elseif ($report->getRepFilterEstatesFeeReceipts()) {
            // ES30: Fee Receipts
            $query = $estatesFilterQueryService
                ->reAddEstatesFeeReceiptsQuery(
                    $filterData,
                    $viewName,
                    $query,
                    $whereCodes,
                    $orCodes,
                    $whereTexts
                );
        } elseif ($report->getRepFilterEstatesRentalPaymentGenerationPlan()) {
            // ES53: Rental Payment Generation Plan
            $query = $estatesFilterQueryService
                ->reAddEstatesRentalPaymentGenerationPlanQuery(
                    $filterData,
                    $viewName,
                    $query,
                    $whereCodes,
                    $orCodes,
                    $whereTexts
                );
        } elseif ($report->getRepFilterEstatesNotificationOfPayments()) {
            // ES54: Notification Of Payments
            $query = $estatesFilterQueryService
                ->reAddEstatesNotificationOfPaymentsQuery(
                    $filterData,
                    $viewName,
                    $query,
                    $whereCodes,
                    $orCodes,
                    $whereTexts
                );
        } elseif ($report->getRepFilterRentalPayment()) {
            // ES56: Rental Payment
            $query = $estatesFilterQueryService
                ->reAddEstatesRentalPaymentQuery(
                    $filterData,
                    $viewName,
                    $query,
                    $whereCodes,
                    $orCodes,
                    $whereTexts
                );
        } elseif ($report->getRepFilterGenChecklist()) {
            // ES42: Lease In Checklist
            $filterQueryService = new GenChecklistFilterQueryService($this->permissionService);
            $query = $filterQueryService
                ->reAddGenChecklistQuery(
                    $filterData,
                    $viewName,
                    $query,
                    $whereCodes,
                    $orCodes,
                    $whereTexts
                );
        } elseif ($report->getRepFilterSalesInvoice()) {
            // ES61: Sales Invoice Lines
            $query = $estatesFilterQueryService->reAddEstatesSalesInvoiceQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        } elseif ($report->getRepFilterServiceCharge()) {
            // ES60: Service Charge
            $query = $estatesFilterQueryService->reAddEstatesServiceChargeQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        } elseif ($report->getRepFilterServiceChargeActual()) {
            // ES62: Service Charge Actual
            $query = $estatesFilterQueryService->reAddEstatesServiceChargeActualQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        } elseif ($report->getRepFilterServiceChargeLetu()) {
            // ES63: Service Charge Letu
            $query = $estatesFilterQueryService->reAddEstatesServiceChargeLetuQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        } elseif ($report->getRepFilterServiceChargeSchedule()) {
            // ES64: Service Charge Schedule
            $query = $estatesFilterQueryService->reAddEstatesServiceChargeScheduleQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        } elseif ($report->getRepFilterLeaseInBreakDate()) {
            // ES65: Lease In Break Dates
            $query = $estatesFilterQueryService->reAddEstateLeaseInBreakDateQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        } elseif ($report->getRepFilterLeaseOutBreakDate()) {
            // ES66: Lease Out Break Dates
            $query = $estatesFilterQueryService->reAddEstateLeaseOutBreakDateQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        } elseif ($report->getRepFilterLettableUnitEnergyRating()) {
            // ES68: Lettable Unit Energy Rating
            $query = $estatesFilterQueryService->reAddEstateLettableUnitEnergyRatingQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        } elseif ($report->getRepFilterLettableUnitEnergyRatingWorkRequired()) {
            // ES69: Lettable Unit Energy Rating Work Required
            $query = $estatesFilterQueryService->reAddEstateLettableUnitEnergyRatingQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        } elseif ($report->getRepFilterCapitalAccounting() || $report->getRepFilterCapitalAccountingFull()) {
            // Fixed Asset
            $filterQueryService = new FixedAssetsFilterQueryService($this->permissionService);
            $query = $filterQueryService->reAddFixedAssetsQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        } elseif ($report->getRepFilterCapitalAccountingTransaction()) {
            $filterQueryService = new FixedAssetsFilterQueryService($this->permissionService);
            $query = $filterQueryService->reAddFixedAssetTransactionQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        } elseif ($report->getRepFilterStock()) {
        /**
         * Stock Module
         */
            switch ($report->getSystemReportCode()) {
                case ReportConstant::SYSTEM_REPORT_STK01:
                    $stockFilterQueryService = new StockFilterQueryService($this->permissionService);
                    $query = $stockFilterQueryService->reAddInventoryQuery(
                        $filterData,
                        $viewName,
                        $query,
                        $whereCodes,
                        $orCodes,
                        $whereTexts
                    );
                    break;
            }
        } elseif ($report->getRepFilterTreeSurvey()) {
            switch ($report->getSystemReportCode()) {
                case ReportConstant::SYSTEM_REPORT_TREE02:
                case ReportConstant::SYSTEM_REPORT_TREE03:
                    $treeFilterQueryService = new TreeFilterQueryService($this->permissionService);
                    $query = $treeFilterQueryService->reAddSurveyQuery(
                        $filterData,
                        $viewName,
                        $query,
                        $whereCodes,
                        $orCodes,
                        $whereTexts
                    );
                    break;
            }
        } elseif ($report->getRepFilterPtw()) { // Permit To Work
            switch ($report->getRepFilterPtw()) {
                case ReportConstant::SYSTEM_REPORT_PTW01:
                    $stockFilterQueryService = new PermitToWorkFilterQueryService($this->permissionService);
                    $query = $stockFilterQueryService->reAddPermitToWorkQuery(
                        $filterData,
                        $viewName,
                        $query,
                        $whereCodes,
                        $orCodes,
                        $whereTexts
                    );
                    break;
            }
        } elseif ($report->getRepFilterInvoiceSOR()) {
            // ADM18: Invoices SOR
            $sorFilterQueryService = new InvoiceSORFilterQueryService($this->permissionService);
            $query = $sorFilterQueryService->reAddQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        } elseif ($report->getRepFilterInvoices()) {
            switch ($report->getSystemReportCode()) {
                // INV01, INV02, INV03
                case ReportConstant::SYSTEM_REPORT_INV01:
                case ReportConstant::SYSTEM_REPORT_INV02:
                case ReportConstant::SYSTEM_REPORT_INV03:
                case ReportConstant::SYSTEM_REPORT_INV04:
                    $invoicesFilterQueryService = new InvoicesFilterQueryService($this->permissionService);
                    $query = $invoicesFilterQueryService->reAddQuery(
                        $filterData,
                        $viewName,
                        $query,
                        $whereCodes,
                        $orCodes,
                        $whereTexts
                    );
                    break;

                case ReportConstant::SYSTEM_REPORT_INV_CONT01:
                case ReportConstant::SYSTEM_REPORT_INV_CONT02:
                case ReportConstant::SYSTEM_REPORT_INV_CONT03:
                    $invoicesFilterQueryService = new InvoicesFilterQueryService($this->permissionService);
                    $query = $invoicesFilterQueryService->reAddInvContQuery(
                        $filterData,
                        $viewName,
                        $query,
                        $whereCodes,
                        $orCodes,
                        $whereTexts
                    );
                    break;
                case ReportConstant::SYSTEM_REPORT_INV_CONT_CLI_REDB01:
                    $invoicesFilterQueryService = new InvoicesFilterQueryService($this->permissionService);
                    $query = $invoicesFilterQueryService->reAddInvContCliRedb01Query(
                        $filterData,
                        $viewName,
                        $query,
                        $whereCodes,
                        $orCodes,
                        $whereTexts
                    );
                    break;
                case ReportConstant::SYSTEM_REPORT_CONT09:
                    $invoicesFilterQueryService = new InvoicesFilterQueryService($this->permissionService);
                    $query = $invoicesFilterQueryService->reAddContInvDatesQuery(
                        $filterData,
                        $viewName,
                        $query,
                        $whereCodes,
                        $orCodes,
                        $whereTexts
                    );
                    break;
            }
        /**
         * Project module
         */
        } elseif ($report->getRepFilterProject()) {
            $projectsFilterQueryService = new ProjectsFilterQueryService($this->permissionService);
            switch ($report->getSystemReportCode()) {
                case ReportConstant::SYSTEM_REPORT_PRJ01:
                case ReportConstant::SYSTEM_REPORT_PRJ08:
                    // PRJ01: Project
                    // PRJ08: Project Sales Invoices
                    $query = $projectsFilterQueryService->reAddProjectQuery(
                        $filterData,
                        $viewName,
                        $query,
                        $whereCodes,
                        $orCodes,
                        $whereTexts
                    );
                    break;

                case ReportConstant::SYSTEM_REPORT_PRJ02:
                    // PRJ02: Project Note
                    $query = $projectsFilterQueryService->reAddProjectNoteQuery(
                        $filterData,
                        $viewName,
                        $query,
                        $whereCodes,
                        $orCodes,
                        $whereTexts
                    );
                    break;

                case ReportConstant::SYSTEM_REPORT_PRJ03:
                    // PRJ03: Project Issue
                    $query = $projectsFilterQueryService->reAddProjectItemsQuery(
                        $filterData,
                        $viewName,
                        $query,
                        $whereCodes,
                        $orCodes,
                        $whereTexts,
                        ProjectRecordType::TYPE_ISSUE
                    );
                    break;
                case ReportConstant::SYSTEM_REPORT_PRJ04:
                    // PRJ04: Project Progress
                    $query = $projectsFilterQueryService->reAddProjectProgressQuery(
                        $filterData,
                        $viewName,
                        $query,
                        $whereCodes,
                        $orCodes,
                        $whereTexts
                    );
                    break;

                case ReportConstant::SYSTEM_REPORT_PRJ05:
                    // PRJ05: Project Risk
                    $query = $projectsFilterQueryService->reAddProjectItemsQuery(
                        $filterData,
                        $viewName,
                        $query,
                        $whereCodes,
                        $orCodes,
                        $whereTexts,
                        ProjectRecordType::TYPE_RISK
                    );
                    break;

                case ReportConstant::SYSTEM_REPORT_PRJ06:
                    // PRJ06: Project Action
                    $query = $projectsFilterQueryService->reAddProjectItemsQuery(
                        $filterData,
                        $viewName,
                        $query,
                        $whereCodes,
                        $orCodes,
                        $whereTexts,
                        ProjectRecordType::TYPE_ACTION
                    );
                    break;

                case ReportConstant::SYSTEM_REPORT_PRJ07:
                    // PRJ07: Project Timesheets
                    $query = $projectsFilterQueryService->reAddTimesheetQuery(
                        $filterData,
                        $viewName,
                        $query,
                        $whereCodes,
                        $orCodes,
                        $whereTexts
                    );
                    break;

                case ReportConstant::SYSTEM_REPORT_PRJ09:
                    // PRJ09: Project Change Request
                    $query = $projectsFilterQueryService->reAddProjectChangeRequestQuery(
                        $filterData,
                        $viewName,
                        $query,
                        $whereCodes,
                        $orCodes,
                        $whereTexts
                    );
                    break;

                case ReportConstant::SYSTEM_REPORT_PRJ10:
                case ReportConstant::SYSTEM_REPORT_PRJ16:
                    // PRJ10: Project Cash Flow
                    $query = $projectsFilterQueryService->reAddProjectCashFlowQuery(
                        $filterData,
                        $viewName,
                        $query,
                        $whereCodes,
                        $orCodes,
                        $whereTexts
                    );
                    $query = $projectsFilterQueryService->reAddProjectQuery(
                        $filterData,
                        $viewName,
                        $query,
                        $whereCodes,
                        $orCodes,
                        $whereTexts
                    );
                    break;

                case ReportConstant::SYSTEM_REPORT_PRJ14:
                    // PRJ14: Project Tasks
                    $query = $projectsFilterQueryService->reAddProjectTaskQuery(
                        $filterData,
                        $viewName,
                        $query,
                        $whereCodes,
                        $orCodes,
                        $whereTexts
                    );
                    break;

                case ReportConstant::SYSTEM_REPORT_PRJ15:
                    // PRJ09: Project Change Request
                    $query = $projectsFilterQueryService->reAddProjAdditionalContactsQuery(
                        $filterData,
                        $viewName,
                        $query,
                        $whereCodes,
                        $orCodes,
                        $whereTexts
                    );

                    break;

                case ReportConstant::SYSTEM_REPORT_PRJ18:
                    // PRJ18: Project Bill Of Qty
                    $query = $projectsFilterQueryService->reAddProjectBillOfQtyQuery(
                        $filterData,
                        $viewName,
                        $query,
                        $whereCodes,
                        $orCodes,
                        $whereTexts
                    );

                    break;

                case ReportConstant::SYSTEM_REPORT_PRJ20:
                    // PRJ20: Project Certificated Payments
                    $query = $projectsFilterQueryService->reAddProjectCertificatedPaymentsQuery(
                        $filterData,
                        $viewName,
                        $query,
                        $whereCodes,
                        $orCodes,
                        $whereTexts
                    );
                    break;
                default:
                    break;
            }
        /**
         * Instruction module
         */
        } elseif ($report->getRepFilterInstructions()) {
            $instructionsFilterQueryService = new InstructionsFilterQueryService($this->permissionService);
            switch ($report->getSystemReportCode()) {
                case ReportConstant::SYSTEM_REPORT_ADM32:
                    // ADM32: Account Code
                    $query = $instructionsFilterQueryService->reAddAccountCodeQuery(
                        $filterData,
                        $viewName,
                        $query,
                        $whereCodes,
                        $orCodes,
                        $whereTexts
                    );
                    break;

                case ReportConstant::SYSTEM_REPORT_INT01:
                case ReportConstant::SYSTEM_REPORT_INT02:
                case ReportConstant::SYSTEM_REPORT_INT04:
                case ReportConstant::SYSTEM_REPORT_INT05:
                case ReportConstant::SYSTEM_REPORT_INT06:
                case ReportConstant::SYSTEM_REPORT_INT08:
                case ReportConstant::SYSTEM_REPORT_INT09:
                case ReportConstant::SYSTEM_REPORT_INT10:
                case ReportConstant::SYSTEM_REPORT_INT11:
                case ReportConstant::SYSTEM_REPORT_INT12:
                case ReportConstant::SYSTEM_REPORT_INT13:
                case ReportConstant::SYSTEM_REPORT_INT14:
                case ReportConstant::SYSTEM_REPORT_INT15:
                case ReportConstant::SYSTEM_REPORT_INT16:
                case ReportConstant::SYSTEM_REPORT_INT17:
                case ReportConstant::SYSTEM_REPORT_INT18:
                case ReportConstant::SYSTEM_REPORT_INT19:
                case ReportConstant::SYSTEM_REPORT_INT20:
                case ReportConstant::SYSTEM_REPORT_INT21:
                case ReportConstant::SYSTEM_REPORT_INT22:
                case ReportConstant::SYSTEM_REPORT_INT25:
                case ReportConstant::SYSTEM_REPORT_INT_CLI_OSS01:
                case ReportConstant::SYSTEM_REPORT_INT_CLI_PP01:
                    // INT01, INT02, INT04, INT05, INT06, INT09, INT10, INT11, INT12, INT13
                    $query = $instructionsFilterQueryService->reAddInstructionQuery(
                        $filterData,
                        $viewName,
                        $query,
                        $whereCodes,
                        $orCodes,
                        $whereTexts
                    );

                    break;

                case ReportConstant::SYSTEM_REPORT_INT_CLI_MK01:
                    $query = $instructionsFilterQueryService->reAddInstructionQuery(
                        $filterData,
                        $viewName,
                        $query,
                        $whereCodes,
                        $orCodes,
                        $whereTexts,
                        true
                    );

                    break;

                case ReportConstant::SYSTEM_REPORT_INT_CLI_NBS01:
                    $instructionsNBS01FilterQueryService =
                        new InstructionsNBS01FilterQueryService($this->permissionService);
                    $query = $instructionsNBS01FilterQueryService->reAddInstructionQuery(
                        $filterData,
                        $viewName,
                        $query,
                        $whereCodes,
                        $orCodes,
                        $whereTexts,
                        true
                    );

                    break;

                case ReportConstant::SYSTEM_REPORT_INT07:
                    // INT07
                    $filterData['type'] = FinAccountType::EXPENDITURE;
                    $query = $instructionsFilterQueryService->reAddInstructionINT07Query(
                        $filterData,
                        $query,
                        $whereCodes,
                        $orCodes,
                        $whereTexts
                    );
                    break;

                default:
                    break;
            }

        /**
         * Interfaces
         */
        } elseif ($report->getRepFilterInterfaces()) {
            $interfacesFilterQueryService = new InterfacesFilterQueryService($this->permissionService);
            switch ($report->getRepFilterInterfaces()) {
                case ReportConstant::SYSTEM_REPORT_INF01:
                    // INF01: Interface Execution Log Report
                    $query = $interfacesFilterQueryService->reAddInterfacesQuery(
                        $filterData,
                        $viewName,
                        $query,
                        $whereCodes,
                        $orCodes,
                        $whereTexts
                    );
                    break;
                default:
                    break;
            }
        /**
         * Questionnaire module
         */
        } elseif ($report->getRepFilterFiledQst()) {
            // QSTx01, QSTx04
            $questionnairesFilterQueryService = new QuestionnairesFilterQueryService($this->permissionService);
            $query = $questionnairesFilterQueryService->reAddFiledQstQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        } elseif ($report->getRepFilterQst()) {
            // QSTx02, QSTx03
            $questionnairesFilterQueryService = new QuestionnairesFilterQueryService($this->permissionService);
            $query = $questionnairesFilterQueryService->reAddQstQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        } elseif ($report->getRepFilterFiledQstAction()) {
            // QSTx05
            $questionnairesFilterQueryService = new QuestionnairesFilterQueryService($this->permissionService);
            $filterData['qst_type_id'] = $report->getType();
            $query = $questionnairesFilterQueryService->reAddFiledQstActionQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        } elseif ($report->getReportQstDesign()) {
            $questionnairesFilterQueryService = new QuestionnairesFilterQueryService($this->permissionService);
            $query = $questionnairesFilterQueryService->reAddFiledQstDesignQuery(
                $filterData,
                $viewName,
                $query
            );
        /**
         * Condition module
         */
        } elseif ($report->getRepFilterCondition()) {
            // CND01: Condition Survey Condition
            // CND02: Condition Survey Report
            // CND04: Identified Work by Condition Survey
            // CND05: Condition Survey (Overall And Building Score)
            // CND07: Identified Work with Specific Remedies
            $conditionsFilterQueryService = new ConditionsFilterQueryService($this->permissionService);
            $query = $conditionsFilterQueryService->reAddCondQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        } elseif ($report->getRepFilterConditionIdentifiedWork() || $report->getRepFilterIdworkTotalCostByProperty()) {
            // CND03: Identified Work AND CND11: Identified Work Total Cost by Property
            $conditionsFilterQueryService = new ConditionsFilterQueryService($this->permissionService);
            $query = $conditionsFilterQueryService->reAddCondIWQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        } elseif ($report->getRepFilterConditionItemsBySurvey()) {
            // CND06: Condition Items by Condition Survey
            $conditionsFilterQueryService = new ConditionsFilterQueryService($this->permissionService);
            $query = $conditionsFilterQueryService->reAddCondItemsQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        /*
         * Suitability Module
         */
        } elseif ($report->getRepFilterSuitability()) {
            $suitabilitiesFilterQueryService = new SuitabilitiesFilterQueryService($this->permissionService);
            $query = $suitabilitiesFilterQueryService->reAddSuitabilityQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        /*
         * Sufficiency Module
         */
        } elseif ($report->getRepFilterSufficiency()) {
            $sufficienciesFilterQueryService = new SufficienciesFilterQueryService($this->permissionService);
            $query = $sufficienciesFilterQueryService->reAddSufficiencyQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        /*
         * Grounds Maintenance Module
         */
        } elseif ($reportCode = $report->getRepFilterGroundsMaintenance()) {
            $gmFilterQueryService = new GroundsMaintenanceFilterQueryService($this->permissionService);
            switch ($reportCode) {
                case ReportConstant::SYSTEM_REPORT_GM01:
                    $query = $gmFilterQueryService->reAddGroundsMaintenanceContractQuery(
                        $filterData,
                        $viewName,
                        $query,
                        $whereCodes,
                        $orCodes,
                        $whereTexts
                    );
                    break;
                case ReportConstant::SYSTEM_REPORT_GM02:
                    $query = $gmFilterQueryService->reAddGroundsMaintenanceJobQuery(
                        $filterData,
                        $viewName,
                        $query,
                        $whereCodes,
                        $orCodes,
                        $whereTexts
                    );
                    break;
                case ReportConstant::SYSTEM_REPORT_GM03:
                    $query = $gmFilterQueryService->reAddGroundsMaintenanceExtTaskQuery(
                        $filterData,
                        $viewName,
                        $query,
                        $whereCodes,
                        $orCodes,
                        $whereTexts
                    );
                    break;
            }
        } elseif ($reportCode = $report->getRepFilterCaseManagement()) {
            $cmFilterQueryService = new CaseManagementFilterQueryService($this->permissionService);
            switch ($reportCode) {
                case ReportConstant::SYSTEM_REPORT_CM01:
                case ReportConstant::SYTEM_REPORT_CM_CLI_SAYR_01:
                    $query = $cmFilterQueryService->reAddCaseManagementCaseQuery(
                        $filterData,
                        $viewName,
                        $query,
                        $whereCodes,
                        $orCodes,
                        $whereTexts
                    );
                    break;
                case ReportConstant::SYSTEM_REPORT_CM02:
                    $query = $cmFilterQueryService->reAddCaseManagementStageQuery(
                        $filterData,
                        $viewName,
                        $query,
                        $whereCodes,
                        $orCodes,
                        $whereTexts
                    );
                    break;
            }
        /*
         * Capacity Module
         */
        } elseif ($report->getRepFilterCapacity()) {
            $capacitiesFilterQueryService = new CapacitiesFilterQueryService($this->permissionService);
            $query = $capacitiesFilterQueryService->reAddCapacityQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );

        /*
         * Resource Booking Module
         */
        } elseif ($report->getRepFilterBookableResource()) {
            $resourceBookingsFilterQueryService = new ResourceBookingsFilterQueryService($this->permissionService);
            $query = $resourceBookingsFilterQueryService->reAddBookableResourceQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        } elseif ($report->getRepFilterBookableResourceAvailability()) {
            $resourceBookingsFilterQueryService = new ResourceBookingsFilterQueryService($this->permissionService);
            $query = $resourceBookingsFilterQueryService->reAddBookableResourceAvailabilityQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        } elseif ($report->getRepFilterResourceBooking()) {
            $resourceBookingFilterQueryService = new ResourceBookingsFilterQueryService($this->permissionService);
            $query = $resourceBookingFilterQueryService->reAddResourceBookingQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        } elseif ($report->getRepFilterCateringItem()) {
            $resourceBookingsFilterQueryService = new ResourceBookingsFilterQueryService($this->permissionService);
            $query = $resourceBookingsFilterQueryService->reAddCateringItemQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        } elseif ($report->getRepFilterCateringBooking()) {
            $filterQueryService = new ResourceBookingsFilterQueryService($this->permissionService);
            $query = $filterQueryService->reAddCateringBookingQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );

        /*
         * Utility Module
         */
        } elseif ($report->getRepFilterUtilityRecord()) {
            //UT04: Utility Record
            $utilityFilterQueryService = new UtilityFilterQueryService(
                $this->permissionService,
                new UserDefinedService()
            );
            $query = $utilityFilterQueryService->reAddUtilityRecordQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        } elseif ($report->getRepFilterUtilityBill()) {
            //UT05: Utility Bill
            $utilityFilterQueryService = new UtilityFilterQueryService(
                $this->permissionService,
                new UserDefinedService()
            );
            $query = $utilityFilterQueryService->reAddUtilityBillQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );


            /*
             * Plant
             */
        } elseif ($report->getRepFilterPlantGrp()) {
            $plantFilterQueryService = new PlantFilterQueryService($this->permissionService);
            $query = $plantFilterQueryService->reAddPlantQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        /*
         * Datafeed
         */
        } elseif ($report->getRepFilterDatafeed()) {
            $datafeedFilterQueryService = new DatafeedFilterQueryService($this->permissionService);
            $query = $datafeedFilterQueryService->reAddDatafeedQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        /*
        * Contract Module
        */
        } elseif ($report->getRepFilterPl03()) {
            $plantOOSHistoryService = new PlantOutOfServiceHistoryQueryService($this->permissionService);
            $query = $plantOOSHistoryService->reAddPlantOOSHistoryRecordsQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        } elseif ($report->getRepFilterContract()) {
            //CONT01
            $contractFilterQueryService = new ContractFilterQueryService($this->permissionService);
            $query = $contractFilterQueryService->reAddContractQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        } elseif ($report->getRepFilterContractInstructions()) {
            //CONT02
            $contractFilterQueryService = new ContractFilterQueryService($this->permissionService);
            $query = $contractFilterQueryService->reAddContractInstructionQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        } elseif ($report->getRepFilterContractInvoice()) {
            //CONT03
            $contractFilterQueryService = new ContractFilterQueryService($this->permissionService);
            $query = $contractFilterQueryService->reAddContractInvoiceQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        } elseif ($report->getRepFilterContractInvoiceInspection()) {
            //CONT06
            $contractFilterQueryService = new ContractFilterQueryService($this->permissionService);
            $query = $contractFilterQueryService->reAddContractInvoiceQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        } elseif ($report->getRepFilterContractCredit()) {
            $contractFilterQueryService = new ContractFilterQueryService($this->permissionService);
            $query = $contractFilterQueryService->reAddContractCreditQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        } elseif ($report->getRepFilterContractCreditInspection()) {
            $contractFilterQueryService = new ContractFilterQueryService($this->permissionService);
            $query = $contractFilterQueryService->reAddContractCreditQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        } elseif ($report->getRepFilterContractCreditApplication()) {
            $contractFilterQueryService = new ContractFilterQueryService($this->permissionService);
            $query = $contractFilterQueryService->reAddContractCreditQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        } elseif ($report->getRepFilterContractInvoiceCredit()) {
            $contractFilterQueryService = new ContractFilterQueryService($this->permissionService);
            $query = $contractFilterQueryService->reAddContractInvoiceCreditQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        } elseif ($report->getRepFilterContractInvoiceCreditInspection()) {
            $contractFilterQueryService = new ContractFilterQueryService($this->permissionService);
            $query = $contractFilterQueryService->reAddContractInvoiceCreditQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        } elseif ($report->getRepFilterContractInvoiceCreditApplication()) {
            $contractFilterQueryService = new ContractFilterQueryService($this->permissionService);
            $query = $contractFilterQueryService->reAddContractInvoiceCreditQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        } elseif ($report->getRepFilterContractInspection()) {
            $contractFilterQueryService = new ContractFilterQueryService($this->permissionService);
            $query = $contractFilterQueryService->reAddContractInspectionQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        } elseif ($report->getRepFilterContractAFP()) {
            // CONT12
            $contractFilterQueryService = new ContractFilterQueryService($this->permissionService);
            $query = $contractFilterQueryService->reAddContractAFPQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        } elseif ($report->getRepFilterContractCertificate()) {
            // CONT13
            $contractFilterQueryService = new ContractFilterQueryService($this->permissionService);
            $query = $contractFilterQueryService->reAddContractCertificateQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        } elseif ($report->getRepFilterContractInvoiceApplication()) {
            //CONT15
            $contractFilterQueryService = new ContractFilterQueryService($this->permissionService);
            $query = $contractFilterQueryService->reAddContractInvoiceQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        } elseif ($report->getRepFilterInspection()) {
            switch ($report->getSystemReportCode()) {
                case ReportConstant::SYSTEM_REPORT_INT_CLI_NBS02:
                    \log::error('NBS02');
                    $inspectionNBS02FilterQueryService =
                        new InspectionsNBS02FilterQueryService($this->permissionService);
                    $query = $inspectionNBS02FilterQueryService->reAddInspectionQuery(
                        $filterData,
                        $viewName,
                        $query,
                        $whereCodes,
                        $orCodes,
                        $whereTexts,
                        true
                    );
                    break;
                default:
                    $query = $this->reAddInspectionQuery(
                        $filterData,
                        $viewName,
                        $query,
                        $whereCodes,
                        $orCodes,
                        $whereTexts
                    );
            }
        } elseif ($report->getRepFilterCodeDescActive()) {
            $adminCodeService = new AdminCodeFilterQueryService($this->permissionService);

            switch ($report->getSystemReportCode()) {
                case ReportConstant::SYSTEM_REPORT_UT01:
                    // UT01: Utility Limit
                    $query = $adminCodeService->reAddUtilityLimitQuery(
                        $filterData,
                        $viewName,
                        $query,
                        $whereCodes,
                        $orCodes,
                        $whereTexts
                    );
                    break;
                case ReportConstant::SYSTEM_REPORT_UT02:
                    // UT02: Utility Use
                    $query = $adminCodeService->reAddUtilityUseQuery(
                        $filterData,
                        $viewName,
                        $query,
                        $whereCodes,
                        $orCodes,
                        $whereTexts
                    );
                    break;

                default:
                    break;
            }
        } elseif ($report->getRepFilterRoomCapacity()) {
            // CAP03
            $capacityRoomService = new CapacitiesFilterQueryService($this->permissionService);
            $query = $capacityRoomService->reAddRoomCapQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        } elseif ($report->getRepFilterUser()) {
            $userService = new UsersFilterQueryService($this->permissionService);
            $query = $userService->reAddUserQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        } elseif ($report->getRepFilterUserLogin()) {
            $userService = new UsersFilterQueryService($this->permissionService);
            $query = $userService->reAddUserLoginQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        }

        if ($report->getRepFilterInterfaceLog()) {
            $this->reInterfaceLogQuery($filterData, $query, $whereTexts);
        }


        if ($report->getRepFilterEmail()) {
            $this->reAddEmailQuery($report, $query, $filterData, $viewName, $whereCodes, $whereTexts);
        }

        if ($report->getRepFilterIncidentRecords()) {
            $incidentRecordsService = new IncidentManagementQueryService($this->permissionService);
            $query = $incidentRecordsService->reAddIncidentRecordsQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        }

        if ($report->getRepFilterIncidentAction()) {
            $incidentRecordsService = new IncidentManagementQueryService($this->permissionService);
            $query = $incidentRecordsService->reAddIncidentActionsQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        }

        if ($report->getRepFilterTreeSurveyAction()) {
            $treeSurveyRecordsService = new TreeFilterQueryService($this->permissionService);
            $query = $treeSurveyRecordsService->reAddTreeSurveyActionsQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        }

        if ($report->getRepFilterPers03()) {
            $personnelFilterQueryService = new PersonnelFilterQueryService($this->permissionService);
            $query = $personnelFilterQueryService->reAddPersonnelBookingsQuery(
                $filterData,
                $viewName,
                $query,
                $whereCodes,
                $orCodes,
                $whereTexts
            );
        }

        return $query;
    }

    private function reAddEmailQuery($report, &$query, $filterData, $viewName, &$whereCodes, &$whereTexts)
    {
        $repCode = $report->getSystemReportCode();
        $emailService = new EmailFilterQueryService($this->permissionService);
        switch ($repCode) {
            case ReportConstant::SYSTEM_REPORT_ADM36:
                $query = $emailService->reAddADM36Query(
                    $filterData,
                    $viewName,
                    $query,
                    $whereCodes,
                    $whereTexts
                );
                break;
            case ReportConstant::SYSTEM_REPORT_ADM38:
                $query = $emailService->reAddADM38Query(
                    $filterData,
                    $viewName,
                    $query,
                    $whereCodes,
                    $whereTexts
                );
                break;
        }
    }

    private function viewFormat($viewName)
    {
        $viewName = explode('_', $viewName);
        $viewNameModel = '';
        if (count($viewName) > 0) {
            foreach ($viewName as $value) {
                $viewNameModel .= ucfirst(strtolower($value));
            }
        }

        return $viewNameModel;
    }

    private function reGetReportColumns($report, $viewTable)
    {
        if (!$report instanceof Report) {
            return null;
        }

        $reportFields = ReportField::select('report_field_code')
                ->where('output_by_default', '=', CommonConstant::DATABASE_VALUE_YES)
                ->where('system_report_id', '=', $report->getRepReportId())
                ->orderBy('output_order')
                ->get();

        $repUserDefData = $report->getRepUserdefData();

        $columns = [];
        foreach ($reportFields as $reportField) {
            $bAddColumn = false;
            $labelIndex = $this->reGetUserDefLabel($reportField->report_field_code, $repUserDefData);

            if (isset($repUserDefData[$labelIndex])) {
                // Don't need user defined columns where the label is blank.
                if ($repUserDefData[$labelIndex] != '') {
                    $bAddColumn = true;
                }
            } else {
                $bAddColumn = true;
            }

            if ($bAddColumn) {
                $columns[] = $viewTable . '.' . "`" . $reportField->report_field_code . "`";
            }
        }

        // Concatenate into a string to be returned.
        return implode(",", $columns);
    }

    private function reAddInspectionQuery($filterData, $viewName, $query, &$whereCodes, &$orCodes, &$whereTexts)
    {
        $inspectionService = new InspectionService($this->permissionService);
        $inspectionQuery = $inspectionService->filterAll($query, $filterData, $viewName);

        if ($val = Common::iset($filterData['targetFrom'])) {
            array_push($whereTexts, "Due Date from {$val}");
        }

        if ($val = Common::iset($filterData['targetTo'])) {
            array_push($whereTexts, "Due Date to {$val}");
        }

        if (Common::iset($filterData['includeDateFloat'])) {
            if (Common::valueYorNtoBoolean($filterData['includeDateFloat']) && Common::iset($filterData['targetTo'])) {
                array_push($whereTexts, "Include Date Float = Y");
            }
        }

        if ($val = Common::iset($filterData['inspection_status_id'])) {
            array_push(
                $whereCodes,
                array('inspection_status', 'inspection_status_id', 'code', $val, 'Inspection Status')
            );
        }

        $statusType = [];
        if (Common::iset($filterData['draft'])) {
            array_push($statusType, InspSFG20InspectionStatusType::DRAFT);
        }
        if (Common::iset($filterData['open'])) {
            array_push($statusType, InspSFG20InspectionStatusType::OPEN);
        }
        if (Common::iset($filterData['issued'])) {
            array_push($statusType, InspSFG20InspectionStatusType::ISSUED);
        }
        if (Common::iset($filterData['complete'])) {
            array_push($statusType, InspSFG20InspectionStatusType::COMPLETE);
        }
        if (Common::iset($filterData['cancelled'])) {
            array_push($statusType, InspSFG20InspectionStatusType::CANCELLED);
        }
        if (Common::iset($filterData['onHold'])) {
            array_push($statusType, InspSFG20InspectionStatusType::ONHOLD);
        }
        if (Common::iset($filterData['closed'])) {
            array_push($statusType, InspSFG20InspectionStatusType::CLOSED);
        }
        if (is_array($statusType) && count($statusType)) {
            array_push(
                $orCodes,
                array(
                    'inspection_status_type',
                    'inspection_status_type_id',
                    'inspection_status_type_code',
                    $statusType,
                    'Inspection Status Type'
                )
            );
        }

        if ($val = Common::iset($filterData['inspection'])) {
            array_push($whereTexts, "Inspection  Code contains '" . $val . "'");
        }

        if ($val = Common::iset($filterData['inspection_group_id'])) {
            array_push(
                $whereCodes,
                array('inspection_group', 'inspection_group_id', 'inspection_group_code', $val, 'Inspection Group')
            );

            if ($val = Common::iset($filterData['inspection_type_id'])) {
                array_push(
                    $whereCodes,
                    array('inspection_type', 'inspection_type_id', 'inspection_type_code', $val, 'Inspection Type')
                );
            }
        }

        if ($val = Common::iset($filterData['owner'])) {
            array_push(
                $whereCodes,
                array('user', 'id', 'display_name', $val, 'Owner')
            );
        }

        if ($val = Common::iset($filterData['plantOwner'])) {
            array_push(
                $whereCodes,
                array('user', 'id', 'display_name', $val, 'Plant Owner')
            );
        }

        if ($val = Common::iset($filterData['plant_group_id'])) {
            array_push(
                $whereCodes,
                array('plant_group', 'plant_group_id', 'plant_group_code', $val, 'Plant Group')
            );

            if ($val = Common::iset($filterData['plant_subgroup_id'])) {
                array_push(
                    $whereCodes,
                    array('plant_subgroup', 'plant_subgroup_id', 'plant_subgroup_code', $val, 'Plant Subgroup')
                );

                if ($val = Common::iset($filterData['plant_type_id'])) {
                    array_push(
                        $whereCodes,
                        array('plant_type', 'plant_type_id', 'plant_type_code', $val, 'Plant Type')
                    );
                }
            }
        }

        if ($val = Common::iset($filterData['supplier'])) {
            array_push(
                $whereCodes,
                array('contact', 'contact_id', 'contact_name', $val, 'Supplier')
            );
        }

        if ($val = Common::iset($filterData['contract_id'])) {
            array_push(
                $whereCodes,
                array('contract', 'contract_id', 'contract_code', $val, 'Contract')
            );
        }

        if ($val = Common::iset($filterData['establishment'])) {
            array_push(
                $whereCodes,
                array('establishment', 'establishment_id', 'establishment_code', $val, 'Establishment')
            );
        }

        if ($val = Common::iset($filterData['plant_code'])) {
            array_push($whereTexts, "Plant Code contains '" . $val . "'");
        }

        if ($val = array_get($filterData, 'appointmentFrom', null)) {
            array_push($whereTexts, 'Appointment From ' . " $val" . "'");
        }

        if ($val = array_get($filterData, 'appointmentTo', null)) {
            array_push($whereTexts, "Appointment To '" . " $val" . "'");
        }

        $val = array_get($filterData, 'perf_in_house', null);
        if (!is_null($val)) {
            if ($val == '1') {
                array_push($whereTexts, "Performed In House = Yes");
            } elseif ($val == '0') {
                array_push($whereTexts, "Performed In House = No");
            }
        }

        $val = array_get($filterData, 'statutory', null);
        if (!is_null($val)) {
            if ($val == '1') {
                array_push($whereTexts, "Statutory = Yes");
            } elseif ($val == '0') {
                array_push($whereTexts, "Statutory = No");
            }
        }

        if ($val = Common::iset($filterData['prop_zone_id'])) {
            array_push($whereCodes, ['prop_zone', 'prop_zone_id', 'prop_zone_code', $val, 'Zone']);

            if ($val = Common::iset($filterData['prop_external_id'])) {
                array_push(
                    $whereCodes,
                    ['prop_external', 'prop_external_id', 'prop_external_code', $val, 'External Area']
                );
            }
        }

        if ($val = array_get($filterData, 'completedFrom', null)) {
            array_push($whereTexts, "Completed From = '$val'");
        }

        if ($val = array_get($filterData, 'completedTo', null)) {
            array_push($whereTexts, "Completed To = '$val'");
        }

        $val = array_get($filterData, 'completed_late', null);
        if (!is_null($val)) {
            if ($val == '1') {
                array_push($whereTexts, "Completed Late = Yes");
            } elseif ($val == '0') {
                array_push($whereTexts, "Completed Late = No");
            }
        }

        $seriesActive = Common::iset($filterData['series_active']);
        if ($seriesActive) {
            switch ($seriesActive) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, 'Active Series = Yes');
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, 'Active Series = No');
                    break;
            }
        }

        $val = array_get($filterData, 'series_list', null);
        if (!is_null($val)) {
            if ($val == 'Y') {
                array_push($whereTexts, "Series List = Series");
            } elseif ($val == 'N') {
                array_push($whereTexts, "Series List = All");
            }
        }

        if ($val = Common::iset($filterData['account'])) {
            array_push(
                $whereCodes,
                array('fin_account', 'fin_account_id', 'fin_account_code', $val, 'Account')
            );
        }

        $spotCheckRequired = Common::iset($filterData['spotCheckRequired']);
        if ($spotCheckRequired) {
            switch ($spotCheckRequired) {
                case CommonConstant::DATABASE_VALUE_YES:
                    array_push($whereTexts, 'Spot Check Required = Yes');
                    break;
                case CommonConstant::DATABASE_VALUE_NO:
                    array_push($whereTexts, 'Spot Check Required = No');
                    break;
                default:
                    array_push($whereTexts, 'Spot Check Required = All');
                    break;
            }
        }

        $spotCheckStatusType = [];
        if (Common::iset($filterData['spotCheckForApproval'])) {
            array_push($spotCheckStatusType, FinAuthStatus::APPROVAL);
        }
        if (Common::iset($filterData['spotCheckApproved'])) {
            array_push($spotCheckStatusType, FinAuthStatus::APPROVED);
        }
        if (Common::iset($filterData['spotCheckRejected'])) {
            array_push($spotCheckStatusType, FinAuthStatus::REJECTED);
        }
        if (is_array($spotCheckStatusType) && count($spotCheckStatusType)) {
            array_push(
                $orCodes,
                array(
                    'fin_auth_status',
                    'fin_auth_status_id',
                    'name',
                    $spotCheckStatusType,
                    'Spot Check Audit'
                )
            );
        }

        $this->reAddUserDefinesQuery(
            GenTable::INSPECTION,
            $filterData,
            $whereCodes,
            $whereTexts
        );

        return $inspectionQuery;
    }

    private function reInterfaceLogQuery($filterData, $query, &$whereTexts)
    {
        if ($id = Common::iset($filterData['id'])) {
            $query->where('interface_log_id', $id);
            array_push($whereTexts, "Interface Log Id contains '" . $id . "'");
        }

        return $query;
    }

    private function getSummedTotals($query, $groupByColumn, $summedNumericFields, $arrOrder, $iStartRowNum, $iNumRows)
    {
        $totals = [];

        $query->select($groupByColumn);
        foreach ($summedNumericFields as $summedNumericField) {
            $query->addSelect(DB::raw($summedNumericField));
        }

        $query->groupBy($groupByColumn)
            ->orderBy($groupByColumn, 'ASC');

        if (count($arrOrder) > 0) {
            foreach ($arrOrder as $order) {
                $query->orderBy(DB::raw($order['key']), DB::raw($order['dir']));
            }
        }

        $query->skip($iStartRowNum)->take($iNumRows);

        if (!Common::invokedFromCommandLine()) {
            // Up running time to 5 minutes
            set_time_limit(CommonConstant::TIME_LIMIT_300_SECOND);
        }

        $result = $query->get();
        if (!is_array($result)) {
            $result = $result->toArray();
        }

        foreach ($result as $row) {
            $totals[$groupByColumn][$row[$groupByColumn]] = $row;
        }

        return $totals;
    }


    /**
     * PRJ10: Replace heading Period 1, Period 2, Period 3... to Period Code
     * @param array $inputs
     * @param integer $iTotalNextYear
     * @return array
     */
    private function replaceHeadingPRJ10($inputs, &$iTotalNextYear, &$prj10FinYearPeriodCount)
    {
        $columnHeadingsToReplace = [];
        $finYearId = array_get($inputs, 'finYear');
        $finYearPeriods = FinYearPeriod::where('fin_year_id', $finYearId)
            ->orderBy('period_start_date')->get()->toArray()
        ;
        $prj10FinYearPeriodCount = count($finYearPeriods);

        for ($index = 0; $index < $prj10FinYearPeriodCount; $index++) {
            $columnHeadingsToReplace = array_add(
                $columnHeadingsToReplace,
                'Period ' . ($index + 1),
                $finYearPeriods[$index]["fin_year_period_code"]
            );
        }

        $finYear = FinYear::find($finYearId);
        if ($finYear) {
            $columnHeadingsToReplace['Total'] = 'Total (' . $finYear->fin_year_code . ')';
            if ($finYear->previous()) {
                $columnHeadingsToReplace['Previous Year'] =
                    $finYear->previous()->fin_year_code . " (Previous Year)";
            }
            // Need move last record
            for ($iTotalNextYear = 1; $iTotalNextYear <= 5; $iTotalNextYear++) {
                if ($finYear->next($iTotalNextYear)) {
                    $columnHeadingsToReplace["Next {$iTotalNextYear} Year"] =
                        $finYear->next($iTotalNextYear)->fin_year_code;
                } else {
                    break;
                }
            }
            $iTotalNextYear--;
        }
        return $columnHeadingsToReplace;
    }

    /**
     * HLP09: Replace heading QUESTION001, QUESTION002, QUESTION003, QUESTION004,... to Question
     * @return array
     */
    private function replaceHeadingHLP09()
    {
        $columnHeadingsToReplace    = [];
        $helpcallSurveyQsts         = HelpcallSurveyQst::userSiteGroup()->orderBy('helpcall_survey_qst_code')->get();
        foreach ($helpcallSurveyQsts as $helpcallSurveyQst) {
            $columnHeadingsToReplace[$helpcallSurveyQst->helpcall_survey_qst_code] = $helpcallSurveyQst->question;
        }
        return $columnHeadingsToReplace;
    }

    private function getFinYearData($row, &$columnHeadingsToReplace)
    {
        $allFinYear = FinYear::userSiteGroup()->with(['finYearPeriods'])->orderBy('year_start_date', 'asc')->get();
        $projectCashFlowQuery = ProjectCashFlow::userSiteGroup()
            ->select([
                'project_cash_flow.project_id',
                'project_cash_flow.fin_year_id',
                'project_cash_flow_acc.p1_value',
                'project_cash_flow_acc.p2_value',
                'project_cash_flow_acc.p3_value',
                'project_cash_flow_acc.p4_value',
                'project_cash_flow_acc.p5_value',
                'project_cash_flow_acc.p6_value',
                'project_cash_flow_acc.p7_value',
                'project_cash_flow_acc.p8_value',
                'project_cash_flow_acc.p9_value',
                'project_cash_flow_acc.p10_value',
                'project_cash_flow_acc.p11_value',
                'project_cash_flow_acc.p12_value',
                'project_cash_flow_acc.p13_value',
                'project_cash_flow_acc.total_value',
                DB::raw('project_budget_line.project_budget_line_code AS `Budget Line Code`'),
                DB::raw('project_budget_line.project_budget_line_desc AS `Budget Line Description`'),
                DB::raw('fin_account.fin_account_code AS `Account Code`'),
                DB::raw('fin_account.fin_account_desc AS `Account Description`'),
                'project_budget_line.project_budget_line_id',
                'fin_account.fin_account_id'
            ])
            ->join(
                'project_cash_flow_acc',
                'project_cash_flow_acc.project_cash_flow_id',
                '=',
                'project_cash_flow.project_cash_flow_id'
            )
            ->join(
                'project_budget_line',
                'project_budget_line.project_budget_line_id',
                '=',
                'project_cash_flow.project_budget_line_id'
            )
            ->join(
                'fin_account',
                'fin_account.fin_account_id',
                '=',
                'project_cash_flow_acc.fin_account_id'
            )
            ->where('project_cash_flow.project_id', $row['project_id'])
            ->get();

        $arr = [];
        foreach ($allFinYear as $year) {
            $result = $projectCashFlowQuery->where('fin_year_id', $year->fin_year_id)
                ->where('project_budget_line_id', $row['project_budget_line_id'])
                ->where('fin_account_id', $row['fin_account_id'])
                ->first();
            $total = isset($result) ? $result->total_value : 0;

            if ($this->isCurrentFinYear($year) || $this->isNextFinYear($year)) {
                $index = 1;
                foreach ($year->finYearPeriods as $item) {
                    $yearStr = explode('-', $item->period_start_date)[0];
                    $arr[$yearStr . '/' . $item["fin_year_period_code"]] =
                        isset($result['p' . $index . '_value']) ? $result['p' . $index . '_value'] : 0;
                    $index++;
                }
                $arr["Total ( $year->fin_year_code )"] = $total;
            } else {
                $arr[$year->fin_year_code . '_sub'] = $total;
                $columnHeadingsToReplace = array_add(
                    $columnHeadingsToReplace,
                    $year->fin_year_code . '_sub',
                    $year->fin_year_code
                );
            }
        }

        return $arr;
    }

    private function isCurrentFinYear($finYear)
    {
        $date = new \DateTime();
        return $finYear->year_start_date <= $date->format('Y-m-d')
            && $finYear->year_end_date >= $date->format('Y-m-d');
    }

    private function isNextFinYear($finYear)
    {
        $date = date('Y-m-d', strtotime('+1 years'));
        return $finYear->year_start_date <= $date && $finYear->year_end_date >= $date;
    }

    private function getKeyHeaderLabel($key, $repSystemReportCode, $columnHeadingsToReplace)
    {
        switch ($repSystemReportCode) {
            case ReportConstant::SYSTEM_REPORT_PRJ10:
            case ReportConstant::SYSTEM_REPORT_PRJ16:
            case ReportConstant::SYSTEM_REPORT_HLP09:
                $replacedHeading = array_get($columnHeadingsToReplace, $key);
                if ($replacedHeading) {
                    $key = $replacedHeading;
                }
                break;
            default:
                break;
        }
        return $key;
    }

    private function selectedSiteAccessQuery($repCode, $repViewModel, $reportColumns)
    {
        $query = false;

        if ($repCode == ReportConstant::SYSTEM_REPORT_INV03) {
            $query = $repViewModel::select(DB::raw($reportColumns))
            ->leftJoin(
                'invoice_fin_account AS invFinAcc',
                "{$repViewModel->getTable()}.invoice_id",
                '=',
                'invFinAcc.invoice_id'
            )
            ->leftjoin(
                'instruction AS instr',
                'instr.instruction_id',
                '=',
                'invFinAcc.instruction_id'
            )
            ->leftJoin(
                'user_access',
                'instr.site_id',
                '=',
                'user_access.site_id'
            )
            ->distinct();
        } elseif (
            $repCode == ReportConstant::SYSTEM_REPORT_CONT03 ||
            $repCode == ReportConstant::SYSTEM_REPORT_CONT09
        ) {
            $query = $repViewModel::select(DB::raw($reportColumns))
            ->leftJoin(
                'insp_sfg20_scheduled_inspection as sched_insp',
                "{$repViewModel->getTable()}.contract_invoice_id",
                '=',
                'sched_insp.contract_invoice_id'
            )
            ->leftJoin(
                'insp_sfg20_inspection_version',
                "insp_sfg20_inspection_version.insp_sfg20_inspection_version_id",
                '=',
                'sched_insp.insp_sfg20_inspection_version_id'
            )
            ->leftJoin(
                'insp_sfg20_inspection',
                "insp_sfg20_inspection.insp_sfg20_inspection_id",
                '=',
                'insp_sfg20_inspection_version.insp_sfg20_inspection_id'
            )
            ->leftJoin(
                'user_access',
                'insp_sfg20_inspection.site_id',
                '=',
                'user_access.site_id'
            )
            ->distinct();
        } elseif ($repCode == ReportConstant::SYSTEM_REPORT_CONT04) {
            $query = $repViewModel::select(DB::raw($reportColumns))
                ->leftJoin(
                    'insp_sfg20_scheduled_inspection as sched_insp',
                    "{$repViewModel->getTable()}.contract_invoice_id",
                    '=',
                    'sched_insp.contract_invoice_id'
                )
                ->leftJoin(
                    'insp_sfg20_contract_credit_scheduled_inspection',
                    'insp_sfg20_contract_credit_scheduled_inspection.insp_sfg20_scheduled_inspection_id',
                    '=',
                    'sched_insp.insp_sfg20_scheduled_inspection_id'
                )
                ->leftJoin(
                    'insp_sfg20_inspection_version',
                    "insp_sfg20_inspection_version.insp_sfg20_inspection_version_id",
                    '=',
                    'sched_insp.insp_sfg20_inspection_version_id'
                )
                ->leftJoin(
                    'insp_sfg20_inspection',
                    "insp_sfg20_inspection.insp_sfg20_inspection_id",
                    '=',
                    'insp_sfg20_inspection_version.insp_sfg20_inspection_id'
                )
                ->leftJoin('user_access', 'insp_sfg20_inspection.site_id', '=', 'user_access.site_id')
                ->distinct();
        } elseif ($repCode == ReportConstant::SYSTEM_REPORT_CONT08) {
            $query = $repViewModel::select(DB::raw($reportColumns))
                ->leftJoin(
                    'insp_sfg20_scheduled_inspection as sched_insp',
                    "{$repViewModel->getTable()}.contract_invoice_id",
                    '=',
                    'sched_insp.contract_invoice_id'
                )
                ->leftJoin(
                    'insp_sfg20_inspection_version',
                    "insp_sfg20_inspection_version.insp_sfg20_inspection_version_id",
                    '=',
                    'sched_insp.insp_sfg20_inspection_version_id'
                )
                ->leftJoin(
                    'insp_sfg20_inspection',
                    "insp_sfg20_inspection.insp_sfg20_inspection_id",
                    '=',
                    'insp_sfg20_inspection_version.insp_sfg20_inspection_id'
                )
                ->leftJoin('user_access', 'insp_sfg20_inspection.site_id', '=', 'user_access.site_id')
                ->distinct();  /* Need to add in credit notes query */
        } elseif ($repCode == ReportConstant::SYSTEM_REPORT_CONT02) {
            $query = $repViewModel::select(DB::raw($reportColumns))
                ->leftJoin(
                    'insp_sfg20_scheduled_inspection as sched_insp',
                    "{$repViewModel->getTable()}.contract_instruction_id",
                    '=',
                    'sched_insp.contract_instruction_id'
                )
                ->leftJoin(
                    'insp_sfg20_inspection_version',
                    "insp_sfg20_inspection_version.insp_sfg20_inspection_version_id",
                    '=',
                    'sched_insp.insp_sfg20_inspection_version_id'
                )
                ->leftJoin(
                    'insp_sfg20_inspection',
                    "insp_sfg20_inspection.insp_sfg20_inspection_id",
                    '=',
                    'insp_sfg20_inspection_version.insp_sfg20_inspection_id'
                )
                ->leftJoin(
                    'user_access',
                    'insp_sfg20_inspection.site_id',
                    '=',
                    'user_access.site_id'
                )
                ->distinct();
        } elseif ($repCode == ReportConstant::SYSTEM_REPORT_PPP05) {
            $query = $repViewModel::select(DB::raw($reportColumns))
            ->join(
                'ppp_pupil_year',
                "{$repViewModel->getTable()}.ppp_pupil_id",
                '=',
                'ppp_pupil_year.ppp_pupil_id'
            )
            ->leftJoin(
                'user_access',
                'ppp_pupil_year.site_id',
                '=',
                'user_access.site_id'
            )
            ->where("{$repViewModel->getTable()}.site_group_id", Auth::User()->site_group_id)
            ->distinct();
        } elseif (
            $repCode == ReportConstant::SYSTEM_REPORT_CONT01 ||
            $repCode == ReportConstant::SYSTEM_REPORT_ADM18 ||
            $repCode == ReportConstant::SYSTEM_REPORT_ADM31 ||
            $repCode == ReportConstant::SYSTEM_REPORT_ADM32 ||
            $repCode == ReportConstant::SYSTEM_REPORT_DLO01 ||
            $repCode == ReportConstant::SYSTEM_REPORT_DLO02 ||
            $repCode == ReportConstant::SYSTEM_REPORT_NOT01 ||
            $repCode == ReportConstant::SYSTEM_REPORT_ADM38 ||
            $repCode == ReportConstant::SYSTEM_REPORT_ADM38_ALT ||
            $repCode == ReportConstant::SYSTEM_REPORT_DOC01 ||
            $repCode == ReportConstant::SYSTEM_REPORT_DOC02 ||
            $repCode == ReportConstant::SYSTEM_REPORT_DLO03 ||
            //$repCode == ReportConstant::SYSTEM_REPORT_CONT12 || // So consistent with list view which doesn't fully
            $repCode == ReportConstant::SYSTEM_REPORT_CONT13    // restrict by site access.
        ) {
                    $query = $repViewModel::select(DB::raw($reportColumns));
                    $this->allSiteAccess = false;
        } else {
                $query = $repViewModel::select(DB::raw($reportColumns))
                ->leftJoin('user_access', "{$repViewModel->getTable()}.site_id", '=', 'user_access.site_id');
        }
        return $query;
    }

    //the Gate to open Grouping Service
    private function groupingQuery($query, $options)
    {
        return $this->reportGroupingService->processGroupingQuery($query, $options);
    }
}
