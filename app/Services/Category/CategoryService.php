<?php

namespace App\Services\Category;

use App\Libs\AuditFields;
use App\Libs\Common\Common;
use App\Models\AuditAction;
use App\Models\Category;
use App\Services\AuditService;
use App\Services\BaseService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CategoryService extends BaseService
{
    private Category $category;

    public function getBrandValidator()
    {
        $attributes = $this->category->getAttributes();

        $rules = [
            'category_code' => [
                'required',
                'max:15',
                'unique:category,category_code,' . $this->category->getKey() . ",category_id"
            ],
            'category_name' => ['max:15'],
            'category_slug' => ['max:512'],
            'company_id' => 'required'
        ];

        return Validator::make($attributes, $rules);
    }
    public function setModel(): string
    {
        return Category::class;
    }
    public function setModelFields(array $inputs = []): Category
    {
        $this->category = new Category();
        Common::assignField($this->category, 'category_code', $inputs);
        Common::assignField($this->category, 'category_name', $inputs);
        Common::assignField($this->category, 'category_slug', $inputs);
        Common::assignField($this->category, 'category_active', $inputs);
        Common::assignField($this->category, 'company_id', $inputs);

        return $this->category;
    }

    public function storeCategory($input): array
    {
        $this->setModelFields($input);
        $validator = $this->getBrandValidator();
        if ($validator->passes())
        {
            DB::beginTransaction();
            $this->category->save();
            //            $audit = new AuditService();
//            $audit->addAuditEntry($this->category, AuditAction::ACT_INSERT);
            DB::commit();
            return [
                'data' => $this->category,
                'success' => true
            ];
        } else {
            DB::rollBack();
            return [
                'success' => false,
                'error' => $validator->errors()
            ];
        }
    }

    private function setupAuditFields(): array
    {
        $category = Category::find($this->category->getOriginal('category_id'));
        $categoryAuditText = empty($category) ? '' : $category->category_code;

        $auditFields = new AuditFields($this->category);

        return $auditFields->addField('Description', 'category_slug')
            ->addField('Name', 'category_name')
            ->addField('Code', 'category_code', ['originalText' => $categoryAuditText])
            ->getFields();
    }

    public function updateCategory($input, $id): array
    {
        $this->getRecord($id);
        $validator = $this->getBrandValidator();
        if ($validator->passes())
        {
            DB::beginTransaction();
            $this->category->update($input);
            $this->category->save();

//            $auditFields = $this->setupAuditFields();
//            $audit = new AuditService();
//            $audit->auditFields($auditFields);
//            $audit->addAuditEntry($this->category, AuditAction::ACT_UPDATE);
            DB::commit();
            return [
                'data' => $this->category,
                'success' => true
            ];

        } else {
            DB::rollBack();
            return [
                'success' => false,
                'error' => $validator->errors()
            ];
        }
    }

    public function getRecord($id)
    {
        $this->category = Category::find($id);
        return $this->category;
    }

    /**
     * @throws \Exception
     */
    public function deleteCategory($id): bool
    {
        $category = $this->getRecord($id);
        $result = Common::canDelete($category, 'category_id', $id);
        if ($result)
        {
//            $audit = new AuditService();
//            $audit->addAuditEntry($category, AuditAction::ACT_DELETE);
            $category->delete();
        }
        return $result;
    }
}
