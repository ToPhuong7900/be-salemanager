<?php

namespace App\Services\Category;

use App\Libs\AuditFields;
use App\Libs\Common\Common;
use App\Models\Category;
use App\Models\SubCategory;
use App\Services\BaseService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class SubCategoryService extends BaseService
{
    private SubCategory $subCategory;

    public function getSubCategoryValidator()
    {
        $attributes = $this->subCategory->getAttributes();

        $rules = [
            'sub_category_code' => [
                'required',
                'max:15',
                'unique:sub_category,sub_category_code,' . $this->subCategory->getKey() . ",sub_category_id"
            ],
            'sub_category_name' => ['max:15'],
            'sub_category_slug' => ['max:512'],
            'company_id' => 'required',
            'category_id' => 'required'
        ];

        return Validator::make($attributes, $rules);
    }

    public function setModel(): string
    {
        return SubCategory::class;
    }

    public function setModelFields(array $inputs = []): SubCategory
    {
        $this->subCategory = new SubCategory();
        Common::assignField($this->subCategory, 'sub_category_code', $inputs);
        Common::assignField($this->subCategory, 'sub_category_name', $inputs);
        Common::assignField($this->subCategory, 'sub_category_slug', $inputs);
        Common::assignField($this->subCategory, 'sub_category_active', $inputs);
        Common::assignField($this->subCategory, 'company_id', $inputs);
        Common::assignField($this->subCategory, 'category_id', $inputs);

        return $this->subCategory;
    }

    public function storeSubCategory($input): array
    {
        $this->setModelFields($input);

        $validator = $this->getSubCategoryValidator();
        if ($validator->passes())
        {
            DB::beginTransaction();
            $this->subCategory->save();
            //            $audit = new AuditService();
//            $audit->addAuditEntry($this->$this->subCategory, AuditAction::ACT_INSERT);
            DB::commit();
            return [
                'data' => $this->subCategory,
                'success' => true
            ];
        } else {
            DB::rollBack();
            return [
                'success' => false,
                'error' => $validator->errors()
            ];
        }
    }

    private function setupAuditFields(): array
    {
        $subCategory = SubCategory::find($this->subCategory->getOriginal('sub_category_id'));
        $subCategoryAuditText = empty($subCategory) ? '' : $subCategory->sub_category_code;

        $auditFields = new AuditFields($this->subCategory);

        return $auditFields->addField('Description', 'sub_category_slug')
            ->addField('Name', 'sub_category_name')
            ->addField('Code', 'sub_category_code', ['originalText' => $subCategoryAuditText])
            ->getFields();
    }

    public function updateCategory($input, $id): array
    {
        $this->getRecord($id);
        $validator = $this->getSubCategoryValidator();
        if ($validator->passes())
        {
            DB::beginTransaction();
            $this->subCategory->update($input);
            $this->subCategory->save();

//            $auditFields = $this->setupAuditFields();
//            $audit = new AuditService();
//            $audit->auditFields($auditFields);
//            $audit->addAuditEntry($this->$this->subCategory, AuditAction::ACT_UPDATE);
            DB::commit();
            return [
                'data' => $this->subCategory,
                'success' => true
            ];

        } else {
            DB::rollBack();
            return [
                'success' => false,
                'error' => $validator->errors()
            ];
        }
    }

    public function getRecord($id)
    {
        $this->subCategory = SubCategory::find($id);
        return $this->subCategory;
    }

    /**
     * @throws \Exception
     */
    public function deleteCategory($id): bool
    {
        $subCategory = $this->getRecord($id);
        $result = Common::canDelete($subCategory, 'sub_category_id', $id);
        if ($result)
        {
//            $audit = new AuditService();
//            $audit->addAuditEntry($subCategory, AuditAction::ACT_DELETE);
            $subCategory->delete();
        }
        return $result;
    }
}
