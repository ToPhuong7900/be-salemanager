<?php

namespace App\Services;

use App\Exceptions\NotFoundException;
use App\Libs\Common\CommonConstant;
use App\Models\BaseModel;
use BadMethodCallException;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

abstract class BaseService
{
    private BaseModel $model;
    public function __construct()
    {
        $this->model = $this->getModel();
    }

    public function __call($method, $arguments)
    {
        if (method_exists($this, $method)) {
            return $this->$method(...$arguments);
        }
        if (Str::startsWith($method, 'getBy')) {
            $column = strtolower(str_replace('getBy', '', $method));
            return $this->getByColumn($column, ...$arguments);
        }

        throw new BadMethodCallException("Method {$method} does not exist.");
    }

    abstract public function setModel(): string;

    public function getModel()
    {
        $modelClass = $this->setModel();
        $this->model = new $modelClass;
        return $this->model;
    }
    public array $pageSize = [
        '5' => '5 Per Page',
        '10' => '10 Per Page',
        '15' => '15 Per Page',
        '25' => '25 Per Page',
        '50' => '50 Per Page',
        '75' => '75 Per Page',
        '100' => '100 Per Page',
        '150' => '150 Per Page',
        '500' => '500 Per Page'
    ];

    public function pagePer($pagePer)
    {
        $pagePer = $pagePer && array_key_exists($pagePer, $this->pageSize) ? $pagePer : \Auth::user()->default_pagesize;

        if ($pagePer >= 576) {
            set_time_limit(CommonConstant::TIME_LIMIT_300_SECOND);
        }

        return $pagePer;
    }

    /**
     * @throws NotFoundException
     */
    public function get(int $id, array $options = [], array $columns = ['*'])
    {
        $record = $this->model->newQuery()->select($columns)->first();
        if (Arr::get($options, 'throw_exception', true) && empty($record)) {
            throw new NotFoundException('Record does not exist');
        }
        return $record;
    }
}
