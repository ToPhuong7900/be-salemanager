<?php

namespace App\Services\Auth;

use App\Libs\Common\CommonConstant;
use App\Models\User;
use App\Rules\Auth\LoginValidator;
use App\Services\BaseService;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthService extends BaseService
{

    public function setModel(): string
    {
        return User::class;
    }

    public function login(array $inputs = []): bool|LoginValidator
    {
        $validator = new LoginValidator($this->getModel(), $inputs);
        if ($validator->isSuccess()) {
            $user = User::where('email', Arr::get($inputs, 'email'))
                ->where('user_active', CommonConstant::DATABASE_VALUE_YES)
                ->first();
            if (!$user || !Hash::check(Arr::get($inputs, 'password'), $user->password)) {
                return false;
            }

            $validator->getValidator()->setValue(
                'token',
                $user->createToken(Arr::get($inputs, 'email'))->accessToken
            );
        }
        return $validator;
    }

    public function getInfo(User $user)
    {
//        return
    }
}
