<?php

namespace App\Services\Stores;

use App\Libs\Common\Common;
use App\Models\Category;
use App\Models\Store;
use App\Services\BaseService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class StoresService extends BaseService
{
    private Store $store;

    public function setModel(): string
    {
        return Store::class;
    }

    public function getStoreValidator()
    {
        $attributes = $this->store->getAttributes();

        $rules = [
            'store_code' => [
                'required',
                'max:15',
                'unique:store,store_code,' . $this->store->getKey() . ",store_id"
            ],
            'store_name' => ['max:15'],
            'store_phone' => ['max:512'],
            'store_country' => ['max:512'],
            'store_city' => ['max:512'],
            'store_province' => ['max:512'],
            'store_manager_id' => 'required',
            'company_id' => 'required'
        ];

        return Validator::make($attributes, $rules);
    }

    public function setModelFields(array $inputs = []): Store
    {
        $this->store = new Store();
        Common::assignField($this->store, 'store_code', $inputs);
        Common::assignField($this->store, 'store_name', $inputs);
        Common::assignField($this->store, 'store_phone', $inputs);
        Common::assignField($this->store, 'store_address', $inputs);
        Common::assignField($this->store, 'store_country', $inputs);
        Common::assignField($this->store, 'store_city', $inputs);
        Common::assignField($this->store, 'store_manager_id', $inputs);
        Common::assignField($this->store, 'store_province', $inputs);
        Common::assignField($this->store, 'company_id', $inputs);
        Common::assignField($this->store, 'date_format_active', $inputs);

        return $this->store;
    }

    public function storeStores($input): array
    {
        $this->setModelFields($input);
        $validator = $this->getStoreValidator();
        if ($validator->passes())
        {
            DB::beginTransaction();
            $this->store->save();

            DB::commit();
            return [
                'data' => $this->store,
                'success' => true
            ];
        } else {
            DB::rollBack();
            return [
                'success' => false,
                'error' => $validator->errors()
            ];
        }
    }

    public function updateStores($input, $id): array
    {
        $this->getRecord($id);
        $validator = $this->getStoreValidator();
        if ($validator->passes())
        {
            DB::beginTransaction();
            $this->store->update($input);
            $this->store->save();

            DB::commit();
            return [
                'data' => $this->store,
                'success' => true
            ];

        } else {
            DB::rollBack();
            return [
                'success' => false,
                'error' => $validator->errors()
            ];
        }
    }

    public function getRecord($id)
    {
        $this->store = Store::find($id);
        return $this->store;
    }

    /**
     * @throws \Exception
     */
    public function deleteStore($id): bool
    {
        $category = $this->getRecord($id);
        $result = Common::canDelete($category, 'store_id', $id);
        if ($result)
        {

            $category->delete();
        }
        return $result;
    }
}
