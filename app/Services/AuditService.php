<?php

namespace App\Services;

use App\Libs\Audit\IAuditable;
use App\Libs\Common\Common;
use App\Libs\Common\CommonConstant;
use App\Models\Audit;
use App\Models\AuditAction;
use App\Models\AuditTable;
use App\Models\ConfidentialMessageAudit;
use App\Models\CrudAccessLevel;
use App\Models\System;
use App\Models\User;
use App\Models\UserGroupModule;
use Illuminate\Support\Arr;

class AuditService extends BaseService
{
    /**
     * The audit text to write to db. This is autopopulated when calling auditFields.
     * @var string
     */
    public string $auditText = '';

    /**
     * Set to true if there have been any auditable changes after calling auditFields().
     * @var boolean
     */
    public bool $auitableDiff = false;

    /**
     * This is used to record the interface run (interface_log_id) the audit entry was record was added under.
     * @var integer
     */
    public ?int $processorId = null;

    public $latitude;

    public $longitude;

    public $restrictByModule = null;
    /**
     * Audit field indexes.
     */
    private string $displayName   = 'field';
    private string $originalValue = 'original';
    private string $currentValue  = 'new';
    private string $fromText      = 'originalText';
    private string $toText        = 'newText';
    private string $auditNew      = 'auditNew';
    private string $currencyFormat = 'currencyFormat';

    /**
     * The field seperator use within the generated audit text.
     * @var string
     */
    private $fieldSeperator = ', ';

    /**
     * The field/value opening seperator.
     * @var string
     */
    private $openingSeperator = ' changed from (';

    /**
     * The optional field/value middle seperator.
     * @var string
     */
    private $middleSeperator = ') to (';

    /**
     * The field/value closing seperator.
     * @var string
     */
    private $closingSeperator = ')';

    /**
     * The maximum single audit string length. Audit string lengths greater than this
     * will be split into chunks, and saved as multiple audit records.
     * @var integer
     */
    private $maxLength = 3000;

    /**
     * Generates the audit text for the supplied fields, setting the public $auditText property.
     * For lookup fields, the id's should be supplied for originalValue and currentValue, with
     * the orignal code being supplied for the optional fromText.
     *
     * @param  array  $fields Fields to audit.
     * @param  string $prefix Audit prefix
     */
    public function auditFields(array $fields, $prefix = '')
    {
        // Clear any previous audit text.
        $this->auditText = $prefix;

        $changes = 0;
        // Loop through each audit field to build audit text.
        foreach ($fields as $field) {
            // Each field is made up of:
            // - Display text
            // - Original field value
            // - Current field value

            // Check if the current value is not the same as the original value.
            if ($this->fieldChanged($field)) {
                // Values are different so add to audit string
                if ($this->auditText != '' && $this->auditText !== $prefix) {
                    $this->auditText .= $this->fieldSeperator;
                }

                if (isset($field[$this->currencyFormat])) {
                    $field[$this->originalValue] = Common::numberFormat($field[$this->originalValue], true);
                    $field[$this->currentValue] = Common::numberFormat($field[$this->currentValue], true);
                }

                // If alternative audit text has been supplied, use it to build audit string.
                // If no alternative text supplied, use original value.
                $auditText = $field[$this->fromText] ?? $field[$this->originalValue];
                // See if there is the to text to be used as well
                if (isset($field[$this->toText])) {
                    $toAuditText = $this->middleSeperator . $field[$this->toText];
                } elseif (isset($field[$this->auditNew])) {
                    $toAuditText = $this->middleSeperator . $field[$this->currentValue];
                } else {
                    $toAuditText = '';
                }

                $this->auditText .= $field[$this->displayName] . $this->openingSeperator
                        . $auditText . $toAuditText . $this->closingSeperator;

                $changes++;
            }
        }

        /** if changes were detected in the loop of auditable fields then set
         *  the property to true and allow access via auditableChanges() */
        if ($changes > 0) {
            $this->auitableDiff = true;
        } else {
            $this->auitableDiff = false;
        }
    }

    /**
     * Set audit message for special case like: add letable unit to lease out,....
     *
     * @param array $messages
     */
    public function auditMessages(array $messages)
    {
        foreach ($messages as $message) {
            if ($this->auditText !== '') {
                $this->auditText .= $this->fieldSeperator . $message;
            } else {
                $this->auditText .= $message;
            }
        }
        if ($this->auditText) {
            $this->auitableDiff = true;
        } else {
            $this->auitableDiff = false;
        }
    }
    /**
     * Adds audit entry to database.  Audit text is taken from $auditText which is either populated manually or
     * by calling auditFields().
     * Optionally the parent record can be supplied.  This is used when rolling up child audit records for
     * display against a parent.
     *
     * @param IAuditable $record Site, Building, Room etc.
     * @param int $action Audit action AuditAction::ACT_INSERT, AuditAction::ACT_UPDATE etc.
     * @param IAuditable $parentRecord Site, Building, Room etc.
     */
    public function addAuditEntry(
        IAuditable $record,
        int        $action,
        IAuditable $parentRecord = null,
                   $options = []
    ) {
        // Audit datetime.
        $datetime = new \DateTime();

        $recordTable = $record->getAuditTable();

        $recordId = $record->getKey();

        $recordCode  = $record->getAuditCode();
        $maxAuditCodeLen = 255;
        $len = strlen($recordCode);
        if ($len > $maxAuditCodeLen) {
            $recordCode = substr($recordCode, -1 * $maxAuditCodeLen);
        }

        // Assume we will write the audit record.
        $writeAudit = true;

        if ($action == AuditAction::ACT_VIEW) {
            $recordTableName = $record->getTable();
            $writeAudit = !\Session::has("audit.view-$recordTableName-$recordId");
            \Session::put("audit.view-$recordTableName-$recordId", true);
        }

        if ($writeAudit) {
            // If the audit string exceeds maxLength then it will be split up and added as multiple entries.
            foreach ($this->auditSplit() as $auditText) {
                if (\Auth::check()) {
                    $audit = new Audit();
                    $audit->setAttribute('user_id', \Auth::user()->id);
                    $source = \Auth::user()->getSourceController();
                } elseif ($userId = Arr::get($options, "user-id")) {
                    /**
                      *  A user id can be supplied.  This will be used if there is no current logged in user.
                      *  This occurs when a user resets their password after clicking on the forgotten password link.
                      */
                    $audit = new Audit($userId);
                    $audit->user_id = $userId;
                    $source = false;
                } elseif ($record->user_id) {
                    throw new \Exception("Current user must be logged in order to record Audit Actions (user_id).");
                    // The following is dangerous, the action is being recorded by a user other that he current user.
                    //$audit = new Audit($record->user_id);
                    //$audit->user_id = $record->user_id;
                    //$source = false;
                } else {
                    throw new \Exception("Current user must be logged in order to record Audit Actions (record_id).");
                    // The following is even more dangerous, if
                    // the record_id does match a user_id that would be pure luck and wrong.
                    //$audit = new Audit($recordId);
                    //$audit->user_id = $recordId;
                    //$source = false;
                }

                if (isset($options) && array_key_exists('auditText', $options)) {
                    $auditText = Arr::get($options, 'auditText');
                }

                $audit->timestamp       = $datetime->format('Y-m-d H:i:s');
                $audit->audit_action_id = $action;
                $audit->table_name      = $recordTable;
                $audit->record_id       = $recordId;
                $audit->record_code     = $recordCode;
                $audit->audit_text      = $auditText;
                $audit->audit_v2        = CommonConstant::VERSION_SALE;

                if ($source !== false) {
                    $audit->source_id = $source;
                }

                if ($parentRecord) {
                    $audit->setAttribute('parent_table_name',  $parentRecord->getAuditTable());
                    $audit->setAttribute('parent_record_id',  $parentRecord->getKey());
                }

                $audit->setAttribute('processor_id', $this->processorId);
                $audit->setAttribute('lat', Common::emptyToNull($this->latitude));
                $audit->setAttribute('lng', Common::emptyToNull($this->longitude));

                /**
                 * CLD-8773: This is temp fix until all sites are upgraded past 3.16
                 * This code may be removed once all sites are past this version. e.g.
                 * just leave the line:
                 * $audit->restrict_by_module = $this->restrictByModule;
                 */
                $system = System::first();
                if (
                    $system && ($system->major_release_version > 3 ||
                    ($system->major_release_version == 3 && $system->minor_release_version > 24))
                ) {
                    $audit->setAttribute('restrict_by_module', $this->restrictByModule);
                }

                $audit->save();
            }
        }
    }

    /**
     * Returns a collection of audit entries for a specific record.
     *
     * @param string $tableName e.g. site, building, instruction
     * @param int $recordId e.g. site_id, building_id, instruction_id
     * @param bool $bincludeChildren e.g. if set to true then immediate children audit records are shown.
     * @return
     */
    public function recordAuditList($tableName, $recordId, $inputs, $includeChildren = false)
    {
        $canViewConfidentialMessages = \Auth::user()->canViewConfidentialMessages();

        if ($canViewConfidentialMessages) {
            $confAuditTable = 'confidential_message_audit';

            $confAudit = ConfidentialMessageAudit::with(['user', 'auditAction', 'auditTable']);

            $confAudit->select(
                $confAuditTable . '.confidential_message_audit_id',
                $confAuditTable . '.user_id',
                $confAuditTable . '.timestamp',
                $confAuditTable . '.audit_v2',
                $confAuditTable . '.source_id',
                $confAuditTable . '.table_name',
                $confAuditTable . '.audit_action_id',
                $confAuditTable . '.record_id',
                $confAuditTable . '.record_code',
                $confAuditTable . '.parent_table_name',
                $confAuditTable . '.parent_record_id',
                'audit_action.audit_action_code',
                'user.display_name',
                'audit_table.table_display_name',
                $confAuditTable . '.audit_text'
            );
        }

        if ($includeChildren) {
            $auditTable = new Audit();
            $audit = Audit::with(['user', 'auditAction', 'auditTable']);
//                $auditTable->getTable() . '.audit_id',

            $audit->select(
                $auditTable->getTable() . '.audit_id',
                $auditTable->getTable() . '.user_id',
                $auditTable->getTable() . '.timestamp',
                $auditTable->getTable() . '.audit_v2',
                $auditTable->getTable() . '.source_id',
                $auditTable->getTable() . '.table_name',
                $auditTable->getTable() . '.audit_action_id',
                $auditTable->getTable() . '.record_id',
                $auditTable->getTable() . '.record_code',
                $auditTable->getTable() . '.parent_table_name',
                $auditTable->getTable() . '.parent_record_id',
                'audit_action.audit_action_code',
                'user.display_name',
                'audit_table.table_display_name',
                $auditTable->getTable() . '.audit_text'
            );

            if ($recordId) {
                $this->addTableNameQuery($audit, $tableName, $recordId);
                if ($canViewConfidentialMessages) {
                    $this->addTableNameQuery($confAudit, $tableName, $recordId);
                }
            }

            $this->addAuditJoins($audit, $auditTable->getTable());
            if ($canViewConfidentialMessages) {
                $this->addAuditJoins($confAudit, $confAuditTable);
            }
        } else {
            $audit = Audit::with(['user', 'auditAction', 'auditTable'])->where('table_name', $tableName);
            if ($recordId) {
                $audit->where('record_id', $recordId);
            }
        }

        if ($canViewConfidentialMessages) {
            // filter($query, $inputs, $showModule = false, $moduleFilter = false, $confidentialMessageAudit = false)
            $confAudit = $this->filter($confAudit, $inputs, false, false, true);
            $audit->union($confAudit);
        }

        $audit = $this->filter($audit, $inputs, false);

        return $audit;
    }

    private function addAuditJoins(&$audit, $tableName)
    {
        $audit->leftjoin(
            'audit_action',
            'audit_action.audit_action_id',
            '=',
            $tableName . ".audit_action_id"
        )
            ->leftjoin('audit_table', 'audit_table.table_dbname', '=', $tableName . ".table_name")
            ->join('user', 'user.id', '=', $tableName . '.user_id');
    }

    private function addTableNameQuery(&$auditTable, $tableName, $recordId)
    {
        $auditTable->where(
            function ($query) use ($tableName, $recordId) {
                $query->where(function ($query) use ($tableName, $recordId) {
                    $query->where('table_name', $tableName)
                    ->where('record_id', $recordId);
                });
                $query->orWhere(function ($query) use ($tableName, $recordId) {
                    $query->where('parent_table_name', $tableName)
                    ->where('parent_record_id', $recordId);
                });
            }
        );
    }

    public function all()
    {
        $audit = Audit::with(['user', 'auditAction', 'auditTable']);

        $auditTable = new Audit();
        $audit->leftjoin(
            'audit_action',
            'audit_action.audit_action_id',
            '=',
            $auditTable->getTable() . ".audit_action_id"
        )
           ->leftjoin('audit_table', 'audit_table.table_dbname', '=', $auditTable->getTable() . ".table_name")
           ->leftjoin('user_source', 'user_source.user_source_id', '=', $auditTable->getTable() . ".source_id")
           ->join('user', 'user.id', '=', $auditTable->getTable() . '.user_id');

        return $audit;
    }

    /**
     * Return a collection of audit on a table.
     *
     * @param string $tableName
     * @param array $inputs
     * @return
     */
    public function tableAuditList(string $tableName, array $inputs)
    {
        $auditTable = new Audit();
        $audit = Audit::with(['user', 'auditAction', 'auditTable'])
                ->leftjoin(
                    'audit_action',
                    'audit_action.audit_action_id',
                    '=',
                    $auditTable->getTable() . ".audit_action_id"
                )
                ->where('table_name', $tableName);
        return $this->filter($audit, $inputs);
    }
    /**
     * Returns a collection of audit entries for a specific user.
     *
     * @param int $userId
     * @param array $inputs
     * @return Audit
     */
    public function userAuditList(int $userId, array $inputs, $showModule = false): Audit
    {
        $audit = Audit::with(['user', 'auditAction', 'auditTable', 'modules']);

        if (!is_null($userId)) {
            $audit->where('user_id', $userId);
        }

        if ($showModule) {
            $auditTable = new Audit();
            $table = $auditTable->getTable();

            $audit = Audit::select([
                \DB::raw(
                    "IF(`{$table}`.`table_name` = 'document',"
                    . " `{$table}`.`parent_table_name`, `{$table}`.`table_name`)"
                    . " as new_table, `{$table}`.*"
                    . ", table_display_name, display_name, audit_action_code, module_description"
                )])
            ->leftjoin(
                'audit_action',
                'audit_action.audit_action_id',
                '=',
                "{$table}.audit_action_id"
            )
            ->leftjoin(
                'audit_table',
                'audit_table.table_dbname',
                '=',
                \DB::raw(
                    "IF(`{$table}`.`table_name` = 'document' || `{$table}`.`table_name` = 'filed_qst_section'"
                    . " || `{$table}`.`table_name` = 'filed_qst_action' "
                    . " || `{$table}`.`table_name` = 'filed_qst_question' "
                    . ", `{$table}`.`parent_table_name`, `{$table}`.`table_name`)"
                )
            )
            ->leftJoin('module', 'audit_table.module_id', '=', "module.module_id")
            ->join('user', 'user.id', '=', "{$table}.user_id");

            if (!is_null($userId)) {
                $audit->where("{$table}.user_id", $userId);
            }
        }


        return $this->filter($audit, $inputs, $showModule, $userId == 0);
    }

    /**
     * Return a collection of audit entries by source.
     *
     * @param string $sourceId
     * @param array $inputs
     * @return Audit
     */
    public function sourceAuditList(string $sourceId, array $inputs): Audit
    {
        $auditTable = new Audit();
        $audit = Audit::with(['user', 'auditAction', 'auditTable'])
        ->leftjoin(
            'audit_action',
            'audit_action.audit_action_id',
            '=',
            $auditTable->getTable() . ".audit_action_id"
        )
        ->join('user', 'user.id', '=', $auditTable->getTable() . '.user_id')
        ->where('source_id', $sourceId);
        return $this->filter($audit, $inputs);
    }

    public function allHistoryList($inputs)
    {
        $auditTable = new Audit();
        $table = $auditTable->getTable();

        $selList = [];
        $selList[] = "{$table}.audit_id AS Id";
        $selList[] = "{$table}.audit_v2 AS Version";

        if (\Auth::user()->isAdministratorOrSuperuser()) {
            $selList[] = "user_source_code AS Source";
            $selList[] = \DB::raw("CASE {$table}.table_name WHEN 'note' THEN 'Note' " .
                    "ELSE `table_display_name` END AS `Record`");
        }

        $selList[] = \DB::raw("IF({$table}.table_name = 'note'," .
            " vw_noteall_lookup.parent_code, {$table}.record_code) AS Code");

        $selList[] = "{$table}.timestamp AS Date";
        $selList[] = "display_name AS User";
        $selList[] = "audit_action_code AS Action";
        $selList[] = "{$table}.audit_text AS Description";

        $audit = Audit::select($selList)
        ->leftjoin(
            'audit_action',
            'audit_action.audit_action_id',
            '=',
            "{$table}.audit_action_id"
        )
        ->leftjoin(
            'user_source',
            'user_source.user_source_id',
            '=',
            "{$table}.source_id"
        )
        ->leftjoin(
            'audit_table',
            'audit_table.table_dbname',
            '=',
            \DB::raw(
                "IF(`{$table}`.`table_name` = 'document' OR `{$table}`.`table_name` = 'note',"
                . " `{$table}`.`parent_table_name`, `{$table}`.`table_name`)"
            )
        )
        ->leftJoin('module', 'audit_table.module_id', '=', "module.module_id")
        ->join('user', 'user.id', '=', "{$table}.user_id")
        ->leftJoin(
            'vw_noteall_lookup',
            "{$table}.record_id",
            '=',
            \DB::raw("vw_noteall_lookup.note_id AND {$table}.table_name = 'note'")
        );

        return $this->filter($audit, $inputs, true, true);
    }

    public function filter(
        $query,
        $inputs,
        $showModule = false,
        $moduleFilter = false,
        $confidentialMessageAudit = false
    ) {
        $filters    = new AuditFilter($inputs);
        $siteGroup  = SiteGroup::get();

        if ($confidentialMessageAudit) {
            $auditTable = 'confidential_message_audit';
        } else {
            $auditTable = 'audit' . $siteGroup->getKey();
        }

        if ($moduleFilter) {
            if (!is_null($filters->module) && $filters->module !== '') {
                $query->where('module.module_id', $filters->module);
            } else {
                if (!\Auth::user()->isAdministratorOrSuperuser()) {
                    $query->whereIn(
                        'audit_table.module_id',
                        \Auth::user()->getModuleSupervisorIds()->toArray()
                    );
                }
            }
        }

        if (! is_null($filters->user) && $filters->user !== '') {
            $query->where($auditTable . '.user_id', $filters->user);
        }

        if (!is_null($filters->code) && $filters->code !== '') {
            $query->where('record_code', 'like', '%' . $filters->code . '%');
        }

        if (!is_null($filters->description) && $filters->description !== '') {
            $query->where('audit_text', 'like', '%' . $filters->description . '%');
        }

        if (!is_null($filters->table) && $filters->table !== '') {
            $query->where('audit_table.audit_table_id', $filters->table);
        }

        // Narrow results for Reports History
        if (isset($inputs['actionEmailOrReportOnly']) && $inputs['actionEmailOrReportOnly']) {
            $query->whereIn('audit_action.audit_action_id', [AuditAction::ACT_EMAIL, AuditAction::ACT_REPORT]);
        }

        if (! is_null($filters->action) && $filters->action !== '') {
            if ($showModule) {
                $query->where('audit_action.audit_action_id', $filters->action);
            } else {
                $query->where($auditTable . '.audit_action_id', $filters->action);
            }
        }

        if (! is_null($filters->from) && $filters->from !== '') {
            $from = \DateTime::createFromFormat('d/m/Y', $filters->from);

            if ($from) {
                $query->where(
                    \DB::raw("DATE_FORMAT({$auditTable}.timestamp, '%Y-%m-%d')"),
                    '>=',
                    $from->format('Y-m-d')
                );
            }
        }

        if (! is_null($filters->to) && $filters->to !== '') {
            $to = \DateTime::createFromFormat('d/m/Y', $filters->to);

            if ($to) {
                $query->where(\DB::raw("DATE_FORMAT({$auditTable}.timestamp, '%Y-%m-%d')"), '<=', $to->format('Y-m-d'));
            }
        }

        // RB 22/08/2017 Not sure this is required since the query has been changed.
//        if ($addSortLinks) {
//
//            if (starts_with($filters->sort, 'user.')) {
//                $query->join('user', 'user.id', '=', $auditTable . '.user_id');
//            }
//
//            if (starts_with($filters->sort, 'audit_action.')) {
//                $query->join('audit_action', 'audit_action.audit_action_id', '=', $auditTable . '.audit_action_id');
//            }
//
//            if (starts_with($filters->sort, 'audit_table.')) {
//                $query->join('audit_table', 'audit_table.table_dbname', '=', $auditTable . '.table_name');
//            }
//        }

        if ($filters->sort === "timestamp") {
            //$query->orderby($auditTable . ".audit_id", $filters->sortOrder);
            $query->orderby("timestamp", $filters->sortOrder);
        } else {
            $query->orderby($filters->sort, $filters->sortOrder);
        }

        if ($filters->sourceId) {
            $query->where($auditTable . '.source_id', $filters->sourceId);
        }

        return $query;
    }

    /**
     * Returns the standard audit filter lookups.
     *
     * @return array
     */
    public function filterLookups()
    {
        // Had to stop using a the model as it will also have sites_string appended to it which for Gwyned,
        // when converted to a Json string caused the History page to bloat out to 100Mb.
        // On their live and uat site this meant that the History page took 60 seconds to load the page header
        // and then 20 seconds to load the history data.
        //$users = User::userSiteGroup()->where('superuser', '<>', 1)->get(['id', 'display_name', 'description']);
        $users = \DB::table('user')
            ->select(['id', 'display_name', 'description'])
            ->where('superuser', '<>', 1)
            ->where('site_group_id', '=', \Auth::User()->site_group_id)->get();

        // Above change broke it for lists with old filters and user dropdowns.
        // encoding and decoding made the result compatibly with old and new filters.
        $users = json_decode(json_encode($users), true);

        $actions = AuditAction::where('audit_action_id', '<>', AuditAction::ACT_REPORT)->get();
        $modules = $this->getSupervisorModuleList();
        $tables  = $this->getSupervisorTableList();

        return ['users' => $users, 'actions' => $actions, 'modules' => $modules, 'tables' => $tables];
    }

    /**
     * Returns specific audit filter lookups for Report history
     *
     * @return array
     */
    public function filterLookupsReportHistory(): array
    {
        $users = User::userSiteGroup()->where('superuser', '<>', 1)->get();
        $actions = AuditAction::whereIn('audit_action_id', [AuditAction::ACT_EMAIL, AuditAction::ACT_REPORT])->get();
        $modules = $this->getSupervisorModuleList();
        $tables  = $this->getSupervisorTableList();

        return ['users' => $users, 'actions' => $actions, 'modules' => $modules, 'tables' => $tables];
    }

    public function resetText()
    {
        $this->auditText = '';
    }

    private function getSupervisorTableList()
    {
        $tableList  = AuditTable::select()
            ->whereIn('audit_table.module_id', \Auth::user()->getModuleSupervisorIds()->toArray());

        //if (!\Auth::user()->isAdministratorOrSuperuser()) {
            $tableList->whereIn('audit_table.module_id', \Auth::user()->getModuleSupervisorIds()->toArray());
        //}

        return $tableList->orderBy('table_display_name')->get();
    }

    private function getSupervisorModuleList()
    {
        $possibleModules = [
            Module::MODULE_ASBESTOS,
            Module::MODULE_ESTATES,
            Module::MODULE_CAPITAL_ACCOUNTING,
            Module::MODULE_HELP,
            Module::MODULE_INCIDENT_MANAGEMENT,
            Module::MODULE_PROPERTY,
            Module::MODULE_INSPECTION,
            Module::MODULE_INSTRUCTIONS,
            Module::MODULE_INVOICING,
            Module::MODULE_PLANT,
            Module::MODULE_CONDITION,
            Module::MODULE_PROJECT,
            Module::MODULE_CAPACITY,
            Module::MODULE_SUFFICIENCY,
            Module::MODULE_SUITABILITY,
            Module::MODULE_GENERAL,
            Module::MODULE_GROUNDS_MAINTENANCE,
            Module::MODULE_CONTRACT,
            Module::MODULE_DLO,
            Module::MODULE_QST_GENERAL,
            Module::MODULE_QST_CORE_FACTS,
            Module::MODULE_QST_COVID_19,
            Module::MODULE_QST_DAA,
            Module::MODULE_QST_FIRE,
            Module::MODULE_QST_LEGIONELLA,
            Module::MODULE_UTILITY,
            Module::MODULE_RESOURCE_BOOKING,
            Module::MODULE_COST_PLUS,
            Module::MODULE_OCCUPANCY,
            Module::MODULE_WATER_MANAGEMENT,
            Module::MODULE_CASE_MANAGEMENT,
            Module::MODULE_CASE_MANAGEMENT_2,
            Module::MODULE_CASE_MANAGEMENT_3,
            Module::MODULE_CASE_MANAGEMENT_4,
            Module::MODULE_CASE_MANAGEMENT_5,
            Module::MODULE_RISK_ASSESSMENT,
            Module::MODULE_PERMIT_TO_WORK,
        ];

        // Just for module Fixed Asset
        if (\Auth::user()->isSuperuser()) {
            $possibleModules[] = Module::MODULE_CAPITAL_ACCOUNTING;
        }

        //Get access module list to limit the list in drop down box in filter
        if (!\Auth::user()->isAdministratorOrSuperuser()) {
            $moduleAllowList = UsergroupModule::where('usergroup_id', \Auth::user()->usergroup_id)
                ->where('crud_access_level_id', '=', CrudAccessLevel::CRUD_ACCESS_LEVEL_SUPERVISOR)
                ->pluck('module_id')->toArray();
        } elseif (\Auth::user()->isAdministrator()) {
            $moduleAllowList = UsergroupModule::where('usergroup_id', \Auth::user()->usergroup_id)
                ->pluck('module_id')->toArray();
        } elseif (\Auth::user()->isSuperuser()) {
            $moduleAllowList = UsergroupModule::pluck('module_id')->toArray();
        }

        $arrModuleAllow = array_intersect($possibleModules, $moduleAllowList);

        if ($arrModuleAllow) {
            $modules = \Auth::user()
                ->moduleAccess()
                ->leftJoin('module_site_group', 'module_site_group.module_id', '=', 'module.module_id')
                ->whereIn('module.module_id', $arrModuleAllow)
                ->orderBy('module_description')
                ->groupBy('module.module_id')
                ->get([
                    'module.module_id',
                    'module.module_code',
                    \DB::raw(
                        "(
                            CASE
                                WHEN module_site_group.module_description_config IS NULL
                                THEN module.module_description
                                ELSE module_site_group.module_description_config
                            END
                        ) AS module_description"
                    ),
                ]);
        } else {
            $modules = [];
        }

        return $modules;
    }


    /**
     * Returns true if the option is present and set to true.
     *
     * @param $field
     * @param string $optionName
     * @return bool
     */
    private function boolOptionEnabled(&$field, string $optionName)
    {
        $enabled = false;
        if (isset($field['options'])) {
            $options = $field['options'];
            $enabled = isset($options[$optionName]) && $options[$optionName];
        }

        return $enabled;
    }

    /**
     * Returns true if the original and current fields values are considered different.
     *
     * @param string $field
     * @return boolean
     */
    private function fieldChanged(string $field): bool
    {
        $changed = false;

        // Updated to ensure InvoiceNo 500 is recognised as being different to 500.
        if (
            strlen($field[$this->originalValue]) <> strlen($field[$this->currentValue])
            || $field[$this->originalValue] <> $field[$this->currentValue]
        ) {
            // Consider any set options:
            //
            // For lookup fields then consider null and 0 as the same value.
            if (
                $this->boolOptionEnabled($field, 'nullZeroEqual') &&
                $this->nullZeroEqual($field[$this->originalValue], $field[$this->currentValue])
            ) {
                $changed = false;
            } else {
                $changed = true;
            }
        }
        return $changed;
    }

    /**
     * Returns true if the only difference between the two fields is 0 or null.
     * @param type $field1
     * @param string $field2
     * @return bool
     */
    private function nullZeroEqual($field1, $field2)
    {
        return (is_null($field1) && $field2 == 0) || ($field1 == 0 && is_null($field2));
    }

    /**
     * Returns the audit text as an array.  If the audit text is greater than maxLength,
     * the string is split into chunks of maxLength.
     *
     * @return array containg the audit string chuncks with a maximum size of maxLength.
     */
    private function auditSplit(): array
    {
        $splitAudit = array();

        if (strlen($this->auditText) > $this->maxLength) {
            $splitAudit = str_split($this->auditText, $this->maxLength);
        } else {
            // auditText less than maxLength size, so no need to split.
            // Return an array with a single entry.
            $splitAudit[0] = $this->auditText;
        }

        return $splitAudit;
    }

    public function setModel(): string
    {
        return '';
    }
}
