<?php

namespace App\Services;

interface IBaseService
{
    public function get(int $id, array $options = [], array $columns = ['*']);
}
