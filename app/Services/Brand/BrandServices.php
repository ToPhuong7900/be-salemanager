<?php

namespace App\Services\Brand;

use App\Libs\AuditFields;
use App\Libs\Common\Common;
use App\Models\AuditAction;
use App\Models\Brand;
use App\Models\Category;
use App\Services\AuditService;
use App\Services\BaseService;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Support\Facades\Validator;

class BrandServices extends BaseService
{
    private Brand $brand;

    public function setModelFields(array $inputs = []): Brand
    {
        $this->brand = new Brand();
        Common::assignField($this->brand, 'brand_code', $inputs);
        Common::assignField($this->brand, 'brand_name', $inputs);
        Common::assignField($this->brand, 'brand_desc', $inputs);
        Common::assignField($this->brand, 'brand_active', $inputs);
        Common::assignField($this->brand, 'company_id', $inputs);

        return $this->brand;
    }

    /**
     * @throws Exception
     */
    public function storeBrand($input)
    {
        $this->setModelFields($input);
        $validator = $this->getBrandValidator();
        if ($validator->passes()) {
            DB::beginTransaction();
            $this->brand->save();

            DB::commit();
            return [
                'data' => $this->brand,
                'success' => true
            ];
        } else {
            DB::rollBack();
            return [
                'success' => false,
                'error' => $validator->errors()
            ];
        }
    }

    public function getBrandValidator()
    {
        $attributes = $this->brand->getAttributes();
        $rules = [
            'brand_code' => [
                'required',
                'max:15',
                'unique:brand,brand_code,' . $this->brand->getKey() . ",brand_id"
            ],
            'brand_name' => ['max:15'],
            'brand_desc' => ['max:512'],
            'company_id' => 'required'
        ];

        return Validator::make($attributes, $rules);
    }

    private function setupAuditFields()
    {
        $brand = Brand::find($this->brand->getOriginal('brand_id'));
        $brandAuditText = empty($brand) ? '' : $brand->brand_code;

        $auditFields = new AuditFields($this->brand);

        return $auditFields->addField('Description', 'brand_desc')
            ->addField('Name', 'brand_name')
            ->addField('Code', 'brand_code', ['originalText' => $brandAuditText])
            ->getFields();
    }

    /**
     * @throws Exception
     */
    public function updateBrand($input, $id)
    {
        $this->getRecord($id);
        $validator = $this->getBrandValidator();
        if ($validator->passes())
        {
            DB::beginTransaction();
            $this->brand->update($input);
            $this->brand->save();

            DB::commit();
            return [
                'data' => $this->brand,
                'success' => true
            ];
        } else {
            DB::rollBack();
            return [
                'success' => false,
                'error' => $validator->errors()
            ];
        }
    }

    public function getRecord($id)
    {
        $this->brand = Brand::find($id);
        return $this->brand;
    }

    /**
     * @throws Exception
     */
    public function deleteBrand($brand): bool
    {
        $result = Common::canDelete($brand, 'brand_id', $brand->getKey());
        if ($result)
        {
//            $audit = new AuditService();
//            $audit->addAuditEntry($brand, AuditAction::ACT_DELETE);
            $brand->delete();
        }
        return $result;
    }

    public function setModel(): string
    {
        return Brand::class;
    }
}
