<?php

namespace App\Services\Unit;

use App\Libs\AuditFields;
use App\Libs\Common\Common;
use App\Models\AuditAction;
use App\Models\BaseUnit;
use App\Models\Brand;
use App\Models\Category;
use App\Services\AuditService;
use App\Services\BaseService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class BaseUnitService extends BaseService
{
    private BaseUnit $baseUnit;
    public function setModel(): string
    {
        return BaseUnit::class;
    }

    public function setModelFields(array $inputs = []): BaseUnit
    {
        $this->baseUnit = new BaseUnit();
        Common::assignField($this->baseUnit, 'base_unit_code', $inputs);
        Common::assignField($this->baseUnit, 'base_unit_name', $inputs);
        Common::assignField($this->baseUnit, 'base_unit_desc', $inputs);
        Common::assignField($this->baseUnit, 'base_unit_active', $inputs);

        return $this->baseUnit;
    }

    public function getBaseUnitValidator($input)
    {
        $rules = [
            'base_unit_code' => [
                'required',
                'max:15'
            ],
            'base_unit_name' => ['max:15'],
            'base_unit_desc' => ['max:512'],
        ];

        return Validator::make($input, $rules);
    }

    public function storeBaseUnit($input)
    {
        $this->setModelFields($input);
        $validator = $this->getBrandValidator();
        if ($validator->passes()) {
            DB::beginTransaction();
            $this->baseUnit->save();

//            $audit = new AuditService();
//            $audit->addAuditEntry($this->baseUnit, AuditAction::ACT_INSERT);
            DB::commit();
            return [
                'data' => $this->baseUnit,
                'success' => true
            ];
        } else {
            DB::rollBack();
            return [
                'success' => false,
                'error' => $validator->errors()
            ];
        }
    }

    private function setupAuditFields()
    {
        $baseUnit = BaseUnit::find($this->baseUnit->getOriginal('base_unit_id'));
        $baseUnitAuditText = empty($baseUnit) ? '' : $baseUnit->base_unit_code;

        $auditFields = new AuditFields($this->baseUnit);

        return $auditFields->addField('Description', 'base_unit_desc')
            ->addField('Code', 'base_unit_code', ['originalText' => $baseUnitAuditText])
            ->getFields();
    }

    /**
     * @throws \Exception
     */
    public function updateBaseUnit($input, $id)
    {
        $this->getRecord($id);
        $validator = $this->getBaseUnitValidator();
        if ($validator->passes()) {
            DB::beginTransaction();
            $this->baseUnit->update($input);
            $this->baseUnit->save();

//            $auditFields = $this->setupAuditFields();
//            $audit = new AuditService();
//            $audit->auditFields($auditFields);
//            $audit->addAuditEntry($this->baseUnit, AuditAction::ACT_UPDATE);
            DB::commit();
            return [
                'data' => $this->baseUnit,
                'success' => true
            ];
        } else {
            DB::rollBack();
            return [
                'success' => false,
                'error' => $validator->errors()
            ];
        }
    }

    public function deleteBrand($baseUnit): bool
    {
        $result = Common::canDelete($baseUnit, 'base_unit_id', $baseUnit->getKey());
        if ($result)
        {
//            $audit = new AuditService();
//            $audit->addAuditEntry($baseUnit, AuditAction::ACT_DELETE);
            $baseUnit->delete();
        }
        return $result;
    }

    public function getRecord($id)
    {
        $this->baseUnit = BaseUnit::find($id);
        return $this->baseUnit;
    }
}

