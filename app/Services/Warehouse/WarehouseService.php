<?php

namespace App\Services\Warehouse;

use App\Libs\AuditFields;
use App\Libs\Common\Common;
use App\Models\Category;
use App\Models\Warehouse;
use App\Services\BaseService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class WarehouseService extends BaseService
{

    private Warehouse $warehouse;
    public function setModel(): string
    {
        return Warehouse::class;
    }

    public function getWarehouseValidator()
    {
        $attributes = $this->warehouse->getAttributes();

        $rules = [
            'warehouse_code' => [
                'required',
                'max:15',
                'unique:warehouse,warehouse_code,' . $this->warehouse->getKey() . ",warehouse_id"
            ],
            'warehouse_name' => ['max:15'],
            'warehouse_phone' => ['required', 'numeric'],
            'warehouse_address' => 'required',
            'warehouse_country' => 'required',
            'warehouse_city' => 'required',
            'warehouse_province' => 'required',
            'warehouse_manager_id' => 'required',
            'company_id' => 'required'
        ];

        return Validator::make($attributes, $rules);
    }

    public function setModelFields(array $inputs = []): Warehouse
    {
        $this->warehouse = new Warehouse();
        Common::assignField($this->warehouse, 'warehouse_code', $inputs);
        Common::assignField($this->warehouse, 'warehouse_name', $inputs);
        Common::assignField($this->warehouse, 'warehouse_phone', $inputs);
        Common::assignField($this->warehouse, 'warehouse_address', $inputs);
        Common::assignField($this->warehouse, 'warehouse_country', $inputs);
        Common::assignField($this->warehouse, 'warehouse_city', $inputs);
        Common::assignField($this->warehouse, 'warehouse_slug', $inputs);
        Common::assignField($this->warehouse, 'warehouse_province', $inputs);
        Common::assignField($this->warehouse, 'warehouse_manager_id', $inputs);
        Common::assignField($this->warehouse, 'company_id', $inputs);

        return $this->warehouse;
    }

    public function storeWarehouse($input): array
    {
        $this->setModelFields($input);
        $validator = $this->getWarehouseValidator();
        if ($validator->passes())
        {
            DB::beginTransaction();
            $this->warehouse->save();
            //            $audit = new AuditService();
//            $audit->addAuditEntry($this->category, AuditAction::ACT_INSERT);
            DB::commit();
            return [
                'data' => $this->warehouse,
                'success' => true
            ];
        } else {
            DB::rollBack();
            return [
                'success' => false,
                'error' => $validator->errors()
            ];
        }
    }

    private function setupAuditFields(): array
    {
        $warehouse = Warehouse::find($this->warehouse->getOriginal('warehouse_id'));
        $warehouseAuditText = empty($warehouse) ? '' : $warehouse->warehouse_code;

        $auditFields = new AuditFields($this->warehouse);

        return $auditFields->addField('Phone', 'warehouse_phone')
            ->addField('Name', 'warehouse_name')
            ->addField('Address', 'warehouse_address')
            ->addField('Country', 'warehouse_country')
            ->addField('City', 'warehouse_city')
            ->addField('Province', 'warehouse_province')
            ->addField('Code', 'warehouse_code', ['originalText' => $warehouseAuditText])
            ->getFields();
    }

    public function updateWarehouse($input, $id): array
    {
        $this->getRecord($id);
        $validator = $this->getWarehouseValidator();
        if ($validator->passes())
        {
            DB::beginTransaction();
            $this->warehouse->update($input);
            $this->warehouse->save();

//            $auditFields = $this->setupAuditFields();
//            $audit = new AuditService();
//            $audit->auditFields($auditFields);
//            $audit->addAuditEntry($this->$this->warehouse, AuditAction::ACT_UPDATE);
            DB::commit();
            return [
                'data' => $this->warehouse,
                'success' => true
            ];

        } else {
            DB::rollBack();
            return [
                'success' => false,
                'error' => $validator->errors()
            ];
        }
    }

    public function getRecord($id)
    {
        $this->warehouse = Warehouse::find($id);
        return $this->warehouse;
    }

    /**
     * @throws \Exception
     */
    public function deleteWarehouse($id): bool
    {
        $warehouse = $this->getRecord($id);
        $result = Common::canDelete($warehouse, 'warehouse_id', $id);
        if ($result)
        {
//            $audit = new AuditService();
//            $audit->addAuditEntry($warehouse, AuditAction::ACT_DELETE);
            $warehouse->delete();
        }
        return $result;
    }
}
