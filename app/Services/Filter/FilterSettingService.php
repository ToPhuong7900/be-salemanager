<?php

namespace Tfcloud\Services\Api\CloudApi\Filter;

use App\Models\FilterUserSetting;
use App\Services\AuditService;

class FilterSettingService extends FilterService
{
    /**
     * @var FilterUserSetting
     */
    protected $filterUserSetting;
    private $filterValueOriginal;

    public function getAll($userId, $filterNameId)
    {
        $query = FilterUserSetting::userSiteGroup()
            ->where('filter_name_id', $filterNameId)
            ->where('user_id', $userId)
            ->orderBy('display_order', 'ASC');
        return $query->get();
    }

    public function addFilterUserSetting($filterNameId, $inputs)
    {
        $loginUser = \Auth::user();
        $this->newFilterUserSetting($loginUser->site_group_id, $loginUser->getKey(), $filterNameId);
        $inputs = $this->formatInputs($inputs);
        $this->setModelFields($inputs);
        $validator = $this->getFilterUserSettingValidator($inputs);
        if ($validator->passes()) {
            \DB::transaction(function () {
                $this->filterUserSetting->save();
                $audit = new AuditService();
                $audit->addAuditEntry($this->filterUserSetting, AuditAction::ACT_INSERT);
                $this->unCheckOtherDefault($this->filterUserSetting);
            });
            $validator->success = true;
        }
        return $validator;
    }

    public function updateFilterUserSetting($filterUserSettingId, $inputs)
    {
        $this->filterUserSetting = $this->getFilterUserSetting($filterUserSettingId);
        $inputs = $this->formatInputs($inputs);
        $this->setModelFields($inputs);
        $validator = $this->getFilterUserSettingValidator($inputs);

        if ($validator->passes()) {
            $validator->success = true;
            \DB::transaction(function () {
                $auditFields = $this->setupAuditFields();
                $oldDisplayOrder = $this->filterUserSetting->getOriginal('display_order');
                $newDisplayOrder = $this->filterUserSetting->display_order;
                $this->filterUserSetting->save();
                $this->reOrder($this->filterUserSetting, $oldDisplayOrder, $newDisplayOrder);
                $this->unCheckOtherDefault($this->filterUserSetting);
                $audit = new AuditService();
                $audit->auditFields($auditFields);
                $audit->addAuditEntry($this->filterUserSetting, AuditAction::ACT_UPDATE);
            });
        }
        return $validator;
    }

    public function deleteFilterUserSetting($filterUserSettingId)
    {
        $this->filterUserSetting = $this->getFilterUserSetting($filterUserSettingId);
        \DB::transaction(function () {
            $reOrderQuery = FilterUserSetting::ofUser($this->filterUserSetting->user_id)
                ->ofFilterName($this->filterUserSetting->filter_name_id)
                ->where('display_order', '>', $this->filterUserSetting->display_order);
            $this->filterUserSetting->delete();
            $reOrderQuery->decrement('display_order');
            $audit = new AuditService();
            $audit->addAuditEntry($this->filterUserSetting, AuditAction::ACT_DELETE);
        });
    }

    private function getFilterUserSetting($filterUserSettingId)
    {
        $query = FilterUserSetting::userSiteGroup()
            ->where('filter_user_setting_id', $filterUserSettingId)
            ->where('user_id', \Auth::user()->getKey());
        $filterUserSetting = $query->first();
        if (!$filterUserSetting) {
            throw new InvalidRecordException("No filter setting found.");
        }
        return $filterUserSetting;
    }

    private function newFilterUserSetting($siteGroupId, $userId, $filterNameId)
    {
        $this->filterUserSetting = new FilterUserSetting();
        $this->filterUserSetting->site_group_id = $siteGroupId;
        $this->filterUserSetting->filter_name_id = $filterNameId;
        $this->filterUserSetting->user_id = $userId;
        $this->filterUserSetting->default = CommonConstant::DATABASE_VALUE_NO;
        $this->filterUserSetting->display_order = $this->getNextDisplayOrder($userId, $filterNameId);
    }

    private function formatInputs($inputs)
    {
        $default = array_get($inputs, 'default');
        parse_str(array_get($inputs, 'filter_value'), $filterValue);
        if (!empty($filterValue)) {
            $this->filterValueOriginal = $inputs['filter_value'];
            $inputs['filter_value'] = json_encode($filterValue);
        }

        if (isset($default)) {
            $inputs['default'] = Common::valueBooleanNumericToYesNo($default);
        }
        return $inputs;
    }

    private function setModelFields($inputs)
    {
        Common::assignField($this->filterUserSetting, 'filter_user_setting_desc', $inputs);
        Common::assignField($this->filterUserSetting, 'default', $inputs);
        if ($this->filterUserSetting->getKey()) {
            Common::assignField($this->filterUserSetting, 'display_order', $inputs);
        } else {
            Common::assignField($this->filterUserSetting, 'filter_value', $inputs);
        }
    }

    private function getFilterUserSettingValidator($inputs)
    {
        $rules = [
            'filter_user_setting_desc' => ['required', 'max:128'],
            'site_group_id' => ['required'],
            'filter_name_id' => ['required'],
            'user_id' => ['required'],
            'filter_value' => ['required', 'json'],
            'filter_value_original' => ['required', 'max:2000'],
            'default' => ['nullable', 'in:Y,N'],
            'display_order' => ['required', 'integer', 'max:20'],
        ];
        $attributes = array_merge(
            $this->filterUserSetting->getAttributes(),
            ['filter_value_original' => $this->filterValueOriginal]
        );
        if (isset($inputs['isFromReport']) && $inputs['isFromReport'] === true) {
            $rules['filter_value_original'] = ['nullable'];
        }

        $validator = \Validator::make($attributes, $rules);
        $validator->success = false;

        return $validator;
    }

    private function unCheckOtherDefault(FilterUserSetting $filterUserSetting)
    {
        if ($filterUserSetting->isDefault()) {
            FilterUserSetting::userSiteGroup()
                ->where('filter_name_id', $filterUserSetting->filter_name_id)
                ->where('user_id', $filterUserSetting->user_id)
                ->where('filter_user_setting_id', '<>', $filterUserSetting->filter_user_setting_id)
                ->where('default', CommonConstant::DATABASE_VALUE_YES)
                ->update(['default' => CommonConstant::DATABASE_VALUE_NO]);
        }
    }

    private function reOrder(FilterUserSetting $selectedItem, $oldOrder, $newOrder)
    {
        if ($oldOrder != $newOrder) {
            $down = $oldOrder < $newOrder;
            $filterNameId = $selectedItem->filter_name_id;
            $userId = $selectedItem->user_id;
            $selectedId = $selectedItem->getKey();
            $query = FilterUserSetting::ofUser($userId)->ofFilterName($filterNameId)
                ->where('filter_user_setting_id', '<>', $selectedId);
            if ($down) {
                $query->where('display_order', '<=', $newOrder)
                    ->where('display_order', '>', $oldOrder)
                    ->decrement('display_order');
            } else {
                $query->where('display_order', '>=', $newOrder)
                    ->where('display_order', '<', $oldOrder)
                    ->increment('display_order');
            }
        }
    }

    private function getNextDisplayOrder($userId, $filterNameId)
    {
        $count = FilterUserSetting::userSiteGroup()->ofUser($userId)->ofFilterName($filterNameId)->count();
        return $count + 1;
    }

    private function setupAuditFields()
    {
        $auditFields = new AuditFields($this->filterUserSetting);
        return $auditFields
            ->addField('Description', 'filter_user_setting_desc')
            ->addField('Filter Value', 'filter_value')
            ->addField('Default', 'default')
            ->addField('Display Order', 'display_order')
            ->getFields();
    }
}
