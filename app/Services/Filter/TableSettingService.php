<?php

namespace Tfcloud\Services\Api\CloudApi\Filter;

use Tfcloud\Lib\Common;
use Tfcloud\Lib\Constant\CommonConstant;
use Tfcloud\Models\Filter\FilterTableHeaderUser;

class TableSettingService extends FilterService
{
    /**
     * @var FilterTableHeaderUser
     */
    private $filterTableHeaderUser;

    public function processFilterTableHeaderUser($filterNameId, $inputs)
    {
        $loginUser = \Auth::user();
        $siteGroupId = $loginUser->site_group_id;
        $userId = $loginUser->getKey();

        $this->filterTableHeaderUser = $this->getFilterTableHeaderUser($filterNameId, $userId);
        if ($this->filterTableHeaderUser) {
            return $this->handleFilterTableHeaderUser($inputs);
        } else {
            $this->newFilterTableHeaderUser($filterNameId, $siteGroupId, $userId);
            return $this->handleFilterTableHeaderUser($inputs);
        }
    }

    private function handleFilterTableHeaderUser($inputs)
    {
        $this->setModelFields($inputs);
        $validator = $this->getFilterTableHeaderUserValidator();
        if ($validator->passes()) {
            $this->formatColumnVisible();
            \DB::transaction(function () {
                $this->filterTableHeaderUser->save();
            });
            $validator->success = true;
        }
        return $validator;
    }

    private function getFilterTableHeaderUser($filterNameId, $userId)
    {
        $query = FilterTableHeaderUser::userSiteGroup()
            ->where('filter_name_id', $filterNameId)
            ->where('user_id', $userId);
        return $query->first();
    }

    private function newFilterTableHeaderUser($filterNameId, $siteGroupId, $userId)
    {
        $this->filterTableHeaderUser = new FilterTableHeaderUser();
        $this->filterTableHeaderUser->filter_name_id = $filterNameId;
        $this->filterTableHeaderUser->site_group_id = $siteGroupId;
        $this->filterTableHeaderUser->user_id = $userId;
        $this->filterTableHeaderUser->wrap_rows = CommonConstant::DATABASE_VALUE_YES;
        return $this->filterTableHeaderUser;
    }

    private function formatColumnVisible()
    {
        $columns = collect($this->filterTableHeaderUser->column_visible);
        $sortedColumns = $columns->sortBy('displayOrder');
        $columnArr = $sortedColumns->toArray();
        $displayOrder = 1;
        foreach ($columnArr as $title => $column) {
            $columnArr[$title][FilterTableHeaderUser::COLUMN_VISIBLE_DISPLAY_ORDER_KEY] = $displayOrder;
            $displayOrder++;
        }
        $this->filterTableHeaderUser->column_visible = json_encode($columnArr);
    }

    private function setModelFields($inputs)
    {
        Common::assignField($this->filterTableHeaderUser, 'column_visible', $inputs, [], ['emptyToNull' => false]);
        Common::assignField($this->filterTableHeaderUser, 'wrap_rows', $inputs);
    }

    private function getFilterTableHeaderUserValidator()
    {
        $rules = [
            'site_group_id' => ['required', 'integer'],
            'filter_name_id' => ['required', 'unique:filter_table_header_user,filter_name_id,'
                . $this->filterTableHeaderUser->filter_table_header_user_id .
                ',filter_table_header_user_id,user_id,' . $this->filterTableHeaderUser->user_id],
            'user_id' => ['required', 'integer'],
            'column_visible' => ['required', 'array'],
            'column_visible.*.visible' => ['required_with:column_visible', 'boolean'],
            'column_visible.*.displayOrder' => ['required_with:column_visible', 'integer'],
            'wrap_rows' => ['required', 'in:Y,N'],
        ];

        $messages = [
            'filter_name_id.unique' => 'Only one setting for each list.',
        ];

        $validator = \Validator::make($this->filterTableHeaderUser->getAttributes(), $rules, $messages);
        $validator->success = false;
        return $validator;
    }

    public function deleteFilterTableHeader($filterNameId)
    {
        $loginUser = \Auth::user();
        $userId = $loginUser->getKey();
        $headerSettings = $this->getFilterTableHeaderUser($filterNameId, $userId);

        if ($headerSettings) {
            $headerSettings->delete();
        }

        return true;
    }
}
