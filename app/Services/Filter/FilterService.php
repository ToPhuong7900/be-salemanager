<?php

namespace App\Services\Filter;

use App\Models\FilterName;
use App\Services\BaseService;

class FilterService extends BaseService
{
    public function setModel(): string
    {
        return FilterName::class;
    }
}
