<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;


/**
 * App\Models\ManageStock
 *
 * @property int $id
 * @property int $warehouse_id
 * @property int $product_id
 * @property float $quantity
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Product $product
 * @property-read \App\Models\Warehouse $warehouse
 *
 * @method static \Illuminate\Database\Eloquent\Builder|ManageStock newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ManageStock newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ManageStock query()
 * @method static \Illuminate\Database\Eloquent\Builder|ManageStock whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ManageStock whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ManageStock whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ManageStock whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ManageStock whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ManageStock whereWarehouseId($value)
 *
 * @property float $alert
 *
 * @method static \Illuminate\Database\Eloquent\Builder|ManageStock whereAlert($value)
 *
 * @mixin \Eloquent
 */
class ManageStock extends BaseModel
{
    protected $table = 'manage_stock';

    protected $primaryKey = 'manage_stock_id';

    protected $fillable = [
        'warehouse_id',
        'product_id',
        'quantity',
        'alert',
    ];

    protected $casts = [
        'warehouse_id' => 'integer',
        'product_id' => 'integer',
        'quantity' => 'float',
        'alert' => 'float',
    ];

    public static array $rules = [
        'warehouse_id' => 'required|exists:warehouses,id',
        'product_id' => 'required|exists:products,id',
        'quantity' => 'required|numeric',
    ];

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class, 'product_id', 'product_id');
    }

    public function warehouse(): BelongsTo
    {
        return $this->belongsTo(Warehouse::class, 'warehouse_id', 'warehouse_id');
    }
}
