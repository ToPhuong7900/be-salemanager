<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserAccess extends BaseModel
{
    use HasFactory;
    protected $table = 'user_access';
    protected $primaryKey = 'user_access_id';


    // User store Access Levels
    public const USER_ACCESS_ALL = 1;
    public const USER_ACCESS_SELECTED = 2;
    public const USER_ACCESS_NONE = 3;
}
