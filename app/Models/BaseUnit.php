<?php

namespace App\Models;

use App\Libs\AuditTableInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Libs\Audit\IAuditable;

/**
 * App\Models\Brand
 *
 * @property int $base_unit_id
 * @property string $base_unit_code
 * @property string|null $base_unit_description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 *
 */
class BaseUnit extends BaseModel implements AuditTableInterface, IAuditable
{
    use HasFactory;

    protected $table = 'base_unit';

    protected $primaryKey = "base_unit_id";

    protected $fillable = ['base_unit_code'];

    public function getAuditCode(): string
    {
        return $this->base_unit_code;
    }

    public function getAuditFields(): array
    {
        // TODO: Implement getAuditFields() method.
    }
}
