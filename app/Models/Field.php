<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Field extends BaseModel
{
    protected $table = "field";
    protected $primaryKey = "field_id";

    public function variantType(): BelongsTo
    {
        return $this->belongsTo(VariantType::class, 'field_id', 'field_id');
    }
}
