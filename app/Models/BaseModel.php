<?php

namespace App\Models;

use App\Libs\Common\CommonConstant;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class BaseModel extends Model
{
    public $timestamps = true;
    protected bool $activeField = true;
    protected bool $siteGroupIdField = true;
    protected $codeField = null;
    protected $descField = null;
    public function getAuditTable()
    {
        return $this->getTable();
    }
    public function scopeActive($query)
    {
        if ($this->activeField) {
            $query->where('active', CommonConstant::DATABASE_VALUE_YES);
        }
        return $query;
    }

    public function scopeUserSiteGroup($query)
    {
        if ($this->siteGroupIdField) {
            return $query->where($this->table . '.site_group_id', \Auth::User()->site_group_id);
        }

        return $query;
    }
}
