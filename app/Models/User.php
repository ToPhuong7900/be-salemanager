<?php

namespace App\Models;

use App\Libs\Common\CommonConstant;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Laravel\Sanctum\HasApiTokens;

class User extends BaseModel implements AuthenticatableContract, AuthorizableContract
{
    use HasApiTokens;
    use HasFactory;
    use Notifiable;
    use Authorizable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'site_group_id',
        'user_group_id'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];
    private $elevatePermission;

    /**
     * Get the attributes that should be cast.
     *
     * @return array<string, string>
     */
    protected function casts(): array
    {
        return [
            'email_verified_at' => 'datetime',
            'password' => 'hashed',
        ];
    }
    public function elevatedPermission($value): void
    {
        $this->elevatePermission = $value;
    }

    public function hasElevatedPermission(): bool
    {
        return $this->elevatePermission === true;
    }
    public function isAdministrator(): bool
    {
        return $this->getAttribute('administrator') === CommonConstant::DATABASE_VALUE_YES;
    }

    /**
     * Returns true if the user is a superuser.
     */
    public function isSuperuser(): bool
    {
        return $this->getAttribute('superuser') === CommonConstant::DATABASE_VALUE_YES;
    }
    public function approverPurchase(): HasMany
    {
        return $this->hasMany(Product::class, 'purchase_approver_id', 'user_id');
    }

    public function requesterPurchase(): HasMany
    {
        return $this->hasMany(Product::class, 'purchase_requester_id', 'user_id');
    }

    public function userStoreAccesses(): HasMany
    {
        return $this->hasMany(UserStoreAccess::class, 'user_id', 'user_id');
    }

    public function getAuthIdentifierName(): string
    {
    return 'id';
}

    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    public function getAuthPassword()
    {
        return $this->getAttribute('password');
    }

    public function getRememberToken()
    {
        return $this->getAttribute('remember_token');
    }

    public function setRememberToken($value): void
    {
        $this->setAttribute('remember_token', $value);
    }

    public function getRememberTokenName(): string
    {
        return 'remember_token';
    }
}
