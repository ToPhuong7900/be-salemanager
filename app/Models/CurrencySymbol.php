<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CurrencySymbol extends BaseModel
{
    use HasFactory;

    protected $table = "currency_symbol";
    protected $codeField = 'currency_symbol_code';
    protected $primaryKey = "currency_symbol_id";
}
