<?php

namespace App\Models;

use App\Libs\Audit\IAuditable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Product extends BaseModel implements IAuditable
{
    use HasFactory;
    protected $table = "product";
    protected $primaryKey = "product_id";

    protected $fillable = [
        'product_name',
        'product_code',
        'category_id',
        'brand_id',
        'warehouse_id',
        'stock_id',
        'product_price',
        'order_tax'
    ];

    public function productCategory(): BelongsTo
    {
        return $this->belongsTo(Category::class, 'product_category_id', 'product_id');
    }

    public function brand(): BelongsTo
    {
        return $this->belongsTo(Brand::class, 'brand_id', 'id');
    }

    public function purchasesItem(): HasMany
    {
        return $this->hasMany(PurchaseItem::class, 'product_id', 'product_id');
    }



    public function getAuditCode(): string
    {
        return $this->getAttribute('product_code');
    }

    public function getAuditFields(): array
    {
        return [

        ];
    }
}
