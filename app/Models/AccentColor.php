<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class AccentColor extends BaseModel
{
    use HasFactory;

    protected $table = "accent_color";
    protected $codeField = 'accent_color_code';
    protected $primaryKey = "accent_color_id";
}
