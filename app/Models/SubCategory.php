<?php

namespace App\Models;

use App\Libs\AuditTableInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubCategory extends BaseModel implements AuditTableInterface
{
    use HasFactory;
    protected $table = 'sub_category';
    protected $codeField = 'sub_category_code';
    protected $primaryKey = 'sub_category_id';

    protected $fillable = [
        'sub_category_code',
        'sub_category_name',
        'sub_category_slug',
        'company_id',
        'category_id'
    ];

    public function getAuditCode()
    {
        return $this->sub_category_code;
    }
}
