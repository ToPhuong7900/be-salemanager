<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class ProductVariantType extends BaseModel
{
    use HasFactory;

    protected $table = 'product_variant_type';
    protected $primaryKey = "product_variant_type_id";
}
