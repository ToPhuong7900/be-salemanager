<?php

namespace App\Models;

use App\Libs\Audit\IAuditable;
use App\Libs\Common\CommonConstant;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class FilterUserSetting extends BaseModel implements IAuditable
{
    use HasFactory;
    public $table = "filter_user_setting";
    protected $primaryKey = "filter_user_setting_id";

    /**
     * @inheritDoc
     */
    public function getAuditCode(): string
    {
        return $this->getAttribute('filter_user_setting_desc');
    }

    public function isDefault(): bool
    {
        return $this->getAttribute('default') === CommonConstant::DATABASE_VALUE_YES;
    }

    public function scopeDefault($query)
    {
        return $query->where('default', CommonConstant::DATABASE_VALUE_YES);
    }

    public function scopeOfUser($query, $userId)
    {
        return $query->where('user_id', $userId);
    }

    public function scopeOfFilterName($query, $filterNameId)
    {
        return $query->where('filter_name_id', $filterNameId);
    }

    public function scopeOfDisplayOrder($query, $displayOrder)
    {
        return $query->where('display_order', $displayOrder);
    }

    public function getAuditFields(): array
    {
        return [];
    }
}
