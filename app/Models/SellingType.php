<?php

namespace App\Models;

use App\Libs\AuditTableInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class SellingType extends BaseModel implements AuditTableInterface
{
    use HasFactory;

    protected $table = 'selling_type';
    protected $primaryKey = 'selling_type_id';

    const TRANSACTIONAL = 1;
    const SOLUTION = 2;

    public function getAuditCode()
    {
        return $this->selling_type_code;
    }
}
