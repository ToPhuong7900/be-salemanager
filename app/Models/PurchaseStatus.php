<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class PurchaseStatus extends Model
{
    use HasFactory;

    protected $table = 'purchase_status';
    protected $primaryKey = 'purchase_status_id';
    protected $codeField = 'code';
    protected $descField = 'description';
    protected $appends = ['code_description'];

    public function purchaseStatusType(): BelongsTo
    {
        return $this->belongsTo(PurchaseStatusType::class, 'purchase_status_type_id');
    }

    public function purchase(): HasMany
    {
        return $this->hasMany(Purchase::class, 'purchase_status_id');
    }
}
