<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SiteGroupGoogleCaptcha extends BaseModel
{
    use HasFactory;

    protected $table = "company_google_analytics";
    protected $codeField = 'company_google_analytics_code';
    protected $primaryKey = "company_google_analytics_id";
}
