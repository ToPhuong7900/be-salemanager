<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SiteGroupGoogleAnalytics extends Model
{
    use HasFactory;
}
