<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class SaleProduct extends BaseModel
{
    use HasFactory;

    protected $table = 'sale_product';
    protected $primaryKey = 'sale_product_id';

    protected $fillable = [
        'sale_id',
        'product_variant_type_id',
        'sale_tax_value',
        'discount_id',
        'sale_price'
    ];

    protected $casts = [
        'sale_price' => 'double',
        'sale_tax_value' => 'double'
    ];

    public function sale(): BelongsTo
    {
        return $this->belongsTo(Sale::class, 'sale_id', 'sale_product_id');
    }
}
