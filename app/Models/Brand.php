<?php

namespace App\Models;

use App\Libs\AuditTableInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Brand
 *
 * @property int $brand_id
 * @property string $brand_code
 * @property string $brand_name
 * @property int $company_id
 * @property string|null $brand_description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 *
 */
class Brand extends BaseModel implements AuditTableInterface
{
    use HasFactory;

    protected $table = "brand";
    protected $codeField = 'brand_code';
    protected $primaryKey = "brand_id";

    protected $fillable = [
        'brand_code',
        'brand_name',
        'brand_desc',
        'company_id'
    ];

    public function products(): HasMany
    {
        return $this->hasMany(Product::class, 'brand_id', 'brand_id');
    }

    public function getAuditCode()
    {
        return $this->brand_code;
    }
}
