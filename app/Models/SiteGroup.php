<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SiteGroup extends BaseModel
{
    use HasFactory;
    protected $table = 'site_group';
    protected $primaryKey = 'site_group_id';
    protected $fillable = [
        'site_group_code',
        'site_group_desc',
    ];
}
