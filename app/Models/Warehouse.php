<?php

namespace App\Models;

use App\Libs\AuditTableInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Warehouse extends BaseModel implements AuditTableInterface
{
    use HasFactory;

    protected $table = 'warehouse';

    protected $primaryKey = 'warehouse_id';

    protected $fillable = [
        'warehouse_code',
        'warehouse_name',
        'warehouse_phone',
        'warehouse_address',
        'warehouse_country',
        'warehouse_city',
        'warehouse_manager_id'
    ];

    public function products(): HasMany
    {
        return $this->hasMany(Product::class, 'warehouse_id', 'id');
    }

    public function getAuditCode()
    {
        return $this->warehouse_code;
    }
}
