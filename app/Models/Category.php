<?php

namespace App\Models;

use App\Libs\AuditTableInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Category extends BaseModel implements AuditTableInterface
{
    use HasFactory;

    protected $table = "category";
    protected $codeField = 'category_code';
    protected $primaryKey = "category_id";

    protected $fillable = [
        'category_code',
        'category_name',
        'category_slug',
        'company_id'
    ];

    public function products(): HasMany
    {
        return $this->hasMany(Product::class, 'product_category_id', 'category_id');
    }

    public function getAuditCode()
    {
        return $this->category_code;
    }
}
