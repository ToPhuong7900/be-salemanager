<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Sale
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon $date
 * @property int $customer_id
 * @property int $warehouse_id
 * @property float|null $tax_rate
 * @property float|null $tax_amount
 * @property float|null $discount
 * @property float|null $shipping
 * @property float|null $grand_total
 * @property float|null $received_amount
 * @property float|null $paid_amount
 * @property int|null $payment_type
 * @property string|null $note
 * @property string|null $reference_code
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $status
 * @property int|null $payment_status
 * @property-read \App\Models\Customer $customer
 * @property-read int|null $sale_items_count
 * @property-read \App\Models\Warehouse $warehouse
 *
 * @property int $is_return
 * @property-read int|null $payments_count
 *
 * @mixin Eloquent
 */
class Sale extends BaseModel
{
    protected $table = 'sales';
    protected $primaryKey = 'sale_id';

    // discount type const
    const PERCENTAGE = 1;

    const FIXED = 2;

    protected $fillable = [
        'sale_date_in',
        'sale_date_out',
        'customer_id',
        'sale_person_id',
        'sale_grand_total',
        'discount_id',
        'sale_shipping_cost',
        'sale_tax_value',
        'sale_payment_status_id'
    ];

    public $casts = [
        'sale_date_in' => 'date',
        'sale_date_out' => 'date',
        'sale_grand_total' => 'double',
        'sale_shipping_cost' => 'double',
        'sale_tax_value' => 'double',
        'sale_shipping_method' => 'integer'
    ];

    public function customer(): BelongsTo
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'customer_id');
    }

    public function saleItems(): HasMany
    {
        return $this->hasMany(Sale::class, 'sale_id', 'id');
    }

}

