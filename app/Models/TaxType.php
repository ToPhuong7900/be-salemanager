<?php

namespace App\Models;

use App\Libs\AuditTableInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TaxType extends BaseModel implements AuditTableInterface
{
    use HasFactory;

    protected $table = 'tax_type';
    protected $primaryKey = 'tax_type_id';

    const EXCLUSIVE = 1;
    const INCLUSIVE = 2;

    public function getAuditCode()
    {
        return $this->tax_type_code;
    }
}
