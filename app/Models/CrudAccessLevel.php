<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrudAccessLevel extends Model
{
    use HasFactory;
    protected $table      = 'crud_access_level';
    protected $primaryKey = 'crud_access_level_id';

    // Fixed values in database.
    public const CRUD_ACCESS_LEVEL_NONE        = 1;
    public const CRUD_ACCESS_LEVEL_READ        = 2;
    public const CRUD_ACCESS_LEVEL_UPDATE      = 3;
    public const CRUD_ACCESS_LEVEL_CREATE      = 4;
    public const CRUD_ACCESS_LEVEL_DELETE      = 5;
    public const CRUD_ACCESS_LEVEL_SUPERVISOR  = 6;
}
