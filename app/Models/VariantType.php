<?php

namespace App\Models;

use App\Libs\AuditTableInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class VariantType extends BaseModel implements AuditTableInterface
{
    use HasFactory;

    protected $table = "variant_type";
    protected $primaryKey = "variant_type_id";

    public function getAuditCode()
    {
        return $this->variant_type_name;
    }
}
