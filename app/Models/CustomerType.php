<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class CustomerType extends BaseModel
{
    use HasFactory;

    protected $table = "customer_type";
    protected $primaryKey = "customer_type_id";

    public const NORMAL = 1;
    public const NORMAL_CODE = 'NORMAL';
}
