<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class PurchaseProduct extends BaseModel
{
    use HasFactory;

    protected $table = "purchase_product";
    protected $primaryKey = "purchase_product_id";

    protected $fillable = [
        'purchase_id',
        'product_variation_type_id',
        'purchase_product_quantity',
        'purchase_product_net_price',
        'purchase_product_tax',
        'purchase_product_discount',
        'discount_unit_id',
        'purchase_product_price',
    ];

    public function purchase(): BelongsTo
    {
        return $this->belongsTo(Purchase::class, 'purchase_id', 'purchase_product_id');
    }

    public function productVariantType(): BelongsTo
    {
        return $this->belongsTo(
            ProductVariantType::class,
            'product_variant_type_id',
            'purchase_product_id'
        );
    }
}
