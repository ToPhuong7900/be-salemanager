<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Company extends BaseModel
{
    use HasFactory;

    protected $table = "company";
    protected $codeField = 'company_code';
    protected $primaryKey = "company_id";

    protected $fillable = [
        'company_code'
    ];
}
