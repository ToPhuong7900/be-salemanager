<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends BaseModel
{
    protected $table = "employee";
    protected $primaryKey = "employee_id";
    protected $codeField = "employee_code";

    protected $fillable = [
        'employee_code',
        'employee_first_name',
        'employee_last_name',
        'employee_phone',
        'employee_email_address'
    ];
}
