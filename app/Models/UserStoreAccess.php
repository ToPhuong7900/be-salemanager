<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserStoreAccess extends Model
{
    use HasFactory;
    protected $table = 'user_store_access';
    protected $primaryKey = 'user_store_access_id';

    protected $fillable = array('user_id', 'store_id', 'access');
}
