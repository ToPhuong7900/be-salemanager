<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class FilterTableHeaderUser extends BaseModel
{
    use HasFactory;
    public const COLUMN_VISIBLE_KEY = 'visible';
    public const COLUMN_VISIBLE_DISPLAY_ORDER_KEY = 'displayOrder';

    public $table = "filter_table_header_user";
    protected $primaryKey = "filter_table_header_user_id";
    protected bool $siteGroupIdField = false;

    public function user()
    {
        return $this->belongsTo('Tfcloud\Models\User', 'user_id', 'id');
    }

    public function filterName(): BelongsTo
    {
        return $this->belongsTo(FilterName::class, 'filter_name_id', 'filter_name_id');
    }

    public function scopeOfFilterNameId($query, $filterNameId)
    {
        return $query
            ->join('filter_name', 'filter_name.filter_name_id', '=', 'filter_table_header_user.filter_name_id')
            ->where('user_id', \Auth::user()->getKey())
            ->where('filter_name.filter_name_id', $filterNameId);
    }
}
