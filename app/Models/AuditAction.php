<?php

namespace App\Models;

use App\Libs\Audit\IAuditable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AuditAction extends BaseModel implements IAuditable
{
    use HasFactory;
    protected $table = 'audit_action';
    protected $primaryKey = 'audit_action_id';
    public $timestamps = false;
    protected bool $siteGroupIdField = false;

    /**
     * Audit Actions
     */
    /** @staticvar integer ACT_INSERT - record insert */
    public const ACT_INSERT = 1;

    /** @staticvar integer ACT_UPDATE - record update */
    public const ACT_UPDATE = 2;

    /** @staticvar integer ACT_DELETE - record delete */
    public const ACT_DELETE = 3;

    /** @staticvar integer ACT_VIEW - record view */
    public const ACT_VIEW = 4;

    /** @staticvar integer ACT_EMAIL - send/receive email*/
    public const ACT_EMAIL = 5;

    /** @staticvar integer ACT_PRINT - record print */
    public const ACT_PRINT = 6;

    /** @staticvar integer ACT_EXPORT - data exported by interface  */
    public const ACT_EXPORT = 7;

    /** @staticvar integer ACT_IMPORT - data imported by interface  */
    public const ACT_IMPORT = 8;

    /** @staticvar integer ACT_DATALOAD - data updated/inserted via dataload */
    public const ACT_DATALOAD = 9;

    /** @staticvar integer ACT_DOWNLOAD - file downloaded */
    public const ACT_DOWNLOAD = 10;

    /** @staticvar integer ACT_REPORT - run report */
    public const ACT_REPORT = 11;

    /** @staticvar integer ACT_REPORT - run report */
    public const ACT_INSTALL = 12;

    /** @staticvar integer ACT_REPORT - run report */
    public const ACT_UNINSTALL = 13;

    /** @staticvar integer ACT_CADSYNC - process cad file */
    public const ACT_CADSYNC = 14;
    public function getAuditCode(): string
    {
        return $this->getAttribute('audit_action_code');
    }

    public function getAuditFields(): array
    {
        return [];
    }
}
