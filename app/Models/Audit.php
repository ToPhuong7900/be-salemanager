<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Audit extends BaseModel
{
    use HasFactory;
}
