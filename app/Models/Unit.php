<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;


/**
 * Class Unit
 *
 * @property int $id
 * @property string $name
 * @property string $short_name
 * @property string $base_unit
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Unit newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Unit newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Unit query()
 * @method static \Illuminate\Database\Eloquent\Builder|Unit whereBaseUnit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Unit whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Unit whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Unit whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Unit whereShortName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Unit whereUpdatedAt($value)
 *
 * @mixin \Eloquent
 */

class Unit extends BaseModel
{
    use HasFactory;

    protected $table = 'units';
    protected $fillable = [
        'name',
        'short_name',
        'base_unit',
    ];

    /**
     * @return array|string
     */
    public function getBaseUnitName()
    {
        $productUnit = BaseUnit::whereId($this->base_unit)->first();
        if ($productUnit) {
            return $productUnit->toArray();
        }

        return '';
    }
}
