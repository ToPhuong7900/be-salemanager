<?php

namespace App\Models;

use App\Libs\AuditTableInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Store extends BaseModel implements AuditTableInterface
{
    use HasFactory;

    protected $table = "store";
    protected $primaryKey = "store_id";
    protected $codeField = "store_code";

    public function employee(): BelongsTo
    {
        return $this->belongsTo(Employee::class);
    }

    public function getAuditCode()
    {
       return $this->store_code;
    }
}
