<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DateFormat extends BaseModel
{
    use HasFactory;

    protected $table = "date_format";
    protected $primaryKey = "date_format_id";
    protected $codeField = "date_format_code";
}
