<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{
    use HasFactory;
    protected $table = 'user_group';
    protected $primaryKey = 'user_group_id';
    protected $fillable = [
        'user_group_name',
        'user_group_desc',
        'user_group_active',
        'site_group_id'
    ];
}
