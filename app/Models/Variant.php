<?php

namespace App\Models;

/**
 * App\Models\Brand
 *
 * @property int $variant_id
 * @property int $product_id
 * @property int $variant_type_id
 */
class Variant extends BaseModel
{
    protected $table = "variant";
    protected $primaryKey = "variant_id";

    protected $fillable = [
        'product_id',
        'variant_type_id'
    ];
}
