<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Module extends BaseModel
{
    use HasFactory;
    public const MODULE_INVENTORY = 1;
    public const MODULE_SALES = 2;
    public const MODULE_STOCK = 3;
    public const MODULE_PURCHASE = 4;
    public const MODULE_PROMO = 5;
    public const MODULE_REPORT = 6;
    public const MODULE_HRM = 7;
    public const MODULE_FINANCE = 8;
}
