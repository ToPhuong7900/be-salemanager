<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Department extends BaseModel
{
    use HasFactory;

    protected $table = "department";
    protected $primaryKey = "department_id";
    protected $codeField = "department_code";

    public function employee(): HasMany
    {
        return $this->hasMany(Employee::class, 'employee_id');
    }
}
