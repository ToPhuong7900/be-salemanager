<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PurchaseStatusType extends Model
{
    use HasFactory;

    protected $table      = 'purchase_status_type';
    protected $primaryKey = 'purchase_status_type_id';

    public const RECEIVED = 1;

    public const PENDING = 2;

    public const ORDERED = 3;
}
