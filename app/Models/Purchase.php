<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Purchase extends BaseModel
{
    use HasFactory;

    protected $table = 'purchase';
    protected $primaryKey = 'purchase_id';

    //tax type  const
    const EXCLUSIVE = 1;

    const INCLUSIVE = 2;

    const PERCENTAGE = 1;
    const CASH = 2;

    protected $fillable = [
        'purchase_date',
        'supplier_id',
        'warehouse_id',
        'purchase_order_tax',
        'purchase_discount',
        'purchase_shipping_cost',
        'purchase_grand_total',
        'received_amount',
        'purchase_note',
        'purchase_status_id',
        'grand_total',
        'status'
    ];

    public static $rules = [
        'date' => 'required|date',
        'supplier_id' => 'required|exists:suppliers,id',
        'warehouse_id' => 'required|exists:warehouses,id',
        'tax_rate' => 'nullable|numeric',
        'tax_amount' => 'nullable|numeric',
        'discount' => 'nullable|numeric',
        'shipping' => 'nullable|numeric',
        'grand_total' => 'nullable|numeric',
        'received_amount' => 'numeric|nullable',
        'paid_amount' => 'numeric|nullable',
        'payment_type' => 'numeric|integer',
        'notes' => 'nullable',
        'status' => 'integer|required',
        'reference_code' => 'nullable',
    ];

    public $casts = [
        'purchase_date' => 'date',
        'purchase_order_tax' => 'double',
        'purchase_discount' => 'double',
        'purchase_shipping_cost' => 'double',
        'purchase_grand_total' => 'double',
        'received_amount' => 'double',
    ];

    public function warehouse(): BelongsTo
    {
        return $this->belongsTo(Warehouse::class, 'warehouse_id', 'id');
    }

    public function purchaseStatus(): BelongsTo
    {
        return $this->belongsTo(PurchaseStatus::class, 'purchase_status_id');
    }

    public function purchaseItems(): HasMany
    {
        return $this->hasMany(PurchaseItem::class, 'purchase_id', 'purchase_id');
    }

    public function supplier(): BelongsTo
    {
        return $this->belongsTo(Supplier::class, 'supplier_id', 'supplier_id');
    }
}
