<?php

namespace App\Models;

use App\Libs\AuditTableInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class VariantTypeAttributes extends BaseModel implements AuditTableInterface
{
    use HasFactory;

    protected $table = "variant_type_attributes";
    protected $codeField = 'variant_type_attributes_name';
    protected $primaryKey = "variant_type_attributes_id";

    protected $fillable = [
        'variant_type_attributes_name',
        'variant_type_id'
    ];

    public function getAuditCode()
    {
        return $this->variant_type_attributes_name;
    }
}
