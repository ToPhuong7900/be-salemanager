<?php

namespace App\Models;

use App\Libs\Common\Common;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class System extends BaseModel
{
    use HasFactory;
    protected $table = 'system';
    protected $primaryKey = 'system_id';
    protected $appends = ['release_version_under_scored'];

    public function getReleaseVersionUnderScoredAttribute(): float|int|string
    {
        return Common::concatFields(
            [
                $this->getAttribute('major_release_version'),
                $this->getAttribute('minor_release_version'),
                $this->getAttribute('fix_release_version')
            ],
            ['separator' => '_']
        );
    }
}
