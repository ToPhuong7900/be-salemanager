<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class AuditTable extends BaseModel
{
    use HasFactory;

    protected $table = "company_google_captcha";
    protected $codeField = 'company_google_captcha_code';
    protected $primaryKey = "company_google_captcha_id";
}
