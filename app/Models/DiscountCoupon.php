<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * App\Models\CouponCode
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property \Illuminate\Support\Carbon $start_date
 * @property \Illuminate\Support\Carbon $end_date
 * @property int $how_many_time_can_use
 * @property float $discount
 * @property int $how_many_time_used
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 *
 * @property int $discount_type
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Product> $products
 * @property-read int|null $products_count

 *
 * @mixin \Eloquent
 */


class DiscountCoupon extends BaseModel
{
    use HasFactory;

    protected $table = "discount_coupon";
    protected $primaryKey = "discount_coupon_id";
    public const FIXED = 1;

    public const PERCENTAGE = 2;

    protected $fillable = [
        'name',
        'code',
        'start_date',
        'end_date',
        'how_many_time_can_use',
        'discount_type',
        'discount',
        'how_many_time_used',
    ];

    public function prepareAttributes(): array
    {
        $products = [];

        foreach ($this->products as $product) {
            $products[] = [
                'id' => $product->id,
                'name' => $product->name,
            ];
        }

        return [
            'name' => $this->name,
            'code' => $this->code,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'how_many_time_can_use' => $this->how_many_time_can_use,
            'discount_type' => $this->discount_type,
            'discount' => $this->discount,
            'how_many_time_used' => $this->how_many_time_used,
            'products' => $products,
        ];
    }

    /**
     * The products that belong to the CouponCode
     */
    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, 'coupon_product');
    }
}
