<?php

namespace App\Models;

use Faker\Provider\Base;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class PurchaseItem extends BaseModel
{
    protected $table = 'purchase_item';

    protected $fillable = [
        'purchase_id',
        'product_id',
        'product_price',
        'net_unit_cost',
        'tax_type',
        'tax_value',
        'tax_amount',
        'discount_type',
        'discount_value',
        'discount_amount',
        'purchase_unit',
        'quantity',
        'grand_total',
        'status',
        'variant_id'
    ];

    public static array $rules = [
        'product_id' => 'required|exists:products,id',
        'product_cost' => 'nullable|numeric',
        'net_unit_cost' => 'nullable|numeric',
        'tax_type' => 'nullable|numeric',
        'tax_value' => 'nullable|numeric',
        'tax_amount' => 'nullable|numeric',
        'discount_type' => 'nullable|numeric',
        'discount_value' => 'nullable|numeric',
        'discount_amount' => 'nullable|numeric',
        'purchase_unit' => 'nullable|numeric',
        'quantity' => 'nullable|numeric',
        'sub_total' => 'nullable|numeric',
        'unit_id' => 'integer',
    ];

    public $casts = [
        'product_cost' => 'double',
        'net_unit_cost' => 'double',
        'tax_value' => 'double',
        'tax_amount' => 'double',
        'discount_value' => 'double',
        'discount_amount' => 'double',
        'quantity' => 'double',
        'grand_total' => 'double',
    ];

    public function purchase(): BelongsTo
    {
        return $this->belongsTo(Purchase::class, 'purchase_id', 'purchase_id');
    }

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class, 'product_id', 'product_id');
    }
}
