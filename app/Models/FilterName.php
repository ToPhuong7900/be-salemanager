<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FilterName extends BaseModel
{
    use HasFactory;
    public $table = "filter_name";
    public $incrementing = false;
    protected $primaryKey = "filter_name_id";
    protected bool $siteGroupIdField = false;
    public function scopeOfFilterNameCode($query, $filterNameCode)
    {
        return $query->where('filter_name_code', $filterNameCode);
    }
}
