<?php

namespace App\Libs;

interface AuditTableInterface
{
    /**
     * Get the table associated with the model.
     *
     * @return string
     */
    public function getTable();

    /**
     * Get the value of the model's primary key.
     *
     * @return mixed
     */
    public function getKey();

    /**
     * Get the value of the models audit code.
     *
     * @return string
     */
    public function getAuditCode();
}
