<?php

namespace App\Libs\BoxFilter;

use Illuminate\Support\Arr;
use Tfcloud\Http\Middleware\BoxFilterRecall;
use App\Libs\BoxFilter\Constants\OperatorTypeConstant;
use App\Libs\BoxFilter\Elements\IElement;
use App\Libs\BoxFilter\Helper\UserDefineBoxFilter;
use App\Libs\BoxFilterQuery\Constants\FilterQueryConstant;
use App\Libs\BoxFilterQuery\FilterFieldSet\FilterSet;
use App\Libs\BoxFilterQuery\Parsers\FilterFieldParser;
use Tfcloud\Models\Filter\FilterName;
use Tfcloud\Models\Filter\FilterUserSetting;
use Tfcloud\Models\User;

abstract class BoxFilter implements IBoxFilter
{
    private const LOCKED_FILTER = 'lockedFilter';
    private const SHOW_PATTERN_TO_KPI_CHART_COLOUR = 'showPatternToKpiChartColour';
    protected const BOX_FILTER_COMPONENT_NAME = 'boxComponentName';
    protected const FILTER_NAME_ID_STR = 'filterNameId';
    protected const FILTER_ELEMENTS_STR = 'filterElements';
    protected const DEFAULT_USER_FILTER_VALUE_STR = 'defaultUserFilterValue';
    protected const SUPPORTED_OPERATORS = 'supportedOperators';
    protected const RELATED_RESTRICT_DATA = 'relatedRestrictData';
    protected const SHOW_FILTER = 'showFilter';
    protected const IS_CUSTOM_FILTER_VALUE = 'isCustomFilterValue';
    protected const SHOW_ALL_NOT_APPLIED_FILTERS = 'showAllNotAppliedFilters';
    protected const SHOW_SAVED_FILTER = 'showSavedFilter';

    /**
     * Provide gen_table_id of user define record
     * @var null|integer
     */
    protected $genTableId = null;

    /**
     * Enable user define fields in filters
     * @var bool
     */
    protected $isUserDefineFilter = false;

    /**
     * Hide operator and set radio mode for picker of UD filter
     * @var bool
     */
    protected $allowSingleOperatorUDFilters = false;

    /**
     * Provide user define status option
     * @var array
     */
    protected $udfStatus = [];

    /**
     * @var bool
     */
    protected $showFilter = true;

    /**
     * @var bool
     */
    protected $lockedFilter = false;

    /**
     * @var bool
     */
    protected $showSavedFilter = true;

    /**
     * Allow to show all filters in Filters Applied without defaultFilter option setting
     * @var bool
     */
    protected $showAllNotAppliedFilters = false;

    /**
     * @var integer
     */
    private $filterNameId;

    /**
     * Array object of ElementInterface
     * @var IElement[]
     */
    private $filterElements = [];

    /**
     * Array object of ElementInterface that uses for blind filer fields
     * @var IElement[]
     */
    private $blindElements = [];

    /**
     * @var array
     */
    private $filterLookups = [];

    /**
     * @var IBoxComponent
     */
    private $boxComponent;

    /**
     * @var User
     */
    private $loginUser;

    /**
     * @var integer
     */
    private $moduleId;

    /**
     *
     * @var null|UserDefineBoxFilter
     */
    private $userDefineBoxFilter = null;

    /**
     * @var null|Object
     */
    private $defaultFilterUserSetting = null;

    /**
     * @var bool
     */
    protected $isFromReport = false;

    public function __construct(int $filterNameId)
    {
        $this->init($filterNameId);
    }

    public function getFilterNameId()
    {
        return $this->filterNameId;
    }

    public function getModuleId()
    {
        return $this->moduleId;
    }

    public function setModuleId(int $moduleId)
    {
        $this->moduleId = $moduleId;
        return $this;
    }

    public function generate()
    {
        $filterElements = [];
        foreach ($this->filterElements as $element) {
            $filterElements[] = $element->getElement();
        }
        $sortedFilterElements = array_values(array_sort($filterElements, function ($value) {
            return $value['groupLabel'] . $value['label'];
        }));
        $boxSettings = [
            self::FILTER_NAME_ID_STR => $this->getFilterNameId(),
            self::FILTER_ELEMENTS_STR => $sortedFilterElements,
            self::DEFAULT_USER_FILTER_VALUE_STR => $this->getDefaultUserFilterValue(),
            self::SUPPORTED_OPERATORS => OperatorTypeConstant::getSupportedOperators(),
            self::RELATED_RESTRICT_DATA => $this->generateBlindElements(),
            self::SHOW_FILTER => $this->showFilter,
            self::IS_CUSTOM_FILTER_VALUE => (bool)$this->defaultFilterUserSetting,
            self::SHOW_ALL_NOT_APPLIED_FILTERS => $this->showAllNotAppliedFilters,
            self::SHOW_SAVED_FILTER => $this->showSavedFilter,
            self::LOCKED_FILTER => $this->lockedFilter,
            self::SHOW_PATTERN_TO_KPI_CHART_COLOUR => \Auth::user()->showPatternToKpiChartColour(),
        ];

        if ($this->boxComponent instanceof IBoxComponent) {
            $componentName = $this->boxComponent->getComponentName();
            $boxSettings[self::BOX_FILTER_COMPONENT_NAME] = $componentName;
            $boxSettings[$componentName] = $this->boxComponent->generate();
        }

        return $boxSettings;
    }

    public function getFilterSet()
    {
        return new FilterSet(
            empty($this->blindElements) ? $this->filterElements :
                array_merge($this->filterElements, $this->blindElements),
            $this->userDefineBoxFilter
        );
    }

    public function getElements()
    {
        return $this->filterElements;
    }

    public function addElement(IElement $element)
    {
        $this->filterElements[] = $element;
        return $this;
    }

    public function addBlindElement(IElement $element)
    {
        $this->blindElements[] = $element;
        return $this;
    }

    public function addBlindElements(array $elements)
    {
        $this->blindElements = array_merge($this->blindElements, $elements);
        return $this;
    }

    public function getBlindElements()
    {
        return $this->blindElements;
    }

    public function addElements(array $elements)
    {
        $this->filterElements = array_merge($this->filterElements, $elements);
        return $this;
    }

    public function removeElementsByName($removeElements = [])
    {
        foreach ($this->getElements() as $key => $element) {
            if (in_array($element->getName(), $removeElements)) {
                unset($this->filterElements[$key]);
            }
        }

        return $this;
    }

    public function setFilterLookups(array $filterLookups)
    {
        $this->filterLookups = $filterLookups;
        return $this;
    }

    public function setIsFromReport(bool $isFromReport)
    {
        $this->isFromReport = $isFromReport;
        return $this;
    }

    public function getFilterLookups()
    {
        return $this->filterLookups;
    }

    /**
     * Init filterNameId, moduleId and set defaultTableHeader for this filter.
     * @param int $filterNameId
     */
    protected function init(int $filterNameId)
    {
        $filterName = FilterName::findOrFail($filterNameId);

        $this->setLoginUser(\Auth::user())
            ->setIsUserDefineFilter(empty($this->genTableId) ? false : $this->isUserDefineFilter)
            ->setUserDefineBoxFilter(empty($this->genTableId) ? null : new UserDefineBoxFilter($this->genTableId))
            ->setFilterNameId($filterName->getKey())
            ->addUserDefinedFilters()
            ->setModuleId($filterName->module_id)
            ->setDefaultFilterUserSetting()
            ->setIsFromReport($this->isFromReport);
    }

    protected function setLoginUser($user)
    {
        $this->loginUser = $user;
        return $this;
    }

    protected function setIsUserDefineFilter($isUserDefineFilter)
    {
        $this->isUserDefineFilter = $isUserDefineFilter;
        return $this;
    }

    protected function setUserDefineBoxFilter($userDefineBoxFilter)
    {
        $this->userDefineBoxFilter = $userDefineBoxFilter;
        return $this;
    }

    protected function setFilterNameId(int $filterNameId)
    {
        $this->filterNameId = $filterNameId;
        return $this;
    }

    protected function setBoxComponent(IBoxComponent $boxComponent)
    {
        $this->boxComponent = $boxComponent;
        return $this;
    }

    public function getBoxComponent()
    {
        return $this->boxComponent;
    }

    /**
     * Get user default filter setting.
     * @return string|null
     */
    protected function getDefaultUserFilterValue()
    {
        $sessionKey = BoxFilterRecall::getSessionKey(request()->path(), $this->filterNameId);
        $isFilterAll = request(FilterQueryConstant::NOT_APPLY_RECALL_FILTER_KEY, false);
        $recallFilter = session($sessionKey, null);
        $isHasRecallFilter = Arr::get($recallFilter, FilterQueryConstant::FILTER_PARAMETER_KEY, false);

        if ($isHasRecallFilter && !$isFilterAll) {
            if ($this->defaultFilterUserSetting) {
                $defaultFilterList = json_decode($this->defaultFilterUserSetting->filter_value, true);
                $notAppliedFilterUserSetting = $this->getNotAppliedFilter(
                    $defaultFilterList,
                    $recallFilter[FilterQueryConstant::FILTER_PARAMETER_KEY]
                );

                if (empty($notAppliedFilterUserSetting)) {
                    return json_encode($recallFilter);
                }

                $recallFilter[FilterQueryConstant::FILTER_PARAMETER_KEY] = array_merge(
                    $recallFilter[FilterQueryConstant::FILTER_PARAMETER_KEY],
                    $notAppliedFilterUserSetting
                );

                return json_encode($recallFilter);
            }
            $this->defaultFilterUserSetting = null;
            return json_encode($recallFilter);
        }

        if ($this->defaultFilterUserSetting) {
            return $this->defaultFilterUserSetting->filter_value;
        }

        $defaultFilterList = $this->getDefaultFilterParams();
        return empty($defaultFilterList) ? null : json_encode([
            FilterQueryConstant::FILTER_PARAMETER_KEY => $defaultFilterList
        ]);
    }

    private function getNotAppliedFilter($filters, $recallFilter): array
    {
        $notAppliedFilters = [];

        $appliedKeys = array_map(function ($filter) {
            return $filter[FilterFieldParser::FIELD_KEY];
        }, $recallFilter);

        foreach ($filters[FilterQueryConstant::FILTER_PARAMETER_KEY] as $filter) {
            if (
                $filter[FilterFieldParser::OPERATOR_KEY] != null
                || in_array($filter[FilterFieldParser::FIELD_KEY], $appliedKeys)
            ) {
                continue;
            }
            $notAppliedFilters[] = $filter;
        }
        return $notAppliedFilters;
    }

    protected function setDefaultFilterUserSetting()
    {
        $this->defaultFilterUserSetting = FilterUserSetting::userSiteGroup()
            ->OfUser($this->loginUser->getKey())
            ->OfFilterName($this->getFilterNameId())
            ->default()->first();

        return $this;
    }

    /**
     * @return UserDefineBoxFilter|null
     */
    protected function getUserDefineBoxFilter()
    {
        return $this->userDefineBoxFilter;
    }

    /**
     * @param array $udfStatus
     * @return $this
     */
    protected function addUserDefinedFilters()
    {
        if ($this->isUserDefineFilter) {
            $elementsAndHeaders = $this->userDefineBoxFilter->getElementsAndHeaders(
                $this->udfStatus,
                $this->allowSingleOperatorUDFilters
            );

            $this->addElements(array_get($elementsAndHeaders, 'elements', []));
        }
        return $this;
    }

    /**
     * Get default filter params of a list if they are exiting
     * @return array
     */
    protected function getDefaultFilterParams()
    {
        return [];
    }

    protected function getFilterLookup($key, $default = [])
    {
        return array_get($this->filterLookups, $key, $default);
    }

    protected function generateBlindElements()
    {
        $output = [];
        foreach ($this->blindElements as $element) {
            $output[$element->getName()] =  $element->getDefaultValue();
        }
        return $output;
    }

    protected function getElementByName(string $fieldName)
    {
        /* @var $element IElement*/
        $element = array_first(
            $this->getElements(),
            fn($el) => $el->getName() == $fieldName
        );

        return $element;
    }

    protected function whenElement(string $fieldName, \Closure $callback)
    {
        $element = $this->getElementByName($fieldName);

        if ($element) {
            $callback($element);
        }

        return $this;
    }
}
