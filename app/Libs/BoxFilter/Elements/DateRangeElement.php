<?php

namespace App\Libs\BoxFilter\Elements;

use App\Libs\BoxFilter\Constants\ElementTypeConstant;
use App\Libs\BoxFilter\Constants\OperatorTypeConstant;

class DateRangeElement extends Element
{
    /**
     * DateRangeElement constructor.
     * @param string $name
     * @param string $label
     * @param array $options
     */
    public function __construct(string $name, string $label, $options = [])
    {
        parent::__construct($name, $label, ElementTypeConstant::DATE_RANGE, [
            OperatorTypeConstant::INCLUDE_RANGE,
            OperatorTypeConstant::EXCLUDE_RANGE,
        ], [], $options);
        $this->setDefaultOperator(OperatorTypeConstant::INCLUDE_RANGE);
    }
}
