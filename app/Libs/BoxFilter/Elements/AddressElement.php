<?php

namespace App\Libs\BoxFilter\Elements;

use App\Libs\BoxFilter\Constants\OperatorTypeConstant;

class AddressElement
{
    public const ADDRESS_GROUP_NAME = 'address';

    private $elements = [];

    public function __construct($setNotApplied = false)
    {
        $this->setupElements($setNotApplied);
    }

    private function setupElements($setNotApplied)
    {
        $this->elements[] = ((new TextElement('address', 'Address'))
            ->setNotAppliedFilter($setNotApplied)
            ->setGroup(self::ADDRESS_GROUP_NAME)
            ->setOperators([OperatorTypeConstant::CONTAIN])
            ->showOperator(false));
    }

    public function addNumberNameElement()
    {
        return $this->pushElement(new TextElement('first_addr_obj', 'Number / Name'));
    }

    public function addSubDwellingElement()
    {
        return $this->pushElement(new TextElement('second_addr_obj', 'Sub Dwelling'));
    }

    public function addStreetElement()
    {
        return $this->pushElement(new TextElement('street', 'Street'));
    }

    public function addLocalityElement()
    {
        return $this->pushElement(new TextElement('locality', 'Locality'));
    }

    public function addTownElement()
    {
        return $this->pushElement(new TextElement('town', 'Town'));
    }

    public function addRegionElement()
    {
        return $this->pushElement(new TextElement('region', 'Region'));
    }

    public function addPostcodeElement()
    {
        return $this->pushElement(new TextElement('postcode', 'Postcode'));
    }

    public function addCountryElement()
    {
        return $this->pushElement(new TextElement('country', 'Country'));
    }

    private function pushElement(Element $element)
    {
        $this->elements[] = $element
            ->setGroup(self::ADDRESS_GROUP_NAME)
            ->setParent('address')
            ->setCustomOption('order', count($this->elements));
        return $this;
    }

    public function getElements()
    {
        return $this->elements;
    }
}
