<?php

namespace App\Libs\BoxFilter\Elements;

use Illuminate\Support\Collection;
use App\Libs\BoxFilter\Constants\ElementTypeConstant;
use App\Libs\BoxFilter\Constants\OperatorTypeConstant;
use App\Libs\BoxFilter\Elements\Configuration\DataConfig;

class DropDownElement extends Element
{
    protected $operatorTypes = [
        OperatorTypeConstant::INCLUDE,
        OperatorTypeConstant::NOT_INCLUDE,
        OperatorTypeConstant::BLANK,
        OperatorTypeConstant::NOT_BLANK
    ];

    protected $defaultDropDownOptions = [
        'showCheckedItemOnTop' => true,
        'showValueSection' => true,
        'radioMode' => false,
        'dropFlushClass' => '',
        'showSelectAll' => true,
    ];

    /**
     * DropDownElement constructor.
     * @param string $name Element filter name to submit for query.
     * @param string $label Element label text to display.
     * @param Collection|array $dataLookup Values for Data
     * @param array $dataConfig Data config
     * @param array $options Options of element
     */
    public function __construct(string $name, string $label, $dataLookup, $dataConfig = [], $options = [])
    {
        parent::__construct(
            $name,
            $label,
            ElementTypeConstant::DROP_DOWN,
            $this->operatorTypes,
            new DataConfig($dataLookup, $dataConfig),
            $options
        );
        $this->options->addOptions($this->defaultDropDownOptions);
        $this->setDefaultOperator(OperatorTypeConstant::INCLUDE);
    }

    /**
     * Allow to show or not show the search text box to find the value.
     * @param bool $isShow
     * @return IElement
     */
    public function showValueSection(bool $isShow)
    {
        $this->options->setOption('showValueSection', $isShow);
        return $this;
    }

    public function showSelectAll(bool $isShow)
    {
        $this->options->setOption('showSelectAll', $isShow);
        return $this;
    }

    public function showCheckedItemOnTop($isShow)
    {
        $this->options->setOption('showCheckedItemOnTop', $isShow);
        return $this;
    }

    public function setOperatorsForRequiredField()
    {
        $this->setOperators(OperatorTypeConstant::getRequiredDropdownFieldOperators());
        return $this;
    }

    public function setOperatorsForAll()
    {
        $this->setOperators($this->operatorTypes);
        return $this;
    }

    /**
     * @param string $valueFieldName
     * @return $this
     */
    public function setValueFieldName(string $valueFieldName)
    {
        $this->data->setOption('valueFieldName', $valueFieldName);
        return $this;
    }

    /**
     * @param string[] $textFieldNames
     * @return $this
     */
    public function setTextFieldNames(array $textFieldNames)
    {
        $this->data->setOption('textFieldName', $textFieldNames);
        return $this;
    }

    /**
     * @param string $cssStyle
     * @return $this
     */
    public function setRecordCssFieldName(string $cssStyle = 'recordCssFieldName')
    {
        $this->data->setOption('recordCssFieldName', $cssStyle);
        return $this;
    }

    public function setDropFlushClass(string $class = '')
    {
        $this->options->setOption('dropFlushClass', $class);
        return $this;
    }

    /**
     * @param bool $isRadioMode
     * @return $this
     */
    public function enableRadioMode(bool $isRadioMode = true)
    {
        $this->options->setOption('radioMode', $isRadioMode);
        $this->setOperators([OperatorTypeConstant::MATCH]);
        $this->setDefaultOperator(OperatorTypeConstant::MATCH);
        $this->showOperator(false);
        return $this;
    }
}
