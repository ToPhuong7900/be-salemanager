<?php

namespace App\Libs\BoxFilter\Elements\Pickers;

use App\Libs\BoxFilter\Elements\PickerElement;

class AssetCategoryPickerElement extends PickerElement
{
    protected string $api = '/api/property/asset-category';

    protected string $valueFiledName = 'prop_facility_asset_category_id';

    protected array $textFiledNames = ['prop_facility_asset_category_desc', 'prop_facility_asset_category_type_desc'];

    /**
     * AssetCategoryPickerElement constructor.
     * @param string $name Element filter name to submit for query.
     * @param string $label Element label text to display.
     * @param array $dataLookupFilters
     * @param array $options Options of element
     */
    public function __construct(string $name, string $label, array $dataLookupFilters = [], $options = [])
    {
        parent::__construct($name, $label, $options);
        $this->setDataLookupFilters($dataLookupFilters);
    }
}
