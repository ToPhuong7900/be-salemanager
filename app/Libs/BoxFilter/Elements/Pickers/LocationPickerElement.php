<?php

namespace App\Libs\BoxFilter\Elements\Pickers;

use App\Libs\BoxFilter\Constants\OperatorTypeConstant;
use App\Libs\BoxFilter\Elements\DropDownApiElement;
use App\Libs\BoxFilter\Elements\DropDownElement;
use App\Libs\BoxFilter\Elements\Element;
use App\Libs\BoxFilter\Elements\IElement;
use App\Libs\TfHttp;

class LocationPickerElement
{
    public const LOCATION_PICKER_GROUP_NAME = 'location';
    public const SITE_ID_FIELD = 'site_id';
    public const BUILDING_ID_FIELD = 'building_id';
    public const BUILDING_BLOCK_ID_FIELD = 'building_block_id';
    public const ROOM_ID_FIELD = 'room_id';
    public const PROP_ZONE_ID_FIELD = 'prop_zone_id';
    public const PROP_EXT_ID_FIELD = 'prop_external_id';

    public const LIMIT_BY_PROJECT_ID = 'limitByProjectId';
    public const LIMIT_BY_TYPE = 'limitByType';
    public const LIMIT_BY_ID = 'limitById';
    public const LIMIT_BY_CP_CONTRACT_ID = 'limitLocationByCpContractId';

    private const BUILDING_API = "/api/property/sites/{%s}/buildings";
    private const ROOM_API = "/api/property/sites/{%s}/buildings/{%s}/rooms";
    private const BUILDING_BLOCK_API = "/api/property/sites/{%s}/buildings/{%s}/blocks";
    private const PROP_ZONE_API = "/api/property/sites/{%s}/zones";
    private const PROP_EXT_API = "/api/property/zones/{%s}/prop-external-area";
    private const BUILDING_ADVANCED_SEARCH_URL = "/modals/building-box-filter";
    private const BUILDING_ADVANCED_SEARCH_TITLE = "Building";
    private const ROOM_ADVANCED_SEARCH_URL = "/modals/room-box-filter";
    private const ROOM_ADVANCED_SEARCH_TITLE = "Room";
    private const QUICK_SEARCH_API = '/api/property/location';

    /**
     * @var SitePickerElement
     */
    private $siteElement;
    /**
     * @var DropDownApiElement
     */
    private $buildingElement;
    /**
     * @var DropDownApiElement
     */
    private $blockElement;
    /**
     * @var DropDownApiElement
     */
    private $roomElement;
    /**
     * @var DropDownApiElement
     */
    private $zoneElement;
    /**
     * @var DropDownApiElement
     */
    private $externalElement;

    private $maxLevel;
    private $elements = [];
    private $incZone;
    private $incExternal;
    private $incBlock;
    private $siteChildren = [];

    /**
     * LocationPickerElement constructor.
     * @param int $maxLevel Max supported level
     * @param bool $incZone Include Zone level
     * @param bool $incExternal Include External level
     * @param bool $incBlock Include Block level
     */
    public function __construct(
        int $maxLevel = 1,
        bool $incZone = false,
        bool $incExternal = false,
        bool $incBlock = false
    ) {
        $this->maxLevel = $maxLevel;
        $this->incZone = $incZone;
        $this->incExternal = $incExternal;
        $this->incBlock = $incBlock;
        $this->setupElements();
    }

    public function setNotAppliedFilter($flag = true)
    {
        $this->siteElement->setNotAppliedFilter($flag);
        return $this;
    }

    public function showOperator($flag)
    {
        return $this->siteElement->showOperator($flag);
    }

    public function setDefaultSite($siteId)
    {
        $this->siteElement->setDefaultValue($siteId);
        return $this;
    }

    public function setKeepAliveSite($flag)
    {
        $this->siteElement->setKeepAlive($flag);
        return $this;
    }

    public function setKeepAliveBuilding($flag)
    {
        if ($this->maxLevel > 1) {
            $this->buildingElement->setKeepAlive($flag);
        }
        return $this;
    }

    public function setKeepAliveBuildingBlock($flag)
    {
        if ($this->maxLevel > 1 && $this->incBlock === true) {
            $this->blockElement->setKeepAlive($flag);
        }
        return $this;
    }

    public function setKeepAliveRoom($flag)
    {
        if ($this->maxLevel == 3) {
            $this->roomElement->setKeepAlive($flag);
        }
        return $this;
    }

    public function setKeepAliveZone($flag)
    {
        if ($this->maxLevel > 1 && $this->incZone === true) {
            $this->zoneElement->setKeepAlive($flag);
        }
        return $this;
    }

    public function setKeepAliveExt($flag)
    {
        if ($this->maxLevel > 1 && $this->incZone === true && $this->incExternal === true) {
            $this->externalElement->setKeepAlive($flag);
        }
        return $this;
    }

    public function setDisabledSite($flag)
    {
        $this->siteElement->setDisabled($flag);
        return $this;
    }

    public function setDisabledBuilding($flag)
    {
        if ($this->maxLevel > 1) {
            $this->buildingElement->setDisabled($flag);
        }
        return $this;
    }

    public function setDefaultBuilding($buildingId)
    {
        if ($this->maxLevel > 1) {
            $this->buildingElement->setDefaultValue($buildingId);
        }
        return $this;
    }

    public function setDefaultBuildingBlock($buildingBlockId)
    {
        if ($this->maxLevel > 1 && $this->incBlock === true) {
            $this->blockElement->setDefaultValue($buildingBlockId);
        }
        return $this;
    }

    public function setDisabledRoom($flag)
    {
        if ($this->maxLevel > 1) {
            $this->roomElement->setDisabled($flag);
        }
        return $this;
    }

    public function setDefaultRoom($roomId)
    {
        if ($this->maxLevel == 3) {
            $this->roomElement->setDefaultValue($roomId);
        }
        return $this;
    }

    public function setDefaultZone($zoneId)
    {
        if ($this->maxLevel > 1 && $this->incZone === true) {
            $this->zoneElement->setDefaultValue($zoneId);
        }
        return $this;
    }

    public function setDefaultExt($extId)
    {
        if ($this->maxLevel > 1 && $this->incZone === true && $this->incExternal === true) {
            $this->externalElement->setDefaultValue($extId);
        }
        return $this;
    }

    public function getElements()
    {
        $this->resetDefaultOperators();
        $this->siteElement->setQuickSearchURL($this->getQuickSearchURL());
        return $this->elements;
    }

    /**
     * Reset default operators to work with new filter in UI correctly.
     * @return $this
     */
    protected function resetDefaultOperators()
    {
        if ($this->roomElement && $this->roomElement->getDefaultValue()) {
            $this->buildingElement->setDefaultOperator(OperatorTypeConstant::MATCH);
            $this->siteElement->setDefaultOperator(OperatorTypeConstant::MATCH);

            return $this;
        }

        if ($this->buildingElement && $this->buildingElement->getDefaultValue()) {
            $this->siteElement->setDefaultOperator(OperatorTypeConstant::MATCH);
        }

        return $this;
    }

    private function getQuickSearchURL()
    {
        $params = [
            'maxLevel' => $this->maxLevel,
            'maxLevelInput' => $this->maxLevel,
            'isShowBlock' => $this->incBlock,
            'fixedLevel' => 0,
        ];
        $defSiteId = $this->siteElement->getDefaultValue();
        $defBuildingId = $this->buildingElement ? $this->buildingElement->getDefaultValue() : null;
        $defRoomId = $this->roomElement ? $this->roomElement->getDefaultValue() : null;
        $defBlockId = $this->blockElement ? $this->blockElement->getDefaultValue() : null;
        if ($defSiteId) {
            $params['fixedLevel'] = 1;
            $params['siteId'] = $defSiteId;
        }
        if ($defBuildingId) {
            $params['fixedLevel'] = 2;
            $params['buildingId'] = $defBuildingId;
        }
        if ($this->incBlock && $defBlockId) {
            $params['fixedLevel'] = 2;
            $params['blockId'] = $defBlockId;
        }
        if ($defRoomId) {
            $params['fixedLevel'] = 3;
            $params['roomId'] = $defRoomId;
        }
        return self::QUICK_SEARCH_API . TfHttp::buildParams($params);
    }

    public function setLabel($label)
    {
        $this->siteElement->setGroupLabel($label);
        return $this;
    }

    public function disableSiteAdvancedSearch()
    {
        $this->siteElement->disableAdvancedSearch();
        return $this;
    }

    public function setRadioModSite(bool $radioMode = true)
    {
        $this->siteElement->enableRadioMode($radioMode);
        return $this;
    }

    public function setRadioModBuilding(bool $radioMode = true)
    {
        $this->buildingElement->enableRadioMode($radioMode);
        return $this;
    }

    public function setRadioModBlock(bool $radioMode = true)
    {
        $this->blockElement->enableRadioMode($radioMode);
        return $this;
    }

    public function setRadioModRoom(bool $radioMode = true)
    {
        $this->roomElement->enableRadioMode($radioMode);
        return $this;
    }

    public function setRadioModZone(bool $radioMode = true)
    {
        $this->zoneElement->enableRadioMode($radioMode);
        return $this;
    }

    public function setRadioModExternal(bool $radioMode = true)
    {
        $this->externalElement->enableRadioMode($radioMode);
        return $this;
    }

    public function setRadioModAllLevel()
    {
        $this->setRadioModSite();
        if ($this->buildingElement instanceof DropDownElement) {
            $this->setRadioModBuilding();
        }
        if ($this->blockElement instanceof DropDownElement) {
            $this->setRadioModBlock();
        }
        if ($this->roomElement instanceof DropDownElement) {
            $this->setRadioModRoom();
        }
        if ($this->zoneElement instanceof DropDownElement) {
            $this->setRadioModZone();
        }
        if ($this->externalElement instanceof DropDownElement) {
            $this->setRadioModExternal();
        }
        return $this;
    }

    public function addLimitByProject($projectId)
    {
        $this->addDataLookupFilter(self::LIMIT_BY_PROJECT_ID, $projectId);
        return $this;
    }

    public function addLimitByType($limitByType, $limitById)
    {
        $this->addDataLookupFilter(self::LIMIT_BY_TYPE, $limitByType);
        $this->addDataLookupFilter(self::LIMIT_BY_ID, $limitById);
        return $this;
    }

    public function addLimitByCpContractId($cpContractId)
    {
        $this->addDataLookupFilter(self::LIMIT_BY_CP_CONTRACT_ID, $cpContractId);
        return $this;
    }

    protected function addDataLookupFilter($key, $value)
    {
        $params = [$key => $value];
        $this->siteElement->addDataLookupFilter($key, $value);
        if ($siteAdvancedURL = $this->siteElement->getAdvancedSearchURL()) {
            $this->siteElement->setAdvancedSearchURL($siteAdvancedURL . TfHttp::buildParams($params));
        }

        if ($this->buildingElement instanceof DropDownElement) {
            $this->buildingElement->addDataLookupFilter($key, $value);
            if ($buildingAdvancedURL = $this->buildingElement->getAdvancedSearchURL()) {
                $this->buildingElement->setAdvancedSearchURL($buildingAdvancedURL . TfHttp::buildParams($params));
            }
        }

        if ($this->roomElement instanceof DropDownElement) {
            $this->roomElement->addDataLookupFilter($key, $value);
            if ($roomAdvancedURL = $this->roomElement->getAdvancedSearchURL()) {
                $this->roomElement->setAdvancedSearchURL($roomAdvancedURL . TfHttp::buildParams($params));
            }
        }
        return $this;
    }

    private function pushElement(Element $element)
    {
        $this->elements[] = $element;
    }

    private function pushSiteChildren(string $fieldName)
    {
        $this->siteChildren[] = $fieldName;
    }

    private function initSiteElement()
    {
        $this->siteElement = new SitePickerElement();
        $this->siteElement->setGroup(self::LOCATION_PICKER_GROUP_NAME)
            ->setGroupLabel(ucfirst(self::LOCATION_PICKER_GROUP_NAME));
        $this->pushElement($this->siteElement);
    }

    private function initBuildingElement()
    {
        $this->buildingElement = new DropDownApiElement(self::BUILDING_ID_FIELD, 'Building', [
            'valueFieldName' => self::BUILDING_ID_FIELD,
            'textFieldName' => ['building_code', 'building_desc']
        ]);
        $this->buildingElement->setGroup(self::LOCATION_PICKER_GROUP_NAME)
            ->setApi(sprintf(self::BUILDING_API, self::SITE_ID_FIELD))
            ->setParent(self::SITE_ID_FIELD)->showOperator(false)
            ->setAdvancedSearchURL(self::BUILDING_ADVANCED_SEARCH_URL)
            ->setAdvancedSearchTitle(self::BUILDING_ADVANCED_SEARCH_TITLE)
        ;
        $this->pushElement($this->buildingElement);
    }

    private function initRoomElement($incBlock = false)
    {
        $this->roomElement = new DropDownApiElement(self::ROOM_ID_FIELD, 'Room', [
            'valueFieldName' => self::ROOM_ID_FIELD,
            'textFieldName' => ['room_number', 'room_desc']
        ]);
        $this->roomElement->setGroup(self::LOCATION_PICKER_GROUP_NAME)
            ->setApi(sprintf(self::ROOM_API, self::SITE_ID_FIELD, self::BUILDING_ID_FIELD))
            ->showOperator(false)
            ->setAdvancedSearchURL(self::ROOM_ADVANCED_SEARCH_URL)
            ->setAdvancedSearchTitle(self::ROOM_ADVANCED_SEARCH_TITLE);
        if ($incBlock) {
            $this->roomElement->setParent(self::BUILDING_BLOCK_ID_FIELD);
            $this->blockElement->setChild(self::ROOM_ID_FIELD);
        } else {
            $this->roomElement->setParent(self::BUILDING_ID_FIELD);
            $this->buildingElement->setChild(self::ROOM_ID_FIELD);
        }

        $this->pushElement($this->roomElement);
    }

    private function initBuildingBlockElement()
    {
        $this->blockElement = new DropDownApiElement(self::BUILDING_BLOCK_ID_FIELD, 'Block', [
            'valueFieldName' => self::BUILDING_BLOCK_ID_FIELD,
            'textFieldName' => ['building_block_code', 'building_block_desc']
        ]);
        $this->blockElement->setGroup(self::LOCATION_PICKER_GROUP_NAME)
            ->setApi(sprintf(self::BUILDING_BLOCK_API, self::SITE_ID_FIELD, self::BUILDING_ID_FIELD))
            ->setParent(self::BUILDING_ID_FIELD)->showOperator(false);
        $this->buildingElement->setChild(self::BUILDING_BLOCK_ID_FIELD);
        $this->pushElement($this->blockElement);
    }

    private function initPropZoneElement()
    {
        $this->zoneElement = new DropDownApiElement(self::PROP_ZONE_ID_FIELD, 'Zone', [
            'valueFieldName' => self::PROP_ZONE_ID_FIELD,
            'textFieldName' => ['prop_zone_code', 'prop_zone_desc']
        ]);
        $this->zoneElement->setGroup(self::LOCATION_PICKER_GROUP_NAME)
            ->setApi(sprintf(self::PROP_ZONE_API, self::SITE_ID_FIELD))
            ->setParent(self::SITE_ID_FIELD)->showOperator(false);
        $this->pushElement($this->zoneElement);
    }

    private function initPropExtElement()
    {
        $this->zoneElement->setChild(self::PROP_EXT_ID_FIELD);
        $this->externalElement = new DropDownApiElement(self::PROP_EXT_ID_FIELD, 'External Area', [
            'valueFieldName' => self::PROP_EXT_ID_FIELD,
            'textFieldName' => ['prop_external_code', 'prop_external_desc']
        ]);
        $this->externalElement->setGroup(self::LOCATION_PICKER_GROUP_NAME)
            ->setApi(sprintf(self::PROP_EXT_API, self::PROP_ZONE_ID_FIELD))
            ->setParent(self::PROP_ZONE_ID_FIELD)->showOperator(false);
        $this->pushElement($this->externalElement);
    }

    private function setupElements()
    {
        $this->initSiteElement();
        if ($this->maxLevel > 1) {
            $this->initBuildingElement();
            $this->pushSiteChildren(self::BUILDING_ID_FIELD);

            if ($this->maxLevel >= 3) {
                if ($this->incBlock) {
                    $this->initBuildingBlockElement();
                    if ($this->maxLevel === 4) {
                        $this->initRoomElement($this->incBlock);
                    }
                } else {
                    $this->initRoomElement();
                }
            }

            if ($this->incZone === true) {
                $this->initPropZoneElement();
                if ($this->incExternal === true) {
                    $this->initPropExtElement();
                }
                $this->pushSiteChildren(self::PROP_ZONE_ID_FIELD);
            }
        }
        $this->siteElement->setChild($this->siteChildren);
    }
}
