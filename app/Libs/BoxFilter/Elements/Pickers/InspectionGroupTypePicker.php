<?php

namespace App\Libs\BoxFilter\Elements\Pickers;

use App\Libs\BoxFilter\Constants\OperatorTypeConstant;
use App\Libs\BoxFilter\Elements\DropDownApiElement;
use App\Libs\BoxFilter\Elements\DropDownElement;
use App\Libs\BoxFilter\Elements\Element;
use App\Libs\Common;
use Tfcloud\Models\InspectionSFG20\InspSFG20GroupLevel1;

class InspectionGroupTypePicker
{
    private const INSPECTION_GROUP_TYPE = 'inspection_group_type';
    private array $elements = [];

    public function __construct($type, $notAppliedFilter = true)
    {
        $this->setupElements($notAppliedFilter, $type);
    }

    private function setupElements($notAppliedFilter, $type)
    {
        $inactiveFilters = Common::valueYorNtoBoolean(\Auth::User()->show_inactive_codes);
        $typeElement =  new DropDownElement('insp_sfg20_type_id', 'Type', $type, [
            'valueFieldName' => 'insp_sfg20_type_id',
            'textFieldName' => ['insp_sfg20_type_code']
        ]);
        $typeElement
            ->setNotAppliedFilter($notAppliedFilter)
            ->setChild('insp_sfg20_type_version_id')
            ->setGroup(self::INSPECTION_GROUP_TYPE)
            ->setOperators([OperatorTypeConstant::INCLUDE])
            ->setDefaultOperator(OperatorTypeConstant::INCLUDE)
            ->showOperator(false);
        $this->pushElement($typeElement);

        $versionElement = new DropDownApiElement('insp_sfg20_type_version_id', 'Version', [
            'valueFieldName' => 'insp_sfg20_type_version_id',
            'textFieldName' => ['insp_sfg20_type_version_code_desc']
        ]);
        $versionElement->setGroup('insp_sfg20_type_version_id')
            ->setApi(
                '/api/inspection/inspection-types/{insp_sfg20_type_id}/version'
            )
            ->setParent('insp_sfg20_type_version_id')
            ->setGroup(self::INSPECTION_GROUP_TYPE)
            ->setOperators([OperatorTypeConstant::INCLUDE])
            ->setDefaultOperator(OperatorTypeConstant::INCLUDE)
            ->showOperator(false);
        $this->pushElement($versionElement);

        $groupLv1 = (new InspSFG20GroupLevel1())->lookupList([
            'incInactiveId' => null,
            'incInactive' => $inactiveFilters,
            'return_query' => true
        ])->select(['insp_sfg20_group_level_1_id as id', 'insp_sfg20_group_level_1_code as text'])->get();

        $groupElement = new InspectionGroupPickerElement(
            "Group 1",
            1,
            $groupLv1->toArray(),
            ['childGroup' => 'insp_sfg20_group_level_2_id', 'apiParam' => 'groupLevel1Id=']
        );
        $groupElement->setGroup(self::INSPECTION_GROUP_TYPE);
        $this->pushElement($groupElement);

        for ($i = 2; $i <= 5; $i++) {
            $groupElement = new InspectionGroupPickerElement(
                "Group {$i}",
                $i,
                [],
                [
                    'childGroup' => 'insp_sfg20_group_level_' . $i + 1 . '_id',
                    'apiParam' => "groupLevel{$i}Id="
                ]
            );
            $groupElement->setGroup(self::INSPECTION_GROUP_TYPE);
            $this->pushElement($groupElement);
        }
    }

    public function getElements()
    {
        return $this->elements;
    }

    private function pushElement(Element $element)
    {
        $this->elements[] = $element;
        return $this;
    }
}
