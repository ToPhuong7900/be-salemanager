<?php

namespace App\Libs\BoxFilter\Elements\Pickers;

use App\Libs\BoxFilter\Elements\PickerElement;

class StockIssuePickerElement extends PickerElement
{
    protected string $api = '/api/stock/stock-issues';

    protected string $valueFiledName = 'stock_issue_id';

    protected array $textFiledNames = ['stock_issue_code', 'job'];

    /**
     * ProjectPickerElement constructor.
     * @param string $name Element filter name to submit for query.
     * @param string $label Element label text to display.
     * @param array $dataLookupFilters
     * @param array $options Options of element
     */
    public function __construct(
        string $name = 'stock_issue_id',
        string $label = 'Job',
        array $dataLookupFilters = [],
        $options = []
    ) {
        parent::__construct($name, $label, $options);
        $this->setDataLookupFilters($dataLookupFilters);
    }
}
