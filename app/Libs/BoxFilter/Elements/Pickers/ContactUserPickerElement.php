<?php

namespace App\Libs\BoxFilter\Elements\Pickers;

use App\Libs\BoxFilter\Elements\PickerElement;

class ContactUserPickerElement extends PickerElement
{
    protected string $api = '/api/all-contact-user';

    protected string $valueFiledName = 'record_id';

    protected array $textFiledNames = ['record_code', 'record_organisation'];

    protected $iconField = 'image';

    /**
     * ContactPickerElement constructor.
     * @param string $name Element filter name to submit for query.
     * @param string $label Element label text to display.
     * @param array $dataLookupFilters
     * @param array $options Options of element
     */
    public function __construct(string $name, string $label, array $dataLookupFilters = [], $options = [])
    {
        $options = array_merge($options, ['iconField' => $this->iconField]);
        parent::__construct($name, $label, $options);
        $this->setDataLookupFilters($dataLookupFilters);
    }

    public function setIconField($iconField)
    {
        $this->iconField = $iconField;
        return $this;
    }
}
