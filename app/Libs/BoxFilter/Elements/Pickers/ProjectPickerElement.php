<?php

namespace App\Libs\BoxFilter\Elements\Pickers;

use App\Libs\BoxFilter\Elements\PickerElement;

class ProjectPickerElement extends PickerElement
{
    protected string $api = '/api/project/projects';

    protected string $valueFiledName = 'project_id';

    protected array $textFiledNames = ['project_code', 'project_title'];

    /**
     * ProjectPickerElement constructor.
     * @param string $name Element filter name to submit for query.
     * @param string $label Element label text to display.
     * @param array $dataLookupFilters
     * @param array $options Options of element
     */
    public function __construct(string $name, string $label, array $dataLookupFilters = [], $options = [])
    {
        parent::__construct($name, $label, $options);
        $this->setDataLookupFilters($dataLookupFilters);
    }
}
