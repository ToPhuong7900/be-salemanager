<?php

namespace App\Libs\BoxFilter\Elements\Pickers;

use App\Libs\BoxFilter\Elements\PickerElement;

class EstateUnitPickerElement extends PickerElement
{
    protected string $api = '/api/estate/letting/estate-units';

    protected string $valueFiledName = 'lettable_unit_id';

    protected array $textFiledNames = ['lettable_unit_code', 'lettable_unit_desc'];

    /**
     * EstateUnitPickerElement constructor.
     * @param string $name Element filter name to submit for query.
     * @param string $label Element label text to display.
     * @param array $dataLookupFilters
     * @param array $options Options of element
     */
    public function __construct(string $name, string $label, array $dataLookupFilters = [], $options = [])
    {
        parent::__construct($name, $label, $options);
        $this->setDataLookupFilters($dataLookupFilters);
    }
}
