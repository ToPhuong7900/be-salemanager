<?php

namespace App\Libs\BoxFilter\Elements\Pickers;

use App\Libs\BoxFilter\Constants\OperatorTypeConstant;
use App\Libs\BoxFilter\Elements\PickerElement;

class SitePickerElement extends PickerElement
{
    protected $operatorTypes = [
        OperatorTypeConstant::INCLUDE,
        OperatorTypeConstant::NOT_INCLUDE
    ];

    protected string $fieldName = 'site_id';

    protected string $api = '/api/property/sites';

    protected ?string $advancedSearchURL = '/modals/site-box-filter';

    protected ?string $advancedSearchTitle = 'Site';

    protected string $valueFiledName = 'site_id';

    protected array $textFiledNames = ['site_code', 'site_desc'];

    /**
     * SitePickerElement constructor.
     * @param string $label Element label text to display.
     * @param array $dataLookupFilters
     * @param array $options Options of element
     */
    public function __construct(string $label = 'Site', array $dataLookupFilters = [], $options = [])
    {
        parent::__construct($this->fieldName, $label, $options);
        $this->setDataLookupFilters($dataLookupFilters);
    }
}
