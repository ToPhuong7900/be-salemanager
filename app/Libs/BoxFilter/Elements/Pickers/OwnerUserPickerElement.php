<?php

namespace App\Libs\BoxFilter\Elements\Pickers;

use App\Libs\BoxFilter\Constants\ElementTypeConstant;
use App\Libs\BoxFilter\Elements\Pickers\UserPickerElement;

class OwnerUserPickerElement extends UserPickerElement
{
    public const UNASSIGNED_USER_ID = -1;

    /**
     * UserPickerElement constructor.
     * @param string $name Element filter name to submit for query.
     * @param string $label Element label text to display.
     * @param array $dataLookupFilters
     * @param array $options Options of element
     */
    public function __construct(string $name, string $label, array $dataLookupFilters = [], $options = [])
    {
        parent::__construct($name, $label, $dataLookupFilters, $options);
        $this->setDataLookupFilters($dataLookupFilters);
        $this->setType(ElementTypeConstant::USER_PICKER);
        $this->setDefaultValue(self::UNASSIGNED_USER_ID);
    }

    public static function formatValue($values): array
    {
        $formattedValues = [];
        foreach ($values as $key => $value) {
            if ((int)$value === self::UNASSIGNED_USER_ID) {
                $formattedValues[] = \Auth::user()->id;
                continue;
            }
            $formattedValues[] = $value;
        }
        return array_unique($formattedValues);
    }
}
