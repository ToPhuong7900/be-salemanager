<?php

namespace App\Libs\BoxFilter\Elements\Pickers;

use App\Libs\BoxFilter\Constants\OperatorTypeConstant;
use App\Libs\BoxFilter\Elements\PickerElement;

class InspectionGroupPickerElement extends PickerElement
{
    protected $operatorTypes = [
        OperatorTypeConstant::INCLUDE
    ];

    protected array $textFiledNames = [];

    /**
     * SitePickerElement constructor.
     * @param string $label Element label text to display.
     * @param array $dataLookupFilters
     * @param array $options Options of element
     */
    public function __construct(
        string $label = 'Group 1',
        string $level = '1',
        array $dataLookupFilters = [],
        $options = []
    ) {
        $this->valueFiledName = $fieldName = "insp_sfg20_group_level_{$level}_id";
        $this->api = "/api/inspection/inspection-types/get-group-level?groupLevel{$level}Id=";
        $this->textFiledNames = [
            "insp_sfg20_group_level_{$level}_code",
            "insp_sfg20_group_level_{$level}_desc"
        ];
        parent::__construct($fieldName, $label, $options);
        $this->setDataLookupFilters($dataLookupFilters);
    }
}
