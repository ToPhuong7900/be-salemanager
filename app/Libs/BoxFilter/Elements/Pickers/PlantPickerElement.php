<?php

namespace App\Libs\BoxFilter\Elements\Pickers;

use App\Libs\BoxFilter\Elements\PickerElement;

class PlantPickerElement extends PickerElement
{
    protected string $api = '/api/property/plants';

    protected string $valueFiledName = 'plant_id';

    protected array $textFiledNames = ['plant_code', 'plant_desc'];

    /**
     * PlantPickerElement constructor.
     * @param string $name Element filter name to submit for query.
     * @param string $label Element label text to display.
     * @param array $dataLookupFilters
     * @param array $options Options of element
     */
    public function __construct(string $name, string $label, array $dataLookupFilters = [], $options = [])
    {
        parent::__construct($name, $label, $options);
        $this->setDataLookupFilters($dataLookupFilters);
    }
}
