<?php

namespace App\Libs\BoxFilter\Elements\Pickers;

use App\Libs\BoxFilter\Elements\PickerElement;

class DLOJobPickerElement extends PickerElement
{
    protected string $api = '/api/dlo/dlo-jobs';

    protected string $valueFiledName = 'dlo_job_id';

    protected array $textFiledNames = ['dlo_job_code', 'dlo_job_desc'];

    /**
     * ProjectPickerElement constructor.
     * @param string $name Element filter name to submit for query.
     * @param string $label Element label text to display.
     * @param array $dataLookupFilters
     * @param array $options Options of element
     */
    public function __construct(
        string $name = 'dlo_job_id',
        string $label = 'DLO Job',
        array $dataLookupFilters = [],
        $options = []
    ) {
        parent::__construct($name, $label, $options);
        $this->setDataLookupFilters($dataLookupFilters);
    }
}
