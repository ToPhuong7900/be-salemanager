<?php

namespace App\Libs\BoxFilter\Elements\Pickers;

use App\Libs\BoxFilter\Elements\PickerElement;

class CostPlusPickerElement extends PickerElement
{
    protected string $api = '/api/cost-plus/cp-contracts';

    protected string $valueFiledName = 'cp_contract_id';

    protected array $textFiledNames = ['cp_contract_code', 'cp_contract_desc'];

    /**
     * ProjectPickerElement constructor.
     * @param string $name Element filter name to submit for query.
     * @param string $label Element label text to display.
     * @param array $dataLookupFilters
     * @param array $options Options of element
     */
    public function __construct(
        string $name = 'cp_contract_id',
        string $label = 'Cost Plus Contract',
        array $dataLookupFilters = [],
        $options = []
    ) {
        parent::__construct($name, $label, $options);
        $this->setDataLookupFilters($dataLookupFilters);
    }
}
