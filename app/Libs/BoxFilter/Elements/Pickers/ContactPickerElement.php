<?php

namespace App\Libs\BoxFilter\Elements\Pickers;

use App\Libs\BoxFilter\Elements\PickerElement;

class ContactPickerElement extends PickerElement
{
    protected string $api = '/api/contacts';

    protected string $valueFiledName = 'contact_id';

    protected array $textFiledNames = ['contact_name', 'organisation'];

    /**
     * ContactPickerElement constructor.
     * @param string $name Element filter name to submit for query.
     * @param string $label Element label text to display.
     * @param array $dataLookupFilters
     * @param array $options Options of element
     */
    public function __construct(string $name, string $label, array $dataLookupFilters = [], $options = [])
    {
        parent::__construct($name, $label, $options);
        $this->setDataLookupFilters($dataLookupFilters);
    }
}
