<?php

namespace App\Libs\BoxFilter\Elements\Pickers;

use App\Libs\BoxFilter\Elements\PickerElement;

class AccountCodePickerElement extends PickerElement
{
    protected string $api = '/api/account-code';

    protected string $valueFiledName = 'fin_account_id';

    protected array $textFiledNames = ['fin_account_code', 'fin_account_desc'];

    /**
     * ProjectPickerElement constructor.
     * @param string $name Element filter name to submit for query.
     * @param string $label Element label text to display.
     * @param array $dataLookupFilters
     * @param array $options Options of element
     */
    public function __construct(string $name, string $label, array $dataLookupFilters = [], $options = [])
    {
        parent::__construct($name, $label, $options);
        $this->setDataLookupFilters($dataLookupFilters);
    }
}
