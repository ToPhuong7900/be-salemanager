<?php

namespace App\Libs\BoxFilter\Elements;

use App\Libs\BoxFilter\Constants\ElementTypeConstant;
use App\Libs\BoxFilter\Constants\OperatorTypeConstant;

class NumberElement extends Element
{
    /**
     * NumberElement constructor.
     * @param string $name
     * @param string $label
     * @param array $options
     */
    public function __construct(string $name, string $label, $options = [])
    {
        parent::__construct($name, $label, ElementTypeConstant::NUMBER, [OperatorTypeConstant::MATCH], [], $options);
        $this->setDefaultOperator(OperatorTypeConstant::MATCH)->showOperator(false);
    }
}
