<?php

namespace App\Libs\BoxFilter\Elements;

use App\Libs\BoxFilter\Elements\Configuration\IDataConfig;
use App\Libs\BoxFilter\Helper\IOptionConfig;

interface IElement
{
    /**
     * Get the name of element which maps with a field in the database table
     * @return string
     */
    public function getName();

    /**
     * Get the label of element which is displayed in the filter list
     * @return string
     */
    public function getLabel();

    /**
     * Get the element type which is defined in app/lib/BoxFilter/Constants/ElementTypeConstant.php
     * @param string $type
     * @return IElement
     */
    public function setType(string $type);

    /**
     * Get the element type
     * @return string
     */
    public function getType();

    /**
     * Set the group value which is used for cascading filter element such as Plant Group
     * @param int|string|null $group
     * @return IElement
     */
    public function setGroup($group);

    /**
     * Get the group value
     * @return int|string|null
     */
    public function getGroup();

    /**
     * Set group label to display on filter list such as 'Plant Group' or 'Location'
     * Only use for cascading filter element such as Plan Group or Location filter
     * @param string|null $groupLabel
     * @return IElement
     */
    public function setGroupLabel(?string $groupLabel);

    /**
     * Get the group label
     * @return string|null
     */
    public function getGroupLabel();

    /**
     * Set Operator list that is available for this element
     * @param array $operators
     * @return IElement
     */
    public function setOperators(array $operators);

    /**
     * Get Operator list
     * @return array
     */
    public function getOperators();

    /**
     * Add operator item to the operator list
     * @param int[] ...$operators
     * @return IElement
     */
    public function addOperator(...$operators);

    /**
     * Remove operator item to the operator list
     * @param int[] ...$operators
     * @return IElement
     */
    public function removeOperator(...$operators);

    /**
     * Set a default value of the element when it is displayed on the filter applied
     * @param string|int|array $value
     * @return IElement
     */
    public function setDefaultValue($value);

    /**
     * Get default value of element
     * @return mixed
     */
    public function getDefaultValue();

    /**
     * Keep the element always display on filter applied, there is no (x) icon for this element.
     * @param bool $value
     * @return IElement
     */
    public function setKeepAlive(bool $value = true);

    /**
     * Make the element to display as not applied filter state on the filter applied
     * @param bool $value
     * @return IElement
     */
    public function setNotAppliedFilter(bool $value = true);

    /**
     * @return bool
     */
    public function getNotAppliedFilter();

    /**
     * @return array
     */
    public function getOptions();

    /**
     * @return mixed
     */
    public function getKeepAlive();

    /**
     * Make the element to display as disable mode which doesn't allow to change its value.
     * @param bool $value
     * @return IElement
     */
    public function setDisabled(bool $value = true);

    /**
     * @return mixed
     */
    public function getDisabled();

    /**
     * Allow to show or not show the filter type section of an element, it is showed by default.
     * @param bool $isShow
     * @return IElement
     */
    public function showOperator(bool $isShow);

    /**
     * Set the default filter type of a element, it will be selected when the element is selected on the first time
     * @param int $operator
     * @return IElement
     */
    public function setDefaultOperator(int $operator);

    /**
     * @param string $parent
     * @return IElement
     */
    public function setParent(string $parent);

    /**
     * @param string|string[] $child
     * @return IElement
     */
    public function setChild($child);

    /**
     * Get element data
     * @return array
     */
    public function getElement();

    /**
     * @param IDataConfig|array $data Data structure to define selection list for kind of drop down element types.
     * @return IElement
     */
    public function setData($data);

    /**
     * @param IOptionConfig|array $options Option items to config element filter.
     * @return IElement
     */
    public function setOptions($options);
}
