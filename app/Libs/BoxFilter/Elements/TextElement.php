<?php

namespace App\Libs\BoxFilter\Elements;

use App\Libs\BoxFilter\Constants\ElementTypeConstant;
use App\Libs\BoxFilter\Constants\OperatorTypeConstant;

class TextElement extends Element
{
    protected $defaultTextOptions = [
        'barcodeMode' => false,
    ];

    /**
     * TextElement constructor.
     * @param string $name
     * @param string $label
     * @param array $options
     */
    public function __construct(string $name, string $label, $options = [])
    {
        parent::__construct($name, $label, ElementTypeConstant::TEXT, [
            OperatorTypeConstant::CONTAIN,
            OperatorTypeConstant::NOT_CONTAIN,
            OperatorTypeConstant::MATCH,
            OperatorTypeConstant::NOT_MATCH,
            OperatorTypeConstant::BLANK,
            OperatorTypeConstant::NOT_BLANK,
        ], [], $options);
        $this->options->addOptions($this->defaultTextOptions);
        $this->setDefaultOperator(OperatorTypeConstant::CONTAIN);
    }

    public function setOperatorsForRequiredField()
    {
        $operators = OperatorTypeConstant::getRequiredTextFieldOperators();
        if (in_array(OperatorTypeConstant::MULTIPLE_VALUE, $this->getOperators())) {
            $operators[] = OperatorTypeConstant::MULTIPLE_VALUE;
        }
        $this->setOperators($operators);
        return $this;
    }

    public function allowMultipleMode()
    {
        if (!in_array(OperatorTypeConstant::MULTIPLE_VALUE, $this->operators)) {
            $this->addOperator(OperatorTypeConstant::MULTIPLE_VALUE);
        }
        return $this;
    }

    public function disableMultipleMode()
    {
        if (in_array(OperatorTypeConstant::MULTIPLE_VALUE, $this->operators)) {
            $this->removeOperator(OperatorTypeConstant::MULTIPLE_VALUE);
        }
        return $this;
    }

    public function setBarcodeMode(bool $on = true)
    {
        $this->options->setOption('barcodeMode', $on);
        return $this;
    }
}
