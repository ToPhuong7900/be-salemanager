<?php

namespace App\Libs\BoxFilter\Elements;

use App\Libs\BoxFilter\Constants\ElementTypeConstant;
use App\Libs\TfHttp;
use App\SBoxFilter\General\ModalBoxFilterList;

class DropDownApiElement extends DropDownElement
{
    public const DATA_LOOKUP_FILTERS = 'dataLookupFilters';
    private const ADVANCED_SEARCH_URL_KEY = 'advancedSearchURL';
    private const ADVANCED_SEARCH_TITLE_KEY = 'advancedSearchTitle';

    /**
     * Advanced search modal URl
     * @example modals/site-box-filter
     * @var string|null
     */
    protected ?string $advancedSearchURL = null;

    /**
     * @var string|null
     */
    protected ?string $advancedSearchTitle = null;

    /**
     * DropDownApiElement constructor.
     * @param string $name Element filter name to submit for query.
     * @param string $label Element label text to display.
     * @param array $dataConfig
     * @param array $options Options of element
     */
    public function __construct(string $name, string $label, $dataConfig = [], $options = [])
    {
        parent::__construct($name, $label, [], $dataConfig, $options);
        $this->setType(ElementTypeConstant::DROP_DOWN_API);
        $this->addAdvancedSearchURL();
        $this->addAdvancedSearchTitle();
    }

    /**
     * @param array $dataLookupFilters
     * @return $this
     */
    public function setDataLookupFilters(array $dataLookupFilters)
    {
        $this->data->setOption(self::DATA_LOOKUP_FILTERS, $dataLookupFilters);
        return $this;
    }

    public function addDataLookupFilter(string $key, $value)
    {
        $this->data->setOption(
            self::DATA_LOOKUP_FILTERS,
            array_merge($this->data->getOption(self::DATA_LOOKUP_FILTERS), [$key => $value])
        );
        return $this;
    }

    public function getDataLookupFilter()
    {
        return $this->data->getOption(self::DATA_LOOKUP_FILTERS);
    }

    /**
     * @param string $api
     * @return $this
     */
    public function setApi(string $api)
    {
        $this->data->setOption('api', $api);
        return $this;
    }

    public function getElement()
    {
        $this->addParamsToAdvancedSearch();
        $this->encodeDataLookupFilters();
        return parent::getElement();
    }

    public function disableAdvancedSearch()
    {
        $this->options->setOption(self::ADVANCED_SEARCH_URL_KEY, null);
        $this->options->setOption(self::ADVANCED_SEARCH_TITLE_KEY, null);
        return $this;
    }

    public function setAdvancedSearchURL(string $url)
    {
        $this->options->setOption(self::ADVANCED_SEARCH_URL_KEY, $url);
        return $this;
    }

    public function getAdvancedSearchURL()
    {
        return $this->options->getOption(self::ADVANCED_SEARCH_URL_KEY);
    }

    public function setAdvancedSearchTitle(string $title)
    {
        $this->options->setOption(self::ADVANCED_SEARCH_TITLE_KEY, $title);
        return $this;
    }

    /**
     * @return $this
     */
    protected function encodeDataLookupFilters()
    {
        $this->data->setOption(
            self::DATA_LOOKUP_FILTERS,
            json_encode($this->data->getOption(self::DATA_LOOKUP_FILTERS))
        );
        return $this;
    }

    /**
     * @return $this
     */
    protected function addParamsToAdvancedSearch()
    {
        if ($advancedSearchURL = $this->options->getOption(self::ADVANCED_SEARCH_URL_KEY)) {
            $params = [];
            if ($radioMode = $this->options->getOption(ModalBoxFilterList::MODAL_RADIO_MODE_KEY)) {
                $params[ModalBoxFilterList::MODAL_RADIO_MODE_KEY] = $radioMode;
            }
            if (!empty($params)) {
                $this->options->setOption(
                    self::ADVANCED_SEARCH_URL_KEY,
                    $advancedSearchURL . TfHttp::buildParams($params)
                );
            }
        }

        return $this;
    }

    /**
     * Enable advanced search of a picker, provide the URL to open the popup.
     * @return IElement
     */
    private function addAdvancedSearchURL()
    {
        $this->options->addOption(self::ADVANCED_SEARCH_URL_KEY, $this->advancedSearchURL);
        return $this;
    }

    /**
     * Enable advanced search of a picker, provide the URL to open the popup.
     * @return IElement
     */
    private function addAdvancedSearchTitle()
    {
        $this->options->addOption(self::ADVANCED_SEARCH_TITLE_KEY, $this->advancedSearchTitle);
        return $this;
    }
}
