<?php

namespace App\Libs\BoxFilter\Elements;

use App\Libs\BoxFilter\Constants\ElementTypeConstant;

abstract class PickerElement extends DropDownApiElement
{
    protected const QUICK_SEARCH_API_OPTION = 'quickSearchAPI';

    /**
     * @var string
     */
    protected string $api = '';

    /**
     * @var string
     */
    protected string $valueFiledName = '';

    /**
     * @var array
     */
    protected array $textFiledNames = [];

    /**
     * PickerElement constructor.
     * @param string $name Element filter name to submit for query.
     * @param string $label Element label text to display.
     * @param array $options Options of element
     */
    public function __construct(string $name, string $label, $options = [])
    {
        parent::__construct($name, $label, [], $options);
        $this->setType(ElementTypeConstant::PICKER);
        $this->setApi($this->api)->setValueFieldName($this->valueFiledName)->setTextFieldNames($this->textFiledNames);
    }

    public function setQuickSearchURL($quickSearchAPI)
    {
        $this->options->addOption(self::QUICK_SEARCH_API_OPTION, $quickSearchAPI);
    }
}
