<?php

namespace App\Libs\BoxFilter\Elements;

use App\Libs\BoxFilter\Constants\ElementTypeConstant;
use App\Libs\BoxFilter\Constants\OperatorTypeConstant;

class NumberRangeElement extends Element
{
    /**
     * NumberRangeElement constructor.
     * @param string $name
     * @param string $label
     * @param array $options
     */
    public function __construct(string $name, string $label, $options = [])
    {
        parent::__construct($name, $label, ElementTypeConstant::NUMBER_RANGE, [
            OperatorTypeConstant::INCLUDE_RANGE
        ], [], $options);
        $this->setDefaultOperator(OperatorTypeConstant::INCLUDE_RANGE)->showOperator(false);
    }
}
