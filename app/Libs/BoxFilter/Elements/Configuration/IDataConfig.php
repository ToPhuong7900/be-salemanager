<?php

namespace App\Libs\BoxFilter\Elements\Configuration;

use Illuminate\Support\Collection;
use App\Libs\BoxFilter\Helper\IOptionConfig;

interface IDataConfig extends IOptionConfig
{
    /**
     * Retrieve data structure
     * @example ['values' => [], 'configs' => []]
     * @return array
     */
    public function getData();

    /**
     * Set values for data
     * @param array|Collection $values
     * @return $this
     */
    public function setDataValues($values);

    /**
     * Get values
     * @return array
     */
    public function getDataValues();
}
