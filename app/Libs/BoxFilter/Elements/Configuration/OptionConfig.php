<?php

namespace App\Libs\BoxFilter\Elements\Configuration;

use App\Libs\BoxFilter\Helper\IOptionConfig;
use App\Libs\BoxFilter\Helper\OptionConfigTrait;

class OptionConfig implements IOptionConfig
{
    use OptionConfigTrait;

    /**
     * Holds default settings for element filter.
     * They should be always presented for every element type even it will not be used.
     * @var array
     * [
     *      'showOperator' => (bool) A flag to allow display operator list.
     *      'defaultValue' => (string|int) A default value of element.
     *                          @example 'CND000000X'. If element filter is a text box field.
     *                          @example 15. The valude of the selection If element filter is a drop down.
     *      'defaultOperator' => (int) A default operator. @example OperatorTypeConstant::CONTAIN.
     * ]
     */
    protected $defaultOptions = [
        'showOperator' => true,
        'defaultValue' => null,
        'defaultOperator' => null,
        'keepAlive' => false,
        'disabled' => false,
        'notAppliedFilter' => false,
    ];

    public function __construct($options = [])
    {
        $this->options = collect(array_merge($this->defaultOptions, $options));
    }
}
