<?php

namespace App\Libs\BoxFilter\Elements\Configuration;

use Illuminate\Support\Collection;
use App\Libs\BoxFilter\Helper\OptionConfigTrait;

class DataConfig implements IDataConfig
{
    use OptionConfigTrait;

    /**
     * Holds default configuration settings for data.
     * They should be always presented for every element type even it will not be used.
     * @var array
     * [
     *      'valueFieldName' => (string) Field name of _id field in $values. @example 'project_status_id'.
     *      'textFieldName' => (string[]) Field names of _code/_desc in $values. @example 'project_status_code'.
     *      'textGlueSymbol' => (string) A symbol that is used to connect each text field. @example '-'.
     *      'maxLength' => (int) A limited max length of text field could have. @example 50.
     *                      If the text is longer the maxLength, it is truncated and '...' is appended.
     *      'maxLengthBackward' => (bool) If it is true the '...' is appended at first. @example true
     *      'api' => (string) API endpoint to dynamically load data for $values. @example 'api/plan/planTypes'.
     *      'parent' => (string) A element name of parent element, using for DROP_DOWN_CASCADING element.
     *      'child' => (string|string[]) A element name of child element, using for DROP_DOWN_CASCADING element.
     *      'dataLookupFilters' => (json string) A parameter lookup for picker. @example {"supplier": 1, "isFilter": 1}
     * ]
     */
    protected $defaultOptions = [
        'valueFieldName' => null, //string
        'textFieldName' => null, //string[]
        'textGlueSymbol' => ' - ', //string
        'recordCssFieldName' => null, //css style for each record
        'maxLength' => null, //int
        'maxLengthBackward' => false, //bool
        'api' => null, //string
        'parent' => null, //string
        'child' => null, //string|string[]
        'dataLookupFilters' => [], //json string
    ];

    /**
     * @var array
     */
    protected $values = [];

    public function __construct($values, array $options = [])
    {
        $this->options = collect(array_merge($this->defaultOptions, $options));
        $this->setDataValues($values);
    }

    public function setDataValues($values)
    {
        $this->values = $values instanceof Collection ? $values->toArray() : $values;
        return $this;
    }

    public function getDataValues()
    {
        return $this->values;
    }

    public function getData()
    {
        return [
            'values' => $this->values,
            'configs' => $this->getOptions()
        ];
    }
}
