<?php

namespace App\Libs\BoxFilter\Elements;

use App\Libs\BoxFilter\Elements\Configuration\DataConfig;
use App\Libs\BoxFilter\Elements\Configuration\IDataConfig;
use App\Libs\BoxFilter\Elements\Configuration\OptionConfig;
use App\Libs\BoxFilter\Helper\IOptionConfig;

class Element implements IElement
{
    /**
     * @var string Should contain a element name that match with a column name of table in DB
     * @example "project_code"
     */
    protected $name;

    /**
     * @var string Should contain a element type that is supported
     * @example App\Libs\BoxFilter\Constants\ElementTypeConstant::ELEMENT_TYPE_TEXT
     */
    protected $type;

    /**
     * @var string Should contain a element label to display on the form
     * @example "Project Code"
     */
    protected $label;

    /**
     * @var array Should contain one of App\Libs\BoxFilter\Constants\OperatorTypeConstant
     * @example [App\Libs\BoxFilter\Constants\OperatorTypeConstant::CONTAIN]
     */
    protected $operators;

    /**
     * @var null|string|int To define cascading elements which are in the same group
     */
    protected $group = null;

    /**
     * @var null|string To define a label for a group of cascading elements which is displayed in the fitler list
     */
    protected $groupLabel = null;

    /**
     * @var IDataConfig
     */
    protected $data;

    /**
     * @var IOptionConfig
     */
    protected $options;

    protected array $secondOperators = [];
    protected array $dateFieldColumns = [];

    /**
     * Element constructor.
     * @param string $name Element filter name to submit for query.
     * @param string $label Element label to display.
     * @param string $type Element Type.
     * @param array $operators List of operator type.
     * @param IDataConfig|array $data Data structure to define selection list for kind of drop down element types.
     * @param IOptionConfig|array $options Option items to config element filter.
     */
    public function __construct(
        string $name,
        string $label,
        string $type,
        array $operators = [],
        $data = [],
        $options = []
    ) {
        $this->name = $name;
        $this->label = $label;
        $this->type = $type;
        $this->operators = $operators;
        $this->setData($data);
        $this->setOptions($options);
    }

    public function getName()
    {
        return $this->name;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setLabel(string $label)
    {
        return $this->label = $label;
    }

    public function setType(string $type)
    {
        $this->type = $type;
        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getGroup()
    {
        return $this->group;
    }

    public function setGroup($group)
    {
        $this->group = $group;
        return $this;
    }

    public function getGroupLabel()
    {
        return $this->groupLabel;
    }

    public function setGroupLabel(?string $groupLabel)
    {
        $this->groupLabel = $groupLabel;
        return $this;
    }

    public function setOperators(array $operators)
    {
        $this->operators = $operators;
        return $this;
    }

    public function getOperators()
    {
        return $this->operators;
    }

    public function setSecondOperators(array $operators): static
    {
        $this->secondOperators = $operators;
        return $this;
    }
    public function getSecondOperators(): array
    {
        return $this->secondOperators;
    }

    public function addOperator(...$operators)
    {
        foreach ($operators as $operator) {
            $this->operators[] = $operator;
        }
        return $this;
    }

    public function setDefaultValue($value)
    {
        $this->options->setOption('defaultValue', $value);
        return $this;
    }

    public function getDefaultValue()
    {
        return $this->options->getOption('defaultValue');
    }

    public function setKeepAlive(bool $value = true)
    {
        $this->options->setOption('keepAlive', $value);
        return $this;
    }

    public function getKeepAlive()
    {
        return $this->options->getOption('keepAlive');
    }

    public function setDisabled(bool $value = true)
    {
        $this->options->setOption('disabled', $value);
        return $this;
    }

    public function getDisabled()
    {
        return $this->options->getOption('disabled');
    }

    public function showOperator(bool $isShow)
    {
        $this->options->setOption('showOperator', $isShow);
        return $this;
    }

    public function setDefaultOperator(int $operator)
    {
        $this->options->setOption('defaultOperator', $operator);
        return $this;
    }

    public function setDefaultSecondOperator(int $operator)
    {
        $this->options->setOption('defaultSecondOperator', $operator);
        return $this;
    }

    public function setParent(string $parent)
    {
        $this->data->setOption('parent', $parent);
        return $this;
    }

    public function setChild($child)
    {
        if (is_array($child) && count($child) === 1) {
            $child = $child[0];
        }
        $this->data->setOption('child', $child);
        return $this;
    }

    public function getElement()
    {
        $element = [
            'name' => $this->getName(),
            'type' => $this->getType(),
            'label' => $this->getLabel(),
            'group' => $this->getGroup(),
            'groupLabel' => $this->getGroupLabel(),
            'operators' => $this->getOperators(),
            'data' => $this->data->getData(),
            'options' => $this->options->getOptions()
        ];
        if (!empty($this->dateFieldColumns) && !empty($this->secondOperators)) {
            $element['dateFieldColumns'] = $this->dateFieldColumns;
            $element['secondOperators'] = $this->getSecondOperators();
            $element['isShowDateFiledMode'] = true;
        }
        if (empty($this->dateFieldColumns)) {
            $element['isShowDateFiledMode'] = false;
        }
        return $element;
    }

    public function setData($data)
    {
        $this->data = $data instanceof IDataConfig ? $data : new DataConfig(
            is_array($data) ? $data : []
        );
        return $this;
    }

    public function setDateFieldColumns($dateFieldColumns)
    {
        usort($dateFieldColumns, fn($a, $b) => ($a['text'] ?? '') <=> ($b['text'] ?? ''));
        $this->dateFieldColumns = $dateFieldColumns;
        return $this;
    }

    public function setOptions($options)
    {
        $this->options = $options instanceof IOptionConfig ? $options : new OptionConfig(
            is_array($options) ? $options : []
        );
        return $this;
    }

    public function setNotAppliedFilter(bool $value = true)
    {
        $this->options->setOption('notAppliedFilter', $value);
        return $this;
    }

    public function setNotAppliedDateFiledMode(bool $value = true)
    {
        $this->options->setOption('notAppliedDateFiledMode', $value);
        return $this;
    }

    public function getNotAppliedFilter()
    {
        return $this->options->getOption('notAppliedFilter');
    }

    public function getOptions()
    {
        return $this->options->getOptions();
    }

    public function setCustomOption($key, $value)
    {
        $this->options->addOption($key, $value);
        return $this;
    }

    public function removeOperator(...$operators)
    {
        $this->setOperators(array_diff($this->getOperators(), $operators));
        return $this;
    }
}
