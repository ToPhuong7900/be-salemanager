<?php

namespace App\Libs\BoxFilter\Elements;

use App\Libs\BoxFilter\Constants\ElementTypeConstant;
use App\Libs\BoxFilter\Constants\OperatorTypeConstant;
use App\Libs\BoxFilter\Elements\Configuration\DataConfig;

class DateRangeDropDownElement extends Element
{
    public const TODAY = 1;
    public const YESTERDAY = 2;
    public const TOMORROW = 3;
    public const BEFORE_TODAY = 4;
    public const AFTER_TODAY = 5;
    public const LAST_7_DAYS = 6;
    public const LAST_14_DAYS = 7;
    public const LAST_21_DAYS = 8;
    public const LAST_28_DAYS = 9;
    public const LAST_31_DAYS = 10;
    public const LAST_FULL_WEEK = 11;
    public const LAST_FULL_MONTH = 12;
    public const LAST_3_MONTHS = 13;
    public const LAST_6_MONTHS = 14;
    public const LAST_12_MONTHS = 15;
    public const LAST_FULL_YEAR = 16;
    public const NEXT_7_DAYS = 17;
    public const NEXT_14_DAYS = 18;
    public const NEXT_21_DAYS = 19;
    public const NEXT_31_DAYS = 20;
    public const NEXT_FULL_WEEK = 21;
    public const NEXT_FULL_MONTH = 22;
    public const NEXT_3_MONTHS = 23;
    public const NEXT_6_MONTHS = 24;
    public const NEXT_12_MONTHS = 25;
    public const NEXT_FULL_YEAR = 26;
    public const TWENTY_FOUR_HOURS = 27;
    public const SEVEN_DAYS = 28;
    public const FOUR_WEEKS = 29;
    public const THREE_MONTHS = 30;
    public const SIX_MONTHS = 31;
    public const TWELVE_MONTHS = 32;
    public const LAST_30_DAYS = 33;
    private array $defaultDataLookups = [
        [
            'id' => self::TODAY,
            'text' => 'Today',
        ],
        [
            'id' => self::YESTERDAY,
            'text' => 'Yesterday',
        ],
        [
            'id' => self::TOMORROW,
            'text' => 'Tomorrow',
        ],
        [
            'id' => self::BEFORE_TODAY,
            'text' => 'Before today',
        ],
        [
            'id' => self::AFTER_TODAY,
            'text' => 'After today',
        ],
        [
            'id' => self::LAST_7_DAYS,
            'text' => 'Last 7 days',
        ],
        [
            'id' => self::LAST_14_DAYS,
            'text' => 'Last 14 days',
        ],
        [
            'id' => self::LAST_21_DAYS,
            'text' => 'Last 21 days',
        ],
        [
            'id' => self::LAST_31_DAYS,
            'text' => 'Last 31 days',
        ],
        [
            'id' => self::LAST_FULL_WEEK,
            'text' => 'Last Full Week',
        ],
        [
            'id' => self::LAST_FULL_MONTH,
            'text' => 'Last Full Month',
        ],
        [
            'id' => self::FOUR_WEEKS,
            'text' => 'Last 4 weeks',
        ],
        [
            'id' => self::LAST_3_MONTHS,
            'text' => 'Last 3 months',
        ],
        [
            'id' => self::LAST_6_MONTHS,
            'text' => 'Last 6 months',
        ],
        [
            'id' => self::LAST_12_MONTHS,
            'text' => 'Last 12 months',
        ],
        [
            'id' => self::LAST_FULL_YEAR,
            'text' => 'Last Full Year',
        ],
        [
            'id' => self::NEXT_7_DAYS,
            'text' => 'Next 7 days',
        ],
        [
            'id' => self::NEXT_14_DAYS,
            'text' => 'Next 14 days',
        ],
        [
            'id' => self::NEXT_21_DAYS,
            'text' => 'Next 21 days',
        ],
        [
            'id' => self::NEXT_31_DAYS,
            'text' => 'Next 31 days',
        ],
        [
            'id' => self::NEXT_FULL_WEEK,
            'text' => 'Next Full Week',
        ],
        [
            'id' => self::NEXT_FULL_MONTH,
            'text' => 'Next Full Month',
        ],
        [
            'id' => self::NEXT_3_MONTHS,
            'text' => 'Next 3 months',
        ],
        [
            'id' => self::NEXT_6_MONTHS,
            'text' => 'Next 6 months',
        ],
        [
            'id' => self::NEXT_12_MONTHS,
            'text' => 'Next 12 months',
        ],
        [
            'id' => self::NEXT_FULL_YEAR,
            'text' => 'Next Full Year',
        ],
    ];

    public function __construct(string $name, string $label, array $dataLookup = [], $requiredOperator = false)
    {
        parent::__construct(
            $name,
            $label,
            ElementTypeConstant::DATE_FIELD,
            $requiredOperator ?
                [OperatorTypeConstant::INCLUDE_RANGE]
                : [OperatorTypeConstant::INCLUDE_RANGE, OperatorTypeConstant::BLANK],
            new DataConfig(empty($dataLookup) ? $this->defaultDataLookups : $dataLookup, [
                'valueFieldName' => 'id',
                'textFieldName' => ['text']
            ])
        );
        $this->setSecondOperators([
            OperatorTypeConstant::AFTER,
            OperatorTypeConstant::BEFORE,
            OperatorTypeConstant::EQUAL,
            OperatorTypeConstant::AFTER_OR_EQUAL,
            OperatorTypeConstant::BEFORE_OR_EQUAL,
        ]);
        $this->options->addOption('defaultSecondOperator', OperatorTypeConstant::AFTER);
        $this->setDefaultOperator(OperatorTypeConstant::INCLUDE_RANGE);
    }
}
