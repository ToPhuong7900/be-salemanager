<?php

namespace App\Libs\BoxFilter\Elements;

use Illuminate\Support\Collection;
use App\Libs\BoxFilter\Constants\ElementTypeConstant;
use App\Libs\BoxFilter\Constants\OperatorTypeConstant;

class RadioDropDownElement extends DropDownElement
{
    protected $yesNoDataLookups = [
        ['value' => 'Y', 'text' => 'Yes'],
        ['value' => 'N', 'text' => 'No'],
    ];

    protected $yesDataLookups = [
        ['value' => 'Y', 'text' => 'Yes']
    ];

    protected $dataConfig = [
        'valueFieldName' => 'value',
        'textFieldName' => ['text']
    ];

    /**
     * RadioDropDownElement constructor.
     * @param string $name Element filter name to submit for query.
     * @param string $label Element label text to display.
     * @param array $options Options of element
     */
    public function __construct(string $name, string $label, $options = [])
    {
        parent::__construct($name, $label, [], $this->dataConfig, $options);
        $this->showValueSection(false)
            ->setDefaultOperator(OperatorTypeConstant::MATCH)
            ->showCheckedItemOnTop(false)
            ->enableRadioMode(true);
    }

    public function setDataLookUps($dataLookups)
    {
        $this->data->setDataValues($dataLookups);
        return $this;
    }

    public function buildYesNoElement()
    {
        $dataLookups = empty($this->data->getDataValues()) ? $this->yesNoDataLookups : $this->data->getDataValues();
        $this->setDataLookUps($dataLookups);
        $this->showOperator(false);
        return $this;
    }

    public function buildYesElement()
    {
        $dataLookups = empty($this->data->getDataValues()) ? $this->yesDataLookups : $this->data->getDataValues();
        $this->setDataLookUps($dataLookups);
        $this->showOperator(false);
        return $this;
    }
}
