<?php

namespace App\Libs\BoxFilter\Elements;

use App\Libs\BoxFilter\Constants\ElementTypeConstant;
use App\Libs\BoxFilter\Constants\OperatorTypeConstant;

class SingleTimeElement extends Element
{
    /**
     * SingleDateElement constructor.
     * @param string $name
     * @param string $label
     * @param array $options
     */
    public function __construct(string $name, string $label, $options = [])
    {
        parent::__construct($name, $label, ElementTypeConstant::SINGLE_TIME, [
            OperatorTypeConstant::MATCH,
            OperatorTypeConstant::NOT_MATCH,
            OperatorTypeConstant::BLANK,
            OperatorTypeConstant::NOT_BLANK,
        ], [], $options);
        $this->setDefaultOperator(OperatorTypeConstant::MATCH);
    }
}
