<?php

namespace App\Libs\BoxFilter;

use App\Libs\BoxFilter\ReportOptions\BaseReport;
use App\Libs\BoxFilter\ReportOptions\IBaseReport;
use App\Libs\Constant\CommonConstant;
use App\Libs\Exceptions\PermissionException;
use Tfcloud\Models\CrudAccessLevel;
use Tfcloud\Models\Report;
use Tfcloud\Services\Report\ReportQueueService;
use Tfcloud\Services\Report\ReportService;

abstract class BoxFilterReport extends BoxFilter implements IBoxFilterReport
{
    private const SYSTEM_REPORT_CSV_XLSX = 1;
    private const SYSTEM_REPORT_WKHTMLPDF = 2;
    private const SYSTEM_REPORT_PHP_EXCEL = 3;
    private const IS_FROM_REPORT = 'isFromReport';

    /**
     * @var Report
     */
    private $report;

    /**
     * @var integer
     */
    private $reportStage;

    /**
     * @var IBoxComponent|IBaseReport
     */
    protected $baseReport;

    protected $isFromReport = true;

    public function __construct(int $filterNameId, Report $report)
    {
        parent::__construct($filterNameId);
        if (!$this->checkReportPermission()) {
            throw new PermissionException();
        }
        $this->setReport($report)->setBaseReport($this->initBaseReport())->initReportStage();
        $this->setBoxComponent($this->baseReport);
    }

    public function generate()
    {
        $boxSettings = parent::generate();
        $boxSettings[self::IS_FROM_REPORT] = $this->isFromReport;
        return $boxSettings;
    }

    public function setReport(Report $report)
    {
        $this->report = $report;
        return $this;
    }

    public function getReport()
    {
        return $this->report;
    }

    public function setBaseReport(IBaseReport $baseReport)
    {
        $this->baseReport = $baseReport;
        return $this;
    }

    protected function setExportAsWKHTMLPDF()
    {
        $this->baseReport->addExportAsOption('PDF', 'generateWkhtmltopdf');
    }

    protected function setExportAsPHPExcel()
    {
        $this->baseReport->addExportAsOption('XLSX', 'generatePhpExcel');
    }

    protected function setExportAsCSV()
    {
        $this->baseReport->addExportAsOption('CSV', 'generate', ['output_option' => 1]);
    }

    protected function setExportAsXLSX()
    {
        $this->baseReport->addExportAsOption('XLSX', 'generate', ['output_option' => 2]);
    }

    protected function setExportAsWPDF()
    {
        $this->baseReport->addExportAsOption('PDF', 'generate', ['output_option' => 1]);
    }

    /**
     * @return IBaseReport
     */
    protected function initBaseReport()
    {
        $baseReport = new BaseReport();
        if (\Auth::User()->siteGroup->enableRunBackGroundReport()) {
            if (
                \Auth::User()->hasModuleAccess(
                    $this->report->getRepModuleId(),
                    CrudAccessLevel::CRUD_ACCESS_LEVEL_CREATE
                )
            ) {
                $baseReport->setAllowRunInBackground(true);
                $baseReport->setExistRunningReport(ReportQueueService::existRunningReport());
            }
        }
        return $baseReport;
    }

    private function initReportStage()
    {
        $this->baseReport
            ->setSystemReportId($this->report->getSystemReportId())
            ->setUserReportId($this->report->getRepReportId())
            ->setReportCode($this->report->getRepCode())
            ->setReportTitle($this->report->getRepTitle())
            ->setReportType($this->report->getRepType());

        $includeReturnedData = true;
        if (in_array($this->report->getRepCode(), ReportService::getWkHtmlToPdfWhiteList())) {
            $this->reportStage = self::SYSTEM_REPORT_WKHTMLPDF;
            $includeReturnedData = false;
            $this->setExportAsWKHTMLPDF();
        } elseif (in_array($this->report->getRepCode(), ReportService::getExcelWhiteList())) {
            $this->reportStage = self::SYSTEM_REPORT_PHP_EXCEL;
            $includeReturnedData = false;
            $this->setExportAsPHPExcel();
        } else {
            $this->reportStage = self::SYSTEM_REPORT_CSV_XLSX;
            $isQueryReport = $this->report->getRepUserReportUseQuery() == CommonConstant::DATABASE_VALUE_YES;
            $isCustomCSVReport = in_array($this->report->getRepCode(), ReportService::getCustomizeCsvWhiteList());
            if ($isQueryReport || $isCustomCSVReport) {
                $includeReturnedData = false;
            }
            if ($isQueryReport) {
                $this->baseReport->setShowFilter(false);
            }
            if ($this->report->getRepUserReportWpdf() == CommonConstant::DATABASE_VALUE_YES) {
                $includeReturnedData = true;
                $this->setExportAsWPDF();
            } else {
                $this->setExportAsCSV();
                $this->setExportAsXLSX();
            }
        }
        $this->baseReport->setShowReturnedData($includeReturnedData);
        return $this;
    }

    protected function checkReportPermission()
    {
        return true;
    }
}
