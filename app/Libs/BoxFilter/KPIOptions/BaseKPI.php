<?php

namespace App\Libs\BoxFilter\KPIOptions;

use Illuminate\Support\Str;

class BaseKPI implements IBaseKPI
{
    private const BOX_COMPONENT_NAME = 'baseKPI';
    private const KPI_SETTINGS = 'kpiSettings';
    private const IS_GROUPED = 'isGrouped';

    protected $isGrouped = false;
    /**
     * @var array
     */
    protected $kpiSettings = [];

    public function __construct(array $displayKPIs)
    {
        $this->setKpiSettings($displayKPIs);
    }

    public function generate()
    {
        return [
            self::KPI_SETTINGS => $this->kpiSettings,
            self::IS_GROUPED => $this->isGrouped,
        ];
    }

    public function getComponentName()
    {
        return self::BOX_COMPONENT_NAME;
    }

    public function setKpiSettings(array $displayKPIs)
    {
        $this->kpiSettings = $this->formatDisplayKPIs($displayKPIs);
        return $this;
    }

    /**
     * @param array $displayKPIs
     * @return array
     */
    protected function formatDisplayKPIs(array $displayKPIs)
    {
        $formatted = [];
        foreach ($displayKPIs as $kpiID => $settings) {
            if (is_string($kpiID)) {
                $key = Str::camel($kpiID);
                $formatted[$key] = $settings;
            }
        }
        return $formatted;
    }
}
