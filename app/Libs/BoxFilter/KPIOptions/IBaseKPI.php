<?php

namespace App\Libs\BoxFilter\KPIOptions;

use App\Libs\BoxFilter\IBoxComponent;

interface IBaseKPI extends IBoxComponent
{
    /**
     * @param array $displayKPIs
     * @return $this
     */
    public function setKpiSettings(array $displayKPIs);
}
