<?php

namespace App\Libs\BoxFilter;

use App\Libs\BoxFilter\Elements\IElement;
use App\Libs\BoxFilterQuery\FilterFieldSet\IFilterSet;

interface IBoxFilter
{
    /**
     * @return string
     */
    public function getFilterNameId();

    /**
     * @return int
     */
    public function getModuleId();

    /**
     * @param int $moduleId
     * @return IBoxFilter
     */
    public function setModuleId(int $moduleId);

    /**
     * @return array
     */
    public function generate();

    /**
     * @return IFilterSet
     */
    public function getFilterSet();

    /**
     * @return IElement[]
     */
    public function getElements();

    /**
     * @param IElement $element
     * @return IBoxFilter
     */
    public function addElement(IElement $element);

    /**
     * @param IElement[] $elements
     * @return IBoxFilter
     */
    public function addElements(array $elements);

    /**
     * @param array $filterLookups
     * @return IBoxFilter
     */
    public function setFilterLookups(array $filterLookups);

    /**
     * @return array
     */
    public function getFilterLookups();

    /**
     * @param IElement $element
     * @return IBoxFilter
     */
    public function addBlindElement(IElement $element);

    /**
     * @return IElement[]
     */
    public function getBlindElements();

    /**
     * Setup filter elements
     * @return IBoxFilter
     */
    public function setFilterElements();
}
