<?php

namespace App\Libs\BoxFilter;

use App\Libs\BoxFilter\KPIOptions\IBaseKPI;

interface IBoxFilterKPI
{
    /**
     * @param array $displayKPIs
     * @return $this
     */
    public function setDisplayKPIs(array $displayKPIs);

    /**
     * @param IBaseKPI $baseKPI
     * @return $this
     */
    public function setBaseKPI(IBaseKPI $baseKPI);
}
