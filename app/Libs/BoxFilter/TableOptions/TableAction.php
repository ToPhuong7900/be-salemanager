<?php

namespace App\Libs\BoxFilter\TableOptions;

class TableAction
{
    /**
     * @var string
     */
    private $label;

    /**
     * @var string[]
     */
    private $set;

    /**
     * @var string
     */
    private $col;

    public function __construct(string $label, array $set, string $col)
    {
        $this->label = $label;
        $this->set = $set;
        $this->col = $col;
    }

    public function toArray()
    {
        return [
            'label' => $this->label,
            'set' => $this->set,
            'col' => $this->col,
        ];
    }
}
