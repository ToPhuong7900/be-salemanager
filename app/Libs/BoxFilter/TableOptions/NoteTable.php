<?php

namespace App\Libs\BoxFilter\TableOptions;

use App\Libs\BoxFilter\IBoxComponent;

class NoteTable implements IBoxComponent
{
    private const BOX_COMPONENT_NAME = 'noteList';

    public function getComponentName()
    {
        return self::BOX_COMPONENT_NAME;
    }

    public function generate()
    {
        return [];
    }
}
