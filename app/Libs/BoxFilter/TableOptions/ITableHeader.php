<?php

namespace App\Libs\BoxFilter\TableOptions;

interface ITableHeader
{
    /**
     * @param bool $sortHeader
     * @return array
     */
    public function getHeaders(bool $sortHeader = false);

    /**
     * @param array $userColVisible
     * @return ITableHeader
     */
    public function applyUserSetting(array $userColVisible);

    /**
     * @return string|null
     */
    public function getTableIconPath();

    /**
     * @param string|null $tableIconPath
     * @return ITableHeader
     */
    public function setTableIconPath(?string $tableIconPath);

    /**
     * @return string
     */
    public function getTableTitle();

    /**
     * @param string $tableTitle
     * @return ITableHeader
     */
    public function setTableTitle(string $tableTitle);

    /**
     * @param string $fieldName
     * @return ITableHeader
     */
    public function setDefaultSortField(string $fieldName);

    /**
     * @return string|null
     */
    public function getDefaultSortField();

    /**
     * @param string $defaultSortType
     * @return ITableHeader
     */
    public function setDefaultSortType(string $defaultSortType);

    /**
     * @return string
     */
    public function getDefaultSortType();

    /**
     * @param Header $header
     * @return ITableHeader
     */
    public function addHeader(Header $header);

    /**
     * @param Header[] $headers
     * @return ITableHeader
     */
    public function addHeaders(array $headers);
}
