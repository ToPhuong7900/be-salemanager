<?php

namespace App\Libs\BoxFilter\TableOptions;

interface ITableFooter
{
    /**
     * @return array
     */
    public function generate();

    /**
     * @param string $title
     * @param string[] $cssClass
     * @return ITableFooter
     */
    public function setTitle(string $title, array $cssClass = []);

    /**
     * @param string[] $cssClass
     * @return ITableFooter
     */
    public function setCssClass(array $cssClass);

    /**
     * @return string[]
     */
    public function getCssClass();

    /**
     * @param FooterField $footerField
     * @return ITableFooter
     */
    public function addFooterField(FooterField $footerField);

    /**
     * @param FooterField[] $footerFields
     * @return ITableFooter
     */
    public function setFooterFields(array $footerFields);

    /**
     * @return FooterField[]
     */
    public function getFooterFields();

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @return string
     */
    public function getKeyRow();

    /**
     * @param string $keyRow
     * @return TableFooter
     */
    public function setKeyRow(string $keyRow);
}
