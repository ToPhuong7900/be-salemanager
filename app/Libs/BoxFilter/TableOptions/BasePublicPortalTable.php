<?php

namespace App\Libs\BoxFilter\TableOptions;

use Illuminate\Support\Collection;
use App\Libs\BoxFilter\Helper\IOptionConfig;
use App\Libs\BoxFilter\Helper\OptionConfigTrait;
use App\Libs\Constant\CommonConstant;
use Tfcloud\Models\Filter\FilterTableHeaderUser;

class BasePublicPortalTable implements IBaseTable, IOptionConfig
{
    use OptionConfigTrait;

    public const DEFAULT_TABLE_NAME = 'tfTable';

    protected const BOX_COMPONENT_NAME = 'baseTable';
    protected const TABLE_HEADERS_STR = 'tableHeader';
    protected const TABLE_DEFAULT_HEADERS_STR = 'tableDefaultHeader';
    protected const TABLE_FOOTERS_STR = 'tableFooters';
    protected const TABLE_ACTION_SET_STR = 'tableActionSet';
    protected const TABLE_HEADERS_TITLE_STR = 'tableHeaderTitle';
    protected const TABLE_DEFAULT_SORT_STR = 'tableDefaultSort';
    protected const TABLE_DEFAULT_SORT_FIELD_STR = 'fieldName';
    protected const TABLE_DEFAULT_SORT_TYPE_STR = 'type';
    protected const TABLE_HEADERS_ICON_PATH = 'tableHeaderIcon';
    protected const IS_WRAP_TABLE_ROWS_STR = 'wrapTableRows';
    protected const DEFAULT_USER_PER_PAGE_STR = 'defaultUserPerPage';
    protected const TABLE_FORM = 'tableForm';
    protected const TABLE_NAME = 'tableName';
    protected const EXPORT_REPORT_OPTIONS = 'exportReportOptions';
    protected const TABLE_OPTIONS = 'tableOptions';
    protected const SHOW_PAGINATION_BAR = 'showPaginationBar';
    protected const SHOW_TABLE_OPTIONS = 'showTableOptions';

    /**
     * @var ITableHeader
     */
    protected $tableHeader;

    /**
     * @var ITableFooter[]
     */
    protected $tableFooters = [];

    /**
     * @var bool
     */
    protected $isWrapTable = true;

    /**
     * @var array
     */
    protected $userColVisible = [];

    /**
     * @var TableAction[]
     */
    protected $tableActions = [];

    /**
     * @var null|TableForm
     */
    protected $tableForm = null;

    /**
     * @var array
     */
    protected $exportAsLookup = [];

    /**
     * @var string
     */
    protected $tableName = self::DEFAULT_TABLE_NAME;

    /**
     * @var bool
     */
    protected $showPaginationBar = true;

    /**
     * Show the table options button on the table
     * @var bool
     */
    protected $showTableOptions = true;


    /**
     * BaseTable constructor.
     * @param integer $filterNameId
     * @param ITableHeader $tableHeader
     */
    public function __construct(ITableHeader $tableHeader)
    {
        $this->tableHeader = $tableHeader;
        $this->options = new Collection();
    }

    public function generate()
    {
        return [
            self::TABLE_NAME => $this->tableName,
            self::TABLE_DEFAULT_HEADERS_STR => $this->tableHeader->getHeaders(true),
            self::TABLE_HEADERS_STR => $this->tableHeader->applyUserSetting($this->userColVisible)->getHeaders(true),
            self::TABLE_HEADERS_TITLE_STR => $this->tableHeader->getTableTitle(),
            self::TABLE_DEFAULT_SORT_STR => [
                self::TABLE_DEFAULT_SORT_FIELD_STR => $this->tableHeader->getDefaultSortField(),
                self::TABLE_DEFAULT_SORT_TYPE_STR => $this->tableHeader->getDefaultSortType(),
            ],
            self::TABLE_ACTION_SET_STR => $this->generateTableActions(),
            self::TABLE_HEADERS_ICON_PATH => $this->tableHeader->getTableIconPath(),
            self::IS_WRAP_TABLE_ROWS_STR => $this->isWrapTable,
            self::TABLE_FOOTERS_STR => $this->getTableFooters(),
            self::DEFAULT_USER_PER_PAGE_STR => 12,
            self::TABLE_FORM => $this->generateTableForm(),
            self::EXPORT_REPORT_OPTIONS => $this->exportAsLookup,
            self::TABLE_OPTIONS => $this->getOptions(),
            self::SHOW_PAGINATION_BAR => $this->showPaginationBar,
            self::SHOW_TABLE_OPTIONS => $this->showTableOptions,
        ];
    }

    public function getComponentName()
    {
        return self::BOX_COMPONENT_NAME;
    }

    public function setWrapTable(bool $isWrapTable)
    {
        $this->isWrapTable = $isWrapTable;
        return $this;
    }

    public function addHeaders(array $headers)
    {
        $this->tableHeader->addHeaders($headers);
    }

    public function setTableActions(array $tableActions)
    {
        $this->tableActions = $tableActions;
        return $this;
    }

    public function setTableFooters($tableFooters)
    {
        if (is_array($tableFooters) && !empty($tableFooters) && $tableFooters[0] instanceof TableFooter) {
            $this->tableFooters = $tableFooters;
        } elseif ($tableFooters instanceof TableFooter) {
            $this->tableFooters = [$tableFooters];
        }
        return $this;
    }

    public function showPaginationBar(bool $showPaginationBar)
    {
        $this->showPaginationBar = $showPaginationBar;
        return $this;
    }

    public function showTableOptions(bool $showTableOptions)
    {
        $this->showTableOptions = $showTableOptions;
        return $this;
    }

    public function setTableForm(TableForm $tableForm)
    {
        $this->tableForm = $tableForm;
        return $this;
    }

    public function setTableName(string $name)
    {
        $this->tableName = $name;
        return $this;
    }

    public function addExportAsOption(string $label, string $api, array $params = [])
    {
        $this->exportAsLookup[] = [
            'label' => $label,
            'api' => $api,
            'params' => $params
        ];
        return $this;
    }

    public function addExportRedirectUrl(array $options = [])
    {
        foreach ($options as $option) {
            $this->exportAsLookup[] =  $option;
        }
        return $this;
    }

    protected function generateTableForm()
    {
        if ($this->tableForm instanceof TableForm) {
            return $this->tableForm->generate();
        }
        return '';
    }

    protected function generateTableActions()
    {
        return array_map(function (TableAction $tableActions) {
            return $tableActions->toArray();
        }, $this->tableActions);
    }

    protected function getTableFooters()
    {
        if (empty($this->tableFooters)) {
            return [];
        }
        $tableFooters = [];
        foreach ($this->tableFooters as $tableFooter) {
            $tableFooters[$tableFooter->getKeyRow()] = $tableFooter->generate();
        }
        return $tableFooters;
    }
}
