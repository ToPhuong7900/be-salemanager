<?php

namespace App\Libs\BoxFilter\TableOptions;

use App\Libs\BoxFilter\IBoxComponent;

interface IBaseTable extends IBoxComponent
{
    /**
     * @param bool $isWrapTable
     * @return IBaseTable
     */
    public function setWrapTable(bool $isWrapTable);

    /**
     * @param Header[] $headers
     * @return IBaseTable
     */
    public function addHeaders(array $headers);

    /**
     * @param TableAction[] $tableActions
     * @return IBaseTable
     */
    public function setTableActions(array $tableActions);

    /**
     * @param ITableFooter[]|ITableFooter $tableFooters
     * @return IBaseTable
     */
    public function setTableFooters($tableFooters);

    /**
     * @param bool $showPaginationBar
     * @return IBaseTable
     */
    public function showPaginationBar(bool $showPaginationBar);
    /**
     * @param bool $showTableOptions
     * @return IBaseTable
     */
    public function showTableOptions(bool $showTableOptions);

    /**
     * @param TableForm $tableForm
     * @return IBaseTable
     */
    public function setTableForm(TableForm $tableForm);

    /**
     * @param string $name
     * @return IBaseTable
     */
    public function setTableName(string $name);

    /**
     * @param string $label
     * @param string $api
     * @param array $params
     * @return IBaseTable
     */
    public function addExportAsOption(string $label, string $api, array $params = []);
    public function addExportRedirectUrl(array $options = []);
}
