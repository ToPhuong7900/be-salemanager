<?php

namespace App\Libs\BoxFilter\TableOptions;

interface IFooterField
{
    /**
     * @return array
     */
    public function toArray();
}
