<?php

namespace App\Libs\BoxFilter\TableOptions;

class FooterField implements IFooterField
{
    /**
     * @var string
     */
    protected $fieldName;

    /**
     * @var string[]
     */
    protected $cssClass = [];

    /**
     * @var string|null
     */
    protected $altLabel = null;

    /**
     * FooterField constructor.
     * @param string $fieldName
     * @param array $cssClass
     * @param string|null $altLabel
     */
    public function __construct(string $fieldName, array $cssClass = [], $altLabel = null)
    {
        $this->setFieldName($fieldName)->setCssClass($cssClass)->setAltLabel($altLabel);
    }

    /**
     * @return string
     */
    public function getFieldName()
    {
        return $this->fieldName;
    }

    /**
     * @param string $fieldName
     * @return FooterField
     */
    public function setFieldName(string $fieldName)
    {
        $this->fieldName = $fieldName;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getCssClass()
    {
        return $this->cssClass;
    }

    /**
     * @param string[] $cssClass
     * @return FooterField
     */
    public function setCssClass(array $cssClass)
    {
        $this->cssClass = $cssClass;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAltLabel()
    {
        return $this->altLabel;
    }

    /**
     * @param string|null $altLabel
     * @return FooterField
     */
    public function setAltLabel($altLabel)
    {
        $this->altLabel = $altLabel;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function toArray()
    {
        return [
            'fieldName' => $this->getFieldName(),
            'class' => $this->getCssClass(),
            'altLabel' => $this->getAltLabel()
        ];
    }
}
