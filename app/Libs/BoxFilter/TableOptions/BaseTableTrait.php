<?php

namespace App\Libs\BoxFilter\TableOptions;

trait BaseTableTrait
{
    /**
     * @var ITableHeader
     */
    protected $tableHeader;

    /**
     * @var ITableFooter[]
     */
    protected $tableFooters = [];

    /**
     * @var TableAction[]
     */
    protected $tableActions = [];

    /**
     * @var array
     */
    protected $exportAsLookup = [];

    /**
     * @var string
     */
    protected $tableName = self::DEFAULT_TABLE_NAME;

    /**
     * @var bool
     */
    protected $showPaginationBar = true;

    /**
     * @var bool
     */
    protected $showCaptionBar = true;

    public function addHeaders(array $headers)
    {
        $this->tableHeader->addHeaders($headers);
    }

    public function setTableActions(array $tableActions)
    {
        $this->tableActions = $tableActions;
        return $this;
    }

    public function setTableFooters($tableFooters)
    {
        if (is_array($tableFooters) && !empty($tableFooters) && $tableFooters[0] instanceof TableFooter) {
            $this->tableFooters = $tableFooters;
        } elseif ($tableFooters instanceof TableFooter) {
            $this->tableFooters = [$tableFooters];
        }
        return $this;
    }

    public function showPaginationBar(bool $showPaginationBar)
    {
        $this->showPaginationBar = $showPaginationBar;
        return $this;
    }

    public function showCaptionBar(bool $showCaptionBar)
    {
        $this->showCaptionBar = $showCaptionBar;
        return $this;
    }

    public function setTableName(string $name)
    {
        $this->tableName = $name;
        return $this;
    }

    public function addExportAsOption(string $label, string $api, array $params = [])
    {
        $this->exportAsLookup[] = [
            'label' => $label,
            'api' => $api,
            'params' => $params
        ];
        return $this;
    }

    public function addExportRedirectUrl(array $options = [])
    {
        foreach ($options as $option) {
            $this->exportAsLookup[] =  $option;
        }
    }

    protected function generateTableActions()
    {
        return array_map(function (TableAction $tableActions) {
            return $tableActions->toArray();
        }, $this->tableActions);
    }

    protected function getTableFooters()
    {
        if (empty($this->tableFooters)) {
            return [];
        }
        $tableFooters = [];
        foreach ($this->tableFooters as $tableFooter) {
            $tableFooters[$tableFooter->getKeyRow()] = $tableFooter->generate();
        }
        return $tableFooters;
    }
}
