<?php

namespace App\Libs\BoxFilter\TableOptions;

use Tfcloud\Models\Filter\FilterTableHeaderUser;

class TableHeader implements ITableHeader
{
    /**
     * @var string
     */
    protected $tableTitle;

    /**
     * @var string|null
     */
    protected $tableIconPath = null;

    /**
     * @var string
     */
    protected $defaultSortField;

    /**
     * @var string
     */
    protected $defaultSortType = 'ASC';

    /**
     * @var Header[]
     */
    protected $headers = [];

    /**
     * TableHeader constructor.
     * @param string $tableTitle table title
     * @param string $defaultSortField default sort field of this table, should map with a field name in DB
     * @param null|string $tableIconPath path of icon file for this table, it is null by default
     * @param string $defaultSortType default sort mode, it is 'ASC' by default
     */
    public function __construct(
        string $tableTitle,
        string $defaultSortField,
        $tableIconPath = null,
        $defaultSortType = 'ASC'
    ) {
        $this->setTableTitle($tableTitle)
            ->setDefaultSortField($defaultSortField)
            ->setTableIconPath($tableIconPath)
            ->setDefaultSortType($defaultSortType);
    }

    public function addHeader(Header $header)
    {
        if (!$header->getDisplayOrder()) {
            $header->setDisplayOrder(count($this->headers) + 1);
        } else {
            $this->reOrderHeaders($header->getDisplayOrder());
        }
        $this->headers[] = $header;
        return $this;
    }

    public function addHeaders(array $headers)
    {
        foreach ($headers as $header) {
            $this->addHeader($header);
        }
        return $this;
    }

    public function getHeaders(bool $sortHeader = false)
    {
        if ($sortHeader) {
            $this->sortHeaders();
        }

        $headers = [];
        foreach ($this->headers as $header) {
            $headers[] = $header->toArray();
        }

        return $headers;
    }

    /**
     * @return Header[]
     */
    public function getHeaderData(): array
    {
        return $this->headers;
    }

    public function applyUserSetting(array $userColVisible)
    {
        if (count($userColVisible)) {
            foreach ($this->headers as $header) {
                $matchedHeader = array_get($userColVisible, $header->getTitle());
                if ($matchedHeader) {
                    $header->setColumnVisible(
                        array_get($matchedHeader, FilterTableHeaderUser::COLUMN_VISIBLE_KEY, $header->isColumnVisible())
                    );
                    $header->setDisplayOrder(array_get(
                        $matchedHeader,
                        FilterTableHeaderUser::COLUMN_VISIBLE_DISPLAY_ORDER_KEY,
                        $header->getDisplayOrder()
                    ));
                }
            }
        }
        return $this;
    }

    public function getTableIconPath()
    {
        return $this->tableIconPath;
    }

    public function setTableIconPath(?string $tableIconPath)
    {
        $this->tableIconPath = $tableIconPath;
        return $this;
    }

    public function getTableTitle()
    {
        return $this->tableTitle;
    }

    public function setTableTitle(string $tableTitle)
    {
        $this->tableTitle = $tableTitle;
        return $this;
    }

    public function setDefaultSortField(string $fieldName)
    {
        $this->defaultSortField = $fieldName;
        return $this;
    }

    public function getDefaultSortField()
    {
        return $this->defaultSortField;
    }

    public function setDefaultSortType(string $defaultSortType)
    {
        $this->defaultSortType = $defaultSortType;
        return $this;
    }

    public function getDefaultSortType()
    {
        return $this->defaultSortType;
    }

    protected function sortHeaders()
    {
        usort(
            $this->headers,
            function ($a, $b) {
                return $a->getDisplayOrder() <=> $b->getDisplayOrder();
            }
        );
        foreach ($this->headers as $index => $header) {
            $header->setDisplayOrder($index + 1);
        }
    }

    protected function reOrderHeaders($displayOrder)
    {
        foreach ($this->headers as $index => $header) {
            if ($header->getDisplayOrder() >= $displayOrder) {
                $header->setDisplayOrder($header->getDisplayOrder() + 1);
            }
        }
    }
}
