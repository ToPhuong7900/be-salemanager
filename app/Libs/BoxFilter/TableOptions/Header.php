<?php

namespace App\Libs\BoxFilter\TableOptions;

use App\Libs\LandingPageResponse\WidgetThumbnail\Faker;

class Header
{
    private const TITLE = 'title';
    private const FIELD_NAME = 'fieldName';
    private const COLUMNS_VISIBLE = 'columnsVisible';
    private const SORT_FIELD_NAME = 'sortFieldName';
    private const ATTACHED_LINK = 'attachedLink';
    private const RECORD_ACTION_CODE = 'recordActionCode';
    private const CSS_CLASS = 'class';
    private const DISPLAY_ORDER = 'displayOrder';
    private const COLSPAN = 'colspan';
    private const ESCAPE_HTML = 'escapeHTML';
    private const HEADER_CSS_CLASS = 'headerClass';

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $fieldName;

    /**
     * @var null|string
     */
    private $sortFieldName;

    /**
     * @var string|null
     */
    private $recordActionCode;

    /**
     * @var array
     */
    private $cssClass;

    /**
     * @var integer
     */
    private $displayOrder;

    /**
     * @var integer
     */
    private $colspan = null;

    /**
     * @var bool
     */
    private $columnVisible;

    /**
     * @var bool
     */
    private $escapeHTML = true;

    /**
     * @var string[]
     */
    private $headerClass = [];

    private ?Faker $faker = null;

    /**
     * Header constructor.
     * @param string $title header title
     * @param string $fieldName field name of this column, it maps with data field
     * @param string|null $sortFieldName field name to sort for this column
     * @param string|null $recordActionCode  a record action code to generate URL link
     * @param array $cssClass a css class name in array to apply for this column
     * @param bool $columnVisible a option to allow show/hide this column
     * @param int $displayOrder display order of this column in table options list
     */
    public function __construct(
        string $title,
        string $fieldName,
        string $sortFieldName = null,
        string $recordActionCode = null,
        array $cssClass = [],
        bool $columnVisible = true,
        int $displayOrder = 0
    ) {
        $this->title = $title;
        $this->fieldName = $fieldName;
        $this->sortFieldName = $sortFieldName;
        $this->recordActionCode = $recordActionCode;
        $this->cssClass = $cssClass;
        $this->columnVisible = $columnVisible;
        $this->displayOrder = $displayOrder;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            self::TITLE => $this->getTitle(),
            self::FIELD_NAME => $this->getFieldName(),
            self::DISPLAY_ORDER => $this->getDisplayOrder(),
            self::COLUMNS_VISIBLE => $this->isColumnVisible(),
            self::SORT_FIELD_NAME => $this->getSortFieldName(),
            self::ATTACHED_LINK => (bool)$this->getRecordActionCode(),
            self::RECORD_ACTION_CODE => $this->getRecordActionCode(),
            self::CSS_CLASS => $this->getCssClass(),
            self::COLSPAN => $this->getColspan(),
            self::ESCAPE_HTML => $this->isEscapeHTML(),
            self::HEADER_CSS_CLASS => $this->getHeaderClass(),
        ];
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getFieldName()
    {
        return $this->fieldName;
    }

    /**
     * @return string|null
     */
    public function getSortFieldName()
    {
        return $this->sortFieldName;
    }

    /**
     * @return string
     */
    public function getRecordActionCode()
    {
        return $this->recordActionCode;
    }

    /**
     * @return array
     */
    public function getCssClass()
    {
        return $this->cssClass;
    }

    /**
     * @return bool
     */
    public function isColumnVisible()
    {
        return $this->columnVisible;
    }

    /**
     * @param bool $columnVisible
     * @return Header
     */
    public function setColumnVisible(bool $columnVisible)
    {
        $this->columnVisible = $columnVisible;
        return $this;
    }

    /**
     * @return int
     */
    public function getDisplayOrder()
    {
        return $this->displayOrder;
    }

    /**
     * @param int $displayOrder
     * @return Header
     */

    public function setDisplayOrder(int $displayOrder)
    {
        $this->displayOrder = $displayOrder;
        return $this;
    }

    public function setColspan(int $colspan)
    {
        $this->colspan = $colspan;
        return $this;
    }

    public function getColspan()
    {
        return $this->colspan;
    }

    /**
     * @return bool
     */
    public function isEscapeHTML(): bool
    {
        return $this->escapeHTML;
    }

    /**
     * @param bool $escapeHTML
     * @return Header
     */
    public function setEscapeHTML(bool $escapeHTML = false)
    {
        $this->escapeHTML = $escapeHTML;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getHeaderClass()
    {
        return $this->headerClass;
    }

    /**
     * @param string[] $headerClass
     * @return Header
     */
    public function setHeaderClass(array $headerClass)
    {
        $this->headerClass = $headerClass;
        return $this;
    }

    public function setFaker(Faker $faker)
    {
        $this->faker = $faker;
        return $this;
    }

    public function getDummy()
    {
        return $this->faker?->generate();
    }
}
