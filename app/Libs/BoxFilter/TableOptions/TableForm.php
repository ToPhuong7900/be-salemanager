<?php

namespace App\Libs\BoxFilter\TableOptions;

class TableForm
{
    private const FORM_POST_URL = 'postUrl';
    private const FORM_CANCEL_URL = 'cancelUrl';
    private const FROM_CHECKBOX_NAME = 'checkboxName';
    private const FORM_WARNING_TEXT = 'warningText';
    private const FORM_CSRF_TOKEN = 'csrfToken';
    private const FORM_ELEMENT = 'formElement';
    private const FORM_POST_LABEL = 'postLabel';
    private const FORM_INCLUDE_BOX_FILTER = 'includeBoxFilter';

    /**
     * @var string
     */
    private $postUrl;

    /**
     * @var string
     */
    private $postLabel = 'Save';

    /**
     * @var string
     */
    private $cancelUrl;

    /**
     * @var string
     */
    private $checkboxName;

    /**
     * @var string
     */
    private $warningText = '';

    /**
     * @var boolean
     */
    private $includeBoxFilter = false;

    public function __construct($checkboxName, $postUrl, $cancelUrl, $formElement = 'bulk-update-form')
    {
        $this->checkboxName = $checkboxName;
        $this->postUrl = $postUrl;
        $this->cancelUrl = $cancelUrl;
        $this->form_element = $formElement;
    }

    public function generate()
    {
        return [
            self::FROM_CHECKBOX_NAME => $this->checkboxName,
            self::FORM_POST_URL => $this->postUrl,
            self::FORM_CANCEL_URL => $this->cancelUrl,
            self::FORM_WARNING_TEXT => $this->warningText,
            self::FORM_CSRF_TOKEN => csrf_token(),
            self::FORM_ELEMENT => $this->form_element,
            self::FORM_POST_LABEL => $this->postLabel,
            self::FORM_INCLUDE_BOX_FILTER => $this->includeBoxFilter
        ];
    }

    public function setWarningText(string $text)
    {
        $this->warningText = $text;
        return $this;
    }

    public function setPostLabelButton(string $text)
    {
        $this->postLabel = $text;
        return $this;
    }

    public function setIncludeBoxFilter(bool $value = true)
    {
        $this->includeBoxFilter = $value;
        return $this;
    }
}
