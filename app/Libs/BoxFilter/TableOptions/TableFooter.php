<?php

namespace App\Libs\BoxFilter\TableOptions;

class TableFooter implements ITableFooter
{
    /**
     * @var string
     */
    protected $keyRow;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string[]
     */
    protected $cssClass;

    /**
     * @var FooterField[]
     */
    protected $footerFields = [];

    /**
     * TableHeader constructor.
     * @param string $keyRow
     * @param string $title
     * @param FooterField[] $footerFields
     * @param string[] $cssClass
     */
    public function __construct(string $keyRow, string $title, array $footerFields = [], array $cssClass = [])
    {
        $this->setKeyRow($keyRow)->setTitle($title, $cssClass)->setFooterFields($footerFields);
    }

    /**
     * @inheritDoc
     */
    public function generate()
    {
        $fields = array_map(function (FooterField $footerField) {
            return $footerField->toArray();
        }, $this->getFooterFields());
        return [
            'title' => $this->getTitle(),
            'class' => $this->getCssClass(),
            'fields' => $fields
        ];
    }

    /**
     * @inheritDoc
     */
    public function setTitle(string $title, array $cssClass = [])
    {
        $this->title = $title;
        $this->cssClass = $cssClass;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setCssClass(array $cssClass)
    {
        $this->cssClass = $cssClass;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getCssClass()
    {
        return $this->cssClass;
    }

    /**
     * @inheritDoc
     */
    public function addFooterField(FooterField $footerField)
    {
        $this->footerFields[] = $footerField;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setFooterFields(array $footerFields)
    {
        $this->footerFields = $footerFields;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getFooterFields()
    {
        return $this->footerFields;
    }

    /**
     * @inheritDoc
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @inheritDoc
     */
    public function getKeyRow()
    {
        return $this->keyRow;
    }

    /**
     * @inheritDoc
     */
    public function setKeyRow(string $keyRow)
    {
        $this->keyRow = $keyRow;
        return $this;
    }
}
