<?php

namespace App\Libs\BoxFilter\Helper;

use Illuminate\Support\Collection;
use InvalidArgumentException;

trait OptionConfigTrait
{
    /**
     * Collection options
     * @var Collection
     */
    protected $options;

    public function addOptions($options)
    {
        $this->options = $this->options->merge($options);
        return $this;
    }

    public function addOption($key, $value)
    {
        $this->options->put($key, $value);
        return $this;
    }

    public function setOption($key, $value)
    {
        if ($this->options->has($key)) {
            $this->options[$key] = $value;
        } else {
            throw new InvalidArgumentException("The '$key' is invalid.");
        }
        return $this;
    }

    public function getOptions()
    {
        return $this->options->toArray();
    }

    public function getOption(string $key)
    {
        return $this->options->get($key);
    }
}
