<?php

namespace App\Libs\BoxFilter\Helper;

use App\Libs\BoxFilter\Elements\DropDownElement;
use App\Libs\BoxFilter\Elements\Pickers\ContactPickerElement;
use App\Libs\BoxFilter\Elements\RadioDropDownElement;
use App\Libs\BoxFilter\Elements\SingleDateElement;
use App\Libs\BoxFilter\Elements\TextElement;
use App\Libs\BoxFilter\TableOptions\Header;
use Tfcloud\Models\GenTable;
use Tfcloud\Services\UserDefinedService;

class UserDefineBoxFilter
{
    private const UDF_TEXTS = [
        'user_text1' => 'user_text1_label',
        'user_text2' => 'user_text2_label',
        'user_text3' => 'user_text3_label',
        'user_text4' => 'user_text4_label',
        'user_text5' => 'user_text5_label',
        'user_text6' => 'user_text6_label',
    ];

    private const UDF_CHECKS = [
        'user_check1' => 'user_check1_label',
        'user_check2' => 'user_check2_label',
        'user_check3' => 'user_check3_label',
        'user_check4' => 'user_check4_label',
    ];

    private const UDF_DATES = [
        'user_date1' => 'user_date1_label',
        'user_date2' => 'user_date2_label',
        'user_date3' => 'user_date3_label',
        'user_date4' => 'user_date4_label',
    ];

    private const UDF_SELECT_IDS = [
        'sel1_gen_userdef_sel_id' => 'user_def_sel1',
        'sel2_gen_userdef_sel_id' => 'user_def_sel2',
        'sel3_gen_userdef_sel_id' => 'user_def_sel3',
        'sel4_gen_userdef_sel_id' => 'user_def_sel4',
    ];

    private const UDF_SELECTS = [
        'sel1_gen_userdef_sel_id' => 'user_selection1_label',
        'sel2_gen_userdef_sel_id' => 'user_selection2_label',
        'sel3_gen_userdef_sel_id' => 'user_selection3_label',
        'sel4_gen_userdef_sel_id' => 'user_selection4_label',
    ];

    private const UDF_CONTACTS = [
        'user_contact1' => 'user_contact1_label',
        'user_contact2' => 'user_contact2_label',
        'user_contact3' => 'user_contact3_label',
        'user_contact4' => 'user_contact4_label',
        'user_contact5' => 'user_contact5_label',
    ];

    private const UDF_EXTRA_CONTACTS = [
        'user_contact6' => 'user_contact6_label',
        'user_contact7' => 'user_contact7_label',
        'user_contact8' => 'user_contact8_label',
        'user_contact9' => 'user_contact9_label',
        'user_contact10' => 'user_contact10_label',
    ];
    private const UDF_MEMO = [
        'user_memo1' => 'user_memo1_label',
        'user_memo2' => 'user_memo2_label',
    ];

    public const UDF_TYPE_TEXT = 'text';
    public const UDF_TYPE_CHECK = 'check';
    public const UDF_TYPE_DATE = 'date';
    public const UDF_TYPE_SELECT = 'select';
    public const UDF_TYPE_CONTACT = 'contact';
    public const UDF_TYPE_MEMO = 'memo';

    /**
     * @var UserDefinedService
     */
    private $userDefinedService;

    /**
     * @var int
     */
    private $genTableId;

    /**
     * @var array
     */
    private $userDefine;

    /**
     * @var array
     */
    private $availableUserDefines;

    /**
     * @var string
     */
    private $userDefineTableName;

    public static function getUserDefinedSet()
    {
        return array_merge(
            self::UDF_TEXTS,
            self::UDF_CHECKS,
            self::UDF_DATES,
            self::UDF_SELECTS,
            self::UDF_CONTACTS,
            self::UDF_EXTRA_CONTACTS,
            self::UDF_MEMO,
        );
    }

    public function __construct($genTableId)
    {
        $genTable = GenTable::findOrFail($genTableId);
        $this->userDefinedService = new UserDefinedService();
        $this->userDefineTableName = $genTable->table_name;
        $this->genTableId = $genTableId;
        $this->userDefine = $this->userDefinedService->get($this->genTableId);
        $this->setAvailableUserDefines();
    }

    public function getElementsAndHeaders($udfStatus = [], $allowSingleOperatorUDFilters = false)
    {
        $isText = array_get($udfStatus, 'isText', true);
        $isCheck = array_get($udfStatus, 'isCheck', true);
        $isDate = array_get($udfStatus, 'isDate', true);
        $isSection = array_get($udfStatus, 'isSection', true);
        $isContact = array_get($udfStatus, 'isContact', true);
        $isMemo = array_get($udfStatus, 'isMemo', false);
        $udfElements = [];
        $udfHeaders = [];

        foreach ($this->availableUserDefines as $type => $userDefines) {
            switch ($type) {
                case self::UDF_TYPE_TEXT:
                    if ($isText) {
                        foreach ($userDefines as $name => $label) {
                            $udfElements[] = new TextElement($name, $label);
                            $udfHeaders[] = new Header($label, $name, $name, false, [], false);
                        }
                    }
                    break;
                case self::UDF_TYPE_CHECK:
                    if ($isCheck) {
                        foreach ($userDefines as $name => $label) {
                            $udfElements[] = (new RadioDropDownElement($name, $label))->buildYesNoElement();
                            $udfHeaders[] = new Header($label, $name, $name, false, [], false);
                        }
                    }
                    break;
                case self::UDF_TYPE_DATE:
                    if ($isDate) {
                        foreach ($userDefines as $name => $label) {
                            $udfElements[] = new SingleDateElement($name, $label);
                            $udfHeaders[] = new Header($label, $name, $name, false, [], false);
                        }
                    }
                    break;
                case self::UDF_TYPE_SELECT:
                    if ($isSection) {
                        foreach ($userDefines as $name => $label) {
                            $lookupName = array_get(self::UDF_SELECT_IDS, $name, '');
                            $udfElements[] = new DropDownElement(
                                $name,
                                $label,
                                isset($this->userDefine['lookups']["{$lookupName}Options"]) ?
                                    $this->userDefine['lookups']["{$lookupName}Options"] : [],
                                [
                                    'valueFieldName' => 'gen_userdef_sel_id',
                                    'textFieldName' => ['gen_userdef_sel_code', 'gen_userdef_sel_desc']
                                ]
                            );
                            $udfHeaders[] = new Header($label, $name, $name, false, [], false);
                        }
                    }
                    break;
                case self::UDF_TYPE_CONTACT:
                    if ($isContact) {
                        foreach ($userDefines as $name => $label) {
                            $udfElements[] = new ContactPickerElement(
                                $name,
                                $label,
                                ['isFilter' => 1]
                            );
                            $udfHeaders[] = new Header($label, $name, $name, false, [], false);
                        }
                    }
                    break;
                case self::UDF_TYPE_MEMO:
                    if ($isMemo) {
                        foreach ($userDefines as $name => $label) {
                            $udfElements[] = new TextElement($name, $label);
                            $udfHeaders[] = new Header($label, $name, $name, false, [], false);
                        }
                    }
                    break;
            }
        }

        if ($allowSingleOperatorUDFilters) {
            $udfElements = $this->setSingleOperatorUDFilters($udfElements);
        }

        return ['elements' => $udfElements, 'headers' => $udfHeaders];
    }

    protected function setSingleOperatorUDFilters(array $udfElements)
    {
        foreach ($udfElements as $element) {
            if ($element instanceof DropDownElement) {
                $element->enableRadioMode();
            } else {
                $element->showOperator(false);
            }
        }
        return $udfElements;
    }

    public function getUserDefines()
    {
        return $this->userDefine;
    }

    public function getUserDefineTableName()
    {
        return $this->userDefineTableName;
    }

    public function getAvailableUserDefines()
    {
        return $this->availableUserDefines;
    }

    private function setAvailableUserDefines()
    {
        $availableUserDefines = [];
        if (!empty($this->userDefine['visible'])) {
            foreach (self::UDF_TEXTS as $name => $label) {
                if (!empty($this->userDefine['label']->$label)) {
                    $availableUserDefines[self::UDF_TYPE_TEXT][$name] = $this->userDefine['label']->$label;
                }
            }
            foreach (self::UDF_CHECKS as $name => $label) {
                if (!empty($this->userDefine['label']->$label)) {
                    $availableUserDefines[self::UDF_TYPE_CHECK][$name] = $this->userDefine['label']->$label;
                }
            }
            foreach (self::UDF_DATES as $name => $label) {
                if (!empty($this->userDefine['label']->$label)) {
                    $availableUserDefines[self::UDF_TYPE_DATE][$name] = $this->userDefine['label']->$label;
                }
            }
            foreach (self::UDF_SELECTS as $name => $label) {
                if (!empty($this->userDefine['label']->$label)) {
                    $availableUserDefines[self::UDF_TYPE_SELECT][$name] = $this->userDefine['label']->$label;
                }
            }
            $isExtraContact = $this->userDefinedService->getGenTableService()->isExtraContactAllow($this->genTableId);
            $udfContacts = $isExtraContact ? array_merge(self::UDF_CONTACTS, self::UDF_EXTRA_CONTACTS) :
                self::UDF_CONTACTS;
            foreach ($udfContacts as $name => $label) {
                if (!empty($this->userDefine['label']->$label)) {
                    $availableUserDefines[self::UDF_TYPE_CONTACT][$name] = $this->userDefine['label']->$label;
                }
            }
            foreach (self::UDF_MEMO as $name => $label) {
                if (!empty($this->userDefine['label']->$label)) {
                    $availableUserDefines[self::UDF_TYPE_MEMO][$name] = $this->userDefine['label']->$label;
                }
            }
        }
        $this->availableUserDefines = $availableUserDefines;
    }
}
