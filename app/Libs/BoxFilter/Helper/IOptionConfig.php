<?php

namespace App\Libs\BoxFilter\Helper;

use InvalidArgumentException;

interface IOptionConfig
{
    /**
     * Retrieve option list of the element
     * @return array
     */
    public function getOptions();

    /**
     * Retrieve value of an option by giving key
     * @param string $key
     * @return mixed
     */
    public function getOption(string $key);

    /**
     * Set value of 'ShowOperator' option key.
     * @param $key
     * @param $value
     * @return $this
     * @throws InvalidArgumentException
     */
    public function setOption($key, $value);

    /**
     * Add single option to options.
     * @param string $key
     * @param mixed $value
     * @return $this
     */
    public function addOption(string $key, $value);

    /**
     * Add multi options to options
     * @param array $options
     * @return $this
     */
    public function addOptions($options);
}
