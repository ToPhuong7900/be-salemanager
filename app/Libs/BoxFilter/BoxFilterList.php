<?php

namespace App\Libs\BoxFilter;

use App\Libs\BoxFilter\Constants\RecordActionConstant;
use App\Libs\BoxFilter\TableOptions\BaseTable;
use App\Libs\BoxFilter\TableOptions\IBaseTable;
use App\Libs\BoxFilter\TableOptions\ITableHeader;
use App\Libs\BoxFilter\TableOptions\TableAction;
use App\Libs\Constant\ReportConstant;
use Tfcloud\Models\CrudAccessLevel;
use Tfcloud\Models\Module;
use Tfcloud\Models\Report;

abstract class BoxFilterList extends BoxFilter
{
    protected const EXPORT_REPORT_CSV_LABEL = 'Export CSV';
    protected const EXPORT_REPORT_XLSX_LABEL = 'Export XLSX';
    protected const EXPORT_REPORT_HTML_LABEL = 'Export HTML';
    protected const EXPORT_REPORT_API = 'export-as';
    protected const EXPORT_REPORT_ZIP_LABEL = "Download these records files as a zip";

    public const REPORT_CODE_KEY = 'repCode';
    public const REPORT_EXPORT_AS_KEY = 'output_option';
    public const REPORT_TYPE_KEY = 'repType';

    /**
     * Enable user define fields in table header setting
     * @var bool
     */
    protected $isUserDefineHeader = false;

    /**
     * @var bool
     */
    protected $enableEditAction = true;

    /**
     * @var bool
     */
    protected $enableDeleteAction = true;

    /**
     * @var bool
     */
    protected $enableCopyAction = false;

    /**
     * @var bool
     */
    protected $enableShowAction = false;

    /**
     * @var bool
     */
    protected $enableDetailsAction = false;

    /**
     * @var bool
     */
    protected $enableExtractAction = false;

    /**
     * Report Code to export the list
     * @var null|string
     */
    protected $exportAsRepCode = null;

    /**
     * Support to export as CSV file
     * @var bool
     */
    protected $exportAsCSV = true;

    /**
     * Support to export as XLSX file
     * @var bool
     */
    protected $exportAsXLSX = true;

    /**
     * Support to export as HTML file
     * @var bool
     */
    protected $exportAsHTML = false;

    /**
     * Support to export as file Zip
     * @var bool
     */
    protected $enableExportDirectUrl = false;

    /**
     * @var string
     */
    protected $actionColLabel = RecordActionConstant::ACTION_COL_LABEL_KEY;

    /**
     * @var string
     */
    protected $showColLabel = RecordActionConstant::SHOW_ACTION_LABEL;

    /**
     * @var string
     */
    protected $detailsColLabel = RecordActionConstant::DETAILS_COL_LABEL_KEY;

    /**
     * @var IBaseTable
     */
    protected $baseTable;

    public function __construct(int $filterNameId)
    {
        parent::__construct($filterNameId);
        $this->isUserDefineHeader = empty($this->genTableId) ? false : $this->isUserDefineHeader;
        $this->baseTable = $this->initBaseTable();
        $this->setBoxComponent($this->baseTable);
    }

    /**
     * @param array $options
     * @return $this
     */
    public function addTableOptions(array $options)
    {
        $this->baseTable->addOptions($options);
        return $this;
    }

    protected function getTableFooters()
    {
        return [];
    }

    /**
     * @return ITableHeader
     */
    abstract protected function getTableHeader();

    /**
     * @return TableAction[]
     */
    protected function getTableActionSet()
    {
        $tableActions = [];
        $actions = [];
        $shows = [];
        $details = [];

        if ($this->enableEditAction) {
            $actions[] =
                RecordActionConstant::EDIT_ACTION_KEY;
        }
        if ($this->enableDeleteAction) {
            $actions[] = RecordActionConstant::DELETE_ACTION_KEY;
        }
        if ($this->enableCopyAction) {
            $actions[] = RecordActionConstant::COPY_ACTION_KEY;
        }
        if ($this->enableShowAction) {
            $shows[] = RecordActionConstant::SHOW_ACTION_KEY;
        }
        if ($this->enableDetailsAction) {
            $details[] = RecordActionConstant::DETAILS_ACTION_KEY;
        }
        if ($this->enableExtractAction) {
            $actions[] = RecordActionConstant::EXTRACT_ACTION_KEY;
        }

        if (!empty($details)) {
            $tableActions[] = new TableAction($this->detailsColLabel, $details, RecordActionConstant::DETAILS_COL_KEY);
        }

        if (!empty($shows)) {
            $tableActions[] = new TableAction($this->showColLabel, $shows, RecordActionConstant::SHOW_COL_KEY);
        }

        if (!empty($actions)) {
            $tableActions[] = new TableAction(
                $this->actionColLabel,
                $actions,
                RecordActionConstant::ACTION_COL_KEY
            );
        }

        return $tableActions;
    }

    protected function initBaseTable()
    {
        $baseTable = new BaseTable($this->getFilterNameId(), $this->getTableHeader());
        $baseTable->setTableActions($this->getTableActionSet())->setTableFooters($this->getTableFooters());

        if ($this->isUserDefineHeader) {
            $elementsAndHeaders = $this->getUserDefineBoxFilter()->getElementsAndHeaders();
            $baseTable->addHeaders(array_get($elementsAndHeaders, 'headers', []));
        }

        if ($this->checkReportPermission()) {
            $params  = [
                self::REPORT_CODE_KEY => $this->exportAsRepCode,
                self::REPORT_EXPORT_AS_KEY => null,
                self::REPORT_TYPE_KEY => Report::SYSTEM_REPORT,
            ];
            $exportAPI = self::EXPORT_REPORT_API;
            if ($this->exportAsCSV) {
                $params[self::REPORT_EXPORT_AS_KEY] = 1;
                $params['fileExtension'] = ReportConstant::REPORT_OUTPUT_CSV_FORMAT;
                $baseTable->addExportAsOption(self::EXPORT_REPORT_CSV_LABEL, $exportAPI, $params);
            }
            if ($this->exportAsXLSX) {
                $params[self::REPORT_EXPORT_AS_KEY] = 2;
                $params['fileExtension'] = ReportConstant::REPORT_OUTPUT_XLSX_FORMAT;
                $baseTable->addExportAsOption(self::EXPORT_REPORT_XLSX_LABEL, $exportAPI, $params);
            }
            if ($this->exportAsHTML) {
                $params['fileExtension'] = ReportConstant::REPORT_OUTPUT_HTML_FORMAT;
                $baseTable->addExportAsOption(self::EXPORT_REPORT_HTML_LABEL, $exportAPI, $params);
            }
            if ($this->enableExportDirectUrl) {
                $options = $this->getExportDirectUrl();
                if (!empty($options)) {
                    $baseTable->addExportRedirectUrl($options);
                }
            }
        }
        return $baseTable;
    }

    protected function checkReportPermission()
    {
        return !empty($this->exportAsRepCode)
            &&
            \Auth::user()->hasModuleAccess(Module::MODULE_REPORTS, CrudAccessLevel::CRUD_ACCESS_LEVEL_READ);
    }

    protected function getExportDirectUrl()
    {
        return [];
    }
}
