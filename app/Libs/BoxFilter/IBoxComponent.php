<?php

namespace App\Libs\BoxFilter;

interface IBoxComponent
{
    /**
     * @return array
     */
    public function generate();

    /**
     * @return string
     */
    public function getComponentName();
}
