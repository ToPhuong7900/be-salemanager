<?php

namespace App\Libs\BoxFilter;

trait BoxFilterAuditTrait
{
    public function audit($orgId)
    {
        $keys = func_get_args();
        $id   = end($keys);
        $parent = $this->getAuditModel($id);
        $view = $this->getAuditView();
        $filterElements = $this->setAuditBoxFilterElements($id);
        $this->addAuditTableHeaders($filterElements);
        $pageTitle = 'View ' . $this->getAuditPageTitle() . ' History';
        $filterElements = $filterElements->setFilterLookups($this->getAuditFilterLookups($id))
            ->setFilterElements($id)
            ->generate();

        return \View::make(
            $view,
            array_merge(
                [
                    $this->getAuditModelVariableName() => $parent,
                    'filterElements' => $filterElements,
                    'titlePage' => $pageTitle,
                    'brief' => $this->getAuditBriefPath(),
                    'audit' => [
                        'showRecordType' => $this->showAuditRecordTypeColumn(),
                        'title' => $this->getAuditPageTitle(),
                        'prefix' => $this->getAuditPathPrefix(),
                        'briefFormName' => $this->getAuditBriefDetailName(),
                        'sidebarPath' => $this->getAuditSidebarPath(),
                        'showRecentItems' => false,
                    ]
                ],
                $this->getAuditSidebarData($parent)
            )
        )->with(CommonConstant::EONASDAN_DATE_PICKER_OPTION, true);
    }

    abstract protected function getAuditFilterNameId();

    /**
     * Returns the primary db table the audit enires should be shown for. e.g. AuditTable::SITE
     */
    abstract protected function getAuditTable();

    /**
     * Returns the model record the audit records relate to.  This is used when displaying the brief details
     * and sidebar.
     */
    abstract protected function getAuditModel($id);

    /**
     * Returns the page title. e.g. Site, building, etc.  This will be combined into the string 'View [title] History'
     */
    abstract protected function getAuditPageTitle();

    /**
     * Returns the model variable name used witin the sidebar and brief detail views.
     */
    abstract protected function getAuditModelVariableName();

    /**
     * Returns the root path prefix required for the sidebar and brief details e.g. 'property.site.', etc.
     */
    abstract protected function getAuditPathPrefix();

    /**
     * Returns the name of the brief detail form name.  This is used to display the brief record details.
     * Override as required.
     */
    protected function getAuditBriefDetailName()
    {
        return '_brief';
    }

    protected function getAuditBriefPath()
    {
        return $this->getAuditPathPrefix() . $this->getAuditBriefDetailName();
    }

    protected function getAuditSideBarName()
    {
        return '_edit_sidebar';
    }

    protected function getAuditSidebarPath()
    {
        if (\Auth::user()->isContractor()) {
            if (\View::exists($this->getAuditPathPrefix() . "contractor._edit_sidebar")) {
                return $this->getAuditPathPrefix() . "contractor._edit_sidebar";
            } elseif (\View::exists($this->getAuditPathPrefix() . "contractor._sidebar")) {
                return $this->getAuditPathPrefix() . "contractor._sidebar";
            }
        }

        return $this->getAuditPathPrefix() . $this->getAuditSideBarName();
    }

    protected function getAuditSidebarData($parent)
    {
        return [];
    }

    /**
     * Return true if the record type and code should be shown.  This is generally enabled when
     * displaying child audit records.
     * Override as required.
     */
    protected function showAuditRecordTypeColumn()
    {
        return true;
    }

    protected function getAuditView()
    {
        return '_audit-new-filter';
    }

    protected function setAuditBoxFilterElements($id)
    {
        return new AuditBoxFilter($this->getAuditFilterNameId(), $this->getAuditTable());
    }

    protected function addAuditTableHeaders(IAuditBoxFilter $auditBoxFilter)
    {
        if ($this->showAuditRecordTypeColumn()) {
            $auditBoxFilter->addRecordTypeHeaders();
        }
        $auditBoxFilter
            ->addSourceNameHeaders()
            ->addIdHeaders()
            ->addVersionHeaders();
    }

    protected function getAuditFilterLookups($id)
    {
        $auditService = new AuditService();
        return $auditService->filterLookups();
    }
}
