<?php

namespace App\Libs\BoxFilter\ReportOptions;

use App\Libs\BoxFilter\IBoxComponent;

interface IBaseReport extends IBoxComponent
{
    /**
     * @param int $systemReportId
     * @return IBaseReport
     */
    public function setSystemReportId(int $systemReportId);

    /**
     * @param int $userReportId
     * @return IBaseReport
     */
    public function setUserReportId(int $userReportId);

    /**
     * @param string $reportCode
     * @return IBaseReport
     */
    public function setReportCode(string $reportCode);

    /**
     * @param string $reportTitle
     * @return IBaseReport
     */
    public function setReportTitle(string $reportTitle);

    /**
     * @param int $startRow
     * @return IBaseReport
     */
    public function setStartRow(int $startRow);

    /**
     * @param int $maxRows
     * @return IBaseReport
     */
    public function setMaxRows(int $maxRows);

    /**
     * @param array $exportAsLookup
     * @return IBaseReport
     */
    public function setExportAsLookup(array $exportAsLookup);

    /**
     * @param bool $allowRunInBackground
     * @return IBaseReport
     */
    public function setAllowRunInBackground(bool $allowRunInBackground);

    /**
     * @param bool $existRunningReport
     * @return IBaseReport
     */
    public function setExistRunningReport(bool $existRunningReport);

    /**
     * @param string $label
     * @param string $api
     * @param array $params
     */
    public function addExportAsOption(string $label, string $api, array $params = []);

    /**
     * @param bool $showReturnedData
     * @return IBaseReport
     */
    public function setShowReturnedData(bool $showReturnedData);

    /**
     * @param bool $showFilter
     * @return IBaseReport
     */
    public function setShowFilter(bool $showFilter);

    /**
     * @param bool $extraFilters
     * @return IBaseReport
     */
    public function setExtraFilters(array $extraFilters);
}
