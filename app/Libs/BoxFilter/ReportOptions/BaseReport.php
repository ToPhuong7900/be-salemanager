<?php

namespace App\Libs\BoxFilter\ReportOptions;

use Tfcloud\Models\Report;

class BaseReport implements IBaseReport
{
    private const BOX_COMPONENT_NAME = 'baseReport';
    private const SYSTEM_REPORT_ID = 'systemReportID';
    private const USER_REPORT_ID = 'userReportID';
    private const REPORT_TYPE = 'repType';
    private const REPORT_CODE = 'repCode';
    private const REPORT_TITLE = 'repTitle';
    private const OPTIONS_FIELD_SET = 'options';
    private const OPTIONS_EXPORT_AS_LOOKUP = 'exportAs';
    private const OPTIONS_RUN_IN_BACK_GROUND = 'enabledBackgroundReport';
    private const OPTIONS_EXIST_RUNNING_REPORT = 'existRunningReport';
    private const OPTIONS_SHOW_FILTER = 'showFilter';

    private const RETURN_DATA_FIELD_SET = 'returnedData';
    private const RETURN_DATA_START_ROW = 'startRow';
    private const RETURN_DATA_MAX_ROWS = 'maxRows';
    private const EXTRA_FILTERS = 'extraFilters';

    /**
     * @var integer
     */
    private $systemReportId;

    /**
     * @var integer
     */
    private $userReportId;

    /**
     * @var string
     */
    private $reportCode;

    /**
     * @var string
     */
    private $reportTitle;

    /**
     * @var string
     */
    private $reportype;

    /**
     * @var bool
     */
    private $showFilter = true;

    /**
     * @var bool
     */
    private $showReturnedData = true;

    /**
     * @var int $startRow
     */
    private $startRow = 1;

    /**
     * @var int $maxRows
     */
    private $maxRows = 100000;

    /**
     * @var array $exportAsLookup
     */
    private $exportAsLookup = [];

    /**
     * @var bool $allowRunInBackground
     */
    private $allowRunInBackground = false;

    /**
     * @var bool
     */
    private $existRunningReport = false;

    /**
     * @var array $extraFilters
     */
    private $extraFilters = [];

    public function generate()
    {
        // RB 28/10/2022 This method of determining the report type isn't reliable. There is a chance that a
        // User Report's ID matches its corresponding Sytem Report ID. Thia has occurred for Somerset CLD-16790.
        //$reportType = $this->systemReportId === $this->userReportId ? Report::SYSTEM_REPORT : Report::USER_REPORT;
        $outputs = [
            self::SYSTEM_REPORT_ID    => $this->systemReportId,
            self::USER_REPORT_ID      => $this->userReportId,
            self::REPORT_TYPE         => $this->reportType, //$reportType,
            self::REPORT_TITLE        => $this->reportTitle,
            self::REPORT_CODE         => $this->reportCode,
            self::OPTIONS_SHOW_FILTER => $this->showFilter,
            self::EXTRA_FILTERS       => $this->extraFilters,
            self::OPTIONS_FIELD_SET   => $this->generateOptionsFieldSet()
        ];

        if ($this->showReturnedData) {
            $outputs[self::RETURN_DATA_FIELD_SET] = $this->generateReturnDataSet();
        }

        return $outputs;
    }

    public function getComponentName()
    {
        return self::BOX_COMPONENT_NAME;
    }

    public function setSystemReportId(int $systemReportId)
    {
        $this->systemReportId = $systemReportId;
        return $this;
    }

    public function setUserReportId(int $userReportId)
    {
        $this->userReportId = $userReportId;
        return $this;
    }

    public function setReportCode(string $reportCode)
    {
        $this->reportCode = $reportCode;
        return $this;
    }

    public function setReportTitle(string $reportTitle)
    {
        $this->reportTitle = $reportTitle;
        return $this;
    }

    public function setReportType(string $reportType)
    {
        $this->reportType = $reportType;
        return $this;
    }

    public function setStartRow(int $startRow)
    {
        $this->startRow = $startRow;
        return $this;
    }

    public function setMaxRows(int $maxRows)
    {
        $this->maxRows = $maxRows;
        return $this;
    }

    public function setExportAsLookup(array $exportAsLookup)
    {
        $this->exportAsLookup = $exportAsLookup;
        return $this;
    }

    public function setExtraFilters(array $extraFilters)
    {
        $this->extraFilters = $extraFilters;
        return $this;
    }

    public function setAllowRunInBackground(bool $allowRunInBackground)
    {
        $this->allowRunInBackground = $allowRunInBackground;
        return $this;
    }

    public function setExistRunningReport(bool $existRunningReport)
    {
        $this->existRunningReport = $existRunningReport;
        return $this;
    }

    public function addExportAsOption(string $label, string $api, array $params = [])
    {
        $this->exportAsLookup[] = [
            'label' => $label,
            'api' => $api,
            'params' => $params
        ];
    }

    public function setShowReturnedData(bool $showReturnedData)
    {
        $this->showReturnedData = $showReturnedData;
        return $this;
    }

    public function setShowFilter(bool $showFilter)
    {
        $this->showFilter = $showFilter;
        return $this;
    }

    protected function generateOptionsFieldSet()
    {
        return [
            self::OPTIONS_EXPORT_AS_LOOKUP => $this->exportAsLookup,
            self::OPTIONS_RUN_IN_BACK_GROUND => $this->allowRunInBackground,
            self::OPTIONS_EXIST_RUNNING_REPORT => $this->existRunningReport
        ];
    }

    protected function generateReturnDataSet()
    {
        return [
            self::RETURN_DATA_START_ROW => $this->startRow,
            self::RETURN_DATA_MAX_ROWS => $this->maxRows
        ];
    }
}
