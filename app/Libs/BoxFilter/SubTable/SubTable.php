<?php

namespace App\Libs\BoxFilter\SubTable;

class SubTable
{
    public const SUB_TABLE_KEY = 'subTable';
    private const API = 'api';
    private const SUB_TABLE_COMPONENT = 'subTableComponent';
    private const RELATED_RESTRICT_DATA = 'relatedRestrictData';
    private const DEFAULT_SORT = 'defaultSort';
    private const DEFAULT_PER_PAGE = 'defaultPerPage';

    private string $subTableComponent;
    private string $api;
    private string $defaultSort;
    private array $relatedRestrictData = [];

    public function __construct(string $subTableComponent, string $api, string $defaultSort)
    {
        $this->subTableComponent = $subTableComponent;
        $this->api = $api;
        $this->defaultSort = $defaultSort;
    }

    public function setRelatedRestrictData($relatedRestrictData)
    {
        $this->relatedRestrictData = $relatedRestrictData;
        return $this;
    }

    public function mergeSubTableInto(array $settings)
    {
        return array_merge(
            $settings,
            [self::SUB_TABLE_KEY => $this->generate()]
        );
    }

    public function generate()
    {
        return [
            self::API => $this->api,
            self::SUB_TABLE_COMPONENT => $this->subTableComponent,
            self::DEFAULT_SORT => $this->defaultSort,
            self::DEFAULT_PER_PAGE => \Auth::user()->default_pagesize,
            self::RELATED_RESTRICT_DATA => $this->relatedRestrictData
        ];
    }
}
