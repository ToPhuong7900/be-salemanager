<?php

namespace App\Libs\BoxFilter;

use App\Libs\BoxFilter\ReportOptions\IBaseReport;
use Tfcloud\Models\Report;

interface IBoxFilterReport
{
    /**
     * @param Report $report
     * @return IBoxFilterReport
     */
    public function setReport(Report $report);

    /**
     * @return Report
     */
    public function getReport();

    /**
     * @param IBaseReport $baseReport
     * @return $this
     */
    public function setBaseReport(IBaseReport $baseReport);
}
