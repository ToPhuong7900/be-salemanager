<?php

namespace App\Libs\BoxFilter;

use App\Libs\BoxFilter\KPIOptions\BaseKPI;
use App\Libs\BoxFilter\KPIOptions\IBaseKPI;

abstract class BoxFilterKPI extends BoxFilter implements IBoxFilterKPI
{
    /**
     * @var array
     */
    protected $displayKPIs;

    /**
     * @var IBaseKPI
     */
    protected $baseKPI;

    public function __construct(int $filterNameId, array $displayKPIs)
    {
        parent::__construct($filterNameId);
        $this->setDisplayKPIs($displayKPIs)
            ->setBaseKPI($this->initBaseKPI());

        $this->setBoxComponent($this->baseKPI);
    }

    public function setDisplayKPIs(array $displayKPIs)
    {
        $this->displayKPIs = $displayKPIs;
        return $this;
    }

    public function setBaseKPI(IBaseKPI $baseKPI)
    {
        $this->baseKPI = $baseKPI;
        return $this;
    }

    /**
     * @return IBaseKPI
     */
    protected function initBaseKPI()
    {
        return new BaseKPI($this->displayKPIs);
    }
}
