<?php

namespace App\Libs\BoxFilter;

use App\Libs\BoxFilter\Constants\OperatorTypeConstant;
use App\Libs\BoxFilter\Elements\AddressElement;
use App\Libs\BoxFilter\Elements\DateRangeDropDownElement;
use App\Libs\BoxFilter\Elements\DateRangeElement;
use App\Libs\BoxFilter\Elements\DropDownApiElement;
use App\Libs\BoxFilter\Elements\DropDownElement;
use App\Libs\BoxFilter\Elements\NumberRangeElement;
use App\Libs\BoxFilter\Elements\Pickers\AccountCodePickerElement;
use App\Libs\BoxFilter\Elements\Pickers\ContactPickerElement;
use App\Libs\BoxFilter\Elements\Pickers\ContactUserPickerElement;
use App\Libs\BoxFilter\Elements\Pickers\EstateUnitPickerElement;
use App\Libs\BoxFilter\Elements\Pickers\OwnerUserPickerElement;
use App\Libs\BoxFilter\Elements\Pickers\PlantPickerElement;
use App\Libs\BoxFilter\Elements\Pickers\SitePickerElement;
use App\Libs\BoxFilter\Elements\Pickers\UserPickerElement;
use App\Libs\BoxFilter\Elements\RadioDropDownElement;
use App\Libs\BoxFilter\Elements\TextElement;
use App\Libs\BoxFilter\Elements\NumberElement;
use App\Libs\BoxFilter\Elements\TimeRangeElement;
use Tfcloud\Models\LeaseType;
use App\Libs\Common;
use App\Libs\Constant\CommonConstant;
use Tfcloud\Models\PlantGroup;

trait MasterElementBoxFilter
{
    /**
     * @param string $label
     * @param false $requiredOperator
     * @param null $defaultValue
     * @param string $userField
     * @return UserPickerElement
     */
    protected function elementUserId(
        $label = 'Owner',
        $requiredOperator = false,
        $defaultValue = null,
        $userField = 'user_id',
        $dataLookups = []
    ) {

        $showInActiveCodes = \Auth::User()->show_inactive_codes;
        $dataLookups = array_merge($dataLookups, [
            'incActiveUsers' => $showInActiveCodes
        ]);

        $userPickerElement = new UserPickerElement($userField, $label, $dataLookups);
        if ($defaultValue) {
            $userPickerElement->setDefaultValue($defaultValue);
        }
        if ($requiredOperator) {
            $userPickerElement->setOperatorsForRequiredField();
        }
        $userPickerElement->setDefaultValue(\Auth::user()->getKey());
        return $userPickerElement;
    }

    /**
     * @param bool $requiredOperator
     * @param null $defaultValue
     * @param string $label
     * @return UserPickerElement
     */
    protected function elementOwnerUserId($requiredOperator = true, $defaultValue = null, $label = 'Owner')
    {
        return $this->elementUserId($label, $requiredOperator, $defaultValue, 'owner_user_id');
    }

    protected function elementOwnerUserLandingPage(
        $label = 'Owner',
        $requiredOperator = true,
        $userField = 'owner_user_id',
        $dataLookups = []
    ) {
        $showInActiveCodes = \Auth::User()->show_inactive_codes;

        $dataLookups = array_merge($dataLookups, [
            'incActiveUsers' => $showInActiveCodes,
        ]);

        $userPickerElement = new OwnerUserPickerElement(
            $userField,
            $label,
            $dataLookups
        );

        if ($requiredOperator) {
            $userPickerElement->setOperatorsForRequiredField();
        }
        return $userPickerElement;
    }

    /**
     * @param string $field
     * @param string $label
     * @param false $requiredOperator
     * @param null $defaultValue
     * @return ContactPickerElement
     */
    protected function elementContactId(
        $field = 'contact_id',
        $label = 'Contact',
        $requiredOperator = false,
        $defaultValue = null
    ) {
        $showInActiveCodes = \Auth::User()->show_inactive_codes;
        $dataLookups = [
            'showInactive' => $showInActiveCodes
        ];

        $pickerElement = new ContactPickerElement($field, $label, $dataLookups);
        if ($defaultValue) {
            $pickerElement->setDefaultValue($defaultValue);
        }
        if ($requiredOperator) {
            $pickerElement->setOperatorsForRequiredField();
        }
        return $pickerElement;
    }

    protected function elementSupplierId(
        $label = 'Supplier',
        $defaultValue = null,
        $field = 'supplier_id',
        $includeContactId = null
    ) {
        $dataLookupFilters = [
            'supplier' => 1
        ];
        if ($includeContactId) {
            $dataLookupFilters = array_merge($dataLookupFilters, [
                'includeContactId' => $includeContactId
            ]);
        }
        $pickerElement = new ContactPickerElement(
            $field,
            $label,
            $dataLookupFilters
        );
        if ($defaultValue) {
            $pickerElement->setDefaultValue($defaultValue);
        }
        return $pickerElement;
    }

    protected function elementStartDate($label = 'Start Date', $options = [])
    {
        return new DateRangeElement('start_date', $label, $options);
    }

    protected function elementEndDate($label = 'End Date', $options = [])
    {
        return new DateRangeElement('end_date', $label, $options);
    }

    protected function elementCreatedDate($label = 'Created Date', $options = [])
    {
        return new DateRangeElement('created_date', $label, $options);
    }

    protected function elementClosedDate($label = 'Closed Date', $options = [])
    {
        return new DateRangeElement('closed_date', $label, $options);
    }

    protected function elementFinAccountId($label = 'Account', $dataLookupFilters = [], $options = [], $required = true)
    {
        $accountCodePickerElement = new AccountCodePickerElement(
            'fin_account_id',
            $label,
            $dataLookupFilters,
            $options
        );
        if ($required) {
            $accountCodePickerElement->setOperatorsForRequiredField();
        }
        return $accountCodePickerElement;
    }

    protected function elementFinYearId($financialYears, $label = 'Financial Year')
    {
        $element =  new DropDownElement('fin_year_id', $label, $financialYears);
        $element->setValueFieldName('fin_year_id')->setTextFieldNames([
            'fin_year_code', 'fin_year_desc'
        ]);
        $element->setOperatorsForRequiredField();
        return $element;
    }

    protected function elementPlantId($label = 'Plant', $requiredOperator = false)
    {
        $element = new PlantPickerElement('plant_id', $label);

        if ($requiredOperator) {
            $element->setOperatorsForRequiredField();
        }

        return $element;
    }

    protected function elementPlantSystemId(
        $types,
        $label = 'Plant System Type',
        $dataConfig = [],
        $options = []
    ) {
        $element =  new DropDownElement('plant_system_id', $label, $types, $dataConfig, $options);
        $element->setValueFieldName('plant_system_id')->setTextFieldNames([
            'plant_system_code', 'plant_system_desc'
        ]);
        return $element;
    }

    protected function elementSiteCode($label = 'Site Code')
    {
        return (new TextElement('site_code', $label))
            ->setOperatorsForRequiredField()
            ->allowMultipleMode();
    }

    protected function elementStorey($label = 'Storey', $options = [])
    {
        return new NumberElement('storey', $label, $options);
    }

    protected function elementSiteDesc($label = 'Site Description')
    {
        return new TextElement('site_desc', $label);
    }

    protected function elementBuildingCode($label = 'Building Code')
    {
        return (new TextElement('building_code', $label))->setOperatorsForRequiredField()
            ->allowMultipleMode();
    }

    protected function elementBuildingDesc($label = 'Building Description')
    {
        return new TextElement('building_desc', $label);
    }

    protected function elementRoomDesc($label = 'Room Description', $options = [])
    {
        return new TextElement('room_desc', $label, $options);
    }

    protected function elementRoomNumber($label = 'Room Number', $options = [])
    {
        return (new TextElement('room_number', $label, $options))->setOperatorsForRequiredField();
    }

    protected function elementPlantCode($label = 'Plant Code', $required = true)
    {
        $element = new TextElement('plant_code', $label);
        if ($required) {
            $element->setOperatorsForRequiredField();
        }
        $element->allowMultipleMode();
        return $element;
    }

    protected function elementRoomTypeGroup($roomTypes, $notAppliedFilter = false)
    {
        $operatorTypes = [
            OperatorTypeConstant::INCLUDE,
            OperatorTypeConstant::NOT_INCLUDE
        ];
        $element = new DropDownElement('room_type_id', 'Type', $roomTypes);
        $element->setValueFieldName('room_type_id')->setTextFieldNames([
            'room_type_code', 'room_type_desc'
        ]);
        $element->setGroup('room_type_id')->setChild('room_subtype_id')->setOperators($operatorTypes)
            ->setNotAppliedFilter($notAppliedFilter);

        $subElement = new DropDownApiElement('room_subtype_id', 'Sub Type', [
            'valueFieldName' => 'room_subtype_id',
            'textFieldName' => ['code_description']
        ]);
        $subElement->setGroup('room_type_id')
            ->setApi('/api/property/room-types/{room_type_id}/room-subtypes')
            ->setParent('room_type_id')
            ->setOperators($operatorTypes);

        return [$element, $subElement];
    }

    protected function getElementPlantGroupCascading($notAppliedFilter = false, $operators = null, $showOperator = true)
    {
        $operatorTypes = $operators ?? [
            OperatorTypeConstant::INCLUDE,
            OperatorTypeConstant::NOT_INCLUDE
        ];
        $incInactive = \Auth::User()->show_inactive_codes == CommonConstant::DATABASE_VALUE_YES;
        $plantGroups = (new PlantGroup())->lookupList([
            'incInactive' => $incInactive,
        ]);
        $plantGroupElement =  new DropDownElement('plant_group_id', 'Plant Group', $plantGroups, [
            'valueFieldName' => 'plant_group_id',
            'textFieldName' => ['plant_group_code', 'plant_group_desc']
        ]);
        $plantGroupElement
            ->setGroup('plant_group_id')
            ->setChild('plant_subgroup_id')
            ->setOperators($operatorTypes)
            ->setNotAppliedFilter($notAppliedFilter)
            ->showOperator($showOperator);

        $planSubGroupElement = new DropDownApiElement('plant_subgroup_id', 'Plant Sub Group', [
            'valueFieldName' => 'plant_subgroup_id',
            'textFieldName' => ['code_description']
        ]);
        $planSubGroupElement->setGroup('plant_group_id')
            ->setApi('/api/property/plant-groups/{plant_group_id}/plant-subgroups')
            ->setParent('plant_group_id')
            ->setChild('plant_type_id')
            ->setDataLookupFilters(['inListFilter' => 1])
            ->setOperators($operatorTypes)
            ->showOperator($showOperator);

        $plantTypeElement = new DropDownApiElement('plant_type_id', 'Plant Type', [
            'valueFieldName' => 'plant_type_id',
            'textFieldName' => ['code_description']
        ]);
        $plantTypeElement->setGroup('plant_group_id')
            ->setApi('/api/property/plant-subgroups/{plant_subgroup_id}/plant-types')
            ->setParent('plant_subgroup_id')
            ->setDataLookupFilters(['inListFilter' => 1])
            ->setOperators($operatorTypes)
            ->showOperator($showOperator);

        return [$plantGroupElement, $planSubGroupElement, $plantTypeElement];
    }

    protected function elementModuleId(
        $modules,
        $label = 'Module',
        $dataConfig = [],
        $options = []
    ) {
        return (new DropDownElement(
            'module_id',
            $label,
            $modules,
            array_merge([
                'valueFieldName' => 'module_id',
                'textFieldName' => ['module_description']
            ], $dataConfig),
            $options
        ))->setOperatorsForRequiredField();
    }

    protected function elementEstablishmentId(
        $establishments,
        $label = 'Establishment',
        $dataConfig = [],
        $options = []
    ) {
        $element =  new DropDownElement('establishment_id', $label, $establishments, $dataConfig, $options);
        $element->setValueFieldName('establishment_id')->setTextFieldNames([
            'establishment_code', 'establishment_desc'
        ]);
        return $element;
    }

    protected function elementEstablishementTypeId($types, $label = 'Type', $dataConfig = [], $options = [])
    {
        $element =  new DropDownElement('establishment_type_id', $label, $types, $dataConfig, $options);
        $element->setValueFieldName('establishment_type_id')->setTextFieldNames([
            'establishment_type_code', 'establishment_type_desc'
        ]);
        return $element;
    }

    protected function elementSiteTypeId($siteTypes, $label = 'Site Type', $dataConfig = [], $options = [])
    {
        $element =  new DropDownElement('site_type_id', $label, $siteTypes, $dataConfig, $options);
        $element->setValueFieldName('site_type_id')->setTextFieldNames([
            'site_type_code', 'site_type_desc'
        ]);
        return $element;
    }

    protected function elementBuildingTypeId($siteTypes, $label = 'Building Type', $dataConfig = [], $options = [])
    {
        $element =  new DropDownElement('building_type_id', $label, $siteTypes, $dataConfig, $options);
        $element->setValueFieldName('building_type_id')->setTextFieldNames([
            'building_type_code', 'building_type_desc'
        ]);
        return $element;
    }

    protected function elementTargetDate($label = 'Target Date')
    {
        return new DateRangeElement('target_date', $label);
    }

    protected function elementComment($label = 'Comments', $name = 'comment', $options = [])
    {
        return new TextElement($name, $label, $options);
    }


    protected function elementNextReviewDate($label = 'Next Review Date', $options = [])
    {
        return new DateRangeElement('next_review_date', $label, $options);
    }

    protected function elementReviewDate()
    {
        return new DateRangeElement('review_date', 'Review Date');
    }

    protected function elementActive($label = 'Active')
    {
        $element = new RadioDropDownElement('active', $label);
        $element->buildYesNoElement();
        return $element;
    }

    protected function elementSpotCheckRequired($name = 'spot_check_required')
    {
        $element = new RadioDropDownElement($name, 'Spot Check Required');
        $element->buildYesNoElement();
        return $element;
    }

    protected function elementFinAuthStatusId(
        $spotCheckStatus,
        $label = 'Spot Check Audit',
        $name = 'fin_auth_status_id'
    ) {
        return new DropDownElement(
            $name,
            $label,
            $spotCheckStatus,
            [
                'valueFieldName' => 'fin_auth_status_id',
                'textFieldName' => ['name']
            ]
        );
    }

    protected function elementAFPStatusId($afpStatus, $label = 'Status')
    {
        $element = new DropDownElement('afp_status_id', $label, $afpStatus);
        $element->setValueFieldName('afp_status_id')->setTextFieldNames([
            'afp_status_code', 'afp_status_desc'
        ])->setOperatorsForRequiredField();
        return $element;
    }

    protected function elementContactUserId(
        $label = 'Contact',
        $requiredOperator = false,
        $defaultValue = null,
        $recordField = 'record_id'
    ) {
        $contactUserPickerElement = new ContactUserPickerElement($recordField, $label);
        if ($defaultValue) {
            $contactUserPickerElement->setDefaultValue($defaultValue);
        }
        if ($requiredOperator) {
            $contactUserPickerElement->setOperatorsForRequiredField();
        }
        return $contactUserPickerElement;
    }

    protected function elementHazardWarningIcon(
        $hazardWarningIcons,
        $label = 'Hazard & Warning Icon'
    ) {
        $operatorTypes = [
            OperatorTypeConstant::INCLUDE,
            OperatorTypeConstant::NOT_INCLUDE,
            OperatorTypeConstant::MATCH,
            OperatorTypeConstant::NOT_MATCH,
            OperatorTypeConstant::BLANK,
            OperatorTypeConstant::NOT_BLANK,
        ];
        $element =  new DropDownElement('hazard_warning_icon_id', $label, $hazardWarningIcons);
        $element->setValueFieldName('hazard_warning_icon_id')->setTextFieldNames([
            'hazard_warning_icon_type', 'hazard_warning_icon_desc'
        ])->setOperators($operatorTypes);
        return $element;
    }

    protected function addBlindElementParentId($parentId)
    {
        $this->addBlindElement(
            (new DropDownElement('parent_id', 'Parent', []))
                ->setDefaultValue($parentId)
                ->setDefaultOperator(OperatorTypeConstant::MATCH)
        );
        return $this;
    }

    protected function elementFloorId(
        $establishments,
        $label = 'Floor',
        $dataConfig = [],
        $options = []
    ) {
        $element =  new DropDownElement('room_floor_id', $label, $establishments, $dataConfig, $options);
        $element->setValueFieldName('room_floor_id')->setTextFieldNames([
            'room_floor_code', 'room_floor_desc'
        ]);
        return $element;
    }

    protected function elementStatus($name = 'active', $label = 'Status')
    {
        $dataLookups = [
            ['value' => 'Y', 'text' => 'Active'],
            ['value' => 'N', 'text' => 'Inactive'],
        ];
        return (new RadioDropDownElement($name, $label))->setDataLookUps($dataLookups);
    }

    protected function elementBuildingId($buildings)
    {
        $element = new DropDownElement('building_id', 'Building', $buildings);
        $element->setValueFieldName('building_id')->setTextFieldNames([
            'building_code', 'building_desc'
        ])->setOperatorsForRequiredField();
        return $element;
    }

    protected function elementDateRange($name = 'start_date', $label = 'Start Date', $options = [])
    {
        return new DateRangeElement($name, $label, $options);
    }

    protected function elementTimeRange($name = 'start_time', $label = 'Start Time', $options = [])
    {
        return new TimeRangeElement($name, $label, $options);
    }

    protected function elementNumberRange($name = 'start_number', $label = 'Start Number', $options = [])
    {
        return new NumberRangeElement($name, $label, $options);
    }

    protected function elementBuildingBlockTypeId(
        $siteTypes,
        $label = 'Building Block Type',
        $dataConfig = [],
        $options = []
    ) {
        $element =  new DropDownElement('building_block_type_id', $label, $siteTypes, $dataConfig, $options);
        $element->setValueFieldName('building_block_type_id')->setTextFieldNames([
            'building_block_type_code', 'building_block_type_desc'
        ]);
        return $element;
    }

    protected function elementBuildingBlockCode($label = 'Building Block Code')
    {
        return (new TextElement('building_block_code', $label))->setOperatorsForRequiredField()
            ->allowMultipleMode();
    }

    protected function elementBuildingBlockDesc($label = 'Building Block Description')
    {
        return new TextElement('building_block_desc', $label);
    }

    protected function elementEstateUnitId($label = 'Unit', $dataLookupFilters = [], $options = [])
    {
        return (new EstateUnitPickerElement('lettable_unit_id', $label, $dataLookupFilters, $options))
            ->setOperatorsForRequiredField();
    }

    protected function elementNetTotal($name = 'net_total', $label = 'Net Total (£)')
    {
        return new NumberRangeElement($name, $label);
    }

    protected function elementGrossTotal($name = 'gross_total', $label = 'Gross Total (£)'): NumberRangeElement
    {
        return new NumberRangeElement($name, $label);
    }

    protected function elementGrandTotal($name = 'grand_total', $label = 'Grand Total (£)')
    {
        return new NumberRangeElement($name, $label);
    }

    protected function elementSiteId($label = 'Site', $dataLookupFilters = [], $options = [])
    {
        return (new SitePickerElement($label, $dataLookupFilters, $options))
            ->setOperatorsForRequiredField();
    }

    protected function elementJobRoleId($jobRoles = null, $label = 'Job Role')
    {
        return (new DropDownElement(
            'job_role_id',
            $label,
            $jobRoles,
            [
                'valueFieldName' => 'job_role_id',
                'textFieldName' => ['job_role_code', 'job_role_desc']
            ]
        ))->setOperatorsForRequiredField();
    }

    protected function elementLandlordId(
        $field = 'landlord_contact_id',
        $label = 'Landlord',
        $requiredOperator = false,
        $defaultValue = null
    ) {
        $pickerElement = new ContactPickerElement(
            $field,
            $label,
            [
                'landlord' => 1,
                'isFilter' => 1
            ]
        );
        if ($defaultValue) {
            $pickerElement->setDefaultValue($defaultValue);
        }

        if ($requiredOperator) {
            $pickerElement->setOperatorsForRequiredField();
        }

        return $pickerElement;
    }

    protected function elementAddress($setNotApplied = false)
    {
        $addressElement = (new AddressElement($setNotApplied));
        $addressElement->addSubDwellingElement()
            ->addNumberNameElement()
            ->addStreetElement()
            ->addLocalityElement()
            ->addTownElement()
            ->addRegionElement()
            ->addPostcodeElement()
            ->addCountryElement();

        return $addressElement->getElements();
    }

    protected function getElementLeaseTypeSubTypeCascading(
        $notAppliedFilter = true,
        $operators = null,
        $showOperator = true,
        $enableRadioMode = false
    ) {
        $operatorTypes = $operators ?? [
            OperatorTypeConstant::INCLUDE,
            OperatorTypeConstant::NOT_INCLUDE
        ];
        $typeGroup = (new LeaseType())->lookupList([
            'incInactive' => true,
            'orders' => [
                ['sortField' => 'lease_type_code']
            ],
        ]);

        $typeGroupElement = new DropDownElement('lease_type_id', 'Type', $typeGroup, [
            'valueFieldName' => 'lease_type_id',
            'textFieldName' => ['lease_type_code', 'lease_type_desc']
        ]);
        $typeGroupElement
            ->setGroup('lease_type_id')
            ->setChild('est_lease_sub_type_id')
            ->setOperators($operatorTypes)
            ->setNotAppliedFilter($notAppliedFilter)
            ->showOperator($showOperator);

        $typeSubGroupElement = new DropDownApiElement('est_lease_sub_type_id', 'Sub Type', [
            'valueFieldName' => 'est_lease_sub_type_id',
            'textFieldName' => ['code_description']
        ]);
        $typeSubGroupElement->setGroup('lease_type_id')
            ->setApi('/api/estate/estates/type/{lease_type_id}/lease-subtypes')
            ->setParent('lease_type_id')
            ->setDataLookupFilters(['inListFilter' => 1])
            ->setOperators($operatorTypes)
            ->showOperator($showOperator);
        if ($enableRadioMode) {
            $typeGroupElement->enableRadioMode($enableRadioMode);
            $typeSubGroupElement->enableRadioMode($enableRadioMode);
        }
        return [$typeGroupElement, $typeSubGroupElement];
    }

    protected function elementShowFirstPageOnly($label = 'Show first page only', $options = [])
    {
        $dataLookups = [
            ['value' => 1, 'text' => 'Yes'],
            ['value' => 0, 'text' => 'No'],
        ];
        return (new RadioDropDownElement('show_first_page_only', $label, $options))
            ->setDataLookUps($dataLookups)
            ->buildYesNoElement();
    }

    protected function elementTenureId($tenures)
    {
        return new DropDownElement(
            'prop_tenure_id',
            'Tenure',
            $tenures,
            [
                'valueFieldName' => 'prop_tenure_id',
                'textFieldName' => ['prop_tenure_code', 'prop_tenure_desc']
            ]
        );
    }

    protected function elementEPCExpiryDate($label = 'EPC Expiry Date')
    {
        return new DateRangeElement('energy_epc_expiry_date', $label);
    }
    protected function elementEPCDateOfInspection($label = 'EPC Date of Inspection')
    {
        return new DateRangeElement('energy_epc_date_of_inspection', $label);
    }
    protected function elementLineTotalCost($label = 'Line Total Cost')
    {
        return new NumberRangeElement('total_cost', $label);
    }

    protected function elementSiteUsageId($usages, $label = 'Usage')
    {
        return new DropDownElement(
            'site_usage_id',
            $label,
            $usages,
            [
                'valueFieldName' => 'site_usage_id',
                'textFieldName' => ['site_usage_code', 'site_usage_desc']
            ]
        );
    }

    protected function elementCurrentEPCRating($epcRating, $label = 'Current EPC Rating')
    {
        $element = new DropDownElement('energy_current_epc_rating_id', $label, $epcRating);
        $element->setValueFieldName('energy_epc_rating_id')->setTextFieldNames([
            'energy_epc_rating_code', 'energy_epc_rating_desc'
        ]);
        return $element;
    }

    protected function elementAuditAction(
        $actions,
        $label = 'Action',
        $dataConfig = [],
        $options = []
    ) {
        $element = new DropDownElement('audit_action_id', $label, $actions, $dataConfig, $options);
        $element->setValueFieldName('audit_action_id')->setTextFieldNames([
            'audit_action_code', 'audit_action_desc'
        ])->setOperatorsForRequiredField();
        return $element;
    }

    protected function elementAuditDate($name = 'timestamp', $label = 'Audit Date')
    {
        return new DateRangeElement($name, $label);
    }

    protected function elementInstructionCode($label = 'Instruction Code')
    {
        return (new TextElement('instruction_code', $label))->setOperatorsForRequiredField()
            ->allowMultipleMode();
    }

    protected function elementShowIncompletePeriod($label = 'Show Incomplete Period')
    {
        $dataLookups = [
            ['value' => CommonConstant::DATABASE_VALUE_YES, 'text' => 'Yes'],
            ['value' => CommonConstant::DATABASE_VALUE_NO, 'text' => 'No'],
        ];
        return (new RadioDropDownElement('show_incomplete_period', $label))
            ->setDataLookUps($dataLookups)
            ->setDefaultValue(CommonConstant::DATABASE_VALUE_NO)
            ->setKeepAlive()
            ->buildYesNoElement();
    }

    private function elementInvoicePostDate()
    {
        return new DateRangeElement('last_changed_to_posted_date', 'Invoice Posted Date');
    }
}
