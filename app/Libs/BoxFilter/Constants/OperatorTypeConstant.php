<?php

namespace App\Libs\BoxFilter\Constants;

class OperatorTypeConstant
{
    public const CONTAIN = 1;
    public const NOT_CONTAIN = 2;
    public const NOT_MATCH = 3;
    public const MATCH = 4;
    public const INCLUDE = 5;
    public const NOT_INCLUDE = 6;
    public const INCLUDE_RANGE = 7;
    public const EXCLUDE_RANGE = 8;
    public const BLANK = 9;
    public const NOT_BLANK = 10;
    public const MULTIPLE_VALUE = 11;
    public const DATE_RANGE = 12;
    public const AFTER = 13;
    public const BEFORE = 14;
    public const EQUAL = 15;
    public const AFTER_OR_EQUAL = 16;
    public const BEFORE_OR_EQUAL = 17;

    public static function getSupportedOperators()
    {
        return  [
            self::CONTAIN     => 'Contains',
            self::NOT_CONTAIN      => 'Not Contains',
            self::NOT_MATCH   => 'Not Matches',
            self::MATCH     => 'Matches',
            self::INCLUDE  => 'Includes',
            self::NOT_INCLUDE     => 'Not Includes',
            self::INCLUDE_RANGE => 'Includes Range',
            self::EXCLUDE_RANGE => 'Excludes Range',
            self::BLANK => 'Blank',
            self::NOT_BLANK => 'Not Blank',
            self::MULTIPLE_VALUE => 'Multiple Values',
            self::DATE_RANGE => 'Date Range',
            self::AFTER => 'After',
            self::BEFORE => 'Before',
            self::EQUAL => 'Equal',
            self::AFTER_OR_EQUAL => 'After or Equal',
            self::BEFORE_OR_EQUAL => 'Before or Equal',
        ];
    }

    public static function getRequiredTextFieldOperators()
    {
        return [
            self::CONTAIN,
            self::NOT_CONTAIN,
            self::MATCH,
            self::NOT_MATCH,
        ];
    }

    public static function getRequiredDropdownFieldOperators()
    {
        return [
            self::INCLUDE,
            self::NOT_INCLUDE,
        ];
    }

    public static function getOperatorName(int $operatorId)
    {
        $supportedOperators = self::getSupportedOperators();
        return $supportedOperators[$operatorId] ?? null;
    }
}
