<?php

namespace App\Libs\BoxFilter\Constants;

class ElementTypeConstant
{
    public const TEXT = 'TEXT';
    public const SINGLE_DATE = 'SINGLE_DATE';
    public const SINGLE_TIME = 'SINGLE_TIME';
    public const DATE_RANGE = 'DATE_RANGE';
    public const DATE_TIME_RANGE = 'DATE_TIME_RANGE';
    public const TIME_RANGE = 'TIME_RANGE';
    public const DROP_DOWN = 'DROP_DOWN';
    public const DROP_DOWN_API = 'DROP_DOWN_API';
    public const NUMBER = 'NUMBER';
    public const NUMBER_RANGE = 'NUMBER_RANGE';
    public const PICKER = 'PICKER';
    public const RADIO = 'RADIO';
    public const USER_PICKER = 'USER_PICKER';
    public const DATE_FIELD = 'DATE_FIELD';
}
