<?php

namespace App\Libs\BoxFilter\Constants;

class RecordActionConstant
{
    public const ACTION_COL_KEY = 'action';
    public const ACTION_COL_LABEL_KEY = 'Action';

    public const EDIT_ACTION_KEY = 'edit';
    public const EDIT_ACTION_LABEL = 'Edit';
    public const VIEW_ACTION_KEY = 'view';
    public const VIEW_ACTION_LABEL = 'View';

    public const DOWNLOAD_ACTION_KEY = 'download';
    public const DOWNLOAD_ACTION_LABEL = 'Download';

    public const DELETE_ACTION_KEY = 'delete';
    public const DELETE_ACTION_LABEL = 'Del';
    public const REMOVE_ACTION_LABEL = 'Remove';

    public const RUN_ACTION_KEY = 'run';
    public const RUN_ACTION_LABEL = 'Run now ';
    public const RUNNING_ACTION_LABEL = 'Running';

    public const COPY_ACTION_KEY = 'copy';
    public const COPY_ACTION_LABEL = 'Copy';
    public const REVALUE_ACTION_LABEL = 'Revalue';

    public const GET_ACTION_KEY = 'get';
    public const GET_ACTION_LABEL = 'Get';

    public const SHOW_COL_KEY = 'show';
    public const SHOW_COL_LABEL_KEY = 'show';

    public const SHOW_ACTION_KEY = 'show';
    public const SHOW_ACTION_LABEL = 'Show';

    public const DETAILS_COL_KEY = 'details';
    public const DETAILS_COL_LABEL_KEY = 'Details';

    public const DETAILS_ACTION_KEY = 'details';
    public const DETAILS_ACTION_LABEL = 'Details';

    public const RECORD_ACTION_EDIT_SITE = 'edit_site';
    public const RECORD_ACTION_EDIT_BUILDING = 'edit_building';
    public const RECORD_ACTION_EDIT_ZONE = 'edit_zone';
    public const RECORD_ACTION_EDIT_ROOM = 'edit_room';
    public const RECORD_ACTION_EDIT_EXTERNAL_AREA = 'edit_external_area';
    public const RECORD_ACTION_EDIT_PLANT = 'edit_plant';

    // list property action, when add new action related to property, pls add this into this list
    public const PROPERTY_ACTION = [
        self::RECORD_ACTION_EDIT_SITE,
        self::RECORD_ACTION_EDIT_BUILDING,
        self::RECORD_ACTION_EDIT_ZONE,
        self::RECORD_ACTION_EDIT_ROOM,
        self::RECORD_ACTION_EDIT_EXTERNAL_AREA,
        self::RECORD_ACTION_EDIT_PROPERTY,
        self::RECORD_ACTION_SUMMARY_SITE,
        self::RECORD_ACTION_SUMMARY_BUILDING,
        self::RECORD_ACTION_SUMMARY_SITE_INSPECTION,
        self::RECORD_ACTION_EDIT_PLANT,
        self::RECORD_ACTION_EDIT_ESTATE_SITE_FIELD
    ];

    public const RECORD_ACTION_EDIT_PARENT = 'edit_parent';
    public const RECORD_ACTION_EDIT_QST = 'edit_question';
    public const RECORD_ACTION_EDIT_ACCOUNT = 'edit_account';
    //project module
    public const RECORD_ACTION_EDIT_PROJECT = 'edit_project';
    public const RECORD_ACTION_EDIT_PRJ_PACKAGE_ACTION = 'edit_package';
    public const RECORD_ACTION_EDIT_PRJ_STAGE_ACTION = 'edit_stage';
    public const RECORD_ACTION_EDIT_PRJ_TASK_ACTION = 'edit_task';

    // asbestos module
    public const RECORD_ACTION_EDIT_ASB_SURVEY = 'edit_asb_survey';
    public const RECORD_ACTION_EDIT_ASR = 'edit_asr';
    public const RECORD_ACTION_UNLINK_ASR = 'unlink_asr';

    // Inspection module
    public const RECORD_ACTION_EDIT_INSPECTION = 'edit_inspection';
    public const RECORD_ACTION_REMOVE_INSPECTION = 'remove_inspection';
    public const RECORD_ACTION_SUMMARY_SITE_INSPECTION = 'summary_site_inspection';

    public const RECORD_ACTION_SUMMARY_ASB_SURVEY = 'summary_asb_survey';
    public const RECORD_ACTION_SUMMARY_ASB_ASR_SURVEY = 'summary_asb_asr_survey';
    public const RECORD_ACTION_SUMMARY_ASR_SURVEY = 'summary_asr_survey';
    public const RECORD_ACTION_SUMMARY_SITE = 'summary_site';
    public const RECORD_ACTION_SUMMARY_BUILDING = 'summary_building';
    public const RECORD_ACTION_SUMMARY_ROOM = 'summary_room';

    // Estate - Lettable Unit
    public const RECORD_ACTION_EDIT_LETTABLE_UNIT = 'edit_lettable_unit';
    public const RECORD_ACTION_REMOVE_LETTABLE_UNIT = 'remove_lettable_unit';

    //Estate - title - property
    public const RECORD_ACTION_EDIT_DEED_CODE = 'edit_deed_code';

    // Estate
    public const RECORD_ACTION_SUMMARY_TENANT = 'summary_tenant';
    public const RECORD_ACTION_EDIT_LEASE_IN = 'edit_lease_in';
    public const RECORD_ACTION_EDIT_LEASE_OUT = 'edit_lease_out';

    // Fixed Asset module
    public const RECORD_ACTION_EDIT_FIXED_ASSET = 'edit_fixed_asset';

    // Instruction module
    public const RECORD_ACTION_EDIT_INSTRUCTION = 'edit_instruction';

    // ptw
    public const RECORD_ACTION_EDIT_PTW = 'edit_ptw';

    // cleaning survey
    public const RECORD_ACTION_EDIT_CLEANING_SURVEY = 'edit_cleaning_survey';

    // dlog
    public const RECORD_ACTION_REMOVE_DLO = 'remove_dlo';

    // tree
    public const RECORD_ACTION_EDIT_TREE_SURVEY = 'edit_tree_survey';
    public const RECORD_ACTION_EDIT_TREE_SURVEY_ITEM = 'edit_tree_survey_item';

    public const EXTRACT_ACTION_KEY = 'extract';
    public const EXTRACT_ACTION_LABEL = 'Extract';
    public const RECORD_ACTION_EDIT_PROPERTY = 'edit_property';

    // Utility
    public const EDIT_UTILITY_BILL_ACTION_LABEL = 'edit_utility_bill';

    public const DEFAULT_ACTION_LABELS = [
        self::EDIT_ACTION_KEY => self::EDIT_ACTION_LABEL,
        self::DELETE_ACTION_KEY => self::DELETE_ACTION_LABEL,
        self::COPY_ACTION_KEY => self::COPY_ACTION_LABEL
    ];

    public const EDIT_ACTION_PARENT_INSTRUCTION = 'edit_parent_instruction';

    // Contract
    public const RECORD_ACTION_EDIT_CONTRACT = 'edit_contract';
    public const RECORD_ACTION_EDIT_CONTRACT_INSTRUCTION = 'edit_contract_instruction';
    public const RECORD_ACTION_EDIT_CONTRACT_CERTIFICATE = 'edit_contract_certificate';
    public const RECORD_ACTION_EDIT_CONTRACT_INVOICE = 'edit_contract_invoice';
    public const RECORD_ACTION_EDIT_PARENT_CONTRACT = 'edit_parent_contract';
    public const RECORD_ACTION_SELECT_LINK_INSPECTION = 'select_inspection';
    public const RECORD_ACTION_EDIT_FIN_ACCOUNT = 'edit_fin_account';

    //Variation
    public const RECORD_ACTION_APPROVE_INSTRUCTION_VARIATION = 'approve_instruction_variation';
    public const RECORD_LABEL_APPROVE_INSTRUCTION_VARIATION = 'Approve';
    public const RECORD_ACTION_REJECT_INSTRUCTION_VARIATION = 'reject_instruction_variation';
    public const RECORD_LABEL_REJECT_INSTRUCTION_VARIATION = 'Reject';
    public const RECORD_ACTION_VIEW_INSTRUCTION_VARIATION = 'view';

    // Budgets
    public const RECORD_ACTION_FINANCE_LIST = 'finance_list';
    public const RECORD_LABEL_FINANCE_LIST = 'Finance';
    public const RECORD_ACTION_CONTRACT_LIST = 'contract_list';
    public const RECORD_LABEL_CONTRACT_LIST = 'Contract';

    // Estate
    public const RECORD_ACTION_EDIT_SALES_INVOICE = 'edit_sales_invoice';
    public const REPEAT_ACTION_LABEL = 'Repeat';
    public const RECORD_ACTION_EDIT_RECORD_CODE = 'edit_estate_code';
    public const RECORD_ACTION_EDIT_RECORD_ESTABLISHMENTS = 'edit_estate_establishments';
    public const RECORD_ACTION_EDIT_QST_ACTION = 'edit_questionnaire_action';

    //CM
    public const RECORD_ACTION_EDIT_CM = 'edit_cm';

    // Freehold
    public const RECORD_ACTION_VIEW_FREEHOLD = 'view_freehold';

    // user
    public const RECORD_ACTION_REMOVE_USER = 'remove_user';

    public const RECORD_ACTION_SUMMARY_LEASE_IN = 'summary_lease_in';
    public const RECORD_ACTION_EDIT_GENERAL_CONTACT = 'edit_general_contact';
    public const RECORD_ACTION_EDIT_GENERAL_PAYEE_CONTACT = 'edit_general_payee_contact';

    public const RECORD_ACTION_REMOVE_LOCATION = 'remove_location';

    // Helpcall
    public const RECORD_ACTION_EDIT_HELPCALL_CATEGORY = 'edit_helpcall_category';
    public const RECORD_ACTION_REMOVE_HELPCALL_CATEGORY = 'remove_helpcall_category';
    public const RECORD_ACTION_REMOVE_CONTRACT_PTW = 'remove_contract';

    public const RECORD_ACTION_UNLINK_HELPCALL_QST_LABEL = 'Unlink';
    public const RECORD_ACTION_UNLINK_HELPCALL_QST = 'unlink';

    public const RECORD_ACTION_SHOW_TENANT_CONTACT = 'show_tenant_contact';

    public const RECORD_ACTION_EDIT_OR_SUMMARY = 'edit_or_summary';

    public const RECORD_ACTION_SELECT = 'select';
    public const SELECT_ACTION_LABEL = 'Select';
    public const RECORD_ACTION_VIEW_ACQUISITION = 'view_acquisition';
    public const RECORD_ACTION_SUMMARY_LEASE_OUT = 'summary_lease_out';
    public const RECORD_ACTION_EDIT_ESTATE_SITE_FIELD = 'edit_estate_site_field';

    public const RECORD_ACTION_RE_LET = 're-let';
    public const RECORD_ACTION_RE_LET_LABEL = 'Re-let';
    public const RECORD_ACTION_REMOVE_LEASE_OUT = 'remove-lease-out';
    public const ADD_ACTION_LABEL = 'Add';

    public const RECORD_ACTION_REMOVE_OCCUPANCY_UNIT = 'remove_occupancy_unit';

    public const RECORD_ACTION_REMOVE_CHARGE_RENT = 'remove_charge_rent';
    public const RECORD_ACTION_EDIT_CHARGE_RENT = 'edit_charge_rent';

    public const RECORD_ACTION_ADD_ALIENATION_CHILD = 'add_alienation_child';
    public const RECORD_ACTION_SELECT_LEASE_OUT = 'select-lease-out';

    public const RECORD_ACTION_DOWNLOAD_BACKGROUND_REPORT = 'download_background_report';
    public const RECORD_ACTION_RUN_SCHEDULED_REPORT = 'run_scheduled_report';
    public const RECORD_ACTION_RUN_REPORT = 'run_report';
    public const RECORD_ACTION_CUSTOMIZE_REPORT = 'customize_report';
    public const RECORD_ACTION_CUSTOMIZE_LABEL = 'Customise';
    public const RECORD_ACTION_RUN_LABEL = 'Run';
    public const RECORD_ACTION_CHANGE_REPORT_AVAILABILITY = 'change_report_availability';
    public const RECORD_ACTION_CHANGE_REPORT_FAVOURITE = 'change_report_favourite';

    public const RECORD_ACTION_EDIT_DLO_JOB = 'edit-dlo-job';

    // NHS clean audit
    public const RECORD_ACTION_EDIT_NHS_CLEAN_AUDIT = 'edit_nhs_clean_audit';
    public const RECORD_ACTION_REMOVE_FUNC_AREA_ROOM = 'Remove';

    public const VALIDATE_ACTION_KEY = 'validate';
    public const VALIDATE_ACTION_LABEL = 'Validate';
    public const LOAD_ACTION_KEY = 'load';
    public const LOAD_ACTION_LABEL = 'Load';

    // SFG20
    public const RECORD_ACTION_EDIT_SFG20_INSPECTION = 'edit_sfg20_inspection';
    public const RECORD_ACTION_EDIT_HELPCALL = 'edit_help_call';
    public const RECORD_ACTION_CANCEL_BACKGROUND_PROCESS = 'cancel_background_process';
    public const RECORD_ACTION_EDIT_COST_PLUS_CONTRACT_SALE_INVOICE = 'edit_sale_invoice';

    public const RECORD_ACTION_EDIT_JOB_INSTRUCTION = 'edit_job_instruction';
    public const RECORD_ACTION_EDIT_INVOICE = 'edit_invoice';
}
