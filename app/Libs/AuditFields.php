<?php

namespace App\Libs;

use Illuminate\Support\Arr;

class AuditFields
{
    private $model = null;
    private $fields = null;

    /**
     *
     * @param object $model Eloquent model e.g. Site, Building, etc.  It is expected that the model properties will
     * have already been updated from the input fields array.
     */
    public function __construct($model)
    {
        $this->model = $model;
        $this->fields = [];
    }

    /**
     * Get audit field array.  This is in the format expected by the AudtService.
     * @return array
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Add audit field entry.
     * @param string $fieldLabel This is normally the for field label text, and is used to indicate
     * which field has changed.
     * @param string $fieldName Model/form field e.g. site_code, site_desc, etc.
     * @param array $options input array.  See AuditService for available options.
     * @return \App\Libs\AuditFields
     */
    public function addField($fieldLabel, $fieldName, array $options = [])
    {
        $original   = '';
        $new        = '';
        if (is_array($this->model->getOriginal())) {
            $original = Arr::get($this->model->getOriginal(), $fieldName);
        }
        if (is_array($this->model->getAttributes())) {
            $new = Arr::get($this->model->getAttributes(), $fieldName);
        }
        $this->fields[] = array_merge([
            'field' => $fieldLabel,
            'original' => $original,
            'new' => $new
        ], $options);

        return $this;
    }
}
