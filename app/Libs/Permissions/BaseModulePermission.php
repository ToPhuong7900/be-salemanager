<?php

namespace App\Libs\Permissions;

use App\Libs\Common\Common;
use App\Libs\Common\CommonConstant;
use App\Models\CrudAccessLevel;
use App\Models\SiteGroup;
use App\Models\Store;
use App\Models\User;
use App\Models\UserAccess;
use App\Models\UserGroup;
use App\Models\UserGroupModule;
use Illuminate\Database\Eloquent\Collection;

abstract class BaseModulePermission implements BaseModulePermissionContract
{
    protected array $siteGroupSelectedFields = ['*'];
    protected array $userGroupSelectedFields = ['*'];

    protected ?User $user = null;
    protected ?bool $isAdministrator = null;
    protected int $crudLevel = CrudAccessLevel::CRUD_ACCESS_LEVEL_NONE;
    protected Collection $userGroupModules;

    private ?UserGroup $userGroup = null;
    private ?SiteGroup $siteGroup = null;
    private ?bool $isSuperuser = null;
    private bool $moduleAccess = false;
    private bool $isSiteAccess = true;
    private ?int $storeId = null;

    public function getSiteGroup(): ?SiteGroup
    {
        return $this->siteGroup;
    }

    public function getUserGroup(): ?UserGroup
    {
        return $this->userGroup;
    }
    public function isAdministrator(): ?bool
    {
        return $this->isAdministrator;
    }

    public function isSupervisor(): ?bool
    {
        return $this->crudLevel == CrudAccessLevel::CRUD_ACCESS_LEVEL_SUPERVISOR;
    }

    public function isSuperuser(): ?bool
    {
        return $this->isSuperuser;
    }

    public function isEnableSiteGroupField(string $siteGroupField): bool
    {
        return $this->siteGroup && Common::valueYorNtoBoolean($this->siteGroup->getAttribute($siteGroupField));
    }

    public function setUser(User $user): static
    {
        if (!$this->user || $this->user->getKey() != $user->getKey()) {
            $this->user = $user;
            $this->siteGroup = SiteGroup::query()
                ->where('site_group_id', $user->site_group_id)->first($this->siteGroupSelectedFields);
            $this->userGroup = Usergroup::query()
                ->where('user_group_id', $user->usergroup_id)->first($this->userGroupSelectedFields);
            $this->isSuperuser = $user->superuser === CommonConstant::DATABASE_VALUE_YES;
            $this->isAdministrator = $user->administrator === CommonConstant::DATABASE_VALUE_YES;

            if ($this->user->hasElevatedPermission()) {
                $this->moduleAccess = true;
            } else {
                $this->userGroupModules = UsergroupModule::select([
                    'user_group_module.*',
                    'user_group.site_group_id'
                ])
                    ->join('user_group', 'user_group.user_group_id', '=', 'user_group_module.user_group_id')
                    ->where('user_group_module.user_group_id', $this->user->getAttribute('user_group_id'))
                    ->where('user_group.site_group_id', $this->user->getAttribute('site_group_id'))
                    ->where('user_group_module.crud_access_level_id', '>', CrudAccessLevel::CRUD_ACCESS_LEVEL_NONE)
                    ->get();
                $this->moduleAccess = $this->userGroupModules->whereIn('module_id', $this->getAccessModules())
                        ->count() == count($this->getAccessModules());

                if ($this->moduleAccess) {
                    if ($this->user->isSuperuser()) {
                        $this->crudLevel = CrudAccessLevel::CRUD_ACCESS_LEVEL_SUPERVISOR;
                    } elseif (
                        $mainUserGroupModule = $this->userGroupModules->firstWhere(
                            'module_id',
                            $this->getMainModuleId()
                        )
                    ) {
                        $this->crudLevel = $mainUserGroupModule->crud_access_level_id;
                    }
                }
            }
        }
        return $this;
    }

    public function canAssessStore(?int $storeId = null): bool
    {
        if (is_null($storeId)) {
            return true;
        }

        if (
            $this->user && $storeId && (!$this->storeId || $this->storeId != $storeId) &&
            Store::userSiteGroup()->where('site_id', $storeId)->exists()
        ) {
            $this->storeId = $storeId;
            $this->isSiteAccess = match ($this->user->user_store_access_id) {
                UserAccess::USER_ACCESS_ALL => true,
                UserAccess::USER_ACCESS_SELECTED => $this->user->userStoreAccesses()
                    ->where('store_id', $storeId)->exists(),
                default => false,
            };
        }
        return $this->isSiteAccess;
    }

    public function canAccessModule(): bool
    {
        return $this->moduleAccess;
    }

    public function canAccessRecord(?int $siteId = null): bool
    {
        return $this->canAccessModule() && $this->canAssessStore($siteId);
    }

    public function canCreate(): bool
    {
        return $this->crudLevel === CrudAccessLevel::CRUD_ACCESS_LEVEL_CREATE;
    }

    public function canUpdate(?int $siteId = null): bool
    {
        return $this->crudLevel === CrudAccessLevel::CRUD_ACCESS_LEVEL_UPDATE && $this->canAssessStore($siteId);
    }

    public function canDelete(?int $siteId = null): bool
    {
        return $this->crudLevel === CrudAccessLevel::CRUD_ACCESS_LEVEL_DELETE && $this->canAssessStore($siteId);
    }

    public function setElevatedPermission($isElevated = true): static
    {
        $this->user->elevatedPermission($isElevated);
        return $this;
    }

    public function canAccessToModule(
        array|int $moduleIds,
        array|int $crudLevels = CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
    ): bool {
        if (!$this->user->hasElevatedPermission()) {
            $moduleIds = is_array($moduleIds) ? $moduleIds : [$moduleIds];
            $crudLevels = is_array($crudLevels) ? $crudLevels : [$crudLevels];

            $userGroupModules = $this->userGroupModules->whereIn('module_id', $moduleIds)->values();

            if (
                count($userGroupModules) !== count($moduleIds) ||
                count($userGroupModules) !== count($crudLevels) && count($crudLevels) != 1
            ) {
                return false;
            }

            foreach ($userGroupModules as $i => $userGroupModule) {
                if ($userGroupModule->crud_access_level_id < $crudLevels[$i]) {
                    return false;
                }
            }
        }

        return true;
    }

//    public function isDataLoad(): ?bool
//    {
//        return $this->user->getSourceController() == User::SOURCE_DATA_LOAD;
//    }

    /**
     * Legacy business moving from \Tfcloud\Services\PermissionService::hasRestrictionBasedOnUserGroupType
     * @return bool
     */
//    public function hasRestrictionBasedOnUserGroupType(): bool
//    {
//        // Used to restrict access to notes and documents
//        $userGroupTypeId = $this->userGroup->usergroup_type_id;
//
//        // Mobile user can see all, based on Chris comment on Ticket CLD-2936, Comment ID: #15625
//        return  $userGroupTypeId === UsergroupType::SELF_SERVICE   ||
//            $userGroupTypeId === UsergroupType::SELF_SERVICE_RESOURCE_BOOKING   ||
//            $userGroupTypeId === UsergroupType::CONTRACTORS;
//    }

    /**
     * @return int[]
     */
    abstract protected function getAccessModules(): array;

    abstract protected function getMainModuleId(): int;
}
