<?php

namespace App\Libs\Permissions;

use App\Models\Module;

class ProductPermission extends BaseModulePermission
{

    protected function getAccessModules(): array
    {
        return [
            Module::MODULE_PRODUCT
        ];
    }

    protected function getMainModuleId(): int
    {
        return  Module::MODULE_PRODUCT;
    }
}
