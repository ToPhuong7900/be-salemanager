<?php

namespace App\Libs\Permissions;

use App\Models\CrudAccessLevel;
use App\Models\UserGroup;
use App\Models\SiteGroup;
use App\Models\User;

interface BaseModulePermissionContract
{
    public function setUser(User $user): static;
    public function setElevatedPermission($isElevated = true): static;

    public function isSupervisor(): ?bool;
    public function isAdministrator(): ?bool;
    public function isSuperuser(): ?bool;
    public function getSiteGroup(): ?SiteGroup;
    public function getUserGroup(): ?UserGroup;
    public function isEnableSiteGroupField(string $siteGroupField): bool;
    public function canAccessModule(): bool;
    public function canAssessStore(?int $storeId = null): bool;
    public function canAccessRecord(?int $siteId = null): bool;
    public function canCreate(): bool;
    public function canUpdate(?int $siteId = null): bool;
    public function canDelete(?int $siteId = null): bool;
    public function canAccessToModule(
        int|array $moduleIds,
        int|array $crudLevels = CrudAccessLevel::CRUD_ACCESS_LEVEL_READ
    ): bool;
//    public function isDataLoad(): ?bool;
//    public function hasRestrictionBasedOnUserGroupType(): bool;
}
