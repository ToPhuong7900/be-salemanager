<?php

namespace App\Libs\Permissions;

interface IPermissionRelationship
{
    public function site();
    public function getSiteId(): int|null;
    public function getChildRelationships(): array;
}
