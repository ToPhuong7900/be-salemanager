<?php

namespace App\Libs\BoxFilterQuery\Helper;

class RecordHelper
{
    /**
     * @param Object $item
     * @param array $class
     * @return string
     */
    public static function addRowClass(object $item, array $class)
    {
        if (empty($class)) {
            return '';
        }

        if (count($class) !== count($class, COUNT_RECURSIVE)) {
            $class = array_flatten($class);
        }

        $classString = implode(' ', $class);
        $item->setAttribute('rowClass', $classString);
    }

    /**
     * @param $path
     * @param $parameters
     * @return string
     */
    public static function generateUrlFromParameters(string $path, array $parameters)
    {
        foreach ($parameters as $parameterName => $value) {
            $path = str_replace($parameterName, $value, $path);
        }
        return '/' . $path;
    }

    public static function makeObjectCell($text, $style = '', $class = '')
    {
        return [
            'text' => $text,
            'style' => $style,
            'class' => $class,
        ];
    }
}
