<?php

namespace App\Libs\BoxFilterQuery\Helper;

use App\Libs\BoxFilter\Constants\RecordActionConstant;
use Tfcloud\Models\CrudAccessLevel;

class RecordAction
{
    /**
     * @var string
     */
    private $actionCode;

    /**
     * @var string[]
     */
    private $parameterNames = [];

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $actionLabel;

    /**
     * @var integer
     */
    private $crudLevel;


    /**
     * @var string
     */
    private $template;

    /**
     * RecordAction constructor.
     * @param string $actionCode
     * @param string $url
     * @param string $actionLabel
     * @param int $crudLevel
     * @param string $template
     */
    public function __construct(
        string $actionCode,
        string $url,
        string $actionLabel = '',
        int $crudLevel = 0,
        string $template = ''
    ) {
        $this->actionCode = $actionCode;
        $this->setUrlParameters($url);
        $this->setDefaultActionLabel($actionCode, $actionLabel);
        $this->setCRUDLevel($actionCode, $crudLevel);
        $this->template = $template;
    }

    /**
     * @return string
     */
    public function getActionCode()
    {
        return $this->actionCode;
    }

    /**
     * @return string[]
     */
    public function getParameterNames()
    {
        return $this->parameterNames;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getActionLabel()
    {
        return $this->actionLabel;
    }

    /**
     * @param string $actionLabel
     * @return RecordAction
     */
    public function setActionLabel(string $actionLabel)
    {
        $this->actionLabel = $actionLabel;
        return $this;
    }

    /**
     * @return int
     */
    public function getCrudLevel()
    {
        return $this->crudLevel;
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    private function setUrlParameters(string $url)
    {
        $this->url = $url;
        preg_match_all('/\{\w+\}/', $url, $parameterNames);
        $this->parameterNames = array_flatten($parameterNames);
    }

    private function setCRUDLevel(string $actionCode, int $level = 0)
    {
        if (!empty($level)) {
            $this->crudLevel = $level;
        } else {
            switch ($actionCode) {
                case RecordActionConstant::EDIT_ACTION_KEY:
                    $this->crudLevel = CrudAccessLevel::CRUD_ACCESS_LEVEL_UPDATE;
                    break;
                case RecordActionConstant::DELETE_ACTION_KEY:
                    $this->crudLevel = CrudAccessLevel::CRUD_ACCESS_LEVEL_DELETE;
                    break;
                case RecordActionConstant::COPY_ACTION_KEY:
                    $this->crudLevel = CrudAccessLevel::CRUD_ACCESS_LEVEL_CREATE;
                    break;
                default:
                    $this->crudLevel = 0;
                    break;
            }
        }
    }

    private function setDefaultActionLabel(string $actionCode, string $label = '')
    {
        $this->actionLabel = !empty($label) ? $label :
            array_get(RecordActionConstant::DEFAULT_ACTION_LABELS, $actionCode, $label);
    }
}
