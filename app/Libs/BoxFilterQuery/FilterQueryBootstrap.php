<?php

namespace App\Libs\BoxFilterQuery;

use App\Libs\BoxFilter\IBoxFilter;
use App\Libs\BoxFilterQuery\ClassMap\ClassMapModuleLoader;
use App\Libs\Exceptions\StateException;

class FilterQueryBootstrap
{
    /**
     * @param mixed $filterNameId
     * @return array
     * @throws StateException
     */
    public static function getFilterNameClassMap($filterNameId)
    {
        $classMaps = ClassMapModuleLoader::getClassMaps();
        $filterNameClassMap = array_get($classMaps, $filterNameId);
        if (count($filterNameClassMap ?? []) > 1) {
            return $filterNameClassMap;
        }
        throw new StateException("Invalid filter name id: $filterNameId.");
    }

    /**
     * @param $filterNameId
     * @param array $inputs
     * @return IFilterQuery
     * @throws StateException
     */
    public static function getFilterQuery($filterNameId, array $inputs)
    {
        $filterNameClassMap = self::getFilterNameClassMap($filterNameId);
        $filterQuery = $filterNameClassMap[0] ? new $filterNameClassMap[0]() : null;
        $boxFilter = $filterNameClassMap[1] ? new $filterNameClassMap[1]($filterNameId) : null;
        if ($filterQuery instanceof IFilterQuery && $boxFilter instanceof IBoxFilter) {
            $boxFilter->setFilterElements();
            $filterQuery->init($filterNameId, $boxFilter->getFilterSet(), $inputs);
            return $filterQuery;
        }
        throw new StateException("filterQuery must be instance of IFilterQuery");
    }

    /**
     * @param $filterNameId
     * @param $kpiID
     * @param array $inputs
     * @return IChartFilterQuery
     * @throws StateException
     */
    public static function getKpiFilterQuery($filterNameId, $kpiID, array $inputs)
    {
        list($filterQuery, $boxFilter) = self::getFilterNameClassMap($filterNameId);
        $filterQuery = $filterQuery ? new $filterQuery() : null;
        $boxFilter = $boxFilter ? new $boxFilter($filterNameId) : null;
        if ($filterQuery instanceof IKpiFilterQuery && $boxFilter instanceof IBoxFilter) {
            $boxFilter->setFilterElements();
            $chartFilterQuery = $filterQuery->getChartFilterQuery($kpiID);
            $chartFilterQuery->init($filterNameId, $boxFilter->getFilterSet(), $inputs);
            return $chartFilterQuery;
        }
        throw new StateException("filterQuery must be instance of IKpiFilterQuery");
    }
}
