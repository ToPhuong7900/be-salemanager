<?php

namespace App\Libs\BoxFilterQuery;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use App\Libs\BoxFilterQuery\FilterFieldSet\IFilterSet;
use App\Libs\BoxFilterQuery\Parsers\IOperatorParser;
use App\Libs\BoxFilterQuery\Parsers\IRecordParser;

interface IFilterQuery extends IBaseFilterQuery
{
    /**
     * @param $filterNameId
     * @param IFilterSet $filterSet
     * @param array $inputs
     * @param IRecordParser|null $recordParser
     * @param IOperatorParser|null $operatorParser
     * @return IFilterQuery
     */
    public function init(
        $filterNameId,
        IFilterSet $filterSet,
        array $inputs,
        IOperatorParser $operatorParser = null,
        IRecordParser $recordParser = null
    );
    /**
     * @param array $inputs
     * @return IFilterQuery
     */
    public function setSortInputs(array $inputs);

    /**
     * @param IRecordParser $recordParser
     * @return IFilterQuery
     */
    public function setRecordParser(IRecordParser $recordParser);

    /**
     * @param integer $pagePer
     * @return LengthAwarePaginator
     */
    public function getAll(int $pagePer);

    /**
     * @return bool
     */
    public function canAccess();

    /**
     * @return array
     */
    public function getTableFooterData();

    /**
     * @return array
     */
    public function getFormattedSortFields();

    /**
     * @return string
     */
    public function getSortField();

    /**
     * @return string
     */
    public function getSortType();

    /**
     * @return IFilterSet
     */
    public function getFilterSet();

    /**
     * @return array
     */
    public function getTableMessageData();
}
