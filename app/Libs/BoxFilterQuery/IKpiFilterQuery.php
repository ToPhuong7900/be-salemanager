<?php

namespace App\Libs\BoxFilterQuery;

use App\Libs\Exceptions\StateException;

interface IKpiFilterQuery
{
    /**
     * @param $kpiId
     * @return IChartFilterQuery
     * @throws StateException
     */
    public function getChartFilterQuery($kpiId);
}
