<?php

namespace App\Libs\BoxFilterQuery\ClassMap;

use App\Models\Module;

class ClassMapModuleLoader
{
    public const MODULE_REGISTER = [
        Module::MODULE_INVENTORY => GenBoxFilterClassMap::FILTER_NAME_CLASS_MAP,
    ];

//    public const EMAIL_CLASS_MAP = [EmailFilterQuery::class, EmailBoxFilter::class];
//    public const AUDIT_CLASS_MAP = [AuditFilterQuery::class, AuditBoxFilter::class];
//    public const DOCUMENT_CLASS_MAP = [DocumentFilterQuery::class, DocumentBoxFilter::class];
//    public const NOTES_CLASS_MAP = [NotesFilterQuery::class, NotesBoxFilter::class];
//    public const AUDIT_NO_USER_CLASS_MAP = [AuditNoUserFilterQuery::class, AuditNoUserBoxFilter::class];
//    public const WORK_REQUIRE_CLASS_MAP = [WorkRequireFilterQuery::class, WorkRequireBoxFilter::class];

    public static function getClassMaps()
    {
        $arr = [];
        foreach (self::MODULE_REGISTER as $moduleId => $classMap) {
            $arr += $classMap;
        }
        return $arr;
    }
}
