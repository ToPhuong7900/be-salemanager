<?php

namespace App\Libs\BoxFilterQuery\ClassMap;

use App\Libs\BoxFilter\IBoxFilterReport;
use App\Libs\BoxFilterQuery\IReportFilterQuery;
use App\Libs\Exceptions\StateException;
use Tfcloud\Models\Module;
use Tfcloud\Models\Report;
use App\SBoxFilter\Asbestos\AsbReportBoxFilterClassMap;
use App\SBoxFilter\CaseManagement\CaseManagementReportBoxFilterClassMap;
use App\SBoxFilter\Cleaning\CleaningReportBoxFilterClassMap;
use App\SBoxFilter\Condition\ConditionReportClassMap;
use App\SBoxFilter\CostPlus\CostPlusReportBoxFilterClassMap;
use App\SBoxFilter\Estate\EstateReportBoxFilterClassMap;
use App\SBoxFilter\DLO\DLOReportBoxFilterClassMap;
use App\SBoxFilter\FixedAsset\FixedAssetReportBoxFilterClassMap;
use App\SBoxFilter\General\GenReportBoxFilterClassMap;
use App\SBoxFilter\Helpcall\HelpReportBoxFilterClassMap;
use App\SBoxFilter\Incident\IncidentReportFilterClassMap;
use App\SBoxFilter\Contract\ContractReportBoxFilterClassMap;
use App\SBoxFilter\InspectionSFG20\InspectionSFG20ReportBoxFilterClassMap;
use App\SBoxFilter\Instruction\InstructionReportBoxFilterClassMap;
use App\SBoxFilter\Invoice\InvoiceReportBoxFilterClassMap;
use App\SBoxFilter\NhsCleaning\NhsCleanReportBoxFilterClassMap;
use App\SBoxFilter\Occupancy\OccupancyReportClassMap;
use App\SBoxFilter\PermitsToWork\PermitsToWorkReportBoxFilterClassMap;
use App\SBoxFilter\Property\PropertyReportBoxFilterClassMap;
use App\SBoxFilter\Project\PrjReportBoxFilterClassMap;
use App\SBoxFilter\Questionnaire\QuestionnaireReportClassMap;
use App\SBoxFilter\Risk\RiskReportBoxFilterClassMap;
use App\SBoxFilter\Rural\RuralReportBoxFilterClassMap;
use App\SBoxFilter\Stock\StockReportBoxFilterClassMap;
use App\SBoxFilter\Tree\StoreReportBoxFilterClassMap;
use App\SBoxFilter\Utility\UtilityReportBoxFilterClassMap;
use App\SBoxFilter\WaterManagement\WaterManagementReportFilterClassMap;

class ClassMapReportLoader
{
    private const REPORT_WHITE_LIST_BY_MODULE = [
        Module::MODULE_PROJECT => PrjReportBoxFilterClassMap::PROJECT_REPORT_CLASS_MAP,
        Module::MODULE_ASBESTOS => AsbReportBoxFilterClassMap::ASBESTOS_REPORT_CLASS_MAP,
        Module::MODULE_CAPITAL_ACCOUNTING => FixedAssetReportBoxFilterClassMap::FIXED_ASSET_REPORT_CLASS_MAP,
        Module::MODULE_CLEANING => CleaningReportBoxFilterClassMap::CLEANING_REPORT_CLASS_MAP,
        Module::MODULE_NHS_CLEANING => NhsCleanReportBoxFilterClassMap::NHS_CLEANING_REPORT_CLASS_MAP,
        Module::MODULE_GENERAL => GenReportBoxFilterClassMap::GEN_REPORT_CLASS_MAP,
        Module::MODULE_HELP => HelpReportBoxFilterClassMap::HELPCALL_REPORT_CLASS_MAP,
        Module::MODULE_DLO => DLOReportBoxFilterClassMap::DLO_REPORT_CLASS_MAP,
        Module::MODULE_INSPECTION => InspectionSFG20ReportBoxFilterClassMap::INSPECTION_SFG20_REPORT_CLASS_MAP,
        Module::MODULE_PROPERTY => PropertyReportBoxFilterClassMap::PROPERTY_REPORT_CLASS_MAP,
        Module::MODULE_PLANT => PropertyReportBoxFilterClassMap::PROPERTY_REPORT_CLASS_MAP,
        Module::MODULE_TREE => StoreReportBoxFilterClassMap::TREE_REPORT_CLASS_MAP,
        Module::MODULE_CONDITION => ConditionReportClassMap::CONDITION_REPORT_CLASS_MAP,
        Module::MODULE_UTILITY => UtilityReportBoxFilterClassMap::UTILITY_REPORT_CLASS_MAP,
        Module::MODULE_RISK_ASSESSMENT => RiskReportBoxFilterClassMap::RISK_REPORT_CLASS_MAP,
        Module::MODULE_INSTRUCTIONS => InstructionReportBoxFilterClassMap::INSTRUCTION_REPORT_CLASS_MAP,
        Module::MODULE_CONTRACT => ContractReportBoxFilterClassMap::CONTRACT_REPORT_CLASS_MAP,
        Module::MODULE_QST => QuestionnaireReportClassMap::QUESTIONNAIRE_REPORT_CLASS_MAP,
        Module::MODULE_QST_GENERAL => QuestionnaireReportClassMap::QUESTIONNAIRE_GENERAL_REPORT_CLASS_MAP,
        Module::MODULE_QST_CORE_FACTS => QuestionnaireReportClassMap::QUESTIONNAIRE_CORE_FACTS_REPORT_CLASS_MAP,
        Module::MODULE_QST_COVID_19 => QuestionnaireReportClassMap::QUESTIONNAIRE_COVID19_REPORT_CLASS_MAP,
        Module::MODULE_QST_DAA => QuestionnaireReportClassMap::QUESTIONNAIRE_DAA_REPORT_CLASS_MAP,
        Module::MODULE_QST_FIRE => QuestionnaireReportClassMap::QUESTIONNAIRE_FIRE_REPORT_CLASS_MAP,
        Module::MODULE_QST_LEGIONELLA => QuestionnaireReportClassMap::QUESTIONNAIRE_LEGIONELLA_REPORT_CLASS_MAP,
        Module::MODULE_INVOICING => InvoiceReportBoxFilterClassMap::INVOICE_REPORT_CLASS_MAP,
        Module::MODULE_ESTATES => EstateReportBoxFilterClassMap::ESTATE_REPORT_CLASS_MAP,
        Module::MODULE_RURAL_ESTATES => RuralReportBoxFilterClassMap::RURAL_REPORT_CLASS_MAP,
        Module::MODULE_PERMIT_TO_WORK => PermitsToWorkReportBoxFilterClassMap::PERMIT_TO_WORK_REPORT_CLASS_MAP,
        Module::MODULE_CASE_MANAGEMENT => CaseManagementReportBoxFilterClassMap::CASEMGMT_REPORT_CLASS_MAP,
        Module::MODULE_CASE_MANAGEMENT_2 => CaseManagementReportBoxFilterClassMap::CASEMGMT2_REPORT_CLASS_MAP,
        Module::MODULE_CASE_MANAGEMENT_3 => CaseManagementReportBoxFilterClassMap::CASEMGMT3_REPORT_CLASS_MAP,
        Module::MODULE_CASE_MANAGEMENT_4 => CaseManagementReportBoxFilterClassMap::CASEMGMT4_REPORT_CLASS_MAP,
        Module::MODULE_CASE_MANAGEMENT_5 => CaseManagementReportBoxFilterClassMap::CASEMGMT5_REPORT_CLASS_MAP,
        Module::MODULE_OCCUPANCY => OccupancyReportClassMap::OCCUPANCY_REPORT_CLASS_MAP,
        Module::MODULE_WATER_MANAGEMENT => WaterManagementReportFilterClassMap::WATER_MANAGEMENT_REPORT_CLASS_MAP,
        Module::MODULE_INCIDENT_MANAGEMENT => IncidentReportFilterClassMap::INCIDENT_MANAGEMENT_REPORT_CLASS_MAP,
        Module::MODULE_STOCK => StockReportBoxFilterClassMap::STOCK_REPORT_CLASS_MAP,
        Module::MODULE_COST_PLUS => CostPlusReportBoxFilterClassMap::COST_PLUS_REPORT_CLASS_MAP,
    ];

    /**
     * @var Report
     */
    protected $report;

    /**
     * @var int
     */
    protected $filterNameId;

    /**
     * @var string
     */
    protected $reportBoxFilterClass;

    /**
     * @var string
     */
    protected $reportFilterQueryClass;

    /**
     * @var bool
     */
    private $hasBoxFilter = false;

    public function __construct(Report $report)
    {
        $this->loadClass($report);
    }

    public function hasBoxFilter()
    {
        return $this->hasBoxFilter;
    }

    public function getReport()
    {
        return $this->report;
    }

    public function getBoxFilterReport()
    {
        if ($this->reportBoxFilterClass) {
            $boxFilter = new $this->reportBoxFilterClass($this->report, $this->filterNameId);
            if ($boxFilter instanceof IBoxFilterReport) {
                return $boxFilter;
            }
        }
        throw new StateException();
    }

    public function getFilterQuery($inputs = [])
    {
        if ($this->reportBoxFilterClass) {
            $filterQuery = new $this->reportFilterQueryClass();
            if ($filterQuery instanceof IReportFilterQuery) {
                $boxFilter = $this->getBoxFilterReport();
                $boxFilter->setFilterElements();
                $filterQuery->init($this->filterNameId, $boxFilter->getFilterSet(), $inputs);
                return $filterQuery;
            }
        }
        throw new StateException();
    }

    private function loadClass(Report $report)
    {
        $moduleId = $this->formatModuleId($report->getRepModuleId());
        if ($reportBoxFilterClassMap = array_get(self::REPORT_WHITE_LIST_BY_MODULE, $moduleId)) {
            if (!empty($reportBoxFilterClassMap)) {
                $systemReportCode = $report->getSystemReportCode();
                $classMaps = array_get($reportBoxFilterClassMap, $systemReportCode, []);
                if (count($classMaps) > 2) {
                    $this->hasBoxFilter = true;
                    $this->report = $report;
                    list($this->filterNameId, $this->reportBoxFilterClass, $this->reportFilterQueryClass) = $classMaps;
                }
            }
        }
    }

    private function formatModuleId($moduleId)
    {
        return is_integer($moduleId) ? $moduleId : -1;
    }
}
