<?php

namespace App\Libs\BoxFilterQuery\Constants;

class FilterQueryConstant
{
    public const FILTER_PARAMETER_KEY = 'flt';
    public const PAGE_PARAMETER_KEY = 'page';
    public const PER_PARAMETER_KEY = 'per';
    public const FILTER_SORT_FIELD_KEY = 'sortName';
    public const FILTER_SORT_TYPE_KEY = 'sortType';
    public const FILTER_OPEN_KEY = 'n';
    public const FILTER_RELATED_RESTRICT_DATA_KEY = 'o';
    public const FILTER_SORT_ASC_TYPE = 'ASC';
    public const FILTER_SORT_DESC_TYPE = 'DESC';
    public const FILTER_SORT_ASC_TYPE_TEXT = 'Ascending';
    public const FILTER_SORT_DESC_TYPE_TEXT = 'Descending';
    public const NOT_APPLY_RECALL_FILTER_KEY = 'filterAll';
    public const REQUEST_EMPTY_FILTER_KEY = 'emptyFilter';
    public const LIMIT_CONFIG_DEFAULT = 10;
}
