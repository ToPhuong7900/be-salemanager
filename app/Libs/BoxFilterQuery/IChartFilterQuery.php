<?php

namespace App\Libs\BoxFilterQuery;

interface IChartFilterQuery extends IBaseFilterQuery
{
    /**
     * @return array
     */
    public function getKPIData();

    /**
     * @return bool
     */
    public function canAccess();

    /**
     * @return bool
     */
    public function isManagementOnly(): bool;

    /**
     * @param bool $isManagementOnly
     * @return IChartFilterQuery
     */
    public function setIsManagementOnly(bool $isManagementOnly): IChartFilterQuery;
}
