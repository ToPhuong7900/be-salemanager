<?php

namespace App\Libs\BoxFilterQuery;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Support\Collection;
use App\Libs\BoxFilterQuery\Constants\FilterQueryConstant;
use App\Libs\BoxFilterQuery\FilterFieldSet\IFilterSet;
use App\Libs\BoxFilterQuery\Helper\RecordAction;
use App\Libs\BoxFilterQuery\Parsers\IOperatorParser;
use App\Libs\BoxFilterQuery\Parsers\IRecordParser;
use App\Libs\BoxFilterQuery\Parsers\OperatorParser;
use App\Libs\BoxFilterQuery\Parsers\RecordParser;
use Tfcloud\Models\CrudAccessLevel;
use Tfcloud\Services\PermissionService;

abstract class FilterQuery implements IFilterQuery
{
    use FilterQueryTrait;

    protected const RECORD_ACTIONS_KEY = 'recordActions';

    /**
     * @var PermissionService
     */
    protected $permissionService = null;

    /**
     * @var int
     */
    protected $minimumAccessLevel = CrudAccessLevel::CRUD_ACCESS_LEVEL_READ;

    /**
     * Define default sort field
     * @var string|null
     */
    protected $defaultSortField = null;

    /**
     * @var string
     */
    protected $defaultSortType = FilterQueryConstant::FILTER_SORT_ASC_TYPE;

    /**
     * Define an array of field name that we want to apply sql function on for formatting or calculating in DB
     * @dataProvider
     * [
     *  'field_name' => ['sql_func', 'argument_1', 'argument_2', ...'argument_n']
     * ]
     * @example ['created_date' => ['DATE_FORMAT', '%Y-%m-%d']]
     * Function name is always at index[0], Arguments start from index[1,2,3,..]
     * @var array
     */
    protected $sqlFunctionFields = [];

    /**
     * Define an array of sort field name from app/lib/BoxFilter/TableOptions/Header.php,
     * That we want to apply multiple sorted fields on DB
     * @dataProvider
     * [
     *  'sort_field_name' => [
     *                          ['field_name_1', 'sort_type'],
     *                          ['field_name_2', 'sort_type'],
     *                          ...['field_name_n', 'sort_type']
     *                      ]
     * ]
     * @example ['document_date' => [['document_date', 'DESC'], ['site_code', 'ASC']]]
     * @var array
     */
    protected $sortFieldMap = [];

    /**
     * @var IRecordParser
     */
    private $recordParser;

        /**
     * @var null|string
     */
    private $sortField = null;

    /**
     * @var null|string
     */
    private $sortType = null;

    /**
     * @var null|string
     */
    private $tableMessageData = null;

    /**
     * @var Collection
     */
    private $tableFooterData;

    public function __construct()
    {
        $this->permissionService = new PermissionService();
    }

    public function init(
        $filterNameId,
        IFilterSet $filterSet,
        array $inputs = [],
        IOperatorParser $operatorParser = null,
        IRecordParser $recordParser = null
    ) {
        $filterName = $this->getFilterName($filterNameId);
        $this->setFilterNameId($filterNameId)
            ->setModuleId($filterName->module_id)
            ->setSortInputs($inputs)
            ->setFilterSet($filterSet, $inputs)
            ->setOperatorParser($operatorParser ?? new OperatorParser(
                $this->getPrefixTables(),
                $this->sqlFunctionFields
            ))
            ->setRecordParser($recordParser ?? new RecordParser($filterName->module_id, $this->getRecordActions()));
        $this->tableFooterData = collect();
        return $this;
    }

    public function setSortInputs($inputs)
    {
        $this->setSortField($inputs)->setSortType($inputs);
        return $this;
    }

    public function canAccess()
    {
        return $this->permissionService->hasModuleAccess($this->getModuleId(), $this->minimumAccessLevel);
    }

    public function setRecordParser(IRecordParser $recordParser)
    {
        $this->recordParser = $recordParser;
        return $this;
    }

    public function getAll(int $pagePer)
    {
        $records = $this->sortQuery($this->filterAll($this->getQuery()))->paginate($pagePer);
        $this->attachRecordAction($records);
        $this->removeAppendedAttributes($records);
        return $this->gotAll($records);
    }

    public function getTableFooterData()
    {
        return $this->tableFooterData->toArray();
    }

    public function getTableMessageData()
    {
        return $this->tableMessageData;
    }

    public function getSortField()
    {
        return $this->sortField;
    }

    public function getSortType()
    {
        return $this->sortType;
    }

    /**
     * Get a main query of a list
     * @return EloquentBuilder|QueryBuilder
     */
    abstract protected function getQuery();

    /**
     * Get prefix table to attach to field when run query
     * @dataProvider
     * [
     *  'table_name' => '*' // apply table_name to all filter field such as project.project_code
     *  'table_name' => 'field_name' // apply table_name to field name such as project_status.project_status_type_id
     *  'table_name' => ['field_name'] // support multiple field names as an array
     * ]
     *
     * @return string[]
     */
    abstract protected function getPrefixTables();

    /**
     * Get a set of action link on record such as edit, delete, copy, site code, etc.
     * @return RecordAction[]
     */
    abstract protected function getRecordActions();

    /**
     * Called after a list of record is paged from getAll() method.
     * Acting as a hook to modify each record such as checking permission or adding dynamic field.
     * @param mixed $records
     * @return mixed
     */
    protected function gotAll($records)
    {
        return $records;
    }

    /**
     * @param EloquentBuilder|QueryBuilder $query
     * @return EloquentBuilder|QueryBuilder
     */
    protected function sortQuery($query)
    {
        if ($this->sortField) {
            if ($sortMaps = array_get($this->sortFieldMap, $this->sortField)) {
                foreach ($sortMaps as $sortMap) {
                    list($sortField, $sortType) = $sortMap;
                    $sortType = $sortType ?? $this->sortType;
                    $query->orderBy(array_get($this->getFormattedSortFields(), $sortField, $sortField), $sortType);
                }
            } else {
                $query->orderBy(
                    array_get($this->getFormattedSortFields(), $this->sortField, $this->sortField),
                    $this->sortType
                );
            }
        }
        return $query;
    }

    /**
     * @param array $urlParameters
     * @return $this
     */

    protected function setSortField(array $urlParameters = [])
    {
        $sortField = array_get($urlParameters, FilterQueryConstant::FILTER_SORT_FIELD_KEY, null);
        $this->sortField = $sortField ?? $this->defaultSortField;
        return $this;
    }

    /**
     * @param array $urlParameters
     * @return $this
     */
    protected function setSortType(array $urlParameters = [])
    {
        $sortType = strtoupper(array_get($urlParameters, FilterQueryConstant::FILTER_SORT_TYPE_KEY, ''));
        $this->sortType = $sortType === FilterQueryConstant::FILTER_SORT_ASC_TYPE ||
        $sortType === FilterQueryConstant::FILTER_SORT_DESC_TYPE ? $sortType : $this->defaultSortType;
        return $this;
    }

    /**
     * @param LengthAwarePaginator $records
     */
    protected function attachRecordAction($records)
    {
        foreach ($records as $record) {
            $this->getRecordKey($record);
            $record->{self::RECORD_ACTIONS_KEY} = $this->recordParser->getAttachedRecordActions($record);
        }
    }

    protected function getRecordKey($record)
    {
        $record->append('UID');
    }

    protected function removeAppendedAttributes($records, $attributes = ['base_64_photo'])
    {
        if ($records->getCollection() instanceof EloquentCollection) {
            $records->makeHidden($attributes);
        }
    }

    /**
     * @param string $fieldName
     * @return string
     */
    protected function getAttachedFieldName(string $fieldName)
    {
        return $this->operatorParser->getAttachedTableName($fieldName);
    }

    /**
     * @param string $rowKey
     * @param string $fieldName
     * @param mixed $value
     */
    protected function addTableFooterData(string $rowKey, string $fieldName, $value = null)
    {
        $values = [$fieldName => $value];
        if ($this->tableFooterData->has($rowKey)) {
            $values = $this->tableFooterData->get($rowKey);
            $values = array_add($values, $fieldName, $value);
        }
        $this->tableFooterData->put($rowKey, $values);
    }

    /**
     * @param mixed $message
     */
    protected function setTableMessageData(string $message)
    {
        $this->tableMessageData = $message;
    }
}
