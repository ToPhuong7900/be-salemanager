<?php

namespace App\Libs\BoxFilterQuery\FilterFieldSet;

interface IFilterField
{
    /**
     * @return string
     */
    public function getFieldName();

    /**
     * @return string
     */
    public function getLabel();

    /**
     * @return string
     */
    public function getFilterElementType();

    /**
     * @return integer[]
     */
    public function getAvailableOperators();

    /**
     * @return IFilterOperator[]
     */
    public function getFilterOperators();

    /**
     * @return IFilterOperator|null
     */
    public function getFilterOperator();

    /**
     * @param IFilterOperator[] $filterOperators
     */
    public function setFilterOperators(array $filterOperators);

    /**
     * @return array|float[]|int[]|string[]
     */
    public function getFilterValues();

    /**
     * Convert IFilterField to array using TfHttp::class
     * @param null|string|int $group
     * @return array
     */
    public function convertToBoxFilterParams($group = null);
}
