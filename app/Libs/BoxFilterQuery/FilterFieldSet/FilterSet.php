<?php

namespace App\Libs\BoxFilterQuery\FilterFieldSet;

use App\Libs\BoxFilter\Constants\OperatorTypeConstant;
use App\Libs\BoxFilter\Elements\IElement;
use App\Libs\BoxFilter\Helper\UserDefineBoxFilter;
use App\Libs\BoxFilterQuery\Parsers\FilterFieldParser;
use App\Libs\BoxFilterQuery\Parsers\IFilterFieldParser;

class FilterSet implements IFilterSet
{
    /**
     * @var IFilterField[]
     */
    protected $filterFields = [];

    protected $userDefineTableName = null;

    /**
     * @var UserDefineBoxFilter|null
     */
    protected $userDefineBoxFilter = null;

    /**
     * FilterSet constructor.
     * @param IElement[] $filterElements
     * @param UserDefineBoxFilter|null $userDefineBoxFilter
     */
    public function __construct(array $filterElements, UserDefineBoxFilter $userDefineBoxFilter = null)
    {
        $this->setFilterFields($filterElements);
        $this->userDefineBoxFilter = $userDefineBoxFilter;
    }

    public function getFilterFields()
    {
        return $this->filterFields;
    }

    public function getUserDefineTableName()
    {
        return $this->userDefineTableName ?? $this->userDefineBoxFilter->getUserDefineTableName();
    }

    public function setUserDefineTableName(string $value)
    {
        return $this->userDefineTableName = $value;
    }

    public function enableUserDefine()
    {
        return !empty($this->userDefineBoxFilter);
    }

    public function parseFilterFields(array $filterInputs, array $relatedRestrictData = [])
    {
        $parser = $this->getFilterFieldParser(array_merge(
            $filterInputs,
            $this->translateRelatedRestrictData($relatedRestrictData)
        ));
        $this->filterFields = $parser->parseFilterFields($this->filterFields);
        if ($this->enableUserDefine()) {
            $this->filterFields = $parser->parseUserDefineFields(
                $this->userDefineBoxFilter->getUserDefines(),
                $this->filterFields
            );
        }
        return $this;
    }

    public function getAvailableUserDefines()
    {
        return $this->userDefineBoxFilter->getAvailableUserDefines();
    }

    /**
     * @param IElement[] $filterElements
     */
    protected function setFilterFields(array $filterElements)
    {
        foreach ($filterElements as $element) {
            $filterField = new FilterField(
                $element->getName(),
                $element->getLabel(),
                $element->getType(),
                $element->getOperators()
            );
            $this->filterFields[] = $filterField;
        }
    }

    /**
     * @param array $filterInputs
     * @return IFilterFieldParser
     */
    protected function getFilterFieldParser(array $filterInputs)
    {
        return new FilterFieldParser($filterInputs);
    }

    protected function translateRelatedRestrictData(array $relatedRestrictData = [])
    {
        $output = [];
        $operator = OperatorTypeConstant::INCLUDE;
        foreach ($relatedRestrictData as $fieldName => $value) {
            if (!empty($value)) {
                $values = is_array($value) ? $value : [$value];
                foreach ($values as $valueItem) {
                    $output[] = [
                        FilterFieldParser::FIELD_KEY => $fieldName,
                        FilterFieldParser::OPERATOR_KEY => $operator,
                        FilterFieldParser::VALUE_KEY => $valueItem,
                    ];
                }
            }
        }
        return $output;
    }
}
