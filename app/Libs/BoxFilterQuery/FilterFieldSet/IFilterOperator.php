<?php

namespace App\Libs\BoxFilterQuery\FilterFieldSet;

interface IFilterOperator
{
    /**
     * @return integer
     */
    public function getOperator();

    /**
     * @return int[]|string[]|float[]
     */
    public function getValues();

    /**
     * @param int[]|string[]|float[] $values
     * @return $this
     */
    public function setValues(array $values);
}
