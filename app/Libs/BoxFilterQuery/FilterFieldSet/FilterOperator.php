<?php

namespace App\Libs\BoxFilterQuery\FilterFieldSet;

class FilterOperator implements IFilterOperator
{
    /**
     * @var integer
     */
    protected $operatorType;

    /**
     * @var int[]|string[]|float[]
     */
    protected $values;

    public function __construct(int $operatorType, $values)
    {
        $this->operatorType = $operatorType;
        $this->values = $values;
    }

    public function getOperator()
    {
        return $this->operatorType;
    }

    public function getValues()
    {
        return $this->values;
    }

    public function setValues($values)
    {
        $this->values = $values;
        return $this;
    }
}
