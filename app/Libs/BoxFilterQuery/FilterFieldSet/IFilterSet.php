<?php

namespace App\Libs\BoxFilterQuery\FilterFieldSet;

interface IFilterSet
{
    /**
     * @return IFilterField[]
     */
    public function getFilterFields();

    /**
     * @return string
     */
    public function getUserDefineTableName();

    /**
     * @return string
     */
    public function setUserDefineTableName(string $value);

    /**
     * @return bool
     */
    public function enableUserDefine();

    /**
     * @param array $filterInputs
     * @param array $relatedRestrictData
     * @return IFilterSet
     */
    public function parseFilterFields(array $filterInputs, array $relatedRestrictData = []);

    /**
     * @return array
     */
    public function getAvailableUserDefines();
}
