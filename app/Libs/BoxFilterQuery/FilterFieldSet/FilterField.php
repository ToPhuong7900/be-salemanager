<?php

namespace App\Libs\BoxFilterQuery\FilterFieldSet;

use App\Libs\BoxFilter\Constants\ElementTypeConstant;
use App\Libs\TfHttp;

class FilterField implements IFilterField
{
    /**
     * @var string
     * @example 'project_code'
     */
    protected $fieldName;

    /**
     * @var string
     * @example 'Project Code'
     */
    protected $label;

    /**
     * @var string
     * @example App\Libs\BoxFilter\Constants\ElementTypeConstant::TEXT
     */
    protected $filterElementType;

    /**
     * @var integer[]
     */
    protected $availableOperators = [];

    /**
     * @var IFilterOperator[]
     */
    protected $filterOperators;

    public function __construct(string $fieldName, string $label, string $filterElementType, array $availableOperators)
    {
        $this->fieldName = $fieldName;
        $this->label = $label;
        $this->filterElementType = $filterElementType;
        $this->availableOperators = $availableOperators;
    }

    public function getFieldName()
    {
        return $this->fieldName;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function getFilterElementType()
    {
        return $this->filterElementType;
    }

    public function getAvailableOperators()
    {
        return $this->availableOperators;
    }

    public function getFilterOperators()
    {
        return $this->filterOperators;
    }

    /**
     * @return IFilterOperator|null
     */
    public function getFilterOperator()
    {
        return !empty($this->filterOperators) ? head($this->filterOperators) : null;
    }

    public function setFilterOperators(array $filterOperators)
    {
        $this->filterOperators = $filterOperators;
    }

    /**
     * @return array|float[]|int[]|string[]
     */
    public function getFilterValues()
    {
        $filterOperator = $this->getFilterOperator();
        return $filterOperator ? $filterOperator->getValues() : [];
    }

    public function convertToBoxFilterParams($group = null)
    {
        switch ($this->getFilterElementType()) {
            case ElementTypeConstant::DATE_RANGE:
            case ElementTypeConstant::DATE_FIELD:
                $dateRange = $this->getFilterValues()[0];
                foreach ($dateRange as $index => $date) {
                    if (!empty($date)) {
                        $dateRange[$index] = date("d/m/Y", strtotime($date));
                    }
                }
                $boxFilters[] = TfHttp::getBoxFilterParam(
                    $this->getFieldName(),
                    $this->getFilterOperator()->getOperator(),
                    implode(',', $dateRange),
                    $group
                );
                break;
            case ElementTypeConstant::DATE_TIME_RANGE:
            case ElementTypeConstant::TIME_RANGE:
            case ElementTypeConstant::NUMBER_RANGE:
                $boxFilters[] = TfHttp::getBoxFilterParam(
                    $this->getFieldName(),
                    $this->getFilterOperator()->getOperator(),
                    implode(',', $this->getFilterValues()[0]),
                    $group
                );
                break;
            default:
                $boxFilters = TfHttp::getBoxFilterParams(
                    $this->getFieldName(),
                    $this->getFilterOperator()->getOperator(),
                    $this->getFilterValues(),
                    $group
                );
                break;
        }

        return $boxFilters;
    }
}
