<?php

namespace App\Libs\BoxFilterQuery\Parsers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use App\Libs\AuditableInterface;
use App\Libs\BoxFilter\Constants\ElementTypeConstant;
use App\Libs\BoxFilter\Constants\OperatorTypeConstant;
use App\Libs\BoxFilterQuery\FilterFieldSet\IFilterField;
use App\Libs\Common;
use Tfcloud\Models\BaseModel;
use Tfcloud\Models\Building;
use Tfcloud\Models\Plant;
use Tfcloud\Models\Report\ContactAuditReport;
use Tfcloud\Models\Report\GenUserDefSelAuditReport;
use Tfcloud\Models\Room;
use Tfcloud\Models\Site;

class FilterDetailParser
{
    /**
     * @var IFilterField[]
     */
    private $filterFields;

    /**
     * @var array
     */
    private $filterDetailMaps;

    private $filterTextElements = [];

    protected $glueBtwValuesText = 'and';

    protected $commonDetailMaps = [
        'site_id' => Site::class,
        'building_id' => Building::class,
        'room_id' => Room::class,
        'plant_id' => Plant::class,
        'sel1_gen_userdef_sel_id' => GenUserDefSelAuditReport::class,
        'sel2_gen_userdef_sel_id' => GenUserDefSelAuditReport::class,
        'sel3_gen_userdef_sel_id' => GenUserDefSelAuditReport::class,
        'sel4_gen_userdef_sel_id' => GenUserDefSelAuditReport::class,
        'user_contact1' => ContactAuditReport::class,
        'user_contact2' => ContactAuditReport::class,
        'user_contact3' => ContactAuditReport::class,
        'user_contact4' => ContactAuditReport::class,
        'user_contact5' => ContactAuditReport::class,
        'user_contact6' => ContactAuditReport::class,
        'user_contact7' => ContactAuditReport::class,
        'user_contact8' => ContactAuditReport::class,
        'user_contact9' => ContactAuditReport::class,
        'user_contact10' => ContactAuditReport::class,
    ];

    /**
     * FilterDetailParser constructor.
     * @param IFilterField[] $filterFields
     * @param array $filterDetailMaps
     */
    public function __construct(array $filterFields, array $filterDetailMaps = [])
    {
        $this->filterFields = $filterFields;
        $this->filterDetailMaps = array_merge($filterDetailMaps, $this->commonDetailMaps);
        $this->generate();
    }

    public function getFilterDetailsText()
    {
        return implode(', ', $this->filterTextElements);
    }

    public function addFilterDetailsText(string $text)
    {
        array_push($this->filterTextElements, str_replace('"', '', $text));
    }

    public function addFilterDetails(array $filterDetails)
    {
        $this->filterTextElements = array_merge($this->filterTextElements, $filterDetails);
    }

    public function prependFilterDetailsText(string $text)
    {
        $this->filterTextElements = Arr::prepend($this->filterTextElements, str_replace('"', '', $text));
    }

    protected function generate()
    {
        foreach ($this->filterFields as $filterField) {
            $filterDetailObject = array_get($this->filterDetailMaps, $filterField->getFieldName());
            $filterLabel = $filterField->getLabel();
            $operatorValueText = [];
            foreach ($filterField->getFilterOperators() as $filterOperator) {
                if ($filterDetailObject) {
                    if (is_callable($filterDetailObject)) {
                        $valueText = $this->parseArrayToText(
                            $this->glueBtwValuesText,
                            $filterDetailObject($filterOperator),
                            $filterField->getFilterElementType()
                        );
                    } else {
                        $records = $filterDetailObject::userSiteGroup()->find($filterOperator->getValues());
                        $valueText = $this->parseCollectionToText(
                            $this->glueBtwValuesText,
                            $records,
                            $filterField->getFilterElementType()
                        );
                    }
                } else {
                    $valueText = $this->parseArrayToText(
                        $this->glueBtwValuesText,
                        $filterOperator->getValues(),
                        $filterField->getFilterElementType()
                    );
                }
                $operatorValueText[$this->getOperatorText($filterOperator->getOperator())] = $valueText;
            }
            foreach ($operatorValueText as $operatorText => $valueText) {
                $filterText = $filterLabel . ' ' . $operatorText;
                if (!empty($valueText)) {
                    $filterText .= ' ' . $valueText;
                }
                $this->filterTextElements[] = $filterText;
            }
        }
    }

    protected function getOperatorText($operatorId)
    {
        return OperatorTypeConstant::getOperatorName($operatorId);
    }

    protected function parseArrayToText(string $glue, array $values, string $filterElementType = null)
    {
        $valueText = '';
        if (!empty($values)) {
            $valueText = implode(' ' . $glue . ' ', array_map(function ($value) use ($filterElementType) {
                if ((isset($value) && $value == "0") || !empty($value)) {
                    switch ($filterElementType) {
                        case ElementTypeConstant::SINGLE_DATE:
                            return Common::dateFieldDisplayFormat($value);
                        case ElementTypeConstant::DATE_RANGE:
                        case ElementTypeConstant::DATE_TIME_RANGE:
                            list($from, $to) = $value;
                            return '[' . Common::dateFieldDisplayFormat($from) . ',' .
                                Common::dateFieldDisplayFormat($to) . ']';
                        case ElementTypeConstant::NUMBER_RANGE:
                        case ElementTypeConstant::TIME_RANGE:
                            list($from, $to) = $value;
                            return '[' . $from . ',' . $to . ']';
                        default:
                            return "'$value'";
                    }
                } else {
                    return '';
                }
            }, $values));
        }
        return $valueText;
    }

    protected function parseCollectionToText(string $glue, Collection $values, string $filterElementType = null)
    {
        $codes = $values->reduce(function ($carry, $value) {
            if ($value instanceof AuditableInterface) {
                $carry[] = $value->getAuditCode();
            }
            return $carry;
        }, []);
        return $this->parseArrayToText($glue, $codes, $filterElementType);
    }
}
