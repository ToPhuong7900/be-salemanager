<?php

namespace App\Libs\BoxFilterQuery\Parsers;

use Carbon\Carbon;
use App\Libs\BoxFilter\Constants\ElementTypeConstant;
use App\Libs\BoxFilter\Constants\OperatorTypeConstant;
use App\Libs\BoxFilter\Elements\DateRangeDropDownElement;
use App\Libs\BoxFilter\Elements\Pickers\OwnerUserPickerElement;
use App\Libs\BoxFilter\Helper\UserDefineBoxFilter;
use App\Libs\BoxFilterQuery\FilterFieldSet\FilterOperator;

class FilterFieldParser implements IFilterFieldParser
{
    public const OPERATOR_KEY = 'o';
    public const FIELD_KEY = 'f';
    public const VALUE_KEY = 'v';
    public const GROUP_KEY = 'g';
    public const SEPARATED_VALUE_SYMBOL = ',';

    /**
     * @var array
     */
    protected $filterParameters = [];

    private $emptyValueOperators = [
        OperatorTypeConstant::NOT_BLANK,
        OperatorTypeConstant::BLANK
    ];

    /**
     * FilterFieldParser constructor.
     * @param array $filterParameters
     */
    public function __construct(array $filterParameters)
    {
        $this->filterParameters = $this->formatFilterParameters($filterParameters);
    }

    /**
     * @inheritDoc
     */
    public function parseFilterFields(array $filterFields)
    {
        $validFilterFields = [];
        foreach ($filterFields as $filterField) {
            $operators = array_get($this->filterParameters, $filterField->getFieldName());
            if (!empty($operators)) {
                $filterOperators = [];
                foreach ($operators as $operator => $values) {
                    $formattedValues = $this->formatValues($values, $filterField->getFilterElementType());
                    $filterOperators[] = new FilterOperator($operator, $formattedValues);
                }
                $filterField->setFilterOperators($filterOperators);
                $validFilterFields[] = $filterField;
            }
        }
        return $validFilterFields;
    }

    /**
     * @inheritDoc
     */
    public function parseUserDefineFields(array $userDefines, array $filterFields)
    {
        $userDefineSet = UserDefineBoxFilter::getUserDefinedSet();
        foreach ($userDefineSet as $name => $label) {
            if (empty($userDefines['label']->$label)) {
                foreach ($filterFields as $filterField) {
                    if ($filterField->getFieldName() == $name) {
                        unset($filterField);
                    }
                }
            }
        }
        return $filterFields;
    }

    /**
     * @param array $values
     * @param string $filterElementType
     * @return array
     */
    protected function formatValues(array $values, string $filterElementType)
    {
        $formattedValues = [];
        switch ($filterElementType) {
            case ElementTypeConstant::USER_PICKER:
                $formattedValues = OwnerUserPickerElement::formatValue($values);
                break;
            case ElementTypeConstant::SINGLE_DATE:
                foreach ($values as $value) {
                    $formattedValues[] = $this->formatDateValue($value);
                }
                break;
            case ElementTypeConstant::DATE_RANGE:
            case ElementTypeConstant::DATE_TIME_RANGE:
                foreach ($values as $value) {
                    if ($this->isRangeValue($value)) {
                        $includeTime = $filterElementType == ElementTypeConstant::DATE_TIME_RANGE;
                        list($from, $to) = explode(self::SEPARATED_VALUE_SYMBOL, $value);
                        if ($includeTime) {
                            if (!empty($from)) {
                                $from = $this->addSecondsToTime($from, ':00');
                            }
                            if (!empty($to)) {
                                $to = $this->addSecondsToTime($to, ':59');
                            }
                        }
                        $formattedValues[] =
                            [$this->formatDateValue($from, $includeTime), $this->formatDateValue($to, $includeTime)];
                    }
                }
                break;
            case ElementTypeConstant::NUMBER_RANGE:
            case ElementTypeConstant::TIME_RANGE:
                foreach ($values as $value) {
                    if ($this->isRangeValue($value)) {
                        $ranges = explode(self::SEPARATED_VALUE_SYMBOL, $value);
                        $formattedValues[] = array_map('trim', $ranges);
                    }
                }
                break;
            case ElementTypeConstant::DATE_FIELD:
                $formattedValues = $this->getDateFieldFormatValue($values);
                break;
            default:
                $formattedValues = array_map(function ($value) {
                    return is_array($value) ? $value : trim($value);
                }, $values);
                break;
        }
        return $formattedValues;
    }

    private function getDateFieldFormatValue(array $values): array
    {
        $formattedValues = [];
        $from = '';
        $to = '';
        foreach ($values as $value) {
            switch ($value) {
                case DateRangeDropDownElement::TODAY:
                    $from = $to = Carbon::now()->toDateString();
                    break;
                case DateRangeDropDownElement::YESTERDAY:
                    $from = $to = Carbon::yesterday()->toDateString();
                    break;
                case DateRangeDropDownElement::TOMORROW:
                    $from = $to = Carbon::now()->addDay()->toDateString();
                    break;
                case DateRangeDropDownElement::BEFORE_TODAY:
                    $to = Carbon::now()->toDateString();
                    break;
                case DateRangeDropDownElement::AFTER_TODAY:
                    $from = Carbon::now()->toDateString();
                    break;
                case DateRangeDropDownElement::LAST_7_DAYS:
                case DateRangeDropDownElement::SEVEN_DAYS:
                    $from = Carbon::now()->subDays(7)->toDateString();
                    $to = Carbon::now()->toDateString();
                    break;
                case DateRangeDropDownElement::LAST_14_DAYS:
                    $from = Carbon::now()->subDays(14)->toDateString();
                    $to = Carbon::now()->toDateString();
                    break;
                case DateRangeDropDownElement::LAST_21_DAYS:
                    $from = Carbon::now()->subDays(21)->toDateString();
                    $to = Carbon::now()->toDateString();
                    break;
                case DateRangeDropDownElement::LAST_28_DAYS:
                    $from = Carbon::now()->subDays(28)->toDateString();
                    $to = Carbon::now()->toDateString();
                    break;
                case DateRangeDropDownElement::LAST_31_DAYS:
                    $from = Carbon::now()->subDays(31)->toDateString();
                    $to = Carbon::now()->toDateString();
                    break;
                case DateRangeDropDownElement::LAST_FULL_WEEK:
                    $from = Carbon::now()->subWeek()->startOfWeek()->toDateString();
                    $to = Carbon::now()->subWeek()->endOfWeek()->toDateString();
                    break;
                case DateRangeDropDownElement::LAST_FULL_MONTH:
                    $from = Carbon::now()->subMonth()->startOfMonth()->toDateString();
                    $to = Carbon::now()->subMonth()->endOfMonth()->toDateString();
                    break;
                case DateRangeDropDownElement::LAST_3_MONTHS:
                case DateRangeDropDownElement::THREE_MONTHS:
                    $from = Carbon::now()->subMonths(3)->toDateString();
                    $to = Carbon::now()->toDateString();
                    break;
                case DateRangeDropDownElement::LAST_6_MONTHS:
                case DateRangeDropDownElement::SIX_MONTHS:
                    $from = Carbon::now()->subMonths(6)->toDateString();
                    $to = Carbon::now()->toDateString();
                    break;
                case DateRangeDropDownElement::LAST_12_MONTHS:
                case DateRangeDropDownElement::TWELVE_MONTHS:
                    $from = Carbon::now()->subMonths(12)->toDateString();
                    $to = Carbon::now()->toDateString();
                    break;
                case DateRangeDropDownElement::LAST_FULL_YEAR:
                    $from = Carbon::now()->subYear()->startOfYear()->toDateString();
                    $to = Carbon::now()->subYear()->endOfYear()->toDateString();
                    break;
                case DateRangeDropDownElement::NEXT_7_DAYS:
                    $from = Carbon::now()->toDateString();
                    $to = Carbon::now()->addDays(7)->toDateString();
                    break;
                case DateRangeDropDownElement::NEXT_14_DAYS:
                    $from = Carbon::now()->toDateString();
                    $to = Carbon::now()->addDays(14)->toDateString();
                    break;
                case DateRangeDropDownElement::NEXT_21_DAYS:
                    $from = Carbon::now()->toDateString();
                    $to = Carbon::now()->addDays(21)->toDateString();
                    break;
                case DateRangeDropDownElement::NEXT_31_DAYS:
                    $from = Carbon::now()->toDateString();
                    $to = Carbon::now()->addDays(31)->toDateString();
                    break;
                case DateRangeDropDownElement::NEXT_FULL_WEEK:
                    $from = Carbon::now()->addWeek()->startOfWeek()->toDateString();
                    $to = Carbon::now()->addWeek()->endOfWeek()->toDateString();
                    break;
                case DateRangeDropDownElement::NEXT_FULL_MONTH:
                    $from = Carbon::now()->addMonth()->startOfMonth()->toDateString();
                    $to = Carbon::now()->addMonth()->endOfMonth()->toDateString();
                    break;
                case DateRangeDropDownElement::NEXT_3_MONTHS:
                    $from = Carbon::now()->toDateString();
                    $to = Carbon::now()->addMonths(3)->toDateString();
                    break;
                case DateRangeDropDownElement::NEXT_6_MONTHS:
                    $from = Carbon::now()->toDateString();
                    $to = Carbon::now()->addMonths(6)->toDateString();
                    break;
                case DateRangeDropDownElement::NEXT_12_MONTHS:
                    $from = Carbon::now()->toDateString();
                    $to = Carbon::now()->addMonths(12)->toDateString();
                    break;
                case DateRangeDropDownElement::NEXT_FULL_YEAR:
                    $from = Carbon::now()->addYear()->startOfYear()->toDateString();
                    $to = Carbon::now()->addYear()->endOfYear()->toDateString();
                    break;
                case DateRangeDropDownElement::TWENTY_FOUR_HOURS:
                    $formattedValues[] = [DateRangeDropDownElement::TWENTY_FOUR_HOURS];
                    break;
                case DateRangeDropDownElement::FOUR_WEEKS:
                    $from = Carbon::now()->subWeeks(4)->toDateString();
                    $to = Carbon::now()->toDateString();
                    break;
                case DateRangeDropDownElement::LAST_30_DAYS:
                    $from = Carbon::now()->subDays(30)->toDateString();
                    $to = Carbon::now()->toDateString();
                    break;
                default:
                    $formattedValues[] = [$value];
            }
        }
        if ($from !== '' || $to !== '') {
            $formattedValues[] =
                [
                    $this->formatDateValue($from),
                    $this->formatDateValue($to)
                ];
        }
        return $formattedValues;
    }

    protected function formatDateValue($value, bool $includeTime = false)
    {
        if (empty($value)) {
            return $value;
        }
        $inputFormat = $includeTime ? 'd/m/Y H:i:s' : 'd/m/Y';

        $value = trim($value);
        if (!empty($value) && $this->isDateString($value, $inputFormat)) {
            $date = Carbon::createFromFormat($inputFormat, $value);
            return $includeTime ? $date->toDateTimeString('seconds') : $date->toDateString();
        }
        return $value;
    }

    protected function isDateString(string $value, string $inputFormat = 'd/m/Y')
    {
        $inputs = ['dateString' => $value];
        $validator = \Validator::make($inputs, [
            'dateString' => "date_format:{$inputFormat}, nullable",
        ]);
        return !$validator->fails();
    }

    protected function isRangeValue(string $value)
    {
        return strpos($value, self::SEPARATED_VALUE_SYMBOL) === false ? false : true;
    }

    protected function formatFilterParameters(array $filterParameters)
    {
        $formattedParameters = [];
        foreach ($filterParameters as $parameter) {
            $field = $parameter[self::FIELD_KEY] ?? null;
            $operator = $parameter[self::OPERATOR_KEY] ?? null;
            $value = $parameter[self::VALUE_KEY] ?? null;
            if (
                $field && $operator &&
                (in_array($operator, $this->emptyValueOperators) || (!is_null($value) && $value !== ""))
            ) {
                $formattedParameters[$field][$operator][] = $value;
            }
        }
        return $formattedParameters;
    }

    protected function addSecondsToTime($time, $addTime)
    {
        return $time . $addTime;
    }
}
