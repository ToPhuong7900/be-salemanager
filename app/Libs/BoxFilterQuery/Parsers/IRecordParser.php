<?php

namespace App\Libs\BoxFilterQuery\Parsers;

interface IRecordParser
{
    /**
     * @param mixed $record
     * @param string $url
     * @return array
     */
    public function getAttachedRecordActions($record);
}
