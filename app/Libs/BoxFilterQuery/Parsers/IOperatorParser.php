<?php

namespace App\Libs\BoxFilterQuery\Parsers;

use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Query\Builder as QueryBuilder;

interface IOperatorParser
{
    /**
     * @param EloquentBuilder|QueryBuilder $query
     * @param string $fieldName
     * @param string $elementType
     * @param int $operator
     * @param array $values
     * @return EloquentBuilder|QueryBuilder
     */
    public function addOperator($query, string $fieldName, string $elementType, int $operator, array $values);

    /**
     * @param array $prefixTables
     * @return IOperatorParser
     */
    public function setPrefixTables(array $prefixTables);

    /**
     * @param array $prefixTables
     * @return IOperatorParser
     */
    public function addPrefixTables(array $prefixTables);

    /**
     * @return array
     */
    public function getPrefixTables();

    /**
     * @param string $fieldName
     * @return string
     */
    public function getAttachedTableName(string $fieldName);

    /**
     * @param array $sqlFunctionFields
     * @return IOperatorParser
     */
    public function addSqlFunctionFields(array $sqlFunctionFields);
}
