<?php

namespace App\Libs\BoxFilterQuery\Parsers;

use App\Libs\BoxFilterQuery\FilterFieldSet\IFilterField;

interface IFilterFieldParser
{
    /**
     * @param IFilterField[] $filterFields
     * @return IFilterField[]
     */
    public function parseFilterFields(array $filterFields);

    /**
     *
     * @param array $userDefines
     * @param IFilterField[] $filterFields
     * @return IFilterField[]
     */
    public function parseUserDefineFields(array $userDefines, array $filterFields);
}
