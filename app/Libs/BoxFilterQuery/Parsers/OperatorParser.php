<?php

namespace App\Libs\BoxFilterQuery\Parsers;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use App\Libs\BoxFilter\Constants\ElementTypeConstant;
use App\Libs\BoxFilter\Constants\OperatorTypeConstant;
use App\Libs\Common;

class OperatorParser implements IOperatorParser
{
    protected $prefixTables = [];
    protected $sqlFunctionFields = [];

    public function __construct(array $prefixTables, array $sqlFunctionFields = [])
    {
        $this->setPrefixTables($prefixTables);
        $this->sqlFunctionFields = $sqlFunctionFields;
    }

    public function __call($method, $parameters)
    {
        if ($scope = $this->operatorMethodScope($method)) {
            return $this->{$scope}(...$parameters);
        }
        return $this;
    }

    public function addOperator($query, string $fieldName, string $elementType, int $operator, array $values)
    {
        $operatorName = OperatorTypeConstant::getOperatorName($operator);
        $operatorMethod = is_string($operatorName) ? $this->operatorMethodScope($operatorName) : null;
        if ($operatorMethod) {
            $formattedFieldName = $this->applySqlFunctionToFieldName(
                $fieldName,
                $this->getAttachedTableName($fieldName)
            );
            return $this->{$operatorMethod}($query, $formattedFieldName, $values, $elementType);
        }
        return $query;
    }

    public function setPrefixTables(array $prefixTables)
    {
        $this->prefixTables = $this->formatPrefixTables($prefixTables);
        return $this;
    }

    public function addPrefixTables(array $prefixTables)
    {
        $this->prefixTables = array_merge($this->prefixTables, $this->formatPrefixTables($prefixTables));
    }

    public function getPrefixTables()
    {
        return $this->prefixTables;
    }

    public function getAttachedTableName(string $fieldName)
    {
        $mainTable = array_get($this->prefixTables, '*', '');
        $aliasName = array_get($this->prefixTables, $fieldName);
        return $aliasName ? $aliasName : ($mainTable ? $mainTable . '.' . $fieldName : $fieldName);
    }

    public function addSqlFunctionFields(array $sqlFunctionFields)
    {
        $this->sqlFunctionFields = array_merge($this->sqlFunctionFields, $sqlFunctionFields);
        return $this;
    }

    protected function operatorMethodScope(string $method)
    {
        $scope = 'operator' . ucfirst(camel_case($method));
        return method_exists($this, $scope) ? $scope : null;
    }

    /**
     * OperatorTypeConstant::CONTAIN = 1
     * @param EloquentBuilder|QueryBuilder $query
     * @param string $fieldName
     * @param array $values
     * @param string $elementType
     * @return EloquentBuilder|QueryBuilder
     */
    protected function operatorContains($query, $fieldName, $values, $elementType)
    {
        foreach ($values as $value) {
            $query->where($fieldName, 'LIKE', '%' . $value . '%');
        }
        return $query;
    }

    /**
     * OperatorTypeConstant::NOT_CONTAIN = 2
     * @param EloquentBuilder|QueryBuilder $query
     * @param string $fieldName
     * @param array $values
     * @param string $elementType
     * @return EloquentBuilder|QueryBuilder
     */
    public static function operatorNotContains($query, $fieldName, $values, $elementType)
    {
        foreach ($values as $value) {
            $query->where($fieldName, 'NOT LIKE', '%' . $value . '%');
        }
        return $query;
    }

    /**
     * OperatorTypeConstant::NOT_MATCH = 3
     * @param EloquentBuilder|QueryBuilder $query
     * @param string $fieldName
     * @param array $values
     * @param string $elementType
     * @return EloquentBuilder|QueryBuilder
     */
    public static function operatorNotMatches($query, $fieldName, $values, $elementType)
    {
        foreach ($values as $value) {
            $query->where($fieldName, '<>', $value);
        }
        return $query;
    }

    /**
     * OperatorTypeConstant::MATCH = 4
     * @param EloquentBuilder|QueryBuilder $query
     * @param string $fieldName
     * @param array $values
     * @param string $elementType
     * @return EloquentBuilder|QueryBuilder
     */
    public static function operatorMatches($query, $fieldName, $values, $elementType)
    {
        foreach ($values as $value) {
            $query->where($fieldName, '=', $value);
        }
        return $query;
    }

    /**
     * OperatorTypeConstant::INCLUDE = 5
     * @param EloquentBuilder|QueryBuilder $query
     * @param string $fieldName
     * @param array $values
     * @param string $elementType
     * @return EloquentBuilder|QueryBuilder
     */
    public static function operatorIncludes($query, $fieldName, $values, $elementType)
    {
        $query->whereIn($fieldName, array_flatten($values));
        return $query;
    }

    /**
     * OperatorTypeConstant::MULTIPLE_VALUE = 11
     * @param EloquentBuilder|QueryBuilder $query
     * @param string $fieldName
     * @param array $values
     * @param string $elementType
     * @return EloquentBuilder|QueryBuilder
     */
    public static function operatorMultipleValues($query, $fieldName, $values)
    {
        $flattenValues = array_flatten($values);
        $firstValue = array_shift($flattenValues);
        if (is_string($firstValue)) {
            $flattenValues = array_map('trim', explode(',', $firstValue));
        }
        $query->whereIn($fieldName, $flattenValues);
        return $query;
    }

    /**
     * OperatorTypeConstant::NOT_INCLUDE = 6
     * @param EloquentBuilder|QueryBuilder $query
     * @param string $fieldName
     * @param array $values
     * @param string $elementType
     * @return EloquentBuilder|QueryBuilder
     */
    public static function operatorNotIncludes($query, $fieldName, $values, $elementType)
    {
        $flattenValues = array_flatten($values);
        if (self::isIdLookupValue($elementType)) {
            $query->where(function ($q) use ($fieldName, $flattenValues) {
                $q->whereNull($fieldName)->orWhere($fieldName, '=', '')->orWhereNotIn($fieldName, $flattenValues);
            });
        } else {
            $query->whereNotIn($fieldName, $flattenValues);
        }
        return $query;
    }

    /**
     * OperatorTypeConstant::INCLUDE_RANGE = 7
     * @param EloquentBuilder|QueryBuilder $query
     * @param string $fieldName
     * @param array $values
     * @param string $elementType
     * @return EloquentBuilder|QueryBuilder
     */
    public static function operatorIncludesRange($query, $fieldName, $values, $elementType)
    {
        foreach ($values as $ranges) {
            $from = Arr::first($ranges);
            $to = Arr::last($ranges);
            $query->where(function ($q) use ($fieldName, $from, $to) {
                if (isset($from) && $from !== '') {
                    $q->where($fieldName, '>=', $from);
                }
                if (isset($to) && $to !== '') {
                    $q->where($fieldName, '<=', $to);
                }
            });
        }
        return $query;
    }

    /**
     * OperatorTypeConstant::EXCLUDE_RANGE = 8
     * @param EloquentBuilder|QueryBuilder $query
     * @param string $fieldName
     * @param array $values
     * @param string $elementType
     * @return EloquentBuilder|QueryBuilder
     */
    public static function operatorExcludesRange($query, $fieldName, $values, $elementType = null)
    {
        foreach ($values as $ranges) {
            $from = Arr::first($ranges);
            $to = Arr::last($ranges);
            $query->where(function ($q) use ($fieldName, $from, $to) {
                if (isset($from) && $from !== '') {
                    $q->where($fieldName, '<', $from);
                }
                if (isset($to) && $to !== '') {
                    $q->orWhere($fieldName, '>', $to);
                }
            });
        }
        return $query;
    }

    /**
     * OperatorTypeConstant::BLANK = 9
     * @param EloquentBuilder|QueryBuilder $query
     * @param string $fieldName
     * @param array $values
     * @param string $elementType
     * @return EloquentBuilder|QueryBuilder
     */
    public static function operatorBlank($query, $fieldName, $values, $elementType)
    {
        $query->where(function ($q) use ($fieldName) {
            $q->whereNull($fieldName)->orWhere($fieldName, '=', '');
        });
        return $query;
    }

    /**
     * OperatorTypeConstant::NOT_BLANK = 10
     * @param EloquentBuilder|QueryBuilder $query
     * @param string $fieldName
     * @param array $values
     * @param string $elementType
     * @return EloquentBuilder|QueryBuilder
     */
    public static function operatorNotBlank($query, $fieldName, $values, $elementType)
    {
        $query->whereNotNull($fieldName)->where($fieldName, '<>', '');
        return $query;
    }

    /**
     * @param $query
     * @param $fieldName
     * @param $values
     * @param $elementType
     * @return mixed
     */
    public static function operatorAfter($query, $fieldName, $values, $elementType = null): mixed
    {
        foreach ($values as $value) {
            $column = Arr::first($value);
            $query->whereColumn(
                DB::raw("DATE($fieldName)"),
                '>',
                DB::raw("DATE($column)")
            );
        }
        return $query;
    }

    /**
     * @param $query
     * @param $fieldName
     * @param $values
     * @param $elementType
     * @return mixed
     */
    public static function operatorBefore($query, $fieldName, $values, $elementType = null): mixed
    {
        foreach ($values as $value) {
            $column = Arr::first($value);
            $query->whereColumn(
                DB::raw("DATE($fieldName)"),
                '<',
                DB::raw("DATE($column)")
            );
        }
        return $query;
    }

    /**
     * @param $query
     * @param $fieldName
     * @param $values
     * @param $elementType
     * @return mixed
     */
    public static function operatorEqual($query, $fieldName, $values, $elementType = null): mixed
    {
        foreach ($values as $value) {
            $column = Arr::first($value);
            $query->whereColumn(
                DB::raw("DATE($fieldName)"),
                '=',
                DB::raw("DATE($column)")
            );
        }
        return $query;
    }

    /**
     * @param $query
     * @param $fieldName
     * @param $values
     * @param $elementType
     * @return mixed
     */
    public static function operatorAfterOrEqual($query, $fieldName, $values, $elementType = null): mixed
    {
        foreach ($values as $value) {
            $column = Arr::first($value);
            $query->whereColumn(
                DB::raw("DATE($fieldName)"),
                '>=',
                DB::raw("DATE($column)")
            );
        }
        return $query;
    }

    /**
     * @param $query
     * @param $fieldName
     * @param $values
     * @param $elementType
     * @return mixed
     */
    public static function operatorBeforeOrEqual($query, $fieldName, $values, $elementType = null): mixed
    {
        foreach ($values as $value) {
            $column = Arr::first($value);
            $query->whereColumn(
                DB::raw("DATE($fieldName)"),
                '<=',
                DB::raw("DATE($column)")
            );
        }
        return $query;
    }

    /**
     * @param array $prefixTables
     * @return array
     */
    protected function formatPrefixTables(array $prefixTables)
    {
        $formatted = [];
        $mainTable = $this->getMainPrefixTable($prefixTables);
        if ($mainTable) {
            Arr::pull($prefixTables, $mainTable);
            $formatted['*'] = $mainTable;
        }
        foreach ($prefixTables as $tableName => $columnNames) {
            if (is_string($columnNames)) {
                $formatted[$columnNames] = $tableName . '.' . $columnNames;
            } elseif (is_array($columnNames)) {
                foreach ($columnNames as $key => $value) {
                    if (is_string($key)) {
                        $filterName = $key;
                        $columnName = $value;
                    } else {
                        $filterName = $value;
                        $columnName = $value;
                    }
                    $formatted[$filterName] = $tableName . '.' . $columnName;
                }
            }
        }
        return $formatted;
    }

    protected function getMainPrefixTable(array $prefixTables)
    {
        foreach ($prefixTables as $tableName => $columnNames) {
            if ($columnNames === '*') {
                return $tableName;
            }
        }
        return null;
    }

    protected function applySqlFunctionToFieldName($fieldName, $attachedFieldName)
    {
        $sqlFunction = array_get($this->sqlFunctionFields, $fieldName, []);
        if (count($sqlFunction)) {
            $funcName = array_shift($sqlFunction);
            $args = array_map(function ($arg) {
                return "'$arg'";
            }, $sqlFunction);
            $argString = count($args) ? ', ' . implode(', ', $args) : '';
            return \DB::raw("$funcName($attachedFieldName $argString)");
        }
        return $attachedFieldName;
    }

    public static function isIdLookupValue(string $elementType)
    {
        switch ($elementType) {
            case ElementTypeConstant::DROP_DOWN:
            case ElementTypeConstant::DROP_DOWN_API:
            case ElementTypeConstant::PICKER:
            case ElementTypeConstant::USER_PICKER:
                return true;
        }
        return false;
    }
}
