<?php

namespace App\Libs\BoxFilterQuery\Parsers;

use App\Libs\BoxFilter\Constants\RecordActionConstant;
use App\Libs\BoxFilterQuery\Helper\RecordAction;
use App\Libs\BoxFilterQuery\Helper\RecordHelper;
use Tfcloud\Models\CrudAccessLevel;

class RecordParser implements IRecordParser
{
    public const ACTION_LABEL_STR = 'label';
    public const ACTION_CODE_STR = 'code';
    public const ACTION_URL_STR = 'url';
    public const ACTION_TEMPLATE_STR = 'template';
    /**
     * @var RecordAction[]
     */
    protected $recordActions = [];
    protected array $restrictedPropertyActions = RecordActionConstant::PROPERTY_ACTION;

    /**
     * @var integer
     */
    private $userModuleAccessLevel;

    /**
     * RecordParser constructor.
     * @param int $moduleId
     * @param RecordAction[] $recordActions
     */
    public function __construct(int $moduleId, array $recordActions)
    {
        $this->userModuleAccessLevel = \Auth::user()->isSuperuser() ? CrudAccessLevel::CRUD_ACCESS_LEVEL_SUPERVISOR :
            \Auth::user()->getAccessLevel($moduleId);
        $this->setRecordActions($this->modifyRecordAction($recordActions));
    }

    protected function modifyRecordAction(array $recordActions)
    {
        if (
            \Auth::User()->isContractor()
                && \Auth::user()->siteGroup->prop_restrict_contractor_access === "Y"
        ) {
            foreach ($recordActions as $key => $actionSet) {
                if (in_array($actionSet->getActionCode(), $this->restrictedPropertyActions)) {
                    unset($recordActions[$key]);
                }
            }
        }
        return $recordActions;
    }

    public function getAttachedRecordActions($record)
    {
        $recordActions = [];
        foreach ($this->recordActions as $actionSet) {
            $recordActions[] = $this->addRecordAction($record, $actionSet);
        }
        return $recordActions;
    }

    protected function replaceRouteParameters($path, $parameters)
    {
        return RecordHelper::generateUrlFromParameters($path, $parameters);
    }

    protected function getUrlParameters($record, $parameterNames)
    {
        $parameters = [];
        foreach ($parameterNames as $parameterName) {
            $name = str_replace(['{', '}'], '', $parameterName);
            if (isset($record->$name)) {
                $parameters[$parameterName] = $record->$name;
            }
        }
        return $parameters;
    }

    /**
     * @param RecordAction[] $recordActions
     */
    private function setRecordActions(array $recordActions)
    {
        foreach ($recordActions as $actionSet) {
            $permissionCheck = $this->userModuleAccessLevel >= $actionSet->getCrudLevel();
            switch ($actionSet->getActionCode()) {
                case RecordActionConstant::EDIT_ACTION_KEY:
                    if (!$permissionCheck) {
                        $actionSet->setActionLabel(RecordActionConstant::VIEW_ACTION_LABEL);
                    }
                    $this->recordActions[] = $actionSet;
                    break;
                default:
                    if ($permissionCheck) {
                        $this->recordActions[] = $actionSet;
                    }
                    break;
            }
        }
    }
    public function addRestrictedPropAction(string $actionName)
    {
        return array_push($this->restrictedPropertyActions, $actionName);
    }

    protected function addRecordAction($record, $actionSet, $url = null)
    {
        return [
            self::ACTION_LABEL_STR => $actionSet->getActionLabel(),
            self::ACTION_CODE_STR => $actionSet->getActionCode(),
            self::ACTION_URL_STR => $url ?? $this->replaceRouteParameters(
                $actionSet->getUrl(),
                $this->getUrlParameters($record, $actionSet->getParameterNames())
            ),
            self::ACTION_TEMPLATE_STR => $actionSet->getTemplate()
        ];
    }
}
