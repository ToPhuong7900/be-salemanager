<?php

namespace App\Libs\BoxFilterQuery;

use App\Libs\BoxFilter\Constants\OperatorTypeConstant;
use App\Libs\BoxFilterQuery\FilterFieldSet\IFilterField;
use App\Libs\BoxFilterQuery\FilterFieldSet\IFilterOperator;
use App\Libs\BoxFilterQuery\Parsers\FilterDetailParser;
use App\Libs\Constant\CommonConstant;
use Tfcloud\Models\Contact;

trait ReportFilterQueryTrait
{
    /**
     * Define a field list that we want to apply sql function on for formatting or calculating in DB
     * @dataProvider
     * [
     *  'field_name' => ['sql_func', 'argument_1', 'argument_2', ...'argument_n']
     * ]
     * @example ['created_date' => ['DATE_FORMAT', '%Y-%m-%d']]
     *                              function name is always at index[0], Arguments start from index[1,2,3,..]
     * @var array
     */
    protected $reportViewSqlFunctionFields = [
        'user_date1' => ['STR_TO_DATE', '%d/%m/%Y'],
        'user_date2' => ['STR_TO_DATE', '%d/%m/%Y'],
        'user_date3' => ['STR_TO_DATE', '%d/%m/%Y'],
        'user_date4' => ['STR_TO_DATE', '%d/%m/%Y'],
    ];

    protected $filterDetails = [];

    public function getFilterDetailText(string $currentFilterText = null)
    {
        $filterDetailParser = $this->getFilterDetailParser();
        if ($currentFilterText) {
            $filterDetailParser->prependFilterDetailsText($currentFilterText);
        }

        $filterDetailParser->addFilterDetails($this->filterDetails);
        return $filterDetailParser->getFilterDetailsText();
    }

    public function handleReportMode()
    {
        $operatorParser = $this->getOperatorParser();
        $operatorParser->setPrefixTables($this->getPrefixView());
        $operatorParser->addSqlFunctionFields($this->reportViewSqlFunctionFields);
        return $this;
    }

    public function formatReportInputs($inputs)
    {
        return $inputs;
    }

    public function setReportExtraFilters($inputs)
    {
        $filterSet = $this->getFilterSet();
        $filterSet->extraFilters = array_get($inputs, 'extraFilters', []);
        return $this;
    }

    public function getExtraFilters()
    {
        $filterSet = $this->getFilterSet();
        return $filterSet->extraFilters;
    }

    protected function getPrefixView()
    {
        return [$this->getViewName() ?? '' => '*'];
    }

    protected function getFilterDetailParser()
    {
        $filterSet = $this->getFilterSet();
        return new FilterDetailParser($filterSet->getFilterFields(), $this->getFilterDetailMaps());
    }

    protected function addFilterDetails(string $fieldName, int $operator, array $values = [])
    {
        $operatorName = OperatorTypeConstant::getOperatorName($operator);
        $valueText = '';
        if (!empty($values)) {
            $valueText = implode(' and ', array_map(function ($value) {
                return "'$value'";
            }, $values));
        }
        $this->filterDetails[] = $fieldName . ' ' . $operatorName . ($valueText ? ' ' . $valueText : '');
    }

    protected function filterUserContact1($query, IFilterField $filterField, $view = null)
    {
        $this->applyUserDefineContactQuery($query, $filterField, 1, $view);
    }

    protected function filterUserContact2($query, IFilterField $filterField, $view = null)
    {
        $this->applyUserDefineContactQuery($query, $filterField, 2, $view);
    }

    protected function filterUserContact3($query, IFilterField $filterField, $view = null)
    {
        $this->applyUserDefineContactQuery($query, $filterField, 3, $view);
    }

    protected function filterUserContact4($query, IFilterField $filterField, $view = null)
    {
        $this->applyUserDefineContactQuery($query, $filterField, 4, $view);
    }

    protected function filterUserContact5($query, IFilterField $filterField, $view = null)
    {
        $this->applyUserDefineContactQuery($query, $filterField, 5, $view);
    }

    protected function filterUserContact6($query, IFilterField $filterField, $view = null)
    {
        $this->applyUserDefineContactQuery($query, $filterField, 6, $view);
    }

    protected function filterUserContact7($query, IFilterField $filterField, $view = null)
    {
        $this->applyUserDefineContactQuery($query, $filterField, 7, $view);
    }

    protected function filterUserContact8($query, IFilterField $filterField, $view = null)
    {
        $this->applyUserDefineContactQuery($query, $filterField, 8, $view);
    }

    protected function filterUserContact9($query, IFilterField $filterField, $view = null)
    {
        $this->applyUserDefineContactQuery($query, $filterField, 9, $view);
    }

    protected function filterUserContact10($query, IFilterField $filterField, $view = null)
    {
        $this->applyUserDefineContactQuery($query, $filterField, 10, $view);
    }

    protected function applyUserDefineContactQuery($query, IFilterField $filterField, $udfContactNum, $view)
    {
        if ($view) {
            $contactKey = $view . '.user_contact' . $udfContactNum;
            $contactOrgKey = $view . '.user_contact_org' . $udfContactNum;
            $this->applyUDCQueryForReport($query, $filterField, $contactKey, $contactOrgKey);
        } else {
            $this->applyOperatorToFilterField($query, $filterField);
        }
    }

    protected function applyUDCQueryForReport($query, IFilterField $filterField, $contactKey, $contactOrgKey)
    {
        foreach ($filterField->getFilterOperators() as $filterOperator) {
            $contacts = Contact::select([
                'contact_name', 'organisation'
            ])->userSiteGroup()->whereIn('contact_id', $filterOperator->getValues())->get();
            $names = $contacts->pluck('contact_name');
            $organisations = $contacts->pluck('organisation');

            switch ($filterOperator->getOperator()) {
                case OperatorTypeConstant::INCLUDE:
                    $query->whereIn($contactKey, $names);
                    $query->whereIn($contactOrgKey, $organisations);
                    break;
                case OperatorTypeConstant::NOT_INCLUDE:
                    $query->where(function ($q) use ($contactKey, $organisations, $contactOrgKey, $names) {
                        $q->whereNull($contactKey)->orWhere($contactKey, '=', '')
                            ->orWhere(function ($subQ) use ($contactKey, $organisations, $contactOrgKey, $names) {
                                $subQ->whereNotIn($contactKey, $names);
                                $subQ->whereNotIn($contactOrgKey, $organisations);
                            });
                    });
                    break;
                case OperatorTypeConstant::BLANK:
                    $query->where(function ($q) use ($contactKey) {
                        $q->whereNull($contactKey)->orWhere($contactKey, '=', '');
                    });
                    break;
                case OperatorTypeConstant::NOT_BLANK:
                    $query->whereNotNull($contactKey)->where($contactKey, '<>', '');
                    break;
            }
        }
        return $query;
    }

    protected function getYesNoFilterDetailParser()
    {
        return function (IFilterOperator $filterOperator) {
            $values = $filterOperator->getValues();

            $result = [];
            foreach ($values as $value) {
                if ($value == CommonConstant::DATABASE_VALUE_ONE) {
                    $result[] =  CommonConstant::DATABASE_VALUE_YES;
                } else {
                    $result[] =  CommonConstant::DATABASE_VALUE_NO;
                }
            }

            return $result;
        };
    }

    public function isApplyFilterQuery()
    {
        return true;
    }
}
