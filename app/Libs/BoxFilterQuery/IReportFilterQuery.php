<?php

namespace App\Libs\BoxFilterQuery;

interface IReportFilterQuery extends IBaseFilterQuery
{
    /**
     * @return array
     */
    public function getFilterDetailMaps();

    /**
     * @param string|null $currentFilterText
     * @return string
     */
    public function getFilterDetailText(string $currentFilterText = null);

    /**
     * @return $this
     */
    public function handleReportMode();

    /**
     * @param array $inputs
     * @return array
     */
    public function formatReportInputs(array $inputs);

    /**
    * @return boolean
    */
    public function isApplyFilterQuery();
}
