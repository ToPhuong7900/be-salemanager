<?php

namespace App\Libs\BoxFilterQuery;

use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Query\Builder as QueryBuilder;
use App\Libs\BoxFilter\Elements\Pickers\LocationPickerElement;
use App\Libs\BoxFilterQuery\FilterFieldSet\IFilterSet;
use App\Libs\BoxFilterQuery\Parsers\IOperatorParser;
use App\Libs\BoxFilterQuery\Parsers\OperatorParser;
use App\Libs\Chart\ChartJS\IChartConfig;
use App\Libs\TfHttp;
use Tfcloud\Services\Kpi\KpiService;
use Tfcloud\Services\PermissionService;

abstract class ChartFilterQuery implements IChartFilterQuery
{
    use FilterQueryTrait;

    /**
     * @var PermissionService
     */
    protected PermissionService $permissionService;

    /**
     * @var integer
     */
    protected int $kpiID;

    /**
     * @var null|integer
     */
    protected $limit = null;

    /**
     * @var bool
     */
    protected bool $isManagementOnly = false;

    /**
     * Define a field list that we want to apply sql function on for formatting or calculating in DB
     * @dataProvider
     * [
     *  'field_name' => ['sql_func', 'argument_1', 'argument_2', ...'argument_n']
     * ]
     * @example ['created_date' => ['DATE_FORMAT', '%Y-%m-%d']]
     *                              function name is always at index[0], Arguments start from index[1,2,3,..]
     * @var array
     */
    protected array $sqlFunctionFields = [];

    public function __construct($kpiID)
    {
        $this->kpiID = $kpiID;
        $this->permissionService = new PermissionService();
    }

    public function init(
        $filterNameId,
        IFilterSet $filterSet,
        array $inputs = [],
        IOperatorParser $operatorParser = null
    ) {
        $filterName = $this->getFilterName($filterNameId);
        $this->setFilterNameId($filterNameId);
        if (empty($this->getModuleId())) {
            $this->setModuleId($filterName->module_id);
        }
        $this->setFilterSet($filterSet, $inputs)
            ->setChartDataLimit($inputs)
            ->setOperatorParser($operatorParser ?? new OperatorParser(
                $this->getPrefixTables(),
                $this->sqlFunctionFields
            ));

        return $this;
    }

    public function canAccess()
    {
        $options = $this->isManagementOnly() ?  ['management-only' => true] : [];
        return !empty(KpiService::displayAllowed($this->kpiID, $this->getModuleId(), null, $options));
    }

    public function getKPIData()
    {
        $query = $this->filterAll($this->getQuery());

        if ($this->limit) {
            $query->take($this->limit);
        }

        $records = $query->get();
        $chartConfig = $this->formatKPIData($records);
        return $chartConfig->toArray();
    }

    /**
     * @return bool
     */
    public function isManagementOnly(): bool
    {
        return $this->isManagementOnly;
    }

    /**
     * @param bool $isManagementOnly
     * @return IChartFilterQuery
     */
    public function setIsManagementOnly(bool $isManagementOnly): IChartFilterQuery
    {
        $this->isManagementOnly = $isManagementOnly;
        return $this;
    }

    /**
     * Get prefix table to attach to field when run query
     * @dataProvider
     * [
     *  'table_name' => '*' // apply table_name to all filter field such as project.project_code
     *  'table_name' => 'field_name' // apply table_name to field name such as project_status.project_status_type_id
     *  'table_name' => ['field_name'] // support multiple field names as an array
     * ]
     *
     * @return string[]
     */
    abstract protected function getPrefixTables();

    /**
     * Get a main query of a list
     * @return EloquentBuilder|QueryBuilder
     */
    abstract protected function getQuery();

    /**
     * @param $records
     * @return IChartConfig
     */
    abstract protected function formatKPIData($records);

    protected function getChartFilterParams()
    {
        $filterParams = [];
        $filterFields = $this->getFilterSet()->getFilterFields();
        foreach ($filterFields as $index => $filter) {
            $group = null;
            if ($filter->getFieldName() == 'site_id' || $filter->getFieldName() == 'building_id') {
                $group = LocationPickerElement::LOCATION_PICKER_GROUP_NAME;
            }

            $param = TfHttp::getBoxFilterParams(
                $filter->getFieldName(),
                $filter->getFilterOperators()[0]->getOperator(),
                $filter->getFilterOperators()[0]->getValues(),
                $group
            );
            $filterParams = array_merge($filterParams, $param);
        }

        return $filterParams;
    }

    /**
     * @param $query
     * @param array $onlyFilterFields
     * @return EloquentBuilder|QueryBuilder
     */
    protected function filterOnly($query, array $onlyFilterFields = [])
    {
        foreach ($this->getFilterSet()->getFilterFields() as $filterField) {
            if (!in_array($filterField->getFieldName(), $onlyFilterFields)) {
                continue;
            }
            $this->filterByFilterField($query, $filterField);
        }

        return $this->filtered($query, $this->getViewName());
    }

    /**
     * @param $query
     * @param array $excludedFields
     * @return EloquentBuilder|QueryBuilder
     */
    protected function filterExcludedFields($query, array $excludedFields = [])
    {
        foreach ($this->getFilterSet()->getFilterFields() as $filterField) {
            if (in_array($filterField->getFieldName(), $excludedFields)) {
                continue;
            }
            $this->filterByFilterField($query, $filterField);
        }

        return $this->filtered($query, $this->getViewName());
    }

    private function setChartDataLimit($inputs)
    {
        $limit = array_get($inputs, 'limit', null);

        if ($limit && $limit <= 100) {
            $this->limit = $limit;
        }

        return $this;
    }
}
