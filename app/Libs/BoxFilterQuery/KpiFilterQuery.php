<?php

namespace App\Libs\BoxFilterQuery;

use App\Libs\Exceptions\StateException;

abstract class KpiFilterQuery implements IKpiFilterQuery
{
    public const KPI_MAP_CLASS = [];

    public function getChartFilterQuery($kpiID)
    {
        $chartFilterQuery = $this->getChartFilterClassMap($kpiID);
        $filterQuery = $chartFilterQuery ? new $chartFilterQuery($kpiID) : null;
        if ($filterQuery instanceof IChartFilterQuery) {
            return $filterQuery;
        }
        throw new StateException("filterQuery must be instance of IChartFilterQuery");
    }

    /**
     * @param $kpiID
     * @return array
     */
    protected function getChartFilterClassMap($kpiID)
    {
        return array_get(static::KPI_MAP_CLASS, $kpiID);
    }
}
