<?php

namespace App\Libs\BoxFilterQuery;

use App\Libs\BoxFilterQuery\FilterFieldSet\IFilterSet;
use App\Libs\BoxFilterQuery\Parsers\IOperatorParser;
use App\Libs\BoxFilterQuery\Parsers\OperatorParser;
use Tfcloud\Services\PermissionService;

abstract class ReportFilterQuery implements IReportFilterQuery
{
    use FilterQueryTrait;
    use ReportFilterQueryTrait;

    protected $permissionService;

    public function __construct()
    {
        $this->permissionService = new PermissionService();
    }

    public function init(
        $filterNameId,
        IFilterSet $filterSet,
        array $inputs = [],
        IOperatorParser $operatorParser = null
    ) {
        $filterName = $this->getFilterName($filterNameId);
        $this->setFilterNameId($filterNameId)
            ->setModuleId($filterName->module_id)
            ->setFilterSet($filterSet, $inputs)
            ->setReportExtraFilters($inputs)
            ->setOperatorParser(
                $operatorParser ?? new OperatorParser($this->getPrefixTables(), $this->reportViewSqlFunctionFields)
            );
        return $this;
    }

    protected function getPrefixTables()
    {
        return [];
    }
}
