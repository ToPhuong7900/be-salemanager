<?php

namespace App\Libs\BoxFilterQuery;

use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Query\Builder as QueryBuilder;
use App\Libs\BoxFilterQuery\FilterFieldSet\IFilterField;
use App\Libs\BoxFilterQuery\FilterFieldSet\IFilterSet;
use App\Libs\BoxFilterQuery\Parsers\IOperatorParser;

interface IBaseFilterQuery
{
    /**
     * @param $filterNameId
     * @param IFilterSet $filterSet
     * @param array $inputs
     * @param IOperatorParser|null $operatorParser
     * @return $this
     */
    public function init(
        $filterNameId,
        IFilterSet $filterSet,
        array $inputs,
        IOperatorParser $operatorParser = null
    );

    /**
     * @param IOperatorParser $operatorParser
     * @return $this
     */
    public function setOperatorParser(IOperatorParser $operatorParser);

    /**
     * @return IOperatorParser
     */
    public function getOperatorParser();

    /**
     * @param EloquentBuilder|QueryBuilder $query
     * @return EloquentBuilder|QueryBuilder
     */
    public function filterAll($query);

    /**
     * @param IFilterSet $filterSet
     * @param array $inputs
     * @return $this
     */
    public function setFilterSet(IFilterSet $filterSet, array $inputs);

    /**
     * @param IFilterSet $filterSet
     * @param array $inputs
     * @return mixed
     */
    public function inheritFilterSet(IFilterSet $filterSet);

    /**
     * @param string $fieldName
     * @return IFilterField|null
     */
    public function getFilterField(string $fieldName);

    /**
     * @param string $fieldName
     * @return array
     */
    public function getValueFilterField(string $fieldName);

    /**
     * @param string $viewName
     * @return $this
     */
    public function setViewName(string $viewName);

    /**
     * @return string
     */
    public function getViewName();

    /**
     * @param int $moduleId
     * @return $this
     */
    public function setModuleId(int $moduleId);

    /**
     * @param string $fieldName
     */
    public function getFirstValueFilterField(string $fieldName);
}
