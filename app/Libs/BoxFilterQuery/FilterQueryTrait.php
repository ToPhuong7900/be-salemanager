<?php

namespace App\Libs\BoxFilterQuery;

use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Support\Arr;
use App\Libs\BoxFilter\Helper\UserDefineBoxFilter;
use App\Libs\BoxFilterQuery\Constants\FilterQueryConstant;
use App\Libs\BoxFilterQuery\FilterFieldSet\IFilterField;
use App\Libs\BoxFilterQuery\FilterFieldSet\IFilterSet;
use App\Libs\BoxFilterQuery\Parsers\IOperatorParser;
use Tfcloud\Models\Filter\FilterName;

trait FilterQueryTrait
{
    /**
     * @var integer
     */
    private $filterNameId;

    /**
     * @var integer
     */
    private $moduleId;

    /**
     * @var IFilterSet
     */
    private $filterSet;

    /**
     * @var IOperatorParser
     */
    private $operatorParser;

    /**
     * @var null|string
     */
    private $viewName = null;

    /**
     * @var array
     */
    private $formattedSortFields = [];

    public function __call($method, $parameters)
    {
        if ($scope = $this->filterMethodScope($method)) {
            return $this->{$scope}(...$parameters);
        }
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setOperatorParser(IOperatorParser $operatorParser)
    {
        $this->operatorParser = $operatorParser;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getOperatorParser()
    {
        return $this->operatorParser;
    }

    /**
     * @inheritDoc
     */
    public function setFilterSet(IFilterSet $filterSet, array $inputs)
    {
        $this->filterSet = $filterSet->parseFilterFields(
            array_get($inputs, FilterQueryConstant::FILTER_PARAMETER_KEY, []),
            array_get($inputs, FilterQueryConstant::FILTER_RELATED_RESTRICT_DATA_KEY, [])
        );
        return $this;
    }

    public function inheritFilterSet(IFilterSet $filterSet)
    {
        $this->filterSet = clone $filterSet;
    }

    /**
     * @inheritDoc
     */
    public function getFilterField(string $fieldName)
    {
        foreach ($this->filterSet->getFilterFields() as $filterField) {
            if ($filterField->getFieldName() === $fieldName) {
                return $filterField;
            }
        }
        return null;
    }

    /**
     * @inheritDoc
     */
    public function getValueFilterField(string $fieldName)
    {
        if ($filterField = $this->getFilterField($fieldName)) {
            foreach ($filterField->getFilterOperators() as $filterOperator) {
                return $filterOperator->getValues();
            }
        }
        return null;
    }

    public function getFirstValueFilterField(string $fieldName)
    {
        if ($filterField = $this->getFilterField($fieldName)) {
            foreach ($filterField->getFilterOperators() as $filterOperator) {
                return Arr::first($filterOperator->getValues());
            }
        }
        return null;
    }

    /**
     * @inheritDoc
     */
    public function setViewName(string $viewName)
    {
        $this->viewName = $viewName;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getViewName()
    {
        return $this->viewName;
    }

    /**
     * @inheritDoc
     */
    public function setModuleId(int $moduleId)
    {
        $this->moduleId = $moduleId;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function filterAll($query)
    {
        if ($this->filterSet->enableUserDefine() && empty($this->getViewName())) {
            $this->applyUserDefineSet($query);
        }
        foreach ($this->filterSet->getFilterFields() as $filterField) {
            $this->filterByFilterField($query, $filterField);
        }
        return $this->filtered($query, $this->getViewName());
    }

    protected function filterByFilterField($query, $filterField)
    {
        if ($scope = $this->filterMethodScope($filterField->getFieldName())) {
            $this->{$scope}($query, $filterField, $this->getViewName());
        } else {
            $this->applyOperatorToFilterField($query, $filterField);
        }
        return $query;
    }

    /**
     * Called after a query is filtered from filterAll() method.
     * Acting as a hook to modify query such as checking permission or adding dynamic field.
     * @param EloquentBuilder|QueryBuilder $query
     * @param string|null $view
     * @return EloquentBuilder|QueryBuilder
     */
    protected function filtered($query, string $view = null)
    {
        return $query;
    }

    /**
     * @param EloquentBuilder|QueryBuilder $query
     * @param IFilterField $filterField
     */
    protected function applyOperatorToFilterField($query, IFilterField $filterField)
    {
        foreach ($filterField->getFilterOperators() as $filterOperator) {
            $this->operatorParser->addOperator(
                $query,
                $filterField->getFieldName(),
                $filterField->getFilterElementType(),
                $filterOperator->getOperator(),
                $filterOperator->getValues()
            );
        }
    }

    /**
     * @param string $method
     * @return string|null
     */
    protected function filterMethodScope(string $method)
    {
        $scope = 'filter' . ucfirst(camel_case($method));
        return method_exists($this, $scope) ? $scope : null;
    }

    /**
     * @return int
     */
    protected function getModuleId()
    {
        return $this->moduleId;
    }

    /**
     * @return int
     */
    protected function getFilterNameId()
    {
        return $this->filterNameId;
    }

    /**
     * @param int $filterNameId
     * @return $this
     */
    protected function setFilterNameId(int $filterNameId)
    {
        $this->filterNameId = $filterNameId;
        return $this;
    }

    /**
     * @param $fieldName
     * @param $formattedField
     * @return $this
     */
    protected function addFormattedSortField($fieldName, $formattedField)
    {
        $this->formattedSortFields[$fieldName] = $formattedField;
        return $this;
    }

    /**
     * @return array
     */
    public function getFormattedSortFields()
    {
        return $this->formattedSortFields;
    }

    /**
     * @return IFilterSet
     */
    public function getFilterSet()
    {
        return $this->filterSet;
    }

    /**
     * @param $filterNameId
     * @return FilterName
     * @throw ModelNotFoundException
     */
    protected function getFilterName($filterNameId)
    {
        return FilterName::findOrFail($filterNameId);
    }

    /**
     * @param EloquentBuilder|QueryBuilder $query
     * @return EloquentBuilder|QueryBuilder
     */
    protected function applyUserDefineSet($query)
    {
        $prefixUserDef = 'GUGFilter';
        $mainTableName = $this->filterSet->getUserDefineTableName();
        $genUserDefGroupIdName = $mainTableName ? "$mainTableName.gen_userdef_group_id" : 'gen_userdef_group_id';
        $query->leftjoin(
            "gen_userdef_group AS $prefixUserDef",
            "$prefixUserDef.gen_userdef_group_id",
            '=',
            $genUserDefGroupIdName
        );
        $this->addSelectedUserDefineSelect($prefixUserDef, $this->filterSet->getAvailableUserDefines(), $query);
        $this->operatorParser->addPrefixTables([
            $prefixUserDef => array_keys(UserDefineBoxFilter::getUserDefinedSet())
        ]);
        return $query;
    }

    /**
     * @param string $prefix
     * @param array $availableUserDefines
     * @param EloquentBuilder|QueryBuilder $query
     */
    protected function addSelectedUserDefineSelect(string $prefix, array $availableUserDefines, $query)
    {
        $userDefineSelects = [];
        foreach ($availableUserDefines as $type => $userDefines) {
            switch ($type) {
                case UserDefineBoxFilter::UDF_TYPE_TEXT:
                case UserDefineBoxFilter::UDF_TYPE_CHECK:
                    foreach ($userDefines as $name => $label) {
                        $userDefineSelects[] = $prefix . '.' . $name;
                    }
                    break;
                case UserDefineBoxFilter::UDF_TYPE_DATE:
                    foreach ($userDefines as $name => $label) {
                        $userDefineSelects[] = \DB::raw("DATE_FORMAT($prefix.$name, '%d/%m/%Y') AS $name");
                        $this->addFormattedSortField($name, $prefix . '.' . $name);
                    }
                    break;
                case UserDefineBoxFilter::UDF_TYPE_SELECT:
                    foreach ($userDefines as $name => $label) {
                        $aliasTable = $name . '_table';
                        $query->leftjoin(
                            "gen_userdef_sel AS $aliasTable",
                            "$aliasTable.gen_userdef_sel_id",
                            '=',
                            "$prefix.$name"
                        );
                        $userDefineSelects[] = \DB::raw("$aliasTable.gen_userdef_sel_code AS $name");
                    }
                    break;
                case UserDefineBoxFilter::UDF_TYPE_CONTACT:
                    foreach ($userDefines as $name => $label) {
                        $aliasTable = $name . '_table';
                        $query->leftjoin(
                            "contact AS $aliasTable",
                            "$aliasTable.contact_id",
                            '=',
                            "$prefix.$name"
                        );
                        $userDefineSelects[] = \DB::raw("$aliasTable.contact_name AS $name");
                    }
                    break;
            }
        }
        if (!empty($userDefineSelects)) {
            $query->addSelect($userDefineSelects);
        }
    }

    /**
     * By default, the value is passed to query builder is in string type.
     * In a regular query, there is no difference in performance,
     * Between passing an integer number value as a string type with actually integer type.
     * However, sometimes in a complex query, there is a significant improvement in performance,
     * If we pass the right type of value.
     * This function parses values to the integer type, only use it when we know the value is always a number.
     * @param EloquentBuilder|QueryBuilder $query
     * @param IFilterField $filterField
     * @return EloquentBuilder|QueryBuilder
     */
    protected function applyIntegerFilter($query, IFilterField $filterField)
    {
        foreach ($filterField->getFilterOperators() as $operator) {
            $ids = array_map(function ($item) {
                return (int)$item;
            }, $operator->getValues());
            $operator->setValues($ids);
        }
        $this->applyOperatorToFilterField($query, $filterField);

        return $query;
    }

    public function getOperatorFilterField(string $fieldName)
    {
        if ($filterField = $this->getFilterField($fieldName)) {
            foreach ($filterField->getFilterOperators() as $filterOperator) {
                return $filterOperator->getOperator();
            }
        }
        return null;
    }
}
