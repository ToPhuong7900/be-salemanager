<?php

namespace App\Libs\Audit;

interface IAuditable
{
    public function getTable();

    /**
     * Get the value of the models audit code.
     *
     * @return string
     */
    public function getAuditCode(): string;

    /**
     * @return array[
     * "field_name" => //Key is the field name to audit as 'site_code', 'site_id'
     *  [
     *      'auditLabel', //REQUIRED| The label for this audited field as 'Code', 'Description','Site'
     *      'relationshipModelClass', //OPTIONAL| Provide the model class, if the audited filed is a foreign key field
     *      ['field_name_1', 'field_name_2'] //OPTIONAL with 'relationshipModelClass'|
     *          Provide custom field names to get value from the relationship model.
     *          They are concatenated together via Common::concatFields().
     *          It is present in case we don't want to use the getAuditCode() of the relationship model to audit.
     *  ]
     * ]
     */
    public function getAuditFields(): array;
}
