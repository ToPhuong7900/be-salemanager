<?php

namespace App\Libs\Common;

class CommonConstant
{
    public const VERSION_SALE = '1.0';
    public const PRODUCT_NAME = 'Sale Manager';
    public const DATABASE_VALUE_YES = 'Y';
    public const DATABASE_VALUE_NO = 'N';
    public const TIME_LIMIT_300_SECOND = 300;
    public const SINGLE_PRODUCT = 1;
    public const VARIATION_PRODUCT = 2;
}
