<?php

namespace App\Libs\Common;

use Illuminate\Support\Arr;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Collection;
use App\Libs\Common\CommonConstant;
use App\Exceptions\StateException;
use App\Models\AuditAction;
use App\Models\BaseModel;
use App\Models\CrudAccessLevel;
use App\Models\Module;
use App\Models\SiteGroup;
use App\Models\User;
use App\Services\AuditService;
use Illuminate\Support\Str;

class Common
{
    /**
     * Returns either Windows Temp folder or custom folder as specified in the ENV file.
     */
    public static function getTempFolder()
    {
        $tmpFolder = Config::get('cloud.system.temp_folder');
        if (!file_exists($tmpFolder)) {
            throw new StateException('Failed to find System Temp Folder (' . $tmpFolder . ')');
        }
        return $tmpFolder;
    }

    /**
     * Returs a valid hexadecimal colour string.
     * @param String $colour   The colour to be formatted.
     * @param String $includeHash   Include leading hash in return value.
     * @return String
     */
    public static function formatColour($colour, $includeHash = true)
    {
        $colour = ltrim($colour, '#');

        if (
            ctype_xdigit($colour) &&
            (strlen($colour) == 6 )
        ) {
            return $includeHash ? '#' . $colour : $colour;
        } else {
            return '';
        }
    }

    /**
     * Returns true if the php process has been invoked from the command line.
     * @return bool
     */
    public static function invokedFromCommandLine(): bool
    {
        return php_sapi_name() === 'cli';
    }

    /**
     * Logs one off Audit messages for Records.
     * @param Object $auditObj The record being Audited.
     * @param String $message The message to be displayed.
     * @param null $parent
     * @param Integer $action Optional. The type of audit being recorded
     * @return void
     * @throws \Exception
     */
    public static function adHocAuditMsg($auditObj, string $message, $parent = null, $action = AuditAction::ACT_UPDATE): void
    {
        $audit = new AuditService();
        $audit->addAuditEntry($auditObj, $action, $parent, ['auditText' => $message]);
    }

    /**
     * Concatenates all entries wihin the fields array.
     * @param array $fields
     * @param array $options separator | truncate
     * @return string
     */
    public static function concatFields(array $fields, array $options = [])
    {
        $separator = ' - ';
        if (array_key_exists('separator', $options)) {
            $separator = $options['separator'];
        }

        if (Arr::get($options, 'unique')) {
            $fields = array_unique($fields);
        }

        $output = implode($separator, array_filter($fields, 'strlen'));

        if (array_key_exists('truncate', $options)) {
            $output = Common::truncateString($output, $options['truncate']);
        }

        return trim($output) == trim($separator) ? '' : $output;
    }
    public static function concatBookingFields(array $fields)
    {
        return self::concatFields(array_map(function ($item) {
            return Str::limit($item, 10);
        }, $fields));
    }

    public static function concatContactFields($contactName, $contactOrg, array $options = [])
    {
        if ($contactName === '-') {
            return $contactOrg;
        } else {
            return self::concatFields([$contactName, $contactOrg], $options);
        }
    }

    /**
     * Returns the first non-empty string in the array.
     * @param array $fields
     * @return array
     */
    public static function coalesceFields(array $fields)
    {
        $match = '';
        foreach ($fields as $field) {
            if ($field !== '') {
                $match = $field;
                break;
            }
        }
        return $match;
    }

    /**
     * Returns the first non-empty string (after trim) in the array.
     * @param array $fields
     * @return array
     */
    public static function coalesceFieldsTrim(array $fields)
    {
        $match = '';
        foreach ($fields as $field) {
            $trimmed = trim($field);
            if ($trimmed !== '') {
                $match = $trimmed;
                break;
            }
        }
        return $match;
    }
    /**
     * If the $fullString is longer than $maxLen then it is truncated and ... appended.
     * @param string $fullString
     * @param integer $maxLen
     * @param boolean $hasEtc
     * @return string
     */
    public static function truncateString($fullString, $maxLen, $hasEtc = true)
    {
        if (!$fullString && !is_numeric($fullString)) {
            return '';
        }

        $truncatedString = $fullString;
        if (strlen($fullString) > $maxLen) {
            $truncatedString = mb_substr($fullString, 0, $maxLen - ($hasEtc ? 3 : 0));
            $truncatedString .= $hasEtc ? '...' : '';
        }
        return $truncatedString;
    }

    /**
     * If the $fullString is longer than $maxLen then it is truncated and ... appended at first.
     * @param type $fullString
     * @param type $maxLen
     * @return string
     */
    public static function truncateStringBackward($fullString, $maxLen)
    {
        if (!$fullString) {
            return '';
        }

        $truncString = $fullString;
        if (strlen($fullString) > $maxLen) {
            $start = strlen($fullString) - $maxLen + 4;
            $truncString = "..." . mb_substr($fullString, $start);
        }
        return $truncString;
    }

    /**
     * Converts empty string or the string of null values in to null.  Used when saving non required numeric form fields
     *
     * @param type $value
     * @return null
     */
    public static function emptyToNull($value)
    {
        if (trim($value) == '' || $value === 'null') {
            return null;
        } else {
            return $value;
        }
    }

    /**
     * Returns true if $haystack starts with the supplied $needle.
     */
    public static function strStartsWith($haystack, $needle)
    {
        return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
    }

    /**
     * Returns true if $haystack ends with the $needle.
     */
    public static function strEndsWith($haystack, $needle)
    {
        return $needle === "" ||
            (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
    }

    public static function excelType($file)
    {
        if (
            $file->getMimeType() == "text/csv" ||
            $file->getClientOriginalExtension() == "csv" ||
            $file->getClientOriginalExtension() == "xls" ||
            $file->getClientOriginalExtension() == "xlsx" ||
            $file->getMimeType() == "application/vnd.ms-excel" ||
            $file->getMimeType() == "application/vnd.ms-office"
        ) {
            return true;
        }
        return false;
    }

    /**
     * Convert a Y or N string to a boolean.
     *
     * @param  string $value
     * @return boolean
     */
    public static function valueYorNtoBoolean($value)
    {
        return $value === CommonConstant::DATABASE_VALUE_YES || $value == '1' ? true : false;
    }
    public static function valueYorNtoNumeric($value)
    {
        return $value === CommonConstant::DATABASE_VALUE_YES || $value == '1' ? 1 : 0;
    }

    public static function arrayDepth(array $array)
    {
        $max_depth = 1;

        foreach ($array as $value) {
            if (is_array($value)) {
                $depth = self::arrayDepth($value) + 1;

                if ($depth > $max_depth) {
                    $max_depth = $depth;
                }
            }
        }

        return $max_depth;
    }

    /**
     * Convert a 1 number to X character
     * @param  string $value
     * @return string
     */
    public static function valueBooleanNumericToX($value)
    {
        return $value == '1' ? 'X' : '';
    }

    public static function valueBooleanNumericToYesNo($value)
    {
        return $value == '1' ? 'Y' : 'N';
    }

    public static function valueYesNoToYN($value)
    {
        if ($value === 'Yes') {
            return 'Y';
        }
        if ($value === 'No') {
            return 'N';
        }
        return '';
    }

    public static function valueYNToYesNo($value)
    {
        if ($value == CommonConstant::DATABASE_VALUE_YES) {
            return 'Yes';
        }
        if ($value == CommonConstant::DATABASE_VALUE_NO) {
            return 'No';
        }
        return '';
    }

    public static function getImplodedArrayValue(array $data, $value, $appendZero = false)
    {
        return ((bool)$appendZero ? '0,' : '') . implode(',', array_pluck($data, $value));
    }

    /**
     * Assign field value for new model.
     *
     * @param object $model model object to update e.g. Site.
     * @param string $property model field name e.g. site_code.  This is the name of the model field to set
     * and should match the form field id
     * @param array $inputs input data.
     * @param mixed $default optional default value to assign to model property if the input field isn't present.
     */
    public static function assignField($model, $property, array $inputs, $default = null, $options = [])
    {
        if (array_key_exists($property, $inputs)) {
            $model->setAttribute(
                $property,
                Arr::get($options, 'emptyToNull', true) ? Common::emptyToNull($inputs[$property]) : $inputs[$property]
            );
            if (Arr::get($options, 'emptyToZero', false) && empty($inputs[$property])) {
                $model->setAttribute($property, 0);
            }
        } else {
            if (!is_null($default)) {
                $model->setAttribute($property, $default);
            }
        }
    }

    /**
     * Assign checkbox field value for new model.
     *
     * @param object $model model object to update e.g. Site.
     * @param string $property model field name e.g. active.  This is the name of the model field to set,
     * and should match the form field id
     * @param array $inputs input data.
     * @param mixed $default optional default value to assing to model property if the input field isn't present.
     */
    public static function assignCheckboxField($model, $property, array $inputs, $default = 'N')
    {
        if (array_key_exists($property, $inputs)) {
            $model->setAttribute($property, ($inputs[$property] == 'true' || $inputs[$property] == 'Y') ? 'Y' : 'N');
        } else {
            $model->setAttribute($property, $default);
        }
    }

    public static function assignFieldTrueFalse($model, $property, array $inputs, $default = null)
    {
        if (array_key_exists($property, $inputs)) {
            $model->setAttribute(
                $property,
                $inputs[$property] === 'Y' ? 1 : ($inputs[$property] === 'N' ? 0 : $inputs[$property])
            );
        } else {
            if (!is_null($default)) {
                $model->setAttribute($property, $default);
            }
        }
    }

    public static function assignCheckboxFieldTrueFalse($model, $property, array $inputs, $default = '0')
    {
        if (array_key_exists($property, $inputs)) {
            $model->setAttribute($property, $inputs[$property] == 'true' ? '1' : '0');
        } else {
            $model->setAttribute($property, $default);
        }
    }

    /**
     * Assign field value for new model.
     *
     * @param object $model model object to update e.g. Site.
     * @param string $property model field name e.g. site_code.  This is the name of the model field to set
     * and should match the form field id
     * @param array $inputs input data.
     * @param mixed $default optional default value to assign to model property if the input field isn't present.
     */
    public static function assignArray($model, $property, array $inputs, $default = [])
    {
        if (array_key_exists($property, $inputs)) {
            $model->setAttribute($property, $inputs[$property]);
        } else {
            if ($default) {
                $model->setAttribute($property, $default);
            }
        }
    }


    /**
     * Update field value for existing model.  The field is only updated if it is present in the $inputs array.
     *
     * @param object $model model object to update e.g. Site.
     * @param string $property model field name e.g. site_code.  This is the name of the model field to set.
     * @param string $inputField name of the input field.  This is typically the form field id.
     * @param array $inputs input data.
     */
    public static function updateField($model, $property, $inputField, array $inputs)
    {
        if (array_key_exists($inputField, $inputs)) {
            $model->$property = $inputs[$inputField];
        }
    }

    /**
     * Return a formatted date sting for display in a text field.
     *
     * @param string $field
     * @param array $options
     * @return string
     */
    public static function dateFieldDisplayFormat($field, array $options = [])
    {
        $formattedDate = null;
        $returnEmptyOnFail = Arr::get($options, 'returnEmptyOnFail', false);
        if ($field) {
            $dateRegex = '[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])';
            $timeRegex = '([0-9]{2}:[0-9]{2}:[0-9]{2})';
            if (
                !preg_match("/^{$dateRegex}$/", $field) &&
                !(preg_match("/^{$dateRegex} {$timeRegex}$/", $field))
            ) {
                return $returnEmptyOnFail ? '' : $field;
            }

            $format = isset($options['hide-year']) ? 'd/m' : 'd/m/Y';

            $includeTime = false;
            if (array_key_exists('includeTime', $options)) {
                $includeTime = $options['includeTime'];
            }

            if ($includeTime) {
                $format .= ' H:i';
            }

            if (array_key_exists('dateFormat', $options)) {
                $format = $options['dateFormat'];
            }

            $time = new DateTime($field);
            $formattedDate = $time->format($format);
        }

        return $formattedDate;
    }

    /*
     * Combines the date and time fields into a single string for display.
     */
    public static function dateAndTimeDisplayFormat($dateField, $timeField, array $options = [])
    {
        $dateFormat = self::dateFieldDisplayFormat($dateField, $options);
        if ($dateFormat && $timeField) {
            $dateFormat .= " $timeField";
        }
        return $dateFormat;
    }

    /**
     * Converts the $field value to datetime sql format.
     * @param string $field
     * @param array $options
     * @return string
     */
    public static function dateFieldToSqlFormat($field, array $options = [])
    {
        $formattedDate = null;

        $validator = \Validator::make(['date' => $field], ['date' => ['date_format:d/m/Y']]);
        if (!$validator->passes() || empty($field)) {
            /*
             * Wait for reviewing CLD-6013
             * */
            return $field;
        }

        if ($field != '') {
            $inputFormat = "%d/%d/%d";
            if (array_key_exists('inputFormat', $options)) {
                $inputFormat = $options['inputFormat'];
            }

            $includeTime = true;
            if (array_key_exists('includeTime', $options)) {
                $includeTime = $options['includeTime'];
            }

            $includeTimeValue = '00:00:00';
            if (array_key_exists('includeTimeValue', $options)) {
                $includeTimeValue = $options['includeTimeValue'];
            }

            sscanf($field, $inputFormat, $day, $month, $year);

            $day = str_pad($day, 2, '0', STR_PAD_LEFT);
            $month = str_pad($month, 2, '0', STR_PAD_LEFT);

            $time = $includeTime ? ' ' . $includeTimeValue : '';

            $formattedDate = "$year-$month-$day$time";
        }
        return $formattedDate;
    }

    public static function dateFieldToSqlFormatForFailCase($original, $formattedDate, $options = [])
    {
        $includeTime = Arr::get($options, 'includeTime', false);
        $time = "";
        if ($includeTime) {
            $datetime = explode(' ', $formattedDate);
            if (count($datetime) == 2) {
                $time = " " . $datetime[1];
                $formattedDate = $datetime[0];
            }
        }

        $arrOriginalDate = explode('/', $original);
        if (count($arrOriginalDate) == 3) {
            if (
                (strlen($arrOriginalDate[0]) != 2) || (strlen($arrOriginalDate[1]) != 2) ||
                (strlen($arrOriginalDate[2]) != 4)
            ) {
                $formattedDate = str_replace('/', '-', $original);
            }
        }

        $arrDate = explode('-', $formattedDate);
        if (count($arrDate) == 3) {
            if ((strlen($arrDate[0]) != 4) || (strlen($arrDate[1]) != 2) || (strlen($arrDate[2]) != 2)) {
                $formattedDate = str_replace('-', '/', $formattedDate);
            }
        }

        return $formattedDate . $time;
    }

    public static function getRagIcon($id)
    {
        if (GenRagStatus::RED == $id) {
            return 'background-rag-red';
        } elseif (GenRagStatus::AMBER == $id) {
            return 'background-rag-amber';
        } elseif (GenRagStatus::GREEN == $id) {
            return 'background-rag-green';
        } else {
            return '';
        }
    }
    public static function getRagIconFromTotal($val)
    {
        $ragAll = PlantOverallRag::userSiteGroup()->get();
        $ragAllRed = $ragAll->filter(function ($item) {
            return $item->gen_rag_status_id == GenRagStatus::RED;
        })->first();
        $ragAllAmber = $ragAll->filter(function ($item) {
            return $item->gen_rag_status_id == GenRagStatus::AMBER;
        })->first();
        $ragAllGreen = $ragAll->filter(function ($item) {
            return $item->gen_rag_status_id == GenRagStatus::GREEN;
        })->first();
        if ($val >= $ragAllGreen->overall_weighting_from && $val < $ragAllAmber->overall_weighting_from) {
            return static::getRagIcon(GenRagStatus::GREEN);
        } elseif ($val >= $ragAllAmber->overall_weighting_from && $val < $ragAllRed->overall_weighting_from) {
            return static::getRagIcon(GenRagStatus::AMBER);
        } elseif ($val >= $ragAllRed->overall_weighting_from) {
            return static::getRagIcon(GenRagStatus::RED);
        } else {
            return '';
        }
    }

    public static function dateFieldToSqlFormatWithoutTime($field, array $options = [])
    {
        $options['includeTime'] = false;
        return self::dateFieldToSqlFormat($field, $options);
    }

    /**
     * Converts the $field value to datetime sql format.
     * @param type $field
     * @param array $options
     * @return type
     */
    public static function dateTimeFieldToSqlFormat($field, array $options = [])
    {
        $formattedDate = null;

        if (trim($field) != '') {
            $inputFormat = '%d/%d/%d %d:%d';
            if (array_key_exists('inputFormat', $options)) {
                $inputFormat = $options['inputFormat'];
            }

            sscanf($field, $inputFormat, $day, $month, $year, $hour, $min);

            $day = str_pad($day, 2, '0', STR_PAD_LEFT);
            $month = str_pad($month, 2, '0', STR_PAD_LEFT);
            $hour = str_pad($hour, 2, '0', STR_PAD_LEFT);
            $min = str_pad($min, 2, '0', STR_PAD_LEFT);

            $time = " $hour:$min:00";

            $formattedDate = "$year-$month-$day$time";
        }

        return $formattedDate;
    }


    /**
     * Converts the $field value to datetime sql format (including seconds).
     * @param type $field
     * @param array $options
     * @return type
     */
    public static function dateTimeFieldToSqlFormatInclSeconds($field, array $options = [])
    {
        $formattedDate = null;

        if (trim($field) != '') {
            $inputFormat = '%d/%d/%d %d:%d:%d';
            if (array_key_exists('inputFormat', $options)) {
                $inputFormat = $options['inputFormat'];
            }

            sscanf($field, $inputFormat, $day, $month, $year, $hour, $min, $sec);

            $day = str_pad($day, 2, '0', STR_PAD_LEFT);
            $month = str_pad($month, 2, '0', STR_PAD_LEFT);
            $hour = str_pad($hour, 2, '0', STR_PAD_LEFT);
            $min = str_pad($min, 2, '0', STR_PAD_LEFT);
            $sec = str_pad($sec, 2, '0', STR_PAD_LEFT);

            $time = " $hour:$min:$sec";

            $formattedDate = "$year-$month-$day$time";
        }

        return $formattedDate;
    }


    /**
     * Converts the $field value to datetime sql format.
     * @param type $field
     * @param array $options
     * @return type
     */
    public static function dateTimeFieldToSqlFormatFromDateAndTime($dateField, $timeField, $alias)
    {

        $dateString = "str_to_date(concat(date_format($dateField,"
            . '"%Y-%m-%d"),'
            . '" ",'
            . "$timeField,"
            . '":00"),'
            . '"%Y-%m-%d %H:%i:%s")';

        if ($alias) {
            $dateString .= " AS $alias";
        }


        return $dateString;
    }

    /**
     * Update time element of sql datetime string
     * @param string $inputField time field string hh:ss
     * @param $dbField Y-m-d h:i:s
     * @return string
     */
    public static function updateTimeElement($inputField, $dbField, array $options = [])
    {
        if (empty($dbField) && Arr::get($options, 'ignoreNow')) {
            return null;
        }

        if (empty($dbField)) {
            $dbField = (new \DateTime())->format('Y-m-d H:i:s');
        }

        $dbFieldFormat = '%d-%d-%d %d:%d:%d';
        \sscanf($dbField, $dbFieldFormat, $year, $month, $day, $hour, $min, $second);
        $year = str_pad($year, 4, '0', STR_PAD_LEFT);
        $month = str_pad($month, 2, '0', STR_PAD_LEFT);
        $day = str_pad($day, 2, '0', STR_PAD_LEFT);
        $hour = str_pad($hour, 2, '0', STR_PAD_LEFT);
        $min = str_pad($min, 2, '0', STR_PAD_LEFT);
        $second = str_pad($second, 2, '0', STR_PAD_LEFT);

        // Time input field
        $timeFieldFormat = '%d:%d';
        \sscanf($inputField, $timeFieldFormat, $inputHour, $inputMin);
        $inputHour = str_pad($inputHour, 2, '0', STR_PAD_LEFT);
        $inputMin = str_pad($inputMin, 2, '0', STR_PAD_LEFT);

        return "$year-$month-$day " . $inputHour . ':' . $inputMin . ':' . $second;
    }

    /**
     * Convert Hours:Minutes to a decimal.
     */
    public static function convertHrsToDecimal($timeWorked)
    {
        $hms = explode(":", $timeWorked);
        return $hms[0] + ($hms[1] / 60) + ($hms[2] / 3600);
    }

    /**
     * Check date is valid.  Expected date format is yyyy-mm-dd.
     */
    public static function validSqlDate($date)
    {
        if (strlen($date) !== 10) {
            return false;
        }
        $year = '';
        $month = '';
        $day = '';
        \sscanf($date, '%d-%d-%d', $year, $month, $day);

        return $month && $day && $year ? \checkdate($month, $day, $year) : false;
    }

    /**
     * Update date element of sql datetime string
     * @param string $inputField date field string d/m/Y
     * @param $dbField Y-m-d h:i:s
     * @return string
     */
    public static function updateDateElement($inputField, $dbField)
    {
        if (empty($dbField)) {
            $dbField = (new \DateTime())->format('Y-m-d H:i:s');
        }

        $dbFieldFormat = '%d-%d-%d %d:%d:%d';
        \sscanf($dbField, $dbFieldFormat, $year, $month, $day, $hour, $min, $second);
        $year = str_pad($year, 4, '0', STR_PAD_LEFT);
        $month = str_pad($month, 2, '0', STR_PAD_LEFT);
        $day = str_pad($day, 2, '0', STR_PAD_LEFT);
        $hour = str_pad($hour, 2, '0', STR_PAD_LEFT);
        $min = str_pad($min, 2, '0', STR_PAD_LEFT);
        $second = str_pad($second, 2, '0', STR_PAD_LEFT);

        // Date input field
        $dateFieldFormat = '%d/%d/%d';
        \sscanf($inputField, $dateFieldFormat, $inputDay, $inputMonth, $inputYear);
        $inputYear = str_pad($inputYear, 4, '0', STR_PAD_LEFT);
        $inputMonth = str_pad($inputMonth, 2, '0', STR_PAD_LEFT);
        $inputDay = str_pad($inputDay, 2, '0', STR_PAD_LEFT);

        return "$inputYear-$inputMonth-$inputDay " . $hour . ':' . $min . ':' . $second;
    }

    /**
     * Return a formated time sting for display in a text field.
     *
     * @param string $field
     * @return string
     */
    public static function dateFieldToTimeDisplayFormat($field)
    {
        $formattedTime = '';
        $time = strtotime($field);
        if ($time !== false) {
            $formattedTime = date('H:i', $time);
        }

        return $formattedTime;
    }

    public static function dateFieldToTimeDisplayFormatUseDateTimeObj($field, $format = '')
    {
        $formattedTime = '';
        $timezone = new \DateTimeZone('UTC');
        $dateTimeObj = DateTime::createFromFormat('Y-m-d H:i:s', $field, $timezone);
        if ($dateTimeObj) {
            $formattedTime = $dateTimeObj->format($format ? $format : 'H:i');
        }

        return $formattedTime;
    }

    /**
     * Combine and convert the formatted date/time (optional) strings into a single DateTime object
     * @param type $data Expects ['date' => 'd/m/Y', 'time' => 'hh:ss']
    @return DateTime object
     */
    public static function stringToDateTime(array $data)
    {
        if (!array_key_exists('date', $data)) {
            throw new StateException("Invalid parameters supplied to function Common::stringToDateTime()");
        }

        $time = '00:00';
        if (array_key_exists('time', $data)) {
            $time = $data['time'];
        }

        sscanf($data['date'], "%02s/%02s/%04s", $day, $month, $year);
        return new \DateTime("$year-$month-$day $time");
    }

    /**
     * Returns the count value formatted for display in sidebar link.
     * @param type $count
     * @return string
     */
    public static function formatLinkCount($count)
    {
        return $count && is_numeric($count) ? "($count)" : '(0)';
    }

    /**
     *
     * @param string $string Input string
     * @param int $start Default = 0
     * @param int $length Default = 100
     * @return string
     */
    public static function cutString($string = "", $start = 0, $length = 100)
    {
        if ($string) {
            // Replace html ascii code
            $content = self::removeAllTagsHtml($string);
            if (strlen($content) > $length) {
                $arr_content = explode(" ", $content);
                $c1 = substr($content, $start, $length - 3);
                $arr_c1 = explode(" ", $c1);
                if (count($arr_c1) > 1) {
                    if (
                        isset($arr_content[count($arr_c1) - 1]) &&
                        $arr_c1[count($arr_c1) - 1] != $arr_content[count($arr_c1) - 1]
                    ) {
                        unset($arr_c1[count($arr_c1) - 1]);
                    }
                }
                $c1 = implode(" ", $arr_c1);
                return strlen($c1) < strlen($content) ? $c1 . "..." : $c1;
            } else {
                return $content;
            }
        }
        return $string;
    }

    public static function removeAllTagsHtml($string = "")
    {
        // Convert from html ascii code to tag
        $string = html_entity_decode($string);

        // Disable warning error in DOM
        libxml_use_internal_errors(true);

        // Remove textarea content
        $string = preg_replace("(<textarea .*?>.*?</textarea>)is", "", $string);

        // Remove javascript
        $string = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $string);

        // Replace html tag
        $string = trim(strip_tags($string));
        return $string;
    }

    /**
     * Converts a collection to an array with snake cased keys with the ability to index the array by a string & ability
     * to ignore columns that contain the word id in them. This is mainly used for the public and mobile APIs
     * @param \Illuminate\Database\Eloquent\Collection $data
     * @param string $arrayKey
     * @param boolean $ignoreIds
     * @return array
     */
    public static function convertCollectionToSnakeCaseArray($data, $arrayKey = false, $ignoreIds = false)
    {
        $output = [];
        foreach ($data as $item) {
            $itemData = [];

            foreach ($item->getOriginal() as $key => $value) {
                if (($ignoreIds && !strstr($key, 'id')) || !$ignoreIds) {
                    if ($arrayKey) {
                        $itemData[$arrayKey][self::snakeCase($key)] = $value;
                    } else {
                        $itemData[self::snakeCase($key)] = $value;
                    }
                }
            }

            if ($item->getRelations()) {
                foreach ($item->getRelations() as $relationship => $relationshipData) {
                    if (!is_null($relationshipData)) {
                        if (
                            $relationshipData instanceof \Illuminate\Database\Eloquent\Collection &&
                            count($relationshipData)
                        ) {
                            $subRelationship = rtrim($relationship, 's');
                            if ($arrayKey) {
                                $itemData[$arrayKey][self::snakeCase($relationship)] =
                                    self::convertCollectionToSnakeCaseArray(
                                        $relationshipData,
                                        $subRelationship,
                                        $ignoreIds
                                    );
                            } else {
                                $itemData[self::snakeCase($relationship)] =
                                    self::convertCollectionToSnakeCaseArray(
                                        $relationshipData,
                                        $subRelationship,
                                        $ignoreIds
                                    );
                            }
                        } else {
                            if ($relationshipData->count()) {
                                foreach ($relationshipData->getOriginal() as $k => $v) {
                                    if (($ignoreIds && !strstr($k, 'id')) || !$ignoreIds) {
                                        if ($arrayKey) {
                                            $itemData[$arrayKey][self::snakeCase($relationship)][self::snakeCase($k)] =
                                                $v;
                                        } else {
                                            $itemData[self::snakeCase($relationship)][self::snakeCase($k)] = $v;
                                        }
                                    }
                                }
                            } else {
                                if (($ignoreIds && !strstr($key, 'id')) || !$ignoreIds) {
                                    if ($arrayKey) {
                                        $itemData[$arrayKey][self::snakeCase($relationship)] = [];
                                    } else {
                                        $itemData[self::snakeCase($relationship)] = [];
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $output[] = $itemData;
        }

        return $output;
    }

    public static function snakeCase($value)
    {
        $lowerCase    = strtolower($value);
        $removeSpaces = preg_replace('/\s+/', '_', $lowerCase);
        $replaced     = preg_replace('/\)|\(|[.-]/', '', $removeSpaces);
        return snake_case($replaced);
    }

    public static function coAddWhere(&$query, &$bWhere)
    {
        if (!$bWhere) {
            $query .= " WHERE ";
            $bWhere = true;
        } else {
            $query .= " AND ";
        }

        return;
    }

    public static function addDays($number, $currentDate)
    {
        $dt = new \DateTime($currentDate);
        $dt->add(\DateInterval::createFromDateString("+$number day"));

        return $dt->format('Y-m-d');
    }

    /**
     * Searches an array where the value is equal to one of the array element values and returns the array key
     * @param  mixed $value
     * @param  mixed $key
     * @param  array $array
     * @return mixed
     */
    public static function searchArray($value, $key, array $array)
    {
        foreach ($array as $k => $val) {
            if ($val[$key] == $value) {
                return $k;
            }
        }
        return null;
    }

    public static function subDays($number, $currentDate)
    {
        $dt = new \DateTime($currentDate);
        $dt->add(\DateInterval::createFromDateString("-$number day"));

        return $dt->format('Y-m-d');
    }

    public static function addWeeks($number, $currentDate)
    {
        $dt = new \DateTime($currentDate);
        $dt->add(\DateInterval::createFromDateString("+$number week"));

        return $dt->format('Y-m-d');
    }

    public static function addMonths($number, $currentDate)
    {
        $dt = new \DateTime($currentDate);
        $dt->add(\DateInterval::createFromDateString("+$number month"));

        return $dt->format('Y-m-d');
    }

    public static function addYears($number, $currentDate)
    {
        $dt = new \DateTime($currentDate);
        $dt->add(\DateInterval::createFromDateString("+$number year"));

        return $dt->format('Y-m-d');
    }

    public static function currencyFormat($number, $encoded = true)
    {
        $options = ['encoded' => $encoded];
        return self::numberFormat($number, true, ",", 2, $options);
    }

    public static function currencyNegativeFormat(
        $number,
        $decimals = 2,
        $showCurrencySymbol = false,
        $thousandsSep = ','
    ) {
        // Check if the value is negative so that we can add brackets if required.
        $bNegative = !ctype_digit((string)((int)$number));
        $formattedValue = self::numberFormat(abs($number), $showCurrencySymbol, $thousandsSep, $decimals);
        return $bNegative ? "(" . $formattedValue . ")" : $formattedValue;
    }

    public static function absNumFormat($number)
    {
        return self::numberFormat(abs($number));
    }

    public static function numberFormatEx($number, array $options = [])
    {
        $showCurrencySymbol = Arr::get($options, 'show-currency-symbol', false);
        $thousandsSep = Arr::get($options, 'thousands-seperator', ',');
        $decimals = Arr::get($options, 'decimal', 2);

        return Common::numberFormat($number, $showCurrencySymbol, $thousandsSep, $decimals, $options);
    }

    public static function numberFormat(
        $number,
        $showCurrencySymbol = false,
        $thousandsSep = ',',
        $decimals = 2,
        array $options = []
    ) {

        $defaultZero = Arr::get($options, 'default-zero', true);
        $encoded = Arr::get($options, 'encoded', true);

        $currencySymbol =
            $showCurrencySymbol ? $encoded ? CommonConstant::CURRENCY_SYMBOL :
                CommonConstant::CURRENCY_SYMBOL_CHARACTER : "";
        if (is_numeric($number)) {
            $formated = $currencySymbol . number_format($number, $decimals, '.', $thousandsSep);
        } else {
            if ($defaultZero) {
                $formated = $currencySymbol . $decimals == CommonConstant::DATABASE_VALUE_ZERO ?
                    CommonConstant::DATABASE_VALUE_ZERO : CommonConstant::ZERO_VALUE;
            } else {
                $formated = '';
            }
        }
        return $formated;
    }

    /**
     * Return a formated date sting for display in a text field.
     *
     * @param type $field
     * @return string
     */
    public static function dateTimeFieldDisplayFormat($field, array $options = [])
    {
        if (empty($field) || $field == '0000-00-00') {
            return '';
        }
        $year = "";
        $month = "";
        $day = "";
        $timeHM = "";
        $date = self::verifyDate($field, $year, $month, $day, $timeHM);
        if (!is_null($date)) {
            $formattedDate = $day . "/" . $month;
            if (!isset($options['hide-year'])) {
                $formattedDate .= "/" . $year;
            }

            if (array_key_exists('includeTime', $options)) {
                $formattedDate .= " " . $timeHM;
            }
            return $formattedDate;
        }
    }

    public static function iset(&$variable)
    {
        return ($variable === null) ? "" : $variable;
    }

    public static function routeToPath($route = "")
    {
        if ($route) {
            if (stristr($route, "-")) {
                $route = explode("-", $route);
                foreach ($route as $key => &$value) {
                    $value = $key ? ucwords($value) : $value;
                }
                return implode("", $route);
            }
            return $route;
        }
        return "";
    }

    public static function verifyDate($date, &$year = "", &$month = "", &$day = "", &$timeHM = "")
    {
        // Array of dates which the PHP date handler has an issue with 3969
        $valid400Years = array('2369-12-31', '2769-12-31', '3169-12-31', '3569-12-31', '3969-12-31', '4369-12-31');

        $verifiedDate = null;
        $timeHM = "";

        if (!is_null($date) && !empty($date)) {
            $dateYMD = substr($date, 0, 10);
            if (in_array($dateYMD, $valid400Years)) {
                // Workaround PHP 400 years bug for 31 December
                $verifiedDate = $dateYMD;
                $month = '12';
                $day = '31';
                $year = substr($date, 0, 4);
                $timeHM = substr($date, 11, 5);
            } else {
                // Normal date checking
                $dt = new \DateTime($date);
                $dateYMD = $dt->format('Y-m-d');
                $d = explode('-', $dateYMD);
                if (is_numeric($d[0]) && is_numeric($d[1]) && is_numeric($d[2])) {
                    if (checkdate($d[1], $d[2], $d[0])) {
                        $year = $d[0];
                        $month = $d[1];
                        $day = $d[2];
                        $colonPos = strpos($date, ":");
                        if ($colonPos !== false) {
                            // Just return the HH:MM part
                            $timeHM = substr($date, $colonPos - 2, 5);
                        }
                        $verifiedDate = $dateYMD;
                    }
                }
            }
        }
        return $verifiedDate;
    }

    public static function convertArrayToUtf8($array)
    {
        array_walk_recursive($array, 'self::encodeToUtf8');
        return $array;
    }

    public static function encodeToUtf8(&$item)
    {
        $item = utf8_encode($item);
    }

    /**
     * Lookups and sets the parent location keys from the lowest level supplied.
     *
     * This function is used to ensure the correct location keys are saved.
     *
     * @param type $siteId
     * @param type $buildingId
     * @param type $roomId
     * @return boolean True if lookup was successfull.
     */
    public static function lookupParentLocationKeys(&$siteId, &$buildingId = null, &$roomId = null)
    {
        $bSuccess = false;
        $site = null;
        $building = null;
        $room = null;

        if ($roomId) {
            // Lookup site and building id's for selected room.
            if (($room = Room::find($roomId)) && ($building = Building::find($buildingId))) {
                $buildingId = $room->building_id;
                $siteId = $building->site_id;
                $bSuccess = true;
            }
        } elseif ($buildingId) {
            // Lookup site id for selected building.
            if ($building = Building::find($buildingId)) {
                $siteId = $building->site_id;
                $bSuccess = true;
            }
        } else {
            // Check site id is valid.
            if ($site = Site::find($siteId)) {
                $bSuccess = true;
            }
        }

        return $bSuccess;
    }

    public static function getTimePeriod($timePeriodId)
    {
        $timePeriod = InspSFG20TimePeriod::find($timePeriodId);

        if (!is_null($timePeriod)) {
            return $timePeriod->time_period_code;
        }

        return '';
    }

    public static function getStartAndEndDateByWeek($week = null, $year = null, $sqlFormat = false, $separate = '-')
    {
        $week = !is_null($week) ? $week : (int) date('W');
        $year = !is_null($year) ? $year : (int) date('Y');
        $formatDate = $sqlFormat ? "Y{$separate}m{$separate}d" : "d{$separate}m{$separate}Y";
        $dto = new \DateTime();

        $dto->setISODate($year, $week);
        $ret['firstDay'] = $dto->format($formatDate);
        $dto->modify('+6 days');
        $ret['lastDay'] = $dto->format($formatDate);

        return $ret;
    }

    public static function getStartAndEndDateByMonth($month = null, $year = null, $sqlFormat = false, $separate = '-')
    {
        $month = !is_null($month) ? $month : (int) date('m');
        $year = !is_null($year) ? $year : (int) date('Y');
        $formatDate = $sqlFormat ? "Y{$separate}m{$separate}d" : "d{$separate}m{$separate}Y";
        $dto = new \DateTime();

        $dto->setDate($year, $month, '01');
        $ret['firstDay'] = $dto->format($formatDate);
        $dto->modify('+1 month -1 day');
        $ret['lastDay'] = $dto->format($formatDate);
        return $ret;
    }

    public static function getLastXWeeks(bool $isCompleted = false, int $week = 4): array
    {
        $range = $isCompleted ? $week + 1 : $week;
        $dates = self::getDateRange($range, 'week', '-');

        if ($isCompleted) {
            array_shift($dates);
        }

        return $dates;
    }

    public static function getLast30days()
    {
        return self::getDateRange(1, '-30 day', '-')[0];
    }

    public static function getLast1Month()
    {
        return self::getDateRange(1, '-1 month', '-')[0];
    }

    /**
     * Check if a date is in the given date range (end date - start date).
     * @param String $startDate
     * @param String $endDate
     * @param String $date
     * @return boolean
     */
    public static function isDateInRange($startDate = null, $endDate = null, $date = null)
    {
        $date = (empty($date) ? strtotime('now') : strtotime($date));    // if $date is null then $date = $now
        $startDate = (empty($startDate) ? null : strtotime($startDate));
        $endDate = (empty($endDate) ? null : strtotime($endDate));

        if (is_null($startDate) && is_null($endDate)) {
            // infinite -> infinite => definitely covers any date
            return true;
        } elseif (is_null($startDate)) {
            // infinite -> $endDate
            return $endDate >= $date;
        } elseif (is_null($endDate)) {
            // $startDate -> infinite
            return $startDate <= $date;
        } else {
            // $date in range [$startDate - $endDate] if: ($date >= $startDate) && ($date <= $endDate)
            return ($date >= $startDate) && ($date <= $endDate);
        }
    }

    public static function getLast12Months($reverse = false, bool $isCompleted = false)
    {
        $last12Months = 12;
        $range = $isCompleted ? $last12Months + 1 : $last12Months;
        $dates = self::getDateRange($range, 'month', '-', $reverse);

        if ($isCompleted) {
            if (!$reverse) {
                array_shift($dates);
            } else {
                array_pop($dates);
            }
        }

        return $dates;
    }

    public static function getLast6Months($reverse = false)
    {
        return self::getDateRange(6, 'month', '-', $reverse);
    }

    public static function getLast6MonthCompleted($reverse = false)
    {
        $dates = self::getDateRange(7, 'month', '-', $reverse);
        if (!$reverse) {
            array_shift($dates);
        } else {
            array_pop($dates);
        }
        return $dates;
    }

    public static function getDateRange($range, $type, $operate = '+', $reverse = false)
    {
        $dates = [];
        if (!$reverse) {
            for ($i = 0; $i < $range; $i++) {
                $dateArr = explode("-", date('d-z-W-m-Y', strtotime("{$operate}{$i} {$type}")));
                $dates[] = [
                    'day' => $dateArr[0],
                    'dayOfYear' => $dateArr[1],
                    'week' => $dateArr[2],
                    'month' => $dateArr[3],
                    'year' => $dateArr[4]
                ];
            }
        } else {
            for ($i = $range - 1; $i >= 0; $i--) {
                $dateArr = explode("-", date('d-z-W-m-Y', strtotime("{$operate}{$i} {$type}")));
                $dates[] = [
                    'day' => $dateArr[0],
                    'dayOfYear' => $dateArr[1],
                    'week' => $dateArr[2],
                    'month' => $dateArr[3],
                    'year' => $dateArr[4]
                ];
            }
        }

        return $dates;
    }

    public static function getWeekByDate($date)
    {
        return (new DateTime($date))->format("W");
    }

    public static function getFinYearPeriods($finYearId, $currentId = null)
    {
        $finYearPeriods = FinYearPeriod::where('fin_year_id', $finYearId);

        if ($currentId) {
            $finYearPeriods->orWhere(function ($query) use ($finYearId, $currentId) {
                $query->where('fin_year_id', $finYearId);
                $query->where('fin_year_period_id', $currentId);
            });
        }

        return $finYearPeriods->get()->each(function ($item) {
            return $item->setVisible(
                ['fin_year_period_id', 'fin_year_period_code', 'fin_year_period_desc', 'code_description']
            );
        });
    }

    public static function getFinYearByDate($dateTime)
    {
        if ($dateTime instanceof \DateTime) {
            $dateFormated = $dateTime->format('Y-m-d');
        } else {
            $dateFormated = $dateTime;
        }
        return FinYear::where('year_start_date', '<=', $dateFormated)
            ->where('fin_year.site_group_id', '=', SiteGroup::get()->site_group_id)
            ->where('year_end_date', '>=', $dateFormated)->first();
    }

    public static function getDefaultFinYearPeriodQuery($dateTime = null)
    {
        if ($dateTime instanceof \DateTime) {
            $dateFormated = $dateTime->format('Y-m-d');
        } else {
            $dateFormated = $dateTime;
        }
        return FinYearPeriod::join('fin_year', 'fin_year.fin_year_id', '=', 'fin_year_period.fin_year_id')
            ->where('period_start_date', '<=', $dateFormated)
            ->where('period_end_date', '>=', $dateFormated)
            ->where('fin_year.site_group_id', '=', SiteGroup::get()->site_group_id);
    }

    public static function getDefaultFinYearPeriod($dateTime = null)
    {
        return self::getDefaultFinYearPeriodQuery($dateTime ? : new \DateTime())->first();
    }

    public static function getPHPDateAddType($timePeriodId)
    {
        $type = '';
        switch ($timePeriodId) {
            case EstTimePeriod::ANNUALLY:
                $type = CommonConstant::PHP_YEARS;
                break;

            case EstTimePeriod::MONTHLY:
                $type = CommonConstant::PHP_MONTH;
                break;

            case EstTimePeriod::WEEKLY:
                $type = CommonConstant::PHP_WEEK;
                break;

            case EstTimePeriod::QUARTERLY:
                $type = CommonConstant::PHP_MONTH;
                break;

            default:
                $type = '';
                break;
        }

        return $type;
    }



    public static function getSQLDateAddTypeEstate($timePeriodId)
    {
        $type = '';
        switch ($timePeriodId) {
            case EstTimePeriod::ANNUALLY:
                $type = CommonConstant::SQL_YEAR;
                break;

            case EstTimePeriod::MONTHLY:
                $type = CommonConstant::SQL_MONTH;
                break;

            case EstTimePeriod::WEEKLY:
                $type = CommonConstant::SQL_WEEK;
                break;

            case EstTimePeriod::QUARTERLY:
                $type = CommonConstant::SQL_QUARTER;
                break;

            case EstTimePeriod::QUARTERLY_ENGLISH:
            case EstTimePeriod::QUARTERLY_MODERN:
            case EstTimePeriod::QUARTERLY_SCOTTISH_MODERN:
            case EstTimePeriod::QUARTERLY_SCOTTISH_TRADITIONAL:
            case EstTimePeriod::HALF_YEAR_ENGLISH_MAR_SEP:
            case EstTimePeriod::HALF_YEAR_ENGLISH_JUN_DEC:
            case EstTimePeriod::AD_HOC:
            default:
                $type = '';
                break;
        }

        return $type;
    }

    public static function combindListenedElements($inputs, $elements)
    {
        $listened = [];
        foreach ($inputs as $input) {
            if ((is_object($elements) || is_array($elements)) && count($elements)) {
                foreach ($elements as $element) {
                    $listened[] = $input . (is_object($element) ? $element->getKey() : $element);
                }
            } elseif ($elements) {
                $listened[] = $input . $elements;
            }
        }
        return implode(",", $listened);
    }

    public static function combindListenedOperator($inputs, $elements, $operators, $operatorFirst = false)
    {
        $operatorString = '';
        $operatorValue  = '';
        $endKey         = (is_null($inputs) ? 0 : count($inputs)) * (is_null($elements) ? 0 : count($elements));
        $count          = 0;
        foreach ($inputs as $key => $input) {
            if ((is_object($elements) || is_array($elements)) && count($elements)) {
                $operatorValue =
                    (is_array($operators) ? (isset($operators[$key]) ? $operators[$key] : '') : $operators);
                foreach ($elements as $i => $element) {
                    $operatorValue = !is_array($operators) ? ($count < ($endKey - 1) ? $operators : '') :
                        $operatorValue;
                    $elementValue = (is_object($element) ? $element->getKey() : $element);
                    if ($operatorFirst) {
                        $operatorString .= $operatorValue . $input . $elementValue;
                    } else {
                        $operatorString .= $input . $elementValue . $operatorValue;
                    }
                    if (is_array($operators) && isset($operators[$key])) {
                        unset($operators[$key]);
                    }
                    $count++;
                }
            } elseif ($elements) {
                $operatorValue =
                    (is_array($operators) ? (isset($operators[$key]) ? $operators[$key] : '') : $operators);
                if ($operatorFirst) {
                    $operatorString .= $operatorValue . $input . $elements;
                } else {
                    $operatorString .= $input . $elements . $operatorValue;
                }

                if (is_array($operators) && isset($operators[$key])) {
                    unset($operators[$key]);
                }
            } else {
                $operatorValue =
                    (is_array($operators) ? (isset($operators[$key]) ? $operators[$key] : '') : $operators);
                if ($operatorFirst) {
                    $operatorString .= $operatorValue . $input;
                } else {
                    $operatorString .= $input . $operatorValue;
                }

                if (is_array($operators) && isset($operators[$key])) {
                    unset($operators[$key]);
                }
            }
        }

        return $operatorString . (is_array($operators) && count($operators) ? implode(" ", $operators) : '');
    }

    public static function logPath()
    {
        return storage_path() . '/logs/dataload';
    }

    public static function logPathFile($fileName)
    {
        $path = self::logPath();
        return !is_null($fileName) ? "{$path}/background_log_{$fileName}.log" : '';
    }

    public static function initialiseReportBackgroundLogFile()
    {
        $currentDate = date("Y-m-d");
        // Check path exists.  Create if not.
        $path = self::logReportBackgroundPath();
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
        $file = self::logReportBackgroundPathFile($currentDate);
        return $file;
    }

    public static function logReportBackgroundPath()
    {
        return storage_path() . '/logs/report';
    }

    public static function logReportBackgroundPathFile($fileName)
    {
        $path = self::logReportBackgroundPath();
        return !is_null($fileName) ? "{$path}/background_log_{$fileName}.log" : '';
    }

    public static function presetTags(&$tags, $presetTags = [])
    {
        $tags['tfIncomingEmailSubject'] = $presetTags['tfIncomingEmailSubject'] ?? '';
        $tags['tfIncomingEmailBody']    = $presetTags['tfIncomingEmailBody'] ?? '';
    }

    public static function noteTags(&$tags, $noteInputs = [])
    {
        $dateTime   = new \DateTime();
        $inputs     = \Input::all();

        if (isset($inputs['_method']) && ($inputs['_method'] === 'PUT')) {
            $createdDate = '';
        } else {
            $createdDate = $dateTime->format('d/m/Y H:i');
        }

        // 'note_type_id' is a special case because 0 is valid.
        if (isset($inputs['note_type_id'])) {
            $noteTypeId = Arr::get($inputs, 'note_type_id');
        } elseif (isset($noteInputs['note_type_id'])) {
            $noteTypeId = Arr::get($noteInputs, 'note_type_id');
        } else {
            $noteTypeId = null;
        }
        $noteDetail =
            Arr::get($inputs, 'detail') ?
                Arr::get($inputs, 'detail') : Arr::get($noteInputs, 'detail');
        $notePrivate =
            Arr::get($inputs, 'private') ?
                Arr::get($inputs, 'private') : Arr::get($noteInputs, 'private');
        $duedate =
            Arr::get($inputs, 'due_date') ?
                Arr::get($inputs, 'due_date') : Arr::get($noteInputs, 'due_date');
        $compdate =
            Arr::get($inputs, 'completed_date') ?
                Arr::get($inputs, 'completed_date') : Arr::get($noteInputs, 'completed_date');

        if (isset($inputs['owner_user_id']) && $inputs['owner_user_id'] != '') {
            $reminderOwnerInput = Arr::get($inputs, 'owner_user_id');
            $reminderOwnerEntry = User::where('id', '=', $reminderOwnerInput)->first();

            $reminderOwner = $reminderOwnerEntry ? $reminderOwnerEntry->display_name : null;
        } elseif (isset($noteInputs['owner_user_id']) && $inputs['owner_user_id'] != '') {
            $reminderOwnerInput = Arr::get($noteInputs, 'owner_user_id');
            $reminderOwnerEntry = User::where('id', '=', $reminderOwnerInput)->first();

            $reminderOwner = $reminderOwnerEntry ? $reminderOwnerEntry->display_name : null;
        } else {
            $reminderOwner = null;
        }

        if (isset($inputs['note_subject_id']) && $inputs['note_subject_id'] != 0) {
            $noteSubjectEntry = NoteSubject::where('note_subject_id', '=', $inputs['note_subject_id'])->first();
            $noteSubject = $noteSubjectEntry ? $noteSubjectEntry->note_subject_desc : null;
        } else {
            $noteSubject = null;
        }

        $tags['tfNoteType']         = '';
        $tags['tfNoteAuthor']       = '';
        $tags['tfNoteCreatedDate']  = '';
        $tags['tfNoteUpdatedDate']  = '';
        $tags['tfNoteDetail']       = '';
        $tags['tfNotePrivate']      = '';
        $tags['tfNoteReminderOwner'] = '';
        $tags['tfNoteReminderSubject'] = '';
        $tags['tfNoteDueDate'] = '';
        $tags['tfNoteCompletedDate'] = '';

        // If 'note_type_id' exists then there must be a note.
        if (isset($inputs['note_type_id']) || isset($noteInputs['note_type_id'])) {
            $noteType = $noteTypeId ? NoteType::find($noteTypeId)->code_description : '';
            $tags['tfNoteType']         = $noteType;
            $tags['tfNoteAuthor']       = \Auth::user()->display_name;
            $tags['tfNoteCreatedDate']  = $createdDate;
            $tags['tfNoteUpdatedDate']  = $dateTime->format('d/m/Y H:i');
            $tags['tfNoteDetail']       = $noteDetail;
            $tags['tfNotePrivate']      = $notePrivate;
            $tags['tfNoteDueDate']       = $duedate;
            $tags['tfNoteCompletedDate'] = $compdate;
            $tags['tfNoteReminderOwner'] = $reminderOwner;
            $tags['tfNoteReminderSubject'] = $noteSubject;
        }
    }

    public static function plantTags(BaseModel $model, $tagPrefix = 'tf')
    {
        $plant = $model->getRelationValue('plant');
        if ($plant && $plant instanceof Plant) {
            $plantGroup = $plant->group;
            $plantSubGroup = $plant->subgroup;
            $plantType = $plant->type;
            $tags = [
                $tagPrefix . 'PlantCode' => $plant->plant_code,
                $tagPrefix . 'PlantDesc' => $plant->plant_desc,
                $tagPrefix . 'PlantGroupCode' => $plantGroup ? $plantGroup->plant_group_code : null,
                $tagPrefix . 'PlantGroupDesc' => $plantGroup ? $plantGroup->plant_group_desc : null,
                $tagPrefix . 'PlantSubGroupCode' => $plantSubGroup ? $plantSubGroup->plant_subgroup_code : null,
                $tagPrefix . 'PlantSubGroupDesc' => $plantSubGroup ? $plantSubGroup->plant_subgroup_desc : null,
                $tagPrefix . 'PlantTypeCode' => $plantType ? $plantType->plant_type_code : null,
                $tagPrefix . 'PlantTypeDesc' => $plantType ? $plantType->plant_type_desc : null,
            ];
        } else {
            $tags = [
                $tagPrefix . 'PlantCode' => null,
                $tagPrefix . 'PlantDesc' => null,
                $tagPrefix . 'PlantGroupCode' => null,
                $tagPrefix . 'PlantGroupDesc' => null,
                $tagPrefix . 'PlantSubGroupCode' => null,
                $tagPrefix . 'PlantSubGroupDesc' => null,
                $tagPrefix . 'PlantTypeCode' => null,
                $tagPrefix . 'PlantTypeDesc' => null,
            ];
        }
        return $tags;
    }

    public static function documentTags(&$tags)
    {
        $dateTime   = new \DateTime();
        $inputs     = \Input::all();

        $reviewDate =
            Arr::get($inputs, 'next_review_date') ?
                Arr::get($inputs, 'next_review_date') : null;

        if (isset($inputs['next_review_owner_user_id']) && $inputs['next_review_owner_user_id'] != '') {
            $nextReviewOwnerInput = Arr::get($inputs, 'next_review_owner_user_id');
            $nextReviewOwnerEntry = User::where('id', '=', $nextReviewOwnerInput)->first();
            $nextReviewOwner = $nextReviewOwnerEntry->display_name;
        } else {
            $nextReviewOwner = null;
        }

        $tags['tfDocName']          = '';
        $tags['tfDocDesc']          = '';
        $tags['tfDocDate']          = '';
        $tags['tfDocType']          = '';
        $tags['tfDocURL']           = '';
        $tags['tfDocComments']      = '';
        $tags['tfDocGroup']         = '';
        $tags['tfDocSubgroup']      = '';
        $tags['tfDocAuthor']        = '';
        $tags['tfDocLoadedDate']    = '';
        $tags['tfDocNextReviewDate']    = '';
        $tags['tfDocReviewOwner']    = '';
        $tags['tfDocPrivate']       = '';
        $tags['tfDocReadonly']      = '';

        /*
         * document_form input is defined and merged into Input at trait DocumentsTrait in function store
        */
        if (isset($inputs['document_form'])) {
            $groupId = Arr::get($inputs, 'group', false);

            $documentGroup = $groupId ? DocGroup::find($groupId) : '';
            $documentGroup = $documentGroup ?
                self::concatFields([$documentGroup->doc_group_code, $documentGroup->doc_group_desc]) : '';

            $subgroupId = Arr::get($inputs, 'doc_sub_group_id', false);
            $documentSubgroup = $subgroupId ? DocSubgroup::find($subgroupId) : '';
            $documentSubgroup = $documentSubgroup ?
                self::concatFields([$documentSubgroup->doc_sub_group_code, $documentSubgroup->doc_sub_group_desc]) : '';

            $docName = '';
            if ($inputs['type'] == 'file') {
                if (isset($inputs['file'])) {
                    $docName = $inputs['file']->getClientOriginalName();
                } else {
                    $docName = Arr::get($inputs, 'document_name');
                }
            }

            $tags['tfDocType']          = $inputs['type'] == 'url' ?
                strtoupper($inputs['type']) : ucfirst($inputs['type']);
            $tags['tfDocName']          = $docName;
            $tags['tfDocURL']           = $inputs['type'] == 'url' ? $inputs['url'] : '';
            $tags['tfDocDesc']          = $inputs['description'];
            $tags['tfDocComments']      = $inputs['comments'];
            $tags['tfDocGroup']         = $documentGroup;
            $tags['tfDocSubgroup']      = $documentSubgroup;
            $tags['tfDocAuthor']        = \Auth::user()->display_name;
            $tags['tfDocLoadedDate']    = $dateTime->format('d/m/Y h:i');
            $tags['tfDocNextReviewDate'] = $reviewDate;
            $tags['tfDocReviewOwner']   = $nextReviewOwner;
            $tags['tfDocPrivate']       = $inputs['private'];
            $tags['tfDocReadonly']      = $inputs['read_only'];
            $tags['tfDocDate']          = $inputs['document_date'];
        }
    }

    public static function isImage($inputs)
    {
        if (
            isset($inputs['file']) &&
            $inputs['file'] &&
            $inputs['file']->getMimeType() != "image/jpeg" &&
            $inputs['file']->getMimeType() != "image/jpg" &&
            $inputs['file']->getMimeType() != "image/pjpeg"
        ) {
            return false;
        }
        return true;
    }

    public static function isImageFileName($fileName)
    {
        $firstTypes = in_array(strtolower(substr($fileName, -3)), ['jpg', 'png', 'gif']);
        $secondTypes = in_array(strtolower(substr($fileName, -4)), ['jpeg', 'pjpeg', ]);

        return $firstTypes || $secondTypes;
    }

    public static function checkImageFailed()
    {
        return \Redirect::back()->withInput()->withErrors(
            ['invalidimagetype' => \Lang::get('validation.invalidimagetype')]
        );
    }

    public static function setDefaultContact($type = null, $params = [])
    {
        $contact = [];
        if (\Auth::user()->landlord_contact_id) {
            if (is_null($type)) {
                $contact = [];
                if (!\Input::exists('contact')) {
                    $contact['contact'] = \Auth::user()->landlord_contact_id;
                }
                if (!\Input::exists('landlord')) {
                    $contact['landlord'] = \Auth::user()->landlord_contact_id;
                }
            } else {
                $contact = [$type => \Auth::user()->landlord_contact_id];
            }
        }
        $return = array_merge($contact, $params);
        return count($return) ? $return : null;
    }

    public static function getSqlSelectContactDisplayName(
        $tblName = 'contact',
        $nameCol = 'contact_name',
        $orgCol = 'organisation',
        $displayAlias = 'contact_name'
    ) {
        return \DB::raw(
            "IF($tblName.$nameCol = '-', $tblName.$orgCol, $tblName.$nameCol) as $displayAlias"
        );
    }

    public static function getSqlSelectConcatContactNameOrg()
    {
        return \DB::raw(
            "IF(
                contact.contact_name = '-',
                contact.organisation,
                concat(contact.contact_name, ' - ', contact.organisation)
            ) as concat_contact_name_org"
        );
    }

    /**
     * Gets an empty validator
     * @param  boolean $success
     * @return Validator
     */
    public static function getEmptyValidator($success = true)
    {
        $attributes = [];
        $rules      = [];
        $validator  = \Validator::make($attributes, $rules);
        $validator->success = $success;
        return $validator;
    }

    /*
     * Extracts the first email address from a comma seperated list of email addresses.
     * If string is empty then empty dtring returned.
     */
    public static function extractPrimaryEmail($emailString)
    {
        $emails = explode(',', $emailString);
        if (count($emails)) {
            return $emails[0];
        } else {
            return "";
        }
    }

    /*
     * Search a value in a Eloquent collection with or without a specified key)
     * Return true/false if found/not found
     */
    public static function inCollection(Collection $collection, $value, $key = null)
    {
        $collectionArray = $collection->toArray();
        foreach ($collectionArray as $modelItem) {
            if (is_null($key)) {
                foreach ($modelItem as $fieldName => $fieldValue) {
                    if ($fieldValue == $value) {
                        return true;
                    }
                }
            } else {
                if (array_key_exists($key, $modelItem)) {
                    if ($modelItem[$key] == $value) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static function unsetUnusedModelAttributes(BaseModel $model, array $attributes)
    {
        if (count($attributes)) {
            foreach ($attributes as $attribute) {
                unset($model->$attribute);
            }
        }
    }

    public static function generateNewInputs($srcInputs, array $selectedKeys)
    {
        $destInputs = [];
        foreach ($selectedKeys as $selectedKey) {
            if (array_key_exists($selectedKey, $srcInputs)) {
                $destInputs[$selectedKey] = $srcInputs[$selectedKey];
            }
        }

        return $destInputs;
    }

    public static function queryLogger()
    {
        $queries = \DB::getQueryLog();
        $formattedQueries = [];
        foreach ($queries as $query) :
            $prep = $query['query'];
            foreach ($query['bindings'] as $binding) :
                $prep = preg_replace("#\?#", $binding, $prep, 1);
            endforeach;
            $formattedQueries[] = $prep;
        endforeach;
        return $formattedQueries;
    }

    public static function getSql($queryModel)
    {
        return self::replaceSql($queryModel->toSql(), $queryModel->getBindings());
    }
    public static function replaceSql($sql, $bindings)
    {
        $needle = '?';
        foreach ($bindings as $replace) {
            $pos = strpos($sql, $needle);
            if ($pos !== false) {
                $sql = substr_replace($sql, $replace, $pos, strlen($needle));
            }
        }
        return $sql;
    }

    public static function increaseValueByPercentage($value, $percentage)
    {
        return Math::addCurrency([
            $value,
            Math::mulCurrency([
                $value,
                Math::divCurrency([$percentage, 100])
            ])
        ])
            ;
    }

    public static function calculatePercentageValue($value, $percentage)
    {
        return Math::divCurrency([Math::mulCurrency([$value, $percentage]), 100]);
    }

    public static function getUploadSectionStoragePathway($siteGroupId, $sectionFolder, $recordId)
    {
        return \Config::get('cloud.legacy_paths.uploads')
            . DIRECTORY_SEPARATOR . $siteGroupId
            . DIRECTORY_SEPARATOR . $sectionFolder
            . DIRECTORY_SEPARATOR . $recordId
            . DIRECTORY_SEPARATOR;
    }

    public static function formatCurrencyForCalculating(&$inputs, $key)
    {
        $amount = Arr::get($inputs, $key);
        if (!empty($amount)) {
            $inputs[$key] = str_replace(',', '', str_replace(' ', '', $amount));
        }
    }

    public static function isInteger($input)
    {
        return ctype_digit(strval($input));
    }

    public static function getSystemReportId($systemReportCode)
    {
        $report = SystemReport::where(
            'system_report_code',
            '=',
            $systemReportCode
        )
            ->first();

        return $report->system_report_id;
    }

    public static function userDefinedFields()
    {
        return [
            'User Text 1',
            'User Text 2',
            'User Text 3',
            'User Text 4',
            'User Text 5',
            'User Text 6',

            'User Check 1',
            'User Check 2',
            'User Check 3',
            'User Check 4',

            'User Date 1',
            'User Date 2',
            'User Date 3',
            'User Date 4',

            'User Selection 1',
            'User Selection 2',
            'User Selection 3',
            'User Selection 4',

            'User Contact Name 1',
            'User Contact Org 1',
            'User Contact Name 2',
            'User Contact Org 2',
            'User Contact Name 3',
            'User Contact Org 3',
            'User Contact Name 4',
            'User Contact Org 4',
            'User Contact Name 5',
            'User Contact Org 5',

            'User Memo 1',
            'User Memo 2',
        ];
    }

    public static function reportUserDefinedFields($allowExtraContact = false)
    {
        $fields = [
            UserDefConstant::GEN_USERDEF_GROUP_USERTXT1,
            UserDefConstant::GEN_USERDEF_GROUP_USERTXT2,
            UserDefConstant::GEN_USERDEF_GROUP_USERTXT3,
            UserDefConstant::GEN_USERDEF_GROUP_USERTXT4,
            UserDefConstant::GEN_USERDEF_GROUP_USERTXT5,
            UserDefConstant::GEN_USERDEF_GROUP_USERTXT6,

            UserDefConstant::GEN_USERDEF_GROUP_USERCHK1,
            UserDefConstant::GEN_USERDEF_GROUP_USERCHK2,
            UserDefConstant::GEN_USERDEF_GROUP_USERCHK3,
            UserDefConstant::GEN_USERDEF_GROUP_USERCHK4,

            UserDefConstant::GEN_USERDEF_GROUP_USERDATE1,
            UserDefConstant::GEN_USERDEF_GROUP_USERDATE2,
            UserDefConstant::GEN_USERDEF_GROUP_USERDATE3,
            UserDefConstant::GEN_USERDEF_GROUP_USERDATE4,

            UserDefConstant::GEN_USERDEF_GROUP_USERSEL1_CODE,
            UserDefConstant::GEN_USERDEF_GROUP_USERSEL1_DESC,
            UserDefConstant::GEN_USERDEF_GROUP_USERSEL2_CODE,
            UserDefConstant::GEN_USERDEF_GROUP_USERSEL2_DESC,
            UserDefConstant::GEN_USERDEF_GROUP_USERSEL3_CODE,
            UserDefConstant::GEN_USERDEF_GROUP_USERSEL3_DESC,
            UserDefConstant::GEN_USERDEF_GROUP_USERSEL4_CODE,
            UserDefConstant::GEN_USERDEF_GROUP_USERSEL4_DESC,

            UserDefConstant::GEN_USERDEF_GROUP_USERCONT1,
            UserDefConstant::GEN_USERDEF_GROUP_USERCONTORG1,
            UserDefConstant::GEN_USERDEF_GROUP_USERCONT2,
            UserDefConstant::GEN_USERDEF_GROUP_USERCONTORG2,
            UserDefConstant::GEN_USERDEF_GROUP_USERCONT3,
            UserDefConstant::GEN_USERDEF_GROUP_USERCONTORG3,
            UserDefConstant::GEN_USERDEF_GROUP_USERCONT4,
            UserDefConstant::GEN_USERDEF_GROUP_USERCONTORG4,
            UserDefConstant::GEN_USERDEF_GROUP_USERCONT5,
            UserDefConstant::GEN_USERDEF_GROUP_USERCONTORG5
        ];

        //headings
        if ($allowExtraContact) {
            $fields[] = UserDefConstant::GEN_USERDEF_GROUP_USERCONT6;
            $fields[] = UserDefConstant::GEN_USERDEF_GROUP_USERCONTORG6;
            $fields[] = UserDefConstant::GEN_USERDEF_GROUP_USERCONT7;
            $fields[] = UserDefConstant::GEN_USERDEF_GROUP_USERCONTORG7;
            $fields[] = UserDefConstant::GEN_USERDEF_GROUP_USERCONT8;
            $fields[] = UserDefConstant::GEN_USERDEF_GROUP_USERCONTORG8;
            $fields[] = UserDefConstant::GEN_USERDEF_GROUP_USERCONT9;
            $fields[] = UserDefConstant::GEN_USERDEF_GROUP_USERCONTORG9;
            $fields[] = UserDefConstant::GEN_USERDEF_GROUP_USERCONT10;
            $fields[] = UserDefConstant::GEN_USERDEF_GROUP_USERCONTORG10;
        }

        $fields[] = UserDefConstant::GEN_USERDEF_GROUP_USERMEMO1;
        $fields[] = UserDefConstant::GEN_USERDEF_GROUP_USERMEMO2;

        return $fields;
    }

    public static function isSingleFieldDataload($inputs)
    {
        $isDataload = \Auth::user()->getSourceController() == User::SOURCE_DATA_LOAD;
        if (Arr::get($inputs, 'record_type_code') && $isDataload) {
            return true;
        }
        return false;
    }

    public static function formatNumericString(&$inputs, $key)
    {
        $value = Arr::get($inputs, $key);
        if (!empty($value)) {
            $inputs[$key] = ltrim($value, '0');
        }
    }

    public static function getStatusReport($val, &$whereTexts)
    {
        switch ($val) {
            case CommonConstant::DATABASE_VALUE_NO:
                array_push($whereTexts, "Status = 'Inactive'");
                break;
            case CommonConstant::DATABASE_VALUE_YES:
                array_push($whereTexts, "Status = 'Active'");
                break;
            default:
                array_push($whereTexts, "Status = 'All'");
                break;
        }
    }

    public static function getKpiListPeriodMonthFromToday($numberOfMonth = 12, $backward = true, $isCompleted = false)
    {
        if ($backward) {
            $startDate = $isCompleted
                ? Carbon::now()->subMonth()->firstOfMonth()->subYear()
                : Carbon::now()->firstOfMonth()->subYear();
        } else {
            $startDate = $isCompleted
                ? Carbon::now()->subMonth()->firstOfMonth()
                : Carbon::now()->firstOfMonth();
        }
        $period = [];

        for ($i = 0; $i < $numberOfMonth; $i++) {
            $endDate = $startDate->copy()->addMonth()->endOfMonth()->toDateString();
            $period [] = [
                'startDate' => $startDate->addMonth()->toDateString(),
                'endDate' => $endDate,
                'label' => $startDate->format('M'),
            ];
        }

        return $period;
    }

    public static function ignoreEmailTriggerBySource()
    {
        return \Auth::user()->getSourceController() == User::SOURCE_BULK_PROCESS ||
            (\Auth::user()->getSourceController() == User::SOURCE_DATA_LOAD &&
                \Auth::user()->getFireEmailTrigger() === 'N');
    }

    /**
     * Creates and returns a validator object from the supplied MessageBag instance.
     */
    public static function createValidatorFromMessagebag(\Illuminate\Support\MessageBag $messageBag)
    {
        $attributes = [];
        $rules = [];
        $validator = \Validator::make($attributes, $rules);
        $validator->success = true;

        if (count($messageBag)) {
            foreach ($messageBag->all() as $field => $error) {
                $validator->errors()->add($field, $error);
            }
            $validator->success = false;
        }

        return $validator;
    }

    /**
     * Creates and returns a validator object from the supplied array.
     * [[field1, error], [field2, error], ...]
     */
    public static function createValidatorFromArray(array $values)
    {
        $attributes = [];
        $rules = [];
        $validator = \Validator::make($attributes, $rules);
        $validator->success = true;

        if (count($values)) {
            foreach ($values as $field => $error) {
                $validator->errors()->add($field, $error);
            }
            $validator->success = false;
        }

        return $validator;
    }

    public static function createValidatorFromException(\Exception $ex)
    {
        $attributes = [];
        $rules = [];
        $validator = \Validator::make($attributes, $rules);
        $validator->success = false;
        $validator->errors()->add(null, $ex->getMessage());

        return $validator;
    }

    public static function logValidator($validator, array $options = [])
    {
        $prefix = Arr::get($options, 'message-prefix', '');
        foreach ($validator->messages()->all() as $message) {
            \Log::error("$prefix - $message");
        }
    }

    // Round Up and Round Down function for excel calculating
    public static function roundUp($value)
    {
        if (!is_numeric($value)) {
            return 0;
        }
        return ($value > 0) ? ceil($value) : floor($value);
    }

    public static function roundDown($value)
    {
        if (!is_numeric($value)) {
            return 0;
        }
        return ($value > 0) ? floor($value) : ceil($value);
    }

    public static function maxFileUploadNumber($customNumber = null)
    {
        $maxOfSystem = ini_get('max_file_uploads');

        if ($customNumber && is_int($customNumber) && $customNumber > 0 && $customNumber < $maxOfSystem) {
            return $customNumber;
        }

        $maxOfConfig = config('max_image_uploads', 10);
        if ($maxOfConfig < $maxOfSystem) {
            return $maxOfConfig;
        }
        return $maxOfSystem;
    }

    public static function convertErrorMessage($errors)
    {
        foreach ($errors as &$messages) {
            foreach ($messages as &$message) {
                $message = html_entity_decode($message);
            }
        }
        return $errors;
    }

    // This function used for Layout Profile only. Do not use for other case.
    public static function getAllVariable(array $data)
    {
        return $data;
    }

    public static function isLeapYear($date)
    {
        return $date ? $date->format('L') : false;
    }

    public static function displayEmailsReceivedLink()
    {
        $canShowLink = false;
        $permissionService = new PermissionService();

        if (\Auth::User()->isContractorOrSelfServiceUser()) {
            return $canShowLink;
        }

        $general = $permissionService->hasModuleAccess(
            Module::MODULE_GENERAL,
            CrudAccessLevel::CRUD_ACCESS_LEVEL_SUPERVISOR
        );

        if ($general) {
            if (
                $permissionService->hasModuleAccess(
                    Module::MODULE_CASE_MANAGEMENT,
                    CrudAccessLevel::CRUD_ACCESS_LEVEL_SUPERVISOR
                )
            ) {
                $canShowLink = true;
            } elseif (
                $permissionService->hasModuleAccess(
                    Module::MODULE_CONTRACT,
                    CrudAccessLevel::CRUD_ACCESS_LEVEL_SUPERVISOR
                )
            ) {
                $canShowLink = true;
            } elseif (
                $permissionService->hasModuleAccess(
                    Module::MODULE_HELP,
                    CrudAccessLevel::CRUD_ACCESS_LEVEL_SUPERVISOR
                )
            ) {
                $canShowLink = true;
            } elseif (
                $permissionService->hasModuleAccess(
                    Module::MODULE_INSPECTION,
                    CrudAccessLevel::CRUD_ACCESS_LEVEL_SUPERVISOR
                )
            ) {
                $canShowLink = true;
            } elseif (
                $permissionService->hasModuleAccess(
                    Module::MODULE_INSTRUCTIONS,
                    CrudAccessLevel::CRUD_ACCESS_LEVEL_SUPERVISOR
                )
            ) {
                $canShowLink = true;
            } elseif (
                $permissionService->hasModuleAccess(
                    Module::MODULE_PROJECT,
                    CrudAccessLevel::CRUD_ACCESS_LEVEL_SUPERVISOR
                )
            ) {
                $canShowLink = true;
            }
        } else {
            $canShowLink = false;
        }

        return $canShowLink;
    }

    public static function isNullOrEmptyString($value)
    {
        return is_null($value) || trim($value) == '';
    }

    public static function getModuleNameById($moduleId)
    {
        $module = Module::find($moduleId);
        return $module->module_description;
    }

    public static function numberOfDays($days, $start, $end)
    {
        $start = strtotime($start);
        $end = strtotime($end);
        $w = [date('w', $start), date('w', $end)];
        $x = floor(($end - $start) / 604800);
        $sum = 0;
        for ($day = 0; $day < 7; ++$day) {
            if ($days & pow(2, $day)) {
                $sum += $x + ($w[0] > $w[1] ? $w[0] <= $day || $day <= $w[1] : $w[0] <= $day && $day <= $w[1]);
            }
        }
        return $sum;
    }

    public static function getWeeklyDayNumbers($startDate, $endDate)
    {
        $weekdays = [1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0, 0 => 0];
        $weekdays[1] += Common::numberOfDays(0x02, $startDate, $endDate); // MONDAY
        $weekdays[2] += Common::numberOfDays(0x04, $startDate, $endDate); // TUESDAY
        $weekdays[3] += Common::numberOfDays(0x08, $startDate, $endDate); // WEDNESDAY
        $weekdays[4] += Common::numberOfDays(0x10, $startDate, $endDate); // THURSDAY
        $weekdays[5] += Common::numberOfDays(0x20, $startDate, $endDate); // FRIDAY
        $weekdays[6] += Common::numberOfDays(0x40, $startDate, $endDate); // SATURDAY
        $weekdays[0] += Common::numberOfDays(0x01, $startDate, $endDate); // SUNDAY
        return $weekdays;
    }

    function array_get($array, $key, $default = null)
    {
        return Arr::get($array, $key, $default);
    }

    public static function canDelete($model, string $columnName, int $id): bool
    {
        $result = $model::where($columnName, $id)->exists();

        if ($result) {
            return true;
        }
        return false;
    }
}
