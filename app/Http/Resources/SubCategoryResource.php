<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SubCategoryResource extends JsonResource
{
    public function toArray(Request $request)
    {
        return [
            'sub_category_id' => $this->sub_category_id,
            'sub_category_code' => $this->sub_category_code,
            'sub_category_name' => $this->sub_category_name,
            'sub_category_slug' => $this->sub_category_slug,
            'sub_category_active' => $this->sub_category_active,
            'company_id' => $this->company_id,
            'category_id' => $this->category_id
        ];
    }
}
