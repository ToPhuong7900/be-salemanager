<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class WarehouseResource extends JsonResource
{
    public function toArray(Request $request)
    {
        return [
            'warehouse_id' => $this->warehouse_id,
            'warehouse_code' => $this->warehouse_code,
            'warehouse_name' => $this->warehouse_name,
            'warehouse_phone' => $this->warehouse_phone,
            'warehouse_address' => $this->warehouse_address,
            'warehouse_country' => $this->warehouse_country,
            'warehouse_city' => $this->warehouse_city,
            'warehouse_province' => $this->warehouse_province,
            'warehouse_manager_id' => $this->warehouse_manager_id,
            'company_id' => $this->company_id,
        ];
    }
}
