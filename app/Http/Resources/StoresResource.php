<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class StoresResource extends JsonResource
{
    public function toArray(Request $request)
    {
        return [
            'store_id' => $this->store_id,
            'store_code' => $this->store_code,
            'store_phone' => $this->store_phone,
            'store_address' => $this->store_address,
            'store_country' => $this->store_country,
            'store_manager_id' => $this->store_manager_id,
            'store_province' => $this->store_province,
            'store_city' => $this->store_city,
            'company_id' => $this->company_id,
        ];
    }
}
