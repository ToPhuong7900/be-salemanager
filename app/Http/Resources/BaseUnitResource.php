<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class BaseUnitResource extends JsonResource
{
    public function toArray(Request $request)
    {
        return [
            'base_unit_id' => $this->base_unit_id,
            'base_unit_code' => $this->base_unit_code,
            'base_unit_desc' => $this->base_unit_desc,
            'base_unit_active' => $this->base_unit_active,
        ];
    }
}
