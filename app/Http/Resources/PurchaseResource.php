<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class  PurchaseResource extends JsonResource
{
    public function toArray(Request $request)
    {
        return [
            'purchase_id' => $this->purchase_id,
            'purchase_date' => $this->purchase_date,
            'purchase_shipping_cost' => $this->purchase_shipping_cost,
            'warehouse_id' => $this->warehouse_id,
            'tax_rate' => $this->tax_rate,
            'tax_amount' => $this->tax_amount,
            'discount_value' => $this->discount_value,
            'shipping' => $this->shipping,
            'grand_total' => $this->grand_total,
            'status' => $this->status,
            'purchase_note' => $this->purchase_note,
            'reference_code' => $this->reference_code,
        ];
    }
}
