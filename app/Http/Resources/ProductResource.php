<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        return [
            'product_id' => $this->product_id,
            'product_code' => $this->product_code,
            'product_sku' => $this->product_sku,
            'product_name' => $this->product_name,
            'product_desc' => $this->product_desc,
            'category_id' => $this->category_id,
            'sub_category_id' => $this->sub_category_id,
            'brand_id' => $this->brand_id,
            'warehouse_id' => $this->warehouse_id,
            'store_id' => $this->store_id,
            'site_group_id' => $this->site_group_id,
            'product_active' => $this->product_active,
            'created_at' => $this->created_at->format('d/m/Y'),
            'updated_at' => $this->updated_at->format('d/m/Y'),
        ];
    }
}
