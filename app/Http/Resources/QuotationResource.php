<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class QuotationResource extends JsonResource
{
    public function toArray(Request $request)
    {
        return [
            'quotation_id' => $this->quotation_id,
            'date' => $this->date,
            'customer_id' => $this->customer_id,
            'tax_rate' => $this->tax_rate,
            'tax_amount' => $this->tax_amount,
            'discount' => $this->discount,
            'shipping' => $this->shipping,
            'grand_total' => $this->grand_total,
            'received_amount' => $this->received_amount,
            'paid_amount' => $this->paid_amount,
            'status' => $this->status,
            'description' => $this->description,
            'reference_code' => $this->reference_code
        ];
    }
}
