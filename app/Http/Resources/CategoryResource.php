<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    public function toArray(Request $request)
    {
        return [
            'category_id' => $this->category_id,
            'category_code' => $this->category_code,
            'category_name' => $this->category_name,
            'category_slug' => $this->category_slug,
            'category_active' => $this->category_active,
            'company_id' => $this->company_id,
        ];
    }
}
