<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class BrandResource extends JsonResource
{
    public function toArray(Request $request)
    {
        return [
            'brand_id' => $this->brand_id,
            'brand_code' => $this->brand_code,
            'brand_name' => $this->brand_name,
            'brand_desc' => $this->brand_desc,
            'brand_active' => $this->brand_active,
            'company_id' => $this->company_id,
        ];
    }
}
