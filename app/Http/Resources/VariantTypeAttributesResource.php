<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class VariantTypeAttributesResource extends JsonResource
{
    public function toArray(Request $request)
    {
        return [
            'variant_type_attributes_id' => $this->variant_type_attributes_id,
            'variant_type_id' => $this->variant_type_id,
            'variant_type_attributes_name' => $this->variant_type_attributes_name,
            'variant_type_attributes_active' => $this->variant_type_attributes_active,
        ];
    }
}
