<?php

namespace App\Http\Controllers;

use App\Models\QuotationItem;
use Illuminate\Http\Request;

class QuotationDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(QuotationItem $quotationDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(QuotationItem $quotationDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, QuotationItem $quotationDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(QuotationItem $quotationDetail)
    {
        //
    }
}
