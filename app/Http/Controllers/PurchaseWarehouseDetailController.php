<?php

namespace App\Http\Controllers;

use App\Models\PurchaseWarehouseDetail;
use Illuminate\Http\Request;

class PurchaseWarehouseDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(PurchaseWarehouseDetail $purchaseWarehouseDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(PurchaseWarehouseDetail $purchaseWarehouseDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, PurchaseWarehouseDetail $purchaseWarehouseDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(PurchaseWarehouseDetail $purchaseWarehouseDetail)
    {
        //
    }
}
