<?php

namespace App\Http\Controllers\brand;

use App\Http\Controllers\BaseController;
use App\Services\Brand\BrandServices;
use App\Http\Resources\BrandResource;
use App\Models\Brand;
use Illuminate\Http\Request;

class BrandController extends BaseController
{
    private BrandServices $brandService;
    public function __construct(BrandServices $brandService)
    {
        parent::__construct();
        $this->brandService = $brandService;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $brand = Brand::all();
        return $this->sendResponse(BrandResource::collection($brand), 'Brand retrieved');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = $this->brandService->storeBrand($input);

        if($validator['success']){
            return $this->sendResponse(new BrandResource($validator['data']), 'Brand created successfully.');
        }
        return $this->sendError($validator['error']);
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $brand = $this->brandService->getRecord($id);
        if (is_null($brand))
        {
            return $this->sendError('Brand not found!');
        }
        return $this->sendResponse(new BrandResource($brand), 'Brand is retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $validator = $this->brandService->updateBrand($input, $id);

        if($validator['success']){
            return $this->sendResponse(new BrandResource($validator['data']), 'Brand updated successfully.');
        }
        return $this->sendError($validator['error']);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Brand $brand)
    {
        $result = $this->brandService->deleteBrand($brand);
        if (!$result) {
            return $this->sendError('Can not delete this brand');
        }
        return $this->sendSuccess('Brand deleted successfully');
    }
}
