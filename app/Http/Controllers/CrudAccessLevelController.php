<?php

namespace App\Http\Controllers;

use App\Models\CrudAccessLevel;
use Illuminate\Http\Request;

class CrudAccessLevelController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(CrudAccessLevel $crudAccessLevel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(CrudAccessLevel $crudAccessLevel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, CrudAccessLevel $crudAccessLevel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(CrudAccessLevel $crudAccessLevel)
    {
        //
    }
}
