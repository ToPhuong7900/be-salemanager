<?php

namespace App\Http\Controllers;

use App\Models\CurrencySymbol;
use Illuminate\Http\Request;

class CurrencySymbolController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(CurrencySymbol $currencySymbol)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(CurrencySymbol $currencySymbol)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, CurrencySymbol $currencySymbol)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(CurrencySymbol $currencySymbol)
    {
        //
    }
}
