<?php

namespace App\Http\Controllers\Variant;

use App\Http\Controllers\BaseController;
use App\Http\Resources\VariantTypeAttributesResource;
use App\Models\VariantTypeAttributes;
use App\Services\Variant\VariantTypeService;
use Illuminate\Http\Request;

class VariantTypeAPIController extends BaseController
{
    private VariantTypeService $variantTypeService;

    public function __construct(VariantTypeService $variantTypeService)
    {
        parent::__construct();
        $this->variantTypeService = $variantTypeService;
    }

    public function index()
    {
        $brand = VariantTypeAttributes::all();
        return $this->sendResponse(VariantTypeAttributesResource::collection($brand), 'Variant Type retrieved successfully');
    }

    public function store(Request $request)
    {
        $input = $request->all();

        $validator = $this->variantTypeService->storeVariantTypeAttributes($input);

        if($validator['success']){
            return $this->sendResponse(new VariantTypeAttributesResource($validator['data']), 'Variant Type created successfully.');
        }
        return $this->sendError($validator['error']);
    }
}
