<?php

namespace App\Http\Controllers;

use App\Models\AuditTable;
use Illuminate\Http\Request;

class AuditTableController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(AuditTable $auditTable)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(AuditTable $auditTable)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, AuditTable $auditTable)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(AuditTable $auditTable)
    {
        //
    }
}
