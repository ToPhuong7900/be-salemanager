<?php

namespace App\Http\Controllers\Quotation;

use App\Http\Controllers\BaseController;
use App\Http\Requests\CreateQuotationRequest;
use App\Http\Requests\UpdateQuotationRequest;
use App\Http\Resources\QuotationCollection;
use App\Http\Resources\QuotationResource;
use App\Models\Customer;
use App\Models\Quotation;
use App\Models\Warehouse;
use App\Repositories\Quotation\QuotationRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class QuotationAPIController extends BaseController
{
    /** @var quotationRepository */
    private $quotationRepository;

    public function __construct(QuotationRepository $quotationRepository)
    {
        parent::__construct();
        $this->quotationRepository = $quotationRepository;
    }

    public function index(Request $request)
    {
        $quotation = Quotation::all();
        return $this->sendResponse(new QuotationResource($quotation), "Quotation is retrieved successfully");
    }

    public function store(CreateQuotationRequest $request): QuotationResource
    {
        $input = $request->all();
        $quotation = $this->quotationRepository->storeQuotation($input);

        return new QuotationResource($quotation);
    }

    public function show($id): QuotationResource
    {
        $quotation = $this->quotationRepository->find($id);

        return new QuotationResource($quotation);
    }

    public function update(Request $request, $id): QuotationResource
    {
        $input = $request->all();
        $quotation = $this->quotationRepository->updateQuotation($input, $id);

        return new QuotationResource($quotation);
    }

    public function destroy(Quotation $quotation): JsonResponse
    {
        $this->quotationRepository->delete($quotation->id);

        return $this->sendSuccess('Quotation Deleted successfully');
    }
}
