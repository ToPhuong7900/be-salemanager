<?php

namespace App\Http\Controllers;

use App\Models\SellingType;
use Illuminate\Http\Request;

class SellingTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(SellingType $sellingType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(SellingType $sellingType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, SellingType $sellingType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(SellingType $sellingType)
    {
        //
    }
}
