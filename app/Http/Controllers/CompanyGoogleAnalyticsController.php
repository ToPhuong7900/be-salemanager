<?php

namespace App\Http\Controllers;

use App\Models\SiteGroupGoogleAnalytics;
use Illuminate\Http\Request;

class CompanyGoogleAnalyticsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(SiteGroupGoogleAnalytics $companyGoogleAnalytics)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(SiteGroupGoogleAnalytics $companyGoogleAnalytics)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, SiteGroupGoogleAnalytics $companyGoogleAnalytics)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(SiteGroupGoogleAnalytics $companyGoogleAnalytics)
    {
        //
    }
}
