<?php

namespace App\Http\Controllers\Stores;

use App\Http\Controllers\BaseController;
use App\Http\Resources\StoresResource;
use App\Models\store;
use App\Services\Stores\StoresService;
use Illuminate\Http\Request;

class StoreController extends BaseController
{

    private StoresService $storesService;

    public function __construct(StoresService $storesService)
    {
        parent::__construct();
        $this->storesService = $storesService;
    }

    public function index()
    {
        $stores = Store::all();
        return $this->sendResponse(StoresResource::collection($stores), "Stores retrieved successfully");
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $validator = $this->storesService->storeStores($input);

        if($validator['success']){
            return $this->sendResponse(new StoresResource($validator['data']), 'Store created successfully.');
        }
        return $this->sendError($validator['error']);
    }

    public function show($id)
    {
        $store = $this->storesService->getRecord($id);
        if (is_null($store))
        {
            return $this->sendError('Store not found!');
        }
        return $this->sendResponse(new StoresResource($store), 'Store is retrieved successfully.');
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();

        $validator = $this->storesService->updateStores($input, $id);

        if($validator['success']){
            return $this->sendResponse(new StoresResource($validator['data']), 'Store updated successfully.');

        }
        return $this->sendError($validator['error']);
    }

    public function destroy($id)
    {
        $result = $this->storesService->deleteStore($id);
        if (!$result) {
            return $this->sendError('Can not delete this store');
        }
        return $this->sendSuccess('Store deleted successfully');
    }
}
