<?php

namespace App\Http\Controllers;

use App\Models\CompanyModule;
use Illuminate\Http\Request;

class CompanyModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(CompanyModule $companyModule)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(CompanyModule $companyModule)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, CompanyModule $companyModule)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(CompanyModule $companyModule)
    {
        //
    }
}
