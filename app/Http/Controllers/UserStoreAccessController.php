<?php

namespace App\Http\Controllers;

use App\Models\UserStoreAccess;
use Illuminate\Http\Request;

class UserStoreAccessController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(UserStoreAccess $userStoreAccess)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(UserStoreAccess $userStoreAccess)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, UserStoreAccess $userStoreAccess)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(UserStoreAccess $userStoreAccess)
    {
        //
    }
}
