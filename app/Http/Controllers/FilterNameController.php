<?php

namespace App\Http\Controllers;

use App\Models\FilterName;
use Illuminate\Http\Request;

class FilterNameController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(FilterName $filterName)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(FilterName $filterName)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, FilterName $filterName)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(FilterName $filterName)
    {
        //
    }
}
