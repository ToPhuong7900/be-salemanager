<?php

namespace App\Http\Controllers;

use App\Models\SiteGroupModule;
use Illuminate\Http\Request;

class SiteGroupModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(SiteGroupModule $siteGroupModule)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(SiteGroupModule $siteGroupModule)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, SiteGroupModule $siteGroupModule)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(SiteGroupModule $siteGroupModule)
    {
        //
    }
}
