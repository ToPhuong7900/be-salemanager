<?php

namespace App\Http\Controllers\Products;

use App\Exceptions\PermissionException;
use App\Http\Controllers\BaseController;
use App\Libs\Permissions\ProductPermissionContract;

class ProductBaseController extends BaseController
{
    private ProductPermissionContract $permissionService;

    public function __construct(ProductPermissionContract $permissionService)
    {
        parent::__construct();
        $this->middleware(function ($request, $next) use ($permissionService) {
            $permissionService->setUser($request->user());
            if (!$permissionService->canAccessModule()) {
                $msg = "Not met minimum read access for product module.";
                throw new PermissionException($msg);
            }
            return $next($request);
        });
        $this->setPermissionService($permissionService);
    }
    protected function setPermissionService(ProductPermissionContract $permissionService): void
    {
        $this->permissionService = $permissionService;
    }
}
