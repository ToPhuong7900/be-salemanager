<?php

namespace App\Http\Controllers\Products;

use App\Http\Controllers\BaseController;
use App\Http\Resources\BrandResource;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use App\Services\Products\ProductService;
use Illuminate\Http\Request;

class  ProductAPIController extends BaseController
{
    private ProductService $productService;

    public function __construct(ProductService $productService)
    {
        parent::__construct();
        $this->productService = $productService;
    }

    public function index()
    {
        $brand = Product::all();
        return $this->sendResponse(ProductResource::collection($brand), 'Product retrieved');
    }


    public function show($id)
    {
        $brand = $this->productService->getRecord($id);
        if (is_null($brand))
        {
            return $this->sendError('Product not found!');
        }
        return $this->sendResponse(new ProductResource($brand), 'Product is retrieved successfully.');
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $validator = $this->productService->storeProduct($input);

        if($validator['success']){
            return $this->sendResponse(new ProductResource($validator['data']), 'Product created successfully.');
        }
        return $this->sendError($validator['error']);
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();

        $validator = $this->productService->updateProduct($input, $id);

        if($validator['success']){
            return $this->sendResponse(new ProductResource($validator['data']), 'Product updated successfully.');
        }
        return $this->sendError($validator['error']);
    }

    public function destroy(Product $brand)
    {
        $result = $this->productService->delete($brand);
        if (!$result) {
            return $this->sendError('Can not delete this product');
        }
        return $this->sendSuccess('Product deleted successfully');
    }

}
