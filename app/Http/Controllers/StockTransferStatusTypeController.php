<?php

namespace App\Http\Controllers;

use App\Models\StockTransferStatusType;
use Illuminate\Http\Request;

class StockTransferStatusTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(StockTransferStatusType $stockTransferStatusType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(StockTransferStatusType $stockTransferStatusType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, StockTransferStatusType $stockTransferStatusType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(StockTransferStatusType $stockTransferStatusType)
    {
        //
    }
}
