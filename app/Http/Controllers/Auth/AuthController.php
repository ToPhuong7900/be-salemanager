<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\BaseController;
use App\Http\Request\RegisterRequest;
use App\Models\User;
use App\Rules\Auth\LoginValidator;
use App\Services\Auth\AuthService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends BaseController
{
    private AuthService $authService;

    public function __construct(AuthService $authService)
    {
        parent::__construct();
        $this->authService = $authService;
    }

    public function login(Request $request)
    {
        $inputs = $request->only(['email', 'password']);
        $result = $this->authService->login($inputs);
        if (($result instanceof LoginValidator && !$result->isSuccess()) || !$result) {
            return $this->unauthorizedResponse(__('auth.login_fail'));
        }
        return $this->successResponse([
            'token' => $result->getValidator()->getValue('token')
        ],
            __('auth.login_success')
        );
    }

    public function getInfo(Request $request)
    {
        return $this->successResponse($this->authService->getInfo($request->user()));
    }

    public function register(RegisterRequest $request): JsonResponse
    {
        $input = $request->all();

        $input['password'] = Hash::make($input['password']);
        $user = User::create($input);
        $success['token'] = $user->createToken('token')->plainTextToken;
        $success['name'] = $user->name;
        return $this->sendResponse($success, 'User register successfully');
    }
}
