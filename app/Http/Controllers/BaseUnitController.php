<?php

namespace App\Http\Controllers;

use App\Http\Resources\BaseUnitResource;
use App\Http\Resources\CategoryResource;
use App\Models\BaseUnit;
use App\Models\Category;
use App\Services\Unit\BaseUnitService;
use Illuminate\Http\Request;

class BaseUnitController extends BaseController
{
    private BaseUnitService $baseUnitService;

    public function __construct(BaseUnitService $baseUnitService)
    {
        parent::__construct();
        $this->baseUnitService = $baseUnitService;
    }

    public function index()
    {
        $baseUnit = BaseUnit::all();
        return $this->sendResponse(BaseUnitResource::collection($baseUnit), 'Base Unit retrieved successfully');
    }

    public function show($id)
    {
        $baseUnit = $this->baseUnitService->getRecord($id);
        if (is_null($baseUnit))
        {
            return $this->sendError('Base Unit not found!');
        }
        return $this->sendResponse(new CategoryResource($baseUnit), 'Base Unit is retrieved successfully.');
    }

    public function store(Request $request)
    {
        $input = $request->all();

        $validator = $this->baseUnitService->storeBaseUnit($input);

        if($validator['success']){
            return $this->sendResponse(new BaseUnitResource($validator['data']), "Base Unit created successfully");
        }
        return $this->sendError($validator['error']);
    }

    public function edit(BaseUnit $baseUnit)
    {
        //
    }

    public function update(Request $request,$id)
    {
        $input = $request->all();

        $validator = $this->baseUnitService->getBaseUnitValidator($input);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $baseUnit = $this->baseUnitService->updateBaseUnit($input, $id);
        return $this->sendResponse(new BaseUnitResource($baseUnit), "Base Unit updated successfully");
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(BaseUnit $baseUnit)
    {
        $result = $this->baseUnitService->deleteBrand($baseUnit);
        if (!$result) {
            return $this->sendError('Can not delete this brand');
        }
        return $this->sendSuccess('Base Unit deleted successfully');
    }
}
