<?php

namespace App\Http\Controllers\Category;

use App\Http\Controllers\BaseController;
use App\Http\Resources\CategoryResource;
use App\Models\Category;
use App\Services\Category\CategoryService;
use Illuminate\Http\Request;

class CategoryController extends BaseController
{
    private CategoryService $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        parent::__construct();
        $this->categoryService = $categoryService;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $category = Category::all();
        return $this->sendResponse(CategoryResource::collection($category), 'Category retrieved successfully');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = $this->categoryService->storeCategory($input);

        if($validator['success']){
            return $this->sendResponse(new CategoryResource($validator['data']), 'Category created successfully.');
        }
        return $this->sendError($validator['error']);
    }

    public function show($id)
    {
        $category = $this->categoryService->getRecord($id);
        if (is_null($category))
        {
            return $this->sendError('Category not found!');
        }
        return $this->sendResponse(new CategoryResource($category), 'Category is retrieved successfully.');
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();

        $validator = $this->categoryService->updateCategory($input, $id);

        if($validator['success']){
            return $this->sendResponse(new CategoryResource($validator['data']), 'Category updated successfully.');

        }
        return $this->sendError($validator['error']);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $result = $this->categoryService->deleteCategory($id);
        if (!$result) {
            return $this->sendError('Can not delete this category');
        }
        return $this->sendSuccess('Category deleted successfully');
    }
}
