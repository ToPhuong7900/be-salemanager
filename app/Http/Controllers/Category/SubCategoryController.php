<?php

namespace App\Http\Controllers\Category;

use App\Http\Controllers\BaseController;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\SubCategoryResource;
use App\Models\SubCategory;
use App\Services\Category\SubCategoryService;
use Illuminate\Http\Request;

class SubCategoryController extends BaseController
{
    private SubCategoryService $subCategoryService;

    public function __construct(SubCategoryService $subCategoryService)
    {
        parent::__construct();
        $this->subCategoryService = $subCategoryService;
    }

    public function index()
    {
        $subCategory = SubCategory::all();
        return $this->sendResponse(SubCategoryResource::collection($subCategory), 'Sub Category retrieved successfully');
    }

    public function show($id)
    {
        $category = $this->subCategoryService->getRecord($id);
        if (is_null($category))
        {
            return $this->sendError('Sub Category not found!');
        }
        return $this->sendResponse(new SubCategoryResource($category), 'Sub Category is retrieved successfully.');
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $validator = $this->subCategoryService->storeSubCategory($input);

        if($validator['success']){
            return $this->sendResponse(new SubCategoryResource($validator['data']), 'Sub Category created successfully.');
        }
        return $this->sendError($validator['error']);
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();

        $validator = $this->subCategoryService->updateCategory($input, $id);

        if($validator['success']){
            return $this->sendResponse(new SubCategoryResource($validator['data']), 'Sub Category updated successfully.');

        }
        return $this->sendError($validator['error']);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $result = $this->subCategoryService->deleteCategory($id);
        if (!$result) {
            return $this->sendError('Can not delete this sub category');
        }
        return $this->sendSuccess('Sub Category deleted successfully');
    }

}
