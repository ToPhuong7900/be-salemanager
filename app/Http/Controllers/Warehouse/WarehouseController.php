<?php

namespace App\Http\Controllers\Warehouse;

use App\Http\Controllers\BaseController;
use App\Http\Resources\WarehouseResource;
use App\Models\Warehouse;
use App\Services\Warehouse\WarehouseService;
use Illuminate\Http\Request;

class WarehouseController extends BaseController
{

    private WarehouseService $warehouseService;

    public function __construct(WarehouseService $warehouseService)
    {
        parent::__construct();
        $this->warehouseService = $warehouseService;
    }

    public function index()
    {
        $category = Warehouse::all();
        return $this->sendResponse(WarehouseResource::collection($category), 'Warehouse retrieved successfully');
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $validator = $this->warehouseService->storeWarehouse($input);

        if($validator['success']){
            return $this->sendResponse(
                new WarehouseResource($validator['data']),
                'Warehouse created successfully.'
            );
        }
        return $this->sendError($validator['error']);
    }
    public function show($id)
    {
        $category = $this->warehouseService->getRecord($id);
        if (is_null($category))
        {
            return $this->sendError('Warehouse not found!');
        }
        return $this->sendResponse(new WarehouseResource($category), 'Warehouse is retrieved successfully.');
    }
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $validator = $this->warehouseService->updateWarehouse($input, $id);

        if($validator['success']){
            return $this->sendResponse(new WarehouseResource($validator['data']), 'Warehouse updated successfully.');

        }
        return $this->sendError($validator['error']);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $result = $this->warehouseService->deleteWarehouse($id);
        if (!$result) {
            return $this->sendError('Can not delete this Warehouse');
        }
        return $this->sendSuccess('Warehouse deleted successfully');
    }
}
