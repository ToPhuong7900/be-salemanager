<?php

namespace App\Http\Controllers;

use App\Traits\ApiResponse;
use App\Utils\ResponseUtils;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;

class BaseController extends Controller
{
    use ApiResponse;
    public function __construct()
    {
    }

    public function sendResponse($result, $message): JsonResponse
    {
        return Response::json(ResponseUtils::makeResponse($message, $result));
    }

    public function sendError($error): JsonResponse
    {
        return Response::json(ResponseUtils::makeError($error), 422);
    }

    public function sendSuccess($message): JsonResponse
    {
        return Response::json([
            'success' => true,
            'message' => $message,
        ], 200);
    }
}
