<?php

namespace App\Http\Controllers;

use App\Models\AuditAction;
use Illuminate\Http\Request;

class AuditActionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(AuditAction $auditAction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(AuditAction $auditAction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, AuditAction $auditAction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(AuditAction $auditAction)
    {
        //
    }
}
