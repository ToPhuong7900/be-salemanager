<?php

namespace Tfcloud\Controllers\Api\GeneralFilter;

use Illuminate\Http\Request;
use Tfcloud\services\Api\CloudApi\Filter\TableSettingService;

class TableSettingController extends GeneralFilterController
{
    /**
     * @var TableSettingService
     */
    protected $filterService;

    public function process(Request $request)
    {
        try {
            $inputs = $request->input();
            $validator = $this->filterService->processFilterTableHeaderUser($this->filterNameId, $inputs);
            if ($validator->success) {
                return response()->success();
            }
            return response()->fail($validator->messages()->getMessages());
        } catch (\Exception $exception) {
            return response()->exception($exception);
        }
    }

    public function delete()
    {
        try {
            $this->filterService->deleteFilterTableHeader($this->filterNameId);
            return response()->success();
        } catch (\Exception $exception) {
            return response()->exception($exception);
        }
    }

    protected function getFilterService()
    {
        return new TableSettingService();
    }
}
