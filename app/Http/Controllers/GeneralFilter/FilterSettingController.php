<?php

namespace App\Http\Controllers\GeneralFilter;

use Illuminate\Http\Request;
use Tfcloud\Services\Api\CloudApi\Filter\FilterSettingService;

class FilterSettingController extends GeneralFilterController
{
    /**
     * @var FilterSettingService
     */
    protected $filterService;

    public function index()
    {
        $userId = \Auth::user()->getKey();
        return response()->json($this->filterService->getAll($userId, $this->filterNameId));
    }

    public function store(Request $request)
    {
        try {
            $inputs = $request->input();
            $validator = $this->filterService->addFilterUserSetting($this->filterNameId, $inputs);
            if ($validator->success) {
                return response()->success();
            }
            return response()->fail($validator->messages()->getMessages());
        } catch (\Exception $exception) {
            return response()->exception($exception);
        }
    }

    public function update(Request $request)
    {
        try {
            $filterUserSettingId = $request->offsetGet('filterUserSettingId');
            $inputs = $request->input();
            $validator = $this->filterService->updateFilterUserSetting($filterUserSettingId, $inputs);
            if ($validator->success) {
                return response()->success();
            } else {
                return response()->fail($validator->messages()->getMessages());
            }
        } catch (\Exception $exception) {
            return response()->exception($exception);
        }
    }

    public function destroy(Request $request)
    {
        try {
            $filterUserSettingId = $request->offsetGet('filterUserSettingId');
            $this->filterService->deleteFilterUserSetting($filterUserSettingId);
            return response()->success();
        } catch (\Exception $exception) {
            return response()->exception($exception);
        }
    }

    protected function getFilterService()
    {
        return new FilterSettingService();
    }
}
