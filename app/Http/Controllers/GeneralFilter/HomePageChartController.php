<?php

namespace Tfcloud\Controllers\Api\GeneralFilter;

use Tfcloud\Controllers\ApiController;
use Tfcloud\Lib\Exceptions\PermissionException;
use App\SBoxFilter\General\Home\HomeChart\ChartFactory;

class HomePageChartController extends ApiController
{
    public function index()
    {
        try {
            $chartData = (new ChartFactory())->getChartService()->getChartData();

            return response()->json($chartData);
        } catch (PermissionException $exception) {
            return response()->json('Forbidden', 403);
        }
    }
}
