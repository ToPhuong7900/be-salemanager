<?php

namespace App\Http\Controllers\GeneralFilter;

use App\Exceptions\PermissionException;
use App\Http\Controllers\BaseController;
use App\Libs\BoxFilter\BoxFilterList;
use App\Libs\BoxFilterQuery\Constants\FilterQueryConstant;
use App\Libs\BoxFilterQuery\FilterQueryBootstrap;
use App\Libs\Common\Common;
use App\Services\Filter\FilterService;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class GeneralFilterController extends BaseController
{
    protected const TABLE_FOOTER_DATA_KEY = 'tableFooterData';
    protected const REPORT_CODE_KEY = 'repCode';
    protected const REPORT_FILE_EXT_KEY = 'repFileExt';
    protected const TABLE_MESSAGE_DATA_KEY = 'tableMessageData';
    /**
     * @var int $filterNameId filter_name_id
     */
    protected $filterNameId;

    /**
     * @var FilterService
     */
    protected $filterService;

    public function __construct()
    {
        parent::__construct();
        $this->filterNameId = request()->offsetGet('filterNameId');
        $this->filterService = $this->getFilterService();
    }

    public function show(Request $request)
    {
        try {
            $inputs = $request->all();
            $filterQuery = FilterQueryBootstrap::getFilterQuery($this->filterNameId, $inputs);
            if (!$filterQuery->canAccess()) {
                throw new PermissionException('No access to kpi');
            }
            $pagePer = $this->filterService->pagePer(Arr::get($inputs, FilterQueryConstant::PER_PARAMETER_KEY));
            $records = $filterQuery->getAll($pagePer)->toArray();
            $records[self::TABLE_FOOTER_DATA_KEY] = $filterQuery->getTableFooterData();
            $records[self::TABLE_MESSAGE_DATA_KEY] = $filterQuery->getTableMessageData();
            return response()->json($records);
        } catch (\Exception $exception) {
            $class = (isset($filterQuery) && is_object($filterQuery)) ? get_class($filterQuery) : '';
            \Log::error("Show Filter Errors: {$class}\n{$exception->getMessage()}\n{$exception->getTraceAsString()}");
            return response()->exception($exception);
        }
    }

    public function showKPI(Request $request, $filterNameId, $kpiID)
    {
        try {
            $inputs = $request->all();
            $filterQuery = FilterQueryBootstrap::getKpiFilterQuery($this->filterNameId, $kpiID, $inputs);
            if (!$filterQuery->canAccess()) {
                throw new PermissionException('No access to kpi');
            }
            $kpiData = $filterQuery->getKPIData();
            return response()->json($kpiData);
        } catch (\Exception $exception) {
            $class = (isset($filterQuery) && is_object($filterQuery)) ? get_class($filterQuery) : '';
            \Log::error("Show Filter Errors: {$class}\n{$exception->getMessage()}\n{$exception->getTraceAsString()}");
            return response()->exception($exception);
        }
    }

    public function exportListAsReport(Request $request)
    {
        $reportCode = $request->get(BoxFilterList::REPORT_CODE_KEY);
        $inputs = $request->all();

        $inputs['repId'] = Common::getSystemReportId($reportCode);
        $inputs['outputOption'] = $request->get(BoxFilterList::REPORT_EXPORT_AS_KEY);

        $permissionService = new PermissionService();
        $reportService = new ReportService(
            $permissionService,
            new UserGroupsService($permissionService, new UserGroupModuleService($permissionService))
        );

        if (in_array($reportCode, ReportService::getCustomizeCsvWhiteList())) {
            $csvExcelReportService = new CsvExcelReportService($permissionService, $reportService);
            $returnData = $csvExcelReportService->reOutputCsvData($inputs);
        } elseif (in_array($reportCode, ReportService::getExcelWhiteList())) {
            $phpExcelReportService = new PhpExcelReportService($permissionService, $reportService);
            $returnData = $phpExcelReportService->reOutputPhpExcelData($inputs);
        } else {
            $reportGenerateService = new ReportGenerateService($permissionService, $reportService);
            $returnData = $reportGenerateService->reOutputData($inputs);
        }

        $reportCode = Arr::get($returnData, 'repCode');
        $repFileName = Arr::get($returnData, 'repFileName', '');
        $pathParts = pathinfo($repFileName);
        $extension = $pathParts['extension'];

        return response()->json([
            self::REPORT_CODE_KEY => $reportCode,
            self::REPORT_FILE_EXT_KEY => $extension,
        ]);
    }

    public function downloadExportedFile(Request $request)
    {
        $repCode = $request->get(self::REPORT_CODE_KEY);
        $repFileExt = $request->get(self::REPORT_FILE_EXT_KEY);
        $repFileName = $repCode . '.' . $repFileExt;
        $repFile = $this->getReportFilePath($repFileName);
        if ($repCode && $repFileExt && realpath($repFile)) {
            $contentTypeFile = '';
            switch ($repFileExt) {
                case 'csv':
                    $contentTypeFile = 'text/csv';
                    break;
                case 'xlsx':
                    $contentTypeFile = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                    break;
            }
            $headers = [
                'Content-Type' => $contentTypeFile . '; charset=utf-8',
                'Cache-Control' => 'no-cache, must-revalidate',
            ];
            return response()->download($repFile, $repFileName, $headers);
        } else {
            $messages = 'Cannot download report file.';
            return response($messages, 404);
        }
    }

    protected function getFilterService()
    {
        return new FilterService();
    }

    protected function getReportFilePath(string $repFileName)
    {
        $reportDir = storage_path() . DIRECTORY_SEPARATOR . 'reports'
            . DIRECTORY_SEPARATOR . \Auth::User()->site_group_id . DIRECTORY_SEPARATOR . \Auth::User()->id;
        return $reportDir . DIRECTORY_SEPARATOR . $repFileName;
    }
}
