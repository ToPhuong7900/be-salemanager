<?php

namespace App\Http\Controllers;

use App\Models\DateFormat;
use Illuminate\Http\Request;

class DateFormatController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(DateFormat $dateFormat)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(DateFormat $dateFormat)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, DateFormat $dateFormat)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(DateFormat $dateFormat)
    {
        //
    }
}
