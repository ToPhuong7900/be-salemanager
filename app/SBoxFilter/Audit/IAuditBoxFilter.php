<?php

namespace App\SBoxFilter\Audit;

interface IAuditBoxFilter
{
    public function addRecordTypeHeaders();
    public function addModuleHeaders();
    public function addIdHeaders();
    public function addVersionHeaders();
    public function addSourceNameHeaders();
}
