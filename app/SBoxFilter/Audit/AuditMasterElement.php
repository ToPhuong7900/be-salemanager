<?php

namespace App\SBoxFilter\Audit;

use Tfcloud\Lib\BoxFilter\Elements\DateRangeElement;
use Tfcloud\Lib\BoxFilter\Elements\DropDownElement;
use Tfcloud\Lib\BoxFilter\Elements\Pickers\UserPickerElement;
use Tfcloud\Lib\BoxFilter\Elements\RadioDropDownElement;
use Tfcloud\Lib\BoxFilter\Elements\TextElement;

trait AuditMasterElement
{
    protected function elementUserId()
    {
        $userPickerElement = new UserPickerElement('user_id', "User", $dataLookups = []);


        $userPickerElement->setOperatorsForRequiredField();

        $userPickerElement->setDefaultValue(\Auth::user()->getKey());
        return $userPickerElement;
    }

    protected function elementAction(
        $actions,
        $label = 'Action',
        $dataConfig = [],
        $options = []
    ) {
        $element = new DropDownElement('audit_action_id', $label, $actions, $dataConfig, $options);
        $element->setValueFieldName('audit_action_id')->setTextFieldNames([
            'audit_action_code', 'audit_action_desc'
        ])->setOperatorsForRequiredField();
        return $element;
    }

    protected function elementModuleId(
        $modules,
        $label = 'Module',
        $dataConfig = [],
        $options = []
    ) {
        return new DropDownElement(
            'module_id',
            $label,
            $modules,
            array_merge([
                'valueFieldName' => 'module_id',
                'textFieldName' => ['module_description']
            ], $dataConfig),
            $options
        );
    }

    protected function elementTable(
        $tables,
        $label = 'Table',
        $dataConfig = [],
        $options = []
    ) {
        return new DropDownElement(
            'audit_table_id',
            $label,
            $tables,
            array_merge([
                'valueFieldName' => 'audit_table_id',
                'textFieldName' => ['table_display_name']
            ], $dataConfig),
            $options
        );
    }

    protected function elementRecordCode($label = 'Code')
    {
        return new TextElement('record_code', $label);
    }

    protected function elementAuditDescription($label = 'Description')
    {
        return new TextElement('audit_text', $label);
    }

    protected function elementAuditDate($label = 'Audit Date', $options = [])
    {
        return new DateRangeElement('timestamp', $label, $options);
    }

    protected function elementActionEmailOrReportOnly()
    {
        return (new RadioDropDownElement('action_email_or_report_only', ''))
            ->buildYesNoElement();
    }

    protected function elementSourceId()
    {
        return new TextElement('source_id', 'Source Id');
    }
}
