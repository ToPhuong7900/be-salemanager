<?php

namespace App\SBoxFilter\Audit;


class AuditBoxFilter extends BoxFilterList implements IAuditBoxFilter
{
    use AuditMasterElement;

    protected $bAllFilters = false;
    protected $auditTable;
    protected $tableDefaultTitle = 'History List';
    protected $tableDefaultSortField = 'timestamp';
    protected $tableDefaultIconPath = 'assets/images/suitability.png';
    protected $tableDefaultSortOrder = 'DESC';
    protected $enableEditAction = false;
    protected $enableDeleteAction = false;
    protected $showAllNotAppliedFilters = true;

    public function __construct(
        $filterNameId,
        $auditTable = null
    ) {
        $this->auditTable = $auditTable;
        parent::__construct($filterNameId);
    }

    public function setFilterElements($parentId = null)
    {
        $this->addBlindElement($this->getRecordIdBlindElementFilter($parentId));
        $this->addBlindElement($this->getTableNameBlindElementFilter());
        $filterElements = [
            $this->elementUserId(),
            $this->elementAction($this->getFilterLookup('actions')),
            $this->elementAuditDate(),
        ];
        if ($this->bAllFilters) {
            $filterElements = array_merge(
                $filterElements,
                [
                    $this->elementRecordCode(),
                    $this->elementModuleId($this->getFilterLookup('modules')),
                    $this->elementTable($this->getFilterLookup('tables'))
                ]
            );
        }
        return $this->addElements($filterElements);
    }

    public function getTableHeader()
    {
        $headers = [
            new Header('Date', 'timestamp', 'timestamp', null, [], true, 3),
            new Header('User', 'display_name', 'display_name', null, [], true, 4),
            new Header('Action', 'audit_action_code', 'audit_action_code', null, [], true, 6),
            new Header('Description', 'audit_text', 'audit_text', null, [], true, 7)
        ];

        $tableHeader = new TableHeader(
            $this->tableDefaultTitle,
            $this->tableDefaultSortField,
            null,
            $this->tableDefaultSortOrder
        );
        $tableHeader->addHeaders($headers);
        return $tableHeader;
    }

    public function setShowAllFilters($bShowAllFilters)
    {
        $this->bAllFilters = $bShowAllFilters;
        return $this;
    }

    public function addIdHeaders()
    {
        $user = \Auth::user();
        if ($user->isAdministratorOrSuperuser()) {
            $this->baseTable->addHeaders([
                new Header('Id', 'audit_id', 'audit_id', null, [], true, 1),
            ]);
        }
        return $this;
    }

    public function addVersionHeaders()
    {
        $user = \Auth::user();
        if ($user->isSuperuser()) {
            $this->baseTable->addHeaders([
                new Header('Version', 'audit_version', 'audit_v2', null, [], true, 2),
            ]);
        }
        return $this;
    }

    public function addSourceNameHeaders()
    {
        $user = \Auth::user();
        $displayOrder = 2;
        if ($user->isSuperuser()) {
            $displayOrder = 3;
        }
        if ($user->isAdministratorOrSuperuser()) {
            $this->baseTable->addHeaders([
                new Header('Source', 'user_source_code', 'user_source_code', null, [], true, $displayOrder),
             ]);
        }
        return $this;
    }

    public function addModuleHeaders()
    {
        $this->baseTable->addHeaders([
            new Header('Module', 'module', 'module', null, [], true, 2),
        ]);
        return $this;
    }

    public function addRecordTypeHeaders()
    {
        $displayOrder = 1;
        $user = \Auth::user();
        if ($user->isAdministratorOrSuperuser()) {
            $displayOrder = 2;
        } elseif ($user->isSuperuser()) {
            $displayOrder = 3;
        }
        $this->baseTable->addHeaders([
            new Header('Record', 'table_display_name', 'table_display_name', null, [], true, $displayOrder),
            new Header('Code', 'record_code', 'record_code', null, [], true, $displayOrder + 1)
        ]);
        return $this;
    }

    private function getRecordIdBlindElementFilter($parentId)
    {
        return (new TextElement('record_id', 'Parent'))
            ->setDefaultValue($parentId)
            ->setDefaultOperator(OperatorTypeConstant::MATCH);
    }

    private function getTableNameBlindElementFilter()
    {
        return (new TextElement('table_name', 'Audit Table'))
            ->setDefaultValue($this->auditTable)
            ->setDefaultOperator(OperatorTypeConstant::MATCH);
    }
}
