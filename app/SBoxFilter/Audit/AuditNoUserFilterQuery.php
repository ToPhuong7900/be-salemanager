<?php

namespace App\SBoxFilter\Audit;

use App\Libs\BoxFilterQuery\FilterFieldSet\IFilterField;
use App\Models\Audit;
use App\Models\ConfidentialMessageAudit;
use Illuminate\Support\Arr;

class AuditNoUserFilterQuery extends AuditFilterQuery
{
    protected function getQuery()
    {
        $auditTable = new Audit();
        $query = Audit::with(['user', 'auditAction', 'auditTable']);


        $query->select(
            $auditTable->getTable() . '.audit_id',
            $auditTable->getTable() . '.user_id',
            $auditTable->getTable() . '.timestamp',
            $auditTable->getTable() . '.audit_v2',
            $auditTable->getTable() . '.source_id',
            $auditTable->getTable() . '.table_name',
            $auditTable->getTable() . '.audit_action_id',
            $auditTable->getTable() . '.record_id',
            $auditTable->getTable() . '.record_code',
            $auditTable->getTable() . '.parent_table_name',
            $auditTable->getTable() . '.parent_record_id',
            'audit_action.audit_action_code',
            'user.display_name',
            'audit_table.table_display_name',
            $auditTable->getTable() . '.audit_text'
        );

        return $query;
    }

    private function addTableNameQuery(&$auditTable, $tableName, $recordId)
    {
        $auditTable->where(
            function ($query) use ($tableName, $recordId) {
                $query->where(function ($query) use ($tableName, $recordId) {
                    $query->where('table_name', $tableName)
                        ->where('record_id', $recordId);
                });
                $query->orWhere(function ($query) use ($tableName, $recordId) {
                    $query->where('parent_table_name', $tableName)
                        ->where('parent_record_id', $recordId);
                });
            }
        );
    }

    private function addAuditJoins(&$audit, $tableName)
    {
        $audit->leftjoin(
            'audit_action',
            'audit_action.audit_action_id',
            '=',
            $tableName . ".audit_action_id"
        )
            ->leftjoin('audit_table', 'audit_table.table_dbname', '=', $tableName . ".table_name")
            ->join('user', 'user.id', '=', $tableName . '.user_id');
    }

    protected function filterUserId($query, IFilterField $filterField, $view = null)
    {
        $canViewConfidentialMessages = \Auth::user()->canViewConfidentialMessages();
        $auditTable = new Audit();
        $id = Arr::first($filterField->getFilterValues());
        $confAudit = ConfidentialMessageAudit::with(['user', 'auditAction', 'auditTable']);
        if ($canViewConfidentialMessages) {
            $confAuditTable = 'confidential_message_audit';

            $confAudit->select(
                $confAuditTable . '.confidential_message_audit_id',
                $confAuditTable . '.user_id',
                $confAuditTable . '.timestamp',
                $confAuditTable . '.audit_v2',
                $confAuditTable . '.source_id',
                $confAuditTable . '.table_name',
                $confAuditTable . '.audit_action_id',
                $confAuditTable . '.record_id',
                $confAuditTable . '.record_code',
                $confAuditTable . '.parent_table_name',
                $confAuditTable . '.parent_record_id',
                'audit_action.audit_action_code',
                'user.display_name',
                'audit_table.table_display_name',
                $confAuditTable . '.audit_text'
            );
        }

        if ($id) {
            $this->addTableNameQuery($query, 'user', $id);
            if ($canViewConfidentialMessages) {
                $this->addTableNameQuery($confAudit, 'user', $id);
            }
        }

        $this->addAuditJoins($query, $auditTable->getTable());
        if ($canViewConfidentialMessages) {
            $this->addAuditJoins($confAudit, $confAuditTable);
        }

        if ($canViewConfidentialMessages) {
            $inputs = request()->all();
            $configAudit = $this->auditService->filter($confAudit, $inputs, false, false, true);
            return $query->union($configAudit);
        }

        return $query;
    }
}
