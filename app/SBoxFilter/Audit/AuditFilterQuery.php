<?php

namespace App\SBoxFilter\Audit;

use Tfcloud\Lib\BoxFilter\Constants\OperatorTypeConstant;
use Tfcloud\Lib\BoxFilterQuery\FilterFieldSet\IFilterField;
use Tfcloud\Lib\BoxFilterQuery\FilterQuery;
use Tfcloud\Lib\Common;
use Tfcloud\Models\Audit;
use Tfcloud\Services\AuditService;

class AuditFilterQuery extends FilterQuery
{
    protected $auditService;

    public function __construct()
    {
        parent::__construct();
        $this->auditService = new AuditService();
    }

    protected function getQuery()
    {
        return $this->auditService->all();
    }

    protected function getPrefixTables()
    {
        $auditTable = (new Audit())->getTable();
        return [
            $auditTable => '*',
            'user'        => ['id', 'display_name', 'description'],
            'audit_table' => ['audit_table_id', 'table_dbname', 'table_display_name', 'module_id'],
            'user_source' => ['user_source_id', 'user_source_code', 'user_source_desc']
        ];
    }

    protected function getRecordActions()
    {
        return [];
    }

    protected function filtered($query, $view = false)
    {
        $tableNameArr = $this->getValueFilterField('table_name');
        $recordIdArr = $this->getValueFilterField('record_id');
        $tableName = array_get($tableNameArr, 0);
        $recordId = array_get($recordIdArr, 0);

        if (!empty($recordId) && !empty($tableName)) {
            $query->where(
                function ($query) use ($tableName, $recordId) {
                    $query->where(function ($query) use ($tableName, $recordId) {
                        $query->where('table_name', $tableName)
                            ->where('record_id', $recordId);
                    });
                    $query->orWhere(function ($query) use ($tableName, $recordId) {
                        $query->where('parent_table_name', $tableName)
                            ->where('parent_record_id', $recordId);
                    });
                }
            );
        }
        return $query;
    }

    protected function filterTableName($query)
    {
        return $query;
    }

    protected function filterRecordId($query)
    {
        return $query;
    }

    protected function filterTimestamp($query, IFilterField $filterField)
    {
        $auditTable = (new Audit())->getAuditTable();
        foreach ($filterField->getFilterOperators() as $filterOperator) {
            switch ($filterOperator->getOperator()) {
                case OperatorTypeConstant::INCLUDE_RANGE:
                    foreach ($filterOperator->getValues() as $dates) {
                        $fromDate = array_get($dates, 0);
                        $toDate = array_get($dates, 1);
                        if ($fromDate) {
                            $query->where(
                                \DB::raw("DATE_FORMAT({$auditTable}.timestamp, '%Y-%m-%d')"),
                                '>=',
                                $fromDate
                            );
                        }
                        if ($toDate) {
                            $query->where(
                                \DB::raw("DATE_FORMAT({$auditTable}.timestamp, '%Y-%m-%d')"),
                                '<=',
                                $toDate
                            );
                        }
                    }
                    break;
                case OperatorTypeConstant::EXCLUDE_RANGE:
                    foreach ($filterOperator->getValues() as $dates) {
                        $fromDate = array_get($dates, 0);
                        $toDate = array_get($dates, 1);
                        if (!empty($fromDate) && !empty($toDate)) {
                            $query->where(function ($sub) use ($auditTable, $fromDate, $toDate) {
                                $sub->where(
                                    \DB::raw("DATE_FORMAT({$auditTable}.timestamp, '%Y-%m-%d')"),
                                    '<',
                                    $fromDate
                                );
                                $sub->orWhere(
                                    \DB::raw("DATE_FORMAT({$auditTable}.timestamp, '%Y-%m-%d')"),
                                    '>',
                                    $toDate
                                );
                            });
                        } elseif (!empty($fromDate)) {
                            $query->where(
                                \DB::raw("DATE_FORMAT({$auditTable}.timestamp, '%Y-%m-%d')"),
                                '<',
                                $fromDate
                            );
                        } else {
                            $query->where(
                                \DB::raw("DATE_FORMAT({$auditTable}.timestamp, '%Y-%m-%d')"),
                                '>',
                                $toDate
                            );
                        }
                    }
                    break;
            }
        }
    }

    protected function gotAll($records)
    {
        foreach ($records as $record) {
            $record->timestamp = Common::dateFieldDisplayFormat($record->timestamp, ['dateFormat' => 'd/m/Y H:i:s']);
            $record->audit_text = html_entity_decode($record->audit_text);
        }

        return $records;
    }
// RB CLD-17288:    26/01/2023 Replaced with a table (user_sourve) with the correct values. The function below
//                  gave the wrong values in the list views.
//    private function getSourceName($sourceId)
//    {
//        $sourceName = null;
//        switch ($sourceId) {
//            case 1:
//                $sourceName = 'Mobile';
//                break;
//            case 2:
//                $sourceName = 'Dataload';
//                break;
//            case 3:
//                $sourceName = 'Report';
//                break;
//        }
//        return $sourceName;
//    }
}
