<?php

namespace App\SBoxFilter\Audit;

use Tfcloud\Lib\BoxFilter\Constants\OperatorTypeConstant;
use Tfcloud\Lib\BoxFilter\Elements\TextElement;

class AuditNoUserBoxFilter extends AuditBoxFilter
{
    public function __construct(
        $filterNameId,
        $auditTable = null
    ) {
        $this->auditTable = $auditTable;
        parent::__construct($filterNameId);
    }
    public function setFilterElements($id = null)
    {
        $userIdElement = (new TextElement('user_id', 'User Id'))
            ->setDefaultOperator(OperatorTypeConstant::MATCH)
            ->setDefaultValue($id);
        $this->addBlindElement($userIdElement);
        $this->addBlindElement($this->getRecordIdBlindElementFilter($id));
        $this->addBlindElement($this->getTableNameBlindElementFilter());
        $filterElements = [
            $this->elementAction($this->getFilterLookup('actions')),
            $this->elementAuditDate(),
        ];
        if ($this->bAllFilters) {
            $filterElements = array_merge(
                $filterElements,
                [
                    $this->elementRecordCode(),
                    $this->elementModuleId($this->getFilterLookup('modules')),
                    $this->elementTable($this->getFilterLookup('tables'))
                ]
            );
        }
        return $this->addElements($filterElements);
    }

    private function getRecordIdBlindElementFilter($parentId)
    {
        return (new TextElement('record_id', 'Parent'))
            ->setDefaultValue($parentId)
            ->setDefaultOperator(OperatorTypeConstant::MATCH);
    }

    private function getTableNameBlindElementFilter()
    {
        return (new TextElement('table_name', 'Audit Table'))
            ->setDefaultValue($this->auditTable)
            ->setDefaultOperator(OperatorTypeConstant::MATCH);
    }
}
