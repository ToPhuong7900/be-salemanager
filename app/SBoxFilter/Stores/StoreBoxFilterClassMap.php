<?php

namespace App\SBoxFilter\Tree;

use Tfcloud\Lib\BoxFilterQuery\ClassMap\ClassMapModuleLoader;
use Tfcloud\Models\Filter\FilterName;
use App\SBoxFilter\Tree\ScheduledInspection\LinkScheduledInspectionBoxFilter;
use App\SBoxFilter\Tree\ScheduledInspection\LinkScheduledInspectionFilterQuery;
use App\SBoxFilter\Tree\Survey\TreeSurveyActionAllBoxFilter;
use App\SBoxFilter\Tree\Survey\TreeSurveyActionAllFilterQuery;
use App\SBoxFilter\Tree\Survey\TreeSurveyActionBoxFilter;
use App\SBoxFilter\Tree\Survey\TreeSurveyActionFilterQuery;
use App\SBoxFilter\Tree\Survey\TreeSurveyBoxFilter;
use App\SBoxFilter\Tree\Survey\TreeSurveyFilterQuery;
use App\SBoxFilter\Tree\Survey\TreeSurveyItemBoxFilter;
use App\SBoxFilter\Tree\Survey\TreeSurveyItemFilterQuery;

class StoreBoxFilterClassMap
{
    public const FILTER_NAME_CLASS_MAP = [
        FilterName::TREE_TREE_SURVEY => [
            TreeSurveyFilterQuery::class, TreeSurveyBoxFilter::class
        ],
    ];
}
