<?php

namespace App\SBoxFilter\Tree;

use Tfcloud\Lib\BoxFilter\Constants\OperatorTypeConstant;
use Tfcloud\Lib\BoxFilter\Elements\DropDownElement;
use Tfcloud\Lib\BoxFilter\Elements\Pickers\ContactPickerElement;
use Tfcloud\lib\BoxFilter\Elements\Pickers\LocationPickerElement;
use Tfcloud\Lib\BoxFilter\Elements\RadioDropDownElement;
use Tfcloud\Lib\BoxFilter\Elements\TextElement;
use Tfcloud\Lib\BoxFilter\MasterElementBoxFilter;
use App\SBoxFilter\Property\PlantMasterElement;

trait StoreMasterElement
{
    use MasterElementBoxFilter;
    use PlantMasterElement;

    protected function elementSurveyCode($label = 'Survey Code')
    {
        return (new TextElement('tree_survey_code', $label))->setOperatorsForRequiredField()
            ->allowMultipleMode();
    }

    protected function elementSurveyDesc($label = 'Survey Description')
    {
        return new TextElement('tree_survey_desc', $label);
    }

    protected function elementPlantReference($label = 'Reference')
    {
        return new TextElement('reference', $label);
    }

    protected function elementTreeReference($label = 'Tree Reference')
    {
        return new TextElement('tree_ref_num', $label);
    }

    protected function elementCompleted($label = 'Completed')
    {
        return (new RadioDropDownElement('completed', $label))->buildYesNoElement();
    }

    protected function elementSurveyStatus($surveyStatus, $label = 'Survey Status')
    {
        return (new DropDownElement(
            'tree_survey_status_id',
            $label,
            $surveyStatus,
            [
                'valueFieldName' => 'tree_survey_status_id',
                'textFieldName' => ['tree_survey_status_code', 'tree_survey_status_desc']
            ]
        ))->setOperatorsForRequiredField();
    }

    protected function elementSurveyor()
    {
        return new ContactPickerElement(
            'surveyor_contact_id',
            'Surveyor',
            [
                'surveyor' => 1
            ]
        );
    }

    protected function elementTreeSpecies($condition = null, $label = 'Tree Species')
    {
        return new DropDownElement(
            'tree_species_id',
            $label,
            $condition,
            [
                'valueFieldName' => 'tree_species_id',
                'textFieldName' => ['common_name', 'botanical_name']
            ]
        );
    }

    protected function elementSurveyActionsCode($label = 'Code')
    {
        return (new TextElement('tree_survey_action_code', $label))->setOperatorsForRequiredField()
            ->allowMultipleMode();
    }

    protected function elementResponsibleParty($label = 'Responsible Party')
    {
        return new TextElement('responsible_party_desc', $label);
    }

    protected function elementActionType($actionType, $label = 'Action Type')
    {
        return (new DropDownElement(
            'tree_survey_action_type_id',
            $label,
            $actionType,
            [
                'valueFieldName' => 'tree_survey_action_type_id',
                'textFieldName' => ['tree_survey_action_type_code', 'tree_survey_action_type_desc']
            ]
        ))->setOperatorsForRequiredField();
    }

    protected function elementActionPriority($actionPriority, $label = 'Action Priority')
    {
        return new DropDownElement(
            'tree_survey_action_priority_id',
            $label,
            $actionPriority,
            [
                'valueFieldName' => 'tree_survey_action_priority_id',
                'textFieldName' => ['tree_survey_action_priority_code', 'tree_survey_action_priority_desc']
            ]
        );
    }

    protected function elementPriceBand($actionPriceBand, $label = 'Price Band')
    {
        return new DropDownElement(
            'tree_survey_action_price_band_id',
            $label,
            $actionPriceBand,
            [
                'valueFieldName' => 'tree_survey_action_price_band_id',
                'textFieldName' => ['tree_survey_action_price_band_code', 'tree_survey_action_price_band_desc']
            ]
        );
    }

    protected function getTreeSurveyElements($isTreeSurveyItemBoxFilter = false)
    {
        $elements = [
            $this->elementDateRange('survey_date', 'Survey Date'),
            $this->elementSurveyCode()->setNotAppliedFilter(),
            $this->elementSurveyDesc()->setNotAppliedFilter(),
            $this->elementSurveyor(),
            $this->elementOwnerUserId()->setNotAppliedFilter(),
            $this->elementPlantId('Plant', $isTreeSurveyItemBoxFilter),
            $this->elementPlantReference(),
            $this->elementEstablishmentId($this->getFilterLookup('establishment')),
            $this->elementTreeReference(),
            $this->elementCompleted(),
            $this->elementTreeSpecies($this->getFilterLookup('treeSpecies')),
            $this->elementSurveyStatus($this->getFilterLookup('surveyStatus'))->setNotAppliedFilter()
        ];

        $locationElement = new LocationPickerElement(3, true, true);
        $locationElement->setNotAppliedFilter();
        $plantGroupCascading = $this->getElementPlantGroupCascading();
        $elements = array_merge(
            $elements,
            $locationElement->getElements(),
            $plantGroupCascading
        );

        return $elements;
    }

    protected function elementReplacementYear(
        $replacementYears,
        $label = 'Replacement Year',
        $dataConfig = [],
        $options = []
    ) {
        $element = new DropDownElement('replacementYear', $label, $replacementYears, $dataConfig, $options);
        $element->setValueFieldName('year')->setTextFieldNames([
            'year'
        ]);
        return $element;
    }

    protected function getTreeRegisterElements()
    {
        $plantGroupCascading = $this->getElementPlantGroupCascading(true);
        $elements = [
            $this->elementPlantCode('Code')->setNotAppliedFilter(),
            $this->elementPlantDesc('Description', false),
            $this->elementReplacementYear($this->getFilterLookup('replacementYear'))
                ->setOperators(
                    [
                        OperatorTypeConstant::INCLUDE,
                        OperatorTypeConstant::NOT_INCLUDE
                    ]
                ),
            $this->elementAgeRag(),
            $this->elementTPO(),
            $this->elementPlantCriticality($this->getFilterLookup('plantCriticality')),
            $this->elementPlantCondition($this->getFilterLookup('plantCondition')),
            $this->elementStatus()->setNotAppliedFilter(),
            $this->elementGMPlant(),
            $this->elementMaintContactId(),
            $this->elementOwnerUserId()->setNotAppliedFilter(),
            $this->elementTreeSpecies($this->getFilterLookup('treeSpecies')),
            $this->elementTreeCategory($this->getFilterLookup('treeGrading')),
            $this->elementTreeSubCategory($this->getFilterLookup('treeSubGrading')),
        ];
        $locationElement = new LocationPickerElement(3);
        $locationElement->setNotAppliedFilter();
        return array_merge(
            $elements,
            $locationElement->getElements(),
            $plantGroupCascading
        );
    }

    protected function getTreeSurveyActionsElements($treeSurvey = null)
    {
        $elements = [
            $this->elementSurveyActionsCode()->setNotAppliedFilter(),
            $this->elementResponsibleParty(),
            $this->elementComment('Comments', 'comments', []),
            $this->elementDateRange('target_date', 'Target From'),
            $this->elementActionType($this->getFilterLookup('surveyActionTypes'))->setNotAppliedFilter(),
            $this->elementActionPriority($this->getFilterLookup('surveyActionPriorities'))->setNotAppliedFilter(),
            $this->elementPriceBand($this->getFilterLookup('surveyActionPriceBands'))->setNotAppliedFilter(),
            $this->elementOwnerUserId()->setNotAppliedFilter(),
        ];

        if (empty($treeSurvey)) {
            $locationElement = new LocationPickerElement(3, true, true);

            return array_merge(
                $elements,
                $locationElement->setNotAppliedFilter()->getElements(),
            );
        }

        return $elements;
    }
}
