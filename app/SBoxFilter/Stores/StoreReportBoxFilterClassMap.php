<?php

namespace App\SBoxFilter\Tree;

use Tfcloud\Lib\Constant\ReportConstant;
use Tfcloud\Models\Filter\FilterName;
use App\SBoxFilter\Tree\Report\TREE01BoxFilter;
use App\SBoxFilter\Tree\Report\TREE01FilterQuery;
use App\SBoxFilter\Tree\Report\TREE02BoxFilter;
use App\SBoxFilter\Tree\Report\TREE03BoxFilter;
use App\SBoxFilter\Tree\Report\TREE02FilterQuery;
use App\SBoxFilter\Tree\Report\TREE03FilterQuery;
use App\SBoxFilter\Tree\Report\TREE04BoxFilter;
use App\SBoxFilter\Tree\Report\TREE04FilterQuery;

class StoreReportBoxFilterClassMap
{
    public const TREE_REPORT_CLASS_MAP = [
        ReportConstant::SYSTEM_REPORT_TREE01 => [
            FilterName::TREE_REPORT_TREE01, TREE01BoxFilter::class, TREE01FilterQuery::class
        ],
        ReportConstant::SYSTEM_REPORT_TREE02 => [
            FilterName::TREE_REPORT_TREE02, TREE02BoxFilter::class, TREE02FilterQuery::class
        ],
        ReportConstant::SYSTEM_REPORT_TREE03 => [
            FilterName::TREE_REPORT_TREE03, TREE03BoxFilter::class, TREE03FilterQuery::class
        ],
        ReportConstant::SYSTEM_REPORT_TREE04 => [
            FilterName::TREE_REPORT_TREE04, TREE04BoxFilter::class, TREE04FilterQuery::class
        ]
    ];
}
