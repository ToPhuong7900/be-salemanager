<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository as PrettusBaseRepository;
use Illuminate\Pagination\Paginator;

abstract class BaseRepository extends PrettusBaseRepository
{
    /**
     * Get Searchable Fields
     */
    public function getAvailableRelations(): array
    {
        return [];
    }

    public function boot()
    {
        parent::boot();
        Paginator::currentPageResolver(function () {
            return request()->input('page.number', 1);
        });
    }
}
