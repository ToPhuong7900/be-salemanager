<?php

namespace App\Repositories;

interface IRepository
{
    public function get(int $id, array $options = [], array $columns = ['*']);
    public function all(array $columns = ['*'], array $options = []);
    public function prepare(array $data, $id = null);
    public function save();
}
