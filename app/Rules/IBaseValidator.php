<?php

namespace App\Rules;

use App\Models\BaseModel;
use Illuminate\Contracts\Validation\Validator as ValidatorContract;
use Illuminate\Validation\Validator;

interface IBaseValidator
{
    public function getValidator(): Validator;
    public function getModel(): BaseModel;
    public function succeed(): bool;
    public function isSuccess(): bool;
    public function mergeValidators(ValidatorContract $validator, $errMsgPrefix = ''): void;
    public function setSuccess($value);
}
