<?php

namespace App\Rules;

use App\Models\BaseModel;
use App\Models\User;
use Illuminate\Contracts\Validation\Validator as ValidatorContract;
use Illuminate\Support\Facades\Validator as ValidatorFactory;
use Illuminate\Validation\Validator;

abstract class BaseValidator implements IBaseValidator
{
    protected Validator $validator;

    protected BaseModel $model;

    private bool $isForDataLoad = false;

    private ?bool $success = null;

    protected array $inputs = [];

    /**
     * @param BaseModel $model
     * @param array $inputs
     */
    public function __construct(BaseModel $model, array $inputs = [])
    {
        $this->inputs = $inputs;
        $this->setModel($model)
            ->setValidator($this->model, $this->buildRules());
    }

    /**
     * @return Validator
     */
    public function getValidator(): Validator
    {
        return $this->validator;
    }

    /**
     * @return BaseModel
     */
    public function getModel(): BaseModel
    {
        return $this->model;
    }

    public function succeed(): bool
    {
        $this->success = $this->validator->passes();
        return $this->success;
    }

    public function isSuccess(): bool
    {
        if (is_null($this->success)) {
            return $this->succeed();
        }
        return $this->success;
    }

    abstract protected function buildRules(): array;

    protected function setModel(BaseModel $model): static
    {
        $this->model = $model;
        return $this;
    }

    protected function setValidator(BaseModel $model, array $rules = []): static
    {
        $attributes = $model->getAttributes();
        $this->validator = ValidatorFactory::make($attributes, $rules, $this->message());
        return $this;
    }

    protected function message(): array
    {
        return [];
    }

    public function mergeValidators(ValidatorContract $validator, $errMsgPrefix = ''): void
    {
        if ($validator->fails()) {
            $this->success = false;
            foreach ($validator->errors()->all() as $key => $error) {
                $errorMessage = $errMsgPrefix ? "$errMsgPrefix - $error" : ucfirst($error);
                $this->validator->errors()->add($key, $errorMessage);
            }
        }
    }

    public function setSuccess($value)
    {
        return $this->success = $value;
    }
}
