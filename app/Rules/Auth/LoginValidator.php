<?php

namespace App\Rules\Auth;

use App\Models\BaseModel;
use App\Rules\BaseValidator;
use Illuminate\Support\Facades\Validator as ValidatorFactory;

class LoginValidator extends BaseValidator
{
    public function qu__construct(BaseModel $model, array $inputs = [])
    {
        $this->inputs = $inputs;
        parent::__construct($model, $inputs);
    }

    protected function buildRules(): array
    {
       return [
           'email' => 'required|string|email',
           'password' => 'required|string'
       ];
    }

    protected function setValidator(BaseModel $model, array $rules = []): static
    {
        $attributes = $this->inputs;
        $this->validator = ValidatorFactory::make($attributes, $rules, $this->message());
        return $this;
    }
}
