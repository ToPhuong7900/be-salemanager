<?php

use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\BaseUnitController;
use App\Http\Controllers\brand\BrandController;
use \App\Http\Controllers\Category\CategoryController;
use \App\Http\Controllers\Category\SubCategoryController;
use \App\Http\Controllers\Warehouse\WarehouseController;
use \App\Http\Controllers\Stores\StoreController;
use \App\Http\Controllers\Variant\VariantTypeAPIController;
use \App\Http\Controllers\Products\ProductAPIController;
use \App\Http\Controllers\Purchase\PurchaseAPIController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::controller(AuthController::class)->group(function() {
    Route::post('/login', 'login');
    Route::post('/register', 'register');
});
Route::middleware('auth:api')->group( function () {
    Route::get('me', [AuthController::class, 'getInfo'])->name('me');

    Route::post('/brand', [BrandController::class, 'store']);
    Route::post('/brand/{id}', [BrandController::class, 'update']);
    Route::delete('/brand/{id}', [BrandController::class, 'destroy']);

    Route::post("/base-unit", [BaseUnitController::class, "store"]);
    Route::post("/base-unit/{id}", [BaseUnitController::class, "update"]);
    Route::delete("/base-unit/{id}", [BaseUnitController::class, "destroy"]);

    Route::post("/category", [CategoryController::class, "store"]);
    Route::post("/category/{id}", [CategoryController::class, "update"]);
    Route::delete("/category/{id}", [CategoryController::class, "destroy"]);

    Route::post("/sub-category", [SubCategoryController::class, "store"]);
    Route::post("/sub-category/{id}", [SubCategoryController::class, "update"]);
    Route::delete("/sub-category/{id}", [SubCategoryController::class, "destroy"]);

    Route::post("/warehouse", [WarehouseController::class, "store"]);
    Route::post("/warehouse/{id}", [WarehouseController::class, "update"]);
    Route::delete("/warehouse/{id}", [WarehouseController::class, "destroy"]);

    Route::post("/store", [StoreController::class, "store"]);
    Route::post("/store/{id}", [StoreController::class, "update"]);
    Route::delete("/store/{id}", [StoreController::class, "destroy"]);

    Route::post("/variant-type", [VariantTypeAPIController::class, "store"]);
    Route::post("/variant-type/{id}", [VariantTypeAPIController::class, "update"]);
    Route::delete("/variant-type/{id}", [VariantTypeAPIController::class, "destroy"]);

    Route::post("/product", [ProductAPIController::class, "store"]);
    Route::post("/product/{id}", [ProductAPIController::class, "update"]);
    Route::delete("/product/{id}", [ProductAPIController::class, "destroy"]);

    Route::post("/purchase", [PurchaseAPIController::class, "store"]);
});


Route::controller(BrandController::class)->group(function() {
    Route::get("/brand/{id}", 'show');
    Route::get("/brand", "index");
});

Route::controller(BaseUnitController::class)->group(function() {
    Route::get("/base-unit/{id}", 'show');
    Route::get("/base-unit", "index");
});

Route::controller(CategoryController::class)->group(function() {
    Route::get("/category/{id}", 'show');
    Route::get("/category", "index");
});

Route::controller(SubCategoryController::class)->group(function() {
    Route::get("/sub-category/{id}", 'show');
    Route::get("/sub-category", "index");
});

Route::controller(WarehouseController::class)->group(function() {
    Route::get("/warehouse/{id}", 'show');
    Route::get("/warehouse", "index");
});

Route::controller(StoreController::class)->group(function() {
    Route::get("/store/{id}", 'show');
    Route::get("/store", "index");

});

Route::controller(VariantTypeAPIController::class)->group(function() {
    Route::get("/variant-type/{id}", 'show');
    Route::get("/variant-type", "index");
});

Route::controller(ProductAPIController::class)->group(function() {
    Route::get("/product/{id}", 'show');
    Route::get("/product", "index");
});
