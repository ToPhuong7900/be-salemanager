<?php

return [
    'login_success' => 'You have successfully logged in.',
    'login_fail' => 'The provided credentials do not match our records.',
];
