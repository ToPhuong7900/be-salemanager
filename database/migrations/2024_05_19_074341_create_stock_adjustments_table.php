<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if(!Schema::hasTable('stock_adjustment')) {
            Schema::create('stock_adjustment', function (Blueprint $table) {
                $table->bigIncrements('stock_adjustment_id');
                $table->string('stock_adjustment_code');
                $table->longText('stock_adjustment_note')->nullable();
                $table->date('stock_adjustment_date')->default(now());
                $table->unsignedBigInteger('responsible_person_id');
                $table->unsignedBigInteger('warehouse_id')->nullable();
                $table->unsignedBigInteger('store_id')->nullable();
                $table->unsignedBigInteger('company_id');
                $table->unsignedBigInteger('product_variant_id');
                $table->unsignedBigInteger('quantity');
                $table->unsignedBigInteger('stock_adjustment_type_id');
                $table->unsignedBigInteger('site_group_id');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('stock_addjustments');
    }
};
