<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('site_google_captcha', function (Blueprint $table) {
            $table->bigIncrements('site_group_google_captcha_id');
            $table->unsignedBigInteger('site_group_id');
            $table->string('site_group_google_recaptcha_site_key');
            $table->string('site_group_google_recaptcha_secret_key');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('site_group_google_captcha');
    }
};
