<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('filter_table_header_user', function (Blueprint $table) {
            $table->increments('filter_table_header_user_id');
            $table->integer('filter_name_id');
            $table->integer('user_id');
            $table->json('column_visible');
            $table->char('wrap_rows', 1)->default('Y');
            $table->unique(['filter_name_id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('filter_table_header_users');
    }
};
