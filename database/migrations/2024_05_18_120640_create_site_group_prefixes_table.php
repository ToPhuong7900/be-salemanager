<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('site_group_prefix', function (Blueprint $table) {
            $table->bigIncrements('site_group_prefix_id');
            $table->unsignedBigInteger('site_group_id');
            $table->string('site_group_prefix_product', 5);
            $table->string('site_group_prefix_supplier', 5);
            $table->string('site_group_prefix_purchase', 5);
            $table->string('site_group_prefix_purchase_return', 5);
            $table->string('site_group_prefix_sales', 5);
            $table->string('site_group_prefix_sales_return', 5);
            $table->string('site_group_prefix_customer', 5);
            $table->string('site_group_prefix_expense', 5);
            $table->string('site_group_prefix_stock_transfer', 5);
            $table->string('site_group_prefix_stock_adjustment', 5);
            $table->string('site_group_prefix_sales_order', 5);
            $table->string('site_group_prefix_pos_invoice', 5);
            $table->string('site_group_prefix_estimation', 5);
            $table->string('site_group_prefix_transaction', 5);
            $table->string('site_group_prefix_employee', 5);
            $table->string('site_group_prefix_company', 5);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('site_group_prefix');
    }
};
