<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('accent_color', function (Blueprint $table) {
            $table->bigIncrements('accent_color_id');
            $table->string('accent_color_code', 20);
            $table->string('accent_color_desc')->nullable();
            $table->char('accent_color_active', 1)->default('N');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('accent_color');
    }
};
