<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if(!Schema::hasTable('stock_transfer_status')) {
            Schema::create('stock_transfer_status', function (Blueprint $table) {
                $table->bigIncrements('stock_transfer_status_id');
                $table->string('stock_transfer_status_code', 20);
                $table->string('stock_transfer_status_desc')->nullable();
                $table->char('stock_transfer_status_active')->default('N');
                $table->unsignedBigInteger('stock_transfer_status_type_id');
                $table->unsignedBigInteger('site_group_id');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('stock_transfer_status');
    }
};
