<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('employee', function (Blueprint $table) {
            $table->bigIncrements('employee_id');
            $table->string('employee_code', 20);
            $table->string('employee_name');
            $table->string('employee_phone')->nullable();
            $table->date('employee_birthday')->nullable();
            $table->date('employee_join_date')->nullable();
            $table->string('employee_avatar')->nullable();
            $table->string('employee_address')->nullable();
            $table->string('employee_country')->nullable();
            $table->string('employee_city')->nullable();
            $table->string('employee_province')->nullable();
            $table->longText('employee_desc')->nullable();
            $table->string('employee_link_facebook')->nullable();
            $table->string('employee_link_instagram')->nullable();
            $table->string('employee_link_tiktok')->nullable();
            $table->string('employee_link_shoppe')->nullable();
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('department_id')->nullable();
            $table->unsignedBigInteger('store_id')->nullable();
            $table->unsignedBigInteger('site_group_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('employee');
    }
};
