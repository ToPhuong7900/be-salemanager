<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('unit', function (Blueprint $table) {
            $table->integerIncrements('unit_id');
            $table->unsignedBigInteger('base_unit_id');
            $table->string('unit_code', 20);
            $table->string('unit_desc')->nullable();
            $table->char('unit_active')->default('N');
            $table->unsignedBigInteger('site_group_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('unit');
    }
};
