<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('prefix_default', function (Blueprint $table) {
            $table->string('prefix_default_product', 5);
            $table->string('prefix_default_supplier', 5);
            $table->string('prefix_default_purchase', 5);
            $table->string('prefix_default_purchase_return', 5);
            $table->string('prefix_default_sales', 5);
            $table->string('prefix_default_sales_return', 5);
            $table->string('prefix_default_customer', 5);
            $table->string('prefix_default_expense', 5);
            $table->string('prefix_default_stock_transfer', 5);
            $table->string('prefix_default_stock_adjustment', 5);
            $table->string('prefix_default_sales_order', 5);
            $table->string('prefix_default_pos_invoice', 5);
            $table->string('prefix_default_estimation', 5);
            $table->string('prefix_default_transaction', 5);
            $table->string('prefix_default_employee', 5);
            $table->string('prefix_default_company', 5);
            $table->string('prefix_default_invoice', 5);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('prefix');
    }
};
