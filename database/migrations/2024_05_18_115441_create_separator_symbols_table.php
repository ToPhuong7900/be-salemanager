<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('separator_symbol', function (Blueprint $table) {
            $table->bigIncrements('separator_symbol_id');
            $table->string('separator_symbol_code', 20);
            $table->string('separator_symbol_desc')->nullable();
            $table->char('separator_symbol_active', 1)->default('N');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('separator_symbol');
    }
};
