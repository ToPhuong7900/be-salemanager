<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('date_format', function (Blueprint $table) {
            $table->bigIncrements('date_format_id');
            $table->string('date_format_code', 20);
            $table->string('date_format_desc')->nullable();
            $table->char('date_format_active', 1)->default('N');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('date_format');
    }
};
