<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('stock_transfer_status_type', function (Blueprint $table) {
            $table->bigIncrements('stock_transfer_status_type_id');
            $table->string('stock_transfer_status_type_code', 20);
            $table->string('stock_transfer_status_type_desc')->nullable();
            $table->char('stock_transfer_status_type_active')->default('N');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('stock_transfer_status_type_type');
    }
};
