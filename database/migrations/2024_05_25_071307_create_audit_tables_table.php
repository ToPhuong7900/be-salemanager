<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('audit_table', function (Blueprint $table) {
            $table->increments('audit_table_id');
            $table->char("table_dbname", 64);
            $table->char("table_display_name", 64);
            $table->integer("module_id");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('audit_table');
    }
};
