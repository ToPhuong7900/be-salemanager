<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('sale', function (Blueprint $table) {
            $table->bigIncrements('sale_id');
            $table->string('sale_code', 20);
            $table->date('sale_date_in');
            $table->date('sale_date_out');
            $table->decimal('sale_grand_total');
            $table->decimal('discount_value')->default(0);
            $table->integer('sale_tax_value')->default(0);
            $table->unsignedBigInteger('discount_type_id')->nullable();
            $table->unsignedBigInteger('discount_id')->nullable();
            $table->unsignedBigInteger('customer_id');
            $table->unsignedBigInteger('sale_seller_id');
            $table->unsignedBigInteger('payment_method_id');
            $table->unsignedBigInteger('payment_method_status_id');
            $table->unsignedBigInteger('shipping_method_id');
            $table->unsignedBigInteger('shipping_method_status_id');
            $table->unsignedBigInteger('sale_status_id');
            $table->unsignedBigInteger('store_id');
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('site_group_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('sale');
    }
};
