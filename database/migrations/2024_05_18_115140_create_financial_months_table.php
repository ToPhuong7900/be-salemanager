<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('financial_month', function (Blueprint $table) {
            $table->bigIncrements('financial_month_id');
            $table->string('financial_month_code', 20);
            $table->string('financial_month_desc')->nullable();
            $table->char('financial_month_active', 1)->default('N');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('financial_month');
    }
};
