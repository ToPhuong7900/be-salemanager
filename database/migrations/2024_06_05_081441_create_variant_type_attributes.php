<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('variant_type_attributes', function (Blueprint $table) {
            $table->increments('variant_type_attributes_id');
            $table->integer('variant_type_id');
            $table->string('variant_type_attributes_name');
            $table->char('variant_type_attributes_active', 1)->default('Y');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('variant_type_attributes');
    }
};
