<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('time_format', function (Blueprint $table) {
            $table->bigIncrements('time_format_id');
            $table->string('time_format_code', 20);
            $table->string('time_format_desc')->nullable();
            $table->char('time_format_active', 1)->default('N');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('time_format');
    }
};
