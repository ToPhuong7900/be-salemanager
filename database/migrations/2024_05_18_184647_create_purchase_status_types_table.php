<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('purchase_status_type', function (Blueprint $table) {
            $table->bigIncrements('purchase_status_type_id');
            $table->string('purchase_status_type_code', 20);
            $table->string('purchase_status_type_desc')->nullable();
            $table->char('purchase_status_type_active', 1)->default('N');
            $table->unsignedBigInteger('site_group_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('purchase_status_type');
    }
};
