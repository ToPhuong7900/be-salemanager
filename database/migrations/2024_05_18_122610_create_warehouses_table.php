<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('warehouse', function (Blueprint $table) {
            $table->bigIncrements('warehouse_id');
            $table->string('warehouse_code', 20);
            $table->string('warehouse_name')->default('Default');
            $table->string('warehouse_phone')->nullable();
            $table->string('warehouse_address')->nullable();
            $table->string('warehouse_country')->nullable();
            $table->string('warehouse_city')->nullable();
            $table->string('warehouse_province')->nullable();
            $table->unsignedBigInteger('warehouse_manager_id')->nullable();
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('site_group_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('warehouse');
    }
};
