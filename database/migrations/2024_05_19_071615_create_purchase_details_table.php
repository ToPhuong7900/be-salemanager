<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('purchase_detail', function (Blueprint $table) {
            $table->bigIncrements('purchase_detail_id');
            $table->unsignedBigInteger('product_variant_id');
            $table->unsignedBigInteger('purchase_id');
            $table->string('purchase_product_quantity');
            $table->decimal('purchase_product_price');
            $table->string('purchase_product_tax');
            $table->unsignedBigInteger('discount_type_id');
            $table->decimal('discount_value');
            $table->string('purchase_product_net_price');
            $table->unsignedBigInteger('site_group_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('purchase_detail');
    }
};
