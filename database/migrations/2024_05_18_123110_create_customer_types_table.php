<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('customer_type', function (Blueprint $table) {
            $table->bigIncrements('customer_type_id');
            $table->string('customer_type_code', 20);
            $table->string('customer_type_desc')->nullable();
            $table->char('customer_type_active', 1)->default('N');
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('site_group_id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('customer_type');
    }
};
