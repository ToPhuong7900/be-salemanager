<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('company', function (Blueprint $table) {
            $table->bigIncrements('company_id');
            $table->string('company_code', 20)->unique();
            $table->string('company_name', 128);
            $table->string('company_desc', 128);
            $table->string('company_logo', 128)->nullable();
            $table->string('company_icon', 128)->nullable();
            $table->string('company_favicon', 128)->nullable();
            $table->string('company_email', 128)->nullable();
            $table->string('company_phone', 128)->nullable();
            $table->string('company_fax', 128)->nullable();
            $table->string('company_address', 128)->nullable();
            $table->string('company_country', 128)->nullable();
            $table->string('company_province', 128)->nullable();
            $table->string('company_city', 128)->nullable();
            $table->string('company_postal_code', 128)->nullable();
            $table->char('company_active', 1)->default('N');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('company');
    }
};
