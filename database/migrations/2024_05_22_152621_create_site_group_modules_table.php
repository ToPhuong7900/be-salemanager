<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('site_group_module', function (Blueprint $table) {
            $table->bigIncrements('site_group_module_id');
            $table->unsignedBigInteger('site_group_id');
            $table->unsignedBigInteger('module_id');
            $table->string('module_name')->nullable();
            $table->char('module_active', 1)->default('N');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('site_group_module');
    }
};
