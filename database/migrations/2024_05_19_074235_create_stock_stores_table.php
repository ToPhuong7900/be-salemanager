<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('stock_store', function (Blueprint $table) {
            $table->bigIncrements('stock_store_id');
            $table->unsignedBigInteger('product_variant_id');
            $table->unsignedBigInteger('store_id');
            $table->unsignedBigInteger('company_id');
            $table->integer('stock_store_quantity');
            $table->unsignedBigInteger('site_group_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('stock_store');
    }
};
