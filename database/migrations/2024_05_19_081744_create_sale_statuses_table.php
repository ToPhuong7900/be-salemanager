<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('sale_status', function (Blueprint $table) {
            $table->bigIncrements('sale_status_id');
            $table->string('sale_status_code', 20);
            $table->string('sale_status_desc')->nullable();
            $table->char('sale_status_active')->default('N');
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('site_group_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('sale_status');
    }
};
