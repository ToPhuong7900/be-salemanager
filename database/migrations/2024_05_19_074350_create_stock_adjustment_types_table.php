<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if(!Schema::hasTable('stock_adjustment_type')) {
            Schema::create('stock_adjustment_type', function (Blueprint $table) {
                $table->bigIncrements('stock_adjustment_type_id');
                $table->string('stock_adjustment_type_code');
                $table->string('stock_adjustment_type_desc')->nullable();
                $table->char('stock_adjustment_type_active', 1)->default('N');
                $table->unsignedBigInteger('site_group_id');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('stock_adjustment_typ');
    }
};
