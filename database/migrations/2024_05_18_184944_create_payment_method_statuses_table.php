<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('payment_method_status', function (Blueprint $table) {
            $table->bigIncrements('payment_method_status_id');
            $table->string('payment_method_status_code', 20);
            $table->string('payment_method_status_desc')->nullable();
            $table->char('payment_method_status_active', 1)->default('N');
            $table->integer('payment_method_status_order')->default(1);
            $table->unsignedBigInteger('site_group_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('payment_method_status');
    }
};
