<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('product', function (Blueprint $table) {
            $table->bigIncrements('product_id');
            $table->string('product_code', 20);
            $table->string('product_sku', 20);
            $table->string('product_name');
            $table->longText('product_desc');
            $table->unsignedBigInteger('category_id');
            $table->foreign('category_id')->references('category_id')
            ->on('category')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->unsignedBigInteger('sub_category_id');
            $table->unsignedBigInteger('brand_id');
            $table->foreign('brand_id')->references('brand_id')->on('brand')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->unsignedBigInteger('warehouse_id');
            $table->foreign('warehouse_id')->references('warehouse_id')
                ->on('warehouses')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->unsignedBigInteger('store_id');
            $table->foreign('store_id')->references('store_id')
                ->on('store')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->double('product_price');
            $table->string('stock_alert')->nullable();
            $table->enum('tax_type', [1, 2])->nullable();
            $table->double('order_tax')->nullable();
            $table->integer('selling_type_id')->nullable();
            $table->integer('base_unit_id');
            $table->char('product_active', 1)->default('N');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('product');
    }
};
