<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('filter_name', function (Blueprint $table) {
            $table->integer('filter_name_id')->primary();
            $table->string('filter_name_code', 128)->unique();
            $table->string('filter_name_desc', 256)->nullable();
            $table->integer('module_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('filter_names');
    }
};
