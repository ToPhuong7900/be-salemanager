<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('filter_user_setting', function (Blueprint $table) {
            $table->increments('filter_user_setting_id');
            $table->integer('site_group_id');
            $table->string('filter_user_setting_desc', 128);
            $table->integer('filter_name_id');
            $table->integer('user_id');
            $table->json('filter_value');
            $table->char('default', 1)->default('N');
            $table->unsignedSmallInteger('display_order');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('filter_user_settings');
    }
};
