<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('discount_unit', function (Blueprint $table) {
            $table->bigIncrements('discount_unit_id');
            $table->string('discount_unit_code', 20);
            $table->string('discount_unit_desc')->nullable();
            $table->char('discount_unit_active')->default('N');
            $table->unsignedBigInteger('site_group_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('discount_unit');
    }
};
