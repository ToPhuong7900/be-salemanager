<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('file_extension', function (Blueprint $table) {
            $table->bigIncrements('file_extension_id');
            $table->string('file_extension_code', 20);
            $table->string('file_extension_desc')->nullable();
            $table->char('file_extension_active', 1)->default('N');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('file_extension');
    }
};
