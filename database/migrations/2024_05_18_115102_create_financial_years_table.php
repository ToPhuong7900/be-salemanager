<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('financial_year', function (Blueprint $table) {
            $table->bigIncrements('financial_year_id');
            $table->string('financial_year_code', 20);
            $table->string('financial_year_desc')->nullable();
            $table->char('financial_year_active', 1)->default('N');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('financial_year');
    }
};
