<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('product_variant_type_image', function (Blueprint $table) {
            $table->bigIncrements('product_variant_type_image_id');
            $table->unsignedBigInteger('product_variant_type_id');
            $table->string('image_name');
            $table->string('image_path');
            $table->string('image_extension');
            $table->string('image_size');
            $table->unsignedBigInteger('site_group_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('product_image');
    }
};
