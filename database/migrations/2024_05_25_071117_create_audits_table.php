<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('audit', function (Blueprint $table) {
            $table->increments('audit_id');
            $table->integer('user_id')->default(null);
            $table->integer('source_id')->default(null);
            $table->char("table_name", 45);
            $table->integer('audit_action_id')->default(null);
            $table->integer('record_id')->default(null);
            $table->char("record_code", 45)->default(null);
            $table->char("parent_table_name", 45)->default(null);
            $table->integer('parent_record_id')->default(null);
            $table->char("audit_text", 255)->default("N");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('audit');
    }
};
