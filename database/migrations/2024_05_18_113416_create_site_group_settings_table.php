<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('site_group_setting', function (Blueprint $table) {
            $table->bigIncrements('site_group_setting_id');
            $table->unsignedBigInteger('site_group_id');
            $table->unsignedBigInteger('language_id');
            $table->unsignedBigInteger('time_zone_id');
            $table->unsignedBigInteger('date_format_id');
            $table->unsignedBigInteger('time_format_id');
            $table->unsignedBigInteger('financial_year_id');
            $table->unsignedBigInteger('financial_month_id');
            $table->unsignedBigInteger('currency_id');
            $table->unsignedBigInteger('currency_symbol_id');
            $table->unsignedBigInteger('decimal_separator_symbol_id');
            $table->unsignedBigInteger('thousand_separator_symbol_id');
            $table->unsignedBigInteger('accent_color_id');
            $table->unsignedBigInteger('theme_id');
            $table->unsignedBigInteger('default_customer_type_id');
            $table->json('file_extension_codes');
            $table->integer('max_file_size')->default(5000);
            $table->char('language_switcher', 1)->default('Y');
            $table->char('expand_sidebar', 1)->default('Y');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('site_group_setting');
    }
};
