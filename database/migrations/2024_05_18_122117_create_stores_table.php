<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('store', function (Blueprint $table) {
            $table->bigIncrements('store_id');
            $table->string('store_code', 20);
            $table->string('store_name')->default('Default');
            $table->string('store_phone')->nullable();
            $table->string('store_address')->nullable();
            $table->string('store_country')->nullable();
            $table->string('store_city')->nullable();
            $table->string('store_province')->nullable();
            $table->unsignedBigInteger('store_manager_id')->nullable();
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('site_group_id');
            $table->char('date_format_active', 1)->default('N');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('store');
    }
};
