<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('discount', function (Blueprint $table) {
            $table->bigIncrements('discount_id');
            $table->string('discount_code', 20);
            $table->string('discount_desc');
            $table->longText('discount_rule');
            $table->decimal('discount_value');
            $table->date('discount_start_date');
            $table->date('discount_end_date')->nullable();
            $table->unsignedBigInteger('discount_type_id');//hoa don hay mac hang
            $table->unsignedBigInteger('discount_unit_id');//% hay value
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('store_id');
            $table->unsignedBigInteger('site_group_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('discount');
    }
};
