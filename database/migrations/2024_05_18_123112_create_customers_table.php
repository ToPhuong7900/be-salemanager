<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('customer', function (Blueprint $table) {
            $table->bigIncrements('customer_id');
            $table->string('customer_code', 20);
            $table->string('customer_name')->default('Guest');
            $table->string('customer_phone');
            $table->date('customer_birthday')->nullable();
            $table->string('customer_avatar')->nullable();
            $table->string('customer_address')->nullable();
            $table->string('customer_country')->nullable();
            $table->string('customer_city')->nullable();
            $table->string('customer_province')->nullable();
            $table->longText('customer_desc')->nullable();
            $table->string('customer_link_facebook')->nullable();
            $table->string('customer_link_instagram')->nullable();
            $table->string('customer_link_tiktok')->nullable();
            $table->string('customer_link_shoppe')->nullable();
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('store_id')->nullable();
            $table->unsignedBigInteger('customer_type_id');
            $table->unsignedBigInteger('site_group_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('customer');
    }
};
