<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('base_unit', function (Blueprint $table) {
            $table->bigIncrements('base_unit_id');
            $table->string('base_unit_code', 20);
            $table->string('base_unit_desc')->nullable();
            $table->char('base_unit_active')->default('N');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('base_unit');
    }
};
