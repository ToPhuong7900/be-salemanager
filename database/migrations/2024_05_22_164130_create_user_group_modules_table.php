<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('user_group_module', function (Blueprint $table) {
            $table->bigIncrements('user_group_module_id');
            $table->unsignedBigInteger('user_group_id');
            $table->unsignedBigInteger('module_id');
            $table->unsignedBigInteger('crud_access_level_id');
            $table->unsignedBigInteger('site_group_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('role_module');
    }
};
