<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('site_group_google_map', function (Blueprint $table) {
            $table->bigIncrements('site_group_google_map_id');
            $table->unsignedBigInteger('site_group_id');
            $table->string('site_group_google_map_key');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('site_group_google_map');
    }
};
