<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('selling_type', function (Blueprint $table) {
            $table->bigIncrements('selling_type_id');
            $table->string('selling_type_code', 20);
            $table->string('selling_type_name');
            $table->string('selling_type_desc');
            $table->char('selling_type_active', 1);
            $table->unsignedBigInteger('site_group_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('selling_type');
    }
};
