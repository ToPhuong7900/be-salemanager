<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('stock_transfer', function (Blueprint $table) {
            $table->bigIncrements('stock_transfer_id');
            $table->unsignedBigInteger('source_id');
            $table->unsignedBigInteger('source_type_id');
            $table->unsignedBigInteger('destination_id');
            $table->unsignedBigInteger('destination_type_id');
            $table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('stock_transfer_approver_id');
            $table->unsignedBigInteger('stock_transfer_requester_id');
            $table->date('stock_transfer_date');
            $table->date('stock_transfer_status_id');
            $table->integer('stock_transfer_quantity');
            $table->unsignedBigInteger('site_group_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('stock_transfer');
    }
};
