<?php
namespace Database\Seeders;

use App\Libs\Common\CommonConstant;
use App\Models\SellingType;
use Illuminate\Database\Seeder;

class SellingTypeSeeder extends Seeder
{
    public function run(): void
    {
        $sellingType = [
            [
                'selling_type_id' => 1, 'selling_type_code' => 'TRANSACTIONAL', 'selling_type_name' => 'TRANSACTIONAL',
                'selling_type_desc' => 'TRANSACTIONAL', 'selling_type_active' => CommonConstant::DATABASE_VALUE_YES,
                'site_group_id' => 1
            ],
            [
                'selling_type_id' => 2, 'selling_type_code' => 'SOLUTION', 'selling_type_name' => 'SOLUTION',
                'selling_type_desc' => 'SOLUTION', 'selling_type_active' => CommonConstant::DATABASE_VALUE_YES,
                'site_group_id' => 1
            ]
        ];

        foreach ($sellingType as $type)
        {
            SellingType::create($type);
        }
    }
}
