<?php

namespace Database\Seeders;

use App\Libs\Common\CommonConstant;
use App\Models\Company;
use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    public function run(): void
    {
        Company::Create([
            'company_code' => 'NT',
            'company_name' => 'Nashtech VietNam',
            'company_desc' => 'Cong ty hang dau ap bac',
            'company_active' => CommonConstant::DATABASE_VALUE_YES,
        ]);
    }
}
