<?php
namespace Database\Seeders;

use App\Libs\Common\CommonConstant;
use App\Models\BaseUnit;
use Illuminate\Database\Seeder;

class DefaultBaseUnitSeeder extends Seeder
{
    public function run(): void
    {
        $baseUnits = [
            [
                'base_unit_id' => 1, 'base_unit_code' => 'piece', 'base_unit_desc' => 'piece',
                'base_unit_active' => CommonConstant::DATABASE_VALUE_YES
            ],
            ['base_unit_id' => 2, 'base_unit_code' => 'meter', 'base_unit_desc' => 'meter',
                'base_unit_active' => CommonConstant::DATABASE_VALUE_YES
            ],
            ['base_unit_id' => 3, 'base_unit_code' => 'kilogram', 'base_unit_desc' => 'kilogram',
                'base_unit_active' => CommonConstant::DATABASE_VALUE_YES
            ],
        ];
        foreach ($baseUnits as $baseUnit) {
            BaseUnit::create($baseUnit);
        }
    }
}
