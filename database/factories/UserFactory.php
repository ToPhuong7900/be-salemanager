<?php

namespace Database\Factories;

use App\Libs\Common\CommonConstant;
use App\Models\SiteGroup;
use App\Models\UserGroup;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $siteGroup = SiteGroup::factory()->create();

        // Ensure the UserGroup belongs to the same SiteGroup.
        $userGroup = UserGroup::factory()->create(['site_group_id' => $siteGroup->getKey()]);
        return [
            'name' => 'Super Admin',
            'email' => 'superadmin@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('admin123'),
            'remember_token' => Str::random(10),
            'superuser' => CommonConstant::DATABASE_VALUE_YES,
            'administrator' => CommonConstant::DATABASE_VALUE_YES,
            'user_group_id' => $userGroup->getKey(),
            'site_group_id' => $siteGroup->getKey(),
            'user_active' => CommonConstant::DATABASE_VALUE_YES
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return $this
     */
    public function unverified(): static
    {
        return $this->state(fn (array $attributes) => [
            'email_verified_at' => null,
        ]);
    }
}
