<?php

namespace Database\Factories;

use App\Libs\Common\CommonConstant;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\UserGroup>
 */
class UserGroupFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'user_group_name' => 'SUPERUSER',
            'user_group_desc' => 'Super User group',
            'user_group_active' => CommonConstant::DATABASE_VALUE_YES
        ];
    }
}
